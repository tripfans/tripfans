#!/bin/bash
#
# tomcat        
#
# chkconfig: 
# description:  Start up the Tomcat servlet engine.

EXEC_DIR=/opt/tomcat/webapps/tripfans/WEB-INF

cd $EXEC_DIR

sudo java -classpath classes:lib/*:/opt/tomcat/lib/* br.com.fanaticosporviagens.util.foto.ConfiguracaoPadraoFotosLocais
sudo chown -R tomcat:tomcat /mnt/fanaticos
