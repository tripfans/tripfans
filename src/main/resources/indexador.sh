#!/bin/bash
#
#

BASE_TOMCAT_DIR=/opt/tomcat
LIB_DIR=$BASE_TOMCAT_DIR/lib
EXEC_DIR=$BASE_TOMCAT_DIR/webapps/ROOT/WEB-INF
SOLR_DIR=/appData/solr

cd $EXEC_DIR

sudo java -classpath classes:lib/*:$LIB_DIR/* br.com.fanaticosporviagens.pesquisaTextual.indexacao.IndexadorEntidades $@
sudo chown -R tomcat:tomcat $SOLR_DIR
