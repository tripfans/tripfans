#!/bin/bash
#
# tomcat        
#
# jobImportacaoAmigosFacebook:  
# description:  Faz a importação de amigos dos usuários conectados ao Facebook

BASE_TOMCAT_DIR=/opt/tomcat
LIB_DIR=$BASE_TOMCAT_DIR/lib
EXEC_DIR=$BASE_TOMCAT_DIR/webapps/ROOT/WEB-INF

cd $EXEC_DIR

sudo java -classpath classes:lib/*:$LIB_DIR/* br.com.fanaticosporviagens.social.jobs.facebook.ImportacaoAmigosFacebookJob
