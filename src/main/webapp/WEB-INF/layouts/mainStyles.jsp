<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}"><link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'></c:when>
<c:otherwise><link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'></c:otherwise>
</c:choose>

<link type="text/css" href="${pageContext.request.contextPath}/resources/styles/jquery-ui/fanaticos/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />
<!-- Bootstrap from Twitter - Base CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/bootstrap/bootstrap-custom.css?<%=System.currentTimeMillis()%>" type="text/css"/>
<%-- 		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/bootstrap/bootstrap.css" type="text/css"/> --%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/BreadCrumb.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.jcarousel/skins/skin.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/fancybox/jquery.fancybox-1.3.4.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.rating/jquery.rating.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/page.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/Base.css?<%=System.currentTimeMillis()%>" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/images/icons/icons.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.gritter.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.autoSuggest/autoSuggest.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/fileuploader/fileuploader.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.jcrop/jquery.Jcrop.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.popover/jquery.popover.tripfans.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/wizard-viagem.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/widget-resumo-viagem2.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/fancy-form.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/map/map-markers.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/interesses/interesses.css" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/styles/nivo-slider/nivo-slider.css" />" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/resources/styles/nivo-slider/themes/default/default.css" />" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/resources/styles/nanoscroller.css" />" type="text/css" media="screen" />

<link rel="stylesheet" href="<c:url value="/resources/components/jquery-ui-multiselect-widget/jquery.multiselect.css" />" type="text/css" />
<link rel="stylesheet" href="<c:url value="/resources/components/jquery-ui-multiselect-widget/jquery.multiselect.filter.css" />" type="text/css" />