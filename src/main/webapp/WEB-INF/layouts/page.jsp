<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/spring-social/facebook/tags" prefix="facebook" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>

<%--
Comentarios condicionais para adicionar classes css extras na tag html.
Isso permite criar css especificos de browsers no mesmo arquivo.
Ex: 
html.ie8 #content,
html.ie9 #content {
    color: #555;
}
<!--[if lt IE 7 ]> <html class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->

### Comentado por enquanto pois continua dando problema no IE
--%>
<html style="min-width: 1024px;">
	<head>
		<title><tiles:getAsString name="title"/></title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <%--meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <%--meta http-equiv="X-UA-Compatible" content="IE=8" /--%>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
        <meta name="keywords" content="<tiles:getAsString name="keywords"/>" />
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico">
        
		<!-- Estilos essenciais -->
		<jsp:include page="/WEB-INF/layouts/mainStyles.jsp" />
        <!--link rel="stylesheet" type="text/css" href="<c:url value="/wro/main.css"/>" /-->
		
		<!-- Esse script foi movido para o topo da página, embora o lugar adequado seja no fim da mesma. Isso foi feito para
		facilitar a inclusão de arquivos externos JSP como componentes (vide o exemplo da página de locais que inclui o form
		de pesquisa de serviços externos). Depois podemos ver como resolver isso. -->
		<jsp:include page="/WEB-INF/layouts/mainScripts.jsp" />
        <!--script type="text/javascript" src="<c:url value="/wro/main.js"/>"></script-->

		
		<tiles:insertAttribute name="stylesheets" />
		
        <style>
            #fancybox-overlay {
                min-width: 1024px;
            }
        </style>
        
        <c:choose>
		<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
		</c:when>
		<c:otherwise>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
		</c:otherwise>
		</c:choose>

	</head>
    
    <c:choose>
      <c:when test="${tripFansEnviroment.develAtWork}">
        <c:set var="bodyStyle" value="" />
        <c:set var="footerBGStyle" value="" />
      </c:when>
      <c:otherwise>
        <c:set var="bodyStyle" value="padding-top: 70px; background-image: url('${pageContext.request.contextPath}/resources/images/ceu-bg.jpg'); background-position: center 70px; background-attachment: fixed; background-repeat: no-repeat; background-color: #e2fafe;" />
        <c:set var="footerBGStyle" value="background-image: url('${pageContext.request.contextPath}/resources/images/mar-bg.jpg'); background-repeat: no-repeat; background-position: center 1px;" />
      </c:otherwise>
    </c:choose>
    
    <body style="${bodyStyle}">
<!-- 	<body style="padding-top: 70px; background-image: url(https://si0.twimg.com/images/themes/theme1/bg.png); background-position: left 40px; background-attachment: fixed; background-repeat: no-repeat; background-color: #C0DEED;">
 -->    
		<div id="header">
		     <%--facebook:init appId="@facebookProvider.appId" /--%>
             <%@include file="header.jsp" %>
		</div>
		
		<div id="loadingContainer">
<%-- 			<img src="<c:url value="/resources/images/ajax-loader.gif" />" class="ajax-loader" /> --%>
		</div>
        
        <div class="container content-box">
		    <tiles:insertAttribute name="body" />
        </div>

        <div style="width: 100%; ${footerBGStyle}">

        <div class="container">
          
            <div id="footer-content" class="container-fluid" style="position: relative; min-height: 100%; padding: 14px 14px 20px; margin: 0 auto; background: url(<c:url value="/resources/images/wash-white-30.png" />);">
            
                <div id="top-button" >
                    <div id="top-button-arrow"></div>
                </div>
                
                <c:if test="${not tripFansEnviroment.develAtWork}">

                <div class="row-fluid borda-arredondada" style="padding-top: 92px;">
                  
                  <div class="row borda-arredondada" style="margin-left: 0px; background-color: rgba(31, 127, 165, 0.35);">
                
                    <div class="span3 section">
                        
                        <div class="footer-box pull-right" style="padding-top: 10px;">
                            <img src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" height="46">
                            <div>TripFans &copy; <%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%></div>
                            <div>Todos os direitos reservados</div>
                        </div>
                        
                        <div align="center" class="centerAligned" style="margin-left: 25px;">
                            <a href="https://www.positivessl.com" style="font-family: arial; font-size: 10px; color: #212121; text-decoration: none;">
                                <img src="https://www.positivessl.com/images-new/PositiveSSL_tl_trans2.gif" alt="SSL Certificate" title="SSL Certificate" border="0" style="height: 60px; padding-top: 20px;" />
                            </a>
                        </div>
                    
                    </div>
                    
                    <div class="span8 section middle">
                    
                      <div style="padding: 10px 15px 10px 20px;">
                    
                        <h2 class="title">Sobre o TripFans®</h2>
                            
                        <ul style="list-style: none;">
                            <li style="text-align: justify; font-size: 15px;">
                                O TripFans ajuda você a organizar sua viagem, tornando-a uma experiência inesquecível!
                            </li>
                            <li>
                                <br/>
                            </li>
                            <li>
                                <a href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />" target="_blank" style="color: #F5FF00; text-decoration: none;">Termos de Uso</a> e 
                                <a href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />" target="_blank" style="color: #F5FF00; text-decoration: none;">Política de privacidade</a> do TripFans.
                            </li>
                        </ul>
                        
                      </div>
                        
                    </div>

					<div class="span4 section middle">
						<div style="padding: 10px 15px 10px 20px;">
							<h2 class="title">O que você pode fazer?</h2>
							 <ul style="font-size: 15px; list-style-type: none; margin-left: 5px;">
                                <li>
                                    <a href="<c:url value="/viagem/planejamento/criar"/>" class="main-menu-item">Planeje sua Viagem</a>
                                </li>
                            </ul>
						</div>
					</div>

                    <div class="span5 section right">
                        
                        <div class="follow-us" style="padding: 10px 15px 10px 25px;">
                            <h2 class="title">Contato</h2>
                            
                            <ul>
                                <li style="vertical-align: middle; list-style: none;">
                                  <a href="mailto:tripfans@tripfans.com.br" style="color: white; text-decoration: none;">
                                    tripfans@tripfans.com.br
                                  </a>
                                </li>
                            </ul>
                            
                            <h2 class="title">Nos acompanhe</h2>
                            
                            <div class="servicos">
                                <ul class="servicos">
                                  <a href="http://www.facebook.com/TripFansBr" target="_blank" style="color: white !important; text-decoration: none !important;">
                                    <li class="facebook" style="vertical-align: middle;">
                                        <b></b>
                                    </li>
                                    <li class="clearfix" style="vertical-align: middle;">
                                        Encontre-nos no Facebook
                                    </li>
                                  </a>
                                </ul>
                            </div>
                            
                            <div class="servicos">
                                <ul class="servicos">
                                  <a href="http://twitter.com/tripfans" target="_blank" style="color: white !important; text-decoration: none !important;">
                                    <li class="twitter" style="vertical-align: middle;">
                                        <b></b>
                                    </li>
                                    <li style="vertical-align: middle;">
                                        Siga-nos no Twitter
                                    </li>
                                  </a>
                                </ul>
                            </div>
                            
                            <%-- div class="servicos">
                                <ul class="servicos">
                                    <li class="google" style="vertical-align: middle;">
                                        <b></b>
                                    </li>
                                    <li style="vertical-align: middle;">
                                        Acesse nosso Blog
                                    </li>
                                </ul>
                            </div--%>
                            
                        </div>
                    </div>
                    
                </div>            
        
      <%--  <div class="container footer-box">
             <div class="left">TripFans &copy; <%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%></div>
            <div class="right">
                <a href="#">Sobre</a>
                ·
                <a href="#">Anuncie</a>
                ·
                <a href="#">Avalie locais</a>
                ·
                <a href="#">Fale conosco</a>
                ·
                <a href="#">Ajuda</a>
            </div>


            </div>
         
         </div> --%>  
         
          </div>   
          
          </c:if>
 
        </div>
        
		<div id="footer">

		
		    <!--  Outros Scripts essenciais -->
			<jsp:include page="/WEB-INF/layouts/otherScripts.jsp" />
            <!--script type="text/javascript" src="<c:url value="/wro/other.js"/>"></script-->
            
			<script type="text/javascript">
			
			function jsDump(objeto, recursividade) {   
			          document.writeln("<ul>")   
			          for ( prop in objeto ) {   
			        	  var tipo = typeof objeto[prop];   
			        	  var valor = objeto[prop]+"";   
			        	  //Eliminando o conteudo das funcoes   
			        	  valor = valor.replace(/\{[^\{]*\}/g,"");   
			        	  document.writeln("<li> ("+tipo+") "+prop+" =>"+valor+"<\/li>");   
			        	  // implementando a recursividade   
			        	  if (recursividade > 0 && tipo ==  "object") {   
			        		  jsDump(objeto[prop],recursividade-1);
			        	  }   
			          }   
			          document.writeln("<\/ul>");   
			    	}
			          
			    	function scrollToTop() {
			   	            $('html, body').animate({scrollTop:0}, 'slow');
			   	        }
			
			$.datepicker.setDefaults({
				autoSize: true,
					changeMonth: true,
					changeYear: true,
					constrainInput: true
			});
			
			jQuery(document).ready(function() {
				$('input, textarea').placeholder();
				
				window.onerror = function(m,u,l){
					//$('#loadingContainer').stop();
					//$('#loadingContainer').fadeOut('fast');
				  return true;
				};
				
				$('body')
				.ajaxSend(function(event, xhr, settings){
					$.fancybox.showActivity();
					//$('#loadingContainer').fadeIn('fast');
				})
				.ajaxComplete(function(event, xhr, settings){
					$.fancybox.hideActivity();
					//$('#loadingContainer').stop();
					//$('#loadingContainer').fadeOut('fast');
                    $('.tooltip').hide();
					var ct = xhr.getResponseHeader("content-type") || "";
				    if(ct.indexOf('json') > -1 && $.evalJSON(xhr.responseText).message !== undefined) {
				    	var response = $.evalJSON(xhr.responseText);
						$.gritter.add({
							title: response.title,
							text: response.message,
							time: 4000,
							image: '<c:url value="/resources/images/" />' + response.imageName + '.png',
						});
					}
					$('body').initToolTips();
				});
				
				<c:if test="${not empty statusMessage}">
				$.gritter.add({
					title: '<c:out value="${title}" />',
					text: '<c:out value="${statusMessage}" />',
					image: '<c:url value="/resources/images/${image}.png" />',
					time: 4000
				});
				</c:if>
			
				$('.container').initToolTips();
				
				$(window).scroll(function() {
					var h = $(window).height();
					var top = $(window).scrollTop();
					if( top > (h * .30)) {
					   $("#top-button").fadeIn("slow");
					} else {
					   $('#top-button').fadeOut('slow');
					}
				});
				
				$('#top-button').click(function (e) {
					scrollToTop();
				});
				
				// -- Configurando widgets do panoramio
				$('#terms-of-service .panoramio-wapi-tos').css('background-color', '').css('color', '');
                $('.panoramio-wapi-photo').css('width', '100%');
				$('.local-card').mouseover(function() {
                    var $div = $(this).find('.origem-foto').show();
                    var $img = $div.find('img');
                    $img.attr('title', $img.data('original-title'));
                    $div.initToolTips();
                });
				$('.local-card').mouseout(function() {
                    var $item = $(this);
                    $item.find('.origem-foto').hide();
                });
				// --
				
				<%-- Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela --%>
				$('#login-dialog-link').show();
				
				$(document).on('click', '.btn-adicionar-local-plano', function (e) {
	                e.preventDefault();
	                var urlLocal = $(this).data('url-local');
	                var idViagem = $(this).data('id-viagem');
	                
	                var url = '<c:url value="/viagem/adicionarLocalEmViagem"/>/' + idViagem + '/' + urlLocal;
	                
	                var request = $.ajax({
	                    url: url, 
	                    type: 'POST' 
	                });
	                request.done(function(data) {
	                });
	                request.fail(function(jqXHR, textStatus) {
	                });
	            });
				
				$(document).on('click', '.btn-criar-novo-plano', function (e) {
                    e.preventDefault();
                    var urlLocal = $(this).data('url-local');
                    
                    var url = '<c:url value="/viagem/adicionarLocalEmNovaViagem"/>/' + urlLocal;
                    
                    var request = $.ajax({
                        url: url, 
                        type: 'POST' 
                    });
                    request.done(function(data) {
                    });
                    request.fail(function(jqXHR, textStatus) {
                    });
                });
				
				var mobileDevice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
				
				if(mobileDevice == false && BrowserDetection.readCookie() != 1) {
					BrowserDetection.init();
					if(BrowserDetection.isOldBrowser()) {
						$('#detectBrowserModal .modal-body').load('<c:url value="/popup/popupBrowserIncompativel" />', 
		           				function(e){
		            				$('#detectBrowserModal').modal({
		            					 	keyboard: false,
		   							  		backdrop: 'static'
		            				});
		            			}
		            		);
					}
				}
				
				// Carregando feedback
				carregarFeedback();
				
			});
			
			
			</script>
			<tiles:insertAttribute name="footer" />
            
            <c:if test="${not tripFansEnviroment.develAtWork}">
              <script type="text/javascript">
                function carregarFeedback() {
                    (function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/N3m2vVMPmtAcdd6aUiueWQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
                    
                    UserVoice = window.UserVoice || [];
                    UserVoice.push(['showTab', 'classic_widget', {
                      mode: 'full',
                      primary_color: '#3cb0e2',
                      link_color: '#007dbf',
                      default_mode: 'support',
                      forum_id: 219672,
                      tab_label: 'Feedback',
                      tab_color: '#e68e00',
                      tab_position: 'middle-right',
                      tab_inverted: false
                    }]);
                }
              </script>
            </c:if>
		
		</div>
		
		</div>	
		
		 <div id="detectBrowserModal" class="modal hide fade" role="dialog">
		  <div class="modal-dialog">
		    <div class="modal-content">
		    	<div class="modal-header">
		        	<h1>Ops! Temos um problema</h1>
		      	</div>
		      	<div class="modal-body">
		    		 <p>Aguarde...</p>
		    	</div>
		    </div>
		  </div>
		</div>
		
<c:if test="${usuario == null }">
<div id="fb-root"></div>
</c:if>


	</body>
</html>
