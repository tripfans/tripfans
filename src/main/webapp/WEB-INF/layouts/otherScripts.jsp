<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.form.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-validation-1.9.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.form.wizard.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.popover/jquery.popover-1.1.0-tripfans.js"></script>

<!--  Necessario colocar antes da configuracao de localizacao -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-addon.js" charset="utf-8"></script>

<c:choose>
<c:when test="${pageContext.response.locale.language eq 'pt'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-pt-BR.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-pt-BR.js" ></script>
</c:when>
<c:when test="${pageContext.response.locale.language eq 'es'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-es.js"></script>
</c:when>
</c:choose>

<!-- Bootstrap from Twitter - Javascripts -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-alerts.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-buttons.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-modal.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-collapse.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-dropdown.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-tooltips.js"></script>
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-popover.js"></script> --%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-tabs.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-carousel.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-pagination.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-transition.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-scrollspy.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bootstrap/bootstrap-affix.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.raty/jquery.raty.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/fancy-form.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.inputmask.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.inputmask.extensions.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.inputmask.date.extensions.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.inputmask.numeric.extensions.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.meiomask.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/stickysidebar.jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.json-2.3.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.fanaticos.autocomplete.js?<%=System.currentTimeMillis()%>"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/fileuploader/fileuploader.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/jquery.jcrop/jquery.Jcrop.min.js"></script>
<!-- script src="${pageContext.request.contextPath}/resources/scripts/jquery.jflow/jquery.jflow.js"></script>
<script src="${pageContext.request.contextPath}/resources/scripts/jquery.scrollTo-min.js"></script-->

<script src="${pageContext.request.contextPath}/resources/scripts/jquery.verticalCarousel/jquery.verticalCarousel.min.js"></script>

<c:choose>
<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}"><script type="text/javascript" src="https://www.google.com/jsapi"></script></c:when>
<c:otherwise><script type="text/javascript" src="http://www.google.com/jsapi"></script></c:otherwise>
</c:choose>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.gritter.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.typewatch.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.autoSuggest/jquery.autoSuggest.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.lazyload.js"></script>

<%--script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/footer_plugin.js"></script--%>
<script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.cssAnimate.mini.js" />"></script>
<script src="<c:url value="/resources/scripts/nivo-slider/jquery.nivo.slider.js" />" type="text/javascript"></script>

<script src="<c:url value="/resources/scripts/jquery.scrollTo-1.4.3.1.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.nanoscroller.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/resources/scripts/jquery.ddslick.min.js" />" type="text/javascript"></script>

<script src="<c:url value="/resources/components/jquery-ui-multiselect-widget/jquery.multiselect.filter.min.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/components/jquery-ui-multiselect-widget/jquery.multiselect.js" />" type="text/javascript"></script>

<script src="<c:url value="/resources/scripts/jquery.history.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.lockfixed.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.placeholder.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/browser-detection/browser-detection.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.cookie.js" />" type="text/javascript"></script>