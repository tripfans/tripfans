<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.form.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-validation-1.9.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.form.wizard.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.popover/jquery.popover-1.1.0-tripfans.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.json-2.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/modernizr.js"></script>


<!--  Necessario colocar antes da configuracao de localizacao -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-addon.js" charset="utf-8"></script>

<c:choose>
<c:when test="${pageContext.response.locale.language eq 'pt'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-pt-BR.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-pt-BR.js" ></script>
</c:when>
<c:when test="${pageContext.response.locale.language eq 'es'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-es.js"></script>
</c:when>
</c:choose>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/hammer/jquery.hammer.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/components/mmenu/jquery.mmenu.min.all.js"></script>
<%-- script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/hammer/hammer.js"></script--%>

<!-- Bootstrap from Twitter - Javascripts -->

<%-- script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/fancybox/jquery.easing-1.3.pack.js"></script--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.raty/jquery.raty.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.fanaticos.autocomplete.js?<%=System.currentTimeMillis()%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.gritter.js"></script>


<script src="<c:url value="/resources/scripts/jquery.history.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.lockfixed.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.placeholder.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/browser-detection/browser-detection.js" />" type="text/javascript"></script>
<script src="<c:url value="/resources/scripts/jquery.cookie.js" />" type="text/javascript"></script>