<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<script type="text/javascript">
function selecionaLocal(event, ui) { 
	  document.location.href= '${pageContext.request.contextPath}' + ui.item.url;
}
</script>

<c:if test="${not tripFansEnviroment.develAtWork}">
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner topnavbar">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            
            <a class="brand" href="<c:url value="/"/>">
                <img class="img" src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" style="padding-top: 2px;" alt="">
            </a>
            
            <div class="nav-collapse collapse">
            
                <ul class="nav">
                	<c:if test="${usuario != null}">
                        <li class="">
       	                  <fan:notificationsButton id="btnNotificacoes" user="${usuario.urlPath}" notifications="${notificacoesNaoVisualizadas}"/>
       	             	</li>
                    </c:if>
                </ul>
                
                    <ul class="nav pull-right" style="height: 43px;">
                        <li class="dropdown">
                        
                            <c:if test="${usuario != null}">
                            
                                <a href="login" id="profile" class="dropdown-toggle" data-toggle="dropdown">
                                	<%-- <img class="avatar" style="margin-top: -20px;" src="<c:url value="${usuario.urlFotoPerfil}" />"  /> --%>
                                    <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" imgStyle="margin-top: -20px;" />
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                
                                    <c:choose>
                                        <c:when test="${usuario.confirmado}">
                                            <li>
                                            	<c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                                <a href="${urlPerfil}">
                                                <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                                <br/>
                                                <small class="small muted">Visualizar Perfil</small>
                                                </a>
                                            </li>
                                             <li class="divider"></li>
				                            <li>
				                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
				                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a>
                                            </li>
                                            <li>
                                                <a href="<c:url value="/conta/config" />">Configurações da Conta</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li>
                                                <i><strong style="color: #b94a48; padding: 3px 15px; white-space: nowrap;">Você precisa confirmar seu email.</strong></i>
                                                <a href="<c:url value="/reenviarEmailConfirmacao/${usuario.urlPath}" />">Reenviar email de confirmação</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                            	<c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                                <a href="${urlPerfil}">
                                                <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                                <br/>
                                                <small class="small muted">Visualizar Perfil</small>
                                                </a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<c:url value="/signout" />">Sair</a>
                                    </li>
        
                                </ul>
                                
                            </c:if>
        
                            <c:if test="${usuario == null}">
                                <a id="login-dialog-link" href="<c:url value="/entrarPop" />" class="dropdown-toggle main-menu-item" data-toggle="dropdown" style="display: none">
                                    <span><s:message code="label.login" /></span>
                                </a>
                            </c:if>
                        
                        </li>
                    </ul>
            
            
            </div>
        </div>
    </div>
</div>
</c:if>

<div id="modal-login" class="modal hide fade" role="dialog" style="top: 40%;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title centerAligned">Entrar no <img src="<c:url value="/resources/images/logos/tripfans-t.png"/>" height="18" /></h3>
      </div>
      <div class="modal-body" style="min-height: 515px;">
        <div id="modal-login-body"></div>
      </div>
    </div>
  </div>
</div>