<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE html>
<html>
	<head>
		<jsp:include page="/WEB-INF/layouts/mainStyles.jsp" />
		<jsp:include page="/WEB-INF/layouts/mainScripts.jsp" />
	</head>
	
	<body>
		<div>
		    <tiles:insertAttribute name="body" />
        </div>	
				
		<jsp:include page="/WEB-INF/layouts/otherScripts.jsp" />
		<script type="text/javascript">
			$.datepicker.setDefaults({
				autoSize: true,
		 		changeMonth: true,
		 		changeYear: true,
		 		constrainInput: true
			});
			
			jQuery(document).ready(function() {
				$(function() {
					$( "input:submit, button" ).button();
				});
			});
		</script>	
	</body>
</html>
