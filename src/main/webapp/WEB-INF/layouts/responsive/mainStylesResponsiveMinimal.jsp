<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:choose>
<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
</c:when>
<c:otherwise>
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
</c:otherwise>
</c:choose>


<!-- Bootstrap 3 from Twitter - Base CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/components/bootstrap/bootstrap3/css/bootstrap.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/bootstrap-custom.css" type="text/css"/>
<link type="text/css" href="${pageContext.request.contextPath}/resources/styles/jquery-ui/fanaticos/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/jquery.popover/jquery.popover.tripfans-responsive.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/components/mmenu/jquery.mmenu.all.css" type="text/css" media="screen, projection"/>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/pageResponsive.css" type="text/css" media="screen, projection"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsive.css?<%=System.currentTimeMillis()%>" type="text/css" media="screen, projection"/>
<c:choose>
  <c:when test="${isMobile}">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection"/>
  </c:when>
  <c:otherwise>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection"/>
  </c:otherwise>
</c:choose>