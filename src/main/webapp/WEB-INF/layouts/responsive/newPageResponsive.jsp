<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib uri="http://www.springframework.org/spring-social/facebook/tags" prefix="facebook" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">

    <c:choose>
        <c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
            <c:set var="reqScheme" value="https" />
        </c:when>
        <c:otherwise>
            <c:set var="reqScheme" value="http" />
        </c:otherwise>
    </c:choose>

    <head>
        <title><tiles:getAsString name="title"/></title>
        <%-- Disable zooming capabilities on mobile devices: "user-scalable=no" 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        --%>
        <c:if test="${not isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        </c:if>
        <c:if test="${isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
        </c:if>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="keywords" content="<tiles:getAsString name="keywords"/>" />
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico" />
        
        <tiles:insertAttribute name="stylesheets" >
            <link rel="stylesheet" href="<c:url value="/wro/mainResponsive.css" />" type="text/css" />
            
            <link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css' />
            <link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css' />
            
            <c:choose>
              <c:when test="${isMobile}">
                <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection"/>
              </c:when>
              <c:otherwise>
                <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection"/>
              </c:otherwise>
            </c:choose>

            <style>
                @media (max-width: 767px) {
                    .page-container {
                        min-height: 400px;
                    }
                }
                @media (min-width: 768px) {
                    .page-container {
                        min-height: 486px;
                    }
                }
                
                .body {
                    background-image: initial;
                    background-attachment: fixed;
                    background-repeat: no-repeat;
                    background-color: #ffffff;
                }
                
            </style>
            
            <script type="text/javascript" src="<c:url value="/wro/mainResponsive.js${tripFansEnviroment.devel ? '?minimize=false' : ''}"/>" ></script>

        </tiles:insertAttribute>
        
        
    </head>
    
    <body class="body" ${empty showHeader or showHeader ? '' : 'style="padding-top: 0px;"'}>
    
      <div>

	    <c:if test="${empty showHeader or showHeader}">
          <div id="header">
               <%@include file="newTopbarResponsive.jsp" %>
          </div>
        </c:if>
        
        <div class="container">
	        
	        <c:if test="${not empty usuario and !usuario.confirmado}" >
				<div class="alert alert-warning alert-dismissible" style="margin-top: 10px; font-size: 15px;" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<div style="text-align: center;">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<strong>Atenção, ${usuario.displayName}:</strong> Acesse o seu e-mail ${usuario.username} para confirmar o seu cadastro.
						<br/>
						<a class="resendConfirmEmail" href="<c:url value="/reenviarEmailConfirmacao/${usuario.urlPath}" />">Reenviar email de confirmação</a>
						
					</div>
				</div>
			</c:if>
        
            <div class="row page-container" style="background-color: white; ${empty showHeader or showHeader ? '' : 'border: none;'}">
            
                <div id="left-menu-wrapper" style="${isMobile ? 'display: none;' : ''}">
                  <tiles:insertAttribute name="leftMenu" />
                </div>
                
                <tiles:insertAttribute name="body" />

            </div>
        
            <c:if test="${isMobile}">
              <nav id="right-menu" style="display: none;">
                <ul>
                    <li class="img">
                        <a href="#">
                            <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" />
                            ${usuario.displayName}
                            <br>
                            <small>${usuario.username}</small>
                        </a>
                        <ul>
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                    <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                           <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/config" />">Configurações da Conta</a>
                            </li>
                            <li class="divider"/>
                            <li>
                                <a href="<c:url value="/signout" />">Sair</a>
                            </li>
                        </ul>
                    </li>
                </ul>
              </nav>
            </c:if>
        
        </div>
        
        <c:if test="${empty showFooter or showFooter}">
            <jsp:include page="footer.jsp" />
        </c:if>
        
            <div id="footer">
            
                <tiles:insertAttribute name="footer" >
                	
					<script type="text/javascript">
               		<c:if test="${not tripFansEnviroment.devel}">
					      var _gaq = _gaq || [];
					      _gaq.push(['_setAccount', 'UA-43564931-1']);
					      _gaq.push(['_trackPageview']);
					    
					      (function() {
					        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					      })();
					</c:if>
					
					$(document).on('click', '.btn-adicionar-local-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        var idViagem = $(this).data('id-viagem');
                        
                        var url = '<c:url value="/viagem/adicionarLocalEmViagem"/>/' + idViagem + '/' + urlLocal;
                        
                        var request = $.ajax({
                            url: url, 
                            type: 'POST' 
                        });
                        request.done(function(data) {
                        });
                        request.fail(function(jqXHR, textStatus) {
                        });
                    });
                    
                    $(document).on('click', '.btn-criar-novo-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        
                        <c:choose>
                          <c:when test="${not empty usuario}">
                            var url = '<c:url value="/viagem/adicionarLocalEmNovaViagem"/>/' + urlLocal;
                            var request = $.ajax({
                                url: url, 
                                type: 'POST'
                            });
                            request.done(function(data) {
                            });
                            request.fail(function(jqXHR, textStatus) {
                            });
                          </c:when>
                          <c:otherwise>
                            $('#modal-login-body').load('<c:url value="/entrarPop"/>?to=<c:url value="/viagem/adicionarLocalEmNovaViagem"/>/' + urlLocal, function() {
                              $('#modal-login').modal('show');
                            });
                          </c:otherwise>
                        </c:choose>
                        
                    });
                    
                    function scrollToTop() {
                        $('html, body').animate({scrollTop:0}, 'slow');
                    }
                    
                    $(document).ready(function() {
                    
                        // Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela
                        $('.login-modal-link').show();
                        
                        <%--c:if test="${isMobile}">
                        // Fechar automaticamente os tooltips em browsers mobile
                        $('.tooltip').on('shown.bs.tooltip', function (e) {
                            var trg = e.target
                            setTimeout(function() {
                                $(trg).tooltip('hide');
                            }, 3000);
                        });
                        
                        $('*').bind('touchend', function(e){
                           if ($(e.target).attr('rel') !== 'tooltip' && ($('div.tooltip.in').length > 0)){
                             $('[rel=tooltip]').mouseleave();
                             e.stopPropagation();
                           } else {
                             $(e.target).mouseenter();
                           }
                        });
                        </c:if--%>

                        // Fechar o modal ao clicar no backButton do browser
                        $(document).on('show.bs.modal', 'div.modal', function (e) {
                            var modal = this;
                            var hash = modal.id;
                            window.location.hash = hash;
                            window.onhashchange = function() {
                                if (!location.hash){
                                    $(modal).modal('hide');
                                }
                            }
                        });
                        $(document).on('hide', 'div.modal', function (e) {
                            var hash = this.id;
                            history.pushState('', document.title, window.location.pathname);
                        });
                        //
                        
                        window.onerror = function(m,u,l) {
                            return true;
                        };
                        
                        <c:if test="${not isMobile}">
                          $('.container').initToolTips();
                        </c:if>
                        
                        $(window).scroll(function() {
                            var h = $(window).height();
                            var top = $(window).scrollTop();
                            if( top > (h * .30)) {
                               $("#top-button").fadeIn("slow");
                            } else {
                               $('#top-button').fadeOut('slow');
                            }
                        });
                        
                        $(document).on('click', '#top-button', function (e) {
                            scrollToTop();
                        });
                        
                        $(document).on('mouseover', 'div.card-content', function (e) {
                            var $div = $(this).find('.origem-foto').show();
                            var $img = $div.find('img');
                            $img.attr('title', $img.data('original-title'));
                            $div.initToolTips();
                        });
                        
                        $(document).on('mouseout', 'div.card-content', function (e) {
                            var $item = $(this);
                            $item.find('.origem-foto').hide();
                        });
                        
                        $('.login-modal-link').click(function(e) {
                            e.preventDefault();
                            $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                                $('#modal-login').modal('show');
                            });
                        });
                        
                        $('a.resendConfirmEmail').click(function(e){
                        	e.preventDefault();
                        	var link = $(this);
                        	$.get(link.attr('href'), function(response) {
        						$.gritter.add({
        							title: response.title,
        							text: response.message,
        							time: 4000,
        							image: '<c:url value="/resources/images/" />' + response.imageName + '.png',
        						});
                        	});
                        });
                        
                    });
                    
                    <c:if test="${not isMobile}">
                      function carregarFeedback() {
                          (function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/N3m2vVMPmtAcdd6aUiueWQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
                          
                          UserVoice = window.UserVoice || [];
                          UserVoice.push(['showTab', 'classic_widget', {
                            mode: 'full',
                            primary_color: '#3cb0e2',
                            link_color: '#007dbf',
                            default_mode: 'support',
                            forum_id: 219672,
                            tab_label: 'Feedback',
                            tab_color: '#e68e00',
                            tab_position: 'middle-right',
                            tab_inverted: false
                          }]);
                      }
                  </c:if>
					</script>
					
                </tiles:insertAttribute>
            </div>
        </div>

    </body>
</compress:html>
</html>