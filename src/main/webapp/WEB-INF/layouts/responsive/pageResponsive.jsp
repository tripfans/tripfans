<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib uri="http://www.springframework.org/spring-social/facebook/tags" prefix="facebook" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<!DOCTYPE html>
<html>
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
	<head>
		<title><tiles:getAsString name="title"/></title>
        <%-- Disable zooming capabilities on mobile devices: "user-scalable=no" 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        --%>
        <c:if test="${not isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </c:if>
        <c:if test="${isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        </c:if>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="keywords" content="<tiles:getAsString name="keywords"/>" />
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico">
        
		<!-- Estilos essenciais -->
		<jsp:include page="mainStylesResponsive.jsp" />
		
		<!-- Esse script foi movido para o topo da página, embora o lugar adequado seja no fim da mesma. Isso foi feito para
		facilitar a inclusão de arquivos externos JSP como componentes (vide o exemplo da página de locais que inclui o form
		de pesquisa de serviços externos). Depois podemos ver como resolver isso. -->
		<jsp:include page="mainScriptsResponsive.jsp" />

		<tiles:insertAttribute name="stylesheets" />
        
        <style>
            @media (max-width: 767px) {
                .page-container {
                    min-height: 400px;
                }
            }
            @media (min-width: 768px) {
                .page-container {
                    min-height: 486px;
                }
            }
        </style>

	</head>
    
    <body class="body">

		<div id="header">
             <%@include file="headerResponsive.jsp" %>
		</div>
		
        <div class="container">
            
            <div class="row page-container" style="background-color: white;">
            
                <div id="left-menu-wrapper" style="${isMobile ? 'display: none;' : ''}">
                  <tiles:insertAttribute name="leftMenu" />
                </div>
                
                <tiles:insertAttribute name="body" />

            </div>
            
            <c:if test="${isMobile}">
              <nav id="right-menu" style="display: none;">
                <ul>
                    <li class="img">
                        <a href="#">
                            <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" />
                            ${usuario.displayName}
                            <br>
                            <small>${usuario.username}</small>
                        </a>
                        <ul>
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                    <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/config" />">Configurações da Conta</a>
                            </li>
                            <li class="divider"/>
                            <li>
                                <a href="<c:url value="/signout" />">Sair</a>
                            </li>
                        </ul>
                    </li>
                </ul>
              </nav>
            </c:if>
        
        </div>
        
        <footer class="bs-footer visible-md visible-lg" role="contentinfo">
        <c:if test="${not isMobile}">
          <div class="footer">
            <div class="container" style="padding-top: 110px; position: relative; min-height: 100%; margin: 0 auto; background: url(<c:url value="/resources/images/wash-white-30.png" />);">
             <c:if test="${not tripFansEnviroment.develAtWork}">
              <div class="row borda-arredondada" style="margin: 0px 0px 10px 0px; background-color: rgba(31, 127, 165, 0.35);">
                <div id="footer-content" class="container-fluid">
            
                    <div id="top-button" >
                        <div id="top-button-arrow"></div>
                    </div>
                    
                    <div class="col-md-2 section">
                        <div class="footer-box pull-right" style="padding-top: 10px;">
                            <img src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" height="46">
                            <div>TripFans &copy; <%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%></div>
                            <div>Todos os direitos reservados</div>
                        </div>
                    </div>
                    
                    <div class="col-md-4 section">
                          <div>
                            <h2 class="title">Sobre o TripFans®</h2>
                            <ul style="list-style: none;">
                                <li style="text-align: justify; font-size: 15px;">
                                    O TripFans ajuda você a organizar sua viagem, tornando-a uma experiência inesquecível!
                                </li>
                                <li>
                                    <br/>
                                </li>
                                <li>
                                    <a href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />" target="_blank" style="color: #F5FF00; text-decoration: none;">Termos de Uso</a> e 
                                    <a href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />" target="_blank" style="color: #F5FF00; text-decoration: none;">Política de privacidade</a> do TripFans.
                                </li>
                            </ul>
                          </div>
                    </div>
                    
                    <div class="col-md-3 section">
    
                        <div style="padding: 10px 15px 10px 20px;">
                            <h2 class="title">O que você pode fazer?</h2>
							 <ul style="font-size: 15px; list-style-type: none; margin-left: 5px;">
                                <li>
                                    <a href="<c:url value="/viagem/planejamento/criar"/>" class="main-menu-item">Planeje sua Viagem</a>
                                </li>
                            </ul>
                        </div>
                    </div>
    
                    <div class="col-md-3 section">
                            
                        <div class="follow-us" style="padding: 10px 15px 10px 25px;">
                            <h2 class="title">Contato</h2>
                            
                            <ul>
                                <li style="vertical-align: middle; list-style: none;">
                                  <a href="mailto:tripfans@tripfans.com.br" style="color: white; text-decoration: none;">
                                    tripfans@tripfans.com.br
                                  </a>
                                </li>
                            </ul>
                            
                            <h2 class="title">Nos acompanhe</h2>
                            
                            <div class="servicos">
                                <ul class="servicos">
                                  <a href="http://www.facebook.com/TripFansBr" target="_blank" style="color: white !important; text-decoration: none !important;">
                                    <li class="facebook" style="vertical-align: middle;">
                                        <b></b>
                                    </li>
                                    <li class="clearfix" style="vertical-align: middle;">
                                        Encontre-nos no Facebook
                                    </li>
                                  </a>
                                </ul>
                            </div>
                            
                            <div class="servicos">
                                <ul class="servicos">
                                  <a href="http://twitter.com/tripfans" target="_blank" style="color: white !important; text-decoration: none !important;">
                                    <li class="twitter" style="vertical-align: middle;">
                                        <b></b>
                                    </li>
                                    <li style="vertical-align: middle;">
                                        Siga-nos no Twitter
                                    </li>
                                  </a>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
               </c:if>
              </div>
            </c:if>
            </div>
            
            <div id="footer">
        
                <!--  Outros Scripts essenciais -->
                <jsp:include page="/WEB-INF/layouts/otherScriptsResponsive.jsp" />
                
                <script type="text/javascript">
                
                function jsDump(objeto, recursividade) {   
                    document.writeln("<ul>")   
                    for ( prop in objeto ) {   
                        var tipo = typeof objeto[prop];   
                        var valor = objeto[prop]+"";   
                        //Eliminando o conteudo das funcoes   
                        valor = valor.replace(/\{[^\{]*\}/g,"");   
                        document.writeln("<li> ("+tipo+") "+prop+" =>"+valor+"<\/li>");   
                        // implementando a recursividade   
                        if (recursividade > 0 && tipo ==  "object") {   
                            jsDump(objeto[prop],recursividade-1);
                        }  
                    }   
                    document.writeln("<\/ul>");   
                }
                          
                function scrollToTop() {
                    $('html, body').animate({scrollTop:0}, 'slow');
                }
                
                $.datepicker.setDefaults({
                    autoSize: true,
                        changeMonth: true,
                        changeYear: true,
                        constrainInput: true
                });
                
                jQuery(document).ready(function() {
                    $('input, textarea').placeholder();
                    
                    window.onerror = function(m,u,l){
                        //$('#loadingContainer').stop();
                        //$('#loadingContainer').fadeOut('fast');
                      return true;
                    };
                    
                    <c:if test="${not empty statusMessage}">
                    $.gritter.add({
                        title: '<c:out value="${title}" />',
                        text: '<c:out value="${statusMessage}" />',
                        image: '<c:url value="/resources/images/${image}.png" />',
                        time: 4000
                    });
                    </c:if>
                
                    $('.container').initToolTips();
                    
                    $(window).scroll(function() {
                        var h = $(window).height();
                        var top = $(window).scrollTop();
                        if( top > (h * .30)) {
                           $("#top-button").fadeIn("slow");
                        } else {
                           $('#top-button').fadeOut('slow');
                        }
                    });
                    
                    $('#top-button').click(function (e) {
                        scrollToTop();
                    });
                    
                    // -- Configurando widgets do panoramio
                    $('#terms-of-service .panoramio-wapi-tos').css('background-color', '').css('color', '');
                    $('.panoramio-wapi-photo').css('width', '100%');
                    $('.local-card').mouseover(function() {
                        var $div = $(this).find('.origem-foto').show();
                        var $img = $div.find('img');
                        //$img.attr('title', $img.data('original-title'));
                        //$div.initToolTips();
                    });
                    $('.local-card').mouseout(function() {
                        var $item = $(this);
                        $item.find('.origem-foto').hide();
                    });
                    // --
                    
                    $('.login-modal-link').click(function(e) {
                        e.preventDefault();
                        $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                            $('#modal-login').modal('show');
                        });
                    });
                    
                    <%-- Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela --%>
                    $('.login-modal-link').show();
                    
                    // Carregando feedback
                    carregarFeedback();
                    
                    if (BrowserDetection.readCookie() != 1) {
                        BrowserDetection.init();
                        if(BrowserDetection.isOldBrowser()) {
                            $('#detectBrowserModal').modal({
                                  keyboard: false,
                                  backdrop: 'static'
                             });
                        }
                    }
                });
                
                </script>
                <tiles:insertAttribute name="footer" />
                
                <c:if test="${not isMobile}">
                  <script type="text/javascript">
                    function carregarFeedback() {
                        (function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/N3m2vVMPmtAcdd6aUiueWQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
                        
                        UserVoice = window.UserVoice || [];
                        UserVoice.push(['showTab', 'classic_widget', {
                          mode: 'full',
                          primary_color: '#3cb0e2',
                          link_color: '#007dbf',
                          default_mode: 'support',
                          forum_id: 219672,
                          tab_label: 'Feedback',
                          tab_color: '#e68e00',
                          tab_position: 'middle-right',
                          tab_inverted: false
                        }]);
                    }
                  </script>
                </c:if>
              </div>
            </div>
        </footer>

	</body>
</compress:html>
</html>
