<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/spring-social/facebook/tags" prefix="facebook" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<!DOCTYPE html>
<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
        <%-- Disable zooming capabilities on mobile devices: "user-scalable=no" 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        --%>
        <c:if test="${not isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </c:if>
        <c:if test="${isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        </c:if>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="keywords" content="<tiles:getAsString name="keywords"/>" />
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico">
        
		<!-- Estilos essenciais -->
		<jsp:include page="mainStylesResponsiveMinimal.jsp" />
		
		<!-- Esse script foi movido para o topo da página, embora o lugar adequado seja no fim da mesma. Isso foi feito para
		facilitar a inclusão de arquivos externos JSP como componentes (vide o exemplo da página de locais que inclui o form
		de pesquisa de serviços externos). Depois podemos ver como resolver isso. -->
		<jsp:include page="mainScriptsResponsive.jsp" />

		<tiles:insertAttribute name="stylesheets" />
        
        <style>
            @media (max-width: 767px) {
                .page-container {
                    min-height: 400px;
                }
            }
            @media (min-width: 768px) {
                .page-container {
                    min-height: 486px;
                }
            }
        </style>

	</head>
    
    <body class="body">

		<div id="header">
             <%@include file="headerResponsive.jsp" %>
		</div>
		
        <div class="container">
            
            <div class="row page-container" style="background-color: white;">
            
                <div id="left-menu-wrapper" style="${isMobile ? 'display: none;' : ''}">
                  <tiles:insertAttribute name="leftMenu" />
                </div>
                
                <tiles:insertAttribute name="body" />

            </div>
            
            <c:if test="${isMobile}">
              <nav id="right-menu" style="display: none;">
                <ul>
                    <li class="img">
                        <a href="#">
                            <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" />
                            ${usuario.displayName}
                            <br>
                            <small>${usuario.username}</small>
                        </a>
                        <ul>
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                    <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                           <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/config" />">Configurações da Conta</a>
                            </li>
                            <li class="divider"/>
                            <li>
                                <a href="<c:url value="/signout" />">Sair</a>
                            </li>
                        </ul>
                    </li>
                </ul>
              </nav>
            </c:if>
        
        </div>
        
        <footer class="bs-footer" role="contentinfo">

		<div id="top-button">
			<div id="top-button-arrow"></div>
		</div>

		<div style="background-color: #A5A5A5; padding: 6px;">
			<div class="container">
				<div class="col-xs-4" style="text-align: center;">
					<a href="#" style="color: white;"> Sobre nós </a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="mailto:tripfans@tripfans.com.br" style="color: white;"> Contate-nos </a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="http://blog.tripfans.com.br" target="_blank" style="color: white;"> Blog </a>
				</div>
			</div>
		</div>

		<div style="background-color: rgb(60, 175, 226); padding: 15px;">
			<div class="container">
				<div class="col-xs-2" style="text-align: center;">
					<a href="http://www.facebook.com/TripFansBr" target="_blank" class="footer-social-icon facebook"> F </a>
				</div>
				<div class="col-xs-2" style="text-align: center;">
					<a href="http://twitter.com/tripfans" target="_blank" class="footer-social-icon twitter"> L </a>
				</div>
				<div class="col-xs-2" style="text-align: center;">
					<a href="https://plus.google.com/u/0/b/109665924220757301723/109665924220757301723" target="_blank" class="footer-social-icon google"> G </a>
				</div>
				<div class="col-xs-2" style="text-align: center;">
					<a href="#" target="_blank" class="footer-social-icon foursquare"> / </a>
				</div>
				<div class="col-xs-2" style="text-align: center;">
					<a href="https://www.youtube.com/user/tripfansbr" target="_blank" class="footer-social-icon youtube"> X </a>
				</div>
				<div class="col-xs-2" style="text-align: center;">
					<a href="http://www.pinterest.com/tripfans" target="_blank" class="footer-social-icon pinterest"> : </a>
				</div>
			</div>
		</div>

		<div style="background-color: #D1D1D1; padding: 6px;">
			<div class="container">
				<div class="col-xs-4" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Termos de uso </a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Política de privacidade </a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="#" style="color: #7c7c7c;"> Versão Desktop </a>
				</div>
			</div>
		</div>

		<div style="background-color: #f5f5f5; padding: 6px;">
			<div class="container" style="text-align: center; color: rgb(185, 185, 185); font-size: 11px;">
				&copy; 2012-<%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%>
				TripFans - Todos os direitos reservados.
			</div>
		</div>

		<div id="footer">

			<tiles:insertAttribute name="footer" />

			<script>
                    $(document).on('click', '.btn-adicionar-local-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        var idViagem = $(this).data('id-viagem');
                        
                        var url = '<c:url value="/viagem/adicionarLocalEmViagem"/>/' + idViagem + '/' + urlLocal;
                        
                        var request = $.ajax({
                            url: url, 
                            type: 'POST' 
                        });
                        request.done(function(data) {
                        });
                        request.fail(function(jqXHR, textStatus) {
                        });
                    });
                    
                    $(document).on('click', '.btn-criar-novo-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        
                        var url = '<c:url value="/viagem/adicionarLocalEmNovaViagem"/>/' + urlLocal;
                        
                        var request = $.ajax({
                            url: url, 
                            type: 'POST' 
                        });
                        request.done(function(data) {
                        });
                        request.fail(function(jqXHR, textStatus) {
                        });
                    });
                    
                    $(document).ready(function() {
                    
                        // Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela
                        $('.login-modal-link').show();
                        
                        window.onerror = function(m,u,l){
                            return true;
                        };
                        
                        <c:if test="${not isMobile}">
                          $('.container').initToolTips();
                        </c:if>
                        
                        $(window).scroll(function() {
                            var h = $(window).height();
                            var top = $(window).scrollTop();
                            if( top > (h * .30)) {
                               $("#top-button").fadeIn("slow");
                            } else {
                               $('#top-button').fadeOut('slow');
                            }
                        });
                        
                        $('#top-button').click(function (e) {
                            scrollToTop();
                        });
                        
                        $('.login-modal-link').click(function(e) {
                            e.preventDefault();
                            $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                                $('#modal-login').modal('show');
                            });
                        });
                    });
                </script>

			<c:if test="${not isMobile}">
				<script type="text/javascript">
                    function carregarFeedback() {
                        (function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/N3m2vVMPmtAcdd6aUiueWQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
                        
                        UserVoice = window.UserVoice || [];
                        UserVoice.push(['showTab', 'classic_widget', {
                          mode: 'full',
                          primary_color: '#3cb0e2',
                          link_color: '#007dbf',
                          default_mode: 'support',
                          forum_id: 219672,
                          tab_label: 'Feedback',
                          tab_color: '#e68e00',
                          tab_position: 'middle-right',
                          tab_inverted: false
                        }]);
                    }
                  </script>
			</c:if>
		</div>
	</footer>
        
	</body>
</html>
