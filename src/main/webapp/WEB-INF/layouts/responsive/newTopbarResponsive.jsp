<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<script type="text/javascript">
    $(function() {
    	
        <c:if test="${not isMobile}">
            $('#left-menu').affix({
                offset: {
                  top: 0,
                  bottom: function () {
                    return (this.bottom = $('.bs-footer').outerHeight(true))
                  }
                }
            });
        </c:if>
        
        <c:if test="${isMobile}">
            $('#left-menu').mmenu({
                dragOpen: true,
                classes: 'mm-light mm-left'
                
            }).on('opened.mm',
                function() {
                   setTimeout(function() {
                       $('a.mmenu-left').addClass('opened');
                   }, 500);
                }
            ).on('closed.mm',
                function() {
                   $('a.mmenu-left').removeClass('opened');
                }
            );
            
            $('a.mmenu-left').click(function (e) {
                if ($(this).hasClass('opened')) {
                    $('#left-menu').mmenu().trigger('close.mm');
                }
            });

            $('a.header-right-menu').click(function (e) {
                if ($(this).hasClass('opened')) {
                    $('#right-menu').mmenu().trigger('close.mm');
                }
            });
            
            var $menu = $('#right-menu');
    
            $menu.mmenu({
                position: 'right',
                classes: 'mm-light',
                counters: false,
                searchfield: false,
                slidingSubmenus: false,
                /*header: {
                    add: true,
                    update: true,
                    title: '' 
                },*/
                dragOpen: true
            }).on('opened.mm',
                    function() {
                setTimeout(function() {
                    $('a.header-right-menu').addClass('opened');
                }, 500);
             }
             ).on('closed.mm',
                 function() {
                    $('a.header-right-menu').removeClass('opened');
                 }
             );
            
            $menu.css('display', '');
            
            $('.collapse-all-navbar').click(function (e) {
                if ($('#nav-menu-search').hasClass('in')) {
                    $('#btn-nav-menu-search').click();
                }
                if ($('#nav-menu-opcoes').hasClass('open')) {
                    $('#nav-menu-opcoes').removeClass('open')
                }
            });
            
            $('#btn-nav-menu-search').click(function (e) {
                setTimeout(function() {
                    $('#caixaPesquisaPrincipal').focus();
                }, 500);
            });
            
            var divMmPage = $('.mm-page');
            
            if(divMmPage !== undefined) { // c�digo adicionado pois o scroll no mobile n�o funcionava no Chrome
            	divMmPage.css('touch-action', '');
            }
            
        </c:if>
    });
</script>

<style>
@media (min-width: 768px) {
    .navbar-header-tripfans {
        width: 16%;
    }
    .navbar-header-img-logo {
        width: 100%;
        padding-top: 16px!important;
    }
}
@media (min-width: 992px) {
    .navbar-header-img-logo {
        padding-top: 9px!important;
    }
}
@media (min-width: 1200px) {
    .navbar-header-img-logo {
        padding-top: 3px!important;
    }
}
</style>

<%-- c:if test="${not tripFansEnviroment.develAtWork}"--%>
<header class="navbar navbar-fixed-top main-navbar bs-docs-nav" role="banner">
    <div class="navbar-inner topnavbar">
      <div class="container">
        <div class="navbar-header navbar-header-tripfans">

          <c:if test="${usuario != null}">
            <a href="#right-menu" class="navbar-toggle hidden-sm hidden-md hidden-lg btn-lg header-right-menu collapse-all-navbar">
              <span class="glyphicon glyphicon-user white"></span>
            </a>
          </c:if>
          
          <c:if test="${usuario == null}">
            <a href="<c:url value="/entrarPop" />" class="login-modal-link navbar-toggle hidden-sm hidden-md hidden-lg main-menu-item" style="display: none; padding-left: 0px;">
              <span><s:message code="label.login" /></span>
            </a>
          </c:if>

          <div id="nav-menu-opcoes" class="navbar-toggle hidden-sm hidden-md hidden-lg header-right-menu collapse-all-navbar">
              <a href="#" class="dropdown-toggle btn-lg header-right-menu" data-toggle="dropdown">
                <span class="glyphicon glyphicon-list white"></span>
              </a>
              
          </div>
          
          <%-- button id="btn-nav-menu-notificacoes" class="navbar-toggle btn-lg header-right-menu" type="button" style="margin: 0px 0px -5px;">
            <c:if test="${usuario != null}">
                <fan:notificationsButton id="iconeNotificacoes" user="${usuario.urlPath}" notifications="${notificacoesNaoVisualizadas}" useButton="false" />
            </c:if>
          </button--%>
          
          <%-- >button class="navbar-toggle btn-lg hidden-md hidden-lg" type="button" data-toggle="collapse" data-target="#navbar-search">
            <span class="glyphicon glyphicon-search white"></span>
          </button--%>
          
           <a href="#left-menu" class="mmenu-left hidden-sm hidden-md hidden-lg btn-lg header-left-menu">
               <span class="glyphicon glyphicon-align-justify white"></span>
           </a>
          
          <%-- a class="navbar-brand hidden-sm hidden-md hidden-lg" href="<c:url value="/"/>"--%>
          	<img src="<c:url value="/resources/images/logos/tripfans-medium.png"/>" height="34" class="hidden-sm hidden-md hidden-lg" style="margin-top: 3px; position: absolute; top: 0; left: 12px;" />
          <%--/a--%>
          
          <a class="navbar-brand visible-sm visible-md visible-lg" href="<c:url value="/"/>">
              <img class="navbar-header-img-logo" src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" alt="" />
          </a>
        </div>
        
        <nav id="nav-menu-search" class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
        
                        
          <ul class="nav navbar-nav navbar-right visible-sm visible-md visible-lg" style="margin-left: 0px;">
          
            <c:if test="${usuario != null}">
                <li class="visible-sm visible-md visible-lg">
                    <fan:notificationsButton id="btnNotificacoes" user="${usuario.urlPath}" notifications="${notificacoesNaoVisualizadas}" useButton="false"/>
                </li>
            </c:if>
          
            <li class="dropdown">
                            
              <c:if test="${usuario != null}">
            
                <a href="login" id="profile" class="dropdown-toggle" data-toggle="dropdown">
                    <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" imgStyle="margin-top: -20px;" />
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="margin-left: 0px;">
                
                    <c:choose>
                        <c:when test="${usuario.confirmado}">
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                    <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                    <br/>
                                    <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/conta/perfil" />">Configura��es de Perfil</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/config" />">Configura��es da Conta</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <i><strong style="color: #b94a48; padding: 3px 15px; white-space: nowrap;">Voc� precisa confirmar seu email.</strong></i>
                                <a class="resendConfirmEmail" href="<c:url value="/reenviarEmailConfirmacao/${usuario.urlPath}" />">Reenviar email de confirma��o</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                <br/>
                                <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                    <li class="divider"></li>
                    <li>
                        <a href="<c:url value="/signout" />">Sair</a>
                    </li>
                </ul>
              </c:if>
              <c:if test="${usuario == null}">
                <a href="<c:url value="/entrarPop" />" class="login-modal-link dropdown-toggle main-menu-item" data-toggle="dropdown" style="display: none;">
                    <span><s:message code="label.login" /></span>
                </a>
              </c:if>
            </li>
          </ul>
        </nav>
      </div>
    </div>
</header>
<%--/c:if--%>

<div id="modal-login" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Entrar no TripFans</h4>
      </div>
      <div class="modal-body" style="min-height: 515px;">
        <div id="modal-login-body"></div>
      </div>
    </div>
  </div>
</div>
