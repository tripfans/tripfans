<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-1.7.1.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-ui-1.8.16.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/components/bootstrap/bootstrap3/js/bootstrap.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/fanaticos.js"/>" ></script>

<c:if test="${not tripFansEnviroment.devel}">
  <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-43564931-1']);
      _gaq.push(['_trackPageview']);
    
      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
  </script>
</c:if>