<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<!DOCTYPE html>
<html>
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<head>
<title><tiles:getAsString name="title" /></title>
<%-- Disable zooming capabilities on mobile devices: "user-scalable=no" 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        --%>
<c:if test="${not isMobile}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
</c:if>
<c:if test="${isMobile}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
</c:if>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<meta name="keywords" content="<tiles:getAsString name="keywords"/>" />

<link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico">

<tiles:insertAttribute name="stylesheets" >
	<link rel="stylesheet" href="<c:url value="/wro/mainResponsive.css" />" type="text/css" />
	
	<script type="text/javascript" src="<c:url value="/wro/mainResponsive.js"/>" ></script>
</tiles:insertAttribute>

<style>
@media ( max-width : 767px) {
	.page-container {
		min-height: 400px;
	}
}

@media ( min-width : 768px) {
	.page-container {
		min-height: 486px;
	}
}

.body {
	background-image: initial;
	background-attachment: fixed;
	background-repeat: no-repeat;
	background-color: #ffffff;
}
</style>
</head>

<body class="body">

	<div id="header">
		<%@include file="newTopbarResponsive.jsp"%>
	</div>


	<tiles:insertAttribute name="body" />


	<c:if test="${isMobile}">
		<nav id="right-menu" style="display: none;">
			<ul>
				<li class="img"><a href="#"> <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false"
							marginLeft="0" marginRight="5" imgSize="avatar" /> ${usuario.displayName} <br> <small>${usuario.username}</small>
				</a>
					<ul>
						<li><c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" /> <a href="${urlPerfil}"> <small class="small muted">Visualizar Perfil</small>
						</a></li>
						<li class="divider"></li>
                        <li><a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a></li>
						<li class="divider"></li>
						<li><a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a></li>
						<li><a href="<c:url value="/conta/config" />">Configurações da Conta</a></li>
						<li class="divider" />
						<li><a href="<c:url value="/signout" />">Sair</a></li>
					</ul></li>
			</ul>
		</nav>
	</c:if>

	<jsp:include page="footer.jsp" />
	
	<div id="footer">

			<tiles:insertAttribute name="footer" >
					<c:if test="${not tripFansEnviroment.devel}">
					  <script type="text/javascript">
					      var _gaq = _gaq || [];
					      _gaq.push(['_setAccount', 'UA-43564931-1']);
					      _gaq.push(['_trackPageview']);
					    
					      (function() {
					        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					      })();
					  </script>
					</c:if>
			</tiles:insertAttribute>

			<script>
                    $(document).on('click', '.btn-adicionar-local-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        var idViagem = $(this).data('id-viagem');
                        
                        var url = '<c:url value="/viagem/adicionarLocalEmViagem"/>/' + idViagem + '/' + urlLocal;
                        
                        var request = $.ajax({
                            url: url, 
                            type: 'POST' 
                        });
                        request.done(function(data) {
                        });
                        request.fail(function(jqXHR, textStatus) {
                        });
                    });
                    
                    $(document).on('click', '.btn-criar-novo-plano', function (e) {
                        e.preventDefault();
                        var urlLocal = $(this).data('url-local');
                        
                        var url = '<c:url value="/viagem/adicionarLocalEmNovaViagem"/>/' + urlLocal;
                        
                        var request = $.ajax({
                            url: url, 
                            type: 'POST' 
                        });
                        request.done(function(data) {
                        });
                        request.fail(function(jqXHR, textStatus) {
                        });
                    });
                    
                    $(document).ready(function() {
                    
                        // Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela
                        $('.login-modal-link').show();
                        
                        window.onerror = function(m,u,l){
                            return true;
                        };
                        
                        <c:if test="${not isMobile}">
                          $('.container').initToolTips();
                        </c:if>
                        
                        $(window).scroll(function() {
                            var h = $(window).height();
                            var top = $(window).scrollTop();
                            if( top > (h * .30)) {
                               $("#top-button").fadeIn("slow");
                            } else {
                               $('#top-button').fadeOut('slow');
                            }
                        });
                        
                        $('#top-button').click(function (e) {
                            scrollToTop();
                        });
                        
                        $('.login-modal-link').click(function(e) {
                            e.preventDefault();
                            $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                                $('#modal-login').modal('show');
                            });
                        });
                    });
                </script>

			<c:if test="${not isMobile}">
				<script type="text/javascript">
                    function carregarFeedback() {
                        (function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/N3m2vVMPmtAcdd6aUiueWQ.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})()
                        
                        UserVoice = window.UserVoice || [];
                        UserVoice.push(['showTab', 'classic_widget', {
                          mode: 'full',
                          primary_color: '#3cb0e2',
                          link_color: '#007dbf',
                          default_mode: 'support',
                          forum_id: 219672,
                          tab_label: 'Feedback',
                          tab_color: '#e68e00',
                          tab_position: 'middle-right',
                          tab_inverted: false
                        }]);
                    }
                  </script>
			</c:if>
		</div>

	<div id="fb-root"></div>
	<script>
			(function(d, s, id) {
	            var js, fjs = d.getElementsByTagName(s)[0];
	            if (d.getElementById(id))
		            return;
	            js = d.createElement(s);
	            js.id = id;
	            js.async = true;
	            js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}&version=v2.0";
	            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
		</script>

</body>
</compress:html>
</html>