<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>


<footer class="bs-footer hidden-print" role="contentinfo">

		<div id="top-button">
			<div id="top-button-arrow"></div>
		</div>

		<div style="background-color: #A5A5A5; padding: 6px;">
			<div class="container">
				<div class="col-xs-4" style="text-align: center;">
					<a href="<c:url value="/sobreNos" />" style="color: white;"> Sobre o TripFans </a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="<c:url value="/destinosDestaques" />" style="color: white;"> Encontre seu Destino</a>
				</div>
				<div class="col-xs-4" style="text-align: center;">
					<a href="mailto:tripfans@tripfans.com.br" style="color: white;"> Contate-nos </a>
				</div>
			</div>
		</div>

		<div style="background-color: rgb(60, 175, 226); padding: 15px;">
			<div class="container">
				<div class="col-xs-2" style="text-align: center;">
                        
                </div>
                <div class="col-xs-2" style="text-align: center;">
                    
                </div>
                <div class="col-xs-2" style="text-align: center;">
                    <a href="http://www.facebook.com/TripFansBr" target="_blank" class="footer-social-icon facebook">
                        F
                    </a>
                </div>
                <div class="col-xs-2" style="text-align: center;">
                    <a href="http://twitter.com/tripfans" target="_blank" class="footer-social-icon twitter">
                        L
                    </a>
                </div>
                <div class="col-xs-2" style="text-align: center;">
                    
                </div>
                <div class="col-xs-2" style="text-align: center;">
                    
                </div>
			</div>
		</div>
		
		<c:set var="footerClassWidth" value="col-xs-6" />
		<c:if test="${isMobile}">
			<c:set var="footerClassWidth" value="col-xs-4" />
		</c:if>

		<div style="background-color: #D1D1D1; padding: 6px;">
			<div class="container">
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Termos de uso </a>
				</div>
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Política de privacidade </a>
				</div>
				<c:if test="${isMobile}">
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="#" style="color: #7c7c7c;"> Versão Desktop </a>
				</div>
				</c:if>
			</div>
		</div>

		<div style="background-color: #f5f5f5; padding: 6px;">
			<div class="container" style="text-align: center; color: rgb(185, 185, 185); font-size: 11px;">
				&copy; 2012-<%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%>
				TripFans - Todos os direitos reservados.
			</div>
		</div>

</footer>