<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.popover/jquery.popover-1.1.0-tripfans.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.json-2.3.js"></script>


<!--  Necessario colocar antes da configuracao de localizacao -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-addon.js" charset="utf-8"></script>

<c:choose>
<c:when test="${pageContext.response.locale.language eq 'pt'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-pt-BR.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-pt-BR.js" ></script>
</c:when>
<c:when test="${pageContext.response.locale.language eq 'es'}">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-es.js"></script>
</c:when>
</c:choose>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/components/mmenu/jquery.mmenu.min.all.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.fanaticos.autocomplete.js?<%=System.currentTimeMillis()%>"></script>