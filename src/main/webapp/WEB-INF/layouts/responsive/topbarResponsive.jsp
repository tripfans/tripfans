<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<script type="text/javascript">
    function selecionaLocal(event, ui) { 
        document.location.href= '${pageContext.request.contextPath}' + ui.item.url;
    }
    
    $(function() {
        
        <c:if test="${not isMobile}">
            $('#left-menu').affix({
                offset: {
                  top: 0,
                  bottom: function () {
                    return (this.bottom = $('.bs-footer').outerHeight(true))
                  }
                }
            });
        </c:if>
        
        <c:if test="${isMobile}">
            $('#left-menu').mmenu({
                dragOpen: true,
                classes: 'mm-light'
            });
            
            var $menu = $('#right-menu');
    
            $menu.mmenu({
                position: 'right',
                classes: 'mm-light',
                counters: false,
                searchfield: false,
                slidingSubmenus: false,
                header: {
                    add: true,
                    update: true,
                    title: '' 
                },
                dragOpen: true
            });
            
            $menu.css('display', '');
            
            $('.collapse-all-navbar').click(function (e) {
                if ($('#nav-menu-search').hasClass('in')) {
                    $('#btn-nav-menu-search').click();
                }
                if ($('#nav-menu-opcoes').hasClass('open')) {
                    $('#nav-menu-opcoes').removeClass('open')
                }
            });
            
            $('#btn-nav-menu-search').click(function (e) {
                setTimeout(function() {
                    $('#caixaPesquisaPrincipal').focus();
                }, 500);
            });
            
        </c:if>
    });
</script>

<style>
@media (min-width: 768px) {
    .navbar-header-tripfans {
        width: 16%;
    }
    .navbar-header-img-logo {
        width: 100%;
        padding-top: 16px!important;
    }
}
@media (min-width: 992px) {
    .navbar-header-img-logo {
        padding-top: 9px!important;
    }
}
@media (min-width: 1200px) {
    .navbar-header-img-logo {
        padding-top: 3px!important;
    }
}
</style>

<c:if test="${not tripFansEnviroment.develAtWork}">
<header class="navbar navbar-fixed-top main-navbar bs-docs-nav" role="banner">
    <div class="navbar-inner topnavbar">
      <div class="container">
        <div class="navbar-header navbar-header-tripfans">

          <c:if test="${usuario != null}">
            <a href="#right-menu" class="navbar-toggle hidden-sm hidden-md hidden-lg btn-lg header-right-menu collapse-all-navbar">
              <span class="glyphicon glyphicon-user white"></span>
            </a>
          </c:if>
          
          <c:if test="${usuario == null}">
            <a href="<c:url value="/entrarPop" />" class="login-modal-link navbar-toggle hidden-sm hidden-md hidden-lg main-menu-item" style="display: none; padding-left: 0px;">
              <span><s:message code="label.login" /></span>
            </a>
          </c:if>

          <button id="btn-nav-menu-search" class="navbar-toggle collapsed btn-lg header-right-menu" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
            <span class="sr-only"></span>
            <span class="glyphicon glyphicon-search white"></span>
          </button>
          
          <div id="nav-menu-opcoes" class="navbar-toggle hidden-sm hidden-md hidden-lg header-right-menu collapse-all-navbar">
              <a href="#" class="dropdown-toggle btn-lg header-right-menu" data-toggle="dropdown">
                <span class="glyphicon glyphicon-list white"></span>
              </a>
          </div>
          
          <%-- >button class="navbar-toggle btn-lg hidden-md hidden-lg" type="button" data-toggle="collapse" data-target="#navbar-search">
            <span class="glyphicon glyphicon-search white"></span>
          </button--%>
          
          <a href="#left-menu" class="mmenu-left hidden-sm hidden-md hidden-lg btn-lg header-left-menu" style="margin-left: -16px;">
              <span class="glyphicon glyphicon-align-justify white"></span>
          </a>
          
          <img src="<c:url value="/resources/images/logos/tripfans-medium.png"/>" height="34" class="hidden-sm hidden-md hidden-lg" style="margin-top: 3px; position: absolute; top: 0; left: 35px;">
          
          <a class="navbar-brand visible-sm visible-md visible-lg" href="<c:url value="/"/>">
              <img class="navbar-header-img-logo" src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" alt="">
          </a>
        </div>
        
        <nav id="nav-menu-search" class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
        
                                  
          <ul class="nav navbar-nav navbar-right visible-sm visible-md visible-lg" style="margin-left: 0px;">
          
            <li class="dropdown">
                            
              <c:if test="${usuario != null}">
            
                <a href="login" id="profile" class="dropdown-toggle" data-toggle="dropdown">
                    <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" showShadow="false" enableLink="false" marginLeft="0" marginRight="5" imgSize="avatar" imgStyle="margin-top: -20px;" />
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu" style="margin-left: 0px;">
                
                    <c:choose>
                        <c:when test="${usuario.confirmado}">
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                    <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                    <br/>
                                    <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/viagem/planejamento/inicio" />">Minhas Viagens</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<c:url value="/conta/perfil" />">Configurações de Perfil</a>
                            </li>
                            <li>
                                <a href="<c:url value="/conta/config" />">Configurações da Conta</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li>
                                <i><strong style="color: #b94a48; padding: 3px 15px; white-space: nowrap;">Você precisa confirmar seu email.</strong></i>
                                <a href="<c:url value="/reenviarEmailConfirmacao/${usuario.urlPath}" />">Reenviar email de confirmação</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <c:url var="urlPerfil" value="/perfil/${usuario.urlPath}" />
                                <a href="${urlPerfil}">
                                <strong>${usuario.displayName != null ? usuario.displayName : usuario.username}</strong>
                                <br/>
                                <small class="small muted">Visualizar Perfil</small>
                                </a>
                            </li>
                        </c:otherwise>
                    </c:choose>
                    <li class="divider"></li>
                    <li>
                        <a href="<c:url value="/signout" />">Sair</a>
                    </li>
                </ul>
              </c:if>
              <c:if test="${usuario == null}">
                <a href="<c:url value="/entrarPop" />" class="login-modal-link dropdown-toggle main-menu-item" data-toggle="dropdown" style="display: none;">
                    <span><s:message code="label.login" /></span>
                </a>
              </c:if>
            </li>
          </ul>
        </nav>
      </div>
    </div>
</header>
</c:if>

<div id="modal-login" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Entrar no TripFans</h4>
      </div>
      <div class="modal-body" style="min-height: 515px;">
        <div id="modal-login-body"></div>
      </div>
    </div>
  </div>
</div>

<c:if test="${not isMobile}">
  
</c:if>