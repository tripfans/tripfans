<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ include file="topbar.jsp" %>

<script type="text/javascript">
    $(document).ready(function () {
    	//openLoginWindow("login-dialog-link");
    	
        $('#login-dialog-link').click(function (e) {
            e.preventDefault();
            $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                $('#modal-login').modal({
                    backdrop : 'static',
                    show : true
                });
            });
        })
    });
</script>
<style>
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>    

<div style="display:none">
<c:choose>
    <c:when test="${requererConfirmacao}">
<%--        <c:import url="/WEB-INF/views/signup/confirmar.jsp" /> --%>
    </c:when>
    <c:otherwise>
        <c:if test="${requererComplemento}">
<%--             <c:import url="/WEB-INF/views/signup/completar.jsp" /> --%>
        </c:if>
    </c:otherwise>
</c:choose>

</div>

<c:if test="${requererLogin || param.requererLogin}">

  <sec:authorize access="isAnonymous()">

    <script>
    jQuery(document).ready(function() {
        //$.fancybox.init();
        $("#login-dialog-link").trigger('click');
    });
    </script>

  </sec:authorize>

</c:if>
     