<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">

FB.init({appId: '${tripFansEnviroment.facebookClientId}', xfbml: true, cookie: true, frictionlessRequests : true });

function facebookSendMessage(to) {
    FB.ui({
        app_id:'${tripFansEnviroment.facebookClientId}',
        method: 'send',
        name: 'Junte-se ao TripFans',
        link: '${tripFansEnviroment.serverUrl}',
        to: to,
        description: 'Texto que a gente tem que bolar para colocar aqui e chamar a atenção dos usuários'
        //picture: ,
        //redirect_uri: ,
    });
}

function enivarConviteViaRequisaoAplicativo(idsUsuarios, tripfansCallback) {
	
    var obj = {
        method: 'apprequests',
        title: 'TripFans ',
        message: 'Venha fazer parte da minha rede de amigos no TripFans.',
        to : idsUsuarios,
        data: '${usuario.urlPath}'
    };
    
    function callback(response) {
        if (response !== null && response !== undefined && response.request !== null && response.request !== undefined) {
            if(response !== null && response.post_id !== null) {
                var usersIds = new Array();
                var requestId = response.request;
                $(response.to).each(function(index, el) {
                    usersIds.push(el);
                });
                // registrar convite no TripFans
                $.post('<c:url value="/enviarConviteTripFansViaRequisicaoAplicativoFacebook" />', {'requestId' : requestId, 'idsFacebookConvidados' : usersIds}, function(response) {
                	tripfansCallback?tripfansCallback():null;
                });
            }
        }
    }
    FB.ui(obj, callback);
}

function enviarConviteAmigosSelecionados() {
    var user_ids = '';
    $('.card-selected').each(function(index) {
    	user_ids += $(this).attr('data-id') + ',';
    });
    enivarConviteViaRequisaoAplicativo(user_ids)
}

function facebookPost($link, method) {
    facebookPostToFeed($link);
}
    
function facebookPostToFeed($link, tripfansCallback) {
	
	var from = $link.attr('data-from');
    var to = $link.attr('data-to');
    var name = $link.attr('data-name');
    var caption = $link.attr('data-caption');
    var description = $link.attr('data-description');
    var link = $link.attr('data-link');
    var picture = $link.attr('data-picture');
    var urlReload = $link.attr('data-url-reload');
    var loadTo = $link.attr('data-url-loadto');
    
    var obj = {
            app_id:'${tripFansEnviroment.facebookClientId}',
            method: 'feed',
            link: link,
            from : from,
            to : to,
            picture: picture,
            name: name,
            caption: caption,
            description: description
        };

        function callback(response) {
            if(response !== null && response.post_id !== null) {
            	// registrar convite no TripFans
            	$.post('<c:url value="/enviarConviteTripFansViaPostMuralFacebook" />', {'idFacebookConvidado' : to, 'requestId': response.post_id}, function(response) {
            		if(urlReload !== null && loadTo !== null) {
            			$("#"+loadTo).load(urlReload);
            		}
            		tripfansCallback?tripfansCallback():null;
            	});
            }
        }
        FB.ui(obj, callback);
}

</script>