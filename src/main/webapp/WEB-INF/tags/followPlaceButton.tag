<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 

 <a href="${pageContext.request.contextPath}/locais/seguir/${local.urlPath}" class="btn btn-mini btnSiga" id="${local.urlPath}" title="Siga este local e receba todas as informações postadas por ele" >
		<i class="icon-arrow-right"></i>
		<b>Siga este Local</b>
</a>

<script type="text/javascript">
	$('a.btnSiga').click(function(event){
		event.preventDefault();
		
		var urlPath = $(this).attr('id');
		
		var parent = $(this).parent();
		
		$.post(this.href, function(response){
			if(response.success) {
				$(parent).html(response.params.message);
				
				alert('TESTE OK');
			}
		});
	 });
</script>