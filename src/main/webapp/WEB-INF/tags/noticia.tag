<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="noticia" required="true" type="br.com.fanaticosporviagens.model.entity.NoticiaSobreLocal" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="row span6">
<div class="span1">&nbsp;</div>
<div class="span5">
<h5>
<c:choose>
	<c:when test="${noticia.sobreAvaliacao}">Avaliação</c:when>
	<c:when test="${noticia.sobreDica}">Dica</c:when>
</c:choose>
sobre <fan:localLink local="${noticia.localPublicado}" />
<c:choose>
<c:when test="${noticia.tipoLocal.tipoLocalEnderecavel}">
, <fan:localLink local="${noticia.cidadeLocalPublicado}" />
, <fan:localLink local="${noticia.estadoLocalPublicado}" />
, <fan:localLink local="${noticia.paisLocalPublicado}" />
</c:when>
<c:when test="${noticia.tipoLocal.tipoCidade}">
, <fan:localLink local="${noticia.estadoLocalPublicado}" />
, <fan:localLink local="${noticia.paisLocalPublicado}" />
</c:when>
<c:when test="${noticia.tipoLocal.tipoEstado}">
, <fan:localLink local="${noticia.paisLocalPublicado}" />
</c:when>
</c:choose>
</h5>
</div>
</div>
<br/>
<c:choose>
	<c:when test="${noticia.sobreAvaliacao}"><fan:avaliacao avaliacao="${noticia.avaliacaoPublicada}"/></c:when>
	<c:when test="${noticia.sobreDica}"><fan:dica dica="${noticia.dicaPublicada}"/></c:when>
</c:choose>
