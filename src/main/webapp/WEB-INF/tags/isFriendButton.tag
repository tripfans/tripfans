<%@tag pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ attribute name="cssButtonClassSize" required="false" type="java.lang.String" description="large, small ou mini" %>
<%@ attribute name="buttonGroupClass" required="false" type="java.lang.String" description="Css do grupo do botao" %>
<%@ attribute name="buttonGroupStyle" required="false" type="java.lang.String" description="Style do grupo botao" %>
<%@ attribute name="buttonStyle" required="false" type="java.lang.String" description="Style do botao" %>
<%@ attribute name="direction" required="false" type="java.lang.String" description="Direcao do dropdown (dropup é para cima)" %>
<%@ attribute name="importadaFacebook" required="true" type="java.lang.Boolean" description="Informa se a amizade foi importada do Facebook" %>
<%@ attribute name="somenteFacebook" required="true" type="java.lang.Boolean" description="Informa se a amizade existe somente no Facebook" %>
<%@ attribute name="nomeAmigo" required="true" type="java.lang.String" description="Nome do amigo" %>
<%@ attribute name="idUsuarioAmigo" required="false" type="java.lang.Long" description="Identificador do amigo; este valor ou idFacebookAmigo deve ser informado" %>
<%@ attribute name="idFacebookAmigo" required="false" type="java.lang.String" description="Identificador Facebook do amigo; este valor ou idUsuarioAmigo deve ser informado" %>
<%@ attribute name="urlReload" required="true" type="java.lang.String" description="Url a para recarregamento quando a acao de desfazer amizade for feita" %>

<c:url var="urlDesfazerAmizade" value="/desfazerAmizadeTripFans/${idUsuarioAmigo}" />
<c:if test="${somenteFacebook}">
  <c:url var="urlDesfazerAmizade" value="/desfazerAmizadeSomenteFacebook/${idFacebookAmigo}" />
</c:if>

<div class="${direction} btn-group ${buttonGroupClass}" style="${buttonGroupStyle}">
	<a class="btn btn-${cssButtonClassSize} btn-secondary btnAmizade dropdown-toggle" data-toggle="dropdown" title="Você e ${nomeAmigo} são amigos" style="${buttonStyle}">
	<i class="icon-ok"></i> <b>Amigos</b> <span class="caret"></span>
	</a>
	<ul class="dropdown-menu">
		<li><a id="link-${idUsuarioAmigo}${idFacebookAmigo}" href="#">Desfazer Amizade</a></li>
	</ul>
</div>

<c:set var="textoConfirmacao" value="Você tem certeza de que deseja remover ${nomeAmigo} da sua lista de amigos?" />
<c:if test="${importadaFacebook}">
<c:set var="notaConfirmacao" value="Nota: Esta amizade é importada do Facebook. Ao desfazê-la, você removerá ${nomeAmigo} da sua lista de amigos no TripFans. Mas ele ainda continuará seu amigo no Facebook." />
</c:if>

<div id="modal-${idUsuarioAmigo}${idFacebookAmigo}" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>Deseja remover ${nomeAmigo} da sua lista de amigos?</h3>
	</div>
	<div class="modal-body">
    	<p>${textoConfirmacao}</p>
    	<p><small>${notaConfirmacao}</small></p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    	<button class="btn btn-primary">Remover dos Amigos</button>
  	</div>
</div>

<div id="modal-${idUsuarioAmigo}${idFacebookAmigo}success" class="modal hide fade">
	<div class="modal-header">
		<h3>Amigo Removido</h3>
	</div>
	<div class="modal-body">
    	<p>${nomeAmigo} foi removido da sua lista de amigos</p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn btn-primary">Ok</button>
  	</div>
</div>
    
<script type="text/javascript">
$('#link-${idUsuarioAmigo}${idFacebookAmigo}').bind('click',function(event){
	event.preventDefault();
	$('#modal-${idUsuarioAmigo}${idFacebookAmigo}').modal();	
});

$('#modal-${idUsuarioAmigo}${idFacebookAmigo} > div.modal-footer > button.btn-primary').bind('click', function(event){
	event.preventDefault();
	$('#modal-${idUsuarioAmigo}${idFacebookAmigo}').modal('hide');
	$.post('${urlDesfazerAmizade}', function(response){
		$('#modal-${idUsuarioAmigo}${idFacebookAmigo}success').modal();
	});
});

$('#modal-${idUsuarioAmigo}${idFacebookAmigo}success > div.modal-footer > button.btn-primary').bind('click', function(event){
	event.preventDefault();
	location.href= '${urlReload}';
});

</script>