<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ attribute name="text" required="true" type="java.lang.String" description="Texto a ser exibido" %>
<%@ attribute name="showCloseButton" required="false" type="java.lang.Boolean" description="Se deve exibir o botao de fechar o alerta (valor default: true)" %>
<%@ attribute name="showQuestion" required="false" type="java.lang.Boolean" description="Se deve exibir a pergunta de justificativa de pedido de informaçoes (valor default: true)" %>

<c:if test="${showCloseButton == null}"><c:set var="showCloseButton" value="true" /></c:if>
<c:if test="${showQuestion == null}"><c:set var="showQuestion" value="true" /></c:if>

<div class="alert alert-info alert-block" style="font-size:  12pt;  text-align:  justify;">
	<c:if test="${showCloseButton}"><button type="button" class="close" data-dismiss="alert">x</button></c:if>
	<c:if test="${showQuestion}"><strong>Por que estamos pedindo essas informações?</strong><br/></c:if>
	${text}
</div>