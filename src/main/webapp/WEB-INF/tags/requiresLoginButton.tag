<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %>
<%@ attribute name="id" required="true" rtexprvalue="true" description="Identificador do elemento" %> 
<%@ attribute name="url" rtexprvalue="true" required="true" description="Url de acao do botao" %> 
<%@ attribute name="cssClass" required="false" type="java.lang.String" description="Classe CSS para ser usado no botao" %>
<%@ attribute name="cssStyle" required="false" type="java.lang.String" description="Estilo CSS para ser usado no botao" %>
<%@ attribute name="iconClass" required="false" type="java.lang.String" description="Classe do icone para ser usado no botao" %>
<%@ attribute name="title" required="true" type="java.lang.String" description="O titulo do botao" %>
<%@ attribute name="tooltip" required="false" type="java.lang.String" description="Texto explicativo do campo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:url value="/login" var="loginUrl" />

<c:choose>
<c:when test="${usuario == null}">
	<a id="${id}" href="${loginUrl}?targetUrlParameter=${url}" class="${cssClass} authButton" style="${cssStyle}" <c:if test="${tooltip != null}">title="${tooltip}"</c:if>>
	<c:if test="${iconClass != null}"><i class="${iconClass}"></i></c:if>
	${title}</a>
	<script type="text/javascript">
	 	$(function() {
	 		openLoginWindow('${id}');
		});
	</script>
</c:when>
<c:otherwise>
	<a id="${id}" href="${url}" class="${cssClass}" style="${cssStyle}"><c:if test="${iconClass != null}"><i class="${iconClass}"></i></c:if>${title}</a>
</c:otherwise>
</c:choose>

