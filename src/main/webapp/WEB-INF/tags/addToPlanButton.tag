<%@tag pageEncoding="UTF-8"%>

<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="tf" uri="/WEB-INF/tripfansFunctions.tld" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" description="O local a ser adicionado ao plano." %>
<%@ attribute name="planosViagem" required="true" type="java.util.Collection" description="A lista planos abertos do usuario." %>
<%@ attribute name="style" required="false" type="java.lang.String" description="Style do div principal" %>
<%@ attribute name="btnClass" required="false" type="java.lang.String" description="Classe do botão" %>

<div class="btn-group" style="${style}">                       
    <%--a href="#" class="dropdown-toggle transparencia-75 blackAndWhite colorOnHover ${btnClass}" data-toggle="dropdown" style="text-decoration: none;" title="Adicione este local ao seu Plano de Viagem">
        <img src="<c:url value="/resources/images/icons/luggage.png" />" height="16" width="16" />
        <span>Adicionar ao meu plano</span>
        <b class="caret"></b>
    </a--%>
    <a href="#" class="dropdown-toggle ${not empty btnClass ? btnClass : 'btn btn-success btn-sm'}" data-toggle="dropdown" style="text-decoration: none;" title="Adicione este local ao seu Plano de Viagem">
        <span class="glyphicon glyphicon-plus"></span>
        <span>Adicionar ao meu plano</span>
        <b class="caret"></b>
    </a>
    <ul class="dropdown-menu">
      <c:if test="${not empty planosViagem}">
        <c:forEach items="${planosViagem}" var="planoViagem">
          <li>
            <a href="#" class="btn-adicionar-local-plano" data-id-viagem="${planoViagem.id}" data-url-local="${local.urlPath}">
                ${planoViagem.titulo}
            </a>
          </li>
        </c:forEach>
        <li class="divider"></li>
      </c:if>
      <li>
        <a href="#" class="btn-criar-novo-plano" data-url-local="${local.urlPath}">
            Criar um novo Plano de Viagem
        </a>
      </li>
    </ul>
</div>