<%@tag import="net.sf.sprockets.google.Place.Review"%>
<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="avaliacao" required="false" type="br.com.fanaticosporviagens.model.entity.Avaliacao" %> 
<%@ attribute name="review" required="false" type="java.lang.Object" %> 
<%@ attribute name="fullSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado em classe CSS .span" %>
<%@ attribute name="userSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado pelos dados do usuário em classe CSS .span" %>
<%@ attribute name="userMarginLeft" required="false" type="java.lang.String" %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado pelos dados da avaliação em classe CSS .span" %>
<%@ attribute name="mostraUsuarioAvaliador" required="false" type="java.lang.Boolean" description="Se deve mostrar o usuário que avaliou" %>
<%@ attribute name="mostraTextoAvaliacaoSobre" required="false" type="java.lang.Boolean" description="Se deve mostrar o texto 'Avaliacao sobre xxx'" %>
<%@ attribute name="mostraAcoes" required="false" type="java.lang.Boolean" description="Se deve mostrar os botões de ações" %>
<%@ attribute name="mostraFoto" required="false" type="java.lang.Boolean" description="Se deve mostrar a foto do local avaliado" %>
<%@ attribute name="callbackAfterDelete" required="false" type="java.lang.String" description="Função javascript que será chamada ao excluir avaliacao" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<c:if test="${fullSize == null}"><c:set var="fullSize" value="11" /></c:if>
<c:if test="${userSize == null}"><c:set var="userSize" value="2" /></c:if>
<c:if test="${dataSize == null}"><c:set var="dataSize" value="9" /></c:if>
<c:if test="${mostraUsuarioAvaliador == null}"><c:set var="mostraUsuarioAvaliador" value="true" /></c:if>
<c:if test="${mostraTextoAvaliacaoSobre == null}"><c:set var="mostraTextoAvaliacaoSobre" value="false" /></c:if>
<c:if test="${mostraAcoes == null}"><c:set var="mostraAcoes" value="false" /></c:if>

<style>
.panoramio-wapi-photo .panoramio-wapi-images {
    background-color: transparent;
}
</style>

<c:choose>
  <c:when test="${avaliacao != null}">

    <c:if test="${not empty avaliacao.avaliacoesClassificacaoAplicaveis || (not empty avaliacao.quantidadeFotos && avaliacao.quantidadeFotos != 0) 
    	|| not empty avaliacao.itensBomPara || not empty avaliacao.tiposViagemRealizada}">
    	<c:set var="mostrarMais" value="true" />
    </c:if>

	<div id="box-avaliacao-${avaliacao.id}" class="span${fullSize} page-container" style="background-color: #FFF; margin: 10px 0 10px; ">
		<c:if test="${mostraUsuarioAvaliador}">
		  <div class="span${userSize}" style="margin-left: 0px; padding-top: 10px" align="center">
			<fan:userAvatar user="${avaliacao.autor}" displayDetails="true" displayName="true" orientation="vertical" imgFloat="center" />
		  </div>
		</c:if>
		<div class="span${mostraFoto ? dataSize - 3 : dataSize}" style="margin: 0px 1px 0px 0px; padding-left: 8px; padding-top: 10px; border-left: 1px solid #eee; ${mostraFoto ? 'border-right: 1px solid #eee;' : ''}">
			<%-- <a><span class="stylizedText">${avaliacao.titulo}</span></a> --%>
            <a>
                <span style="font-size: 22px;">
                    <span style="font-size: 28px;">&ldquo;</span><span style="font-weight: bold;">${avaliacao.titulo}</span><span style="font-size: 28px;">&rdquo;</span>
                </span>
            </a>
			<c:if test="${mostraTextoAvaliacaoSobre}">
    			<p>
                    Avaliação sobre <fan:localLink local="${avaliacao.localAvaliado}" nomeCompleto="true" destaqueNomeLocal="true" />
    			</p>
			</c:if>

            <p class="textoUsuario">
                ${avaliacao.descricao}
            </p>

    		<p style="margin-bottom: 0px;">
    			<span style="font-weight: bold; vertical-align: top;">Nota geral:</span>
   				<span title="${avaliacao.classificacaoGeral.descricao}" style="margin-top: -4px; margin-left: 5px; display: inline-block;"  class="starsCategory cat${avaliacao.classificacaoGeral.codigo}"></span> 
			</p>

			<p>
			  <c:if test="${mostrarMais}">
                <a id="mostraAvaliacao-${avaliacao.urlPath}" data-target="${avaliacao.urlPath}" class="btn btn-mini btn-secondary" 
                   onclick="mostraDadosAvaliacao(this);" style="cursor: pointer;">
                   <i class="icon-chevron-down"></i> Mais
                </a>
              </c:if>
                <a id="escondeAvaliacao-${avaliacao.urlPath}" class="btn btn-mini btn-secondary hide" data-target="${avaliacao.urlPath}" onclick="escondeDadosAvaliacao(this);" style="cursor: pointer;">
                    <i class="icon-chevron-up"></i> Menos
                </a>
			</p>
			<div class="hide well" id="${avaliacao.urlPath}" style="margin-right: 10px; margin-bottom: 5px; padding: 0px 0px 10px 15px;">
				<c:if test="${not empty avaliacao.avaliacoesClassificacaoAplicaveis}">
			 		<div id="group-criterios-${avaliacao.id}" style="height: auto; margin: 15px 0 15px;">
						<div class="row">
						  <c:forEach var="avaliacaoClassificacao" items="${avaliacao.avaliacoesClassificacaoAplicaveis}" varStatus="classificacao">
							<div class="span2">
							${avaliacaoClassificacao.criterio.descricao}:
							<span title="${avaliacaoClassificacao.qualidade.descricao}" class="starsCategoryScaled cat${avaliacaoClassificacao.qualidade.codigo}">&nbsp;</span>
							</div>
							<c:if test="${classificacao.count % 3 == 0 && not classificacao.last}">
                              </div><div class="row">
                            </c:if>
						  </c:forEach>
						</div>
					</div>
				</c:if>
				
				<c:if test="${not empty avaliacao.itensBomPara}">
                  <div style="margin-bottom: 5px;">
					<strong>Aconselhável para: </strong>
					<c:forEach items="${avaliacao.itensBomPara}" var="itemBomPara">
						<span class="label label-success">${itemBomPara.item.descricao}</span>
					</c:forEach>
                  </div>
				</c:if>
				
				<c:if test="${not empty avaliacao.tiposViagemRealizada}">
                  <div style="margin-bottom: 5px;">
					<strong>Tipo de viagem: </strong>
					<c:forEach items="${avaliacao.tiposViagemRealizada}" var="tiposViagem">
						<span class="label label-info">${tiposViagem.tipo.descricao}</span>
					</c:forEach>
                  </div>
				</c:if>

			</div>
            
            <p>
                <small>
                    Visitou em ${avaliacao.mesVisita.descricao} de ${avaliacao.anoVisita} e avaliou <fan:passedTime date="${avaliacao.dataAvaliacao}"/>
                    
                    <c:if test="${usuario != null && usuario.id == avaliacao.autor.id}">
                      -
                      <a id="excluir-avaliacao-${avaliacao.id}" data-avaliacao-id="${avaliacao.id}" href="#">Excluir</a>
                    </c:if>                    
                </small>
            </p>
            
			<c:if test="${not avaliacao.participaConsolidacao}"><div class="row"><div class="span8"><b>Observação:</b> Esta avaliação não está sendo considerada na consolidação pois seu autor fez uma nova avaliação.</div></div><hr/></c:if>
		</div>
        
        <c:if test="${mostraFoto and not empty avaliacao.localAvaliado.coordenadaGeografica.latitude}">
          
            <div id="foto-local-avaliacao-${avaliacao.id}" class="span3" style="margin-left: 0px; padding-top: 10px">
              <c:set var="photoUrl" value="${avaliacao.localAvaliado.urlFotoAlbum}" />
              <c:choose>
                <c:when test="${avaliacao.localAvaliado.fotoPadraoAnonima}">
                  <img src="${photoUrl}" class="local-card-img ${not avaliacao.localAvaliado.fotoPadraoAnonima ? 'sombra' : 'transparencia-75'} borda-arredondada" style="float: center; padding-left: 35px; min-height: ${avaliacao.localAvaliado.fotoPadraoAnonima ? '100px; max-width: 100px; margin-top: 30px;' : '141px'}">
                </c:when>
                <c:otherwise>
                  <img src="${avaliacao.localAvaliado.urlFotoAlbum}" 
                       class="sombra borda-arredondada" style="min-height: 170px; max-width: 110%; margin-left: 0px;">
                </c:otherwise>
              </c:choose>
            </div>
            
            <c:if test="${avaliacao.localAvaliado.fotoPadraoAnonima and avaliacao.localAvaliado.usarFotoPanoramio}">
        
                <c:set var="latSW" value="${avaliacao.localAvaliado.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
                <c:set var="lngSW" value="${avaliacao.localAvaliado.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
                <c:set var="latNE" value="${avaliacao.localAvaliado.perimetroParaConsultaNoMapa.latitudeNordeste}" />
                <c:set var="lngNE" value="${avaliacao.localAvaliado.perimetroParaConsultaNoMapa.longitudeNordeste}" />
                
                <c:if test="${not unsuportedIEVersion}">
                  <script>
                    var requestOptions = {
                        'set' : 'public',
                        'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}}
                    };
                    var request = new panoramio.PhotoRequest(requestOptions);
                    var widgetOptions = {
                        'width': 200,
                        'height': 170,
                        'bgcolor' : 'white',
                        'croppedPhotos' : panoramio.Cropping.TO_FILL,
                        'attributionStyle': panoramio.tos.Style.HIDDEN,
                        'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
                    };
                    var photoWidget_${avaliacao.id} = new panoramio.PhotoWidget('foto-local-avaliacao-${avaliacao.id}', request, widgetOptions);
                    
                    <c:if test="${not avaliacao.localAvaliado.tipoLocal.tipoLocalGeografico}">
                    photoWidget_${avaliacao.id}.enablePreviousArrow(false);
                    photoWidget_${avaliacao.id}.enableNextArrow(false);
                    </c:if>
                    
                    function photoChanged(event) {
                        if (event.target.getPhoto() == null) {
                            var $div = $('#foto-local-avaliacao-${avaliacao.id}');
                            // esconder widget
                            $div.find('.panoramio-wapi-photo').hide();
                            // foto padrao
                            $div.append('<div align="center" style="width: 100%; display: inline-table; margin-left: 20px;">' +
                                    '<img src="${avaliacao.localAvaliado.urlFotoAlbum}" class="local-card-img borda-arredondada" style="float: center; min-height: 100px; max-width: 100px; margin-top: 30px;">' + 
                                    '</div>');
                        }
                    }                    
    
                    function photoClicked(event) {
                        //document.location.href = '${itemUrl}'
                        return false;
                    }
                    
                    panoramio.events.listen(photoWidget_${avaliacao.id}, panoramio.events.EventType.PHOTO_CHANGED, photoChanged);
                    panoramio.events.listen(photoWidget_${avaliacao.id}, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);                
                    
                    photoWidget_${avaliacao.id}.setPosition(0);
                    
                    $(document).ready(function() {
                        $('#foto-local-avaliacao-${avaliacao.id}').find('.panoramio-wapi-photo').css('max-width', '112%').addClass('borda-arredondada');
                    });
                  </script>
            
                  <style>
                    #foto-local-avaliacao-${id} .panoramio-wapi-images {
                        background-color: transparent;
                    }
                    
                    #foto-local-avaliacao-${id} .panoramio-wapi-crop-div {
                        width: 190px; 
                        height: 160px;
                        border-radius: 4px 0px 0px 0px;
                    }
                  </style>
                </c:if>
            </c:if>
        </c:if>
        
        <c:if test="${mostraAcoes}">
          <div class="span${fullSize}" style="margin-left: 0px; background-color: rgb(244, 245, 244);" id="avaliacaoActions">
            <div style="border-bottom: 1px solid #dedede; width: 100%;"></div>
            <div class="pull-right" style="padding-right: 6px; margin: 6px 0 6px;">
                <c:if test="${not empty avaliacao.quantidadeFotos && avaliacao.quantidadeFotos != 0}">
                  <button class="btn" title="Veja fotos desta avaliação" data-href="${pageContext.request.contextPath}/fotos/fotosAvaliacao/${avaliacao.urlPath}" onclick="verFotosAvaliacao(this);">
                    <i class="icon-picture"></i>
                  </button>
                </c:if>
                <fan:votarUtilButton target="${avaliacao}" />
                
                <c:if test="${usuario.id == avaliacao.autor.id}">
                
                  <c:choose>
                    <c:when test="${avaliacao.localAvaliado.fotoPadraoAnonima}">
                      <c:set var="imgUrl" value="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
                    </c:when>
                    <c:otherwise>
                      <c:set var="imgUrl" value="${avaliacao.localAvaliado.urlFotoAlbum}" />
                    </c:otherwise>
                  </c:choose>
                  
                  <fan:shareButton facebook="true" 
                                   url="${tripFansEnviroment.serverUrl}/${avaliacao.shareableLink}"
                                   pictureUrl="${imgUrl}"
                                   titulo="${usuario.primeiroNome} compartilhou uma avaliação sobre ${avaliacao.localAvaliado.nome} no TripFans"
                                   subTitulo="${avaliacao.titulo}"
                                   texto="${avaliacao.facebookPostText}"
                                   iconStyle="margin-top: -4px;"  />
                </c:if>
            </div>
          </div>
        </c:if>
    </div>
    
    <sec:authorize access="isAuthenticated()">
    
      <c:if test="${usuario != null && usuario.id == avaliacao.autor.id}">

        <div id="modal-excluir-avaliacao-${avaliacao.id}" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Excluir avaliação</h3>
            </div>
            <div class="modal-body">
                <p>Deseja realmente excluir esta avaliação?</p>
            </div>
            <div class="modal-footer">
                <a id="confirmar-excluir-avaliacao-${avaliacao.id}" href="#" data-avaliacao-id="" class="btn btn-primary">
                    <i class="icon-ok icon-white"></i> 
                    Sim
                </a>
                <a href="#" class="btn" data-dismiss="modal">
                    <i class="icon-remove"></i> 
                    Não
                </a>
            </div>
        </div>    

        <script>
        
            $(document).ready(function () {
                $('#excluir-avaliacao-${avaliacao.id}').live('click', function (event) {
                    event.preventDefault();
                    $('#confirmar-excluir-avaliacao-${avaliacao.id}').data('avaliacao-id', $(this).data('avaliacao-id'));
                    $('#modal-excluir-avaliacao-${avaliacao.id}').modal('show');
                });
                
                $('#confirmar-excluir-avaliacao-${avaliacao.id}').click(function(e) {
                    e.preventDefault();
                    var idAvaliacao = $(this).data('avaliacao-id')
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/avaliacao/excluir/" />' + idAvaliacao,
                        success: function(data, status, xhr) {
                            if (data.success) {
                                $('#modal-excluir-avaliacao-${avaliacao.id}').modal('hide');
                                $('#box-avaliacao-' + idAvaliacao).hide('explode', {}, 1000);
                                <c:if test="${not empty callbackAfterDelete}">
                                    ${callbackAfterDelete}();
                                </c:if>
                            }
                        }
                    });
                });
            });
            
        </script>
        
      </c:if>
    
    </sec:authorize>        
  </c:when>
  
  <c:when test="${review != null}">
    <c:if test="${mostraUsuarioAvaliador}">
        <c:set var="authorUrl" value="${review.authorUrl}" scope="request" />
        <c:set var="time" value="${review.time}" scope="request" />
        <% 
          String url = (String) request.getAttribute("authorUrl");
          String userId = url.substring(url.lastIndexOf("/") + 1);
          java.util.Date dataReview = null;
          if (request.getAttribute("time") != null) {
              String time = request.getAttribute("time") + "";
              // Por algum motivo o time vem errado, aparentemente faltando digitos e fazendo com q a data fique em 1970
              // Acrescentando digitos para resolver o problema
              if (time != null) {
                  if (time.length() < 13) {
                      for (int i = time.length(); i < 13; i++) {
                          time += "0";
                      }
                  }
                  System.out.println(time);
                  dataReview = new java.util.Date(Long.valueOf(time));
              }
          }
          
        %>
        <div class="span${userSize}">
            <div style="float:left;">
                <img src="https://plus.google.com/s2/photos/profile/<%=userId%>?sz=60" class="sombra borda-arredondada"
                     onError="this.onerror=null;this.src='<c:url value="/resources/images/anonimo-google.jpg"/>';" 
                     style="background-color: #FEFEFE; width: 40px;" />
            </div>
        </div>
        
        <div class="span${dataSize}">
            <div>
                <span>
                    <strong>${review.authorName}</strong>
                </span>
                <div>
                  <c:forEach items="${review.aspects}" var="aspect" varStatus="status">
                    <%-- 
                     O review do Google vai de 0 a 3, mas parece que tem um "legacy rating".
                     Então, para tentar manter a compatiblidade do nosso, somamos 2 pontos caso seja menor que 4 
                    --%>
                    <%-- Exibir só a primeiro nota --%>
                    <c:if test="${not empty status.index and status.index == 0}">
                      <c:set var="rating" value="${aspect.rating < 4 ? aspect.rating + 2 : aspect.rating}"/>
                      <%-- span title="${aspect.type}" style="display: inline-block;" class="starsRating cat${rating}"></span--%>
                      <span style="display: inline-block;" class="starsRating cat${rating}"></span>
                    </c:if>
                  </c:forEach>
                </div>
            </div>
            <div>
                <c:if test="${mostraTextoAvaliacaoSobre}">
                    <p style="font-size: 14px;">${review.text}</p>
                    <p><small>Avaliação feita <fan:passedTime date="<%=dataReview%>"/></small></p>
                </c:if>        
            </div>
            
        </div>
    </c:if>
  </c:when>
</c:choose>  