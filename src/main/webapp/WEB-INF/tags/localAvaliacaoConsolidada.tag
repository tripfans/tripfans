<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="avaliacoesConsolidadas" required="false" type="java.util.List" %> 

<c:if test="${((not empty local.avaliacaoConsolidadaGeral) && (local.avaliacaoConsolidadaGeral.quantidadeAvaliacoes != 0))}">
	<a name="resumoAvaliacao"></a>
	
	<div class="span12" style="margin-left: 0px;">
	<div class="page-header">
		<h3>Resumo das avaliações</h3>
	</div>
		<c:set var="avaliacaoConsolidada" value="${local.avaliacaoConsolidadaGeral}" />

		<span style="margin-top: 1px;" data-content="<%@include file="popoverAvaliacaoConsolidada.jsp" %>" rel="popover"  data-original-title="Classificação Geral">
		<div class="row" style="margin-bottom: 10px;">
			<div class="span3"><h4><strong>Classificação Geral:</strong></h4></div>
			<div class="span8">
			<span title="${local.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}" class="starsCategory cat${local.avaliacaoConsolidadaGeral.classificacaoGeral.codigo}">&nbsp;</span>
			</div>
		</div>
		</span>

		<p style="margin-bottom: 18px;">
		<strong class="large">${local.avaliacaoConsolidadaGeral.porcentagemAvaliacaoPositivas}% dos viajantes recomendam a visita.</strong>
		<c:choose>
			<c:when test="${local.avaliacaoConsolidadaGeral.quantidadeAvaliacoes == 1}"><br/>Foi considerada ${local.avaliacaoConsolidadaGeral.quantidadeAvaliacoes} avaliação nesta consolidação. A nota foi ${local.avaliacaoConsolidadaGeral.notaGeral}. *</c:when>
			<c:otherwise><br/>Foram consideradas ${local.avaliacaoConsolidadaGeral.quantidadeAvaliacoes} avaliações nesta consolidação. A nota média foi ${local.avaliacaoConsolidadaGeral.notaGeral}. *</c:otherwise>
		</c:choose>
		</p>

		<hr/>
		<c:if test="${not empty avaliacoesConsolidadas}">
			<h5>Critérios avaliados</h5>
			<div class="row">
			<c:forEach var="avaliacaoConsolidada" items="${avaliacoesConsolidadas}" varStatus="varStatus">
            	<span style="margin-top: 1px;" data-content="<%@include file="popoverAvaliacaoConsolidada.jsp" %>" rel="popover"  data-original-title="${avaliacaoConsolidada.criterio.descricao}">
				<div class="span2">${avaliacaoConsolidada.criterio.descricao}:
				<span title="${avaliacaoConsolidada.classificacaoGeral.descricao}" class="starsCategoryScaled cat${avaliacaoConsolidada.classificacaoGeral.codigo}">&nbsp;</span>
				<p><small>Nota: ${avaliacaoConsolidada.notaGeral} - ${avaliacaoConsolidada.quantidadeAvaliacoes} avaliações.</small></p>
				</div>
				</span>
				<c:if test="${varStatus.count % 5 == 0 && not varStatus.last}"></div><div class="row"></c:if>
			</c:forEach>
			</div>
		</c:if>
		
		<br/>
		<p style="margin-bottom: 18px;">
		* Na consolidação dos dados é considerada apenas a avaliação mais recente de cada autor.
		</p>
		
		<div class="horizontalSpacer"></div>
	</div>


	<script>
	$(function () {
		$("*[rel=popover]").popover({
			offset: 80,
			html : true,
			delayIn : 1000
		});
	});
	</script>

</c:if>
