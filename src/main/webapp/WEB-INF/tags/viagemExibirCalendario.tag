<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2" %> 
<%@ attribute name="modoVisao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemModoVisao" %>
<%@ attribute name="permissao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemPermissao" %>

<c:if test="${not empty viagem && modoVisao.calendario}">
<div class="alert alert-info">
<h3>Calendário da Viagem</h3>
</div>

<c:set var="quantidadeDiasPorLinha" value="${viagem.calendario.quantidadeDiasPorLinha}" />
<c:set var="quantidadeDiasCompletarSemana" value="${viagem.calendario.quantidadeDiasCompletarSemana}" />
<c:set var="percentualWidthColuna" value="${viagem.calendario.percentualWidthColuna}" />
<c:set var="semanas" value="${viagem.calendario.semanas}" />

<c:if test="${permissao.usuarioPodeAdicionarDia}">
	<c:set var="quantidadeDiasPorLinha" value="${viagem.calendario.quantidadeDiasPorLinhaEdicao}" />
	<c:set var="quantidadeDiasCompletarSemana" value="${viagem.calendario.quantidadeDiasCompletarSemanaEdicao}" />
	<c:set var="percentualWidthColuna" value="${viagem.calendario.percentualWidthColunaEdicao}" />
	<c:set var="semanas" value="${viagem.calendario.semanasEdicao}" />
</c:if>

<table class="table table-bordered table-hover table-condensed">
<thead>
	<tr style="background: #d9edf7">
		<th colspan="${quantidadeDiasPorLinha}">
		<h3>Itens da Viagem</h3>
		<c:if test="${permissao.usuarioPodeIncluirItem}">
		<div class="btn-group">
			<a href="#formViagemItemIncluirModal" role="button" class="btn btn-mini btnAdicionarItem" data-toggle="modal" idDia="viagem">
			<img alt="Adicionar item" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
	           Item
			</a>	
		</div>
		</c:if>
		</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td colspan="${quantidadeDiasPorLinha}">
		<c:if test="${not empty viagem.itensOrdenados}">
		<ol class="inline">
		<c:forEach items="${viagem.itensOrdenados}" var="item" varStatus="itemStatus" >
		<fan:viagemItemLi viagem="${viagem}" item="${item}" modoVisao="${modoVisao}" permissao="${permissao}" />
		</c:forEach>
		</ol>
		</c:if>
		</td>
	</tr>
	<tr>
		<td colspan="${quantidadeDiasPorLinha}">
		<strong>Subtotal dos ítens da viagem: </strong>${viagem.valorItens}
		</td>
	</tr>
	<c:forEach var="semana" varStatus="semanaStatus" items="${semanas}">
		<tr style="background: #d9edf7">
			<c:forEach var="dia" items="${semana}">
			<th width="${percentualWidthColuna}%">Dia ${dia.numero}
				<c:if test="${not empty dia.dataViagem}">
				<br/> <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd/MM/yyyy" />
				</c:if>
				<c:if test="${permissao.usuarioPodeIncluirItem || permissao.usuarioPodeExcluirDia}">
				<div class="btn-group">
					<c:if test="${permissao.usuarioPodeIncluirItem}">
					<a href="#formViagemItemIncluirModal" role="button" class="btn btn-mini btnAdicionarItem" data-toggle="modal" idDia="${dia.id}">
					<img alt="Adicionar item" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
			           Item
					</a>	
					</c:if>
					<c:if test="${permissao.usuarioPodeExcluirDia}">
					<a class="btn btn-mini btnExcluirDia" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/excluirDia" idDia="${dia.id}" >
					<img alt="Excluir dia" src="${pageContext.request.contextPath}/resources/images/icons/delete.png" />
			           Excluir dia
					</a>	
					</c:if>
				</div>
				</c:if>
			</th>
			</c:forEach>
			<c:if test="${semanaStatus.last}">
			<c:forEach begin="1" end="${quantidadeDiasCompletarSemana}" varStatus="complementoStatus">
			<td width="${percentualWidthColuna}%">
			<c:if test="${complementoStatus.first && permissao.usuarioPodeAdicionarDia}">
				<br/><c:if test="${not empty viagem.dataInicio}"><br/></c:if>
				<a id="btnAdicionarDia" class="btn btn-mini" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/adicionarDia" >
				<img alt="Adicionar dia a viagem" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
	            Adicionar dia
				</a>
			</c:if>
			</td>
			</c:forEach>
			</c:if>
		</tr>
		<tr>
		<c:forEach var="dia" items="${semana}">
		<td>
			<c:if test="${not empty dia.itensOrdenados}">
			<ol>
			<c:forEach items="${dia.itensOrdenados}" var="item" varStatus="itemStatus" >
			<fan:viagemItemLi viagem="${viagem}" item="${item}" modoVisao="${modoVisao}" permissao="${permissao}" />
			</c:forEach>
			</ol>
			</c:if>
		</td>
		</c:forEach>
		<c:if test="${semanaStatus.last}">
			<c:forEach begin="1" end="${quantidadeDiasCompletarSemana}">
			<td></td>
			</c:forEach>
		</c:if>
		</tr>
		<tr>
		<c:forEach var="dia" items="${semana}">
		<td>
			<strong>Subtotal do dia:</strong> ${dia.valor}
		</td>
		</c:forEach>
		<c:if test="${semanaStatus.last}">
			<c:forEach begin="1" end="${quantidadeDiasCompletarSemana}">
			<td></td>
			</c:forEach>
		</c:if>
		</tr>
	</c:forEach>
</tbody>
</table>
</c:if>

