<div class='row'>
    <div class='span5'>
		<span title='${avaliacaoConsolidada.classificacaoGeral.descricao}' class='starsCategoryScaled cat${avaliacaoConsolidada.classificacaoGeral.codigo}'>&nbsp;</span>
		<p><small>Nota: ${avaliacaoConsolidada.notaGeral} - ${avaliacaoConsolidada.quantidadeAvaliacoes} avaliações.</small></p>
		<div class='row'>

			<div class='span3'>
				<ul class='barChart'>
				<div class='newLine'>
				<span class='left' style='width: 78px;'>
				<c:out value='<%= br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade.EXCELENTE.getDescricao() %>'></c:out>
				</span>
				<div class='emptyBar'><div style='width:${avaliacaoConsolidada.porcentagemAvaliacaoExcelente}px;' class='fullBar'></div></div>
				<span class='right'>${avaliacaoConsolidada.quantidadeAvaliacaoExcelente}</span>
				</div>
				<div class='newLine'>
				<span class='left' style='width: 78px;'>
				<c:out value='<%= br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade.BOM.getDescricao() %>'></c:out>
				</span>
				<div class='emptyBar'><div style='width:${avaliacaoConsolidada.porcentagemAvaliacaoBom}px;' class='fullBar'></div></div>
				<span class='right'>${avaliacaoConsolidada.quantidadeAvaliacaoBom}</span>
				</div>
				<div class='newLine'>
				<span class='left' style='width: 78px;'>
				<c:out value='<%= br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade.REGULAR.getDescricao() %>'></c:out>
				</span>
				<div class='emptyBar'><div style='width:${avaliacaoConsolidada.porcentagemAvaliacaoRegular}px;' class='fullBar'></div></div>
				<span class='right'>${avaliacaoConsolidada.quantidadeAvaliacaoRegular}</span>
				</div>
				<div class='newLine'>
				<span class='left' style='width: 78px;'>
				<c:out value='<%= br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade.RUIM.getDescricao() %>'></c:out>
				</span>
				<div class='emptyBar'><div style='width:${avaliacaoConsolidada.porcentagemAvaliacaoRuim}px;' class='fullBar'></div></div>
				<span class='right'>${avaliacaoConsolidada.quantidadeAvaliacaoRuim}</span>
				</div>
				<div class='newLine'>
				<span class='left' style='width: 78px;'>
				<c:out value='<%= br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade.PESSIMO.getDescricao() %>'></c:out>
				</span>
				<div class='emptyBar'><div style='width:${avaliacaoConsolidada.porcentagemAvaliacaoPessimo}px;' class='fullBar'></div></div>
				<span class='right'>${avaliacaoConsolidada.quantidadeAvaliacaoPessimo}</span>
				</div>
				</ul> 
			</div>
		</div>
    </div>
</div>