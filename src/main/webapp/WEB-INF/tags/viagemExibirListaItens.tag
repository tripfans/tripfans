<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2" %>
<%@ attribute name="modoVisao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemModoVisao" %>
<%@ attribute name="permissao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemPermissao" %> 

<c:if test="${not empty viagem && modoVisao.listaItens}">
<div class="alert alert-info">
<h3>Itens da viagem</h3>
<c:if test="${permissao.usuarioPodeIncluirItem}">
<div class="btn-group">
	<a href="#formViagemItemIncluirModal" role="button" class="btn btn-mini btnAdicionarItem" data-toggle="modal" value="viagem" >
	<img alt="Adicionar item" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
          Item
	</a>	
</div>
</c:if>
</div>
<c:if test="${not empty viagem.itensOrdenados}">
<ol>
<c:forEach items="${viagem.itensOrdenados}" var="item" varStatus="itemStatus" >
	<fan:viagemItemLi viagem="${viagem}" item="${item}" modoVisao="${modoVisao}" permissao="${permissao}" />
</c:forEach>
</ol>
</c:if>
<br/><strong>Subtotal dos ítens da viagem: </strong>${viagem.valorItens}
<c:forEach var="dia" items="${viagem.dias}">
    <div class="dia" id="dia${dia.id}">
    	<br/>
	    <div class="alert alert-info">
		<h3>Dia ${dia.numero}
			<c:if test="${not empty dia.dataViagem}">
			- <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd/MM/yyyy" />
			</c:if>
		</h3>
		<c:if test="${permissao.usuarioPodeIncluirItem || permissao.usuarioPodeExcluirDia}">
		<div class="btn-group">
			<c:if test="${permissao.usuarioPodeIncluirItem}">
			<a href="#formViagemItemIncluirModal" role="button" class="btn btn-mini btnAdicionarItem" data-toggle="modal" value="${dia.id}" >
			<img alt="Adicionar item" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
	           Item
			</a>	
			</c:if>
			<c:if test="${permissao.usuarioPodeExcluirDia}">
			<a class="btn btn-mini btnExcluirDia" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/excluirDia" idDia="${dia.id}" >
			<img alt="Excluir dia" src="${pageContext.request.contextPath}/resources/images/icons/delete.png" />
	           Excluir dia
			</a>	
			</c:if>
		</div>
		</c:if>
		</div>
		<c:if test="${not empty dia.itensOrdenados}">
			<ol>
			<c:forEach items="${dia.itensOrdenados}" var="item" varStatus="itemStatus" >
				<fan:viagemItemLi viagem="${viagem}" item="${item}"  modoVisao="${modoVisao}" permissao="${permissao}" />
			</c:forEach>
			</ol>
		</c:if>
	    <br/><strong>Subtotal do dia: </strong>${dia.valor}
    </div>
</c:forEach>
<c:if test="${permissao.usuarioPodeAdicionarDia}">
<div class="alert alert-info">
	<div class="btn-group">
		<a id="btnAdicionarDia" class="btn btn-mini" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/adicionarDia" >
		<img alt="Adicionar dia a viagem" src="${pageContext.request.contextPath}/resources/images/icons/add.png" />
	          Adicionar dia
		</a>
	</div>
</div>
</c:if>
</c:if>

