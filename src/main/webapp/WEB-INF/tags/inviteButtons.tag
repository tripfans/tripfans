<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="cssClassAccept" required="true" type="java.lang.String" description="Classe CSS para estilizar o botão" %>
<%@ attribute name="iconClassAccept" required="false" type="java.lang.String" description="Classe CSS do ícone para ser exibido no botão" %>
<%@ attribute name="cssStyleAccept" required="false" type="java.lang.String" description="Css botao accept" %>
<%@ attribute name="idAccept" required="false" type="java.lang.String" description="Identificador HTML do botão de aceite" %>
<%@ attribute name="titleAccept" required="true" type="java.lang.String" description="Título do botão de aceite" %>
<%@ attribute name="urlAccept" required="true" type="java.lang.String" description="Url a ser chamada para aceite do convite." %>
<%@ attribute name="cssClassIgnore" required="true" type="java.lang.String" description="Classe CSS para estilizar o botão" %>
<%@ attribute name="iconClassIgnore" required="false" type="java.lang.String" description="Classe CSS do ícone para ser exibido no botão" %>
<%@ attribute name="cssStyleIgnore" required="false" type="java.lang.String" description="Css botao ignore" %>
<%@ attribute name="idIgnore" required="false" type="java.lang.String" description="Identificador HTML do botão de ignorar" %>
<%@ attribute name="titleIgnore" required="true" type="java.lang.String" description="Título do botão de ignorar" %>
<%@ attribute name="urlIgnore" required="true" type="java.lang.String" description="Url a ser chamada para ignorar o convite." %>


<% String idUnicoAccept  = RandomStringUtils.random(12, true, true);  %>
<c:if test="${not empty idAccept}">	<% idUnicoAccept = idAccept;  %> </c:if>
<% String idUnicoIgnore  = RandomStringUtils.random(12, true, true);  %>
<c:if test="${not empty idIgnore}">	<% idUnicoAccept = idIgnore;  %> </c:if>


<button id="<%= idUnicoAccept %>" href="${urlAccept}" class="${cssClassAccept}" style="${cssStyleAccept}"><c:if test="${iconClassAccept != null}"> <i class="${iconClassAccept}"></i> </c:if>${titleAccept}</button>
<button id="<%= idUnicoIgnore %>" href="${urlIgnore}" class="${cssClassIgnore}" style="${cssStyleIgnore}"><c:if test="${iconClassIgnore != null}"> <i class="${iconClassIgnore}"></i> </c:if>${titleIgnore}</button>

<script type="text/javascript">
 $('#<%= idUnicoAccept %>').bind( "click", function(event) {
	 event.preventDefault();
	 var button = $('#<%= idUnicoAccept %>');
	 $(button).append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="13" style="margin-left: 8px;" >');
	 $.post('${urlAccept}',{},function(response){
		 enableDisableField("<%= idUnicoAccept %>");
		 $(button).html('Convite de amizade aceito.').attr('class', '${cssClassAccept} disabled').attr('disabled', 'disabled');
		 $('#<%= idUnicoIgnore %>').remove();
	 });
 });
 
 $('#<%= idUnicoIgnore %>').bind( "click", function(event) {
	 event.preventDefault();
	 var button = $('#<%= idUnicoIgnore %>');
	 $(button).append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="13" style="margin-left: 8px;" >');
	 $.post('${urlIgnore}',{},function(response){
		 enableDisableField("<%= idUnicoIgnore %>");
		 $(button).html('Convite foi ignorado.').attr('class', '${cssClassIgnore} disabled').attr('disabled', 'disabled');
		 $('#<%= idUnicoAccept %>').remove();
	 });
 });
</script>