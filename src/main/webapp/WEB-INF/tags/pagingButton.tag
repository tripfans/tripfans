<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.infra.component.SearchData"%>
<%@ tag body-content="empty" %> 
<%@ attribute name="cssClass" required="false" type="java.lang.String" description="Classe CSS para ser adicionada ao botao" %>
<%@ attribute name="cssStyle" required="false" type="java.lang.String" description="Css style para ser adicionado ao botao" %>
<%@ attribute name="id" required="false" type="java.lang.String" description="Identificador HTML desse botao." %>
<%@ attribute name="noMoreItensText" required="false" type="java.lang.String" description="Texto que vai aparecer quando nao houver mais itens de paginacao." %> 
<%@ attribute name="targetId" required="true" rtexprvalue="true" type="java.lang.String" description="Identificador HTML do elemento onde os itens devem ser renderizados." %>
<%@ attribute name="title" required="true" type="java.lang.String" description="Texto do botao." %>
<%@ attribute name="url" required="false" type="java.lang.String" description="A URL a ser chamada para recuperar mais itens." %>
<%@ attribute name="formId" required="false" type="String" description="Identificador do HTML Form que possui os filtros da consulta (se for o caso)." %>
<%@ attribute name="limit" required="false" type="java.lang.Integer" description="Quantidade maxima de registros a cada paginacao." %>
<%@ attribute name="oncomplete" required="false" type="java.lang.String" description="Funcao a ser chamanda apos cada paginacao." %>

<%@ attribute name="noMoreItensActions" fragment="true" required="false" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="buttonId" value="botaoPaginacao" />
<c:if test="${id != null}">
<c:set var="buttonId" value="${id}" />
</c:if>

<c:if test="${limit == null}">
    <c:set var="limit" value="${searchData.limit}" />
</c:if>

<c:choose>
  <c:when test="${searchData.moreItens == true}">
	<c:set var="start" value="${searchData.start + limit}" />
	<a class="btn ${cssClass}" id="${buttonId}" href="${not empty url ? url : '#'}" style="${cssStyle}">
        <strong>${title}</strong> <i class="icon-chevron-down"> </i>
    </a>
  </c:when>
  <c:otherwise>
	<c:if test="${not empty noMoreItensText}">
		<div align="center" class="help-block ${cssClass}"><p>${noMoreItensText}</p></div>
	</c:if>
    <div class="${cssClass}" align="center">
        <jsp:invoke fragment="noMoreItensActions" />
    </div>
    <br/>
  </c:otherwise>
</c:choose>
<script type="text/javascript">
 
$('#${buttonId}').click(function(event) {
	 event.preventDefault();

     var button = $(this);
     $(button).html('<img src="${pageContext.request.contextPath}/resources/images/loading.gif" width="25" style="margin-left: 8px;" >');
     var sortField = $('#sortField');
     var dirField = $('#dirField');

	 <c:if test="${not empty url}">
	   var url = this.href;
     </c:if>
     <c:if test="${not empty formId}">
       var url = $('#${formId}').attr('action');
     </c:if>
	   
     if (url.indexOf("?") != -1) {
		 url += "&start=${start}&limit=${limit}"; 
	 } else {
		 url += "?start=${start}&limit=${limit}"; 
	 }
	 
	 if (sortField !== null && dirField !== null) {
       <c:if test="${not empty formId}">
         $.get(url, $('#${formId}').serialize(), function(response) {
             $(button).remove();
             $('#${targetId}').append(response);
         });
       </c:if>
	   <c:if test="${empty formId}">
		 $.get(url,{sort: sortField.attr('value'), dir: dirField.attr('value')},function(response) {
			 $(button).remove();
			 $('#${targetId}').append(response);
			 //eval("${oncomplete}");
		 });
	   </c:if>
	 } else {
		 $.get(url,{},function(response) {
			 $(button).remove();
			 $('#${targetId}').append(response);
			 //eval("${oncomplete}");
		 });
	 }
 });
 
</script>