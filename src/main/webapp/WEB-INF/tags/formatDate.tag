<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %>
<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="value" required="true" type="java.lang.Object" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--c:if test="${usuario != null}">

    <c:if test="${usuario.preferencias != null}">
    </c:if>

</c:if>

<c:when test="${pageContext.response.locale.language eq 'pt'}"--%>
