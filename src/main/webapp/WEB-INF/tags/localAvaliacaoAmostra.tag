<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="amostraAvaliacoes" required="true" type="java.util.List" %> 
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${not empty amostraAvaliacoes}">
	<div class="span12" style="margin-left: 0px;">
		<div class="page-header">
			<h3>Últimas avaliações
			<div class="row pull-right">
				<a id="listarTodasAvaliacoes" href="${pageContext.request.contextPath}/locais/${local.urlPath}?tab=avaliacoes" class="seeMore">Ver todas</a>
			</div>
			</h3>
		</div>
	
		<c:forEach items="${amostraAvaliacoes}" var="varAvaliacao" varStatus="contador" >
			<fan:avaliacao fullSize="12" userSize="2" dataSize="10" mostraTextoAvaliacaoSobre="true" mostraAcoes="true" avaliacao="${varAvaliacao}"/>	
		</c:forEach>
	
	</div>
	
</c:if>
