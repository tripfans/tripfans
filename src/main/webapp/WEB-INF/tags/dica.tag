<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ attribute name="dica" required="false" type="br.com.fanaticosporviagens.model.entity.Dica" %> 
<%@ attribute name="tip" required="false" type="org.springframework.social.foursquare.api.Tip" %> 
<%@ attribute name="fullSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado em classe CSS .span ou md" %>
<%@ attribute name="userSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado pelos dados do usuário em classe CSS .span ou md" %>
<%@ attribute name="userMarginLeft" required="false" type="java.lang.String" %>
<%@ attribute name="dataSize" required="false" type="java.lang.String" description="Tamanho a ser ocupado pelos dados da dica em classe CSS .span ou md" %>
<%@ attribute name="mostraUsuarioEscreveu" required="false" type="java.lang.Boolean" description="Se deve mostrar o usuário que escreveu a dica" %>
<%@ attribute name="mostraTextoDicaSobre" required="false" type="java.lang.Boolean" description="Se deve mostrar o texto 'Dica sobre xxx'" %>
<%@ attribute name="mostraAcoes" required="false" type="java.lang.Boolean" description="Se deve mostrar os botoes de ações" %>
<%@ attribute name="mostraFoto" required="false" type="java.lang.Boolean" description="Se deve mostrar a foto do local da dica" %>

<c:if test="${fullSize == null}"><c:set var="fullSize" value="11" /></c:if>
<c:if test="${userSize == null}"><c:set var="userSize" value="2" /></c:if>
<c:if test="${dataSize == null}"><c:set var="dataSize" value="9" /></c:if>
<c:if test="${mostraUsuarioEscreveu == null}"><c:set var="mostraUsuarioEscreveu" value="true" /></c:if>
<c:if test="${mostraTextoDicaSobre == null}"><c:set var="mostraTextoDicaSobre" value="false" /></c:if>
<c:if test="${mostraAcoes == null}"><c:set var="mostraAcoes" value="false" /></c:if>

<style>
.panoramio-wapi-photo .panoramio-wapi-images {
    background-color: transparent;
}
</style>

<c:choose>
  <c:when test="${dica != null}">
	<div id="box-dica-${dica.id}" class="span${fullSize} col-md-${fullSize}  box-container" style="background-color: #FFF; margin-bottom: 10px; margin-left: 10px; padding-left: 0; padding-right: 0;">
		<c:if test="${mostraUsuarioEscreveu}">
		<div class="span${userSize} col-md-${userSize}" style="margin-left: 0px; padding-top: 10px;" align="center">
			<c:choose>
				<c:when test="${dica.autor != null}">
					<fan:userAvatar user="${dica.autor}" displayDetails="true"  displayName="true" orientation="vertical" imgFloat="center" />
				</c:when>
				<c:otherwise>
					<fan:userAvatar idUsuarioFacebook="${dica.idAutorFacebook}" orientation="vertical" nomeUsuario="${dica.nomeAutor}" imgFloat="center" />
				</c:otherwise>
			</c:choose>
		</div>
		</c:if>
		<div class="span${mostraFoto ? dataSize - 3 : dataSize} " style="margin: 0px 2px 0px 0px; padding-left: 7px; padding-top: 10px; ${mostraUsuarioEscreveu?  'border-left: 1px solid #eee;' : ''}  ${mostraFoto ? 'border-right: 1px solid #eee; min-height: 175px;' : 'min-height: 120px;'}">
			<c:if test="${mostraTextoDicaSobre}">
			<p style="font-size: 15px;">
			Dica sobre <fan:localLink local="${dica.localDica}" nomeCompleto="true" destaqueNomeLocal="true"/> 
			</p>
			</c:if>
			<p class="textoUsuario">${dica.descricao}</p>
			
			<p>
                <small>
                    Dica escrita em <fan:passedTime date="${dica.dataCadastro}"/>
                    <c:if test="${usuario != null && usuario.id == dica.autor.id}">
                      <a id="excluir-dica-${dica.id}" data-dica-id="${dica.id}" href="#">Excluir</a>
                    </c:if>
                </small>
            </p>
            
			<c:if test="${dica.importadaFoursquare}">
				<p>
                    <small>
                        Dica Importada do <img src="<c:url value="/resources/images/logos/foursquare.png"/>" width="75" style="margin-top: -3px;" />
                    </small>
                </p>
			</c:if>
			
			<c:if test="${not empty dica.itensClassificacao}">
            	<p><small>Marcadores:</small>
            	<c:forEach items="${dica.itensClassificacao}" var="itemClassificacao">
            		<%--span class="label label-info" title="Ver todas as dicas sobre '${itemClassificacao.descricao}'">${itemClassificacao.descricao}</span--%>
                    <span class="label label-info">${itemClassificacao.descricao}&nbsp;</span>
            	</c:forEach>
            	</p>
            </c:if>

		</div>
        
        <c:if test="${mostraFoto and not empty dica.localDica.coordenadaGeografica.latitude}">
          
            <div id="foto-local-dica-${dica.id}" class="span3 col-md-3" style="margin-left: 0px; padding-top: 10px">
              <c:set var="photoUrl" value="${dica.localDica.urlFotoAlbum}" />
              <c:choose>
                <c:when test="${dica.localDica.fotoPadraoAnonima}">
                  <img src="${photoUrl}" class="local-card-img ${not dica.localDica.fotoPadraoAnonima ? 'sombra' : 'transparencia-75'} borda-arredondada" style="float: center; padding-left: 35px; min-height: ${dica.localDica.fotoPadraoAnonima ? '100px; max-width: 100px; margin-top: 30px;' : '141px'}">
                </c:when>
                <c:otherwise>
                  <img src="${dica.localDica.urlFotoAlbum}" 
                       class="sombra borda-arredondada" style="min-height: 170px; max-width: 110%; margin-left: 0px;">
                </c:otherwise>
              </c:choose>
            </div>
            
            <c:if test="${dica.localDica.fotoPadraoAnonima and dica.localDica.usarFotoPanoramio}">
        
                <c:set var="latSW" value="${dica.localDica.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
                <c:set var="lngSW" value="${dica.localDica.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
                <c:set var="latNE" value="${dica.localDica.perimetroParaConsultaNoMapa.latitudeNordeste}" />
                <c:set var="lngNE" value="${dica.localDica.perimetroParaConsultaNoMapa.longitudeNordeste}" />
                
                <c:if test="${not unsuportedIEVersion}">
                  <script>
                    var requestOptions = {
                        'set' : 'public',
                        'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}}
                    };
                    var request = new panoramio.PhotoRequest(requestOptions);
                    var widgetOptions = {
                        'width': 200,
                        'height': 170,
                        'bgcolor' : 'white',
                        'croppedPhotos' : panoramio.Cropping.TO_FILL,
                        'attributionStyle': panoramio.tos.Style.HIDDEN,
                        'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
                    };
                    var photoWidget_${dica.id} = new panoramio.PhotoWidget('foto-local-dica-${dica.id}', request, widgetOptions);
                    
                    <c:if test="${not dica.localDica.tipoLocal.tipoLocalGeografico}">
                    photoWidget_${dica.id}.enablePreviousArrow(false);
                    photoWidget_${dica.id}.enableNextArrow(false);
                    </c:if>
                    
                    function photoChanged(event) {
                        if (event.target.getPhoto() == null) {
                            var $div = $('#foto-local-dica-${dica.id}');
                            // esconder widget
                            $div.find('.panoramio-wapi-photo').hide();
                            // foto padrao
                            $div.append('<div align="center" style="width: 100%; display: inline-table; margin-left: 20px;">' +
                                    '<img src="${dica.localDica.urlFotoAlbum}" class="local-card-img borda-arredondada" style="float: center; min-height: 100px; max-width: 100px; margin-top: 30px;">' + 
                                    '</div>');
                        }
                    }
    
                    function photoClicked(event) {
                        //document.location.href = '${itemUrl}'
                        return false;
                    }
                    
                    panoramio.events.listen(photoWidget_${dica.id}, panoramio.events.EventType.PHOTO_CHANGED, photoChanged);
                    panoramio.events.listen(photoWidget_${dica.id}, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);                
                    
                    photoWidget_${dica.id}.setPosition(0);
                    
                    $(document).ready(function() {
                        $('#foto-local-dica-${dica.id}').find('.panoramio-wapi-photo').css('max-width', '112%').addClass('borda-arredondada');
                    });
                  </script>
            
                  <style>
                    #foto-local-dica-${id} .panoramio-wapi-images {
                        background-color: transparent;
                    }
                    
                    #foto-local-dica-${id} .panoramio-wapi-crop-div {
                        width: 190px; 
                        height: 160px;
                        border-radius: 4px 0px 0px 0px;
                    }
                  </style>
                </c:if>
            </c:if>
        </c:if>        
        
        <c:if test="${mostraAcoes}">
        
          <div class="span${fullSize}" style="border-top: 1px solid #eee; margin-left: 0px; background-color: rgb(244, 245, 244);" id="dicaActions-${dica.id}">
            <div style="padding: 3px; text-align: right;">
                <%--c:if test="${not empty dica.quantidadeFotos && dica.quantidadeFotos != 0}">
                  <button class="btn" title="Veja fotos desta dica" data-href="${pageContext.request.contextPath}/fotos/fotosDica/${dica.urlPath}" onclick="verFotosDica(this);">
                    <i class="icon-picture"></i>
                  </button>
                </c:if--%>
                <fan:votarUtilButton target="${dica}" />
                
                <c:if test="${usuario.id == dica.autor.id}">
                
                  <c:choose>
                    <c:when test="${dica.localDica.fotoPadraoAnonima}">
                      <c:set var="imgUrl" value="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
                    </c:when>
                    <c:otherwise>
                      <c:set var="imgUrl" value="${dica.localDica.urlFotoAlbum}" />
                    </c:otherwise>
                  </c:choose>
                
                  <fan:shareButton facebook="true" 
                                   url="${tripFansEnviroment.serverUrl}/${dica.shareableLink}"
                                   pictureUrl="${imgUrl}"
                                   titulo="${usuario.primeiroNome} compartilhou uma dica sobre ${dica.localDica.nome} no TripFans"
                                   subTitulo="Dica sobre ${dica.localDica.nome}"
                                   texto="${dica.facebookPostText}"
                                   iconStyle="margin-top: -4px;"  />
                </c:if>
            </div>
          </div>        
        </c:if>
        
	</div>
    
    <sec:authorize access="isAuthenticated()">
    
      <c:if test="${usuario != null && usuario.id == dica.autor.id}">

        <div id="modal-excluir-dica-${dica.id}" class="modal hide fade">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Excluir dica</h3>
            </div>
            <div class="modal-body">
                <p>Deseja realmente excluir esta dica?</p>
            </div>
            <div class="modal-footer">
                <a id="confirmar-excluir-dica-${dica.id}" href="#" data-dica-id="" class="btn btn-primary">
                    <i class="icon-ok icon-white"></i> 
                    Sim
                </a>
                <a href="#" class="btn" data-dismiss="modal">
                    <i class="icon-remove"></i> 
                    Não
                </a>
            </div>
        </div>
        
        <script>
        
            $(document).ready(function () {
            	$('#excluir-dica-${dica.id}').live('click', function (event) {
                    event.preventDefault();
                    $('#confirmar-excluir-dica-${dica.id}').data('dica-id', $(this).data('dica-id'));
                    $('#modal-excluir-dica-${dica.id}').modal('show');
                });
                
                $('#confirmar-excluir-dica-${dica.id}').click(function(e) {
                    e.preventDefault();
                    var idDica = $(this).data('dica-id')
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/dicas/excluir/" />' + idDica,
                        success: function(data, status, xhr) {
                            if (data.success) {
                                $('#modal-excluir-dica-${dica.id}').modal('hide');
                                $('#box-dica-' + idDica).hide('explode', {}, 1000);
                            }
                        }
                    });
                });
            });
            
        </script>
        
      </c:if>
    
    </sec:authorize>
    
  </c:when>
  
  <c:when test="${tip != null}">
    <c:if test="${mostraUsuarioEscreveu}">
        <div class="span${userSize} col-md-${userSize}">
            <div style="float:left;">
                <img src="${tip.user.photoUrl}" class="sombra borda-arredondada" style="background-color: #FEFEFE; width: 40px;" />
            </div>
        </div>
        
        <div class="span${dataSize} col-md-${dataSize}">
            <div>
                <span>
                    <strong>${tip.user.firstName} ${tip.user.lastName}</strong>
                </span>
            </div>
        </div>
        
        <div class="span${dataSize} col-md-${dataSize}">
        
            <c:if test="${mostraTextoDicaSobre}">
<%--                 <p>
                    Dica sobre <strong>${tip.venue.name}</strong> 
                </p>
 --%>           
                <p style="font-size: 14px;">${tip.text}</p>
                
                <p><small>Dica escrita <fan:passedTime date="${tip.createdAt}"/></small></p>
            </c:if>        
        </div>
        
    </c:if>
  </c:when>
  
</c:choose>