<%@tag import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal" trimDirectiveWhitespaces="true" %>
<%@ tag pageEncoding="UTF-8"%>
<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="locaisEmDestaque" required="true" type="java.util.List" %> 
<%@ attribute name="urlProximos" required="true" type="java.lang.String" %>
<%@ attribute name="selectable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="style" required="false" type="java.lang.String" %>
<%@ attribute name="cardStyle" required="false" type="java.lang.String" %>
<%@ attribute name="cardClass" required="false" type="java.lang.String" %>
<%@ attribute name="photoStyle" required="false" type="java.lang.String" %> 
<%@ attribute name="titleStyle" required="false" type="java.lang.String" %>
<%@ attribute name="cardOrientation" required="false" type="java.lang.String" %> 
<%@ attribute name="columns" required="false" type="java.lang.Integer" %>
<%@ attribute name="columnSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="photoHeight" required="false" type="java.lang.Integer" %>
<%@ attribute name="photoSize" required="false" type="java.lang.String" description="small, album ou big" %>
<%@ attribute name="showActions" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showAddToPlanButton" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showNotaGeral" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showViajantesJaForam" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showBotaoAdicionar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showBotaoDispensar" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showReservarBooking" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showAmigosJaForam" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showAdditionalInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="useButtonsRecomendacao" required="false" type="java.lang.Boolean" %>

<c:if test="${showAddToPlanButton == null}"><c:set var="showAddToPlanButton" value="true" /></c:if>
<c:if test="${showNotaGeral == null}"><c:set var="showNotaGeral" value="true" /></c:if>
<c:if test="${showViajantesJaForam == null}"><c:set var="showViajantesJaForam" value="true" /></c:if>
<c:if test="${selectable == null}"><c:set var="selectable" value="true" /></c:if>

<c:if test="${empty columns}">
  <c:set var="columns" value="3" />
</c:if> 

<c:if test="${empty columnSize}">
  <c:set var="columnSize" value="4" />
</c:if> 
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<c:if test="${not empty locaisEmDestaque}">

    <div style="${style}">
        <c:if test="${empty photoSize}">
          <c:set var="photoSize" value="${isMobile ? 'small' : 'album'}"></c:set>
        </c:if>
        <c:forEach items="${locaisEmDestaque}" var="localEmDestaque" varStatus="status">
            <c:set var="coluna" value="${status.index + 1}" />
            
            <%-- Verificar se é um EntidadeIndexada --%>
            <% try { %>
            <c:choose>
              <c:when test="${not empty localEmDestaque.localEnderecavel}">
                <c:set var="local" value="${localEmDestaque.localEnderecavel}"></c:set>
              </c:when>
              <c:otherwise>
                <c:set var="local" value="${localEmDestaque.local}"></c:set>
              </c:otherwise>
            </c:choose>
            <% } catch(Exception e) { %>
              <c:set var="local" value="${localEmDestaque.local}"></c:set>
              <c:set var="sugestaoLocal" value="${localEmDestaque}"></c:set>
            <% } %>
            
            <c:if test="${status.first}">
              <%-- >div style="border-top: 1px solid #eee">
              </div--%>
              <div class="row" style="margin-right: 0px; margin-left: 0px;">
            </c:if>
            
            <div class="${not empty columnSize ? 'col-sm-' : ''}${columnSize}" style="padding-right: 4px; padding-left: 4px; padding-bottom: 8px;">
            
            <fan:cardResponsive type="local" local="${local}" sugestaoLocal="${sugestaoLocal}" colSize="${columnSize}" showActions="${showActions}" cardClass="${cardClass}"
                                name="${local.nome}" selectable="${selectable}" selectableUnique="false" selected="${locaisJaSelecionados.contains(local.id)}"
                                useButtonsRecomendacao="${useButtonsRecomendacao}" buttonRecomendoOn="${locaisJaSelecionados.contains(local.id)}" buttonImperdivelOn="${locaisJaSelecionadosImperdiveis.contains(local.id)}"
                                orientation="${cardOrientation}" cardStyle="${cardStyle};" photoSize="${photoSize}" photoHeight="${photoHeight}" imgStyle="${photoStyle}"
                                titleStyle="${titleStyle}" showLink="false" 
                                showAdditionalInfo="${showAdditionalInfo}" newCard="true">
                <jsp:attribute name="info">
                    <c:if test="${showAddToPlanButton}">
                        <div class="pull-right" style="margin-bottom: 5px;">
                            <fan:addToPlanButton local="${local}" planosViagem="${planosViagem}" />
                        </div>
                    </c:if>
                    <c:if test="${local.tipoHotel}">
                        <div>
                            <c:if test="${not empty local.estrelas and local.estrelas > 0}">
                                <img src="<c:url value="/resources/images/star${local.estrelas}.png"/>"/>
                            </c:if>
                        </div>
                    </c:if>
                    
                    <c:if test="${not empty local.endereco}">
                      <small>
                        ${local.endereco.descricao}
                        ${local.endereco.bairro}
                      </small>
                    </c:if>
                    
                    <div>
                       <c:if test="${local.notaTripAdvisor > 0}">
                           <span class="tripAdvisorRating rate-${fn:replace(local.notaTripAdvisor, '.', '') }" title="Nota dos viajantes site TripAdvisor.com - ${local.quantidadeReviewsTripAdvisor} avaliações"></span>
                        </c:if>
                    </div>
                    
                    <c:if test="${not empty local.idsTags}">
	                    <div style="padding-top: 3px; display: inline-block;">
	                      <small>
	                      <c:set var="tags" value="${local.idsTags}" />
	                      <c:set var="tagsCategorias" value="${tagsLocal.getTagsCategoria(tags)}" />
	                      <c:set var="tagsBomPara" value="${tagsLocal.getTagsBomPara(tags)}" />
	                      <c:if test="${not empty tagsCategorias }">
	                        <div>
	                      	<strong>Categoria:&nbsp;</strong> ${tagsCategorias}
	                      	</div>
	                      </c:if>
	                      <c:if test="${not empty tagsBomPara}">
	                       <div>
	                      	<strong>Bom Para:&nbsp;</strong> ${tagsBomPara}
	                      </div>
	                      </c:if>
	                      </small>
	                     </div>
                    </c:if>
                    
                    <%-- c:if test="${showNotaGeral and not local.tipoHotel}">
                        <c:if test="${localEmDestaque.quantidadeAvaliacoes > 0}">
                            <c:choose>
                                <c:when test="${local.quantidadeAvaliacoes == 1}"><c:set var="textoAvaliacoes" value="${local.quantidadeAvaliacoes} avaliação" /></c:when>
                                <c:otherwise><c:set var="textoAvaliacoes" value="${local.quantidadeAvaliacoes} avaliações" /></c:otherwise>
                            </c:choose>
                            <p style="margin: 0">
                                <span title="Classificado como ${local.avaliacaoConsolidadaGeral.classificacaoGeral.descricao} em um total de ${textoAvaliacoes}" class="starsCategory cat${localEmDestaque.classificacaoNota}" style="display: inline-block;"></span>
                            </p>
                        </c:if>
                    </c:if--%>
                    <c:if test="${showViajantesJaForam}">
                        <c:if test="${localEmDestaque.possuiInteresseJaFoi}">
                            <p style="margin: 0; padding-top: 3px;">
                                <span class="label label-info">${localEmDestaque.descricaoQuantidadeJaForam}</span>
                            </p>
                        </c:if>
                    </c:if>
                    
                    <c:if test="${showAmigosJaForam}">
                        <c:if test="${localEmDestaque.possuiInteresseJaFoi and not empty localEmDestaque.amigosJaForam}">
                            <br/>
                            <ul class="friend-list clearfix" style="overflow: hidden;">
                               <c:forEach items="${localEmDestaque.amigosJaForam}" var="usuario" varStatus="varStatus" >
                                <li>
                                   <fan:userAvatar displayName="false" user="${usuario}" idUsuarioFacebook="${usuario.idFacebook}"
                                                   marginLeft="0" marginRight="0" showFrame="false" imgSize="mini-avatar" positionRelative="false"
                                                   showShadow="false" imgStyle="width: 32px; height: 32px;" displayNameTooltip="true" showFacebookMark="false" />
                                </li>
                               </c:forEach>
                            </ul>
                        </c:if>
                    </c:if>
                    
                    <c:if test="${not empty sugestaoLocal}">
                    	<c:if test="${localEmDestaque.imperdivel}">
		                    <div style="padding-top: 15px; margin-bottom: 4px; font-size: 11px;">
								<fan:userAvatar user="${sugestaoLocal.autor}" displayName="false" width="25" height="25" showImgAsCircle="true" marginLeft="0"
				                                showFrame="false" showShadow="false" imgStyle="margin-top: -10px;" enableLink="false" displayNameTooltip="true" />
				                ${sugestaoLocal.autor.displayName} marcou como <span class="label label-danger">Imperdível</span>
		                    </div>
		                </c:if>
					</c:if>
					
                    <div style="padding-top: 5px;">
                        <c:if test="${showBotaoAdicionar}">
                           <a href="#" class="btn btn-sm btn-success btn-adicionar-plano" title="Adicionar ao meu plano">
                               <%--c:choose>
                                   <c:when test="${local.tipoHotel}">
                                       <span class="glyphicon"></span>
                                       <span class="text">Selecionar</span>
                                   </c:when>
                                   <c:otherwise--%>
                                       <span class="glyphicon glyphicon-plus"></span>
                                       <span class="text">Adicionar</span>
                                   <%--/c:otherwise>
                               </c:choose--%>
                           </a>
                        </c:if>
                        <c:if test="${showBotaoDispensar}">
                           <a href="#" class="btn btn-sm btn-default btn-dispensar-plano" title="Dispensar">
                               <span class="glyphicon glyphicon-remove"></span>
                               <span class="text">Dispensar</span>
                           </a>
                        </c:if>
                        <c:if test="${showReservarBooking}">
                            <c:if test="${local.tipoHotel}">
                               <a href="${local.urlBooking}" class="btn btn-large btn-warning" target="_blank" title="Reservar no Booking.com">
                                 Reservar no Booking.com
                               </a>
                            </c:if>
                        </c:if>
                    </div>

                </jsp:attribute>
                
                <jsp:attribute name="additionalInfo">
                  <div class="row">
                    <c:if test="${local.tipoLocalGeografico}">
                      <c:if test="${local.quantidadeHoteis > 0}">
                        <div class="pull-left" style="width: 30%; text-align: center;">
                          <a href="<c:url value="/locais/${local.urlPath}/hospedar"/>" style="color: #808080;">
                            ${local.quantidadeHoteis} Hotéis
                          </a>
                        </div>
                      </c:if>
                      <c:if test="${local.quantidadeAtracoes > 0}">
                        <div class="pull-left" style="width: 40%; text-align: center;">
                          <a href="<c:url value="/locais/${local.urlPath}/oque-fazer"/>" style="color: #808080;">
                            ${local.quantidadeAtracoes} Atrações
                          </a>
                        </div>
                      </c:if>
                      <c:if test="${local.quantidadeRestaurantes > 0}">
                        <div class="pull-left" style="width: 30%; text-align: center;">
                          <a href="<c:url value="/locais/${local.urlPath}/onde-comer"/>" style="color: #808080;">
                            ${local.quantidadeRestaurantes} Restaurantes
                          </a>
                        </div>
                      </c:if>
                    </c:if>
                  </div>
                </jsp:attribute>
                
                <jsp:attribute name="actions">
                    <div class="card-btn" style="width: 30%;">
                        <fan:marcarLocalButton isResponsive="true" listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" 
                              usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                    </div>
                    <div class="card-btn" style="width: 40%;">
                        <fan:marcarLocalButton isResponsive="true" listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" 
                              usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                    </div>
                    <div class="card-btn" style="width: 30%;">
                      <div class="btn-group">
                          <a type="button" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 11px; cursor: pointer; color: #A8A8A8;">
                            Mais <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu pull-right" role="menu" style="text-align: left">
                            <li>
                                <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" 
                                                       isResponsive="true" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                                
                            </li>
                            <li>
                                <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" 
                                                       isResponsive="true" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                                
                            </li>
                            <li class="divider"></li>
                            <li>
                               <a href="<c:url value="/avaliacao/avaliar/${local.urlPath}"/>" id="btn-avaliar-<%@tag import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>.id}" class="btn btn-mini" title="Escrever uma avaliação">
                                 <img src="<c:url value="/resources/images/icons/award_star_gold_3.png" />"> Escrever uma avaliação
                               </a>
                            </li>
                            <li>
                               <a href="<c:url value="/dicas/escrever/${local.urlPath}"/>" id="btn-dica-${local.id}" class="btn btn-mini" title="Escrever uma dica">
                                 <img src="<c:url value="/resources/images/icons/lightbulb.png" />"> Escrever uma dica
                               </a>
                            </li>
                          </ul>
                      </div>
                    </div>
                </jsp:attribute>
            </fan:cardResponsive>
            
            </div>
            
            <c:if test="${(columns mod coluna == 0 and not status.first) or status.last}">
              </div>
              <c:if test="${not status.last}">
              	<div class="row coluna-${coluna} columns-${columns} mod-${columns mod coluna == 0}" style="margin-right: 0px; margin-left: 0px;">
              </c:if>
            </c:if>
            
        </c:forEach>
    </div>
    <a href="<c:url value="${urlProximos}"/>" class="jscroll-next"> </a>
	
</c:if>

<c:if test="${empty locaisEmDestaque}">
    <div style="height: 150px;"></div>
</c:if>
</compress:html>