<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="id" required="false" type="java.lang.Object" description="Id do botao" %>
<%@ attribute name="value" required="true" type="java.lang.Object" description="Valor para submissao" %>
<%@ attribute name="label" required="false" type="java.lang.String" description="Label que aparece no botao" %>
<%@ attribute name="title" required="false" type="java.lang.String" description="Hint que aparece ao passar o mouse sobre o botao" %>
<%@ attribute name="icon" required="false" type="java.lang.String" description="Classe CSS do icone para aparecer no botao" %>
<%@ attribute name="style" required="false" type="java.lang.String" description="Estilo CSS do botão" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="btnSize" value="btn" />
<c:if test="${size != null && size != 'normal'}">
  <c:set var="btnSize" value="btn btn-${size}" />
</c:if>

<c:if test="${selectedValue != null && selectedValue == value}">
  <c:set var="active" value="active" />
</c:if>

<button id="${id}" class="btn radioBtn ${cssClass} ${btnSize} ${active}" value="${value}" style="${style}" title="${title}">
  <c:if test="${not empty icon}">
    <i class="${icon}"></i>
  </c:if>
  ${label}
</button>