<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="visibilidades" required="true" type="java.util.Collection" %> 
<%@ attribute name="visoesViagem" required="true" type="java.util.Collection" %> 
<%@ attribute name="viagensProximas" required="true" type="java.util.Collection" %> 
<%@ attribute name="viagensRealizadas" required="true" type="java.util.Collection" %> 

<div class="alert alert-info">
	<h2>${visaoListar.descricao}</h2>

	<c:if test="${permissao.usuarioPodeIncluirViagem}">
	<div class="btn-toolbar">
		<div class="btn-group">
			<a href="#formNovaViagemModal" role="button" class="btn btn-mini" data-toggle="modal">
			<img alt="Nova viagem" src="${pageContext.request.contextPath}/resources/images/icons/application_form_add.png" />
				Nova viagem
			</a>
		</div>
	</div>
	</c:if>
</div>

<div id="formNovaViagemModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formNovaViagemModalLabel" aria-hidden="true">
<form id="formInclusaoViagem" action="${pageContext.request.contextPath}/viagem2/adicionar" method="post">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="formNovaViagemModalLabel">Nova viagem</h3>
  </div>
  <div class="modal-body">
	<label for="titulo">Título*: </label><input type="text" name="titulo" />
	<label for="visibilidade">Visibilidade*: </label>
			<select name="visibilidade">
				<c:forEach var="visibilidade" items="${visibilidades}">
					<option value="${visibilidade}">${visibilidade.descricao}</option>
				</c:forEach>
			</select>
	<label for="quantidadeDias">Quantidade de dias: </label>
	<input type="number" name="quantidadeDias" />
	<label for="dataInicio">Data de início: </label>
	<input type="date" name="dataInicio" />
	<label for="descricao">Descrição: </label>
	<textarea name="descricao" rows="5" cols="500"></textarea>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">
    <img alt="Cancelar" src="${pageContext.request.contextPath}/resources/images/icons/cancel.png" />
	Cancelar
	</button>
    <button id="btnFormInclusaoViagem" class="btn btn-primary">
    <img alt="Salvar" src="${pageContext.request.contextPath}/resources/images/icons/table_save.png" />
    Salvar
    </button>
  </div>
</form>  
</div>

<c:if test="${not empty viagensProximas}">
	<div class="alert alert-info">
		<h2>Próximas Viagens</h2>
	</div>

	<fan:viagemListarTable viagens="${viagensProximas}" visoesViagem="${visoesViagem}" titulo="Próximas Viagens" />
</c:if>

<c:if test="${not empty viagensRealizadas}">
	<div class="alert alert-info">
		<h2>Viagens Realizadas</h2>
	</div>

	<fan:viagemListarTable viagens="${viagensRealizadas}" visoesViagem="${visoesViagem}" titulo="Viagens Realizadas" />
</c:if>
