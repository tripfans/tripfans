<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="album" required="true" type="br.com.fanaticosporviagens.model.entity.LocalAlbum" description="Album de um determinado local" %>
<%@ attribute name="indiceInicial" required="false" type="java.lang.Integer" description="Indice inicial das fotos" %>
<%@ attribute name="quantidadeTotalFotos" required="false" type="java.lang.Integer" description="Quantidade total de fotos" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fan" tagdir="/WEB-INF/tags"  %>

<c:set var="indiceFoto" value="0" />

<c:if test="${not empty indiceInicial}">
	<c:set var="indiceFoto" value="${indiceInicial}" />
</c:if>

<c:if test="${not empty quantidadeTotalFotos}">
	<c:set var="quantidadeTotalFotos" value="${album.quantidadeTotalFotos}" />
</c:if>

<h2><fan:localLink local="${album.local}" nomeCompleto="true" destaqueNomeLocal="true" /></h2>
<c:forEach items="${album.fotos}" var="foto" varStatus="status">
	 <c:set var="indiceFoto" value="${indiceFoto + 1}" />
     <a id="box-foto-link-${foto.id}" 
	        href="<c:url value="/fotos/exibir/${foto.id}?i=${indiceFoto}&u=${quantidadeTotalFotos == indiceFoto}"/>" 
	        class="box-foto fancybox" 
	        title="${foto.alt}" 
	        data-id-foto="${foto.id}" 
	        data-index-foto="${indiceFoto}"
	        data-prev-index="${indiceFoto - 1}" 
	        data-next-index="${indiceFoto + 1}">                        
	    <img alt="${foto.alt}" src="${foto.urlSmall}" class="img-polaroid">
	</a>
</c:forEach>
<ul>
<c:forEach items="${album.albunsFilhos}" var="albumFilho" varStatus="statusFilho">
<li><fan:albumFotoLocal album="${albumFilho}" indiceInicial="${indiceFoto}" quantidadeTotalFotos="${quantidadeTotalFotos}" /></li>
<c:set var="indiceFoto" value="${indiceFoto + fn:length(albumFilho.fotos)}" />
</c:forEach>
</ul>

