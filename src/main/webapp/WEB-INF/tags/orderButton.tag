<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.infra.component.SearchData"%>
<%@ attribute name="cssClass" required="false" type="java.lang.String" description="Classe CSS para ser adicionada ao botao" %>
<%@ attribute name="dir" required="true" type="java.lang.String" description="Direcao da ordenacao: ASC ou DESC" %>
<%@ attribute name="orderAttr" required="true" type="java.lang.String" description="Nome do campo a ser utilizado na ordenacao" %>
<%@ attribute name="title" required="true" type="java.lang.String" description="Titulo do botao" %>
<%@ attribute name="tooltip" required="false" type="java.lang.String" description="Tooltip do botao" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<button class="btn btn-mini orderBtn ${cssClass} ${selectedOrder == orderAttr ? 'active' : ''}" orderby="${orderAttr}" dir="${dir}" url="${urlOrdenacao}" 
        data-loading-text="Aguarde..." title="${tooltip}">${title}</button>