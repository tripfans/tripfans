<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="adType" required="true" type="java.lang.Integer" description="Tipo do anúncio: 0 (retângulo lateral), 1 (arranha-céu lateral), 2 (retângulo footer)"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
	<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
      		<c:set var="reqScheme" value="https" />
	</c:when>
	<c:otherwise>
		<c:set var="reqScheme" value="http" />
	</c:otherwise>
</c:choose>

<c:choose>
	<c:when test="${adType == 0}">
		<c:set var="adSlot" value="3212225710" />
		<c:set var="height" value="250px" />
		<c:set var="width" value="270px" />
	</c:when>
	<c:when test="${adType == 1}">
		<c:set var="adSlot" value="4688958917" />
		<c:set var="height" value="600px" />
		<c:set var="width" value="160px" />
	</c:when>
	<c:when test="${adType == 2}">
		<c:set var="adSlot" value="6544455317" />
		<c:set var="height" value="90px" />
		<c:set var="width" value="728px" />
	</c:when>
</c:choose>

<c:if test="${not tripFansEnviroment.devel}">
	<c:if test="${adType == 1}">
	  <div style="text-align: center;">
	</c:if>
	<c:if test="${adType == 2}">
	  <div class="span20" align="center" style="margin-left: 0px;">
	</c:if>
	<script async src="${reqScheme}://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<ins class="adsbygoogle" style="display:inline-block;width:${width};height:${height}" data-ad-client="ca-pub-6849076210763348" data-ad-slot="${adSlot}"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	<c:if test="${adType == 1 || adType == 2}">
	</div>
	</c:if>
</c:if>