<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="cssClass" required="false" type="java.lang.String" description="Classe CSS para ser colocada no campo" %>
<%@ attribute name="cssStyle" required="false" type="java.lang.String" description="Estilo CSS para ser colocado no campo" %>
<%@ attribute name="datePicker" required="false" type="java.lang.Boolean" description="Se deve ou não ter o DatePicker do Jquery" %>
<%@ attribute name="id" required="true" type="java.lang.String" description="Identificador HTML do elemento" %>
<%@ attribute name="textOnly" required="false" type="java.lang.Boolean" description="Se o campo vai ser utilizado somente para exibição de texto" %>
<%@ attribute name="maxDate" required="false" type="java.util.Date" description="Maior data a ser aceita no campo" %>
<%@ attribute name="minDate" required="false" type="java.util.Date" description="Menor data a ser aceita no campo" %>
<%@ attribute name="name" required="true" type="java.lang.Object" description="Nome do campo para ser usado no submit de um formulário" %>
<%@ attribute name="value" required="false" type="java.lang.Object" description="Valor a ser exibido no campo" %>
<%@ attribute name="placeholder" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%
    // TODO ver se vamos mudar tudo para JodaTime
    java.util.Date data = null;
    if (value != null) {
        if (value instanceof org.joda.time.LocalDate) {
            data = ((org.joda.time.LocalDate) value).toDate();
        } else {
            data = (java.util.Date) value;
        }
    }
%>

<c:choose>
<c:when test="${pageContext.response.locale == 'en_US'}">
	<fmt:formatDate value="<%=data%>" pattern="yyyy/MM/dd" var="formattedValue" />
	<c:set var="dateMask" value="y/m/d" />
</c:when>
<c:otherwise>
	<fmt:formatDate value="<%=data%>" pattern="dd/MM/yyyy" var="formattedValue" />
	<c:set var="dateMask" value="d/m/y" />
</c:otherwise>
</c:choose>

<c:choose>
<c:when test="${textOnly == true}">
	<c:out value="${formattedValue}"/>
</c:when>
<c:otherwise>
	<jsp:useBean id="date" class="java.util.Date" />
	<fmt:formatDate value="${date}" pattern="yyyy" var="currentYear" />
	<c:set var="startYear" value="${currentYear - 100}" />
	<c:if test="${datePicker == null}"><c:set var="datePicker" value="true" /></c:if>
	<input type="text" name="${name}" id="${id}" class="${cssClass}" value="${formattedValue}" style="${cssStyle}" placeholder="${placeholder}" />
	<c:if test="${datePicker == true}">
	<script type="text/javascript">
	$('#${id}').datepicker( {
	    changeMonth: true,
	    changeYear: true,
	    <c:if test="${not empty value}">
	    defaultDate: new Date('<fmt:formatDate value="<%=data%>" pattern="yyyy" />', '<fmt:formatDate value="<%=data%>" pattern="MM" />' - 1, '<fmt:formatDate value="<%=data%>" pattern="dd" />'),
	    </c:if>
	    <c:if test="${not empty minDate}">
	    minDate: new Date('<fmt:formatDate value="${minDate}" pattern="yyyy" />', '<fmt:formatDate value="${minDate}" pattern="MM" />' - 1, '<fmt:formatDate value="${minDate}" pattern="dd" />'), 
	    </c:if>
	    <c:if test="${not empty maxDate}">
	    maxDate: new Date('<fmt:formatDate value="${maxDate}" pattern="yyyy" />', '<fmt:formatDate value="${maxDate}" pattern="MM" />' - 1, '<fmt:formatDate value="${maxDate}" pattern="dd" />'), 
	    </c:if>
	    yearRange: '${startYear}:${currentYear}'
	});
	
	<c:if test="${not empty formattedValue}"></c:if>
	
	$(document).ready(function () {
		$('#${id}').inputmask('${dateMask}');
	});
	</script>
	</c:if>
</c:otherwise>
</c:choose>