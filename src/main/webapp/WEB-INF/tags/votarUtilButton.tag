<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@tag import="br.com.fanaticosporviagens.model.entity.Resposta"%>
<%@tag import="br.com.fanaticosporviagens.model.entity.Avaliacao"%>
<%@tag import="br.com.fanaticosporviagens.model.entity.Dica"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ attribute name="cssButtonClassSize" required="false" type="java.lang.String" description="Classe Css para o tamanho do botao" %>
<%@ attribute name="target" required="true" type="java.lang.Object" description="O objeto que deve ser marcado como util: uma avaliacao, dica, resposta etc." %>

<c:if test="${empty cssButtonClassSize}"><c:set var="cssButtonClassSize" value="btn-small btn-sm" /></c:if>

<c:set var="idButton" value="<%= RandomStringUtils.random(12, true, true) %>" />

<c:choose>
	<c:when test="<%= target.getClass().getSimpleName().equals(\"Avaliacao\") %>">
		<c:set var="target" value="<%= (Avaliacao) target %>" />
		<c:url var="urlMarcar" value="/voto/avaliacao/votarComoUtil/${target.urlPath }" />
	</c:when>
	<c:when test="<%= target.getClass().getSimpleName().equals(\"Dica\") %>">
		<c:set var="target" value="<%= (Dica) target %>" />
		<c:url var="urlMarcar" value="/voto/dica/votarComoUtil/${target.urlPath }" />
	</c:when>
	<c:when test="<%= target.getClass().getSimpleName().equals(\"Resposta\") %>">
		<c:set var="target" value="<%= (Resposta) target %>" />
		<c:url var="urlMarcar" value="/voto/resposta/votarComoUtil/${target.urlPath }" />
	</c:when>
</c:choose>

<c:if test="${avaliacao.quantidadeVotoUtil > 0}">
  <c:set var="hint" value="<strong>${avaliacao.quantidadeVotoUtil}</strong> ${avaliacao.quantidadeVotoUtil == 1 ? 'marcou' : 'marcaram'} como útil" />
</c:if>

<sec:authorize access="isAuthenticated()">
  <a id="${idButton}" class="btn ${cssButtonClassSize} btn-primary" title="Marcar como útil <br> ${hint}">
	<i class="icon-thumbs-up icon-white glyphicon glyphicon-thumbs-up"></i><span><c:if test="${target.quantidadeVotoUtil != 0}">&nbsp;${target.quantidadeVotoUtil}</c:if></span>
  </a>
</sec:authorize>
<c:if test="${usuario == null}">
  <a href="<c:url value="/entrarPop" />" class="btn ${cssButtonClassSize} btn-primary" title="Marcar como útil <br> ${hint}">
    <i class="icon-thumbs-up icon-white glyphicon glyphicon-thumbs-up"></i><span><c:if test="${target.quantidadeVotoUtil != 0}">&nbsp;${target.quantidadeVotoUtil}</c:if></span>
  </a>
</c:if>
    
<script type="text/javascript">

$(document).ready(function() {
	$('#${idButton}').click(function(event) {
		if($(this).attr('class').indexOf('disabled') == -1) {
			$.post('${urlMarcar}', function(data) {
				var imageUrl = '';
				if(data.params.success) {
					$('#${idButton}').addClass('disabled');
					$('#${idButton} > span').html('&nbsp;'+data.params.quantidadeVotoUtil);
					imageUrl = '<c:url value="/resources/images/info.png" />';
				} else {
					imageUrl = '<c:url value="/resources/images/warn.png" />';
				}
				
				$.gritter.add({
					title: 'Informação',
					text: data.params.message,
					time: 8000,
					image: imageUrl
				});
			});
		} else {
			// desmarcar util?
		}
	});
});

</script>