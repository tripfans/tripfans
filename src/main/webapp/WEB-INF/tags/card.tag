<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.apache.commons.lang.RandomStringUtils"%>

<%@ attribute name="type" required="true" type="java.lang.String" description="user, local, viagem" %>
<%@ attribute name="user" required="false" type="br.com.fanaticosporviagens.model.entity.Usuario" %>
<%@ attribute name="local" required="false" type="br.com.fanaticosporviagens.model.entity.Local" %>
<%@ attribute name="friendship" required="false" type="br.com.fanaticosporviagens.model.entity.Amizade" %> 
<%@ attribute name="trip" required="false" type="br.com.fanaticosporviagens.model.entity.Viagem" %> 

<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="nameStyle" required="false" type="java.lang.String" %>
<%@ attribute name="itemId" required="false" type="java.lang.String" description="ID do usuario, local, amizde, etc" %>
<%@ attribute name="itemUrl" required="false" type="java.lang.String" %>
<%@ attribute name="photoUrl" required="false" type="java.lang.String" %>
<%@ attribute name="showActions" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selectable" required="false" type="java.lang.Boolean" %>

<%@ attribute name="imgSize" required="false" type="java.lang.String" description="Tamanho da imagem em classe CSS .span" %>
<%@ attribute name="width" required="false" type="java.lang.Integer" %> 
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="showBorder" required="false" type="java.lang.Boolean" %>
<%@ attribute name="imgStyle" required="false" type="java.lang.String" %>
<%@ attribute name="imgClass" required="false" type="java.lang.String" %>
<%@ attribute name="panoramioWidth" required="false" type="java.lang.Integer" description="Largura da imagem do panoramio em pixels" %>
<%@ attribute name="panoramioHeight" required="false" type="java.lang.Integer" description="Altura da imagem do panoramio em pixels" %>

<%@ attribute name="marginLeft" required="false" type="java.lang.Integer" %> 
<%@ attribute name="marginRight" required="false" type="java.lang.Integer" %>
<%@ attribute name="spanSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="cardStyle" required="false" type="java.lang.String" %>
<%@ attribute name="cardClass" required="false" type="java.lang.String" %>

<%@ attribute name="subtitle" fragment="true" required="false" %>
<%@ attribute name="info" fragment="true" required="false" %>
<%@ attribute name="aditionalInfo" fragment="true" required="false" %>
<%@ attribute name="actions" fragment="true" required="false" %>

<%@ attribute name="id" required="false" type="java.lang.String" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% String idUnico = RandomStringUtils.random(12, true, true); %>

<c:if test="${height == null}"><c:set var="height" value="50" /></c:if>
<c:if test="${width == null}"><c:set var="width" value="50" /></c:if>
<c:if test="${marginLeft == null}"><c:set var="marginLeft" value="5" /></c:if>
<c:if test="${marginRight == null}"><c:set var="marginRight" value="5" /></c:if>
<c:if test="${displayName == null}"><c:set var="displayName" value="true" /></c:if>
<c:if test="${showFrame == null}"><c:set var="showFrame" value="true" /></c:if>
<c:if test="${imgSize == null}"><c:set var="imgSize" value="1" /></c:if>
<c:if test="${spanSize == null}"><c:set var="spanSize" value="4" /></c:if>
<c:if test="${id == null}"><c:set var="id" value="<%= idUnico %>" /></c:if>
<c:if test="${showActions == null}"><c:set var="showActions" value="true" /></c:if>
<c:if test="${panoramioWidth == null}"><c:set var="panoramioWidth" value="220" /></c:if>
<c:if test="${panoramioHeight == null}"><c:set var="panoramioHeight" value="170" /></c:if>

<c:choose>
    <c:when test="${not empty user}">
        <c:url value="/perfil/${user.urlPath}" var="itemUrl" />
        <c:set value="${user.urlFotoPerfil}" var="photoUrl" />
        <c:set value="${user}" var="usuarioCard"/>
        <c:set value="${user.displayName}" var="name" />
    </c:when>
    <c:when test="${not empty friendship}">
        
        <sec:authorize access="@securityService.isFriend('${friendship.amigo.id}')">
          <c:set var="mostrarLinkAmigo" value="true" />
          <c:if test="${usuario.id != perfil.id}">
            <c:set var="isAmigoComum" value="true" />
          </c:if>
        </sec:authorize>
        
        <c:set value="${friendship.nomeAmigo}" var="name" />
        <c:set value="${friendship.idAmigoFacebook}" var="itemId" />
        <c:if test="${friendship.tripfans}">
            <c:set var="mostrarLinkAmigo" value="true" />
            <c:url value="/perfil/${friendship.amigo.urlPath}" var="itemUrl" />
            <c:set value="${friendship.amigo}" var="usuarioCard"/>
            <c:set value="${usuarioCard.urlFotoPerfil}" var="photoUrl" />
        </c:if>
        <c:if test="${friendship.somenteFacebook}">
            <c:url var="showFacebookMark" value="true" />
            <c:set var="idFacebook" value="${friendship.idAmigoFacebook}" />
            <c:url value="/perfil/fb/${friendship.idAmigoFacebook}" var="itemUrl" />
            <c:url value="http://graph.facebook.com/${friendship.idAmigoFacebook}/picture" var="photoUrl" />
        </c:if>
    </c:when>
    <c:when test="${not empty local}">
        <c:url value="${local.url}" var="itemUrl" />
        <c:set value="${local.urlFotoAlbum}" var="photoUrl" />
        <c:if test="${local.tipoCidade}">
            <c:set value="${local.nomeComSiglaEstadoNomePais}" var="name" />
        </c:if>
    </c:when>
    <c:when test="${not empty trip}">
      <c:if test="${empty photoUrl}">
      	<c:set var="photoUrl" value="${pageContext.request.contextPath}/resources/images/travel.jpg" />
      </c:if>
      
      <c:set value="${trip.nome}" var="name" />
    </c:when>
    <c:otherwise>
        <c:url value="${itemUrl}" var="itemUrl" />
    </c:otherwise>
</c:choose>

<c:choose>

    <c:when test="${not empty local or type == 'local'}">
        
        <div id="${id}" data-id="${itemId}" class="span${spanSize} local-card borda-arredondada ${cardClass}" style="margin-left: .3%; margin-right: .2%; min-height: 200px;">
        	<div class="row" style="margin-left: ${not local.fotoPadraoAnonima ? 'inherit' : '-8px' }; position: relative; min-height: 170px; max-height: 170px;">
                <div class="origem-foto">
                  <c:choose>
                    <c:when test="${local.usarFotoPanoramio}">
                      <img src="<c:url value="/resources/images/logos/panoramio-o.png" />" width="20" height="20" title="Origem da foto: Panoramio">
                    </c:when>
                    <c:when test="${not local.fotoPadraoAnonima}">
                      <c:if test="${local.fotoPadrao.tipoArmazenamentoLocal}">
                        <img src="<c:url value="/resources/images/logos/tripfans-small.png" />" width="20" height="20" title="Origem da foto: TripFans">
                      </c:if>
                      <c:if test="${local.fotoPadrao.tipoArmazenamentoExterno}">
                        <c:if test="${not local.fotoPadrao.foto500px}">
                         <!-- Simbolo do 500px -->
                        </c:if>
                      </c:if>
                    </c:when>
                    <c:otherwise>
                    </c:otherwise>
                  </c:choose>
                </div>
                
	            <div id="panoramio-image-${id}" align="${local.fotoPadraoAnonima ? 'center' : 'left'}" class="span${imgSize} card-photo" style="${local.usarFotoPanoramio or local.fotoPadraoAnonima ? 'margin-left: 8px;' : 'margin-left: -1px;'} margin-right: 11px; ${local.fotoPadraoAnonima ? 'background-color: #e8e8e8;' : ''} min-height: 171px; position: relative;">
	                <a href="${itemUrl}">
	                    <img src="${photoUrl}" class="${not local.fotoPadraoAnonima ? 'local-card-img-center' : 'transparencia-75'} borda-arredondada-cima-esquerda ${imgClass}" style="${imgStyle}; min-height: ${local.fotoPadraoAnonima ? '100px; max-width: 100px; margin-top: 30px; margin-left: 24%; margin-right: 24%;' : '171px'}">
	                </a>
                    <c:if test="${local.fotoPadrao.foto500px}">
                        <p class="foto-externa-info " style="opacity: .9;">
                          <span class="pull-right" style="margin-right: 8px;">
                            Por <a class="foto-externa-autor" target="_blank" href="${local.fotoPadrao.urlAutorImagem}">${local.fotoPadrao.idUsuarioFoto500px}</a>
                            em <a class="foto-externa-fonte" target="_blank" href="${local.fotoPadrao.urlFonteImagem}">${local.fotoPadrao.fonteImagem}</a>
                          </span>
                        </p>
                    </c:if>
	            </div>
	            <div style="margin-left: 10px; max-height: 170px; overflow: hidden;">
	                <h3 class="local-card-title card-name">
	                  <a href="${itemUrl}">
	                    ${name}
	                  </a>
	                </h3>
	                <div class="local-card-rate" style="display: block;">
	                    <jsp:invoke fragment="subtitle" />
                        <div>
                            <c:if test="${local.tipoHotel and not empty local.estrelas and local.estrelas > 0}">
                              <span title="Hotel ${local.estrelas} estrela${local.estrelas > 1 ? 's' : ''}" style="margin-top: 5px; display: inline-block;" class="starsRating cat${local.estrelas}"></span>
                            </c:if>
                        </div>
	                </div>
	                <div class="local-card-info" style="display: block;">
	                    <jsp:invoke fragment="info" />
	                </div>
	                <div style="display: block;">
	                </div>
                </div>
            </div>
            <c:if test="${showActions == true}">
	            <div class="row" style="margin-left: inherit; background-color: rgb(244, 245, 244); min-height: 32px;" >
		            <div style="margin-left: 0px; width: 100%;">
		                <jsp:invoke fragment="actions" />
		            </div>
	            </div>
            </c:if>
        </div>
        
        <c:if test="${local.usarFotoPanoramio and not empty local.coordenadaGeografica.latitude}">
            <c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
            <c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
            <c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
            <c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />
            
            <c:if test="${not unsuportedIEVersion}">
              <script type="text/javascript">
                var requestOptions = {
                    <c:if test="${not empty local.idFotoPanoramio}">
                    'ids': [{'photoId': ${local.idFotoPanoramio}, 'userId': ${local.idUsuarioFotoPanoramio}}],
                    </c:if>
                    <c:if test="${empty local.idFotoPanoramio}">
                    //'tag': 'urban',
                    'set' : 'public',
                    'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}},
                    </c:if>
                };
                var request = new panoramio.PhotoRequest(requestOptions);
                var widgetOptions = {
                    'width': ${panoramioWidth},
                    'height': ${panoramioHeight},
                    'bgcolor' : 'white',
                    'croppedPhotos' : panoramio.Cropping.TO_FILL,
                    'attributionStyle': panoramio.tos.Style.HIDDEN,
                    'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
                };
                
                var photoWidget_${id} = new panoramio.PhotoWidget('panoramio-image-${id}', request, widgetOptions);
                
                //photo_ex_widget_${id}.enablePreviousArrow(false);
                //photo_ex_widget_${id}.enableNextArrow(false);

                function photoChanged(event) {
                    var $div = $('#${id}')
                	if (event.target.getPhoto() == null) {
                		var $divFoto = $div.find('.card-photo');
                		// esconder widget
                		$div.find('.panoramio-wapi-photo').hide();
                		$div.find('.origem-foto').remove();
                		// foto padrao
                		$divFoto.append('<div align="center" style="width: 100%; display: inline-table; "><a href="${itemUrl}">' +
                                '<img src="${photoUrl}" class="local-card-img borda-arredondada ${imgClass}" style="${imgStyle}; float: center; min-height: 100px; max-width: 100px; margin-top: 30px;">' + 
                                '</a></div>');
                	} else {
                	    $div.find('.panoramio-wapi-photo').css('max-width', '100%');
                	}
                }
                
                function photoClicked(event) {
                	document.location.href = "${itemUrl}"
            	    return false;
            	}
            	panoramio.events.listen(photoWidget_${id}, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);
            	panoramio.events.listen(photoWidget_${id}, panoramio.events.EventType.PHOTO_CHANGED, photoChanged);
                
            	photoWidget_${id}.setPosition(0);
            	
            	$('#${id}').find('.panoramio-wapi-crop-div').css('width', '').css('height', '');
              </script>
        
              <style>
                #panoramio-image-${id} .panoramio-wapi-images {
                    background-color: transparent;
                }
                
                #panoramio-image-${id} .panoramio-wapi-crop-div {
                    width: 190px; 
                    height: 160px;
                    border-radius: 4px 0px 0px 0px;
                }
              </style>
            </c:if>
        </c:if>
        
    </c:when>
    
    <c:when test="${not empty trip}">
        <c:if test="${empty marginLeft}">
            <c:set var="marginLeft" value="-15"/>
        </c:if>
        <c:if test="${empty marginRight}">
            <c:set var="marginRight" value="-15"/>
        </c:if>

        <div id="${id}" class="span${spanSize} local-card borda-arredondada" style="margin-left: .3%; margin-right: .3%; min-height: 172px;">
            <div class="span${imgSize} col-md-${imgSize}" style="margin-left: ${marginLeft}px; margin-right: ${marginRight}px;">
                <a href="${itemUrl}">
                    <%-- img src="${photoUrl}" class="img-responsive carrossel-thumb sombra borda-arredondada" style="${imgStyle}; max-height: 141px;"--%>
                    <i class="carrossel-thumb borda-arredondada-esquerda" style="background-image: url('${photoUrl}'); height: 170px;"></i>
                </a>
            </div>
            <div class="col-md-${spanSize - imgSize}">
                <h3 class="local-card-title">
                  <a href="<c:url value="/viagem/${viagem.id}" />">
                                    ${viagem.titulo}
                                </a>
                  <a href="${itemUrl}">
                    ${name}
                  </a>
                </h3>
                <div class="local-card-rate" style="display: block;">
                    <jsp:invoke fragment="subtitle" />
                </div>
                <div class="local-card-info" style="display: block;">
                    <jsp:invoke fragment="info" />
                </div>
                <div style="display: block;">
                </div>
            </div>
            <div class="span${spanSize}" style="margin-left: 0px;">
                <jsp:invoke fragment="actions" />
            </div>
        </div>
        
    </c:when>
    
    <c:otherwise>

        <div id="${id}" class="span${spanSize} well card ${cardClass}" style="${cardStyle}" data-id="${itemId}" data-fb-id="${idFacebook}" data-name="${name}">
        
            <table cellpadding="5" class="gray" style="width: 100%; height: 100%;">
                <tr class="${selectable ? 'card-content' : ''}">
                    <td valign="top" class="card-photo" width="${width + 6}">
                        <div style="float:left; margin-left:${marginLeft}px; margin-right: ${marginRight}px;">
                          <c:if test="${mostrarLinkAmigo}">
                            <a href="${itemUrl}" target="_blank">
                          </c:if>
                          
                              <fan:userAvatar imgSize="mini-small" user="${usuarioCard}" enableLink="false" showFacebookMark="${showFacebookMark}" idUsuarioFacebook="${idFacebook}" displayName="false" displayNameTooltip="false" width="${width}" height="${height}" />
                                
                          <c:if test="${mostrarLinkAmigo}">
                            </a>
                          </c:if>
                        </div>
                    </td>
                    <td valign="top">
                        <table width="100%">
                            <tr valign="top">
                                <td style="font-weight:bold" valign="top" class="card-name">
                                  <c:if test="${mostrarLinkAmigo}">
                                    <a href="${itemUrl}" target="_blank">
                                  </c:if>
                                  
                                    <span class="" style="${nameStyle}; color: #0088cc;">
                                        ${name}
                                    </span>
                                    
                                  <c:if test="${mostrarLinkAmigo}">
                                    </a>
                                  </c:if>
                                </td>
                            </tr>
                            <c:if test="${isAmigoComum}">
                                <tr>
                                    <td style="color: #898989;">
                                      <span class="badge">
                                        Amigo em comum
                                      </span>
                                    </td>
                                </tr>
                            </c:if>
                            <tr>
                                <td class="card-info">
                                    <jsp:invoke fragment="info" />
                                </td>
                            </tr>
                            <tr>
                                <td class="card-aditionalInfo">
                                    <jsp:invoke fragment="aditionalInfo" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <c:if test="${selectable}">
                        <td align="right" valign="bottom">
                            <input type="checkbox" class="card-check" data-user-id="" data-user-id-facebook="${idFacebook}" />
                        </td>
                    </c:if>
                </tr>
        
                <c:if test="${showActions}">
                    <tr style="background-color: #d9edf7; border-top: 1px solid #bce8f1;">
                        <td colspan="3" style="height: 24px; min-height: 26px;">
                            <jsp:invoke fragment="actions" />
                        </td>
                    </tr>
                </c:if>
            </table>
        </div>

	</c:otherwise>
</c:choose>