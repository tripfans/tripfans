<%@ tag pageEncoding="UTF-8" import="org.apache.commons.lang.RandomStringUtils" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="tipo" required="true" type="java.lang.String" description="Indica qual objeto esta sendo comentado." %>
<%@ attribute name="idAlvo" required="false" type="java.lang.String" %>
<%@ attribute name="idComentarioPai" required="false" type="java.lang.String" %>
<%@ attribute name="url" required="false" type="java.lang.String" %>
<%@ attribute name="inputType" required="false" type="java.lang.String" description="text ou textarea" %>
<%@ attribute name="inputClass" required="false" type="java.lang.String" description="css class do input" %>
<%@ attribute name="actionsClass" required="false" type="java.lang.String" description="css class container das acoes" %>
<%@ attribute name="imgSize" required="false" type="java.lang.String" description="classe css da imagem do usuario (avatar, profile, mini-avatar etc.)" %>
<%@ attribute name="buttonOnSameLine" required="false" type="java.lang.Boolean" %>

<c:if test="${url == null}">
    <c:url value="/comentarios/postarComentario" var="url" />
</c:if>
<c:if test="${inputType == null || inputType != 'text'}">
    <c:set value="${inputType}" var="textarea" />
</c:if>
<c:set var="formClass" value="comentarioForm" />
<c:if test="${idComentarioPai != null}">
    <c:set var="formClass" value="subComentarioForm" />
</c:if>

<% 
  String idBotaoPostFB = RandomStringUtils.random(12, true, true);
  request.setAttribute("idBotaoPostFB", idBotaoPostFB);
%>
<sec:authorize access="isAuthenticated()">

  <form action="${url}" class="${formClass}" data-comentario-pai-id="${idComentarioPai}" style="margin: 0 0 6px;">
    
    <div>
        <input type="hidden" name="comentarioPai" value="${idComentarioPai}" />
        <input type="hidden" name="tipo" value="${idComentarioPai != null ? 'COMENTARIO' : tipo}" />
        <input type="hidden" name="idObjetoAlvo" value="${idAlvo}" />

        <div class="row inputComentario" style="margin-left: 0px;">
            <div>
                <fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" marginLeft="0" marginRight="5" imgSize="${imgSize}" />
            </div>
            
            <div style="margin-left: 8px;">
                <%--${inputType == 'text' ? 'input type="text"' : 'textarea'} class="${inputClass}" id="texto-comentario-${id}" name="texto" placeholder="Escreva um comentário" style="min-width: 60%;"></${inputType == 'text' ? 'input' : 'textarea'}--%>
                <input type="text" class="${inputClass}" id="texto-comentario-${id}" name="texto" placeholder="Escreva um comentário" style="min-width: 60%;" />
                <%--c:if test="${buttonOnSameLine}">
                    <input type="submit" value="Enviar" class="btn btn-info" style="${inputType == 'text' ? 'margin-top: -8px;' : ''}" />
                </c:if--%>
                <fan:shareButton id="btnCheckPostFB_${idBotaoPostFB}" facebook="true" usarComoCheckbox="true" hint="Selecione esta opção para compartilhar o seu comentário com seus amigos." iconStyle="margin-top: -9px;" />
                <input id="checkPostFB_${idBotaoPostFB}" type="checkbox" name="postarNoFacebook" value="true" class="hide" />
                <input type="submit" value="Enviar" class="btn btn-info" style="margin-top: -9px;" />
            </div>
            
        </div>
        
        <%--c:if test="${not buttonOnSameLine}">
          <div>
            <div class="${actionsClass}">
            	<div class="pull-right">
                  <fan:shareButton id="btnCheckPostFB_${id}" facebook="true" usarComoCheckbox="true" hint="Selecione esta opção para compartilhar o seu comentário com seus amigos." />
                  <input id="checkPostFB_${id}" type="checkbox" name="postarNoFacebook" value="true" class="hide" />
                  <input type="submit" value="Enviar" class="btn btn-info" />
                </div>
            </div>
          </div>
        </c:if--%>
        
    </div>
    
    <input type="hidden" name="perfil" value="${perfil.id}" />
            
  </form>

<script>

$(document).ready(function () {
	
    <c:if test="${idComentarioPai == null}">
    
    var $comentarioForm = $('.comentarioForm');
    $comentarioForm.unbind('submit');
    
    $comentarioForm.submit(function(event) {
    	var selectedFBShare = $('#btnCheckPostFB_${idBotaoPostFB}').data('selected');
    	$('#checkPostFB_${idBotaoPostFB}').attr('checked', selectedFBShare);
        event.preventDefault();
        if ($(this).valid()) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(data) {
                    $('#listaComentarios-${id}').html(data);
                    $('#listaComentarios-${id}').parent().effect('highlight', {}, 5000);
                    $('#texto-comentario-${id}').val('');
                    $('#texto-comentario-${id}').focus();
                }
            });
        }
    });
    </c:if>
    
    var $subComentarioForm = $('.subComentarioForm');
    $subComentarioForm.unbind('submit');
    
    $subComentarioForm.submit(function(event) {
        event.preventDefault();
        var idComentarioPai = $(this).attr('data-comentario-pai-id');
        if ($(this).valid()) {
            $.ajax({
                type: 'POST',
                url: '<c:url value="/comentarios/postarComentario" />',
                data: $(this).serialize(),
                success: function(response) {
                    var comentarioContent = $('#box-comentario-content-' + idComentarioPai);
                    comentarioContent.html(response);
                    comentarioContent.parent().find('input[name="texto"]').val('').focus();
                }
            });
        }
    });
    
    var validar = { 
        rules: {
            texto: { 
                required: true, 
                maxlength : 300
            }
        }, 
        messages: { 
            texto: {
                required : 'Por favor, escreva algo'
            }
        },
        errorPlacement: function(error, element) {
            var parent = element.parents('.inputComentario')[0];
            error.appendTo(parent);
        }
    }
    
    $('.comentarioForm').validate(validar);
    $('.subComentarioForm').validate(validar);

});
</script>

</sec:authorize>
