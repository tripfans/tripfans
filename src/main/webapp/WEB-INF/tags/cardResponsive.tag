<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.apache.commons.lang.RandomStringUtils"%>

<%@ attribute name="type" required="true" type="java.lang.String" description="user, local, viagem" %>
<%@ attribute name="user" required="false" type="br.com.fanaticosporviagens.model.entity.Usuario" %>
<%@ attribute name="local" required="false" type="br.com.fanaticosporviagens.model.entity.Local" %>
<%@ attribute name="friendship" required="false" type="br.com.fanaticosporviagens.model.entity.Amizade" %> 
<%@ attribute name="trip" required="false" type="br.com.fanaticosporviagens.model.entity.Viagem" %> 

<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="nameStyle" required="false" type="java.lang.String" %>
<%@ attribute name="itemId" required="false" type="java.lang.String" description="ID do usuario, local, amizde, etc" %>
<%@ attribute name="itemUrl" required="false" type="java.lang.String" %>
<%@ attribute name="photoUrl" required="false" type="java.lang.String" %>
<%@ attribute name="photoSize" required="false" type="java.lang.String" %>
<%@ attribute name="showActions" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showAdditionalInfo" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selectable" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selectableUnique" required="false" type="java.lang.Boolean" %>
<%@ attribute name="selected" required="false" type="java.lang.Boolean" %>
<%@ attribute name="useButtonsRecomendacao" required="false" type="java.lang.Boolean" %>
<%@ attribute name="buttonRecomendoOn" required="false" type="java.lang.Boolean" %>
<%@ attribute name="buttonImperdivelOn" required="false" type="java.lang.Boolean" %>

<%@ attribute name="newCard" required="false" type="java.lang.Boolean" %>
<%@ attribute name="useLazyLoadImg" required="false" type="java.lang.Boolean" %>

<%@ attribute name="colSize" required="false" type="java.lang.String" description="Tamanho coluna em CSS .col-sm" %>
<%@ attribute name="photoColSize" required="false" type="java.lang.String" description="Tamanho coluna da foto em CSS .col-sm" %>
<%@ attribute name="infoColSize" required="false" type="java.lang.String" description="Tamanho coluna da Info em CSS .col-sm" %>
<%@ attribute name="orientation" required="false" type="java.lang.String" description="horizontal ou vertical" %>
<%@ attribute name="photoHeight" required="false" type="java.lang.String" description="Altura da foto" %>
<%@ attribute name="width" required="false" type="java.lang.Integer" %> 
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="showBorder" required="false" type="java.lang.Boolean" %>
<%@ attribute name="imgStyle" required="false" type="java.lang.String" %>
<%@ attribute name="imgClass" required="false" type="java.lang.String" %>
<%@ attribute name="panoramioWidth" required="false" type="java.lang.Integer" description="Largura da imagem do panoramio em pixels" %>
<%@ attribute name="panoramioHeight" required="false" type="java.lang.Integer" description="Altura da imagem do panoramio em pixels" %>
<%@ attribute name="showLink" required="false" type="java.lang.Boolean" %>
<%@ attribute name="sugestaoLocal" required="false" type="br.com.fanaticosporviagens.model.entity.SugestaoLocal" %>

<%@ attribute name="marginLeft" required="false" type="java.lang.Integer" %> 
<%@ attribute name="marginRight" required="false" type="java.lang.Integer" %>
<%@ attribute name="spanSize" required="false" type="java.lang.Integer" %>
<%@ attribute name="cardStyle" required="false" type="java.lang.String" %>
<%@ attribute name="cardClass" required="false" type="java.lang.String" %>
<%@ attribute name="titleStyle" required="false" type="java.lang.String" %>

<%@ attribute name="subtitle" fragment="true" required="false" %>
<%@ attribute name="info" fragment="true" required="false" %>
<%@ attribute name="additionalInfo" fragment="true" required="false" %>
<%@ attribute name="actions" fragment="true" required="false" %>

<%@ attribute name="id" required="false" type="java.lang.String" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% String idUnico = RandomStringUtils.random(12, true, true); %>

<c:if test="${height == null}"><c:set var="height" value="50" /></c:if>
<c:if test="${width == null}"><c:set var="width" value="50" /></c:if>
<c:if test="${marginLeft == null}"><c:set var="marginLeft" value="5" /></c:if>
<c:if test="${marginRight == null}"><c:set var="marginRight" value="5" /></c:if>
<c:if test="${displayName == null}"><c:set var="displayName" value="true" /></c:if>
<c:if test="${showFrame == null}"><c:set var="showFrame" value="true" /></c:if>
<c:if test="${imgSize == null}"><c:set var="imgSize" value="1" /></c:if>
<c:if test="${spanSize == null}"><c:set var="spanSize" value="4" /></c:if>
<c:if test="${id == null}"><c:set var="id" value="<%= idUnico %>" /></c:if>
<c:if test="${showActions == null}"><c:set var="showActions" value="true" /></c:if>
<c:if test="${panoramioWidth == null}"><c:set var="panoramioWidth" value="220" /></c:if>
<c:if test="${panoramioHeight == null}"><c:set var="panoramioHeight" value="170" /></c:if>
<c:if test="${orientation == null}"><c:set var="orientation" value="vertical" /></c:if>
<c:if test="${photoColSize == null}"><c:set var="photoColSize" value="4" /></c:if>
<c:if test="${infoColSize == null}"><c:set var="infoColSize" value="8" /></c:if>
<c:if test="${showAdditionalInfo == null}"><c:set var="showAdditionalInfo" value="true" /></c:if>
<c:if test="${useButtonsRecomendacao == null}"><c:set var="useButtonsRecomendacao" value="false" /></c:if>

<c:choose>
    <c:when test="${not empty user}">
        <c:url value="/perfil/${user.urlPath}" var="itemUrl" />
        <c:set value="${user.urlFotoPerfil}" var="photoUrl" />
        <c:set value="${user}" var="usuarioCard"/>
        <c:set var="idFacebook" value="${user.idFacebook}" />
        <c:set value="${user.displayName}" var="name" />
    </c:when>
    <c:when test="${not empty friendship}">
        
        <sec:authorize access="@securityService.isFriend('${friendship.amigo.id}')">
          <c:set var="mostrarLinkAmigo" value="true" />
          <c:if test="${usuario.id != perfil.id}">
            <c:set var="isAmigoComum" value="true" />
          </c:if>
        </sec:authorize>
        
        <c:set value="${friendship.nomeAmigo}" var="name" />
        <c:set value="${friendship.idAmigoFacebook}" var="itemId" />
        <c:if test="${friendship.tripfans}">
            <c:set var="mostrarLinkAmigo" value="true" />
            <c:url value="/perfil/${friendship.amigo.urlPath}" var="itemUrl" />
            <c:set value="${friendship.amigo}" var="usuarioCard"/>
            <c:set value="${usuarioCard.urlFotoPerfil}" var="photoUrl" />
        </c:if>
        <c:if test="${friendship.somenteFacebook}">
            <c:url var="showFacebookMark" value="true" />
            <c:set var="idFacebook" value="${friendship.idAmigoFacebook}" />
            <c:url value="/perfil/fb/${friendship.idAmigoFacebook}" var="itemUrl" />
            <c:url value="http://graph.facebook.com/${friendship.idAmigoFacebook}/picture" var="photoUrl" />
        </c:if>
    </c:when>
    <c:when test="${not empty local}">
        <c:url value="#" var="itemUrl" />
        <c:if test="${showLink}">
            <c:url value="/locais/${local.urlPath}" var="itemUrl" />
        </c:if>
        <c:choose>
          <c:when test="${not empty photoSize}">
            <c:choose>
              <c:when test="${photoSize eq 'small'}">
                <c:set value="${local.urlFotoSmall}" var="photoUrl" />
              </c:when>
              <c:when test="${photoSize eq 'big'}">
                <c:set value="${local.urlFotoBig}" var="photoUrl" />
              </c:when>
              <c:otherwise>
                <c:set value="${local.urlFotoAlbum}" var="photoUrl" />
              </c:otherwise>
            </c:choose>
          </c:when>
          <c:otherwise>
            <c:set value="${local.urlFotoAlbum}" var="photoUrl" />
          </c:otherwise>
        </c:choose>
        <c:if test="${local.fotoPadraoAnonima}">
            <c:set value="${local.urlFotoAlbum}" var="photoUrl" />
        </c:if>
        <c:if test="${empty name}">
          <c:choose>
            <c:when test="${local.tipoCidade or local.tipoLocalInteresseTuristico}">
                <c:set value="${local.nomeComSiglaEstadoNomePais}" var="name" />
            </c:when>
            <c:otherwise>
                <c:set value="${local.nomeCompleto}" var="name" />
            </c:otherwise>
          </c:choose>
        </c:if>
    </c:when>
    <c:when test="${not empty trip}">
      <%--c:choose>
        <c:when test="${trip.possuiDiario and trip.diario.publicado}">
          <c:url value="/viagem/diario/${trip.diario.id}" var="itemUrl" />
        </c:when>
        <c:when test="${trip.possuiDiario and not trip.diario.publicado}">
          <c:url value="/viagem/editarDiario/${trip.diario.id}" var="itemUrl" />
        </c:when>
        <c:otherwise>
          <c:url value="/viagem/editar/${trip.id}" var="itemUrl" />
        </c:otherwise>
      </c:choose--%>
      <c:url value="/viagem/${trip.id}" var="itemUrl" />
      
      <%--c:choose>
        <c:when test="${not empty trip.fotoIlustrativa}">
          <c:set var="photoUrl" value="${trip.fotoIlustrativa.urlAlbum}" />
        </c:when>
        <c:otherwise>
          <c:set var="photoUrl" value="${pageContext.request.contextPath}/resources/images/travel.jpg" />
        </c:otherwise>
      </c:choose--%>
      <c:set var="photoUrl" value="${pageContext.request.contextPath}/resources/images/travel.jpg" />
      
      <c:set value="${trip.nome}" var="name" />
    </c:when>
    <c:otherwise>
        <c:url value="${itemUrl}" var="itemUrl" />
    </c:otherwise>
</c:choose>

<c:choose>

    <c:when test="${not empty local or type == 'local'}">
    
      <c:choose>
      
        <c:when test="${newCard}">
            <div class="media card ${cardClass} ${selectable ? 'selectable' : ''} ${selectableUnique ? 'selectableUnique' : ''} ${selected ? 'selected' : ''}" style="${cardStyle}" 
                 data-local-id="${local.id}" data-local-url="${local.urlPath}" data-sugestao-local-id="${not empty sugestaoLocal ? sugestaoLocal.id : ''}">
              <div class="media-left">
                <a href="#">
	                <div class="origem-foto">
	                  <c:choose>
	                    <c:when test="${local.usarFotoPanoramio}">
	                      <img src="<c:url value="/resources/images/logos/panoramio-o.png" />" width="20" height="20" title="Origem da foto: Panoramio">
	                    </c:when>
	                    <c:when test="${not local.fotoPadraoAnonima}">
	                      <c:if test="${local.fotoPadrao.tipoArmazenamentoLocal}">
	                        <img src="<c:url value="/resources/images/logos/tripfans-small.png" />" width="20" height="20" title="Origem da foto: TripFans">
	                      </c:if>
	                      <c:if test="${local.fotoPadrao.tipoArmazenamentoExterno}">
	                        <c:if test="${not local.fotoPadrao.foto500px}">
	                          <!-- Simbolo do 500px -->
	                        </c:if>
	                      </c:if>
	                    </c:when>
	                    <c:otherwise>
	                    </c:otherwise>
	                  </c:choose>
	                </div>
	                <div id="card-photo-${id}" class="">
	                    <%-- c:if test="${local.fotoPadrao.foto500px}">
	                        <p class="foto-externa-info " style="opacity: .9;">
	                          <span class="pull-right" style="margin-right: 8px;">
	                            Por <a class="foto-externa-autor" target="_blank" href="${local.fotoPadrao.urlAutorImagem}">${local.fotoPadrao.idUsuarioFoto500px}</a>
	                            em <a class="foto-externa-fonte" target="_blank" href="${local.fotoPadrao.urlFonteImagem}">${local.fotoPadrao.fonteImagem}</a>
	                          </span>
	                        </p>
	                    </c:if--%>
	                    <img src="${photoUrl}" class="img-local-atividade" style="width: 64px; height: 64px; ${imgStyle}" />
	                </div>
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading">
                    <a href="${itemUrl}" class="card-title" style="${titleStyle}">
                        ${name}
                    </a>
                    <c:if test="${selectable and empty sugestaoLocal}">
	                    <c:if test="${not useButtonsRecomendacao}">
	                    	<span class="icon-selectable glyphicon glyphicon-ok"></span>
	                    </c:if>
	                    <c:if test="${useButtonsRecomendacao}">
	                    	<div class="btn-group pull-right" data-toggle="buttons">
								  <label class="btn btn-default btn-xs btn-recomendo ${buttonRecomendoOn ? 'active btn-warning' : ''}">
								    <input type="checkbox" autocomplete="off">
								    <span class="glyphicon glyphicon-ok"></span>
								    Recomendo
								  </label>
								  <label class="btn btn-default btn-xs btn-imperdivel ${buttonImperdivelOn ? 'active btn-danger' : ''}" data-local-id="${local.id}">
								    <input type="checkbox" autocomplete="off">
								    <span class="glyphicon glyphicon-star"></span>
								    Imperdível
								  </label>
							</div>
	                    </c:if>
	                </c:if>
                </h4>
                <div class="card-content">
                    <c:if test="${not empty info}">
                      <div class="card-info" style="${orientation == 'horizontal' ? 'padding-left: 0px; padding-top: 0;' : ''}">
                        <jsp:invoke fragment="info" />
                      </div>
                    </c:if>
                    <c:if test="${showAdditionalInfo}">
                      <div class="card-additional-info">
                        <jsp:invoke fragment="additionalInfo" />
                      </div>
                    </c:if>
                </div>
              </div>
            </div>
        </c:when>
        <c:otherwise>
    
          <div class="col-sm-${colSize}">
            <div class="card borda-arredondada ${selectable ? 'selectable' : ''} ${selectableUnique ? 'selectableUnique' : ''}" style="${cardStyle}" data-local-id="${local.id}" data-local-url="${local.urlPath}">
                <c:if test="${orientation == 'vertical'}">
                  <div class="card-header">
                    <h3>
                        <a href="${itemUrl}" class="card-title" style="${titleStyle}">
                            ${name}
                        </a>
                    </h3>
                  </div>
                </c:if>
                <div class="card-content">
                  <c:if test="${orientation == 'horizontal'}">
                    <div class="row">
                      <div class="col-xs-${photoColSize}" style="padding-right: 8px;">
                  </c:if>
                        <div class="origem-foto">
                          <c:choose>
                            <c:when test="${local.usarFotoPanoramio}">
                              <img src="<c:url value="/resources/images/logos/panoramio-o.png" />" width="20" height="20" title="Origem da foto: Panoramio">
                            </c:when>
                            <c:when test="${not local.fotoPadraoAnonima}">
                              <c:if test="${local.fotoPadrao.tipoArmazenamentoLocal}">
                                <img src="<c:url value="/resources/images/logos/tripfans-small.png" />" width="20" height="20" title="Origem da foto: TripFans">
                              </c:if>
                              <c:if test="${local.fotoPadrao.tipoArmazenamentoExterno}">
                                <c:if test="${not local.fotoPadrao.foto500px}">
                                  <!-- Simbolo do 500px -->
                                </c:if>
                              </c:if>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                          </c:choose>
                        </div>
                        <div id="card-photo-${id}" class="card-photo">
                            <c:if test="${local.fotoPadrao.foto500px}">
                                <p class="foto-externa-info " style="opacity: .9;">
                                  <span class="pull-right" style="margin-right: 8px;">
                                    Por <a class="foto-externa-autor" target="_blank" href="${local.fotoPadrao.urlAutorImagem}">${local.fotoPadrao.idUsuarioFoto500px}</a>
                                    em <a class="foto-externa-fonte" target="_blank" href="${local.fotoPadrao.urlFonteImagem}">${local.fotoPadrao.fonteImagem}</a>
                                  </span>
                                </p>
                            </c:if>
                            <a href="${itemUrl}">
                                <div class="card-photo-img ${local.fotoPadraoAnonima ? 'no-photo' : ''}" style="background-image: url('${photoUrl}'); height: ${not empty photoHeight ? photoHeight : 300 }px;">
                                </div>
                            </a>
                        </div>
                  <c:if test="${orientation == 'horizontal'}">
                      </div>
                      <div class="col-xs-${infoColSize}" style="padding-left: 0px;">
                          <h3>
                            <a href="${itemUrl}" class="card-title" style="${titleStyle}">
                                ${name}
                            </a>
                          </h3>
                  </c:if>
                        <c:if test="${not empty info}">
                          <div class="card-info" style="${orientation == 'horizontal' ? 'padding-left: 0px;' : ''}">
                            <jsp:invoke fragment="info" />
                          </div>
                        </c:if>
                        <c:if test="${showAdditionalInfo}">
                          <div class="card-additional-info">
                            <jsp:invoke fragment="additionalInfo" />
                          </div>
                        </c:if>
                  <c:if test="${orientation == 'horizontal'}">
                      </div>
                    </div>
                  </c:if>
                </div>
                <c:if test="${showActions}">
                  <div class="card-footer borda-arredondada">
                    <div style="margin-left: 0px; width: 100%;">
                        <div style="border-top: 1px solid #eaeaea; height: 1px; width: 100%;">
                            <jsp:invoke fragment="actions" />
                        </div>
                    </div>
                  </div>
                </c:if>
            </div>
            
            <c:if test="${local.usarFotoPanoramio and not empty local.coordenadaGeografica.latitude}">
                <c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
                <c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
                <c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
                <c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />
                
                <c:if test="${not unsuportedIEVersion}">
                  <script type="text/javascript">
                    var requestOptions = {
                        <c:if test="${not empty local.idFotoPanoramio}">
                        'ids': [{'photoId': ${local.idFotoPanoramio}, 'userId': ${local.idUsuarioFotoPanoramio}}],
                        </c:if>
                        <c:if test="${empty local.idFotoPanoramio}">
                        //'tag': 'urban',
                        'set' : 'public',
                        'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}},
                        </c:if>
                    };
                    var request = new panoramio.PhotoRequest(requestOptions);
                    var widgetOptions = {
                        'width': '100%',
                        'height': ${photoHeight},
                        'bgcolor' : 'white',
                        'croppedPhotos' : panoramio.Cropping.TO_FILL,
                        'attributionStyle': panoramio.tos.Style.HIDDEN,
                        'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
                    };
                    
                    var photoWidget_${id} = new panoramio.PhotoWidget('card-photo-${id}', request, widgetOptions);
                    
                    //photo_ex_widget_${id}.enablePreviousArrow(false);
                    //photo_ex_widget_${id}.enableNextArrow(false);
    
                    function photoChanged(event) {
                        var $div = $('#${id}')
                        if (event.target.getPhoto() == null) {
                            var $divFoto = $div.find('.card-photo');
                            // esconder widget
                            $div.find('.panoramio-wapi-photo').hide();
                            $div.find('.origem-foto').remove();
                            // foto padrao
                            $divFoto.append('<div align="center" style="width: 100%; display: inline-table; "><a href="${itemUrl}">' +
                                    '<img src="${photoUrl}" class="local-card-img borda-arredondada ${imgClass}" style="${imgStyle}; float: center; min-height: 100px; max-width: 100px; margin-top: 30px;">' + 
                                    '</a></div>');
                        } else {
                            $div.find('.panoramio-wapi-photo').css('max-width', '100%');
                        }
                    }
                    
                    function photoClicked(event) {
                        document.location.href = "${itemUrl}"
                        return false;
                    }
                    panoramio.events.listen(photoWidget_${id}, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);
                    panoramio.events.listen(photoWidget_${id}, panoramio.events.EventType.PHOTO_CHANGED, photoChanged);
                    
                    photoWidget_${id}.setPosition(0);
                    
                    $('#${id}').find('.panoramio-wapi-crop-div').css('width', '').css('height', '');
                  </script>
            
                  <style>
                    #card-photo-${id} .panoramio-wapi-images {
                        background-color: transparent;
                    }
                    
                    .panoramio-wapi-photo .panoramio-wapi-loaded-img {
                        position: relative;
                    }
                    
                    .panoramio-wapi-photo .panoramio-wapi-title {
                        width: 100%;
                    }
                  </style>
                </c:if>
            </c:if>
          </div>
        </c:otherwise>
      </c:choose>
    </c:when>
    
    <c:when test="${not empty trip}">
      
        <div class="card borda-arredondada">
            <div class="card-header">
                <h3>
                    Rio de Janeiro
                </h3>
            </div>
            <div class="card-content" >
                <a href="teste">
                    <div class="card-photo" style="background: url('http://www.tripfans.com.br/storeFotos/locais/21868/FotoPerfil_album.jpg'); background-size: cover; height: 300px; background-repeat: no-repeat !important; background-position: center center !important;">
                    </div>
                </a>
                <div class="card-info">
                    
                </div>
                <div class="card-additional-info">
                </div>
            </div>
            <div class="card-footer borda-arredondada">
                <div style="margin-left: 0px; width: 100%;">
                    <div style="border-top: 1px solid #eaeaea; height: 1px; width: 100%;"></div>
                </div>
            </div>
        </div>
        
     </div>
    </c:when>
    
    <c:otherwise>
    
        <div class="col-md-${spanSize} usercard" style="padding: 4px;" data-id="${itemId}" data-fb-id="${idFacebook}" data-name="${name}">

          <div id="${id}" class="row ${cardClass}" style="${cardStyle};" data-usuario-id="${user.id}" data-usuario-ativo-tripfans="${user.ativo}" data-fb-id="${idFacebook}" data-name="${name}" >
        
            <div class="col-md-3" style="padding-left: 0px;">
	            <c:if test="${mostrarLinkAmigo}">
	              <a href="${itemUrl}" target="_blank">
	            </c:if>
	                <fan:userAvatar user="${usuarioCard}" enableLink="false" showFacebookMark="${showFacebookMark}" idUsuarioFacebook="${idFacebook}" 
	                                marginLeft="0" displayName="false" displayNameTooltip="false" width="${width}" height="${height}" useLazyLoad="${useLazyLoadImg}"
	                                showFrame="false" showShadow="false" />
	            <c:if test="${mostrarLinkAmigo}">
	              </a>
	            </c:if>
            </div>

            <div class="col-md-9" style="padding-left: 0px;">
                <div>
	            	<c:if test="${mostrarLinkAmigo}">
					  <a href="${itemUrl}" target="_blank">
					</c:if>
					
					  <span class="" style="${nameStyle}; color: #0088cc;">
					      ${name}
					  </span>
					  
					<c:if test="${mostrarLinkAmigo}">
					  </a>
					</c:if>
				</div>
				<div>
					<jsp:invoke fragment="info" />
				</div>
				<div>
                	<jsp:invoke fragment="additionalInfo" />
				</div>
                <c:if test="${selectable}">
                    <td align="right" valign="bottom">
                        <input type="checkbox" class="card-check" data-user-id="" data-user-id-facebook="${idFacebook}" />
                    </td>
                </c:if>
            </div>
            
          </div>
          
        </div>

	</c:otherwise>
</c:choose>