<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ tag body-content="empty" %> 
<%@ attribute name="defaultValue" required="false" type="br.com.fanaticosporviagens.model.entity.TipoVisibilidade" description="Valor padrao para ser exibido. Se nao for informado, sera usado o PUBLICO." %> 
<%@ attribute name="direction" required="false" type="java.lang.String" description="Direcao do dropdown: down (default) ou up" %>
<%@ attribute name="id" required="true" type="java.lang.String" description="Identificador do elemento" %> 
<%@ attribute name="items" required="false" type="br.com.fanaticosporviagens.model.entity.TipoVisibilidade[]" description="Lista de valores a ser exibido. Se nao for informado, todos os valores serao exibidos" %>
<%@ attribute name="name" required="true" type="java.lang.String" description="Nome do campo (para ser enviado no post de um form, por exemplo)" %>
<%@ attribute name="tooltip" required="false" type="java.lang.String" description="Texto explicativo do campo" %>
<%@ attribute name="cssButtonClass" required="false" type="java.lang.String" description="Classe do botao" %>
<%@ attribute name="dropDownStyle" required="false" type="java.lang.String" description="CSS style do dropdown menu" %>
<%@ attribute name="dropDownItemStyle" required="false" type="java.lang.String" description="CSS style dos itens de menu do dropdown" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<% TipoVisibilidade tipoDefault  = TipoVisibilidade.PUBLICO;  %>
	<c:if test="${not empty defaultValue}">
		<% tipoDefault = defaultValue;  %>
	 </c:if>
	 
	 <% TipoVisibilidade[] lista = TipoVisibilidade.valuesOrdenadosPorCodigo(); %>
	 <c:if test="${not empty items}">
		<% lista  = items;  %>
	 </c:if>
	 
	 <c:if test="${direction == 'up'}">
	 	<c:set var="dropDownUpStyle" value="top: -59px;" />
	 </c:if>
	 
	 <c:set var="cssClass" value="btn" />
	 <c:if test="${not empty cssButtonClass}">
	 	<c:set var="cssClass" value="${cssButtonClass}" />
	 </c:if>
	
	<input id="privValue_${id}" type="hidden" name="${name}" value="<%= tipoDefault %>" />
	<div class="btn-group" style="font-weight: bold;"
		<c:if test="${not empty tooltip}">
			title="${tooltip}"
		</c:if> 
	>
		  <a class=" ${cssClass}" data-toggle="dropdown" href="#">
		  <i id="currentPrivIcon" class="<%= tipoDefault.getIcone() %>"></i>
		  <span id="currentPrivText"><%= tipoDefault.getDescricao() %></span>
		    <span class="caret"></span>
		  </a>
		  <ul class="dropdown-menu" id="${id}" style="${dropDownUpStyle} ${dropDownStyle}">
		  	<c:forEach items="<%= lista %>" var="tipo" varStatus="contador" >
		  		<li style="${dropDownItemStyle}"><a style="font-weight: bold; font-size: 11px;" href="#" value="${tipo}">
		  			<i class="${tipo.icone}" style="padding-right: 10px;"></i>${tipo.descricao}
		  			</a>
		  		</li>
		  	</c:forEach>
		  </ul>
	</div>
    
<script type="text/javascript">
<c:if test="${direction == 'up'}">
jQuery(document).ready(function() {
	$('div.btn-group').tooltip({placement: 'bottom'});
});
</c:if>

$('#${id} a').bind('click',function(event){
	event.preventDefault();
	var form = $(this).parents('form')[0];
	if(form === null || form === undefined) {
		throw "O elemento deve estar associado a um formulário!";
	}	
	var currIconClass = $(this).find('i').attr('class');
	var value = $(this).attr('value');
	$('#currentPrivIcon').attr('class', currIconClass);
	$('#currentPrivText').text($(this).text());
	if($('#privValue_${id}').length !== 0) {
		$('#privValue_${id}').attr('value', value);
	} else {
		$('<input>').attr({
		    type: 'hidden',
		    id: 'privValue_${id}',
		    name: '${name}',
		    value: value
		}).appendTo(form);
	}
});
</script>