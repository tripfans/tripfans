<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="nomeCompleto" required="false" type="java.lang.Boolean"  %>
<%@ attribute name="destaqueNomeLocal" required="false" type="java.lang.Boolean"  %>  

<a href="<c:url value="${local.url}" />">

<c:if test="${not empty destaqueNomeLocal}">
	<c:set var="styleNome" value="font-size: 15px; font-weight: bold;" />
	<c:set var="styleLocalizacao" value="font-size: 12px;" />
</c:if>
<span style="${styleNome}">${local.nome}</span>
<c:if test="${not empty nomeCompleto && nomeCompleto }"><span style="${styleLocalizacao}">, ${local.localizacaoCompleta}</span></c:if>
</a>