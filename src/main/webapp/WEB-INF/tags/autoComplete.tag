<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.infra.component.SearchData"%>
<%@ tag body-content="empty" %>
<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="url" required="true" type="java.lang.String" %>
<%@ attribute name="hiddenName" required="true" type="java.lang.String" %>
<%@ attribute name="name" required="false" type="java.lang.String" %>
<%@ attribute name="valueField" required="false" type="java.lang.String" %>
<%@ attribute name="labelField" required="false" type="java.lang.String" %>
<%@ attribute name="value" required="false" type="java.lang.String" %>
<%@ attribute name="hiddenValue" required="false" type="java.lang.String" %>
<%@ attribute name="minLength" required="false" type="java.lang.Integer" %>
<%@ attribute name="cssClass" required="false" type="java.lang.String" %>
<%@ attribute name="cssStyle" required="false" type="java.lang.String" %>
<%@ attribute name="showActions" required="false" type="java.lang.Boolean" %>
<%@ attribute name="blockOnSelect" required="false" type="java.lang.Boolean" %>
<%@ attribute name="onSelect" required="false" type="java.lang.String" %>
<%@ attribute name="onRemove" required="false" type="java.lang.String" %>
<%@ attribute name="extraParams" required="false" type="java.lang.String" %>
<%@ attribute name="jsonFields" required="false" type="java.lang.String" %>
<%@ attribute name="limit" required="false" type="java.lang.Integer" %>
<%@ attribute name="start" required="false" type="java.lang.Integer" %>
<%@ attribute name="placeholder" required="false" type="java.lang.String" %>
<%@ attribute name="tooltip" required="false" type="java.lang.String" %>
<%@ attribute name="group" required="false" type="java.lang.String" %>
<%@ attribute name="groupName" required="false" type="java.lang.String" %>
<%@ attribute name="highlighter" required="false" type="java.lang.String" %>
<%@ attribute name="moreResults" required="false" type="java.lang.Boolean" %>
<%@ attribute name="moreResultsUrl" required="false" type="java.lang.String" %>
<%@ attribute name="showNotFound" required="false" type="java.lang.Boolean" %>
<%@ attribute name="notFoundMsg" required="false" type="java.lang.String" %>
<%@ attribute name="notFoundUrl" required="false" type="java.lang.String" %>
<%@ attribute name="position" required="false" type="java.lang.String" %>
<%@ attribute name="showImage" required="false" type="java.lang.Boolean" %>
<%@ attribute name="imageName" required="false" type="java.lang.String" %>
<%@ attribute name="imageClass" required="false" type="java.lang.String" %>
<%@ attribute name="imageStyle" required="false" type="java.lang.String" %>
<%@ attribute name="lineStyle" required="false" type="java.lang.String" %>
<%@ attribute name="addOnLarge" required="false" type="java.lang.Boolean" %>
<%@ attribute name="disabled" required="false" type="java.lang.Boolean" %>
<%@ attribute name="useDocumentReady" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showAddress" required="false" type="java.lang.Boolean" %>
<%@ attribute name="responsive" required="false" type="java.lang.Boolean" %>
<%@ attribute name="index" required="false" type="java.lang.Integer" %>
<%@ attribute name="addressName" required="false" type="java.lang.String" %>
<%@ attribute name="rel" required="false" type="java.lang.String" %>
<%@ attribute name="resultListClassWidth" required="false" type="java.lang.String" %>
<%@ attribute name="resultListStyle" required="false" type="java.lang.String" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${addOnLarge}">
    <c:set var="addOnStyle" value="padding-top: 9px;"></c:set>
</c:if>

<c:if test="${empty useDocumentReady}">
    <c:set var="useDocumentReady" value="true" />
</c:if>

<c:if test="${showAddress and empty addressName}">
	<c:set var="addressName" value="endereco" />
</c:if>
    <input type="hidden" id="${id}_hidden" name="${hiddenName}" value="${hiddenValue}" />

<c:if test="${showActions}">
<div class="${responsive ? 'input-group' : 'input-append' } ">
</c:if>
    <input type="text" id="${id}" name="${name}" value="${value}" class="${responsive ? 'form-control' : ''} autoCompleteFanaticos ${cssClass}" ${disabled == true ? 'disabled="true"' : ''} data-hidden-id="${id}_hidden" 
           placeholder="${placeholder}" style="${cssStyle}" <c:if test="${tooltip != null}">title="${tooltip}"</c:if> rel="${rel}" />
    <c:if test="${showActions and not disabled}">
      <c:if test="${not responsive}">
	    <span id="autoCompleteAddOn_${id}" class="autoCompleteAddOn">
	  </c:if>
	      <c:choose>
	        <c:when test="${value == null || value == ''}">
	          <c:if test="${not responsive}">
	            <span id="addOnSearch_${id}" class="add-on" style="${addOnStyle}"><i class="icon-search"></i></span>
	          </c:if>
	          <c:if test="${responsive}">
	            <div id="addOnSearch_${id}" class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
	            <div id="addOnRemove_${id}" class="input-group-addon" style="${addOnStyle}; display:none;" title="Limpar"><span class="glyphicon glyphicon-remove"></span></div>
	          </c:if>
	        </c:when>
	        <c:otherwise>
	          <c:if test="${not responsive}">
	            <span id="addOnRemove_${id}" class="add-on" style="${addOnStyle}" title="Limpar"><i class="icon-remove"></i></span>
	          </c:if>
	          <c:if test="${responsive}">
	            <div id="addOnSearch_${id}" class="input-group-addon" style="display:none;"><span class="glyphicon glyphicon-search"></span></div>
	            <div id="addOnRemove_${id}" class="input-group-addon" style="${addOnStyle};" title="Limpar"><span class="glyphicon glyphicon-remove"></span></div>
	          </c:if>
	        </c:otherwise>
	      </c:choose>
	  <c:if test="${responsive}">
	    </span>
	  </c:if>
    </c:if>
<c:if test="${showActions}">    
</div>
</c:if>
    
    <script>
    
    var campo_${id};
    
    <c:if test="${useDocumentReady}">
      $(document).ready(function () {
    </c:if>
        campo_${id} = $('#${id}').autocompleteFanaticos({
            id: '${id}',
            source: '<c:url value="${url}"/>',
            //hiddenName : '${hiddenName}',
            showActions : ${showActions == null ? 'true' : showActions},
            addOnStyle : '${addOnStyle}',
            blockOnSelect : ${blockOnSelect == null ? 'true' : blockOnSelect},
            <c:if test="${onSelect != null}">
            onSelect : '${onSelect}',
            </c:if>
            <c:if test="${onRemove != null}">
            onRemove : '${onRemove}',
            </c:if>
            <c:if test="${valueField != null}">
            valueField: '${valueField}',
            </c:if>
            <c:if test="${labelField != null}">
            labelField: '${labelField}',
            </c:if>
            <c:if test="${extraParams != null}">
            extraParams: ${extraParams},
            </c:if>
            <c:if test="${jsonFields != null}">
            jsonFields: "${jsonFields}",
            </c:if>
            start : ${start == null ? 0 : start},
            limit : ${limit == null ? 10 : limit},
            <c:if test="${minLength != null}">
            minLength: ${minLength},
            </c:if>
            <c:if test="${group != null}">
            group: ${group},
            </c:if>
            <c:if test="${groupName != null}">
            groupName: '${groupName}',
            </c:if>
            <c:if test="${highlighter != null}">
            highlighter: ${highlighter},
            </c:if>
            <c:if test="${moreResults}">
            moreResults: true,
            <c:if test="${moreResultsUrl != null}">
            moreResultsUrl: '${moreResultsUrl}',
            </c:if>
            </c:if>
            <c:if test="${showNotFound}">
            showNotFound: true,
            <c:if test="${not empty notFoundMsg}">
            notFoundMsg: '${notFoundMsg}',
            </c:if>
            notFoundUrl: '${not empty notFoundUrl ? notFoundUrl : '/locais/sugerirLocal'}',
            </c:if>
            <c:if test="${position != null}">
            position: {${position}},
            </c:if>
            <c:if test="${showImage}">
            showImage: ${showImage},
            <c:if test="${imageName != null}">
            imageName: '${imageName}',
            </c:if>
            <c:if test="${imageClass != null}">
            imageClass: '${imageClass}',
            </c:if>
            <c:if test="${imageStyle != null}">
            imageStyle: '${imageStyle}',
            </c:if>
            </c:if>
            <c:if test="${lineStyle != null}">
            lineStyle: '${lineStyle}',
            </c:if>
            <c:if test="${index != null}">
            zIndex: '${index}',
            </c:if>
            <c:if test="${responsive != null}">
            responsive: ${responsive}
            </c:if>
            <c:if test="${showAddress}">
            showAddress: ${showAddress},
            addressName: '${addressName}',
            </c:if>
            <c:if test="${resultListClassWidth}">
            resultListClassWidth: '${resultListClassWidth}',
            </c:if>
            <c:if test="${resultListStyle}">
            resultListStyle: '${resultListStyle}',
            </c:if>
        });
        
        $('#${id}').keyup(function() {
        	if(!this.value) {
        		$('#${id}_hidden').val("");
        	}
        });
        
      <c:if test="${useDocumentReady}">
        });
      </c:if>
    </script>

