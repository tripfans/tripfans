<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.model.entity.Viagem"%>
<%@ tag body-content="empty" %>
<%@ attribute name="idPorData" required="true" type="java.lang.String" %>
<%@ attribute name="idPorDia" required="true" type="java.lang.String" %>
<%@ attribute name="pathPorData" required="true" type="java.lang.String" %>
<%@ attribute name="pathPorDia" required="true" type="java.lang.String" %>
<%@ attribute name="labelPorData" required="true" type="java.lang.String" %>
<%@ attribute name="labelPorDia" required="true" type="java.lang.String" %>
<%@ attribute name="tipoPeriodo" required="true" type="java.lang.Integer" %>
<%@ attribute name="valuePorData" required="false" type="java.lang.Object" %>
<%@ attribute name="valuePorDia" required="false" type="java.lang.Object" %>
<%@ attribute name="quantidadeDias" required="true" type="java.lang.Integer" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${tipoPeriodo == 1}">
    <label for="${idPorData}">
        ${labelPorData}
    </label>
</c:if>
<c:if test="${tipoPeriodo == 2}">
    <label for="${idPorDia}">
        ${labelPorDia} 
    </label>
</c:if>
<form:errors cssClass="error" path="${name}" element="label"/>
<div class="controls">
    <c:if test="${tipoPeriodo == 1}">
        <form:input id="${idPorData}" alt="date" path="${pathPorData}" class="span2" />
    </c:if>
    <c:if test="${tipoPeriodo == 2}">
        
        <c:if test="${tipoPeriodo == 1}">
            <c:set var="value" value="${valuePorData}" />
        </c:if>
        <c:if test="${tipoPeriodo == 2}">
            <c:set var="value" value="${valuePorDia}" />
        </c:if>
        
        <select id="${idPorDia}" name="${pathPorDia}" class="span1">
            <c:forEach begin="1" end="${quantidadeDias}" var="index">
                <c:if test="${value == null}">
                    <option value="${index}" ${param.inicio == index ? 'selected="selected"' : ''}>
                </c:if>
                <c:if test="${value != null}">
                    <option value="${index}" ${value == index ? 'selected="selected"' : ''}>
                </c:if>
                    ${index}º dia
                </option>
            </c:forEach>
        </select>
    </c:if>
</div>
