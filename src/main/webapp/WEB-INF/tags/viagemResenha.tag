<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2" %> 

<c:if test="${not empty viagem.resenha}">
<table class="table table-condensed">
  <tr class="info">
  	<th colspan="4">Resenha da Viagem</th>
  </tr>
  <tr>
  	<th>#</th>
  	<th>Item</th>
  	<th>Quantidade</th>
  	<th>Valor</th>
  </tr>
  <c:forEach var="resenha" items="${viagem.resenha}">
  <tr class="success">
  	<td><img alt="Resenha de ${resenha.descricao}" src="${pageContext.request.contextPath}/resources/images/${resenha.icone}" width="20" height="20" /></td>
  	<td>${resenha.descricao}</td>
  	<td>${resenha.quantidade}</td>
  	<td>${resenha.valor}</td>
  </tr>
  </c:forEach>
  <tr>
  	<th>&sum;</th>
  	<th>Total</th>
  	<th>${viagem.quantidadeTotalItens}</th>
  	<th>${viagem.valorTotal}</th>
  </tr>
</table>
</c:if>

