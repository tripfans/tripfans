<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ tag body-content="empty" %> 
<%@ attribute name="id" required="true" type="java.lang.String" %> 
<%@ attribute name="name" required="true" type="java.lang.String" %> 
<%@ attribute name="orientation" required="false" type="java.lang.String" description="Indica se o nome do usuario deve aparecer ao lado (horizontal) ou abaixo (padrao) da foto." %>
<%@ attribute name="displayName" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showFrame" required="false" type="java.lang.Boolean" %>
<%@ attribute name="imgSize" required="false" type="java.lang.String" description="Tamanho da imagem em classe CSS .span" %>
<%@ attribute name="type" required="false" type="java.lang.String" description="small, normal, large, square" %>
<%@ attribute name="width" required="false" type="java.lang.Integer" %> 
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="marginLeft" required="false" type="java.lang.Integer" %> 
<%@ attribute name="marginRight" required="false" type="java.lang.Integer" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${height == null}"><c:set var="height" value="50" /></c:if>
<c:if test="${width == null}"><c:set var="width" value="50" /></c:if>
<c:if test="${marginLeft == null}"><c:set var="marginLeft" value="5" /></c:if>
<c:if test="${marginRight == null}"><c:set var="marginRight" value="5" /></c:if>
<c:if test="${displayName == null}"><c:set var="displayName" value="true" /></c:if>
<c:if test="${showFrame == null}"><c:set var="showFrame" value="true" /></c:if>
<c:if test="${imgSize == null}"><c:set var="imgSize" value="span1" /></c:if>
<c:if test="${type == null}"><c:set var="type" value="square" /></c:if>

<c:url value="/perfil/fb/${id}" var="userUrl" />
<c:url value="http://graph.facebook.com/${id}/picture?type=${type}" var="urlFotoPerfil" />

<% String idUnico = RandomStringUtils.random(12, true, true); %>
	
	<a href="${userUrl}">
    <c:choose>
    	<c:when test="${orientation == 'horizontal' }">
    	<div>
    		<div style="float:left; margin-left:${marginLeft}px; margin-right: ${marginRight}px;">
           		<img id="<%= idUnico %>" class="${imgSize} thumbnail" src="${urlFotoPerfil}"   
           			<c:if test="${displayDetails}">
    			        data-content="<%@include file="popover.jsp" %>"
    			        rel="popover" data-original-title="${name}"
    	    		</c:if>
           		/>
        	</div>
            <c:if test="${displayName}">
            	<div class="span2">
                	<span style="margin-top: 1px;"><strong>${name}</strong></span>
            	</div>
            </c:if>
    	</div>
    	</c:when>
    	<c:otherwise>
    	<div style="float:left; margin-left:${marginLeft}px; margin-right: ${marginRight}px;">
    		<div class="centerAligned">
    	        <div>
    	            <img id="<%= idUnico %>" class="${imgSize} thumbnail" src="${urlFotoPerfil}" 
        	            <c:if test="${displayDetails}">
        			        data-content="<%@include file="popover.jsp" %>"
        			        rel="popover" data-original-title="${name}"
        	    		</c:if>
    	            />
    	        </div>
                <c:if test="${displayName}">
        	        <div>
        	           <strong>${name}</strong>
        	        </div>
                </c:if>
	        </div>
	    </div>
    	</c:otherwise>
    </c:choose>
    </a>
    
<c:if test="${displayDetails}">
<script>
$('#<%= idUnico %>').mouseenter(function(){
	$(this).popover({
		offset: 80,
		html : true,
		delayIn : 1000,
		trigger: 'manual',
		template: '<div class="popover" onmouseover="popoverMouseover_<%= idUnico %>();"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'
	}).popover('show');
}).mouseleave( function() {
	 var el=$(this);
	 var timeout = setTimeout(function() { 
		el.popover('hide');
	 }, 800);
	 el.data('timeout', timeout);
});


function popoverMouseover_<%= idUnico %>() {
	$('.popover-inner').mouseenter(function() {
	 var el = $('#<%= idUnico %>');
	 var t = el.data('timeout');
	 clearTimeout(t); 
	} 
	).mouseleave(function() {$('.popover').hide();});
}
</script>
</c:if>