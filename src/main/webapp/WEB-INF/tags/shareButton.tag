<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="id" required="false" type="java.lang.String"  description="Id do botão" %>
<%@ attribute name="facebook" required="false" type="java.lang.Boolean" %>
<%@ attribute name="labelBotaoFacebook" required="false" type="java.lang.String" %>
<%@ attribute name="styleLabelBotaoFacebook" required="false" type="java.lang.String" %>
<%@ attribute name="esconderBotao" required="false" type="java.lang.Boolean" description="Informa se o botao aparecerá oculto" %>
<%@ attribute name="usarBotaoEspecial" required="false" type="java.lang.Boolean" %>
<%@ attribute name="usarComoCheckbox" required="false" type="java.lang.Boolean" description="Indica se o botao vai funcionar apenas como checkbox. Se for false, abre diretamente o compartilhamento" %>
<%@ attribute name="url" required="false" type="java.lang.String" %>
<%@ attribute name="pictureUrl" required="false" type="java.lang.String" %>
<%@ attribute name="titulo" required="false" type="java.lang.String" %>
<%@ attribute name="subTitulo" required="false" type="java.lang.String" %>
<%@ attribute name="texto" required="false" type="java.lang.String" %>
<%@ attribute name="iconStyle" required="false" type="java.lang.String" %>
<%@ attribute name="hint" required="false" type="java.lang.String" %>
<%@ attribute name="includeFacebookApi" required="false" type="java.lang.Boolean" %>

<c:if test="${tripFansEnviroment.enableSocialNetworkActions}">

<style>
a.btn-facebook-share i.facebook-mark-small {
    position: initial;
    display: inline-block;
    vertical-align: text-top;
}

a.btn-facebook-share:hover i.facebook-mark-small {
    opacity: 1;
    -moz-opacity: 1;
    filter: none;
    -webkit-filter: none; /* Chrome 19+ & Safari 6+ */
}

</style>

    <c:if test="${facebook == null}">
        <c:set var="facebook" value="true" />
    </c:if>
    <c:if test="${includeFacebookApi == null}">
        <c:set var="includeFacebookApi" value="true" />
    </c:if>
    <c:if test="${hint == null}">
        <c:set var="hint" value="Compartilhar no Facebook" />
    </c:if>
    <c:if test="${usarBotaoEspecial == null}">
        <c:set var="usarBotaoEspecial" value="true" />
    </c:if>
    
    <% 
       String idBotao = RandomStringUtils.random(12, true, true);
       if (id != null) {
           idBotao = id;
       }
       if (usarComoCheckbox == null) {
    	   usarComoCheckbox = false;
       }
    %>
    <c:if test="${facebook}">
      
        <a id="<%=idBotao%>" class="btn-facebook-share ${usarBotaoEspecial ? '' : 'btn btn-secondary'}" href="#" title="${hint}" style="${esconderBotao ? 'display : none;' : ''}" data-selected="false" data-function-name="postToFeed_<%=idBotao%>">
            <c:if test="${usarBotaoEspecial}">
                <i class="social-icon-facebook blackAndWhite transparencia-75 colorOnHover" style="${iconStyle}" > </i>
            </c:if>
            <c:if test="${not usarBotaoEspecial}">
                <c:if test="${not empty labelBotaoFacebook}">
                  <i class="facebook-mark-small blackAndWhite transparencia-75 colorOnHover" style="${iconStyle}"> </i>
                  ${labelBotaoFacebook}
                </c:if>
            </c:if>
        </a>
        
        <c:if test="${includeFacebookApi}">
            <div id="fb-root" style="display: none;"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>  
        </c:if>    
        
    </c:if>
    
    <script>
    
      $('#<%=idBotao%>').click(function (e) {
    	  e.preventDefault();
    	  <c:choose>
    	    <c:when test="${usarComoCheckbox}">
    	    marcar_<%=idBotao%>($(this));
            </c:when>
            <c:otherwise>
            postToFeed_<%=idBotao%>();
            </c:otherwise>
          </c:choose>
      });
    
      function marcar_<%=idBotao%>($botao) {
    	  $botao.find('i').toggleClass('selected');
          $botao.attr('data-selected', $botao.find('i').hasClass('selected'));
      }
      
      function postToFeed_<%=idBotao%>() {
        
    	var descricaoFormatada = '${texto}';
    	descricaoFormatada = descricaoFormatada.replace(/\"/g,'\\"');
   	    
    	  
        var obj = {
          method: 'feed',
          link: '${url}',
          display: 'popup',
          <c:if test="${not empty pictureUrl}">
          picture: '${pictureUrl}',
          </c:if>
          <c:if test="${not empty titulo}">
          name: "${titulo}",
          </c:if>
          <c:if test="${not empty texto}">
          description: descricaoFormatada,
          </c:if>
          <c:if test="${not empty subTitulo}">
          caption: "${subTitulo}"
          </c:if>
        };
    
        function callback(response) {
          if (response['post_id']) {
              $('#<%=idBotao%>').addClass('selected');
              $.gritter.add({
					title: 'Informação',
					text: 'Compartilhamento foi realizado com sucesso.',
					time: 4000,
					image: '<c:url value="/resources/images/info.png" />',
			  });
          } else {
        	  $.gritter.add({
					title: 'Erro',
					text: 'Erro ao compartilhar no Facebook.',
					time: 4000,
					image: '<c:url value="/resources/images/error.png" />',
			  });
          }
        }
    
        FB.ui(obj, callback);
      }  
    
      window.fbAsyncInit = function() {
        FB.init({appId: '${tripFansEnviroment.facebookClientId}', status: true, cookie: true});
        
        /*twttr.events.bind('tweet', function(event) {
            $('#tw-share').find('a').hide();
            $('#tw-share').find('b').show();
        });*/
      }
      
    </script>
    
</c:if>    
        