<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="buttonId" required="true" type="java.lang.String" %>
<%@ attribute name="modalId" required="true" type="java.lang.String" %>
<%@ attribute name="uploadUrl" required="true" type="java.lang.String" %>
<%@ attribute name="reloadUrl" required="false" type="java.lang.String" %>
<%@ attribute name="albumId" required="false" type="java.lang.Long" %>
<%@ attribute name="idAtividade" required="false" type="java.lang.Long" %>
<%@ attribute name="onlyUpload" required="false" type="java.lang.Boolean" %>

<%@ attribute name="callback" required="false" type="java.lang.String" description="Função javascript que será chamada ao confirmar as fotos" %>

<%-- <a id="${buttonId}" href="#${modalId}" class="btn">
 --%>
 
<c:choose>

  <c:when test="${onlyUpload}">
    <a id="${buttonId}" href="<c:url value="/fotos/upload?url=${uploadUrl}&idAlbum=${albumId}&idAtividade=${idAtividade}&cb=${callback}"/>" class="btn btn-select-photo">
      <i class="icon-plus"></i> Adicionar fotos
    </a>
  </c:when>
  <c:otherwise>
    <div class="btn-group">
        <button class="btn dropdown-toggle" data-toggle="dropdown">
            <i class="icon-plus"></i> Adicionar fotos <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
          <li>
            <a id="${buttonId}" href="<c:url value="/fotos/upload?url=${uploadUrl}&idAlbum=${albumId}&idAtividade=${idAtividade}&cb=${callback}"/>" class="btn-select-photo">
              <i class="icon-upload"></i> Enviar fotos
            </a>
          </li>
          <li>
            <a href="<c:url value="/fotos/${usuario.urlPath}/albuns/selecionar?atividade=${idAtividade}" />" class="btn-select-photo">
              <i class=icon-book></i> Escolher de Album
            </a>
          </li>
          <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
            <li>
              <a href="<c:url value="/fotos/albuns/facebook/selecionar?atividade=${idAtividade}"/>" class="btn-select-photo">
                <i class="icon-book"></i> Escolher de Album do Facebook
              </a>
            </li>
          </c:if>
        </ul>
    </div>
  </c:otherwise>
</c:choose>

<style>

.box-foto-div {
    position: relative;
}

.box-foto-div div.opcoes {
    display: none;
    position: absolute;
    left: 10px;
    top: 10px;
    overflow: hidden;
}

.box-foto-div div.opcoes:hover {
    display: block;
}

.box-foto-div:hover div.opcoes {
    display: block;
}
</style>

<script>
    jQuery(function() {
        
        $('.btn-select-photo').fancybox({
            hideOnContentClick: false,
            hideOnOverlayClick: false,
            width             : '85%',
            height            : '85%',
            centerOnScroll: true,
            onComplete   : function() {
                $.fancybox.center();
            },
            onClosed: function() {
            	$(document).off('click', '.btn-remover-foto');
            	<c:if test="${not empty callback}">
            	if (${callback}) {
            		${callback}()
            	}
            	</c:if>
            	<c:if test="${not empty reloadUrl}">
            		parent.location = '<c:url value="${reloadUrl}" />';
            	</c:if>
            }
        });
        
    });
</script>