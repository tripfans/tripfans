<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="acaoUsuario" required="true" type="br.com.fanaticosporviagens.model.entity.AcaoUsuario" %> 
<%@ attribute name="mostrarFotoAutor" required="false" type="java.lang.Boolean" %>
<%@ attribute name="mostrarIcone" required="false" type="java.lang.Boolean" %>
<%@ attribute name="mostrarData" required="false" type="java.lang.Boolean" %>
<%@ attribute name="mostrarLinkNomeUsuario" required="false" type="java.lang.Boolean" %>
<%@ attribute name="quebrarLinhaAposNome" required="false" type="java.lang.Boolean" %>
<%@ attribute name="usarFontePequena" required="false" type="java.lang.Boolean" %>
<%@ attribute name="usarSomentePrimeiroNome" required="false" type="java.lang.Boolean" %>

<c:set var="nomeAutor" value="${acaoUsuario.autor.displayName}" />
<c:if test="${usarSomentePrimeiroNome}">
  <c:set var="nomeAutor" value="${autorAtividade.primeiroNome}" />
</c:if>

<c:choose>
    <c:when test="${acaoUsuario.acaoSobreAvaliacao}">
      <c:url var="urlAtividade" value="/locais/${acaoUsuario.rastreioAvaliacao.localAvaliado.urlPath}?tab=avaliacoes&item=${acaoUsuario.rastreioAvaliacao.urlPath}" />
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreDica}">
      <c:url var="urlAtividade" value="/locais/${acaoUsuario.rastreioDica.localDica.urlPath}?tab=dicas&item=${acaoUsuario.rastreioDica.urlPath}" />
    </c:when>
    <c:when test="${acaoUsuario.acaoSobrePergunta}">
      <c:url var="urlAtividade" value="/perguntas/verPergunta/${acaoUsuario.rastreioPergunta.urlPath}" />
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreResposta}">
      <c:url var="urlAtividade" value="/perguntas/verPergunta/${acaoUsuario.rastreioResposta.pergunta.urlPath}/#respostas" />
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreVotoUtil}">
      <c:set var="votoUtil" value="${acaoUsuario.acaoPrincipal}" />
      <c:if test="${votoUtil.votoAvaliacao}">
        <c:url var="urlAtividade" value="/perfil/${votoUtil.objetoVotado.autor.urlPath}?tab=avaliacoes&destaque=${votoUtil.idObjetoVotado}" />
      </c:if>
      <c:if test="${votoUtil.votoDica}">
        <c:url var="urlAtividade" value="/perfil/${votoUtil.objetoVotado.autor.urlPath}?tab=dicas&destaque=${votoUtil.idObjetoVotado}" />
      </c:if>
      <c:if test="${votoUtil.votoRelatoViagem}">
        
      </c:if>
      <c:if test="${votoUtil.votoResposta}">
        <c:url var="urlAtividade" value="/perguntas/verPergunta/${votoUtil.resposta.pergunta.urlPath}/#respostas" />
      </c:if>
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreAlbum}">
      <c:url var="urlAtividade" value="/perfil/${acaoUsuario.acaoPrincipal.autor.urlPath}/albuns/${acaoUsuario.acaoPrincipal.urlPath}" />
    </c:when>
    <%--c:when test="${acaoUsuario.acaoSobreFoto}">
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreViagem}">
    </c:when--%>
    <c:when test="${acaoUsuario.acaoSobreComentario}">
      <c:choose>
        <c:when test="${acaoUsuario.acaoPrincipal.respostaComentario}">
          <c:set var="comentario" value="${acaoUsuario.acaoPrincipal.comentarioPai}" />
        </c:when>
        <c:otherwise>
          <c:set var="comentario" value="${acaoUsuario.acaoPrincipal}" />
        </c:otherwise>
      </c:choose>
    
      <c:if test="${comentario.comentarioAlbum}">
        <c:url var="urlAtividade" value="/perfil/${comentario.album.autor.urlPath}/albuns/${comentario.album.urlPath}#comentarios" />
      </c:if>
      <c:if test="${comentario.comentarioAvaliacao}">
        <c:url var="urlAtividade" value="/perfil/${comentario.avaliacao.autor.urlPath}?tab=avaliacoes&destaque=${comentario.idObjetoComentado}" />
      </c:if>
      <c:if test="${comentario.comentarioDica}">
        <c:url var="urlAtividade" value="/perfil/${comentario.dica.autor.urlPath}?tab=dicas&destaque=${comentario.idObjetoComentado}" />
      </c:if>
      <c:if test="${comentario.comentarioDiarioViagem}">
        <c:url var="urlAtividade" value="/viagem/diario/${comentario.idObjetoComentado}" />
      </c:if>
      <c:if test="${comentario.comentarioFoto}">
        <c:url var="urlAtividade" value="/fotos/${comentario.foto.urlPath}" />
      </c:if>
      <c:if test="${comentario.comentarioPerfil}">
        <c:url var="urlAtividade" value="/perfil/${comentario.usuario.urlPath}?tab=principal#${comentario.urlPath}" />
      </c:if>
      <c:if test="${comentario.comentarioRelatoViagem}">
        <c:url var="urlAtividade" value="/perfil/${comentario.urlPath}" />
      </c:if>
      <c:if test="${comentario.comentarioViagem}">
        <c:url var="urlAtividade" value="/perfil/${comentario.urlPath}" />
      </c:if>
    </c:when>
    <c:when test="${acaoUsuario.acaoSobreInteressesViagem}">
       <c:url var="urlAtividade" value="/perfil/${acaoUsuario.acaoPrincipal.autor.urlPath}/interesses" />
    </c:when>
    <%--c:when test="${acaoUsuario.acaoSobreConvite}">
    </c:when>
    <c:when test="${acaoUsuario.acaoSobrePreferenciasViagem}">
    </c:when --%>
    <c:otherwise>
      <c:url var="urlAtividade" value="#" />
    </c:otherwise>
</c:choose>

<div style="top: 101px;" class="news">
    <div class="history">
      <c:if test="${mostrarFotoAutor == true}">
        <div class="left" style="width: 65px">
            <fan:userAvatar user="${acaoUsuario.autor}" displayName="false" displayDetails="false" marginLeft="0" marginRight="8" />
        </div>
      </c:if>
    </div>
    <div class="description">
        <div>
            <c:if test="${mostrarIcone == true}">
              <div style="float:left; margin-right: 4px;">
                <img src="<c:url value="/resources/images/icons/mini/32/${acaoUsuario.tipoAcao.icone}" />" />
              </div>
            </c:if>            
            <p style="max-height: 60px;">
                <c:if test="${usarFontePequena}">
                  <small>
                </c:if>
                <c:choose>
                  <c:when test="${mostrarLinkNomeUsuario == true}">
                    <a href="<c:url value="/perfil/${acaoUsuario.autor.urlPath}" />">
                        ${nomeAutor}
                    </a>
                  </c:when>
                  <c:otherwise>
                    <strong>${nomeAutor}</strong>
                  </c:otherwise>
                </c:choose>
                <c:if test="${usarFontePequena}">
                  </small>
                </c:if>
                <c:if test="${quebrarLinhaAposNome}">
                  <br/>
                </c:if>
                <a href="${urlAtividade}" class="link-notificacao" style="text-decoration: none; color: #444;" data-acao-usuario-id="${acaoUsuario.id}">
                  <c:if test="${usarFontePequena}">
                    <small style="color: #444">
                  </c:if>
                    ${acaoUsuario.tipoAcao.descricaoAcaoNoPassado}
                    ${acaoUsuario.tipoAcao.preposicaoPosAcao}
                    ${acaoUsuario.tipoAcao.descricaoAlvo}
                    
                    <c:if test="${acaoUsuario.alvo != null and acaoUsuario.tipoAcao.preposicaoPosAlvo != ''}">
                        ${acaoUsuario.tipoAcao.preposicaoPosAlvo}
                        ${acaoUsuario.alvo.descricaoAlvo}
                    </c:if>
                  <c:if test="${usarFontePequena}">
                    </small>
                  </c:if>
                </a>
                <br/>
                <small>
                  <c:if test="${mostrarData == true}">
                    <fan:passedTime date="${acaoUsuario.data}"/>
                  </c:if>
                </small>
            </p>
        </div>

    </div>
</div>