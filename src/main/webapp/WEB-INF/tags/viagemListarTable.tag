<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<%@ attribute name="visoesViagem" required="true" type="java.util.Collection"%>
<%@ attribute name="viagens" required="true" type="java.util.Collection"%>
<%@ attribute name="titulo" required="false" type="java.lang.String"%>

<c:if test="${not empty viagens}">
<table class="table table-striped table-bordered table-hover table-condensed">
	<c:if test="${not empty titulo}">
	<thead>
		<tr><th colspan="2">${titulo}</th></tr>
	</thead>
	</c:if>
	<tbody>
		<c:forEach var="viagem" items="${viagens}">
			<tr>
				<td> 
				<ul class="unstyled">
					<li><h3>${viagem.titulo}(${viagem.quantidadeDias}dias)</h3></li>
					<li><strong>Criada por: </strong>${viagem.usuarioCriador.nomeExibicao}</li>
					<li><strong>Data da criação: </strong><fmt:formatDate value="${viagem.dataInclusao}" pattern="dd/MM/yyyy" /></li>
					<li><strong>Quem pode ver: </strong><i class="${viagem.visibilidade.icone}"></i> ${viagem.visibilidade.descricao}</li>
					<c:if test="${not empty viagem.dataInicio}">
					<li><strong>Data da viagem: </strong><fmt:formatDate value="${viagem.dataInicio}" pattern="dd/MM/yyyy" /></li>
					</c:if>
					<c:if test="${not empty viagem.descricao}">
					<li><strong>Descrição da viagem: </strong>${viagem.descricao}</li>
					</c:if>
					<li><div class="btn-toolbar">
							<div class="btn-group">
								<c:forEach var="visao" items="${visoesViagem}">
									<a class="btn btn-mini" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/exibir/${visao}">
										<img alt="Modo de visão ${visao.nome}" src="${pageContext.request.contextPath}/resources/images/${visao.icone}" />
										Visualizar no modo ${visao.nome}
									</a>
								</c:forEach>
							</div>
						</div>
					</li>
				</ul>
				</td>
				<td>
					<fan:viagemLocaisVisitadosEstadoSemContinente viagem="${viagem}" />
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
</c:if>