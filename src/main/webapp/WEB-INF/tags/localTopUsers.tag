<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="amostraPontuacaoUsuario" required="true" type="java.util.List" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %> 

<c:if test="${not empty amostraPontuacaoUsuario}">

<div id="top_users">
	<div class="borda-arredondada page-container">
		<div class="page-header module-box-header" style="margin: 0px;">
			<h3>Usuários mais ativos</h3>
		</div>
		<div style="padding: 5px;">
			<p><small>Usuários que mais contribuíram para esse destino em termos de avaliações, dicas, fotos etc.</small></p>
			<c:forEach items="${amostraPontuacaoUsuario}" var="pontuacao" varStatus="varStatus">
				<div class="row" style="margin-left : 0px; padding: 5px;">
					<h2><span style="float: left;">${varStatus.count}.</span></h2>
					<fan:userAvatar showFrame="true" user="${pontuacao.usuario}" displayDetails="true" detailsPosition="left" orientation="horizontal" nameSize="span2" />
					<div style="float: right;"><b>${pontuacao.pontos}</b></div>
				</div>
			</c:forEach>
		</div>
	</div>
</div>

</c:if>
