<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ attribute name="id" required="true" type="java.lang.String" %>
<%@ attribute name="tipo" required="true" type="java.lang.String" description="Indica qual objeto esta sendo comentado." %>
<%@ attribute name="idAlvo" required="false" type="java.lang.String" %>
<%@ attribute name="url" required="false" type="java.lang.String" %>
<%@ attribute name="inputType" required="false" type="java.lang.String" description="text ou textarea" %>
<%@ attribute name="inputClass" required="false" type="java.lang.String" description="css class do input" %>
<%@ attribute name="actionsClass" required="false" type="java.lang.String" description="css class container das ações" %>
<%@ attribute name="imgSize" required="false" type="java.lang.String" description="classe css da imagem do usuário (avatar, profile, mini-avatar etc.)" %>
<%@ attribute name="buttonOnSameLine" required="false" type="java.lang.Boolean" %>

<c:if test="${url == null}">
    <c:url value="/comentarios/postarComentario" var="url" />
</c:if>
<c:if test="${imgSize == null}"><c:set var="imgSize" value="avatar" /></c:if>

<sec:authorize access="isAuthenticated()">

    <fan:formComentario tipo="${tipo}" id="${id}" idAlvo="${idAlvo}" actionsClass="${actionsClass}" 
                        imgSize="${imgSize}" inputClass="${inputClass}" inputType="${inputType}" url="${url}" buttonOnSameLine="${buttonOnSameLine}" />
    
    <div id="modal-excluir-comentario" class="modal hide fade">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Excluir comentário</h3>
        </div>
        <div class="modal-body">
            <p>Deseja realmente excluir este comentário?</p>
        </div>
        <div class="modal-footer">
            <a id="confirmar-excluir-comentario" href="#" data-comentario-id="" class="btn btn-primary">
                <i class="icon-ok icon-white"></i> 
                Sim
            </a>
            <a href="#" class="btn" data-dismiss="modal">
                <i class="icon-remove"></i> 
                Não
            </a>
        </div>
    </div>

</sec:authorize>
    
<script>

$(document).ready(function () {
	
    $('#listaComentarios-${id}').load('<c:url value="/comentarios/lista/${tipo}/${idAlvo}?id=${id}&imgSize=${imgSize}&start=0&limit=5" />');
	
    $('.excluir-comentario').live('click', function (event) {
        event.preventDefault();
        $('#confirmar-excluir-comentario').attr('data-comentario-id', $(this).attr('data-comentario-id'));
        $('#modal-excluir-comentario').modal('show');
    });
    
    $('#confirmar-excluir-comentario').click(function(e) {
        e.preventDefault();
        var idComentario = $(this).attr('data-comentario-id')
        $.ajax({
            type: 'POST',
            url: '<c:url value="/comentarios/excluir/" />' + idComentario,
            success: function(data, status, xhr) {
                if (data.success) {
                    // fechar modal
                    $('#modal-excluir-comentario').modal('hide');
                    // remover o box do comentario excluido
                    $('#box-comentario-' + idComentario).fadeOut(1000, function() {});
                }
            }
        });
    });
    
});
</script>

<div style="height: 4px;"></div>
<div id="listaComentarios-${id}" style="width: 99%; padding-right: 2px;">
    <%--@ include file="/WEB-INF/views/comentarios/listaComentarios.jsp" --%>
</div>