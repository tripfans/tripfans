<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2" %> 
<%@ attribute name="item" required="true" type="br.com.fanaticosporviagens.model.entity.ViagemItem" %> 
<%@ attribute name="modoVisao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemModoVisao" %>
<%@ attribute name="permissao" required="true" type="br.com.fanaticosporviagens.viagem.ViagemPermissao" %> 

<li>
	<ul class="unstyled" >
	<li>
	<c:choose>
		<c:when test="${not empty item.url}">
			<a href="${pageContext.request.contextPath}${item.url}"> 
				<img alt="${item.iconeAlt}"
				src="${pageContext.request.contextPath}/resources/images/${item.icone}"
				width="20" height="20" /> <strong>${item.tituloItem}</strong>
			</a>
		</c:when>
		<c:otherwise>
			<img alt="${item.iconeAlt}"
				src="${pageContext.request.contextPath}/resources/images/${item.icone}"
				width="20" height="20" />
			<strong>${item.tituloItem}</strong>
		</c:otherwise>
	</c:choose>
	</li>
    <c:if test="${not empty item.local && item.local.quantidadeAvaliacoes > 0 && not empty item.local.avaliacaoConsolidadaGeral}">
    <li><span title="${local.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}" class="starsCategory cat${item.local.avaliacaoConsolidadaGeral.classificacaoGeral.codigo}">&nbsp;</span></li>
    </c:if>
    <c:if test="${not empty item.transporteLocalOrigem}"><li><strong>Origem: </strong><a href="${pageContext.request.contextPath}${item.transporteLocalOrigemUrl}">${item.transporteLocalOrigem.nome}</a></li></c:if>
    <c:if test="${not empty item.transporteLocalDestino}"><li><strong>Destino: </strong><a href="${pageContext.request.contextPath}${item.transporteLocalDestinoUrl}">${item.transporteLocalDestino.nome}</a></li></c:if>
    <c:if test="${not empty item.endereco}"><li><strong>Endereço: </strong>${item.endereco}</li></c:if>
    <c:if test="${not empty item.telefone}"><li><strong>Fone: </strong>${item.telefone}</li></c:if>
    <c:choose>
    	<c:when test="${modoVisao.calendario}">
		    <c:if test="${not empty item.site}"><li><strong>Site: </strong><a href="${item.site}"><abbr title="${item.site}">link</abbr></a></li></c:if>
		    <c:if test="${not empty item.email}"><li><strong>Email: </strong><a href="mailto:${item.email}"><abbr title="${item.email}">link</abbr></a></li></c:if>
    	</c:when>
    	<c:otherwise>
		    <c:if test="${not empty item.site}"><li><strong>Site: </strong><a href="${item.site}">${item.site}</a></li></c:if>
		    <c:if test="${not empty item.email}"><li><strong>Email: </strong>${item.email}</li></c:if>
    	</c:otherwise>
    </c:choose>
    <c:if test="${not empty item.dataHoraInicio or not empty item.dataHoraFim}"><li><strong>Horário: </strong><fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" /> - <fmt:formatDate value="${item.dataHoraFim}" pattern="HH:mm" /></li></c:if>
    <c:if test="${not empty item.valor}"><li><strong>Valor: </strong>${item.valor}</li></c:if>
    <c:if test="${not empty item.descricao}"><li>${item.descricao}</li></c:if>
    <li>
    <c:if test="${permissao.usuarioPodeAlterarItem 
    		|| permissao.usuarioPodeExcluirItem 
    		|| permissao.usuarioPodeMoverItem 
    		|| permissao.usuarioPodeMovimentarItem}">
    <div class="btn-group">
    	<a class="btn dropdown-toggle btn-mini" data-toggle="dropdown" href="#">
    	<i class="icon-tasks"></i> Ações <span class="caret"></span>
    	</a>
    	<ul class="dropdown-menu" role="menu">
    		<c:if test="${permissao.usuarioPodeAlterarItem}">
    		<li><a href="#formViagemItemAlterarModal" role="button" class="btnAlterarItem" data-toggle="modal" idItem="${item.id}">
    			<i class="icon-edit"></i> - Alterar</a>
    		</li>
    		</c:if>
    		<c:if test="${permissao.usuarioPodeExcluirItem}">
    		<li><a tabindex="-1" class="btnExcluirItem" idItem="${item.id}" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/excluirItem">
    		    <i class="icon-trash"></i> - Excluir
    		    </a>
    		</li>
    		</c:if>
    		<c:if test="${permissao.usuarioPodeMoverItem}">
    		<li class="divider"></li>
    		<c:if test="${not item.primeiro}">
    		<li><a class="btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/moverItemParaPrimeiro" idItem="${item.id}" >
    			<i class="icon-circle-arrow-up"></i> - Mover para primeiro</a>
    		</li>
    		</c:if>
    		<c:if test="${not item.primeiro and not item.segundo}">
    		<li><a class="btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/moverItemDecrementarPosicao" idItem="${item.id}" >
    			<i class="icon-arrow-up"></i> - Mover para cima</a>
    		</li>
    		</c:if>
    		<c:if test="${not item.ultimo and not item.penultimo}">
    		<li><a class="btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/moverItemIncrementarPosicao" idItem="${item.id}" >
    			<i class="icon-arrow-down"></i> - Mover para baixo</a></li>
    		</c:if>
    		<c:if test="${not item.ultimo}">
    		<li><a class="btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/moverItemParaUltimo" idItem="${item.id}" >
    			<i class="icon-circle-arrow-down"></i> - Mover para último</a>
    		</li>
    		</c:if>
    		</c:if>
    		<c:if test="${permissao.usuarioPodeMovimentarItem}">
    		<li class="divider"></li>
    		<c:choose>
    		<c:when test="${empty item.viagem}">
	 			<li><a class="btnMovimentarParaViagem" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/movimentarItemParaViagem" idItem="${item.id}">
	 				<i class="icon-upload"></i> - Mover para item da viagem
	 				</a>
	 			</li>
	 			<c:forEach var="dia" items="${item.dia.viagem.dias}">
	 			    <c:if test="${item.dia != dia}">
	 				<li><a class="btnMovimentarParaDia" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/movimentarItemParaDia" idItem="${item.id}" idDiaDestino="${dia.id}">
		 				<c:choose>
		 				<c:when test="${dia.numero > item.dia.numero}"><i class="icon-arrow-right"></i></c:when>
		 				<c:otherwise><i class="icon-arrow-left"></i></c:otherwise>
		 				</c:choose>
		 				- Mover para o ${dia.descricao}
		 				</a>
		 			</li>
	 				</c:if>
	 			</c:forEach>
 			</c:when>
 			<c:otherwise>
	 			<c:forEach var="dia" items="${item.viagem.dias}">
	 				<li><a class="btnMovimentarParaDia" tabindex="-1" href="${pageContext.request.contextPath}/viagem2/${viagem.id}/movimentarItemParaDia" idItem="${item.id}" idDiaDestino="${dia.id}">
	 					<i class="icon-download"></i> - Mover para o ${dia.descricao}
	 					</a>
	 				</li>
	 			</c:forEach>
 			</c:otherwise>
 			</c:choose>
 			</c:if>
    	</ul>
	</div>
	<br/>
	</c:if>
	</li>
	</ul>
</li>
