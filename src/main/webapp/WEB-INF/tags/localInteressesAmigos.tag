<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="interessesAmigos" required="true" type="java.util.List" %> 

<c:if test="${not empty interessesAmigos}">

	<div class="horizontalSpacer"></div>
 	
 	<div class="page-header">
		<h3>
		Atividade dos Amigos
		</h3>
	</div>
 	
 	<div class="span11">
		<ul class="nav nav-tabs" id="tabs-interesses-amigos">
			<c:forEach items="${interessesAmigos}" var="interesseAmigo" varStatus="varStatusTabs">
            
                <c:if test="${interesseAmigo.tipo.interesseJaFoi}">
                    <c:set var="badge" value="success"/>
                </c:if>
                
                <c:if test="${interesseAmigo.tipo.interesseDesejaIr}">
                    <c:set var="badge" value="info"/>
                </c:if>
                
                <c:if test="${interesseAmigo.tipo.interesseFavorito}">
                    <c:set var="badge" value="important"/>
                </c:if>
                
				<li <c:if test="${varStatusTabs.first}"> class="active"</c:if> >
					<a href="#tab-interesses-amigos-${varStatusTabs.count}" data-toggle="tab">
						${interesseAmigo.titulo} <span class="badge badge-${badge}">${interesseAmigo.quantidadeAmigos}</span>
					</a>
				</li>
			</c:forEach>
		</ul>
		
		<div class="tab-content">
		    <c:forEach items="${interessesAmigos}" var="interesseAmigo" varStatus="varStatusTabs">
				<div class="tab-pane<c:if test="${varStatusTabs.first}"> active</c:if>" id="tab-interesses-amigos-${varStatusTabs.count}">
                  <div class="span9">
					<c:forEach items="${interesseAmigo.amostraInteresses}" var="interesse">
						<c:choose>
					        <c:when test="${interesse.usuario != null}">
					            <div><fan:userAvatar user="${interesse.usuario}" displayName="false" displayDetails="true" displayNameTooltip="true"/></div>
					        </c:when>
						    <c:otherwise>
					            <div>
				            		<fan:userAvatar displayName="false" showFacebookMark="true" idUsuarioFacebook="${interesse.idUsuarioFacebook}" displayNameTooltip="true" nomeUsuario="${interesse.nomeUsuario}" />
					            </div>
					        </c:otherwise>
					    </c:choose>
					</c:forEach>
                  </div>
				</div>
			</c:forEach>
            <div class="span2" style="margin: 25px 0 0 0">
              <div class="row">
                <a id="verTodasAtividades" href="<c:url value="/locais/interessesAmigos/${local.urlPath}" />" class="seeMore">
                Ver todos
                </a>
              </div>
            </div>
		</div>
	</div>
    
    <c:if test="${not empty atividades}">
      <div class="span11" style="margin-top: 15px;">
        <fieldset>
            <legend>
                Últimas atividades
            </legend>    
            <jsp:include page="/WEB-INF/views/locais/ultimasAtividadesAmigos.jsp">
              <jsp:param name="cssSpan" value="span11"/>
            </jsp:include>
        </fieldset>
      </div>
    </c:if>
 	
	<script>
		$('a.interesseTodosAmigos').bind('click',function(event){
			event.preventDefault();
		    $.get(this.href,{},function(response) {
		    	$('#pageContent').html(response);
		 	  	$("#pageContent").initToolTips();
		    });
		});
		
		jQuery(document).ready(function() {
            $('#verTodasAtividades').fancybox({
                autoDimensions : false,
                width: 700,
                height: 500,
                scrolling: 'auto'
            });
        });
	</script>
</c:if>
