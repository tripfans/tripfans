<%@tag pageEncoding="UTF-8"%>

<%@ attribute name="cssClass" required="false" type="java.lang.String" description="Style CSS para serem aplicadas aos botoes" %>
<%@ attribute name="id" required="true" type="java.lang.String" description="Identificador HTML do componente." %>
<%@ attribute name="name" required="true" type="java.lang.String" description="Nome do atributo que sera submetido no form" %>
<%@ attribute name="selectedValue" required="false" type="java.lang.Object" description="Valor pre-selecionado" %>
<%@ attribute name="size" required="false" type="java.lang.String" description="Tamanho do botao: mini, normal ou large. Default: normal" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% jspContext.setAttribute("cssClass", cssClass, PageContext.REQUEST_SCOPE); %>
<% jspContext.setAttribute("selectedValue", selectedValue, PageContext.REQUEST_SCOPE); %>
<% jspContext.setAttribute("size", size, PageContext.REQUEST_SCOPE); %>

<c:if test="${not empty selectedValue}"><c:set var="hiddenValue" value="value=${selectedValue}" /></c:if>

<div id="${id}" class="btn-group" data-toggle="buttons-radio">
	<jsp:doBody />
</div>
<input type="hidden" name="${name}" id="${id}_hiddenRadio" ${hiddenValue} />

<script type="text/javascript">
$('#${id} > button.radioBtn').click(function(event){
	event.preventDefault();
	var button = $(this);
	if ($('#${id}_hiddenRadio').val() != button.attr('value')) {
        $('#${id}_hiddenRadio').val(button.attr('value'));
        // força a chamada do 'changed'
        $('#${id}_hiddenRadio').trigger('change');
    }
});
</script>