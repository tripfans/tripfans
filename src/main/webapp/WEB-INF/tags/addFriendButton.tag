<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="usuario" required="true" type="java.lang.String" description="Nome (urlPath) do usuário para envio da solicitação de amizade" %>
<%@ attribute name="buttonStyle" required="false" type="java.lang.String" description="Css botao" %>

<a href="<c:url value="/convidarParaAmizade/${usuario}" />" class="btn btn-mini btn-secondary" id="btnAmizade_${usuario}" style="${buttonStyle}">
   <i class="icon-plus-sign"></i>
   <b>Adicionar aos amigos</b>
</a>

<script type="text/javascript">
 $('#btnAmizade_${usuario}').click(function(event) {
	 event.preventDefault();
	 var button = $(this);
	 $(button).append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="13" style="margin-left: 8px;" >');
	 $.post(this.href,{},function(response){
		 $(button).remove();
	 });
 });
</script>