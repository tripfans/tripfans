<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2"%>

<c:if test="${not empty viagem.locaisVisitados}">
	<c:forEach var="continente" varStatus="continenteStatus" items="${viagem.locaisVisitados}">
		<c:if test="${not empty continente.sublocais}">
			<c:forEach var="pais" items="${continente.sublocais}"
				varStatus="paisStatus">
				<c:if test="${not paisStatus.first and empty pais.sublocais}"><br/></c:if>
				<strong><img alt="Local do tipo"
					src="${pageContext.request.contextPath}/resources/images/${pais.local.iconeMapa}"
					width="20" height="20" /> ${pais.local.nome}</strong>
				<c:if test="${not empty pais.sublocais}">
					<blockquote>
						<c:forEach var="estado" items="${pais.sublocais}" varStatus="estadoStatus">
							<c:if test="${not estadoStatus.first or empty estado.sublocais}"><br/></c:if>
							<strong><img alt="Local do tipo" src="${pageContext.request.contextPath}/resources/images/${estado.local.iconeMapa}" width="20" height="20" />
							${estado.local.nome}
							</strong>
						</c:forEach>
					</blockquote>
				</c:if>
			</c:forEach>
		</c:if>
	</c:forEach>
</c:if>

