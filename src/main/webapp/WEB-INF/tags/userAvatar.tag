<%@tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>

<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ tag body-content="empty" %> 
<%@ attribute name="user" required="false" type="br.com.fanaticosporviagens.model.entity.Usuario" %>
<%@ attribute name="friendship" required="false" type="br.com.fanaticosporviagens.model.entity.Amizade" %>
<%@ attribute name="idUsuarioFacebook" required="false" type="java.lang.String" %>
<%@ attribute name="nomeUsuario" required="false" type="java.lang.String" %>   
<%@ attribute name="orientation" required="false" type="java.lang.String" description="Indica se o nome do usuario deve aparecer ao lado (horizontal) ou abaixo (padrao) da foto." %>
<%@ attribute name="displayName" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayUsername" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayNameTooltip" required="false" type="java.lang.Boolean" %>
<%@ attribute name="displayDetails" required="false" type="java.lang.Boolean" %>
<%@ attribute name="enableLink" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showFrame" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showShadow" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showFacebookMark" required="false" type="java.lang.Boolean" %>
<%@ attribute name="imgSize" required="false" type="java.lang.String" description="Tamanho da imagem em classe CSS (profile, avatar, mini-avatar)" %>
<%@ attribute name="imgStyle" required="false" type="java.lang.String" %>
<%@ attribute name="imgFloat" required="false" type="java.lang.String" description="left, right ou center. Default: left" %>
<%@ attribute name="useImgFloat" required="false" type="java.lang.Boolean" %>
<%@ attribute name="useLazyLoad" required="false" type="java.lang.Boolean" %>
<%@ attribute name="divClass" required="false" type="java.lang.String" %>
<%@ attribute name="nameSize" required="false" type="java.lang.String" description="Tamanho do container do nome em classe CSS .span" %>
<%@ attribute name="width" required="false" type="java.lang.Integer" %> 
<%@ attribute name="height" required="false" type="java.lang.Integer" %>
<%@ attribute name="marginLeft" required="false" type="java.lang.Integer" %> 
<%@ attribute name="marginRight" required="false" type="java.lang.Integer" %>
<%@ attribute name="detailsPosition" required="false" type="java.lang.String" description="Posicao em que o popover aparecer: top, right (default), bottom, left" %>
<%@ attribute name="positionRelative" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showImgAsCircle" required="false" type="java.lang.Boolean" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty showFacebookMark}">
    <c:set var="showFacebookMark" value="true" />
</c:if>
<c:if test="${empty positionRelative}">
    <c:set var="positionRelative" value="true" />
</c:if>

<c:choose>
    <c:when test="${not empty user and not empty user.id}">
    
        <c:url value="/perfil/${user.urlPath}" var="userUrl" />
        <c:choose>
            <c:when test="${user.fotoExterna}">
                <c:if test="${user.conectadoFacebook}">
                    <c:url value="http://graph.facebook.com/${user.idFacebook}/picture?type=large" var="urlFotoPerfil" />
                </c:if>
                <c:if test="${user.importadoGoogle}">
                    <c:url value="${user.urlFotoAvatar}" var="urlFotoPerfil" />
                </c:if>
            </c:when>
            <c:otherwise>
                <%--c:set value="${tripFansEnviroment.imagesServerUrl}/usuarios/${user.id}/FotoPerfil_small.jpg" var="urlFotoPerfil" /--%>
                <c:url var="urlFotoPerfil" value="${user.urlFotoAvatar}" />
            </c:otherwise>
        </c:choose>
        <c:if test="${not user.ativo}">
          <c:set var="displayDetails" value="false" />
          <c:if test="${not empty user.idFacebook}">
            <c:set var="showFBMark" value="true" />
            <c:url value="/perfil/fb/${user.idFacebook}" var="userUrl" />
            <c:url value="http://graph.facebook.com/${user.idFacebook}/picture?type=large" var="urlFotoPerfil" />
          </c:if>
        </c:if>
        <c:set var="nomeCompletoUsuario" value="${user.displayName}" />
        <c:set var="nomeUsuario" value="${user.displayName}" />
    </c:when>
    <c:when test="${not empty friendship}">
        <c:set var="nomeCompletoUsuario" value="${friendship.nomeAmigo}" />
        <c:if test="${friendship.tripfans}">
            <c:url value="/perfil/${friendship.idAmigo}" var="userUrl" />
            <c:choose>
	            <c:when test="${friendship.amigo.fotoExterna}">
	                <c:if test="${friendship.amigo.conectadoFacebook}">
	                    <c:url value="http://graph.facebook.com/${friendship.amigo.idFacebook}/picture" var="urlFotoPerfil" />
	                </c:if>
	            </c:when>
	            <c:otherwise>
	                <%-- c:set value="${tripFansEnviroment.imagesServerUrl}/usuarios/${friendship.amigo.id}/FotoPerfil_small.jpg" var="urlFotoPerfil" /--%>
                    <c:url var="urlFotoPerfil" value="${friendship.amigo.urlFotoSmall}" />
	            </c:otherwise>
        	</c:choose>
        </c:if>
        <c:if test="${friendship.facebook}">
            <c:set var="showFBMark" value="true" />
            <c:url value="/perfil/fb/${friendship.idAmigoFacebook}" var="userUrl" />
            <c:url value="http://graph.facebook.com/${friendship.idAmigoFacebook}/picture" var="urlFotoPerfil" />
        </c:if>
    </c:when>
    <c:when test="${idUsuarioFacebook != null}">
    	<c:url value="/perfil/fb/${idUsuarioFacebook}" var="userUrl" />
        <c:url value="http://graph.facebook.com/${idUsuarioFacebook}/picture" var="urlFotoPerfil" />
    </c:when>
</c:choose>

<c:if test="${nomeUsuario != null}"><c:set var="nomeCompletoUsuario" value="${nomeUsuario}" /></c:if>

<c:if test="${enableLink == null}"><c:set var="enableLink" value="true" /></c:if>
<c:if test="${height == null}"><c:set var="height" value="50" /></c:if>
<c:if test="${width == null}"><c:set var="width" value="50" /></c:if>
<c:if test="${marginLeft == null}"><c:set var="marginLeft" value="5" /></c:if>
<c:if test="${marginRight == null}"><c:set var="marginRight" value="5" /></c:if>
<c:if test="${displayName == null}"><c:set var="displayName" value="true" /></c:if>
<c:if test="${showFrame == null}"><c:set var="showFrame" value="true" /></c:if>
<c:if test="${showShadow == null}"><c:set var="showShadow" value="true" /></c:if>
<c:if test="${imgSize == null && width == null}"><c:set var="imgSize" value="avatar" /></c:if>
<c:if test="${imgFloat == null}"><c:set var="imgFloat" value="left" /></c:if>
<c:if test="${useImgFloat == null}"><c:set var="useImgFloat" value="true" /></c:if>
<c:if test="${nameSize == null}"><c:set var="nameSize" value="span4" /></c:if>
<c:if test="${detailsPosition == null}"><c:set var="detailsPosition" value="right" /></c:if>

<% String idUnico = RandomStringUtils.random(12, true, true); %>
	
    <c:if test="${enableLink}">
	  <a href="${userUrl}">
    </c:if>
    <c:if test="${useImgFloat}">
        <c:set var="styleImgFloat" value="float: ${imgFloat}" />    
    </c:if>
    
    <c:choose>
        <c:when test="${user.feminino}">
          <c:url var="urlPlaceHolder" value="/resources/images/perfil/blank_female_small.jpg" />
        </c:when>
        <c:otherwise>
          <c:url var="urlPlaceHolder" value="/resources/images/perfil/blank_male_small.jpg" />
        </c:otherwise>
    </c:choose>
    
    <c:choose>
    	<c:when test="${orientation == 'horizontal' }">
    	  <div>
    		<div style="${not empty styleImgFloat ? styleImgFloat : ''}; margin-left:${marginLeft}px; margin-right: ${marginRight}px;" class="${divClass}">
           		<img id="<%= idUnico %>" class="${imgSize} ${showFrame ? ' thumbnail ' : ''} ${showShadow ? ' sombra ' : ''} ${useLazyLoad ? ' lazy ' : ''} ${showImgAsCircle ? 'img-circle' : 'borda-arredondada'}" 
           		     ${useLazyLoad ? 'data-original' : 'src'}="${urlFotoPerfil}" 
           		     data-place-holder="${urlPlaceHolder}"
           		     style="background-color: #FEFEFE; ${imgStyle}" 
           		     width="${width}" height="${height}"
           			<c:if test="${displayDetails}">
        			        data-url="<c:url value="/perfil/popover/${user.urlPath}" />"
                            data-loaded="false"
        			        data-title="${nomeUsuario}"
                    </c:if>
           		/>
        	</div>
            <c:if test="${displayName}">
            	<div class="${nameSize}" style="margin-left: 5px; padding-top: 4px;">
                	<span style="margin-top: 1px;"><strong>${nomeUsuario}</strong></span>
            	</div>
            </c:if>
            <c:if test="${displayUsername and not empty user}">
                <div class="${nameSize}" style="margin-left: 5px; padding-top: 4px;">
                    <span style="margin-top: 1px;">
                        ${user.username}
                    </span>
                </div>
            </c:if>
    	  </div>
    	</c:when>
    	<c:otherwise>
    	  <div style="${not empty styleImgFloat ? styleImgFloat : ''}; margin-left:${marginLeft}px; margin-right: ${marginRight}px;">
    		<div>
    	        <div style="margin-bottom: 5px; ${positionRelative ? 'position: relative;' : '' }" >
                    <c:if test="${showFacebookMark and showFBMark}">
                      <div>
                        <i class="facebook-mark-small"></i>
                      </div>
                    </c:if>
    	            <img id="<%= idUnico %>" class="${imgSize} ${showFrame ? ' thumbnail ' : ''} ${showShadow ? ' sombra ' : ''} ${useLazyLoad ? ' lazy ' : ''} ${showImgAsCircle ? 'img-circle' : 'borda-arredondada'}" 
    	                 ${useLazyLoad ? 'data-original' : 'src'}="${urlFotoPerfil}"
    	                 data-place-holder="${urlPlaceHolder}"
    	                 style="background-color: #FEFEFE; ${imgStyle}" 
    	                 width="${width}" height="${height}" 
        	            <c:if test="${displayDetails}">
        			        data-url="<c:url value="/perfil/popover/${user.urlPath}" />"
                            data-loaded="false"
        			        data-title="${nomeUsuario}"
                        </c:if>
                        <c:if test="${displayNameTooltip == true}">
                        	title="${nomeUsuario}"
                        </c:if>
    	            />
    	        </div>
                <c:if test="${displayName}">
        	        <div>
        	           <strong>${nomeUsuario}</strong>
        	        </div>
                </c:if>
                <c:if test="${displayUsername and not empty user}">
                    <div>
                       <strong>${user.username}</strong>
                    </div>
                </c:if>
	        </div>
	      </div>
    	</c:otherwise>
    </c:choose>
    <c:if test="${enableLink}">
      </a>
    </c:if>

<c:if test="${displayDetails}">
<script>

jQuery(function ($) {
    
    $('#<%=idUnico%>').mouseover( function(event) {
        
        event.preventDefault();
        event.stopPropagation();
        var $img = $(this);
        
        setTimeout(function() {
            if ($img.is(':hover')) {
            	
                if ($img.attr('data-loaded') != 'true') {

                	$('#<%=idUnico%>').popover({
                        trigger : 'hover',
                        position : '${detailsPosition}',
                        title: 'Carregando...',
                        content: '<div align="center"><img src="<c:url value="/resources/images/loading.gif"/>" /></div>',
                    })
            	
                    $img.popover('ajax', $img.attr('data-url')).popover('title', $img.attr('data-title')).popover({
                        trigger : 'hover',
                        position : 'right'
                    }).popover('show');
                    $img.attr('data-loaded', 'true');
            	} else {
            		$img.popover('show');
            	}
            }
        }, 1000);
        
    });
    
});

</script>
</c:if>