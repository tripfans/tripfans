<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ attribute name="id" required="true" type="java.lang.String" %> 
<%@ attribute name="locais" required="true" type="java.util.List" %>

<div class="borda-arredondada" style="padding: 15px; display: inline-block; margin-left: 0px; position: relative; width: 97%; height: 450px;">
    <div>
        <div id="local-${id}" class="showbiz-container whitebg sb-retro-skin" style="padding-bottom: 0px;">

            <!-- THE NAVIGATION -->
            <div class="showbiz-navigation sb-nav-retro hidden-xs">
                <a id="showbiz_left-${id}" class="sb-navigation-left"><i class="sb-icon-left-open"></i></a>
                <a id="showbiz_right-${id}" class="sb-navigation-right"><i class="sb-icon-right-open"></i></a>
                <div class="sbclear"></div>
            </div> <!-- END OF THE NAVIGATION -->

            <div class="divide10"></div>

            <!--    THE PORTFOLIO ENTRIES   -->
            <div class="showbiz" data-left="#showbiz_left-${id}" data-right="#showbiz_right-${id}" data-play="#showbiz_play">

                <!-- THE OVERFLOW HOLDER CONTAINER, DONT REMOVE IT !! -->
                <div class="overflowholder">
                    <!-- LIST OF THE ENTRIES -->
                    <ul id="ul-list-locais-${id}">
                    
                      <c:forEach items="${locais}" var="local">

                        <li class="sb-retro-skin" style="cursor: pointer;">
                            <!-- THE MEDIA HOLDER -->
                            <div class="mediaholder conteudoLocal" style="border: 0px;">
                                <div class="mediaholder_innerwrap borda-arredondada">
                                    <i class="carrossel-thumb carrossel-home-img" style="background-image: url('${local.urlFotoAlbum}'); min-height: 200px;"></i>
                                    <!-- HOVER COVER CONAINER -->
                                    <div class="hovercover borda-arredondada" style="z-index: 21;">
                                        
                                    </div><!-- END OF HOVER COVER -->
                                </div>
                            </div><!-- END OF MEDIA HOLDER CONTAINER -->

                            <!-- DETAIL CONTAINER -->
                            <div class="detailholder" style="margin-top: 10px;">
                                <p style="min-height: 50px; font-family: 'PT Sans Narrow', sans-serif;">
                                    <strong class="local-card-title" style="padding-top: 0px; margin-bottom: 0px;">
                                        ${local.nome}
                                    </strong>
                                    <small>
                                            ${local.endereco}
                                    </small>
                                    <c:if test="${local.notaTripAdvisor > 0}">
					                           <span class="tripAdvisorRating rate-${fn:replace(local.notaTripAdvisor, '.', '') }" title="Nota dos viajantes site TripAdvisor.com - ${local.numAvaliacoesTripAdvisor} avaliações"></span>
					                </c:if>
                                    <c:if test="${not empty local.urlBooking}">
                                    <div class="text-center">
                                    	<a href="${local.urlBooking}" target="_blank" class="btn btn-sm btn-warning">Reservar no Booking.com</a>
                                    </div>
                                    </c:if>
                                </p>
                            </div><!-- END OF DETAIL CONTAINER -->
                            
                        </li>
                      </c:forEach>
                    </ul>
                    <div class="sbclear"></div>
                </div> <!-- END OF OVERFLOWHOLDER -->
                <div class="sbclear"></div>
            </div>
        </div>
        
    </div>
    
</div>

<compress:js enabled="true" jsCompressor="closure" >
<script type="text/javascript">
     $(document).ready(function() {
    	 
 
     })

</script>
</compress:js>