<%@ tag pageEncoding="UTF-8" body-content="empty" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %>
<%@ attribute name="tamanhoTitulo" required="false" type="java.lang.String" description="small, medium, large" %>
<%@ attribute name="tamanhoImagem" required="false" type="java.lang.String" description="small, medium, large" %>
<%@ attribute name="exibirAcaoAvaliacao" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirAcaoDica" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirAcaoFoto" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirAcaoPergunta" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirAcaoMapa" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirDescricao" required="false" type="java.lang.Boolean" %>

<c:set var="tamanhoImagem" value="${empty tamanhoImagem ? 'medium' : tamanhoImagem}" />
<c:set var="tamanhoTitulo" value="${empty tamanhoTitulo ? 'large' : tamanhoTitulo}" />

<c:set var="tagTitulo" value="${tamanhoTitulo == 'large' ? 'h2' : 'h4'}"/>

<c:set var="urlPathLocal" value="${not empty local.urlPath ? local.urlPath : local.id}"/>

<style>
div.opcaoAcao {
    padding: 10px;
    padding-top: 40px;
    min-height: 124px !important;
    cursor: pointer;
    width: 22%;
    display: inline-table;
}
div.opcaoAcao h4 {
    padding-top: 8px;
}
</style>
<div style="text-align: center;">
    <c:if test="${exibirAcaoAvaliacao}">
      <a href="<c:url value="/avaliacao/avaliar/${urlPathLocal}" />" style="text-decoration: none;">
        <div class="homeOpcaoAcao opcaoAcao borda-arredondada">
            <img src="<c:url value="/resources/images/icons/mini/64/avaliacao.png" />" height="64"/>
            <${tagTitulo} class="avaliacao">Escreva avaliações</${tagTitulo}>
            <c:if test="${exibirDescricao}">
              <p class="muted">
                Escreva avaliações relatando suas experiências nos locais em que você já esteve.
              </p>
            </c:if>
        </div>
      </a>
    </c:if>
    <c:if test="${exibirAcaoDica}">
      <a href="<c:url value="/dicas/escrever/${urlPathLocal}" />" style="text-decoration: none;">
        <div class="homeOpcaoAcao opcaoAcao">
            <img src="<c:url value="/resources/images/icons/mini/64/dica.png" />" width="64" />
            <${tagTitulo} class="dica">Deixe dicas</${tagTitulo}>
            <c:if test="${exibirDescricao}">
              <p class="muted">
                Deixe dicas para outros viajantes.
              </p>
            </c:if>
        </div>
      </a>
    </c:if>
    <c:if test="${exibirAcaoPergunta}">
      <a href="<c:url value="/perguntas/perguntar?local=${urlPathLocal}" />" style="text-decoration: none;">
        <div class="homeOpcaoAcao opcaoAcao">
            <img src="<c:url value="/resources/images/icons/mini/64/pergunta.png" />" width="64" />
            <${tagTitulo} class="pergunta">Faça perguntas</${tagTitulo}>
            <c:if test="${exibirDescricao}">
              <p class="muted">
                Faça perguntas e deixe a comunidade ajudá-lo em suas próximas viagens.
              </p>
            </c:if>
        </div>
      </a>
    </c:if>
    <c:if test="${exibirAcaoFoto}">
      
    </c:if>
    <c:if test="${exibirAcaoMapa}">
      <c:url var="urlMapa" value="/perfil/meuPerfil?tab=mapa"/>
      <c:if test="${usuario != null}">
        <c:url var="urlMapa" value="/perfil/${usuario.urlPath}/mapa"/>
      </c:if>
      <a href="${urlMapa}" style="text-decoration: none;">
        <div class="homeOpcaoAcao opcaoAcao">
            <img src="<c:url value="/resources/images/icons/mini/64/mapa2.png" />" width="64" />
            <${tagTitulo} class="mapa">Crie seu mapa de viagens</${tagTitulo}>
            <c:if test="${exibirDescricao}">
              <p class="muted">
                Crie seu mapa personalizado de viagens, dizendo onde já esteve, para onde quer ir e destinos favoritos.
              </p>
            </c:if>
        </div>
      </a>
    </c:if>
</div> 