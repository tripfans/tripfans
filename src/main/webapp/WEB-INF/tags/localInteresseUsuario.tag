<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="interesse" required="true" type="br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal" %> 

<c:if test="${not empty interesse}">

<div class="btn-group">
  <a class="btn btn-warning dropdown-toggle" data-toggle="dropdown" href="#">
    Você
    <span class="caret"></span>
  </a>
	<ul class="dropdown-menu">
	<c:if test="${not empty interesse.tiposInteresseNaoSelecionadosIrFoiLocal}">
		<c:forEach items="${interesse.tiposInteresseNaoSelecionadosIrFoiLocal}" var="tipo">
			<li><a href="${pageContext.request.contextPath}/locais/registrarInteresse/${tipo.urlPath}/${local.urlPath}" class="btnInteresse" >${tipo.tituloInteresse}</a></li>  
		</c:forEach>
		<li class="divider"></li>  
	</c:if>
	<c:if test="${not interesse.seguir}">
		<li><a href="${pageContext.request.contextPath}/locais/registrarInteresse/<%= br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal.SEGUIR.getUrlPath() %>/${local.urlPath}" class="btnInteresse" ><%= br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal.SEGUIR.getTituloInteresse() %></a></li>  
		<li class="divider"></li>  
	</c:if> 

	<c:if test="${not empty interesse.tiposInteresseNaoSelecionadosOpiniao}">
		<li><b>Falaria para seus amigos que o local é:</b></li>
		<c:forEach items="${interesse.tiposInteresseNaoSelecionadosOpiniao}" var="tipo">
			<li><a href="${pageContext.request.contextPath}/locais/registrarInteresse/${tipo.urlPath}/${local.urlPath}" class="btnInteresse" >${tipo.tituloInteresse}</a></li>  
		</c:forEach>
		<li class="divider"></li>  
	</c:if>
	</ul>
</div>

<script>
$('a.btnInteresse').click(function(event){
	event.preventDefault();
	
	var urlPath = $(this).attr('href');
	
	$.post(urlPath,{},function(response){
    	$('#pageContent').html(response);
 	  	$("#pageContent").initToolTips();
    });
});
</script>

</c:if>
