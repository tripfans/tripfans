<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="linkText" required="true" type="java.lang.String" description="Texto do Link" %>
<%@ attribute name="url" required="true" type="java.lang.String" description="Url que retornara o conteudo a ser exibido dentro do popover" %>
<%@ attribute name="placement" required="false" type="java.lang.String" description="Localizacao do popover:  top | bottom | left | right" %>

<% String idUnico = RandomStringUtils.random(12, true, true); %>

<a id="<%=idUnico%>-popover" href="#">
    ${linkText}
</a>

<script type="text/javascript">
 $('#<%=idUnico%>-popover').mouseenter( function() {
	 var el=$(this);
	 $.get('<c:url value="${url}" />', function(response) {
		 el.popover({
			content: response, 
			placement: '${placement != null ? placement : 'right'}', 
			//title: '',
			delayIn : 1000,
			trigger: 'manual',
			template: '<div class="popover" style="z-index: 9000;" onmouseover="popoverMouseover_<%=idUnico%>_popover();"><div class="arrow"></div><div class="popover-inner" style="width: auto;"><h5 class="popover-title"></h5><div class="popover-content"><p></p></div></div></div>'
		}).popover('show');
	 });
 })
 .mouseleave( function() {
	 var el=$(this);
	 var timeout = setTimeout(function() { 
		el.popover('hide');
	 }, 800);
	 el.data('timeout', timeout);
 });
 
 function popoverMouseover_<%=idUnico%>_popover() {
	 $('.popover-inner').mouseenter(function() {
	     var el = $('#<%= idUnico %>');
	     var t = el.data('timeout');
	     clearTimeout(t); 
	 }).mouseleave(function() {
		 $('.popover').hide();}
	 );
 }
</script>