<%@ tag pageEncoding="UTF-8"%>
<%@ tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ tag import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ attribute name="tipoLocalListado" required="false" type="br.com.fanaticosporviagens.model.entity.LocalType" %> 
<%@ attribute name="localGeografico" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="locais" required="true" type="java.util.List" %> 
<%@ attribute name="mostrarLinkVerTodos" required="true" type="java.lang.Boolean" %> 
<%@ attribute name="linkVerTodos" required="false" type="java.lang.String" %>
<%@ attribute name="titulo" required="false" type="java.lang.String" %> 
<%@ attribute name="quantidade" required="false" type="java.lang.Integer"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:if test="${not empty locais}">
	<div class="span12" style="margin-left: 0px;">
		<div class="page-header">
			<h3><c:choose><c:when test="${not empty titulo}">${titulo}</c:when><c:when test="${not empty tipoLocalListado}">${tipoLocalListado.tituloListaLocais}</c:when></c:choose>
			<c:if test="${quantidade != null && quantidade > 0}">
    			<span class="badge badge-info" style="margin-top: 10px;">${quantidade}</span>
			</c:if>
			<c:if test="${mostrarLinkVerTodos}">
	            <div class="pull-right">
				  <a href="<c:url value="/locais/${localGeografico.urlPath}/${linkVerTodos}" />" class="listarTodosLocaisEnderecaveis seeMore">Ver Todos</a>
	            </div>
			</c:if>
			</h3>
		</div>
		
		<c:forEach items="${locais}" var="localLista" varStatus="status">
			<fan:card type="local" local="${localLista}" name="${localLista.nome}" imgSize="3" spanSize="6" imgStyle="max-width: 100%;" panoramioWidth="160">
				 <jsp:attribute name="subtitle">
				 	<c:if test="${localLista.quantidadeAvaliacoes != 0}">
	                    <p>
	                     <c:choose>
                        	<c:when test="${localLista.avaliacaoConsolidadaGeral.quantidadeAvaliacoes == 1}"><c:set var="textoAvaliacoes" value="${localLista.avaliacaoConsolidadaGeral.quantidadeAvaliacoes} avaliação" /></c:when>
                        	<c:otherwise><c:set var="textoAvaliacoes" value="${localLista.avaliacaoConsolidadaGeral.quantidadeAvaliacoes} avaliações" /></c:otherwise>
                    	</c:choose>
                    	<span title="Classificado como <strong>${localLista.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}</strong> em um total de ${textoAvaliacoes}" class="starsCategory cat${localLista.avaliacaoConsolidadaGeral.classificacaoGeral.codigo}" style="display: inline-block; float: left"></span>
	                    </p>
                    </c:if>
                    <c:if test="${localLista.quantidadeInteressesJaFoi != 0}">
						 <br/><span class="label label-info">${localLista.quantidadeInteressesJaFoi} viajantes já foram</span>
					</c:if>
                  </jsp:attribute>
                  <jsp:attribute name="info">
                  </jsp:attribute>
                  <jsp:attribute name="actions">
                        <div style="border-bottom: 1px solid #eee; height: 2px;"></div>
                        <div class="pull-right" style="padding-right: 6px;">
                            <sec:authorize access="@securityService.isAdmin()">
                                 <fan:addToPlanButton local="${localLista}" planosViagem="${planosViagem}" style="margin: 6px 0 0 15px;" />
                            </sec:authorize>

                            <div class="pull-right buttonToolBar btn-toolbar" style="margin-bottom: 0px;">
                                <div class="btn-group">
                                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" cssButtonClassSize="mini" />
                                    <sec:authorize access="!@securityService.isAdmin()">
                                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" cssButtonClassSize="mini" />
                                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" cssButtonClassSize="mini" />
                                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" cssButtonClassSize="mini" />
                                    </sec:authorize>
                                </div>
                            </div>
                        </div>
                  </jsp:attribute>
			</fan:card>
		</c:forEach>

	</div>
    
    <c:if test="${not unsuportedIEVersion}">
        <% String randomId = RandomStringUtils.random(12, true, true); %>
       	<div id="terms-of-service-<%=randomId%>"></div>
    	<script>
               var terms_widget = new panoramio.TermsOfServiceWidget(
                       'terms-of-service-<%=randomId%>', {'width': '100%'});
    	</script>
    </c:if>
	
</c:if>
