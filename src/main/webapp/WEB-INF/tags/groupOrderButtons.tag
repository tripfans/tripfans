<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.infra.component.SearchData"%>
<%@ attribute name="cssClass" required="false" type="String" description="Classe CSS para ser adicionada ao botao" %>
<%@ attribute name="id" required="false" type="String" description="Identificador HTML do componente." %>
<%@ attribute name="targetId" required="true" type="String" description="Identificador HTML do elemento onde a ordenacao sera feita." %>
<%@ attribute name="formId" required="false" type="String" description="Identificador do HTML Form que possui os filtros da consulta (se for o caso)." %>
<%@ attribute name="url" required="true" type="java.lang.String" description="A URL a ser chamada para realizar a ordenacao." %>
<%@ attribute name="selectedOrder" required="false" type="java.lang.String" description="A ordem que aparecerá selecionada" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% jspContext.setAttribute("urlOrdenacao", url, PageContext.REQUEST_SCOPE); %>
<% jspContext.setAttribute("selectedOrder", selectedOrder, PageContext.REQUEST_SCOPE); %>

<c:set var="thisId" value="botoesOrdenacao" />
<c:if test="${id != null}">
<c:set var="thisId" value="${id}" />
</c:if>

<div id="${thisId}" class="btn-group ${cssClass}" data-toggle="buttons-radio" style="display: inline;">
	<span style="float: left; margin-right: 5px; font-weight: bold;">Ordernar por:</span>
	<jsp:doBody />
</div>


<script type="text/javascript">
$('button.orderBtn').click(function(event){
	event.preventDefault();
	var $button = $(this);
	if($button.attr('class').indexOf('active') === -1) {
	    $button.button('loading');
        <c:if test="${not empty formId}">
        $('#sortField').remove();
        $('#dirField').remove();
        $('#${formId}').append($('<input/>',{type:'hidden', name: 'sort', id:'sortField', value:$button.attr('orderby')}));
        $('#${formId}').append($('<input/>',{type:'hidden', name: 'dir', id:'dirField', value:$button.attr('dir')}));
        $.get($('#${formId}').attr('action'), $('#${formId}').serialize(), function(response) {
            $('#${targetId}').html(response);
            $button.button('reset');
        });
		</c:if>
        <c:if test="${empty formId}">
		  $.get($button.attr('url'),{sort: $button.attr('orderby'), dir: $button.attr('dir'), start: 0},function(response) {
			 $('#${targetId}').html(response);
			 $button.button('reset');
			 $('#sortField').remove();
			 $('#dirField').remove();
			 $('<input/>',{type:'hidden',id:'sortField',value:$button.attr('orderby')}).appendTo('body');
			 $('<input/>',{type:'hidden',id:'dirField',value:$button.attr('dir')}).appendTo('body');
		  });
		</c:if>
	}
});
</script>