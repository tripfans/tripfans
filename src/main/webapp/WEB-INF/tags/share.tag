<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="facebook" required="false" type="java.lang.Boolean" %>
<%@ attribute name="includeFacebookApi" required="false" type="java.lang.Boolean" %>
<%@ attribute name="google" required="false" type="java.lang.Boolean" %>
<%@ attribute name="includeGoogleApi" required="false" type="java.lang.Boolean" %>
<%@ attribute name="twitter" required="false" type="java.lang.Boolean" %>
<%@ attribute name="includeTwitterApi" required="false" type="java.lang.Boolean" %>
<%@ attribute name="showComments" required="false" type="java.lang.Boolean" %>
<%@ attribute name="commentsAnchor" required="false" type="java.lang.String" %>

<c:if test="${tripFansEnviroment.enableSocialNetworkActions}">

    <style>
    
    /*div.social-buttons ul li {
        display: inline-block;
    }
    
    div.social-buttons ul {
        height: 21px;
        width: 360px;
        padding: 0px;
        margin: 0px;
        list-style: none outside none;
        list-style-type: none;
    }*/
    
    ul.social-buttons li.facebook {
        width: 130px;
        height: 21px;
        margin: 0px;
        margin-bottom: 5px;
    }
    
    ul.social-buttons li.google {
        width: 80px;
        height: 21px;
        margin: 0px;
    }
    
    ul.social-buttons li.twitter {
        width: 110px;
        height: 21px;
        margin: 0px;
    }
    
    ul.social-buttons li.comments {
        width: 78px;
        height: 21px;
        margin: 0px;
        padding-left: 20px;
        padding-top: 1px;
    }
    
    </style>
    
    <c:if test="${facebook == null}">
        <c:set var="facebook" value="true" />
    </c:if>
    <c:if test="${google == null}">
        <c:set var="google" value="true" />
    </c:if>
    <c:if test="${twitter == null}">
        <c:set var="twitter" value="true" />
    </c:if>
    <c:if test="${includeFacebookApi == null}">
        <c:set var="includeFacebookApi" value="true" />
    </c:if>
    <c:if test="${includeGoogleApi == null}">
        <c:set var="includeGoogleApi" value="true" />
    </c:if>
    <c:if test="${includeTwitterApi == null}">
        <c:set var="includeTwitterApi" value="true" />
    </c:if>
    
    <div class="row" style="max-height: 23px;">
        
        <ul class="social-buttons" style="list-style-type: none;">
          <c:if test="${facebook}">
            <li class="facebook pull-right">
                <div class="fb-like" data-send="false" data-layout="button_count" data-width="450" data-show-faces="true" data-action="recommend"></div>
                <c:if test="${includeFacebookApi}">
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>  
                </c:if>            
            </li>
          </c:if>
          <c:if test="${google}">
            <li class="google pull-right">
                <div class="g-plusone" data-size="medium" data-annotation="bubble"></div>
                <c:if test="${includeGoogleApi}">
                    <!-- Place this tag after the last +1 button tag. -->
                    <script type="text/javascript">
                      window.___gcfg = {lang: 'pt-BR'};
                    
                      (function() {
                        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                        po.src = 'https://apis.google.com/js/plusone.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                      })();
                    </script>
                </c:if>            
            </li>
          </c:if>
          <c:if test="${twitter}">
            <li class="twitter pull-right">
                <a href="https://twitter.com/share" class="twitter-share-button" data-via="tripfans" data-lang="pt">Tweetar</a>                    
                <c:if test="${includeTwitterApi}">
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </c:if>
            </li>
          </c:if>
          <c:if test="${showComments}">
            <li class="comments pull-right">
              <a href="#${commentsAnchor}">
                Comentários
              </a>
            </li>
          </c:if>
        </ul>
        
    </div>

</c:if>
        