<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ attribute name="id" required="true" type="java.lang.String" description="Id do botão" %>
<%@ attribute name="user" required="true" type="java.lang.String" description="Nome (urlPath) do usuário que terá suas notificações exibidas" %>
<%@ attribute name="notifications" required="true" type="java.lang.Long" description="Quantidade de notificações presentes" %>
<%@ attribute name="useButton" required="false" type="java.lang.Boolean" description="Usar botao ou icone" %>

<c:set var="buttonClass" value="btn-info"></c:set>
<c:if test="${notificacoesNaoVisualizadas != null}">
    <c:set var="buttonClass" value="btn-danger"></c:set>
</c:if>

<c:if test="${empty useButton}">
    <c:set var="useButton" value="true" />
</c:if>
<c:if test="${isMobile}">
    <c:set var="useButton" value="false" />
</c:if>

<style>
.header-menu-icon-inactive {
    color: rgba(21, 62, 90, 0.35)
}

.header-menu-icon-inactive:hover {
    color: white
}

.btnNotificacoes {
    cursor: pointer;
}

</style>

<c:if test="${useButton}">
  <button class="btn btn-small btnNotificacoes ${buttonClass}" data-toggle="button" data-load="<c:url value="/perfil/mostrarNotificacoesRecentes/${user}?limit=6" />" id="${id}">
    <i class="icon-exclamation-sign icon-white glyphicon glyphicon-bell white"></i>
    <c:if test="${notificacoesNaoVisualizadas != null}">
        <b><span id="qtd-notificacoes" style="font-size: 15px;" class="qtd-notificacoes">${notifications}</span></b>
    </c:if>
  </button>
</c:if>

<c:if test="${not useButton}">
    <span id="${id}" class="btnNotificacoes glyphicon glyphicon-bell white btn-lg ${notifications > 0 ? '' : 'header-menu-icon-inactive'}" 
          data-load="<c:url value="/perfil/mostrarNotificacoesRecentes/${user}?limit=6"/>" 
          data-toggle="tooltip" data-placement="bottom" title="${notifications} ${notifications == 1 ? 'Notificação não lida' : 'Notificações não lidas'} ">
      <c:if test="${notifications > 0}">
        <h6 style="height: 0px; margin: 0px;">
            <span id="qtd-notificacoes" class="label label-danger" style="position: relative; top: -26px; left: -12px;">${notifications}</span>
        </h6>
      </c:if>
    </span>
</c:if>

<script type="text/javascript">

$(document).ready(function () {
	
    <c:if test="${useButton}">
      $('#${id}').button();
    </c:if>

    $('#${id}').popover({
        animation: true,
        placement: 'bottom',
    });
    
    /*$(document).click(function (e) {
        alert('doc');
    })*/
    
    var hideEvent = 'click.popover';
    if ('ontouchstart' in document.documentElement) {
        hideEvent = 'touchstart.popover';
    }
    $('html').unbind(hideEvent).bind(hideEvent, function(event) {
        if ($('#btnNotificacoes').hasClass('active')) {
            $('#btnNotificacoes').removeClass('active').addClass('header-menu-icon-inactive');
            $('.btnNotificacoes').popover('hide');
        }
        
        if ($('#iconeNotificacoes').hasClass('active')) {
            $('#iconeNotificacoes').removeClass('active').addClass('header-menu-icon-inactive');
            $('.btnNotificacoes').popover('hide');
        }
    });
    
    $('#${id}').click( function(event) {
    	console.log('1')
        event.preventDefault();
        event.stopPropagation();
    	console.log('2')
        var $botao = $(this);
        if ($botao.is('.active')) {
        	console.log('3')
            $botao.removeClass('active');
            $botao.addClass('header-menu-icon-inactive');
            $('.btnNotificacoes').popover('hide');
            <c:if test="${useButton}">
              $botao.button('toggle');
              $botao.removeClass('btn-danger');
              $botao.addClass('btn-info');
            </c:if>
        } else {
        	console.log('4')
            <c:if test="${useButton}">
              $botao.popover('ajax', $botao.attr('data-load')).popover('title', 'Notificações').popover('show');
              $botao.button('toggle');
            </c:if>
            <c:if test="${not useButton}">
        	console.log('5')
              if (!$botao.hasClass('active')) {
              	console.log('6')
                  $botao.removeClass('header-menu-icon-inactive');
                  $botao.addClass('active');
                  $('#nav-menu-opcoes').removeClass('open');
                	console.log('7')
                  $botao.popover('ajax', $botao.attr('data-load')).popover('title', 'Notificações').popover('show');
                  	console.log('8')
              } else {
                  $botao.removeClass('active');
                	console.log('9')
                  $botao.addClass('header-menu-icon-inactive');
              }
              $('#qtd-notificacoes').hide();
          	console.log('10')
            </c:if>
        }
    });
});
</script>