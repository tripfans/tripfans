<%@tag import="org.joda.time.LocalDateTime"%>
<%@tag import="org.joda.time.format.DateTimeFormatterBuilder"%>
<%@tag pageEncoding="UTF-8"%>

<%@tag import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@tag import="org.joda.time.format.DateTimeFormat"%>
<%@ tag import="org.joda.time.format.DateTimeFormatter"%>
<%@ tag import="java.util.Locale"%>
<%@ tag import="java.util.Date"%>
<%@ tag import="org.joda.time.Period"%>
<%@ tag import="org.joda.time.DateTime"%>
<%@ tag import="org.joda.time.Days"%>
<%@ tag import="org.joda.time.Hours"%>
<%@ tag import="org.joda.time.LocalDate"%>
<%@ tag body-content="empty" %>
<%@ attribute name="date" required="true" type="java.lang.Object" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%

	Locale locale = RequestContextUtils.getLocale(request);

    DateTime dataAlvo = null;
    DateTimeFormatter formatter = null;
    if (date instanceof java.util.Date) {
        dataAlvo = new DateTime((java.util.Date) date);        
    } else if (date instanceof org.joda.time.LocalDate) {
        dataAlvo = new DateTime(((org.joda.time.LocalDate) date).toDate());        
    } else if (date instanceof LocalDateTime) {
    	dataAlvo = new DateTime(((org.joda.time.LocalDateTime) date).toDate());
    }

    DateTime dataAtual = new DateTime();
    
    if (dataAlvo != null) {
    
        Period p = new Period(dataAlvo, dataAtual);
        int days = p.getDays();
        String dayOfWeek = dataAlvo.dayOfWeek().getAsText(locale);
        int hours = p.getHours();
        int minutes = p.getMinutes();
        boolean mostrarMesDia = false;
        boolean mostrarData = false;
        boolean mostrarHora = true;
        
        // se for no mesmo dia 
        if (dataAlvo.getDayOfYear() == dataAtual.getDayOfYear() && dataAtual.getYear() == dataAlvo.getYear()) {
        	mostrarHora = false;
            // se for na mesma hora
            if (hours == 0) {
            	// se for no mesmo minuto
                if (minutes == 0) {
                	out.print(" há menos de 1 minuto");
                } else if (minutes == 1) {
                	out.print(" há 1 minuto");
                } else {
                	out.print(" há " + minutes + " minutos");
                }
            } else if (hours == 1) {
            	out.print(" há 1 hora");
            } else {
            	out.print(" há " + hours + " horas");
            }
        } // se for no dia anterior 
        else if (dataAtual.getDayOfYear() - dataAlvo.getDayOfYear() == 1 && dataAtual.getYear() == dataAlvo.getYear()) {
        	out.print(" Ontem");
        } // se for até 4 dias antes - imprimir o dia da semana 
        else if (dataAtual.getDayOfYear() - dataAlvo.getDayOfYear() < 4 && dataAtual.getYear() == dataAlvo.getYear()) {
        	out.print(" " + dayOfWeek);
        } // se for o mesmo ano - imprimir apenas dia/mes
        else if (dataAtual.getYear() == dataAlvo.getYear()) {
            mostrarMesDia = true;
        } // caso contrario, imprimir a data completa  
        else {
            mostrarData = true;
        }
    
	%>

    <%
    DateTimeFormatterBuilder formatBuilder = new DateTimeFormatterBuilder();
    if (mostrarData) {
    	formatBuilder = formatBuilder.appendLiteral(" em ").appendDayOfMonth(2).appendLiteral(" de ").appendMonthOfYearText().appendLiteral(" de ").appendYear(4, 4);
    } else if (mostrarMesDia) {
    	formatBuilder = formatBuilder.appendLiteral(" em ").appendDayOfMonth(2).appendLiteral(" de ").appendMonthOfYearText();
    }
    if (mostrarHora) {
    	formatBuilder = formatBuilder.appendLiteral(" às ").appendHourOfDay(2).appendLiteral(":").appendMinuteOfHour(2);
    }
    if(formatBuilder.canBuildFormatter()) {
	    formatter = formatBuilder.toFormatter().withLocale(locale);
	    out.print(dataAlvo.toString(formatter));
    }
    %>

<% } %> 