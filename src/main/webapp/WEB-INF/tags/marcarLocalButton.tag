<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@tag import="org.apache.commons.lang.RandomStringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="tf" uri="/WEB-INF/tripfansFunctions.tld" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ attribute name="cssButtonClassSize" required="false" type="java.lang.String" description="large, small ou mini" %>
<%@ attribute name="isResponsive" required="false" type="java.lang.Boolean" %>
<%@ attribute name="usarBotoes" required="false" type="java.lang.Boolean" %>
<%@ attribute name="exibirLabel" required="false" type="java.lang.Boolean" %>
<%@ attribute name="marginLeftBotao" required="false" type="java.lang.String" %>
<%@ attribute name="marginRightBotao" required="false" type="java.lang.String" %>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" description="O local a ser marcado." %>
<%@ attribute name="listaMarcados" required="true" type="java.util.Collection" description="A lista de locais que o usuario ja foi." %>
<%@ attribute name="tipoMarcacao" required="true" type="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal" description="O tipo de interesse." %>

<c:if test="${empty cssButtonClassSize}"><c:set var="cssButtonClassSize" value="small" /></c:if>

<c:if test="${usarBotoes == null}"><c:set var="usarBotoes" value="true" /></c:if>
<c:if test="${marginLeftBotao == null}"><c:set var="marginLeftBotao" value="5px" /></c:if>
<c:if test="${marginRightBotao == null}"><c:set var="marginRightBotao" value="5px" /></c:if>

<s:eval expression="tipoMarcacao == T(br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal).DESEJA_IR" var="desejaIr" />
<s:eval expression="tipoMarcacao == T(br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal).FAVORITO" var="favorito" />
<s:eval expression="tipoMarcacao == T(br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal).JA_FOI" var="jaFoi" />
<s:eval expression="tipoMarcacao == T(br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal).SEGUIR" var="seguir" />

<c:choose>
	<c:when test="${desejaIr}">
        <c:set var="label" value="Desejo ir" />
		<c:set var="titleMarcar" value="Marque se deseja ir para ${local.nome}." />
		<c:set var="titleDesmarcar" value="Desmarcar que deseja ir para ${local.nome}." />
		<c:url var="urlMarcar" value="/locais/marcarDesejaIr/${local.urlPath}" />
		<c:url var="urlDesmarcar" value="/locais/desmarcarDesejaIr/${local.urlPath}" />
		<c:url var="img" value="/resources/images/markers/des-pin.png" />
        <c:set var="width" value="18" />
	</c:when>
	<c:when test="${favorito}">
        <c:set var="label" value="Favorito" />
		<c:set var="titleMarcar" value="Marque se ${local.nome} é um de seus locais favoritos." />
		<c:set var="titleDesmarcar" value="Desmarcar ${local.nome} como favorito." />
		<c:url var="urlMarcar" value="/locais/marcarComoFavorito/${local.urlPath}" />
		<c:url var="urlDesmarcar" value="/locais/desmarcarComoFavorito/${local.urlPath}" />
		<c:url var="img" value="/resources/images/markers/est-fav-pin.png" />
        <c:set var="width" value="18" />
	</c:when>
	<c:when test="${jaFoi}">
        <c:set var="label" value="Já fui" />
		<c:set var="titleMarcar" value="Marque se já foi para ${local.nome}." />
		<c:set var="titleDesmarcar" value="Desmarcar que já foi para ${local.nome}." />
		<c:url var="urlMarcar" value="/locais/marcarJaFoi/${local.urlPath}" />
		<c:url var="urlDesmarcar" value="/locais/desmarcarJaFoi/${local.urlPath}" />
		<c:url var="img" value="/resources/images/markers/est-pin.png" />
        <c:set var="width" value="18" />
	</c:when>
	<c:when test="${seguir}">
        <c:set var="label" value="Seguir" />
		<c:set var="titleMarcar" value="Marque se quer seguir ${local.nome}." />
		<c:set var="titleDesmarcar" value="Deixar de seguir ${local.nome}." />
		<c:url var="urlMarcar" value="/locais/marcarSeguir/${local.urlPath}" />
		<c:url var="urlDesmarcar" value="/locais/desmarcarSeguir/${local.urlPath}" />
		<c:url var="img" value="/resources/images/icons/gps_arrow.png" />
        <c:set var="width" value="16" />
	</c:when>
</c:choose>

<c:choose>
	<c:when test="${tf:contains(listaMarcados, local.id) == true}">
		<c:set var="activeClass" value="active" />
		<c:set var="url" value="${urlDesmarcar}" />
		<c:set var="title" value="${titleDesmarcar}" />
	</c:when>
	<c:otherwise>
		<c:set var="url" value="${urlMarcar}" />
		<c:set var="title" value="${titleMarcar}" />
	</c:otherwise>
</c:choose>

<c:if test="${usuario == null and not isResponsive}">
	<c:url var="url" value="/entrarPop" /> 
</c:if>

<c:set var="idButton" value="<%= RandomStringUtils.random(12, true, true) %>" />

<c:choose>
    <c:when test="${isResponsive}">
      <c:if test="${isMobile}">
          <a href="${url}" id="${idButton}" class="${activeClass} colorOnHover" title="${title}">
            <img src="${img}" height="16" width="${width}" class="${activeClass == 'active' ? '' : 'blackAndWhite colorOnHover'}" />
            <span style="font-weight: normal; font-size: 11px; color: ${activeClass == 'active' ? '#0088cc;' :  '#A8A8A8'}">${label}</span>
          </a>
      </c:if>
      <c:if test="${not isMobile}">
          <a href="${url}" id="${idButton}" class="${activeClass} ${activeClass == 'active' ? '' : 'blackAndWhite colorOnHover'}" title="${title}">
            <img src="${img}" height="16" width="${width}" />
            <span style="font-weight: normal; font-size: 11px; color: #0088cc;">${label}</span>
          </a>
      </c:if>
    </c:when>
    <c:otherwise>
        <c:if test="${usarBotoes}">
          <a href="${url}" id="${idButton}" class="btn btn-${cssButtonClassSize} ${activeClass} ${activeClass == 'active' ? '' : 'transparencia-75 blackAndWhite colorOnHover'}" title="${title}">
            <img src="${img}" height="16" width="${width}" />
            <span style="font-weight: normal; color: #0088cc;">${label}</span>
          </a>
        </c:if>
        <c:if test="${not usarBotoes}">
          <span class="${activeClass == 'active' ? '' : 'transparencia-75 blackAndWhite colorOnHover'}" style="margin-left: ${marginLeftBotao}; margin-right: ${marginRightBotao};">
              <a href="${url}" id="${idButton}" class="${activeClass}" title="${title}" style="text-decoration: none;">
                <img src="${img}" height="16" width="${width}" />
                <c:if test="${exibirLabel}">
                    <span style="font-size: 11px; color: #0088cc;">${label}</span>
                </c:if>
              </a>
          </span>
        </c:if>
    </c:otherwise>
</c:choose>

<script type="text/javascript">
	
	$(document).ready(function() {
		$("#${idButton}").click(function(event) {
			var button = $(this); 
			event.preventDefault();
<c:choose>
	<c:when test="${usuario != null}">
			$.post(button.attr("href"), function(data) {
				if (button.attr("class").indexOf("active") == -1) {
					button.attr("data-original-title", "${titleDesmarcar}");
					button.addClass("active");
					button.attr("href", "${urlDesmarcar}");
                    <c:if test="${usarBotoes}">
                    button.removeClass('transparencia-75');
                    button.removeClass('blackAndWhite');
                    button.removeClass('colorOnHover');
                    </c:if>
					<c:if test="${not usarBotoes}">
					button.parent().removeClass('transparencia-75');
					button.parent().removeClass('blackAndWhite');
					button.parent().removeClass('colorOnHover');
					</c:if>
				} else {
					button.attr("data-original-title", "${titleMarcar}");
					button.removeClass("active");
					button.attr("href", "${urlMarcar}");
                    <c:if test="${usarBotoes}">
                    button.addClass('transparencia-75');
                    button.addClass('blackAndWhite');
                    button.addClass('colorOnHover');
                    </c:if>
					<c:if test="${not usarBotoes}">
					button.parent().addClass('transparencia-75');
					button.parent().addClass('blackAndWhite');
					button.parent().addClass('colorOnHover');
					</c:if>
				}
			});
	</c:when>
	<c:otherwise>
	    <c:if test="${isResponsive}">
	      $('#modal-login-body').load('<c:url value="/entrarPop"/>?to=' + button.attr('href'), function() {
            $('#modal-login').modal('show');
          });
	    </c:if>
	    <c:if test="${not isResponsive}">
			$(".login-modal-link").trigger('click');
	    </c:if>			
	</c:otherwise>
</c:choose>
		});
	});
	
</script>