<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="listaAmigos" required="true" type="java.util.List" description="Lista de amigos que deve ser exibida" %>
<%@ attribute name="itemSize" required="false" type="java.lang.String" description="Classe CSS para tamanho da exibição de cada item" %>

				
<c:forEach items="${listaAmigos}" var="interesse">
	<div class="${itemSize}" >
		<div class="row">
		<fan:userAvatar imgStyle="height: 100px; width: 100px;" showFacebookMark="false" user="${interesse.usuario}" displayName="false" displayDetails="true" displayNameTooltip="true"  imgSize="${itemSize}" />
		</div>
		<c:if test="${tripFansEnviroment.enabledPedidoDica == true}">
			<div class="${itemSize}" style="text-align: center; margin-left: 0px; padding: 10px;">
			<a data-ativo="${interesse.usuario.enabled}" data-from="${usuario.idFacebook}" 
				data-to="${interesse.usuario.idFacebook}" data-usuario-destinatario="${interesse.usuario.id}" 
	                       data-name="${usuario.displayName} está pedindo uma dica de viagem pra você no TripFans sobre ${local.nome}" 
	                       data-description="Clique aqui para enviar dicas sobre ${local.nome} para ${usuario.displayName}" 
	                       data-link="${tripFansEnviroment.serverUrl}/entrar?ucv=${sessionScope.idUsuarioCriptografado}&local=${local.urlPath}&responderPedidoDica=true" 
	                       data-picture="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans-vertical.png"  
	                       href="<c:url value="/dicas/pedidoDica/formPedirDica/${local.id}?usuario=${interesse.usuario.id}" />" 
	                       class="btn btn-secondary btn-small btn-pedir-dica" title="Peça uma dica sobre ${local.nome} a ${interesse.usuario.displayName}">
	                       Peça uma dica
	                     </a>
			</div>
		</c:if>
	</div>
</c:forEach>
           	            
<script type="text/javascript">
	$('.btn-pedir-dica').click(function(e) {
		e.preventDefault();
		var btn=$(this);
		var ativo = btn.data('ativo');
	
		if(ativo == true) {
	 		$.get(btn.attr('href'), function(data) {
	 			var $modal = $(data).filter('.modal');
	 			var $script = $(data).filter('script'); 
	 			$('body').remove('#' + $modal.attr('id'));
	 			$('body').remove('#' + $script.attr('id'));
	 	        $('body').append($modal);
	 	        $('body').append($script);
	 	        $modal.modal();
 			});
		} else {
			var from = btn.attr('data-from');
		    var to = btn.attr('data-to');
		    var usuarioDestinatario = btn.attr('data-usuario-destinatario');
		    var name = btn.attr('data-name');
		    var description = btn.attr('data-description');
		    var link = btn.attr('data-link');
		    var picture = btn.attr('data-picture');
		    
		    var obj = {
	            app_id:'${tripFansEnviroment.facebookClientId}',
	            method: 'feed',
	            link: link,
	            from : from,
	            to : to,
	            picture: picture,
	            name: name,
	            description: description
	        };

	        function callback(response) {
	            if(response !== null && response.post_id !== null) {
	            	console.log(response)
	            	// registrar pedido no TripFans
	            	$.post('<c:url value="/dicas/pedidoDica/pedirDica" />', 
	            			{'quemPedir' : usuarioDestinatario, 'idPostFacebook': response.post_id, 'localDica' : '${local.id}'}, 
            			function(response) {}
	            	);
	            }
	        }
	        FB.ui(obj, callback);
		}
	});
</script>
