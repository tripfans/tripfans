<%@tag pageEncoding="UTF-8"%>

<%@ tag body-content="empty" %>
<%@ attribute name="value" required="true" type="java.lang.Integer"%>
<%@ attribute name="badgeType" required="false" type="java.lang.String" description="info, success, warning, etc."%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${value != null && value > 0}">
    <span class="badge ${not empty badgeType ? 'badge-' : ''}${badgeType} right">${value}</span>
</c:if>