<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@ tag body-content="empty" %>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" description="Local" %>
<%@ attribute name="nomeLocal" required="true" type="java.lang.String" description="Nome do local" %>
<%@ attribute name="mostrarUploadFotos" required="false" type="java.lang.Boolean" description="Se deve exibir a seção de upload de fotos" %>
<%@ attribute name="quantidadeFotos" required="false" type="java.lang.Boolean" description="Quantidade uploads de fotos permitido" %>
<%@ attribute name="mainLabelSpanClass" required="false" type="java.lang.String" description="ccs class dos label dos campos check" %>
<%@ attribute name="labelChkSpanClass" required="false" type="java.lang.String" description="ccs class dos label dos campos check" %>
<%@ attribute name="fieldSpanClass" required="false" type="java.lang.String" description="css class dos campos" %>
<%@ attribute name="backgroundColor" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<c:if test="${mostrarUploadFotos == null}"><c:set var="mostrarUploadFotos" value="true" /></c:if>
<c:if test="${labelSpanClass == null}"><c:set var="labelSpanClass" value="span3" /></c:if>
<c:if test="${fieldSpanClass == null}"><c:set var="fieldSpanClass" value="span8" /></c:if>
<c:if test="${quantidadeFotos == null}"><c:set var="quantidadeFotos" value="4" /></c:if>

	<div id="errorBox" style="display: none;" class="alert alert-error centerAligned"> 
		<p id="errorMsg">
            <s:message code="validation.containErrors" />
		</p>
	</div>
	
    <input type="hidden" name="localDica" value="${local.id}" />
	<div>
	  <fieldset  style="background-color: ${backgroundColor}">
		<legend>
            Sua dica
            <c:if test="${not empty nomeLocal}">
                sobre ${nomeLocal}
            </c:if>            
        </legend>
		 
		<ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
    		<li>
    			<label for="dica-textarea" class="fancy-text ${mainLabelSpanClass}">
    				<span class="required">
	        			<s:message code="dica.label.descricao" />
	                    <s:message code="dica.tooltip.descricao" var="tooltipDescricao" />
	                    <i class="icon-question-sign helper glyphicon glyphicon-question-sign" title="${tooltipDescricao}"></i>
                    </span>
    			</label>
    			<textarea class="${fieldSpanClass}" id="dica-textarea" name="descricao" rows="3">${dica.descricao}</textarea>
    		</li>
		</ul>
	  </fieldset>
	</div>
	
		<fieldset style="background-color: ${backgroundColor}">
            <legend>Essa dica tem a ver com</legend>
			<div class="row">
			<c:forEach var="item" items="${itensClassificacao}" varStatus="status" begin="0">
				<div class="checkbox ${labelChkSpanClass}" style="padding: 0px; margin-bottom: 0px;">
				    <label>
					    <input type="checkbox" value="${item.id}" name="itensClassificacao[${status.count - 1}].item" />
					    <span style="font-size: 15px;">${item.descricao}</span>
				    </label>
				</div>
			</c:forEach>
			</div>
		</fieldset>

    <c:if test="${mostrarUploadFotos}">
        <fieldset style="background-color: ${backgroundColor}">
            <legend>
                <s:message code="avaliacao.legend.fotos" />
            </legend>
            <ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
     		  <c:forEach var="i" begin="1" end="${quantidadeFotos}" step="1">
    		 	<li>
    				<label for="fotos${i}.arquivoFoto" class="fancy-text ${labelSpanClass}">
                        <s:message code="avaliacao.label.foto" />
                    </label>
    				<s:message code="avaliacao.tooltip.foto"  var="tooltipFoto"/>
    				<s:message code="avaliacao.tooltip.descricaoFoto" var="tooltipDescricao" />
    				<input id="fotos${i}.arquivoFoto"  name="fotos[${i}].arquivoFoto" type="file"  onchange="enableFields('descricaoFoto_${i}');">
    			</li>
    			<li>
    				<label for="descricaoFoto_${i}" class="fancy-text ${labelSpanClass}">
                        <s:message code="avaliacao.label.descricaoFoto" />
                    </label>  
    				<input class="${fieldSpanClass}" disabled="true" id="descricaoFoto_${i}" name="fotos[${i}].descricao"  />
    			</li>
    		  </c:forEach>
    		</ul>
    	</fieldset>
    </c:if>
	
	<c:if test="${local.tipoLocalEnderecavel}">
		<div class="control-group alert alert-block" style="font-size: 14px;">
		    <input type="checkbox"  id="dicaSegura" name="dicaSegura"/>
		    <span class="required"> 
	   	        <s:message code="dica.termoDeCompromisso" />
	    	</span>
		</div>
	</c:if>
	
	
	<div class="form-actions rightAligned">
        <s:message code="dica.botaoSalvar" var="labelBotaoSalvar" />
        <input type="submit" id="botao-salvar-dica" class="btn btn-primary btn-large" data-loading-text="Aguarde..." value="${labelBotaoSalvar}" >
        <input type="button" id="botao-salvar-escrever-mais" class="btn btn-success btn-large" data-loading-text="Aguarde..." title="Clique para salvar e escrever outra dica sobre ${local.nome}" value="Enviar Dica e Escrever Outra" >
        <input type="button" id="botao-cancelar_dica" class="btn btn-large" value="Cancelar" >
	</div>
