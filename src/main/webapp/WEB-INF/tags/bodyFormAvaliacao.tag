<%@tag pageEncoding="UTF-8"%>

<%@tag import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@tag import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@ tag body-content="empty" %>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" description="Local a ser avaliado" %>
<%@ attribute name="mostraQuandoFoi" required="false" type="java.lang.Boolean" description="Se deve exibir quando foi" %>
<%@ attribute name="desabilitarQuandoFoi" required="false" type="java.lang.Boolean" description="Se deve desabilitar os campos de quando foi" %>
<%@ attribute name="mesVisita" required="false" type="br.com.fanaticosporviagens.model.entity.Mes" description="Mes da visita" %>
<%@ attribute name="anoVisita" required="false" type="java.lang.Integer" description="Ano da visita" %>
<%@ attribute name="mostrarUploadFotos" required="false" type="java.lang.Boolean" description="Se deve exibir a seção de upload de fotos" %>
<%@ attribute name="quantidadeFotos" required="false" type="java.lang.Boolean" description="Quantidade uploads de fotos permitido" %>
<%@ attribute name="labelSpanClass" required="false" type="java.lang.String" description="ccs class dos label dos campos" %>
<%@ attribute name="fieldSpanClass" required="false" type="java.lang.String" description="css class dos campos" %>
<%@ attribute name="ratySuffix" required="false" type="java.lang.String" description="Sufixo para o rating. Importante se há diversos forms na página." %>
<%@ attribute name="criteriosAvaliacao" required="true" type="java.util.List" description="Critérios de avaliação desse local." %>
<%@ attribute name="backgroundColor" required="false" type="java.lang.String" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${mostraQuandoFoi == null}"><c:set var="mostraQuandoFoi" value="true" /></c:if>
<c:if test="${desabilitarQuandoFoi == null}"><c:set var="desabilitarQuandoFoi" value="false" /></c:if>
<c:if test="${mesVisita == null}"><c:set var="mesVisita" value="${avaliacao.mesVisita}" /></c:if>
<c:if test="${anoVisita == null}"><c:set var="anoVisita" value="${avaliacao.anoVisita}" /></c:if>
<c:if test="${mostrarUploadFotos == null}"><c:set var="mostrarUploadFotos" value="true" /></c:if>
<c:if test="${labelSpanClass == null}"><c:set var="labelSpanClass" value="span4" /></c:if>
<c:if test="${fieldSpanClass == null}"><c:set var="fieldSpanClass" value="span7" /></c:if>
<c:if test="${quantidadeFotos == null}"><c:set var="quantidadeFotos" value="4" /></c:if>

<script type="text/javascript">
var avaliacoesQualidade = new Array();
<c:forEach var="qualidade" items="${avaliacoesQualidade}">
	avaliacoesQualidade.push({'codigo':'${qualidade.codigo}', 'enumName' : '${qualidade}'});
</c:forEach>
</script>

	<div id="errorBox" style="display: none;" class="alert alert-error centerAligned"> 
		 <p id="errorMsg">
		<s:message code="validation.containErrors" />
		</p>
	</div>
	
	<input type="hidden" name="localAvaliado" value="${local.id}" />
	<div>
	  <fieldset style="background-color: ${backgroundColor}">
		<legend>Sua avaliação</legend>
		<s:message var="textoCamposObrigatorios" code="formulario.camposObrigatorios" />
		<fan:helpText text="${textoCamposObrigatorios}"  showQuestion="false" showCloseButton="false"/>
		 
		<ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
			<li style="min-height: 36px;">
    			<label for="classificacaoGeral" class="fancy-text ${labelSpanClass}">
            		<span class="required">
                        Sua nota para este lugar
                        <i class="icon-question-sign helper" title="Dê a sua nota para a sua experiência neste lugar."></i>
            		</span>
    			</label>
    			
                <div id="rating-classificacaoGeral"  class="span4" style="margin-left: 0px; padding-top: 4px;"></div>
                <div id="target-classificacaoGeral" style="padding-top: 6px;"></div>
                 <input type="hidden" name="classificacaoGeral" class="required" />
    		</li>
    		
    		  <script type="text/javascript">
                    $(function() {
                    	
                        $('#rating-classificacaoGeral').raty({
							cancel: false,
							target     : '#target-classificacaoGeral',
							targetText: '<span class="label label-info">Clique ao lado para classificar</span>',
							targetKeep : true,
							path: '<c:url value="/resources/images/" />',
							starOn  : 'tripfans-small.png',
							starOff : 'tripfans-small-gray.png',
							size: 24,
							hints: [
							       <c:forEach var="qualidade" items="${avaliacoesQualidade}">
							         '${qualidade.descricao}',
							       </c:forEach>
							],
							click: function(score, event) {
								var selecionado = '';
								$.each(avaliacoesQualidade, function(index, aq) {
									if(aq.codigo === score) {
										selecionado = aq.enumName;
										return false;
									}
								});
								if(selecionado !== '') {
									$('input[name="classificacaoGeral"]').val(selecionado);
								} else {
									$('input[name="classificacaoGeral"]').removeAttr('value');
								}
							}
                    	});
                    });
                </script>
		
    		<li>
    			<label for="tituloAvaliacao" class="fancy-text ${labelSpanClass}">
            		<span class="required">
                        <s:message code="avaliacao.label.titulo" />
                        <s:message code="avaliacao.tooltip.titulo" var="tooltipTitulo" />
                        <i class="icon-question-sign helper" title="${tooltipTitulo}"></i>
            		</span>
    			</label>
    			<input id="tituloAvaliacao" name="titulo" value="${avaliacao.titulo}" class="${fieldSpanClass}" />
    		</li>
    		<li style="${mostraQuandoFoi == true ? '' : 'display : none;'}">
    			<label class="fancy-text ${labelSpanClass}">
        			<span  class="required">
            			<s:message code="avaliacao.label.dataVisita" />
            			<i class="icon-question-sign helper" title="Informe o mês e ano de sua visita"></i>
        			</span>
    			</label> 
                <div class="span3" style="margin-left: 0px;">
    			  <select name="mesVisita" id="mesVisita" class="body-avaliacao-fancy span3" ${desabilitarQuandoFoi ? 'disabled="disabled"' : ''}>
    				<c:forEach var="mes" items="<%= Mes.getMesesOrdenados() %>">
    					<c:choose>
    					   <c:when test="${mes == mesVisita}">
                               <c:set var="selected" value="selected = 'selected'" />
                           </c:when>
                           <c:otherwise>
                               <c:set var="selected" value="" />
                           </c:otherwise>
    					</c:choose>
    					<option value="${mes}" label="${mes.descricao}" ${selected}>${mes.descricao}</option>
    				</c:forEach>
    			  </select>
                  <c:if test="${desabilitarQuandoFoi}">
                    <input type="hidden" name="mesVisita" value="${mesVisita}" />
                  </c:if>
                </div>
                <div class="span5">
    			  <select name="anoVisita" id="anoVisita" class="body-avaliacao-fancy span2" ${desabilitarQuandoFoi ? 'disabled="disabled"' : ''}>
    				<c:forEach var="ano" items="${anosVisita}">
    					<c:choose>
    					   <c:when test="${ano == anoVisita}">
                               <c:set var="selected" value="selected = 'selected'" />
                           </c:when>
                           <c:otherwise>
                               <c:set var="selected" value="" />
                           </c:otherwise>
    					</c:choose>
    					<option value="${ano}" label="${ano}" ${selected}>${ano}</option>
    				</c:forEach>
    			  </select>
                  <c:if test="${desabilitarQuandoFoi}">
                    <input type="hidden" name="anoVisita" value="${anoVisita}" />
                  </c:if>
                </div>
    		</li>
    		<li>
    			<label for="textarea" class="fancy-text ${labelSpanClass}">
    				<span  class="required">
        			<s:message code="avaliacao.label.descricao" />
        			<i class="icon-question-sign helper" title="Coloque um texto que descreva sua experiência nesse local, relatando aspectos positivos e/ou negativos, se houver."></i>
        			</span>
    			</label>
    			<textarea class="${fieldSpanClass}" id="textarea" name="descricao" rows="6">${avaliacao.descricao}</textarea>
    		</li>
		</ul>
	  </fieldset>
	</div>

	<fieldset style="background-color: ${backgroundColor}">
        <legend>
            <s:message code="avaliacao.legend.classificacaoItens" />
        </legend>
        
        <s:message var="textoCamposOpcionais" code="avaliacao.textoExplicativoCamposOpcionais" />
        <fan:helpText text="${textoCamposOpcionais}"  showQuestion="false" showCloseButton="false"/>
        
		<ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
            <c:set var="avaliacoesClassificacaoAplicaveis" value="${avaliacao.avaliacoesClassificacaoAplicaveis}" />
			<c:forEach var="criterio" items="${criteriosAvaliacao}">
				<c:set var="score"  value="-1" />
                <c:choose>
	                <c:when test="${not empty avaliacoesClassificacaoAplicaveis}" >
	                	<c:forEach var="avaliacaoAplicavel" items="${avaliacoesClassificacaoAplicaveis}">
	                		<c:if test="${avaliacaoAplicavel.criterio.codigo == criterio.codigo}">
		                		<input type="hidden" name="${criterio.nomeAtributo}" value="${avaliacaoAplicavel.qualidade.codigo}" />
		                		<c:set var="score"  value="${avaliacaoAplicavel.qualidade.codigo}" />
	    	            	</c:if>
	                	</c:forEach>
	                </c:when>
	                <c:otherwise>
	                	<input type="hidden" name="${criterio.nomeAtributo}" value="<%= AvaliacaoQualidade.NAO_SE_APLICA %>" />
	                </c:otherwise>
                </c:choose>
                
				<li style="min-height: 36px;">
                  <label for="classificacaoServicos" class="fancy-text ${labelSpanClass}">${criterio.descricao}</label>
                  <div id="rating-${criterio.codigo}${ratySuffix}" class="span4" style="padding-top: 4px;"></div>
                  <div id="target-${criterio.codigo}${ratySuffix}" style="padding-top: 6px;"></div>
                </li>

                <script type="text/javascript">
                    $(function() {
                    	
                        $('#rating-${criterio.codigo}${ratySuffix}').raty({
							cancel: true,
							cancelHint: 'Cancele sua classificação para esse item',
							target     : '#target-${criterio.codigo}${ratySuffix}',
							targetText: '<span class="label label-info">Clique ao lado para classificar</span>',
							targetKeep : true,
							<c:if test="${score != -1}">score: ${score},</c:if>
							path: '<c:url value="/resources/images/" />',
							starOn  : 'tripfans-small.png',
							starOff : 'tripfans-small-gray.png',
							size: 24,
							hints: [
							       <c:forEach var="qualidade" items="${avaliacoesQualidade}">
							         '${qualidade.descricao}',
							       </c:forEach>
							],
							click: function(score, event) {
								var selecionado = '';
								$.each(avaliacoesQualidade, function(index, aq) {
									if(aq.codigo === score) {
										selecionado = aq.enumName;
										return false;
									}
								});
								if(selecionado !== '') {
									$('input[name="${criterio.nomeAtributo}"]').val(selecionado);
								} else {
									$('input[name="${criterio.nomeAtributo}"]').val('<%= AvaliacaoQualidade.NAO_SE_APLICA %>');
								}
							}
                    	});
                    });
                </script>
			</c:forEach>
		</ul>
	</fieldset>
	
	<c:if test="${not empty itensBomPara}">
		<fieldset style="background-color: ${backgroundColor}">
	           <legend>Você aconselha este local para</legend>
				<c:forEach var="item" items="${itensBomPara}" varStatus="status" begin="0">
				<div class="offset2 ${labelSpanClass}">
				    <input type="checkbox" value="${item}" name="itensBomPara[${status.count - 1}].item" />
				    <span style="font-size: 15px;">${item.descricao}</span>
				</div>
			</c:forEach>
		</fieldset>
	</c:if>
	
	<fieldset style="background-color: ${backgroundColor}">
           <legend>Tipo de viagem que você fez</legend>
			<c:forEach var="item" items="${tiposViagem}" varStatus="status" begin="0">
			<div class="offset2 ${labelSpanClass}">
			    <input type="checkbox" value="${item.id}" name="tiposViagemRealizada[${status.count - 1}].tipo" />
			    <span style="font-size: 15px;">${item.descricao}</span>
			</div>
		</c:forEach>
	</fieldset>
    
    <c:if test="${mostrarUploadFotos}">
        <fieldset style="background-color: ${backgroundColor}">
            <legend><s:message code="avaliacao.legend.fotos" /></legend>
            
            <s:message var="textoTamMaximo" code="foto.texto.tamanhoMaximo" arguments="4" />
			<s:message var="textoResolucaoMaxima" code="foto.texto.resolucaoMaxima" arguments="2048" />
			<c:set var="textoFotosPublicas" value="<ul><li>As fotos postadas aqui são <strong>públicas e serão exibidas</strong> na página de ${local.nome}</li>"  />
			<c:set var="textoTamMaximo" value="<li>${textoTamMaximo}</li>"  />
			<c:set var="textoResolucaoMaxima" value="<li>${textoResolucaoMaxima}</li></ul>"  />
			<fan:helpText text="${textoFotosPublicas}${textoTamMaximo}${textoResolucaoMaxima}"  showQuestion="false" showCloseButton="false"/>
            
            <ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
     		  <c:forEach var="i" begin="0" end="${quantidadeFotos}" step="1">
    		 	<li>
    				<label for="fotos${i}.arquivoFoto" class="fancy-text ${labelSpanClass}">
                        <s:message code="avaliacao.label.foto" />
                    </label>
    				<s:message code="avaliacao.tooltip.foto"  var="tooltipFoto"/>
    				<s:message code="avaliacao.tooltip.descricaoFoto" var="tooltipDescricao" />
    				<input id="fotos${i}.arquivoFoto"  name="fotos[${i}].arquivoFoto" type="file"  onchange="enableFields('descricaoFoto_${i}');">
    			</li>
    			<li>
    				<label for="descricaoFoto_${i}" class="fancy-text ${labelSpanClass}">
                        <s:message code="avaliacao.label.descricaoFoto" />
                    </label>  
    				<input class="${fieldSpanClass}" disabled="true" id="descricaoFoto_${i}" name="fotos[${i}].descricao"  />
    			</li>
    		  </c:forEach>
    		</ul>
    	</fieldset>
    </c:if>
	
	<c:if test="${local.tipoLocalEnderecavel}">
		<div class="control-group alert alert-block">
		    <input type="checkbox"  id="avaliacaoSegura" name="avaliacaoSegura"/>
		    <span class="required"> 
	   	        <s:message code="avaliacao.termoDeCompromisso" />
	    	</span>
		</div>
	</c:if>
	
	<div class="form-actions rightAligned">
		<s:message code="avaliacao.botaoSalvar" var="labelBotaoSalvar" />
        <input type="submit" id="botao-salvar-avaliacao" class="btn btn-primary btn-large" data-loading-text="Aguarde..." value="${labelBotaoSalvar}" >
        <input type="button" id="botao-cancelar-avaliacao" class="btn btn-large" value="Cancelar" >
	</div>
    
<script type="text/javascript">
    $(function() {
        //$('.body-avaliacao-fancy').fancy();
    });
</script>    
    
