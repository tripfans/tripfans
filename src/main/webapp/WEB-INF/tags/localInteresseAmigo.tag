<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="interesseAmigo" required="true" type="br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal" %> 

<div class="row">
<c:forEach items="${interesseAmigo.amostraInteresses}" var="interesse" varStatus="status">
    <c:choose>
        <c:when test="${interesse.usuario != null}">
            <div class="span2"><fan:userAvatar user="${interesse.usuario}" displayDetails="true"/></div>
        </c:when>
	    <c:otherwise>
            <div class="span2"><fan:userFacebookAvatar id="${interesse.idUsuarioFacebook}" name="${interesse.nomeUsuario}" displayDetails="true"/></div>
        </c:otherwise>
    </c:choose>
	<c:if test="${status.count % 5 == 0 && not status.last}"></div><div class="row"></c:if>
</c:forEach>
</div>
<c:if test="${interesseAmigo.amostraMenorQueQuantidade}">
<div class="row pull-right"><a href="${pageContext.request.contextPath}/locais/interesseTodosAmigos/${interesseAmigo.tipo.urlPath}/${local.urlPath}" class="interesseTodosAmigos">Ver Todos os ${interesseAmigo.quantidadeAmigos} amigos</a></div>
</c:if>