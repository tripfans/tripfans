<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%@ attribute name="visoesViagem" required="true" type="java.util.Collection" %> 
<%@ attribute name="viagensPublicas" required="true" type="java.util.Collection" %> 

<div class="alert alert-info">
	<h2>${visaoListar.descricao}</h2>
</div>

<c:if test="${not empty viagensPublicas}">
	<fan:viagemListarTable viagens="${viagensPublicas}" visoesViagem="${visoesViagem}" titulo="Viagens públicas" />
</c:if>
