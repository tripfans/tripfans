<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="generator" required="true" type="br.com.fanaticosporviagens.infra.component.BreadCrumbGenerator" %> 

<div itemprop="breadcrumb">
<ul class="breadcrumb"> 
	<li><a href="<c:url value="/" />">Home</a><span class="divider">/</span></li> 
	<c:forEach items="${generator.breadCrumb.items}" var="item" varStatus="status">
		<c:choose>
		<c:when test="${status.last}"><li class="active">${item.nome}</li></c:when>
		<c:when test="${item.url == null}"><li class="active">${item.nome}<span class="divider">/</span></li></c:when>
		<c:otherwise><li><a href="${pageContext.request.contextPath}${item.url}">${item.nome}</a><span class="divider">/</span></li></c:otherwise>
		</c:choose> 
	</c:forEach>
</ul>
</div>