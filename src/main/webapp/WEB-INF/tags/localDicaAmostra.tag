<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ attribute name="local" required="true" type="br.com.fanaticosporviagens.model.entity.Local" %> 
<%@ attribute name="amostraDicas" required="true" type="java.util.List" %> 
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${not empty amostraDicas}">
	<div class="span12" style="margin-left: 0px;">
		<div class="page-header">
			<h3>
			Últimas dicas
			<span class="badge badge-info" style="margin-top: 10px;">${local.quantidadeDicas}</span>
			</h3>
		</div>
	
		<c:forEach items="${amostraDicas}" var="varDica" varStatus="contador" >
			<fan:dica dica="${varDica}" />	
		</c:forEach>
	
		<div class="pull-right">
		<a id="listarTodasDicas" href="${pageContext.request.contextPath}/dica/listar/${local.urlPath}?start=0" class="menu-item">Ver todas</a>
		</div>
	</div>
	
</c:if>
