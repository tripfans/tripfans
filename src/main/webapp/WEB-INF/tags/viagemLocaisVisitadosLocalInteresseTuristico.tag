<%@tag pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ attribute name="viagem" required="true" type="br.com.fanaticosporviagens.model.entity.Viagem2"%>

<c:if test="${not empty viagem.locaisVisitados}">
	<c:forEach var="continente" varStatus="continenteStatus" items="${viagem.locaisVisitados}">
		<c:if test="${not continenteStatus.first and empty continente.sublocais}"><br/></c:if>
		<strong><img alt="Local do tipo"
			src="${pageContext.request.contextPath}/resources/images/${continente.local.iconeMapa}"
			width="20" height="20" /> ${continente.local.nome}</strong>
		<c:if test="${not empty continente.sublocais}">
			<blockquote>
				<c:forEach var="pais" items="${continente.sublocais}"
					varStatus="paisStatus">
					<c:if test="${not paisStatus.first and empty pais.sublocais}"><br/></c:if>
					<strong><img alt="Local do tipo"
						src="${pageContext.request.contextPath}/resources/images/${pais.local.iconeMapa}"
						width="20" height="20" /> ${pais.local.nome}</strong>
					<c:if test="${not empty pais.sublocais}">
						<blockquote>
							<c:forEach var="estado" items="${pais.sublocais}"
								varStatus="estadoStatus">
								<c:if test="${not estadoStatus.first and empty estado.sublocais}"><br/></c:if>
								<strong><img alt="Local do tipo"
									src="${pageContext.request.contextPath}/resources/images/${estado.local.iconeMapa}"
									width="20" height="20" /> ${estado.local.nome}</strong>
								<c:if test="${not empty estado.sublocais}">
									<blockquote>
										<c:forEach var="cidade" items="${estado.sublocais}"
											varStatus="cidadeStatus">
											<c:if test="${not cidadeStatus.first and empty cidade.sublocais}"><br/></c:if>
											<strong> <img alt="Local do tipo"
												src="${pageContext.request.contextPath}/resources/images/${cidade.local.iconeMapa}"
												width="20" height="20" /> ${cidade.local.nome}
											</strong>
											<c:if test="${not empty cidade.sublocais}">
												<blockquote>
													<c:forEach var="turistico" items="${cidade.sublocais}"
														varStatus="turisticoStatus">
														<c:if test="${not turisticoStatus.first}"><br/></c:if>
														<strong> <img alt="Local do tipo"
															src="${pageContext.request.contextPath}/resources/images/${turistico.local.iconeMapa}"
															width="20" height="20" /> ${turistico.local.nome}
														</strong>
													</c:forEach>
												</blockquote>
											</c:if>
										</c:forEach>
									</blockquote>
								</c:if>
							</c:forEach>
						</blockquote>
					</c:if>
				</c:forEach>
			</blockquote>
		</c:if>
	</c:forEach>
</c:if>

