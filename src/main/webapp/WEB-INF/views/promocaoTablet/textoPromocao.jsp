<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<p class="c8">
	<span>&nbsp;</span>
</p>
<p class="c8">
	<span class="c0">1. &nbsp; &nbsp; &nbsp; Vig&ecirc;ncia da promo&ccedil;&atilde;o</span>
</p>
<p class="c8">
	<span>&nbsp;</span>
</p>
<p class="c5 c13 c14 c8">
	<span>1.1. Esta promo&ccedil;&atilde;o &eacute; v&aacute;lida do
		dia 22 de dezembro de 2013 a 31 de janeiro de 2014</span>
</p>
<p class="c5 c13 c8 c14">
	<span>1.2. Ser&atilde;o consideradas as atividades feitas entre
		00h00min do dia 22 de dezembro de 2013 at&eacute; as 23h59m (vinte e
		tr&ecirc;s horas e cinquenta e nove minutos &ndash; hor&aacute;rio de
		Bras&iacute;lia) do dia 31 de janeiro de 2014.</span>
</p>
<p class="c14 c8">
	<span>&nbsp;</span>
</p>
<p class="c8">
	<span class="c0">2. &nbsp; &nbsp; &nbsp; Como participar do
		Concurso</span>
</p>
<p class="c3 c5">
	<span class="c0"></span>
</p>
<p class="c9 c8">
	<span>2.1. Podem participar deste concurso qualquer pessoa
		f&iacute;sica, maior de 16 anos de idade.</span>
</p>
<p class="c9 c8">
	<span>2.2. A promo&ccedil;&atilde;o &eacute; v&aacute;lida para
		todo o territ&oacute;rio nacional.</span>
</p>
<p class="c9 c8">
	<span>2.3. Para participar, os interessados dever&atilde;o
		postar no </span><span class="c0">TripFans</span><span>&nbsp;dicas ou
		avalia&ccedil;&otilde;es sobre qualquer destino, hotel, restaurante ou
		atra&ccedil;&atilde;o que j&aacute; tenham visitado. Poder&atilde;o
		ser feitas avalia&ccedil;&otilde;es e dicas de qualquer lugar do
		Brasil, inclusive de suas cidades de resid&ecirc;ncia. Al&eacute;m
		disso, a partir do </span><span class="c0">TripFans</span><span>, o
		usu&aacute;rio tamb&eacute;m poder&aacute; enviar convites para que os
		amigos tamb&eacute;m se cadastrem no </span><span class="c0">TripFans</span><span>.</span>
</p>
<p class="c9 c8">
	<span>2.4. O conte&uacute;do das avalia&ccedil;&otilde;es e
		dicas dever&aacute; ser fruto de experi&ecirc;ncias vividas e as
		informa&ccedil;&otilde;es dever&atilde;o ser ver&iacute;dicas.</span>
</p>
<p class="c8 c9">
	<span>2.5. Cada atividade realizada pelo usu&aacute;rio, a
		partir da data de lan&ccedil;amento da promo&ccedil;&atilde;o,
		receber&aacute; uma pontua&ccedil;&atilde;o de acordo com o
		estabelecido na Tabela 1.</span>
</p>
<p class="c3 c15">
	<span></span>
</p>
<a href="#" name="54fa3ea8a9755ecabd39e3543415ea9ac7a8fffe"></a>
<a href="#" name="0"></a>
<div style="width: 80%; margin-left: auto; margin-right: auto;">
<table cellpadding="0" cellspacing="0" class="c21">
	<tbody>
		<tr>
			<td class="c10 c29"><p class="c11 c8 c17">
					<span class="c0 c10">A&ccedil;&atilde;o</span>
				</p></td>
			<td class="c10 c27"><p class="c11 c8 c17">
					<span class="c10 c0">Descri&ccedil;&atilde;o</span>
				</p></td>
			<td class="c10 c23"><p class="c11 c8 c17">
					<span class="c10 c0">Pontos</span>
				</p></td>
		</tr>
		<tr>
			<td class="c12"><p class="c7 c11">
					<span>Avalia&ccedil;&atilde;o</span>
				</p></td>
			<td class="c20"><p class="c7">
					<span>Escrever uma avalia&ccedil;&atilde;o sobre um destino,
						hotel, restaurante ou atra&ccedil;&atilde;o.</span>
				</p></td>
			<td class="c22"><p class="c7 c11">
					<span>6</span>
				</p></td>
		</tr>
		<tr>
			<td class="c12"><p class="c7 c11">
					<span>Dica</span>
				</p></td>
			<td class="c20"><p class="c7">
					<span>Escrever uma dica sobre um destino, hotel, restaurante
						ou atra&ccedil;&atilde;o.</span>
				</p></td>
			<td class="c22"><p class="c7 c11">
					<span>4</span>
				</p></td>
		</tr>
		<tr>
			<td class="c12"><p class="c7 c11">
					<span>Convite</span>
				</p></td>
			<td class="c20"><p class="c7">
					<span>Quando um amigo se cadastrar no TripFans a partir de
						um convite enviado pelo usu&aacute;rio.</span>
				</p></td>
			<td class="c22"><p class="c7 c11">
					<span>2</span>
				</p></td>
		</tr>
	</tbody>
</table>
</div>
<p class="c11 c8">
	<span>Tabela 1</span>
</p>
<p class="c3 c15">
	<span></span>
</p>
<p class="c8 c15">
	<span>2.6. As atividades que foram feitas pelo usu&aacute;rio
		antes do lan&ccedil;amento da promo&ccedil;&atilde;o, receber&atilde;o
		50% da pontua&ccedil;&atilde;o definida na Tabela 1. </span>
</p>
<p class="c3 c15">
	<span></span>
</p>
<a href="#" name="fb9fb93011ad78ca1e36c4c1e56fdf04d009bd67"></a>
<a href="#" name="1"></a>
<table cellpadding="0" cellspacing="0" class="c21">
	<tbody></tbody>
</table>
<p class="c8">
	<span class="c0">3. &nbsp; &nbsp; &nbsp; Crit&eacute;rios para
		sele&ccedil;&atilde;o dos ganhadores</span>
</p>
<p class="c3 c5">
	<span class="c0"></span>
</p>
<p class="c8 c15">
	<span>3.1. Ao final do concurso, ser&atilde;o contabilizados os
		pontos de cada usu&aacute;rio, de acordo com as regras estabelecidas
		no item 2 desse regulamento. </span>
</p>
<p class="c9 c8">
	<span>3.2. O usu&aacute;rio que tiver maior
		pontua&ccedil;&atilde;o ao final do concurso, ter&aacute; suas dicas e
		avalia&ccedil;&otilde;es analisadas pela comiss&atilde;o julgadora do
	</span><span class="c0">TripFans</span><span>&nbsp;quanto a sua
		pertin&ecirc;ncia e relev&acirc;ncia, &nbsp;sendo a decis&atilde;o
		desta soberana e incontest&aacute;vel. </span><span>Caso o
		usu&aacute;rio com maior pontua&ccedil;&atilde;o n&atilde;o tenha suas
		postagens aprovadas pela comiss&atilde;o julgadora, o usu&aacute;rio
		com a segunda maior pontua&ccedil;&atilde;o ter&aacute; suas
		a&ccedil;&otilde;es avaliadas e assim, sucessivamente, at&eacute; se
		encontrar um ganhador.</span><span>&nbsp;O Anexo I deste documento
		possui orienta&ccedil;&otilde;es para se fazer uma boa dica e uma boa
		&nbsp;avalia&ccedil;&atilde;o. &nbsp;</span>
</p>
<p class="c9 c3">
	<span></span>
</p>
<p class="c9 c8">
	<span>3.3. O vencedor do concurso ser&aacute; o usu&aacute;rio
		com maior pontua&ccedil;&atilde;o e que tiver suas
		avalia&ccedil;&otilde;es e dicas aprovadas pela comiss&atilde;o
		julgadora do </span><span class="c0">TripFans</span><span>. A
		comiss&atilde;o julgadora ter&aacute; 10 dias para divulgar o
		ganhador.</span>
</p>
<p class="c9 c3">
	<span></span>
</p>
<p class="c9 c8">
	<span>3.4 Em caso de empate, ser&aacute; considerado vencedor
		aquele que tiver escrito mais avalia&ccedil;&otilde;es. Persistindo o
		empate, ser&aacute; considerado o n&uacute;mero de dicas escritas. Se
		o empate persistir, ser&aacute; considerado vencedor aquele que teve a
		maior quantidade de convites aceitos pelos amigos. Por fim, se o
		empate ainda existir, ser&aacute; realizado sorteio.</span>
</p>
<p class="c9 c3">
	<span></span>
</p>
<p class="c9 c8">
	<span>3.5. &nbsp;Al&eacute;m do ganhador ser&atilde;o premiados
		os 10 primeiros colocados. Sendo que, do segundo ao d&eacute;cimo
		colocados, aplicar-se-&atilde;o as mesmas regras definidas nos itens
		anteriores. </span>
</p>
<p class="c3 c15">
	<span></span>
</p>
<p class="c9 c8">
	<span>3.6. O nome dos ganhadores ser&aacute; divulgado em nossa
		p&aacute;gina do Facebook (www.facebook.com/TripFansBR) e em nosso
		Twitter (twitter.com/tripfans) &nbsp;no dia 10 &nbsp;de fevereiro de
		2014, &agrave;s 16 horas (hor&aacute;rio de Bras&iacute;lia).</span>
</p>
<p class="c3 c15">
	<span></span>
</p>
<p class="c8">
	<span class="c0">4. &nbsp; &nbsp; &nbsp; Premia&ccedil;&atilde;o</span>
</p>
<p class="c8">
	<span class="c0">&nbsp;</span>
</p>
<p class="c8 c18">
	<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Como
		descrito no item 3.5 ser&atilde;o premiados os 10 usu&aacute;rios com
		maior pontua&ccedil;&atilde;o. Essas premia&ccedil;&otilde;es se
		dar&atilde;o da seguinte forma:</span>
</p>
<p class="c8">
	<span class="c0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
</p>
<p class="c1">
	<span>4.1. O(a) vencedor(a): &nbsp;</span>
</p>
<p class="c1">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Tablet &nbsp;-
		&nbsp;Samsung de 7 polegadas e</span>
</p>
<p class="c1">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Camiseta do
		TripFans.</span>
</p>
<p class="c1">
	<span>4.2. Segundo(a): </span>
</p>
<p class="c1">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Livro &nbsp;-
		&nbsp;C</span><span class="c0">em dias entre C&eacute;u e Mar (</span><span
		class="c0 c6">Amyr Klink</span><span class="c0">) e</span>
</p>
<p class="c8">
	<span class="c0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp; &nbsp; &nbsp; 1 Camiseta do TripFans</span>
</p>
<p class="c4 c8">
	<span>4.3. Terceiro(a): </span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Livro &nbsp;-
		&nbsp;1.000 Lugares para Conhecer Antes de Morrer (</span><span class="c0 c6">Patr&iacute;cia
		Shultz</span><span class="c0">) e</span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Camiseta do TripFans</span>
</p>
<p class="c4 c8">
	<span>4.4. Quarto(a): </span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;</span><span class="c0">1
		Livro &nbsp;- &nbsp; 100 Viagens Inspiradoras (</span><span class="c0 c6 c24">Michael
		Ondaatje; Joseph Marshall III; Paul Theroux</span><span class="c0">) </span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp;1 Camiseta do TripFans</span>
</p>
<p class="c4 c8">
	<span>4.5. Quinto(a): </span>
</p>
<p class="c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp;1 Livro - </span><span class="c0">50 praias
		inesquec&iacute;veis do Brasil - Cole&ccedil;&atilde;o Biblioteca
		Viaje Mais (</span><span class="c0 c6">Europa</span><span class="c0">)
		e</span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; 1 Camiseta do TripFans</span>
</p>
<p class="c4 c8">
	<span>4.5. Sexto(a) ao D&eacute;cimo(a): </span>
</p>
<p class="c4 c8">
	<span class="c0">&nbsp; &nbsp; &nbsp; &nbsp; 1 Camiseta do TripFans</span>
</p>
<p class="c3 c5">
	<span class="c0"></span>
</p>
<p class="c3 c5">
	<span class="c0"></span>
</p>
<p class="c8">
	<span class="c0">5. &nbsp; &nbsp; &nbsp;
		Disposi&ccedil;&otilde;es Gerais</span>
</p>
<p class="c8">
	<span>&nbsp;</span>
</p>
<p class="c1 c13">
	<span>5.1. Os participantes declaram concordar com todas as
		condi&ccedil;&otilde;es deste Regulamento.</span>
</p>
<p class="c1 c13 c26">
	<span></span>
</p>
<p class="c1 c13">
	<span>5.2. Os participantes ser&atilde;o automaticamente
		exclu&iacute;dos desta promo&ccedil;&atilde;o, no caso de fraude ou
		m&aacute; f&eacute; comprovada.</span>
</p>
<p class="c1 c13">
	<span>&nbsp;</span>
</p>
<p class="c1 c13">
	<span>5.3. &Eacute; vedada a participa&ccedil;&atilde;o de
		pessoas jur&iacute;dicas.</span>
</p>
<p class="c1 c13 c26">
	<span></span>
</p>
<p class="c1 c13">
	<span>5.4. Os participantes e o ganhador desta
		promo&ccedil;&atilde;o declaram, desde j&aacute;, serem os autores das
		dicas e avalia&ccedil;&otilde;es feitas, n&atilde;o tendo cometido
		pl&aacute;gio ou qualquer outra forma de apropria&ccedil;&atilde;o
		autoral ou fraude.</span>
</p>
<p class="c1 c13 c26">
	<span></span>
</p>
<p class="c1 c13">
	<span>5.5. O pr&ecirc;mio n&atilde;o poder&aacute; ser
		convertido em dinheiro em nenhuma hip&oacute;tese.</span>
</p>
<p class="c1 c13 c26">
	<span></span>
</p>
<p class="c1 c13">
	<span>5.6. O pr&ecirc;mio dever&aacute; ser entregue em qualquer
		endere&ccedil;o do territ&oacute;rio nacional. </span>
</p>
<p class="c1 c13 c26">
	<span></span>
</p>
<p class="c1 c13">
	<span>5.7. O </span><span class="c0">TripFans</span><span>&nbsp;ter&aacute;
		at&eacute; 30 dias ap&oacute;s a divulga&ccedil;&atilde;o do nome do
		vencedor &nbsp;para fazer o envio do pr&ecirc;mio. </span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c8">
	<span class="c0">6. &nbsp; &nbsp; &nbsp; Direitos autorais</span>
</p>
<p class="c3">
	<span class="c0"></span>
</p>
<p class="c9 c8">
	<span>6.1. </span><span>Os contemplados, desde j&aacute;, cedem,
		a t&iacute;tulo gratuito e de forma definitiva e irrevog&aacute;vel,
		&agrave; </span><span>TripFans Ltda</span><span>. os direitos de uso
		de suas imagens, som de suas vozes e direitos conexos decorrentes de
		suas participa&ccedil;&otilde;es nesta promo&ccedil;&atilde;o,
		autorizando a divulga&ccedil;&atilde;o de suas imagens, sons de voz,
		nomes, por quaisquer meios de divulga&ccedil;&atilde;o e
		publica&ccedil;&atilde;o, para utiliza&ccedil;&atilde;o comercial ou
		n&atilde;o, publicit&aacute;ria, promocional e/ou institucional, pelo
		TripFans Ltda., sem limita&ccedil;&atilde;o do n&uacute;mero de
		veicula&ccedil;&otilde;es, incluindo em filmes publicit&aacute;rios e
		institucionais veiculados em toda e qualquer forma de
		explora&ccedil;&atilde;o audiovisual (inclusive, mas sem
		limita&ccedil;&atilde;o, em filmes cinematogr&aacute;ficos, fitas
		magn&eacute;ticas ou digitais, DVD, home v&iacute;deo),
		televis&atilde;o, em m&iacute;dia eletr&ocirc;nica, al&eacute;m de
		fotos, cartazetes, an&uacute;ncios veiculados em jornais e revistas ou
		em qualquer outra forma de m&iacute;dia impressa e eletr&ocirc;nica em
		territ&oacute;rio nacional, pelo per&iacute;odo de 12 (doze) meses a
		contar da data da respectiva apura&ccedil;&atilde;o, reservando-se ao
		contemplado apenas o direito de ter o seu nome sempre vinculado ao
		material produzido e veiculado e/ou publicado por qualquer outra forma
		de m&iacute;dia impressa e eletr&ocirc;nica, ou qualquer outro suporte
		f&iacute;sico, digital ou virtual existente ou que venha a existir,
		para fins de divulga&ccedil;&atilde;o desta promo&ccedil;&atilde;o.</span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c8">
	<span>&nbsp;</span><span class="c0">7. &nbsp; &nbsp; &nbsp;
		Disposi&ccedil;&otilde;es finais</span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c9 c8">
	<span>7.1. O </span><span class="c0">TripFans</span><span>&nbsp;ao
		promover essa promo&ccedil;&atilde;o se prop&otilde;e a
		realiz&aacute;-la de forma cont&iacute;nua e ininterrupta, contudo,
		n&atilde;o ser&aacute; respons&aacute;vel pelas dificuldades
		t&eacute;cnicas que decorrentes de culpa exclusiva do consumidor ou de
		terceiros. </span>
</p>
<p class="c9 c3">
	<span></span>
</p>
<p class="c9 c8">
	<span>7.2. Nenhuma responsabilidade ser&aacute; assumida pelo </span><span
		class="c0">TripFans</span><span>, no decorrer e posteriormente
		ao per&iacute;odo de participa&ccedil;&atilde;o nesta
		promo&ccedil;&atilde;o, por quaisquer problemas t&eacute;cnicos ou mau
		funcionamento que independam da atua&ccedil;&atilde;o e/ou
		interven&ccedil;&atilde;o do </span><span class="c0">TripFans</span><span>&nbsp;e
		que sejam decorrentes da conex&atilde;o &agrave; internet que possam
		prejudicar o acesso ao </span><span class="c6">hotsite</span><span>&nbsp;de
		divulga&ccedil;&atilde;o da presente promo&ccedil;&atilde;o, a
		participa&ccedil;&atilde;o e o preenchimento do cadastro de
		participa&ccedil;&atilde;o e/ou outras a&ccedil;&otilde;es requeridas
		para a efetiva participa&ccedil;&atilde;o nesta
		promo&ccedil;&atilde;o, n&atilde;o limitadas &agrave;s seguintes
		ocorr&ecirc;ncias que sejam resultantes de: erros de </span><span class="c6">hardware</span><span>&nbsp;ou
	</span><span class="c6">software</span><span>&nbsp;de propriedade do
		participante ou interessado; defeito ou falha do computador, telefone,
		cabo, sat&eacute;lite, rede, eletroeletr&ocirc;nico, equipamento sem
		fio ou conex&atilde;o &agrave; internet ou outros problemas
		decorrentes do acesso &agrave; internet do participante ou
		interessado; erro ou limita&ccedil;&atilde;o por parte dos provedores
		de servi&ccedil;os, servidores, hospedagem; erro na transmiss&atilde;o
		das informa&ccedil;&otilde;es; falha no envio e/ou recebimento de
		mensagens eletr&ocirc;nicas; atraso ou falha no envio e/ou recebimento
		de mensagens eletr&ocirc;nicas; congestionamento da rede e/ou internet
		e/ou do </span><span class="c6">hotsite</span><span>&nbsp;de
		divulga&ccedil;&atilde;o desta promo&ccedil;&atilde;o;
		interven&ccedil;&otilde;es n&atilde;o autorizadas, sejam essas humanas
		ou n&atilde;o, incluindo, sem limita&ccedil;&atilde;o, as tentativas
		de fraude, falsifica&ccedil;&atilde;o, a&ccedil;&otilde;es de hackers,
		v&iacute;rus, </span><span class="c6">bugs</span><span>, </span><span
		class="c6">worms</span><span>; a perda e/ou
		destrui&ccedil;&atilde;o de qualquer aspecto do </span><span class="c6">hotsite</span><span>&nbsp;de
		divulga&ccedil;&atilde;o desta promo&ccedil;&atilde;o ou
		indisponibilidade tempor&aacute;ria ou permanente desse, ressalvada a
		possibilidade do TripFans realizar a desclassifica&ccedil;&atilde;o
		dos cadastros caso sejam detectadas e comprovadas ocorr&ecirc;ncias de
		fraude</span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c13 c8">
	<span>Esta &eacute; uma Promo&ccedil;&atilde;o Cultural
		promovida pela </span><span class="c0">TripFans</span><span>, conforme
		especificado no art. 30 do Decreto No. 70.951, de 09 de agosto de
		1972. A distribui&ccedil;&atilde;o do pr&ecirc;mio desta
		promo&ccedil;&atilde;o &eacute; GRATUITA. </span>
</p>
<p class="c3">
	<span></span>
</p>
<p class="c8">
	<span>&nbsp;</span><span class="c0 c28">Anexo I</span>
</p>
<p class="c3">
	<span></span>
</p>
<ol class="c19 lst-kix_svjwf9b0kv-0 start" start="1">
	<li class="c2"><span>Orienta&ccedil;&atilde;o para se
			escrever uma boa </span><span class="c0">Dica</span></li>
</ol>
<p class="c3 c13">
	<span></span>
</p>
<ul class="c19 lst-kix_6hjfuauz0749-0 start">
	<li class="c2"><span>Uma dica &eacute; uma sugest&atilde;o
			para tornar a visita do viajante ainda melhor naquele local. Algo que
			ajude o viajante a n&atilde;o perder tempo/dinheiro, coisas que ele
			deve ou n&atilde;o fazer etc...</span></li>
	<li class="c2"><span>Escreva quantas dicas voc&ecirc;
			quiser sobre este local.</span></li>
	<li class="c2"><span>Os seus amigos gostariam de saber se
			voc&ecirc; gostou de ir a este local, se indica a visita, qual seria
			o melhor hor&aacute;rio ou temporada para visit&aacute;-lo</span></li>
	<li class="c2"><span>Voc&ecirc; pode dizer se &eacute; bom
			para ir sozinho, casal, fam&iacute;lia etc.</span></li>
	<li class="c2"><span>Voc&ecirc; pode dizer se esse local
			&eacute; caro ou barato.</span></li>
</ul>
<ul class="c19 lst-kix_zcwmm5gpehq4-0 start">
	<li class="c2"><span>Evite palavras </span><span class="c0">ofensivas.</span></li>
	<li class="c2"><span>Evite </span><span class="c0">dicas
			promocionais</span><span>, como por exemplo, que anunciem descontos
			ou contenham URLs, sites e telefones. Nos reservamos o direito de
			remover dicas com essas caracter&iacute;sticas.</span></li>
</ul>
<p class="c3">
	<span></span>
</p>
<ol class="c19 lst-kix_svjwf9b0kv-0" start="2">
	<li class="c8 c15 c25"><span>Orienta&ccedil;&atilde;o para
			se escrever uma boa </span><span class="c0">Avalia&ccedil;&atilde;o</span></li>
</ol>
<p class="c3">
	<span></span>
</p>
<ul class="c19 lst-kix_x8qq2ox7mmwv-0 start">
	<li class="c2"><span>Deixe sua opini&atilde;o sobre o
			local, a experi&ecirc;ncia vivida, o que gostou e o que n&atilde;o
			gostou.</span></li>
	<li class="c2"><span>D&ecirc; uma nota baseado na sua
			experi&ecirc;ncia geral.</span></li>
	<li class="c2"><span>D&ecirc; um t&iacute;tulo que
			descreva, de forma clara e objetiva, a sua experi&ecirc;ncia nesse
			local</span></li>
	<li class="c2"><span>Tente descrever bem o local. Fale
			sobre a localiza&ccedil;&atilde;o, a estrutura e servi&ccedil;os
			oferecidos</span></li>
	<li class="c2"><span>Voc&ecirc; pode dizer se esse local
			&eacute; caro ou barato.</span></li>
	<li class="c2"><span>Se poss&iacute;vel, compartilhe fotos.
			Elas deixam suas avalia&ccedil;&otilde;es ainda melhores!</span></li>
	<li class="c2"><span>Quanto mais informa&ccedil;&otilde;es
			voc&ecirc; colocar, mais voc&ecirc; estar&aacute; ajudando os seus
			amigos e a comunidade de viajantes.</span></li>
	<li class="c2"><span>Evite palavras </span><span class="c0">ofensivas.</span></li>
	<li class="c2"><span>Evite </span><span class="c0">avalia&ccedil;&otilde;es
			promocionais</span><span>, como por exemplo, que anunciem descontos
			ou contenham URLs, sites e telefones. Nos reservamos o direito de
			remover avalia&ccedil;&otilde;es com essas caracter&iacute;sticas.</span></li>
</ul>
<p class="c3">
	<span></span>
</p>