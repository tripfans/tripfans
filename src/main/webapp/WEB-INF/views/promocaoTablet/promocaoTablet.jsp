<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Promo��o O TripFans d� um tablet pra voc� - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
		<link href="<c:url value="/resources/styles/jquery.jscrollpane.css" />" rel='stylesheet' type='text/css'>
	
		<style>
		
		#tableRankingUsuario td, 
		#tableRankingUsuario th {text-align: center;}
		
		#textoFaqPromocao dd {margin-bottom: 10px;}
		#textoFaqPromocao li {font-size: 18px;}
		
		ul.inline {list-style-type: none;}
		
		ul.inline > li {display: inline-block; padding-right: 5px; padding-left: 5px; font-size: 18px;}
		
		.lst-kix_svjwf9b0kv-4>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-4,lower-latin) ". "}ol.lst-kix_svjwf9b0kv-4.start{counter-reset:lst-ctn-kix_svjwf9b0kv-4 0}ol.lst-kix_svjwf9b0kv-6.start{counter-reset:lst-ctn-kix_svjwf9b0kv-6 0}.lst-kix_6hjfuauz0749-0>li:before{content:"\0025cf  "}.lst-kix_svjwf9b0kv-0>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-0,decimal) ". "}.lst-kix_6hjfuauz0749-2>li:before{content:"\0025a0  "}.lst-kix_x8qq2ox7mmwv-0>li:before{content:"\0025cf  "}.lst-kix_6hjfuauz0749-8>li:before{content:"\0025a0  "}ol.lst-kix_svjwf9b0kv-5.start{counter-reset:lst-ctn-kix_svjwf9b0kv-5 0}.lst-kix_svjwf9b0kv-3>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-3,decimal) ". "}.lst-kix_svjwf9b0kv-6>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-6,decimal) ". "}ul.lst-kix_x8qq2ox7mmwv-3{list-style-type:none}.lst-kix_6hjfuauz0749-3>li:before{content:"\0025cf  "}.lst-kix_x8qq2ox7mmwv-7>li:before{content:"\0025cb  "}ul.lst-kix_x8qq2ox7mmwv-2{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-5{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-4{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-1{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-0{list-style-type:none}.lst-kix_svjwf9b0kv-7>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-7,lower-latin) ". "}.lst-kix_6hjfuauz0749-1>li:before{content:"\0025cb  "}ul.lst-kix_zcwmm5gpehq4-2{list-style-type:none}ul.lst-kix_zcwmm5gpehq4-1{list-style-type:none}ul.lst-kix_zcwmm5gpehq4-0{list-style-type:none}ul.lst-kix_zcwmm5gpehq4-6{list-style-type:none}.lst-kix_svjwf9b0kv-1>li{counter-increment:lst-ctn-kix_svjwf9b0kv-1}ul.lst-kix_zcwmm5gpehq4-5{list-style-type:none}.lst-kix_zcwmm5gpehq4-4>li:before{content:"\0025cb  "}ul.lst-kix_zcwmm5gpehq4-4{list-style-type:none}ul.lst-kix_zcwmm5gpehq4-3{list-style-type:none}.lst-kix_6hjfuauz0749-7>li:before{content:"\0025cb  "}.lst-kix_zcwmm5gpehq4-2>li:before{content:"\0025a0  "}ol.lst-kix_svjwf9b0kv-1.start{counter-reset:lst-ctn-kix_svjwf9b0kv-1 0}ol.lst-kix_svjwf9b0kv-3.start{counter-reset:lst-ctn-kix_svjwf9b0kv-3 0}ul.lst-kix_zcwmm5gpehq4-8{list-style-type:none}.lst-kix_zcwmm5gpehq4-7>li:before{content:"\0025cb  "}ul.lst-kix_zcwmm5gpehq4-7{list-style-type:none}ol.lst-kix_svjwf9b0kv-0.start{counter-reset:lst-ctn-kix_svjwf9b0kv-0 0}.lst-kix_x8qq2ox7mmwv-1>li:before{content:"\0025cb  "}.lst-kix_x8qq2ox7mmwv-3>li:before{content:"\0025cf  "}.lst-kix_svjwf9b0kv-4>li{counter-increment:lst-ctn-kix_svjwf9b0kv-4}.lst-kix_svjwf9b0kv-2>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-2,lower-roman) ". "}.lst-kix_6hjfuauz0749-4>li:before{content:"\0025cb  "}ol.lst-kix_svjwf9b0kv-7.start{counter-reset:lst-ctn-kix_svjwf9b0kv-7 0}ol.lst-kix_svjwf9b0kv-0{list-style-type:none}ol.lst-kix_svjwf9b0kv-1{list-style-type:none}ol.lst-kix_svjwf9b0kv-2{list-style-type:none}.lst-kix_svjwf9b0kv-1>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-1,lower-latin) ". "}.lst-kix_6hjfuauz0749-5>li:before{content:"\0025a0  "}ol.lst-kix_svjwf9b0kv-3{list-style-type:none}.lst-kix_zcwmm5gpehq4-5>li:before{content:"\0025a0  "}ol.lst-kix_svjwf9b0kv-4{list-style-type:none}.lst-kix_x8qq2ox7mmwv-5>li:before{content:"\0025a0  "}ol.lst-kix_svjwf9b0kv-5{list-style-type:none}ol.lst-kix_svjwf9b0kv-6{list-style-type:none}.lst-kix_svjwf9b0kv-2>li{counter-increment:lst-ctn-kix_svjwf9b0kv-2}ol.lst-kix_svjwf9b0kv-7{list-style-type:none}ol.lst-kix_svjwf9b0kv-8{list-style-type:none}.lst-kix_6hjfuauz0749-6>li:before{content:"\0025cf  "}ol.lst-kix_svjwf9b0kv-2.start{counter-reset:lst-ctn-kix_svjwf9b0kv-2 0}.lst-kix_svjwf9b0kv-3>li{counter-increment:lst-ctn-kix_svjwf9b0kv-3}.lst-kix_x8qq2ox7mmwv-4>li:before{content:"\0025cb  "}.lst-kix_svjwf9b0kv-8>li{counter-increment:lst-ctn-kix_svjwf9b0kv-8}.lst-kix_zcwmm5gpehq4-1>li:before{content:"\0025cb  "}.lst-kix_svjwf9b0kv-5>li{counter-increment:lst-ctn-kix_svjwf9b0kv-5}.lst-kix_zcwmm5gpehq4-0>li:before{content:"\0025cf  "}.lst-kix_x8qq2ox7mmwv-8>li:before{content:"\0025a0  "}.lst-kix_svjwf9b0kv-0>li{counter-increment:lst-ctn-kix_svjwf9b0kv-0}ol.lst-kix_svjwf9b0kv-8.start{counter-reset:lst-ctn-kix_svjwf9b0kv-8 0}.lst-kix_svjwf9b0kv-7>li{counter-increment:lst-ctn-kix_svjwf9b0kv-7}ul.lst-kix_6hjfuauz0749-1{list-style-type:none}ul.lst-kix_6hjfuauz0749-0{list-style-type:none}ul.lst-kix_6hjfuauz0749-3{list-style-type:none}ul.lst-kix_6hjfuauz0749-2{list-style-type:none}.lst-kix_zcwmm5gpehq4-8>li:before{content:"\0025a0  "}ul.lst-kix_6hjfuauz0749-5{list-style-type:none}ul.lst-kix_6hjfuauz0749-4{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-8{list-style-type:none}ul.lst-kix_x8qq2ox7mmwv-7{list-style-type:none}.lst-kix_svjwf9b0kv-5>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-5,lower-roman) ". "}ul.lst-kix_x8qq2ox7mmwv-6{list-style-type:none}.lst-kix_x8qq2ox7mmwv-6>li:before{content:"\0025cf  "}.lst-kix_zcwmm5gpehq4-3>li:before{content:"\0025cf  "}.lst-kix_x8qq2ox7mmwv-2>li:before{content:"\0025a0  "}.lst-kix_zcwmm5gpehq4-6>li:before{content:"\0025cf  "}.lst-kix_svjwf9b0kv-8>li:before{content:"" counter(lst-ctn-kix_svjwf9b0kv-8,lower-roman) ". "}ul.lst-kix_6hjfuauz0749-6{list-style-type:none}ul.lst-kix_6hjfuauz0749-7{list-style-type:none}ul.lst-kix_6hjfuauz0749-8{list-style-type:none}.lst-kix_svjwf9b0kv-6>li{counter-increment:lst-ctn-kix_svjwf9b0kv-6}ol{margin:0;padding:0}.c20{vertical-align:top;width:309.8pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 5pt 5pt 5pt}.c23{vertical-align:top;width:81pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 1pt 5pt 1pt}.c27{vertical-align:top;width:309.8pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 1pt 5pt 1pt}.c29{vertical-align:top;width:69.8pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 1pt 5pt 1pt}.c22{vertical-align:top;width:81pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 5pt 5pt 5pt}.c12{vertical-align:top;width:69.8pt;border-style:solid;border-color:#000000;border-width:1pt;padding:5pt 5pt 5pt 5pt}.c7{line-height:1.0;padding-top:0pt;direction:ltr;padding-bottom:0pt}.c2{padding-left:0pt;text-align:justify;direction:ltr;margin-left:36pt}.c30{max-width:480.8pt;background-color:#ffffff;padding:72pt 59.2pt 72pt 72pt}.c21{margin-right:auto;border-collapse:collapse}.c1{direction:ltr;margin-left:35pt}.c9{text-align:justify;margin-left:36pt}.c19{margin:0;padding:0}.c3{height:11pt;direction:ltr}.c5{text-indent:-18pt}.c25{padding-left:0pt}.c11{text-align:center}.c4{text-indent:36pt}.c13{text-align:justify}.c16{font-size:24pt}.c26{height:11pt}.c28{font-size:12pt}.c10{background-color:#bfbfbf}.c18{margin-left:34.5pt}.c14{margin-left:54pt}.c0{font-weight:bold}.c8{direction:ltr}.c15{margin-left:36pt}.c6{font-style:italic}.c24{background-color:#fafafa}.c17{line-height:1.0}.title{padding-top:0pt;line-height:1.15;text-align:left;color:#000000;font-size:21pt; padding-bottom:0pt}.subtitle{padding-top:0pt;line-height:1.15;text-align:left;color:#666666;font-style:italic;font-size:13pt; padding-bottom:10pt}li{font-size:11pt;}p{color:#000000;font-size:11pt;margin:0;}h1{padding-top:10pt;line-height:1.15;text-align:left;color:#000000;font-size:16pt; padding-bottom:0pt}h2{padding-top:10pt;line-height:1.15;text-align:left;color:#000000;font-size:13pt; font-weight:bold;padding-bottom:0pt}h3{padding-top:8pt;line-height:1.15;text-align:left;color:#666666;font-size:12pt; font-weight:bold;padding-bottom:0pt}h4{padding-top:8pt;line-height:1.15;text-align:left;color:#666666;font-size:11pt;text-decoration:underline; padding-bottom:0pt}h5{padding-top:8pt;line-height:1.15;text-align:left;color:#666666;font-size:11pt; padding-bottom:0pt}h6{padding-top:8pt;line-height:1.15;text-align:left;color:#666666;font-style:italic;font-size:11pt; padding-bottom:0pt}
		
		</style>
	
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    	<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.jscrollpane.min.js" />"></script>
    
        <script type="text/javascript">
            $(document).ready(function () {
            	$('#textoRegulamento').jScrollPane({showArrows: true});
            	
            	$('.btnRegulamento').click(function (e){
            		e.preventDefault();
            		$.scrollTo($('#regulamentoPromocao'), {offset: -80, duration: 500} );
            	});
            	
            	$('.btnRanking').click(function (e){
            		e.preventDefault();
            		$.scrollTo($('#rankingPromocao'), {offset: -80, duration: 500} );
            	});
            	
            	$('.btnFAQ').click(function (e){
            		e.preventDefault();
            		$.scrollTo($('#faqPromocao'), {offset: -80, duration: 500} );
            	});
            });
            
        </script>
        
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-content" style="text-align: center;">
	            		<p style="font-size: 30px; font-weight: bold;"> Promo��o "O TripFans d� um Tablet para voc�"</p>
	            		
	            		<div class="horizontalSpacer"></div>
	            		
	            		<img src="<c:url value="/resources/images/promocoes/promocaoTabletSite.jpg" />" height="650" />

						<div class="horizontalSpacer"></div>
	            		
	            		
		            	<div class="row">
		            		<a class="btn btn-primary btn-large btnRegulamento" href="#">Veja o Regulamento</a>
			            	<a class="btn btn-success btn-large btnRanking" href="#">Confira o ranking</a>
			            	<a class="btn btn-warning btn-large btnFAQ" href="#">Leia o FAQ da Promo��o</a>
		            	</div>
		            	
		            	<div class="horizontalSpacer"></div>
		            	<div class="horizontalSpacer"></div>
		            	
		            	<a id="regulamentoPromocao" style="font-size: 25px; font-weight: bold;">Regulamento da Promo��o</a>
		            	
		            	<div id="textoRegulamento" style="font-size: 18px; overflow: auto; padding: 15px; text-align: justify; overflow: hidden; width: 80%; margin-left: auto; margin-right: auto; margin-top: 30px; height: 400px;">
		            		<jsp:include page="textoPromocao.jsp" />
		            	</div>
		            	
		            	<div class="horizontalSpacer"></div>
		            	<div class="horizontalSpacer"></div>
		            	
		            	<a id="rankingPromocao" style="font-size: 25px; font-weight: bold;">Ranking da Promo��o</a>
		            	
		            	<c:set var="exibeRankingCompleto" value="${not empty usuario and (usuario.id == 2 or usuario.id == 91 or usuario.id == 112 or usuario.id == 25 or usuario.id == 93)}" />
		            	
		            	
		            	<c:choose>
		            	<c:when test="${exibeRankingCompleto}">
	            			<div style="width: 80%; margin-left: auto; margin-right: auto; padding-top: 15px;">
		            	</c:when>
		            	<c:otherwise>
	            			<div style="width: 40%; margin-left: auto; margin-right: auto; padding-top: 15px;">
		            	</c:otherwise>
	            		</c:choose>
	            			<table id="tableRankingUsuario" class="table" style="font-size: 18px;">
	            				<thead>
	            					<tr>
	            						<th>#</th>
	            						<th>Nome</th>
	            						<c:if test="${exibeRankingCompleto}">
		            						<th>Quantidade Avalia��es</th>
		            						<th>Quantidade Dicas</th>
		            						<th>Quantidade Convites Aceitos</th>
		            						<th>Pontua��o</th>
	            						</c:if>
	            					</tr>
	            				</thead>
		            			<tbody>
		            			<c:forEach items="${usuarios}" var="usuarioPojo" varStatus="status">
		            				<c:set var="current" value="" />
		            				<c:if test="${status.count <= 10}">
		            					<c:set var="current" value="style=\"font-weight: bold;\"" />
		            				</c:if> 
		            				<c:if test="${usuario.id == usuarioPojo.idUsuario}">
		            					<c:set var="current" value="style=\"font-weight: bold; background-color: #d9edf7;\"" />
		            				</c:if>
		            					<tr ${current}>
            								<td>${status.count}</td>
		            						<td><a href="<c:url value="/perfil/${usuarioPojo.urlPath}" />" target="_blank">${usuarioPojo.nome}</a></td>
		            						<c:if test="${exibeRankingCompleto}">
			            						<td>${usuarioPojo.quantidadeAvaliacoes}</td>
			            						<td>${usuarioPojo.quantidadeDicas}</td>
			            						<td>${usuarioPojo.quantidadeConvitesAceitos}</td>
			            						<td>${usuarioPojo.pontuacao}</td>
		            						</c:if>
		            					</tr>
		            			</c:forEach>
	            				<c:if test="${not empty usuarioAtual}">
									<tr style="font-weight: bold; background-color: #d9edf7;">
	       								<td></td>
	       								<td><a href="<c:url value="/perfil/${usuarioAtual.urlPath}" />" target="_blank">${usuarioAtual.nome}</a></td>
	       								<c:if test="${exibeRankingCompleto}">
			           						<td>${usuarioAtual.quantidadeAvaliacoes}</td>
			           						<td>${usuarioAtual.quantidadeDicas}</td>
			           						<td>${usuarioAtual.quantidadeConvitesAceitos}</td>
			           						<td>${usuarioAtual.pontuacao}</td>
		           						</c:if>
	            					</tr>	            						
	            				</c:if>
	            				</tbody>
	            			</table>
	            			<span class="alert alert-error">
	            				<strong>Aten��o: </strong> as a��es feitas antes da vig�ncia da promo��o contam 50% da pontua��o de cada item. 
	            			</span>
	            		</div>
	            		
	            		<div class="horizontalSpacer"></div>
		            	<div class="horizontalSpacer"></div>
		            	
		            	<a id="faqPromocao" style="font-size: 25px; font-weight: bold;">FAQ da Promo��o</a>
		            	
		            	<div id="textoFaqPromocao" style="text-align: left; font-size: 18px;">
		            		<dl>
							  <dt>Como participo da promo��o?</dt>
							  <dd>
							  Para participar � simples. Basta se cadastrar (se voc� ainda n�o tiver cadastro) no TripFans e come�ar a escrever avalia��es, dicas e que seus amigos aceitem seus convites
							  para entrar no TripFans. Cada uma dessas a��es geram pontos, conforme descrito no <a href="#regulamentoPromocao">Regulamento da Promo��o</a>. Ao final da promo��o,
							  o usu�rio que tiver mais pontos e tiver suas postagens aprovadas pela comiss�o julgadora ser� o vencedor.
							  </dd>
							  
							   <dt>J� fiz avalia��es e dicas nos TripFans. Elas v�o contar pontos pra mim?</dt>
							  <dd>
							  Sim. Conforme descrito no item 2.6 do <a href="#regulamentoPromocao">Regulamento da Promo��o</a>, as a��es feitas antes da vig�ncia da promo��o contam pontos para
							  os usu�rios. No entanto, elas contam metade dos pontos da tabela. Assim, por exemplo, avalia��es escritas antes do dia de in�cio da promo��o
							  contam 3 pontos em vez dos 6 pontos discriminados no regulamento.
							  </dd>
							  
							  <dt>Posso escrever avalia��es e dicas da cidade onde moro?</dt>
							  <dd>Com certeza! Voc� pode escrever avalia��es e dicas de qualquer lugar que visitou no Brasil, inclusive da sua cidade de resid�ncia e dos
							  locais (atra��es, hot�is e restaurantes) existentes nela.
							  </dd>
							  
							  <dt>Fui em um local mas n�o o encontrei no TripFans. Como fa�o para avali�-lo ou escrever uma dica?</dt>
							  <dd>
							  Se voc� n�o encontrou um local no TripFans, basta sugeri-lo por meio do nosso <a href="<c:url value="/locais/sugerirLocal" />" target="_blank">formul�rio de sugest�o</a>. Em seguida, voc� j� pode 
							  escrever avalia��es e dicas para o mesmo. O local ser� aprovado pela nossa equipe o quanto antes e as suas a��es feitas nele contar�o pontos
							  pra voc�.
							  </dd>
							  
							   <dt>Como fa�o para enviar convites para os amigos entrarem no TripFans?</dt>
							  <dd>Para enviar convites, voc� precisa fazer o login. Ap�s isso, os convites podem ser feitos da seguinte forma:
							  		<ul style="margin-top: 5px;">
							  			<c:url var="urlPerfilAmigos" value="/perfil/meuPerfil?tab=amigos&convidar=true"/>
	                                	<c:if test="${usuario != null}">
	                                		<c:url var="urlPerfilAmigos" value="/perfil/${usuario.urlPath}/amigos?convidar=true"/>
	                                	</c:if>
							  			<li >Se voc� possui conta no Facebook e quer convidar seus amigos, <a href="${urlPerfilAmigos}&acao=meusAmigos" target="_blank">clique aqui</a></li>
							  			<li>Se voc� quer convidar seus contatos do Gmail ou enviar e-mails diretamente, <a href="${urlPerfilAmigos}&acao=encontrarAmigos" target="_blank">clique aqui</a></li>
							  		</ul>
							  </dd>
							  
							</dl>
		            	</div>
		            	
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>