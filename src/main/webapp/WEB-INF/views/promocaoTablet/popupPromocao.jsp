<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="popupPromocaoTablet" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 630px; margin-left: -285px;">
        <div class="modal-header">
        <span style="font-size: 25px; font-weight: bold;">Promoção TripFans dá um Tablet para você</span>
        </div>
        <div class="modal-body">
                        <div class="" style="width: 90%; margin-left: auto; margin-right: auto">
                                <img src="<c:url value="/resources/images/promocoes/promocaoTabletSite.jpg" />" width="550" />
                        </div>

        </div>
        <div class="modal-footer" style="text-align: center;">
                <p style="font-size: 20px;"><strong>Começe agora mesmo</strong></p>
                <c:url var="urlConvidar" value="/perfil/meuPerfil?tab=amigos&acao=encontrarAmigos"/>
        <c:if test="${usuario != null}">
                <c:url var="urlConvidar" value="/perfil/${usuario.urlPath}/amigos?acao=encontrarAmigos"/>
        </c:if>
<a  class="btn btn-large btn-primary btnPopupPromocao" href="<c:url value="/avaliacao/oQueAvaliar" />">Escreva avaliações</a>
                <a  class="btn btn-large btn-primary btnPopupPromocao" href="<c:url value="/dicas/sobreOQueEscreverDica" />">Deixe dicas</a>
            <a  class="btn btn-large btn-primary btnPopupPromocao" href="${urlConvidar}">Envie convites</a>
            <a  class="btn btn-large btn-secondary btnPopupPromocao" href="<c:url value="/promocaoTablet" />">Saiba mais</a>
            <a  class="btn btn-large btnPopupPromocao" href="#">Não obrigado</a>
        </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
$('a.btnPopupPromocao').click(function (e) {
                e.preventDefault();
                $.cookie('popup_promocao_tablet', 'true', {expires: 60  , path: '/'});
                $('#popupPromocaoTablet').modal('hide');
                window.location.href = $(this).attr('href');
        });     
});

</script>
