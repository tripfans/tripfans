<%@page
	import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title"
		value="${param.term} - Pesquisa do TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			$(document).ready(function() {
				carregar(0);

				$('a.menu-item').bind('click',function(event){
					event.preventDefault();
					$('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    $(this).parent().addClass('active'); //Set clicked link class to active
				});
				
			});
			
			function carregar(codigoCategoriasPesquisa) {
				$('#conteudo').hide();
				$('#conteudo').load("pesquisaGeralResultados?start=0&term=" + encodeURIComponent("${param.term}") +"&codigoCategoriasPesquisa=" + codigoCategoriasPesquisa, function(){ inicializarDivs();});
				$('#conteudo').fadeIn(1000);
			}
			
			function inicializarDivs(){
				$(".breadcrumb").each(function(index, element){
					if(element.inicializado == undefined){
						$(element).hover(function(){
							$(element).toggleClass("breadcrumb sombreado");
							$(element).stop().animate({
								  height: 155, width: 355						  
								}, "fast");
							
						}, 
						function(){
							$(element).toggleClass("breadcrumb sombreado");
							$(element).stop().animate({
								  height: 150, width: 350
								  
								}, "fast");
						});
					}
					element.inicializado = true;
					
				});
			}
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="container-fluid page-container">
				<div class="title gradient-background">
					<h2>Resultados para "${param.term}"</h2>
				</div>
				<div class="row">
					<div class="span4" id="filtroPesquisa">
						<div class="sidebar-nav">    
						    <div class="tabbable tabs-left" >
						    	<ul  class="nav nav-tabs span4" style="margin-left: 0px; margin-top: 2px; background-color: #F0F4F5;" id="categorias">
									<li class="active">
										<a id="tudo" href="#" class="menu-item" onclick="carregar(0)">
											<img src='<c:url value="/resources/images/icons/zoom.png"/>' border="0" />
											Tudo
										</a>
									</li>
									<c:forEach var="categoria" items="${categorias}">
										<li>
											<a href="#" onclick="carregar(${categoria.codigo})" class="menu-item">
												<img src='<c:url value="/resources/images/icons/${categoria.icone}"/>' border="0" />
												${categoria.descricao}
											</a>
										</li>
									</c:forEach>
								</ul>
							</div>
						</div>
					</div>
					<div class="span14">
						&nbsp;
						<div id="conteudo"></div>
						<div class="span14">
							<form></form>
						</div>


					</div>
				</div>
		</div>
	</tiles:putAttribute>
</tiles:insertDefinition>