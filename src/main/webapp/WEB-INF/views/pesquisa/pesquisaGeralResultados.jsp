<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<div id="lista">


	<c:forEach var="item" items="${lista}">
		<c:url value='${item.url}' var="itemUrl"/>
		<div class="span6"
			style="margin-top: 20px; padding: 2px 2px 15px 2px; width:300px; height: 100px">
			<c:if test="${not (item.tipoEntidade eq 11) }">
				<fan:card type="local" name="${item.nomeExibicao}"
					itemUrl="${itemUrl}"
					photoUrl="${item.urlFotoAlbum}" />
			</c:if>
			<c:if test="${item.tipoEntidade eq 11}">
				<fan:card type="user" name="${item.nomeExibicao}"
					itemUrl="${itemUrl}"
					photoUrl="${item.urlFotoAlbum}" />
			</c:if>
		</div>
	</c:forEach>

	<c:url var="urlMore"
		value="/pesquisa/pesquisaGeralResultados?term=${param.term}&codigoCategoriasPesquisa=${param.codigoCategoriasPesquisa}" />

	<fan:pagingButton url="${urlMore}" targetId="lista" cssClass="offset3"
		title="Ver Mais Itens" noMoreItensText=""
		oncomplete="inicializarDivs()" />


</div>