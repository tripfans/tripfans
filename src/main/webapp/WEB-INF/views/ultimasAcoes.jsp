<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
    
<div class="module-box-content-list">
    <div class="news-contents-wrapper">
        <div class="news-contents">
        
          <c:forEach items="${ultimasAcoesUsuarios}" var="acaoUsuario">
          
           <c:choose>
            <c:when test="${acaoUsuario.acaoSobreAvaliacao}">
              <c:url var="urlAtividade" value="/locais/${acaoUsuario.rastreioAvaliacao.localAvaliado.urlPath}?tab=avaliacoes&item=${acaoUsuario.rastreioAvaliacao.urlPath}" />
            </c:when>
            <c:when test="${acaoUsuario.acaoSobreDica}">
              <c:url var="urlAtividade" value="/locais/${acaoUsuario.rastreioDica.localDica.urlPath}?tab=dicas&item=${acaoUsuario.rastreioDica.urlPath}" />
            </c:when>
            <c:when test="${acaoUsuario.acaoSobrePergunta}">
              <c:url var="urlAtividade" value="/perguntas/verPergunta/${acaoUsuario.rastreioPergunta.urlPath}" />
            </c:when>
            <c:when test="${acaoUsuario.acaoSobreResposta}">
              <c:url var="urlAtividade" value="/perguntas/verPergunta/${acaoUsuario.rastreioResposta.pergunta.urlPath}/#respostas" />
            </c:when>
            <c:otherwise>
              <c:url var="urlAtividade" value="#" />
            </c:otherwise>
           </c:choose>
          
            <a href="${urlAtividade}" class="link-notificacao" style="text-decoration: none;" data-acao-usuario-id="${acaoUsuario.id}">
            
                <fan:atividade acaoUsuario="${acaoUsuario}" mostrarFotoAutor="true" mostrarIcone="false" mostrarData="true"
                               usarFontePequena="true" mostrarLinkNomeUsuario="true" />
                
            </a>
            
          </c:forEach>
        </div>
    </div>
</div>