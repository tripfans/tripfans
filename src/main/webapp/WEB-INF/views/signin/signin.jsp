<%@ page session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/spring-social/facebook/tags" prefix="facebook" %>

<tiles:insertDefinition name="tripfans.responsive.new">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
        <script type="text/javascript"> 
        $(document).ready(function () {
            $('.login-modal-link').hide();
        });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="horizontalSpacer"></div>
    
        <div class="container-fluid">
        
            <div style="margin-left:auto; margin-right:auto;" class="">
            
               <div class="col-md-2"></div>
            
                <div class="col-md-8 centerAligned">
                	<c:if test="${not empty showMsgRoteiro}">
   	                	<div class="alert alert-warning lead" role="alert">
   	                		<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					     	<strong>Aten��o</strong> Para criar um roteiro de viagem, � necess�rio fazer login.
					    </div>
   	                </c:if>
            
    	            <div class="span12 borda-arredondada module-box" style="background-color: #ffffff; min-height: 520px;">
    	                <div class="module-box-header" style="border-color: #ddd; ">
    	                    <h3>
    	                        Entrar no
                                <img src="<c:url value="/resources/images/logos/tripfans-t.png"/>" height="20" />
    	                    </h3>
    	                </div>
    	                
    	                <div class="module-box-content">
    	                    <c:import url="/WEB-INF/views/signin/signInUpPanel.jsp">
    	                        <c:param name="newUser" value="true"></c:param>
    	                    </c:import>
    	                </div>
    	            </div>
                
                </div>
                
                <div class="col-md-2"></div>
            </div>

        </div>
        
        <div class="horizontalSpacer"></div>
    
    </tiles:putAttribute>
    
</tiles:insertDefinition>