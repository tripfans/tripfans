<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<style>
    //input.error { border: 1px solid red; width: auto; }
    label.error {
        background: url('http://dev.jquery.com/view/trunk/plugins/validate/demo/images/unchecked.gif') no-repeat;
        padding-left: 16px;
        margin-left: .3em;
    }
    label.valid {
        background: url('http://dev.jquery.com/view/trunk/plugins/validate/demo/images/checked.gif') no-repeat;
        display: block;
        width: 16px;
        height: 16px;
    }
</style>

<c:if test="${codigoConfirmado}">

<div>
    <div>
        <form id="alterarSenhaForm" method="post" action="<c:url value="/recuperarConta/alterarSenha" />" class="form-stacked form-horizontal">
    
            <fieldset>
            
                <div class="form-group">
                    <label class="span4 col-sm-4 control-label" style="margin-left: 0px;">
                        <span class="required">
                            <s:message code="label.usuario.senha.nova" />
                        </span>
                    </label>
                    <div class="col-xs-5">
                        <input type="password" id="senhaNova" name="senhaNova" class="span6 form-control" />
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="span4 col-sm-4 control-label" style="margin-left: 0px;">
                        <span class="required">
                            <s:message code="label.usuario.senha.confirmar" />
                        </span>
                    </label>
                    <div class="col-xs-5">
                        <input type="password" id="confirmaSenha" name="confirmaSenha" class="span6 form-control" />
                    </div>
                </div>             

            </fieldset>
            <input type="hidden" name="emailRecuperacao" value="${emailRecuperacao}" />
            <input type="hidden" name="codigoRecuperacao" value="${codigoRecuperacao}" />
            
            <div align="center" style="display: inline-table; width: 100%;">
                <input type="submit" id="botaoConcluir" value="Concluir" class="btn btn-large btn-primary span2" />
            </div>             
        </form>
        
    </div>
</div>

<script>

$(document).ready(function () {
    
    $('#alterarSenhaForm').validate({ 
        rules: {
            senhaNova: { 
                required: true,
                minlength : 6,
                maxlength : 20
            }, 
            confirmaSenha: {
                required: true,
                equalTo: "#senhaNova",
                minlength : 6,
                maxlength : 20
            }, 
        }, 
        messages: { 
            senhaNova: {
                required : "<s:message code="validation.password.required" />",
                minlength : "<s:message code="validation.password.minlength" />",
                maxlength : "<s:message code="validation.password.maxlength" />"
            },
            confirmaSenha: {
            	required : "<s:message code="validation.password.required" />",
            	equalTo : "<s:message code="validation.confirmPassword.required" />"
            }
        } 
    });
    
});

</script>

</c:if>

<c:if test="${not codigoConfirmado}">
    <div class="alert alert-error centerAligned" style="text-align: justify;">
      <p>
        Ocorreu um erro durante recupera��o de sua senha.
        <br/>
        Voc� pode ter digitado o c�digo incorreto.
        <br/>
        Por favor, tente novamente.
        <br/>
        Se o problema persistir, entre em contato conosco.
      </p>
    </div>
    
    <div align="center" style="display: inline-table; width: 100%;">
        <input type="submit" id="botaoVoltar" value="Voltar" class="btn btn-primary btn-large span2" />
    </div>    
</c:if>

<script>
    $('#botaoVoltar').click(function(event) {
        event.preventDefault();
        $('#divCodigoInformado').hide();
        $('#divInformarCodigo').show('');
        esconder($('#divCodigoInformado'));
        exibir($('#divInformarCodigo'));
        //retornarConfirmarUsername();
    });
</script>
