<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:url value="/conta/openidlogin" var="form_url_google_openid" />
<c:url var="google-account-logo-url" value="/resources/images/logos/google-account-logo.ico" />
<c:url var="facebook-account-logo-url" value="/resources/images/logos/facebook-account-logo.gif" />

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<style>

input {
	padding-bottom: 10px;
	font-size: 15px;
}

select {
	height: 35px;
	font-size: 15px;
}

#wrapper {
	border-left: 1px solid #cdcdcd;
	padding-left: 20px;
}
</style>

<c:set var="targetUrlParameter" value="${not empty targetUrlParameter ? targetUrlParameter : param.targetUrlParameter}"/>

<div class="popup-box ${span} ${empty usuario.id ? 'show' : 'hide hidden'}" style="padding: 20px;">
	
	<div class="row centerAligned">
		<c:if test="${param.responderPedidoDica == true}">
		<div class="alert alert-info lead">
			<strong>Para responder ao pedido de dica, voc� precisa se autenticar.</strong>
		</div>
		</c:if>
	
        <div class="row" style="min-width: 160px; display: inline-block;">
            <!-- FACEBOOK SIGNIN -->
            <div class="col-sm-1 col-md-1"></div>
            <div class="span3 col-md-5 col-sm-5" style="margin-bottom: 5px; min-width: 160px;">
              <form name="fb_signin" id="fb_signin" action="<c:url value="/signin/facebook"/>" method="POST">
                <input type="hidden" name="scope" value="email,user_hometown,public_profile,user_friends" />
                <a href="#" onclick="signInWithFacebook()">
                  <i class="facebookLoginButton"></i>
                </a>
              </form>
            </div>

            <!-- GOOGLE SIGNIN -->
            <div class="span3 col-sm-4">
              <form name="form_google" id="form_google" action="<c:url value="/signin/google"/>" method="POST">
                <input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo#email https://www.googleapis.com/auth/plus.me https://www.google.com/m8/feeds" />
                <input type="hidden" name="access_type" value="offline" />
                <a href="#" onclick="signInWithGoogle()">
                  <i class="googleLoginButton"></i>
                </a>
              </form>
			</div>
            
            <div class="col-sm-1 col-md-2"></div>

            <!-- TWITTER SIGNIN -->
            <form id="form_tw" action="<c:url value="/signin/twitter"/>" method="POST">
                <!--button type="submit"><img src="<c:url value="/resources/images/social/twitter/sign-in-with-twitter-d.png"/>" /></button-->
            </form>

            <!-- FOURSQUARE SIGNIN -->
            <form id="form_fs" action="<c:url value="/signin/foursquare"/>" method="POST">
            </form>

        </div>
<!--     </div> -->

        <div class="row">
          <div class="well well-sm" style="max-width: 35px; margin: 10px auto 10px; padding: 6px;">
            OU
          </div>
        </div>

    <div class="centerAligned">
      <div id="divLogin" class="${cadastro ? 'hide hidden' : 'show'}">
    	<div style="padding-bottom: 15px;">
		<h4 class="centerAligned">Fa�a o login com sua conta TripFans</h4>
		</div>
		<dt>
		    <div id="errors" class="alert alert-danger" style="display: none;">
		        <a class="close" href="#">�</a>
		    </div>
		</dt>
    
		<c:url value="/signin" var="signinUrl" />
		<c:url value="/signup" var="signupUrl" />
		
		<c:if test="${usuario == null}">
		
		<form id="loginform" action="${signinUrl}" method="post" class="centerAligned">
        
    		<div class="form-group row" align="center">
                <div class="col-sm-2"></div>
    		    <div class="col-sm-8">
    		        <input type="text" name="username" id="username" class="span7 form-control" placeholder="Seu e-mail" />
    		    </div>
                <div class="col-sm-2"></div>
    		</div>  
    		
            <div class="form-group row" align="center">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
    		        <input type="password" id="password" name="password" autocomplete="off" autocorrect="off" autocapitalize="off" class="span7 form-control" placeholder="Sua senha" />
    		    </div>
                <div class="col-sm-2"></div>
    		</div>
    		
    		<div class="control-group">
                <input id="j_remember" checked="checked" name="_spring_security_remember_me" type="checkbox" style="float: initial;" />
				<label for="j_remember" class="checkbox" style="display: inline-block; padding-left: 2px;">
                    Mantenha-me autenticado
            	</label>
            </div>
    		
    		<div class="centerAligned">
    		    <input type="submit" name="signIn" id="signIn" value="<s:message code="label.login" />" class="btn btn-large btn-primary span2" data-loading-text="Aguarde..." />
    		    <input name="signUp" id="botaoCadastro" value="<s:message code="label.cadastreSe" />" class="botaoCadastro btn btn-large btn-secondary span3" style="margin-right: 5px; min-width: 160px;" />
    		</div>
    		
    		<div class="control-group">
    			<div style="margin-top: 5px; margin-bottom: 5px;">
    		        <a id="linkEsqueceuSenha" href="#"><strong>Esqueceu sua senha?</strong></a>
    		    </div>
    		</div>
    		
    		<input type="hidden" name="targetUrlParameter" value="${targetUrlParameter}" />
		      
		</form>
		      
		<div class="alert alert-info centerAligned">
		  Ainda n�o tem conta no TripFans?<br/>
          <a id="linkCadastro" href="#divCadastro">
            <strong>Crie uma agora mesmo.</strong>
          </a>
		</div>
		
		</c:if>
       
      </div>
    
      <div id="divCadastro" class="${cadastro ? 'show' : 'hide hidden'}">
    
    	<div style="padding-bottom: 15px;">
		<h3 class="centerAligned">Crie uma conta no TripFans</h3>
		</div>
		<dt>
		    <div id="errorsCadastro" class="alert alert-danger" style="display: none;">
		        <a class="close" href="#">�</a>
		    </div>
		</dt>
            

		<c:if test="${usuario == null}">
		
		<form id="cadastroform" action="${signupUrl}" method="post">
		
            <div class="form-group control-group row" align="center">
			    <div class="col-sm-2"></div>
    		    <div class="col-sm-8">
    		        <input type="text" name="displayName" id="displayName" class="span7 form-control" placeholder="Nome" />
    		    </div>
                <div class="col-sm-2"></div>
			</div>
			
			<div class="form-group control-group row" align="center">
			    <div class="col-sm-2"></div>
    		    <div class="col-sm-8">
			        <input type="text" name="username" id="email" class="span7 form-control" placeholder="E-mail" value="${username}"/>
			    </div>
                <div class="col-sm-2"></div>
			</div>  
			
			<div class="form-group control-group row" align="center">
			    <div class="col-sm-2"></div>
    		    <div class="col-sm-8">
			        <input type="password" id="password" name="password" autocomplete="off" autocorrect="off" autocapitalize="off" class="span7 form-control" placeholder="<s:message code="label.password" />" />
			    </div>
                <div class="col-sm-2"></div>
			</div>
            
			<div class="control-group">
                <input type="radio" name="genero" id="generoRadios1" value="F">
			    <label for="generoRadios1" class="radio inline" style="display: inline-block; padding-left: 2px; font-weight: normal;">
				  Feminino
				</label>
                <input type="radio" name="genero" id="generoRadios2" value="M">
				<label for="generoRadios2" class="radio inline" style="display: inline-block; padding-left: 2px; font-weight: normal;">
				  Masculino
				</label>
			</div>
			
			<div class="form-group centerAligned">
			  <input type="submit" name="signUp" id="signUp" value="<s:message code="label.cadastreSe" />" class="btn btn-large btn-secondary span4" style="margin-right: 5px;"  data-loading-text="Aguarde..."/>
			  <a id="linkLogin" href="#">
                <strong>J� tenho cadastro</strong>
              </a>
			</div>
			
			<input type="hidden" name="targetUrlParameter" value="${targetUrlParameter}" />
		
		</form>
		
		<div class="alert alert-info centerAligned" style="text-align: justify;" >
		<p>
		      Ao proceder, voc� concorda com nossa
		      <a rel="nofollow" class="js_popup sz800x600" target="_blank" href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />"><strong>Pol�tica de privacidade</strong></a>
		e
		<a rel="nofollow" class="js_popup sz800x600" target="_blank" href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />"><strong>Termos de uso</strong></a>
			</p>
		</div>
		</c:if>
                
       
      </div>
      
      <div id="divSenha" class="hide hidden">
        <div style="padding-bottom: 15px;">
            <h3 class="centerAligned">Recupera��o de senha</h3>
        </div>
        
        <div id="divSolicitarEmail">
            <div class="alert alert-info centerAligned" style="text-align: justify;">
              <p>
                - Se voc� possui conta no <strong>Facebook</strong> ou <strong>Google</strong>, voc� pode fazer o login clicando em um dos bot�es ao lado 
                  e posteriormente redefinir sua senha diretamente em <strong>'Configura��es da Conta'</strong> do seu perfil.
              </p>
              <p>
                - Para receber um e-mail com um link para redefinir sua senha, digite o endere�o de e-mail que voc� usou para fazer o seu cadastro.
              </p>
            </div>
            
            <form id="recuperarEmailForm">
                <div class="form-group control-group">
                    <div class="controls">
                        <input type="text" name="emailRecuperacao" id="emailRecuperacao" class="span7 form-control" placeholder="Informe seu e-mail" />
                    </div>
                </div>
                
                <div align="center" style="width: 100%;">
                    <a id="botaoNaoContinuar" href="#" class="btn btn-default btn-large span2" style="float: center;">
                        Cancelar
                    </a>
                    <a id="botaoContinuar" href="#" class="btn btn-large btn-primary span2" style="float: center;">
                        Continuar
                    </a>
                </div>
            </form>
        </div>
        
        <div id="divConfirmarEmail">
            
        </div>
      </div>
    
    </div> <!-- wrapper -->
    
  </div>
    
    
    <!--ul>
        <li>
            <a id="link-forgot-passwd" href="https://www.google.com/accounts/recovery?service=mail&amp;continue=http%3A%2F%2Fmail.google.com%2Fmail%2F%3Fui%3Dhtml%26zy%3Dl" target="_top">
            N�o consegue acessar sua conta?
            </a>
        </li>
    </ul-->

</div>

<div class="span7 ${not empty usuario.id ? 'show' : 'hide hidden'}" align="center">
    <div class="horizontalSpacer"></div>
    Aguarde...
    <div class="horizontalSpacer"></div>
</div>

<%--c:url value="/signin" var="signinUrl" />
<c:url value="/signup" var="signupUrl" /--%>

<script>

function signInWithGoogle() {
    document.forms['form_google'].submit();
}

function signInWithFacebook() {
	document.forms['fb_signin'].submit();
}

function exibir($elemento) {
    $elemento.show();
    $elemento.removeClass('hide');
    $elemento.removeClass('hidden');
    $elemento.addClass('show');
}

function esconder($elemento) {
    $elemento.hide();
    $elemento.removeClass('show');
    $elemento.addClass('hide');
    $elemento.addClass('hidden');
}

function mostrarTelaCadastro() {
	esconder($('#divLogin'));
	esconder($('#divSenha'));
	exibir($('#divCadastro'));
	$('#displayName').focus();
}

function mostrarTelaLogin() {
    esconder($('#divCadastro'));
    esconder($('#divSenha'));
    exibir($('#divLogin'));
	$('#username').focus();
}

$(document).ready(function () {
    
    $('#username').focus();
	
	$('input, textarea').placeholder();
	
	$('#linkCadastro').click(function(event) {
		event.preventDefault();
		mostrarTelaCadastro();
	});
	
	$('#botaoCadastro').click(function(event) {
		event.preventDefault();
		mostrarTelaCadastro();
	});

    $('#linkEsqueceuSenha').click(function(event) {
        event.preventDefault();
        esconder($('#divLogin'));
        esconder($('#divCadastro'));
        exibir($('#divSenha'));
        $('#emailRecuperacao').focus();
    });
	
	$('#linkLogin').click(function(event) {
		event.preventDefault();
		mostrarTelaLogin();
	});
	
    $('#botaoContinuar').click(function(event) {
        event.preventDefault();
        if ($('#recuperarEmailForm').valid()) {
        	$('#divConfirmarEmail').load('<c:url value="/recuperarConta/verificarUsuario" />', {'emailRecuperacao': $('#emailRecuperacao').val()}, function(response) {
        	    $('#divSolicitarEmail').hide();
        	});
        } else {
        	$('#emailRecuperacao').focus();
        }
    });

    $('#botaoNaoContinuar').click(function(event) {
        event.preventDefault();
        $('#emailRecuperacao').val('');
		mostrarTelaLogin();
    });

    $('#recuperarEmailForm').validate({
        rules: {
            'emailRecuperacao': {
                required: true, 
                email: true 
            }
        }, 
        messages: {
            'emailRecuperacao': {
                required : '<s:message code="validation.username.required" />',
                email : '<s:message code="validation.username.email" />'
            }
        }
    });
    
    $('#loginform').validate({ 
        rules: {
            username: { 
                required: true, 
                email: true 
            }, 
            password: { 
                required: true,
                minlength : 6
            } 
        }, 
        messages: { 
            username: {
            	required : "<s:message code="validation.username.required" />",
                email : "<s:message code="validation.username.email" />"
            },
            password: {
                required : "<s:message code="validation.password.required" />",
                minlength : "<s:message code="validation.password.minlength" />"
            }
        } 
    });
    
    $("#loginform").unbind("submit");

    $("#loginform").submit(function(event) {
        
        $("#errors").hide();
        
        // stop form from submitting normally
        event.preventDefault(); 
        
        var form = $(this);
        
        if ($("#loginform").valid()) {
        	//$.fancybox.showActivity();
        	$('#signIn').button('loading');
        
            $.ajax({
                type: 'POST',
                url: document.forms['loginform'].action,
                data:  
                    form.serialize()
                ,
                success: function(data) {
                    //$('#signIn').button('reset');
                    if (data.error != null) {
                        $("#errors").empty().append(data.error);
                        $("#errors").show();
                        $('#username').focus();
                        $('#signIn').button('reset');
                    } else {
                    	var params = '';
                    	if (data.params != null) {
                    		params = data.params;
                    	}
                    	var sendViaAjax = ('${isAjaxRequest}' == 'true') && ('${targetUrlParameter}' !== '');
                    	if (sendViaAjax) {
                    	    $.ajax({
                    	        type: 'POST',
                                url: '${targetUrlParameter}',
                                success: function(response) {
                                    window.location.href = data.targetUrl;
                                }
                    	    });
                    	} else {
                    	    window.location.href = data.targetUrl;
                    	}
                    }
                }
            });
        } else {
            $('#signIn').button('reset');
        }
        
    });
    
    $("#cadastroform").validate({ 
        rules: {
        	displayName: { 
                required: true
            },
            username: { 
                required: true, 
                email: true 
            }, 
            password: { 
                required: true,
                minlength : 6
            }, 
            genero: { 
                required: true
            }
        }, 
        messages: {
        	displayName: {
            	required : "Voc� deve informar seu nome"
            },
            username: {
            	required : "<s:message code="validation.username.required" />",
                email : "<s:message code="validation.username.email" />"
            },
            password: {
                required : "<s:message code="validation.password.required" />",
                minlength : "<s:message code="validation.password.minlength" />"
            }, 
            genero: {
                required : "Selecione Feminino ou Masculino"
            }
        },
        errorPlacement: function(error, element) {
        		var parent = element.parents("div")[0];
        		error.appendTo(parent);
        }
    });
    
    $("#cadastroform").unbind("submit");

    $("#cadastroform").submit(function(event) {
    	
        $("#errors").hide();
        
        var buttonCadastro = $('#signUp');
        
        event.preventDefault(); 
        
        var form = $(this);
        
        var user = form.find('input[name=username]').val();
        var pass = form.find('input[name=password]').val();
        var displayName = form.find('input[name=displayName]').val();
        var genero = form.find('input:radio[name=genero]:checked').val();
        var targetUrl = form.find('input[name=targetUrlParameter]').val();
        
        if (form.valid()) {
        	
        	buttonCadastro.button('loading');
        	
            $.ajax({
                type: 'POST',
                url: document.forms['cadastroform'].action,
                data: {
                    username: user, 
                    password : pass,
                    displayName: displayName,
                    genero: genero, 
                   	targetUrlParameter : targetUrl
                },
                success: function(data) {
                    if (data.error != null) {
                        $("#errorsCadastro").empty().append(data.error);
                        $("#errorsCadastro").show();
                        $('#displayName').focus();
                        buttonCadastro.button('reset');
                    } else {
                    	var params = '';
                    	if (data.params != null) {
                    		params = data.params;
                    	}
                        window.location.href = data.targetUrl;
                    }
                }
            });
        } else {
        	buttonCadastro.button('reset');
        }
        
    });
    
    
    if ('${usuario.id}' != null && '${usuario.id}' != '') {
    	window.location.href = '<c:url value="/home"/>';
    }
        
});

</script>
</compress:html>