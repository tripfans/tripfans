<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div>

 <c:if test="${not empty usuarioRecuperacao}">
  <div id="divConfirmarRecuperacao">
    <div style="min-height: 200px;">
      <div class="well" style="margin-left: 10px; display: inline-table; min-width: 65%;">
        <div class="span2 col-md-2" style="float: left; margin-left: 0px; margin-top: 12px; width: 75px; display: inline-table;">
           <h2>
             É você?
           </h2>
        </div>
        <div class="span4 col-md-4" style="min-width: 240px;">
          <fan:userAvatar user="${usuarioRecuperacao}" orientation="horizontal" displayName="true" displayUsername="true" displayDetails="false" nameSize="span2" marginRight="8" enableLink="false" />
        </div>
      </div>
      
      <div align="center" style="width: 100%;">
        <a id="botaoNaoConfirmarUsername" href="#" class="btn btn-default btn-large span2 botaoVoltarConfirmacaoEmail" style="float: center;">
            Não
        </a>
        <a id="botaoConfirmarUsername" href="#" class="btn btn-large btn-primary span2" style="float: center;">
            Sim, continuar
        </a>
      </div>
    </div>
  </div>
  
  <div id="divInformarCodigo" class="hide hidden">
    <div style="min-height: 200px;">
        <div class="alert alert-info centerAligned" style="text-align: justify;">
          <p>
            Por favor, verifique seu e-mail.
            <br/>
            Informe abaixo o código de 8 digitos que você recebeu em em e-mail.
          </p>
        </div>
        <div>
            <div class="form-group control-group">
                <div class="controls" align="center">
                    <input type="text" name="codigoRecuperacao" id="codigoRecuperacao" class="span5 form-control" 
                           maxlength="8" placeholder="Informe o código recebido em seu e-mail"
                           style="padding: 10px; text-align: center; font-size: 18px; font-weight: bold;" />
                </div>
            </div>
            
            <div align="center" style="display: inline-table; width: 100%;">
                <a id="botaoConfirmarCodigo" href="#" class="btn btn-large btn-primary span2" style="float: center;">
                    Continuar
                </a>
            </div>            
        </div>
    </div>
  </div>
  
  <div id="divCodigoInformado" class="hide hidden">
  
  </div>
 </c:if>
 
 <c:if test="${empty usuarioRecuperacao}">
    <div class="alert alert-error centerAligned" style="text-align: justify;">
      <p>
        O e-mail informado não está cadastrado no TripFans.
        <br/>
        Por favor, verifique se você digitou o email corretamente.
      </p>
    </div>
    
    <div align="center" style="display: inline-table; width: 100%;">
        <a id="botaoVoltar" href="#" class="btn btn-primary btn-large span2 botaoVoltarConfirmacaoEmail" style="float: center;">
            Voltar
        </a>
    </div>    
 </c:if>
 
</div>

<script>

function retornarConfirmarUsername() {
	$('#divConfirmarEmail').html('');
	$('#emailRecuperacao').val('');
    exibir($('#divSolicitarEmail'));        
	$('#emailRecuperacao').focus();
}

$('.botaoVoltarConfirmacaoEmail').click(function (e) {
    event.preventDefault();
    retornarConfirmarUsername();
});

$('#botaoConfirmarUsername').click(function (e) {
    event.preventDefault();
	
	$.ajax({
        type: 'POST',
        url: '<c:url value="/recuperarConta/confirmarEmail" />',
        data: { 
            'emailRecuperacao': $('#emailRecuperacao').val(), 
        },
        success: function(data) {
            if (data.success) {
                esconder($('#divConfirmarRecuperacao'));
                exibir($('#divInformarCodigo'));
            }
        }
    });    	
	
});

$('#botaoConfirmarCodigo').click(function (e) {
    event.preventDefault();
    exibir($('#divCodigoInformado'));
    esconder($('#divInformarCodigo'));
    $('#divCodigoInformado').load('<c:url value="/recuperarConta/confirmarCodigo" />', 
        {
        	'emailRecuperacao': $('#emailRecuperacao').val(),
        	'codigoRecuperacao': $('#codigoRecuperacao').val()
        }, function(response) {
            
        }
    );
});

</script>