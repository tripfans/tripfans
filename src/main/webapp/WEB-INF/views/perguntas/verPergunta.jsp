<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

<tiles:putAttribute name="title" value="${pergunta.textoPergunta} - TripFans" />

<tiles:putAttribute name="stylesheets">
</tiles:putAttribute>

<tiles:putAttribute name="footer">
<script type="text/javascript">
<c:if test="${usuario != null}">
	$.validator.setDefaults({ ignore: [""] });
	$("#formResposta").validate({
	    rules: {
	        textoResposta: {
	        	required: true,
	        	minlength: 30,
	        	maxlength: 1000
	        }
	    },
	    messages: { 
	    	textoResposta: {
	        	required: "Escreva a sua resposta",
	        	minlength: "Sua resposta deve ter pelo menos 30 caracteres",
	        	maxlength: "Sua resposta não pode ter mais do que 1000 caracteres"
	        }
	    },
	    errorPlacement: function(error, element) {
	    	var parent = element.parents("div.controls")[0];
	    	error.appendTo(parent);
	    },
	    errorContainer: "#errorBox"
	});
	
	$('.formResposta').submit(function(event) {
        event.preventDefault();
        var idRender = $(this).find('input[type=hidden]:first').val();
        if ($(this).valid()) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(data) {
                	mostraRespostas('<c:url value="/perguntas/verRespostas/" />' + idRender, '#respostas-' + idRender);
                }
            });
        }
    });
</c:if>
</script>
</tiles:putAttribute>

<tiles:putAttribute name="body">
<div class="container-fluid">
<div class="block">
<div class="block_head">
    <div class="bheadl">
    </div>
    <div class="bheadr">
    </div>
    <h2>Perguntas e Respostas</h2>
</div>
<div class="block_content">
	<div class="span12 offset1">
	<div class="row">
		<div>
			<fan:userAvatar user="${pergunta.autor}" displayDetails="true" />
		</div>
		<div class="span10">
			<h3>
			<strong>${fn:replace(pergunta.textoPergunta, newLineChar, "<br />")}</strong>
			</h3>
			<c:if test="${pergunta.local != null}">
				<h5><strong>Sobre:</strong> <a href="<c:url value="${pergunta.local.url}"/>">${pergunta.local.nomeCompleto}</a></h5>
			</c:if>
			<p><small>Feita em <fan:passedTime date="${pergunta.data}"/></small></p>
			
			<c:choose>
				<c:when test="${pergunta.quantidadeRespostas == null || pergunta.quantidadeRespostas == 0}">Ninguém respondeu</c:when>
				<c:otherwise>
					<a id="${pergunta.urlPath}" href="<c:url value="/perguntas/verRespostas/${pergunta.urlPath}" />" class="verResposta" title="Clique para ver todas as respostas">
					<strong>${pergunta.quantidadeRespostas} resposta(s)</strong>
					</a>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
	<div class="pull-right"><a class="btn btn-mini btn-secondary" href="<c:url value="/perguntas/listar?start=0" />">Voltar para a lista perguntas</a></div>
	
	<div class="page-header">
	<a name="respostas"></a>
		<h3>Respostas</h3>
	</div>
	<div class="span9" id="respostas-${pergunta.urlPath}">
		<c:choose>
			<c:when test="${not empty respostas}">
				<jsp:include page="listaRespostas.jsp"></jsp:include>
			</c:when>
			<c:otherwise>
			<div class="help-block offset2">
				<p>Sem respostas, ainda.</p>
			</div>
			</c:otherwise>
		</c:choose>
	</div>
	<div>
	<c:if test="${usuario != null}">
		<jsp:include page="formResposta.jsp">
			<jsp:param value="${pergunta.urlPath}" name="pergunta"/>
			<jsp:param value="9" name="dataSize"/>
		</jsp:include>
	</c:if>
	</div>
	</div>
	
	<div class="span4 offset1">
		<c:if test="${not empty perguntas}">
		<div class="page-header"><h4>Perguntas Semelhantes</h4></div>
		<c:forEach items="${perguntas}" var="pergunta">
		<div class="span4">
		<a href="<c:url value="/perguntas/verPergunta/${pergunta.urlPath}" />">${pergunta.textoPergunta}</a>
		<c:if test="${pergunta.quantidadeRespostas != null}"><span class="badge badge-yellow pull-right">${pergunta.quantidadeRespostas} resposta(s)</span></c:if>
		<p><small>Feita em <fmt:formatDate value="${pergunta.data}" pattern="dd/MM/yyyy"/></small></p> 
		</div>
		</c:forEach>
		<div class="pull-right"><p><small><a href="<c:url value="/perguntas/listar/${pergunta.local.urlPath}" />">Ver outras desse mesmo local</a></small></p></div>
		<div class="horizontalSpacer"></div>
		</c:if>
		
		<fan:adsBlock adType="0"/>
		
	</div>
</div>
</div>
</div>

</tiles:putAttribute>

</tiles:insertDefinition>