<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<div class="page-header">
	<h4>Pesquisar Perguntas</h4>
</div>

<form action="#" method="post" id="formFiltro">
<div class="control-group">
<div class="controls">
<fan:autoComplete url="/pesquisaTextual/consultar" id="localPesquisa" hiddenName="local" valueField="id" onSelect="localSelecionado()"
labelField="nomeExibicao" minLength="3" cssClass="span4" blockOnSelect="false" placeholder="Fortaleza, Rio de Janeiro, França etc." 
tooltip="Digite o nome do local para fazer a pesquisa" />
</div>
</div>
</form>


<script type="text/javascript">
function localSelecionado(event, ui) {
	document.location.href= '<c:url value="/locais/"/>' + ui.item.urlPath + '?tab=perguntas';
}
</script>