<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16" style="margin-top: -15px;">
	<div class="page-header">
		<c:if test="${not empty perguntas && local.quantidadePerguntas > 1}">
		<c:url var="urlOrdenacao" value="/perguntas/listar/${local.urlPath}" />
		<fan:groupOrderButtons url="${urlOrdenacao}" targetId="secaoPerguntas" cssClass="pull-right span6">
			<fan:orderButton dir="desc" orderAttr="pergunta.data" title="Mais recentes" tooltip="Ordenar pelas mais recentes" />
				<fan:orderButton dir="desc" orderAttr="pergunta.quantidadeRespostas" title="Com Mais Respostas" tooltip="Ordenar pelas mais respondidas" />
		</fan:groupOrderButtons>
		</c:if>
		<h3>Perguntas</h3>
	</div>

	<div class="span12" style="margin-left: 0px;">
		<div id="secaoPerguntas">
		<c:choose>
			<c:when test="${not empty perguntas}">
				<jsp:include page="listaPerguntas.jsp" />
			</c:when>
			<c:otherwise>
                <div class="blank-state">
                    <div class="span2 offset2">
                        <img src="<c:url value="/resources/images/icons/mini/128/Communication-97.png" />" width="90" height="90" />
                    </div>    
                    <div class="span6" style="margin-top: 20px;">
                        <h3>Não há perguntas para exibir</h3>
                        <p>
                            Ainda não foram feitas perguntas sobre este local.
                        </p>
                        <a class="btn btn-secondary" href="<c:url value="/perguntas/perguntar?local=${local.urlPath}" />">Seja o primeiro a fazer uma pergunta</a>
                    </div>
                </div>    

                <div class="horizontalSpacer">
                </div>
			</c:otherwise>
		</c:choose>
		</div>
		
		 <c:if test="${not empty perguntas}">
			<jsp:include page="/WEB-INF/views/locais/botoesAcaoLocais.jsp">
				<jsp:param name="span" value="span12" />
			</jsp:include>
		</c:if>
	</div>
	<div class="span4" style="padding: 0px;">
      <c:if test="${not empty perguntas}">
		<div style="text-align: center;">
			<a class="btn btn-secondary btn-large" href="<c:url value="/perguntas/perguntar?local=${local.urlPath}" />">Faça uma Pergunta</a>
		</div>
      </c:if>
      <div class="horizontalSpacer"></div>
      <fan:adsBlock adType="1" />
	</div>
</div>