<%@page import="java.net.URL"%>
<%@page import="br.com.fanaticosporviagens.infra.component.SearchData"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="textoMostrar" value="Ver as respostas" />
<c:set var="tooltipMostrar" value="Clique para ver as respostas" />
<c:set var="textoEsconder" value="Esconder as respostas" />
<c:set var="tooltipEsconder" value="Clique para esconder as respostas" />

<div id="listaPerguntas">
    <c:forEach items="${perguntas}" var="pergunta" varStatus="contador" >
    	<fan:pergunta pergunta="${pergunta}" fullSize="11" dataSize="9" userSize="2"
                      mostraUsuario="true" mostraAcoes="true"
                      textoMostrar="${textoMostrar}" tooltipMostrar="${tooltipMostrar}" textoEsconder="${textoEsconder}" tooltipEsconder="${tooltipEsconder}" />
    </c:forEach>
    
    <div class="span12">
        <c:url var="urlPaginacao" value="/perguntas/listar" />
        <c:if test="${local != null}">
            <c:url var="urlPaginacao" value="/perguntas/listar/${local.urlPath}" />
        </c:if>
        <fan:pagingButton targetId="listaPerguntas" title="Ver Mais Perguntas" noMoreItensText="" cssClass="offset4" url="${urlPaginacao}" />
    </div>
</div>

<script type="text/javascript">


jQuery(document).ready(function() {
	
	$('a.verResposta').click(function(event) {
		event.preventDefault();
		var idRender = '#respostas-' + $(this).attr('id');
		if($(idRender).children().length == 0) {
			$(this).html('<i class="icon-chevron-up"></i> ${textoEsconder}');
			$(this).attr("data-original-title", "${tooltipEsconder}");
			mostraRespostas($(this).attr('href'), idRender);
		} else {
			if($(idRender).is(':hidden')) {
				$(this).html('<i class="icon-chevron-up"></i> ${textoEsconder}');
				$(this).attr("data-original-title", "${tooltipEsconder}");
				$(idRender).show();
			} else {
				$(this).html('<i class="icon-chevron-down"></i> ${textoMostrar}');
				$(this).attr("data-original-title", "${tooltipMostrar}");
				$(idRender).hide();
			}
		} 
	});
	
    $('.formResposta').submit(function(event) {
        event.preventDefault();
        var idForm = $(this).attr('id');
        var idRender = $(this).find('input[type=hidden]:first').val();
        var idBotao = $(this).find('input[type=submit]:first').attr('id')
        if ($(this).valid()) {
            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function(data) {
                	mostraRespostas('<c:url value="/perguntas/verRespostas/" />' + idRender, '#respostas-' + idRender);
                	resetForm(idForm);
                	$('#'+idBotao).prop('disabled', true);
                }
            });
        }
    });
    
});


</script>