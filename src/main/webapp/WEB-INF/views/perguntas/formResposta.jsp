<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<form action="<c:url value="/perguntas/responder" />" method="post" class="formResposta" id="formResposta-${param.pergunta}" style="margin-top: 10px; margin-bottom: 10px;">
      <div class="span9" style="margin-left: 0px;">
         	<fan:userAvatar user="${usuario}" displayName="false" displayDetails="false" showFrame="false" marginLeft="0" marginRight="5" imgSize="mini-avatar" />
      	<input type="hidden" name="pergunta" value="${param.pergunta}" />
          <textarea id="resposta-${param.pergunta}" name="textoResposta" placeholder="Escreva uma resposta" rows="3" class="span${param.dataSize -2}"></textarea>
          <input id="botao-${param.pergunta}" type="submit" value="Responder" class="btn btn-primary pull-right" disabled="disabled" />
      </div>
</form>

<script type="text/javascript">
 	$('#resposta-${param.pergunta}').bind('input propertychange', function() {
 		if(this.value.length) {
 			$('#botao-${param.pergunta}').prop('disabled', false);
 		} else {
 			$('#botao-${param.pergunta}').prop('disabled', true);
 		}
 	});
</script>