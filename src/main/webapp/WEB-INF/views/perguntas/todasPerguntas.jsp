<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

<tiles:putAttribute name="title" value="Perguntas - TripFans" />

<tiles:putAttribute name="stylesheets">
</tiles:putAttribute>

<tiles:putAttribute name="footer">
<script type="text/javascript">
jQuery(document).ready(function() {});

</script>
</tiles:putAttribute>

<tiles:putAttribute name="body">
<div class="container-fluid">
<div class="block">
<div class="block_head">
    <div class="bheadl">
    </div>
    <div class="bheadr">
    </div>
    <h2>Perguntas e Respostas</h2>
</div>
<div class="block_content">
<div class="span20">
<div class="span14">
<c:url var="urlOrdenacao" value="/perguntas/listar" />
<c:if test="${local != null}">
<c:url var="urlOrdenacao" value="/perguntas/listar/${local.urlPath}" />
</c:if>
<div class="row page-header">
	<c:if test="${not empty perguntas}">
	<fan:groupOrderButtons url="${urlOrdenacao}" targetId="secaoPerguntas" cssClass="pull-right">
		<fan:orderButton dir="desc" orderAttr="pergunta.dataPublicacao" title="Mais recentes" tooltip="Ordenar pelas mais recentes" />
		<fan:orderButton dir="desc" orderAttr="pergunta.quantidadeRespostas" title="Com Mais Respostas" tooltip="Ordenar pelas mais respondidas" />
	</fan:groupOrderButtons>
	</c:if>
	<h3>Perguntas</h3>
</div>
<div id="secaoPerguntas">
<c:choose>
	<c:when test="${not empty perguntas}">
		<jsp:include page="listaPerguntas.jsp" />
	</c:when>
	<c:otherwise>
	<div class="help-block offset2">
		<p>Ainda não existem perguntas</p>
	</div>
	</c:otherwise>
</c:choose>
</div>
</div>
<div class="span4" id="filtroPerguntas">
	<div style="text-align: center;">
		<a class="btn btn-secondary btn-large" href="<c:url value="/perguntas/perguntar" />">Faça uma Pergunta</a>
	</div>
	<div class="horizontalSpacer">
    	<fan:adsBlock adType="1" />
    </div>
</div>
</div>
</div>
</div>
</div>
</tiles:putAttribute>
</tiles:insertDefinition>