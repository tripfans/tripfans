<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Escreva sua Pergunta - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$("#formPergunta").validate({
					ignore: [],
			        rules: {
			        	textoPergunta: {
				        	required: true,
				        	minlength: 10,
				        	maxlength: 200
				        },
				        local: "required"
			        },
			        messages: { 
			        	textoPergunta: {
				        	required: "Escreva a sua pergunta",
				        	minlength: "Sua pergunta deve ter pelo menos 10 caracteres",
				        	maxlength: "Sua pergunta não pode ter mais de 200 caracteres"
				        },
				        local: "Você deve informar sobre qual destino é a sua pergunta",
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.control-group")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });
				
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">

		<c:set var="tituloEscreva" value="Escreva sua Pergunta" />
		<c:if test="${local != null}">
			<c:set var="tituloEscreva" value="Escreva sua Pergunta sobre ${local.nome}" />
		</c:if>
 			
        <div class="container-fluid">
            <div class="block">         
				<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
		            <h2>${tituloEscreva}</h2>
		        </div>
			
				<div class="block_content">
					<c:url var="urlPerguntar" value="/perguntas/perguntar/incluir" />
					<div class="span19">
						<div>			
							<form:form modelAttribute="pergunta" action="${urlPerguntar}" method="post" id="formPergunta">
								<s:hasBindErrors name="pergunta">
									<div id="errorBox" class="alert alert-error centerAligned"> 
									  <p><s:message code="validation.containErrors" /></p> 
									</div>
								</s:hasBindErrors>
								<div id="errorBox" style="display: none;" class="alert alert-error centerAligned">
    								<p id="errorMsg">
    								    <s:message code="validation.containErrors" />
    								</p>
								</div>
								<div id="success" style="display: none;" class="alert alert-success centerAligned">
    								<p id="successMsg">
    								</p>
								</div>
								
								<p class="lead">
    								Quer saber algo sobre o destino da sua próxima viagem? Então deixe a comunidade te ajudar.
    								Faça sua pergunta agora mesmo!
								</p>
						    	<div class="control-group">
    								<label for="pergunta" class="required">Sua pergunta</label>
    								<textarea id="pergunta" name="textoPergunta" style="width:550px;"></textarea>
								</div>
								<div class="control-group">
    								<label for="local" class="required">Sobre qual destino é sua pergunta</label>
    								<c:choose>
        								<c:when test="${local != null}">
            								<fan:autoComplete url="/pesquisaTextual/consultar" id="localPergunta" hiddenName="local" valueField="id" value="${local.nomeCompleto}" 
            								hiddenValue="${local.id}" labelField="nomeExibicao" minLength="3" cssClass="span6" showActions="true" placeholder="Exemplo: Fortaleza, Rio de Janeiro etc." />
        								</c:when>
        								<c:otherwise>
                                            <%-- showNotFound="true" notFoundUrl="/locais/sugerirLocal?acao=perguntar" --%>
            								<fan:autoComplete url="/pesquisaTextual/consultar" id="localPergunta" hiddenName="local" valueField="id" cssClass="span6"
                                                labelField="nomeExibicao" minLength="3" showActions="true" blockOnSelect="true" placeholder="Exemplo: Fortaleza, Rio de Janeiro etc." />
        								</c:otherwise>
    								</c:choose>
								</div>
								<div class=" form-actions rightAligned">
								    <input type="submit" id="botaoPerguntar" class="btn btn-primary btn-large" value="Perguntar" >
								</div>
							</form:form>
						</div>
					</div>
				</div>
			</div>
		</div>
			
	</tiles:putAttribute>

</tiles:insertDefinition>