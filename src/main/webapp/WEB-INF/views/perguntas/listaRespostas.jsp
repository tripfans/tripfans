<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div style="padding: 4px;"></div>
<c:forEach items="${respostas}" var="resposta" varStatus="contador" >
	<div class="row well" style="padding: 5px; margin-bottom: 10px;">
		<div>
			<fan:userAvatar orientation="horizontal" user="${resposta.autor}" displayDetails="true" displayName="true" imgSize="mini-avatar" />
		</div>
		<div class="span7" style="margin-left: 3px;">
			<span style="margin-top: 1px;">
				<p><small>Respondido <fan:passedTime date="${resposta.data}"/> </small></p>
	        </span>
	        <div class="textoUsuario">
	        ${fn:replace(resposta.textoResposta, newLineChar, "<br />")}
	        </div>
	        <c:if test="${usuario != null}">
		        <br/>
				<div class="row pull-right">
					<fan:votarUtilButton target="${resposta}" /> 
				</div>
			</c:if>
		</div>
	</div>
</c:forEach>

<script type="text/javascript">

</script>