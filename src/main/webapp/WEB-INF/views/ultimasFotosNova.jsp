<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.reveal_container.tofullwidth {
    visibility: hidden;
}

.reveal_container .reveal_wrapper {
    padding: 0px 20px 20px;
}
</style>
<div class="borda-arredondada" style="padding: 15px; display: inline-block; margin-left: 0px; position: relative; width: 97%; height: 450px;">
    
    <div>
    
        <div id="fotos-container" class="showbiz-container whitebg sb-retro-skin" style="padding-bottom: 0px;">

            <!-- THE NAVIGATION -->
            <div class="showbiz-navigation sb-nav-retro hidden-xs">
                <a id="showbiz_left" class="sb-navigation-left"><i class="sb-icon-left-open"></i></a>
                <%-->a id="showbiz_play" class="sb-navigation-play"><i class="sb-icon-play sb-playbutton"></i><i class="sb-icon-pause sb-pausebutton"></i></a--%>                    
                <a id="showbiz_right" class="sb-navigation-right"><i class="sb-icon-right-open"></i></a>
                <div class="sbclear"></div>
            </div> <!-- END OF THE NAVIGATION -->

            <div class="divide10"></div>

            <!--    THE PORTFOLIO ENTRIES   -->
            <div class="showbiz" data-left="#showbiz_left" data-right="#showbiz_right" data-play="#showbiz_play">

                <!-- THE OVERFLOW HOLDER CONTAINER, DONT REMOVE IT !! -->
                <div class="overflowholder">
                    <!-- LIST OF THE ENTRIES -->
                    <ul id="ul-list-fotos">
                    
                      <c:forEach items="${fotosViajantes}" var="foto">

                        <c:set var="urlAutor" value="/perfil/${foto.autor.urlPath}" />
                        <%-- Se for o perfil do usuario TripFans, remover link --%>
                        <c:if test="${foto.autor.id == 1}">
                          <c:set var="urlAutor" value="#" />
                        </c:if>

                        <li class="sb-retro-skin">
                            <!-- THE MEDIA HOLDER -->
                            <div class="mediaholder" style="border: 0px;">
                                <div class="mediaholder_innerwrap borda-arredondada">
                                    <i class="carrossel-thumb carrossel-home-img" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>"
                                       style="background-image: url('<c:url value="${foto.urlAlbum}"/>'); min-height: 200px;"></i>
                                    <!-- HOVER COVER CONAINER -->
                                    <div class="hovercover borda-arredondada" style="z-index: 21;">
                                        <a rel="group">
                                            <div class="linkicon" style="cursor: pointer;"><i class="sb-icon-plus morebutton" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>"></i></div>
                                        </a>
                                    </div><!-- END OF HOVER COVER -->
                                </div>
                            </div><!-- END OF MEDIA HOLDER CONTAINER -->

                            <!-- DETAIL CONTAINER -->
                            <div class="detailholder" style="margin-top: 10px;">
                                <fan:userAvatar displayName="false" user="${foto.autor}" marginLeft="0" showFrame="false" />
                                <p style="min-height: 50px; font-family: 'PT Sans Narrow', sans-serif;">
                                    <strong class="local-card-title" style="padding-top: 0px; margin-bottom: 0px;">
                                      <a href="<c:url value="/locais/${foto.local.urlPath}" />" style="text-decoration: none;">
                                        ${foto.local.nome}
                                      </a>
                                    </strong>
                                    <small>
                                        por
                                        <a href="<c:url value="${urlAutor}" />">
                                            ${foto.autor.displayName}
                                        </a>
                                    </small>
                                </p>
                                <p class="hidden-xs" style="font-family: 'PT Sans Narrow', sans-serif; z-index: 21;">
                                    <a id="btn-detalhar-foto-${foto.id}" class="btn btn-default morebutton" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>" href="#">
                                        Mais detalhes
                                    </a>
                                </p>
                            </div><!-- END OF DETAIL CONTAINER -->
                            
                            <c:if test="${not isMobile}">
                            
                              <!-- THE REVEAL CONTAINER - OPENING IN FULLWIDTH -->
                              <div id="reveal_container_${foto.id}" class="reveal_container tofullwidth">
                                
                                <!-- THE REVEL HIDDEN / VISIBLE CONTAINER -->
                                <div id="reveal_wrapper_${foto.id}" class="reveal_wrapper">
                                    <!-- THE HEIGHT ADJUSTER CONTAINER -->
                                    <div class="heightadjuster table row">

                                        <!-- TABLE CONSTRUCT FOR LEFT / RIGHT CONTENTS -->
                                        <div>
                                            <div class="span13 col-md-8" style="border: 1px solid #DDD; padding: 4px;">
                                                <i class="carrossel-big" data-foto-big-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>" style="height: 370px;"></i>
                                            </div>
                                        </div>

                                        <!-- CONTENT IN TABLE -->
                                        <div style="position: relative; float: left; margin-right: 0px;" class="span6 col-md-4">
                                            <p>
                                                <strong class="card-title" style="font-size: 26px;">
                                                  <a href="<c:url value="/locais/${foto.local.urlPath}" />">
                                                    ${foto.local.nome}
                                                  </a>
                                                </strong>
                                            </p>
                                            <div style="min-height: 60px;">
                                                <fan:userAvatar displayName="false" user="${foto.autor}" marginLeft="0" showFrame="false" />
                                                <small>
                                                    por
                                                    <a href="<c:url value="${urlAutor}" />">${foto.autor.displayName}</a>
                                                </small>
                                                <p style="min-height: 50px; font-family: 'PT Sans Narrow', sans-serif;">
                                                    <small>
                                                        <c:choose>
                                                            <c:when test="${not empty foto.data}">
                                                                <fan:passedTime date="${foto.data}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <fan:passedTime date="${foto.dataPostagem}"/>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </small>
                                                </p>
                                            </div>
                                            <div>
                                                ${foto.descricao}
                                            </div>
                                            <%-- div class="btn-group pull-left" style="margin-bottom: 5px;">
                                                <a href="<c:url value="/avaliacao/avaliar/${foto.local.urlPath}"/>" id="btn-avaliar-<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>.id}" class="btn btn-mini" title="Escreva uma avalia��o">
                                                  <img src="<c:url value="/resources/images/icons/award_star_gold_3.png" />">
                                                </a>
                                                <a href="<c:url value="/dicas/escrever/${foto.local.urlPath}"/>" id="btn-dica-${foto.local.id}" class="btn btn-mini" title="Escreva uma dica">
                                                  <img src="<c:url value="/resources/images/icons/lightbulb.png" />">
                                                </a>
                                            </div--%>
                                            <c:if test="${usuario != null}">
                                              <div class="btn-group pull-left" style="min-width: 250px; margin-left: 0px; margin-bottom: 10px;">
                                                <fan:marcarLocalButton usarBotoes="true" listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" cssButtonClassSize="mini" />
                                                <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" cssButtonClassSize="mini" />
                                                <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" cssButtonClassSize="mini" />
                                                <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" cssButtonClassSize="mini" />
                                              </div>
                                            </c:if>
                                            <div class="row" style="margin-left: 0px;">
                                                <div class="col-md-10 col-sm-4" style="margin-bottom: 10px;">
                                                    <a class="btn btn-default" href="<c:url value="/locais/${foto.local.urlPath}/avaliacoes" />" style="width: 159px;">Ver avalia��es deste local</a>
                                                </div>
                                                <div class="col-md-10 col-sm-4" style="margin-bottom: 10px;">
                                                    <a class="btn btn-default" href="<c:url value="/locais/${foto.local.urlPath}/dicas" />" style="width: 159px;">Ver dicas deste local</a>
                                                </div>
                                                <div class="col-md-10 col-sm-4" style="margin-bottom: 10px;">
                                                    <a class="btn btn-default" href="<c:url value="/locais/${foto.local.urlPath}/fotos" />" style="width: 159px;">Ver mais fotos deste local</a>
                                                </div>
                                                <!-- <div class="col-md-10 col-sm-4">
                                                    <a class="btn">Ver mais fotos deste usu�rio</a>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="theme1">
                                            <div class="closer close-reveal" data-foto-id="${foto.id}" style="margin-right: 0px; margin-top: 0px;"></div>
                                        </div>
                                        <!-- END OF CONTENT  -->

                                    </div><!-- END OF HEIGHT ADJUSTER CONTAINER -->
                                </div><!-- END OF REVEAL HIDDEN/VISIBLE CONTAINER -->

                                <!-- THE CLOSER / OPENER FUNCTION -->
                                <a class="reveal_opener opener_big_grey" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>">
                                    <span class="openme" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>">+</span>
                                    <span class="closeme">-</span>
                                </a><!-- END OF CLOSER / OPENER -->

                              </div><!-- END OF THE REVEAL CONTAINER -->
                            </c:if>
                        </li>
                        
                      </c:forEach>
                    </ul>
                    <div class="sbclear"></div>
                </div> <!-- END OF OVERFLOWHOLDER -->
                <div class="sbclear"></div>
            </div>
        </div>
        
    </div>
    
</div>