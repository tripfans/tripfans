<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<style>
.comentario {
    font-family: 'Georgia','Times',serif;
    font-style: italic;
    font-size: 16px;
    color: #686868;
}
</style>

<c:if test="${url == null}">
    <c:url value="/comentarios/postarComentario" var="url" />
</c:if>

<c:if test="${imgSize == null}">
    <c:url var="imgSize" value="avatar" />
</c:if>


<c:forEach items="${comentarios}" var="comentario">

<div id="box-comentario-${comentario.id}" class="well" style="padding: 6px 10px 4px 10px; margin-bottom: 8px;">

    <div id="box-comentario-content-${comentario.id}" class="box-comentario-content">
        <%@include file="comentario.jsp"%>
    </div>
    
    <sec:authorize access="isAuthenticated()">
      <div style="padding-left: 42px;">
        <hr style="margin-top: 2px; margin-bottom: 6px;" />
        <fan:formComentario tipo="${tipo}" id="${id}" idAlvo="${idAlvo}" idComentarioPai="${comentario.id}" actionsClass="${actionsClass}" 
                            imgSize="mini-avatar" inputClass="${inputClass}" inputType="text" url="${url}" buttonOnSameLine="true" />
      </div>
    </sec:authorize>
         
</div>

</c:forEach>

<c:if test="${empty comentarios}">
	<h4>N�o h� coment�rios publicados.</h4>
</c:if>

<c:url var="url" value="/comentarios/lista/${tipo}/${idAlvo}?id=${id}&imgSize=${imgSize}" />
<fan:pagingButton targetId="listaComentarios-${id}" limit="5" title="Ver mais coment�rios" noMoreItensText="" url="${url}" cssStyle="width: 95%;" />