<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<a name="${comentario.urlPath}"></a>

<div class="row" style="margin-left: -10px;">

    <div>
        <fan:userAvatar imgSize="${imgSize}" user="${comentario.autor}" displayName="false" displayDetails="true" marginRight="8" />
    </div>
    
    <span style="margin-top: 1px;">
    
        <span>
            <strong>
                <a href="<c:url value="/perfil/${comentario.autor.urlPath}" />" target="_blank">
                    ${comentario.autor.displayName}
                </a>
            </strong>
        </span>
        
        <br/>
        
        <span style="font-size: 11px; text-decoration: none;" class="grayLight">
            <fan:passedTime date="${comentario.data}" />
            <c:if test="${usuario != null && usuario.id == comentario.autor.id}">
                �
                <a class="excluir-comentario" data-comentario-id="${comentario.id}" href="#">Excluir</a>
            </c:if>
<%--                 
                <c:if test="${usuario != null && usuario.id != comentario.autor.id}">
                    �
                    <a href="#">Denunciar</a>
                </c:if>
                
 --%>            
        </span>
        
    </span>
    
    <br/>
    
    <q class="comentario">"${comentario.texto}"</q>

</div>

<div style="padding-left: 32px;">

    <c:forEach items="${comentario.comentarios}" var="subComentario">
    
      <div id="box-comentario-${subComentario.id}">

        <hr style="margin: 5px 5px 10px 0px;" />

        <div>
            <fan:userAvatar user="${subComentario.autor}" displayName="false" displayDetails="true" marginRight="8" imgStyle="width: 30px; height: 30px;" />
        </div>
        
        <span style="margin-top: 1px;">
    
        <span>
            <strong>
                <a href="<c:url value="/perfil/${subComentario.autor.urlPath}" />" target="_blank">
                    ${subComentario.autor.displayName}
                </a>
            </strong>
        </span>
        
        <br/>
        
        <span style="font-size: 11px; text-decoration: none;" class="grayLight">
            <fan:passedTime date="${subComentario.data}" />
            <c:if test="${usuario != null && usuario.id == subComentario.autor.id}">
                �
                <a class="excluir-comentario" data-comentario-id="${subComentario.id}" href="#">Excluir</a>
            </c:if>
<%--                 
                <c:if test="${usuario != null && usuario.id != subComentario.autor.id}">
                    �
                    <a href="#">Denunciar</a>
                </c:if>
                
 --%>            
        </span>
        
        <br/>
        
        <q class="comentario">"${subComentario.texto}"</q>
        
      </div>
    </c:forEach>
    
</div>