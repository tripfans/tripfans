<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

      	
<div id="browser-detection">
	<p class="lead">
  			Parece que você está usando um navegador <strong>desatualizado</strong>. Ele pode <strong>apresentar problemas</strong> para exibir algumas funcionalidades de nosso website. 
  			<br/>
  			<span class="alert-error" style="color: #b94a48;">
                 <strong>É extremamente recomendável que você atualize ou instale um novo navegador!</strong>
             </span>
 			<br/>
 			Para atualizar ou instalar um novo navegador, visite os sites oficiais dos navegadores populares abaixo:
	</p>
    <div class="row">
	  <ul class="bd-browsers-list" style="margin-left: 12%;">
			<li class="firefox"><a href="http://www.getfirefox.com/" target="_blank"></a></li>
			<li class="chrome"><a href="http://www.google.com/chrome/" target="_blank"></a></li>
			<li class="msie"><a href="http://www.getie.com/" target="_blank"></a></li>
			<li class="opera"><a href="http://www.opera.com/" target="_blank"></a></li>
			<li class="safari"><a href="http://www.apple.com/safari/" target="_blank"></a></li>
	  </ul>
	</div>
</div>
<div class="row" align="right">
	<a href="http://browser-update.org/pt/update.html" target="_blank" style="font-weight: bold; color: #E25600; font-size: 13px;">Porque devo atualizar meu navegador?</a>
</div>
<div class="row"  align="right">
	<button id="browser-detection-remind-later" class="btn btn-large btn-primary" style="font-size: 17px;">AGORA NÃO. Lembre-me mais tarde!</button>
</div>
    
<script type="text/javascript">
	$('#browser-detection-remind-later').on('click', function (e) {
		BrowserDetection.remindMe();
		$('#detectBrowserModal').modal('hide');
	});
</script>