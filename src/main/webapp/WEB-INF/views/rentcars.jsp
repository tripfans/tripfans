<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>

<!DOCTYPE html>
<html>
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
	<head>
		
        <c:if test="${not isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
        </c:if>
        <c:if test="${isMobile}">
          <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        </c:if>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="keywords" content="" />
        
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/tripfans.ico">
        
		<!-- Estilos essenciais -->
		<link rel="stylesheet" href="<c:url value="/wro/mainResponsive.css" />" type="text/css" />

		<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css' />
        <link href='https://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css' />
		
		<c:choose>
              <c:when test="${isMobile}">
                <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection"/>
              </c:when>
              <c:otherwise>
                <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection"/>
              </c:otherwise>
         </c:choose>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="<c:url value="/resources/components/bootstrap/bootstrap3/js/bootstrap.js" />"></script>
        <script src="<c:url value="/resources/scripts/fanaticos.js" />"></script>
        <script src="<c:url value="/resources/scripts/jquery-validation-1.9.js" />"></script>
        <script src="<c:url value="/resources/components/mmenu/jquery.mmenu.min.all.js" />"></script>
        
		<script src="http://whitelabel.mobicars.com.br/js/plugin.js" type="text/javascript"></script>
		

        <style>
        	        	
        	.rental-compare-title {
				margin-bottom: 7px;
				
			}
        	
        	.car-rentals-list {
				width: 100%;
				overflow: hidden;
				list-style: none;
				padding: 0;
				margin: 0;
			}
			
			.car-rentals-list > li {
				width: 79px;
				float: left;
				display: inline-block;
				overflow: hidden;
				margin-top: 10px;
				margin-right: 15px;
				-webkit-border-radius: 3px;
				border-radius: 3px;
				border: 1px solid #777;
			}
			
			.car-rentals-list > li img {
				width: 100%;
				display: block;
				-webkit-border-radius: 2px;
				border-radius: 2px;
			}
			
			.car-rentals-list>li, .car-rentals-list .avis {
				margin-left: 1px;
			}
			
			.car-rentals-list .avis {
				margin-left: 9px;
			}
			
			@media (min-width: 1200px){
				.car-rentals-list > li, .car-rentals-list .avis {
				margin-left: 20px;
				margin-right: 13px;
				}
            }
            
            @media (max-width: 767px) {
                .page-container {
                    min-height: 400px;
                }
            }
            @media (min-width: 768px) {
                .page-container {
                    min-height: 486px;
                }
            }
            
            .body {
            	font-family: 'PT Sans Narrow', "Helvetica Neue", Helvetica, Arial, sans-serif;
            }
        </style>
        
	</head>
    
    <body class="body">

		<div id="header">
             <%@include file="/WEB-INF/layouts/responsive/headerResponsive.jsp" %>
		</div>
		
        <div class="container">
            
            <div class="row page-container" style="background-color: white;">
            
                
                                
                <div class="row">  
                	<div class="text-center">   
                		<h1>Economize até 60% no aluguel de carro</h1>  
                		<img alt="Rentcars.com" title="Rentcars.com" class="booking" src="<c:url value="/resources/images/parceiros/parceria-rentcars.png" />">
                	</div> 
                </div>
                
			    <div class="center-block"  style="padding-top: 40px; margin-bottom: 40px;">
            		<div id="mobicar-widget-pesquisa" class="col-xs-6"></div>
            		<style>
            			.mobicar-widget-app {
						border: 2px solid #000000;
						
						font-family: 'PT Sans Narrow', "Helvetica Neue", Helvetica, Arial, sans-serif;
						position: relative;
						}
            		
            		</style>
            		
            		<div class="home-banners col-xs-6" style="position: relative; ">
				    <img src="http://www.rentcars.com.br/imagens/modules/home/new-banner-parcelamento-v3.png" alt="Garantia do Melhor Preço Rentcars.com" 
				    	style=" width: 90%; ">
<!-- 				    <img src="/imagens/modules/home/new-banner-parcelamento-v3.png" alt="Parcele em até 12x no Rentcars.com" style="position: absolute; top: 0px; left: 0px; display: block; z-index: 4; opacity: 1; width: 455px; height: 272px;"> -->
<!-- 				    <img src="/imagens/modules/home/new-banner-desconto-boleto-v2.png" alt="Desconto de 5% no pagamento com boleto" style="position: absolute; top: 0px; left: 0px; display: none; z-index: 3; opacity: 0; width: 455px; height: 272px;"> -->
				  </div>
				</div>
				
				
			    <div class="col-xs-12" style="padding: 30px 0 30px;">
				      <div class="rental-compare-title">
						  <div class="center-block text-center">
						    <h2>Compare os melhores preços em mais de 140 locadoras de veículos</h2>
						  </div>
					</div>
			
					<ul class="car-rentals-list">
					  <li class="avis"><img src="http://www.rentcars.com.br/imagens/locadoras/avis.svg" alt="Aluguel de carros na locadora Avis Rent a Car"></li>
					  <li class="localiza"><img src="http://www.rentcars.com.br/imagens/locadoras/localiza.svg" alt="Aluguel de carros na locadora Localiza Rent a Car"></li>
					  <li class="hertz"><img src="http://www.rentcars.com.br/imagens/locadoras/hertz.svg" alt="Aluguel de carros na locadora Hertz Rent a Car"></li>
					  <li class="movida"><img src="http://www.rentcars.com.br/imagens/locadoras/movida.svg" alt="Aluguel de carros na locadora Movida Rent a Car"></li>
					  <li class="alamo"><img src="http://www.rentcars.com.br/imagens/locadoras/alamo.svg" alt="Aluguel de carros na locadora Alamo Rent a Car"></li>
					  <li class="foco"><img src="http://www.rentcars.com.br/imagens/locadoras/foco.svg" alt="Aluguel de carros na locadora Foco Rent a Car"></li>
					  <li class="sixt"><img src="http://www.rentcars.com.br/imagens/locadoras/sixt.svg" alt="Aluguel de carros na locadora Sixt Rent a Car"></li>
					  <li class="dollar"><img src="http://www.rentcars.com.br/imagens/locadoras/dollar.svg" alt="Aluguel de carros na locadora Dollar Rent a Car"></li>
					  <li class="budget"><img src="http://www.rentcars.com.br/imagens/locadoras/budget.svg" alt="Aluguel de carros na locadora Budget Rent a Car"></li>
					  <li class="national hidden-xs"><img src="http://www.rentcars.com.br/imagens/locadoras/national.svg" alt="Aluguel de carros na locadora National Rent a Car"></li>
					</ul>
			    </div>
			  
				
				
				
			  
         </div>
            
		
        
        
        </div>
        
        
        <footer class="bs-footer hidden-print" role="contentinfo">

		
		
		
		<c:set var="footerClassWidth" value="col-xs-6" />
		<c:if test="${isMobile}">
			<c:set var="footerClassWidth" value="col-xs-4" />
		</c:if>

		<div style="background-color: #D1D1D1; padding: 6px;">
			<div class="container">
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Uso_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Termos de uso </a>
				</div>
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="<c:url value="/resources/files/Termo_Privacidade_TripFans.pdf" />" target="_blank" style="color: #7c7c7c;"> Política de privacidade </a>
				</div>
				<c:if test="${isMobile}">
				<div class="${footerClassWidth}" style="text-align: center;">
					<a href="#" style="color: #7c7c7c;"> Versão Desktop </a>
				</div>
				</c:if>
			</div>
		</div>

		<div style="background-color: #f5f5f5; padding: 6px;">
			<div class="container" style="text-align: center; color: rgb(185, 185, 185); font-size: 11px;">
				&copy; 2012-<%=java.util.Calendar.getInstance().get(java.util.Calendar.YEAR)%>
				TripFans - Todos os direitos reservados.
			</div>
		</div>

</footer>
		

		<div id="footer">

			<tiles:insertAttribute name="footer" />

			<script>
                    
                    
                    $(document).ready(function() {
                    
                    	$("#mobicar-widget-pesquisa").mobicarWidgetPlugin({
                    	     url: "tripfans.com.br",
                    	     id: 48,
                    	     BgColor: "#ffffff",
                    	     PxBorder: "2px",
                    	     ColorBorder: "#000000",
                    	     TypeBorder: "double",
                    	     btnBgColor: "#f4b717",
                    	     btnBorderColor: "#f4b717",
                    	     fontSize: "16px",
                    	     btnFontSize: "19px",
                    	     btnColor: "#ffffff",
                    	     lblColor: "#000000"
                    	  });
                    	
                        // Exibir o botão Entrar somente após carregar a página toda para não abrir o link quebrado em outra janela
                        $('.login-modal-link').show();
                        
                        window.onerror = function(m,u,l){
                            return true;
                        };
                        
                                                
                        $('.login-modal-link').click(function(e) {
                            e.preventDefault();
                            $('#modal-login-body').load('<c:url value="/entrarPop" />', function() {
                                $('#modal-login').modal('show');
                            });
                        });
                    });
                </script>

			
		</div>
        
	</body>
</compress:html>
</html>
