<%@page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="opcoes">

    <div class="span18 borda-arredondada module-box" style="margin: 0px; margin-left: 3%; padding: 20px; ">
        <div style="text-align: center;">
          <div class="row" style="margin-left: 20px">
            <c:set var="atracao" value="<%= LocalType.ATRACAO.getCodigo() %>" />
            <a id="${atracao}" class="btnTipoLocal" style="text-decoration: none;">
                <div class="span8 opcao-local">
                    <img src="<c:url value="/resources/images/locais/atracao.png" />" height="128"/>
                    <h2 class="atracao">Uma Atração</h2>
                    <p class="muted">
                        <%-- Escreva avaliações relatando suas experiências nos locais em que você já esteve. --%>
                    </p>
                </div>
            </a>
            <c:set var="hotel" value="<%= LocalType.HOTEL.getCodigo() %>" />
            <a id="${hotel}" class="btnTipoLocal" style="text-decoration: none;">
                <div class="span8 opcao-local">
                    <img src="<c:url value="/resources/images/locais/hotel.png" />" width="128" />
                    <h2 class="hotel">Um Hotel</h2>
                    <p class="muted">
                        <%-- Deixe dicas para outros viajantes.--%>
                    </p>
                </div>
            </a>
          </div>
          <div class="row" style="margin-left: 20px">
            <c:set var="restaurante" value="<%= LocalType.RESTAURANTE.getCodigo() %>" />
            <a id="${restaurante}" class="btnTipoLocal" style="text-decoration: none;">
                <div class="span8 opcao-local">
                    <img src="<c:url value="/resources/images/locais/restaurante.png" />" width="128" />
                    <h2 class="restaurante">Um Restaurante</h2>
                    <p class="muted">
                        <%-- Faça perguntas e deixe a comunidade ajudá-lo em suas próximas viagens.--%>
                    </p>
                </div>
            </a>
            <c:set var="cidade" value="<%= LocalType.CIDADE.getCodigo() %>" />
            <a id="${cidade}" class="btnTipoLocal" style="text-decoration: none;">
                <div class="span8 opcao-local">
                    <img src="<c:url value="/resources/images/locais/cidade.png" />" width="128" />
                    <h2 class="cidade">Uma Cidade</h2>
                    <p class="muted">
                        <%-- Crie seu mapa personalizado de viagens, dizendo onde já esteve, para onde quer ir e destinos favoritos.--%>
                    </p>
                </div>
            </a>
          </div>
        </div>
        
    </div>
	
</div>

<div id="div_opcao_selecionada" class="borda-arredondada module-box hide" style="margin-left: 5%; margin-right: 5%; min-height: 440px;">

  <div style="text-align: center;">
  
    <div id="icone_opcao_selecionada" class="row" style="margin: 50px;">
    </div>

    <div style="margin-left: 0px" align="center">
            
      <form style="width: 50%">
        <div id="div${atracao}" class="hidden">
            <span style="line-height: 1; font-size: 24px; margin: 40px 0 0px; font-weight: bold;">
                Escreva o nome da atração que você quer ${param.texto}
            </span>
            <c:set var="tipoLocal" value="<%=LocalType.ATRACAO.getUrlPath()%>"/>
            <fan:autoComplete url="/pesquisaTextual/consultarAtracoes" labelField="nomeExibicao" minLength="3" valueField="id"
                 onSelect="selecionaLocalAvaliacaoDica()" hiddenName="caixaPesquisaAtracao" id="caixaPesquisa${atracao}" position="collision: 'flip'"
                 showNotFound="true" notFoundUrl="/locais/sugerirLocal?tipoLocal=${tipoLocal}&acao=${param.acaoNovoLocal}"
                 cssStyle="width: 100%; height: 35px; font-size: 15px;" cssClass="span10" />
        </div>
        
        <div id="div${hotel}" class="hidden">
            <span style="line-height: 1; font-size: 24px; margin: 40px 0 0px; font-weight: bold;">
                Escreva o nome do hotel que você quer ${param.texto}
            </span>
            <c:set var="tipoLocal" value="<%=LocalType.HOTEL.getUrlPath()%>"/>
            <fan:autoComplete url="/pesquisaTextual/consultarHoteis" labelField="nomeExibicao" minLength="3" valueField="id"
                 onSelect="selecionaLocalAvaliacaoDica()" hiddenName="caixaPesquisaHotel" id="caixaPesquisa${hotel}" position="collision: 'flip'"
                 showNotFound="true" notFoundUrl="/locais/sugerirLocal?tipoLocal=${tipoLocal}&acao=${param.acaoNovoLocal}"
                 cssStyle="width: 100%; height: 35px; font-size: 15px;" cssClass="span10"/>
        </div>
        
        <div id="div${restaurante}" class="hidden">
            <span style="line-height: 1; font-size: 24px; margin: 40px 0 0px; font-weight: bold;">
            Escreva o nome do restaurante que você quer ${param.texto}
            </span>
            <c:set var="tipoLocal" value="<%=LocalType.RESTAURANTE.getUrlPath()%>"/>
            <fan:autoComplete url="/pesquisaTextual/consultarRestaurantes" labelField="nomeExibicao" minLength="3" valueField="id"
                 onSelect="selecionaLocalAvaliacaoDica()" hiddenName="caixaPesquisaRestaurante" id="caixaPesquisa${restaurante}" position="collision: 'flip'"
                 showNotFound="true" notFoundUrl="/locais/sugerirLocal?tipoLocal=${tipoLocal}&acao=${param.acaoNovoLocal}"
                 cssStyle="width: 100%; height: 35px; font-size: 15px;" cssClass="span10"/>
        </div>
        
        <div id="div${cidade}" class="hidden">
            <span style="line-height: 1; font-size: 24px; margin: 40px 0 0px; font-weight: bold;">
                Escreva o nome da cidade que você quer ${param.texto}
            </span>
            <fan:autoComplete url="/pesquisaTextual/consultarCidades" labelField="nomeExibicao" minLength="3" valueField="id" 
                 onSelect="selecionaLocalAvaliacaoDica()" hiddenName="caixaPesquisaCidade" id="caixaPesquisa${cidade}" position="collision: 'flip'"
                 cssStyle="width: 100%; height: 35px; font-size: 15px;" cssClass="span10"/>
        </div>
        <a href="#" id="linkMostraOpcoes" class="hidden pull-right"><strong>Quero ${param.texto} outro tipo de local</strong></a>
      </form>
    </div>        
  </div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('.btnTipoLocal').click(function(event) {
			event.preventDefault();
			$('#opcoes').hide();
			var id = $(this).attr('id');
			var $div = $('#div' + id);
			$div.parent().children().fadeOut('slow');
			$div.removeClass('hidden');
			
			$('#icone_opcao_selecionada').html($('#' + id).find('.opcao-local').html());
			$('#div_opcao_selecionada').fadeIn('slow');
			
			$div.fadeIn('slow');
			$('#linkMostraOpcoes').removeClass('hidden');
			$('#linkMostraOpcoes').fadeIn('slow');
			$('#caixaPesquisa' + id).focus();
		});
		
		$('#linkMostraOpcoes').click(function(event){
			$(this).parent().children().hide();
			$('#div_opcao_selecionada').hide();
			$('#opcoes').fadeIn('slow');
		});
	});
	
	function selecionaLocalAvaliacaoDica(event, ui) { 
	    if (ui.item.urlPath != null) {
	        document.location.href= '<c:url value="${param.url}"/>' + ui.item.urlPath;
	    } else {
	        document.location.href= '${pageContext.request.contextPath}' + ui.item.url;
	    }
	}
</script>