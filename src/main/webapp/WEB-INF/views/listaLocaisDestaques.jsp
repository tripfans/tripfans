<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<fan:listaLocais locaisEmDestaque="${locaisEmDestaque}" urlProximos="/home/maisDestinosEmDestaque?start=${proximosLocais}" 
                 style="${not isMobile ? 'padding-top: 15px;' : ''}" cardStyle="${isMobile ? 'padding-right: 0px; padding-left: 0px;' : ''}" />