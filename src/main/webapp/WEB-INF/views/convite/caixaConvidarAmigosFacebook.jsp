<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${not empty usuariosParaConvidar}">
<div class="page-container" style="margin-top: 12px;" id="caixaConviteAmigosFacebook">
	<div class="page-header module-box-header" style="margin: 0 0px 0px 0;">
		<h3>Convidar Amigos do Facebook</h3>
	</div>
	<div class="nano" style="height: 300px;">
		<div style="padding: 5px;" class="content" id="conteudoConviteAmigosFacebook">
			<c:forEach items="${usuariosParaConvidar}" var="usuario">
				<div class="row hover" style="padding: 6px;">
					<fan:userFacebookAvatar name="${usuario.nome}" id="${usuario.idFacebook}" displayDetails="false" displayName="false" />
					<span class="span2">${usuario.nome}</span>
					<button class="btn btn-small btn-primary right" 
							data-from="${perfil.idFacebook}" data-to="${usuario.idFacebook}" 
                            data-name="Junte-se ao TripFans - A sua rede social de viagens" 
                            data-description="O TripFans vai permitir a você fazer melhores escolhas na hora de viajar. Tudo feito com a ajuda de seus amigos." 
                            data-link="${tripFansEnviroment.serverUrl}/entrar?ucv=${idUsuarioCriptografado}&idf=${usuario.idFacebook}" 
                            data-picture="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans-vertical.png" 
							title="Convide ${usuario.nome} para entrar no TripFans"
					>
					Convidar</button>
				</div>
			</c:forEach>
		</div>
	</div>		
</div>


<jsp:include page="/WEB-INF/includes/facebookJSFuncs.jsp" />

<script type="text/javascript">

$('div#conteudoConviteAmigosFacebook button').click(function(event){
	var botao = $(this);
	facebookPostToFeed(botao, function() {
		botao.attr('disabled', 'disabled');
		botao.html('Enviado!');
	});
});

</script>
</c:if>