<%@page import="br.com.fanaticosporviagens.locais.DestinoView"%>
<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>


<tiles:insertDefinition name="tripfans.responsive.new">
	<tiles:putAttribute name="title" value="Destinos para sua pr�xima viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
		<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
				<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>


		<meta property="og:title" content="Destinos para sua pr�xima viagem - TripFans" />
		<meta property="og:url" content="http://www.tripfans.com.br" />
		<meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
		<meta property="og:site_name" content="TripFans" />
		<meta property="og:description" content="Encontre o destino para a sua pr�xima viagem!" />

	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="col-md-12">
			<div class="page-header">
				<h1>
					Escolha e explore o destino da sua pr�xima viagem!
					<small>Encontre hot�is, atra��es e restaurantes para visitar em sua pr�xima viagem.</small>
				</h1>
				<p style="font-size: 19px;">
					
				</p>
			</div>
		</div>
		
		<div class="horizontalSpacer"></div>
		
		<div class="col-md-12">
			<c:set var="secaoBrasil" value="<%=DestinoView.SECAO_BRASIL%>" />
			<c:set var="secaoALatina" value="<%=DestinoView.SECAO_AM_LATINA%>" />
			<c:set var="secaoEUACanada" value="<%= DestinoView.SECAO_EUA_CANADA %>" />
			<c:set var="secaoEuropa" value="<%= DestinoView.SECAO_EUROPA %>" />
			
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">${secaoBrasil}</h3>
			  </div>
			  <div class="panel-body">
			    <c:forEach items="${destinos}" var="destino">
			    	<c:if test="${destino.nomeSecao == secaoBrasil}">
			    		<c:set var="destino" value="${destino}" scope="request" />
						<jsp:include page="cardDestinoDestaque.jsp" />
			    	</c:if>
				</c:forEach>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">${secaoALatina}</h3>
			  </div>
			  <div class="panel-body">
			    <c:forEach items="${destinos}" var="destino">
			    	<c:if test="${destino.nomeSecao == secaoALatina}">
			    		<c:set var="destino" value="${destino}" scope="request" />
			    		<jsp:include page="cardDestinoDestaque.jsp" />
					</c:if>
				</c:forEach>
			  </div>
			</div>
		
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">${secaoEUACanada}</h3>
			  </div>
			  <div class="panel-body">
			    <c:forEach items="${destinos}" var="destino">
			    	<c:if test="${destino.nomeSecao == secaoEUACanada}">
			    		<c:set var="destino" value="${destino}" scope="request" />
						<jsp:include page="cardDestinoDestaque.jsp" />
			    	</c:if>
				</c:forEach>
			  </div>
			</div>
			
			<div class="panel panel-default">
			  <div class="panel-heading">
			    <h3 class="panel-title">${secaoEuropa}</h3>
			  </div>
			  <div class="panel-body">
			    <c:forEach items="${destinos}" var="destino">
			    	<c:if test="${destino.nomeSecao == secaoEuropa}">
			    		<c:set var="destino" value="${destino}" scope="request" />
						<jsp:include page="cardDestinoDestaque.jsp" />
			    	</c:if>
				</c:forEach>
			  </div>
			</div>
			
		</div>
		
		<compress:js enabled="true" jsCompressor="closure" >
		<script type="text/javascript">
            $(document).ready(function() {


            })

            function scrollToTop() {
	            $('html, body').animate({
		            scrollTop : 0
	            }, 'slow');
            }

		</script>
		</compress:js>
	</tiles:putAttribute>

</tiles:insertDefinition>