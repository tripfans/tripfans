<%@page trimDirectiveWhitespaces="true"%>
<% pageContext.setAttribute("newLineChar", "\r\n"); %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor"
	prefix="compress"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>


<tiles:insertDefinition name="tripfans.responsive.new">
	<tiles:putAttribute name="title" value="Monte seu roteiro de viagem para ${destino.nome}" />

	<tiles:putAttribute name="stylesheets">
		<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
				<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css"type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>

		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/style.css" />" media="screen" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/settings.css" />" media="screen" />

		<script type="text/javascript" src="<c:url value="/resources/components/showbizpro/js/jquery.themepunch.showbizpro.min.js" />"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/readmore.min.js" />"></script>
		

		<meta property="og:title" content="Monte seu roteiro de viagem para ${destino.nome}" />
		<meta property="og:url" content="http://www.tripfans.com.br/destinos/${destino.urlPath}" />
		<meta property="og:image" content="${destino.urlFotoBig}" />
		<meta property="og:site_name" content="TripFans" />
		<meta property="og:description" content="O TripFans ajuda voc� a organizar sua viagem para ${destino.nome}, tornando-a uma experi�ncia inesquec�vel!" />

		<style>
@media ( max-width : 420px) {
	section.cover {
		max-height: 250px;
	}
	section>span.coverText {
		padding: 10% 0;
	}
}

@media ( min-width : 421px) {
	section>span.coverText {
		padding: 5% 0;
	}
}

@media ( max-height : 768px) {
	section.cover {
		max-height: 360px;
	}
}
</style>


	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div style="min-height: 600px;">

			<div style="width: 100%;">
				<section class="cover" style="height: 200px; position: relative; overflow: hidden; background-color: transparent; margin-bottom: 10px;">
					<span class="coverText" style="margin: auto; width: 100%; position: absolute; z-index: 15; text-align: center;">
						<span style="color: white; font-size: 40px; font-weight: bold; text-shadow: 1px 1px 10px #000; text-transform: uppercase;">${destino.nome}</span><br />
						<span style="font-weight: bold; font-size: 19px; color: white; text-shadow: 1px 1px 6px #555;">${destino.nomePais}</span>
						<p style="padding-top: 10px;">
							<a href="<c:url value="/viagem/planejamento/criar?destino=${destino.urlPath}&?showMsgRoteiro=true"/>" class="btn btn-lg btn-warning" style="font-weight: bold; text-transform: uppercase;"> 
								Crie seu roteiro agora mesmo! 
							</a>
						</p>
					</span> <img class="picture" style="width: 100%; height: 100%" src="${destino.urlFotoBig}">
				</section>

			</div>


<!-- 			<ul id="tab-menu" class="nav nav-tabs"> -->
<!-- 				<li class="active"><a href="#home" aria-controls="home">In�cio</a></li> -->
<!-- 				<li><a href="#hospedagem" aria-controls="hospedagem">Onde ficar</a></li> -->
<!-- 				<li><a href="#atracoes" aria-controls="atracoes">O que Fazer</a></li> -->
<!-- 				<li><a href="#restaurantes" aria-controls="restaurantes">O que Comer</a></li> -->
<!-- 			</ul> -->

			<div class="tab-content">
				<div class="tab-pane active" id="home">
					
					<c:if test="${not empty destino.descricao}">
						<div class="col-md-12">
							<h2 class="page-header" style="margin-top: 5px;">Sobre ${destino.nome}</h2>
							<section id="descricao" data-readmore>
								<p style="font-size: 16px;">${fn:replace(destino.descricao, newLineChar, "<br />")}</p>
							</section>
						</div>
					</c:if>
					
					<c:if test="${not empty destino.roteirosDestaques}">
						<div class="col-md-12">
							<h2 class="page-header" style="margin-top: 10px;">Roteiros para ${destino.nome}</h2>
							<fan:roteirosDestaques roteiros="${destino.roteirosDestaques}" id="roteiros-container"></fan:roteirosDestaques>
						</div>
					</c:if>
				
				</div>

<!-- 				<div class="tab-pane" id="hospedagem"> -->
<!-- 					<div class="col-md-12"> -->
<%-- 						<h2 class="page-header" style="margin-top: 10px;">Melhores hospedagens em ${destino.nome}</h2> --%>
<%-- 						<fan:localEnderecavelDestaques locais="${destino.hoteisDestaques}" id="hospedagem" /> --%>
<!-- 					</div> -->
<!-- 				</div> -->

<!-- 				<div class="tab-pane" id="atracoes"> -->
<!-- 					<div class="col-md-12"> -->
<%-- 						<h2 class="page-header" style="margin-top: 10px;">Melhores Atra��es em ${destino.nome}</h2> --%>
<%-- 						<fan:localEnderecavelDestaques locais="${destino.atracoesDestaques}" id="atracoes" /> --%>
<!-- 					</div> -->
<!-- 				</div> -->

<!-- 				<div class="tab-pane" id="restaurantes"> -->
<!-- 					<div class="col-md-12"> -->
<%-- 						<h2 class="page-header" style="margin-top: 10px;">Melhores Restaurantes em ${destino.nome}</h2> --%>
<%-- 						<fan:localEnderecavelDestaques locais="${destino.restaurantesDestaques}" id="restaurantes" /> --%>
<!-- 					</div> -->
<!-- 				</div> -->

			</div>



		</div>

		<compress:js enabled="true" jsCompressor="closure">
			<script type="text/javascript">
			$(document).ready(function() {
				$('#descricao').readmore();
				
				$('#tab-menu a').click(function(e) {
					e.preventDefault();
					$(this).tab('show');
					var id = $(this).attr('aria-controls');
					jQuery('#local-' + id).showbizpro(
			           {
			               dragAndScroll: 'on',
			               slideAmount : 2, 
			               visibleElementsArray : [ 3, 2, 2, 1 ], 
			               carousel : 'on', 
			               entrySizeOffset : 0,
			               autoPlay : 'on', 
			               delay : 5000, 
			               speed : 300, 
			               rewindFromEnd : 'on'
			           }
			        );
				});
			});
            
            function scrollToTop() {
	            $('html, body').animate({
		            scrollTop : 0
	            }, 'slow');
            }

		</script>
		</compress:js>
	</tiles:putAttribute>

</tiles:insertDefinition>