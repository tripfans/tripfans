<%@page import="br.com.fanaticosporviagens.locais.DestinoView"%>
<%@page import="br.com.fanaticosporviagens.model.entity.DestinoViagem"%>
<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="col-md-3">
	<div class="card borda-arredondada module-box-content-list">
		<div class="card-header"> 
			<h3> 
				<a href="<c:url value="/destinos/${destino.urlPath}" />" class="card-title"> ${destino.nome} </a>
				<i class="flag flag-${fn:toLowerCase(destino.siglaIsoPais)}" title="${destino.nomePais}"></i>
			</h3>
		</div> 
		<div class="card-content" style="height: 180px;">
			<a href="<c:url value="/destinos/${destino.urlPath}" />">
			<img  style="width: 100%; height: 100%" src="${destino.urlFotoAlbum}" />
			</a>
		</div>
	</div>
</div>
