<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>


<tiles:insertDefinition name="tripfans.pageposter.responsive">
	<tiles:putAttribute name="title" value="Ter um roteiro de viagem nunca foi t�o f�cil - TripFans" />

	<tiles:putAttribute name="stylesheets">
		<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
				<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>

		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/style.css" />" media="screen" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/settings.css" />" media="screen" />
		

		<c:if test="${not unsuportedIEVersion}">
			<%-- script type="text/javascript" src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR"></script--%>
		</c:if>

		<script type="text/javascript" src="<c:url value="/resources/components/showbizpro/js/jquery.themepunch.showbizpro.min.js" />"></script>

		<meta property="og:title" content="TripFans | Fan�ticos por Viagens" />
		<meta property="og:url" content="http://www.tripfans.com.br" />
		<meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
		<meta property="og:site_name" content="TripFans" />
		<meta property="og:description"
			content="O TripFans ajuda voc� a organizar sua viagem, tornando-a uma experi�ncia inesquec�vel!" />

		<style>
@media ( min-width : 992px) {
	#formEncontrarDestino {
		width: 80%;
	}
	.carrossel-home-img {
		height: 300px;
	}
}

@media ( max-width : 991px) {
	#formEncontrarDestino {
		width: 100%;
	}
	.showbiz-container.whitebg {
		padding: 10px;
	}
	.divide20 {
		height: 0px;
	}
	.carrossel-home-img {
		height: 240px;
	}
}
</style>

	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="jumbotron" style="color: #fff; padding: 0;">
			<div id="poster"
				style="padding-left:15px; padding-right: 15px; background: no-repeat; 
				-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">

				<div style="text-align: center; padding: 10% 0; margin: auto;">
					<h3>
						<span style="color: white; font-size: 40px; font-weight: bold; text-shadow: 1px 1px 6px #555; text-transform: uppercase;">Ter um roteiro de viagem nunca foi t�o f�cil
						</span> <br />
					</h3>
					<div>
						<span style="font-weight: bold; font-size: 19px; color: white; text-shadow: 1px 1px 6px #555;"> Escolha o que fazer, receba dicas de seus amigos, sugest�es
							personalizadas, monte seu itiner�rio, inspire-se e fa�a uma �tima viagem. </span>
						<div class="row" style="text-align: center; margin-top: 15px;">
							<p>
								<a href="<c:url value="/viagem/planejamento/criar?showMsgRoteiro=true"/>" class="btn btn-lg btn-warning" style="font-weight: bold; text-transform: uppercase;"> Quero criar meu roteiro
									agora! </a>
							</p>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="container">

			<div class="row">
					<div class="col-md-12" style="padding-left: 0; text-align: center;">
						<h3><span style="color: #0981d1; font-size: 32px; font-weight: bold;"> Por que usar? </span></h3>
					</div>
			</div>

			<div class="row" style="margin-top: 40px;">
				<div class="col-md-4">
					<img src="<c:url value="/resources/images/home/tudo-organizado.png" />" width="200" class="img-responsive" style="margin: auto; opacity: 0.8;" /> 
					<span style="text-align: center;">
						<h2><strong>Tudo organizado</strong></h2>
						<p class="lead">Um �nico espa�o para os lugares a visitar, 
						reservas, anota��es e lembretes. Chega de criar v�rios documentos para organizar sua viagem!</p>
					</span>
				</div>

				<div class="col-md-4">
					<img src="<c:url value="/resources/images/home/viagem-perfeita.png" />" width="200" class="img-responsive" style="margin: auto; opacity: 0.8;" />
					    <span style="text-align: center;">
							<h2><strong>Sua viagem perfeita</strong></h2>
							<p class="lead">Aproveite ao m�ximo sua viagem. O TripFans te d� sugest�es de lugares para visitar.</p>
						</span>
				</div>

				<div class="col-md-4">
					<img src="<c:url value="/resources/images/home/roteiros-prontos.png" />" width="200" class="img-responsive" style="margin: auto; opacity: 0.8;" /> 
					<span style="text-align: center;">
						<h2><strong>Roteiros prontos para usar</strong></h2>
						<p class="lead">Inspire-se por quem j� esteve l�. Pegue roteiros prontos, copie e adapte-os como quiser.</p>
					</span>
				</div>

			</div>

			<div class="row" style="margin-top: 20px;">
				<div style="margin: auto; text-align: center;">
					<a href="<c:url value="/viagem/planejamento/criar?showMsgRoteiro=true"/>" class="btn btn-lg btn-warning" style="font-weight: bold; text-transform: uppercase;"> Quero criar meu roteiro agora!
					</a>
				</div>
			</div>


			<div class="row" style="margin-top: 40px; background-color: #fff;">
				<div style="text-align: center; padding-top: 10px;">
					<div class="col-md-12" style="padding-left: 0; text-align: center;">
						<h3 style="text-shadow: none; padding-bottom: 15px;">
							<span style="color: #0981d1; font-size: 32px; font-weight: bold;"> Roteiros em destaque </span> <br />
						</h3>
					</div>
				</div>
				<jsp:include page="roteirosDestaquesHome.jsp" />
			</div>



			<div class="row" style="margin: auto; margin-top: 40px; margin-bottom: 30px; text-align: center;">
				<h3 style="text-shadow: none; padding-bottom: 15px;">
					<span style="color: #0981d1; font-size: 32px; font-weight: bold;"> Conecte-se ao <img src="<c:url value="/resources/images/logos/tripfans-t.png" />" /> no Facebook
					</span>
				</h3>
				<div class="fb-like-box" data-href="https://www.facebook.com/TripFansBR" data-width="640" data-height="300" data-colorscheme="light" data-show-faces="true" 
					data-header="false" data-stream="false" data-show-border="false"></div>
			</div>

		</div>

		<compress:js enabled="true" jsCompressor="closure" >
		<script type="text/javascript">
            $(document).ready(function() {

            	setBackground();

	            $('#btn-criar-conta').click(function(e) {
		            e.preventDefault();
		            $('#modal-login-body').load('<c:url value="/cadastrar" />', function() {
			            $('#modal-login').modal('show');
		            });
	            });

	            
	            montarCarrossel();

	            $(document).on('click', '.conteudoRoteiro', function(e) {
		            e.preventDefault();
		            var urlRoteiro = $(this).data('urlroteiro');
		            window.location.href = urlRoteiro;
	            });

            })

            function setBackground() {
	            var imgs = [
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image001.jpg', position: '50% 80%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image002.jpg', position: '50% 90%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image003.jpg', position: '50% 75%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image004.jpg', position: '50% 75%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image005.jpg', position: '50% 90%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image006.jpg', position: '50% 49%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image007.jpg', position: '50% 65%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image008.jpg', position: '50% 30%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image009.jpg', position: '50% 55%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image010.jpg', position: '50% 80%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image011.jpg', position: '50% 45%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image012.jpg', position: '50% 75%'},
   	                {url: '${tripFansEnviroment.serverUrl}/resources/images/home/image013.jpg', position: '50% 80%'},
   	            ];
	            
	            var index = Math.floor((Math.random() * 100 % imgs.length));
	            
	            $('#poster').css('background-image', 'url("' + imgs[index].url + '")').css('background-position', imgs[index].position);
            }
            
            function scrollToTop() {
	            $('html, body').animate({
		            scrollTop : 0
	            }, 'slow');
            }

            function montarCarrossel() {
            	jQuery('#roteiros-container').showbizpro(
                   {
                       dragAndScroll: 'on',
                       slideAmount : 3, 
                       visibleElementsArray : [ 3, 2, 2, 1 ], 
                       mediaMaxHeight : [ 0, 0, 0, 0 ], 
                       carousel : 'on', 
                       entrySizeOffset : 0,
                       autoPlay : 'on', 
                       delay : 5000, 
                       speed : 300, 
                       rewindFromEnd : 'on'
                   }
                );
            }
		</script>
		</compress:js>
	</tiles:putAttribute>

</tiles:insertDefinition>