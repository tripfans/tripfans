<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div style="text-align: center; margin-top: -10px;">

<%--     <c:url var="urlPlano" value="/viagem/planejamento/inicio"/> --%>
<%--     <a href="${urlPlano}" style="text-decoration: none;" class="blackAndWhite colorOnHoverNoOpacity"> --%>
<!--         <div class="homeOpcaoAcao"> -->
<%--             <img src="<c:url value="/resources/images/planejamento.png" />" width="64" class="transparencia-75" /> --%>
<!--             <h2 class="plano">Crie o seu Plano de Viagem</h2> -->
<!--             <p class="muted"> -->
<!--                 Utilize nossa ferramenta de planejamento personalizada  -->
<!--                 e organize a sua próxima viagem de forma simples e intuitiva. -->
<!--             </p> -->
<!--         </div> -->
<!--     </a> -->

    <c:url var="urlMapa" value="/perfil/meuPerfil?tab=mapa"/>
    <c:if test="${usuario != null}">
        <c:url var="urlMapa" value="/perfil/${usuario.urlPath}/mapa"/>
    </c:if>
    <a href="${urlMapa}" style="text-decoration: none;" class="blackAndWhite colorOnHoverNoOpacity">
        <div class="homeOpcaoAcao">
            <img src="<c:url value="/resources/images/mapa.png" />" width="64" class="transparencia-75" />
            <h2 class="mapa">Marque no mapa os lugares em que você esteve</h2>
            <p class="muted">
                Crie seu mapa personalizado de viagens, dizendo onde já esteve, para onde quer ir e destinos favoritos.
            </p>
        </div>
    </a>
	<a href="<c:url value="/avaliacao/oQueAvaliar" />" style="text-decoration: none;" class="blackAndWhite colorOnHoverNoOpacity">
		<div class="homeOpcaoAcao">
			<img src="<c:url value="/resources/images/icons/mini/64/avaliacao.png" />" height="64" class="transparencia-75" />
			<h2 class="avaliacao">Escreva avaliações</h2>
			<p class="muted">
				Escreva avaliações relatando suas experiências nos locais em que você já esteve.
			</p>
		</div>
	</a>
	<a href="<c:url value="/dicas/sobreOQueEscreverDica" />" style="text-decoration: none;" class="blackAndWhite colorOnHoverNoOpacity">
		<div class="homeOpcaoAcao">
			<img src="<c:url value="/resources/images/icons/mini/64/dica.png" />" width="64" class="transparencia-75" />
			<h2 class="dica">Deixe dicas</h2>
			<p class="muted">
				Escreva dicas para tornar ainda melhor as viagens de seus amigos e de toda a comunidade.
			</p>
		</div>
	</a>
	<%-- a href="<c:url value="/perguntas/perguntar" />" style="text-decoration: none;">
		<div class="homeOpcaoAcao">
			<img src="<c:url value="/resources/images/icons/mini/64/pergunta.png" />" width="64" class="transparencia-75" />
			<h2 class="pergunta">Faça perguntas</h2>
			<p class="muted">
				Faça perguntas e deixe a comunidade ajudá-lo em suas próximas viagens.
			</p>
		</div>
	</a--%>
</div>