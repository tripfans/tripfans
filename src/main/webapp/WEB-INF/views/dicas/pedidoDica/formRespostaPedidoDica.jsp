<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.clean">

	<tiles:putAttribute name="title" value="Dica - TripFans" />

	<tiles:putAttribute name="stylesheets">
			<link rel="stylesheet" href="<c:url value="/resources/components/autosuggest/autoSuggest-custom-tripfans.css" />" type="text/css"/>
			<link rel="stylesheet" href="<c:url value="/resources/styles/jquery.gritter.css" />" type="text/css" />
			<link rel="stylesheet" href="<c:url value="/resources/styles/jquery.bxslider/jquery.bxslider.css" />" type="text/css" />
			
			<style type="text/css">
				ul.as-list {
					max-height: 150px;
					overflow-y: auto;
				}
			</style>
	</tiles:putAttribute>
	
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript" src="<c:url value="/resources/components/autosuggest/jquery.autoSuggest.tripfans.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-validation-1.9.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.scrollTo-1.4.3.1.js" />"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.gritter.js" />"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.bxslider/jquery.bxslider.js" />"></script>
	
		<script type="text/javascript">
			var tagsClassificacaoDica = new Array();
			<c:forEach items="${itensClassificacao}" var="item">
				tagsClassificacaoDica.push({'id': '${item.id}', 'descricao': '${item.descricao}'}) ;
			</c:forEach>
			
			var sliderRecomendacao;	
			
			jQuery(document).ready(function() {
				
				$("#formRespostaPedido").validate({
			        rules: {
			        	textoDica: {
			            	required: true,
			            	minlength: 10
			            }
			        },
			        messages: { 
			        	textoDica: {
			            	required: "<s:message code="validation.dica.descricao.required" />",
			            	minlength: "<s:message code="validation.dica.descricao.minlength" arguments="10" />"
			            }
			        },
			       errorPlacement: function(error, element) {
		        		var parent = element.parents("div")[0];
		        		error.appendTo(parent);
		        		element.focus();
			        },
			        invalidHandler: function(form, validator) {
			        	$.scrollTo($('#errorBox'), {offset: -50, duration: 500} );
			        },
			        errorContainer: "#errorBox"
			    });
				
				$('#tagsDica').focus(function(e) {
					var field = $(this);
					field.attr('placeholder', '');
					if(field.parent().get(0).tagName.toUpperCase() !== 'LI') {
						field. autoSuggest(tagsClassificacaoDica, {
					        asHtmlID : 'tags',
					        selectedValuesProp : 'id',// 'id',
					        selectedItemProp: 'descricao', 
					        searchObjProps: 'descricao',
					        startText: '',
					        emptyText: 'Nenhum resultado encontrado',
					        resultsHighlight: false
					    });
					}
				});
				
				// Chamada Ajax para carregar lugares que já visitou
				sliderRecomendacao = $('.locais-recomendacao').bxSlider({
					pager: false,
					minSlides: 1,
					maxSlides: 4,
					moveSlides: 1,
					slideMargin: 5,
					infiniteLoop: false,
					slideWidth: 280
				});
				
				// Chamada Ajax para carregar lugares mais bem avaliados
			 	
			 	$('#botao-cancelar-dica').click(function(e) {
			 		$('#modal-cancelar-dica').modal('show');
			 	})

			});
			
			$('.btn-recomendacao').on('click', function(e) {
				e.preventDefault();
				var btn = $(this);				
				$.post('<c:url value="/dicas/pedidoDica/recomendarLocal" />', {
					'idPedido' : btn.data('pedido'),
					'idLocal' : btn.data('local') 
				}, function(response) {
					btn.closest('.sliderLocal').remove();
					sliderRecomendacao.reloadSlider();
					if(sliderRecomendacao.getSlideCount() == 0) {
						$('div.bx-wrapper').remove();
						$('.wrapper-locais-recomendacao').html('<p class="lead text-center">Não há mais locais para recomendar.</p>');
					}
					$.gritter.add({
						title: response.title,
						text: response.message,
						time: 4000,
						image: '<c:url value="/resources/images/" />' + response.imageName + '.png',
					});
				});
				
			});
			
			$('#botao-salvar-dica').on('click', function(e){
				e.preventDefault();
				var btn = $(this); 
				btn.button('loading');
				var form = $('#formRespostaPedido');
				if(form.valid()) {
					$.post('<c:url value="/dicas/pedidoDica/responderPedidoDica" />', form.serialize(), function(response){
						$.gritter.add({
							title: response.title,
							text: response.message,
							time: 4000,
							image: '<c:url value="/resources/images/" />' + response.imageName + '.png',
						});
						form.find("input[type=text], textarea").val("");
						$('a.as-close').trigger('click');
						btn.button('reset');
					});
				} else {
					btn.button('reset');
				}
			});
			

		</script>
	</tiles:putAttribute>
	
	 <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
 			
        <div class="container">
				
			<div  style="margin-top: 5px; margin-bottom: 25px; padding: 10px 0 0 10px;">
	        <h3>
	        	<strong>Você está respondendo um pedido de dica de ${pedido.autor.displayName} sobre  ${pedido.localDica.nome}
	        	<small>Escreva quantas dicas achar necessário</small>
	        	</strong>
        	</h3>
	        
					<div class="" style="margin-top: 10px;">
						<c:if test="${not empty pedido.mensagem}">
						<div class="lead">Mensagem enviada por André Thiago: <span class="text-primary" style="font-size: 14px; ">${pedido.mensagem}</span></div>
						</c:if>
						
						<form class="form-horizontal" id="formRespostaPedido">
						
							<div id="errorBox" style="display: none;" class="alert alert-danger text-center"> 
								<p id="errorMsg">
						            <s:message code="validation.containErrors" />
								</p>
							</div>
							
							<fieldset>
								<input type="hidden" name="idPedido" value="${pedido.id}"/>
								
								<div class="form-group">
									<label for="dica" class="required col-xs-3 text-right">Sua dica</label>
									<div class="col-xs-9">
										<textarea id="textoDica" name="textoDica" class="form-control" rows="3"></textarea>
									</div>
								</div>
										
								<div class="form-group">
									<label class="col-xs-3 text-right">Informe tags para sua dica</label>
									<div class="col-xs-9">
										<input type="text" id="tagsDica" name="tags" class="form-control" placeholder="Digite para receber sugestões..." />
									</div>
								</div>
							</fieldset>
								
							<div class="form-actions text-right">
							     <input type="submit" id="botao-salvar-dica" class="btn btn-lg btn-primary" data-loading-text="Aguarde..." value="Responder" >
							</div>
						
						</form>
						<div class="horizontalSpacer"></div>
						<p class="lead"><strong>Conhece algum dos locais abaixo? Aproveite, também, e recomende-os para ${pedido.autor.displayName}</strong></p>
						
						<div class="wrapper-locais-recomendacao">
							<div class="locais-recomendacao">
								<c:forEach items="${locaisRecomendacao}" var="local">
										<div class="sliderLocal" id="${local.id}">
											<div class="thumbnail">
											 <div class="card-title" style="min-height: 65px;">${local.nome}</div>
											  <img class="thumbnail" src="${local.urlFotoAlbum}" style="height: 150px; width: 90%;">
											  <div class="caption text-center">
											    <p>
											    	<a href="#" class="btn btn-primary btn-recomendacao" data-pedido="${pedido.id}" data-local="${local.id}" role="button">
											    		<span class="glyphicon glyphicon-thumbs-up"></span> Recomendar
											    	</a>
										    	</p>
											  </div>
											</div>
										</div>
								</c:forEach>
							</div>
						</div>
						
						<div class="horizontalSpacer"></div>
						<div class="well text-center" style="margin-left: -5px;">
					        <a id="botao-finalizar" href="<c:url value="/dicas/pedidoDica/finalizar/${pedido.id}" />" data-loading-text="Aguarde..." class="btn btn-lg btn-success">
					        	<span class="glyphicon glyphicon-ok"></span> Finalizar
					        </a>
					        <button id="botao-cancelar" class="btn btn-default">Cancelar</button>
					    </div>
						
					</div>
					
			</div>
			
			<div>
			
			</div>
			
		
			 <div id="modal-cancelar-dica" class="modal hide fade">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Confirma</h3>
                </div>
                <div class="modal-body">
                    <p>Deseja realmente sair desta página e não responder ao pedido de dica de ${pedido.autor.displayName}?</p>
                </div>
                <div class="modal-footer">
                    <a id="confirmar-cancelar_dica" href="<c:url value="/perfil/${pedido.praQuemPedir.urlPath}/pedidosDica" />"  class="btn btn-primary">
                        <i class="icon-ok icon-white"></i> 
                        Sim
                    </a>
                    <a href="#" class="btn" data-dismiss="modal">
                        <i class="icon-remove"></i> 
                        Não
                    </a>
                </div>
            </div>
			
		
	</div>
			
	</tiles:putAttribute>

</tiles:insertDefinition>