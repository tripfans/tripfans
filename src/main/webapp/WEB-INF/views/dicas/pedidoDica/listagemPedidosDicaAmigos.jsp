<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.clean">

	<tiles:putAttribute name="title" value="Pedidos de dicas dos seus amigos - TripFans | Fanáticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
		<link rel="stylesheet" href="<c:url value="/resources/styles/jquery.gritter.css" />" type="text/css" />
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    	<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.gritter.js" />"></script>
    	
        <script type="text/javascript">
            $(document).ready(function () {
            	
            	<c:if test="${not empty statusMessage}">
				$.gritter.add({
					title: '<c:out value="${title}" />',
					text: '<c:out value="${statusMessage}" />',
					image: '<c:url value="/resources/images/${image}.png" />',
					time: 4000
				});
				</c:if>
            	
            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="col-md-12">
        	
       		<div class="row" style="margin-top: 5px; margin-bottom: 25px; padding: 10px 0 0 10px;">
       			<h2><strong>Pedidos de dicas dos meus amigos</strong></h2>
       		</div>
       		
       		<c:forEach items="${pedidos}" var="pedido">
       			<div class="panel panel-info" style="margin-bottom: 14px; ">
       				<div class="panel-heading">
       					<div class="panel-title">
		       				<div class="row">
			       				<div style="float:left; margin-left:5px; margin-right: 5px;">
				       				<img class="avatar borda-arredondada" style="background-color: #FEFEFE;" src="${pedido.urlFotoUsuarioPediu}" />
				       			</div>
				       			<h3><a href="<c:url value="/perfil/${pedido.idUsuarioPediu}" /> "><strong>${pedido.nomeUsuarioPediu}</strong></a></h3>
			       			</div>
		       			</div>
	       			</div>
	       			<div class="panel-body">
		       			<c:forEach items="${pedido.itens}" var="item" varStatus="status">
		       			<c:set var="border" value="border-top: 1px solid #bce8f1;" />
		       			<c:if test="${status.first}"><c:set var="border" value="" /></c:if>
	       				<div class="row" style="padding: 15px; ${border} ">
		       				<div class="col-md-6" style="">
			       				<p style="font-size: 18px; ">
			       					Quer dicas sobre <strong><a href="<c:url value="/locais/${item.urlPathLocalPedido}" />">${item.nomeLocalPedido}</a></strong>
			       				</p>
			       				Mensagem enviada:<p class="text-primary" style="font-size: 14px; ">${item.mensagemPedido}</p>
			       				<small>(<fan:passedTime date="${item.dataPedido}"/>)</small>
		       				</div>
		       				<div class="col-md-2 text-right">
		       					<c:choose>
		       					<c:when test="${not item.respondido}">
		       						<a class="btn btn-primary" data-responder="true" data-idpedido="${item.idPedido}" href="<c:url value="/dicas/pedidoDica/formRespostaPedidoDica/${item.idPedido}" />">Responder</a>
		       					</c:when>
		       					<c:otherwise>
		       						<div class="alert alert-success">
		       							<p class="lead"><span class="glyphicon glyphicon-ok"></span> Respondido</p>
		       						</div>
		       					</c:otherwise>
		       					</c:choose>
		       				</div>
	       				</div>
		       			</c:forEach>
	       			</div>
       			</div>
       		</c:forEach>
        </div>        
        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>