<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="modalPedirDica${usuario.id}" class="modal hide fade in">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h2>Você está pedindo uma dica sobre ${local.nome}</h2>
	</div>
    
	<div class="modal-body">
	    <div class="span9">
			<form id="formPedirDica${usuario.id}">
			
				<input type="hidden" name="quemPedir" value="${usuario.id}" />
				<input type="hidden" name="localDica" value="${local.id}" />
			
				<div class="control-group">
					<label class="control-label" for="textMessage">Mensagem:</label>
					<div class="controls">
						<textarea rows="3" name="mensagem" class="span8" placeholder="Envie uma mensagem (opcional) para ${usuario.displayName}"></textarea>
					</div>
				</div>
			
			</form>
		</div>
	</div>
	
	<div class="modal-footer">
		<div align="right">
			<button class="btn btn-large btn-primary" id="btnPedirDica${usuario.id}">Enviar Pedido de Dica</button>
			<button class="btn btn-large" id="btnCancelar${usuario.id}">Cancelar</button>
		</div>
	</div>
</div>

<script id="scriptFormPedirDica${usuario.id}" type="text/javascript">
jQuery(document).ready(function() {

	$('#btnPedirDica${usuario.id}').click(function (e) {
		e.preventDefault();
		$(this).attr("disabled", "disabled");
		var form = $('#formPedirDica${usuario.id}');
		
		$.post('<c:url value="/dicas/pedidoDica/pedirDica" />', form.serialize(), function(data) {
			$('#modalPedirDica${usuario.id}').modal('hide');
		});

	});

	$('#btnCancelar${usuario.id}').click(function (e) {
		e.preventDefault();
		$('#modalPedirDica${usuario.id}').modal('hide');
	});

});
</script>