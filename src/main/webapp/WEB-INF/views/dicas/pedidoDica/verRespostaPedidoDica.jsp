<%@page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.clean">

	<tiles:putAttribute name="title" value="Resposta do meu pedido de dica sobre ${nomeLocal} - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
		<link rel="stylesheet" href="<c:url value="/resources/styles/jquery.gritter.css" />" type="text/css" />
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    	<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.gritter.js" />"></script>
        <script type="text/javascript">
            $(document).ready(function () {
            	
            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="col-md-12">
        	
   			<div class="panel panel-default" style="margin-top: 14px; ">
   				<div class="panel-heading">
       					<div class="panel-title">
		       				<h3><strong>
		       					<a href="<c:url value="/dicas/pedidoDica/listarRespostasMeusPedidosDica" /> ">Respostas dos meus pedidos de dicas</a> > Resposta de ${nomeUsuarioRespondeu} sobre ${nomeLocal}
		       				</strong></h3>
		       			</div>
       			</div>
   				<div class="panel-body">
   					<p class="lead"><strong>Dicas Enviadas</strong></p>
   					<div class="row col-md-offset-1">
   					<c:forEach items="${resposta.dicas}" var="dica">
   							<fan:dica dica="${dica}" mostraTextoDicaSobre="true" mostraUsuarioEscreveu="false" mostraAcoes="true" fullSize="5" dataSize="5"/>
    				</c:forEach>
    				</div>
    				<!-- TODO carregar o conte�do abaixo via ajax -->
    				<c:if test="${not empty resposta.recomendacoesLocal}">
	    				<div class="horizontalSpacer"></div>
	    				<p class="lead"><strong>${nomeUsuarioRespondeu} tamb�m lhe recomendou os seguintes locais para voc� visitar</strong></p>
	    				<c:forEach items="${resposta.recomendacoesLocal}" var="recomendacao">
			     			<fan:cardResponsive type="local" local="${recomendacao.localRecomendado}" colSize="4" photoHeight="250" name="${recomendacao.localRecomendado.nome}" />
	    				</c:forEach>
    				</c:if>
    			</div>
   			</div>
   			
        </div>        
        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>