<%@page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.clean">

	<tiles:putAttribute name="title" value="Respostas dos meus pedidos de dicas - TripFans | Fanáticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
		
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {
            	
            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="col-md-12">
        	
   			<div class="panel panel-default" style="margin-top: 14px; ">
   				<div class="panel-heading">
       					<div class="panel-title">
		       				<h2><strong>Respostas dos meus pedidos de dicas</strong></h2>
		       			</div>
       			</div>
   				<div class="panel-body">
   					<c:forEach items="${pedidosRespondidos}" var="pedido" varStatus="status">
   						<c:set var="border" value=" border-top: 1px solid #DDDDDD;" />
		       			<c:if test="${status.first}"><c:set var="border" value="" /></c:if>
   						<div class="row" style="padding: 15px;${border}">
		     				<div class="col-md-9">
		     					<div style="float:left; margin-right: 5px;">
	     							<img class="avatar borda-arredondada" style="background-color: #FEFEFE;" src="${pedido.praQuemPedir.urlFotoSmall}" />
	     						</div>
	     						<p style="font-size: 21px;">
	     							<a href="<c:url value="/perfil/${pedido.praQuemPedir.urlPath}" />">${pedido.praQuemPedir.displayName}</a> 
	     							lhe enviou dicas sobre <a href="<c:url value="/perfil/${pedido.localDica.urlPath}" />">${pedido.localDica.nome}</a>
	     						</p> 
	     						<small style="color: #999999;"><fan:passedTime date="${pedido.dataResposta}"/></small>
	     					</div>
		     				<div class="col-md-2">
		     					<a class="btn btn-primary btn-lg"  style="font-weight: bold;" href="<c:url value="/dicas/pedidoDica/verResposta/${pedido.id}" />">Ver Resposta <span class="glyphicon glyphicon-chevron-right"></span></a>
		     				</div> 
    				</div>
    				</c:forEach>
    			</div>
   			</div>
   			
        </div>        
        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>