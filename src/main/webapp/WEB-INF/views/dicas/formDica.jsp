<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="br.com.fanaticosporviagens.model.entity.Dica"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Dica - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$("#dicaForm").validate({
			        rules: {
			            descricao: {
			            	required: true,
			            	minlength: 10
			            }
			            <c:if test="${local.tipoLocalEnderecavel}">
		            	,dicaSegura: "required"
		            	</c:if>
			        },
			        messages: { 
			            descricao: {
			            	required: "<s:message code="validation.dica.descricao.required" />",
			            	minlength: "<s:message code="validation.dica.descricao.minlength" arguments="10" />"
			            }
			        	<c:if test="${local.tipoLocalEnderecavel}">
		            	,dicaSegura: "Favor marcar o termo de compromisso "
		            	</c:if>
			        },
			        errorPlacement: function(error, element) {
			        	if($(element).attr('name') == 'dicaSegura') {
			        		var parent = element.parents("div")[0];
			        		$(parent).css('border', '2px solid red');
			        		error.appendTo(parent);
			        	} else {
			        		var parent = element.parents("li")[0];
			        		error.appendTo(parent);
			        	}
			        },
			        invalidHandler: function(form, validator) {
			        	$.scrollTo($('.block_head'), {offset: -50, duration: 500} );
			        },
			        errorContainer: "#errorBox"
			    });

			 	$("#dicaForm").initToolTips();
			 	
			 	$("#botao-salvar-escrever-mais").click(function(e) {
			 		if($('#dicaForm').valid()) {
			 			$('#dicaForm').attr('action', '<c:url value="/dicas/escrever/incluirEscreverOutra" />')
			 			$('#dicaForm').submit();
			 		}
			 	});
			 	
			 	$("#btnComoEscreverDicas").fancybox({
			 		'scrolling'		: 'no',
			 		'titleShow'		: false
			 	});
			 	
			 	$('#botao-cancelar_dica').click(function(e) {
			 		$('#modal-cancelar-dica').modal('show');
			 	})

			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
 			
        <div class="container-fluid">
            <div class="block">         
				<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
		            <h2><s:message code="dica.titulo" arguments="${local.nome}" /></h2>
		        </div>
			
				<div class="block_content">
					<div class="row">
						<div class="span13">			
							<form:form modelAttribute="dica" id="dicaForm" action="${pageContext.request.contextPath}/dicas/escrever/incluir" method="post" enctype="multipart/form-data">
                            
                                <fan:bodyFormDica local="${local}" nomeLocal="${local.nome}" mostrarUploadFotos="false" 
                                mainLabelSpanClass="span3" labelChkSpanClass="offset2  span3" fieldSpanClass="span8" />

							</form:form>
						</div>
						
						<div class="span6">
							<div style="text-align: center;">
							<a id="btnComoEscreverDicas" class="btn btn-large btn-secondary" href="#comoEscreverDicas">Como escrever uma boa dica?</a>
							</div>
							<div class="horizontalSpacer"></div>
							<s:message var="texto1" code="avaliacao.dicasPreenchimento" />
							<s:message var="texto2" code="formulario.camposObrigatorios" />
							<c:set var="texto1" value="<strong>${texto1}</strong><br/><br/>"  />
							<c:set var="texto2" value="<ul><li>${texto2}</li></ul>"  />
							<fan:helpText text="${texto1}${texto2}"  showQuestion="false" showCloseButton="false"/>
							
						
							<c:if test="${not empty amostraDicas}">
								<jsp:include page="/WEB-INF/views/locais/listaDicas.jsp" />
							</c:if>
						</div>
						
					</div>
				</div>
			</div>
			
			<div style="display: none;">
				<div class="span9 lead" id="comoEscreverDicas">
					<h2>Algumas dicas para escrever uma boa dica</h2>
					<div style="padding: 15px; font-size: 18px;">
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
						Uma dica é <strong>uma sugestão para tornar a visita do viajante ainda melhor naquele local</strong>. Algo que ajude o viajante a não perder tempo/dinheiro, 
						coisas que ele deve ou não deve fazer etc...
						</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							Escreva quantas dicas você quiser sobre este local.
						</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							Os seus amigos gostariam de saber <strong>se você gostou</strong> de ir a este local, <strong>se indica</strong> a visita,
							qual seria o <strong>melhor horário ou temporada</strong> para visitá-lo
						</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							Você pode dizer se é bom para ir <strong>sozinho, casal, família</strong> etc.
						</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							Você pode dizer se esse local é <strong>caro ou barato</strong>.
						</span>
						<br/>
						<c:if test="${local.tipoRestaurante}">
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Você pode sugerir um prato para ser pedido.
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Diga se é preciso fazer reserva.
							</span>
						</c:if>
						<c:if test="${local.tipoAtracao}">
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Diga se é preciso pagar para visitar essa atração.
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Dê dicas de como agilizar o passeio, evitar filas, etc
							</span>
						</c:if>
						<br/>
						<span class="red"><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							<strong>Evite palavras ofensivas</strong>
						</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							<strong class="red">Evite dicas promocionais</strong>, como por exemplo, que anunciem descontos ou contenham URLs, sites e telefones. 
							<strong class="red">Nos reservamos o direito de remover dicas com essas características.</strong>
						</span>
					</div>
				</div> 
			</div>
			
			 <div id="modal-cancelar-dica" class="modal hide fade">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Confirma</h3>
                </div>
                <div class="modal-body">
                    <p>Deseja realmente sair desta página e não escrever uma dica?</p>
                </div>
                <div class="modal-footer">
                    <a id="confirmar-cancelar_dica" href="<c:url value="/locais/${local.urlPath}" />" class="btn btn-primary">
                        <i class="icon-ok icon-white"></i> 
                        Sim
                    </a>
                    <a href="#" class="btn" data-dismiss="modal">
                        <i class="icon-remove"></i> 
                        Não
                    </a>
                </div>
            </div>
			
		</div>
			
	</tiles:putAttribute>

</tiles:insertDefinition>