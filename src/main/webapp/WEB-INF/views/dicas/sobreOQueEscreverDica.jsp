<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Sobre o que você quer escrever uma dica - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
			<div class="container-fluid page-container">
				<div class="title module-box-header">
					<span class="title">Sobre o que você quer escrever uma dica?</span>
				</div>
				
				<div id="pageContent" style="margin-top: 5%;">
                  	<div class="span20" style="margin-left: 0px;">
              			<jsp:include page="/WEB-INF/views/listaOpcoesAvaliacaoDica.jsp">
              				<jsp:param name="texto" value="escrever dica sobre" />
                            <jsp:param name="acaoNovoLocal" value="escreverDica" />
              				<jsp:param name="url" value="/dicas/escrever/"  />
              			</jsp:include>
                  	</div>
				</div>
				
			</div>
	
    </tiles:putAttribute>

</tiles:insertDefinition>