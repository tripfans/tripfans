<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div>
    <fan:dica dica="${dica}"/>   
</div>

<script>

	$(document).ready(function () {
		
		$('.votoUtil').bind('click', function (event) {

			event.preventDefault();

			var dicaUrlPath = $(this).attr('id');

			$.ajax({
			    type: 'GET',
			    url: this.href,
			    success: function(data) {
			        if (data != null) {
			        	$('#inputQuantidadeVotoUtil-' + dicaUrlPath).val(data.params.quantidadeVotoUtil);
			        }
			    }
			});
        });
	});


</script>