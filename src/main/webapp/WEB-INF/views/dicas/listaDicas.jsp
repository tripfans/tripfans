<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="listaDicas">
	<c:forEach items="${dicas}" var="dica" varStatus="contador" >
      <c:if test="${dicaDestaque == null or dicaDestaque.id != dica.id}">
		<fan:dica dica="${dica}" fullSize="12" userSize="2" dataSize="10" mostraTextoDicaSobre="true" mostraUsuarioEscreveu="true" mostraAcoes="true" />
      </c:if>						
	</c:forEach>
	
	<div class="row">
		<c:url var="urlDica" value="/dicas/listar/${local.urlPath}?item=${dicaDestaque.id}" />
		<fan:pagingButton limit="10" targetId="listaDicas" title="Ver Mais Dicas" noMoreItensText="" cssClass="span12" url="${urlDica}" />
	</div>
</div>