<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="listaDicas4sq">
  <c:if test="${not empty dicasFoursquare}">
	<c:forEach items="${dicasFoursquare}" var="tip" varStatus="contador" >
      <div class="row" style="margin-left: 0;">
        <fan:dica tip="${tip}" fullSize="12" userSize="1" dataSize="10" mostraTextoDicaSobre="true" mostraUsuarioEscreveu="true" mostraAcoes="true"/>
      </div>
      <hr style="margin: 10px;"/>
	</c:forEach>
  </c:if>
  <c:if test="${empty dicasFoursquare}">
    <div class="blank-state">
    
        <div class="span2 offset1">
            <img src="<c:url value="/resources/images/icons/mini/128/Objects-66.png" />" width="90" height="90" />
        </div>    

        <div class="span6" style="margin-top: 20px;">
            <h3>Não há dicas para exibir</h3>
            <p>
                Não foram encontradas dicas no <img src="<c:url value="/resources/images/logos/foursquare.png"/>" width="75" style="margin-top: -3px;" /> para este local.
            </p>
        </div>
    </div>    
  </c:if>
</div>