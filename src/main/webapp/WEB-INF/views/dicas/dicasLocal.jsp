<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16" style="margin-top: -15px;">

	<div class="span12" style="margin-left: 0px;">

      <div class="page-header">
          <c:if test="${not empty dicas && local.quantidadeDicas > 1}">
            <c:url var="urlDica" value="/dicas/listar/${local.urlPath}" />
            <fan:groupOrderButtons url="${urlDica}" targetId="listaAvaliacoes" cssClass="pull-right span5">
                <fan:orderButton dir="desc" orderAttr="dica.dataPublicacao" title="Mais recentes" tooltip="Ordenar pelas dicas mais recentes" />
                <fan:orderButton dir="desc" orderAttr="dica.quantidadeVotoUtil" title="Mais Úteis" tooltip="Ordenar pelas dicas mais úteis" />
            </fan:groupOrderButtons>
          </c:if>
          <h3>
              Dicas no <img src="<c:url value="/resources/images/logos/tripfans-t.png"/>" width="110" style="margin-top: -3px;" />
          </h3>
      </div>
    
      <c:if test="${dicaDestaque != null}">
          <fan:dica dica="${dicaDestaque}" fullSize="12" userSize="2" dataSize="10" mostraTextoDicaSobre="true" mostraUsuarioEscreveu="true" mostraAcoes="true"/>
      </c:if>
    
      <c:choose>
			<c:when test="${not empty dicas}">
				<jsp:include page="listaDicas.jsp" />
			</c:when>
			<c:otherwise>

                <div class="blank-state">
                    <div class="span2 offset2">
                        <img src="<c:url value="/resources/images/icons/mini/128/Objects-66.png" />" width="90" height="90" />
                    </div>    
                    <div class="span6" style="margin-top: 20px;">
                        <h3>Não há dicas para exibir</h3>
                        <p>
                            Não foram adicionadas dicas para este local.
                        </p>
                        <a class="btn btn-secondary" href="${pageContext.request.contextPath}/dicas/escrever/${local.urlPath}">Seja o primeiro a deixar uma Dica</a>
                    </div>
                </div>    

                <div class="horizontalSpacer">
                </div>

			</c:otherwise>
     </c:choose>
		
     <c:if test="${not empty dicas}">
		<jsp:include page="/WEB-INF/views/locais/botoesAcaoLocais.jsp">
			<jsp:param name="span" value="span12" />
		</jsp:include>
	 </c:if>
     
     <c:if test="${not empty local.idFoursquare}">
        <div class="span12" style="margin: -10px 10px 20px 0px;">
            <div class="page-header">
                <h3>
                  Dicas do 
                  <img src="<c:url value="/resources/images/logos/foursquare.png"/>" width="110" style="margin-top: -3px;" />
                </h3>
            </div>
            
            <div id="lista-4sq-tips" class="span12" style="margin-left: 0px;">
                <div class="span6" style="margin-top: 20px;">
                    <p>
                        <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
                        Carregando dicas do <img src="<c:url value="/resources/images/logos/foursquare.png"/>" width="75" style="margin-top: -3px;" /> ...
                    </p>
                </div>
            </div>
            <div class="pull-right">
                <img src="<c:url value="/resources/images/logos/poweredByFoursquare_gray.png" />" height="40" />
            </div>
            
        </div>
        
        <script>
            jQuery(document).ready(function() {
                $.get('<c:url value="/dicas/4sq/listar/${local.urlPath}"/> ',{},function(response) {
                    $('#lista-4sq-tips').html(response);
                    $('#lista-4sq-tips').initToolTips();
                });
            });
        </script>
    </c:if>
        
	</div>
    
	<div class="span4" style="margin-top: 25px;">
      <c:if test="${not empty dicas}">
		<div style="text-align: center;">
		    <a class="btn btn-secondary btn-large" href="${pageContext.request.contextPath}/dicas/escrever/${local.urlPath}">Deixe uma Dica</a>
		</div>
      </c:if>
      
      <div class="horizontalSpacer"></div>
      <fan:adsBlock adType="1" />
	</div>
</div>