<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Confirme seu cadastro - TripFans | Fan�ticos Por Viagens" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
    <div class="container">
        <div class="span20">
            <div class="page-container" style="margin-top: 50px;">    
            	<c:if test="${username != null and displayName != null}">
                	<div class="centerAligned" style="min-height: 70px; padding: 20px; font-size: 12pt;">
                	   <div class="alert span18">
                        <strong>Aten��o, ${displayName}:</strong> Acesse o seu e-mail ${username} para confirmar o seu cadastro.
                        <br/>
                        	<a href="<c:url value="/reenviarEmailConfirmacao/${urlPath}" />">Reenviar email de confirma��o</a>
                    	</div>
                	</div>
            	</c:if>
            </div>
        </div>
     </div>
    
    </tiles:putAttribute>
    
</tiles:insertDefinition>
 