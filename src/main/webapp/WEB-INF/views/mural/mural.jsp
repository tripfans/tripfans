<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="span8">

	<div class="row page-header">
		<h3>Mural</h3>
	</div>
	
		
		<form id="formMural">
		
			<div class="control-group">
			<label for="texto"><span class="comments" style="display: inline-block; height: 16px; width: 16px; margin-right: 4px;"></span>Escreva alguma coisa</label>
			<div class="controls">
			<textarea class="input-xxlarge" id="texto" name="texto" style="height: 40px;"></textarea>
			<a href="#" id="botaoCompartilhar" class="btn btn-small btn-primary">Compartilhe!</a>
			</div>
			</div>
			
		
		</form>
	
	<hr>
	<div class="row">
		<c:choose>
		<c:when test="${not empty itensMural}">
			<%@include file="listaItensMural.jsp" %>
		</c:when>
		<c:otherwise>
			<div class="help-block span4 offset3"><p>Nenhuma atividade encontrada.</p></div>
		</c:otherwise>
	</c:choose>
	
	</div>
	
</div>
