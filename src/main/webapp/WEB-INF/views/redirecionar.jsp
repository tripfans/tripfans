<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<tiles:insertDefinition name="tripfans.pageposter.responsive">
	<tiles:putAttribute name="title" value="Ter um roteiro de viagem nunca foi t�o f�cil - TripFans" />

	<tiles:putAttribute name="stylesheets">
		<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
				<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>

		<meta property="og:title" content="TripFans | Fan�ticos por Viagens" />
		<meta property="og:url" content="http://www.tripfans.com.br" />
		<meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
		<meta property="og:site_name" content="TripFans" />
		<meta property="og:description"
			content="O TripFans ajuda voc� a organizar sua viagem, tornando-a uma experi�ncia inesquec�vel!" />

		<style>
        @media ( min-width : 992px) {
            .media-link {
                padding-top: 4%;
                text-align: right;
            }
        }
        
        @media ( max-width : 991px) {
            .media-link {
                padding-top: 1%;
                text-align: center;
            }
            
            h2.media-heading, h3.bonus, div.bonus-text {
                text-align: center;
            }
        }
        </style>

	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container">
        
            <div class="row borda-arredondada" style="background: #EFEFEF; margin-top: 30px; padding: 20px; min-height: 90px;">
                <div class="col-md-3 media-middle">
                    <div style="margin-right: 10px; text-align: center;">
                        <a href="${urlParceiro}">
                          <img src="<c:url value="/resources/images/parceiros/${imgParceiro}"/>" alt="${nomeParceiro}" class="img-rounded" style="-webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.1); box-shadow: 0 1px 2px rgba(0,0,0,0.1);" />
                        </a>
                    </div>
                </div>
                <div class="col-md-6 media-middle">
                  <a href="${urlParceiro}" style="text-decoration: none;">
                    <h2 class="media-heading" style="text-transform: uppercase; color: #337ab7;">
                      ${nomeParceiro}
                    </h2>
                  </a>
                  
                  <h3 class="bonus">
                    <span class="label label-success" style="font-weight: normal;">At� <span style="font-weight: bold; font-size: x-large;">${percentualCashbackParceiro}</span> do dinheiro de volta</span>
                  </h3>
                  <div class="bonus-text" style="padding-top: 10px;">                          
                    <p style="color: #4D4D4D; font-size: 16px;">${textoParceiro}</p>
                  </div>
                </div>
                <div class="col-md-3 media-link media-middle">
                    <a href="${urlParceiro}" class="btn btn-lg btn-primary">
                        Ir para ${nomeLinkParceiro}
                    </a>
                </div>
            </div>        
        
          <div style="height: 300px;">
          </div>
        </div>
	</tiles:putAttribute>

</tiles:insertDefinition>