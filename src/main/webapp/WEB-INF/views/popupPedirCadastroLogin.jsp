<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="pedirCadastroModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 630px; margin-left: -285px;">
	<div class="modal-header">
    	<span style="font-size: 30px; font-weight: bold;">Junte-se ao TripFans e aproveite melhor suas viagens</span>
  	</div>
  	<div class="modal-body">
  		<div class="row" style="margin-left: 0px;">
			<div class="span3" style="margin-left: 0px;">
				<img src="<c:url value="/resources/images/logos/tripfans-vertical.png" />" style="max-width: 190px;" />				
			</div>
			<div class="span7 lead" style="margin-left: 35px; margin-right: 0px;">
				<div>
					<span style="font-size: 25px;">Cadastre-se agora mesmo, faça seu login e faça parte da mais nova rede de viagens do Brasil. <strong>É grátis.</strong></span>
				</div>
				<div style="padding: 15px;">
					<span>Por que devo me cadastrar e navegar autenticado?</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>Navegando autenticado, você pode interagir com seus amigos em uma experiência única de viagem.</span>
						<br/>
						<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							Você terá acesso a avaliações e dicas de amigos e outras funcionalidades que só estão disponíveis aos membros cadastrados.
						</span>
					
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer"">
		<button id="popupBotaoLogin" class="btn btn-large btn-primary" style="font-size: 17px;">Fazer Login</button>
		<button id="popupJaCadastrado" class="btn btn-large btn-secondary" style="font-size: 17px;">Já sou cadastrado</button>
		<button id="popupBotaoDepois" class="btn btn-large btn-success" style="font-size: 17px;">AGORA NÃO. Lembre-me mais tarde!</button>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
	$('#popupBotaoLogin').click(function (e) {
		$('#pedirCadastroModal').modal('hide');
		$.cookie('usuario_cadastrado_tripfans_lembrar_depois', 'true', {expires: 1, path: '/'});
		$('#login-dialog-link').click();
	});
	
	$('#popupJaCadastrado').on('click', function (e) {
		$('#pedirCadastroModal').modal('hide');
		$.cookie('usuario_cadastrado_tripfans', 'true', {expires: 30, path: '/'});
		$('#login-dialog-link').click();
	});
	
	$('#popupBotaoDepois').click(function (e) {
		$('#pedirCadastroModal').modal('hide');
		$.cookie('usuario_cadastrado_tripfans_lembrar_depois', 'true', {expires: 1, path: '/'});
	});
});

</script>