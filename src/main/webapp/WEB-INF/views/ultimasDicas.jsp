<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<ul class="list">
  <c:forEach items="${dicasViajantes}" var="dica">
    <li style="min-height: 80px;">
      <div style="padding: 2px 5px 0px 5px;">
        <div class="left" style="width: 65px">
            <fan:userAvatar user="${dica.autorDica}" displayName="false" displayDetails="true" marginLeft="0" marginRight="8" />
        </div>
        <div style="overflow: hidden; text-overflow: ellipsis; word-wrap: break-word; max-width: 100%; margin-bottom: 0px; max-height: 60px; min-width: 70%; text-align: justify;">
            <p style="max-height: 51px; line-height: 14px;">
                <small>
                    <a href="<c:url value="/perfil/${dica.autorDica.urlPath}" />">
                        ${dica.autorDica.nome}
                    </a>
                    <span class="gray">sobre</span>
                    <a href="<c:url value="/locais/${dica.urlPathLocal}" />">
                        ${dica.nomeLocal}
                    </a>
                </small>
                <br/>
                <small>
                  <a href="<c:url value="/locais/${dica.urlPathLocal}?tab=dicas&dica=${dica.urlPathDica}" />" style="color: #999;">
                    ${dica.textoDica}
                  </a>
                </small>
            </p>
        </div>
        <div class="pull-right">
            <p style="margin: 0 0 3px;">
                <small>
                    <a href="<c:url value="/locais/${dica.urlPathLocal}?tab=dicas&dica=${dica.urlPathDica}" />">
                        Ler mais...
                    </a>
                </small>
            </p>
        </div>
      </div>
    </li>
  </c:forEach>
</ul>