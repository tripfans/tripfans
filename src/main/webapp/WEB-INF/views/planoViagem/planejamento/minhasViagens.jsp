<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">

        <script type="text/javascript">
        
            $(document).ready(function () {
            	/*
                var tab = '${param.tab}';
                if (tab == 'perfil' ) {
                    carregarPagina('informacoesBasicas');
                    $('#informacoesBasicas').parent().addClass('active');
                } else if (tab == 'config') {
                    carregarPagina('geral');
                    $('#geral').parent().addClass('active');
                } else if (tab == 'privacidade') {
                    carregarPagina('visibilidade');
                    $('#visibilidade').parent().addClass('active');
                }
                */
                $('a.menu-item').bind('click',function(event) { //When any link is clicked
                	$('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    $(this).parent().addClass('active'); //Set clicked link class to active
                    carregarPagina(this.id);
                });
                
            });
            
            function carregarPagina(pagina) {
            	if (pagina=="viagem"){}else{
	            	$("#conteudo").load('<c:url value="/planoViagem/" />' + pagina);
            	}
            }
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <meta http-equiv="cache-control" content="no-cache,must-revalidate">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="pragma" content="no-cache">

        <div class="container-fluid">
        
            <div class="block withsidebar">
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>Viagens de ${usuario.displayName}</h2>
                </div>
                <div class="block_content">
                    <div class="sidebar">
                        <ul class="sidemenu">
                            <li class="nav-header">
                                &nbsp;&nbsp;VIAGENS
                            </li>
                        
                        	<li>
                                <a id="viagem" href="<c:url value="/planoViagem/viagem"/>" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Planeje uma Nova Viagem</span>
                                </a>
                            </li>
                            <li>
                                <a id="viagensFuturas" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/camera.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
                                    <span>Viagens Futuras</span>
                                </a>
                            </li>
                            <li>
                                <a id="viagensAntigas" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Histórico de Viagens</span>
                                </a>
                            </li>
                            <li class="nav-header">
                                &nbsp;&nbsp;ORGANIZAÇÃO
                            </li>
                            <li>
                                <a id="calendario" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/group.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
                                    <span>Sincronize com o Calendário do Google</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="sidebar_content">
                        <div id="conteudo">
                        </div>
                    </div>
                </div>
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>            
            </div>
        
            
            
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>