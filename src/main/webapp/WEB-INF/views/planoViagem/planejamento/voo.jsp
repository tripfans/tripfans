<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Voo" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
			jQuery(document).ready(function() {
				
			    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
			    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
			    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
				
				$('#vooForm').validate({
			        rules: {
			            codigoReserva: 'required',
			        	emailContato: { 
		                	email: true
		            	},
		            	/*dataInicio: {
		                    date: true
		                },		            	
		                dataFim: { 
                            date: true
                        },
                        horaInicio: { 
                            time: true
                        },
                        horaFim: { 
                            time: true
                        },*/
                        dataReserva: { 
                            date: true
                        }
			        },
			        messages: { 
			        	nomeLocal: "<s:message code="validation.atividade.nomeLocal.required" />",
			        	emailContato:"<s:message code="validation.atividade.emailContato.valid" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });
				
				$('input:text').setMask();
				
				$('#botaoSalvar').bind('click', function(event) {
				    event.preventDefault();
				    $('#vooForm').submit();
				});
				
			 	$('#vooForm').initToolTips();

			 	$('#observacoes').stickySidebar();
			 	
				$('#adicionar-trecho').bind('click', function(event) {
					
                    event.preventDefault();
                    var indice = $(this).attr('indice-trecho');
                    var quantidadeDias = $(this).attr('quantidade-dias');
                    var tipoPeriodo = $(this).attr('tipo-periodo');
                    
                    $('#lista-trechos').append('<div id="trecho-' + indice + '-div"></div>');
                    
                    $('#trecho-' + indice + '-div').load(
                        '<c:url value="/planejamento/voo/adicionarTrecho"/>',
                        {
                            'indice': indice,
                            'quantidadeDias' : quantidadeDias, 
                            'tipoPeriodo' : tipoPeriodo
                        }
                    );
                    
                    $(this).attr('indice-trecho', parseInt(indice) + 1);
                    
                });
			 	
			});
			
			function removerTrecho(indice) {
			    //event.preventDefault();
			    // Esconder os dados do trecho excluido
			    $('#div-trecho-' + indice).hide();
			    // Alternar para o botao desfazer
			    $('#remover-trecho-' + indice).hide();
			    $('#desfazer-remover-trecho-' + indice).show();
			    // Exibir status do trecho
			    $('#status-trecho-' + indice).show();
			}
			
			function desfazerRemoverTrecho(indice) {
			    //event.preventDefault();
			    // Exibir os dados do trecho
			    $('#div-trecho-' + indice).show();
			    // Alternar para o botao desfazer
			    $('#remover-trecho-' + indice).show();
			    $('#desfazer-remover-trecho-' + indice).hide();
			    // Esconder status do trecho
			    $('#status-trecho-' + indice).hide();
			}

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container-fluid">
            <div class="block withsidebar">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${editar == true ? 'Editar' : ''} Viagem '${viagem.nome}'</h2>
                </div>
                
                <div class="block_content">
                
                    <div class="sidebar_content" id="pageContent">
                    
                        <c:url value="/planejamento/salvarVoo" var="salvar_url" />
                        
                        <form:form id="vooForm" action="${salvar_url}" modelAttribute="voo" method="post" enctype="multipart/form-data">
                        
                            <div class="form-actions">
                                <div class="row">
                                    <div class="span10">
                                        <h2 class="leftAligned">
                                            Adicionar Voo - <span>${viagem.nome}</span>
                                            <a href="${pageContext.request.contextPath}/planoViagem/${idViagem}">
                                                
                                            </a>
                                        </h2>
                                    </div>
                                </div>
                            </div>                            
                        
                            <input type="hidden" name="voo" value="${voo.id}" />
                            <input type="hidden" name="viagem" value="${viagem.id}" />
                            
                            <div class="row">
                            
                                <s:hasBindErrors name="voo">
                                    <div id="errorBox" class="alert alert-error span6"> 
                                        <span class="large"><s:message code="validation.containErrors" /></span> 
                                    </div>
                                </s:hasBindErrors>
                                <div id="errorBox" class="alert alert-error span6" style="display: none;"> 
                                    <span class="large"><s:message code="validation.containErrors" /></span>
                                </div>
                            
                            </div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Informações do Voo
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-info"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-info" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">
                                    
                                        <div class="control-group">
                                            <label for="codigoReserva">
                                                <span>Código da reserva (Localizador)</span>
                                                <i id="help-codigo" class="icon-question-sign helper" title=""></i>
                                            </label>
                                            <form:errors cssClass="error" path="codigoReserva" element="label"/>
                                            <div class="controls">
                                                <form:input id="codigoReserva" path="codigoReserva" cssClass="span2" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Trechos do Voo
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-trechos"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-trechos" class="accordion-body collapse in" style="height: auto; ">
                                    
                                    <div class="accordion-inner">
                                    
                                        <div id="lista-trechos">
                                    
                                            <c:forEach items="${voo.trechos}" var="trecho" varStatus="counter">
            
                                                <c:import url="trechoVoo.jsp">
                                                    <c:param name="indice" value="${counter.index}" />
                                                    <c:param name="quantidadeDias" value="${viagem.quantidadeDias}" />
                                                    <c:param name="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}" />
                                                </c:import>
                                                <c:set var="indice" value="${counter.index}"></c:set>
                                                <c:set var="quantidadeDias" value="${viagem.quantidadeDias}"></c:set>
                                                <c:set var="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}"></c:set>
                                                
                                            </c:forEach>
                                    
                                        </div>
                                        
                                        <a id="adicionar-trecho" class="btn btn-success btn-mini" href="#" indice-trecho="${indice + 1}" tipo-periodo="${tipoPeriodo}" quantidade-dias="${quantidadeDias}">
                                            <i class="icon-plus icon-white"></i> Adicionar
                                        </a>
                                        
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="form-actions" align="right">
                                <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
                                <a id="botaoCancelar" class="btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${hospedagem.id}" />">
                                    <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
                                </a>                                <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                <a id="botaoSalvar" class="btn btn-success" href="#">
                                    <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                </a>
                            </div>
                            
                        </form:form>
                        
                    </div>
                
                </div>
                
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
           </div>
        </div>                        

	</tiles:putAttribute>

</tiles:insertDefinition>