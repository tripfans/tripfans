<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Aluguel de Veículo" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
			jQuery(document).ready(function() {
				
			    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
			    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
			    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
				
				$('#aluguelVeiculoForm').validate({
			        rules: {
			        	nomeLocal: 'required',
			        	emailContato: { 
		                	email: true
		            	},
		            	/*dataInicio: {
		                    date: true
		                },		            	
		                dataFim: { 
                            date: true
                        },
                        horaInicio: { 
                            time: true
                        },
                        horaFim: { 
                            time: true
                        },*/
                        dataReserva: { 
                            date: true
                        }
			        },
			        messages: { 
			        	nomeLocal: "<s:message code="validation.atividade.nomeLocal.required" />",
			        	emailContato:"<s:message code="validation.atividade.emailContato.valid" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });
				
				$('input:text').setMask();
				
				$('#botaoSalvar').bind('click', function(event) {
				    event.preventDefault();
				    $('#aluguelVeiculoForm').submit();
				});
				
			 	$('#aluguelVeiculoForm').initToolTips();

			 	$('#dataInicio').datepicker({
			 		position: {
						my: 'left top',
						at: 'right top'
					},
					onSelect: function(dateText, inst) {
						$('#dataFim').datepicker('option', 'minDate', dateText );
					}
				});
			 	
			 	$('#dataFim').datepicker({
			 		position: {
						my: 'left',
					    at: 'right'
					}
				});

			 	$('#dataReserva').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	
                $('#fimMesmoLocal').bind('click', function(event) {
                	// Mostrar/ocultar campos de local/endereco de devolucao
                	if ($(this).is(':checked') == true) {
                		$('#div-local-devolucao').hide();
                	} else {
                        $('#div-local-devolucao').show();
                	}
                });
				
			});
			
			$('#horaInicio').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaInicio').val('<joda:format value="${atividade.horaInicio}" style="-S" />');
			
			$('#horaFim').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaFim').val('<joda:format value="${atividade.horaFim}" style="-S" />');
			
			jQuery(function($){
			    $("#horaInicio").mask("99:99");
			});

            function selecionarEmpresa(event, ui) {
                $('#locadora').setJsonFields('e');
            }
            
            function removerEmpresa(event, ui) {
                $('#descricaoLocal').val('');
                enableDisableField("descricaoLocal");
                $('#enderecoLocal').val('');
                enableDisableField("enderecoLocal");
            }
			
			function selecionarLocadora(event, ui) {
                $('#enderecoLocal').val(ui.item.endereco.descricao);
                enableDisableField("enderecoLocal");
                $('#dataInicio').focus();
            }
			
            function removerLocadora(event, ui) {
                $('#enderecoLocal').val('');
                enableDisableField("enderecoLocal");
            }
            
            function selecionarLocadoraDevolucao(event, ui) {
                $('#enderecoLocalFim').val(ui.item.endereco.descricao);
                enableDisableField("enderecoLocalFim");
                $('#dataFim').focus();
            }
            
            function removerLocadoraDevolucao(event, ui) {
                $('#enderecoLocalFim').val('');
                enableDisableField("enderecoLocalFim");
            }

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container-fluid">
            <div class="block withsidebar">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${editar == true ? 'Editar' : ''} Viagem '${viagem.nome}'</h2>
                </div>
                
                <div class="block_content">
                
                    <div class="sidebar_content" id="pageContent">
                    
                        <c:url value="/planejamento/salvarAluguelVeiculo" var="salvar_url" />
                        
                        <form:form id="aluguelVeiculoForm" action="${salvar_url}" modelAttribute="atividade" method="post" enctype="multipart/form-data">
                        
                            <c:set value="${atividade}" var="aluguelVeiculo"></c:set>
                        
                            <div class="form-actions">
                                <div class="row">
                                    <div class="span10">
                                        <h2 class="leftAligned">
                                            Aluguel de Veículo
                                            <a href="${pageContext.request.contextPath}/planoViagem/${idViagem}">
                                                <span>${viagem.nome}</span>
                                            </a>
                                        </h2>
                                    </div>
                                    <!--div class="span3">
                                        <div class=rightAligned>
                                            <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                            
                                            <a id="botaoSalvar" class="btn btn-success" href="#">
                                                <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                            </a>
                                            
                                        </div>
                                    </div-->
                                </div>
                            </div>
                        
                            <input type="hidden" name="aluguelVeiculo" value="${aluguelVeiculo.id}" />
                            <input type="hidden" name="viagem" value="${viagem.id}" />
                            
                            <div class="row">
                            
                                <s:hasBindErrors name="aluguelVeiculo">
                                    <div id="errorBox" class="alert alert-error span6"> 
                                        <span class="large"><s:message code="validation.containErrors" /></span> 
                                    </div>
                                </s:hasBindErrors>
                                <div id="errorBox" class="alert alert-error span6" style="display: none;"> 
                                    <span class="large"><s:message code="validation.containErrors" /></span>
                                </div>
                            
                            </div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Aluguel
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-local"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-local" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">
                                    
                                        <div class="row">
                                            <div class="span4">
                                    
                                                <label for="empresaAerea">
                                                    Empresa
                                                </label>
                                                <div class="controls">
                                                    <fan:autoComplete url="/planejamento/consultarLocadoras" name="empresa"
                                                          value="${aluguelVeiculo.empresa.nome}" 
                                                          hiddenName="aluguelVeiculo.empresa" id="empresa"
                                                          valueField="id" labelField="nome" cssClass="span3" 
                                                          hiddenValue="${aluguelVeiculo.empresa.id}" limit="10"
                                                          jsonFields="[{name : 'id'}, {name : 'nome'}]"
                                                          showActions="true" blockOnSelect="true"
                                                          onSelect="selecionarEmpresa()" onRemove="removerEmpresa()"
                                                          placeholder="Informe a empresa..." />
                                                </div>
                                            </div>
                                            
                                            <div class="span4">
                                    
                                                <label for="empresaAerea">
                                                    Número de confirmação/reserva
                                                </label>
                                                <div class="controls">
                                                    <form:input id="numeroConfirmacao" path="confirmacao" cssClass="span3" />
                                                </div>
                                            </div>
                                            
                                    
                                        </div>
                                        
                                        <div class="control-group">
                                        
                                            <h4><span class="green">Retirada</span></h4>
                                        
                                            <div class="row">
                                            
                                                <div class="span4">
                                                    <label for="nomeLocal">
                                                        <span class="required">Loja</span>
                                                        <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                                    </label>
                                                    <form:errors cssClass="error" path="nomeLocal" element="label"/>
                                                    <div class="controls">
                                                        <fan:autoComplete url="/locais/consultarLocadorasVeiculos" name="nomeLocal" hiddenName="local" id="locadora" 
                                                              valueField="id" labelField="nome" cssClass="span3" 
                                                              value="${aluguelVeiculo.local != null ? aluguelVeiculo.local.nome : ''}"
                                                              hiddenValue="${aluguelVeiculo.local != null ? aluguelVeiculo.local.id : ''}"
                                                              extraParams="{idViagem : ${viagem.id}}"
                                                              jsonFields="[{name : 'id'}, {name : 'nome'}, {name : 'descricao'}, {name : 'endereco.descricao'}]"
                                                              showActions="true" onSelect="selecionarLocadora()" blockOnSelect="true" onRemove="removerLocadora()"
                                                              placeholder="Informe a loja..." />
                                                    </div>
                                                </div>
                                                
                                                <div class="span4">
                                        
                                                    <label for="enderecoLocal">
                                                        <s:message code="atividade.label.enderecoLocal" />
                                                    </label>
                                                    <form:errors cssClass="error" path="enderecoLocal" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="enderecoLocal" path="enderecoLocal" cssClass="span4" disabled="${aluguelVeiculo.local != null}" />
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="control-group">
                                        
                                            <div class="row">
                                            
                                                <div class="span3">
                                                    <viagem:exibirPeriodo tipoPeriodo="${viagem.tipoPeriodo.codigo}"
                                                        idPorData="dataInicio" idPorDia="diaInicio" 
                                                        valuePorData="${aluguelVeiculo.dataInicio}" valuePorDia="${aluguelVeiculo.diaInicio}"
                                                        labelPorData="Data da retirada" labelPorDia="Dia da retirada" 
                                                        quantidadeDias="${viagem.quantidadeDias}"
                                                        pathPorData="dataInicio" pathPorDia="diaInicio" />
                                                </div>
                                                
                                                <div class="span2">
                                                    <label for="horaInicio">
                                                        Hora da retirada
                                                    </label>
                                                    <form:errors cssClass="error" path="horaInicio" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="horaInicio" alt="time" path="horaInicio" class="span1" />
                                                    </div>
                                                </div>
                                                <%--div class="span5">
                                                        <label for="dataInicioTZ"><s:message code="aluguelVeiculo.label.dataInicioTZ" /></label>
                                                        <form:errors cssClass="error" path="dataInicioTZ" element="label"/>
                                                        <div class="controls">
                                                            <form:select id="dataInicioTZ" cssClass="span4" path="dataInicioTZ" title="${tooltipTeste}">
                                                                <form:option value="1">1</form:option>
                                                                <form:option value="2">2</form:option>
                                                                <form:option value="3">3</form:option>
                                                            </form:select>
                                                        </div>
                                                </div--%>
                                            </div>
                                        </div>
                                        
                                        <h4><span class="red">Devolução</span></h4>
                                        
                                        <div class="control-group">
                                        
                                            <div class="row">
                                            
                                                <div style="height: 5px;"></div>
                                                <div class="span10">
                                                    <div class="controls">
                                                        <form:checkbox id="fimMesmoLocal" path="fimMesmoLocal"/>
                                                        <strong>Local de de retirada e devolução são os mesmos</strong>
                                                        <div style="height: 10px;"></div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                            <div id="div-local-devolucao" class="row" style="${aluguelVeiculo.fimMesmoLocal ? 'display : none' : ''}">
                                            
                                                <div class="span4">
                                                    <label for="nomeLocal">
                                                        <span class="required">Loja</span>
                                                        <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                                    </label>
                                                    <form:errors cssClass="error" path="nomeLocal" element="label"/>
                                                    <div class="controls">
                                                        <fan:autoComplete url="/locais/consultarLocadorasVeiculos" name="nomeLocalDevolucao" hiddenName="localDevolucao" id="locadoraDevolucao" 
                                                              valueField="id" labelField="nome" cssClass="span3" 
                                                              value="${aluguelVeiculo.localFim != null ? aluguelVeiculo.localFim.nome : ''}"
                                                              hiddenValue="${aluguelVeiculo.localFim != null ? aluguelVeiculo.localFim.id : ''}"
                                                              extraParams="{idViagem : ${viagem.id}}"
                                                              jsonFields="[{name : 'id'}, {name : 'nome'}, {name : 'descricao'}, {name : 'endereco.descricao'}]"
                                                              showActions="true" onSelect="selecionarLocadoraDevolucao()" blockOnSelect="true" onRemove="removerLocadoraDevolucao()"
                                                              placeholder="Informe a loja..." />
                                                    </div>
                                                </div>
                                                
                                                <div class="span4">
                                        
                                                    <label for="enderecoLocalFim">
                                                        <s:message code="atividade.label.enderecoLocal" />
                                                    </label>
                                                    <form:errors cssClass="error" path="enderecoLocalFim" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="enderecoLocalFim" path="enderecoLocalFim" cssClass="span4" disabled="${aluguelVeiculo.localFim != null}" />
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>
                                            
                                            <div class="row">
                                            
                                                <div class="span3">
                                                    <viagem:exibirPeriodo tipoPeriodo="${viagem.tipoPeriodo.codigo}"
                                                        idPorData="dataFim" idPorDia="diaFim" 
                                                        valuePorData="${aluguelVeiculo.dataFim}" valuePorDia="${aluguelVeiculo.diaFim}"
                                                        labelPorData="Data da devolução" labelPorDia="Dia da devolução" 
                                                        quantidadeDias="${viagem.quantidadeDias}"
                                                        pathPorData="dataFim" pathPorDia="diaFim" />
                                                </div>
                                                
                                                <div class="span2">
                                                    <label for="horaFim">
                                                        Hora da devolução
                                                    </label>
                                                    <form:errors cssClass="error" path="horaFim" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="horaFim" alt="time" path="horaFim" class="span1" />
                                                    </div>
                                                </div>
                                                 
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Quem irá dirigir?
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-detalhes"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-detalhes" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                						 	
                                            <c:import url="selecionarParticipantesAtividade.jsp" />
                                            
                						</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="form-actions" align="right">
                                <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
                                <a id="botaoCancelar" class="btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${aluguelVeiculo.id}" />">
                                    <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
                                </a>
                                <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                <a id="botaoSalvar" class="btn btn-success" href="#">
                                    <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                </a>
                            </div>
                            
                        </form:form>
                        
                    </div>
                
                </div>
                
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
           </div>
        </div>                        
			
	</tiles:putAttribute>

</tiles:insertDefinition>