<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Atração" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
			jQuery(document).ready(function() {
				
			    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
			    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
			    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
				
				$('#atracaoForm').validate({
			        rules: {
			        	nomeLocal: 'required',
			        	emailContato: { 
		                	email: true
		            	},
		            	/*dataInicio: {
		                    date: true
		                },		            	
		                dataFim: { 
                            date: true
                        },
                        horaInicio: { 
                            time: true
                        },
                        horaFim: { 
                            time: true
                        },*/
                        dataReserva: { 
                            date: true
                        }
			        },
			        messages: { 
			        	nomeLocal: "<s:message code="validation.atividade.nomeLocal.required" />",
			        	emailContato:"<s:message code="validation.atividade.emailContato.valid" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });
				
				$('input:text').setMask();
				
				$('#botaoSalvar').bind('click', function(event) {
				    event.preventDefault();
				    $('#atracaoForm').submit();
				});
				
			 	$('#atracaoForm').initToolTips();

			 	$('#observacoes').stickySidebar();
			 	
			 	$('#dataInicio').datepicker({
			 		position: {
						my: 'left top',
						at: 'right top'
					},
					onSelect: function(dateText, inst) {
						$('#dataFim').datepicker('option', 'minDate', dateText );
					}
				});
			 	
			 	//$('#dataInicio').mask('99/99/9999');
			 	//$('#dataFim').mask('99/99/9999');
			 	
                //$('#dataInicio').datepicker('option', 'minDate', '<fmt:formatDate value="${viagem.dataInicio}" pattern="dd/MM/yyyy" />');
			 	//$('#dataFim').datepicker('option', 'maxDate', '<fmt:formatDate value="${viagem.dataFim}" pattern="dd/MM/yyyy" />');
			 	
			 	$('#dataFim').datepicker({
			 		position: {
						my: 'left',
					    at: 'right'
					}
				});

			 	$('#dataReserva').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	
			});
			
			$('#horaInicio').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaInicio').val('<joda:format value="${atividade.horaInicio}" style="-S" />');
			
			$('#horaFim').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaFim').val('<joda:format value="${atividade.horaFim}" style="-S" />');
			
			jQuery(function($){
			    $("#horaInicio").mask("99:99");
			});
			
			function selecionarAtracao(event, ui) {
                $('#descricaoLocal').val(ui.item.descricao);
                enableDisableField("descricaoLocal");
                $('#enderecoLocal').val(ui.item.endereco.descricao);
                enableDisableField("enderecoLocal");
                $('#dataInicio').focus();
            }
			
            function removerAtracao(event, ui) {
                $('#descricaoLocal').val('');
                enableDisableField("descricaoLocal");
                $('#enderecoLocal').val('');
                enableDisableField("enderecoLocal");
            }

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container-fluid">
            <div class="block withsidebar">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${editar == true ? 'Editar' : ''} Viagem '${viagem.nome}'</h2>
                </div>
                
                <div class="block_content">
                
                    <div class="sidebar_content" id="pageContent">
                    
                        <c:url value="/planejamento/salvarAtracao" var="salvar_url" />
                        
                        <form:form id="atracaoForm" action="${salvar_url}" modelAttribute="atividade" method="post" enctype="multipart/form-data">
                        
                            <c:set value="${atividade}" var="atracao"></c:set>
                        
                            <div class="form-actions">
                                <div class="row">
                                    <div class="span10">
                                        <h2 class="leftAligned">
                                            Atração
                                            <a href="${pageContext.request.contextPath}/planoViagem/${idViagem}">
                                                <span>${viagem.nome}</span>
                                            </a>
                                        </h2>
                                    </div>
                                    <!--div class="span3">
                                        <div class=rightAligned>
                                            <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                            
                                            <a id="botaoSalvar" class="btn btn-success" href="#">
                                                <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                            </a>
                                            
                                        </div>
                                    </div-->
                                </div>
                            </div>
                        
                            <input type="hidden" name="atracao" value="${atracao.id}" />
                            <input type="hidden" name="viagem" value="${viagem.id}" />
                            
                            <div class="row">
                            
                                <s:hasBindErrors name="atracao">
                                    <div id="errorBox" class="alert alert-error span6"> 
                                        <span class="large"><s:message code="validation.containErrors" /></span> 
                                    </div>
                                </s:hasBindErrors>
                                <div id="errorBox" class="alert alert-error span6" style="display: none;"> 
                                    <span class="large"><s:message code="validation.containErrors" /></span>
                                </div>
                            
                            </div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Nome da atração
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-local"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-local" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">
                                    
                                        <div class="control-group">
                                            <label for="nomeLocal">
                                                <span class="required"><s:message code="atividade.label.nomeLocal" /></span>
                                                <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                            </label>
                                            <form:errors cssClass="error" path="nomeLocal" element="label"/>
                                            <div class="controls">
                                                <fan:autoComplete url="/locais/consultarAtracoes" name="nomeLocal" hiddenName="local" id="nomeAtracao" 
                                                      valueField="id" labelField="nome" cssClass="span5" 
                                                      value="${atracao.local != null ? atracao.local.nome : ''}"
                                                      hiddenValue="${atracao.local != null ? atracao.local.id : ''}"
                                                      showActions="true" onSelect="selecionarAtracao()" blockOnSelect="true" onRemove="removerAtracao()"
                                                      jsonFields="[{name : 'id'}, {name : 'nome'}, {name : 'descricao'}, {name : 'endereco.descricao'}]"
                                                      placeholder="Informe o local..." />
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <label for="enderecoLocal">
                                                <s:message code="atividade.label.enderecoLocal" />
                                            </label>
                                            <form:errors cssClass="error" path="enderecoLocal" element="label"/>
                                            <div class="controls">
                                                <form:input id="enderecoLocal" path="enderecoLocal" cssClass="span7" disabled="${atracao.local != null}" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="descricaoLocal">
                                                <s:message code="atividade.label.descricaoLocal" />
                                                <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                            </label>
                                            <form:errors cssClass="error" path="descricaoLocal" element="label"/>
                                            <div class="controls">
                                                <form:input id="descricaoLocal" path="descricaoLocal" cssClass="span7" disabled="${atracao.local != null}" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Quando?
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-periodo"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-periodo" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                                            <div class="row">
                                                <div class="span3">
                                                    <label for="dataInicio">
                                                        Data
                                                    </label>
                                                    <form:errors cssClass="error" path="dataInicio" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="dataInicio" alt="date" path="dataInicio" class="span2" />
                                                    </div>
                                                </div>  
                                                <div class="span2">
                                                    <label for="horaInicio">
                                                        Hora
                                                    </label>
                                                    <form:errors cssClass="error" path="horaInicio" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="horaInicio" alt="time" path="horaInicio" class="span1" />
                                                    </div>
                                                </div>
                                                <%--div class="span5">
                                                        <label for="dataInicioTZ"><s:message code="atracao.label.dataInicioTZ" /></label>
                                                        <form:errors cssClass="error" path="dataInicioTZ" element="label"/>
                                                        <div class="controls">
                                                            <form:select id="dataInicioTZ" cssClass="span4" path="dataInicioTZ" title="${tooltipTeste}">
                                                                <form:option value="1">1</form:option>
                                                                <form:option value="2">2</form:option>
                                                                <form:option value="3">3</form:option>
                                                            </form:select>
                                                        </div>
                                                </div--%>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Participantes
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-detalhes"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-detalhes" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                                        
                						 	<c:import url="selecionarParticipantesAtividade.jsp" />
                						</div>
                						
                                    </div>
                                </div>
                            </div>
                            
                            <%@include file="/WEB-INF/views/planoViagem/planejamento/barraAcoes.jsp" %>
                            
                        </form:form>
                        
                    </div>
                
                </div>
                
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
           </div>
        </div>                        

	</tiles:putAttribute>

</tiles:insertDefinition>