<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${not empty viagem.participantes}">

    <div class="row">
    
        <div class="span9">
        
            <table class="table table-striped">
            
                <tr>
                    <th width="5%">
                        <input type="checkbox" />
                    </th>
                    <th width="50%">
                        Nome
                    </th>
                    <c:if test="${not atividade.aluguelVeiculo}">
                        <th width="40%">
                            <c:choose>
                                <c:when test="${viagem.planejamento}">
                                    Custo estimado <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                </c:when>
                                <c:otherwise>
                                    Valor gasto <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                </c:otherwise>
                            </c:choose>
                        </th>
                    </c:if>
                </tr>
            
                <c:forEach items="${viagem.participantes}" var="participante" varStatus="counter">
                
                    <c:set var="indice" value="${counter.index}" />
                    
                    <input type="hidden" name="participantes[${indice}]" value="${participantes[indice].id}" />
                    <input type="hidden" name="participantes[${indice}].atividade" value="${atividade.id}" />
                    <input type="hidden" name="participantes[${indice}].participante" value="${participante.id}" />
                    
                    <tr>
                        <td>
                            <form:checkbox path="participantes[${indice}].confirmado" />
                        </td>
                        <td>
                            <form:input path="participantes[${indice}].participante.nome" disabled="true" cssClass="span4" />
                        </td>
                        
                        <c:if test="${not atividade.aluguelVeiculo}">
                          <fmt:formatNumber value="${atividade.participantes[indice].valor}" maxFractionDigits="2" minFractionDigits="2" var="valor" />
                          <td>
                            <input type="text" name="participantes[${indice}].valor" value="${valor}" class="span2" />
                          </td>
                        </c:if>
                    </tr>
                    
                </c:forEach>
            
            </table>
        
        </div>
        
    </div>
    
</c:if>
