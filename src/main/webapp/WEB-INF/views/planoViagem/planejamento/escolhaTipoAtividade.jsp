<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<div class="row">
	    <div class="span8">
	    	<div align="center">
				<h2>Selecione o Tipo de Atividade</h2>
			</div>
            
	    	<div class="row">
		        <div class="span3 offset1">
		            <a href="<c:url value="/planejamento/hospedagem/planoViagem/${viagem.id}"/>?inicio=${viagem.periodoPorDatas or viagem.periodoPorQuantidadeDias ? grupoAtividades.chave : ''}">
		                <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
                        Hotel
		            </a>
		        </div>
		    </div>
            
		    <div class="horizontalSpacer"/>
            
		    <div class="row">
                <div class="span3 offset1">
                    <a href="<c:url value="/planejamento/voo/incluir/${viagem.id}"/>">
                        <img src="<c:url value="/resources/images/plane.png" />" width="68" height="68" />
                        Voo
                    </a>
                </div>
		    </div>
            
            <div class="horizontalSpacer"/>
            
            <div class="row">
                <div class="span3 offset1">
                    <a href="<c:url value="/planejamento/restaurante/incluir/${viagem.id}"/>">
                        <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
                        Restaurante
                    </a>
                </div>
            </div>
            
            <div class="horizontalSpacer"/>
            
            <div class="row">
                <div class="span3 offset1">
                    <a href="<c:url value="/planejamento/atracao/incluir/${viagem.id}"/>">
                        <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
                        Atração
                    </a>
                </div>
            </div>

            <div class="horizontalSpacer"/>
            
            <div class="row">
                <div class="span3 offset1">
                    <a href="<c:url value="/planejamento/aluguelVeiculo/incluir/${viagem.id}"/>">
                        <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
                        Aluguel de veículo
                    </a>
                </div>
            </div>

            
		</div>
	</div>
		