<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div style="width: 100%">
    <div id="error" style="display: none;" class="alert alert-success">
        <p id="errorMsg">
        </p>
    </div>
    <div id="success" style="display: none;" class="alert alert-success">
        <p id="successMsg">
        </p>
    </div>
</div>

<div class="horizontalSpacer"></div>

<div class="form-actions" align="right">
    <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
    <a id="botaoCancelar" class="btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${atividade.id}" />">
        <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
    </a>
    <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
    <a id="botaoSalvar" class="btn btn-success" href="#">
        <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
    </a>
</div>
