<%@ page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade" %>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Planejar Viagem" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
		
			jQuery(document).ready(function() {
				$("#planejarViagemForm").validate({
			        rules: {
						destino:"required",
						mostrarDetalhesViagem:"required"
			        },
			        messages: { 
			        	destino: "<s:message code="validation.viagem.destino.required" />",
			        	mostrarDetalhesViagem: "<s:message code="validation.viagem.mostrarDetalhesViagem.required" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });

				
				$("input:text").setMask();
				
			 	$("#planejarViagemForm").initToolTips();

			 	$('#observacoes').stickySidebar();
			 	
			 	$("#destino").autocompleteFanaticos({
					source: "${pageContext.request.contextPath}/planoViagem/consultarCidadeEPaisPorNome",
					hiddenName : 'destino',
				    valueField: 'id',
				    labelField: 'nome',
					minLength: 3
				});
			 	
			 	$('#dataInicio').datepicker({
			 		position: {
						my: "left top",
						at: "right top"
					},
					onSelect: function(dateText, inst) {
						var minDate = $("#dataFim").datepicker( "option", "minDate" );
						$("#dataFim").datepicker( "option", "minDate", dateText );
					}
				});
			 	$('#dataFim').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	$('.collapsible').bind('click',function(event) {
			        $(this).parent().find(".panelContent").slideToggle('fast');
			        //$(this).toggleClass("active");
			    });
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="row">
			<div class="span8">
				<form:form modelAttribute="viagem" id="planejarViagemForm" action="${pageContext.request.contextPath}/planoViagem/incluirViagem" method="post" enctype="multipart/form-data">
					<s:hasBindErrors name="viagem">
						<div id="errorBox" class="alert alert-error span6"> 
							<span class="large"><s:message code="validation.containErrors" /></span> 
						</div>
					</s:hasBindErrors>
					<div id="errorBox" class="alert alert-error span6" style="display: none;"> 
							  <span class="large"><s:message code="validation.containErrors" /></span> 
					</div>
					
					<div class="form-actions">
						<div class="row">
							<div class="span6">
								<h2 class="leftAligned"><s:message code="planejarViagem.titulo"/>
								</h2>
							</div>
							<div class="span1">
								<div class=rightAligned>
					  				<s:message code="planejarViagem.label.botaoSalvar" var="labelBotaoSalvar" />
									<input type="submit" id="botaoSalvar" value="${labelBotaoSalvar}" class="btn btn-primary" >
								</div>
							</div>
						</div>
		     		</div> 
					<div class="panel">
						<div class="panelHeader collapsible">
							<s:message code="planejarViagem.legend" />
	                    	<div class="headerActions"></div>
	                	</div>
	                	<div class="panelContent">
							<div class="control-group">
								<label for="destino" class="required"><s:message code="planejarViagem.label.destino" /></label>
								<form:errors cssClass="error" path="destino" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<form:input id="destino" path="destino" title="${tooltipTeste}" cssClass="span7" />
								</div>
							</div>
							
							<div class="control-group">
								<label for="nomeViagem"><s:message code="planejarViagem.label.nome" /></label>
								<form:errors cssClass="error" path="nome" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<form:input id="nomeViagem" path="nome" title="${tooltipTeste}" cssClass="span7"/>
								</div>
							</div>
							
							<div class="control-group">
								<label for="descricao"><s:message code="planejarViagem.label.descricao" /></label>
								<form:errors cssClass="error" path="descricao" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<form:textarea id="descricao" path="descricao" title="${tooltipTeste}" cssClass="span7" rows="5"/>
								</div>
							</div>
	
						    <div class="control-group">
							    <div class="row">
									<div class="span5">
										<label for="dataInicio">
											<s:message code="planejarViagem.label.dataInicio" />
										</label>
										<form:errors cssClass="error" path="dataInicio" element="label"/>
										<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
										<div class="controls">
											<form:input id="dataInicio" alt="date" path="dataInicio" title="${tooltipTeste}" class="span4" />
										</div>
									</div>  
									<div class="span5">
											<label for="dataInicioTZ"><s:message code="planejarViagem.label.dataInicioTZ" /></label>
											<form:errors cssClass="error" path="dataInicioTZ" element="label"/>
											<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
											<div class="controls">
												<form:select id="dataInicioTZ" cssClass="span4" path="dataInicioTZ" title="${tooltipTeste}">
	                                    			<form:option value="1">1</form:option>
	                                    			<form:option value="2">2</form:option>
	                                    			<form:option value="3">3</form:option>
	                                			</form:select>
											</div>
									</div>
								</div>
							</div>
							<div class="control-group">
							    <div class="row">
									<div class="span5">
										<label for="dataFim">
											<s:message code="planejarViagem.label.dataFim" />
										</label>
										<form:errors cssClass="error" path="dataFim" element="label"/>
										<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
										<div class="controls">
											<form:input id="dataFim" path="dataFim" alt="date" title="${tooltipTeste}" class="span4" />
										</div>
									</div>  
									<div class="span5">
											<label for="dataFimTZ"><s:message code="planejarViagem.label.dataFimTZ" /></label>
											<form:errors cssClass="error" path="dataFimTZ" element="label"/>
											<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
											<div class="controls">
												<form:select id="dataFimTZ" cssClass="span4" path="dataFimTZ" title="${tooltipTeste}">
	                                    			<form:option value="1">1</form:option>
	                                    			<form:option value="2">2</form:option>
	                                    			<form:option value="3">3</form:option>
	                                			</form:select>
											</div>
									</div>
								</div>
							</div>
							<div class="control-group">
								<label for="mostrarDetalhesViagem" class="required">
									<s:message code="planejarViagem.label.mostrarDetalhesViagem" />
								</label>
								<form:errors cssClass="error" path="mostrarDetalhes" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<div class="row media-grid">
										<div class="span3" align="center">
											<img src="<c:url value="/resources/images/icons/world.png" />"/>
											Todos<br/>
											<form:radiobutton id="mostrarDetalhesViagem" path="mostrarDetalhes" value="<%=TipoVisibilidade.PUBLICO %>"/>
										</div>
					                    <div class="span3" align="center">
					                    	<img src="<c:url value="/resources/images/icons/group.png" />"/>
					                    	Amigos<br/>
					                    	<form:radiobutton id="mostrarDetalhesViagem" path="mostrarDetalhes" value="<%=TipoVisibilidade.AMIGOS %>"/>
					                    </div>
										<div class="span3" align="center">
											<img src="<c:url value="/resources/images/icons/user.png" />"/>
											Somente eu<br/>
											<form:radiobutton id="mostrarDetalhesViagem" path="mostrarDetalhes" value="<%=TipoVisibilidade.SOMENTE_EU %>" />
										</div>
									</div>
								</div>                 
							</div>
						
							<div class="control-group">
								<label for="quantidadeDias"><s:message code="planejarViagem.label.quantidadeDias" /></label>
								<form:errors cssClass="error" path="quantidadeDias" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<form:input id="quantidadeDias" path="quantidadeDias" title="${tooltipTeste}"/>
								</div>
							</div>
						
							<div class="control-group">
								<form:errors cssClass="error" path="criadorViaja" element="label"/>
								<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
								<div class="controls">
									<label for="criadorViaja" class="checkbox"><s:message code="planejarViagem.label.criadorViaja" />
									<form:checkbox id="criadorViaja" path="criadorViaja" title="${tooltipTeste}"/>
									</label>
								</div>
							</div>
						</div>
					</div>
		   			<div class="form-actions" align="right">
		  				<s:message code="planejarViagem.label.botaoSalvar" var="labelBotaoSalvar" />
						<input type="submit" id="botaoSalvar" value="${labelBotaoSalvar}" class="btn btn-primary" >
		     		</div>
				</form:form>
			</div>
			
			<div class="span4" id="observacoes">
				<div class="alert block-message alert-info" style="padding-top: 30px;">
				 		<strong class="large"><s:message code="fanaticos.compromissoInformacoesUteis.titulo" /></strong>
						<p>
						<s:message code="fanaticos.compromissoInformacoesUteis.parte1" />
						</p>
						<p>
						<s:message code="fanaticos.compromissoInformacoesUteis.parte2" />
						</p>
						<br/><br/>
						<strong class="large"><s:message code="avaliacao.dicasPreenchimento" /></strong>
						<ul>
						<li><s:message code="formulario.camposObrigatorios" /></li> 
						<li><s:message code="foto.texto.tamanhoMaximo" arguments="${tamanhoMaximoFoto}" /></li>
						<li><s:message code="foto.texto.resolucaoMaxima" arguments="${resolucaoMaximaFoto}" /></li>
						</ul>
				</div>
			</div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>