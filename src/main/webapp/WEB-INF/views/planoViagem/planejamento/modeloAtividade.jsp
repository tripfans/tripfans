<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="fanaticos.default">

	<!-- TODO vamos ter que implementar uma forma dinâmica de obter as palavras chaves, pois, essa página será gerada dinamicamente -->
	<tiles:putAttribute name="title" value="Hospedagem" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
		
			jQuery(document).ready(function() {
				$("#atividadeForm").validate({
			        rules: {
			        	nomeLocal:"required",
			        	dataInicio: {
			        		date: true,
			        		required: true
			        	},
					 	dataFim: {
			        		date: true,
			        		required: true
			        		
			        	},
					 	dataReserva: {
			        		date: true
			        	},
			        	emailContato: { 
		                	email: true 
		            	} 
			        },
			        messages: { 
			        	nomeLocal: "<s:message code="validation.atividade.nomeLocal.required" />",
			        	dataInicio: "<s:message code="validation.atividade.dataInicio.required" />",
			        	dataFim: "<s:message code="validation.atividade.dataFim.required" />",
			        	emailContato:"<s:message code="validation.atividade.emailContato.valid" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });

				
				$("input:text").setMask();
				
			 	$("#atividadeForm").initToolTips();

			 	$('#observacoes').stickySidebar();
			 	
			 	$("#nomeLocal").autocompleteFanaticos({
					source: "${pageContext.request.contextPath}/atividade/consultarHoteisPorNome",
					hiddenName : 'hotel',
				    valueField: 'id',
				    labelField: 'nome',
					minLength: 3,
					 select: function(event, ui) { 
					     $("#descricaoLocal").val(ui.item.descricao);
					     enableDisableField("descricaoLocal");
					     $("#enderecoLocal").val(ui.item.endereco.descricao);
					     enableDisableField("enderecoLocal");
					 }
				});

			 	$("#nomeLocal").change(function() {
			 		$("#descricaoLocal").val("");
				     enableDisableField("descricaoLocal");
				     $("#enderecoLocal").val("");
				     enableDisableField("enderecoLocal");
			 	});
			 	$('#dataInicio').datepicker({
			 		position: {
						my: "left top",
						at: "right top"
					},
					onSelect: function(dateText, inst) {
						var minDate = $("#dataFim").datepicker( "option", "minDate" );
						$("#dataFim").datepicker( "option", "minDate", dateText );
						minDate = $("#dataReserva").datepicker( "option", "minDate" );
						$("#dataReserva").datepicker( "option", "minDate", dateText );
					}
				});
			 	$('#dataFim').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	$('#dataReserva').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	$("#nomeAgencia").autocompleteFanaticos({
					source: "${pageContext.request.contextPath}/atividade/consultarAgenciasPorNome",
					hiddenName : 'agencia',
				    valueField: 'id',
				    labelField: 'nome',
					minLength: 3,
					 select: function(event, ui) { 
					     $("#telefoneAgencia").val(ui.item.telefone);
					     enableDisableField("telefoneAgencia");
					     $("#siteAgencia").val(ui.item.site);
					     enableDisableField("siteAgencia");
					 }
				});
			 	$("#nomeAgencia").change(function() {
			 		$("#telefoneAgencia").val("");
			 		enableDisableField("telefoneAgencia");
				     $("#siteAgencia").val("");
				 	enableDisableField("siteAgencia");
			 	});

			 	$('.collapsible').bind('click',function(event) {
			        $(this).parent().find(".panelContent").slideToggle('fast');
			        //$(this).toggleClass("active");
			    });
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="row">
			<h2 class="centerAligned"><s:message code="atividade.titulo"/>
				<a href="${pageContext.request.contextPath}/planoViagem/${idViagem}">
				<span>${viagem.nome}</span>
				</a>
			</h2>
			
			<div class="span12">
				<form:form cssClass="form-stacked" modelAttribute="atividade" id="atividadeForm" action="${pageContext.request.contextPath}/atividade/incluirAtividade" method="post" enctype="multipart/form-data">
					<s:hasBindErrors name="atividade">
						<div id="errorBox" class="alert alert-error span6"> 
							<span class="large"><s:message code="validation.containErrors" /></span> 
						</div>
					</s:hasBindErrors>
					<div id="errorBox" class="alert alert-error span6" style="display: none;"> 
							  <span class="large"><s:message code="validation.containErrors" /></span> 
					</div>
					
					<input type="hidden" name="viagem" value="${idViagem}" /> 
	
				<div class="panel">
                	<div class="panelHeader collapsible">
						<s:message code="atividade.legend.local" />
                    	<div class="headerActions"></div>
                	</div>
                	<div class="panelContent">
						<div class="control-group">
							<label for="nomeLocal" class="required"><s:message code="atividade.label.nomeLocal" /></label>
							<form:errors cssClass="error" path="nomeLocal" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input id="nomeLocal" path="nomeLocal" title="${tooltipTeste}" cssClass="span11" />
							</div>
						</div>
						
						<div class="control-group">
							<label for="descricaoLocal"><s:message code="atividade.label.descricaoLocal" /></label>
							<form:errors cssClass="error" path="descricaoLocal" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input id="descricaoLocal" path="descricaoLocal" title="${tooltipTeste}" cssClass="span11"/>
							</div>
						</div>
						
						<div class="control-group">
							<label for="enderecoLocal"><s:message code="atividade.label.enderecoLocal" /></label>
							<form:errors cssClass="error" path="enderecoLocal" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input id="enderecoLocal" path="enderecoLocal" title="${tooltipTeste}" cssClass="span11"/>
							</div>
						</div>
					</div>
				</div>
					
				<div class="panel">
                	<div class="panelHeader collapsible">
					 	<s:message code="atividade.legend.periodo" />
                    	<div class="headerActions"></div>
                	</div>
                	<div class="panelContent">
					    <div class="control-group">
						    <div class="row">
								<div class="span5">
									<label for="dataInicio" class="required">
										<s:message code="atividade.label.dataInicio" />
									</label>
									<form:errors cssClass="error" path="dataInicio" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input alt="date" id="dataInicio" path="dataInicio" title="${tooltipTeste}" class="span4" />
									</div>
								</div>  
								<div class="span5">
										<label for="dataInicioTZ"><s:message code="atividade.label.dataInicioTZ" /></label>
										<form:errors cssClass="error" path="dataInicioTZ" element="label"/>
										<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
										<div class="controls">
											<form:select id="dataInicioTZ" cssClass="span4" path="dataInicioTZ" title="${tooltipTeste}">
                                    			<form:option value="1">1</form:option>
                                    			<form:option value="2">2</form:option>
                                    			<form:option value="3">3</form:option>
                                			</form:select>
										</div>
								</div>
							</div>
						</div>
						<div class="control-group">
						    <div class="row">
								<div class="span5">
									<label for="dataFim" class="required">
										<s:message code="atividade.label.dataFim" />
									</label>
									<form:errors cssClass="error" path="dataFim" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input alt="date" id="dataFim" path="dataFim" title="${tooltipTeste}" class="span4" />
									</div>
								</div>  
								<div class="span5">
										<label for="dataFimTZ"><s:message code="atividade.label.dataFimTZ" /></label>
										<form:errors cssClass="error" path="dataFimTZ" element="label"/>
										<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
										<div class="controls">
											<form:select id="dataFimTZ" cssClass="span4" path="dataFimTZ" title="${tooltipTeste}">
                                    			<form:option value="1">1</form:option>
                                    			<form:option value="2">2</form:option>
                                    			<form:option value="3">3</form:option>
                                			</form:select>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	
				<div class="panel">
                	<div class="panelHeader collapsible">
						<s:message code="atividade.legend.participantes" />
                    	<div class="headerActions"></div>
                	</div>
                	<div class="panelContent">
						<div class="control-group">
							<label for="quantidadeParticipantes"><s:message code="atividade.label.quantidadeParticipantes" /></label>
							<form:errors cssClass="error" path="quantidadeParticipantes" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input id="quantidadeParticipantes" path="quantidadeParticipantes" title="${tooltipTeste}" />
							</div>
						</div>
					</div>
				</div>
					
					
				<div class="panel">
                	<div class="panelHeader collapsible">
					 <s:message code="atividade.legend.reserva" />
                    	<div class="headerActions"></div>
                	</div>
                	<div class="panelContent">
						
						<div class="control-group">
				 			<label for="dataReserva"><s:message code="atividade.label.dataReserva" /></label>
							<form:errors cssClass="error" path="dataReserva" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input alt="date" id="dataReserva" path="dataReserva" title="${tooltipTeste}" />
							</div>
						</div>
						<div class="control-group">
							<label for="nomeAgencia"><s:message code="atividade.label.nomeAgencia" /></label>
							<form:errors cssClass="error" path="nomeAgencia" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input id="nomeAgencia" path="nomeAgencia" title="${tooltipTeste}" class="span11"/>
							</div>
						</div>
						 
						<div class="control-group">
						 	<div class="row">
								<div class="span5">
									<label for="telefoneAgencia"><s:message code="atividade.label.telefoneAgencia" /></label>
									<form:errors cssClass="error" path="telefoneAgencia" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input alt="phone-internacional" id="telefoneAgencia" path="telefoneAgencia" title="${tooltipTeste}" />
									</div>
								</div>
								<div class="span5">
									<label for="siteAgencia"><s:message code="atividade.label.siteAgencia" /></label>
									<form:errors cssClass="error" path="siteAgencia" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input id="siteAgencia" path="siteAgencia" title="${tooltipTeste}" />
									</div>
								</div>
							</div>
						</div>
							
						<div class="control-group">
						 	<div class="row">
								<div class="span5">
									<label for="nomeContato"><s:message code="atividade.label.nomeContato" /></label>
									<form:errors cssClass="error" path="nomeContato" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input id="nomeContato" path="nomeContato" title="${tooltipTeste}" />
									</div>
								</div>
								<div class="span5">
									<label for="telefoneContato"><s:message code="atividade.label.telefoneContato" /></label>
									<form:errors cssClass="error" path="telefoneContato" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input alt="phone-internacional" id="telefoneContato" path="telefoneContato" title="${tooltipTeste}" />
									</div>
								</div>
							</div>
						</div>
						
						<div class="control-group">
							<label for="emailContato"><s:message code="atividade.label.emailContato" /></label>
							<form:errors cssClass="error" path="emailContato" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls-prepend">
								<span class="add-on">
									<i class="icon-envelope"></i>
								</span>
								<form:input id="emailContato" path="emailContato" title="${tooltipTeste}" class="span10" />
							</div>
						</div>
												
						<div class="control-group">
						 	<div class="row">
								<div class="span5">
									<label for="codigoReferenciaReserva"><s:message code="atividade.label.codigoReferenciaReserva" /></label>
									<form:errors cssClass="error" path="codigoReferenciaReserva" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input id="codigoReferenciaReserva" path="codigoReferenciaReserva" title="${tooltipTeste}" />
									</div>
								</div>
								<div class="span5">
									<label for="confirmacao"><s:message code="atividade.label.confirmacao" /></label>
									<form:errors cssClass="error" path="confirmacao" element="label"/>
									<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
									<div class="controls">
										<form:input id="confirmacao" path="confirmacao" title="${tooltipTeste}" />
									</div>
								</div>
							</div>
						</div>
						
						<div class="control-group">
							<label for="valorPago"><s:message code="atividade.label.valorPago" /></label>
							<form:errors cssClass="error" path="valorPago" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:input alt="decimal" id="valorPago" path="valorPago" title="${tooltipTeste}" />
							</div>
						</div>
						
					</div>
				</div>
					
				<div class="panel">
                	<div class="panelHeader collapsible">
					<s:message code="atividade.legend.outros" />
                    	<div class="headerActions"></div>
                	</div>
                	<div class="panelContent">
						<div class="control-group">
							<label for="anotacao"><s:message code="atividade.label.anotacao" /></label>
							<form:errors cssClass="error" path="anotacao" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:textarea id="anotacao" path="anotacao" title="${tooltipTeste}" rows="10" cssClass="span11"/>
							</div>
						</div>
						
						<div class="control-group">
							<label for="comentario"><s:message code="atividade.label.comentario" /></label>
							<form:errors cssClass="error" path="comentario" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:textarea id="comentario" path="comentario" title="${tooltipTeste}" rows="10" cssClass="span11" />
							</div>
						</div>
						
						<div class="control-group">
							<label for="restricoes"><s:message code="atividade.label.restricoes" /></label>
							<form:errors cssClass="error" path="restricoes" element="label"/>
							<s:message code="atividade.tooltip.teste" var="tooltipTeste" />
							<div class="controls">
								<form:textarea id="restricoes" path="restricoes" title="${tooltipTeste}" rows="10" cssClass="span11"/>
							</div>
						</div>
					</div>
				</div>
    			<div>
	    			<div class="form-actions" align="right">
	   					<s:message code="atividade.botaoSalvar" var="labelBotaoSalvar" />
						<input type="submit" id="botaoSalvar" value="${labelBotaoSalvar}" class="btn btn-primary" >
	      			</div>
	      		</div>
			</form:form>
			</div>
			
			<div class="span7 offset1" id="observacoes">
				<div class="alert block-message info" style="padding-top: 40px;">
				 		<strong class="large"><s:message code="fanaticos.compromissoInformacoesUteis.titulo" /></strong>
						<p>
						<s:message code="fanaticos.compromissoInformacoesUteis.parte1" />
						</p>
						<p>
						<s:message code="fanaticos.compromissoInformacoesUteis.parte2" />
						</p>
						<br/><br/>
						<strong class="large"><s:message code="avaliacao.dicasPreenchimento" /></strong>
						<ul>
						<li><s:message code="formulario.camposObrigatorios" /></li> 
						<li><s:message code="foto.texto.tamanhoMaximo" arguments="${tamanhoMaximoFoto}" /></li>
						<li><s:message code="foto.texto.resolucaoMaxima" arguments="${resolucaoMaximaFoto}" /></li>
						</ul>
				</div>
			</div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>