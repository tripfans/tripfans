<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Hospedagem" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		
			jQuery(document).ready(function() {
				
			    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
			    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
			    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
				
				$('#hospedagemForm').validate({
			        rules: {
			        	nomeLocal: 'required',
			        	emailContato: { 
		                	email: true
		            	},
		            	/*dataInicio: {
		                    date: true
		                },		            	
		                dataFim: { 
                            date: true
                        },
                        horaInicio: { 
                            time: true
                        },
                        horaFim: { 
                            time: true
                        },*/
                        dataReserva: { 
                            date: true
                        }
			        },
			        messages: { 
			        	nomeLocal: "<s:message code="validation.atividade.nomeLocal.required" />",
			        	emailContato:"<s:message code="validation.atividade.emailContato.valid" />"
			        },
			        //errorPlacement: function(error, element) {
			        //	var parent = element.parents("div.clearfix")[0];
			        //	error.appendTo(parent);
			        //},
			        errorContainer: "#errorBox"
			    });
				
				$('input:text').setMask();
				
				$('#botaoSalvar').bind('click', function(event) {
				    event.preventDefault();
				    $('#hospedagemForm').submit();
				});
				
			 	$('#hospedagemForm').initToolTips();

			 	$('#observacoes').stickySidebar();
			 	
			 	$('#dataInicio').datepicker({
			 		position: {
						my: 'left top',
						at: 'right top'
					},
					onSelect: function(dateText, inst) {
						$('#dataFim').datepicker('option', 'minDate', dateText );
					}
				});
			 	
			 	//$('#dataInicio').mask('99/99/9999');
			 	//$('#dataFim').mask('99/99/9999');
			 	
                //$('#dataInicio').datepicker('option', 'minDate', '<fmt:formatDate value="${viagem.dataInicio}" pattern="dd/MM/yyyy" />');
			 	//$('#dataFim').datepicker('option', 'maxDate', '<fmt:formatDate value="${viagem.dataFim}" pattern="dd/MM/yyyy" />');
			 	
			 	$('#dataFim').datepicker({
			 		position: {
						my: 'left',
					    at: 'right'
					}
				});

			 	$('#dataReserva').datepicker({
			 		position: {
						my: "left",
					    at: "right"
					}
				});
			 	$('#nomeAgencia').autocompleteFanaticos({
					 source: '${pageContext.request.contextPath}/planejamento/consultarAgenciasPorNome',
					 hiddenName : 'agencia',
				     valueField: 'id',
				     labelField: 'nome',
					 minLength: 3,
					 select: function(event, ui) { 
					     $('#telefoneAgencia').val(ui.item.telefone);
				//	     enableDisableField("telefoneAgencia");
					     $('#siteAgencia').val(ui.item.site);
				//	     enableDisableField("siteAgencia");
					 }
				});
			 	//$("#nomeAgencia").change(function() {
			 	//	$("#telefoneAgencia").val("");
			 	//	enableDisableField("telefoneAgencia");
				//     $("#siteAgencia").val("");
				// 	enableDisableField("siteAgencia");
			 	//});

			 	$('.collapsible').bind('click',function(event) {
			        $(this).parent().find(".panelContent").slideToggle('fast');
			        //$(this).toggleClass("active");
			    });
			});
			
			$('#horaInicio').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaInicio').val('<joda:format value="${hospedagem.horaInicio}" style="-S" />');
			
			$('#horaFim').timepicker({
		 		hourGrid: 4,
		 		minuteGrid: 10
			});
			
			$('#horaFim').val('<joda:format value="${hospedagem.horaFim}" style="-S" />');
			
			jQuery(function($){
			    $("#horaInicio").mask("99/99");
			});
			
			function selecionarHospedagem(event, ui) {
                $('#descricaoLocal').val(ui.item.descricao);
                enableDisableField("descricaoLocal");
                $('#enderecoLocal').val(ui.item.endereco.descricao);
                enableDisableField("enderecoLocal");
                $('#dataInicio').focus();
            }
			
            function removerHospedagem(event, ui) {
                $('#descricaoLocal').val('');
                enableDisableField("descricaoLocal");
                $('#enderecoLocal').val('');
                enableDisableField("enderecoLocal");
            }

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container-fluid">
            <div class="block withsidebar">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${editar == true ? 'Editar' : ''} Viagem '${viagem.nome}'</h2>
                </div>
                
                <div class="block_content">
                
                    <div class="sidebar_content" id="pageContent">
                    
                        <c:url value="/planejamento/salvarHospedagem" var="salvar_url" />
                        
                        <form:form id="hospedagemForm" action="${salvar_url}" modelAttribute="hospedagem" method="post" enctype="multipart/form-data">
                        
                            <div class="form-actions">
                                <div class="row">
                                    <div class="span10">
                                        <h2 class="leftAligned"><s:message code="hospedagem.titulo"/>
                                            <a href="${pageContext.request.contextPath}/planoViagem/${idViagem}">
                                                <span>${viagem.nome}</span>
                                            </a>
                                        </h2>
                                    </div>
                                    <!--div class="span3">
                                        <div class=rightAligned>
                                            <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                            
                                            <a id="botaoSalvar" class="btn btn-success" href="#">
                                                <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                            </a>
                                            
                                        </div>
                                    </div-->
                                </div>
                            </div>
                        
                            <input type="hidden" name="hospedagem" value="${hospedagem.id}" />
                            <input type="hidden" name="viagem" value="${viagem.id}" />
                            
                            <div class="row">
                            
                                <s:hasBindErrors name="hospedagem">
                                    <div id="errorBox" class="alert alert-error span6"> 
                                        <span class="large"><s:message code="validation.containErrors" /></span> 
                                    </div>
                                </s:hasBindErrors>
                                <div id="errorBox" class="alert alert-error span6" style="display: none;"> 
                                    <span class="large"><s:message code="validation.containErrors" /></span>
                                </div>
                            
                            </div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        <s:message code="hospedagem.legend.local" />
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-local"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-local" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">
                                    
                                        <div class="control-group">
                                            <label for="nomeLocal">
                                                <span class="required"><s:message code="atividade.label.nomeLocal" /></span>
                                                <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                            </label>
                                            <form:errors cssClass="error" path="nomeLocal" element="label"/>
                                            <div class="controls">
                                                <fan:autoComplete url="/planejamento/consultarHoteisPorNome" name="nomeLocal" hiddenName="local" id="novoDestino" 
                                                      valueField="id" labelField="nome" cssClass="span5" 
                                                      value="${hospedagem.local != null ? hospedagem.local.nome : ''}"
                                                      hiddenValue="${hospedagem.local != null ? hospedagem.local.id : ''}"
                                                      extraParams="{idViagem : ${viagem.id}}"
                                                      showActions="true" onSelect="selecionarHospedagem()" blockOnSelect="true" onRemove="removerHospedagem()"
                                                      placeholder="Informe o local..." />
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <label for="enderecoLocal">
                                                <s:message code="atividade.label.enderecoLocal" />
                                            </label>
                                            <form:errors cssClass="error" path="enderecoLocal" element="label"/>
                                            <div class="controls">
                                                <form:input id="enderecoLocal" path="enderecoLocal" cssClass="span7" disabled="${hospedagem.local != null}" />
                                            </div>
                                        </div>

                                        <div class="control-group">
                                            <label for="descricaoLocal">
                                                <s:message code="atividade.label.descricaoLocal" />
                                                <i id="help-nome" class="icon-question-sign helper" title=""></i>
                                            </label>
                                            <form:errors cssClass="error" path="descricaoLocal" element="label"/>
                                            <div class="controls">
                                                <form:input id="descricaoLocal" path="descricaoLocal" cssClass="span7" disabled="${hospedagem.local != null}" />
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        <s:message code="atividade.legend.periodo" />
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-periodo"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-periodo" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                                            <div class="row">
                                                
                                                <div class="span3">
                                                    <viagem:exibirPeriodo tipoPeriodo="${viagem.tipoPeriodo.codigo}"
                                                        idPorData="dataInicio" idPorDia="diaInicio" 
                                                        valuePorData="${hospedagem.dataInicio}" valuePorDia="${hospedagem.diaInicio}"
                                                        labelPorData="Data da chegada" labelPorDia="Dia da chegada" 
                                                        quantidadeDias="${viagem.quantidadeDias}"
                                                        pathPorData="dataInicio" pathPorDia="diaInicio" />
                                                </div>
                                                
                                                <div class="span2">
                                                    <label for="horaInicio">
                                                        Hora chegada
                                                    </label>
                                                    <form:errors cssClass="error" path="horaInicio" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="horaInicio" alt="time" path="horaInicio" class="span1" />
                                                    </div>
                                                </div>
                                                <%--div class="span5">
                                                        <label for="dataInicioTZ"><s:message code="hospedagem.label.dataInicioTZ" /></label>
                                                        <form:errors cssClass="error" path="dataInicioTZ" element="label"/>
                                                        <div class="controls">
                                                            <form:select id="dataInicioTZ" cssClass="span4" path="dataInicioTZ" title="${tooltipTeste}">
                                                                <form:option value="1">1</form:option>
                                                                <form:option value="2">2</form:option>
                                                                <form:option value="3">3</form:option>
                                                            </form:select>
                                                        </div>
                                                </div--%>
                                            </div>
                                        </div>
                                        
                                        <div class="control-group">
                                            <div class="row">
                                                <div class="span3">
                                                    <viagem:exibirPeriodo tipoPeriodo="${viagem.tipoPeriodo.codigo}"
                                                        idPorData="dataFim" idPorDia="diaFim" 
                                                        valuePorData="${hospedagem.dataFim}" valuePorDia="${hospedagem.diaFim}"
                                                        labelPorData="Data da partida" labelPorDia="Dia da partida" 
                                                        quantidadeDias="${viagem.quantidadeDias}"
                                                        pathPorData="dataFim" pathPorDia="diaFim" />
                                                </div>
                                                <div class="span2">
                                                    <label for="horaFim">
                                                        Hora partida
                                                    </label>
                                                    <form:errors cssClass="error" path="horaFim" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="horaFim" alt="time" path="horaFim" class="span1" />
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        <s:message code="hospedagem.legend.detalhesHospedagem" />
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-detalhes"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-detalhes" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                						 	<div class="row">
                								<div class="span3">
                									<label for="quantidadeParticipantes"><s:message code="atividade.label.quantidadeParticipantes" /></label>
                									<form:errors cssClass="error" path="quantidadeParticipantes" element="label"/>
                									<div class="controls">
                										<form:input id="quantidadeParticipantes" path="quantidadeParticipantes" cssClass="span1" alt="integer" maxlength="3" />
                									</div>
                								</div>
                								<div class="span2">
                									<label for="quantidadeQuartos"><s:message code="hospedagem.label.quantidadeQuartos" /></label>
                									<form:errors cssClass="error" path="quantidadeQuartos" element="label"/>
                									<div class="controls">
                										<form:input id="quantidadeQuartos" path="quantidadeQuartos" cssClass="span1" alt="integer" maxlength="3" />
                									</div>
                								</div>
                                                <div class="span2">
                                                    <label for="tipoQuartos"><s:message code="hospedagem.label.tipoQuartos" /></label>
                                                    <form:errors cssClass="error" path="tipoQuartos" element="label"/>
                                                    <div class="controls">
                                                        <form:input id="tipoQuartos" path="tipoQuartos" />
                                                    </div>
                                                </div>                                                
                							</div>
                						</div>
                						<div class="control-group">
                						 	<div class="row">

                								<div class="span3">
                									<label for="descricaoQuartos"><s:message code="hospedagem.label.descricaoQuartos" /></label>
                									<form:errors cssClass="error" path="descricaoQuartos" element="label"/>
                									<div class="controls">
                										<form:input id="descricaoQuartos" path="descricaoQuartos" cssClass="span8" />
                									</div>
                								</div>
                							</div>
                						</div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="accordion-group">
                                <div class="accordion-heading collapsible">
                                    <span class="accordion-toggle">
                                        Hóspedes?
                                        <div class="header-actions">
                                            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-detalhes"></i>
                                        </div>
                                    </span>
                                </div>
                                <div id="group-detalhes" class="accordion-body collapse in" style="height: auto; ">
                                    <div class="accordion-inner">

                                        <div class="control-group">
                                            
                                            <c:import url="selecionarParticipantesAtividade.jsp" />
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="horizontalSpacer"></div>
                            
                            <div class="form-actions" align="right">
                                <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
                                <a id="botaoCancelar" class="btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${hospedagem.id}" />">
                                    <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
                                </a>
                                <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
                                <a id="botaoSalvar" class="btn btn-success" href="#">
                                    <i class="icon-ok icon-white"></i> ${labelBotaoSalvar}
                                </a>
                            </div>
                            
                        </form:form>
                        
                    </div>
                
                </div>
                
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
           </div>
        </div>                        
			
		<%-->div class="span7 offset1" id="observacoes">
			<div class="alert block-message info" style="padding-top: 40px;">
			 		<strong class="large"><s:message code="fanaticos.compromissoInformacoesUteis.titulo" /></strong>
					<p>
					<s:message code="fanaticos.compromissoInformacoesUteis.parte1" />
					</p>
					<p>
					<s:message code="fanaticos.compromissoInformacoesUteis.parte2" />
					</p>
					<br/><br/>
					<strong class="large"><s:message code="avaliacao.dicasPreenchimento" /></strong>
					<ul>
					<li><s:message code="formulario.camposObrigatorios" /></li> 
					<li><s:message code="foto.texto.tamanhoMaximo" arguments="${tamanhoMaximoFoto}" /></li>
					<li><s:message code="foto.texto.resolucaoMaxima" arguments="${resolucaoMaximaFoto}" /></li>
					</ul>
			</div>
		</div--%>
	</tiles:putAttribute>

</tiles:insertDefinition>