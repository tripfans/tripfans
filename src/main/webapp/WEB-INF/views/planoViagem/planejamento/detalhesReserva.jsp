    <div class="horizontalSpacer"></div>

    <div class="accordion-group">
        <div class="accordion-heading collapsible">
            <span class="accordion-toggle">
                <s:message code="atividade.legend.reserva" />
                <div class="header-actions">
                    <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-reserva"></i>
                </div>
            </span>
        </div>
        <div id="group-reserva" class="accordion-body collapse in" style="height: auto; ">
            <div class="accordion-inner">

                <div class="control-group">
                    <div class="row">
                        <div class="span5">
                            <label for="dataReserva"><s:message code="atividade.label.dataReserva" /></label>
                            <form:errors cssClass="error" path="detalheReserva.data" element="label"/>
                            <div class="controls">
                                <form:input alt="date" id="dataReserva" path="detalheReserva.data" />
                            </div>
                        </div>
                        <%--div class="span5">
                                <label for="dataReservaTZ"><s:message code="atividade.label.dataReservaTZ" /></label>
                                <form:errors cssClass="error" path="dataReservaTZ" element="label"/>
                                <div class="controls">
                                    <form:select id="dataReservaTZ" cssClass="span4" path="dataReservaTZ" title="${tooltipTeste}">
                                            <form:option value="1">1</form:option>
                                            <form:option value="2">2</form:option>
                                            <form:option value="3">3</form:option>
                                        </form:select>
                                </div>
                        </div --%>
                    </div>
                </div>
                <%--div class="control-group">
                    <label for="nomeAgencia"><s:message code="atividade.label.nomeAgencia" /></label>
                    <form:errors cssClass="error" path="nomeAgencia" element="label"/>
                    <div class="controls">
                        <form:input id="nomeAgencia" path="nomeAgencia" class="span11"/>
                    </div>
                </div>
                 
                <div class="control-group">
                    <div class="row">
                        <div class="span5">
                            <label for="telefoneAgencia"><s:message code="atividade.label.telefoneAgencia" /></label>
                            <form:errors cssClass="error" path="telefoneAgencia" element="label"/>
                            <div class="controls">
                                <form:input alt="phone-internacional" id="telefoneAgencia" path="telefoneAgencia" />
                            </div>
                        </div>
                        <div class="span5">
                            <label for="siteAgencia"><s:message code="atividade.label.siteAgencia" /></label>
                            <form:errors cssClass="error" path="siteAgencia" element="label"/>
                            <div class="controls">
                                <form:input id="siteAgencia" path="siteAgencia" />
                            </div>
                        </div>
                    </div>
                </div>
                    
                <div class="control-group">
                    <div class="row">
                        <div class="span5">
                            <label for="nomeContato"><s:message code="atividade.label.nomeContato" /></label>
                            <form:errors cssClass="error" path="nomeContato" element="label"/>
                            <div class="controls">
                                <form:input id="nomeContato" path="nomeContato" />
                            </div>
                        </div>
                        <div class="span5">
                            <label for="telefoneContato"><s:message code="atividade.label.telefoneContato" /></label>
                            <form:errors cssClass="error" path="telefoneContato" element="label"/>
                            <div class="controls">
                                <form:input alt="phone-internacional" id="telefoneContato" path="telefoneContato" />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="emailContato"><s:message code="atividade.label.emailContato" /></label>
                    <form:errors cssClass="error" path="emailContato" element="label"/>
                    <div class="controls-prepend">
                        <span class="add-on">
                            <i class="icon-envelope"></i>
                        </span>
                        <form:input id="emailContato" path="emailContato" class="span10" />
                    </div>
                </div>
                                        
                <div class="control-group">
                    <div class="row">
                        <div class="span5">
                            <label for="codigoReferenciaReserva"><s:message code="atividade.label.codigoReferenciaReserva" /></label>
                            <form:errors cssClass="error" path="codigoReferenciaReserva" element="label"/>
                            <div class="controls">
                                <form:input id="codigoReferenciaReserva" path="codigoReferenciaReserva" />
                            </div>
                        </div>
                        <div class="span5">
                            <label for="confirmacao"><s:message code="atividade.label.confirmacao" /></label>
                            <form:errors cssClass="error" path="confirmacao" element="label"/>
                            <div class="controls">
                                <form:input id="confirmacao" path="confirmacao" />
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="control-group">
                    <label for="valorPago"><s:message code="atividade.label.valorPago" /></label>
                    <form:errors cssClass="error" path="valorPago" element="label"/>
                    <div class="controls">
                        <form:input alt="decimal" id="valorPago" path="valorPago" />
                    </div>
                </div --%>
                
            </div>
        </div>
    </div>