<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <script type="text/javascript">
    
        $(document).ready(function() {
            $('.helper').tooltip();
            configurarForm('viagemForm');
            
            $('#next-button').bind('click', function(event) {
            	$(this).button('loading');
                $('#viagemForm').submit();
            });

        });
        
        function configurarForm(nomeForm) {
            
            var form = $('#' + nomeForm)
            
            form.unbind('submit');
            
            form.submit(function(event) {
                
                /* stop form from submitting normally */
                event.preventDefault();
                
                var formData = form.serialize();
                
                //if (form.valid()) {
                    
                    $.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        data: formData,
                        success: function(data) {
                        	
                            if (data.error != null) {
                                //$("#errorMsg").empty().append(data.error);
                                //$("#errors").show();
                            } else {
                                var params = '';
                                if (data.params != null) {
                                    params = data.params;
                                }
                                
                                if (params.success) {
                                    var url = '<c:url value="/planoViagem/" />' + params.idViagem + '/wizard/passo/' + $('#proximoPasso').val();
                                    $("#content").load(url);
                                    $('#proximoPasso').val(parseInt($('#proximoPasso').val()) + 1);
                                    $('#next-button').button('reset');
                                }
                                
                            }
                        }, failure : function() {
                        	$('#next-button').button('reset');
                        }
                    });
                //}
                
            });
            
        }
        
    </script>
        
    <div>
        <h2>Criar uma viagem</h2>
        
        <div style="border-bottom: 1px solid #eeeeee;"></div>
        
            <div id="content" style="height: 450px; width: 900px; overflow: auto;">
               
                <c:url value="/planoViagem/wizard/salvar" var="salvar_url" />
        
                <form:form id="viagemForm" action="${salvar_url}" modelAttribute="viagem" enctype="multipart/form-data">
            
                    <input id="proximoPasso" type="hidden" name="proximoPasso" value="2" />
                
                    <%@include file="passo1.jsp" %>
                        
                </form:form>
                
            </div>

        </div>
        
    </div>

    <div id="navigation" class="form-actions" style="margin-top: 0px; margin-bottom: 0px;">
    
        <div class="left">
            <a id="cancel-button" class="btn span2"">
                <i class="icon-remove"></i>
                Cancelar
            </a>
        </div>

        <div class="right">
            <a id="pass-button" class="btn span2" id="next" data-loading-text="Aguarde...">
                <i class="icon-share-alt"></i>
                Pular
            </a>

            <a id="next-button" class="btn btn-primary span3" id="next" data-loading-text="Aguarde...">
                <i class="icon-arrow-right icon-white"></i>
                Salvar e Continuar
            </a>
        </div>

    </div>