<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <style>
        
        .vertical-carousel {
            background-color: #EEE;
        }
        
        .vertical-carousel ul.vertical-carousel-list {
            position: relative;
            margin: 0 auto;
            top: 0px;
            padding:0px;
        }
        
        </style>
    </tiles:putAttribute>

    <tiles:putAttribute name="body">
    
        <script>
            $(document).ready(function() {
                
                $('#painel-atividades').verticalCarousel({
                    nSlots: 4, speed: 400
                });

                $('#barra-dias-atividades').verticalCarousel({
                    nSlots: 7, speed: 400
                });
                
                $('#tab-dias-atividades a:first').tab('show');
                
                $('#tab-dias-atividades a').click(function (e) {
            	    e.preventDefault();
            	    $(this).tab('show');
            	});
                
                $('.sortable').sortable({
                    revert: true
                });
                
                $('.draggable').draggable({
                    connectToSortable: '.sortable',
                    //helper: 'clone',
                    revert: 'invalid',
                    //cursor: 'move',
                    axis: 'y',
                });
                
                $('.scroll-button').mouseout(function() {
                    var $item = $(this);
                    clearTimeout($item.data('timeout'));
                });
                
                $('.scroll-button').mouseover(function() {
                	var $item = $(this);
                	var timeout = setTimeout(function() {
            		    $item.click();
                    }, 400);
                	
                	$item.data('timeout', timeout);
                });
                
                $('.draggable').mouseover(function() {
                    var item = $(this);
                    
                    //clearTimeout($('#flip').data('timeout'));
                    
                    //item.css('background-color', '#f11');
                    //item.toggleClass('alert-success');
                    var timeout = setTimeout(function() { 
                        item.click();
                    }, 400);
                    //$('#flip').data('timeout', timeout);
                });
                
                $('ul, li').disableSelection();
                
            });
        </script>
    
        <div class="titulo">
            <a href="#">
                Resumo da Viagem
            </a>
        </div>
        
        <div class="widget-resumo-viagem">
        
            <div id="accordion2" class="accordion">
    
                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
                            Atividades
                        </a>
                    </div>
                    
                    <div style="height: auto;" id="collapseOne" class="accordion-body in collapse">
                    
                        <div class="accordion-inner" style="padding: 0px;">
                        
                            <div style="width: 40px; float: left">
                                
                                <div id="tab-dias-atividades" class="tabbable tabs-left">
                                
                                    <div id="barra-dias-atividades" class="vertical-carousel">
                                        
                                        <div class="widget-resumo-viagem-botao-topo-small">
                                            <a href="#" class="scru scroll-button">Anterior</a>
                                        </div>
                                        
                                        <div class="vertical-carousel-container">
                                    
                                            <ul class="nav nav-tabs vertical-carousel-list" style="padding-left: 4px; padding-right: 0px;">
                                                
                                                <li>
                                                    <a href="#A" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>1</p>
                                                        Jun
                                                    </a>
                                                </li>
                                
                                                <li>
                                                    <a href="#B" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>2</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#C" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>3</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#D" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>4</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#E data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>5</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#F" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>6</p>
                                                        Jun
                                                    </a>
                                                </li>
                                
                                                <li>
                                                    <a href="#G data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>7</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#H" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>8</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#I" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>9</p>
                                                        Jun
                                                    </a>
                                                </li>
        
                                                <li>
                                                    <a href="#J" data-toggle="tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                        <p>10</p>
                                                        Jun
                                                    </a>
                                                </li>
                                            </ul>
                                            
                                        </div>
                                        
                                        <div class="widget-resumo-viagem-botao-rodape-small">
                                            <a href="#" class="scrd scroll-button">Proximo</a>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                        
                            </div>
                            
                            <div style="width: 280px; float: right">
                            
                                <div id="painel-atividades" class="vertical-carousel">
                            
                                    <div class="widget-resumo-viagem-botao-topo">
                                        <a href="#" class="scru scroll-button">Anterior</a>
                                    </div>
                                    
                                    <div class="vertical-carousel-container">
                                    
                                        <div class="tab-content" style="overflow: hidden;">
                                        
                                            <div class="tab-pane active" id="A">
                                            
                                                <ul class="barra-dias">
                                                
                                                    <li>
                                                    
                                                        <ul class="vertical-carousel-list sortable">
                    
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto" >
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                    <div class="acoes">
                                                                       <i class="icon-arrow-up"></i>
                                                                       <i class="icon-arrow-down"></i>
                                                                       <i class="icon-trash"></i>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto">
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                    
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto">
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                    
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto">
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto">
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                    
                                                            <li class="item draggable">
                                                                <div class="conteudo">
                                                                    <div class="foto">
                                                                        <a href="#">
                                                                            <img width="90" height="68" src="http://s2.glbimg.com/hl5BLvOGw9TG96bj-ynV0mlhTTJLThqaZGfvvj0jZwuNNUzLmNFMwhEmqc9xuK4i/s.glbimg.com/es/ge/f/original/2011/05/05/ingressostorcidacoritiba_diegoribeiro.jpg" alt="Apesar da chuva, torcida do Coxa faz fila por ingressos contra o São Paulo">
                                                                        </a>
                                                                    </div>
                                                                    <div class="texto">
                                                                        <p class="hora">
                                                                            <i class="icon-time"></i>
                                                                            14:57
                                                                        </p>
                                                                        <a href="#">
                                                                            <strong>Restaurante Porcão</strong>
                                                                        </a>
                                                                        <p>Rio de Janeiro, RJ</p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            
                                                        </ul>
                                                        
                                                    </li>
            
                                                </ul>
                                            </div>
                                            <div class="tab-pane" id="B">
                                            </div>
                                            <div class="tab-pane" id="C">...</div>
                                            <div class="tab-pane" id="D">...</div>
                                        </div>                                    
                                    
                                    </div>
                                    
                                    <div class="widget-resumo-viagem-botao-rodape">
                                        <a href="#" class="scrd scroll-button">Anterior</a>
                                    </div>
                                    
                                </div>

                            </div>
    
                        </div>
                        
                    </div>
                    
                </div>
            
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                      Collapsible Group Item #2
                    </a>
                  </div>
                  <div id="collapseTwo" class="accordion-body collapse">
                    <div class="accordion-inner">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                <div class="accordion-group">
                  <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
                      Collapsible Group Item #3
                    </a>
                  </div>
                  <div id="collapseThree" class="accordion-body collapse">
                    <div class="accordion-inner">
                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                  </div>
                </div>
                
            </div>
                
        </div>        
        
    </tiles:putAttribute>

</tiles:insertDefinition>