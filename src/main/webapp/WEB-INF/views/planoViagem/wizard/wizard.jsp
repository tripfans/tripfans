<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <style>
        
            .nav-pills li>a:hover{background-color: transparent;}

            .passed {
                -webkit-border-radius:5px;-moz-border-radius:5px;border-radius:5px;
                background-color:#d9edf7;
            }
        </style>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <c:choose>
            <c:when test="${criacaoDiario == true}">
                <c:set var="verboPassado" value="foi" />
                <c:set var="urlWizard" value="/diario/wizard" />
            </c:when>
            <c:otherwise>
                <c:set var="verboPassado" value="vai" />
                <c:set var="urlWizard" value="/wizard" />
            </c:otherwise>
        </c:choose>
    
        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 15px 14px 35px; margin: 0 auto;">
    
            <div class="row">
            
                <div class="page-header">
                    <h1>
                        <c:choose>
                            <c:when test="${criacaoDiario == true}">
                                Criar um Diário de Viagem
                            </c:when>
                            <c:otherwise>
                                Criar um Plano de Viagem
                            </c:otherwise>
                        </c:choose>
                    </h1>
                </div>
                
                <div>
                
                  <c:if test="${not criacaoDiario}">
                
                    <ul id="passos-tabs" class="nav nav-pills">
                        <li id="passo1" class="active">
                            <a>1. Sobre a viagem</a>
                        </li>
                
                      <c:choose>
                        <c:when test="${criacaoDiario == true}">
                            <li id="passo2" class="disabled" data-passo="4">
                                <a>2. Companheiros</a>
                            </li>
                            <li id="passo3" class="disabled" data-passo="4">
                                <a>3. Concluído</a>
                            </li>
                        </c:when>
                        <c:otherwise>
<%-- 
                            <li id="passo2" class="disabled" data-passo="2">
                                <a class="disabled">2. Como você vai?</a>
                            </li>
--%>
                            <li id="passo3" class="disabled" data-passo="2">
                                <a>2. Atividades/Atrações</a>
                            </li>
<%-- 
                            <li id="passo4" class="disabled" data-passo="3">
                                <a>3. Companheiros</a>
                            </li>
--%>
                            <li id="passo4" class="disabled" data-passo="3">
                                <a>3. Concluído</a>
                            </li>

                        </c:otherwise>
                      </c:choose>
                
                    </ul>
                    
                  </c:if>
                    
                    <div id="content" class="page-container">
                        <%@include file="passo1.jsp" %>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
    
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
    
        <script type="text/javascript">
        
            $(document).ready(function() {
                //$('.helper').tooltip();
                /*configurarForm('viagemForm');
                
                $('#next-button').bind('click', function(event) {
                    
                    //$(this).button('loading');
                    $('#viagemForm').submit();
                });*/
            });
            
            function submitForm(form) {
                var formData = form.serialize();
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: formData,
                    success: function(data) {
                        
                        if (data.error != null) {
                            //$("#errorMsg").empty().append(data.error);
                            //$("#errors").show();
                        } else {
                            var params = '';
                            if (data.params != null) {
                                params = data.params;
                            }
                            
                            if (params.success) {
                                var proximoPasso = $('#proximoPasso').val();
                                var passoAtual = parseInt(proximoPasso) - 1;
                                var url = '<c:url value="/planoViagem/" />' + params.idViagem + '${urlWizard}/passo/' + proximoPasso;
                                $('#passo' + passoAtual).toggleClass('active');
                                $('#passo' + passoAtual).toggleClass('passed');
                                
                                $('#passo' + proximoPasso).toggleClass('active');
                                
                                $('#content').load(url, {}, 
                                    function(response) {
                                        $('#content').initToolTips();
                                    }
                                );
                                
                                $('#proximoPasso').val(parseInt($('#proximoPasso').val()) + 1);
                                //$('#next-button').button('reset');
                            }
                            
                        }
                    }, failure : function() {
                        $('#next-button').button('reset');
                    }
                });
            }
            
            function configurarForm(nomeForm) {
                
                var form = $('#' + nomeForm)
                
                form.unbind('submit');
                
                form.submit(function(event) {
                    
                    event.preventDefault();
                    
                    var formData = form.serialize();
                    
                    //if (form.valid()) {
                        
                        $.ajax({
                            type: 'POST',
                            url: form.attr('action'),
                            data: formData,
                            success: function(data) {
                                
                                if (data.error != null) {
                                    //$("#errorMsg").empty().append(data.error);
                                    //$("#errors").show();
                                } else {
                                    var params = '';
                                    if (data.params != null) {
                                        params = data.params;
                                    }
                                    
                                    if (params.success) {
                                        var proximoPasso = $('#proximoPasso').val();
                                        var passoAtual = parseInt(proximoPasso) - 1;
                                        var url = '<c:url value="/planoViagem/" />' + params.idViagem + '${urlWizard}/passo/' + proximoPasso;
                                        $('#passo' + passoAtual).toggleClass('active');
                                        $('#passo' + passoAtual).toggleClass('passed');
                                        
                                        $('#passo' + proximoPasso).toggleClass('active');
                                        
                                        $('#content').load(url, {}, 
                                            function(response) {
                                                $('#content').initToolTips();
                                            }
                                        );
                                        //$('#content').load('<c:url value="/planoViagem/" />2/wizard/passo/3');
                                        
                                        $('#proximoPasso').val(parseInt($('#proximoPasso').val()) + 1);
                                        $('#next-button').button('reset');
                                    }
                                    
                                }
                            }, failure : function() {
                                $('#next-button').button('reset');
                            }
                        });
                    //}
                    
                });
                
            }
        </script>
    </tiles:putAttribute>    

</tiles:insertDefinition>