<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${empty amigos}">
    <div style="height: 50px;"></div>
    <div class="blank-state">
        <div class="row">
            <div class="span1 offset1">
                <img src="<c:url value="/resources/images/no_photo.png" />" />
            </div>
            <div class="span3 offset1">
                <h3>N�o h� amigos para exibir</h3>
                <p>
                    Este usu�rio ainda n�o adicionou nenhum amigo.
                </p>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${not empty amigos}">

    <h3>Amigos</h3>

</c:if>