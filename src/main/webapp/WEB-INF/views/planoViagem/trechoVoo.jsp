<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>

    <c:if test="${indice == null}">
        <c:set var="indice" value="${param.indice}" />
    </c:if>
    
    <h3>
        <span class="blue">Voo ${indice + 1}</span> <span id="status-trecho-${indice + 1}" class="label label-important" style="display: none;">Trecho removido</span>
        
        <div class="right">
            <a id="remover-trecho-${indice + 1}"
               class="btn btn-danger btn-mini removerTrecho" title="Remover este trecho" 
               href="#" onclick="removerTrecho(${indice + 1})">
                <i class="icon-minus icon-white"></i> Excluir
            </a>
            <a id="desfazer-remover-trecho-${indice + 1}" 
               class="btn btn-info btn-mini desfazerRemoverTrecho" title="Desfazer exlus�o do trecho" 
               href="#" onclick="desfazerRemoverTrecho(${indice + 1})" style="display: none;">
                <i class="icon-arrow-left icon-white"></i> Desfazer
            </a>
        </div>
    </h3>
    
    <div class="horizontalBar"></div>

    <div id="div-trecho-${indice + 1}" class="control-group">
        <div class="row">
            <div class="span8">
                <label for="empresaAerea">
                    Empresa a�rea
                </label>
                <form:errors cssClass="error" path="trechos[${indice}].nomeCompanhia" element="label"/>
                <div class="controls">
                    <fan:autoComplete url="/planejamento/consultarCompanhias" name="trechos[${indice}].nomeCompanhia"
                          value="${voo.trechos[indice].nomeCompanhia}" minLength="3"
                          hiddenName="trechos[${indice}].companhia" id="companhia-${indice}"
                          valueField="id" labelField="nome" cssClass="span7" 
                          hiddenValue="${voo.trechos[indice].companhia.id}" limit="10"
                          showActions="true" blockOnSelect="true"
                          placeholder="Informe a companhia a�rea..." />
                </div>
            </div>
              
            <div class="span2">
                <label for="numeroVoo">
                    N�mero do Voo
                </label>
                <form:errors cssClass="error" path="trechos[${indice}].codigo" element="label"/>
                <div class="controls">
                    <form:input path="trechos[${indice}].codigo" class="span2" />
                </div>
            </div>
            
        </div>
        
        <h4><span class="green">Partida</span></h4>
        
        <div class="row">
        
            <div class="span8">
                <label for="aeroporto">
                    De (cidade ou aeroporto)
                </label>
                <%--form:errors cssClass="error" path="trechos[${indice}].partida.data" element="label"/ --%>
                <div class="controls">
                    <fan:autoComplete url="/locais/consultarAeroportos" name="trechos[${indice}].partida.nomeLocal"
                          value="${voo.trechos[indice].partida.nomeLocal}"  minLength="3"
                          hiddenName="trechos[${indice}].partida.local" id="aeroportoOrigem-${indice}" 
                          valueField="id" labelField="nome" cssClass="span7" 
                          hiddenValue="${voo.trechos[indice].partida.local.id}" limit="10"
                          showActions="true" blockOnSelect="true"
                          jsonFields="[{name : 'id'}, {name : 'nome'}]"
                          placeholder="Informe o local de partida..." />
                </div>
            </div>
            
            <div class="span2">
                <viagem:exibirPeriodo tipoPeriodo="${param.tipoPeriodo}"
                    idPorData="data-partida-${indice}" idPorDia="dia-partida-${indice}" 
                    valuePorData="${voo.dataInicio}" valuePorDia="${voo.diaInicio}"
                    labelPorData="Data" labelPorDia="Dia" 
                    quantidadeDias="${param.quantidadeDias}"
                    pathPorData="trechos[${indice}].partida.data" pathPorDia="trechos[${indice}].partida.dia" />
            </div>

            <div class="span2">
                <label for="hora-partida-${indice}">
                    Hor�rio
                </label>
                <form:errors cssClass="error" path="trechos[${indice}].partida.hora" element="label"/>
                <div class="controls">
                    <form:input id="hora-partida-${indice}" alt="time" path="trechos[${indice}].partida.hora" class="span1 timePicker" />
                </div>
            </div>
            
        </div>
        
        <h4><span class="red">Chegada</span></h4>
        
        <div class="row">
        
            <div class="span8">
                <label for="aeroportoChegada">
                    Para (cidade ou aeroporto)
                </label>
                <form:errors cssClass="error" path="trechos[${indice}].chegada.data" element="label"/>
                <div class="controls">
                    <fan:autoComplete url="/locais/consultarAeroportos" name="trechos[${indice}].chegada.nomeLocal"
                          value="${voo.trechos[indice].chegada.nomeLocal}" minLength="3"
                          hiddenName="trechos[${indice}].chegada.local" id="aeroportoDestino-${indice}" 
                          valueField="id" labelField="nome" cssClass="span7" 
                          hiddenValue="${voo.trechos[indice].chegada.local.id}" limit="10"
                          showActions="true" blockOnSelect="true"
                          jsonFields="[{name : 'id'}, {name : 'nome'}]"
                          placeholder="Informe o local de chegada..." />
                </div>
            </div>
            
            <div class="span2">
            
                <viagem:exibirPeriodo tipoPeriodo="${param.tipoPeriodo}"
                        idPorData="data-chegada-${indice}" idPorDia="dia-chegada-${indice}" 
                        valuePorData="${voo.dataFim}" valuePorDia="${voo.diaFim}"
                        labelPorData="Data" labelPorDia="Dia" 
                        quantidadeDias="${param.quantidadeDias}"
                        pathPorData="trechos[${indice}].chegada.data" pathPorDia="trechos[${indice}].chegada.dia" />
                        
            </div>

            <div class="span2">
                <label for="hora-chegada-${indice}">
                    Hor�rio
                </label>
                <form:errors cssClass="error" path="trechos[${indice}].chegada.hora" element="label"/>
                <div class="controls">
                    <form:input id="hora-chegada-${indice}" alt="time" path="trechos[${indice}].chegada.hora" class="span1 timePicker" />
                </div>
            </div>

            
        </div>
        
    </div>
    
     <script>
        $(document).ready(function() {
            $('#data-partida-${indice}').datepicker({
                position: {
                    my: 'left top',
                    at: 'right top'
                },
                onSelect: function(dateText, inst) {
                    $('#data-chegada-${indice}').datepicker('option', 'minDate', dateText );
                }
            });
            
            $('#data-chegada-${indice}').datepicker({
                position: {
                    my: 'left',
                    at: 'right'
                }
            });
            
            $('#hora-partida-${indice}').timepicker({
                hourGrid: 4,
                minuteGrid: 10
            });

            $('#hora-chegada-${indice}').timepicker({
                hourGrid: 4,
                minuteGrid: 10
            });
        	
        });
            
        </script>