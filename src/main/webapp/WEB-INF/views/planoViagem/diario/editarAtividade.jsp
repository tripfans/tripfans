<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <div>
                            
        <fieldset style="background-color: #fff;">
          
            <legend>
                <h4>Atividade</h4>
            </legend>
            
<%--             <span>
                Você foi à este local?
                <div class="btn-group " data-toggle="buttons-radio">
                    <a class="btn exibirAtividade active" data-atividade-id="${item.atividade.id}" data-value="true">
                        Sim
                    </a>
                    <a class="btn exibirAtividade" data-atividade-id="${item.atividade.id}" data-value="false">
                        Não
                    </a>
                </div>
            
                <label for="foi-${item.atividade.id}" class="first on" >
                    Fui
                    <input id="foi-${item.atividade.id}" type="radio" name="item[${item.atividade.id}].foi" value="true" class="fancy exibirAtividade" data-atividade-id="${item.atividade.id}">
                </label>
                <label for="nao-fui-${item.atividade.id}">
                    Não fui
                    <input id="nao-fui-${item.atividade.id}" type="radio" name="item[${item.atividade.id}].foi" value="false" class="fancy exibirAtividade" data-atividade-id="${item.atividade.id}">
                </label
            </span>
 --%>
 
            <fieldset id="fieldset-add-atividade" class="fieldset-form">
            
                <legend>
                    Informações sobre esta Atividade
                </legend>
            
                <form class="options-list label-right" style="margin: 0px;">
                
                    <ul style="margin: 0px; list-style: none;">
                    
                        <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Título
                                </span>
                            </label>
                            <input type="text" id="titulo" name="titulo" value="${atividade.titulo}" class="span7" placeholder="Informe um título para sua história (opcional)" />
                        </li>
                    

                        <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Horário
                                </span>
                            </label>
                            <input type="text" id="horaInicio" name="horaInicio" value="<joda:format value="${atividade.horaInicio}" pattern="HH:mm" />" alt="time" class="span2" placeholder="Horário (opcional)" />
                        </li>

                        <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Local
                                </span>
                            </label>
                            <fan:autoComplete url="/pesquisaTextual/consultarLocaisAtividades" hiddenName="local" id="novaAtividade" 
                                              valueField="id" labelField="nomeExibicao" cssStyle="position: relative;"
                                              showActions="true" blockOnSelect="false" name="pesquisaLocais" cssClass="span7"
                                              placeholder="Informe no nome do local que você visitou" 
                                              hiddenValue="${atividade.local.id}" value="${atividade.nomeLocal}" 
                                              disabled="${atividade.preencheuDadosDiario}" addOnLarge="true"
                                              tooltip="${atividade.preencheuDadosDiario ? 'Não é possível alterar o local de atividades que já possuam relatos, fotos, avaliações e dicas' : ''}"  />
                        </li>
                        <li>
                            <label class="span2" style="text-align: right; margin-right: 15px;">
                            </label>
                            <span>
                                <a id="btn-salvar-atividade" href="#" class="btn btn-primary" data-loading-text="Aguarde...">
                                    <i class="icon-ok icon-white"></i> Salvar Alterações
                                </a>
                            </span>                            
                        </li>

                    </ul>
                    <input type="hidden" name="dataInicio" value="<joda:format pattern="dd/MM/yyyy" value="${diaViagem.data}" />" />
                </form>
            
            </fieldset> 
 
            <ul id="tabs-atividade" class="nav nav-tabs" style="height: 35px; max-height: 35px;">
                <li class="active">
                    <a href="#tab-relato" data-toggle="tab">Relato</a>
                </li>
                <li>
                    <a href="#tab-fotos" data-toggle="tab">Fotos</a>
                </li>
                <c:if test="${not empty atividade.local}">
                  <li>
                    <a href="#tab-avaliacao" data-toggle="tab">Avaliação</a>
                  </li>
                  <li>
                    <a href="#tab-dicas" data-toggle="tab">Dicas</a>
                  </li>
                </c:if>
            </ul>
            
            <div class="tab-content">
                <div class="tab-pane active" id="tab-relato">

                    <div style="margin: 8px;">
                        
                        <c:url value="/planoViagem/diario/atividade/salvar" var="salvar_relato_url" />
                    
                        <form:form id="relato-form" action="${salvar_relato_url}" modelAttribute="atividade" class="diarioForm">
                    
                            <input type="hidden" name="atividade" value="${atividade.id}" />
                            <input type="hidden" name="relato" value="${atividade.relato.id}" />
        <%--                     <form:hidden id="hidden-${item.atividade.id}" path="itens[${status.index}].ocultar" />
         --%>
         
        <%--                     Exibir no meu diário
                            
                            <div class="btn-group " data-toggle="buttons-radio">
                                <a class="btn exibirRelato active" data-atividade-id="${item.atividade.id}" data-value="true">
                                    Sim
                                </a>
                                <a class="btn exibirRelato" data-atividade-id="${item.atividade.id}" data-value="false">
                                    Não
                                </a>
                            </div>
         --%>                    
                            <div id="painel-relato">
                        
                                Seu relato sobre esta atividade
                                <i class="icon-question-sign helper" title="Conte-nos suas histórias durante sua vista a este local."></i>
                                
                                <div id="error-box-relato" style="display: none;" class="alert alert-error centerAligned"> 
                                    <p id="errorMsg">
                                    Corrija os erros a seguir antes de prosseguir
                                    </p>
                                </div>
                                
                                <form:textarea id="text-editor" path="relato.texto" cols="80" rows="10" class="text-editor" cssStyle="display: none;" />
                                
                                <div id="relato-error"></div>
                                                            
                                <div style="width: 100%; margin-bottom: 0px;">
                                    <div align="right">
                                        <div class="form-actions">
                                          <div class="btn-toolbar">
                                            <button id="btn-excluir-relato" class="btn btn-danger ${empty atividade.relato.id ? 'hide' : ''} excluir" data-loading-text="Aguarde..." data-complete-text="Excluído!">
                                                <i class="icon-remove icon-white"></i> Excluir Relato
                                            </button>
                                            <button id="btn-salvar-relato" class="btn btn-primary salvar" data-loading-text="Aguarde..." data-complete-text="Salvo!">
                                                <i class="icon-ok icon-white"></i> Salvar Relato
                                            </button>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </form:form>
                    </div>

                </div>
                <div class="tab-pane" id="tab-fotos" style="min-height: 130px;">
                    
                    <div style="margin: 8px;">
                        <div style="padding-bottom: 10px;">
                            <fan:addPhotosButton modalId="add-foto-modal" uploadUrl="/fotos/atividade/adicionarFoto" 
                                                 buttonId="add-foto-btn" id="add-foto" idAtividade="${atividade.id}"
                                                 callback="aposUploadFotos" />
                        </div>
                        
                        <div id="painel-fotos">
                            <c:import url="carrosselFotos.jsp">
                                <c:param name="exibirOpcoes" value="true"/>
                            </c:import>
                        </div>
                    </div>
                    
                </div>
                
                <c:if test="${not empty atividade.local}">
                
                  <div class="tab-pane" id="tab-avaliacao">
                  
                    <fieldset style="background-color: #f8f8f8;">
          
                        <legend>
                            <h4>Avalição sobre este Local</h4>
                        </legend>
                    
                        <div style="margin: 8px;" class="row">
                            
                            <div id="painel-avaliacao">
                                <div id="div-form-avaliacao" class="${atividade.avaliacao == null ? 'show' : 'hide'}">
                                    <div id="error-box-avaliacao" style="display: none;" class="alert alert-error centerAligned"> 
                                        <p id="errorMsg">
                                        Corrija os erros a seguir antes de prosseguir
                                        </p>
                                    </div>
                                    <form:form id="avaliacaoForm" action="${pageContext.request.contextPath}/avaliacao/atividade/${atividade.id}/incluir" method="post" enctype="multipart/form-data">
                                      <fan:bodyFormAvaliacao local="${local}" criteriosAvaliacao="${atividade.local.tipoLocal.avaliacoesCriterios}"
                                                             mesVisita="${atividade.mesInicio}" anoVisita="${atividade.anoInicio}" desabilitarQuandoFoi="${atividade.viagem.periodoPorDatas}"
                                                             labelSpanClass="span4" fieldSpanClass="span8" mostrarUploadFotos="false" backgroundColor="#fff" />
                                    </form:form>
                                </div>
                                
                                <c:if test="${atividade.avaliacao != null}">
                                    <fan:avaliacao avaliacao="${atividade.avaliacao}" fullSize="12" userSize="2" dataSize="8" callbackAfterDelete="aposExclusaoAvaliacao" />
                                </c:if>
                            </div>
                            
                        </div>
                        
                    </fieldset>
                    
                  </div>
                  <div class="tab-pane" id="tab-dicas">
                  
                    <fieldset style="background-color: #f8f8f8;">
          
                        <legend>
                            <h4>Deixe dicas sobre este Local</h4>
                        </legend>
                        
                        <div id="painel-add-dica" class="btn-group" data-toggle="buttons-checkbox" style="margin-bottom: 5px; margin-left: 10px;">
                          <button id="btn-add-dica" type="button" class="btn btn-adicionar-dica ${not empty atividade.dicas ? '' : 'active'}" data-fieldset-id="form-dicas">
                            <i class="icon-plus"></i>
                            Adicionar nova dica
                          </button>
                        </div>

                        <div style="margin: 8px;">
                            
                            <div class="row" style="margin-left: 0;">
                                <div id="form-dicas" class="${not empty atividade.dicas ? 'hide' : ''}">
                                    <form:form id="dicaForm" action="${pageContext.request.contextPath}/dicas/atividade/${atividade.id}/incluir" method="post" enctype="multipart/form-data">
                                        <div id="error-box-dica" style="display: none;" class="alert alert-error centerAligned"> 
                                            <p id="errorMsg">
                                            Corrija os erros a seguir antes de prosseguir
                                            </p>
                                        </div>
                                        <fan:bodyFormDica local="${atividade.local}" nomeLocal="${atividade.local.nome}" 
                                                          mostrarUploadFotos="false" labelSpanClass="span3" fieldSpanClass="span8" backgroundColor="#fff"  />
                                    </form:form>
                                </div>
                            </div>
                            
                        </div>
                        
                    </fieldset>
                    
                    <fieldset style="background-color: #f8f8f8; padding-left: 20px;" class="${empty atividade.dicas ? 'hide' : 'show'}">
          
                        <legend>
                            <h4>Suas dicas sobre este Local</h4>
                        </legend>
                        
                        <div class="row" style="margin-left: 0;">
                            <div id="painel-dicas">
                                
                                <c:forEach items="${atividade.dicas}" var="dica">
                                    <fan:dica dica="${dica}" userSize="2"></fan:dica>
                                </c:forEach>
                                
                            </div>
                        </div>
                    
                    </fieldset>
                    
                  </div>
                </c:if>
            </div>
            
        </fieldset>
        
        <script>
        
            // Necessario para funcionar o reload
            var editor = CKEDITOR.instances['text-editor'];
            if (editor) {
            	editor.destroy(true);
            }
            $('.text-editor').ckeditor({
                toolbar : 'Basic',
                uiColor : '#DDDDDD'
            });
            
            function aposUploadFotos() {
            	$('#painel-fotos').load('<c:url value="/planoViagem/diario/atividade/${atividade.id}/carrosselFotos?exibirOpcoes=true"/>');
            }
            
            function aposExclucaoFotos() {
            	aposUploadFotos();
            }

            function aposExclusaoAvaliacao() {
                $('#div-form-avaliacao').fadeIn('1000');
                $('#tituloAvaliacao').focus();
            }
            
            $('.btn-adicionar-dica').on('click', function(e) {
                e.preventDefault();
                
                var fieldsetId = $(this).data('fieldset-id');
                
                if ($(this).attr('class').indexOf('active') === -1) {
                    $('.btn-selecionar-form').removeClass('active')
                    $('#' + fieldsetId).show();
                    $('#dica-textarea').focus();
                } else {
                    $('#' + fieldsetId).hide();
                }
            });
            
            $('#btn-excluir-relato').on('click', function(e) {
            	e.preventDefault();
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/planoViagem//diario/atividade/relato/excluir"/>',
                    data: {
                        atividade : ${atividade.id},
                    },
                    success: function(data, status, xhr) {
                        if (data.success) {
                        	$('#text-editor').val('');
                        	$('#btn-excluir-relato').hide();
                        }
                    }
                });
            });
            
            function salvarAtividade() {
                var idLocal = $('input[name="local"]').val();
                var horaInicio = $('input[name="horaInicio"]').val();
                var titulo = $('#titulo').val();
                
                var $btnSalvarAtividade = $('#btn-salvar-atividade');
                $btnSalvarAtividade.button('loading');
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/planoViagem/diario/${diario.id}/alterarAtividade/${atividade.id}"/>',
                    data: {
                        titulo : titulo,
                        horaInicio : horaInicio,
                        local : idLocal
                    },
                    success: function(data, status, xhr) {
                        if (data.success) {
                            $btnSalvarAtividade.button('reset');
                        } else {
                            $btnSalvarAtividade.button('reset');
                        }
                    }
                });
            }
        
            function submeterForm($form, $botao, $painel, url, append) {
                $form.unbind('submit');
                var formData = $form.serialize();
                if ($form.valid()) {
                    $.ajax({
                        type: 'POST',
                        url: $form.attr('action'),
                        data: formData,
                        success: function(data) {
                            if (data.error != null) {
                                //$("#errorMsg").empty().append(data.error);
                                //$("#errors").show();
                            } else {
                                var params = '';
                                if (data.params != null) {
                                    params = data.params;
                                }
                                if (data.success) {
                                	if (append) {
                                	    $painel.append($('<div>').load(url + params.id));
                                	    $painel.parents('fieldset').show();
                                        $botao.button('reset');
                                        $form.find('textarea[name="descricao"]').val('');
                                        $form.find('textarea[name="descricao"]').focus();
                                	} else {
                                	    $painel.load(url + params.id);
                                	}
                                }
                            }
                        }, failure : function() {
                        	$botao.button('reset');
                        }
                    });
                } else {
                	$botao.button('reset');
                }
            }
        
            $(document).ready(function() {
            	
            	$('#horaInicio').timepicker({
                    hourGrid: 4,
                    minuteGrid: 10,
                    stepMinute: 5,
                    addSliderAccess: true,
                    sliderAccessArgs: { touchonly: false },
                });
            	
            	var $form = $('#relato-form');
                $form.unbind('submit');
                
                $form.submit(function(event) {
                	event.preventDefault();
                    $('#btn-salvar-relato').button('loading');
                    var formData = $form.serialize();
                    
                    for (var i in CKEDITOR.instances) {
                        CKEDITOR.instances[i].updateElement();
                        <%-- força a exibição do textarea para funcionar a validação --%>
                    	$('textarea[name="relato.texto"]').css('height', '0px').show();
                    }
                    
                    if ($form.valid()) {
                        $.ajax({
                            type: 'POST',
                            url: $form.attr('action'),
                            data: formData,
                            success: function(data) {
                                if (data.error != null) {
                                } else {
                                    if (data.success) {
                                        $('#btn-salvar-relato').button('reset');
                                        $('#btn-excluir-relato').show();
                                    }
                                }
                            }, failure : function() {
                                $('#btn-salvar-relato').button('reset');
                            }
                        });
                    } else {
                    	$('#btn-salvar-relato').button('reset');
                    }
                });
                
                $('#relato-form').validate({ 
                    rules: {
                        'relato.texto' : { 
                            required: true, 
                            minlength : 50
                        }
                    }, 
                    messages: { 
                    	'relato.texto' : {
                            required : 'Escreva algo sobre o local que você visitou',
                            minlength: 'Seu relato deve conter no mínimo 50 caracteres'
                        }
                    },
                    errorPlacement: function(error, element) {
                        error.appendTo($('#relato-error'));
                    },
                    errorContainer: '#error-box-relato',
                    highlight: function(element, errorClass, validClass) {
                    	$('#cke_text-editor').find('.cke_wrapper').css('border', '1px solid #EE5F5B');
                        $(element).parent().addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $('#cke_text-editor').find('.cke_wrapper').css('border', '0px');
                        $(element).parent().addClass(validClass).removeClass(errorClass);
                    }
                });

                $('#dicaForm').validate({
                    rules: {
                        descricao: {
                            required: true,
                            minlength: 100,
                            maxlength: 255
                        }
                    },
                    messages: { 
                        descricao: {
                            required: '<s:message code="validation.dica.descricao.required" />',
                            minlength: '<s:message code="validation.dica.descricao.minlength" />',
                            minlength: '<s:message code="validation.dica.descricao.maxlength" />'
                        }
                    },
                    errorPlacement: function(error, element) {
                    	var parent = element.parent()[0];
                        error.appendTo(parent);
                    },
                    errorContainer: '#error-box-dica'
                });
                
                $('#avaliacaoForm').validate({
                    rules: {
                        titulo: 'required',
                        mesVisita: 'required',
                        anoVisita: 'required',
                        descricao: {
                            required: true,
                            minlength: 100
                        }
                    },
                    messages: { 
                        titulo: '<s:message code="validation.avaliacao.titulo.required" />',
                        mesVisita: '<s:message code="validation.avaliacao.mesVisita.required" />',
                        anoVisita: '<s:message code="validation.avaliacao.anoVisita.required" />',
                        descricao: {
                            required: '<s:message code="validation.avaliacao.descricao.required" />',
                            minlength: '<s:message code="validation.avaliacao.descricao.minlength" />'
                        }
                    },
                    errorPlacement: function(error, element) {
                        var parent = element.parent()[0];
                        error.appendTo(parent);
                    },
                    errorContainer: '#error-box-avaliacao'
                });
                
                $('#btn-salvar-atividade').on('click', function(event) {
                    event.preventDefault();
                    salvarAtividade();
                });

                //botao-salvar-avaliacao
                $('#botao-salvar-avaliacao').bind('click', function(event) {
                	event.preventDefault();
                    $(this).button('loading');
                    submeterForm($('#avaliacaoForm'), $(this), $('#painel-avaliacao'), '<c:url value="/avaliacao/exibirCard/"/>', false);
                    $(this).button('reset');
                });

                $('#botao-salvar-dica').bind('click', function(event) {
                    event.preventDefault();
                    $(this).button('loading');
                    submeterForm($('#dicaForm'), $(this), $('#painel-dicas'), '<c:url value="/dicas/exibirCard/"/>', true);
                    $(this).button('reset');
                    $('#form-dicas').hide();
                    $('#btn-add-dica').removeClass('active');
                });
                
            });
        </script>

    </div>