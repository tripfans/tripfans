<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>

.box-foto-div {
    position: relative;
}

.box-foto-item div.opcoes {
    display: none;
    position: absolute;
    left: 10px;
    top: 10px;
    overflow: hidden;
}

.box-foto-item div.opcoes:hover {
    display: block;
}

.box-foto-item:hover div.opcoes {
    display: block;
}
</style>

<c:if test="${param.exibirOpcoes}">
  <%@include file="../../fotos/acoesSobreFoto.jsp" %>
</c:if>

<c:set var="width" value="${width != null ? width : 770}"/>
<c:set var="slideAmount" value="${slideAmount != null ? slideAmount : 4}"/>

<div>
    <c:if test="${atividade.possuiFotos}">
      <div class="borda-arredondada" style="padding: 15px; display: ''; margin-left: 0px; position: relative; width: ${width}px; height: 175px;">
        <div id="fotos-atividade-${atividade.id}" class="theme1">
                        
            <ul style="list-style: none;">
                <c:forEach items="${atividade.fotos}" var="foto" varStatus="status">
                    <li id="form-edit-foto-${foto.id}" class="box-foto-item">
                        <a id="box-foto-link-${foto.id}" href="<c:url value="/fotos/exibir/${foto.id}?i=${status.index}&u=${status.last}"/>" 
                           class="fancybox box-foto" title="" data-id-foto="${foto.id}" data-index-foto="${status.index}" 
                           data-prev-index="${status.index - 1}" data-next-index="${status.index + 1}">
                            
                            <i class="carrossel-thumb borda-arredondada" data-foto-id="${foto.id}" 
                               style="background-image: url('<c:url value="${foto.urlAlbum}"/>'); height: 130px; width: 170px;"></i>
                               
                            <p style="min-height: 50px;">
                                <br/>
                                <small>
                                    ${foto.descricao}
                                </small>
                            </p>
                        </a>
                        <c:if test="${param.exibirOpcoes}">
                          <div class="btn-group opcoes" style="position: absolute; left: 5px; top: 5px;">
                            <a href="#" class="btn btn-remover-foto" data-atividade-id="${atividade.id}" data-foto-id="${foto.id}" title="Remover esta foto">
                                <i class="icon-trash"></i>
                            </a>
                          </div>
                        </c:if>                    
                    </li>
                </c:forEach>
            </ul>
            <div class="toolbar">
                <div class="left"></div><div class="right"></div>
            </div>
        </div>
      </div>
      
      <div id="modal-foto" class="modal hide fade" role="dialog" >
        <div class="modal-header" style="border-bottom: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body" style="overflow: hidden; max-height: 800px; min-width: 960px;">
            <div id="modal-foto-body"></div>
        </div>
      </div>
      
    </c:if>
</div>

<script>
	$(document).ready(function () {
        jQuery('#fotos-atividade-${atividade.id}').services({
            width: ${width - 30},
            height: 130,
            slideAmount: ${slideAmount},
            slideSpacing: 10,
            touchenabled: 'on',
            mouseWheel: 'off',
            //transition: 4,
            carousel: 'off',
            hoverAlpha: 'on',
            //slideshow: 5000
        });
        
        $('a.fancybox').click(function(e) {
            e.preventDefault();
            var idFoto = $(this).data('id-foto');
            // pegar a url da foto
            var url = $('#box-foto-link-' + idFoto).attr('href');
            $('#modal-foto-body').load(url, function() {
                $('#modal-foto').modal().css({
                    /*'width': function () { 
                         return ($(document).width() * .9) + 'px';  
                    },
                    'margin-left': function () { 
                        return -($(this).width() / 2); 
                    }*/
                    width: 'auto',
                    'margin-left': function () {
                        return -($(this).width() / 2);
                    }
                });                 
                $('#modal-foto').modal('show');
                
            });
        });        
	});
</script>