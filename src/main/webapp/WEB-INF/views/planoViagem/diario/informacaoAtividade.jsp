<%@page import="br.com.fanaticosporviagens.model.entity.Avaliacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:choose>
  
  <c:when test="${empty atividade}">
  
    <h3>
        <b class="green">
            ${diario.viagem.nome} 
        </b> 
    </h3>
    <br/>
    
    <div>
        <div>
          <h2>
            <c:forEach items="${diario.viagem.cidades}" var="cidade" varStatus="status">
                <a href="#">${cidade.nomeComSiglaEstado}</a>
                ${not status.last ? ' - ' : ''}
            </c:forEach>
            <p>
            <small>${diario.viagem.periodoFormatado}</small>
            </p>
          </h2>
        </div>
    </div>

    <div>
      <p class="texto-blog">
        ${diario.viagem.descricao}
      </p>
    </div>                    
      
  </c:when>
  <c:otherwise>
  
    <h3>
        <b class="green">
            ${diaViagem.numero}º dia - 
        </b> 
        ${diaViagem.diaSemanaLongoDiaMesComMesLongo}
    </h3>
    <br/>
        
    <h4 class="green">
        <c:choose>
          <c:when test="${not empty atividade.titulo}">
            ${atividade.titulo}
          </c:when>
          <c:otherwise>
            <c:choose>
              <c:when test="${atividade.hospedagem}">Hospedagem em</c:when>
              <c:otherwise>Visitando</c:otherwise>
            </c:choose>
            <a href="#" target="_blank">${atividade.local.nome}</a>
          </c:otherwise>
        </c:choose>
    </h4>
      
    <c:if test="${atividade.possuiRelato}">
        <span class="texto-blog">
            ${atividade.relato.texto}
        </span>
    </c:if>

  </c:otherwise>

</c:choose>