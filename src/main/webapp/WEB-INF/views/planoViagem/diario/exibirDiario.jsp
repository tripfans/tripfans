<%@page import="br.com.fanaticosporviagens.model.entity.Avaliacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <style>
            .texto-blog {
                /*font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                line-height: 17px;
                font-weight: normal;*/
                text-align: justify;
            }
            
            .bs-docs-sidenav {
              padding: 0;
              background-color: #fff;
              -webkit-border-radius: 6px;
                 -moz-border-radius: 6px;
                      border-radius: 6px;
              -webkit-box-shadow: 0 1px 4px rgba(0,0,0,.065);
                 -moz-box-shadow: 0 1px 4px rgba(0,0,0,.065);
                      box-shadow: 0 1px 4px rgba(0,0,0,.065);
            }
            .bs-docs-sidenav > li > a {
              display: block;
              width: 190px \9;
              margin: 0 0 -1px;
              padding: 8px 14px;
              border: 1px solid #e5e5e5;
            }
            .bs-docs-sidenav > li:first-child > a {
              -webkit-border-radius: 6px 6px 0 0;
                 -moz-border-radius: 6px 6px 0 0;
                      border-radius: 6px 6px 0 0;
            }
            .bs-docs-sidenav > li:last-child > a {
              -webkit-border-radius: 0 0 6px 6px;
                 -moz-border-radius: 0 0 6px 6px;
                      border-radius: 0 0 6px 6px;
            }
            .bs-docs-sidenav > .active > a {
              position: relative;
              z-index: 2;
              padding: 9px 15px;
              border: 0;
              text-shadow: 0 1px 0 rgba(0,0,0,.15);
              -webkit-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
                 -moz-box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
                      box-shadow: inset 1px 0 0 rgba(0,0,0,.1), inset -1px 0 0 rgba(0,0,0,.1);
            }
            
            /* Chevrons */
            .bs-docs-sidenav .icon-chevron-right {
              float: right;
              margin-top: 2px;
              margin-right: -6px;
              opacity: .25;
            }
            .bs-docs-sidenav > li > a:hover {
              background-color: #f5f5f5;
            }
            .bs-docs-sidenav a:hover .icon-chevron-right {
              opacity: .5;
            }
            .bs-docs-sidenav .active .icon-chevron-right,
            .bs-docs-sidenav .active a:hover .icon-chevron-right {
              background-image: url(../../resources/images/glyphicons-halflings-white.png);
              opacity: 1;
            }
            .bs-docs-sidenav.affix {
              top: 100px;
            }
            .bs-docs-sidenav.affix-bottom {
              position: absolute;
              top: auto;
              bottom: 270px;
            }
            
            .jcarousel-skin-tango .jcarousel-container-horizontal {
                padding: 2px 75px 5px 60px;
            }
            
            .jcarousel-skin-tango .jcarousel-clip-horizontal {
                width: 100%;
                height: 32px;
            }
            
            .jcarousel-skin-tango .jcarousel-item {
                width: 75px;
                height: 35px;
            }
            
            .jcarousel-skin-tango .jcarousel-prev-horizontal {
                top: 5px;
            }
            
            .jcarousel-skin-tango .jcarousel-next-horizontal {
                top: 5px;
            }
            
        </style>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
      <meta property="og:title" content="${diario.viagem.nome}"/>
      <meta property="og:url" content="${tripFansEnviroment.serverUrl}/<c:url value="/planoViagem/diario/${diario.id}"/>"/>
      <meta property="og:image" content="${tripFansEnviroment.serverUrl}/<c:url value="${diario.viagem.fotoIlustrativa.urlAlbum}"/>"/>
      <meta property="og:site_name" content="TripFans"/>
      <meta property="og:description" content="${diario.viagem.descricao}"/>    
    
      <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
    
        <br/>
        
        <div class="row">
        
            <div class="span14">
                <h1 class="green">
                    ${diario.viagem.nome}
                    <c:if test="${usuario.id == diario.viagem.criador.id}">
                      <a href="<c:url value="/planoViagem/editarDiario/${diario.id}"/>" class="btn btn-primary">
                        <i class="icon-pencil icon-white"></i>
                        Editar Diário
                      </a>
                    </c:if>
                </h1>
            </div>
        
            <div class="span4 pull-right">
                <fan:userAvatar user="${diario.viagem.criador}" displayName="false" width="30" height="30" showFrame="false" />
                <h5>
                    Por <strong><a href="<c:url value="/perfil/${diario.viagem.criador.urlPath}" />">${diario.viagem.criador.displayName}</a></strong>
                    <p>
                      <c:if test="${diario.publicado}">
                        <small>Em <joda:format value="${diario.dataPublicacao}" pattern="dd 'de' MMMM 'de' yyyy" /></small>
                      </c:if>
                    </p>
                </h5>
            </div>
        </div>
        
        <div id="div-foto-capa" style="display: ${diario.viagem.fotoIlustrativa != null ? 'block' : 'none'}; position: relative;">
                
            <div style="display: block; position: relative; height: 200px; overflow: hidden; text-decoration: none; cursor: hand;">
                <div id="img-foto-capa">
                    <img src="${diario.viagem.fotoIlustrativa.urlBig}" style="min-height: 100%; min-width: 100%; position: absolute; left: 0; top: -164px;" />
                </div>
            </div>
        
        </div>
        
        <fieldset style="background-color: #F8F8F8; margin: 8px 0 6px 0;">
          <legend>
            
          </legend> 
        
          <div>
            <div>
              <h2>
                <c:forEach items="${diario.viagem.cidades}" var="cidade" varStatus="status">
                    <a href="#">${cidade.nomeComSiglaEstado}</a>
                    ${not status.last ? ' - ' : ''}
                </c:forEach>
                <p>
                <small>${diario.viagem.periodoFormatado}</small>
                </p>
              </h2>
            </div>
          </div>

          <div>
            <p>
                ${diario.viagem.descricao}
            </p>
          </div>
          
          <div align="center">
            <a href="<c:url value="/planoViagem/diario/${diario.id}/slideshow" />" class="btn btn-large btn-block btn-primary" type="button">
                <i class="icon-white icon-play"></i> Iniciar Slide Show
            </a>
          </div>
          
        </fieldset>
        
        <c:if test="${diario.publicado}">
          <div>
            <fan:share includeFacebookApi="false" showComments="true" commentsAnchor="painel-comentarios" />
          </div>
        </c:if>
        
        <c:if test="${not diario.viagem.periodoNaoDefinido}">
        
          <div class="horizontalBar" style="margin: 0px 0px 4px 0px;"></div>
        
          <ul class="nav nav-pills jcarousel-skin-tango" style="margin: 0 0 0px 5px;">
            <%-- <li>
                <a href="#tab-capa" data-toggle="tab">Capa</a>
            </li>--%>
            <c:forEach items="${diario.capitulos}" var="capitulo" varStatus="status">
                <li class="${status.index == 0 ? 'active' : ''}" style="text-align: center;" data-dia="${capitulo.numero}">
                    <a href="#" data-toggle="pill" data-dia="${capitulo.numero}">${capitulo.numero}º dia</a>
                </li>
            </c:forEach>
          </ul>

          <div class="horizontalBar"></div>
          
        </c:if>
        
        <div id="page-content">
        </div>
        
        <c:if test="${not diario.viagem.periodoNaoDefinido}">
          
          <div class="horizontalBar" style="margin: 0px 0px 4px 0px;"></div>
        
          <ul class="nav nav-pills jcarousel-skin-tango" style="margin: 0 0 0px 5px;">
            <c:forEach items="${diario.capitulos}" var="capitulo" varStatus="status">
                <li class="${status.index == 0 ? 'active' : ''}" style="text-align: center;" data-dia="${capitulo.numero}">
                    <a href="#" data-toggle="pill" data-dia="${capitulo.numero}">${capitulo.numero}º dia</a>
                </li>
            </c:forEach>
          </ul>
        
          <div class="horizontalBar"></div>
          
        </c:if>
        
        <div>
        
            <h3>
                <a name="painel-comentarios">Comentários deste Diário</a>
            </h3>
            
            <div class="horizontalBar"></div>
            
            <fan:comments tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.DIARIO.name()%>" 
                          id="diario-comments" idAlvo="${diario.id}" inputType="text" inputClass="span10" buttonOnSameLine="true" />
        
        </div>
        
      </div>
        
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    
        <!-- TOUCH AND MOUSE WHEEL SETTINGS -->
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.touchwipe.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.mousewheel.min.js" />"></script> 
        
        <!-- jQuery SERVICES Slider -->
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.themepunch.services.js" />"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/services-plugin/css/settings.css" />" media="screen" />
    
        <script>

            jQuery(function() {
            	
                // carreando dados do primeiro dia
                $('#page-content').load('<c:url value="/planoViagem/diario/${diario.id}/dia"/>/1', {}, function(response) {
                    $('#page-content').initToolTips();
                });
                
                $('.jcarousel-skin-tango').jcarousel({
                    visibile: 10,
                    //wrap: 'last',
                    scroll: 10
                });
            	
            	$('.jcarousel-skin-tango a').click(function (e) {
                    e.preventDefault();
                    var dia = $(this).data('dia');
                    $('.jcarousel-skin-tango li').removeClass('active');
                    $('.jcarousel-skin-tango li[data-dia=' + dia + ']').addClass('active');
                    $('#page-content').load('<c:url value="/planoViagem/diario/${diario.id}/dia"/>/' + dia, {}, function(response) {
                    	
                        $('body').scrollspy('refresh');
                        
                        $('#page-content').initToolTips();
                    });
            	});
            	
            });
            
            (function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        
    </tiles:putAttribute>

</tiles:insertDefinition>