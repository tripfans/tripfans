<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%--             <div class="tab-pane active" id="tab-capa">
        
                <div>
                
                    <h3 class="green">Início</h3>
                
                    <span class="texto-blog">
                        ${diario.textoIntroducao}
                    </span>
                
                </div>
                
                <div class="horizontalBar"></div>
                
            </div> --%>
            

<div class="row" style="margin-left: 0px;">

  <fieldset style="background-color: #F8F8F8;">

    <legend>
      <h3>${diaViagem.diaSemanaLongoDiaMesComMesLongo}</h3>
    </legend>                

    <div class="span3" style="margin-left: 0px;">
  
      <div style="max-height: 1px;">
      &nbsp;
      </div>
  
      <div class="bs-docs-sidebar">
        <ul class="nav nav-list bs-docs-sidenav span3" style="margin-left: 0px; z-index: 99;">
          <c:if test="${diario.viagem.quantidadeDias > 1}">
            <li class="borda-arredondada-cima" style="background-color: #6FC46F; min-height: 22px; text-align: center; padding-top: 4px;">
                <c:if test="${diaViagem.numero > 1}">
                    <i class="icon-arrow-left icon-white left mudar-dia" style="margin-left: 5px; cursor: pointer;" title="Dia anterior" data-proximo-dia="${diaViagem.numero - 1}"></i>
                </c:if>
                <strong class="white">${diaViagem.numero}º dia</strong>
                <c:if test="${diaViagem.numero < diario.viagem.quantidadeDias}">
                    <i class="icon-arrow-right icon-white right mudar-dia" style="margin-right: 5px; cursor: pointer;" title="Próximo dia" data-proximo-dia="${diaViagem.numero + 1}"></i>
                </c:if>
            </li>
          </c:if>
          
          <c:if test="${diaViagem.possuiAtividadesAExibir}">
            <c:forEach items="${diaViagem.atividades}" var="atividade" varStatus="status">
              <c:if test="${not atividade.oculta}">
                <li class="${status.index == 0 ? 'active' : ''}">
                  <a href="#atividade-${atividade.id}">
                    <c:if test="${atividade.possuiTitulo}">
                        ${atividade.titulo} - 
                    </c:if> 
                    <i class="icon-chevron-right"></i> ${atividade.local.nome}
                  </a>
                </li>
              </c:if>
            </c:forEach>
          </c:if>
          <c:if test="${not diaViagem.possuiAtividadesAExibir}">
            <li style="min-height: 25px; text-align: center; margin-top: 5px;">
                Nenhuma atividade neste dia
            </li>
          </c:if>
        </ul>
      </div>
    </div>        

    <div id="tab-content" class="span12">
      <div id="tab-dia-${diaViagem.numero}">
      
        <c:if test="${not diaViagem.possuiAtividadesAExibir}">
            <fieldset style="background-color: #FFF;">
                <div class="blank-state">
                    <div class="row" style="text-align: center;">
                        <div>
                            <img src="<c:url value="/resources/images/icons/mini/128/Time-3.png" />" width="90" height="90" />
                            <h3>Nenhuma atividade neste dia</h3>
                            <p>
                                <c:choose>
                                  <c:when test="${usuario.id == diario.viagem.criador.id}">
                                      Você
                                  </c:when>
                                  <c:otherwise>
                                      Este usuário
                                  </c:otherwise>
                                </c:choose>
                                não registrou nenhuma atividade neste dia.
                            </p>
                        </div>
                    </div>
                </div>            
            </fieldset>
        </c:if>
        
        <c:if test="${diaViagem.possuiAtividadesAExibir}">
      
          <c:forEach items="${diaViagem.atividades}" var="atividade" varStatus="status">
        
            <c:if test="${not atividade.oculta}">
        
              <section id="atividade-${atividade.id}">
              
                <fieldset style="background-color: #FFF;">
                
                    <legend>
                      <h3 class="green">
                        <c:choose>
                          <c:when test="${not empty atividade.titulo}">
                            ${atividade.titulo}
                          </c:when>
                          <c:otherwise>
                            <c:choose>
                              <c:when test="${atividade.hospedagem}">Hospedagem em</c:when>
                              <c:otherwise>Visita a(o)</c:otherwise>
                            </c:choose>
                            <a href="#" target="_blank">${atividade.local.nome}</a>
                          </c:otherwise>
                        </c:choose>
                      </h3>
                    </legend>           
                
                    <h3 style="text-shadow: none;">
                        <p>
                            <c:if test="${atividade.dataInicio != null}">
                              <small>
                                ${diaViagem.diaSemanaLongoDiaMesComMesLongo}
                                -
                              </small>
                            </c:if>
                            <small class="blue">${atividade.local.nome}</small>
                            <%-- <small>${atividade.local.nomeCidadeComSiglaEstado}</small> --%>
                        </p>
                    </h3>
                    
                    <c:if test="${not atividade.preencheuDadosDiario}">
                        <div class="blank-state">
                            <div class="row" style="text-align: center;">
                                <div>
                                    <%-- <img src="<c:url value="/resources/images/icons/mini/128/Time-3.png" />" width="90" height="90" /> --%>
                                    <h3>Sem informações para exibir</h3>
                                    <p>
                                        <c:choose>
                                          <c:when test="${usuario.id == diario.viagem.criador.id}">
                                              Você
                                          </c:when>
                                          <c:otherwise>
                                              Este usuário
                                          </c:otherwise>
                                        </c:choose>
                                        ainda não adicionou informações para esta atividade.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    
                    <c:if test="${atividade.possuiRelato}">
                      <fieldset>
                        <span class="texto-blog">
                            ${atividade.relato.texto}
                        </span>
                      </fieldset>
                    </c:if>
                    
                    <c:if test="${atividade.possuiFotos}">
                    
                        <fieldset style="background-color: #F8F8F8;">
                        
                            <legend>
                                <h4 class="green">Fotos</h4>
                            </legend>
                            
                            <div id="painel-fotos">
                                <c:set var="width" value="630"></c:set>
                                <c:set var="slideAmount" value="3"></c:set>
                                <%@include file="carrosselFotos.jsp" %>
                            </div>
                            
                        </fieldset>
                    
                    </c:if>
            
                    <c:if test="${atividade.possuiAvaliacao}">
                        <fieldset id="fieldset-avaliacao" style="background-color: #F8F8F8;">
                        
                            <legend>
                                
                                <div class="btn-group" data-toggle="buttons-checkbox" style="margin-bottom: 5px">
                                  <button id="btn-ver-avaliacao" type="button" class="btn btn-mini btn-success btn-exibir-painel" data-painel-id="painel-avaliacao-${atividade.id}">
                                    <i class="icon-eye-open icon-white"></i>
                                    Ver avaliação sobre este local
                                  </button>
                                </div>
                                
                            </legend>
                            
                            <div id="painel-avaliacao-${atividade.id}" style="display: none;">
                                <fan:avaliacao avaliacao="${atividade.avaliacao}"></fan:avaliacao>
                            </div>
                        
                        </fieldset>
                    </c:if>
                        
                    <c:if test="${atividade.possuiDicas}">
                        <fieldset id="fieldset-dicas" style="background-color: #F8F8F8;">
                            <legend>
                                <h4 class="green"></h4>
                                
                                <div class="btn-group" data-toggle="buttons-checkbox" style="margin-bottom: 5px">
                                  <button id="btn-ver-dicas" type="button" class="btn btn-mini btn-success btn-exibir-painel" data-painel-id="painel-dicas-${atividade.id}">
                                    <i class="icon-eye-open icon-white"></i>
                                    Ver dicas sobre este local
                                  </button>
                                </div>
                                
                            </legend>
                            
                            <div id="painel-dicas-${atividade.id}" style="display: none;">
                                <c:forEach items="${atividade.dicas}" var="dica">
                                    <fan:dica dica="${dica}"></fan:dica>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </c:if>
                    
                    <%-- <div id="painel-comentarios-${atividade.id}">
                        <fan:comments tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.ITEM_DIARIO.name()%>" 
                                      id="comentario-${atividade.id}" idAlvo="${atividade.relato.id}" inputClass="span8"
                                      imgSize="mini-avatar" inputType="text" buttonOnSameLine="true" />
                    </div> --%>
                    
                </fieldset>
                
              </section>
          
            </c:if>
            
          </c:forEach>
          
        </c:if>
        
      </div>
      
      <div id="fb-root"></div>
  
    </div>
    
    <div class="span4">
        
    </div>
    
  </fieldset>
  
</div>

<script>

    jQuery(function() {
    	
        $('body').scrollspy({
        	offset : -400
        });
        
        $('.bs-docs-sidenav').affix({
          offset: {
            top: 460,
            bottom: 570
          }
        });
        
        $('.btn-exibir-painel').on('click', function(e) {
            e.preventDefault();
            
            var painelId = $(this).attr('data-painel-id');
            
            if ($(this).attr('class').indexOf('active') === -1) {
                $('#' + painelId).css('display', '');
            } else {
                $('#' + painelId).css('display', 'none');
            }
        });
        
        $('.bs-docs-sidenav i.mudar-dia').on('click', function(e) {
        	var dia = $(this).data('proximo-dia');
        	$('.jcarousel-skin-tango a[data-dia=' + dia + ']').click();
        })

        $('a[rel="slideShow"]').fancybox({
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type              : 'image',
            autoScale : false,
            autoDimensions : false,
            width : 560,
            height : 340,
            speedIn: 400, 
            speedOut: 200, 
            overlayShow: true,
            overlayOpacity: 0.1,
            hideOnContentClick: true,
            'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
            },
            titlePosition: 'inside'
        });        
    });
    
</script>