<%@page import="br.com.fanaticosporviagens.model.entity.Avaliacao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <style>
            #galleria { 
                width: 60%; 
                height: 460px; 
                background: #000 
            }
            
            .texto-blog {
                text-align: justify;
            }
        </style>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <style>
            
        </style>
    
        <%-- <div class="ppy" id="ppy-tf">
            <ul class="ppy-imglist">
              <c:forEach items="${diario.viagem.diasViagem}" var="dia">
                <c:forEach items="${dia.atividades}" var="atividade">
                    <c:forEach items="${atividade.fotos}" var="foto">

                      <li>
                        <a href="${foto.urlBig}">
                            <img src="${foto.urlAlbum}" alt="" />
                        </a>
                        
                        <span class="ppy-extcaption">
                            <h4>
                                ${dia.numero}º dia - ${dia.diaSemanaCurtoDiaMesComMesLongo}
                            </h4>
                            <strong>
                                <c:choose>
                                  <c:when test="${not empty atividade.titulo}">
                                    ${atividade.titulo}
                                  </c:when>
                                  <c:otherwise>
                                    <c:choose>
                                      <c:when test="${atividade.hospedagem}">Hospedagem em</c:when>
                                      <c:otherwise>Visita a(o)</c:otherwise>
                                    </c:choose>
                                    <a href="#" target="_blank">${atividade.local.nome}</a>
                                  </c:otherwise>
                                </c:choose>                                
                            </strong>
                            <br>
                                ${atividade.relato.texto}
                            <br>
                            <!-- <a href="http://www.flickr.com/photos/opoterser/3760102198/">View on flickr.com</a> -->
                        </span>
                      </li>
                      
                    </c:forEach>
                  </c:forEach>
              </c:forEach>
            </ul>
            <div class="ppy-outer">
                <div class="ppy-stage">
                    <div class="ppy-counter">
                        <strong class="ppy-current"></strong> / <strong class="ppy-total"></strong> 
                     </div>
                </div>
                <div class="ppy-nav">
                    <a class="ppy-prev" title="Foto anterior">Foto anterior</a>
                    <a class="ppy-switch-enlarge" title="Aumentar">Aumentar</a>
                    <a class="ppy-next" title="Próxima foto">Próxima foto</a>
                    <a class="ppy-switch-compact" title="Fechar">Fechar</a>
                </div>
            </div>
            <div class="ppy-caption">
                <div class="ppy-counter">
                    Foto <strong class="ppy-current"></strong> de <strong class="ppy-total"></strong> 
                </div>
                <span class="ppy-text"></span>
            </div>
        </div> --%>
        
        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
          <br/>
        
          <div class="row">
        
            <div class="span14">
                <div class="span7" style="margin-left: 0;">
                    <b class="green" style="font-size: 30px; line-height: 36px;">
                      ${diario.viagem.nome}
                        <a href="<c:url value="/planoViagem/diario/${diario.id}"/>" class="btn btn-primary">
                          <i class="icon-book icon-white"></i>
                          Voltar para o Diário
                        </a>
                    </b>
                </div>
                <div class="span6" style="margin-top: 0px;">
                    <div>
                      Tempo de transição da foto: 
                      <span id="tempo" class="badge badge-important">5</span> segundos
                    </div>
                
                    <div id="slider" style="max-width: 200px;"></div>
                </div>
            </div>
            
            <div class="span4 pull-right">
                <fan:userAvatar user="${diario.viagem.criador}" displayName="false" width="30" height="30" showFrame="false" />
                <h5>
                    Por <strong><a href="<c:url value="/perfil/${diario.viagem.criador.urlPath}" />">${diario.viagem.criador.displayName}</a></strong>
                    <p>
                      <c:if test="${diario.publicado}">
                        <small>Em <joda:format value="${diario.dataPublicacao}" pattern="dd 'de' MMMM 'de' yyyy" /></small>
                      </c:if>
                    </p>
                </h5>
            </div>
          </div>
          
          <hr style="margin: 0px 0px 10px 0;" />
          
          <div class="row">
          
              <div id="galleria" class="span10">
              
                <li data-dia="0" data-atividade="0" class="foto-slide" data-foto-src="${diario.viagem.fotoIlustrativa.urlBig}">
                    <a href="${diario.viagem.fotoIlustrativa.urlBig}">
                        <img data-big="big.jpg" src="${diario.viagem.fotoIlustrativa.urlAlbum}" />
                    </a>
                </li>
            
                <c:forEach items="${diario.viagem.diasViagem}" var="dia">
                    <c:forEach items="${dia.atividades}" var="atividade">
                        <c:forEach items="${atividade.fotos}" var="foto">
                            <%-- <img src="${foto.urlBig}" /> --%>
                            
                            <li data-dia="${dia.numero}" data-atividade="${atividade.id}" data-foto-src="${foto.urlBig}" class="foto-slide">
                                <a href="${foto.urlBig}">
                                    <img data-big="big.jpg" src="${foto.urlAlbum}" />
                                    <c:if test="${not empty foto.descricao}">
                                      <p>
                                        ${foto.descricao}
                                      </p>
                                    </c:if>
                                </a>
                            </li>
                            
                        </c:forEach>
                    </c:forEach>
                </c:forEach>        
              </div>
          
              <div id="painel-detalhe-atividade" class="span7" data-dia="" data-atividade="">
                <c:import url="informacaoAtividade.jsp"></c:import>
              </div>
          
        </div>
    
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    
        <!-- jQuery SERVICES Slider -->
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/jquery.popeye/css/popeye/jquery.popeye.css" />" media="screen" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/jquery.popeye/css/popeye/jquery.popeye.tripfans.css" />" media="screen" />
        
        <!-- jQuery.popeye scripts -->
        <script type="text/javascript" src="<c:url value="/resources/components/jquery.popeye/lib/popeye/jquery.popeye-2.1.js" />"></script> 
        
        <!-- Galleria -->
        <script type="text/javascript" src="<c:url value="/resources/components/galleria/galleria-1.2.9.min.js" />"></script> 
    
        <script>
        
            var mapaImagens = {};
        
            function montarMapaImagens() {
            	$('li.foto-slide').each(function(index) {
                    mapaImagens[$(this).data('foto-src')] = {
            			'src' : $(this).data('foto-src'),
            			'dia' : $(this).data('dia'),
            			'atividade' : $(this).data('atividade'),
            		};
                });
            } 

            jQuery(function() {
            	
        	    /*$('#ppy-tf').popeye({
        	    	slidespeed : 2000,
        	    	autoslide : true
                });
        	    
        	    $('.ppy-stage').css('max-width', '1024px');
        	    $('.ppy-stage').css('max-height', '640px');*/
        	    
        	    montarMapaImagens();
        	    
        	    Galleria.loadTheme('<c:url value="/resources/components/galleria/themes/classic/galleria.classic.min.js" />');
                Galleria.run('#galleria', {
                	dataConfig: function(img) {
                        return {
                            description: $(img).next('p').html()
                        }
                    },
                    preload: 3,
                    responsive: true,
                    //imageCrop: true,
                    transition: 'fade',
                    autoplay: 5000
                });

                Galleria.on('image', function(e) {
                    var dia, diaAtual, atividade, atividadeAtual;
                    
                    //var diaAtual = $('#painel-detalhe-atividade').data('dia');
                    var atividadeAtual = $('#painel-detalhe-atividade').data('atividade');
                    
                    if (e.imageTarget.src in mapaImagens) {
                    	dia = mapaImagens[e.imageTarget.src].dia
                    	atividade = mapaImagens[e.imageTarget.src].atividade
                    	
                        if (atividade != null && atividade != atividadeAtual) {
                        	$('#painel-detalhe-atividade').data('atividade', atividade)
                        	$('#painel-detalhe-atividade').load('<c:url value="/planoViagem/diario/${diario.id}/slideshow/informacaoAtividade/"/>/' + dia + '/' + atividade, {}, function(response) {
                                
                            }).hide().fadeIn('1000');
                        }
                    }
                    
                });
                
                $('#slider').slider({
                    value: 5,
                    min: 1,
                    max: 10,
                    step: .5,
                    slide: function(event, ui) {
                        var tempo = ui.value;
                        $('#tempo').text(tempo);
                        $('#galleria').data('galleria').setPlaytime(tempo * 1000)
                    }
                  });
                  //$( "#amount" ).val( "$" + $( "#slider" ).slider( "value" ) );
            	
            });
            
        </script>
        
    </tiles:putAttribute>

</tiles:insertDefinition>