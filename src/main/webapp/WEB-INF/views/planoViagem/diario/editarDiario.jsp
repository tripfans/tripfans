<%@page import="br.com.fanaticosporviagens.model.entity.Avaliacao"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    
        <style>
        
        list li:first-child {
           border-top: 1px solid white;
        }
        
        ul.options-list li {
            padding: 15px 0 15px 263px;
            min-height: 34px;
            border-top: 1px solid #E8E8E8;
        }
        
        #img-foto-capa:hover { 
            cursor: n-resize; 
        }
        
        .diario-sidenav {
            margin: 30px 0 0;
            padding: 0;
            /*background-color: white;*/
            -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border: 1px solid #eee;
            border-radius: 6px;
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, .065);
            -moz-box-shadow: 0 1px 4px rgba(0,0,0,.065);
            box-shadow: 0 1px 4px rgba(0, 0, 0, .065);
        }
        
        a.item-relato.active, a.item-relato.active:hover {
            background-color: #EDF7FF;
        }
        
        </style>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">

        <meta property="og:title" content="${diario.viagem.nome}"/>
        <meta property="og:url" content="${tripFansEnviroment.serverUrl}/<c:url value="/planoViagem/diario/${diario.id}"/>"/>
        <meta property="og:image" content="${tripFansEnviroment.serverUrl}/<c:url value="${diario.viagem.fotoIlustrativa.urlAlbum}"/>"/>
        <meta property="og:site_name" content="TripFans"/>
        <meta property="og:description" content="${diario.viagem.descricao}"/>    
    
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/ckeditor/adapters/jquery.js"></script>
        
        <c:url value="/planoViagem/diario/salvarDiario" var="salvar_diario_url" />
        
        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
            <div>
            
                <div class="page-header" style="margin-bottom: 10px;">
                    <div id="div-ajuda-top" class="pull-right hide" style="padding-top: 10px;">
                        <a class="btn btn-info btn-mini btn-mostrar-ajuda" data-toggle="button" href="#">
                            <i class="icon-question-sign icon-white"></i> Exibir Ajuda
                        </a>
                    </div>
                    <h2>Diário de Viagem</h2>
                </div>
                
                <c:if test="${diario.rascunho}">
                  <div class="alert alert-info" style="margin-bottom: 8px;">
                    <button id="btn-fechar-ajuda" type="button" class="close" data-dismiss="alert">×</button>
                    <h3 class="blue">
                        Olá ${diario.viagem.criador.primeiroNome},
                    </h3>
                    <p>
                        Seja bem-vindo a edição do Diário de Viagem!
                        <br/>
                        Se você precisa de ajuda para criar ou editar o seu diário, clique em 
                        <a class="btn btn-info btn-mini btn-mostrar-ajuda" data-toggle="button" href="#">
                            <i class="icon-question-sign icon-white"></i> Exibir Ajuda
                        </a> -
                        Você pode a qualquer momento, exibir/esconder a ajuda, basta clicar novamente no botão. 
                    </p>
                  </div>
                </c:if>
                
                <fieldset style="background-color: #F8F8F8;">

                  <legend>
                      <h3>Informações sobre a sua viagem</h3>
                  </legend>                
                
                  <div class="alert alert-info painel-ajuda" style="margin-bottom: 8px; display: none;">
                    <h4 class="blue">
                        <i class="icon-question-sign"></i> Ajuda
                    </h4>
                    <p>
                        Nesta seção, você vê as informações básicas sobre sua viagem. 
                        <br/>
                        Para alterar estas informações, clique em <i class="icon-pencil"></i>
                        <br/>
                        Se você desejar ver como está ficando o seu diário, clique em <i class="icon-eye-open"></i>
                        <br/>
                        Ao concluir seu diário, clique em <i class="icon-book"></i> para publicá-lo e 
                        compartilhar com seus amigos. 
                        <br/>
                    </p>
                  </div>
                
                  <div>
                  
                    <fieldset style="background-color: #FFF;">
                
                      <ul style="min-height: 34px; margin: 0px; list-style: none;">
                        
                        <li style="min-height: 44px;">
                            <label class="span1" style="margin-top: 10px; margin-right: 30px;">
                                <span>
                                    Título
                                </span>
                            </label>
                            <h2>${diario.viagem.nome}</h2>
                        </li>
                        
                        <li style="min-height: 34px;">
                            <label class="span1" style="margin-right: 30px;">
                                <span>
                                    <c:choose>
                                        <c:when test="${diario.viagem.periodoPorQuantidadeDias}">
                                            Duração
                                        </c:when>
                                        <c:otherwise>
                                            Período
                                        </c:otherwise>
                                    </c:choose>
                                </span>
                            </label>
                            <c:choose>
                                <c:when test="${diario.viagem.periodoPorQuantidadeDias}">
                                    ${diario.viagem.quantidadeDias} dias
                                </c:when>
                                <c:when test="${diario.viagem.periodoPorDatas}">
                                    ${diario.viagem.periodoFormatado} (${diario.viagem.quantidadeDias} dias)
                                </c:when>
                                <c:otherwise>
                                    Não informado
                                </c:otherwise>
                            </c:choose>
                        </li>

                        <li style="min-height: 24px;">
                            <label class="span1" style="margin-right: 30px;">
                                <span>
                                    Destinos
                                </span>
                            </label>
                            <p>
                              <strong>
                                <c:forEach items="${diario.viagem.cidades}" var="cidade" varStatus="status">
                                  <a href="#">${cidade.nomeComSiglaEstado}</a>
                                  ${not status.last ? '-' : ''}
                                </c:forEach>
                              </strong>
                            </p>
                        </li>

                        <li style="min-height: 44px;">
                            <label class="span1" style="margin-right: 30px;">
                                <span>
                                    Descrição
                                </span>
                            </label>
                            <span>
                                ${diario.viagem.descricao}
                            </span>
                        </li>

                        <li style="min-height: 34px;">
                            <label class="span1" style="margin-right: 30px;">
                                <span>
                                    
                                </span>
                            </label>
                            <span>
                                <a href="<c:url value="/planoViagem/editar/${diario.viagem.id}?d=${diario.id}"/>" class="btn">
                                    <i class="icon-pencil"></i> Editar detalhes
                                </a>
                                <a href="<c:url value="/planoViagem/diario/${diario.id}"/>" class="btn">
                                    <i class="icon-eye-open"></i>
                                    Ver como outros veem
                                </a>
                                <c:if test="${diario.rascunho}">
                                  <a id="btn-publicar-diario" href="#" class="btn btn-primary">
                                    <i class="icon-book icon-white"></i> Publicar
                                  </a>
                                </c:if>
                            </span>
                        </li>
                      </ul>
                      
                      <div style="position: relative; height: 200px;" class="hide">
                      
                        <div style="display: ${diario.viagem.fotoIlustrativa != null ? 'block' : 'none'}; position: relative; height: 1140px; ">

                            <div id="div-foto-capa" style="display: block; position: relative; height: 200px; overflow: hidden; text-decoration: none; cursor: hand;">
                                <div id="img-foto-capa">
                                    <img src="${diario.viagem.fotoIlustrativa.urlBig}" style="min-height: 100%; min-width: 100%; position: absolute; left: 0; top: -164px;" />
                                </div>
                            </div>
                        
                            <div class="instructionWrap" style="display: none; line-height: 26px; position: absolute; text-align: center; top: 145px;">
                                <div class="instructions" style="background: #546185 9px 8px no-repeat; background-color: rgba(84, 97, 133, .4); -webkit-border-radius: 2px; -webkit-box-shadow: inset 0 1px 0 rgba(0, 0, 0, .12); color: white; display: inline; font-size: 13px; font-weight: bold; padding: 4px 9px 6px 29px;">
                                    <i class="icon-resize-vertical icon-white"></i> Arraste para reposicionar a capa
                                </div>
                            </div>
                            
                            <div class="coverBorder"></div>
                            
                        </div>
                        
                      <div>
          
                    </fieldset>
                  </div>
                  
                </fieldset>

                <fieldset style="background-color: #F8F8F8;">
                
                  <legend>
                      <h3>Atividades/Histórias da sua viagem</h3>
                  </legend>
                  
                  <div class="alert alert-info painel-ajuda" style="margin-bottom: 8px; display: none;">
                    <h4 class="blue">
                        <i class="icon-question-sign"></i> Ajuda
                    </h4>
                    <p>
                        Nesta seção, você poderá incluir/alterar suas atividades durante sua viagem.
                        <br/>
                        Conte-nos suas experiências em locais que você vistou, adicione fotos, avalie locais e deixe dicas para 
                        amigos e outras pessoas.
                        <br/>
                        Do lado esquerdo, você tem uma lista com os dias de sua viagem. Clique no dia desejado para visualizar/alterar
                        atividades do dia.
                        <br/>
                        Do lado direito, você vê uma lista com as atividades do dia selecionado. 
                        Clique em <i class="icon-plus"></i> para adicionar novas atividades.
                        <br/>
                    </p>
                  </div>
                
                  <div class="row">
                  
                    <div class="span4 menu-lateral" id="menuLateral" style="margin-left: 20px;">
                    
                      <fieldset style="background-color: #FFF; min-height: 200px;">
                      
                        <legend>
                          <h4>
                            <c:choose>
                              <c:when test="${not diario.viagem.periodoNaoDefinido}">
                                Calendário
                              </c:when>
                              <c:otherwise>
                                Atividades
                              </c:otherwise>
                            </c:choose>
                          </h4>
                        </legend>
                        

                        <div class="accordion" id="lista-dias">
                          <c:forEach items="${diario.viagem.diasViagem}" var="diaViagem" varStatus="status">
                            <div class="accordion-group">
                                <div class="accordion-heading" style="padding-top: 6px; padding-bottom: 6px; ${status.index == 0 ? 'background-color: #DCEAF4' : ''}">
                                  <span>
                                    <a href="#item-dia-${diaViagem.numero}" class="accordion-toggle" data-toggle="collapse" data-parent="#lista-dias" data-dia="${diaViagem.numero}" style="display: inline;">
                                        <span style="background-image: url('<c:url value="/resources/images/icons/date.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                        
                                        <c:choose>
                                          <c:when test="${not diario.viagem.periodoNaoDefinido}">
                                            ${diaViagem.numero}º dia
                                            <c:if test="${diario.viagem.periodoPorDatas}">
                                                (${diaViagem.diaSemanaCurto} - ${diaViagem.diaMesComMesCurto})
                                            </c:if>
                                          </c:when>
                                          <c:otherwise>
                                            Todas
                                          </c:otherwise>
                                        </c:choose>
                                                                                                
                                    </a>
                                  </span>
                                  <span style="float: right; margin-right: 10px; padding-top: 3px; position: relative;">
                                      <span class="label label-info label-qtd-itens" title="${diaViagem.quantidadeAtividades} atividade(s) ${not diario.viagem.periodoNaoDefinido ? 'neste dia' : ''}" style="${diaViagem.quantidadeAtividades > 0 ? '' : 'display : none;'}">${diaViagem.quantidadeAtividades}</span>
<!--                                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Adicionar Local / Atividade">
                                          <i class="icon-plus"></i>
                                      </a>
                                      <ul class="dropdown-menu">
                                          <li>
                                            <a tabindex="-1" href="#">
                                                <i class="icon-globe"></i>
                                                Local
                                            </a>
                                          </li>
                                          <li>
                                            <a tabindex="-1" href="#">
                                                <i class="icon-book"></i>
                                                História
                                            </a>
                                          </li>
                                          <li class="divider"></li>
                                          <li>
                                            <a tabindex="-1" href="#">
                                                <i class="icon-question-sign"></i>
                                                Ajuda
                                            </a>
                                          </li>
                                      </ul>                                      
 -->                                  
                                    </span>
                                </div>
                              <div id="item-dia-${diaViagem.numero}" class="accordion-body collapse ${status.index == 0 ? 'in' : ''}">
                                  <div class="accordion-inner" style="padding: 0px;">
                                      <ul id="menu-atividades-dia-${diaViagem.numero}" class="nav nav-list sortable connected-sortable">
                                        <c:forEach items="${diaViagem.atividades}" var="atividade" varStatus="status">
                                          <li id="item-menu-atividade-${atividade.id}">
                                            <a class="item-relato" href="<c:url value="/planoViagem/diario/editarAtividade/${atividade.id}"/>" style="padding: 6px 15px 6px 15px">
                                                <i class="${atividade.tipo.iconClass}" title="${atividade.decricaoTipo}"></i>
                                                <c:if test="${atividade.possuiTitulo}">
                                                    ${atividade.titulo}
                                                    <c:if test="${not empty atividade.nomeLocal}">
                                                        -
                                                    </c:if> 
                                                </c:if> 
                                                ${atividade.nomeLocal}
                                            </a>
                                          </li>
                                        </c:forEach>
                                      </ul>
                                   </div>
                              </div>
                            </div>
                          </c:forEach>
                        </div>
                          
                       <%--  <ul id="lista-dias" class="nav nav-list diario-sidenav" style="margin-left: 0px; background-color: #F0F4F5;" data-spy="affix">
                          <c:forEach items="${diario.capitulos}" var="capitulo">
                            <li>
                                <a id="nav-dia-${capitulo.numero}" href="#dia-${capitulo.numero}" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/date.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    ${capitulo.numero}º dia
                                </a>
                            </li>
                          </c:forEach>
                        </ul> --%>
                        
                      </fieldset>
                    </div>
                
                    <div id="pageContent" class="span15" style="margin-top: 0px; margin-left: 15px;">
                    
                        <%-- <ul class="nav nav-pills" id="tab-dia-a-dia">
                        
                            <li>
                                <a href="#tab-capa" data-toggle="tab">Capa</a>
                            </li>
                
                            <c:forEach items="${diario.capitulos}" var="capitulo">
                            
                                <li>
                                    <a href="#tab-dia-${capitulo.numero}" data-toggle="tab">${capitulo.numero}º dia</a>
                                </li>
                
                            </c:forEach>
                        </ul> --%>
                        
                        <%-- <div class="tab-content">
                        
                            <div class="tab-pane active" id="tab-capa">
                        
                                <fieldset>
                                    
                                    <legend>
                                        <h3>Imagem de Capa</h3>
                                    </legend>
                                    
                                    <div class="btn-group">
                                        <a class="btn dropdown-toggle btn-warning" data-toggle="dropdown" href="#">
                                            <i class="icon-camera icon-white"></i> Adicionar uma capa <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a id="enviar-foto-capa" href="#">
                                                    Enviar uma foto
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Escolher do album
                                                </a> 
                                            </li>
                                        </ul>
                                    </div>
                                    
                                    <input type="file" id="arquivo-foto-capa" name="fotoCapa" style="display: none" />
                                    
                                </fieldset>
                           
                                <fieldset>
                                    
                                    <legend>
                                        <h3>Introdução</h3>
                                    </legend>
                                    
                                    <span>
                                        
                                        Exibir no meu diário
                                        <div class="btn-group " data-toggle="buttons-radio">
                                            <a class="btn exibirAtividade active" data-atividade-id="introducao" data-value="true">
                                                Sim
                                            </a>
                                            <a class="btn exibirAtividade" data-atividade-id="introducao" data-value="false">
                                                Não
                                            </a>
                                        </div>
                                        
                                        label for="exibir-introducao" class="first on" >
                                            Exibir no meu diário
                                            <input id="exibir-introducao" type="radio" name="exibirIntroducao" value="false" class="fancy exibirAtividade" data-atividade-id="introducao">
                                        </label>
                                        <label for="ocultar-introducao">
                                            Ocultar no meu diário
                                            <input id="ocultar-introducao" type="radio" name="exibirIntroducao" value="true" class="fancy exibirAtividade" data-atividade-id="introducao">
                                        </label
                                    </span>
                                    
                                    <div id="painel-atividade-introducao">
                                    
                                      <form:form action="${salvar_diario_url}" modelAttribute="diario" class="diarioForm">
                                      
                                        <input type="hidden" name="diario" value="${diario.id}" />
                                        <form:hidden id="hidden-introducao" path="exibirIntroducao" />
                                    
                                        <form:textarea id="text-editor-introducao" path="textoIntroducao" cols="80" rows="10" />
                                        
                                        <div style="width: 100%">
                                            <div align="right">
                                                <div class="form-actions">
                                                    <button class="btn btn-primary salvar" data-loading-text="Aguarde..." data-complete-text="Salvo!">
                                                        <i class="icon-ok icon-white"></i> Salvar alterações
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                      </form:form>
                                    
                                    </div>
                                    
                                </fieldset>
                                
                            </div>
                
                        <c:forEach items="${diario.capitulos}" var="capitulo">
                        
                            <div class="tab-pane" id="tab-dia-${capitulo.numero}">
                            
                                <h2>${item.tituloCapitulo}</h2>
                                
                                <c:forEach items="${capitulo.itens}" var="item" varStatus="status">
                                
                                  <fieldset>
                                  
                                    <legend>
                                        <h3>${item.atividade.local.nome}</h3>
                                    </legend>
                                    
                                    <span>
                                    
                                        Você foi à este local?
                                        <div class="btn-group " data-toggle="buttons-radio">
                                            <a class="btn exibirAtividade active" data-atividade-id="${item.atividade.id}" data-value="true">
                                                Sim
                                            </a>
                                            <a class="btn exibirAtividade" data-atividade-id="${item.atividade.id}" data-value="false">
                                                Não
                                            </a>
                                        </div>
                                    
                                        <label for="foi-${item.atividade.id}" class="first on" >
                                            Fui
                                            <input id="foi-${item.atividade.id}" type="radio" name="item[${item.atividade.id}].foi" value="true" class="fancy exibirAtividade" data-atividade-id="${item.atividade.id}">
                                        </label>
                                        <label for="nao-fui-${item.atividade.id}">
                                            Não fui
                                            <input id="nao-fui-${item.atividade.id}" type="radio" name="item[${item.atividade.id}].foi" value="false" class="fancy exibirAtividade" data-atividade-id="${item.atividade.id}">
                                        </label
                                    </span>
                                    
                                    <div id="painel-atividade-${item.atividade.id}">
                                    
                                    <form:form action="${salvar_diario_url}" modelAttribute="diario" class="diarioForm">
                                    
                                        <fieldset>
                                          
                                            <legend>
                                                Relato
                                            </legend>
                                            
                                            <input type="hidden" name="diario" value="${diario.id}" />
                                            <form:hidden id="hidden-${item.atividade.id}" path="itens[${status.index}].ocultar" />
                
                                            Exibir no meu diário
                                            
                                            <div class="btn-group " data-toggle="buttons-radio">
                                                <a class="btn exibirRelato active" data-atividade-id="${item.atividade.id}" data-value="true">
                                                    Sim
                                                </a>
                                                <a class="btn exibirRelato" data-atividade-id="${item.atividade.id}" data-value="false">
                                                    Não
                                                </a>
                                            </div>
                                            
                                            <div id="painel-relato-${item.atividade.id}">
                                        
                                                Seu relato sobre esta atividade
                                                <i class="icon-question-sign helper" title="Conte-nos suas histórias durante sua vista a este local."></i>
                                                
                                                <form:textarea id="text-editor-${item.atividade.id}" path="itens[${status.index}].texto" cols="80" rows="10" />
                                                                            
                                                <div style="width: 100%">
                                                    <div align="right">
                                                        <div class="form-actions">
                                                            <button class="btn btn-primary salvar" data-loading-text="Aguarde..." data-complete-text="Salvo!">
                                                                <i class="icon-ok icon-white"></i> Salvar alterações
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                            
                                        </fieldset>
                                        
                                    </form:form>
                                    
                                    <fieldset>
                                    
                                        <legend>
                                            Fotos desta atividade
                                        </legend>
                                        
                                        <div class="span11">
                                            <ul id="carrossel-${item.atividade.id}" class="jcarousel-skin-tango">
                                                <!-- The content goes in here -->
                                                <c:forEach items="${item.atividade.fotos}" var="foto">
                                                    <li>
                                                        <a href="<c:url value="${foto.urlOriginal}"/>" class="fancybox" rel="slideShow">
                                                            <img src="${foto.urlCarrosel}" width="134" height="94">
                                                        </a>                                
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        
                                        <a href="#modal-${item.atividade.id}" class="modal-file-upload">
                                            <img src="<c:url value="/resources/images/no_photo.png"/>" width="134" height="94" />
                                        </a>
                                        
                                        <div style="display:none">
                                            <div id="modal-${item.atividade.id}">
                                            
                                                <div class="modal-header">
                                                    <h3>Incluir fotos</h3>
                                                    Dia tal - Atividade tal
                                                </div>
                                                <div class="modal-body" style="display: inline-block;">
                
                                                    <p>
                                                        <div id="uploaded-list-${item.atividade.id}" class="uploaded-list span16">
                                
                                                        </div>
                                                    </p>
                                                    
                                                    <p>
                                                        <div id="file-uploader-${item.atividade.id}" rel="uploadable" data-id-atividade="${item.atividade.id}">
                                                        </div>
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <!-- <a href="#" class="btn" data-dismiss="modal">
                                                        <i class="icon-remove"></i> Fechar>
                                                    </a> -->
                                                </div>
                                            </div>
                                        </div>
                                          
                                        <script>
                                            
                                            CKEDITOR.replace( 'text-editor-${item.atividade.id}', {
                                                toolbar : 'Basic',
                                                uiColor : '#DDDDDD'
                                            });
                                            
                                            $('#modal-${item.atividade.id}').on('show', function () {
                                                $('#file-uploader-${item.atividade.id}').find('input[name="file"]').click();
                                            });
                                        </script>
                                    
                                    </fieldset>                        
                                    
                                    <fieldset>
                                    
                                        <legend>
                                            Avalie este local
                                        </legend>
                                        
                                        fan:formAvaliacao action="" modelAttribute="avaliacao" idLocal="${item.atividade.local.id}" id="form-avaliacao-${item.atividade.id}" /
                                        
                                    </fieldset>
                                        
                                    <fieldset>
                                    
                                        <legend>
                                            Deixe dicas sobre este local
                                        </legend>
                                        
                                        fan:formAvaliacao action="" modelAttribute="avaliacao" idLocal="${item.atividade.local.id}" id="form-avaliacao-${item.atividade.id}" /
                                        
                                    </fieldset>
                                    
                                    </div>
                                    
                                  </fieldset>
                                  
                                  <fieldset>
                                    
                                      <a class="btn">
                                          <i class="icon-list-alt"></i> Adicionar História
                                      </a>
                                  
                                  </fieldset>
                                    
                                </c:forEach>
                                
                            </div>
                            
                        </c:forEach>
                        
                        </div> --%>
                        
                    </div>
                  </div>
                
                </fieldset>
                
                <div id="modal-excluir-atividade" class="modal hide fade">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3>Excluir atividade</h3>
                    </div>
                    <div class="modal-body">
                        <p>Deseja realmente excluir esta atividade?</p>
                        <p><span class="red"><strong>ATENÇÃO:</strong></span> Todas as fotos, relatos, dicas e avaliações serão perdidos!</p>
                    </div>
                    <div class="modal-footer">
                        <a id="confirmar-excluir-atividade" href="#" class="btn btn-primary" data-atividade-id="">
                            <i class="icon-ok icon-white"></i> 
                            Sim
                        </a>
                        <a href="#" class="btn" data-dismiss="modal">
                            <i class="icon-remove"></i> 
                            Não
                        </a>
                    </div>
                </div>                    
                
                <div id="modal-publicar-diario" class="modal hide fade">
                    <div class="modal-header">
                        <button id="btn-close-modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3>Publicar Diário de Viagem</h3>
                    </div>
                    <div id="modal-publicar-diario-body" class="modal-body">
                      <div id="painel-publicar-diario">
                        <p>
                             Compartilhe suas histórias, fotos, avaliações e dicas com os outros fanáticos por viagens.
                             Ao publicar seu diário de viagem, outras pessoas poderão ver e comentar o conteúdo.
                        </p>
                        <p>
                            <span class="red"><strong>ATENÇÃO:</strong></span>
                            Selecione a privacidade desejada para este diário
                        </p>
                        <p>
                            <label class="span4 fancy-text">
                                 <span>
                                     Quem poderá ver este Diário?
                                 </span>
                            </label>
                            <div class="span3" style="margin-left: 0;">
                                <fan:privButton name="visibilidade" id="btnVisibilidade" defaultValue="<%=TipoVisibilidade.PUBLICO %>"
                                                dropDownStyle="z-index: 900;" 
                                                dropDownItemStyle="min-height: 20px; padding: 0px;"
                                                items="<%= new TipoVisibilidade[] {TipoVisibilidade.PUBLICO, TipoVisibilidade.AMIGOS} %>" />
                            </div>
                        </p>
                        <div style="height: 80px;"></div>
                      </div>
                      <div id="painel-compartilhar-diario" class="hide">
                        
                        <div class="alert alert-block alert-success fade in">
                            <h3 class="alert-heading">Parabéns! Você publicou seu diário de viagens!</h3>
                            <p>Compartilhe agora mesmo com seus amigos nas redes sociais.</p>
                        </div>
                        <p>
                          <div class="servicos">
                            <ul class="servicos centerAligned">
                                <li id="fb-share" class="facebook">
                                    <a href="#" onclick="postToFeed(); return false;" title="Compartilhar no Facebook"></a>
                                    <b title="Você compartilhou no Facebook" style="display: none;"></b>
                                </li>
                                <li id="tw-share" class="twitter">
                                    <a href="http://twitter.com/intent/tweet?url=http://developers.facebook.com/docs/reference/dialogs&lang=pt_BR" target="_blank" data-lang="pt_BR" title="Compartilhar no Twitter"></a>
                                    <b title="Você compartilhou no Twitter"  style="display: none;"></b>
                                    <script type="text/javascript" charset="utf-8">
                                      window.twttr = (function (d,s,id) {
                                        var t, js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
                                        js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
                                        return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
                                      }(document, "script", "twitter-wjs"));
                                    </script>
                                </li>
                            </ul>
                          </div>                        
                        </p>
                        <script>
                        
                          window.fbAsyncInit = function() {
                            FB.init({appId: '${tripFansEnviroment.facebookClientId}', status: true, cookie: true});
                            
                            twttr.events.bind('tweet', function(event) {
                                $('#tw-share').find('a').hide();
                                $('#tw-share').find('b').show();
                            });
                          }
                        
                          function postToFeed() {
                            var obj = {
                              method: 'feed',
                              redirect_uri: '${serverUrl}/<c:url value="/planoViagem/diario/${diario.id}"/>',
                              link: '${serverUrl}/<c:url value="/planoViagem/diario/${diario.id}"/>',
                              picture: '<c:url value="${diario.viagem.fotoIlustrativa.urlAlbum}"/>',
                              /*link: 'http://www.tripfans.com.br/',
                              picture: 'http://www.tripfans.com.br/resources/images/tripfanslogo.png',*/
                              name: 'Diário de viagem',
                              caption: '${diario.viagem.nome}',
                              description: '${diario.viagem.descricao}'
                            };

                            function callback(response) {
                              if (response['post_id']) {
                            	  $('#fb-share').find('a').hide();
                            	  $('#fb-share').find('b').show();
                              }
                            }

                            FB.ui(obj, callback);
                          }                        
                        </script>
                      </div>
                    </div>
                    <div class="modal-footer">
                        <a id="cancelar-publicacao" href="#" class="btn" data-dismiss="modal">
                            <i class="icon-remove"></i> 
                            Cancelar
                        </a>
                        <a id="confirmar-publicacao" href="#" class="btn btn-primary" data-loading-text="Aguarde...">
                            <i class="icon-ok icon-white"></i> 
                            Publicar
                        </a>
                        <a id="concluir-publicacao" href="<c:url value="/planoViagem//diario/${diario.id}" />" class="btn btn-primary hide">
                            <i class="icon-ok icon-white"></i> 
                            Concluir
                        </a>
                    </div>
                </div>
                
            </div>
            
            <div id="fb-root"></div>
        </div>
        
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
    
        <!-- TOUCH AND MOUSE WHEEL SETTINGS -->
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.touchwipe.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.mousewheel.min.js" />"></script> 
        
        <!-- jQuery SERVICES Slider -->
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.themepunch.services.js" />"></script>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/services-plugin/css/settings.css" />" media="screen" />
    
        <script>

            //CKEDITOR.config.removePlugins = 'elementspath';
            
            function atualizarContagemItens($labelContagem, qtd) {
                var qtdAtual = parseInt($labelContagem.html());
                if (isNaN(qtdAtual)) {
                	qtdAtual = 0;
                }
            	qtdAtual = qtdAtual + (qtd);
                $labelContagem.html(qtdAtual);
                if (qtdAtual > 0) {
                	$labelContagem.css('display', '');
                } else {
                	$labelContagem.css('display', 'none');
                }
            }
            
            jQuery(function() {
            	// carreando dados do primeiro dia
                $('#pageContent').load('<c:url value="/planoViagem/diario/${diario.id}/listarAtividadesDia"/>/1', {}, function(response) {
                    $('#pageContent').initToolTips();
                });
            	
                var $window = $(window);
                
                $('.fancy').fancy();
                
                $('.sortable').sortable({
                	//placeholder: 'ui-state-highlight'
                	connectWith: ".connected-sortable"
                });
                
                $('.accordion-toggle').click(function(event) {
                    event.preventDefault();
                    var dia = $(this).attr('data-dia');
                    var $painel = $('#item-dia-' + dia);
                    if (!$painel.hasClass('in')) {
                        $('.accordion-heading').css('background-color', 'transparent');// removeClass('active');
                        $(this).closest('.accordion-heading').css('background-color', '#DCEAF4') //.addClass('active');
                    } else {
                    	event.stopPropagation();                    	
                    }
                    $('.item-relato').removeClass('active');
                    $('#pageContent').load('<c:url value="/planoViagem/diario/${diario.id}/listarAtividadesDia/"/>' + $(this).attr('data-dia'), {}, 
                        function(response) {
                            $('#pageContent').initToolTips();
                        }
                    );
                });
                
                $('#btn-fechar-ajuda').on('click', function(e) {
                	$('#div-ajuda-top').show();
                });
                
                $('.btn-mostrar-ajuda').on('click', function(e) {
                    e.preventDefault();
                    if ($(this).attr('class').indexOf('active') === -1) {
                        //$('.painel-ajuda').css('display', '');
                        $('.painel-ajuda').fadeIn('slow', function() {});
                        $(this).html('<i class="icon-question-sign icon-white"></i> Esconder Ajuda');
                    } else {
                        //$('.painel-ajuda').css('display', 'none');
                        $('.painel-ajuda').effect('blind', {}, 500, function() {});// ('slow', function() {});
                        $(this).html('<i class="icon-question-sign icon-white"></i> Exibir Ajuda');
                    }
                });
                
                
                $(document).on('click', '.ocultar-atividade', function (e) {
                    e.preventDefault();
                    
                    var idAtividade = $(this).data('atividade-id');
                    var ocultar = ($(this).attr('class').indexOf('active') != -1);
                    
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/planoViagem/diario/ocultarAtividade/"/>' + idAtividade + '/' + ocultar,
                        success: function(data, status, xhr) {
                            if (data.success) {
                            }
                        }
                    });
                });
                
                $(document).on('click', '.excluir-atividade', function (e) {
                    e.preventDefault();
                    $('#modal-excluir-atividade').modal({
                        backdrop : 'static',
                        keyboard : false
                    })
                    $('#confirmar-excluir-atividade').data('atividade-id', $(this).data('atividade-id'));
                    $('#modal-excluir-atividade').modal('show');
                });
                
                $('#confirmar-excluir-atividade').click(function(e) {
                    var $link = $(this);
                    if (!$link.hasClass('disabled')) {
                        var idAtividade = $link.data('atividade-id');
                        $.ajax({
                            type: 'POST',
                            url: '<c:url value="/planoViagem/diario/excluirAtividade/"/>' + idAtividade,
                            success: function(data, status, xhr) {
                                if (data.success) {
                                    $link.tooltip('hide');
                                    // remover da lista
                                    $('#item-lista-atividade-' + idAtividade).remove();
                                    // subtrair 1 da quantidade de itens do dia
                                    var $label = $('#item-menu-atividade-' + idAtividade).closest('.accordion-group').find('.label-qtd-itens');
                                    atualizarContagemItens($label, -1)
                                    // remover do menu
                                    $('#item-menu-atividade-' + idAtividade).remove();
                                    $('#modal-excluir-atividade').modal('hide');
                                }
                            }
                        });
                    }
                });
                
                $(document).on('click', '.editar-atividade', function (e) {
                    e.preventDefault();
                    $('.item-relato').removeClass('active');
                    
                    var idAtividade = $(this).data('atividade-id');
                    $('#item-menu-atividade-' + idAtividade).find('a.item-relato').addClass('active');
                    $('#pageContent').load($(this).attr('href'), {}, 
                        function(response) {
                            $('#pageContent').initToolTips();
                        }
                    );
                });
                
                $('#btn-publicar-diario').click(function(e) {
                    e.preventDefault();
                    $('#modal-publicar-diario').modal({
                    	backdrop : 'static',
                    	keyboard : false
                    })
                    $('#modal-publicar-diario').modal('show');
                });
                
                $('#confirmar-publicacao').click(function(e) {
                    e.preventDefault();
                    var $botao = $(this);
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/planoViagem/diario/${diario.id}/publicar/" />',
                        success: function(data, status, xhr) {
                            $('#btn-close-modal').hide();
                            $('#painel-compartilhar-diario').show();
                        	$('#painel-publicar-diario').hide();
                        	$('#confirmar-publicacao').hide();
                            $('#cancelar-publicacao').hide();
                            $('#concluir-publicacao').show();
                            $botao.button('reset');
                        }
                    });

                });
                
                $( '#img-foto-capa' ).draggable({ 
                    axis: 'y',
                    containment: 'parent'
                    //containment: [0, -512, 0, 1536]
                });
                
                $('#div-foto-capa').mouseout(function() {
                    $('.instructionWrap').hide();
                });
                
                $('#div-foto-capa').mouseover(function() {
                    $('.instructionWrap').show();
                });
                
                
                /*
                Comentando provisoriamente
                twttr.events.bind('tweet', function(event) {
                    $('#tw-share').find('a').hide();
                    $('#tw-share').find('b').show();
                });*/

                /*$('.accordion-toggle').mouseover(function() {
                    var $item = $(this);
                    var timeout = setTimeout(function() {
                        //$item.click();
                        var $accordionBody = $($item.attr('href'));
                        if (!$accordionBody.hasClass('in')) {
                        	$accordionBody.collapse('show');
                        	//$item.click();
                        }
                    }, 1000);
                    $item.data('timeout', timeout);
                });*/
                
                /*var $tabs = $('.accordion-group');
                
                var $tab_items = $('ul:first li', $tabs ).droppable({
                    accept: '.connected-sortable li',
                    hoverClass: 'ui-state-hover',
                    drop: function( event, ui ) {
                        var $item = $(this);
                        alert('1')
                        var $list = $($item.find('a').attr('href'))
                            .find('.connected-sortable');

                        ui.draggable.hide( 'slow', function() {
                            $tabs.tabs( 'select', $tab_items.index( $item ) );
                            $( this ).appendTo( $list ).show( "slow" );
                        });
                    }
                });*/
                
                // side bar
                /*$('#lista-dias').affix({
                    offset: {
                        top: function () { return $window.width() <= 980 ? 290 : 210 }, 
                        bottom: 270
                    }
                });*/

            	/*$('a.modal-file-upload').fancybox({
                    'hideOnContentClick': false
                });
            	
                //$('.text-editor').on('show', function() {
                	//alert('1')
                	
                //});
                
                /*$('.text-editor').ckeditor({
                    toolbar : 'Basic',
                    uiColor : '#DDDDDD'
                });*/
                
                $(document).on('click', '.item-relato', function (e) {
                	e.preventDefault();
                    $('.item-relato').removeClass('active');
                    $(this).addClass('active');
                    $('#pageContent').load($(this).attr('href'), {}, 
                        function(response) {
                            $('#pageContent').initToolTips();
                        }
                    );
                });
                
                /*var uploaderCapa = new qq.FileUploader({
                    element: document.getElementById('enviar-foto-capa'),
                    //listElement : document.getElementById(id + '-list'),
                    action: '<c:url value="/planoViagem/diario/salvarFotoCapa" />',
                    params : {
                        idDiario : '${diario.id}',
                    },
                    allowedExtensions: ['jpg', 'jpeg', 'png'],
                    multiple: false,
                    debug: false,
                    onSubmit: function(id, fileName) {
                        //console.log ($(element))
                        //$(element).append("<span class='image'><div id='progress-" + id + "'></div></span>")
                    },
                    onProgress: function(id, fileName, loaded, total){
                        //var progress = (loaded / total) * 100;
                        //$("#progress-" + id).progressbar({value: progress})
                    },
                    onComplete: function(id, fileName, responseJSON) {
                        
                    	urlFotoCapa = '';
                        $.each(responseJSON, function(key, item) {
                            if (key == "params") {
                            	urlFotoCapa = item.urlFotoCapa;
                            }
                        });
                        
                        if (urlFotoCapa != null) {
                            $('#img-foto-capa').append('<img src="' + urlFotoCapa + '" style="min-height: 100%; min-width: 100%; position: absolute; left: 0;" />');
                            $('#div-foto-capa').show();
                        }
                    },
                    onCancel: function(id, fileName){
                        
                    },
                    template: '<div class="qq-uploader">' +
                        '<div class="qq-upload-drop-area"><span></span></div>' +
                        '<div class="qq-upload-button">Enviar Foto</div>' +
                        '<ul class="qq-upload-list unstyled" style="display:none"></ul>' +
                    '</div>',
                    fileTemplate:  '<li>' +
                        '<span class="qq-upload-file"></span>' +
                        '<span class="qq-upload-spinner"></span>' +
                        '<span class="qq-upload-size"></span>' +
                        '<a class="qq-upload-cancel label important" href="#">Cancelar</a>' +
                        '<span class="qq-upload-failed-text">Falhou</span>' +
                     '</li>',
                    messages: {
                        typeError: "{file} has invalid extension. Only {extensions} are allowed.",
                        sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
                        minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                        emptyError: "{file} is empty, please select files again without it.",
                        onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
                    },
                    showMessage: function(message){
                        alert(message);
                    } 
                });*/
                    
                /*$('#tab-dia-a-dia a').click(function (e) {
                	e.preventDefault();
            	    $(this).tab('show');
            	})*/
            	
            	//$('#tab-dia-a-dia a:first').click();
            	
                /*$('[rel="uploadable"]').each(function() {
                    initializeUploader($(this).attr('id'), $(this).attr('data-id-atividade'));
                });
                
                $('.fancy').fancy();
                
                jQuery('.jcarousel-skin-tango').jcarousel({
                    // Configuration goes here
                });
                
                $("a[rel=slideShow]").fancybox({
                    transitionIn      : 'elastic',
                    transitionOut     : 'elastic',
                    type              : 'image',
                    autoScale : false,
                    autoDimensions : false,
                    width : 560,
                    height : 340,
                    speedIn: 400, 
                    speedOut: 200, 
                    overlayShow: true,
                    overlayOpacity: 0.1,
                    hideOnContentClick: true,
                    'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                        return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                    },
                    titlePosition: 'inside'
                });
                
                //$('#tab-dia-a-dia a:first').tab('show');
                
                $('.exibirAtividade').click(function (e) {
                    e.preventDefault();
                    var $button = $(this);
                    
                    var idAtividade = $button.attr('data-atividade-id');
                    var checked = $button.attr('data-value');
                    // Exibir ou ocultar painel
                    if (checked == 'true') {
                        $('#painel-atividade-' + idAtividade).show();
                        $('#hidden-' + idAtividade).val(true);
                    } else {
                        $('#painel-atividade-' + idAtividade).hide();
                        $('#hidden-' + idAtividade).val(false);
                    }
                });
                
                $('.exibirRelato').click(function (e) {
                    e.preventDefault();
                    var $button = $(this);
                    
                    var idAtividade = $button.attr('data-atividade-id');
                    var checked = $button.attr('data-value');
                    // Exibir ou ocultar painel
                    if (checked == 'true') {
                        $('#painel-relato-' + idAtividade).show();
                        $('#hidden-' + idAtividade).val(true);
                    } else {
                        $('#painel-relato-' + idAtividade).hide();
                        $('#hidden-' + idAtividade).val(false);
                    }
                });

                
                $('.diarioForm').unbind('submit');
                $('.diarioForm').submit(function(event) {
                    event.preventDefault();
                    
                    var $form = $(this);
                    
                    var $botaoSalvar = $form.find('.salvar');
                    $botaoSalvar.button('loading');
                    
                    var formData = $form.serialize();
                    
                    if ($form.valid()) {
                        $.ajax({
                            type: 'POST',
                            url: $form.attr('action'),
                            data: formData,
                            success: function(data, status, xhr) {
                                alert('ae!');
                                $botaoSalvar.button('complete');
                                setTimeout(function() {
                                	$botaoSalvar.button('reset');
                                }, 1000);
                            }
                        });
                    }
                });*/
                
            });
            
            /*function initializeUploader(idFileUploader, idAtividade) {
            	var element = document.getElementById(idFileUploader);
            	var uploader = new qq.FileUploader({
                    element: element,
                    //listElement : document.getElementById(id + '-list'),
                    action: '<c:url value="/planoViagem/diario/salvarFoto" />',
                    params : {
                    	idAtividade : idAtividade,
                    },
                    allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
                    multiple: true,
                    debug: false,
                    onSubmit: function(id, fileName) {
                    	//console.log ($(element))
                        //$(element).append("<span class='image'><div id='progress-" + id + "'></div></span>")
                    },
                    onProgress: function(id, fileName, loaded, total){
                    	//var progress = (loaded / total) * 100;
                        //$("#progress-" + id).progressbar({value: progress})
                    },
                    onComplete: function(id, fileName, responseJSON) {
                    	
                    	urlFotoCarrosel = '';
                    	urlFotoOriginal = '';
                        $.each(responseJSON, function(key, item) {
                            if (key == "params") {
                            	urlFotoCarrosel = item.urlFotoCarrosel;
                            	urlFotoOriginal = item.urlFotoOriginal;
                            }
                        });
                        
                        //$("#progress-" + id).remove();
                        console.log($(element).parent())
                        console.log($(element).closest(".quanta"))
                        console.log(urlFotoCarrosel);
                        
                        //$('#uploaded-list-' + idAtividade).append($('<div></div>').css({'width': '280px', 'height': '210px', 'background-color': '', 'background-image': 'url(' + image_url + ')'}));
                        $('#uploaded-list-' + idAtividade).append('<div class="span5"><img src="' + urlFotoCarrosel + '" width="280" height="210" /></div>');
                        //$('.qq-upload-list').html('');
                        
                        var htmlImg = '<a href="' + urlFotoOriginal + '" class="fancybox" rel="slideShow">' +
                        '<img src=' + urlFotoOriginal + '" width="134" height="94"></a>';
                      
                        adicionarFotoCarrossel('#carrossel-' + idAtividade, htmlImg);
                    },
                    onCancel: function(id, fileName){
                    	
                    },
                    template: '<div class="qq-uploader">' +
                        '<div class="qq-upload-drop-area"><span></span></div>' +
                        '<div class="qq-upload-button btn btn-primary"><i class="icon-picture icon-white"></i> Adicionar Fotos</div>' +
                        '<ul class="qq-upload-list unstyled"></ul>' +
                    '</div>',
                    fileTemplate:  '<li>' +
                        '<span class="qq-upload-file"></span>' +
                        '<span class="qq-upload-spinner"></span>' +
                        '<span class="qq-upload-size"></span>' +
                        '<a class="qq-upload-cancel label important" href="#">Cancelar</a>' +
                        '<span class="qq-upload-failed-text">Falhou</span>' +
                     '</li>',
                    messages: {
                        typeError: "{file} has invalid extension. Only {extensions} are allowed.",
                        sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
                        minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                        emptyError: "{file} is empty, please select files again without it.",
                        onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
                    },
                    showMessage: function(message){
                        alert(message);
                    } 
                });
            }*/
            
            function adicionarFotoCarrossel(idCarrossel, htmlImg) {
            	var $carrossel = $('' + idCarrossel);
            	
                $carrossel.add($carrossel.last, htmlImg);
                
                $('a[rel=slideShow]').fancybox({
                	transitionIn      : 'elastic',
                    transitionOut     : 'elastic',
                    type              : 'image',
                    autoScale : false,
                    autoDimensions : false,
                    width : 560,
                    height : 340,
                    speedIn: 400, 
                    speedOut: 200, 
                    overlayShow: true,
                    overlayOpacity: 0.1,
                    hideOnContentClick: true,
                    'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                        return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                    },
                    titlePosition: 'inside',
                    onComplete: function() {
                        
                    }
                });
            }
            
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            
            /*CKEDITOR.replace( 'text-editor-introducao', {
                toolbar : 'Basic',
                uiColor : '#DDDDDD'
            });*/
        </script>
        
    </tiles:putAttribute>

</tiles:insertDefinition>