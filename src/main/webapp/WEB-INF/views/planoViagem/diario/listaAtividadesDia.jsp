<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <div>
        <fieldset style="background-color: #FFF;">
          
            <c:if test="${not diario.viagem.periodoNaoDefinido}">
              <legend>
                <h4>Atividades do ${diaViagem.numero}º dia - ${diaViagem.diaSemanaLongoDiaMesComMesLongo}</h4>
              </legend>
            </c:if>
            
            <c:if test="${empty diaViagem.atividades}">
                <div class="blank-state">
                    <div class="row">
                        <div class="span2 offset1">
                            <img src="<c:url value="/resources/images/icons/mini/128/Time-3.png" />" width="90" height="90" />
                        </div>
                        <div class="span8">
                            <h3>
                                Sem atividades
                                <c:if test="${not diario.viagem.periodoNaoDefinido}">
                                    neste dia
                                </c:if>
                            </h3>
                            <p>
                                <c:choose>
                                  <c:when test="${usuario.id == diario.viagem.criador.id}">
                                      Você
                                  </c:when>
                                  <c:otherwise>
                                      ${diario.viagem.criador.displayName}
                                  </c:otherwise>
                                </c:choose>
                                ainda não adicionou atividades
                                <c:if test="${not diario.viagem.periodoNaoDefinido}">
                                    neste dia.
                                </c:if>
                            </p>
                            <p>
                                <div class="btn-group" data-toggle="buttons-checkbox" style="margin-bottom: 5px">
                                  <button id="btn-add-atividade" type="button" class="btn btn-selecionar-form" data-fieldset-id="fieldset-add-atividade">
                                    <i class="icon-plus"></i>
                                    Adicionar atividade
                                  </button>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
                <br/>
            </c:if>
              
            <c:if test="${not empty diaViagem.atividades}">
                <div class="btn-group" data-toggle="buttons-checkbox" style="margin-bottom: 5px">
                  <button  id="btn-add-atividade" type="button" class="btn btn-selecionar-form" data-fieldset-id="fieldset-add-atividade">
                    <i class="icon-plus"></i>
                    Adicionar atividade
                  </button>
                  <!-- <button type="button" class="btn btn-selecionar-form" data-fieldset-id="fieldset-add-historia">Adicionar história</button> -->
                </div>
            </c:if>
            
            <fieldset id="fieldset-add-atividade" class="fieldset-form" style="display: none;">
            
                <form id="form-add-atividade" class="options-list label-right" style="margin: 0px;">
                
                    <div id="error-box-atividade" style="display: none;" class="alert alert-error centerAligned"> 
                        <p id="error-title">
                            Corrija os erros a seguir antes de prosseguir:
                        </p>
                        <p id="error-message">
                        </p>
                    </div>
                
                
                    <ul style="margin: 0px; list-style: none;">
                    
                        <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Título
                                </span>
                            </label>
                            <input type="text" id="titulo" name="titulo" class="span7" placeholder="Informe um título para sua história" />
                        </li>
                    

                        <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Local
                                </span>
                            </label>
                            <fan:autoComplete url="/pesquisaTextual/consultarLocaisAtividades" hiddenName="local" id="novaAtividade" 
                                              valueField="id" labelField="nomeExibicao" cssStyle="position: relative;"
                                              showActions="true" blockOnSelect="false" name="pesquisaLocais" cssClass="span7"
                                              placeholder="Informe no nome do local que você visitou"  
                                              addOnLarge="true" />
                        </li>
                        
                        <c:if test="${not diario.viagem.periodoNaoDefinido}">
                          <li>
                            <label class="span2" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                <span>
                                    Horário
                                </span>
                            </label>
                            <input type="text" id="horaInicio" name="horaInicio" alt="time" class="span2" placeholder="Horário (opcional)" />
                          </li>
                        </c:if>
                        
                        <li>
                            <label class="span2" style="text-align: right; margin-right: 15px;">
                            </label>
                            <span>
                                <a id="btn-add-local" href="#" class="btn" data-loading-text="Aguarde...">
                                    <i class="icon-ok"></i> Adicionar
                                </a>
                            </span>                            
                        </li>

                    </ul>
                    <input type="hidden" name="dataInicio" value="<joda:format pattern="dd/MM/yyyy" value="${diaViagem.data}" />" />
                </form>
            
            </fieldset>
              
            <c:if test="${not empty diaViagem.atividades}">
                
                <fieldset id="fieldset-add-historia" class="fieldset-form" style="display: none;">
                
                    <form class="options-list label-right" style="margin: 0px;">
                    
                        <ul style="margin: 0px; list-style: none;">
    
                            <li>
                                <label class="span3" style="margin-top: 10px; text-align: right; margin-right: 15px;">
                                    <span>
                                        Título
                                    </span>
                                </label>
                                <input type="text" class="span8" placeholder="Informe um título para sua história" />
                            </li>
    
                            <li>
                                <label class="span3" style="text-align: right; margin-right: 15px;">
                                </label>
                                <span>
                                    <a id="btn-add-local-2" href="#" class="btn" data-loading-text="Aguarde...">
                                        <i class="icon-plus"></i> Adicionar
                                    </a>
                                </span>                            
                            </li>
    
                        </ul>
                        <input type="hidden" name="dataInicio" value="<joda:format pattern="dd/MM/yyyy" value="${diaViagem.data}" />" />
                    </form>
                
                </fieldset>            
                
                <fieldset>
    
                    <legend>
                        Lista de atividades
                    </legend>
                
                    <table class="table table-striped">
                        <thead>
                            <tr>
                              <th>#</th>
                              <c:if test="${not diario.viagem.periodoNaoDefinido}">
                                <th style="text-align: center;">Horário</th>
                              </c:if>
                              <th style="text-align: center;">Tipo</th>
                              <th>Título/Local</th>
                              <th style="text-align: center;">Relato</th>
                              <th style="text-align: center;">Fotos</th>
                              <th style="text-align: center;">Avaliação</th>
                              <th style="text-align: center;">Dicas</th>
                              <th style="text-align: right;"></th>
                            </tr>
                        </thead>
                        <tbody id="lista-atividades">
                          <c:forEach items="${diaViagem.atividades}" var="atividade" varStatus="status">
                            <tr id="item-lista-atividade-${atividade.id}">
                              <td>${status.index + 1}</td>
                              <c:if test="${not diario.viagem.periodoNaoDefinido}">
                                  <td style="text-align: center;">
                                    <!-- i class="icon-time"></i-->
                                    <span class="label label-success">
                                        <joda:format value="${atividade.horaInicio}" pattern="HH:mm"/>
                                    </span> 
                                  </td>
                              </c:if>
                              <td style="text-align: center;">
                                <i class="${atividade.tipo.iconClass}" title="${atividade.decricaoTipo}"></i>
                              </td>
                              <td>
                                <c:if test="${atividade.possuiTitulo}">
                                    ${atividade.titulo}
                                    <c:if test="${not empty atividade.nomeLocal}">
                                      -
                                    </c:if> 
                                </c:if> 
                                <a href="#">${atividade.nomeLocal}</a>
                              </td>
                              <td style="text-align: center;">
                                <c:if test="${atividade.possuiRelato}">
                                 <i class="icon-ok" title="Você já escreveu uma história sobre esta atividade"></i>
                                </c:if>
                              </td>
                              <td style="text-align: center;">
                                <c:if test="${atividade.possuiFotos}">
                                  <span class="badge" title="Você já adicionou ${atividade.quantidadeFotos} foto(s) a esta atividade">${atividade.quantidadeFotos}</span>
                                </c:if>
                              </td>
                              <td style="text-align: center;">
                                <c:if test="${atividade.possuiAvaliacao}">
                                    <i class="icon-ok" title="Você já avaliou esta atividade"></i>
                                </c:if>
                              </td>
                              <td style="text-align: center;">
                                <c:if test="${atividade.possuiDicas}">
                                  <span class="badge" title="Você já adicionou ${atividade.quantidadeDicas} dica(s) a esta ativdade">${atividade.quantidadeDicas}</span>
                                </c:if>
                              </td>
                              <td style="text-align: right;">
                                <div class="btn-group pull-right">
                                    <a href="<c:url value="/planoViagem/diario/editarAtividade/${atividade.id}"/>" data-atividade-id="${atividade.id}" class="btn btn-mini editar-atividade" title="Editar">
                                        <i class="icon-pencil"></i>
                                    </a>
                                    <a href="#" class="btn btn-mini ocultar-atividade ${atividade.oculta ? 'active' : ''}" title="Não exibir no Diário" data-toggle="button" data-atividade-id="${atividade.id}">
                                        <i class="icon-eye-close"></i>
                                    </a>
                                    <a href="#" class="btn btn-mini excluir-atividade" data-atividade-id="${atividade.id}" title="Excluir">
                                        <i class="icon-remove"></i>
                                    </a>
                                </div>                      
                              </td>
                            </tr>
                          </c:forEach>
                        </tbody>
                    </table>
                    
                </fieldset>
              
            </c:if>
            
        </fieldset>
    </div>
    
    <script>
    
        function adicionarAtividade() {
            var $form = $('#form-add-atividade');
            
            //if ($form.isValid()) {

                var idLocal = $('input[name="local"]').val();
                var dataInicio = $('input[name="dataInicio"]').val();
                var horaInicio = $('input[name="horaInicio"]').val();
                var titulo = $('input[name="titulo"]').val();
                
                var $btnAdicionarLocal = $('#btn-add-local');
                $btnAdicionarLocal.button('loading');
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/planoViagem/diario/${diario.id}/dia/${diaViagem.numero}/adicionarAtividade/"/>',
                    data: {
                    	titulo : titulo,
                    	local : idLocal,
                    	dataInicio : dataInicio,
                        horaInicio : horaInicio
                    },
                    success: function(data, status, xhr) {
                    	if (data.success) {
                            if (data.params) {
                            	// Adicionar atividade ao menu
                                $('#menu-atividades-dia-${diaViagem.numero}').append('' +
                                		'<li id="item-menu-atividade-' + data.params.idAtividade + '">' +
                                		'  <a class="item-relato" href="<c:url value="/planoViagem/diario/editarAtividade/"/>' + data.params.idAtividade + '" style="padding: 6px 15px 6px 15px">' +
                                		'    <i class="' + data.params.iconeAtividade + '"></i> ' + data.params.localAtividade +
                                		'  </a>' +
                                		'</li>'
                                );
                                var $label = $('#menu-atividades-dia-${diaViagem.numero}').closest('.accordion-group').find('.label-qtd-itens');
                            	// Somar +1 ao da quantidade de itens do dia
                            	atualizarContagemItens($label, 1);
                            }
                        	$('#pageContent').load('<c:url value="/planoViagem/diario/${diario.id}/listarAtividadesDia/${diaViagem.numero}"/>', {}, 
                                function(response) {
                                    $('#pageContent').initToolTips();
                                    $('input[name="horaInicio"]').val(horaInicio);
                                    $('#btn-add-atividade').click();
                                    $('input[name="titulo"]').focus();
                                }
                        	);
                        	$btnAdicionarLocal.button('reset');
                    	} else {
                    		$('#error-message').html(data.params.message);
                    		$('#error-box-atividade').show();
                    		$('#titulo').focus();
                    		$btnAdicionarLocal.button('reset');
                    	}
                    }
                });
            /*} else {

            }
        	*/
        }
        
        jQuery(function() {
        	
            $('.fancy').fancy();
        	
        	$('#horaInicio').timepicker({
                hourGrid: 4,
                minuteGrid: 10,
                stepMinute: 5,
                addSliderAccess: true,
                sliderAccessArgs: { touchonly: false },
            });
        	
            $('#btn-add-local').on('click', function(event) {
                event.preventDefault();
                adicionarAtividade();
            });

            $('.btn-selecionar-form').on('click', function(e) {
                e.preventDefault();
                
                var fieldsetId = $(this).attr('data-fieldset-id');
                
                if ($(this).attr('class').indexOf('active') === -1) {
                	$('.btn-selecionar-form').removeClass('active')
                	$('.fieldset-form').css('display', 'none');
                    $('#' + fieldsetId).css('display', '');
                    $('#titulo').focus();
                } else {
                	$('.fieldset-form').css('display', 'none');
                }
            });
            
            //$('#form-add-atividade').validate({debug : true});
            
            $('#form-add-atividade').validate({
                rules: {
                    titulo : { 
                        required  : true, 
                        minlength : 3
                    },
                    'local' : { 
                        required  : true, 
                        minlength : 3
                    }
                }, 
                messages: { 
                    'titulo' : {
                        required : 'Escreva algo sobre o local que você visitou',
                        minlength: 'Seu título deve conter no mínimo {1} caracteres'
                    },
                    'local' : {
                        required : 'Informe o local que você visitou'
                    }
                },
                errorPlacement: function(error, element) {
                    var parent = element.parent()[0];
                    error.appendTo(parent);
                },
                errorContainer: '#error-box-atividade'
            });            
            
        });
    </script>