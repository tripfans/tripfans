<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div>

    <div id="lista-viagens" class="container-fluid" style="margin-left: 0">
    
      <div class="row">
    
        <c:forEach items="${viagens}" var="viagem">
        
            <c:url value="/planoViagem/detalharViagem/${viagem.id}" var="url_exibir" />
            <sec:authorize access="isAuthenticated()">
                <c:if test="${principal.id == viagem.usuarioCriador.id}">
                    <%-- c:url value="/planoViagem/detalharViagem/${viagem.id}" var="url_exibir" /--%>
                    <c:url var="url_exibir" value="/viagem/${viagem.id}" />
                </c:if>
            </sec:authorize>
            
            <div class="box-viagem col-xs-12 col-sm-6 col-md-6 col-lg-6">
            
                <fan:cardResponsive id="box-viagem-${viagem.id}" type="viagem" trip="${viagem}"
                          itemUrl="${url_exibir}" showActions="true" imgStyle="margin-right: -15px;" nameStyle="font-size : 16px;" imgSize="4"
                          marginLeft="${marginLeft}" marginRight="${marginRight}"
                          selectable="false" spanSize="11" cardStyle="z-index: 9000; " width="150" height="112" >
                          
                    <jsp:attribute name="info">
                    
                        <div class="local-box-details">
                            <div>
                                <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
                                <div>
                                    ${periodoFormatado}
                                    <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                                      - 
                                    </c:if>
                                    <c:if test="${not empty viagem.quantidadeDias}">
                                      ${viagem.quantidadeDias} dias
                                    </c:if>
                                    <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                                      Sem data definida 
                                    </c:if>
                                </div>
                                
                            </div>
                            <div>
                                <p>
                                  <c:forEach items="${viagem.cidades}" var="cidade" varStatus="status">
                                    <a href="#">${cidade.nomeComSiglaEstado}</a>${not status.last ? ' - ' : ''}
                                  </c:forEach>
                                </p>
                            </div>
                        </div>
                        
                        <!-- <div style="word-wrap: break-word; display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; max-width: 100%;"> -->
                        <div style="word-wrap: break-word; overflow: hidden; text-overflow: ellipsis; max-width: 100%; max-height: 60px;">
                            ${viagem.descricao}
                        </div>
                        
                    </jsp:attribute>
    
                    <jsp:attribute name="aditionalInfo">
                    
                        <p>
                          <c:forEach items="${viagem.participantes}" var="participante">
                            <div style="float:left; padding: 1px;" title="">
                                <a href="#" target="_blank"> 
                                    <!-- <img src="http://graph.facebook.com/100002024342727/picture" style="width: 50px; height: 50px;" /> -->
                                </a>
                            </div>
                          </c:forEach>
                        </p>
                        
                        <p></p>
                        
                    </jsp:attribute>
                    
                    
                    <jsp:attribute name="actions">
    
                      <c:if test="${usuario != null and usuario.id == perfil.id}">
                    
                        <div style="border-bottom: 1px solid #eee; height: 2px; width: 100%;"></div>
                        <div class="pull-right" style="padding-right: 6px; margin: 6px 0 6px;">
                            <a class="btn btn-mini btn-info action-button dropdown-toggle btn-excluir" href="#" data-viagem-id="${viagem.id}">
                                <i class="icon-trash icon-white"></i> 
                                Excluir
                            </a>
                            <%-- <a class="btn btn-mini btn-info action-button dropdown-toggle" href="#">
                                <i class="icon-pencil icon-white"></i> 
                                Editar
                            </a> --%>
                            <%-- c:choose>
                              <c:when test="${not viagem.possuiDiario}">
                                <a class="btn btn-mini btn-info action-button dropdown-toggle" 
                                   href="<c:url value="/planoViagem/${viagem.id}/criarDiario" />">
                                    <i class="icon-list-alt icon-white"></i> 
                                    Criar Di�rio de Viagem
                                </a>
                              </c:when>
                              <c:otherwise>
                                <a class="btn btn-mini btn-info action-button dropdown-toggle" 
                                   href="<c:url value="/planoViagem/editarDiario/${viagem.diario.id}" />">
                                    <i class="icon-list-alt icon-white"></i> 
                                    Editar Di�rio de Viagem
                                </a>
                              </c:otherwise>
                            </c:choose--%>
                        </div>
    
                      </c:if>
                        
                    </jsp:attribute>
                    
                </fan:cardResponsive>
            </div>
            
            <div class="box-viagem col-xs-12 col-sm-6 col-md-6 col-lg-6">
            
                <fan:cardResponsive id="box-viagem-${viagem.id}" type="viagem" trip="${viagem}"
                          itemUrl="${url_exibir}" showActions="true" imgStyle="margin-right: -15px;" nameStyle="font-size : 16px;" imgSize="4"
                          marginLeft="${marginLeft}" marginRight="${marginRight}"
                          selectable="false" spanSize="11" cardStyle="z-index: 9000; " width="150" height="112" >
                          
                    <jsp:attribute name="info">
                    
                        <div class="local-box-details">
                            <div>
                                <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
                                <div>
                                    ${periodoFormatado}
                                    <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                                      - 
                                    </c:if>
                                    <c:if test="${not empty viagem.quantidadeDias}">
                                      ${viagem.quantidadeDias} dias
                                    </c:if>
                                    <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                                      Sem data definida 
                                    </c:if>
                                </div>
                                
                            </div>
                            <div>
                                <p>
                                  <c:forEach items="${viagem.cidades}" var="cidade" varStatus="status">
                                    <a href="#">${cidade.nomeComSiglaEstado}</a>${not status.last ? ' - ' : ''}
                                  </c:forEach>
                                </p>
                            </div>
                        </div>
                        
                        <!-- <div style="word-wrap: break-word; display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; max-width: 100%;"> -->
                        <div style="word-wrap: break-word; overflow: hidden; text-overflow: ellipsis; max-width: 100%; max-height: 60px;">
                            ${viagem.descricao}
                        </div>
                        
                    </jsp:attribute>
    
                    <jsp:attribute name="aditionalInfo">
                    
                        <p>
                          <c:forEach items="${viagem.participantes}" var="participante">
                            <div style="float:left; padding: 1px;" title="">
                                <a href="#" target="_blank"> 
                                    <!-- <img src="http://graph.facebook.com/100002024342727/picture" style="width: 50px; height: 50px;" /> -->
                                </a>
                            </div>
                          </c:forEach>
                        </p>
                        
                        <p></p>
                        
                    </jsp:attribute>
                    
                    
                    <jsp:attribute name="actions">
    
                      <c:if test="${usuario != null and usuario.id == perfil.id}">
                    
                        <div style="border-bottom: 1px solid #eee; height: 2px; width: 100%;"></div>
                        <div class="pull-right" style="padding-right: 6px; margin: 6px 0 6px;">
                            <a class="btn btn-mini btn-info action-button dropdown-toggle btn-excluir" href="#" data-viagem-id="${viagem.id}">
                                <i class="icon-trash icon-white"></i> 
                                Excluir
                            </a>
                            <%-- <a class="btn btn-mini btn-info action-button dropdown-toggle" href="#">
                                <i class="icon-pencil icon-white"></i> 
                                Editar
                            </a> --%>
                            <%-- c:choose>
                              <c:when test="${not viagem.possuiDiario}">
                                <a class="btn btn-mini btn-info action-button dropdown-toggle" 
                                   href="<c:url value="/planoViagem/${viagem.id}/criarDiario" />">
                                    <i class="icon-list-alt icon-white"></i> 
                                    Criar Di�rio de Viagem
                                </a>
                              </c:when>
                              <c:otherwise>
                                <a class="btn btn-mini btn-info action-button dropdown-toggle" 
                                   href="<c:url value="/planoViagem/editarDiario/${viagem.diario.id}" />">
                                    <i class="icon-list-alt icon-white"></i> 
                                    Editar Di�rio de Viagem
                                </a>
                              </c:otherwise>
                            </c:choose--%>
                        </div>
    
                      </c:if>
                        
                    </jsp:attribute>
                    
                </fan:cardResponsive>
            </div>
        
        </c:forEach>
        
      </div>
    
    </div> 
    
    <div class="span5">
    
    </div>
    
</div>

<div id="modal-excluir-viagem" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Excluir Viagem</h3>
    </div>
    <div class="modal-body">
        <p>Deseja realmente excluir esta Viagem?</p>
        <p><span class="red"><strong>ATEN��O:</strong></span> Todas as fotos, coment�rios, dicas, avalia��es, relatos ser�o perdidos!</p>
    </div>
    <div class="modal-footer">
        <a id="confirmar-excluir-viagem" href="#" class="btn btn-primary" data-viagem-id="">
            <i class="icon-ok icon-white"></i> 
            Sim
        </a>
        <a href="#" class="btn" data-dismiss="modal">
            <i class="icon-remove"></i> 
            N�o
        </a>
    </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/masonry/masonry.pkgd.js"></script>
<script>
    $(document).ready(function () {
        
        $('.btn-excluir').click(function(e) {
            e.preventDefault();
            $('#confirmar-excluir-viagem').data('viagem-id', $(this).data('viagem-id'));
            $('#modal-excluir-viagem').modal('show');
        });
        
        $('#confirmar-excluir-viagem').click(function(e) {
            e.preventDefault();
            
            var idViagem = $(this).data('viagem-id');
            var url = '<c:url value="/planoViagem/excluir/" />' + idViagem;

            $.ajax({
                type: 'POST',
                url: url,
                success: function(data, status, xhr) {
                    if (data.success) {
                        // fechar modal
                        $('#modal-excluir-viagem').modal('hide');
                        // remover o box da viagem excluida
                        $('#box-viagem-' + idViagem).hide('explode', {}, 1000);
                    }
                }
            });
        });        
    });
    
    var container = document.querySelector('#lista-viagens');
    var msnry = new Masonry( container, {
      // options
      //columnWidth: 400,
      gutter: 0,
      itemSelector: '.box-viagem'
    });
</script>
        