<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${atividade.hospedagem}">
    <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
</c:if>

<c:if test="${atividade.voo}">
    <img src="<c:url value="/resources/images/plane.png" />" width="68" height="68" />
</c:if>

<c:if test="${atividade.alimentacao}">
    <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
</c:if>

<c:if test="${atividade.visitaAtracao}">
    <img src="<c:url value="/resources/images/hotel2.png" />" width="68" height="68" />
</c:if>

<c:if test="${atividade.aluguelVeiculo}">
    <img src="<c:url value="/resources/images/car.png" />" width="68" height="68" />
</c:if>
