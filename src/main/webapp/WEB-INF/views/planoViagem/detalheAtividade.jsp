<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="span5">
    <h3>
    
        <c:if test="${atividade.hospedagem}">
            ${atividade.descricaoAtividade}
        </c:if>

        <c:if test="${atividade.alimentacao}">
            ${atividade.descricaoAtividade}
        </c:if>
        
        <c:if test="${atividade.voo}">
            ${atividade.descricaoAtividade}
        </c:if>

        <c:if test="${atividade.visitaAtracao}">
            ${atividade.descricaoAtividade}
        </c:if>

        <c:if test="${atividade.aluguelVeiculo}">
            ${atividade.descricaoAtividade}
        </c:if>
        
        <c:if test="${atividade.longaDuracao}">
            <c:if test="${not atividadeView.termino}">
                <span class="label label-success">
                    ${atividade.descricaoInicio}
                </span>
            </c:if>
            <c:if test="${atividadeView.termino}">
                <span class="label label-important">
                    ${atividade.descricaoFim}
                </span>
            </c:if>
        </c:if>
    </h3>
    
    <div class="horizontalBar"></div>

    <div class="row">
        <div class="span6">
            
            <c:if test="${atividade.hospedagem}">
        
                <c:choose>
                    <c:when test="${atividadeView.termino}">
                        <div class="row">
                            <div class="span6">
                                <a href="#INICIO_${atividade.id}">
                                    Clique para ir para o dia/hora da chegada
                                    
                                    <c:choose>
                                        <c:when test="${viagem.periodoPorDatas}">
                                           (<joda:format value="${atividade.dataInicio}" style="S-" />)
                                        </c:when>
                                        <c:when test="${viagem.periodoPorQuantidadeDias}">
                                            (${atividade.diaInicio}º dia)
                                        </c:when>
                                        <c:otherwise>
                                            
                                        </c:otherwise>
                                    </c:choose>
                                    
                                </a>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
        
                        <div class="row">
                            <div class="span2">
                                <b>${atividade.descricaoLocal}</b>
                                <a href="#">
                                    ${atividade.enderecoLocal}
                                </a>
                            </div>
                            <div class="span2">
                                <div class="alert alert-warning">
                                    <h4 class="alert-heading green">${atividade.descricaoInicio}</h4>
                                    <span class="green">
                                        <joda:format value="${atividade.dataInicio}" style="S-" />
                                        <joda:format value="${atividade.horaInicio}" style="-S" />
                                    </span>
        
                                    <a href="#TERMINO_${atividade.id}" style="text-decoration: none;" title="Clique para ir para o dia da partida">
                                        <h4 class="alert-heading red">${atividade.descricaoFim}</h4>
                                        <span class="red">
                                            <joda:format value="${atividade.dataFim}" style="S-" />
                                            <joda:format value="${atividade.horaFim}" style="-S" />
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="span2">
                                <c:if test="${atividade.quantidadeQuartos != null}">
                                    ${atividade.quantidadeQuartos} quartos
                                </c:if>
                                <br/>
                                ${atividade.tipoQuartos}
                            </div>
                        </div>
                        
                        <div class="horizontalBar"></div>
                        
                        <div class="row">
                            <div class="span6">
                                <b>Hóspedes</b>
                                
                                <c:import url="listaParticipantesAtividade.jsp"></c:import>
                            </div>
                        </div>
                
                    </c:otherwise>
                </c:choose>
                                                
            </c:if>
        
            <c:if test="${atividade.voo}">
            
                <div class="row">
                    <div class="span6">
                        <h3>
                            ${atividade.trechoInicial.nomeCompanhia != null ? atividade.trechoInicial.nomeCompanhia : 'Companhia aérea não informada'}
                            - 
                            ${atividade.trechoInicial.codigo}
                        </h3>
                    </div>
                </div>
                
                <div class="horizontalBar"></div>
                
                <div class="row">
                    <div class="span6">
                        <div class="alert alert-warning">
                            <h4 class="alert-heading green">Partida</h4>
                            <span class="green">
                                ${atividade.trechoInicial.partida.nomeLocalEspecifico} <strong><joda:format value="${atividade.horaInicio}" style="-S" /></strong>
                            </span>
                            <h4 class="alert-heading red">Chegada</h4>
                            <span class="red">
                                ${atividade.trechoInicial.chegada.nomeLocalEspecifico} <strong><joda:format value="${atividade.horaFim}" style="-S" /></strong>
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="horizontalBar"></div>
                
                <div class="row">
                    <div class="span6">
                        <b>Passageiros</b>
                        
                        <c:import url="listaParticipantesAtividade.jsp"></c:import>
                    </div>
                </div>
                
            </c:if>
        
            <c:if test="${atividade.alimentacao}">
        
                <div class="row">
                    <div class="span2">
                        <b>${atividade.descricaoLocal}</b>
                        <a href="#">
                            ${atividade.enderecoLocal}
                        </a>
                    </div>
                    <div class="span2">
                        <div class="alert alert-warning">
                            <h4 class="alert-heading green">Data/Hora</h4>
                            <span class="green">
                                <joda:format value="${atividade.dataInicio}" style="S-" />
                                <joda:format value="${atividade.horaInicio}" style="-S" />
                            </span>
                        </div>
                    </div>
                    <div class="span2">
        
                    </div>
                </div>
                
                <div class="horizontalBar"></div>
                
                <div class="row">
                    <div class="span6">
                        <b>Participantes</b>
                        
                        <c:import url="listaParticipantesAtividade.jsp"></c:import>
                    </div>
                    
                </div>
                                                
            </c:if>
            
            <c:if test="${atividade.visitaAtracao}">
        
                <div class="row">
                    <div class="span2">
                        <b>${atividade.descricaoLocal}</b>
                        <a href="#">
                            ${atividade.enderecoLocal}
                        </a>
                    </div>
                    <div class="span2">
                        <div class="alert alert-warning">
                            <h4 class="alert-heading green">Data/Hora</h4>
                            <span class="green">
                                <joda:format value="${atividade.dataInicio}" style="S-" />
                                <joda:format value="${atividade.horaInicio}" style="-S" />
                            </span>
                        </div>
                    </div>
                    <div class="span2">
        
                    </div>
                </div>
                
                <div class="horizontalBar"></div>
                
                <div class="row">
                    <div class="span8">
                        <b>Participantes</b>
                        
                        <c:import url="listaParticipantesAtividade.jsp"></c:import>
                    </div>
                    
                </div>
                                                
            </c:if>
            
            <c:if test="${atividade.aluguelVeiculo}">
            
                <c:choose>
                    <c:when test="${atividadeView.termino}">
                        <div class="row">
                            <div class="span6">
                                <a href="#INICIO_${atividade.id}">
                                    Clique para ir para o dia/hora da retirada
                                    
                                    <c:choose>
                                        <c:when test="${viagem.periodoPorDatas}">
                                           (<joda:format value="${atividade.dataInicio}" style="S-" />)
                                        </c:when>
                                        <c:when test="${viagem.periodoPorQuantidadeDias}">
                                            (${atividade.diaInicio}º dia)
                                        </c:when>
                                        <c:otherwise>
                                            
                                        </c:otherwise>
                                    </c:choose>
                                    
                                </a>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>    
        
                        <div class="row">
                            <div class="span2">
                                <b>${atividade.descricaoLocal}</b>
                                <a href="#">
                                    ${atividade.enderecoLocal}
                                </a>
                            </div>
                            <div class="span2">
                                <div class="alert alert-warning">
                                    <h4 class="alert-heading green">Data/Hora</h4>
                                    <span class="green">
                                        <joda:format value="${atividade.dataInicio}" style="S-" />
                                        <joda:format value="${atividade.horaInicio}" style="-S" />
                                    </span>
                                </div>
                            </div>
                            <div class="span2">
                
                            </div>
                        </div>
                        
                        <div class="horizontalBar"></div>
                        
                        <div class="row">
                            <div class="span6">
                                <b>Motoristas</b>
                                
                                <c:import url="listaParticipantesAtividade.jsp"></c:import>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
        
            </c:if>
    
        </div>
    </div>
</div>    
