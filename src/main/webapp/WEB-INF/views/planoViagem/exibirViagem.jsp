<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">
    <%--tiles:putAttribute name="title" value="${restaurante.nome} - TripFans" /--%>

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <style>
            .city-header {
                border: 1px solid #E78F08;
                background: #F6A828;
                color: white;
                font-size: 0.70em;
                height: 35px;
                border-image: initial;
            }        
        </style>
            
        <div class="container-fluid">
            <div class="block">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${editar == true ? 'Editar' : ''} Viagem '${viagem.nome}'</h2>
                </div>
                
				<div class="block_content">
<%--                
					<div class="sidebar">
                    
                        <div style="padding-top: 15px; padding-left: 18px;">
                            Imagem da Viagem
                        </div>
                    
                        <div align="center">
                            <img id="profile_pic" class="moldura" src="http://localhost:8080/fanaticosporviagens/resources/images/default-trip-image.jpg" width="150" height="150">
                            <a class="btn btn-mini btn-warning"><i class="icon-refresh icon-white"></i> Trocar imagem</a>
                        </div>
                    
					</div>
 --%>					
					<div class="sidebar_content" id="pageContent">
                    
                        <c:url value="/planoViagem/salvarViagem" var="salvar_url" />
                        
                        <form:form id="viagemForm" action="${salvar_url}" modelAttribute="viagem">
                        
                            <input type="hidden" name="idDiario" value="${idDiario}"/>
                        
                            <%@include file="informacoesViagem.jsp" %>
                            
                            <div style="width: 100%">
                                <div id="error" style="display: none;" class="alert alert-success">
                                    <p id="errorMsg">
                                    </p>
                                </div>
                                <div id="success" style="display: none;" class="alert alert-success">
                                    <p id="successMsg">
                                    </p>
                                </div>
                                <div align="right">
                                    <div class="form-actions">
                                        <c:if test="${param.d != null}">
                                          <a id="botao-cancelar" href="#" class="btn btn-danger" data-loading-text="Cancelando...">
                                            <i class="icon-remove icon-white"></i>
                                            Cancelar
                                          </a>
                                        </c:if>
                                        <a id="botao-salvar" href="#" class="btn btn-success" data-loading-text="Salvando...">
                                            <i class="icon-ok icon-white"></i>
                                            Salvar alterações
                                        </a>
                                    </div>
                                </div>
                            </div>                                

                        </form:form>
                        
					</div>
				
				</div>
                
				<div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
		   </div>
		</div>
        
        <script type="text/javascript">
        
            $(document).ready(function () {
            	$('.remover').bind('click', function(event) {
		  	      	event.preventDefault();
		  	       var idCidade = $(this).attr('id');
		  	       $('#cidade-' + idCidade).remove();
		  	   });
            	
            	$('.collapsible').collapse();
            	
                $('.helper').tooltip();
                
                $('#botao-salvar').button();
                
                $('#botao-salvar').bind('click', function (event) {
                	event.preventDefault();
                	$('#viagemForm').submit();
                });
                
                <c:if test="${param.d != null}">

                $('#botao-cancelar').button();
                
                $('#botao-cancelar').bind('click', function (event) {
                    event.preventDefault();
                    $(location).attr('href', '<c:url value="/planoViagem/editarDiario/${param.d}" />');
                });
                
                </c:if>
                
            	/*$('.collapsible').on('hide', function () {
            		var item = $(this).find('.icon-chevron-down');
            		item.removeClass('icon-chevron-down');
            		item.addClass('icon-chevron-up');
            	})*/
            	
            	$('#dataInicio').datepicker( {
                    changeMonth: true,
                    changeYear: true
                });
            	
            	$('#dataFim').datepicker( {
                    changeMonth: true,
                    changeYear: true
                });
                
                $('input[name="tipoPeriodo"]').bind('change', function () {
                    var radio = $(this);
                    $('#dataInicio').attr('disabled', (radio.val() != 'POR_DATAS'));
                    $('#dataFim').attr('disabled', (radio.val() != 'POR_DATAS'));
                    if (!$('#dataInicio').is(':disabled')) {
                    	$('#dataFim').focus();
                    }
                    
                    $('#quantidadeDias').attr('disabled', (radio.val() != 'POR_QUANTIDADE_DIAS'));
                    if (!$('#quantidadeDias').is(':disabled')) {
                        $('#quantidadeDias').focus();
                    }
                    
                });
                
                $('input[name="tipoPeriodo"]').bind('click', function () {
                    $(this).change();
                });
                
                var viagemForm = $('#viagemForm');
                
                viagemForm.unbind("submit");
                
                viagemForm.submit(function(event) {
                	configurarDestinos();
                    
                    if (!viagemForm.valid()) {
                        /* stop form from submitting normally */
                        event.preventDefault();
                    }                    
                    
                });
                
                viagemForm.validate({ 
                    rules: {
                        nome: { 
                            required: true, 
                            maxlength : 100
                        }
                    }, 
                    messages: { 
                    	nome: {
                            required : "<s:message code="validation.usuario.nome.required" />"
                        }
                    }
                });
            });
            
        </script>        
		
	</tiles:putAttribute>

</tiles:insertDefinition>