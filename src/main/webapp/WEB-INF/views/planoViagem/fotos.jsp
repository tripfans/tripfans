<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${empty fotos}">
    <div style="height: 50px;"></div>
    <div class="blank-state">
        <div class="row">
            <div class="span1 offset1">
                <img src="<c:url value="/resources/images/no_photo.png" />" />
            </div>
            <div class="span3 offset1">
                <h3>N�o h� fotos para exibir</h3>
                <p>
                    Este usu�rio ainda n�o adicionou fotos ao seu perfil.
                </p>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${not empty fotos}">

	<div class="page-header">
    <h3>Fotos da Viagem - "Lembrar de melhorar essa visualiza��o"</h3>
    </div>
    
    <ul class="media-grid">
    
    <c:set var="atividade" value="0" scope="request" />
    <c:forEach items="${fotos}" var="item" varStatus="rowCounter">
    
    	<c:if test="${item.atividade.id != atividade}">
    		<span> Local da Atividade ${rowCounter.count}: ${item.atividade.nomeLocal}</span>
    	</c:if>
        <li>
            <a href="${item.urlNormal}" class = "fancybox" rel="slideShow" title="teste">
                <img src="${item.urlCarrosel}" class="thumbnail" width="134" height="94">
            </a>
        </li>
		<c:set var="atividade" value="item.atividade.id" scope="request" />        
    </c:forEach>
    
    </ul>
    
    <script>
        $("a[rel=slideShow]").fancybox({
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type              : 'image',
            speedIn: 400, 
            speedOut: 200, 
            overlayShow: true,
            overlayOpacity: 0.1,
            hideOnContentClick: true,
            'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
            },
            titlePosition: 'inside'
        });
    </script>
    
</c:if>