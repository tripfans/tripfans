<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.SituacaoViagem"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.TipoPeriodoViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:url value="/planoViagem/salvarFoto" var="url_salvarFoto" />
<c:url value="/planoViagem/carregarFoto" var="url_carregarFoto" />
        
<input type="hidden" name="viagem" value="${viagem.id}" />
                            
<fieldset>
    
    <legend>
        <h3>Informa��es b�sicas da viagem</h3>
    </legend>
    
    <div>
        
        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
            <li>
                <label class="span4 fancy-text">
                    <span class="required">
                        Nome da Viagem
                    </span>
                    <i id="help-nome" class="icon-question-sign helper" title="Informe um nome para identificar esta viagem"></i>
                </label>
                <form:input path="nome" cssClass="span8 required" />
            </li>
            <li class="hide error">
                <label class="span4"></label>
                <div class="span6 error-message"></div>
            </li>
            <li>
                <label class="span4 fancy-text">
                    Uma breve descri��o da Viagem 
                    <i id="help-descricao" class="icon-question-sign helper" title="Escreva algo sobre esta viagem (opcional)"></i>
                </label>
                <form:textarea path="descricao" cssClass="span8" rows="4" />
            </li>
            <li>
                <label class="span4 fancy-text">
                    Imagem da Viagem
                </label>
                <span id="foto-viagem-container" class="span14 ${empty viagem.fotoIlustrativa ? 'hide' : ''}" style="margin-left: 0;">
                    <i class="carrossel-thumb borda-arredondada" style="background-image: url('<c:url value="${viagem.fotoIlustrativa.urlAlbum}"/>'); height: 130px; width: 170px; margin-bottom: 5px;"></i>
                    <!-- <img class="borda-arredondada" src="http://localhost:8080/tripfans/resources/images/default-trip-image.jpg" width="150" height="150" style="margin-bottom: 5px;"> -->
                </span>
                <div class="row" style="margin-left: 0;">
                    <div id="foto-viagem-spacer" class="span4 fancy-text ${empty viagem.fotoIlustrativa ? 'hide' : ''}">&nbsp;</div>
                    <%-- Div necess�ria para funcionar o file-uploader (o bot�o de selecionar arquivo � renderizado aqui) --%>
                    <div id="file-uploader" class="span4" style="margin: 0px; max-width: 150px;"></div>
                </div>
            </li>
            <li>
                <label class="span4 fancy-text">
                     <span>
                         Quem pode ver esta Viagem?
                     </span>
                </label>
                <div class="span6" style="margin-left: 0;">
                    <fan:privButton name="visibilidade" id="btnVisibilidade" defaultValue="<%=TipoVisibilidade.PUBLICO %>"
                                    dropDownItemStyle="min-height: 20px; padding: 0px;"
                                    items="<%= new TipoVisibilidade[] {TipoVisibilidade.PUBLICO, TipoVisibilidade.AMIGOS, TipoVisibilidade.SOMENTE_EU} %>" />
                </div>
            </li>
        </ul>

<%--         <div class="control-group">
            <div class="controls">
               <label class="checkbox">
                   <form:checkbox id="criadorViaja" path="criadorViaja" value="true" />
                   <span>Sou um dos viajantes</span>
               </label>
            </div>
        </div>
 --%>      
    </div>
                  
</fieldset>

<%@include file="periodoViagem.jsp" %>

<%@include file="destinos.jsp" %>

<script>

function createUploader() {

    // Configurar o File-uploader 
    var uploader = new qq.FileUploader({
        element: document.getElementById('file-uploader'),
        action: '${url_salvarFoto}',
        allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
        multiple: false,
        onSubmit: function(id, fileName) {
            //$(".edicaoFoto").css("display", "none");
            //$(".remocaoFoto").css("display", "none");
        },
        onProgress: function(id, fileName, loaded, total) {
            //$(".fotoPerfil").html('<img id="profile_pic" src="<c:url value="/resources/images/loading.gif" />" />');
        },
        onComplete: function(id, fileName, responseJSON) {
            $('.qq-upload-list').html('');
            if (responseJSON.params != null && responseJSON.params.urlFoto != null) {
                $('#foto-viagem-container').html('<img class="borda-arredondada" src="' + responseJSON.params.urlFoto + '" width="150" height="150" style="margin-bottom: 5px;">');
                $('#foto-viagem-spacer').show();
                $('#foto-viagem-container').show();
            }
        },
        onCancel: function(id, fileName) {
            $('.qq-upload-list').html('');
        },
        template: '<div class="qq-uploader">' +
                      '<div class="qq-upload-button btn btn-warning"> <i class="icon-picture icon-white"></i> ${viagem.fotoIlustrativa != null ? 'Trocar' : 'Adicionar'} foto</div>' +
                      '<ul class="qq-upload-list unstyled"></ul>' +
                  '</div>',
        fileTemplate:  '<li>' +
                          '<span class="qq-upload-file"></span>' +
                          '<span class="qq-upload-spinner"></span>' +
                          '<span class="qq-upload-size"></span>' +
                          '<a class="qq-upload-cancel label important" href="#">Cancelar</a>' +
                          '<span class="qq-upload-failed-text">Falhou</span>' +
                       '</li>'
    });
    
}

window.onload = createUploader;

</script>