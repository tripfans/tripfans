<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<tiles:insertDefinition name="fanaticos.default">
    <%--tiles:putAttribute name="title" value="${restaurante.nome} - TripFans" /--%>

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
        <script type="text/javascript">
        
        </script>
    </tiles:putAttribute>

    <tiles:putAttribute name="body">
            
        <div class="container-fluid">
            <div class="block">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <c:choose>
                        <c:when test="${usuario != null and usuario.id == requestParam.id}">
                            <h2>Minhas Viagens</h2>
                            
                            <div class="pull-right">
                                <a href="#" class="btn btn-mini btn-warning" title="Importar plano de viagens">
                                    <i class="icon-star icon-white"></i>
                                    <b>Criar viagem</b>
                                </a>
                                
                                <a href="#" class="btn btn-mini btn-warning" title="Importar plano de viagens">
                                    <i class="icon-star icon-white"></i>
                                    <b>Importar</b>
                                </a>
                            </div>
                            
                        </c:when>
                        <c:otherwise>
                            <h2>Viagens de ${perfil.displayName}</h2>
                        </c:otherwise>
                    </c:choose>
                </div>
			
				<div class="block_content">
					<!--div class="sidebar">
						<ul class="sidemenu">
                            <li>
                                <a id="principal" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    Planeje sua viagem
                                </a>
                            </li>
                            <li>
                                <a id="principal" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    Passadas
                                </a>
                            </li>
                            <li>
                                <a id="principal" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    Planejadas
                                </a>
                            </li>
						</ul>
					</div-->
					
					<div class="sidebar_content" id="pageContent">
						<%@include file="listaViagens.jsp" %>
					</div>
				
				</div>
                
				<div class="bendl">
                </div>
                <div class="bendr">
                </div>
			
		   </div>
		</div>
		
	</tiles:putAttribute>

</tiles:insertDefinition>