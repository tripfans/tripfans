<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<script>
//$(function() {
    
    $('.draggable').mouseover(function() {
    	var $item = $(this);
    	$item.css('cursor', 'move');
        //item.find( '.drag-to-cart' ).show();
    });

    $('.draggable').mouseout(function() {
        var $item = $(this);
        $item.css('cursor', 'pointer');
        //item.find( '.drag-to-cart' ).hide();
    });
    
    $('.droppable').mouseover(function() {
    	var $item = $(this);
    	
    	//clearTimeout($('#flip').data('timeout'));
    	
    	//item.css('background-color', '#f11');
    	//item.toggleClass('alert-success');
    	
    	//$item.addClass('active')
    	
    	var timeout = setTimeout(function() { 
    		$item.click();
        }, 400);
    	//$('#flip').data('timeout', timeout);
    });

    $('.droppable').mouseout(function() {
    	var item = $(this);
    	//clearTimeout($('#flip').data('timeout'));
    	//item.toggleClass('alert-success');
    	
        //$(this).css('background-color', 'transparent');
    });

    $('.droppable').droppable({
        accept: '.painelAtracoes > .draggable',
        //activeClass: 'active',
        hoverClass: 'over',
        tolerance: 'pointer',
        drop: function( event, ui ) {
        	adicionarAtividade( ui.draggable , $(this).attr('data-numero-dia'));
        },
        activate: function(event, ui) {
        	$(this).addClass('active');
        },
        deactivate: function(event, ui) {
            $(this).removeClass('active');
        },
        /*over: function(event, ui) {
            $(this).removeClass('over');
        },
        out: function(event, ui) {
            $(this).removeClass('over');
        },*/
    });
    
    // painel de atrac�es e painel do dia-a-dia
    var //$painelAtracoes = $('#painelAtracoes'),
        $painelDias = $('#painelDias');
	
    // fazendo os items do painel de atracoes serem arrastaveis
    $( '.draggable', $('.painelAtracoes') ).draggable({
        cancel: 'a.ui-icon', // clicking an icon won't initiate dragging
        revert: 'invalid', // quando for largada fora, o item retorna para sua posi��o incial
        //containment: $( '#demo-frame' ).length ? '#demo-frame' : 'document', // stick to demo-frame if present
        helper: 'clone',
        cursorAt: { top: 30, right: -30 },
        /*helper: function (event) {
            return $('<div class=""></div>'); 
        },*/
        opacity: 0.90,
        cursor: 'move'
    });
    
    $('.remover-atividade').bind('click', function(event) {
        excluirAtividade($('#atividade-' + $(this).attr('data-id-atividade')));
    });
    
    // adicionar atividade no painel de atividades
    function adicionarAtividade( $item , dia) {

        // var $painelAlvo = $('#tab-dia-' + dia).find('ul.vertical-carousel-list');
        var $painelAlvo = $('#painel-atividades').find('ul.vertical-carousel-list');
            
        // Recupera o id do local do item arrastado
        var idLocal = $item.attr('data-id');
        
        var url = '<c:url value="/planoViagem/${viagem.id}/wizard/incluirAtividade" />';
        
        $.ajax({
            type: 'POST',
            url: url,
            data : {
            	local : idLocal,
            	//dia : dia
            },
            success: function(data) {
                if (data.error != null) {

                } else {
                    var params = '';
                    if (data.params != null) {
                        params = data.params;
                    }
                    if (params.success) {
                    	// Recupera o id da Atividade incluida
                    	var idAtividade = params.idAtividade;
                    	
                        var nomeLocal = $item.find('.card-name').text();
                        var fotoDiv = $item.find('.card-photo').html();
                        
                    	// Monta o link para remover a atividade
                    	var linkRemover = '<a id="remover-atividade-' + idAtividade +'" data-id-atividade="' + idAtividade + '" href="#" class="remover-atividade"><i class="icon-trash"></i></a>'; 
                    	
                    	// Cria um novo item na lista de atividades
                    	var itemAtividade = '';
                    	
                	    itemAtividade += '<li id="atividade-' + idAtividade + '" class="item draggable hide" style="height: 70px;">';
                	    itemAtividade += '    <div class="conteudo">';
                	    itemAtividade += '        <div class="foto span7">';
                	    itemAtividade += fotoDiv;
                	    itemAtividade += '        </div>';
                	    
                	    itemAtividade += '        <div class="texto">';
                	    /*itemAtividade += '            <p class="hora">';
                	    itemAtividade += '                <i class="icon-time"></i>';
                	    itemAtividade += '            </p>';*/
                	    itemAtividade += '            <a href="#">';
                	    itemAtividade += '                <strong>';
                	    itemAtividade += nomeLocal;
                        itemAtividade += '                </strong>';
                        itemAtividade += '            </a>';
                	    itemAtividade += '        </div>';
                        
                	    itemAtividade += '       <div class="acoes">';
                        itemAtividade += '           <i class="icon-arrow-up"></i>';
                	    itemAtividade += '           <i class="icon-arrow-down"></i>';
                        itemAtividade += linkRemover
                	    itemAtividade += '        </div>';
                	    itemAtividade += '    </div>';
                    
                	    itemAtividade += '</li>';
                	    
                	    $painelAlvo.append(itemAtividade);
                	    
                	    // Exibir com fade
                	    $('#atividade-' + idAtividade + '').fadeIn(function() {
                	    	itemAtividade.show();
                	    });
                        
                    	/*var statusLocal = $item.find('.local-status');
                        // Somando + 1 a contagem de quantas vezes o lugar foi incluido
                        var quantidade = parseInt(statusLocal.attr('data-quantidade-vezes'));
                    	statusLocal.attr('data-quantidade-vezes', quantidade + 1);
                        // Exibe o label de que o local foi incluido na lista
                    	statusLocal.show();*/
                        
                        // adicionar evento ao link de remover atividade
                    	$('#remover-atividade-' + idAtividade).bind('click', function(event) {
                            excluirAtividade($('#atividade-' + idAtividade));
                        });
                    }
                    
                }
            }
        });

    }
    
    // remover atracao do painel do dia-a-dia
    function excluirAtividade( $item ) {

    	var url = '<c:url value="/planoViagem/${viagem.id}/wizard/excluirAtividade" />';
    	
        $.ajax({
            type: 'POST',
            url: url,
            data : {
                atividade : $item.attr('data-id-atividade')
            },
            success: function(data) {
                if (data.error != null) {

                } else {
                    var params = '';
                    if (data.params != null) {
                        params = data.params;
                    }
                    
                    if (params.success) {
                    	
                        // Remove o local na lista de atividades
                        
                        $item.fadeOut(function() {
                            $item.remove();
                        });
                        
                    	// Recuperar o elemento referente ao local da atividade
                    	var localPane = $('local-' + idLocal);
                    	
                    	// Recuperar o "status"
                    	var statusLocal = localPane.find('.local-status');
                        // Subtrai 1 da contagem de quantas vezes o lugar foi incluido
                        var quantidade = parseInt(statusLocal.attr('data-quantidade-vezes')) - 1;
                        // Esconde o label de que o local foi incluido na lista, caso a quantidade seja menor que 1
                        if (quantidade == 0) {
                        	statusLocal.hide();
                        }
                        statusLocal.attr('data-quantidade-vezes', quantidade);
                        
                    }
                    
                }
            }
        });
        
    }
    
//});
</script>
<div>

    <ul class="painelAtracoes ui-helper-reset ui-helper-clearfix" style="height: 300px; z-index: 9000;">

        <c:forEach items="${locais}" var="local">
        
            <fan:card type="local" itemId="${local.id}" name="${local.nome}" photoUrl="${local.urlFotoSmall}" 
                      itemUrl="/locais/${local.urlPath}" showActions="true" nameStyle="font-size : 16px;"
                      selectable="false" spanSize="10" imgSize="10" cardClass="draggable" cardStyle="z-index: 9000;" width="100" height="75" >
            
                <jsp:attribute name="info">
                
                    <c:if test="${local.notaGeral != null}">
                        <div class="local-box-details">
                            <div class="span6" style="display: inline-block;">
                                <span class='starsCategoryScaled cat${local.classificacaoGeral}'>&nbsp;
                                </span>
                                <span>
                                    <a href="#">
                                        ${local.quantidadeAvaliacoes} Avalia��es
                                    </a>
                                </span>
                            </div>
                            <div class="span6">

                            </div>
                        </div>
                    </c:if>
                    
                    <c:if test="${local.quantidadeRecomendacoes != null}">
                    
                        <div class="local-box-details">
                            <div class="span10">
                                ${local.quantidadeAmigosIndicam} amigos indicam
                                <%-- <fan:showPopoverCard url="/amigos/consultarInteressesLocal/${local.id}?tipoInteresse=INDICA" linkText="${local.quantidadeAmigosIndicam} amigos indicam"/> --%>
                            </div>
                            <div class="span10" style="margin-left: 0px;">
                                ${local.quantidadeAmigosIndicam} amigos acham imperd�vel
                                <%-- <fan:showPopoverCard url="/amigos/consultarInteressesLocal/${local.id}?tipoInteresse=IMPERDIVEL" linkText="${local.quantidadeAmigosIndicam} amigos acham imperd�vel"/> --%>
                            </div>
                        </div>
    
                    </c:if>
    
                    <span class="label label-success local-status" data-quantidade-vezes="0" style="display : none;">
                        Inclu�do
                    </span>
                
                </jsp:attribute>
                
                <jsp:attribute name="actions">
                    <div style="border-bottom: 1px solid #eee; height: 2px; width: 100%;"></div>
                    <div class="pull-right" style="padding-right: 6px;">
                        <a class="btn btn-mini btn-info action-button dropdown-toggle" data-toggle="dropdown" href="#">
                            Adicionar ao plano
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                              <c:forEach items="${viagem.diasViagem}" var="diaViagem">
                                <a href="#">
                                    ${diaViagem.numero}� dia - ${diaViagem.diaSemanaCurtoDiaMesComMesCurto}
                                </a> 
                              </c:forEach>
                            </li>
                        </ul>
                    </div>
                </jsp:attribute>
                
            </fan:card>            
        
            <%--li id="local-${local.id}" class="local-box draggable span5" data-local-id="${local.id}" data-local-category="" style="z-index: 9000;">
        
                <div class="tool-tips">
                    <span class="drag-to-cart">Arraste para o dia desejado</span>
                </div>
                                
                <div class="local-box-content">
                
                    <table cellpadding="5">
                        <tr>
                            <td>
                                <img src="<c:url value="/resources/images/no_photo.png" />" width="50" height="50" />
                            </td>
                            <td valign="top">
                                <span style="vertical-align: top;">
                                    <p>
                                        <a href="#" target="_blank">
                                            ${local.nome}
                                        </a>
                                    </p>
                                </span>
                            </td>
                        </tr>
                    </table>
                    
                    <!-- a href="#" target="_blank">Ver endere�o</a-->
                </div>
                
                <c:if test="${local.notaGeral != null}">
                    <div class="local-box-details">
                        <div class="span1">
                            <h1 class="blue">
                                ${local.notaGeral} 
                            </h1>
                        </div>
                        <div class="span1">
                            <a href="#">
                                ${local.quantidadeAvaliacoes} Avalia��es
                            </a>
                        </div>
                    </div>
                </c:if>
                
                <c:if test="${local.quantidadeRecomendacoes != null}">
                
                    <div class="local-box-details">
                        <div class="span1">
                            <fan:showPopoverCard url="/amigos/consultarInteressesLocal/${local.id}?tipoInteresse=INDICA" linkText="${local.quantidadeAmigosIndicam} amigos indicam"/>
                        </div>
                        <div class="span1">
                            <fan:showPopoverCard url="/amigos/consultarInteressesLocal/${local.id}?tipoInteresse=IMPERDIVEL" linkText="${local.quantidadeAmigosIndicam} amigos acham imperd�vel"/>
                        </div>
                    </div>

                </c:if>

                <span class="label label-success local-status" data-quantidade-vezes="0" style="display : none;">
                    Inclu�do
                </span>
            
            </li --%>
        
        </c:forEach>

    </ul>
    
</div>

<div class="centerAligned">

    <c:url value="${urlConsulta}" var="url_mais_locais" />

    <fan:pagingButton url="${url_mais_locais}" cssClass="span20"
                      targetId="${param.filtroLocais}" title="Carregar mais locais..." limit="6"
                      noMoreItensText="N�o h� mais locais" />
        
</div>
