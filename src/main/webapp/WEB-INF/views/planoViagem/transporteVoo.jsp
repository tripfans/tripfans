<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>


<script type="text/javascript">

	jQuery(document).ready(function() {
		
	    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
	    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
	    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
		
		$('input:text').setMask();
		
		$('#botaoSalvar').bind('click', function(event) {
		    event.preventDefault();
		    //$('#vooForm').submit();
		});
		
	 	$('#vooForm').initToolTips();

	 	$('#observacoes').stickySidebar();
	 	
		$('#adicionar-trecho').bind('click', function(event) {
			
                  event.preventDefault();
                  var indice = $(this).attr('indice-trecho');
                  var quantidadeDias = $(this).attr('quantidade-dias');
                  var tipoPeriodo = $(this).attr('tipo-periodo');
                  
                  $('#lista-trechos').append('<div id="trecho-' + indice + '-div"></div>');
                  
                  $('#trecho-' + indice + '-div').load(
                      '<c:url value="/planejamento/voo/adicionarTrecho"/>',
                      {
                          'indice': indice,
                          'quantidadeDias' : quantidadeDias, 
                          'tipoPeriodo' : tipoPeriodo
                      }
                  );
                  
                  $(this).attr('indice-trecho', parseInt(indice) + 1);
                  
              });
	 	
	});
	
	function removerTrecho(indice) {
	    //event.preventDefault();
	    // Esconder os dados do trecho excluido
	    $('#div-trecho-' + indice).hide();
	    // Alternar para o botao desfazer
	    $('#remover-trecho-' + indice).hide();
	    $('#desfazer-remover-trecho-' + indice).show();
	    // Exibir status do trecho
	    $('#status-trecho-' + indice).show();
	}
	
	function desfazerRemoverTrecho(indice) {
	    //event.preventDefault();
	    // Exibir os dados do trecho
	    $('#div-trecho-' + indice).show();
	    // Alternar para o botao desfazer
	    $('#remover-trecho-' + indice).show();
	    $('#desfazer-remover-trecho-' + indice).hide();
	    // Esconder status do trecho
	    $('#status-trecho-' + indice).hide();
	}

</script>

    
<c:url value="/planejamento/salvarVoo" var="salvar_url" />

<form:form id="vooForm" action="${salvar_url}" modelAttribute="voo" method="post">

    <input type="hidden" name="voo" value="${voo.id}" />
    <input type="hidden" name="viagem" value="${viagem.id}" />
    
    <div class="row">
    
        <s:hasBindErrors name="voo">
            <div id="errorBox" class="alert alert-error span6"> 
                <span class="large"><s:message code="validation.containErrors" /></span> 
            </div>
        </s:hasBindErrors>
        <div id="errorBox" class="alert alert-error span6" style="display: none;"> 
            <span class="large"><s:message code="validation.containErrors" /></span>
        </div>
    
    </div>
    
	<fieldset>
		<div class="page-header"><h3>Trechos do Voo</h3></div>
        <div id="lista-trechos">
    
            <c:forEach items="${voo.trechos}" var="trecho" varStatus="counter">

                <jsp:include page="/WEB-INF/views/planoViagem/trechoVoo.jsp">
                    <jsp:param name="indice" value="${counter.index}" />
                    <jsp:param name="quantidadeDias" value="${viagem.quantidadeDias}" />
                    <jsp:param name="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}" />
                </jsp:include>
                <c:set var="indice" value="${counter.index}"></c:set>
                <c:set var="quantidadeDias" value="${viagem.quantidadeDias}"></c:set>
                <c:set var="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}"></c:set>
                
            </c:forEach>
    
        </div>
        
        <a id="adicionar-trecho" class="btn btn-primary btn-mini" href="#" indice-trecho="${indice + 1}" tipo-periodo="${tipoPeriodo}" quantidade-dias="${quantidadeDias}">
            <i class="icon-plus icon-white"></i> Adicionar novo trecho
        </a>
	</fieldset>
    
    <div class="horizontalSpacer"></div>
    
    <div class="" align="right">
        <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
        <a id="botaoCancelarVoo" class="botaoCancelarTransporte btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${hospedagem.id}" />">
            <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
        </a>                                <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
        <a id="botaoSalvar" class="btn btn-primary" href="#">
            <i class="icon-ok icon-white"></i> Salvar Informações do Voo
        </a>
    </div>
    
</form:form>
