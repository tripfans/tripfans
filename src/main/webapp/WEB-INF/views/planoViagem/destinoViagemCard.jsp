<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<fan:card id="card-local-${local.id}" type="local" local="${local}" showActions="true" spanSize="4" imgSize="4">
	<jsp:attribute name="actions">
        <div style="border-bottom: 1px solid #eee; height: 2px; width: 100%;"></div>
        <div class="pull-right" style="padding: 6px;">
           <a id="${local.id}" class="remover" href="#"><i class="icon-trash helper" title="Remover este destino"></i></a>
        </div>
   </jsp:attribute>	
</fan:card>