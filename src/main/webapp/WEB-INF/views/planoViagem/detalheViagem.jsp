<%@page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.SituacaoViagem"%>
<%@page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.TipoPeriodoViagem"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<tiles:insertDefinition name="fanaticos.default">
    <%--tiles:putAttribute name="title" value="${restaurante.nome} - TripFans" /--%>

    <tiles:putAttribute name="stylesheets">
    
        <!-- Necessário para o Facebook Sharer -->
        <meta property="og:title" content="Plano de viagem para " /> 
        <meta property="og:description" content="Clique e veja todos os detalhes desta viagem" /> 
        <meta property="og:image" content="http://localhost:8080/fanaticosporviagens/resources/images/default-trip-image.jpg" />
        
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">    
        <script type="text/javascript">
        
            $(document).ready(function () {

                $('a.menu-item').bind('click',function(event) {
                    $('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    $(this).parent().addClass('active'); //Set clicked link class to active
                    carregarPagina(this.id);
                });
            	
            	var tab = '${param.tab}';
                if (tab !== '' ) {
                    $('#' + tab).click();
                } else {
                    $('#atividades').click();
                }
                
            });
            
            function carregarPagina(pagina) {
                $("#pageContent").load('<c:url value="/planoViagem/" />' + pagina + '?id=${viagem.id}');
            }
            
        </script>
        
    </tiles:putAttribute>
        
    <tiles:putAttribute name="body">
            
        <div class="container-fluid">
            <div class="block withsidebar">         
                <div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>'${viagem.nome}'</h2>
                </div>
			
				<div class="block_content">
                
					<div class="sidebar">
                    
                        <ul class="sidemenu">
                        
                            <li>
                                <a id="companheiros" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/group.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Companheiros de viagem</span>
                                </a>
                            </li>

                            <li>
                                <a id="meusDestinos" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/world.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Destinos</span>
                                </a>
                            </li>

                            <li>
                                <a id="atividades" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/group.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Atividades</span>
                                </a>
                            </li>

                            <li>
                                <a id="dicasImportadas" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/comments.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Dicas importadas</span>
                                </a>
                            </li>

                            <li>
                                <a id="diario" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/report_edit.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Diário de viagem</span>
                                </a>
                            </li>

                            <li>
                                <a id="minhasDicas" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/user_comment.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Minhas dicas</span>
                                </a>
                            </li>
                            
                            <li>
                                <a id="fotos" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/camera.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Fotos</span>
                                </a>
                            </li>

                            <li>
                                <a id="videos" href="#" class="menu-item">
                                    <span style="background-image: url('<c:url value="/resources/images/icons/film.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                                    <span>Vídeos</span>
                                </a>
                            </li>

                        </ul>                    
					</div>
					
					<div class="sidebar_content" id="pageContent">
					</div>
				
				</div>
                
				<div class="bendl">
                </div>
                <div class="bendr">
                </div>
                
		   </div>
		</div>
        
        <script type="text/javascript">
        
            $(document).ready(function () {
                
            });
            
        </script>        
		
	</tiles:putAttribute>

</tiles:insertDefinition>