<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<script src="http://connect.facebook.net/en_US/all.js"></script>

<script>
$(document).ready(function() {
    //$('.action-button').button('complete');
    
    $('.send-message').click(function (e) {
        e.preventDefault();

        var idFacebook = $(this).attr('data-user-facebook-id');
        
        facebook_send_message(idFacebook);
    });
    
    $('.post-to-feed').click(function (e) {
        e.preventDefault();

        var idFacebook = $(this).attr('data-user-facebook-id');
        
        facebook_post_to_feed('', idFacebook);
    });

    $('.card').click(function (e) {
        var $card = $(this);
        //$card.find('.card-check').click();
    });
    
    $('.card-check').click(function (e) {
        var $check = $(this);
        if ($check.is(':checked')) {
            $check.parents('div.card').addClass('card-selected');
        } else {
            $check.parents('div.card').removeClass('card-selected');
        }
    });
    
    $('#select-all').click(function (e) {
        if ($(this).attr('class').indexOf('active') === -1) {
            $('.card-check:visible').attr('checked', true).parents('div.card').addClass('card-selected');
        } else {
            $('.card-check').attr('checked', false).parents('div.card').removeClass('card-selected');
        }
    });
    
    $('#filtro-nome-amigos').keyup(function(e) {
        //if (e.keyCode == 13) {}
        var text = $(this).val();
        var regex = new RegExp(text, "i")
        $('.card').each(function() {
            var $card = $(this);
            if (! $card.attr('data-name').match(regex)) {
                $card.hide();
            } else {
                $card.show();
            }
        })
    });
    
    $('.modal').modal({
        show: false
    });

    $('#link-convidar-modal').click(function (e) {
        e.preventDefault();
        $('#convidar-modal').modal('toggle');
   });
    
    $('#link-solicitar-dicas-modal').click(function (e) {
    	 $('#solicitar-dicas-modal').modal('toggle')
    });
    
    // fix sub nav on scroll
    var $win = $(window)
        , $nav = $('.subnav')
        , navTop = $('.subnav').length && $('.subnav').offset().top - 40
        , $alphabetList = $('#alphabet-list')
        , isFixed = 0;
        
    //processScroll();
    
    // hack sad times - holdover until rewrite for 2.1
    $nav.on('click', function () {
        if (!isFixed) setTimeout(function () { $win.scrollTop($win.scrollTop() - 47) }, 10)
    });
    
    //$win.on('scroll', processScroll);
    
    function processScroll() {
        var i, scrollTop = $win.scrollTop();
        if (scrollTop >= navTop && !isFixed) {
            isFixed = 1
            $nav.addClass('subnav-fixed');
            $alphabetList.addClass('ordenacao-alfabetica-fixed');
        } else if (scrollTop <= navTop && isFixed) {
            isFixed = 0;
            $nav.removeClass('subnav-fixed');
            $alphabetList.removeClass('ordenacao-alfabetica-fixed');
        }
    } 
    
});

//$('.subnav').scrollspy();

//$('.action-button').button('loading');
//$('.action-button').button('complete');

// assume we are already logged in
FB.init({appId: '${tripFansEnviroment.facebookClientId}', xfbml: true, cookie: true});

function facebook_send_message(to) {
    
    FB.ui({
        app_id:'${tripFansEnviroment.facebookClientId}',
        method: 'send',
        name: 'Junte-se ao TripFans',
        link: 'http://ec2-50-19-46-111.compute-1.amazonaws.com:8080/tripfans',
        to: to,
        description: 'Texto que a gente tem que bolar para colocar aqui e chamar a aten��o dos usu�rios'
        //picture: ,
        //redirect_uri: ,
    });
    
}

function facebook_post_to_feed(from, to) {

    // calling the API ...
    var obj = {
        app_id:'${tripFansEnviroment.facebookClientId}',
        method: 'feed',
        link: 'http://ec2-50-19-46-111.compute-1.amazonaws.com:8080/tripfans',
        from : '100002024342727',
        to : to,
        //picture: 'http://fbrell.com/f8.jpg',
        name: 'Junte-se ao TripFans',
        //caption: 'Reference Documentation',
        description: 'Texto que a gente tem que bolar para colocar aqui e chamar a aten��o dos usu�rios.'
    };

    function callback(response) {
        //document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
    }

    FB.ui(obj, callback);
}

</script>
<div class="accordion-group">

    <div class="accordion-heading collapsible">
        <span class="accordion-toggle">
            Companheiros de Viagem
            <div class="header-actions">
                <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#grupo-amigos"></i>
            </div>
        </span>
    </div>
    
    <div id="grupo-amigos" class="accordion-body collapse in" style="height: auto; ">
    
        <div class="accordion-inner"> 
        
            <div class="subnav">

                <ul class="nav nav-pills">
                    <!-- li class="active">
                        <a href="#javascript">TripFans</a>
                    </li>
            
                    <li >
                        <a href="#javascript">Facebook</a>
                    </li-->
                    
                    <li>
                        <c:if test="${not empty amizades}">
                            <div class="left" style="margin-bottom: 9px; margin-top: 5px;">
                                <div class="input-append">
                                    <input id="filtro-nome-amigos" class="filtro-nome-local span4" placeholder="Procure por seus amigos" />
                                    <span class="add-on"><i class="icon-search"></i></span>
                                </div>
                            </div>
                        </c:if>
                    </li>
            
                    <li>
                        <div class="left" style="margin: 10px; font-size: 11px;">
                            Com selecionados:
                        </div>
                    </li>
            
                    <li>
                        <div class="btn-toolbar" style="margin-bottom: 9px">
                        
                            <a id="link-convidar-modal" class="btn btn-mini btn-info action-button">
                                Convidar
                            </a>
                            
                            <a id="link-solicitar-dicas-modal" class="btn btn-mini btn-info action-button">
                                Solicitar dicas
                            </a>
                            
                        </div>
                    </li>
            
                </ul>
                
            </div>
            
            <div class="modal fade" id=convidar-modal">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">�</button>
                    <h3>Convidar amigos para sua viagem</h3>
                </div>
                <div class="modal-body">
                    <form>
                        <label>Escreva uma mensagem para seus amigos</label>
                        <textarea maxlength="300" placeholder="Escreva uma mensagem para seus amigos" style="width: 100%;"></textarea>
                        Para seus amigos do Facebook voc� deseja:

                        <div class="control-group">
                            <label class="control-label">Para seus amigos do Facebook voc� deseja:</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio">
                                    Enviar mensagem privada
                                </label>
                                <label class="radio">
                                    <input type="radio">
                                    Postar convite no mural
                                </label>
                            </div>
                        </div>
                        
                        ? - Seus amigos do TripFans receber�o uma notifica��o 
                        
                    </form>                
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn">Cancelar</a>
                    <a href="#" class="btn btn-primary">Enviar</a>
                </div>
            </div>

            <div class="modal fade" id="solicitar-dicas-modal">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">�</button>
                    <h3>Solicitar recomenda��es de amigos para sua viagem</h3>
                </div>
                <div class="modal-body">
                    <form>
                        <label>Mensagem</label>
                        <textarea maxlength="300" placeholder="Escreva uma mensagem para seus amigos" style="width: 100%;"></textarea>

                        <div class="control-group">
                            <label class="control-label">Para seus amigos do Facebook voc� deseja:</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio" name="tipo-mensagem" checked="checked">
                                    Enviar mensagem privada
                                </label>
                                <label class="radio">
                                    <input type="radio" name="tipo-mensagem">
                                    Postar convite no mural
                                </label>
                            </div>
                        </div>
                        
                        ? - Seus amigos do TripFans receber�o uma notifica��o 
                        
                    </form> 
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal">Cancelar</a>
                    <a href="#" class="btn btn-primary">Enviar</a>
                </div>
            </div>
            
            <div>
                &nbsp;
            </div>
            
            <div style="height: 300px; overflow-y: scroll; overflow-x: hidden;">
                    
            <c:if test="${not empty amizades}">
            
                <div class="horizontalSpacer"></div>
                
                <div class="row">
                
                    <div id="listaAmigosFB" class="span15">
                    
                        <c:set var="letra" value="${'A'}" />
                
                        <c:forEach items="${amizades}" var="amizade" varStatus="contador">
                            
                            <c:set var="letraAtual" value="${fn:substring(amizade.nomeAmigo, 0, 1)}" />
                            
                            <c:if test="${letraAtual != letra}">
                                <div class="span1">
                                    <c:set var="letra" value="${letraAtual}" />
                                    <a name="${letra}"> </a>
                                </div>
                            </c:if>
                            
                            <fan:card type="user" friendship="${amizade}" showActions="false" selectable="true" spanSize="5" />
                                    
                        </c:forEach>
                        
                        <div class="row span8" style="padding-top: 15px;">
                            <c:url var="urlAmigos" value="/amigos/${perfil.urlPath}" />
                            <fan:pagingButton targetId="listaAmigosFB" title="Ver mais amigos deste usu�rio" noMoreItensText="N�o h� mais amigos para serem exibidos." cssClass="offset4 span6" url="${urlAmigos}" />
                        </div>
                    </div>
                    
                    <div id="alphabet-list" class="span1">
                        <div class="tabbable tabs-right">
                            <ul class="nav nav-tabs" style="padding-left: 4px; padding-right: 0px;">
                                
                                <li class="active">
                                    <a href="#A" class="ordenacao-alfabetica-tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">A</a>
                                </li>
                                <% 
                                    for(char alphabet = 'B'; alphabet <= 'Z'; alphabet++) {
                                %>              
                                <li>
                                    <a href="#<%=alphabet%>" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;"><%=alphabet%></a>
                                </li>
                                <%
                                    }
                                %>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                
            </c:if>
            
            </div>            
            
        </div>
        
    </div>
    
</div>