<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="modal hide" id="modal-aviso">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">�</button>
        <h3>Aten��o</h3>
    </div>
    <div class="modal-body">
        <p>Voc� j� incluiu esse destino!</p>
    </div>
    <div class="modal-footer">
        <a id="btn-fechar-modal-aviso" href="#" class="btn btn-danger" data-dismiss="modal">
            <i class="icon-remove icon-white"></i> Fechar
        </a>
    </div>
</div>

<fieldset>
    
    <legend>
      <h3>
      <c:if test="${viagem.id == null}">
        Para onde voc� ${verboPassado}?
      </c:if>
      <c:if test="${viagem.id != null}">
        Destinos desta Viagem
      </c:if>
      </h3>
    </legend>
    
    <div>
        
        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
        
            <li>
                <fan:autoComplete url="/pesquisaTextual/consultarCidades" hiddenName="cidade" id="novoDestino" 
                                  valueField="id" labelField="nomeExibicao"  cssStyle="position: relative;"
                                  showActions="true" blockOnSelect="false" name="pesquisaCidades"
                                  placeholder="Adicione um destino..." addOnLarge="true"
                                  onSelect="adicionarDestino()" position="my: 'center bottom', at: 'center top'" />
            </li>
            <li class="hide error">
                <div class="span6 error-message"></div>
            </li>
        </ul>
        
    </div>
    
	<div style="padding-left: 0px; padding-bottom: 10px">
	    <div>
			<div>
				<ul id="ul-destinos">
                    <c:forEach items="${viagem.destinosViagem}" var="destinoViagem" varStatus="status">
                        <c:set var="local" value="${destinoViagem.destino}"/>
                        <%@ include file="destinoViagemCard.jsp" %>
                        <input id="destino-viagem-${local.id}" type="hidden" value="${destinoViagem.id}" name="list[${status.index}].id">
                        <input id="destino-${local.id}" class="destinoViagem" type="hidden" value="${local.id}" name="list[${status.index}].destino">
                        <input id="ordem-${local.id}" class="ordemDestinoViagem" type="hidden" value="" name="list[${status.index}].ordem">
                    </c:forEach>
                </ul>
	       </div>
		</div>
	</div>
</fieldset>

<script type="text/javascript">

	$(function() {
	    $('#ul-destinos').sortable({
	        update: function(event, ui) {
	      	  configurarDestinos();
	        }
	    });
	    $('#ul-destinos').disableSelection();
	});

 	function verificaDuplicidadeDestino(idCidade, destinos){
       	// Varrer dos destinos selecionados
       	for (var i = 0; i < destinos.length; i++) {
       		if (destinos[i].value == idCidade){
       			return true;
       		}
       	}
        return false;
 	}
       
    function adicionarDestino() {
       var idCidade = $('input[name="cidade"]').val();
	   var destinos = $('#ul-destinos .destinoViagem').sortable('toArray');
       if (!verificaDuplicidadeDestino(idCidade, destinos)){
	       var url = '<c:url value="/planoViagem/destinoViagemCard/"/>'+idCidade;
	       $('#novoDestino').val('');
	       $('#novoDestino').focus();
		   $.get(url, function(response) {
			   $('#ul-destinos').append('<input id="destino-viagem-'+idCidade+'" type="hidden" value="" name="">');
			   $('#ul-destinos').append('<input id="destino-'+idCidade+'" class="destinoViagem" type="hidden" value="'+idCidade+'" name="">');
			   $('#ul-destinos').append('<input id="ordem-'+idCidade+'" class="ordemDestinoViagem" type="hidden" value="" name="">');
			   $('#ul-destinos').append(response);
			   configurarDestinos();
		   });
       } else {
		   $('#modal-aviso').modal({keyboard: true});
		   $('#novoDestino').val('');
           $('#novoDestino').focus();
	   }
    }
   
    function configurarDestinos() {
       	var destinos = $('#ul-destinos > input.destinoViagem').sortable('toArray');
       	// Varrer dos destinos selecionados
       	for (var i = 0; i < destinos.length; i++) {
       		// Alterar o nome do hidden para comtemplar o indice de cada objeto na lista
       		$('#destino-viagem-' + $(destinos[i]).attr('value')).attr('name', 'list[' + i + '].id');
       		$('#destino-' + $(destinos[i]).attr('value')).attr('name', 'list[' + i + '].destino');
       		// Alterar o atributo ordem de acordo com a posic�o na lista (somado + 1 pois come�a do 0)
            $('#ordem-' + $(destinos[i]).attr('value')).attr('name', 'list[' + i + '].ordem');
            $('#ordem-' + $(destinos[i]).attr('value')).val(i + 1);
       	}
    }
       
    $(document).ready(function() {
    	
    	//$('#ul-destinos').delegate('.remover', 'click', function(event) {
    	$('#ul-destinos').on('click', '.remover', function (e) {
    		event.preventDefault();
    		var id = $(this).attr('id');
    		$(this).tooltip('hide');
    		$('#destino-'+id).remove();
            $('#destino-viagem-'+id).remove();
			$('#ordem-'+id).remove();
			$('#card-local-'+id).remove();
			configurarDestinos();
			$('#novoDestino').focus();
        });
    	
    	$('#btn-fechar-modal-aviso').click(function (e) {
            event.preventDefault();
            $('#novoDestino').focus();
    	});
        
    });
       
</script>