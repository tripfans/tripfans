<%@page import="br.com.fanaticosporviagens.model.entity.TipoCozinha"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoHospedagem"%>
<%@page import="br.com.fanaticosporviagens.model.entity.CategoriaLocal"%>
<%@ page import="br.com.fanaticosporviagens.viagem.view.FiltroLocaisViagem"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>

    #wrapper {
        height: 300px;
        width: 630px;
        position: relative;
        display: block;
    }
    
    #demo-frame > div.demo { padding: 10px !important; }
    .scroll-pane { overflow: auto; width: 99%; float:left; }
    .scroll-content { width: 2200px; float: left; }
    .scroll-content-item { width: 50px; height: 50px; float: left; margin: 1px; font-size: 3em; line-height: 48px; text-align: center; }
    * html .scroll-content-item { display: inline; } /* IE6 float double margin bug */
    .scroll-bar-wrap { clear: left; padding: 0 4px 0 2px; margin: 0 -1px -1px -1px; }
    .scroll-bar-wrap .ui-slider { background: none; border:0; height: 2em; margin: 0 auto;  }
    .scroll-bar-wrap .ui-handle-helper-parent { position: relative; width: 100%; height: 100%; margin: 0 auto; }
    .scroll-bar-wrap .ui-slider-handle { top:.2em; height: 1.5em; }
    .scroll-bar-wrap .ui-slider-handle .ui-icon { margin: -8px auto 0; position: relative; top: 50%; }

    .local-box {
        border: 1px solid #ddd;
        -webkit-border-radius: 6px;
           -moz-border-radius: 6px;
                border-radius: 6px;
         -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.075);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.075);
                box-shadow: 0 1px 2px rgba(0,0,0,.075);
    }
    .local-box {
        margin-bottom: 20px;
        padding: 9px;
    }
    .local-box div {
        -webkit-border-radius: 3px;
           -moz-border-radius: 3px;
                border-radius: 3px;
    }
    
    .local-box .local-box-details,
    .local-box .local-box-content {
        float: left;
        top: 0px;
        padding-top: 0px;
        margin-top: 0px
    }
    .local-box .local-box-details {
        width: 45%;
    }
    .local-box .local-box-content {
        width: 50.5%;
    }
    
    .local-box .tool-tips .drag-to-cart {
        color: #C4151C;
        top: -22px;
    }
    
    .local-box:hover { 
        background: #f0f0f0;
        cursor: move;  
    }
    
    //.local-box:hover.draggable { display: block; }
    
    .local-box .tool-tips { position: relative; z-index: 1; margin: 0 9px; }
    .local-box .tool-tips span { 
        background: #f2f2f2 url("../images/interface/forms-sprite.png") 0 -22px repeat-x; 
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        border-bottom: 1px solid #ccc; 
        display: none; 
        font: 11px DINMediumRegular; padding: 4px 10px 2px 0; position: absolute; text-transform: uppercase; white-space: nowrap; width: 168px; text-shadow: 0 0 0 transparent, rgba(255, 255, 255, 0.5) 0 1px 0 ; -moz-border-radius: 2px; -webkit-border-radius: 2px; border-radius: 2px; }
    .local-box .tool-tips span:before { background: url("../images/interface/tool-tip-sprite.png"); content: ''; display: block; position: absolute; left: 50%; margin-left: -7px; width: 14px; height: 7px; }
    .local-box .tool-tips span:after { background: url("../images/interface/tool-tip-sprite.png"); content: ''; display: block; position: absolute; right: 5px; top: 3px; }
    
    .local-box .tool-tips .drag-to-cart { 
        color: #c4151c; 
        top: -30px;
        left: 75px;
    }
    
    .local-box .tool-tips .drag-to-cart:before { background-position: -10px -10px; bottom: -7px; }
    .local-box .tool-tips .drag-to-cart:after { background-position: -58px -10px; width: 13px; height: 12px; }
    .local-box .tool-tips .added-to-cart { color: #3e802b; top: 172px; }
    .local-box .tool-tips .added-to-cart:before { background-position: -34px -10px; top: -7px; }
    .local-box .tool-tips .added-to-cart:after { background-position: -81px -10px; width: 13px; height: 13px; }
    
    #filter-menu {
        //-moz-border-radius-topleft:5px;
        -moz-border-radius-bottomleft:5px;
        -moz-border-radius-bottomright:5px;
        //-webkit-border-top-left-radius:5px;
        -webkit-border-bottom-left-radius:5px;
        -webkit-border-bottom-right-radius:5px;
        display:none;
        z-index: 9999;
        background-color: #ddeef6;
        position:absolute;
        border:1px transparent;
        text-align:left;
        padding: 12px;
        //top: 120px;
        //right: 0px;
        margin-top:5px;
        margin-right: 0px;
        *margin-right: -1px;
        color:#789;
        font-size:11px;
    }
    
    .vertical-carousel {
        background-color: #EEE;
    }
    
    .vertical-carousel ul.vertical-carousel-list {
        position: relative;
        margin: 0 auto;
        top: 0px;
        padding:0px;
    }
    
</style>
    
<script type="text/javascript">

//$(document).ready(function() {
	
	// Remover o scroll
	$('#content').css('overflow', 'visible');
	
    $('#locaisTab a:first').tab('show');
    
    $('#locaisTab a').click(function (e) {
        e.preventDefault();
        
        var abaSelecionada = $(this).attr('data-tab-content-id');
        
        $('#' + abaSelecionada).load(getURLConsultaLocais(null, abaSelecionada));
        $('#filtro-nome-local').val('');
        $(this).tab('show');
    });
    
    $('#destino').change(function() {
        var abaSelecionada = getIdAbaSelecionada();
        $('#' + abaSelecionada).load(getURLConsultaLocais());
        $('#filtro-nome-local').val('');
    });

    $('#tipo-local a').click(function (e) {
        e.preventDefault();
        var abaSelecionada = getIdAbaSelecionada();
        var tipoLocal = $(this).attr('data-tipo-local');
        
        // Esconder os filtros
        $('.span-filter').hide();
        
        // Mostrar apenas os que seja do tipo do local selecionado
        $('.' + tipoLocal).show();
        
        if ($(this).attr('class').indexOf('active') === -1) {
            $('#' + abaSelecionada).load(getURLConsultaLocais(tipoLocal));
            $('#filtro-nome-local').val('');
        }
    });
    
    $('#locaisTab a:first').click();
    
    /*$('.carousel').carousel({
	    interval: false
	});*/

    $('.filtro-nome-local').keyup(function(e) {
        if (e.keyCode == 13) {
        	 $('#filtrar').click();
        } else {
        	//
        }
    });
    
    $('.filtro-nome-local').click(function(e) {
        e.preventDefault();
        
        //get the position of the placeholder element
        pos   = $(this).offset();
        width = $(this).width();
        
        //show the menu directly over the placeholder
        //$('fieldset#filter-menu').css({ 'left': (pos.left - width) + 'px', 'top': pos.top + 'px'})
        
        $('fieldset#filter-menu').toggle();
        //$('.filtro-nome-local').toggleClass('menu-open');
    });

    $('fieldset#filter-menu').mouseup(function() {
        return false;
    });
    
    $(document).mouseup(function(e) {
        if($(e.target).parent('a.filtro-nome-local').length==0) {
            $('.filtro-nome-local').removeClass('menu-open');
            $('fieldset#filter-menu').hide();
        }
    });
    
    var categorias = {items: [
    <%
        for (CategoriaLocal categoria : CategoriaLocal.values()) {
    %>
        {value : '<%=categoria.getCodigo()%>', name : '<%=categoria.getDescricao()%>'},
    <%
        }
    %>
    ]};

    var tiposHospedagem = {items: [
    <%
        for (TipoHospedagem tipoHospedagem : TipoHospedagem.values()) {
    %>
        {value : '<%=tipoHospedagem.getCodigo()%>', name : '<%=tipoHospedagem%>'},
    <%
        }
    %>
    ]};
    
    var tiposCozinha = {items: [
    <%
       for (TipoCozinha tipoCozinha : TipoCozinha.values()) {
    %>
       {value : '<%=tipoCozinha.getCodigo()%>', name : '<%=tipoCozinha.getDescricao()%>'},
    <%
       }
    %>
    ]};

    $('#filtro-categoria-atracao').autoSuggest(categorias.items, {
    	asHtmlID : 'categorias',
        selectedItemProp: 'name', 
        searchObjProps: 'name',
        startText: '',
        emptyText: 'Sem resultados',
    });
    
    $('#filtro-tipo-hospedagem').autoSuggest(tiposHospedagem.items, {
    	asHtmlID : 'tiposHospedagem',
        selectedItemProp: 'name', 
        searchObjProps: 'name',
        startText: '',
        emptyText: 'Sem resultados',
        selectionRemoved: function(elem) { 
        	 jsDump(elem, 1)
        }
    });

    $('#filtro-cozinha').autoSuggest(tiposCozinha.items, {
    	asHtmlID : 'tiposCozinha',
        selectedItemProp: 'name', 
        searchObjProps: 'name',
        startText: '',
        emptyText: 'Sem resultados',
    });
    
    $( '#slider-rangePreco' ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 75, 300 ],
        slide: function( event, ui ) {
            $( '#rangePreco' ).val( '$' + ui.values[ 0 ] + ' - $' + ui.values[ 1 ] );
        }
    });
    
    $( '#rangePreco' ).val( '$' + $( '#slider-rangePreco' ).slider( 'values', 0 ) +
        ' - $' + $( '#slider-rangePreco' ).slider( 'values', 1 ) );
    
    $('#filtrar').click(function (e) {
    	var abaSelecionada = getIdAbaSelecionada();
        $('#' + abaSelecionada).load(getURLConsultaLocais());
        $('fieldset#filter-menu').toggle();
    });

    $('#limpar').click(function (e) {
    	
    });
    
    $('#barra-dias-atividades').verticalCarousel({
        nSlots: 7, speed: 400
    });
    
    $('#tab-dias-atividades a:first').tab('show');
    
    $('#tab-dias-atividades a').click(function (e) {
	    e.preventDefault();
	    $(this).tab('show');
	});
    
    $('.sortable').sortable({
        revert: true
    });
    
    $('.draggable').draggable({
        connectToSortable: '.sortable',
        //helper: 'clone',
        revert: 'invalid',
        //cursor: 'move',
        axis: 'y',
    });
    
    $('.scroll-button').mouseout(function() {
        var $item = $(this);
        clearTimeout($item.data('timeout'));
    });
    
    $('.scroll-button').mouseover(function() {
    	var $item = $(this);
    	var timeout = setTimeout(function() {
		    $item.click();
        }, 400);
    	
    	$item.data('timeout', timeout);
    });
    
    $('.draggable').mouseover(function() {
        var item = $(this);
        
        //clearTimeout($('#flip').data('timeout'));
        
        //item.css('background-color', '#f11');
        //item.toggleClass('alert-success');
        var timeout = setTimeout(function() { 
            item.click();
        }, 400);
        //$('#flip').data('timeout', timeout);
    });
    
    $('ul, li').disableSelection();
    
    $('.box-data-link').click(function (e) {
        e.preventDefault();
        var $link = $(this);
        
        $('.box-data').removeClass('active');
        $link.parent().addClass('active');
        
        var tabContent = $('.tab-content-atividades');
        
        tabContent.find('.tab-pane').removeClass('active');
        tabContent.find('#tab-dia-' + $link.attr('data-dia')).addClass('active')
    });

//});

function getIdAbaSelecionada() {
	return $('#locaisTab li.active a').attr('data-tab-content-id');
}

function getURLConsultaLocais(tipoLocal, filtro) {
	
    var idCidade = $('#destino').val();
    var tipoLocalSelecionado = tipoLocal ? tipoLocal : $('#tipo-local').find('a.active').attr('data-tipo-local');
    var filtroAbaSelecionada = filtro ? filtro : getIdAbaSelecionada();
    
    var url = '<c:url value="/planoViagem/${viagem.id}/wizard/listarLocais" />';
    url += '?idCidade=' + idCidade;
    url += '&tipoLocal=' + tipoLocalSelecionado;
    url += '&filtroLocais=' + filtroAbaSelecionada;
    
    var nomeLocal = $('#filtro-nome-local').val();
    if (nomeLocal != null) {
        nomeLocal = $.trim(nomeLocal);
        if (nomeLocal != '') { 
            url += '&nomeLocal=' + nomeLocal;
        }
    }
    
    url += getParametrosFiltro(tipoLocalSelecionado);
    
    url += '&start=0&limit=6';
    
    return url;
}

function getParametrosFiltro(tipoLocalSelecionado) {
	var params = '';
	if (tipoLocalSelecionado == '<%=LocalType.ATRACAO%>') {
		$('.<%=LocalType.ATRACAO%>').each(function() {
			params += getParametro($(this));
		})
	} else if (tipoLocalSelecionado == '<%=LocalType.RESTAURANTE%>') {
        $('.<%=LocalType.RESTAURANTE%>').each(function() {
            params += getParametro($(this));
        })
    } else if (tipoLocalSelecionado == '<%=LocalType.HOTEL%>') {
        $('.<%=LocalType.HOTEL%>').each(function() {
            params += getParametro($(this));
        })
    }
	return params;
}

function getParametro($filtro) {
    var name, value;
    // Verificar se esta selecionado
    if ($filtro.find('.enable-filter').is(':checked')) {
        // Recuperar o valor
        name = $filtro.find('.filter-value').attr('name');
        value = $filtro.find('.as-values').val();
        
        if (name != null && (value != null && value != '')) {

            // TODO retirar quando possivel
            // gato para remover a ultima virgula, caso exista
            var tamanho = value.length;
            var ultimoChar = value.substr(tamanho -1, tamanho);
            if (ultimoChar == ',') {
            	value = value.substr(0, tamanho -1);
            }
            // fim do gato

            return '&' + name + '=' + value;
        }
    }
    return '';
}

//$(function() {
	
    //scrollpane parts
    var scrollPane = $( ".scroll-pane" ),
        scrollContent = $( ".scroll-content" );
    
    //build slider
    var scrollbar = $( ".scroll-bar" ).slider({
        slide: function( event, ui ) {
            if ( scrollContent.width() > scrollPane.width() ) {
                scrollContent.css( "margin-left", Math.round(
                    ui.value / 100 * ( scrollPane.width() - scrollContent.width() )
                ) + "px" );
            } else {
                scrollContent.css( "margin-left", 0 );
            }
        }
    });
    
    //append icon to handle
    var handleHelper = scrollbar.find( ".ui-slider-handle" )
                                .mousedown(function() {
                                    scrollbar.width( handleHelper.width() );
                                })
                                .mouseup(function() {
                                    scrollbar.width( "100%" );
                                })
                                .append( "<span class='ui-icon ui-icon-grip-dotted-vertical'></span>" )
                                .wrap( "<div class='ui-handle-helper-parent'></div>" ).parent();
    
    //change overflow to hidden now that slider handles the scrolling
    scrollPane.css( "overflow", "hidden" );
    
    //size scrollbar and handle proportionally to scroll distance
    function sizeScrollbar() {
        var remainder = scrollContent.width() - scrollPane.width();
        var proportion = remainder / scrollContent.width();
        var handleSize = scrollPane.width() - ( proportion * scrollPane.width() );
        scrollbar.find( ".ui-slider-handle" ).css({
            width: handleSize,
            "margin-left": -handleSize / 2
        });
        handleHelper.width( "" ).width( scrollbar.width() - handleSize );
    }
    
    //reset slider value based on scroll content position
    function resetValue() {
        var remainder = scrollPane.width() - scrollContent.width();
        var leftVal = scrollContent.css( "margin-left" ) === "auto" ? 0 :
            parseInt( scrollContent.css( "margin-left" ) );
        var percentage = Math.round( leftVal / remainder * 100 );
        scrollbar.slider( "value", percentage );
    }
    
    //if the slider is 100% and window gets larger, reveal content
    function reflowContent() {
        var showing = scrollContent.width() + parseInt( scrollContent.css( "margin-left" ), 10 );
        var gap = scrollPane.width() - showing;
        if ( gap > 0 ) {
            scrollContent.css( "margin-left", parseInt( scrollContent.css( "margin-left" ), 10 ) + gap );
        }
    }
    
    //change handle position on window resize
    $( window ).resize(function() {
        resetValue();
        sizeScrollbar();
        reflowContent();
    });
    //init scrollbar size
    setTimeout( sizeScrollbar, 10 );//safari wants a timeout
        
//});
</script>

<div class="row" style="margin-left: 0px;">

    <div class="span14">

        <ul class="nav nav-tabs" id="locaisTab" style="margin-bottom: 5px;">
            <li class="active">
                <a href="#tab1" data-toggle="tab" data-tab-content-id="<%=FiltroLocaisViagem.LOCAIS_MELHORES_AVALIADOS%>">Melhores avalia��es</a>
            </li>
            <li>
                <a href="#tab2" data-toggle="tab" data-tab-content-id="<%=FiltroLocaisViagem.LOCAIS_RECOMENDADOS_POR_AMIGOS%>">Recomenda��es de Amigos</a>
            </li>
            <li>
                <a href="#tab3" data-toggle="tab" data-tab-content-id="<%=FiltroLocaisViagem.LOCAIS_MAIS_VISITADOS%>">Mais Visitados</a>
            </li>
            <%--li>
                <a href="#tab4" data-toggle="tab" data-tab-content-id="<%=FiltroLocaisViagem.LOCAIS_PATROCINADOS%>">Patrocinado</a>
            </li--%>
            <li>
                <a href="#tab5" data-toggle="tab" data-tab-content-id="<%=FiltroLocaisViagem.CONSULTAR_LOCAIS%>">Pesquisar</a>
            </li>        
        </ul>
        
        <div>
                
            <div class="subnav">
                
                <div class="btn-toolbar left" style="margin-bottom: 9px">
                    <div class="btn-group" data-toggle="buttons-radio">
                    
                        <a href="#" class="btn btn-mini" title="Ver em modo lista">
                            <i class="icon-th-list"></i>
                        </a>
                        <a href="#" class="btn btn-mini active" title="Ver em modo tabela">
                            <i class="icon-th"></i>
                        </a>
                    </div>
                </div>
                
                <div class="left" style="margin: 10px; font-size: 11px;">
                    Cidade
                </div>
                
                <div class="left span4" style="margin: 5px 0 9px 0;">
                    <select id="destino" class="span20">
                        <c:forEach items="${viagem.destinosViagem}" var="destinoViagem">
                            <option value="${destinoViagem.destino.id}">${destinoViagem.destino.nome}</option>
                        </c:forEach>
                    </select>
                </div>
                
                <div class="left" style="margin: 10px; font-size: 11px;">
                    Lugares
                </div>
                
                <div class="btn-toolbar left" style="margin-bottom: 9px">
                    <div id="tipo-local" class="btn-group" data-toggle="buttons-radio">
                        <a class="btn btn-mini active" href="#" data-tipo-local="<%=LocalType.ATRACAO%>">
                            Atra��es
                        </a>
                         
                        <a class="btn btn-mini" href="#" data-tipo-local="<%=LocalType.RESTAURANTE%>">
                            Restaurantes
                        </a>
                         
                        <%--a class="btn btn-mini" href="#" data-tipo-local="<%=LocalType.HOTEL%>">
                            Hot�is
                        </a--%>
                    </div>
                </div>
        
        
                <div class="left" style="margin: 10px; font-size: 11px;">
                    Filtrar
                </div>
        
                <div class="left span7" style="margin: 5px 0 9px 0;">
                    <div class="input-append">
                        <input id="filtro-nome-local" class="filtro-nome-local" placeholder="Informe o nome do local" />
                        <span class="add-on"><i class="icon-search"></i></span>
                    </div>
                    <fieldset id="filter-menu" style="display: none; padding-left: 0px;">
                    
                        <span class="<%=LocalType.ATRACAO%> span-filter">
                            <label for="filtro-categoria-atracao" class="checkbox">
                                <input type="checkbox" checked="checked" class="enable-filter" title="Habilitar este filtro">
                                Tipos de atra��o
                            </label>
                            <input id="filtro-categoria-atracao" name="categorias" type="text" class="filter-value span4">
                            <p></p>
                        </span>
        
                        <span class="<%=LocalType.RESTAURANTE%> span-filter" style="display: none;">
                            <label for="filtro-cozinha" class="checkbox">
                                <input type="checkbox" checked="checked" class="enable-filter" title="Habilitar este filtro">
                                Tipos de cozinha
                            </label>
                            <input id="filtro-cozinha" name="tiposCozinha" type="text" class="filter-value">
                        </span>
        
                        <span class="<%=LocalType.HOTEL%> span-filter" style="display: none;">
                            <label for="filtro-tipo-hospedagem" class="checkbox">
                                <input type="checkbox" checked="checked" class="enable-filter" title="Habilitar este filtro">
                                Tipos de hospedadem
                            </label>
                            <input id="filtro-tipo-hospedagem" name="tiposHospedagem" type="text" class="filter-value">
                        </span>
        
                        <span class="<%=LocalType.HOTEL%> span-filter" style="display: none;">
                            <label for="filtro-estrelas" class="checkbox">
                                <input type="checkbox" checked="checked" class="enable-filter" title="Habilitar este filtro">
                                Estrelas
                            </label>
                            <input id="filtro-estrelas" name="estrelas" type="text" class="filter-value">
                        </span>
                        
                        <span class="<%=LocalType.RESTAURANTE%> span-filter" style="display: none;">
                            <label for="rangePreco" class="checkbox">
                                <input type="checkbox" checked="checked" class="enable-filter" title="Habilitar este filtro">
                                Pre�o m�dio entre:
                            </label>
                            <input type="text" id="rangePreco" style="color:#f6931f; font-weight:bold;" />
                            <div id="slider-rangePreco"></div>
                        </span>
        
                        <p></p>
                        <div class="span4"></div>
                        <div style="border-bottom: 1px solid #dddddd;"></div>
                        <p></p>
        
                        <div class="right">
                            <a id="limpar" class="btn" href="#">
                                <i class="icon-remove"></i> Limpar
                            </a>
                            <a id="filtrar" class="btn btn-info" href="#">
                                <i class="icon-zoom-in icon-white"></i> Filtrar
                            </a>
                        </div>
                    </fieldset>
                </div>
        
            </div>
        
        </div>
        
        <div class="row">
            <%-- N�o excluir essa DIV. N�o sei pq, mas se tirar desalinha a tela toda --%>
        </div>
        
        <div class="tab-content" style="overflow: visible;">
        
            <div class="tab-pane active" id="tab1">
            
                <div class="alert alert-info fade in" data-dismiss="alert">
                    <a class="close" data-dismiss="alert">�</a>
                    <p>
                        Encontre os lugares que tiveram as melhores avalia��es de nossos usu�rios.
                    </p>
                </div>
                
                <div id="<%=FiltroLocaisViagem.LOCAIS_MELHORES_AVALIADOS%>">
                </div>
                
            </div>
        
            <div class="tab-pane" id="tab2">
                    
                <div id="<%=FiltroLocaisViagem.LOCAIS_RECOMENDADOS_POR_AMIGOS%>">
                </div>
            
            </div>
        
            <div class="tab-pane" id="tab3">
        
                <div id="<%=FiltroLocaisViagem.LOCAIS_MAIS_VISITADOS%>">
        
                </div>
            
            </div>
        
            <div class="tab-pane" id="tab4">
        
                <div id="<%=FiltroLocaisViagem.LOCAIS_PATROCINADOS%>">
        
                </div>
            
            </div>
        
            <div class="tab-pane" id="tab5">
        
                <div id="<%=FiltroLocaisViagem.CONSULTAR_LOCAIS%>">
        
                </div>
            
            </div>
        
        </div>
        
        <%--div class="scroll-pane ui-widget ui-widget-header ui-corner-all">
        
            <div>
            </div>
            <div class="scroll-content left">
            
                <div id="scroll-left" class="scroll-content-item well">?</div>
                <div id="scroll-left" class="scroll-content-item well">&lsaquo;</div>
                <div id="scroll-right" class="scroll-content-item well right">&gt;</div>
            
                <c:forEach begin="1" end="${viagem.quantidadeDias}" var="dia">
        
                    <div class="scroll-content-item droppable well" data-day-number="${dia}" data-selected="false">${dia}</div>
                
                </c:forEach>
            
            </div>
                
            <div class="scroll-bar-wrap ui-widget-content ui-corner-bottom">
                <div class="scroll-bar"></div>
            </div>
        </div--%>
        
    </div>

    <div class="span6">
    
        <%-- <div class="widget-resumo-viagem">
        
            <div id="accordion-resumo" class="accordion">

                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse-sobre">
                            Sobre esta viagem
                        </a>
                    </div>
                    
                    <div id="collapse-sobre" class="accordion-body collapse">
                        <div class="accordion-inner">
                          
                        </div>
                    </div>
                    
                </div>
                
                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse-destinos">
                            Destinos
                        </a>
                    </div>
                    
                    <div id="collapse-destinos" class="accordion-body collapse">
                        <div class="accordion-inner">
                          
                        </div>
                    </div>
                    
                </div>
                
                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse-transportes">
                            Transportes
                        </a>
                    </div>
                    
                    <div id="collapse-transportes" class="accordion-body collapse">
                        <div class="accordion-inner">
                          
                        </div>
                    </div>
                    
                </div>

                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse-hospedagem">
                            Hospedagem
                        </a>
                    </div>
                    
                    <div id="collapse-hospedagem" class="accordion-body collapse">
                        <div class="accordion-inner">
                          
                        </div>
                    </div>
                    
                </div>
    
                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-resumo" href="#collapse-atividades">
                            Atividades
                        </a>
                    </div>
                    
                    <div id="collapse-atividades" class="accordion-body in collapse">
                    
                        <div class="accordion-inner" style="padding: 0px;">
                        
                            <div style="width: 40px; float: left">
                                
                                <div id="tab-dias-atividades" class="tabbable tabs-left">
                                
                                    <div id="barra-dias-atividades" class="vertical-carousel">
                                        
                                        <div class="widget-resumo-viagem-botao-topo-small">
                                            <a href="#" class="scru scroll-button">Anterior</a>
                                        </div>
                                        
                                        <div class="vertical-carousel-container" style="min-height: 385px;">
                                    
                                            <ul class="vertical-carousel-list" style="padding: 0px; margin: 0px; background-color: #EFEFEF;">
                                            
                                              <c:forEach items="${viagem.diasViagem}" var="diaViagem">
                                                
                                                <li class="droppable" data-numero-dia="${diaViagem.numero}">
                                                
                                                    <div class="box-data">
                                                        <a href="#" class="box-data-link" data-dia="${diaViagem.numero}" style="text-decoration: none; min-width: 20px; max-width: 20px; padding-top: 0px; padding-left: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">
                                                            <p>
                                                                <div class="mes">
                                                                    ${diaViagem.mesCurto}
                                                                </div>
                                                                <div class="dia">
                                                                    ${diaViagem.diaMes}
                                                                </div>
                                                                <div class="diaSemana">
                                                                    ${diaViagem.diaSemanaCurto}
                                                                </div>
                                                            </p>
                                                        </a>
                                                    </div>                                                
                                                </li>
                                                
                                              </c:forEach>

                                            </ul>
                                            
                                        </div>
                                        
                                        <div class="widget-resumo-viagem-botao-rodape-small">
                                            <a href="#" class="scrd scroll-button">Proximo</a>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                                        
                            </div>
                            
                            <div style="width: 280px; float: right">
                            
                                <div class="tab-content tab-content-atividades" style="overflow: hidden;">
                                
                                  <c:forEach begin="1" end="${viagem.quantidadeDias}" var="dia">
                                
                                    <div class="tab-pane ${dia == 1 ? 'active' : ''} lista-atividades" id="tab-dia-${dia}">
                                    
                                        <div class="vertical-carousel">
                            
                                            <div class="widget-resumo-viagem-botao-topo">
                                                <a href="#" class="scru scroll-button">Anterior</a>
                                            </div>
                                            
                                            <div class="vertical-carousel-container droppable" data-numero-dia="${dia}">
                                            
                                                <ul class="vertical-carousel-list sortable itens">
                                                
                                                  <c:forEach items="${viagem.atividades}" var="atividade">
                                                  
                                                    <c:if test="${atividade.diaInicio == dia}">
            
                                                    <li id="atividade-${atividade.id}" data-id-atividade="${atividade.id}" class="item draggable" style="height: 70px;">
                                                    
                                                        <div class="conteudo">
                                                            <div class="foto" >
                                                                <a href="#">
                                                                    <img width="90" height="68" src="<c:url value="/resources/images/atracao.jpg"/>">
                                                                </a>
                                                            </div>
                                                            <div class="texto">
                                                                <p class="hora">
                                                                    <i class="icon-time"></i>
                                                                    ${atividade.horaInicio}
                                                                </p>
                                                                <a href="#">
                                                                    <strong>
                                                                        ${atividade.local.nome}
                                                                    </strong>
                                                                </a>
                                                                <p>${atividade.local.nomeCidadeComSiglaEstado}</p>
                                                                <p>${atividade.local.nome}</p>
                                                            </div>
                                                            <div class="acoes">
                                                               <i class="icon-arrow-up"></i>
                                                               <i class="icon-arrow-down"></i>
                                                               <a id="remover-atividade-${atividade.id}" data-id-atividade="${atividade.id}" href="#" class="remover-atividade"><i class="icon-trash"></i></a>
                                                            </div>
                                                        </div>
                                                        
                                                    </li>
                                                    
                                                    </c:if>
                                                    
                                                  </c:forEach>
                                                    
                                                </ul>
                                                        
                                            </div>
                                            
                                            <div class="widget-resumo-viagem-botao-rodape">
                                                <a href="#" class="scrd scroll-button">Anterior</a>
                                            </div>

                                        </div>
                                    
                                    </div>
                                    
                                    <script>
                                    
                                        $('#tab-dia-${dia}').verticalCarousel({
                                            nSlots: 4, speed: 400
                                        });

                                    </script>
                                    
                                  </c:forEach>

                                </div>
                                
                            </div>
    
                        </div>
                        
                    </div>
                    
                </div>
            
                <div class="accordion-group">
                
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-resumo" href="#collapse-companheiros">
                            Companheiros
                        </a>
                    </div>
                    
                    <div id="collapse-companheiros" class="accordion-body collapse">
                        <div class="accordion-inner">
                          
                        </div>
                    </div>
                    
                </div>
                
            </div>
                
        </div>       --%>
        
        <div class="module-box borda-arredondada" style="min-height: 400px;">
            <div class="module-box-header">
                <h3>Atividades selecionadas</h3>
            </div>

            <div class="module-box-content-list row" style="margin-left: 0px;">
            
                <div id="painel-atividades" class="vertical-carousel">

                    <div class="widget-resumo-viagem-botao-topo">
                        <a href="#" class="scru scroll-button btn span20">
                            <i class="icon-chevron-up"></i>
                        </a>
                    </div>
                                        
                    <div class="vertical-carousel-container droppable" data-numero-dia="${dia}" style="min-height: 400px; max-height: 400px;">
                    
                        <ul class="vertical-carousel-list list sortable itens">
                        
                          <c:forEach items="${viagem.atividades}" var="atividade">
                          
                            <li id="atividade-${atividade.id}" data-id-atividade="${atividade.id}" class="item draggable" style="height: 70px; background-color: #fff;">
                            
                                <div class="conteudo">
                                    <div class="foto span7">
                                        <a href="#">
                                            <img width="90" height="68" src="<c:url value="/resources/images/atracao.jpg"/>">
                                        </a>
                                    </div>
                                    <div class="texto">
<%--                                         <p class="hora">
                                            <i class="icon-time"></i>
                                            ${atividade.horaInicio}
                                        </p>
 --%>
                                        <a href="#">
                                            <strong>
                                                ${atividade.local.nome}
                                            </strong>
                                        </a>
                                        <%-- <p>${atividade.local.nomeCidadeComSiglaEstado}</p> --%>
                                        <p>${atividade.local.nome}</p>
                                    </div>
                                    <div class="acoes">
                                       <i class="icon-arrow-up"></i>
                                       <i class="icon-arrow-down"></i>
                                       <a id="remover-atividade-${atividade.id}" data-id-atividade="${atividade.id}" href="#" class="remover-atividade"><i class="icon-trash"></i></a>
                                    </div>
                                </div>
                                
                            </li>
                            
                          </c:forEach>
                            
                        </ul>
                                
                    </div>
                    
                    <div class="widget-resumo-viagem-botao-rodape">
                        <a href="#" class="scrd scroll-button btn span20">
                            <i class="icon-chevron-down"></i>
                        </a>
                    </div>
            
                </div>
            </div>
        </div>        
    
    </div>

</div>