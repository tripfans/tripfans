<%@page import="br.com.fanaticosporviagens.model.entity.Amizade"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%-- <script src="http://connect.facebook.net/en_US/all.js"></script>
 --%>
<fieldset>
    
    <legend>
        <h3>Companheiros de Viagem</h3>
    </legend>

    <c:if test="${not empty convitesViagem}">
        <div>
        
            <div class="control-group">
                <label class="control-label">Amigos que voc� j� convidou:</label>
            </div>
            
            <div id="amigosConvidados" class="well">
            
              <div class="row">
              
                  <c:forEach items="${convitesViagem}" var="convite">
                  <div class="span1" style="width: 66px;">
                      <b>
                        <c:choose>
                            <c:when test="${convite.convidado != null}">
                                <%-- <img src="${convite.convidado.urlFotoAvatar}" style="width: 50px; height: 50px;" /> --%>
                                <img src="http://graph.facebook.com/${convite.idFacebookConvidado}/picture" style="width: 66px; height: 66px;" />
                            </c:when>
                            <c:otherwise>
                                <img src="http://graph.facebook.com/${convite.idFacebookConvidado}/picture" style="width: 66px; height: 66px;" />
                            </c:otherwise>
                        </c:choose>
                      </b>
                      <b>
                        <c:if test="${convite.aceito}">
                            <span class="label label-success" style="width: 66px;">Aceitou</span>
                        </c:if>
                        <c:if test="${not convite.aceito}">
                            <span class="label label-warning" style="width: 66px;">Convidado</span>
                        </c:if>
                        
                      </b>
                  </div>                        
                  </c:forEach>
                
              </div>
            
            </div>
        
        </div> 
    
    </c:if>
    
    <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
        <li>
            <label class="span4 fancy-text">
                <c:choose>
                    <c:when test="${criacaoDiario == true}">
                        <span class="required">
                            <label>
                                Amigos
                            </label>
                        </span>
                    </c:when>
                    <c:otherwise>
                        <label>
                            <span class="required">
                                Convide ${not empty convitesViagem ? 'mais' : 'seus'} amigos
                                <i id="help-nome" class="icon-question-sign helper" title=""></i>
                            </span>
                        </label>
                    </c:otherwise>
                </c:choose>
            </label>
            <input id="convidar-amigos" name="amigosConvidados" type="text">
        </li>
    </ul>
    
    <div id="mensagem-amigosConvidados" class="well" style="display: none;">
        <div class="control-group">
            <label class="control-label">Escreva uma mensagem para convidar seus amigos:</label>
            <div class="controls" class="span13">
                <textarea name="mensagemAmigosConvidados" rows="3" class="span12"></textarea>
            </div>
        </div>
        <a id="link-convidar" class="btn btn-primary" href="#">
            Convidar para a viagem
        </a>
    </div>

    <c:if test="${not empty convitesRecomendacao and not criacaoDiario}">
        <div>
        
            <div class="control-group">
                <label class="control-label">Amigos que voc� j� pediu recomenda��es:</label>
            </div>
            
            <div id="amigosSolicitados" class="well">
            
              <div class="row">
              
                  <c:forEach items="${convitesRecomendacao}" var="convite">
                    <div class="span1" style="width: 66px;">
                      <b>
                        <c:choose>
                            <c:when test="${convite.convidado != null}">
                                <%-- <img src="${convite.convidado.urlFotoAvatar}" style="width: 50px; height: 50px;" /> --%>
                                <img src="http://graph.facebook.com/${convite.idFacebookConvidado}/picture" style="width: 66px; height: 66px;" />
                            </c:when>
                            <c:otherwise>
                                <img src="http://graph.facebook.com/${convite.idFacebookConvidado}/picture" style="width: 66px; height: 66px;" />
                            </c:otherwise>
                        </c:choose>
                      </b>
                    </div>                        
                  </c:forEach>
                
              </div>
            
            </div>
        
        </div> 
    
    </c:if>            

    <div>
    
        <div class="control-group">
            <label class="control-label">Pe�a ${not empty convitesRecomendacao ? 'mais' : ''} recomenda��es a seus amigos:</label>
            <div class="controls" class="span6">
                <input id="solicitar-dicas-amigos" name="amigosSolicitados" type="text">
            </div>
        </div>

        <div id="mensagem-amigosSolicitados" class="well" style="display: none;">
        
            <div class="control-group">
                <label class="control-label">Escreva uma mensagem para seus amigos:</label>
                <div class="controls" class="span13">
                    <textarea id="mensagemAmigosSolicitados" name="mensagemAmigosSolicitados" rows="3" class="span12"></textarea>
                </div>
            </div>
            <a id="link-solicitar-dicas" class="btn btn-primary" href="#">
                Solicitar recomenda��es
            </a>
            
        </div>
    
    </div>
    
    <%--div class="modal fade" id=convidar-modal">
        <div class="modal-header">
            <button class="close" data-dismiss="modal">�</button>
            <h3>Convidar amigos para sua viagem</h3>
        </div>
        <div class="modal-body">
            <form>
                <label>Escreva uma mensagem para seus amigos</label>
                <textarea maxlength="300" placeholder="Escreva uma mensagem para seus amigos" style="width: 100%;"></textarea>
                Para seus amigos do Facebook voc� deseja:

                <div class="control-group">
                    <label class="control-label">Para seus amigos do Facebook voc� deseja:</label>
                    <div class="controls">
                        <label class="radio">
                            <input type="radio">
                            Enviar mensagem privada
                        </label>
                        <label class="radio">
                            <input type="radio">
                            Postar convite no mural
                        </label>
                    </div>
                </div>
                
                ? - Seus amigos do TripFans receber�o uma notifica��o 
                
            </form>                
        </div>
        <div class="modal-footer">
            <a href="#" class="btn">Cancelar</a>
            <a href="#" class="btn btn-primary">Enviar</a>
        </div>
    </div--%>

    <%--div class="modal fade" id="solicitar-dicas-modal">
        <div class="modal-header">
            <button class="close" data-dismiss="modal">�</button>
            <h3>Solicitar recomenda��es de amigos para sua viagem</h3>
        </div>
        <div class="modal-body">
            <form>
                <label>Mensagem</label>
                <textarea maxlength="300" placeholder="Escreva uma mensagem para seus amigos" style="width: 100%;"></textarea>

                <div class="control-group">
                    <label class="control-label">Para seus amigos do Facebook voc� deseja:</label>
                    <div class="controls">
                        <label class="radio">
                            <input type="radio" name="tipo-mensagem" checked="checked">
                            Enviar mensagem privada
                        </label>
                        <label class="radio">
                            <input type="radio" name="tipo-mensagem">
                            Postar convite no mural
                        </label>
                    </div>
                </div>
                
                ? - Seus amigos do TripFans receber�o uma notifica��o 
                
            </form> 
        </div>
        <div class="modal-footer">
            <a href="#" class="btn" data-dismiss="modal">Cancelar</a>
            <a href="#" class="btn btn-primary">Enviar</a>
        </div>
    </div--%>
    
    <div>
        &nbsp;
    </div>
    
    <%--div style="height: 300px; overflow-y: scroll; overflow-x: hidden;">
            
    <c:if test="${not empty amizades}">
    
        <div class="horizontalSpacer"></div>
        
        <div class="row">
        
            <div id="listaAmigosFB" class="span15">
            
                <c:set var="letra" value="${'A'}" />
        
                <c:forEach items="${amizades}" var="amizade" varStatus="contador">
                    
                    <c:set var="letraAtual" value="${fn:substring(amizade.nomeAmigo, 0, 1)}" />
                    
                    <c:if test="${letraAtual != letra}">
                        <div class="span1">
                            <c:set var="letra" value="${letraAtual}" />
                            <a name="${letra}"> </a>
                        </div>
                    </c:if>
                    
                    <fan:card type="user" friendship="${amizade}" showActions="false" selectable="true" spanSize="5" />
                            
                </c:forEach>
                
                <div class="row span8" style="padding-top: 15px;">
                    <c:url var="urlAmigos" value="/amigos/${perfil.urlPath}" />
                    <fan:pagingButton targetId="listaAmigosFB" title="Ver mais amigos deste usu�rio" noMoreItensText="N�o h� mais amigos para serem exibidos." cssClass="offset4 span6" url="${urlAmigos}" />
                </div>
            </div>
            
            <div id="alphabet-list" class="span1">
                <div class="tabbable tabs-right">
                    <ul class="nav nav-tabs" style="padding-left: 4px; padding-right: 0px;">
                        
                        <li class="active">
                            <a href="#A" class="ordenacao-alfabetica-tab" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;">A</a>
                        </li>
                        <% 
                            for(char alphabet = 'B'; alphabet <= 'Z'; alphabet++) {
                        %>              
                        <li>
                            <a href="#<%=alphabet%>" style="min-width: 20px; max-width: 20px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; margin-bottom: 0px;"><%=alphabet%></a>
                        </li>
                        <%
                            }
                        %>
                    </ul>
                </div>
            </div>
            
        </div>
        
    </c:if>
    
    </div--%>
    
</fieldset>

<script>

$('#convidar-amigos').autoSuggest(amigos.items, {
    asHtmlID : 'amigosConvidados',
    selectedItemProp: 'name', 
    searchObjProps: 'name',
    startText: '',
    emptyText: 'Sem resultados',
    selectionAdded: function(elem) { 
        if(!$('#mensagem-amigosConvidados').is(":visible")) {
            $('#mensagem-amigosConvidados').show('blind', {}, 500);
        }
    },
    selectionRemoved: function(elem) {
        elem.remove();
        var value = $('#as-values-amigosConvidados').val();
        if (value == null || value == '') {
            $('#mensagem-amigosConvidados').hide('blind', {}, 500);
        }
    }        
});


//$(document).ready(function() {
    //$('.action-button').button('complete');

    //alert('0');
    
    /*var amigos = {items: [
    <c:forEach items="${amizades}" var="amizade" varStatus="contador">
        {value : '${amizade.id}', name : '${amizade.nomeAmigo}', idFacebook : '${amizade.idAmigoFacebook}'},
    </c:forEach>
    ]};
    
    alert('1')

    $('#convidar-amigos').autoSuggest(amigos.items, {
        asHtmlID : 'amigosConvidados',
        selectedItemProp: 'name', 
        searchObjProps: 'name',
        startText: '',
        emptyText: 'Sem resultados',
        selectionAdded: function(elem) { 
            if(!$('#mensagem-amigosConvidados').is(":visible")) {
                $('#mensagem-amigosConvidados').show('blind', {}, 500);
            }
        },
        selectionRemoved: function(elem) {
            elem.remove();
            var value = $('#as-values-amigosConvidados').val();
            if (value == null || value == '') {
                $('#mensagem-amigosConvidados').hide('blind', {}, 500);
            }
        }        
    });

    alert('2')
    
    $('#solicitar-dicas-amigos').autoSuggest(amigos.items, {
        asHtmlID : 'amigosSolicitados',
        selectedItemProp: 'name', 
        searchObjProps: 'name',
        startText: '',
        emptyText: 'Sem resultados',
        selectionAdded: function(elem) { 
            if(!$('#mensagem-amigosSolicitados').is(":visible")) {
                $('#mensagem-amigosSolicitados').show('blind', {}, 500);
            }
        },
        selectionRemoved: function(elem) {
            elem.remove();
            var value = $('#as_values_amigosSolicitados').val();
            if (value == null || value == '') {
                $('#mensagem-amigosSolicitados').hide('blind', {}, 500);
            }
        }

    });
    
    alert('3')

    $('#link-convidar').click(function (e) {
        e.preventDefault();
        
        var url = '<c:url value="/planoViagem/${viagem.id}/convidarAmigos" />';
        var mensagem = $('#mensagemAmigosConvidados').val();
        var listaAmigos = $('#as-values-amigosConvidados').val();

        convidarSolicitar(url, listaAmigos, mensagem); 
    });

    alert('4')
    
    $('#link-solicitar-dicas').click(function (e) {
        e.preventDefault();
        var url = '<c:url value="/planoViagem/${viagem.id}/solicitarDicas" />';
        var mensagem = $('#mensagemAmigosSolicitados').val();
        var listaAmigos = $('#as-values-amigosSolicitados').val();

        convidarSolicitar(url, listaAmigos, mensagem); 
    });
    
    alert('5')
    
    function convidarSolicitar(url, mensagem, amigos) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                mensagem : mensagem,
                idsAmizades : amigos
            },
            success: function(data) {
                
                if (data.error != null) {
                    
                } else {
                    var params = '';
                    if (data.params != null) {
                        params = data.params;
                    }
                    if (params.success) {
                        
                    }
                }
            }, failure : function() {
            }
        });
    }
    
    alert('6')*/
    
//});

</script>
