<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<div class="row">
	<div class="span9">
		<div class="page-header">
			<h1>${viagem.nome}</h1>
		</div>
    </div>
</div>
        
<div class="row">

    <div class="span3">
        <img id="profile_pic" class="moldura" src="${pageContext.request.contextPath}/resources/images/default-trip-image.jpg" width="150" height="150">
    </div>
    
    <div class="row">

    	<div class="span6">
            <h3 class="gray">
            <c:choose>
                <c:when test="${viagem.periodoPorDatas}">
                    ${viagem.periodoFormatado} (${viagem.quantidadeDias} dias)
                </c:when>
                <c:when test="${viagem.periodoPorQuantidadeDias}">
                    ${viagem.quantidadeDias} dias
                </c:when>
                <c:otherwise>
                    Não definido
                </c:otherwise>
            </c:choose>
            </h3>
    	</div>
        
        <div class="span6">
            <h3>
            <c:forEach items="${viagem.destinosViagem}" var="destinoViagem" varStatus="counter">
                <c:if test="${counter.index > 0}">
                ·
                </c:if>
                <a href="<c:url value="/locais/cidade/${destinoViagem.destino.urlPath}" />" target="_blank">${destinoViagem.destino.nome}</a>
            </c:forEach>
            </h3>
        </div>
        
        <div class="span4">
            ${viagem.descricao}
        </div>
    </div>
        
</div>

<div class="btn-toolbar">

    <div class="btn-group">
        <a href="<c:url value="/planoViagem/editarViagem?id=" />${viagem.id}" class="btn btn-warning ">
            <i class="icon-pencil icon-white"></i> Editar viagem
        </a>
    </div>

    <div class="btn-group">
        <a class="btn dropdown-toggle btn-primary" data-toggle="dropdown" href="#">
            <i class="icon-share-alt icon-white"></i> Compartilhar <span class="caret"></span>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a name="fb_share" type="icon_link" share_url="<c:url value="/planoViagem/detalharViagem/${viagem.id}" />" style="text-decoration: none;">
                    Facebook
                </a> 
            </li>
            <style type="text/css" media="screen">
              #custom-tweet-button a {
                padding: 2px 5px 2px 20px;
                background: url('http://a4.twimg.com/images/favicon.ico') 1px center no-repeat;
              }
            </style>
            <li>
                <div id="custom-tweet-button">
                    <a href="https://twitter.com/share?url=<c:url value="/planoViagem/detalharViagem/${viagem.id}" />" target="_blank" style="background: url('http://dev.twitter.com/sites/all/themes/twitter_commons/favicon.ico') 1px center no-repeat;" >
                        Twitter
                    </a>
                </div>
            </li>
            
        </ul>
    </div>
    
</div>

<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>
			
<div id="lista-atividades">

<c:forEach items="${atividadesView.gruposAtividades}" var="grupoAtividades" varStatus="count">

    <div class="horizontalSpacer"></div>
    
    <div class="accordion-group">
        <div class="accordion-heading collapsible">
            <span class="accordion-toggle">
                ${grupoAtividades.nome}
                <div class="header-actions">
                    <span>
                        <a href="#tiposAtividadePopup" class="incluir-atividades-link">
                            <i class="icon-plus icon-white"></i>Incluir atividades
                        </a>

                        <div style="display:none">
                            <div id="tiposAtividadePopup">
                                <%@include file="/WEB-INF/views/planoViagem/planejamento/escolhaTipoAtividade.jsp" %>
                            </div>
                        </div>
                        
                    </span>
                    <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-dia-${count.index}"></i>
                </div>
            </span>
        </div>
        <div id="group-dia-${count.index}" class="accordion-body collapse in" style="height: auto; ">
            <div class="accordion-inner">
            
                <c:forEach items="${grupoAtividades.atividadesView}" var="atividadeView">
                
                    <c:set var="atividade" value="${atividadeView.atividade}"></c:set>
                    
                    <c:choose>
                        <c:when test="${atividadeView.termino}">
                            <a name="TERMINO_${atividade.id}"> </a>
                        </c:when>
                        <c:otherwise>
                            <a name="INICIO_${atividade.id}"> </a>
                        </c:otherwise>
                    </c:choose>
                    
                    <div class="horizontalBar"></div>
                    
                    <div class="row">
                        <div class="span2">
                            
                            <%@include file="/WEB-INF/views/planoViagem/imagemAtividade.jsp" %>
                            
                            <c:choose>
                                <c:when test="${atividadeView.termino}">
                                    <h2><joda:format value="${atividade.horaFim}" style="-S" /></h2>
                                </c:when>
                                <c:otherwise>
                                    <h2><joda:format value="${atividade.horaInicio}" style="-S" /></h2>
                                </c:otherwise>
                            </c:choose>
                            
                        </div>

                        <%@include file="/WEB-INF/views/planoViagem/detalheAtividade.jsp" %>
                            
                        <div class="span1">
                            <div class="btn-group">
                                <a class="btn dropdown-toggle btn-warning btn-mini" data-toggle="dropdown" href="#">
                                    Opções <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <c:if test="${atividade.hospedagem}">
                                        <c:url value="/planejamento/hospedagem/editar/${atividade.id}" var="editarAtividadeUrl" />
                                    </c:if>
                                    <c:if test="${atividade.voo}">
                                        <c:url value="/planejamento/voo/editar/${atividade.id}" var="editarAtividadeUrl" />
                                    </c:if>
                                    <c:if test="${atividade.alimentacao}">
                                        <c:url value="/planejamento/restaurante/editar/${atividade.id}" var="editarAtividadeUrl" />
                                    </c:if>
                                    <c:if test="${atividade.visitaAtracao}">
                                        <c:url value="/planejamento/atracao/editar/${atividade.id}" var="editarAtividadeUrl" />
                                    </c:if>
                                    <c:if test="${atividade.aluguelVeiculo}">
                                        <c:url value="/planejamento/aluguelVeiculo/editar/${atividade.id}" var="editarAtividadeUrl" />
                                    </c:if>
                                    <li><a href="${editarAtividadeUrl}"><i class="icon-pencil"></i> Editar</a></li>
                                    <li><a data-toggle="modal" href="#excluir-atividade-modal-${atividade.id}"><i class="icon-trash"></i> Excluir</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
            
                    
                    <div class="modal fade" id="excluir-atividade-modal-${atividade.id}">
                        <div class="modal-header">
                            <a class="close" data-dismiss="modal">×</a>
                            <h3>Excluir atividade</h3>
                        </div>
                        <div class="modal-body">
                            <p>Esta atividade será removida permanentemente! Tem certeza?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#" class="btn" data-dismiss="modal">Cancelar</a>
                            <a href="<c:url value="/planejamento/atividade/excluir/${atividade.id}"/>" class="btn btn-primary">Confirmar</a>
                        </div>
                    </div>
                
                </c:forEach>
            </div>
        </div>
    </div>

</c:forEach>

</div>

<div class="horizontalSpacer"></div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        $(".incluir-atividades-link").fancybox({
            width: 500,
            height: 400
        });
        
        $('.modal').modal({
        	show: false
        });
        
        $('#lista-atividades').initToolTips();
    });
</script>
