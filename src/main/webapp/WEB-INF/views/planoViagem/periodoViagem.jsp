<%@page import="org.joda.time.DateTime"%>
<%@page import="java.util.Calendar"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.SituacaoViagem"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PlanoViagem.TipoPeriodoViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<c:set var="dataAtual" value="<%=DateTime.now()%>" />

<fieldset>

    <legend>
        <h3>
          <c:if test="${viagem.id == null}">
            Quando voc� ${verboPassado}? / Dura��o da Viagem
          </c:if>
          <c:if test="${viagem.id != null}">
            Per�odo / Dura��o da Viagem
          </c:if>
        </h3>
    </legend>
    
    <div>
        
        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
            <li>
                <label class="span1">
                </label>
                <span>
                    <form:radiobutton id="porDatas" path="tipoPeriodo" value="<%=TipoPeriodoViagem.POR_DATAS%>" />
                    <strong>Informar datas</strong>
                    <i id="help-nome" class="icon-question-sign helper" title="Selecione esta op��o quando voc� souber as datas de in�cio e t�rmino da viagem. "></i>
                </span>
                <span class="help-inline" style="color: #555; margin: 0 6px 0 4px;">De </span>
                <input type="text" name="dataInicio" value="<joda:format value="${viagem.dataInicio}" pattern="dd/MM/yyyy" />"
                       id="dataInicio" ${not viagem.periodoPorDatas ? 'disabled="disabled"' : ''}  />
                <span class="help-inline" style="color: #555; margin: 0 6px 0 2px;">a </span>
                <input type="text" name="dataFim" value="<joda:format value="${viagem.dataFim}" pattern="dd/MM/yyyy" />"  
                       id="dataFim" ${not viagem.periodoPorDatas ? 'disabled="disabled"' : ''}  />
            </li>
            <li class="hide error">
                <label class="span4"></label>
                <div class="span6 error-message"></div>
            </li>
            <li>
                <label class="span1">
                </label>
                <span style="margin-right: 6px;">
                    <form:radiobutton id="porQuantidadeDias" path="tipoPeriodo" value="<%=TipoPeriodoViagem.POR_QUANTIDADE_DIAS%>" />
                    <strong>Quantidade de dias</strong> 
                    <i id="help-nome" class="icon-question-sign helper" title="Selecione esta op��o quando voc� souber apenas a quantidade de dias de dura��o da viagem. "></i>
                </span>
                <form:input path="quantidadeDias" id="quantidadeDias" class="input-mini numbersOnly" disabled="${not viagem.periodoPorQuantidadeDias}" />
            </li>
            <li>
                <label class="span1">
                </label>
                <form:radiobutton id="naoDefinido" path="tipoPeriodo" value="<%=TipoPeriodoViagem.NAO_DEFINIDO%>" />
                <span>
                    <strong>N�o sei / Prefiro n�o informar</strong>
                    <i id="help-nome" class="icon-question-sign helper" title="Selecione esta op��o quando voc� n�o souber ou n�o desejar informar o per�odo. "></i>
                </span>
            </li>
        </ul>
        
    </div>

</fieldset>

<script>
    $(document).ready(function() {
        
        jQuery(function($){
           $("#quantidadeDias").mask("9");
        });
        
        $('#dataInicio').datepicker( {
            changeMonth: true,
            changeYear: true,
            //minDate: new Date('${dataAtual.year}/${dataAtual.monthOfYear}/${dataAtual.dayOfMonth}'),
            beforeShow: function() {
                $(this).dialog("widget").css("z-index", 100000);
            },
            onSelect: function( selectedDate ) {
            	$( "#dataFim" ).datepicker( "option", "minDate", selectedDate );
                $( "#dataFim" ).datepicker( "option", "defaultDate", selectedDate );
            }
        });
        
        $('#dataFim').datepicker( {
            defaultDate: new Date('${dataAtual.year}/${dataAtual.monthOfYear}/${dataAtual.dayOfMonth}'),
            changeMonth: true,
            changeYear: true,
            beforeShow: function() {
                $(this).dialog("widget").css("z-index", 100000);
            },
            onSelect: function(selectedDate) {
                $( "#dataInicio" ).datepicker( "option", "maxDate", selectedDate );
            }
        });
        
        $('.helper').tooltip();
        
        $('input[name="tipoPeriodo"]').bind('change', function () {
            var radio = $(this);
            $('#dataInicio').attr('disabled', (radio.val() != 'POR_DATAS'));
            $('#dataFim').attr('disabled', (radio.val() != 'POR_DATAS'));
            if (!$('#dataInicio').is(':disabled')) {
                $('#dataInicio').focus();
            }
            
            $('#quantidadeDias').attr('disabled', (radio.val() != 'POR_QUANTIDADE_DIAS'));
            if (!$('#quantidadeDias').is(':disabled')) {
                $('#quantidadeDias').focus();
            }
            
        });
        
        $('input[name="tipoPeriodo"]').bind('click', function () {
            $(this).change();
        });

    });
    
</script>