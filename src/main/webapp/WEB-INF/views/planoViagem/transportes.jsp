<%@page import="br.com.fanaticosporviagens.model.entity.TipoTransporte"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<script type="text/javascript">
 	      
    $(document).ready(function() {
    	
        
    });
       
</script>

<div class="offset2">
<div class="hidden tipoTransporte" id="<%= TipoTransporte.AVIAO.getCodigo() %>">
<%@include file="transporteVoo.jsp" %>
</div>

<div class="hidden tipoTransporte" id="<%= TipoTransporte.CARRO.getCodigo() %>">
<%@include file="transporteCarro.jsp" %>
</div>

<div class="hidden tipoTransporte" id="<%= TipoTransporte.NAVIO.getCodigo() %>">
<%@include file="transporteNavio.jsp" %>
</div>

<div class="hidden tipoTransporte" id="<%= TipoTransporte.ONIBUS.getCodigo() %>">
<%@include file="transporteOnibus.jsp" %>
</div>

<div class="hidden tipoTransporte" id="<%= TipoTransporte.TREM.getCodigo() %>">
<%@include file="transporteTrem.jsp" %>
</div>
</div>