<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
  <head>
    
    <style>
      .ui-jcoverflip {
        position: relative;
      }
      
      .ui-jcoverflip--item {
        position: absolute;
        display: block;
      }
      
      /* Basic sample CSS */
      #flip {
        height: 60px;
        width: 630px;
        margin-bottom: 50px;
      }
      
      #flip .ui-jcoverflip--title {
        position: absolute;
        bottom: -30px;
        width: 100%;
        text-align: center;
        color: #555;
      }
      
      #flip img {
        display: block;
        border: 0;
        outline: none;
      }
      
      #flip a {
        outline: none;
      }
      
      #wrapper {
        width: 630px;
        position: relative;
      }
      
      .ui-jcoverflip--item {
        cursor: pointer;
      }
      
      body {
        font-family: Arial, sans-serif;
        width: 630px;
        padding: 0;
        margin: 0;
      }
      
      ul,
      ul li {
        margin: 0;
        padding: 0;
        display: block;
        list-style-type: none;
      }
      
      #scrollbar {
        position: absolute;
        left: 20px;
        right: 20px;
        
      }

    </style>
    
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-1.7.1.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.jcoverflip.js"></script>
    
    <script>
        jQuery( document ).ready( function() {
            
            jQuery('#flip').jcoverflip({
                beforeCss: function( el, container, offset ) {
                    return [
                        $.jcoverflip.animationElement( el, { left: ( container.width( )/2 - 210 - 110*offset )+'px', bottom: '20px' }, { } ),
                        $.jcoverflip.animationElement( el.find( 'img' ), { opacity: 0.5, width: '100px' }, {} )
                    ];
                },
                afterCss: function( el, container, offset ) {
                    return [
                        $.jcoverflip.animationElement( el, { left: ( container.width( )/2 + 30 + 110*offset )+'px', bottom: '20px' }, { } ),
                        $.jcoverflip.animationElement( el.find( 'img' ), { opacity: 0.5, width: '100px' }, {} )
                    ];
                },      
                currentCss : function( el, container ) {
                    return [
                        $.jcoverflip.animationElement( el, { left: ( container.width( )/2 - 100 )+'px', bottom: 0 }, { } ),
                        $.jcoverflip.animationElement( el.find( 'img' ), { opacity: 1, width: '120px' }, { } )
                    ];
                }//,
                //change: function(event, ui){
                    //jQuery('#scrollbar').slider('value', ui.to*25);
                    // TODO trocar o dia no painel de atividades selecionadas
                //}
            });
            
            
            $('.droppable').mouseover(function() {
                var li = $(this);
                
                clearTimeout($('#flip').data('timeout'));
                
                li.css('background-color', '#f11');
                var timeout = setTimeout(function() { 
                    li.click();
                }, 400);
                $('#flip').data('timeout', timeout);
            });

            $('.droppable').mouseout(function() {
                clearTimeout($('#flip').data('timeout'));
                
                $(this).css('background-color', 'transparent');
            });

            $('.droppable').droppable({
                accept: '#painelAtracoes > li',
                activeClass: 'ui-state-highlight',
                drop: function( event, ui ) {
                    removerDoPainelAtracoes( ui.draggable );
                }
            });
            
        });
      
    </script>
        
  </head>
  <body>
    <%-->div id="wrapper">
    <ul id="flip">
      <li title="The first image" ><img src="http://www.jcoverflip.com/files/docs/jcoverflip-demo/1.png" /></li>
      <li title="A second image" ><img src="http://www.jcoverflip.com/files/docs/jcoverflip-demo/2.png" /></li>
      <li title="This is the description" ><img src="http://www.jcoverflip.com/files/docs/jcoverflip-demo/5.png" /></li>
      <li title="Another description" ><img src="http://www.jcoverflip.com/files/docs/jcoverflip-demo/4.png" /></li>
      <li title="A title for the image" ><img src="http://www.jcoverflip.com/files/docs/jcoverflip-demo/3.png" /></li>
      
    </ul>
    <div id="scrollbar"></div>
    </div--%>
    
    <div id="wrapper">

        <ul id="flip">
            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        1� Dia
                    </div>
                    <br>
                </div>            
            </li>

            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        2� Dia
                    </div>
                    <br>
                </div>            
            </li>
            
            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        3� Dia
                    </div>
                    <br>
                </div>            
            </li>

            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        4� Dia
                    </div>
                    <br>
                </div>            
            </li>

            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        5� Dia
                    </div>
                    <br>
                </div>            
            </li>

            <li class="droppable">
                <div style="position:relative; background-color: transparent;">
                    <img src="<c:url value="/resources/images/bnd.gif"/>" height="60" style="height:0px;left:1px;position:absolute;top:3px;background-color: transparent;">
                    <div style="font-size:120%;font-weight:bold;left:6px;position:absolute;top:-20px;background-color: transparent; width: 200px;">
                        6� Dia
                    </div>
                    <br>
                </div>            
            </li>
        </ul>
        
    </div>    
  </body>
</html>