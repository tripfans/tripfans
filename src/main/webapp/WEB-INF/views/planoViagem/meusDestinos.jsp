<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

     <c:url value="/planoViagem/atualizarDestinosViagem" var="salvar_url" />
     	<form:form id="destinosForm" action="${salvar_url}" modelAttribute="viagem">
     		<input type="hidden" name="viagem" value="${viagem.id}" />
			<!-- Esse � o conjunto de campos para incluir um destino. Tamb�m � utilizado na p�gina -->
			<!-- de edi��o de viagens.-->
			<%@include file="destinos.jsp" %>
			<!--  -->
			<div style="width: 100%">
			       <div id="error" style="display: none;" class="alert alert-error">
			           <p id="errorMsg">
			           </p>
			       </div>
			       <div id="success" style="display: none;" class="alert alert-success">
			           <p id="successMsg">
			           </p>
			       </div>
			  	<div align="right">
			        <div class="form-actions">
			            <input type="submit" value="Salvar altera��es" class="btn btn-primary" />
			        </div>
			    </div>
			</div> 
			<!--  -->
		</form:form>
		<script type="text/javascript">
	jQuery(document).ready(function() {
		$("#destinosForm").unbind("submit");
	    
	    $("#destinosForm").submit(function(event) {
	    	configurarDestinos();
	        $("#error").hide();
	        $("#success").hide();
	        
	        /* stop form from submitting normally */
	        event.preventDefault();
	        
	        var formData = $('#destinosForm').serialize();
	        
	        if ($("#destinosForm").valid()) {
	        
	            $.ajax({
	                type: 'POST',
	                url: document.forms['destinosForm'].action,
	                data: formData,
	                success: function(data) {
	                    if (data.error != null) {
	                        $("#errorMsg").empty().append(data.error);
	                        $("#error").show();
	                    } else {
	                        var params = '';
	                        if (data.params != null) {
	                            params = data.params;
	                        }
	                        
	                        $("#successMsg").empty().append(data.success);
	                        
	                        $("#success").fadeIn(1000);
	                    }
	                }
	            });
	        }
	        
	    });
	    $('.remover').bind('click', function(event) {
  	      	event.preventDefault();
  	       var idCidade = $(this).attr('id');
  	       $('#cidade-' + idCidade).remove();
  	   });
	});
</script>
