<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib tagdir="/WEB-INF/tags/viagem" prefix="viagem" %>


<script type="text/javascript">

	jQuery(document).ready(function() {
		
	    
	 	
	});


</script>

    
<c:url value="/planejamento/salvarTransporteNavio" var="salvar_url" />

<form:form id="navioForm" action="${salvar_url}" modelAttribute="voo" method="post">

    <input type="hidden" name="voo" value="${voo.id}" />
    <input type="hidden" name="viagem" value="${viagem.id}" />
    
    <fieldset>
		<div class="page-header"><h3>Informações do Navio</h3></div>
		<div class="control-group">
		    <label for="codigoReserva">
		        <span>Nome do navio</span>
		        <i id="help-codigo" class="icon-question-sign helper" title="Nome do navio"></i>
		    </label>
		    <form:errors cssClass="error" path="codigoReserva" element="label"/>
		    <div class="controls">
		        <form:input id="codigoReserva" path="codigoReserva" cssClass="span4" />
		    </div>
		</div>
		
		<div class="control-group">
		    <label for="codigoReserva">
		        <span>Empresa</span>
		        <i id="help-codigo" class="icon-question-sign helper" title="Nome do navio"></i>
		    </label>
		    <div class="controls">
		         <fan:autoComplete url="/locais/consultarEmpresasNavio" name="trechos[${indice}].chegada.nomeLocal"
		          value="${voo.trechos[indice].chegada.nomeLocal}" minLength="3"
		          hiddenName="trechos[${indice}].chegada.local" id="aeroportoDestino-${indice}" 
		          valueField="id" labelField="nome" cssClass="span7" 
		          hiddenValue="${voo.trechos[indice].chegada.local.id}" limit="10"
		          showActions="true" blockOnSelect="true"
		          jsonFields="[{name : 'id'}, {name : 'nome'}]"
		          placeholder="Consultar empresas navio..." />
		    </div>
		</div>
    </fieldset>
    <div class="horizontalSpacer"></div>
    <fieldset>
		<div class="page-header"><h3>Informações do Itinerário</h3></div>
        <div id="group-trechos" class="accordion-body collapse in" style="height: auto; ">
			<div id="lista-trechos">
                    <c:forEach items="${voo.trechos}" var="trecho" varStatus="counter">
                        <jsp:include page="/WEB-INF/views/planoViagem/trechoNavio.jsp">
                            <jsp:param name="indice" value="${counter.index}" />
                            <jsp:param name="quantidadeDias" value="${viagem.quantidadeDias}" />
                            <jsp:param name="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}" />
                        </jsp:include>
                        <c:set var="indice" value="${counter.index}"></c:set>
                        <c:set var="quantidadeDias" value="${viagem.quantidadeDias}"></c:set>
                        <c:set var="tipoPeriodo" value="${viagem.tipoPeriodo.codigo}"></c:set>
                    </c:forEach>
			</div>
			<a id="adicionar-trecho" class="btn btn-primary btn-mini" href="#" indice-trecho="${indice + 1}" tipo-periodo="${tipoPeriodo}" quantidade-dias="${quantidadeDias}">
			    <i class="icon-plus icon-white"></i> Adicionar novo trecho
			</a>
		</div>
    </fieldset>
    <div class="horizontalSpacer"></div>
    <div align="right">
        <s:message code="label.acoes.cancelar" var="labelBotaoCancelar" />
        <a id="botaoCancelarNavio" class="botaoCancelarTransporte btn btn-danger" href="<c:url value="/planoViagem/detalharViagem?id=${viagem.id}#INICIO_${hospedagem.id}" />">
            <i class="icon-remove icon-white"></i> ${labelBotaoCancelar}
        </a>                                <s:message code="label.acoes.salvarAlteracoes" var="labelBotaoSalvar" />
        <a id="botaoSalvar" class="btn btn-primary" href="#">
            <i class="icon-ok icon-white"></i> Salvar Informações
        </a>
    </div>
</form:form>