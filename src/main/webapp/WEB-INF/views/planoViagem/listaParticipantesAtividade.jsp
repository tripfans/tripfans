<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${not empty viagem.participantes}">

    <div class="row">
    
        <div class="span6">
        
            <table class="table table-striped">
            
                <c:forEach items="${viagem.participantes}" var="participante">
                
                    <tr>
                        <td>
                            ${participante.nome}
                        </td>
                    </tr>
                    
                </c:forEach>
            
            </table>
        
        </div>
        
    </div>
    
</c:if>
