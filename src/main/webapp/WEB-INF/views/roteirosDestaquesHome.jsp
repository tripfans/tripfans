<%@page  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.reveal_container.tofullwidth {
    visibility: hidden;
}

.reveal_container .reveal_wrapper {
    padding: 0px 20px 20px;
}
</style>
<div class="borda-arredondada" style="padding: 15px; display: inline-block; margin-left: 0px; position: relative; width: 97%; height: 450px;">
    <div>
        <div id="roteiros-container" class="showbiz-container whitebg sb-retro-skin" style="padding-bottom: 0px;">

            <!-- THE NAVIGATION -->
            <div class="showbiz-navigation sb-nav-retro hidden-xs">
                <a id="showbiz_left" class="sb-navigation-left"><i class="sb-icon-left-open"></i></a>
                <%-->a id="showbiz_play" class="sb-navigation-play"><i class="sb-icon-play sb-playbutton"></i><i class="sb-icon-pause sb-pausebutton"></i></a--%>                    
                <a id="showbiz_right" class="sb-navigation-right"><i class="sb-icon-right-open"></i></a>
                <div class="sbclear"></div>
            </div> <!-- END OF THE NAVIGATION -->

            <div class="divide10"></div>

            <!--    THE PORTFOLIO ENTRIES   -->
            <div class="showbiz" data-left="#showbiz_left" data-right="#showbiz_right" data-play="#showbiz_play">

                <!-- THE OVERFLOW HOLDER CONTAINER, DONT REMOVE IT !! -->
                <div class="overflowholder">
                    <!-- LIST OF THE ENTRIES -->
                    <ul id="ul-list-roteiros">
                    
                      <c:forEach items="${roteirosDestaques}" var="roteiro">

                        <c:set var="urlAutor" value="/perfil/${roteiro.idAutor}" />
                        <%-- Se for o perfil do usuario TripFans, remover link --%>
                        <c:if test="${roteiro.idAutor == 1}">
                          <c:set var="urlAutor" value="#" />
                        </c:if>
                        <li class="sb-retro-skin" style="cursor: pointer;">
                            <!-- THE MEDIA HOLDER -->
                            <div class="mediaholder conteudoRoteiro" style="border: 0px;" data-urlroteiro="<c:url value="/viagem/${roteiro.idRoteiro}" />">
                                <div class="mediaholder_innerwrap borda-arredondada">
                                    <i class="carrossel-thumb carrossel-home-img" data-foto-id="${roteiro.idRoteiro}" data-foto-url-big="<c:url value="${roteiro.urlFotoRoteiro}"/>"
                                       style="background-image: url('<c:url value="${roteiro.urlFotoRoteiro}"/>'); min-height: 200px;"></i>
                                    <!-- HOVER COVER CONAINER -->
                                    <div class="hovercover borda-arredondada" style="z-index: 21;">
                                        <a rel="group">
                                            <div class="linkicon" style="cursor: pointer;"><i class="sb-icon-plus morebutton" data-urlroteiro="<c:url value="/viagem/${roteiro.idRoteiro}" />"></i></div>
                                        </a>
                                    </div><!-- END OF HOVER COVER -->
                                </div>
                            </div><!-- END OF MEDIA HOLDER CONTAINER -->

                            <!-- DETAIL CONTAINER -->
                            <div class="detailholder" style="margin-top: 10px;">
                                <fan:userAvatar displayName="false" user="${roteiro.autor}"  marginLeft="0" showFrame="false" />
                                <p style="min-height: 50px; font-family: 'PT Sans Narrow', sans-serif;">
                                    <strong class="local-card-title" style="padding-top: 0px; margin-bottom: 0px;">
                                      <a href="<c:url value="/viagem/${roteiro.idRoteiro}" />" style="text-decoration: none;">
                                        ${roteiro.tituloRoteiro}
                                      </a>
                                    </strong>
                                    <small>
                                        por
                                        <a href="<c:url value="${urlAutor}" />">
                                            ${roteiro.nomeAutor}
                                        </a>
                                    </small>
                                </p>
                            </div><!-- END OF DETAIL CONTAINER -->
                            
                        </li>
                      </c:forEach>
                    </ul>
                    <div class="sbclear"></div>
                </div> <!-- END OF OVERFLOWHOLDER -->
                <div class="sbclear"></div>
            </div>
        </div>
        
    </div>
    
</div>