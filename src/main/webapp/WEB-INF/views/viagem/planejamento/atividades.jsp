<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<style>

label {
    font-weight: normal;
}

.label {
    font-weight: normal;
}

.header-panel-group {
    display: inline;
    min-width: 180px;
}

.label-horario-atividade {
    margin-left: -81px; margin-right: -20px;
}

.icone-atividade {
    position: absolute;
    z-index: 1;
    top: 5px; 
    left: 5px;
}

</style>

<div id="secao-atividades" class="col-md-12">
    <div class="bs-docs-section">
        <div class="page-header">
          <h3 id="overview" class="font-normal">Atividades</h3>
        </div>
        <p class="lead"></p>
    </div>
    
    <div class="row well well-sm" style="margin-left: 0; margin-right: 0;">
        <div style="margin-top: 4px;">
            <label for="selectFiltroTipoAtividade" class="col-sm-1 control-label" style="margin-top: 10px;">Mostrar</label>
            <div class="col-sm-6">
                <select name="tipo" id="selectFiltroTipoAtividade" class="form-control ddslick-atividade permanente">
                  <option value="0" data-imagesrc="<c:url value="/resources/images/info.png"/>" data-description=" ">Tudo</option>
                  <c:forEach var="tipoAtividade" items="${tiposAtividade}">
                    <c:url var="imageUrl" value="/resources/images/${tipoAtividade.icone}-medium.png"/>
                    <option value="${tipoAtividade}" data-imagesrc="${imageUrl}" 
                                 data-description=" ">
                        ${tipoAtividade.descricao}
                    </option>
                  </c:forEach>
                </select>
            </div>
        </div>
    </div>
    
    <%  
      TipoAtividade[] tiposAtividade = TipoAtividade.listaOrdenada();
      pageContext.setAttribute("tiposAtividade", tiposAtividade);
    %>
    
    <fieldset style="background-color: #fff">
        
        <div style="margin-bottom: 6px;">
            <a id="toggle-all" href="#" class="btn btn-default btn-xs toggle-all" data-toggle="button">
                <span class="glyphicon glyphicon-chevron-down"></span>
                <span class="text">Expandir todos</span>
            </a>
        </div>
        
        <c:forEach var="tipoAtividade" items="${tiposAtividade}">
            <c:set var="atividadesPorTipo" value="${viagem.getAtividadesPorTipoOrdenadas(tipoAtividade)}" />
            <%@include file="painelAtividadesPorTipo.jsp" %>
        </c:forEach>
        
        <br/>
     </fieldset>
    
</div>

<script>

$('.ddslick-atividade').ddslick({
    width: '100%',
    onSelected: function(data) {
        if (data.selectedData.value == '0') {
            $('.panel-group').show();
        } else {
            $('.panel-group').hide();
            $('#panel-group-' + data.selectedData.value).show();
            var panel = $('#panel-group-' + data.selectedData.value).find('a[data-toggle="collapse"]').attr('href');
            $(panel).collapse('show');
        }
    }
});

function recarregarAtividades() {
    carregarPagina('atividades');
}

</script>
</compress:html>