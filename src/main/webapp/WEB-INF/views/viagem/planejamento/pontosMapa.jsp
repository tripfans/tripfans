<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

    <c:forEach var="atividade" items="${viagem.todasAtividadesViagem}" varStatus="status">
      <c:if test="${not empty atividade.local and not empty atividade.local.latitude}"> 
     {
         "number": ${status.index},
         "id": "${atividade.id}",
         "nome": "${atividade.local.nome}",
         "urlPath": "${atividade.local.urlPath}",
         "latitude": ${atividade.local.latitude},
         "longitude": ${atividade.local.longitude},
         "urlFotoSmall": "${atividade.local.urlFotoSmall}",
         "notaTripAdvisor": "${fn:replace(atividade.local.notaTripAdvisor, '.', '') }",
         "quantidadeReviewsTripAdvisor": "${atividade.local.quantidadeReviewsTripAdvisor}",
         "endereco": "${atividade.local.endereco.descricao}",
         "tipo": "${atividade.tipo}", 
         "icone": "<c:url value="/resources/images/markers/${atividade.tipo.icone}-medium-pin.png"/>",
         "idDia": ${not empty atividade.dia ? atividade.dia.id : 0},
     },
     </c:if>
    </c:forEach>
