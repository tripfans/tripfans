<%@page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.mm-toggle {
    cursor: pointer;
}

.mm-menu .mm-list {
	padding-top: 0px;
}

li.mm-label {
	padding-top: 0px;
}

.mm-list > li.mm-label {
	font-size: 11px;
}

.mm-list > li.mm-spacer.mm-label {
	padding-top: 0px;
}

.mm-list>li.mm-spacer.mm-label:first-child {
	padding-top: 15px;
}

#locations li {
    cursor: pointer;
}

.media {
    margin-top: 1px; 
    margin-bottom: 0px;
    padding: 4px;
    /*border-top: 1px solid #eee;
    border-bottom: 1px solid #eee;*/
}

.media-body {
    width: 100%;
}

.media-heading {
    margin-bottom: 0px;
}

.card {
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.card.selectable {
    border-top: 0px;
}
    
.card.selectable:hover {
    cursor: pointer;
    border: 1px solid #A3D6A3;
    border-left: 0px;
    border-right: 0px;
    margin-left: 0;
    margin-top: -1px;
    margin-bottom: -2px;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-right: 6px;
    background-color: #F7F7F7;
    /*margin-top: -1px;*/    
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.card.selected, .card.selected:hover {
    border: 1px solid #A3D6A3;
    border-left: 0px;
    border-right: 0px;
    margin-left: 0;
    margin-top: -1px;
    margin-bottom: -2px;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-right: 8px;
    background-color: #E9FAE9;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.icon-selectable {
    float: right; 
    color: #D3D2D2;
    padding-right: 15px;
}

.card.selectable:hover .icon-selectable {
    color: #83CD83;
}

.card.selected .icon-selectable, .card.selected:hover .icon-selectable {
    color: #5cb85c;
    /*padding-right: 17px;*/
}	

li a.tab-amigo {
	cursor: pointer!important;
	opacity:0.50;
    -moz-opacity: 0.50;
    filter: alpha(opacity=50);
}

.nav-tabs > li.active > a.tab-amigo, li a.tab-amigo:hover  {
    /*border-color: transparent;*/
    /*border-bottom: 1px solid #ddd;*/
}

li a.tab-amigo:hover, li.active a.tab-amigo {
	filter: none;
    -moz-opacity: 1;
    opacity:1;
    -webkit-filter: none; /* Chrome 19+ & Safari 6+ */
}

@media (min-width: 992px) {
	div.well.affix {
		padding-left: 20%!important; 
		padding-right: 20%!important;
	}
	
	div.fixed-top-bar.affix {
		top: 70px;
	}
	
}

@media (max-width: 767px) {
	div.fixed-top-bar.affix {
		top: 40px;
	}
}

</style>

<script>
    jQuery(document).ready(function() {
    	
    	$(document).off('click', 'a.btn-adicionar-plano');
    	
    	$(document).on('click', 'a.btn-adicionar-plano', function (e) {
            e.preventDefault();
            
            var $card = $(this).closest('div.card');

            var idLocal = $card.data('local-id');
            var idSugestaoLocal = $card.data('sugestao-local-id');
            var urlLocal = $card.data('local-url');
            var url = '';
            
            url = '<c:url value="/viagem/adicionarLocalEmViagem/${viagem.id}/${not empty diaInicio ? diaInicio : 0}"/>/' + urlLocal;
            
            var request = $.ajax({
                url: url,
                type: 'POST',
                data: {
                	idSugestaoLocal: idSugestaoLocal
                }
            });
            request.done(function(data) {
            	$card.fadeOut(1000);
            });
            request.fail(function(jqXHR, textStatus){
            });
            
        });
    	
    	/*$(document).on('click', 'a.btn-dispensar-plano', function (e) {
            e.preventDefault();
            
            var $card = $(this).closest('div.card');

            var idLocal = $card.data('local-id');
            var idSugestaoLocal = $card.data('sugestao-local-id');
            var urlLocal = $card.data('local-url');
            var url = '';
            
            url = '<c:url value="/viagem/dispensarLocalSugerido/"/>/' + urlLocal;
            
            var request = $.ajax({
                url: url,
                type: 'POST',
                data: {
                	idSugestaoLocal: idSugestaoLocal
                }
            });
            request.done(function(data) {
            	$card.fadeOut(1000);
            });
            request.fail(function(jqXHR, textStatus){
            });
            
        });*/
        
        $('#btn-pedir-ajuda').click(function(e) {
            e.preventDefault();
			$('#modal-pedir-ajuda-body').load('<c:url value="/viagem/${viagem.id}/pedirAjudaAmigos"/>', function() {
				$('#modal-pedir-ajuda').modal('show');
	        });
    	});
        
        $(document).off('click', 'div.usercard-inner.selectable');
        
        $(document).on(
				'click',
				'div.usercard-inner.selectable',
				function(e) {
					$(this).toggleClass('selected');

					// habilitar botao mover
					var show = false;

					$('#div-amigos').find('div.usercard-inner')
							.each(
									function(index) {
										if ($(this).hasClass(
												'selected')) {
											show = true;
										}
									});
					if (show) {
						$('.btn-enviar-solicitacoes').css(
								'display', '');
					} else {
						$('.btn-enviar-solicitacoes').css(
								'display', 'none');
					}
				});
    	
    });
</script>

<c:if test="${not empty mapaSugestoesAmigos}">

	<div align="center" style="margin-top:10px; margin-bottom: 10px; margin-left: 30%; margin-right: 30%;">
		<button id="btn-pedir-ajuda" type="button" class="btn btn-primary btn-block btn-sm">
			<span class="glyphicon glyphicon-question-sign"></span>
			Pedir ajuda para mais amigos
		</button>
	</div>

</c:if>

<c:choose>
	<c:when test="${not empty mapaSugestoesAmigos and mapaSugestoesAmigos.size() > 1}">
	
		<div style="font-size: 15px; margin-bottom: 5px;">
			Amigos que você solicitou ajuda:
		  <small class="pull-right">
			<span class="glyphicon glyphicon-ok" style="color: #4cae4c;"></span> Enviou sugestões
		  </small>
			
		</div>
	
		<div style="border-top: 1px solid #eee;">
			<ul id="tabs-sugestoes-amigos" class="nav nav-tabs" role="tablist" style="margin-top: 10px;">
				<c:forEach var="sugestoesAmigo" items="${mapaSugestoesAmigos}" varStatus="status">
				    <li ${status.first ? 'class="active"' : ''}>
						<a id="btn-tab-amigo-${sugestoesAmigo.key.id}" href="#tab-amigo-${sugestoesAmigo.key.id}" role="tab" data-toggle="tab" class="tab-amigo" style="  padding-right: 0px;">
				        	<c:if test="${not idsUsuariosEnviaramPedidos.contains(sugestoesAmigo.key.id)}">
				        	<span class="glyphicon btn-lg" style="padding: 0px;">&nbsp;</span>
				        	</c:if>
						    <c:set var="qtdSugestoes" value="${sugestoesAmigo.value.size()}" />
						    <c:if test="${idsUsuariosEnviaramPedidos.contains(sugestoesAmigo.key.id)}">
					        	<span class="glyphicon glyphicon-ok btn-lg" title="Enviou ${qtdSugestoes} ${qtdSugestoes == 1 ? ' sugestão' : ' sugestões'}" 
					        	      style="color: #4cae4c; position: relative; top: -15px; left: -5px; padding: 0px; z-index: 1000;"></span>
					        	<span class="label label-success" style="position: relative; left: -24px;" title="Enviou ${qtdSugestoes} ${qtdSugestoes == 1 ? ' sugestão' : ' sugestões'}">
					        		${qtdSugestoes}
					        	</span>
				        	</c:if>
							<fan:userAvatar user="${sugestoesAmigo.key}" displayName="false" width="30" height="30" showImgAsCircle="true"
			                                showFrame="false" showShadow="false" imgStyle="margin-top: -10px;" enableLink="false" displayNameTooltip="true" />
						</a>
					</li>
				</c:forEach>
			</ul>
		</div>
		
	</c:when>
	<c:otherwise>
		<hr/>
	</c:otherwise>
</c:choose>

<div class="tab-content" style="margin-bottom: 50px;">
	<c:choose>
	  <c:when test="${not empty mapaSugestoesAmigos}">
		<c:forEach var="sugestoesAmigo" items="${mapaSugestoesAmigos}" varStatus="status">
			<div class="tab-pane ${status.first ? 'active' : ''}" id="tab-amigo-${sugestoesAmigo.key.id}" style="background-color: #fff;">
			
				<div style="margin-top: 15px; text-align: center; display: block;">
					<fan:userAvatar user="${sugestoesAmigo.key}" displayName="false" width="30" height="30" showImgAsCircle="true"
	                                showFrame="false" showShadow="false" imgStyle="margin-top: -10px;" enableLink="false" displayNameTooltip="true"
	                                useImgFloat="true" imgFloat="none" />
	                <c:choose>
	                  <c:when test="${idsUsuariosEnviaramPedidos.contains(sugestoesAmigo.key.id)}">
						<h4 style="color: #337ab7;">
							Sugestões de
			                <strong>${sugestoesAmigo.key.displayName}</strong>:
		                </h4>
		              </c:when>
		              <c:otherwise>
						<h4 style="color: #337ab7;">
			                <strong>${sugestoesAmigo.key.displayName}</strong>
			                <br/>
			                ainda não enviou sugestões para sua Viagem.
		                </h4>
		              </c:otherwise>
		            </c:choose>
	                <br/>
				</div>
			
				<fan:listaLocais locaisEmDestaque="${sugestoesAmigo.value}" urlProximos="/"
			                     photoHeight="120" photoSize="small" cardOrientation="horizontal" columns="2" columnSize="6" 
			                     selectable="true" showActions="false" showAdditionalInfo="false"
			                     showAddToPlanButton="false" showNotaGeral="false" showViajantesJaForam="false" 
			                     showAmigosJaForam="false" showReservarBooking="false" showBotaoAdicionar="true"
			                     style="${not isMobile ? 'padding-top: 0px;' : ''}" photoStyle="margin-left: 0px;"
			                     cardStyle="${isMobile ? 'padding-right: 0px; padding-left: 0px;' : ''}; border: 1px solid #eee;"
			                     titleStyle="font-size: .8em;" />			
			</div>
		</c:forEach>
	  </c:when>
	  <c:otherwise>
	  
	  	<div style="margin-top: 15px; margin-bottom: 50px;">
	  
            <div class="alert alert-info alert-dismissible fade in" role="alert" style="margin-bottom: 0; text-align: center">
		    	<h4>Você ainda não recebeu sugestões de seus amigos para sua Viagem.</h4>
		    	<div align="center" style="margin-top:15px; margin-left: 30%; margin-right: 30%;">
		    		<button id="btn-pedir-ajuda" type="button" class="btn btn-primary btn-lg btn-block">
		    			<span class="glyphicon glyphicon-question-sign"></span>
		    			Pedir ajuda para meus amigos
		    		</button>
		    	</div>
			</div>
			
		</div>
	  	
	  </c:otherwise>
	</c:choose>
</div>

<div id="modal-pedir-ajuda" class="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" style="display: inline;">Peça ajuda de seus amigos</h4>
            <button class="btn btn-success btn-enviar-solicitacoes pull-right" style="display: none; margin-right: 15px;">
                <span class="glyphicon glyphicon-send"></span>
                Enviar pedidos
            </button>
          </div>
          <div id="modal-pedir-ajuda-body" class="modal-body">
          </div>
          <div class="modal-footer">
            <button id="" class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                <span class="glyphicon glyphicon-remove"></span>
                Fechar
            </button>
            <button class="btn btn-success btn-enviar-solicitacoes" style="display: none;">
                <span class="glyphicon glyphicon-send"></span>
                Enviar pedidos
            </button>
            
          </div>
        </div>
    </div>
</div>