<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<style>
@media (min-width: 992px) {
    img.img-circle {
        min-width: 95px;
        max-width: 95px;
        height: 95px;
    }
}

@media (max-width: 991px) {
    img.img-circle {
        min-width: 68px;
        max-width: 68px;
        height: 68px;
    }
}
</style>

<div class="col-md-12">
    <div class="bs-docs-section">
        <div class="page-header">
          <h3 id="overview" class="font-normal">Destinos principais</h3>
        </div>
        <p class="lead"></p>
        
        <c:forEach var="destinoViagem" items="${viagem.destinosViagem}">
        
        <div class="row" style="padding-bottom: 10px;">
          <div class="col-xs-2 col-sm-2 col-md-2" align="center">
            <c:choose>
              <c:when test="${destinoViagem.destino.fotoPadraoAnonima}">
                <img src="<c:url value="${destinoViagem.destino.urlFotoAlbum}"/>" class="img-circle img-responsive" />
              </c:when>
              <c:otherwise>
                <img src="<c:url value="${destinoViagem.destino.urlFotoAlbum}"/>" class="img-circle img-responsive" />
              </c:otherwise>
            </c:choose>
          </div>
          <div class="col-xs-9 col-md-9">
            <h4 class="font-normal">
                <%-- fan:localLink local="${destinoViagem.destino}">${destinoViagem.destino.nome}</fan:localLink--%>
                <a href="#">${destinoViagem.destino.nome}</a> 
                <br/>
                <small>${destinoViagem.destino.localizacaoCompleta}</small>
            </h4>
          </div>
        </div>

        </c:forEach>
        
        <%-- div class="row">
          <div class="col-xs-2 col-sm-2 col-md-2" align="center">
            <img src="<c:url value="/resources/images/icons/mini/128/Interface-63.png"/>" class="img-circle img-responsive" />
          </div>
          <div class="col-xs-9 col-md-9">
            <h4>
                <small>Adicionar destino</small>
            </h4>
          </div>
        </div--%>
        
        <br/>
    </div>
</div>
</compress:html>