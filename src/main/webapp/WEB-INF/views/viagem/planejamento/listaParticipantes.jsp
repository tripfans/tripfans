<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<ul class="list-group">

  <c:forEach var="participante" items="${participantes}">
    <li id="li-participante-${participante.id}" class="list-group-item" style="padding-top: 5px; padding-bottom: 0px">
      <c:if test="${not empty participante.usuario.id}">
        <c:set var="nome" value="${participante.usuario.nome}"/>
        <fan:userAvatar user="${participante.usuario}" enableLink="false" showImgAsCircle="true" showFacebookMark="false"
                  		marginLeft="0" displayName="false" displayNameTooltip="false" width="30" height="30" 
                  		showFrame="false" showShadow="false" />
      </c:if>
      <c:if test="${empty participante.usuario.id}">
        <div style="float: left; margin-left:0px; margin-right: 5px;"> 
          <div style="margin-bottom: 5px; position: relative;"> 
		    <img class="img-circle" src="<c:url value="/resources/images/anonymous.png"/>" style="background-color: #FEFEFE; " width="30" height="30">      
          </div> 
        </div>
        <c:set var="nome" value="${participante.email}"/>
      </c:if>
        <div class="row" style="padding-top: 5px;">
         	<div class="col-xs-5" style="padding-left: 1px;">${nome}</div>
         	<div class="col-xs-1">
         	  <c:if test="${participante.viaja and participante.faixaEtaria < 2}">
	         	  <c:choose>
	         	    <c:when test="${participante.podeEditarViagem}">
	         		  <span class="glyphicon glyphicon-star btn-xs" title="Pode alterar todo o roteiro"></span>
	                </c:when>
	         	    <c:when test="${participante.podeAdicionarAtividade}">
	         		  <span class="glyphicon glyphicon-star-empty btn-xs" title="Pode incluir atividades"></span>
	                </c:when>
	         	    <c:otherwise>
	         		  <span class="glyphicon glyphicon-eye-open btn-xs" title="Pode ver o roteiro"></span>
	                </c:otherwise>
	              </c:choose>
              </c:if>
         	</div>
         	<div class="col-xs-3">
         	  <c:if test="${participante.viaja and participante.faixaEtaria < 2 }">
         		<c:choose>
         		  <c:when test="${participante.confirmado}">
         		    Confirmado
         		  </c:when>
         		  <c:when test="${participante.confirmado == false}">
         		  	Não confirmou
         		  </c:when>
         		  <c:otherwise>
         		    A confirmar
         		  </c:otherwise>
         		</c:choose>
         	  </c:if>
         	</div>
         	<div class="col-xs-1 btn-xs pull-right" style="margin: 0; padding: 0;">
         	  <c:if test="${participante.viaja}">
         	    <a href="#" class="btn-editar-viajante" title="Editar" style="color: #999" data-id-participante="${participante.id}" data-nome-participante="${participante.nome}">
         		  <span class="glyphicon glyphicon-edit"></span>
         	    </a>
         	  </c:if>
         	  <a href="#" class="btn-remover-viajante" data-id-participante="${participante.id}" title="Remover" style="color: #999">
         		<span class="glyphicon glyphicon-remove"></span>
         	  </a>
         	</div>
        </div>
    </li>
  </c:forEach>
