<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="panel-heading" data-id="${diaID}">
  <h3 class="header-panel-group" data-id="${diaID}" data-dia-id="${diaID}">
    <a data-toggle="collapse" data-parent="#panel-group-${diaID}" href="#panel-${diaID}" style="text-decoration: none;">
      <span class="label label-info">
        <c:if test="${not empty numeroDia}">
            ${numeroDia}º Dia
        </c:if>
        <c:if test="${not empty dataDia}">
            - <fmt:formatDate value="${dataDia}" pattern="EEE, dd MMM" />
        </c:if>
        <c:if test="${empty numeroDia}">
            A organizar
        </c:if>
        <c:if test="${not empty atividadesOrdenadas}">
            <span class="badge custom-badge" title="${fn:length(atividadesOrdenadas)} atividade(s)">${fn:length(atividadesOrdenadas)}</span>
        </c:if>
        <span id="show-${diaID}" class="glyphicon glyphicon-chevron-down btn-xs icon-collapse" title="Expandir"></span>
      </span>  
    </a>
  </h3>
  <div class="btn-toolbar pull-right" role="toolbar" style="margin-top: 2px;">
    <div class="btn-group btn-group-xs">
      <c:if test="${permissao.usuarioPodeIncluirItem || permissao.usuarioPodeExcluirDia}">
        <c:if test="${permissao.usuarioPodeIncluirItem}">
          <a href="#" role="button" class="btn btn-warning btnAdicionarItem hidden-xs" data-dia="${diaID}">
            <span class="glyphicon glyphicon-plus"></span>
            Adicionar atividade
          </a>
        </c:if>
        <c:if test="${permissao.usuarioPodeExcluirDia and diaID != 0}">
          <a href="#modal-excluir-dia" role="button" class="btn btn-default btnExcluirDia hidden-xs" data-toggle="modal" data-dia="${diaID}">
            <span class="glyphicon glyphicon-trash"></span>
            Excluir dia
          </a>
        </c:if>
      </c:if>
      
      <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}"> 
    
      <div class="btn-group btn-group-xs visible-xs">
        <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
          <span class="visible-xs">
            Opções
            <span class="caret"></span>
          </span>
          <span class="hidden-xs">
            Mais
            <span class="caret"></span>
          </span>
        </button>
        <ul class="dropdown-menu pull-right">
          <c:if test="${permissao.usuarioPodeIncluirItem}">
            <li>
              <a href="#" class="btnAdicionarItem visible-xs" data-dia="${diaID}">
                <span class="glyphicon glyphicon-plus"></span>
                Adicionar atividade
              </a>
            </li>
          </c:if>
          <c:if test="${permissao.usuarioPodeExcluirDia}">
            <li>
              <a type="button" class="visible-xs" data-dia="${diaID}">
                <span class="glyphicon glyphicon-trash"></span>
                Excluir dia
              </a>
            </li>
          </c:if>
          <%-- >li>
              <a href="#" class="">
                <span class="glyphicon glyphicon-retweet"></span>
                Mover selecionadas
              </a>
          </li--%>
        </ul>
      </div>
      
      </c:if>
    </div>
  </div>
</div>
<div id="panel-${diaID}" class="panel-collapse ${diaAberto or diaID == 0 ? 'in' : 'collapse'}">
  <div class="panel-body">
    <c:if test="${not empty atividadesOrdenadas}">
        <c:forEach items="${atividadesOrdenadas}" var="item" varStatus="itemStatus">
            <%@include file="atividade.jsp" %>
        </c:forEach>
    </c:if>
    <c:if test="${empty atividadesOrdenadas and diaID eq 0}">
    	<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
	        <div class="alert alert-info alert-dismissable" style="margin-bottom: 0px;">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <p>Adicione aqui os locais que você pretende ir mas ainda não sabe em qual dia de sua viagem.</p>
	            <p>Você poderá reorganizar a qualquer momento movendo os locais para o dia correto.</p>
	        </div>
        </c:if>
    </c:if>
    <c:if test="${permissao.usuarioPodeIncluirItem}">
        <div style="padding-top: 10px;">
          <a href="#" class="btn btn-warning btn-sm btnAdicionarItem visible-xs" data-dia="${diaID}">
            <span class="glyphicon glyphicon-plus"></span>
            Adicionar atividade
          </a>
        </div>
    </c:if>
  </div>
  <c:if test="${viagem.controlarGastos}">
      <div class="panel-footer" align="right">
        <h4>
            Subtotal do dia:
            <span class="red">
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2">${valorDia}</fmt:formatNumber>
            </span>
        </h4>
      </div>
  </c:if>
</div>
