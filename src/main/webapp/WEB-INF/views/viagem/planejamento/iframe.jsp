<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>

<!DOCTYPE html>
<html>
<c:choose>
    <c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
        <c:set var="reqScheme" value="https" />
    </c:when>
    <c:otherwise>
        <c:set var="reqScheme" value="http" />
    </c:otherwise>
</c:choose>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">

<link rel="stylesheet" href="<c:url value="/wro/mainResponsive.css" />" type="text/css" />

<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

<c:choose>
  <c:when test="${isMobile}">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection"/>
  </c:when>
  <c:otherwise>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection"/>
  </c:otherwise>
</c:choose>

<script type="text/javascript" src="<c:url value="/wro/mainResponsive.js"/>" ></script>

<c:if test="${not unsuportedIEVersion}">
  <script type="text/javascript" src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR"></script>
</c:if>


<style>
label {
    font-weight: normal;
}
</style>

<body>

    <div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li><a id="btn-tab-adicionar-atividade" href="#tab-adicionar-atividade" role="tab" data-toggle="tab" style="font-size: 15px;">Já sei onde quero ir</a></li>
            <li><a id="btn-tab-selecionar-atividades" href="#tab-selecionar-atividades" role="tab" data-toggle="tab" style="font-size: 15px;">Quero sugestões</a></li>
        </ul>

        <div id="iframe-body">
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane" id="tab-adicionar-atividade">
                  <div id="tab-adicionar-atividade-body">
                  </div>
                  
                  <div id="editar-atividade-modal-footer-incluir" class="modal-footer">
                    <button id="btnCancelarAtividade" class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                        <span class="glyphicon glyphicon-remove"></span>
                        Cancelar
                    </button>
                    <button id="btnIncluirAtividade" class="btn btn-primary" data-loading-text="Salvando...">
                        <span class="glyphicon glyphicon-floppy-saved"></span>
                        Salvar
                    </button>
					<button id="btnIncluirVariasAtividades" class="btn btn-info" data-loading-text="Salvando...">
                    	<span class="glyphicon glyphicon-floppy-open"></span>
                    	Salvar e adicionar +
                	</button>                  
                  </div>
                </div>
                <div class="tab-pane" id="tab-selecionar-atividades"></div>
            </div>        
        </div>

    </div>
</body>

<script>
$(document).ready(function () {

    $('#btn-tab-adicionar-atividade').click(function (e) {
        
        var tipoAtividade = '${tipoAtividade}';
    
        $('#tab-adicionar-atividade-body').load('<c:url value="/viagem/${viagem.id}/criarAtividade/"/>' + tipoAtividade + '?diaInicio=${diaInicio}&diaFim=${diaFim}', function() {
        
            /*$('#editar-atividade-modal-footer-incluir').show();
            $('#inputDia option[value="' + dia + '"]').attr('selected', 'selected');
            $('#inputDia').trigger('change');
            
            if (tipo) {
                $('#selectTipoAtividade').find('ul.dd-options li').each(function(index) {
                    if ($(this).find('input.dd-option-value').val() == tipo) {
                        $('#selectTipoAtividade').ddslick('select', { index: index });
                    }
                    $('input[name="nomeLocal"]').focus();
                })
            } else {
                $('#selectTipoAtividade').focus();
            }*/

            var height = $('#iframe-body').css('height');
            
            height = height.substring(0, height.indexOf('px')); 
            height = parseInt(height) + 150;
            
            parent.$('#sugestoes-modal').css('overflow-y', 'auto');
            parent.$('#sugestoes-modal').find('div.modal-dialog').css('height', height + 'px');
        })
    });

    $('#btn-tab-selecionar-atividades').click(function (e) {
        $('#tab-selecionar-atividades').load('<c:url value="/viagem/${viagem.id}/${urlPathCidadeSelecionada}/sugestoesLugares?tipoLocal=${tipoLocal}&diaInicio=${diaInicio} "/>', function(response) {
            
            var height = $('#iframe-body').css('height');
            
            height = height.substring(0, height.indexOf('px')); 
            height = parseInt(height) + 150;
            
            //parent.$('#sugestoes-modal').find('div.modal-dialog').css('height', '100%');
            
            $('#tab-selecionar-atividades').html(response)
            parent.$('#sugestoes-modal').css('overflow-y', 'hidden');
            parent.$('#sugestoes-modal').find('#scroll-div').css('height', $(window).height());
            $('#scroll-div').css('height', $(window).height() - 100);
            $('#scroll-div').find('div.lista-locais').css('min-height', $(window).height() + 100);

            if ($.browser.chrome) {
            } else if ($.browser.mozilla) {
                $('#suggestions-div').css('min-height', '');
                $('#suggestions-div').css('height', '800px');
            } else if ($.browser.msie) {
            }
            
        });
    });
    
    $(document).on('click', '#btnCancelarAtividade', function (e) {
        /*if($('#inputAtualizarQuandoFechar').val() == 'sim') {
            $('#inputAtualizarQuandoFechar').val('nao');
            parent.recarregarPagina();
        }*/
        parent.$('#sugestoes-modal').modal('hide');
    });
    
    $(document).on('click', '#btnIncluirAtividade', function (e) {
        e.preventDefault();
        $(this).button('loading');
        parent.salvarAtividade($('#formIncluirAtividade'), $(this), true);
        parent.$('#sugestoes-modal').modal('hide');
    });
    
    $(document).on('click', '#btnIncluirVariasAtividades', function (e) {
        e.preventDefault();
        $(this).button('loading');
        parent.salvarAtividade($('#formIncluirAtividade'), $(this), false);
        $('#inputAtualizarQuandoFechar').val('sim');
        $('input[name="nomeLocal"]').focus();
    });
    
    $(document).on('click', '#btn-salvar-atividade', function (e) {
        e.preventDefault();
        $(this).button('loading');
        parent.salvarAtividade($('#formAlterarAtividade'), $(this), true);
        parent.$('#sugestoes-modal').modal('hide');
    });
    
    $(document).on('change', 'select[name="diaInicio"][rel="HOSPEDAGEM"]', function (e) {
        var dia = $(this).val();
        $('select[name="itemDono"]').find('option[value="' + dia + '"]').attr('selected', 'selected');
        atualizarOpcoesDataFimAtividade('HOSPEDAGEM');
    });
    
    var acao = '${acao}';
    if (acao !== '') {
        var $tabAtual = $('#btn-tab-' + acao);
        if ($tabAtual.attr('id') != null) {
            $tabAtual.click();
        } else {
            $('#btn-tab-adicionar-atividade').click();
        }
        
    } else {
        $('#btn-tab-adicionar-atividade').click();
    }
    
});
</script>
</compress:html>
</html>