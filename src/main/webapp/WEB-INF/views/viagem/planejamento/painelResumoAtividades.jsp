<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style>
.font-normal {
    font-weight: normal;
}

.div-mais-detalhes {
    margin-right: 0px;
    margin-left: 0px;
}

div.item {  
    margin-left: 15px;
} 

div.div-mais-detalhes:nth-child(even) h5{
    background-color: #f7f7f7;
}

div.div-mais-detalhes:nth-child(odd) h5 {
    background-color: #E8E8E8;
}

</style>

<c:choose>

  <c:when test="${viagem.possuiAtividades}">

    <c:if test="${not empty viagem.atividadesTransporteOrdenadas}">
      <div class="row div-mais-detalhes">
        <c:set var="lista" value="${viagem.atividadesTransporteOrdenadas}" />
        <h5>
            <img src="<c:url value="/resources/images/markers/transporte-medium-pin.png"/>" height="30" />
            <span>Transportes</span>
            <span class="label label-default">${fn:length(lista)}</span>
            
            <small style="padding-left: 6px;">
              <a href="#" class="btn-mais-detalhes" data-txt-btn-fechado="<span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'></span>" data-txt-btn-aberto="<span class='glyphicon glyphicon-chevron-up btn-xs' title='Fechar'></span>">
                <span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'> </span>
              </a>
            </small>
            
            <c:if test="${viagem.controlarGastos}">
              <span class="pull-right red font-normal" style="margin: 8px;">
                <c:set var="valorTransporte" value="${viagem.valorAtividadesTransporte}"></c:set>
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${valorTransporte}" />
              </span>
            </c:if>
        </h5>
        <div class="row campos-mais-detalhes" style="padding: 5px 5px 5px 25px; display: none;">
            <c:forEach var="atividade" items="${lista}">
              <div class="item">
                <img src="<c:url value="/resources/images/${atividade.transporte.icone}-medium.png"/>" height="16"/>
                ${atividade.nomeLocalOrigem} para ${atividade.nomeLocalDestino}
              </div>
            </c:forEach>
        </div>
      </div>
    </c:if>
    
    <c:if test="${not empty viagem.atividadesHospedagemOrdenadas}">
      <div class="row div-mais-detalhes">
        <c:set var="lista" value="${viagem.atividadesHospedagemOrdenadas}" />
        <h5>
            <img src="<c:url value="/resources/images/markers/hotel-medium-pin.png"/>" height="30" />
            Hotéis <span class="label label-info">${fn:length(lista)}</span>
            <small style="padding-left: 6px;">
              <a href="#" class="btn-mais-detalhes" data-txt-btn-fechado="<span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'></span>" data-txt-btn-aberto="<span class='glyphicon glyphicon-chevron-up btn-xs' title='Fechar'></span>">
                <span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'> </span>
              </a>
            </small>
            
            <c:if test="${viagem.controlarGastos}">
              <span class="pull-right red font-normal" style="margin: 8px;">
                <c:set var="valorHospedagem" value="${viagem.valorAtividadesHospedagem}"></c:set>
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${viagem.valorAtividadesHospedagem}"/>
              </span>
            </c:if>
        </h5>
        <div class="row campos-mais-detalhes" style="padding: 5px 5px 5px 25px; display: none;">
            <c:forEach var="atividade" items="${lista}">
              <div class="item">
                <img src="<c:url value="/resources/images/hotel-medium-black.png"/>" height="16"/>
                ${atividade.nomeLocal}
              </div>
            </c:forEach>
        </div>
      </div>
    </c:if>
    
    <c:if test="${not empty viagem.atividadesAlimentacaoOrdenadas}">
      <div class="row div-mais-detalhes">
        <c:set var="lista" value="${viagem.atividadesAlimentacaoOrdenadas}" />
        <h5>
            <img src="<c:url value="/resources/images/markers/restaurante-medium-pin.png"/>" height="30" />
            Restaurantes <span class="label label-danger">${fn:length(lista)}</span>
            <small style="padding-left: 6px;">
              <a href="#" class="btn-mais-detalhes" data-txt-btn-fechado="<span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'></span>" data-txt-btn-aberto="<span class='glyphicon glyphicon-chevron-up btn-xs' title='Fechar'></span>">
                <span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'> </span>
              </a>
            </small>
            <c:if test="${viagem.controlarGastos}">
              <span class="pull-right red font-normal" style="margin: 8px;">
                <c:set var="valorAlimentacao" value="${viagem.valorAtividadesAlimentacao}"></c:set>
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${viagem.valorAtividadesAlimentacao}"/>
              </span>
            </c:if>
        </h5>
        <div class="row campos-mais-detalhes" style="padding: 5px 5px 5px 25px; display: none;">
            <c:forEach var="atividade" items="${lista}">
              <div class="item">
                <img src="<c:url value="/resources/images/restaurante-medium-black.png"/>" height="16"/>
                ${atividade.nomeLocal}
              </div>
            </c:forEach>
        </div>
      </div>
    </c:if>
    
    <c:if test="${not empty viagem.atividadesAtracoesOrdenadas}">
      <div class="row div-mais-detalhes">
        <c:set var="lista" value="${viagem.atividadesAtracoesOrdenadas}" />
        <h5>
            <img src="<c:url value="/resources/images/markers/atracao-medium-pin.png"/>" height="30" />
            O que fazer <span class="label label-warning">${fn:length(lista)}</span>
            <small style="padding-left: 6px;">
              <a href="#" class="btn-mais-detalhes" data-txt-btn-fechado="<span class='glyphicon glyphicon-chevron-down btn-xs' title='' data-original-title='Expandir'></span>" data-txt-btn-aberto="<span class='glyphicon glyphicon-chevron-up btn-xs' title='' data-original-title='Fechar'></span>">
                <span class='glyphicon glyphicon-chevron-down btn-xs' title='Expandir'> </span>
              </a>
            </small>
            <c:if test="${viagem.controlarGastos}">
              <span class="pull-right red font-normal" style="margin: 8px;">
                <c:set var="valorAtracoes" value="${viagem.valorAtividadesAtracoes}"></c:set>
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${viagem.valorAtividadesAtracoes}"/>
              </span>
            </c:if>
        </h5>
        <div class="row campos-mais-detalhes" style="padding: 5px 5px 5px 25px; display: none;">
            <c:forEach var="atividade" items="${lista}">
              <div class="item">
                <img src="<c:url value="/resources/images/atracao-medium-black.png"/>" height="16"/>
                ${atividade.nomeLocal}
              </div>
            </c:forEach>
        </div>
      </div>
    </c:if>
    
    <c:if test="${viagem.controlarGastos}">
      <div class="row pull-right" style="margin-right: 0px;">
        <span style="margin-right: 10px;">
            Outras atividades:
        </span>
        <strong>
          <span class="red font-normal">
            <c:set var="valorPersonalizadas" value="${viagem.valorAtividadesPersonalizadas}"></c:set>
            <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${viagem.valorAtividadesPersonalizadas}" />
          </span>
        </strong>
      </div>
    </c:if>
    
    <c:if test="${viagem.controlarGastos}">
        <br/>
        <div class="row pull-right" style="margin-right: 0px;">
          <strong>
            <span style="margin-right: 10px;">
                <span class="glyphicon glyphicon-usd"></span>
                Custo Total Estimado:
            </span>
            <span class="red" style="font-size: 16px;">
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2">${valorTransporte + valorHospedagem + valorAlimentacao + valorAtracoes + valorPersonalizadas}</fmt:formatNumber>
            </span>
          </strong>
        </div>
    </c:if>
  </c:when>
  <c:otherwise>
    <div class="row" style="text-align: center;">
        <div class="col-md-12">
            <img src="<c:url value="/resources/images/icons/mini/128/Time-3.png" />" width="45" height="45" />
            <p>
                <c:choose>
                  <c:when test="${usuario.id == viagem.usuarioCriador.id}">
                      Você
                  </c:when>
                  <c:otherwise>
                      ${viagem.usuarioCriador.displayName}
                  </c:otherwise>
                </c:choose>
                ainda não adicionou atividades em seu plano
            </p>
            <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
            
                <p>
                
                  <div id="btn-add-hotel" class="btn-group">
                      <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-plus"></span>
                        Adicionar hotel <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="#" role="button" class="btn-sugestoes" data-dia="0" data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" data-tipo-local="HOTEL" data-acao="adicionar-atividade">
                                <span class="glyphicon glyphicon-map-marker"></span> Já sei onde vou me hospedar
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn-sugestoes" data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" data-tipo-local="HOTEL" data-acao="selecionar-atividades">
                                <span class="glyphicon glyphicon-star-empty"></span>
                                Quero sugestões de hotéis
                            </a>
                        </li>
                      </ul>
                  </div>
                
                  <div id="btn-add-atividade" class="btn-group">
                      <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-plus"></span>
                        Adicionar atividades <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="#" role="button" class="btn-sugestoes" data-dia="0" data-tipo="<%=TipoAtividade.VISITA_ATRACAO%>" data-tipo-local="ATRACAO" data-acao="adicionar-atividade">
                                <span class="glyphicon glyphicon-map-marker"></span> Já sei onde quero ir
                            </a>
                        </li>
                        <li>
                            <a href="#" class="btn-sugestoes" data-tipo="<%=TipoAtividade.VISITA_ATRACAO%>" data-tipo-local="ATRACAO" data-acao="selecionar-atividades">
                                <span class="glyphicon glyphicon-star-empty"></span>
                                Quero sugestões de lugares
                            </a>
                        </li>
                      </ul>
                  </div>
                  
                  <button id="btn-add-transporte" type="button" class="btn btn-default btnAdicionarItem" data-tipo="<%=TipoAtividade.TRANSPORTE%>">
                    <span class="glyphicon glyphicon-plus"></span>
                    Adicionar transporte
                  </button>
                </p>
            </c:if>
        </div>
    </div>
  </c:otherwise>
</c:choose>