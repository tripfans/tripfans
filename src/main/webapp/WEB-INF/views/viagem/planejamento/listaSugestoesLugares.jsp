<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<c:set var="idsLocaisSelecionados" value=""/>
	<c:forEach var="idLocal" items="${locaisJaSelecionados}"><c:set var="idsLocaisSelecionados" value="${idsLocaisSelecionados},${idLocal}"/></c:forEach>
	<c:forEach var="idLocal" items="${locaisJaSelecionadosImperdiveis}"><c:set var="idsLocaisSelecionadosImperdiveis" value="${idsLocaisSelecionadosImperdiveis},${idLocal}"/></c:forEach>
    
    <fan:listaLocais locaisEmDestaque="${sugestoesLocais}" urlProximos="/viagem/${viagem.id}/${destino.urlPath}/sugestoesLugares?filtro=${filtro}&tipoLocal=${tipoLocal}&start=${proximasSugestoes}&tags=${tags}&estrelas=${estrelas}&bairros=${bairros}&locaisJaSelecionados=${idsLocaisSelecionados}&locaisJaSelecionadosImperdiveis=${idsLocaisSelecionadosImperdiveis}&recomendacao=${recomendacao}&columns=${columns}&columnSize=${columnSize}" 
                     photoHeight="120" photoSize="small" cardOrientation="horizontal" columns="${not empty columns ? columns : 1}" columnSize="${not empty columnSize ? columnSize : 12}" showActions="false" showAdditionalInfo="false"
                     showAddToPlanButton="false" showNotaGeral="false" showViajantesJaForam="false" showAmigosJaForam="false" showReservarBooking="false" showBotaoAdicionar="false"
                     useButtonsRecomendacao="${recomendacao}"
                     style="${not isMobile ? 'padding-top: 0px;' : ''}" 
                     cardStyle="${isMobile ? 'padding-right: 0px; padding-left: 0px;' : 'min-height: 120px;'};"
                     titleStyle="font-size: .8em;" />

