<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<div rel="atividade" data-atividade-id="${item.id}" data-index="${item.ordem}" class="div-atividade draggable"
     data-nome-local="${item.local.nome}" data-urlpath-local="${item.local.urlPath}" data-latitude="${item.local.latitude}" data-longitude="${item.local.longitude}"
     <c:if test="${item.atividadeTransporte}">
     data-nome-local-origem="${item.transporteLocalOrigem.nome}" data-urlpath-local-origem="${item.transporteLocalOrigem.urlPath}"
     data-latitude-origem="${item.transporteLocalOrigem.latitude}" data-longitude-origem="${item.transporteLocalOrigem.longitude}"
     data-nome-local-destino="${item.transporteLocalDestino.nome}" data-urlpath-local-destino="${item.transporteLocalDestino.urlPath}"
     data-latitude-destino="${item.transporteLocalDestino.latitude}" data-longitude-destino="${item.transporteLocalDestino.longitude}"
     data-icone-transporte="${item.transporte.icone}"
     </c:if>
     data-tipo-atividade="${item.tipo}" data-icone-atividade="${item.tipo.icone}" data-fim-atividade="${not item.isComecaNoDia(diaID)}">

    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="padding: 0; padding-top: 0px; font-size: 85%; margin-left: -5px; margin-right: -15px; color: #999;">
    
      <c:if test="${not item.atividadeHospedagem and not item.atividadeTransporte}">
		  <span class="label label-default number-label label-${item.tipo.icone}">
		  	${index}
		  </span>
	  </c:if>
	  
	  <c:if test="${item.atividadeHospedagem and not item.isTerminaNoDia(diaID)}">
          <div class="row checkin-label">
            <div class="col-md-2 col-sm-2 col-xs-2">
              <span class="label label-success">Checkin</span>
            </div>
          </div>
      </c:if>
      <c:if test="${item.atividadeHospedagem and not item.isComecaNoDia(diaID)}">
          <div class="row checkin-label">
            <div class="col-md-2 col-sm-2 col-xs-2">
              <span class="label label-warning">Checkout</span>
            </div>
          </div>
      </c:if>	  
		    
      <c:if test="${item.isComecaNoDia(diaID)}">
          <c:if test="${not empty item.dataHoraInicio}">
            <div>
                <span class="glyphicon glyphicon-time"></span>
                <span ><fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" /></span>
                <%--span class="visible-xs">
                    <span style="font-size: 14px;"><fmt:formatDate value="${item.dataHoraInicio}" pattern="HH" />h</span>
                    <div style="margin-left: 7px; margin-top: -5px;"><fmt:formatDate value="${item.dataHoraInicio}" pattern="mm" /></div>
                </span--%>
            </div>
          </c:if>
      </c:if>
      
      <c:if test="${item.isTerminaNoDia(diaID) and not item.isComecaNoDia(diaID)}">
          <c:if test="${not empty item.dataHoraFim}">
            <div>
                <span class="glyphicon glyphicon-time"></span> <fmt:formatDate value="${item.dataHoraFim}" pattern="HH:mm" />
            </div>
          </c:if>
      </c:if>
      
      <c:if test="${empty item.dataHoraInicio and empty item.dataHoraFim}">
        <div class="visible-xs" style="padding-top: 32px;"></div>
      </c:if>
      
    </div>
    
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 div-icone-atividade" style="margin-right: -10px;">
        <c:if test="${mostarDiaAtividade}">
            <h5 style="margin-left: 26px; margin-top: 8px">
                <c:if test="${not empty item.dia}">
                    ${item.dia.numero}º Dia
                    <c:if test="${not empty item.dia.dataViagem}">
                        <span style="font-weight: normal;" class="hidden-xs">
                            <fmt:formatDate value="${item.dia.dataViagem}" pattern="EEE, dd MMM" /> 
                        </span>
                    </c:if>
                </c:if>
                <c:if test="${empty item.dia}">
                    Sem data
                </c:if>
            </h5>
        </c:if>
    
        <c:if test="${not empty item.transporte}">
            <img src="<c:url value="/resources/images/planejamento/${item.transporte.icone}-small.png"/>" class="icone-atividade img-circle" title="${item.transporte.descricao}" />
        </c:if>
        <c:if test="${empty item.transporte}">
            <img src="<c:url value="/resources/images/planejamento/${item.icone}-small.png"/>" class="icone-atividade img-circle" title="${item.tipo.descricao}" />
            <%-- c:if test="${not empty item.dataHoraInicio or not empty item.dataHoraFim}">
          <h4 style="position: absolute; top: 45px; right: 0px; display: inline-table;">
            <span class="label label-primary">
                <span class="glyphicon glyphicon-time" title="Hora de início da atividade"></span>
                <fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" />
            </span>
          </h4>
          <h4 style="position: absolute; top: 75px; right: 0px; display: inline-table;">
            <span class="label label-primary">
                <span class="glyphicon glyphicon-time"  title="Hora término da atividade"></span>
                <fmt:formatDate value="${item.dataHoraFim}" pattern="HH:mm" />
            </span>
          </h4>
        </c:if--%>
        </c:if>
        
    </div>
    
    <div class="media card-atividade selectable" data-dia-id="${diaID}" data-atividade-id="${item.id}">
        <a class="media-left" href="#" style="padding-left: 0px; margin-top: 5px; ">
            <c:set var="urlFoto" value="${item.local.urlFotoSmall}" />
            <c:if test="${item.local.fotoPadraoAnonima}">
                <c:choose>
                  <c:when test="${item.local.tipoAtracao}">
                    <c:set var="tipoLocal" value="atracao"/>
                  </c:when>
                  <c:when test="${item.local.tipoHotel}">
                    <c:set var="tipoLocal" value="hotel"/>
                  </c:when>
                  <c:when test="${item.local.tipoRestaurante}">
                    <c:set var="tipoLocal" value="restaurante"/>
                  </c:when>
                  <c:otherwise>
                    <c:set var="tipoLocal" value="lugar"/>
                  </c:otherwise>
                </c:choose>
                <c:url var="urlFoto" value="/resources/images/planejamento/${tipoLocal}-medium-pb.png" />
            </c:if>
            <c:if test="${empty urlFoto}">
                <c:url var="urlFoto" value="/resources/images/planejamento/lugar-medium-pb.png" />
            </c:if>
            <img src="${urlFoto}" class="img-circle img-local-atividade" />
        </a>
        <div class="media-body">
        
            <c:choose>
                <c:when test="${not empty item.url}">
                    <div class="row" style="margin-left: 0px; padding-bottom: 4px;">
                        <div class="${not impressao ? 'col-md-12 col-sm-12 col-xs-12' : ''}" style="${not impressao ? 'margin-left: -15px;' : ''}">
                            <h4 class="media-heading">
                                <a href="#">
                                    ${item.tituloItemReduzido}
                                </a>
                                <c:if test="${not empty item.local}">
                                  <br/>
                                  <small>${item.local.localizacao.cidade.nome}<c:if test="${not empty item.local.localizacao.estado.sigla}">, ${item.local.localizacao.estado.sigla}</c:if></small>
                                </c:if>
                            </h4>
                        </div>
                    </div>
                    <div class="hidden-print">
                       <c:if test="${item.local.notaTripAdvisor > 0 and not impressao}">
                           <span class="tripAdvisorRating rate-${fn:replace(item.local.notaTripAdvisor, '.', '') }" title="Nota dos viajantes site TripAdvisor.com - ${item.local.quantidadeReviewsTripAdvisor} avaliações"></span>
                        </c:if>
                    </div>
                </c:when>
                <c:otherwise>
                    <c:if test="${not empty item.transporte}">
                        <h4 class="tituloAtividade">
                          <c:choose>
                            <c:when test="${item.transporte.aluguelVeiculo}">
                              <c:choose>
                                <c:when test="${item.terminaEmOutroDia and item.isTerminaNoDia(diaID)}">
                                  Devolução
                                </c:when>
                                <c:otherwise>
                                  Aluguel
                                </c:otherwise>
                              </c:choose>
                              de Veículo
                            </c:when>
                            <c:otherwise>
                              <c:choose>
                                  <c:when test="${item.isComecaNoDia(diaID)}">
                                      ${item.transporte.descricao}
                                      <c:if test="${not empty item.transporteLocalOrigem}">
                                        <span style="font-weight: normal"> de</span>
                                        <a href="#">
                                            ${item.transporteLocalOrigem.nome}
                                        </a>
                                      </c:if>
                                      <c:if test="${empty item.transporteLocalOrigem}">
                                        <span style="font-weight: normal"> de</span>
                                        ${item.nomeLocalOrigem}
                                      </c:if>
                                      <c:if test="${not empty item.transporteLocalDestino}">
                                        <span style="font-weight: normal"> para</span>
                                        <a href="#">
                                            ${item.transporteLocalDestino.nome}
                                        </a>
                                      </c:if>
                                      <c:if test="${empty item.transporteLocalDestino}">
                                        <span style="font-weight: normal"> para</span>
                                        ${item.nomeLocalDestino}
                                      </c:if>
                                  </c:when>
                                  <c:when test="${(item.terminaEmOutroDia and item.isTerminaNoDia(diaID))}">
                                      <c:if test="${not empty item.transporteLocalDestino}">
                                        <span style="font-weight: normal">Chegada em</span>
                                        <a href="#">
                                            ${item.transporteLocalDestino.nome}
                                        </a>
                                      </c:if>
                                      <c:if test="${empty item.transporteLocalDestino}">
                                        <span style="font-weight: normal">Chegada em</span>
                                        ${item.nomeLocalDestino}
                                      </c:if>
                                  </c:when>
                              </c:choose>
                            </c:otherwise>
                          </c:choose>
                        </h4>
                    </c:if>
                    <c:if test="${empty item.transporte}">
                        <div class="row" style="margin-left: 0px; padding-bottom: 4px;">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-left: -15px;">
                                <h4 class="tituloAtividade">
                                    ${item.tituloItemReduzido}
                                </h4>
                            </div>
                        </div>
                    </c:if>
                </c:otherwise>
            </c:choose>
            
            <c:if test="${not item.atividadeTransporte}">
              <div style="margin-left: 0px; padding-bottom: 4px;">
                <c:if test="${item.isComecaNoDia(diaID)}">
                  <c:if test="${not empty item.dataHoraInicio}">
                    <%-- div class="col-md-2 col-sm-2 col-xs-2 label-horario-atividade" style="margin-top: 5px;">
                        <h4>
                            <span class="label label-time start">
                                <span class="glyphicon glyphicon-time" title="${not item.atividadeHospedagem ? 'Hora de início' : 'Checkin'}"></span>
                                <span class="digital-clock">
                                  <fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" />
                                </span>
                            </span>
                        </h4>
                    </div--%>
                  </c:if>
                  
                </c:if>
                <c:if test="${item.isTerminaNoDia(diaID)}">
                  <c:if test="${not empty item.dataHoraFim}">
                    <%-- div class="col-md-2 col-sm-2 col-xs-2 label-horario-atividade" style="margin-top: 5px;">
                        <c:if test="${not item.terminaEmOutroDia}">
                          <br/>
                        </c:if>
                        <h4>
                            <span class="label label-time end">
                                <span class="glyphicon glyphicon-time" title="${not item.atividadeHospedagem ? 'Hora de término' : 'Checkout'}"></span>
                                <span class="digital-clock inverse">
                                  <fmt:formatDate value="${item.dataHoraFim}" pattern="HH:mm" />
                                </span>
                            </span>
                        </h4>
                        <c:if test="${not item.terminaEmOutroDia}">
                          <div style="margin: 3px;">
                            <br/>
                          </div>
                        </c:if>
                    </div--%>
                  </c:if>
                  
                </c:if>
              </div>
            </c:if>
    
            <c:if test="${item.isComecaNoDia(diaID)}">
    
              <c:if test="${item.local.tipoHotel}">
                   <%-- c:if test="${not empty item.local.estrelas and local.estrelas > 0}">
                       <img src="<c:url value="/resources/images/star${item.local.estrelas}.png"/>"/>
                   </c:if--%>
                   
                   <c:if test="${not item.reservada and (permissao.usuarioPodeAlterarDadosDaViagem || permissao.usuarioPodeAlterarItem) and mostrarAcoes}">
                     <div class="hidden-print">
                       <a href="${item.local.urlBooking}" class="btn btn-sm btn-warning" target="_blank" title="Reservar no Booking.com">
                          Reservar no Booking.com
                       </a>
                     </div>
                   </c:if>
              </c:if>
    
              <c:if test="${not empty item.nomeLocalOrigem}">
                <div style="padding-bottom: 4px;">
                  <small>
                    <c:if test="${not empty item.dataHoraInicio}">
                        <div style="display: inline-block;">
                            <h4>
                                <span class="label label-time start">
                                    <span class="glyphicon glyphicon-time" title="Hora de ${item.transporte == 'ALUGUEL_VEICULO' ? 'retirada' : 'partida'}"></span>
                                    <span style="color: #444">
                                      <fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" />
                                    </span> 
                                </span>
                            </h4>
                        </div>
                    </c:if>
                    <div style="display: inline-block;">
                        <img src="<c:url value="/resources/images/${item.transporte.icone}-partida-medium.png"/>"
                             height="25" title="${item.transporte == 'ALUGUEL_VEICULO' ? 'Retirada' : 'Partida'}" />
                        <c:if test="${not empty item.transporteLocalOrigem}">
                            <%--a href="${pageContext.request.contextPath}${item.transporteLocalOrigemUrl}"--%>
                            <a href="#">
                                ${item.transporteLocalOrigem.nome}
                            </a>
                        </c:if>
                        <c:if test="${empty item.transporteLocalOrigem}">
                          ${item.nomeLocalOrigem}
                        </c:if>
                    </div>
                    
                    <c:if test="${not empty item.enderecoLocalOrigem}">
                        <div class="col-md-9 col-sm-9 col-xs-8" style="margin-left: -15px; margin-top: 3px;">
                          <small>
                            <strong>Endereço: </strong>${item.enderecoLocalOrigem}
                          </small>
                        </div>
                    </c:if>
                   </small> 
                </div>
              </c:if>
            </c:if>
            <c:if test="${not empty item.nomeLocalDestino}">
              <c:if test="${item.isTerminaNoDia(diaID)}">
                <div style="padding-bottom: 4px;">
                  <small>
                    <c:if test="${not empty item.dataHoraFim}">
                        <div style="display: inline-block;">
                            <h4>
                                <span class="label label-time end">
                                    <span class="glyphicon glyphicon-time" title="Hora de ${item.transporte == 'ALUGUEL_VEICULO' ? 'entrega' : 'chegada'}"></span>
                                    <span style="color: #444">
                                      <fmt:formatDate value="${item.dataHoraFim}" pattern="HH:mm" />
                                    </span>
                                </span>
                            </h4>
                        </div>
                    </c:if>
                    <div style="display: inline-block;">
                        <img src="<c:url value="/resources/images/${item.transporte.icone}-chegada-medium.png"/>"
                             height="25" title="${item.transporte == 'ALUGUEL_VEICULO' ? 'Entrega' : 'Chegada'}" />
                        <c:if test="${not empty item.transporteLocalDestino}">
                            <a href="#">
                                ${item.transporteLocalDestino.nome}
                            </a>
                        </c:if>
                        <c:if test="${empty item.transporteLocalDestino}">   
                            ${item.nomeLocalDestino}
                        </c:if>
                    </div>
                  </small>
                </div>
              </c:if>
            </c:if>
    
            <c:if test="${item.isComecaNoDia(diaID)}">
            
              <div class="div-mais-detalhes">
                  <c:if test="${not impressao}">
                    <small>
                      <a href="#" class="btn-mais-detalhes">+ Mais detalhes <span class="caret"></span></a>
                    </small>
                  </c:if>
                  
                  <div class="campos-mais-detalhes" style="${not impressao ? 'display: none;' : ''}">
                    <small>
                      <c:if test="${not empty item.enderecoLocal}">
                        <div>
                            <strong>Endereço: </strong>${item.enderecoLocal}
                        </div>
                      </c:if>
                      <c:if test="${not empty item.telefone}">
                        <div>
                            <strong>Fone: </strong>${item.telefone}
                        </div>
                      </c:if>
                      <c:choose>
                        <c:when test="${modoVisao.calendario}">
                            <c:if test="${not empty item.site}">
                                <strong>Site: </strong>
                                <a href="${item.site}" target="_blank"><abbr title="${item.site}">link</abbr>
                                </a>
                            </c:if>
                            <c:if test="${not empty item.email}">
                                <strong>Email: </strong>
                                <a href="mailto:${item.email}"><abbr
                                    title="${item.email}">link</abbr>
                                </a>
                            </c:if>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${not empty item.site}">
                                <strong>Site: </strong>
                                <a href="${item.site}" target="_blank">${item.site}</a>
                            </c:if>
                            <c:if test="${not empty item.email}">
                                <strong>Email: </strong>${item.email}
                        </c:if>
                        </c:otherwise>
                      </c:choose>
                      <c:if test="${not empty item.descricao}">
                        <div>${item.descricao}</div>
                      </c:if>
                      <c:if test="${not empty item.valor}">
                        <div>
                            <strong>Valor: </strong>
                            <span class="red">
                              <strong>
                                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2">
                                    ${item.valor}
                                </fmt:formatNumber> 
                              </strong>
                            </span>
                        </div>
                      </c:if>
                    </small>
                  </div>
              </div>
              
              <c:if test="${not empty item.local and not impressao}">
                <div class="hidden-print">
                  <hr style="margin-bottom: 5px; margin-top: 5px;" />
                  <span>
                    <c:if test="${not empty item.local}">
                        <c:if test="${item.local.quantidadeInteressesJaFoi > 0}">
                          <small>
                            <span class="label label-success">${item.local.quantidadeInteressesJaFoi}</span> já foram <%-- (X amigos - Pedir dicas) --%>
                          </small>
                        </c:if>
                        <%-- c:if test="${item.local.quantidadeDicas > 0}">
                            <a href="<c:url value="/locais/${item.local.urlPath}/dicas"/>"
                               target="_blank">${quantidadeDicas}
                               dicas
                            </a>
                        </c:if--%>
                    </c:if>
                  </span>
                </div>
              </c:if>
            
              <c:if test="${not empty item.anotacoes}">
                <hr style="margin-bottom: 7px; margin-top: 5px;" />
                <span class="glyphicon glyphicon-list-alt"></span>
                <small style="font-size: 14px;"> 
                    <strong>Notas:</strong>
                    <div>${fn:replace(item.anotacoes, newLineChar, "<br />")}</div>
                </small>
              </c:if>
            </c:if>
            
        </div>
        <div class="media-right media-middle acoes-atividade" href="#">
            <c:if test="${mostrarAcoes && (permissao.usuarioPodeAlterarItem || permissao.usuarioPodeExcluirItem || permissao.usuarioPodeMoverItem || permissao.usuarioPodeMovimentarItem)}">
                
                <c:if test="${permissao.usuarioPodeMoverItem and mostrarAcoesMover}">
                  <div style="padding-bottom: 15px; text-align: center;">
				    <a class="btn-xs btnMoverItem" tabindex="-1" data-direction="up" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemDecrementarPosicao" 
				       data-atividade-id="${item.id}" style="${not item.primeiro ? '' : 'display: none;'} color: #444; padding: 0px;"
				       title="Mover para cima">
				        <span class="glyphicon glyphicon-arrow-up"></span>
				    </a>
				  </div>
				</c:if>
				
				<div>
				
                  <c:if test="${permissao.usuarioPodeAlterarItem}">
					  <a href="#" role="button" class="btn-xs btnAlterarItem" data-id-atividade="${item.id}" title="Alterar" style="color: #444; padding: 0px; "><span class="glyphicon glyphicon-pencil"></span></a>
                  </c:if>
				  <b class="btn-group" style="bottom: 5px; left: 5px; margin-top: 5px;" title="Mais opções">
                    <a class="btn-xs dropdown-toggle" data-toggle="dropdown" href="#" style="color: #444; padding: 0px;"><span class="glyphicon glyphicon-list"></span><span class="caret"></span></a>
                    <ul class="dropdown-menu pull-right" role="menu">
	                    <c:if test="${permissao.usuarioPodeMoverItem and mostrarAcoesMover}">
	                      <li>
	                          <a class="btnMoverItem" tabindex="-1" data-direction="first" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemParaPrimeiro" data-atividade-id="${item.id}" style="${not item.primeiro ? '' : 'display: none'}">
	                              <span class="glyphicon glyphicon-circle-arrow-up"></span>
	                              Mover para primeiro
	                          </a>
	                      </li>
	                      <li>
	                          <a class="btnMoverItem" tabindex="-1" data-direction="last" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemParaUltimo" data-atividade-id="${item.id}" style="${not item.ultimo ? '' : '/*display: none*/'}">
	                              <span class="glyphicon glyphicon-circle-arrow-down"></span>
	                              Mover para último
	                          </a>
	                      </li>
	                    </c:if>
	                    <c:if test="${permissao.usuarioPodeMovimentarItem and mostrarAcoesMover}">
	                      <li class="divider"></li>
	                      <li>
	                          <a class="btnMovimentarParaDia" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-mover-atividades" data-atividade-id="${item.id}" data-dia-id="${diaID}">
	                              <span class="glyphicon glyphicon-inbox"></span>
	                              Mover para outro dia
	                          </a>
	                      </li>
	                    </c:if>
                    </ul>
                </b>
                <c:if test="${item.isComecaNoDia(diaID)}">
                  <c:if test="${permissao.usuarioPodeExcluirItem}">
					  <a href="#modal-excluir-atividade" role="button" class="btn-xs btnExcluirAtividade" data-toggle="modal" data-atividade-id="${item.id}" title="Excluir" style="margin-left: 10px; color: #444; padding: 0px;">
					    <span class="glyphicon glyphicon-trash"></span>
					  </a>
			      </c:if>
			    </c:if>
				</div>
            
                <!--div class="btn-group" style="bottom: 5px; right: 5px;">
                    <a class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" href="#">
                        <span class="glyphicon glyphicon-list"></span>
                        Ações <span class="caret"></span> 
                    </a>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <c:if test="${permissao.usuarioPodeAlterarItem}">
                            <li>
                                <a href="#" role="button" class="btnAlterarItem" data-id-atividade="${item.id}"> 
                                    <span class="glyphicon glyphicon-edit"></span>
                                    Alterar 
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${item.isComecaNoDia(diaID)}">
                          <c:if test="${permissao.usuarioPodeExcluirItem}">
                            <li>
                                <a href="#modal-excluir-atividade" role="button" class="btnExcluirAtividade" data-toggle="modal" data-atividade-id="${item.id}">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Excluir
                                </a>
                            </li>
                          </c:if>
                          <c:if test="${permissao.usuarioPodeMoverItem and mostrarAcoesMover}">
                            <li class="divider"></li>
                            <li>
                                <a class="btnMoverItem" tabindex="-1" data-direction="first" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemParaPrimeiro" data-atividade-id="${item.id}" style="${not item.primeiro ? '' : 'display: none'}">
                                    <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                    Mover para primeiro
                                </a>
                            </li>
                            <li>
                                <a class="btnMoverItem" tabindex="-1" data-direction="up" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemDecrementarPosicao" data-atividade-id="${item.id}" style="${not item.primeiro and not item.segundo ? '' : 'display: none'}">
                                    <span class="glyphicon glyphicon-arrow-up"></span>
                                    Mover para cima
                                </a>
                            </li>
                            <li>
                                <a class="btnMoverItem" tabindex="-1" data-direction="down" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemIncrementarPosicao" data-atividade-id="${item.id}" style="${not item.ultimo and not item.penultimo ? '' : 'display: none'}">
                                    <span class="glyphicon glyphicon-arrow-down"></span>
                                    Mover para baixo
                                </a>
                            </li>
                            <li>
                                <a class="btnMoverItem" tabindex="-1" data-direction="last" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemParaUltimo" data-atividade-id="${item.id}" style="${not item.ultimo ? '' : 'display: none'}">
                                    <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                    Mover para último
                                </a>
                            </li>
                          </c:if>
                          <c:if test="${permissao.usuarioPodeMovimentarItem and mostrarAcoesMover}">
                            <li class="divider"></li>
                            <li>
                                <a class="btnMovimentarParaDia" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-mover-atividades" data-atividade-id="${item.id}" data-dia-id="${diaID}">
                                    <span class="glyphicon glyphicon-inbox"></span>
                                    Mover para outro dia
                                </a>
                            </li>
                            <c:choose>
                                <c:when test="${empty item.viagem}">
                                    <%-- li>
                                <a class="btnMovimentarParaViagem btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem/${viagem.id}/movimentarItemParaViagem" data-atividade-id="${item.id}">
                                    <span class="glyphicon glyphicon-inbox"></span> Mover para data a definir
                                </a>
                            </li>
                            <li>
                                <a class="btnMovimentarParaDia btnMoverItem" tabindex="-1" href="#" data-toggle="modal" data-target="#modal-mover-atividades" data-atividade-id="${item.id}" data-dia-id="${diaID}">
                                    <span class="glyphicon glyphicon-inbox"></span> Mover para outro dia
                                </a>
                            </li>
                            <c:forEach var="dia" items="${item.dia.viagem.dias}">
                                <c:if test="${item.dia != dia}">
                                <li>
                                    <a class="btnMovimentarParaDia btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem/${viagem.id}/movimentarItemParaDia" data-atividade-id="${item.id}" data-dia-id="${diaID}">
                                        <c:choose>
                                            <c:when test="${dia.numero > item.dia.numero}"><i class="icon-arrow-right"></i></c:when>
                                            <c:otherwise><i class="icon-arrow-left"></i></c:otherwise>
                                        </c:choose>
                                        <span class="glyphicon glyphicon-calendar"></span>
                                        Mover para o ${dia.descricao}
                                    </a>
                                </li>
                                </c:if>
                            </c:forEach--%>
                                </c:when>
                                <c:otherwise>
                                    <%--c:forEach var="dia" items="${item.viagem.dias}">
                                <li><a class="btnMovimentarParaDia btnMoverItem" tabindex="-1" href="${pageContext.request.contextPath}/viagem/${viagem.id}/movimentarItemParaDia" data-atividade-id="${item.id}" data-dia-id="${diaID}">
                                    <i class="icon-download"></i> - Mover para o ${dia.descricao}
                                    </a>
                                </li>
                            </c:forEach--%>
                                </c:otherwise>
                            </c:choose>
                          </c:if>
                        </c:if>
                    </ul>
                </div-->
                
                <c:if test="${permissao.usuarioPodeMoverItem and mostrarAcoesMover}">
                  <div style="padding-top: 15px; text-align: center;">
				    <a class="btn-xs btnMoverItem" tabindex="-1" data-direction="down" href="${pageContext.request.contextPath}/viagem/${viagem.id}/moverItemIncrementarPosicao" data-atividade-id="${item.id}" style="${not item.ultimo ? '' : '/*display: none;*/'} color: #444; padding: 0px;" title="Mover para baixo">
				      <span class="glyphicon glyphicon-arrow-down"></span>
				    </a>
			      </div>
			    </c:if>
                
            </c:if>
        </div>    
    </div>
</div>
