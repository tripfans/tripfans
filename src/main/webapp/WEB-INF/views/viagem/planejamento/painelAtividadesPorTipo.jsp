<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:choose>
  <c:when test="${tipoAtividade.atividadeAlimentacao}">
    <c:set var="descricaoAtividade" value="Restaurante" />
    <c:set var="btnClass" value="btn-danger" />
  </c:when>
  <c:when test="${tipoAtividade.atividadeHospedagem}">
    <c:set var="descricaoAtividade" value="Hospedagem" />
    <c:set var="btnClass" value="btn-primary" />
  </c:when>
  <c:when test="${tipoAtividade.atividadeTransporte}">
    <c:set var="descricaoAtividade" value="Transporte" />
    <c:set var="btnClass" value="btn-default" />
  </c:when>
  <c:when test="${tipoAtividade.atividadeVisitaAtracao}">
    <c:set var="descricaoAtividade" value="Atração" />
    <c:set var="btnClass" value="btn-warning" />
  </c:when>
  <c:otherwise>
    <c:set var="descricaoAtividade" value="atividade personalizada" />
    <c:set var="btnClass" value="btn-success" />
  </c:otherwise>
</c:choose>
        
<div class="panel-group" id="panel-group-${tipoAtividade}" style="margin-bottom: 10px;">
  <div class="panel panel-default" style="overflow: initial;" data-id="${tipoAtividade}" data-tipo-atividade="${tipoAtividade}">
    <div class="panel-heading" data-id="${tipoAtividade}">
      <h3 class="header-panel-group" data-id="${tipoAtividade}" data-tipo-atividade="${tipoAtividade}">
        <a data-toggle="collapse" data-parent="#panel-group-${tipoAtividade}" href="#panel-${tipoAtividade}" style="text-decoration: none;">
          <span class="label label-info">
            ${tipoAtividade.descricao}
            <c:if test="${not empty atividadesPorTipo}">
                <span class="badge custom-badge" title="${fn:length(atividadesPorTipo)} atividade(s)">${fn:length(atividadesPorTipo)}</span>
            </c:if>
            <span id="show-${tipoAtividade}" class="glyphicon glyphicon-chevron-down btn-xs icon-collapse" title="Expandir"></span>
          </span>  
        </a>
      </h3>
      <c:if test="${permissao.usuarioPodeAlterarItem}">
        <div class="btn-toolbar pull-right" role="toolbar" style="margin-top: 2px;">
          <a href="#" role="button" class="btn btn-xs ${btnClass} btnAdicionarItem hidden-xs" data-dia="${diaID}" data-tipo="${tipoAtividade}">
            <span class="glyphicon glyphicon-plus"></span>
            Adicionar ${descricaoAtividade}
          </a>
        </div>
      </c:if>
    </div>
    <div id="panel-${tipoAtividade}" class="panel-collapse ${painelAberto ? 'in' : 'collapse'}">
      <div class="panel-body">
        <c:if test="${not empty atividadesPorTipo}">
            <c:forEach items="${atividadesPorTipo}" var="item" varStatus="itemStatus">
                <%@include file="atividade.jsp" %>
            </c:forEach>
        </c:if>
        
        <%-- >c:if test="${empty atividadesOrdenadas and diaID eq 0}">
            <div class="alert alert-info alert-dismissable" style="margin-bottom: 0px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p>Adicione aqui os locais que você pretende ir mas ainda não sabe em qual dia de sua viagem.</p>
                <p>Você poderá reorganizar a qualquer momento movendo os locais para o dia correto.</p>
            </div>
        </c:if--%>
        <c:if test="${permissao.usuarioPodeAlterarItem}">
          <div style="text-align: center; margin-top: 5px">
            <a href="#" class="btn ${btnClass} btn-sm btnAdicionarItem" data-dia="${diaID}" data-tipo="${tipoAtividade}">
              <span class="glyphicon glyphicon-plus"></span>
              Adicionar ${descricaoAtividade}
            </a>
          </div>
        </c:if>
      </div>
      <c:if test="${viagem.controlarGastos}">
          <div class="panel-footer" align="right">
            <h4>
                Subtotal:
                <span class="red">
                    <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2" value="${viagem.getValorPorTipoAtividade(tipoAtividade)}" />
                </span>
            </h4>
          </div>
      </c:if>
    </div>
  </div>
</div>
