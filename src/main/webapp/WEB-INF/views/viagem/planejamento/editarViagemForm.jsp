<%@page import="org.joda.time.LocalDate" trimDirectiveWhitespaces="true" language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
img.flag {
    margin-left: 8px;
}
</style>

<link rel="stylesheet" href="<c:url value="/resources/components/bootstrap-datepicker/css/bootstrap-datepicker3.css" />" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/components/bootstrap-datepicker/js/bootstrap-datepicker.js" />"></script>

<c:choose>
    <c:when test="${pageContext.response.locale.language eq 'pt'}">
        <%-- script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-pt-BR.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-ui-timepicker-pt-BR.js" ></script--%>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/components/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js" ></script>
        
    </c:when>
    <c:when test="${pageContext.response.locale.language eq 'es'}">
        <%-- script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.ui.datepicker-es.js"></script--%>
    </c:when>
</c:choose>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<form:form id="frm-editar-viagem" modelAttribute="viagem" method="post" action="/viagem/salvar" class="form-horizontal" role="form">
  <input type="hidden" name="viagem" id="inputId" value="${viagem.id}" />
  <c:if test="${viagem.id != null}">
      <div class="form-group">
        <label for="inputTitulo" class="col-sm-3 control-label required">Título</label>
        <div class="col-sm-7">
            <form:input type="text" path="titulo" id="inputTitulo" class="form-control" placeholder="Informe um título" required="true" />
        </div>
      </div>
  </c:if>
  <div class="form-group">
    <label for="inputDestinos" class="col-sm-3 control-label required">
        Destino(s)
        <span class="glyphicon glyphicon-question-sign" title="Selecione a(s) cidade(s) que você planeja visitar."></span>
    </label>
    <div class="col-sm-7">
        <input type="text" name="destinos" id="inputDestinos" placeholder="Para onde deseja ir?" />
    </div>
  </div>
  
  
  
  <div class="form-group">
    <label for="inputDataInicioViagem" class="col-sm-3 control-label">
        <span class="hidden-xs">Período <span class="glyphicon glyphicon-question-sign" title="(Opcional) Se souber, informe a data de início da viagem." ></span></span>
        <span class="visible-xs">Período</span>
    </label>
    
	<div class="span5 col-md-5" id="date-container" style="z-index: 0;">
		<div class="input-daterange input-group" id="inputPeriodoViagem">
	    	<form:input type="text" id="inputDataInicioViagem" path="dataInicio" class="form-control dataInicio periodoViagem" rel="periodo" placeholder="Início" />
	    	<span class="input-group-addon">a</span>
	    	<form:input type="text" id="inputDataFimViagem" path="dataFim" class="form-control dataFim periodoViagem" rel="periodo" placeholder="Término" />
		</div>
	</div>
    
    <%--div class="col-sm-3">
        <input type="hidden" name="hoje" id="hoje" value="<%=new LocalDate().toString("dd/MM/yyyy")%>"/>
        <form:input type="${isMobile ? 'text' : 'text'}" path="dataInicio" id="inputDataInicioViagem" rel="periodo" class="form-control inputDataInicio" placeholder="Informe uma data" />
    </div>
    <label for="inputDataFimViagem" class="col-sm-1 control-label hidden-xs" style="width: 10px; padding-right: 5px; padding-left: 2px;">
        <span>a</span>
    </label>
    <label for="inputDataFimViagem" class="col-sm-3 control-label visible-xs">
        <span>Data término</span>
    </label>
    <div class="col-sm-3">
        <form:input type="text" path="dataFim" id="inputDataFimViagem" rel="periodo" class="form-control" placeholder="Informe uma data" />
    </div--%>
  </div>
  <div class="form-group">
    <label for="inputDias" class="col-sm-3 control-label">
        Quantos dias
        <span class="glyphicon glyphicon-question-sign" title="(Opcional) Se souber, informe a duração (em dias) de sua viagem."></span>
    </label>
    <div class="col-sm-3">
        <form:input type="number" path="quantidadeDias" min="1" id="inputQtdDias" class="form-control" placeholder="Nº de dias" />
    </div>
  </div>
  <%-- div class="form-group">
      <label for="selectVisiblidade" class="col-sm-3 control-label">
        Visibilidade
        <span class="glyphicon glyphicon-question-sign" title="Informe quem poderá ver sua viagem."></span>
      </label>
      <div class="col-sm-5">
        <form:select path="visibilidade" id="selectVisiblidade" class="form-control">
          <option value="<%=TipoVisibilidade.AMIGOS%>">Amigos</option>
          <option value="<%=TipoVisibilidade.PUBLICO%>">Público</option>
          <option value="<%=TipoVisibilidade.SOMENTE_EU%>">Somente eu</option>
        </form:select>
      </div>
  </div--%>

  <c:if test="${not empty viagem.id}">
      <div class="form-group">
        <label for="inputData" class="col-sm-3 control-label">
            Controlar horários?
            <span class="glyphicon glyphicon-question-sign"></span>
        </label>
        <div class="col-sm-8 form-inline">
            <div class="radio col-sm-3">
              <label>
                <input type="radio" name="controlarTempo" id="optionsRadios1" value="true" ${viagem.controlarTempo ? 'checked' : '' }>
                Sim
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="controlarTempo" id="optionsRadios2" value="false" ${viagem.controlarTempo ? '' : 'checked' }>
                Não
              </label>
            </div>
        </div>
      </div>
    
      <div class="form-group">
        <label for="inputData" class="col-sm-3 control-label">
            Controlar gastos?
            <span class="glyphicon glyphicon-question-sign"></span>
        </label>
        <div class="col-sm-8 form-inline">
            <div class="radio col-sm-3">
              <label>
                <input type="radio" name="controlarGastos" value="true" ${viagem.controlarGastos ? 'checked' : '' }>
                Sim
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="controlarGastos" value="false" ${viagem.controlarGastos ? '' : 'checked' }>
                Não
              </label>
            </div>
        </div>
      </div>
  </c:if>

  <%--div class="form-group">
  
    <label for="inputDescricao" class="col-sm-3 control-label">Descrição</label>
    <div class="col-sm-9">
        <form:textarea id="inputDescricao" path="descricao" class="form-control" rows="3"></form:textarea>
    </div>
  </div--%>
</form:form>
</compress:html>

<compress:js enabled="true" jsCompressor="closure">
<script>

    moment.lang('pt-BR');

    $('#inputDestinos').autoSuggest('<c:url value="/pesquisaTextual/consultar" />', {
        asHtmlID : 'destinos',
        queryParam: 'term',
        selectedValuesProp : 'id',
        selectedItemProp: 'nomeComSiglaEstado', 
        searchObjProps: 'nomeComSiglaEstado',
        startText: '',
        minChars: 0,
        extraParams: '&types=4&types=12&extraParams[cidadeDestaque]=true', 
        resultsHighlight: false,
        emptyText: 'Esse destino ainda não está disponível no TripFans.',
        retrieveComplete: function(data) {
        	return data.resultados; 
        },
        formatList: function(data, elem) {
        	var urlImage = data.urlFotoSmall;
            var flag = data.siglaPais;
            var new_elem = elem.html('<img src="' + urlImage + '" class="mini-avatar" style="margin-right: 5px;" />' + data.nomeComSiglaEstado + '<i class="' + (flag != null ? ('flag flag-' + flag.toLowerCase()) : '') + '" title="' + data.nomePais + '" />');
            return new_elem;
        },
        preFill: [
          <c:forEach var="destinoViagem" items="${viagem.destinosViagem}">
            {id: '${destinoViagem.destino.id}', nomeComSiglaEstado: '${destinoViagem.destino.nomeComSiglaEstado}'},
          </c:forEach>
        ]
    });
    
    var i = document.createElement("input");
    i.setAttribute("type", "date");
    
    // Verificar se é mobile e se há suporte nativo para date picker
    //if (i.type == "text" || '${isMobile}' != 'true') {
        
      // No native date picker support :(
      // Use Dojo/jQueryUI/YUI/Closure to create one, then dynamically replace that <input> element.
      
        /*$('#inputDataInicioViagem').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            // Hoje
            //minDate : 1,
            position: {
                my: 'left top',
                at: 'right top'
            },
            onSelect: function(dateText, inst) {
                $('#inputDataFimViagem').datepicker('option', 'minDate', dateText );
                $(this).trigger('change');
            }
        });
        
        $('#inputDataFimViagem').datepicker({
            // Hoje
            minDate : 1,
            position: {
                my: 'left',
                at: 'right'
            }
        });*/
        
        $('#inputPeriodoViagem').datepicker({
        	inputs: $('.periodoViagem').toArray(),
        	language: 'pt-BR',
        	format: 'dd/mm/yyyy'
        });
        
        $('#inputDataInicioViagem').datepicker().on('changeDate', function(e) {
        	$(this).datepicker('hide');
        	$('#inputDataFimViagem').focus();
        	$('#inputDataFimViagem').datepicker('show');
        });
        
        //$('#inputDataFimViagem').datepicker();
        
        /*$("#datepicker").on("changeDate", function(event) {
            $("#my_hidden_input").val(
                $("#datepicker").datepicker('getFormattedDate')
             )
        });*/

    //}

    var $form = $('#frm-editar-viagem');
    $form.unbind('submit');

    $form.submit(function(event) {
        event.preventDefault();
        
        var destinos = $('input[name="as_values_destinos"]').val();
        // remover ',' do campo destinos
        /*if (destinos.charAt('0') == ',') {
            destinos = destinos.substring(1);
        }
        destinos.charAt(destinos.length)
        if (destinos.charAt(destinos.length - 1) == ',') {
            destinos = destinos.substring(0, destinos.length -1);
        }*/
        if (destinos == ',') {
            destinos = '';
        }
        $('input[name="as_values_destinos"]').val(destinos);
        var formData = $form.serialize();
        
        if ($form.valid()) {
        	$('#btn-salvar-viagem').button('loading');
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: formData,
                success: function(data) {
                    if (data.success) {
                    	$('#editar-viagem-modal').modal('hide');
                    	$('#btn-salvar-viagem').button('reset');
                        /*$.gritter.add({
                            title: 'Informação',
                            text: data.params.message,
                            time: 4000,
                            image: '<c:url value="/resources/images/info.png" />'
                        });*/
                        if (typeof(aposInclusaoViagem) != 'undefined') {
                            aposInclusaoViagem(data.params.idViagem);
                        } else {
                            setTimeout(function() {
                                if (typeof(recarregarPagina) != 'undefined') {
                                    recarregarPagina();
                                }
                            }, 500);
                        }
                    }
                }, failure : function() {
                	$('#btn-salvar-viagem').button('reset');
                }
            });
        }
    });
    
    /*$('input[rel="periodo"]').change(function() {
        var dataInicio = moment($('input[name="dataInicio"]').val(), "DD/MM/YYYY");
        var dataFim = moment($('input[name="dataFim"]').val(), "DD/MM/YYYY");
        var $inputQtdDias = $('input[name="quantidadeDias"]');
        var qtdDias = $inputQtdDias.val();
        if (dataInicio.isValid()) {
            if (dataFim.isValid()) {
                var diferenca = dataFim.diff(dataInicio, 'days') + 1;
                if (diferenca) {
                    $inputQtdDias.val(diferenca);
                }
            } else if ($('input[name="dataFim"]').val() != '' && (!isNaN(parseInt(qtdDias)))) {
                dataInicio.add('days', qtdDias);
                $('input[name="dataFim"]').val(dataInicio.format('DD/MM/YYYY'));
            }
        }
    });
    
    $('input[name="quantidadeDias"]').change(function() {
        var data = moment($('input[name="dataInicio"]').val(), "DD/MM/YYYY");
        var qtdDias = $(this).val();
        if (qtdDias && data.isValid()) {
            data.add('days', qtdDias - 1);
            $('input[name="dataFim"]').val(data.format('DD/MM/YYYY'));
        }
    });*/
    
    $('input[rel="periodo"]').change(function() {
        var dataInicio = moment($('input[name="dataInicio"]').val(), "DD/MM/YYYY");
        var dataFim = moment($('input[name="dataFim"]').val(), "DD/MM/YYYY");
        var $inputQtdDias = $('input[name="quantidadeDias"]');
        var qtdDias = $inputQtdDias.val();
        if (dataInicio.isValid()) {
            if (dataFim.isValid()) {
                var diferenca = dataFim.diff(dataInicio, 'days') + 1;
                if (diferenca) {
                    $inputQtdDias.val(diferenca);
                }
            } else if ($('input[name="dataFim"]').val() != '' && (!isNaN(parseInt(qtdDias)))) {
                dataInicio.add('days', qtdDias);
                $('input[name="dataFim"]').val(dataInicio.format('DD/MM/YYYY'));
                //$('#inputPeriodoViagem').datepicker('setEndDate', '2011-03-05');
            }
        }
    });
    
    $('input[name="quantidadeDias"]').change(function() {
        var data = moment($('input[name="dataInicio"]').val(), "DD/MM/YYYY");
        var qtdDias = $(this).val();
        if (qtdDias && data.isValid()) {
            data.add('days', qtdDias - 1);
            $('input[name="dataFim"]').val(data.format('DD/MM/YYYY'));
            $('#inputDataFimViagem').datepicker('setDate', data.format('DD/MM/YYYY'));
        }
    });
    
    $.validator.addMethod(
        'brazilianDate',
        function(value, element) {
            if (value == null || value == '') {
                return true;
            }
            if (element.type == 'date') {
                return value.match(/^\d\d\d\d?\-\d\d?\-\d\d$/);
            }
            // put your own logic here, this is just a (crappy) example
            return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
        },
        "Por favor, informe uma data no formato 'dd/mm/yyyy'"
    );
    
    $.validator.addMethod("dateGreaterThan", 
        function(value, element, params) {
            var date = moment(value, "DD/MM/YYYY");
    
            if (date.isValid()) {
                var otherDate = moment($(params[0]).val(), "DD/MM/YYYY");
                return date.isAfter(otherDate);
            } else {
                return true;
            }
            return isNaN(value) && isNaN($(params[0]).val()) 
                || (Number(value) > Number($(params[0]).val())); 
        }, 'A data deve ser maior que {1}.'
    );
    
    $('#inputDataInicioViagem').mask('00/00/0000', {
        onComplete: function(data) {
            $('#inputDataFimViagem').focus();
        }
    });

    $('#inputDataFimViagem').mask('00/00/0000', {
        onComplete: function(data) {
            $('#inputQtdDias').focus();
        }
    });
    
    $form.validate({
        ignore: [],
        rules: {
            //titulo: 'required',
            as_values_destinos: {
                required : true
            },
            dataInicio: {
                required: false,
                brazilianDate: true,
                //dateGreaterThan: ['#hoje', 'Hoje']
            },
            dataFim: {
                required: false,
                brazilianDate: true,
                dateGreaterThan: ['#inputDataInicioViagem', 'Data início']
            },
            quantidadeDias: {
                number: true
            }
        },
        messages: { 
            //titulo: 'Informe um título para identificar sua viagem',
            'as_values_destinos': 'Informe ao menos um destino',
            //dataInicio: 'Informe uma data válida',
            //dataFim: 'Informe uma data válida',
            quantidadeDias: 'Informe um número válido'
        },
        errorPlacement: function(error, element) {
            //(var parent = element.closest('div')[0];
            //error.appendTo(parent);
            error.appendTo('#date-container');
        },
        errorContainer: "#errorBox",
        //onfocusout: false
    });
    
    $form.initToolTips();
        
</script>
</compress:js>