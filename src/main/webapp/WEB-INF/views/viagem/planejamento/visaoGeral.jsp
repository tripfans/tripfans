<%@page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% pageContext.setAttribute("newLineChar", "\n"); %>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">

<style>

body {
	position: relative;
}

label {
    font-weight: normal;
}

.label {
    font-weight: normal;
}

.custom-badge {
    font-weight: normal;
    font-size: 13px;
    color: #5bc0de !important; 
    background-color: #fff !important;
    margin-left: 5px;
}

.header-panel-group {
    display: inline;
    min-width: 180px;
}

.label-horario-atividade {
    margin-left: -81px; margin-right: -20px;
}

.icone-atividade {
    position: absolute; 
    /*top: 5px;*/ 
    z-index: 1;
    left: 5px;
}

.panel-group {
    padding-bottom: 10px;
}

.circle-line {
    margin: 5px;
    display: inline-block;
    color: white;
    border-radius: 50%;
    width: 100% \9;
    max-width: 100%;
    height: auto;
    padding: 4px;
    border: 1px solid #eee;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}

.circle-bg {
    display: inline-block;
    border-radius: 50%;
    width: 100% \9;
    max-width: 100%;
    height: auto;
    padding: 4px;
    background-color: #47a447;
    -webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}

#lista-dias {
    z-index: 1;
    top: 45px;
    bottom: 48px;
    left: 0;
    overflow: hidden;
    height: 150px;
    width: 100px;
    float: right;
}

#ul-lista-dias { 
    list-style-type: none; 
    margin: 0; 
    padding: 0; 
    width: 450px;
}

#ul-lista-dias li {
    margin: 3px 3px 3px 0; 
    padding: 1px; 
    float: left; 
    height: 150px; 
    text-align: center; 
    /*-webkit-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;*/
}

#visao-geral-tabs li a {
	font-size: 15px; 
	margin-right: 0px; 
	/*border-radius: 0px;*/
}

#visao-geral-tabs li:not(.active) a {
	border-color: #eee #eee #ddd;
	background-color: #fefefe; 
}

#visao-geral-tabs li:not(.active) a:hover {
	background-color: #f6f6f6;
}


#visao-geral-tabs li a.active {
	/*border: 1px solid #ddd;*/
}

/*#visao-geral-tabs li a.tab-hotel {
	color: white; 
	background-color: #8cd3e8; 
}

#visao-geral-tabs li a.tab-hotel:hover, #visao-geral-tabs li.active a.tab-hotel {
	background-color: #31b0d5; 
}

#visao-geral-tabs li a.tab-atracao {
	color: white; 
	background-color: #fcca65; 
}

#visao-geral-tabs li a.tab-atracao:hover, #visao-geral-tabs li.active a.tab-atracao {
	background-color: #fbb21c; 
}

#visao-geral-tabs li a.tab-restaurante {
	color: white; 
	background-color: #ee8483; 
}

#visao-geral-tabs li a.tab-restaurante:hover, #visao-geral-tabs li.active a.tab-restaurante {
	background-color: #e74f4e; 
}*/

#visao-geral-tabs li a.tab-hotel {
	/*color: #31b0d5;*/ 
	/*color: #444;*/
	/*border-color: #D8F5FD;*/
	border-color: #D8F5FD #D8F5FD #ddd; 
}

#visao-geral-tabs li a.tab-hotel:hover, #visao-geral-tabs li.active a.tab-hotel {
	background-color: #5bc0de; 
	color: #fff;
}

#visao-geral-tabs li a.tab-atracao {
	/*color: #fbb21c;*/ 
	/*color: #337ab7;*/
	/*border-color: #FFF5E2;*/
	border-color: #FFF5E2 #FFF5E2 #ddd;
}

#visao-geral-tabs li a.tab-atracao:hover, #visao-geral-tabs li.active a.tab-atracao {
	background-color: #f0ad4e; 
	color: #fff;
}

#visao-geral-tabs li a.tab-restaurante {
	/*color: #e74f4e;*/
	/*color: #337ab7;*/
	border-color: #FFECEC #FFECEC #ddd;  
}

#visao-geral-tabs li a.tab-restaurante:hover, #visao-geral-tabs li.active a.tab-restaurante {
	background-color: #d9534f; 
	color: #fff;
}

#sugestoes-modal .modal-dialog {
    height: 96%;
}

#sugestoes-modal .modal-content {
    height: 100%;
}

#sugestoes-modal .modal-header {
    padding: 8px;
    height: 90px;
}

#sugestoes-modal .modal-footer {
    padding: 8px;
    padding-right: 16px;
}

#sugestoes-modal {
    /*overflow: hidden;*/
}

#sugestoes-modal-body {
    /*max-height: 88%;*/
    height: 100%;
    /*overflow-y: hidden;*/
}

.img-local-atividade {
    width: 80px; 
    height: 80px; 
    margin-bottom: 20px; 
    margin-left: -12px; 
    margin-right: -5px;
}

.card-atividade {
    margin-left: 57px; 
    margin-top: 0px; 
    border-left: 1px solid #e0e0e0; 
    min-height: 60px; 
    padding-bottom: 10px;
}

.add-atividade {
    margin-left: 57px; 
    margin-top: 0px; 
    min-height: 60px; 
    padding-bottom: 10px;
}

div.card-atividade div.media-body {
	width: 100%; 
	background-color: #f8f8f8; 
	border: 1px solid #ECECEC;
	border-right: 0px;
	padding: 10px; 
	padding-bottom: 10px;
}

div.card-atividade:hover div.media-body {
	border-right: 0px;
}

div.card-atividade:hover div.acoes-atividade {
	display: table-cell;
}

div.acoes-atividade {
	background-color: #f8f8f8;
	padding-top: 7px; 
	border: 1px solid #ECECEC; 
	border-left: 0px; 
	min-width: 74px; 
	padding-left: 0;
}

@media (min-width: 992px) {
	div.card-atividade div.media-body {
    	border-right: 1px solid #ECECEC;
    }
    
	div.acoes-atividade {
		display: none;
	}

	div.fixed-top-bar.affix {
		top: 70px;
	}
	
	#div-organizar {
		margin-right: 30%; 
		margin-left: 30%;
	}
}

@media (max-width: 767px) {
    .img-local-atividade {
        width: 60px; 
        height: 60px; 
        margin-top: 25px;
    }
    
    .card-atividade {
        margin-left: 8%;
    }
    
    h5.media-heading {
        margin-right: -80px;
    }
    
    #suggestions-div {
        margin: 0 auto;
    }
    
    #sugestoes-modal .modal-dialog {
        margin: 0;
        height: 100%;
    }

	div.fixed-top-bar.affix {
		top: 40px;
	}
}

<c:if test="${permissao.usuarioPodeMoverItem}">

div.sortable div.div-atividade {
	cursor: pointer;
}

/*.card-atividade.selectable:hover div.media-body {
    cursor: pointer;
    border: 1px solid #A3D6A3;
    border-right: 0px;
    background-color: #F7F7F7;
}

.card-atividade.selectable:hover div.media-right {
    border: 1px solid #A3D6A3;
    border-left: 0px;
}

.card-atividade.selected div.media-body {
    border: 1px solid #42D642;
}

.card-atividade.selected:hover div.media-body {
    border: 1px solid #42D642;
    border-right: 0px;
}


.card-atividade.selected div.media-right, .card-atividade.selected:hover div.media-right {
    border: 1px solid #42D642;
    border-left: 0px;
}

.icon-selectable {
    float: right; 
    color: #D3D2D2;
    padding-right: 15px;
}

.card-atividade.selectable:hover .icon-selectable {
    color: #83CD83;
}

.card-atividade.selected .icon-selectable, .card-atividade.selected:hover .icon-selectable {
    color: #5cb85c;
}*/
</c:if>


/*** Novos estilos ***/

div.media.card-atividade div.media-body:hover {
    -webkit-box-shadow: 0 2px 15px rgba(0,0,0,0.30);
    box-shadow: 0 2px 15px rgba(0,0,0,0.30);
}

div.media.card-atividade.selected div.media-body {
  -webkit-box-shadow: 0 0 0 2px #67C767;
  box-shadow: 0 0 0 2px #67C767;
}

div.media.card-atividade div.check {
    -webkit-user-select: none;
    background: #fff;
    -webkit-box-shadow: 0 1px 1px 1px rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 1px rgba(0,0,0,.1);
    -webkit-border-radius: 50%;
    border-radius: 50%;
    height: 25px;
    width: 25px;
    left: -12px;
    opacity: 0.5;
    position: absolute;
    -webkit-transition-duration: .218s;
    transition-duration: .218s;
    -webkit-transition-property: background,opacity;
    transition-property: background,opacity;
    top: -12px;
    z-index: 6;
}

div.media.card-atividade div.media-body:hover > div.check {
    opacity: 1;
}

div.media.card-atividade div.check-inner {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4KPCFET0NUWVBFIHN2ZyAgUFVCTElDICctLy9XM0MvL0RURCBTVkcgMS4xLy9FTicgICdodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQnPgo8c3ZnIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbDpzcGFjZT0icHJlc2VydmUiIGhlaWdodD0iMzQuMDFweCIgdmlld0JveD0iMCAwIDQ2LjUxIDM0LjAxIiB3aWR0aD0iNDYuNTFweCIgdmVyc2lvbj0iMS4xIiB5PSIwcHgiIHg9IjBweCIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDQ2LjUxIDM0LjAxIj4KICA8cG9seWdvbiBwb2ludHM9IjQ2LjUxIDIuNzcxIDQzLjczOSAwIDE1LjI1NSAyOC40ODQgMi43NzEgMTYgMCAxOC43NzEgMTUuMjM5IDM0LjAxIDE1LjI1NSAzMy45OTQgMTUuMjcxIDM0LjAxIiBmaWxsPSIjMDAwIi8+Cjwvc3ZnPgo=) center no-repeat;
    -webkit-background-size: 10px 10px;
    background-size: 10px 10px;
    cursor: pointer;
    height: 100%;
    opacity: .8;
    -webkit-transition-duration: .1s;
    transition-duration: .1s;
    -webkit-transition-property: background-size,opacity;
    transition-property: background-size,opacity;
    width: 100%;
}

div.media.card-atividade.selected div.check {
    background-color: #5CB85C;
    opacity: 1;    
}

div.media.card-atividade.selected div.check-inner {
    background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNy4wLjIsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4KPCFET0NUWVBFIHN2ZyAgUFVCTElDICctLy9XM0MvL0RURCBTVkcgMS4xLy9FTicgICdodHRwOi8vd3d3LnczLm9yZy9HcmFwaGljcy9TVkcvMS4xL0RURC9zdmcxMS5kdGQnPgo8c3ZnIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbDpzcGFjZT0icHJlc2VydmUiIGhlaWdodD0iMzQuMDFweCIgdmlld0JveD0iMCAwIDQ2LjUxIDM0LjAxIiB3aWR0aD0iNDYuNTFweCIgdmVyc2lvbj0iMS4xIiB5PSIwcHgiIHg9IjBweCIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDQ2LjUxIDM0LjAxIj4KICA8cG9seWdvbiBwb2ludHM9IjQ2LjUxIDIuNzcxIDQzLjczOSAwIDE1LjI1NSAyOC40ODQgMi43NzEgMTYgMCAxOC43NzEgMTUuMjM5IDM0LjAxIDE1LjI1NSAzMy45OTQgMTUuMjcxIDM0LjAxIiBmaWxsPSIjZmZmIi8+Cjwvc3ZnPgo=) center no-repeat;
    -webkit-background-size: 10px 10px;
    background-size: 10px 10px;
}

</style>

<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
  <div style="padding-bottom: 4px;">
    <a href="<c:url value="/viagem/planejamento/inicio"/>">Minhas Viagens</a> &raquo; ${viagem.titulo}
  </div>
</c:if>
<div>

    <fieldset>
        <fieldset class="fieldset-inner">
            <h3>
              <div id="titulo-viagem"> 
                  <p class="bg-primary" style="padding: 10px;">
                      ${viagem.titulo}
                      <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem and mostrarAcoes}">
                        <a href="#" class="btn-xs btn-editar-viagem" data-campo="inputTitulo" style="color: whitesmoke;" title="Alterar o título da Viagem">
                            <span class="glyphicon glyphicon-pencil"></span>
                            <span class="hidden-sm hidden-xs">
                                Alterar
                            </span>
                        </a> 
                      </c:if>
                  </p>
              </div>
            </h3>
            
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-3" style="margin-right: -15px; min-height: 25px;">
	                <fan:userAvatar user="${viagem.autor}" displayName="false" width="30" height="30" showImgAsCircle="true"
	                                showFrame="false" showShadow="false" imgStyle="margin-top: -10px;"  />
	                <div style="margin-top: -10px; display: inline;">
	                  <div style="display: inline;">
	                    <span>
	                      Por <a href="<c:url value="/perfil/${viagem.autor.urlPath}" />">${viagem.autor.displayName}</a>
	                    </span>
	                    <c:if test="${not empty viagem.dataPublicacao}">
	                      <p>
	                        <small>Em <joda:format value="${viagem.dataPublicacao}" pattern="dd 'de' MMMM 'de' yyyy" /></small>
	                      </p>
	                    </c:if>
	                  </div>
	                </div>
                </div>
                
               <div class="col-md-3" style="padding-left: 0px;">
                 <c:if test="${not empty viagem.participantesViajantes}">
                  <div style="display: inline; margin-right: 5px;">
                    <span class="pull-left">com</span>
                    <c:forEach var="participante" items="${viagem.participantesViajantes}">
                      <c:if test="${not empty participante.usuario.id}">
				        <c:set var="nome" value="${participante.usuario.nome}"/>
				        <fan:userAvatar user="${participante.usuario}" enableLink="false" showImgAsCircle="true" showFacebookMark="false" 
				                  		marginLeft="0" displayName="false" displayNameTooltip="true" width="30" height="30" 
				                  		showFrame="false" showShadow="false" imgStyle="margin-top: -10px; margin-left: 5px;"  />
				      </c:if>
				      <c:if test="${empty participante.usuario.id}">
				        <div style="float: left; margin-left:4px;"> 
				          <div style="margin-bottom: 5px; position: relative;"> 
						    <img class="img-circle" src="<c:url value="/resources/images/anonymous.png"/>" title="${participante.nome}" 
						         style="background-color: #FEFEFE; margin-top: -10px;" width="30" height="30">      
				          </div> 
				        </div>
				        <c:set var="nome" value="${participante.nome}"/>
				      </c:if>
                    </c:forEach>
                   </div>
                 </c:if>
                 <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                   <a id="btn-add-participante" href="#" class="btn-xs">
                       <img src="<c:url value="/resources/images/planejamento/adicionar-small.png"/>" 
                            style="margin-top: -10px;" class="img-circle" title="Adicionar Viajante(s)" />
	                   <c:if test="${empty viagem.participantesViajantes}">
	                   	 Adicionar Viajante(s)
	                   </c:if>
                   </a>
                 </c:if>
                </div>
                 
            </div>
            
            <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
            <h3 style="margin-top: 10px;">
                <small>
                    ${periodoFormatado}
                    <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                      - 
                    </c:if>
                    <c:if test="${not empty viagem.quantidadeDias}">
                      ${viagem.quantidadeDias} dias
                    </c:if>
                    <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                      Sem data definida
                    </c:if>
                    <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem and mostrarAcoes}"> 
                        <a href="#" class="btn-xs btn-editar-viagem hidden-print" data-campo="inputDataInicioViagem">
                          <span class="glyphicon glyphicon-calendar"></span> ${empty periodoFormatado ? 'Definir' : 'Alterar'} período
                        </a>
                    </c:if>
                </small>
            </h3>
            <fieldset style="background-color: white; margin-bottom: 12px;">
                <legend id="painel-destinos">
                    <small>Destino${viagem.destinosViagem.size() > 1 ? 's' : ''}</small>
                </legend>
                <div class="row center-block">
                  <c:set var="urlPathOrigem" value="" />
                  <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                  
                    <c:choose>
                      <c:when test="${viagem.destinosViagem.size() == 1 and not permissao.usuarioPodeAlterarDadosDaViagem}">
                        <div>
                            <h3>
                                <a href="#">
                                    ${destinoViagem.destino.nome}<c:if test="${not empty destinoViagem.destino.localizacao.estado.sigla}">, ${destinoViagem.destino.localizacao.estado.sigla}</c:if>
                                </a>
                                <i class="flag flag-${fn:toLowerCase(destinoViagem.destino.localizacao.pais.sigla)}" title="${destinoViagem.destino.localizacao.pais.nome}"></i>
                            </h3>
                        </div>
                        <c:if test="${not local.fotoPadraoAnonima}">
                          <i class="carrossel-thumb borda-arredondada thumbnail" style="background-image: url('<c:url value="${destinoViagem.destino.urlFotoBig}"/>'); height: 175px; width: 100%; margin: auto;"></i>
                        </c:if>
                      </c:when>
                      <c:otherwise>
                        <c:set var="columns" value="${permissao.usuarioPodeAlterarDadosDaViagem ? 2 : 3}"/>
                        
                        <c:if test="${not status.first && status.index % columns == 0}">
                            <div class="clearfix visible-xs-block"></div>
                        </c:if>
                        <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                            <div class="col-xs-2 col-sm-1" style="margin-top: 15px;">
                              <a href="#" role="button" class="btnAdicionarItem" 
                                 data-tipo="<%=TipoAtividade.TRANSPORTE%>" 
                                 data-dia-inicio="${status.index == 0 ? '1' : ''}" 
                                 data-dia-fim="${status.index == 0 ? '1' : ''}"
                                 data-origem="${not empty urlPathOrigem ? urlPathOrigem : viagem.usuarioCriador.cidadeResidencia.urlPath}"
                                 data-destino="${destinoViagem.destino.urlPath}"
                                 style="margin-bottom: 4px;">
                                    <img src="<c:url value="/resources/images/add-transporte.png"/>" height="30" style="margin: auto;" title="Adicionar Transporte" />
                              </a>
                            </div>
                        </c:if>
                        <div class="col-md-2 col-sm-3 col-xs-4" style="text-align: center; padding-bottom: 15px;">
                          <img src="<c:url value="${destinoViagem.destino.urlFotoSmall}"/>" class="img-circle" style="min-width: 50px; max-width: 50px; height: 50px; margin: auto;" />
                          <div style="margin-left: -20px; margin-right: -20px;">${destinoViagem.destino.nome}</div>
                          <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                              <div>
                                <a href="#" class="btn btn-info btn-xs btn-sugestoes" 
                                   data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" 
                                   data-tipo-local="HOTEL"
                                   data-cidade="${destinoViagem.destino.urlPath}"
                                   data-dia-inicio="${viagem.primeiroDestino.diaInicio.numero}" 
                                   data-dia-fim="${viagem.primeiroDestino.diaFim.numero}"
                                   title="Escolher Hotel em ${destinoViagem.destino.nome}">
                                      <span class="glyphicon glyphicon-plus "></span> Hotel
                                </a>
                              </div>
                          </c:if>
                        </div>
                        <c:if test="${status.last}">
                          <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                            <div class="col-xs-2 col-sm-1" style="margin-top: 15px;">
                              <a href="#" role="button" class="btnAdicionarItem" 
                                 data-tipo="<%=TipoAtividade.TRANSPORTE%>" 
                                 data-dia-inicio="${viagem.quantidadeDias}" 
                                 data-dia-fim="${viagem.quantidadeDias}" 
                                 data-origem="${destinoViagem.destino.urlPath}"
                                 data-destino="${viagem.usuarioCriador.cidadeResidencia.urlPath}"
                                 style="margin-bottom: 4px;">
                                    <img src="<c:url value="/resources/images/add-transporte.png"/>" height="30" style="margin: auto;" title="Adicionar Transporte" />
                              </a>
                            </div>
                          </c:if>
                        </c:if>
                        <c:set var="urlPathOrigem" value="${destinoViagem.destino.urlPath}" />
                      </c:otherwise>
                    </c:choose>
                  </c:forEach>
                  <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                    <div class="col-md-2 col-sm-3 col-xs-4" style="text-align: center;">
                      <a href="#" class="btn-editar-viagem" data-campo="destinos">
                          <div>
                            <img src="<c:url value="/resources/images/planejamento/adicionar-medium.png"/>" style="margin: auto;" title="Adicionar Destino(s)" class="img-circle" />
                          </div>
                          <div>
                            <small>
                                Adicionar
                                <br/>
                                Destino(s)
                            </small>
                          </div>
                      </a>
                    </div>
                  </c:if>                  
                </div>
            </fieldset>
            <div>
                <%-- Companheiros (Viajantes e editores do plano)<br/>--%>
                
                <%-- fieldset style="background-color: white; margin-bottom: 0px;">
                    <legend>
                        <small>Resumo das Atividades</small>
                    </legend>
                
                    <div id="painel-resumo-atividades">
                        <%@include file="painelResumoAtividades.jsp" %>
                    </div>
                </fieldset--%>
                <%-- 
                - Mapa <br/>
                - Custos<br/>
                - Resenha (Resumo)<br/>
                --%>
            </div>
        </fieldset>
        
        
          <div class="btn-toolbar pull-right" style="margin-top: 5px;">
            <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
                <div class="btn-group">
                  <button id="btn-editar-viagem" type="button" class="btn btn-default btn-editar-viagem">
                    <span class="glyphicon glyphicon-edit"></span> Editar
                  </button>
                  <a href="<c:url value="/viagem/${viagem.id}/planejamento/imprimir"/>" target="_blank" class="btn btn-default hidden-xs">
                    <span class="glyphicon glyphicon-print"></span> Imprimir
                  </a>
                  <c:choose>
                    <c:when test="${not viagem.publicado}">
                      <a href="#publicar-viagem-modal" data-toggle="modal" role="button" class="btn btn-primary">
                          <span class="glyphicon glyphicon-book"></span> Publicar
                      </a>
                    </c:when>
                    <c:otherwise>
                      <div class="btn-group">
                          <a href="#" role="button" class="btn btn-default hidden-xs dropdown-toggle" data-toggle="dropdown">
                              <span class="glyphicon glyphicon-share"></span>
                              Compartilhar
                              <span class="caret"></span>
                          </a>
                          <ul class="dropdown-menu pull-right">
                            <li>
                              <a href="#" role="button" onclick="postToFeed(); return false;" title="Compartilhar no Facebook">
                                  <span class="glyphicon glyphicon-share"></span> Facebook
                              </a>
                            </li>
                            <li>
                              <a href=
                              "http://twitter.com/intent/tweet?url=http%3A%2F%2Ftripfans.com.br%2Fviagem%2F${viagem.id}&via=tripfans&related=TripFans&hashtags=viagem,tripfans&lang=pt&text=Acabei%20de%20criar%20um%20roteiro%20de%20viagem%20no%20Tripfans.%0A" title="Compartilhar no Twitter">
                                  <span class="glyphicon glyphicon-share"></span> Twitter
                              </a>
                            </li>
                          </ul>
                      </div>
                    </c:otherwise>
                  </c:choose>
                  <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      Mais
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right">
                      <c:if test="${not viagem.publicado}">
                        <li>
                          <a href="#" role="button" class="visible-xs" data-toggle="modal">
                              <span class="glyphicon glyphicon-share"></span> Compartilhar
                          </a>
                        </li>
                      </c:if>
                      <li>
                          <a href="#modal-duplicar-viagem" data-toggle="modal" role="button">
                            <span class="glyphicon glyphicon-random"></span> Copiar
                          </a>
                      </li>
                      <li>
                        <a href="#modal-excluir-viagem" role="button" data-toggle="modal">
                            <span class="glyphicon glyphicon-trash"></span> Excluir
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
            </c:if>
            
            <c:if test="${not permissao.usuarioPodeAlterarDadosDaViagem and permissao.usuarioPodeVerViagem}">
                <a href="#modal-duplicar-viagem" class="btn btn-success" data-toggle="modal" role="button">
                    <span class="glyphicon glyphicon-random"></span> Criar meu plano a partir deste
                </a>
            </c:if>
          </div>
        
    </fieldset>

    <%--fieldset id="fieldset-teste" style="background-color: #fff">
        <legend>Roteiro / Atividades</legend>
        
        <div style="float: left;">
            <ul id="ul-lista-dias">
              
              <li class="ui-state-default draggable">
                  <fan:cardResponsive type="local" local="${localTeste}" colSize="100" showActions="true"
                                    orientation="vertical" cardStyle="" photoSize="110" photoHeight="100" />
              </li>
              <li class="ui-state-default draggable">2</li>
              <li class="ui-state-default draggable">3</li>
              <li class="ui-state-default draggable">4</li>
              <li class="ui-state-default draggable">5</li>
              <li class="ui-state-default draggable">6</li>
              <li class="ui-state-default draggable">7</li>
              <li class="ui-state-default draggable">8</li>
              <li class="ui-state-default draggable">9</li>
              <li class="ui-state-default draggable">10</li>
              <li class="ui-state-default draggable">11</li>
              <li class="ui-state-default draggable">12</li>
            </ul>
        </div>
        
        <div id="lista-dias" style="height: 150px;">
            <ul style="width: 120px; float: right;">
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        1º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        2º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        3º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        4º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        5º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        6º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        7º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        8º dia
                    </div>
                </li>
                <li class="circle-line droppable">
                    <div class="circle-bg">
                        9º dia
                    </div>
                </li>
            </ul>
        </div>
        
    </fieldset--%>
    
	<!-- Nav tabs -->
	<ul id="visao-geral-tabs" class="nav nav-tabs" role="tablist">
	    <li class="active">
            <a id="btn-tab-roteiro" href="#fieldset-roteiro" role="tab" data-toggle="tab" style="font-size: 15px;">
                <!-- Roteiro --> Dia-a-dia
            </a>
        </li>
	    <li><a id="btn-tab-mapa" href="#tab-mapa" role="tab" data-toggle="tab" style="font-size: 15px;">Mapa</a></li>
	    <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
		    <li>
		    	<a id="btn-tab-hoteis" href="#tab-hoteis" class="btn-sugestoes tab-sugestoes tab-hotel" data-dia-inicio="${numeroDia}" data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" data-tipo-local="<%=LocalType.HOTEL%>" data-acao="selecionar-atividades">
		    	    <span style="margin-right: 20px;">
		    			<img id="img-filtro-atracao" src="${pageContext.request.contextPath}/resources/images/planejamento/hotel-small.png" class="icone-atividade img-circle" width="25" height="25">
		    		</span>
		    		<span>
		    			Escolha Hotéis
		    		</span>
		    	</a>
		    </li>
		    <li>
		    	<a id="btn-tab-atracoes" href="#tab-atracoes" style="font-size: 15px;" class="btn-sugestoes tab-sugestoes tab-atracao" data-dia-inicio="${numeroDia}" data-tipo="<%=TipoAtividade.VISITA_ATRACAO%>" data-tipo-local="<%=LocalType.ATRACAO%>" data-acao="selecionar-atividades">
		    	    <span style="margin-right: 20px;">
		    			<img id="img-filtro-atracao" src="${pageContext.request.contextPath}/resources/images/planejamento/atracao-small.png" class="icone-atividade img-circle" width="25" height="25">
		    		</span>
		    		<span>
		    			Escolha O que Fazer
		    		</span>
		    	</a>
		    </li>
		    <li>
		    	<a id="btn-tab-restaurantes" href="#tab-restaurantes" style="font-size: 15px;" class="btn-sugestoes tab-sugestoes tab-restaurante" data-dia-inicio="${numeroDia}" data-tipo="<%=TipoAtividade.ALIMENTACAO%>" data-tipo-local="<%=LocalType.RESTAURANTE%>" data-acao="selecionar-atividades">
		    	    <span style="margin-right: 20px;">
		    			<img id="img-filtro-atracao" src="${pageContext.request.contextPath}/resources/images/planejamento/restaurante-small.png" class="icone-atividade img-circle" width="25" height="25">
		    		</span>
		    		<span>
		    			Escolha Onde comer
		    		</span>
		    	</a>
		    </li>
		    <li>
		    	<a id="btn-tab-sugestoes-amigos" href="#tab-sugestoes-amigos" role="tab" data-toggle="tab" style="font-size: 15px;">
		    		<span>
		    			Sugestões de Amigos
		    		</span>
		    	</a>
		    </li>
		</c:if>
	</ul>
	
	<div id="div-main-body" style="padding: 5px; border-left: 1px solid #ddd; border-right: 1px solid #ddd; position: relative;" data-spy="scroll" data-target=".navbar-dias">
	    <!-- Tab panes -->
	    <div class="tab-content" >
	        <div class="tab-pane active" id="fieldset-roteiro" style="background-color: #fff;">

                <%--div id=fixed-bar" class="well well-sm fixed-top-bar" data-spy="affix" data-offset-top="10" data-offset-bottom="10" 
                 style="width: 100%; margin-left: 1px; margin-bottom: 0; padding: 0px 6px; right: 0; z-index: 99;">
                    <input type="hidden" id="urlCidadeAtual" value="${destino.urlPath}"/>
                    <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
                      <div class="col-xs-3" style="padding-right: 5px; padding-top: 2px;">
                          <button id="btn-dia-anterior" class="btn btn-success btn-sm pull-right" title="Ir para o dia anterior">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          </button>
                      </div>
                      <div class="col-xs-6" style="padding: 0px;">
                      
                        <div class="navbar-dias" style="display: none;">
                            <ul class="nav nav-tabs" role="tablist">
                              <c:forEach var="dia" items="${viagem.dias}">
                                <li class=""><a href="#painel-atividades-${dia.id}" data-dia-id="${dia.id}" tabindex="-1"></a></li>
                              </c:forEach>
                            </ul>
                        </div>
                        
                        <select id="selectDias" class="form-control" style="font-size: 15px; /*color: #FFF; background-color: ##cb85c;*/">
                            <c:forEach var="dia" items="${viagem.dias}">
                                <c:set var="nDia" value="${dia.numero}" />
                                <c:set var="dDia" value="${dia.dataViagem}" />
                                <option value="${dia.id}">
                                    <c:if test="${not empty nDia}">
                                        ${nDia}º Dia
                                    </c:if>
                                    <c:if test="${not empty dDia}">
                                        - <fmt:formatDate value="${dDia}" pattern="EEE, dd MMM" />
                                    </c:if>
                                    <c:if test="${empty nDia}">
                                        A organizar
                                    </c:if>
                                </option>
                            </c:forEach>
                        </select>
                      </div>
                      <div class="col-xs-3" style="padding-top: 2px; padding-left: 5px;">
                          <button id="btn-dia-proximo" class="btn btn-success btn-sm" title="Ir para o próximo dia">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          </button>
                      </div>
                  </div>
                  
                </div--%>
                            
                <div id="painel-dias">
                
                    <%@include file="painelDia3.jsp" %>
                    
                </div>
		        
				<%-- div id=fixed-bar" class="well well-sm fixed-top-bar" data-spy="affix" data-offset-top="10" data-offset-bottom="10" 
			         style="width: 100%; margin-left: 1px; margin-bottom: 0; padding: 0px 6px; right: 0; z-index: 99;">
			      <input type="hidden" id="urlCidadeAtual" value="${destino.urlPath}"/>
			      <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
	            	  <div class="col-xs-3" style="padding-right: 5px; padding-top: 2px;">
		            	  <button id="btn-dia-anterior" class="btn btn-success btn-sm pull-right" title="Ir para o dia anterior">
		            	  	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		            	  </button>
			          </div>
			          <div class="col-xs-6" style="padding: 0px;">
			          
					    <div class="navbar-dias" style="display: none;">
					    	<ul class="nav nav-tabs" role="tablist">
					    	  <c:forEach var="dia" items="${viagem.dias}">
					      		<li class=""><a href="#painel-atividades-${dia.id}" data-dia-id="${dia.id}" tabindex="-1"></a></li>
					          </c:forEach>
					    	</ul>
					  	</div>
			          
			            <select id="selectDias" class="form-control" style="font-size: 15px; /*color: #FFF; background-color: ##cb85c;*/">
							<c:forEach var="dia" items="${viagem.dias}">
				                <c:set var="nDia" value="${dia.numero}" />
				                <c:set var="dDia" value="${dia.dataViagem}" />
			                    <option value="${dia.id}">
									<c:if test="${not empty nDia}">
							            ${nDia}º Dia
							        </c:if>
							        <c:if test="${not empty dDia}">
							            - <fmt:formatDate value="${dDia}" pattern="EEE, dd MMM" />
							        </c:if>
							        <c:if test="${empty nDia}">
							            A organizar
							        </c:if>
			                    </option>
			                </c:forEach>
			            </select>
			          </div>
			          <div class="col-xs-3" style="padding-top: 2px; padding-left: 5px;">
		            	  <button id="btn-dia-proximo" class="btn btn-success btn-sm" title="Ir para o próximo dia">
		            	  	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		            	  </button>
			          </div>
			      </div>
			      
			    </div>
			    
			    <div style="margin-top: 3px; margin-bottom: 3px;">
		            <a id="toggle-all" href="#" class="btn btn-default btn-xs toggle-all" data-toggle="button">
		                <span class="glyphicon glyphicon-chevron-down"></span>
		                <span class="text">Expandir todos</span>
		            </a>
		            <a id="toggle-all-maps" href="#" class="btn btn-default btn-xs toggle-all-maps" data-toggle="button">
		                <span class="glyphicon glyphicon-eye-open"></span>
		                <span class="text">Exibir todos os mapas</span>
		            </a>
		        </div>
		    
		        <div class="panel-group" id="panel-group-dias">
		            <c:set var="diaID" value="0" />
		            <c:set var="valorDia" value="${viagem.valorItens}" />
		            <c:set var="atividadesOrdenadas" value="${viagem.itensOrdenados}" />
		            
		            <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
		              <div class="panel-group" id="panel-group-0">
		                <div id="painel-atividades-0" class="panel panel-default" style="overflow: initial;" rel="dia" data-id="${diaID}" data-dia-id="${diaID}">
		                  <%@include file="painelDiaNovo.jsp" %>
		                </div>
		              </div>
		            </c:if>
		            
		            <c:forEach var="dia" items="${viagem.dias}">
		              <c:set var="diaID" value="${dia.id}" />
		              <c:set var="numeroDia" value="${dia.numero}" />
		              <c:set var="dataDia" value="${dia.dataViagem}" />
		              <c:set var="valorDia" value="${dia.valor}" />
		              <c:set var="atividadesOrdenadas" value="${dia.itensOrdenados}" />
		              <div class="panel-group" id="panel-group-${dia.id}">
		                <div id="painel-atividades-${dia.id}" class="panel panel-default" style="overflow: initial;" rel="dia" data-id="${diaID}" data-dia-id="${diaID}">
		                  <%@include file="painelDiaNovo.jsp" %>
		                </div>
		              </div>
		            </c:forEach>
		            
		            <div style="text-align: center;">
		                <c:if test="${permissao.usuarioPodeAdicionarDia}">
		                	<c:set var="textoBtnAdicionarDia" value="Adicionar Dia" />
		                	<c:if test="${not empty viagem.dias}"><c:set var="textoBtnAdicionarDia" value="Adicionar +1 Dia" /></c:if>
		                    <button type="button" class="btn btn-primary btn-adicionar-dia" data-loading-text="Aguarde...">
		                        <span class="glyphicon glyphicon-plus"></span> ${textoBtnAdicionarDia}
		                    </button>
		                </c:if>
		            </div>
		            
		        </div>
		        <br/--%>	        
	        </div>
	        <div class="tab-pane" id="tab-mapa" style="height: 400px;">
	        	<div id="tab-mapa-body"></div>
	        	<div id="map_canvas" style="width: 100%; height: 90%; min-height: 300px;"></div>
	        </div>
	        <%-- div class="tab-pane" id="tab-hoteis" style="height: 100%;">
	        	
	        </div>
	        <div class="tab-pane" id="tab-atracoes" style="height: 100%;">
	        	
	        </div>
	        <div class="tab-pane" id="tab-restaurantes" style="height: 100%;">
	        	
	        </div--%>
	        <div class="tab-pane" id="tab-sugestoes-amigos" style="height: 100%;">
	        	<div id="tab-sugestoes-amigos-body"></div>
	        </div>
	    </div>        
	</div>    

</div>

<div id="editar-viagem-modal" class="modal" data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Viagem</h4>
      </div>
      <div id="editar-viagem-modal-body" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> Fechar
        </button>
        <button type="button" id="btn-salvar-viagem" class="btn btn-primary" data-loading-text="Salvando ...">
            <span class="glyphicon glyphicon-ok"></span>
            Salvar
        </button>
      </div>
    </div>
  </div>
</div>

<div id="modal-incluir-participantes" class="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Adicionar viajantes / Compartilhar</h4>
          </div>
          <div class="modal-body">
          	<%@include file="adicionarParticipantes.jsp" %>
          </div>
          <div class="modal-footer">
            <button id="" class="btn btn-success" data-dismiss="modal" aria-hidden="true">
                <span class="glyphicon glyphicon-ok"></span>
                Concluído
            </button>
          </div>
        </div>
    </div>
</div>

<div id="publicar-viagem-modal" class="modal" data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Publicar Viagem</h4>
      </div>
      <div id="publicar-viagem-modal-body" class="modal-body">
        <div id="painel-publicar-viagem">
            <div class="alert alert-warning">
                <p>Ao publicar sua viagem, outras pessoas poderão vê-la.</p>
            </div>
            <p>
                Selecione quem você deseja que veja esta viagem:
            </p>
            <p>
                <div class="span3" style="margin-left: 0;">
                    <input type="hidden" name="visibilidade" />
                    <select name="tipo" id="selectTipoVisibilidade" class="form-control">
                        <c:url var="imageUrl" value="/resources/images/${tipoAtividade.icone}-medium.png"/>
                        <option value="<%=TipoVisibilidade.PUBLICO%>" data-description="Todos os viajantes poderão ver sua Viagem">
                            <%=TipoVisibilidade.PUBLICO.getDescricao()%>
                        </option>
                        <option value="<%=TipoVisibilidade.AMIGOS%>" data-description="Apenas seus amigos poderão ver sua Viagem">
                            <%=TipoVisibilidade.AMIGOS.getDescricao()%>
                        </option>
                    </select>
                </div>
            </p>
        </div>
        <div id="painel-compartilhar-viagem" style="display: none;">
            <div class="alert alert-block alert-success fade in">
                <strong style="font-size: 18px;">Parabéns! Você publicou sua viagem!</strong>
                <p>Compartilhe agora mesmo com seus amigos nas redes sociais.</p>
            </div>
            <p>
              <div class="servicos">
                <ul class="servicos centerAligned">
                    <li id="fb-share" class="facebook">
                        <a href="#" onclick="postToFeed(); return false;" title="Compartilhar no Facebook"></a>
                        <b title="Você compartilhou no Facebook" style="display: none;"></b>
                    </li>
                    <li id="tw-share" class="twitter">
                        <a href="http://twitter.com/intent/tweet?url=<c:url value="${serverUrl}/viagem/${viagem.id}" />" target="_blank" data-lang="pt_BR" title="Compartilhar no Twitter"></a>
                        <b title="Você compartilhou no Twitter"  style="display: none;"></b>
                        <script type="text/javascript" charset="utf-8">
                          window.twttr = (function (d,s,id) {
                            var t, js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
                            js.src="//platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
                            return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
                          }(document, "script", "twitter-wjs"));
                        </script>
                    </li>
                </ul>
              </div>                        
            </p>
            <script>
            
                $('#selectTipoVisibilidade').ddslick({
                    width: '100%',
                    onSelected: function(data) {
                        $('input[name="visibilidade"]').val(data.selectedData.value);
                    }
                });
            
                window.fbAsyncInit = function() {
                    FB.init({appId: '${tripFansEnviroment.facebookClientId}', status: true, cookie: true});
                    twttr.events.bind('tweet', function(event) {
                        $('#tw-share').find('a').hide();
                        $('#tw-share').find('b').show();
                    });
                }
            
                function postToFeed() {
                	 var obj = {
                             method: 'feed',
                             link: '${tripFansEnviroment.serverUrl}/viagem/${viagem.id}',
                             picture: '${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans-vertical.png',
                             name: "${usuario.primeiroNome} acabou de montar um roteiro de viagem no TripFans",
                             caption: "${viagem.titulo}",
                             description: "Veja a viagem de ${usuario.primeiroNome} clicando aqui.",
                         };

                  function callback(response) {
                      if (response['post_id']) {
                    	  $.gritter.add({
          					title: 'Informação',
          					text: 'Compartilhamento foi realizado com sucesso.',
          					time: 4000,
          					image: '<c:url value="/resources/images/info.png" />',
          			  });
                    } else {
                  	  $.gritter.add({
          					title: 'Erro',
          					text: 'Erro ao compartilhar no Facebook.',
          					time: 4000,
          					image: '<c:url value="/resources/images/error.png" />',
          			  });
                    }
                  }
                  
                  FB.ui(obj, callback);
                }                        
            </script>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
            <span class="glyphicon glyphicon-remove"></span> Fechar
        </button>
        <button id="btn-confirmar-publicar-viagem" class="btn btn-primary" data-loading-text="Publicando...">
            <span class="glyphicon glyphicon-ok"></span>
            Publicar
        </button>
      </div>
    </div>
  </div>
</div>

<div id="modal-duplicar-viagem" class="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Copiar viagem</h4>
          </div>
          <div id="duplicar-viagem-modal-body" class="modal-body">
            <p>Deseja realmente fazer uma cópia desta viagem?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>
            <button id="btn-confirmar-duplicar-viagem" class="btn btn-primary" data-viagem-id="" data-loading-text="Aguarde...">
                <span class="glyphicon glyphicon-ok"></span>
                Copiar
            </button>
          </div>
        </div>
    </div>
</div>

<div id="modal-excluir-viagem" class="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Excluir viagem</h4>
          </div>
          <div id="excluir-viagem-modal-body" class="modal-body">
            <p>Deseja realmente excluir esta viagem?</p>
            <p><span class="red"><strong>ATENÇÃO:</strong></span> Esta operação não poderá ser desfeita e todas as informações desta viagem serão perdidas!</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>
            <button id="btn-confirmar-excluir-viagem" class="btn btn-danger" data-viagem-id="" data-loading-text="Aguarde...">
                <span class="glyphicon glyphicon-ok"></span>
                Excluir
            </button>
            
          </div>
        </div>
    </div>
</div>

<div id="modal-excluir-dia" class="modal" data-backdrop="static" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Excluir dia</h4>
          </div>
          <div id="excluir-dia-modal-body" class="modal-body">
            <p>Deseja realmente excluir este dia?</p>
            <p><span class="red"><strong>ATENÇÃO:</strong></span> Todas as atividades deste dia serão perdidas!</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Cancelar
            </button>
            <button id="btn-confirmar-excluir-dia" class="btn btn-danger" data-dia="" data-loading-text="Aguarde...">
                <span class="glyphicon glyphicon-ok"></span>
                Excluir
            </button>
            
          </div>
        </div>
    </div>
</div>

<div id="sugestoes-modal" class="modal" data-backdrop="static" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <%--div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        
        <!--h4 class="modal-title">Escolha os lugares que você deseja ir</h4-->
      </div--%>
      <div id="sugestoes-modal-body" class="modal-body" style="padding: 2px; overflow-y: hidden;">
          <iframe id="iframe-sugestoes" src="" style="border: none; width: 100%; height: 100%;" scrolling="no"></iframe>
      </div>
      <%--div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">
            <span class="glyphicon glyphicon-ok"></span> Concluir
        </button>
      </div--%>
    </div>
  </div>
</div>


<div id="fb-root"></div>

<script>

$(document).ready(function () {
	
	<c:if test="${permissao.usuarioPodeMoverItem}">
		carregarOrdenacao();
	</c:if>
	
	$('div[data-spy="affix"]').affix({
        offset: {
            top: 150, 
            bottom: function () {
                return (this.bottom = $('footer.bs-footer').outerHeight(true))
            }
        }
    });
	
	$('#btn-tab-mapa').click(function(e) {
		e.preventDefault();
		if (!$(this).data('loaded')) {
			$('#tab-mapa-body').load('<c:url value="/viagem/${viagem.id}/mapa"/>', function() {
				initialize();
	        });
			$(this).data('loaded', 'true')
		} else {
			recarregarPontosMapa();
		}
	});
	
	$('#btn-tab-sugestoes-amigos').click(function(e) {
		e.preventDefault();
		$('#tab-sugestoes-amigos-body').load('<c:url value="/viagem/${viagem.id}/sugestoesAmigos"/>', function() {
        });
	});
	
	$('#btn-add-participante').click(function(e) {
        e.preventDefault();
        $('#btn-cancelar-add-pessoa').hide();
        $('#btn-cancelar-direto').show();
        $('#modal-incluir-participantes').modal('show');
        $('#btn-add-viajante').click();
	});

	$('#btn-compartilhar-amigos').click(function(e) {
        e.preventDefault();
        $('#btn-cancelar-add-pessoa').hide();
        $('#btn-cancelar-direto').show();
        $('#modal-incluir-participantes').modal('show');
        $('#btn-compartilhar').click();
	});
	
	/*$('a.tab-sugestoes').click(function(e) {
		e.preventDefault();
		var idTab = $(this).attr('href');
		var tipoLocal = $(this).data('tipo-local');
		$(idTab).load('<c:url value="/viagem/${viagem.id}/${viagem.primeiroDestino.destino.urlPath}/sugestoesLugares?tipoLocal=' + tipoLocal + '&diaInicio=0&wizard=false"/>', function() {
            $('#scroll-div').css('height', $(window).height() - 200);
        });
	});*/
	
	$('#selectDias').change(function() {
		var idDia = $(this).val();
		var target = $('#painel-atividades-' + idDia);
		if ($('#show-' + idDia).hasClass('glyphicon-chevron-down')) {
			$('#show-' + idDia).click();
		}
	    if( target.length ) {
	        $('html, body').animate({
	            scrollTop: target.offset().top - ${isMobile ? '80' : '110'}
	        }, 1000);
	    }
    });
	
	var timeout;
	
	$(document).on('click', '#btn-dia-proximo', function (e) {
	    
		e.preventDefault();
		/*if (timeout) {
    		clearTimeout(timeout);
    	}*/
		/*if ($('#selectDias option:selected').next().val()) {
			$('#selectDias option:selected').next().attr('selected', 'selected');
			$('#btn-dia-anterior').removeClass('disabled');
		} else {
			$(this).addClass('disabled');
		}
		timeout = setTimeout(function() {
			$('#selectDias').trigger('change');
    	}, 200);*/
    	
    	var $btn = $(this)
    	//var dia = $btn.data('dia')
    	
    	var numeroDia = $('#painel-dias div.painel-atividades-dia').data('numero-dia');
    	var idDia = $btn.data('dia')
    	
    	if (numeroDia < ${viagem.quantidadeDias}) {
    	    //$btn.button('loading')
    		$('a.filtro-dia[data-numero-dia=' + (numeroDia + 1) + ']').click();
    	    //$btn.reset();
    	}
    	
	});
	
	$(document).on('click', '#btn-dia-anterior', function (e) {
		e.preventDefault();
		/*if (timeout) {
    		clearTimeout(timeout);
    	}
		if ($('#selectDias option:selected').prev().val()) {
			$('#selectDias option:selected').prev().attr('selected', 'selected');
			$('#btn-dia-proximo').removeClass('disabled');
		} else {
			$(this).addClass('disabled');
		}
		timeout = setTimeout(function() {
			$('#selectDias').trigger('change');
    	}, 200);*/
    	
    	var $btn = $(this)
    	
    	var numeroDia = $('#painel-dias div.painel-atividades-dia').data('numero-dia');
    	var idDia = $btn.data('dia')
    	
    	if (numeroDia > 1) {
    	    //$btn.button('loading')
    		$('a.filtro-dia[data-numero-dia=' + (numeroDia -1) + ']').click();
    	    //$btn.reset();
    	}
	});
	
	$(document).on('click', '#btn-organizar', function (e) {
		e.preventDefault();

    	var url = '<c:url value="/viagem/${viagem.id}/organizarRoteiroAutomaticamente"/>';
        var request = $.ajax({
            url: url, 
            type: 'POST' 
        });
        request.done(function(data) {
        	recarregarPagina();
        });
        request.fail(function(jqXHR, textStatus) {
        	$.gritter.add({
				title: 'Erro',
				text: 'Ocorreu um erro durante a organização do Roteiro.',
				time: 4000,
				image: '<c:url value="/resources/images/error.png" />',
		    });
        });

	});
	
    $('body').scrollspy({ target: '.navbar-dias', offset : ${isMobile ? '80' : '110'} }).on('activate.bs.scrollspy', function (e) {
		e.preventDefault();
		var idDia = $('.navbar-dias').find('li.active a').data('dia-id');
		$('#selectDias option[value="' + idDia + '"]').attr('selected', 'selected')
    });
    
    var aba = '${aba}';
    if (aba !== '' ) {
        var $abaAtual = $('#btn-tab-' + aba);
        $abaAtual.click();
    }
	
	/*$('.draggable').draggable({
		axis: 'y',
		connectToSortable: 'div.panel-atividades.sortable',
	    revert: 'invalid'
	});*/
	
    //$('ul, li').disableSelection();
    
    /*var myScroll = new IScroll('#lista-dias', { mouseWheel: true, scrollbars: true });
    
    $( "#ul-lista-dias" ).sortable();
    $( "#ul-lista-dias" ).disableSelection();
    
    $('.draggable').draggable();
    
    $('.droppable').droppable({
      accept: ".draggable",
      activeClass: "ui-state-hover",
      hoverClass: "ui-state-active",
      drop: function( event, ui ) {
      alert('1')
        $( this )
          .addClass( "ui-state-highlight" )
          .find( "p" )
            .html( "Dropped!" );
      }
    });*/
    
    /*var tour = new Tour({
        template: '<div class="popover tour" style="max-width: inherit;">' +
            '<div class="arrow"></div>' +
            '<h3 class="popover-title"></h3>' +
            '<div class="popover-content"></div>' +
            '<div class="popover-navigation">' +
            '  <div class="btn-group">' +
            '    <button class="btn btn-sm btn-default" data-role="prev">« Anterior</button>' +
            '    <button class="btn btn-sm btn-default" data-role="next">Próximo »</button>' +
            '  </div>' +
            '  <button class="btn btn-sm btn-default" data-role="end">Fechar</button>' +
            '</div>' +
          '</div>',   
        steps: [
            {
              //element: "#titulo-viagem",
              orphan: true,
              title: 'Você criou o seu roteiro!',
              content: '<p>Esta é a pagina principal para editar seu roteiro. Aqui você poderá:</p>' +
                  '<ul>' +
                  '<li> Escolher atrações para visitar, restaurantes, hotéis;</li>' +
                  '<li> Registrar os horários de voos e outros meios de transporte;</li>' +
                  '<li> Receber sugestões de lugares e dicas de seus amigos;</li>' +
                  '<li> Adicionar anotações e muito mais...</li>' +
                  '<ul>',
              backdrop: true
            },
            {
                element: '#btn-add-hotel',
                title: 'Escolha onde se hospedar',
                content: 'Informe onde irá se hospedar ou escolha dentre os mais recomendados pela comunidade de viajantes',
                backdrop: true
            },
            {
                element: '#btn-add-atividade',
                title: 'Escolha onde ir, o que fazer, onde comer, etc',
                content: '',
                backdrop: true
            },
            {
                element: '#btn-add-tranporte',
                title: 'Meios de transporte',
                content: 'Registre os seus voos, ônibus, trens e outros meios de transporte que utilizará durante sua viagem',
                backdrop: true
            },
            {
                element: '#btn-add-transporte',
                title: 'Meios de transporte',
                content: 'Registre os seus voos, ônibus, trens e outros meios de transporte que utilizará durante sua viagem',
                backdrop: true
            }
        ]
    });

    // Initialize the tour
    tour.init();

    tour.restart(); // Added this

    // Start the tour
    tour.start(); */
    
});

function recarregarPagina() {
    //carregarPagina('visaoGeral');
    window.location.href = '<c:url value="/viagem/${viagem.id}" />'
}

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
</compress:html>