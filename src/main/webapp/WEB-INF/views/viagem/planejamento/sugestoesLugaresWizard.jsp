<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>

<%-- --%>
<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<style>
.mm-toggle {
    cursor: pointer;
}

#locations li {
    cursor: pointer;
}

.media {
    margin-top: 1px; 
    margin-bottom: 0px;
    padding: 4px;
    /*border-top: 1px solid #eee;
    border-bottom: 1px solid #eee;*/
}

.media-body {
    width: 100%;
}

.media-heading {
    margin-bottom: 0px;
}

.card {
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.card.selectable {
    border-top: 0px;
}
    
.card.selectable:hover {
    cursor: pointer;
    border: 1px solid #A3D6A3;
    border-left: 0px;
    border-right: 0px;
    margin-left: 0;
    margin-top: -1px;
    margin-bottom: -2px;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-right: 6px;
    background-color: #F7F7F7;
    /*margin-top: -1px;*/    
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.card.selected, .card.selected:hover {
    border: 1px solid #A3D6A3;
    border-left: 0px;
    border-right: 0px;
    margin-left: 0;
    margin-top: -1px;
    margin-bottom: -2px;
    padding-top: 5px;
    padding-bottom: 5px;
    padding-right: 8px;
    background-color: #E9FAE9;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.icon-selectable {
    float: right; 
    color: #D3D2D2;
    padding-right: 15px;
}

.card.selectable:hover .icon-selectable {
    color: #83CD83;
}

.card.selected .icon-selectable, .card.selected:hover .icon-selectable {
    color: #5cb85c;
    /*padding-right: 17px;*/
}


</style>

<div id="suggestions-div" style="position: relative; min-height: 100%;">

    <div id="scroll-div" class="row-fluid" style="overflow-x: hidden; overflow-y: scroll; min-height: 500px;">
        <div class="borda-arredondada module-box" style="margin-bottom: 20px;">
            <div>
                <div class="lista-locais" style="padding: 2px;">
                  <c:choose>
                    <c:when test="${not empty sugestoesLocais}">
                        <div id="lista-locais-scroll" class="col-md-12 infinite-scroll" style="padding-right: 0px; margin-bottom: 80px;">
                            <%@include file="listaSugestoesLugares.jsp" %>
                        </div>
                    </c:when>
                    <c:otherwise>
                        No momento, não temos sugestões para esta Cidade.
                    </c:otherwise>
                  </c:choose>
                </div>
            </div>
        </div>
    </div>
    
    <c:if test="${not empty camposAgrupados}">
    
    <form id="optionsForm">
      <input type="hidden" name="nome" />
      <input type="hidden" name="idsCategorias" />
      <input type="hidden" name="idsClasses" />
      <input type="hidden" name="estrelas" />
      <input type="hidden" name="bairro" />
      <input type="hidden" name="ordem" />
      <input type="hidden" name="direcaoOrdem" />
    
      <nav id="options-menu">
        <div id="app">
            <ul id="settings">
                <li class="Label">&nbsp;</li>
                <%-- li id="setting-location">
                    <em class="Counter">
                        ${destino.nome}
                    </em>
                    <input type="hidden" name="idCidade" value="${destino.id}" />
                    <span>Cidade</span>
                    <ul id="locations">
                        <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                            <li data-local-id="${destinoViagem.destino.id}"><span>${destinoViagem.destino.nome}</span></li>
                        </c:forEach>
                    </ul>
                </li--%>
                
                <c:forEach items="${camposAgrupados}" var="campoAgrupado">
                  
                  <c:if test="${not empty campoAgrupado.values and not empty campoAgrupado.name}">

                        <%-->em class="Counter">5 Km</em--%>
                        <c:if test="${campoAgrupado.name == 'estrelas'}">
                            <c:set var="nomeAgrupamento" value="Estrelas" />
                        </c:if>
                        <c:if test="${campoAgrupado.name == 'idsCategorias'}">
                            <c:set var="nomeAgrupamento" value="Categorias" />
                        </c:if>
                        <c:if test="${campoAgrupado.name == 'idsClasses'}">
                            <c:set var="nomeAgrupamento" value="Classificações" />
                        </c:if>
                        <c:if test="${campoAgrupado.name == 'bairro'}">
                            <c:set var="nomeAgrupamento" value="Bairros" />
                        </c:if>
                        
                        <li class="Label Spacer" data-toggle="collapse" data-target="#lista_${nomeAgrupamento}" style="cursor: pointer;">
                            ${nomeAgrupamento}
                        </li>
                        
                        <ul id="lista_${nomeAgrupamento}">
                          <li>
                            <span>Todos</span>
                            <input type="checkbox" id="chkTodos-${campoAgrupado.name}" name="${campoAgrupado.name}_todos" value="TODOS" rel="CHK_TODOS" data-grupo="${campoAgrupado.name}" style="display: inline-block;" checked="checked" class="Toggle" />
                          </li>
                          <c:forEach items="${campoAgrupado.values}" var="valorCampoAgrupado">
                            <li>
                               <c:choose>
                                <c:when test="${valorCampoAgrupado.name == '0'}">
                                  <c:set var="semClassificacao" value="${valorCampoAgrupado.count}" />
                                </c:when>
                                <c:otherwise>
                                  <span>
                                    <c:if test="${campoAgrupado.name == 'estrelas'}">
                                      <img src="<c:url value="/resources/images/star${valorCampoAgrupado.name}.png"/>"/> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                    </c:if>
                                    <c:if test="${campoAgrupado.name != 'estrelas'}">
                                      <c:if test="${campoAgrupado.name == 'idsCategorias'}">
                                        <c:set var="codigoCategoria" value="${valorCampoAgrupado.name}" scope="page" />
                                        ${todasCategoriasLocal.getPorCodigo(codigoCategoria)} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                      </c:if>
                                      <c:if test="${campoAgrupado.name == 'idsClasses'}">
                                        <c:set var="codigoClasse" value="${valorCampoAgrupado.name}" scope="page" />
                                        ${todasClassesLocal.getPorCodigo(codigoClasse)} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                      </c:if>
                                      <c:if test="${campoAgrupado.name == 'bairro'}">
                                         ${valorCampoAgrupado.name} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                      </c:if>
                                    </c:if>
                                  </span>
                                  <input type="checkbox" name="${campoAgrupado.name}" value="${valorCampoAgrupado.name}" rel="CHK" data-grupo="${campoAgrupado.name}" style="display: inline-block;" class="Toggle" />
                                </c:otherwise>
                               </c:choose>
                            </li>
                          </c:forEach>
                        </ul>
                  </c:if>
                        
                </c:forEach>                
                <%--li class="Label Spacer">Tipo da atração</li>
                <li>
                    <span>Praia</span>
                    <input type="checkbox" class="Toggle" />
                </li>
                <li>
                    <span>Museu</span>
                    <input type="checkbox" class="Toggle" />
                </li>
                <li>
                    <span>Parques</span>
                    <input type="checkbox" class="Toggle" />
                </li>

                <!-- footer info -->
                <li class="Footer">
                    <a class="button" href="#">Buscar Locais</a>
                </li--%>
            </ul>

            <!-- header info -->
            <!-- span class="Header">Filtrar Lugares</span-->
        </div>
      </nav>
    </form>
    
    </c:if>
    
</div>   


<div id="terms-of-service"></div>

<script>
   <c:if test="${not unsuportedIEVersion}">
     var termsWidget = new panoramio.TermsOfServiceWidget('terms-of-service', {'width': '100%'});
   </c:if>
   
    var idsLocaisSelecionados = [<c:forEach var="idLocal" items="${locaisJaSelecionados}">${idLocal},</c:forEach>];
   
    jQuery(document).ready(function() {
        $('#scroll-div').bind('scroll', function() {
        	// Is this element visible onscreen?
    		/*var visible = $('a.jscroll-next:last').visible( true );
        	if (visible && !($('a.jscroll-next:last').hasClass('clicked'))) {
        	    $('a.jscroll-next:last').addClass('clicked');
        	    $('a.jscroll-next:last').click();
        	    //$('#btn-mais-sugestoes').click();
        	}*/
            
            if ($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight && (!($('a.jscroll-next:last').hasClass('clicked')))) {
                $('a.jscroll-next:last').addClass('clicked');
                $('a.jscroll-next:last').click();
            }
        });
        
        $("#options-menu").mmenu({
            /*offCanvas   : false,*/
            classes     : 'mm-white',
            header      : true,
            searchfield : {
                add         : true,
                addTo       : '#locations',
                placeholder : 'Address, Suburbs or Regions '
            },
            footer: {
                add: true,
                update: true
            },
            onClick     : {
                setSelected : false
            }
        })
        /*.on("click", "a[href=\"#\"]",
            function() {
                //alert("Thank you for clicking, but that's a demo link.");
                return false;
            }
        )*/.on( "closing.mm", function() {
            
            var somethingChanged = false;
            
            $(':input').each(function () {
                if ($(this).attr('type') == 'checkbox') {
                    if ($(this).data('initialValue') != $(this).attr("checked")) {
                        somethingChanged = true;
                        $(this).data('initialValue', $(this).attr("checked"));
                    }
                } else {
                    if ($(this).data('initialValue') != $(this).val()) {
                        somethingChanged = true;
                    }
                }
            });
            
            if (somethingChanged) {
                var idCidade = $('input[name="idCidade"]').val();
                var urlCidadeAtual = $('#urlCidadeAtual').val();
                
                var url = '<c:url value="/viagem/${viagem.id}/' + urlCidadeAtual + '/sugestoesLugares?filtro=true&tipoLocal=${tipoLocal}&locaisJaSelecionados="/>'  + idsLocaisSelecionados;
                
                /*var bairros = [];
                $('input[name="bairro"]:checked').each(function() {
                    bairros.push($(this).val());
                });*/
                
                var bairros = $('input[name="bairro"]:checked').map(function () {return this.value;}).get().join(",");
                //url += '&idsCategorias=' + $('input[name="idsCategorias"]').val();
                //url += '&idsClasses=' + $('input[name="idsClasses"]').val();
                //url += '&estrelas=' + $('input[name="estrelas"]').val();
                if (bairros != '') {
                    url += '&bairros=' + encodeURI(bairros);
                }
                
                $('#lista-locais-scroll').load(url, function() {
                });
            }
        });

        $(document).off('click', '#scroll-div div.card.selectable');

        $(document).on('click', '#scroll-div div.card.selectable', function (e) {
            //e.preventDefault();
            var idLocal = $(this).data('local-id');
            var urlLocal = $(this).data('local-url');
            var url = '';
            var add = true;
            var $scrollDiv = $(this);
            
            if ($(this).hasClass('selected')) {
                if (!($(this).hasClass('selectableUnique'))) {
                    var idAtividade = $(this).data('atividade-id');
                    url = '<c:url value="/viagem/removerLocalViagem/${viagem.id}"/>/' + idAtividade;
                    add = false;
                } else {
                    add = false;
                    //return;
                }
            } else {
                url = '<c:url value="/viagem/adicionarLocalEmViagem/${viagem.id}/${not empty diaInicio ? diaInicio : 0}"/>/' + urlLocal;
            }
            
            // se for atracao, restaurante
            //if ('${tipoLocal}' != 'HOTEL') {
            
                var request = $.ajax({
                    url: url,
                    type: 'POST'
                });
                request.done(function(data) {
                    var $btn = $scrollDiv.find('.btn-adicionar-plano');
                    if (add) {
                        /*if ($scrollDiv.hasClass('selectableUnique')) {
                            $('#scroll-div div.card.selected').each(function() {
                                $(this).removeClass('selected');
                            });
                        }*/
                        $scrollDiv.addClass('selected');
                        $btn.addClass('btn-warning');
                        $btn.find('span.glyphicon').addClass('glyphicon-minus');
                        $btn.data('text', $btn.text());
                        $btn.find('span.text').text('Remover');
                        $scrollDiv.data('atividade-id', data.params.idAtividade);
                        idsLocaisSelecionados.push(idLocal);
                    } else {
                    	var i = idsLocaisSelecionados.indexOf(idLocal);
                    	if (i != -1) {
                    		idsLocaisSelecionados.splice(i, 1);
                    	}
                        $btn.removeClass('btn-warning');
                        $btn.find('span.glyphicon').removeClass('glyphicon-minus');
                        $btn.find('span.text').text($btn.data('text'));
                        $scrollDiv.removeClass('selected');
                    }
                });
                request.fail(function(jqXHR, textStatus){
                });
            /*} else {
                var $btn = $scrollDiv.find('.btn-adicionar-plano');
                if (add) {
                    $btn.data('text', $btn.text());
                    if ($scrollDiv.hasClass('selectableUnique')) {
                        $('#scroll-div div.card.selected').each(function() {
                            $(this).removeClass('selected');
                            $(this).find('a.btn span.glyphicon').removeClass('glyphicon-ok');
                            $(this).find('span.text').text($btn.data('text'));
                        });
                    }
                    $btn.find('span.text').text('Selecionado');
                    $btn.find('span.glyphicon').addClass('glyphicon-ok');
                    $scrollDiv.addClass('selected');
                    //$scrollDiv.data('atividade-id', data.params.idAtividade);
                } else {
                    $scrollDiv.removeClass('selected');
                    $btn.find('span.text').text('Selecionar');
                    $btn.find('span.glyphicon ').removeClass('glyphicon-ok');
                }
            }*/
        
        });
        
        $('#btn-concluir-selecao').click(function (e) {
            e.preventDefault();
            
            var idsLocais = '';
            $('#scroll-div div.card.selected').each(function() {
                if (idsLocais != '') {
                    idsLocais += ',';
                }
                idsLocais += $(this).data('local-id');
            });
            
            
            // se for hotel
            /*if ('${tipoLocal}' == 'HOTEL') {
                if (idsLocais != '') {
                    var url = '<c:url value="/viagem/${viagem.id}/adicionarAtividades"/>';
                    var request = $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            dia: ${not empty diaInicio ? diaInicio : 0},
                            idsLocais: idsLocais
                        }
                    });
                    request.done(function(data) {
                        parent.recarregarDia(${not empty idDiaInicio ? idDiaInicio : 0});
                        parent.$('#sugestoes-modal').modal('hide');
                    });
                    request.fail(function(jqXHR, textStatus){
                    });
                } else {
                    parent.$('#sugestoes-modal').modal('hide');
                }
            } else*/
            if (idsLocais != '') {
                parent.recarregarDia(${not empty idDiaInicio ? idDiaInicio : 0});
                parent.$('#sugestoes-modal').modal('hide');
            } else {
                parent.$('#sugestoes-modal').modal('hide');
            }
            
        });
        
        $(':input').each(function() {
            if ($(this).attr('type') == 'checkbox') {
                $(this).data('initialValue', $(this).attr('checked')); 
            } else {
                $(this).data('initialValue', $(this).val()); 
            }
        });
        
        
        $('input[rel="CHK_TODOS"]').click(function(e) {
            var grupo = $(this).data('grupo');
            $('input[rel="CHK"][data-grupo="' + grupo +'"]').removeAttr('checked');
            $(this).attr('checked', 'checked');
        });
        
        $('input[rel="CHK"]').click(function(e) {
            var grupo = $(this).data('grupo');
           $('#chkTodos-' + grupo).removeAttr('checked');
        })
        
        var $settings = $('#settings');

        //  Choose location
        var $set_location = $('#setting-location .mm-counter');
        $('#locations').find( 'li span' ).click(function() {
            $set_location.text( $(this).text() );
            $('input[name="idCidade"]').val($(this).data('local-id'));
            $settings.trigger( 'open.mm' );
        });
        
        //  Choose radius
        /*var $set_radius = $('#setting-radius .mm-counter');
        $('#radius').find( 'li span' ).click(function() {
            $set_radius.text( $(this).text() );
            $settings.trigger( 'open.mm' );
        });*/
        
        //  Show/hide searchresults
        var $results = $('.searchresult');
        $('#locations input').keyup(function() {
            $results[ ( $(this).val() == "" ) ? "hide" : "show" ]();
        });

        //  Choose pricerange
        var $set_range = $('#setting-pricerange .mm-counter'),
            $range_from = $('#price-from'),
            $range_till = $('#price-till');

        $('#pricerange').find( '.button' ).click(function() {
            $set_range.text( $range_from.val() + ' - ' + $range_till.val() );
        });
        
        $("#btn-options").click(function() {
            $("#options-menu").trigger("open.mm");
        });
        
        $('.ddslick-cidades').change(function (e) {
            var url = '<c:url value="/viagem/${viagem.id}/' + $(this).val() + '/sugestoesLugares?filtro=false&tipoLocal=${tipoLocal}&locaisJaSelecionados="/>' + idsLocaisSelecionados;
            $('#options-menu').remove();
            $('#tab-selecionar-atividades').load(url, function() {
                parent.$('#sugestoes-modal').find('#scroll-div').css('height', $(window).height());
                $('#scroll-div').css('height', $(window).height());
                $('#scroll-div').find('div.lista-locais').css('min-height', $(window).height() + 200);
            });
        })
        
        /*$('.ddslick-cidades').ddslick({
            width: '100%',
            //<c:if test="${isMobile}">height: '36px',</c:if>
            //height: $(window).height()/2,
            height: 30px,
            onSelected: function(data) {
                if (data.selectedData.value != $('#urlCidadeAtual').val()) {
                    $('#urlCidadeAtual').val(data.selectedData.value);
                    var url = '<c:url value="/viagem/${viagem.id}/' + data.selectedData.value + '/sugestoesLugares?filtro=false&tipoLocal=${tipoLocal}"/>';
                    $('#options-menu').remove();
                    $('#tab-selecionar-atividades').load(url, function() {
                        parent.$('#sugestoes-modal').find('#scroll-div').css('height', $(window).height());
                        $('#scroll-div').css('height', $(window).height());
                        $('#scroll-div').find('div.lista-locais').css('min-height', $(window).height() + 200);
                    });
                }
            }
        });*/
        
    });
    
    $('.infinite-scroll').jscroll({
        loadingHtml: '<img src="<c:url value="/resources/images/loading.gif"/>" alt="Carregando" /> Carregando...',
        padding: 0,
        nextSelector: 'a.jscroll-next:last',
        autoTrigger: false
        //contentSelector: 'li'
    });
   
    $('#fixed-bar').affix({
        offset: {
            top: 10, 
            bottom: function () {
                return (this.bottom = $('.footer').outerHeight(true))
            }
        }
    });
    
</script>
</compress:html>
