<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
    
        <style>
            .page-header {
                padding-bottom: 9px;
                margin: 10px 0 20px;
                border-bottom: 1px solid #eeeeee;
            }
        </style>
        
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
        
        <script>

            $(document).on('click', '#btn-salvar-viagem', function (e) {
                e.preventDefault();
                $('#frm-editar-viagem').submit();
            });
            
            $(document).on('click', '.btn-criar-viagem', function (e) {
                e.preventDefault();
                $('#editar-viagem-modal-body').load('<c:url value="/viagem/criar"/>?destino=${destino.urlPath}', function() {
                    $('#editar-viagem-modal').modal('show');
                    setTimeout(function() {
                        $('#destinos').focus();
                    }, 500);
                });
            });

            function aposInclusaoViagem(idViagem) {
                window.location.href = '<c:url value="/viagem/criar/"/>' + idViagem + '/2';
            }
            
            function recarregarPagina() {
                window.location.href = '<c:url value="/viagem/planejamento/inicio" />';
            }
            
            $(document).ready(function () {
                if ('${abrirPopup}' == 'true') {
                    $('.btn-criar-viagem:first').click();
                }
                
                /*var tour = new Tour({
                    template: '<div class="popover tour" style="max-width: inherit;">' +
                        '<div class="arrow"></div>' +
                        '<h3 class="popover-title"></h3>' +
                        '<div class="popover-content"></div>' +
                        '<div class="popover-navigation">' +
                        '  <div class="btn-group">' +
                        '    <button class="btn btn-sm btn-default" data-role="prev">« Anterior</button>' +
                        '    <button class="btn btn-sm btn-default" data-role="next">Próximo »</button>' +
                        '  </div>' +
                        '  <button class="btn btn-sm btn-default" data-role="end">Fechar</button>' +
                        '</div>' +
                      '</div>',   
                    steps: [
                        {
                          //element: "#titulo-viagem",
                          orphan: true,
                          title: 'Seja bem-vindo ao planejamento de viagens do TripFans!',
                          content: '<p>Aqui você poderá organizar seu Plano de Viagem em seu computador ou dispositivo móvel a qualquer momento e de qualquer lugar!</p>' +
                              '<ul>' +
                              '<li> Escolha atrações para visitar, restaurantes, hotéis;</li>' +
                              '<li> Registre os horários de voos e outros meios de transporte;</li>' +
                              '<li> Receba sugestões de lugares e dicas de seus amigos;</li>' +
                              '<li> Adicione anotações e muito mais...</li>' +
                              '<ul>',
                          backdrop: true
                        },
                        {
                          element: '#as-selections-destinos',
                          title: 'Destinos da sua Viagem',
                          content: 'Informe as cidades que você irá visitar nesta viagem.'
                        },
                        {
                            element: '#inputDataFimViagem',
                            title: 'Período da sua Viagem',
                            content: '<p>Opcionalmente, você pode informar a data de início e término de sua viagem ou informar quantos dias durará a viagem.</p>' +
                                     '<p>Se você não souber, deixe em branco e siga em frente. A qualquer momento você poderá informar este período.</p>'
                        }
                    ]
                });

                // Initialize the tour
                tour.init();

                tour.restart(); // Added this

                // Start the tour
                tour.start();*/
            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        <div class="col-md-2 left-menu-container">
        </div>
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="col-md-8">
            <div class="bs-docs-section">
                <div class="page-header">
                  <h3 id="overview">Minhas viagens</h3>
                </div>
                <p class="lead"></p>
            </div>
            
            <c:if test="${not empty viagensProximas or not empty viagensRealizadas}">
                <div class="well well-sm" align="right">
                    <a href="#" class="float-right btn btn-success btn-criar-viagem">
                        <span class="glyphicon glyphicon-plus"></span> Criar uma Viagem
                    </a>
                </div>
            </c:if>
            
            <c:choose>
            
              <c:when test="${empty viagensProximas and empty viagensRealizadas}">
                <div class="row" style="text-align: center;">
                    <div class="col-md-12">
                        <img src="<c:url value="/resources/images/icons/mini/64/Leisure-1.png" />" width="45" height="45" />
                        <p>
                              Você ainda não criou nenhum plano de viagem
                        </p>
                        
                        <p>
                          <button type="button" class="btn btn-large btn-success btn-criar-viagem">
                            <span class="glyphicon glyphicon-plus"></span>
                            Criar um Plano de Viagem
                          </button>
                        </p>
                    </div>
                </div>              
              </c:when>
              <c:otherwise>
                <div>
                  <c:if test="${not empty viagensProximas}">
                    <h4>Próximas Viagens</h4>
                    <br/>
                    <c:set var="viagens" value="${viagensProximas}" />
                    <c:set var="marginLeft" value="-15"/>
                    <c:set var="marginRight" value="-15"/>
                    <%@include file="../../planoViagem/listaViagens.jsp" %>
                  </c:if>
                </div>
    
                <div>
                  <c:if test="${not empty viagensRealizadas}">
                    <h4>Viagens Realizadas</h4>
                    <br/>
                    <c:set var="viagens" value="${viagensRealizadas}" />
                    <c:set var="marginLeft" value="-15"/>
                    <c:set var="marginRight" value="-15"/>
                    <%@include file="../../planoViagem/listaViagens.jsp" %>
                  </c:if>
                </div>
              </c:otherwise>
            
            </c:choose>
            
            <%-- c:forEach var="viagem" items="${viagensProximas}">
                <!-- The GRID System -->
                <div class="megafolio-container noborder norounded light-bg-entries">
                    <!-- A GALLERY ENTRY -->
                    <div class="mega-entry cat-two cat-all" id="mega-entry-1" data-src="<c:url value="/resources/images/travel.jpg"/>" data-width="504" data-height="400" data-lowsize="200">
                        <!-- ENTRY COVERCAPTION -->
                        <div class="mega-covercaption mega-square-top mega-landscape-right mega-portrait-bottom mega-white mega-transparent mega-withsocialbar mega-smallcaptions">
                            <div class="mega-title">
                                <a href="<c:url value="/viagem/${viagem.id}" />">
                                    ${viagem.titulo}
                                </a>
                            </div>
                            <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
                            <div>
                                ${periodoFormatado}
                                <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                                  - 
                                </c:if>
                                <c:if test="${not empty viagem.quantidadeDias}">
                                  ${viagem.quantidadeDias} dias
                                </c:if>
                                <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                                  Sem data definida 
                                </c:if>
                            </div>
                            <p>${viagem.descricao}<br/></p>
                        </div>
                        <!-- ENTRY SOCIALBAR -->
                        <div class="mega-socialbar">
                            <span class="mega-leftfloat">Compartilhar</span>
                            <a href="#"><div class="mega-soc mega-facebook mega-leftfloat"></div></a>
                            <a href="#"><div class="mega-soc mega-twitter mega-leftfloat"></div></a>
                            <div class="mega-show-more mega-soc mega-more mega-rightfloat"></div>
                            <a href="#"><div class="mega-soc mega-like mega-rightfloat"><span>2</span></div></a>
                            <a href="#"><div class="mega-soc mega-comments mega-rightfloat"><span>5</span></div></a>
                        </div>
                    </div>
                </div>
            </c:forEach--%>
        </div>
        
        <div id="editar-viagem-modal" class="modal fade" data-backdrop="static" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Criar uma Viagem</h4>
              </div>
              <div id="editar-viagem-modal-body" class="modal-body">
                
              </div>
              <div class="modal-footer">
                <!--button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span> Fechar
                </button-->
                
	                <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
	                	<div class="col-xs-3">
	                	</div>
	                	<div class="col-xs-6" style="text-align: center;">
	                		<ul class="list-inline" style="color: #ccc; padding-top: 6px;">
	                			<li style="color: #0089FF;">&#9679</li>
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                		</ul>
	                	</div>
	                	<div class="col-xs-3">
			                <button id="btn-salvar-viagem" class="btn btn-primary" data-loading-text="Salvando...">
			                    Próximo
			                    <span class="glyphicon glyphicon-chevron-right"></span>
			                </button>
	                	</div>
	                </div>
                
                
              </div>
            </div>
          </div>
        </div>
    </tiles:putAttribute>
        
</tiles:insertDefinition>