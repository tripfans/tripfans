<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<label for="${inputId}" class="col-sm-2 control-label">Endereço</label>
<div class="col-sm-9">
    <input type="text" name="${inputName}" id="${inputId}" class="form-control" value="${inputValue}" />
</div>
