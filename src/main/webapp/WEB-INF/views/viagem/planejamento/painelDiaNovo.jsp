<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="panel-heading" data-id="${diaID}" style="background-color: #428bca; padding: 5px 10px; height: 50px;">
  <h4 class="header-panel-group" data-id="${diaID}" data-dia-id="${diaID}" style="float: left;">
    <a data-toggle="collapse" data-parent="#panel-group-${diaID}" href="#panel-${diaID}" style="text-decoration: none; color: white;">
        <c:if test="${not empty numeroDia}">
            ${numeroDia}º Dia
        </c:if>
        <c:if test="${not empty dataDia}">
            - <fmt:formatDate value="${dataDia}" pattern="EEE, dd MMM" />
        </c:if>
        <c:if test="${empty numeroDia}">
            A organizar
        </c:if>
        <c:if test="${not empty atividadesOrdenadas}">
            <span class="badge custom-badge" title="${fn:length(atividadesOrdenadas)} atividade(s)">${fn:length(atividadesOrdenadas)}</span>
        </c:if>
        <span id="show-${diaID}" class="glyphicon glyphicon-chevron-down btn-xs icon-collapse" title="Expandir"></span>
        <br/>
        <small style="color: #CEFAFA;">
          <c:forEach var="destino" items="${viagem.getDia(numeroDia).destinos}" varStatus="status">
            <c:if test="${empty pais}">
              <c:set var="pais" value="${destino.pais}" scope="page" />
            </c:if>
            <c:if test="${pais.id != destino.pais.id}">
              <i class="flag flag-${fn:toLowerCase(pais.siglaIso)}" title="${pais.nome}"></i>
              <c:set var="pais" value="${destino.pais}" scope="page" />
            </c:if>
            <c:if test="${not status.first}"> / </c:if>
            ${destino.nome}
            <c:if test="${status.last}">
              <i class="flag flag-${fn:toLowerCase(destino.pais.siglaIso)}" title="${destino.pais.nome}"></i>
            </c:if>
          </c:forEach>
          <c:remove var="pais" scope="page" />
        </small>
    </a>
  </h4>
  <!-- color: wheat -->
  <div class="btn-toolbar pull-right" role="toolbar" style="margin-top: 2px;">
    <div class="btn-group btn-group-xs" style="padding-top: 8px;">
      <c:if test="${permissao.usuarioPodeIncluirItem || permissao.usuarioPodeExcluirDia}">
        <c:if test="${permissao.usuarioPodeIncluirItem}">
          <a href="#" role="button" class="btn-sm btnAdicionarItem hidden-xs" data-dia="${diaID}" style="color: white; padding-right: 6px;"><span class="glyphicon glyphicon-plus"></span> Adicionar atividade</a>
        </c:if>
        <c:if test="${permissao.usuarioPodeMoverItem}">
          <a href="#" role="button" class="btn-sm btnMoverSelecionados" data-toggle="modal" data-target="#modal-mover-atividades" data-dia="${diaID}" title="Mover selecionada(s) para outro dia"
             style="color: white; padding-right: 6px; display: none;"><span class="glyphicon glyphicon-transfer"></span> Mover</a>
        </c:if>
        <c:if test="${permissao.usuarioPodeExcluirDia and diaID != 0}">
          <a href="#modal-excluir-dia" role="button" class="btn-sm btnExcluirDia hidden-xs" data-toggle="modal" data-dia="${diaID}" style="color: white; padding-top: 6px;" title="Excluir este dia"><span class="glyphicon glyphicon-trash"></span><!--Excluir dia--></a>
        </c:if>
      </c:if>
      
      <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}"> 
    
      <div class="btn-group btn-group-xs visible-xs">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: white;">
          <span class="visible-xs">
            Opções
            <span class="caret"></span>
          </span>
          <span class="hidden-xs">
            Mais
            <span class="caret"></span>
          </span>
        </a>
        <ul class="dropdown-menu pull-right">
          <c:if test="${permissao.usuarioPodeIncluirItem}">
            <li>
              <a href="#" class="btnAdicionarItem visible-xs" data-dia="${diaID}">
                <span class="glyphicon glyphicon-plus"></span>
                Adicionar atividade
              </a>
            </li>
          </c:if>
          <c:if test="${permissao.usuarioPodeExcluirDia}">
            <li>
              <a type="button" class="visible-xs" data-dia="${diaID}">
                <span class="glyphicon glyphicon-trash"></span>
                Excluir dia
              </a>
            </li>
          </c:if>
          <%-- >li>
              <a href="#" class="">
                <span class="glyphicon glyphicon-retweet"></span>
                Mover selecionadas
              </a>
          </li--%>
        </ul>
      </div>
      
      </c:if>
    </div>
  </div>
</div>
<div id="panel-${diaID}" class="panel-collapse ${diaAberto or diaID == 0 ? 'in' : 'collapse'}">
  <div class="panel-body" style="padding-bottom: 0px;">
    <c:if test="${not empty atividadesOrdenadas}">
    
      <c:if test="${diaID eq 0}">
    	<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem and viagem.possuiAtividadesOrganizar and viagem.quantidadeDias != null}">
    	  <div class="well well-sm">
		    <div id="div-organizar">
				<button id="btn-organizar" type="button" class="btn btn-warning btn-lg btn-block" title="Ao utilizar este recurso, o TripFans tentará organizar suas atividades da seção 'A Organizar' para os dias de seu roteiro. ">
					<span class="glyphicon glyphicon-calendar"></span>
					Organizar roteiro automaticamente
				</button>
		    </div>
		  </div>
        </c:if>
      </c:if>
      
      <div id="map-canvas-dia-${diaID}" class="map-canvas-dia" style="width: 100%; height: 100%; min-height: 200px; margin-bottom: 2px; display: none;" data-dia="${diaID}" data-loaded="false"></div>
      <div style="text-align: center; margin-bottom: 5px;">
        <a href="#" id="link-ver-mapa-${diaID}" class="link-ver-mapa" data-dia="${diaID}">
      		<span class="btn-xs glyphicon glyphicon-map-marker" style="padding-right: 1px;"></span><span class="label-ver-mapa">Ver mapa</span>
      	</a>
      </div>
      
    
      <div class="panel-atividades sortable" data-dia="${diaID}">
        <c:set var="index" value="0" />
        <c:forEach items="${atividadesOrdenadas}" var="item" varStatus="itemStatus">
            
		    <c:if test="${not item.atividadeHospedagem and not item.atividadeTransporte}">
				<c:set var="index" value="${index + 1}" />
			</c:if>
            
            <%@include file="atividadeNovo.jsp" %>
        </c:forEach>
      </div>
        
      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="padding: 0; padding-top: 11px; font-size: 85%; margin-left: -5px; margin-right: -15px; color: #999;">
          <c:if test="${not empty item.dataHoraInicio or not empty item.dataHoraFim}">
            <span class="glyphicon glyphicon-time"></span> <fmt:formatDate value="${item.dataHoraInicio}" pattern="HH:mm" />
          </c:if>
      </div>
    </c:if>
    <c:if test="${empty atividadesOrdenadas and diaID eq 0}">
    	<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
	        <div class="alert alert-info alert-dismissable" style="margin-bottom: 5px;">
	            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	            <p>Adicione aqui os locais que você pretende ir mas ainda não sabe em qual dia de sua viagem.</p>
	            <p>Você poderá reorganizar a qualquer momento movendo os locais para o dia correto ou utilizando o recurso de organização automática.</p>
	        </div>
        </c:if>
    </c:if>

    <c:if test="${empty atividadesOrdenadas}">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="padding: 0; padding-top: 11px; font-size: 85%; margin-left: -5px; margin-right: -15px; color: #999;">
        </div>
    </c:if>
        
    <c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
	    <div class="col-xs-2 col-sm-1 col-md-1 col-lg-1">
	        <img src="<c:url value="/resources/images/planejamento/adicionar-small.png"/>" class="icone-atividade img-circle" title="Adicionar atividades" />
	    </div>
	    <div class="media add-atividade">
	      <div class="media-body" style="width: 100%; padding-top: 7px;">
	          <div class="btn-group">
	              <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown">
	                <span class="hidden-xs">Adicionar</span> Hotel <span class="caret"></span>
	              </button>
	              <ul class="dropdown-menu" role="menu">
	                <li>
	                    <a href="#" role="button" class="btn-sugestoes" data-dia-inicio="${numeroDia}" data-dia-fim="${numeroDia == viagem.quantidadeDias ? numeroDia : numeroDia + 1}" data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" data-tipo-local="HOTEL" data-acao="adicionar-atividade">
	                        <span class="glyphicon glyphicon-map-marker"></span> Já sei onde vou me hospedar
	                    </a>
	                </li>
	                <li>
	                    <a href="#" class="btn-sugestoes" data-dia-inicio="${numeroDia}" data-dia-fim="${numeroDia == viagem.quantidadeDias ? numeroDia : numeroDia + 1}" data-tipo="<%=TipoAtividade.HOSPEDAGEM%>" data-tipo-local="HOTEL" data-acao="selecionar-atividades">
	                        <span class="glyphicon glyphicon-star-empty"></span>
	                        Quero sugestões de hotéis
	                    </a>
	                </li>
	              </ul>
	          </div>
	
	          <div class="btn-group">
	              <button type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown">
	                <span class="hidden-xs">Adicionar</span> O que fazer <span class="caret"></span>
	              </button>
	              <ul class="dropdown-menu" role="menu">
	                <li>
	                    <a href="#" role="button" class="btn-sugestoes" data-dia-inicio="${numeroDia}" data-tipo="<%=TipoAtividade.VISITA_ATRACAO%>" data-tipo-local="ATRACAO" data-acao="adicionar-atividade">
	                        <span class="glyphicon glyphicon-map-marker"></span> Já sei onde quero ir
	                    </a>
	                </li>
	                <li>
	                    <a href="#" class="btn-sugestoes" data-dia-inicio="${numeroDia}" data-tipo="<%=TipoAtividade.VISITA_ATRACAO%>" data-tipo-local="ATRACAO" data-acao="selecionar-atividades">
	                        <span class="glyphicon glyphicon-star-empty"></span>
	                        Quero sugestões de lugares
	                    </a>
	                </li>
	              </ul>
	          </div>
	        
	          <button type="button" class="btn btn-sm btn-default btnAdicionarItem" data-tipo="<%=TipoAtividade.TRANSPORTE%>" data-dia="${diaID}">
	            <span class="hidden-xs">Adicionar</span> Transporte
	          </button>
	      </div>
	      
	    </div>
    </c:if>    
    <%-- c:if test="${permissao.usuarioPodeIncluirItem}">
        <div style="padding-top: 10px;">
          <a href="#" class="btn btn-warning btn-sm btnAdicionarItem visible-xs" data-dia="${diaID}">
            <span class="glyphicon glyphicon-plus"></span>
            Adicionar atividade
          </a>
        </div>
    </c:if--%>
  </div>
  <c:if test="${viagem.controlarGastos}">
      <div class="panel-footer" align="right">
        <h5>
            Subtotal do dia:
            <span class="red">
                <fmt:formatNumber minFractionDigits="2" maxFractionDigits="2">${valorDia}</fmt:formatNumber>
            </span>
        </h5>
      </div>
  </c:if>
</div>
