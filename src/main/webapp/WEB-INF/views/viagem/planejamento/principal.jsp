<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="TripFans - ${viagem.titulo}" />

    <c:choose>
        <c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
            <c:set var="reqScheme" value="https" />
        </c:when>
        <c:otherwise>
            <c:set var="reqScheme" value="http" />
        </c:otherwise>
    </c:choose>
    
	<tiles:putAttribute name="stylesheets">
	
        <c:if test="${not unsuportedIEVersion}">
          <!-- script type="text/javascript" src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR"></script-->
        </c:if>
        
		<script type="text/javascript" src="${reqScheme}://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<%-- script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script--%>
		<script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
		<script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel.js"></script>
        
        <style>
            .page-header {
                padding-bottom: 9px;
                margin: 0px 0 20px;
                border-bottom: 1px solid #eeeeee;
            }
            
            label {
                font-weight: normal;
            }
            
            .custom-badge {
                font-weight: normal;
                font-size: 13px;
                color: #5bc0de !important; 
                background-color: #fff !important;
                margin-left: 5px;
            }
                        
            
            @font-face {
                font-family: 'digi';
                src: url('<c:url value="/resources/fonts/SFDigitalReadout-Heavy.ttf" />');
            }

            span.label-time {
                color: black;
                padding: 0;
            }

            span.label-time.start {
                color: #5cb85c;
            }

            span.label-time.end {
                color: #f0ad4e;
            }

            span.digital-clock {
                color: #555;
                font-family: 'digi';
                font-size: 23px;
                font-weight: normal;
                margin-left: 4px;
                margin-top: -1px;
                position: absolute;
            }
            
            /* FireFox */
            .panel-group .panel {
                overflow: inherit;
            }
            
            @media (max-width: 767px) {
                .box-tipo-atividade {
                    min-width: 25%;
                }
            
                .box-detalhe-atividade {
                    max-width: 75%;
                }
                
                .panel-body {
                    padding: 6px;
                }
                
                .panel-heading {
                    padding: 5px 6px 10px 6px;
                }

                .panel-footer {
                    padding: 5px 6px 10px 6px;
                }
                
                #main-content {
                    padding: 1px;
                }
                
                fieldset {
                    padding: 5px;
                    margin-bottom: 7px;
                }
                
                #fieldset-roteiro {
                    padding: 10px 2px 2px 2px;
                }
                
                #secao-atividades {
                    padding: 0 6px 0 6px;
                }
            }

            .number-label {
				font-family: "Arial", sans-serif;
            	font-size: 10px; 
            	font-weight: bold!important; 
            	line-height: 2;
            	padding: .3em .6em .2em;
			}
            
            .marker-label {
			    color: white;
			    background-color: #563d7c;
			    border: 2px solid #fff;
			    /*font-family: "Lucida Grande", "Arial", sans-serif;*/
			    font-size: 10px;
			    font-weight: bold;
			    text-align: center;
			    white-space: nowrap;
			    line-height: 1;
			    vertical-align: baseline;
			    padding: .2em .4em .2em;
			    border-radius: 50%;
			}
            
        </style>
        
	</tiles:putAttribute>
	
    <tiles:putAttribute name="leftMenu">
    
        <div class="col-md-2 left-menu-container">
    
            <%--div id="left-menu" class="bs-sidebar hidden-print" role="complementary">
                <ul class="nav bs-sidenav">
                    <li class="img">
                        <a href="#">
                            <img class="avatar borda-arredondada" style="background-color: #FEFEFE;" src="<c:url value="/resources/images/travel.jpg"/>" width="50" height="50">
                            <span>${viagem.titulo}</span>
                            <br>
                            <small>${viagem.periodoFormatado}</small>
                        </a>
                    </li>
                    <li class="blackAndWhite colorOnHover">
                        <a id="visaoGeral" href="#" rel="menu-item">
                            <span style="background-image: url('<c:url value="/resources/images/icons/map_magnify.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                            Visão geral
                        </a>
                    </li>
                    <li class="blackAndWhite colorOnHover">
                        <a id="destinosPrincipais" href="#" rel="menu-item">
                            <span style="background-image: url('<c:url value="/resources/images/icons/world.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                            Destinos principais
                        </a>
                    </li>
                    <li class="blackAndWhite colorOnHover">
                        <a id="companheiros" href="#" rel="menu-item">
                            <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
                            Companheiros de viagem
                        </a>
                    </li>
                    <li class="blackAndWhite colorOnHover">
                        <a id="atividades" href="#" rel="menu-item">
                            <span style="background-image: url('<c:url value="/resources/images/icons/clock.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
                            Atividades
                        </a>
                    </li>
                    <li class="blackAndWhite colorOnHover">
                        <a id="dicas" href="#" rel="menu-item">
                            <span style="background-image: url('<c:url value="/resources/images/icons/lightbulb.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
                            Dicas
                        </a>
                    </li>
                </ul>
            </div--%>
        </div>
    </tiles:putAttribute>

	<tiles:putAttribute name="body">

        <style>
            .dd-selected {
                overflow: hidden;
                display: block;
                padding: 0px;
                font-weight: bold;
            }
            .dd-selected-image {
                vertical-align: middle;
                float: left;
                margin: 3px 5px 3px 5px;
                max-width: 64px;
            }
            
            label .dd-option-text {
                font-weight: normal;
                margin-bottom: 0px;
            }
            
            .tituloAtividade {
                padding-top: 3px;
                padding-bottom: 3px;
            }
            
            .icone-atividade {
                position: absolute; 
                /*top: 5px; */
                left: 5px;
            }
            
            div.media.add-atividade {
	            margin-top: -6px;
            }
            
            .font-normal {
                font-weight: normal;
            }

            div .btn-group.open {
                z-index: 11
            }

            .btn-group {
                z-index: 10
            }
            
        </style>    
    
        <div id="main-content" class="col-md-8" style="margin-top: 10px;">
        </div>
        
        <div id="editar-atividade-modal" class="modal " data-backdrop="static" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="editar-atividade-modal-title" class="modal-title">Incluir/Alterar atividade</h4>
              </div>
              <div id="editar-atividade-modal-body" class="modal-body">
                
              </div>
              <div id="editar-atividade-modal-footer-alterar" class="modal-footer" style="display: none;">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span> Cancelar
                </button>
                <button id="btn-salvar-atividade" class="btn btn-primary" data-loading-text="Salvando...">
                    <span class="glyphicon glyphicon-ok"></span>
                    Salvar
                </button>
              </div>
              <div id="editar-atividade-modal-footer-incluir" class="modal-footer" style="display: none;">
                <button id="btnCancelarAtividade" class="btn btn-default" data-dismiss="modal" aria-hidden="true">
                    <span class="glyphicon glyphicon-remove"></span>
                    Cancelar
                </button>
                <button id="btnIncluirAtividade" class="btn btn-primary" data-loading-text="Salvando...">
                    <span class="glyphicon glyphicon-floppy-saved"></span>
                    Salvar
                </button>
                <button id="btnIncluirVariasAtividades" class="btn btn-info" data-loading-text="Salvando...">
                    <span class="glyphicon glyphicon-floppy-open"></span>
                    Salvar e adicionar +
                </button>
              </div>
            </div>
          </div>
        </div>
        
        <div id="modal-excluir-atividade" class="modal" data-backdrop="static" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Excluir atividade</h4>
                  </div>
                  <div id="excluir-atividade-modal-body" class="modal-body">
                    <p>Deseja realmente excluir esta atividade?</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                    <button id="btn-confirmar-excluir-atividade" class="btn btn-danger" data-atividade-id="" data-loading-text="Aguarde...">
                        <span class="glyphicon glyphicon-ok"></span>
                        Excluir
                    </button>
                  </div>
                </div>
            </div>
        </div>
        
        <div id="modal-mover-atividades" class="modal" data-backdrop="static" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Mover atividades</h4>
                  </div>
                  <div id="mover-atividades-modal-body" class="modal-body">
                    <form class="form-horizontal">
	                    <div class="form-group">
	                      <label for="input-dia-destino" class="col-sm-4 control-label">Escolha o dia</label>
	                      <div class="col-sm-6">
	                        <select id="input-dia-destino" class="form-control">
	                          <option value="0">Sem dia definido</option>
	                          <c:forEach var="dia" items="${viagem.dias}">
	                            <option value="${dia.id}" ${dia.id eq atividade.dia.id ? 'selected' : ''}>
	                                ${dia.numero}º dia 
	                                <c:if test="${not empty dia.dataViagem}"> 
	                                  - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
	                                </c:if>
	                            </option>
	                          </c:forEach>
	                        </select>
	                      </div>
                      </form>
                  </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <span class="glyphicon glyphicon-remove"></span> Cancelar
                    </button>
                    <button id="btn-confirmar-mover-atividade" class="btn btn-primary" data-atividade-id="" data-dia-id="" data-dia-origem-id="">
                        <span class="glyphicon glyphicon-ok"></span>
                        Mover
                    </button>
                    <button id="btn-confirmar-mover-selecionados-atividade" class="btn btn-primary" data-atividade-id="" data-dia-id="" data-dia-origem-id="">
                        <span class="glyphicon glyphicon-ok"></span>
                        Mover
                    </button>
                  </div>
                </div>
            </div>
        </div>
        
        <script>
        
        function carregarOrdenacao() {
            $('div.panel-atividades.sortable').sortable({
        		axis: 'y',
                revert: true,
                cursor: 'move',
                stop: function(e, ui) {
                	
                	var $sortable = $(this);
                	var idAtividade = ui.item.data('atividade-id');
                	var posicaoAntiga = ui.item.data('index');
                	var posicao = ui.item.index();
                	if (posicao != posicaoAntiga) {
        	        	var url = '<c:url value="/viagem/${viagem.id}/moverAtividade"/>';
        	            var request = $.ajax({
        	                url: url, 
        	                type: 'POST', 
        	                data: {
        	                	idAtividade: idAtividade,
        	                    posicao: posicao
        	                }
        	            });
        	            request.done(function(data) {
        	                var $parent = ui.item.parent();
        	                redesenharListaAtividades($parent);
        	            });
        	            request.fail(function(jqXHR, textStatus) {
        	            	$sortable.sortable('cancel');
        	            });
                	}
                }
            });
        }
        
        $(document).ready(function () {
        	
        	<c:if test="${not empty statusMessage}">
			$.gritter.add({
				title: '<c:out value="${title}" />',
				text: '<c:out value="${statusMessage}" />',
				image: '<c:url value="/resources/images/${image}.png" />',
				time: 4000
			});
			</c:if>
            
            $('a[rel="menu-item"]').bind('click', function(event) {
                event.preventDefault();
                event.stopImmediatePropagation();
                $('a[rel="menu-item"]').parent().removeClass('active'); // Remove active class from all links
                carregarPagina(this.id);
            });
            
            <c:set var="tab" value="${pagina != null ? pagina : param.tab}" />
            
            carregarPagina('visaoGeral', false, '${not empty aba ? aba : ''}');
            
            $(document).on('click', '.btnAlterarItem', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var idAtividade = $(this).data('id-atividade');
                $('#editar-atividade-modal-body').load('<c:url value="/viagem/editarAtividade/"/>' + idAtividade, function() {
                    $('#editar-atividade-modal-footer-incluir').hide();
                    $('#editar-atividade-modal-footer-alterar').show();
                    $('#editar-atividade-modal-title').html('Alterar atividade');
                    $('#editar-atividade-modal').modal('show');
                    $('#inputDia').focus();
                });
            });
            
        	$(document).on('click', '.filtro-dia', function (e) {
        	    e.preventDefault();
        	    var dia = $(this).data('dia')
            	$('#painel-dias').load('<c:url value="/viagem/${viagem.id}/recarregarDia/"/>' + dia, function() {
            	    carregarOrdenacao();
                });
        	});
            
            $(document).on('click', '.btnExcluirAtividade', function (e) {
            	e.preventDefault();
                e.stopPropagation();
                var idAtividade = $(this).attr('data-atividade-id');
                $('#btn-confirmar-excluir-atividade').attr('data-atividade-id', idAtividade);
            });
            
            $('#btn-confirmar-excluir-atividade').click(function(e) {
            	e.preventDefault();
                var $btn = $(this)
                $btn.button('loading');
                var idAtividade = $(this).attr('data-atividade-id');
                var url = '<c:url value="/viagem/${viagem.id}/excluirItem"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST', 
                    data: {
                        idItem: idAtividade
                    }
                });
                request.done(function(data) {
                    $('#modal-excluir-atividade').modal('hide');
                    $btn.button('reset');
                    $('div[data-atividade-id="' + idAtividade + '"]').hide('explode', {}, 500);
                    $('div[data-atividade-id="' + idAtividade + '"]').remove();
                    recarregarResumoAtividades();
                    $.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');
                });
            });
            
            $(document).on('click', '.toggle-all', function (e) {
                e.preventDefault();
                var $btn = $(this);
                var $spanIcon = $btn.find('span.glyphicon'); 
                var $spanText = $btn.find('span.text'); 
                if (!$btn.hasClass('active')) {
                    $('div.panel-collapse').each(function(index) {
                        if ($(this).hasClass('in')) {
                        	$(this).collapse('hide');
                        }
                    });
                    $spanIcon.removeClass('glyphicon-chevron-up')
                    $spanIcon.addClass('glyphicon-chevron-down');
                    $('span.icon-collapse').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                    $spanText.text('Expandir todos');
                } else {
                    $('div.panel-collapse').each(function(index) {
                        if ($(this).hasClass('collapse')) {
                        	$(this).collapse('show');
                        }
                    });
                    $spanIcon.removeClass('glyphicon-chevron-down')
                    $spanIcon.addClass('glyphicon-chevron-up');
                    $('span.icon-collapse').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                    $spanText.text('Recolher todos');
                }
            });
            
            $(document).on('click', '.link-ver-mapa', function (e) {
                e.preventDefault();
                var $btn = $(this);
                var idDia = $(this).data('dia');
                var $spanText = $btn.find('span.label-ver-mapa');
                
                var $map = $('#map-canvas-dia-' + idDia);
                if (!$btn.hasClass('active')) {
               		$map.show();
               		if (!$map.data('loaded')) {
               			if (carregarMapaDia($(this).data('dia'))) {
               				$map.data('loaded', true);
               			}
               		}
               		$spanText.text('Esconder mapa');
               		$btn.addClass('active')
                } else {
                	$map.hide();
                	$spanText.text('Ver mapa')
                	$btn.removeClass('active')
                }
            });
            
            $(document).on('click', '.toggle-all-maps', function (e) {
                e.preventDefault();
                var $btn = $(this);
                var $spanIcon = $btn.find('span.glyphicon'); 
                var $spanText = $btn.find('span.text'); 
                if (!$btn.hasClass('active')) {
                	$('div.map-canvas-dia').each(function(index) {
                		$(this).hide();
                    });
                    $spanIcon.removeClass('glyphicon-eye-close')
                    $spanIcon.addClass('glyphicon-eye-open');
                    $('span.icon-collapse').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
                    $spanText.text('Exibir todos os mapas');
                    $('span.label-ver-mapa').text('Ver mapa')
                } else {
                	$('div.panel-collapse').each(function(index) {
                        if ($(this).hasClass('collapse')) {
                        	$(this).collapse('show');
                        }
                    });
                	$('div.map-canvas-dia').each(function(index) {
                		var show = $(this).data('loaded');
                		if (!show) {
                			if (carregarMapaDia($(this).data('dia'))) {
                				show = true;
                    			$(this).data('loaded', true);
                			}
                		}
                		if (show) {
                			$(this).show();
                		}
                    });
                    $spanIcon.removeClass('glyphicon-eye-open')
                    $spanIcon.addClass('glyphicon-eye-close');
                    $('span.icon-collapse').removeClass('glyphicon-eye-open').addClass('glyphicon-eye-close');
                    $spanText.text('Esconder todos os mapas');
                    $('span.label-ver-mapa').text('Esconder mapa')
                }
            });
            
            $(document).on('click', '.panel-heading', function (e) {
                var idPanel = $(this).find('a[data-toggle="collapse"]').attr('href');
                var id = $(this).data('id');
                $(idPanel).collapse('show');
                var $btnShow = $('#show-' + id);
                $btnShow.removeClass('glyphicon-chevron-down');
                $btnShow.addClass('glyphicon-chevron-up');
                $btnShow.attr('title', 'Recolher')
                $btnShow.attr('data-original-title', 'Recolher')
            });
            
            $(document).on('click', '.header-panel-group', function (e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var $btnShow = $('#show-' + id);
                var $painel = $('#panel-' + id);
                setTimeout(function() {
                    if ($painel.hasClass('collapse')) {
                        $btnShow.removeClass('glyphicon-chevron-up');
                        $btnShow.addClass('glyphicon-chevron-down');
                        $btnShow.attr('title', 'Expandir')
                        $btnShow.attr('data-original-title', 'Expandir')
                    } else {
                        $btnShow.removeClass('glyphicon-chevron-down');
                        $btnShow.addClass('glyphicon-chevron-up');
                        $btnShow.attr('title', 'Recolher')
                        $btnShow.attr('data-original-title', 'Recolher')
                    }
                }, 500);
                e.stopPropagation();
            });
            
            $(document).on('click', '.btnAdicionarItem', function (e) {
                e.preventDefault();
                var dia = $(this).data('dia');
                var tipo = $(this).data('tipo');
                if (!tipo) {
                    tipo = 'VISITA_ATRACAO';
                }
                
                var diaInicio = $(this).data('dia-inicio')
                var diaFim = $(this).data('dia-fim')
                var origem = $(this).data('origem')
                var destino = $(this).data('destino')
                var url = '<c:url value="/viagem/${viagem.id}/criarAtividade"/>/' + tipo;
                var params = '?';

                if (diaInicio) {
                    params += 'diaInicio=' + diaInicio + '&'; 
                }
                if (diaFim) {
                    params += 'diaFim=' + diaFim + '&';
                }
                if (origem) {
                    params += 'urlPathOrigem=' + origem + '&';
                }
                if (destino) {
                    params += 'urlPathDestino=' + destino; 
                }
                
                $('#editar-atividade-modal-body').load(url + params, function() {
                    $('#editar-atividade-modal-footer-incluir').show();
                    $('#editar-atividade-modal-footer-alterar').hide();
                    $('#editar-atividade-modal-title').html('Incluir atividade');
                    $('#editar-atividade-modal').modal('show');
                    $('#inputDia option[value="' + dia + '"]').attr('selected', 'selected');
                    $('#inputDia').trigger('change');
                    if (tipo) {
                        $('#selectTipoAtividade').find('ul.dd-options li').each(function(index) {
                            if ($(this).find('input.dd-option-value').val() == tipo) {
                                $('#selectTipoAtividade').ddslick('select', { index: index });
                            }
                        })
                    } else {
                        //$('#selectTipoAtividade').focus();
                    }
                    setTimeout(function() {
                        $('input[rel="local"]:visible:first').focus();
                    }, 500);                
                    
                });
            });
            
            $(document).on('click', '#btnCancelarAtividade', function (e) {
                if($('#inputAtualizarQuandoFechar').val() == 'sim') {
                    $('#inputAtualizarQuandoFechar').val('nao');
                    recarregarPagina();
                }
            });
            
            $(document).on('click', '#btnIncluirAtividade', function (e) {
                e.preventDefault();
                $(this).button('loading');
                salvarAtividade($('#formIncluirAtividade'), $(this), true);
            });
            
            $(document).on('click', '#btn-salvar-atividade', function (e) {
                e.preventDefault();
                $(this).button('loading');
                salvarAtividade($('#formAlterarAtividade'), $(this), true);
            });
            
            $(document).on('click', '#btnIncluirVariasAtividades', function (e) {
                e.preventDefault();
                $(this).button('loading');
                salvarAtividade($('#formIncluirAtividade'), $(this), false);
                $('#inputAtualizarQuandoFechar').val('sim');
                $('#selectTipoAtividade').focus();
            });
            
            $(document).on('click', '.btnMovimentarParaDia', function (e) {
                e.preventDefault();
                $('#btn-confirmar-mover-selecionados-atividade').hide();
                var idAtividade = $(this).data('atividade-id');
                var idDia = $(this).data('dia-id');
                $('#btn-confirmar-mover-atividade').attr('data-atividade-id', idAtividade);
                $('#btn-confirmar-mover-atividade').attr('data-dia-origem-id', idDia);
            });
            
            $(document).on('click', '.btnMoverSelecionados', function (e) {
                e.preventDefault();
                $('#btn-confirmar-mover-atividade').hide();
                var idDia = $(this).data('dia');
                $('#btn-confirmar-mover-selecionados-atividade').attr('data-dia-origem-id', idDia);
                $('#btn-confirmar-mover-selecionados-atividade').show();
            });

            $('#btn-confirmar-mover-selecionados-atividade').click(function(e) {
            	e.preventDefault();
            	
                var idDiaOrigem = $(this).attr('data-dia-origem-id');
            	var $painel = $('#painel-atividades-' + idDiaOrigem);
            	
                var idsAtividades = $(this).attr('data-atividade-id');
                
                var idsAtividades = '';
                $painel.find('div.media.card-atividade.selected').each(function() {
                    if (idsAtividades != '') {
                    	idsAtividades += ',';
                    }
                    idsAtividades += $(this).data('atividade-id');
                });
                
                var idDiaDestino = $('#input-dia-destino').val();
                var url = '<c:url value="/viagem/${viagem.id}/movimentarAtividadesParaDia"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST', 
                    data: {
                    	idsAtividades: idsAtividades,
                        idDiaDestino: idDiaDestino
                    }
                });
                request.done(function(data) {
                    $('#modal-mover-atividades').modal('hide');
                    
                    var array = idsAtividades.split(',');
                    for (var i in array) {
                        $('div[data-atividade-id="' + array[i] + '"]').remove();
					}
                    //recarregarDia(idDiaOrigem);
                    recarregarDia(idDiaDestino);
                    if (data.params.diaFim) {
                        recarregarDia(data.params.diaFim);
                    }
                    $.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });
                });
                request.fail(function(jqXHR, textStatus) {
                });            	
            });
            
            $('#btn-confirmar-mover-atividade').click(function(e) {
                e.preventDefault();
                var idAtividade = $(this).attr('data-atividade-id');
                var idDiaOrigem = $(this).attr('data-dia-origem-id');
                var idDiaDestino = $('#input-dia-destino').val();
                var url = '<c:url value="/viagem/${viagem.id}/movimentarItemParaDia"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST', 
                    data: {
                        idItem: idAtividade,
                        idDiaDestino: idDiaDestino
                    }
                });
                request.done(function(data) {
                    $('#modal-mover-atividades').modal('hide');
                    $('div[data-atividade-id="' + idAtividade + '"]').remove();
                    //recarregarDia(idDiaOrigem);
                    recarregarDia(idDiaDestino);
                    if (data.params.diaFim) {
                        recarregarDia(data.params.diaFim);
                    }
                    $.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });
                });
                request.fail(function(jqXHR, textStatus) {
                });
            });
            
            $(document).on('click', '.btnMoverItem', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var idAtividade = $(this).data('atividade-id');
                var idDiaDestino = $(this).data('dia-id');
                var direction = $(this).data('direction');
                var url = $(this).attr('href');
                var request = $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        idItem: idAtividade,
                        idDiaDestino: idDiaDestino
                    }
                });
                request.done(function(data) {
                    var $divAtividade = $('div[rel="atividade"][data-atividade-id="' + idAtividade + '"]');
                    var $parent = $divAtividade.parent();
                    if (direction) {
                        if (direction == 'up') {
                            $divAtividade.insertBefore($divAtividade.prev());
                        } else if (direction == 'down') {
                            $divAtividade.insertAfter($divAtividade.next());
                        } else if (direction == 'first') {
                            $parent.prepend($divAtividade);
                        } else if (direction == 'last') {
                            $parent.append($divAtividade);
                        }
                    }
                    redesenharListaAtividades($parent);
                    /*$.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });*/
                });
                request.fail(function(jqXHR, textStatus) {
                });
            });
            
            $(document).on('click', '.btn-adicionar-dia', function (e) {
                e.preventDefault();
                var $btn = $(this);
                $btn.button('loading');
                var dia = $(this).data('dia');
                var url = '<c:url value="/viagem/${viagem.id}/adicionarDia"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST' 
                });
                request.done(function(data) {
                    setTimeout(function() {
                        carregarPagina('visaoGeral', false);
                        $btn.button('reset');
                    }, 500);
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');
                });
            });
            
            $(document).on('click', '.btnExcluirDia', function (e) {
                e.preventDefault();
                var dia = $(this).data('dia');
                $('#btn-confirmar-excluir-dia').data('dia', dia);
            });

            $(document).on('click', '#btn-confirmar-excluir-viagem', function (e) {
                e.preventDefault();
                var $btn = $(this);
                $btn.button('loading')
                var url = '<c:url value="/viagem/${viagem.id}/excluir"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST'
                });
                request.done(function(message) {
                    $('#modal-excluir-viagem').modal('hide');
                    location.href = '<c:url value="/viagem/planejamento/inicio"/>';
                    $btn.button('reset');
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');
                });
            });
            
            $(document).on('click', '#btn-confirmar-publicar-viagem', function (e) {
                e.preventDefault();
                var $btn = $(this);
                $btn.button('loading');
                var url = '<c:url value="/viagem/${viagem.id}/publicar"/>';
                
                var request = $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        visibilidade : $('input[name="visibilidade"]').val()
                    }
                });
                request.done(function(data) {
                    $('#publicar-viagem-modal').modal('hide');
                    $btn.button('reset');
                    setTimeout(function() {
                        carregarPagina('visaoGeral');
                    }, 500);
                    
                    $.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });
                    $('#publicar-viagem-modal').find('button .close').hide();
                    $('#btn-confirmar-publicar-viagem').hide();
                    $('#painel-publicar-viagem').hide();
                    $('#painel-compartilhar-viagem').show();
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');
                });
            });
            
            $(document).on('click', '#btn-confirmar-duplicar-viagem', function (e) {
                e.preventDefault();
                var $btn = $(this);
                $btn.button('loading');
                window.location.href = '<c:url value="/viagem/${viagem.id}/copiar"/>';
            });
            
            $(document).on('click', '#btn-confirmar-excluir-dia', function (e) {
                e.preventDefault();
                var $btn = $(this);
                $btn.button('loading');
                var dia = $(this).data('dia');
                var url = '<c:url value="/viagem/${viagem.id}/excluirDia"/>';
                var request = $.ajax({
                    url: url, 
                    type: 'POST', 
                    data: {
                        idDia: dia
                    }
                });
                request.done(function(data) {
                    $('#modal-excluir-dia').modal('hide');
                    $btn.button('reset');
                    $('div[data-dia-id="' + dia + '"]').hide('explode', {}, 500);
                    $('div[data-dia-id="' + dia + '"]').remove();
                    $.gritter.add({
                        title: data.title,
                        text: data.message,
                        time: 4000,
                        image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                    });
                    setTimeout(function() {
                        carregarPagina('visaoGeral');
                    }, 500);
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');

                });
            });
            
            $(document).on('click', '.btn-editar-viagem', function (e) {
                e.preventDefault();
                var campo = $(this).data('campo');
                $('#editar-viagem-modal-body').load('<c:url value="/viagem/editar/${viagem.id}"/>', function() {
                    $('#editar-viagem-modal').modal('show');
                    if (campo != null) {
                        setTimeout(function() {
                            $('#' + campo).focus();
                        }, 500);
                    }
                });
            });

            $(document).on('click', '#btn-salvar-viagem', function (e) {
                e.preventDefault();
                $('#frm-editar-viagem').submit();
            });
            
            $(document).on('click', '#btnAdicionarDia', function (e) {
                e.preventDefault();
                var varUrl = $(this).attr('href');
                var request = $.ajax({
                    url: varUrl
                    , type: "POST"
                    , dataType: "html"
                });
                request.done(function(data) {
                    recarregarPagina();
                });
                request.fail(function(jqXHR, textStatus){
                });
            });
            
            $(document).on('click', '.btn-sugestoes', function (e) {
                e.preventDefault();
                var tipoLocal = $(this).data('tipo-local');
                var cidade = $(this).data('cidade');
                var tipoAtividade = $(this).data('tipo');
                var acao = $(this).data('acao');
                var diaInicio = $(this).data('dia-inicio')
                var diaFim = $(this).data('dia-fim')
                
                $("#iframe-sugestoes").attr('src', '<c:url value="/viagem/iframe/${viagem.id}"/>?tipoAtividade=' + tipoAtividade + '&tipoLocal=' + tipoLocal + (cidade != null ? ('&cidade=' + cidade) : '') + (diaInicio != null ? ('&diaInicio=' + diaInicio) : '') + (diaFim != null ? ('&diaFim=' + diaFim) : '') + (acao != null ? '&acao=' + acao : ''));
                $('#sugestoes-modal').modal('show');
                $('#btn-tab-selecionar-atividades').click();
                
                /*$('#sugestoes-modal-body').load('<c:url value="/viagem/${viagem.id}/${primeiroDestino.urlPath}/sugestoesLugares"/>', function() {
                    $('#sugestoes-modal').modal('show');
                });*/
            });
            
            // ------- Selectors referentes à edição de atividade 
            
            var i = document.createElement("input");
            i.setAttribute("type", "date");
            if (i.type == "text") {
              // No native date picker support :(
              // Use Dojo/jQueryUI/YUI/Closure to create one, then dynamically replace that <input> element.
            }
            
            $(document).on('change', 'select[name="tipo"]', function (e) {
                atualizarFieldset($(this).val());
            });
            
            $(document).on('change', 'select[name="itemDono"]', function (e) {
                var dia = $(this).val();
                $('select[name="diaInicio"]').find('option[value="' + dia + '"]').attr('selected', 'selected');
                //atualizarOpcoesDataFimAtividade('<%--=TipoAtividade.TRANSPORTE--%>')
                //atualizarOpcoesDataFimAtividade('<%--=TipoAtividade.HOSPEDAGEM--%>')
                atualizarOpcoesDataFimAtividade('TRANSPORTE')
                atualizarOpcoesDataFimAtividade('HOSPEDAGEM')
            });

            $(document).on('change', 'select[name="diaInicio"][rel="HOSPEDAGEM"]', function (e) {
                var dia = $(this).val();
                $('select[name="itemDono"]').find('option[value="' + dia + '"]').attr('selected', 'selected');
                atualizarOpcoesDataFimAtividade('HOSPEDAGEM');
            });

            $(document).on('change', 'select[name="diaInicio"][rel="TRANSPORTE"]', function (e) {
                var dia = $(this).val();
                $('select[name="itemDono"]').find('option[value="' + dia + '"]').attr('selected', 'selected');
                atualizarOpcoesDataFimAtividade('TRANSPORTE');
            });
            
            $(document).on('change', '#check-entrega-mesmo-local', function (e) {
                if ($(this).is(':checked')) {
                    $('#div-destino-transporte').hide();
                    $('#inputDestinoTransporte').attr('disabled', 'disabled');
                } else {
                    $('#div-destino-transporte').show();
                    $('#inputDestinoTransporte').removeAttr('disabled');
                }
            });
            
            $(document).on('click', '.btn-mais-detalhes', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var $btn = $(this);
                var textoBtnAberto = $btn.data('txt-btn-aberto');
                if (textoBtnAberto == null) {
                    textoBtnAberto = '- Menos detalhes';
                }
                var textoBtnFechado = $btn.data('txt-btn-fechado');
                if (textoBtnFechado == null) {
                    textoBtnFechado = '+ Mais detalhes';
                }
                var $div = $(this).parents('.div-mais-detalhes').find('.campos-mais-detalhes');
                if ($btn.hasClass('active')) {
                    $btn.removeClass('active');
                    $div.hide();
                    $btn.html(textoBtnFechado);
                } else {
                    $btn.addClass('active');
                    $div.show();
                    $btn.html(textoBtnAberto);
                    $div.find('input:first').focus();
                }
            });
            
            <c:if test="${permissao.usuarioPodeMoverItem}">
            $(document).on('click', 'div.media.card-atividade.selectable', function (e) {
            	$(this).toggleClass('selected');
            	// habilitar botao mover

            	var show = false;
            	
				$(this).closest('div.panel-atividades').find('div.media.card-atividade').each(function(index) {
					if ($(this).hasClass('selected')) {
						show = true
					}
				});
				if (show) {
					$('a.btnMoverSelecionados[data-dia="'+ $(this).data('dia-id') +'"]').show();
				} else {
					$('a.btnMoverSelecionados[data-dia="'+ $(this).data('dia-id') +'"]').hide();
				}
            });
            </c:if>
            
        });
        
        function recarregarResumoAtividades() {
            var url = '<c:url value="/viagem/${viagem.id}/recarregarResumoAtividades" />';
            $('#painel-resumo-atividades').load(url, function(response) {});
        }
        
        function recarregarDia(dia) {
            var url = '<c:url value="/viagem/${viagem.id}/recarregarDia/" />' + dia;
            $('#painel-atividades-' + dia).load(url, function(response) {
            });
        }

        function recarregarPainelAtividades(tipoAtividade) {
            var url = '<c:url value="/viagem/${viagem.id}/recarregarPainelAtividades/" />' + tipoAtividade;
            $('#panel-group-' + tipoAtividade).load(url, function(response) {
            });
        }
        
        function carregarPagina(pagina, scrollTop, aba) {
            var url = '<c:url value="/viagem/${viagem.id}/planejamento" />/' + pagina
            $('#'+pagina).parent().addClass('active'); //Set clicked link class to active
            //$(document).off('click', '**');
            if (!scrollTop) {
            } else {
                $('body').scrollTop(0);
            }
            if (aba) {
            	url += '/' + aba
            }
            $('#main-content').load(url, 
                function(response) {
                    <c:if test="${not isMobile}">
                        $('#main-content').initToolTips();
                    </c:if>
                    $('#main-content').initToolTips();
                    History.pushState( url, window.title, url );
                }
            );
        }
        
        function carregarMapaDia(idDia) {

       	    var latlngbounds = new google.maps.LatLngBounds();
       	    var infoWindow;
       	 	var markers = [];
       	    
       	    var mapOptions = {
       	 		center: new google.maps.LatLng(${viagem.primeiroDestino.destino.latitude}, ${viagem.primeiroDestino.destino.longitude}),
	       		zoom: 4,
	       		maxZoom: 15,
	     	    mapTypeId: google.maps.MapTypeId.ROADMAP,
       	 		scrollwheel: false,
       	  	};
       	    
    		var mapDia = new google.maps.Map(document.getElementById('map-canvas-dia-' + idDia), mapOptions);
			var points = [];
			
			var $painelAtividades = $('#panel-' + idDia);
			
			$painelAtividades.find('div.div-atividade').each(function(index) {
	   			$painelAtividade = $(this);
	   			var tipoAtividade = $painelAtividade.data('tipo-atividade')
	   			if (tipoAtividade != 'TRANSPORTE') {
	   				var latitude = $painelAtividade.data('latitude');
					if (latitude) {
						var index = tipoAtividade != 'HOSPEDAGEM' ? $painelAtividade.index() : null;
			   			points.push({
			   				'index': index,
			       	        'id': $painelAtividade.data('atividade-id'),
			       	        'nome': $painelAtividade.data('nome-local'),
			       	        'urlPath': $painelAtividade.data('urlpath-local'),
			       	        'latitude': latitude,
			       	        'longitude': $painelAtividade.data('longitude'),
			       	        'tipo': tipoAtividade, 
			       	        'icone': '<c:url value="/resources/images/markers"/>/' + $painelAtividade.data('icone-atividade') + '-medium-pin.png',
			       	     	'fimAtividade' : $painelAtividade.data('fim-atividade')
			   			});
					}
	   			} else {
	   				/*var latitudeOrigem = $painelAtividade.data('latitude-origem');
	   				var latitudeDestino = $painelAtividade.data('latitude-destino');
					var index = $painelAtividade.index();
					if (latitudeOrigem) {
			   			points.push({
			   				'index': index,
			       	        'id': $painelAtividade.data('atividade-id'),
			       	        'nome': $painelAtividade.data('nome-local-origem'),
			       	        'urlPath': $painelAtividade.data('urlpath-local-origem'),
			       	        'latitude': latitudeOrigem,
			       	        'longitude': $painelAtividade.data('longitude-origem'),
			       	        'tipo': tipoAtividade, 
			       	        'icone': '<c:url value="/resources/images/markers"/>/' + $painelAtividade.data('icone-transporte') + '-medium-pin.png',
			       	     	'fimAtividade' : $painelAtividade.data('fim-atividade')
			   			});
					}
					if (latitudeDestino) {
			   			points.push({
			   				'index': index,
			       	        'id': $painelAtividade.data('atividade-id'),
			       	        'nome': $painelAtividade.data('nome-local-destino'),
			       	        'urlPath': $painelAtividade.data('urlpath-local-destino'),
			       	        'latitude': latitudeDestino,
			       	        'longitude': $painelAtividade.data('longitude-destino'),
			       	        'tipo': tipoAtividade, 
			       	        'icone': '<c:url value="/resources/images/markers"/>/' + $painelAtividade.data('icone-transporte') + '-medium-pin.png',
			       	     	'fimAtividade' : $painelAtividade.data('fim-atividade')
			   			});
					}*/	   				
	   			}
	        });
			if (points.length > 0) {
				var index = 0;
	       		for (var key in points) {
	       			var showIndex = points[key].index != null ? true : false;
	       			if (!points[key].fimAtividade) {
		       	        var marker = new MarkerWithLabel({
		       	            position: new google.maps.LatLng(points[key].latitude, points[key].longitude),
		       	         	labelContent: showIndex ? index + 1 : '',
		       	         	labelAnchor: new google.maps.Point(25, 45),
		       	         	labelClass: showIndex ? 'marker-label' : '',
		       	         	labelInBackground: false,
		       	            title: points[key].nome,
		       	            icon: points[key].icone,
		       	            map: mapDia,
		       	        });
		       	        markers.push(marker);
		       	        <%-- Extendendo a area do mapa que ira aparecer na tela --%>
		       	        latlngbounds.extend(marker.position);
		       	        if (showIndex) {
		       	        	index++;
		       	        }
	       			} else {
	       				if (index > 0) {
	       					index--;
	       				}
	       			}
	       	    }
	       	    
	       	    <%-- Agrupando pontos próximos --%>
	       	    //markerCluster = new MarkerClusterer(map, markers);
	
	       		<%-- Ajustando o zoom do mapa para caber todos os pontos na tela --%>
	       		mapDia.fitBounds(latlngbounds);
	       		
	       		return true;
			} else {
				return false;
			}
        }
        
        function salvarAtividade($form, $btn, fecharModal) {
            if ($form.valid()) {
                
                var mesmoLocal = $form.find('input[name="entregaMesmoLocal"]').is(':checked');
                if (mesmoLocal) {
                    $form.find('input[name="nomeLocalTransporteDestino"]').val($('input[name="nomeLocalTransporteOrigem"]').val())
                    $form.find('input[name="localTransporteDestino"]').val($('input[name="localTransporteOrigem"]').val())
                }
                
                var request = $.ajax({
                    type: 'POST',
                    url: $form.attr('action'), 
                    data: $form.serialize() 
                });
                request.done(function(data) {
                    if (fecharModal) {
                        $('#editar-atividade-modal').modal('hide');
                    }
                    var dia = $form.find('select[name="itemDono"]').val();
                    if (dia == null) {
                        dia = $form.find('input[name="itemDono"]').val();
                    }
                    var tipo = $form.find('input[name="tipo"]').val();
                    setTimeout(function() {
                        $.gritter.add({
                            title: data.title,
                            text: data.message,
                            time: 4000,
                            image: '<c:url value="/resources/images/' + data.imageName + '.png" />'
                        });
                        $btn.button('reset');
                        //if ((typeof(recarregarAtividades) != 'undefined')) {
                            //recarregarAtividades();
                       //} else {
                            recarregarResumoAtividades();
                            if (data.params.houveMovimentacao) {
                                var idAtividade = data.params.idAtividade;
                                // remover a atividade do dia em que estava
                                $('div[data-atividade-id="' + idAtividade + '"]').remove();
                                if (data.params.diaFim) {
                                    // e recarregar o dia para qual ela foi
                                    recarregarDia(data.params.diaFim);
                                }
                            }
                            recarregarDia(dia);
                            recarregarPainelAtividades(tipo);
                        //}
                    }, 500);
                    limparForm($form);
                });
                request.fail(function(jqXHR, textStatus) {
                    $btn.button('reset');
                });
            } else {
                $btn.button('reset');
            }
        }

        function limparForm($form) {
            $form.find('input, textarea, select').each(function(index) { 
                if (!$(this).hasClass('permanente')) {
                    $(this).val('');
                }
            });
        }
        
        function redesenharListaAtividades($divPai) {
            var showFirst, showLast, showUp, showDown;
            var $divsAtividades = $divPai.find('div[rel="atividade"]');
            var idDia = $divPai.data('dia');
            var qtd = $divsAtividades.length;
            var $div = null;
            $divsAtividades.each(function(index) {
                $div = $(this);
                
                // Recalculando a posicao
                $div.find('.number-label').text($div.index() + 1);
                
                // Atualizar Mapa
                carregarMapaDia(idDia);
                
                // Reconfigurar Acoes de Mover
                $div.find('.btnMoverItem').hide();
                showFirst = true;
                showLast = true;
                showUp = true;
                showDown = true;
                if ($div.is(':first-child')) {
                    showFirst = false;
                    showUp = false;
                }
                if ($div.is(':last-child')) {
                    showLast = false;
                    showDown = false;
                }
                if (showFirst) {
                    $div.find('a[data-direction="first"]').show();
                }
                if (showLast) {
                    $div.find('a[data-direction="last"]').show();
                }
                if (showUp) {
                    $div.find('a[data-direction="up"]').show();
                }
                if (showDown) {
                    $div.find('a[data-direction="down"]').show();
                }
            });
        }
        </script>
    </tiles:putAttribute>
        
</tiles:insertDefinition>