<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id=fixed-bar" class="well well-sm fixed-top-bar" data-spy="affix" data-offset-top="10" data-offset-bottom="10"
	style="width: 100%; margin-left: 1px; margin-bottom: 0; padding: 0px 6px; right: 0; z-index: 99;">
	<input type="hidden" id="urlCidadeAtual" value="${destino.urlPath}" />
	<div class="row" style="padding-top: 3px; padding-bottom: 2px;">
		<div class="col-xs-3" style="padding-right: 5px; padding-top: 2px;">
			<button id="btn-mapa-dia-anterior" class="btn btn-success btn-sm pull-right" title="Ir para o dia anterior">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			</button>
		</div>
		<div class="col-xs-6" style="padding: 0px;">

			<div class="navbar-dias" style="display: none;">
				<ul class="nav nav-tabs" role="tablist">
					<c:forEach var="dia" items="${viagem.dias}">
						<li class=""><a href="#painel-atividades-${dia.id}" data-dia-id="${dia.id}" tabindex="-1"></a></li>
					</c:forEach>
				</ul>
			</div>

			<select id="selectDiasMapa" class="form-control" style="font-size: 15px;">
				<option value=""> Todos os dias </option>
				<c:forEach var="dia" items="${viagem.dias}">
					<c:set var="nDia" value="${dia.numero}" />
					<c:set var="dDia" value="${dia.dataViagem}" />
					
					<option value="${dia.id}">
						<c:if test="${not empty nDia}">
							            ${nDia}º Dia
							        </c:if>
						<c:if test="${not empty dDia}">
							            - <fmt:formatDate value="${dDia}" pattern="EEE, dd MMM" />
						</c:if>
						<c:if test="${empty nDia}">
							            A organizar
							        </c:if>
					</option>
				</c:forEach>
			</select>
		</div>
		<div class="col-xs-3" style="padding-top: 2px; padding-left: 5px;">
			<button id="btn-mapa-dia-proximo" class="btn btn-success btn-sm" title="Ir para o próximo dia">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			</button>
		</div>
	</div>

</div>

<script>

var map;
var markers = [];
var markersFiltered = [];
var markerCluster;
var latlngbounds
var idInfoBoxAberto;
var infoBoxes = [];

$(document).ready(function () {
	/*$('div[data-spy="affix"]').affix({
	    offset: {
	        top: 150, 
	        bottom: function () {
	            return (this.bottom = $('footer.bs-footer').outerHeight(true))
	        }
	    }
	});*/
	
	$('#btn-mapa-dia-proximo').click(function(e) {
		e.preventDefault();
		if ($('#selectDiasMapa option:selected').next().val()) {
			$('#selectDiasMapa option:selected').next().attr('selected', 'selected');
			$('#btn-mapa-dia-anterior').removeClass('disabled');
		} else {
			$(this).addClass('disabled');
		}
		timeout = setTimeout(function() {
			$('#selectDiasMapa').trigger('change');
    	}, 200);
		
	});

	$('#btn-mapa-dia-anterior').click(function(e) {
		e.preventDefault();
		if ($('#selectDiasMapa option:selected').prev().val()) {
			$('#selectDiasMapa option:selected').prev().attr('selected', 'selected');
			$('#btn-mapa-dia-proximo').removeClass('disabled');
		} else {
			$(this).addClass('disabled');
		}
		timeout = setTimeout(function() {
			$('#selectDiasMapa').trigger('change');
    	}, 200);
	});
	
	$('#selectDiasMapa').change(function() {
		var idDia = $(this).val();
		var markersLength = markers.length;
		markersFiltered = new Array();
		latlngbounds = new google.maps.LatLngBounds();
		for (var i = 0; i < markersLength; i++) {
		    if (idDia == '' || markers[i].labelClass == 'dia-' + idDia) {
		    	markers[i].setVisible(true);
		    	markersFiltered.push(markers[i]);
		        latlngbounds.extend(markers[i].position);
		    } else {
		    	markers[i].setVisible(false);
		    }
		}
		if (markersFiltered.length > 0) {
			markerCluster.clearMarkers();
			markerCluster = new MarkerClusterer(map, markersFiltered);
		    map.fitBounds(latlngbounds);
		}
    });
	
});

function initialize() {
	
	function abrirInfoBox(id, marker) {
	    if (typeof(idInfoBoxAberto) == 'number' && typeof(infoBoxes[idInfoBoxAberto]) == 'object') {
	        infoBoxes[idInfoBoxAberto].close();
	    }
		if (infoBoxes[id] != null) {
			if (infoBoxes[id].open) {
				infoBoxes[id].open(map, marker);
			}
		} else {
		}
	    idInfoBoxAberto = id;
	}
	
    <%-- Montagem dos pontos do mapa --%>
    var points = [
	<%@include file="pontosMapa.jsp" %>
    ];
    
    popularMapa(points);
    
}

function popularMapa(points) {

    latlngbounds = new google.maps.LatLngBounds();
    var infoBoxOptions = {};
    var infoBox;
    var infoWindow;
    
    var mapOptions = {
		center: new google.maps.LatLng(${viagem.primeiroDestino.destino.latitude}, ${viagem.primeiroDestino.destino.longitude}),
	    zoom: 4,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
  	};
    
    if (map == null) {
	  	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    }
    
	for (var key in points) {
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(points[key].latitude, points[key].longitude),
            title: points[key].nome,
            icon: points[key].icone,
            map: map,
            labelClass: 'dia-' + points[key].idDia,
            html: getInfoWindowContent(points[key].id, points[key].urlPath, points[key].nome, points[key].urlFotoSmall, points[key].endereco, points[key].notaTripAdvisor, points[key].quantidadeReviewsTripAdvisor)
        });
        
		/*infoBoxOptions = {
			content: 'Conteúdo do InfoBox',
			pixelOffset: new google.maps.Size(-150, 0)
		};*/
        
        var number = points[key].number;
        //infoBoxes[number] = new google.maps.InfoWindow();
        //infoBoxes[number].setContent(getInfoWindowContent(points[key].id, points[key].urlPath, points[key].nome, points[key].urlFotoSmall, points[key].endereco, points[key].notaTripAdvisor, points[key].quantidadeReviewsTripAdvisor));
        /*infoBoxes[number] = new InfoBox(infoBoxOptions);
        infoBoxes[number].marker = marker;
        google.maps.event.addListener(marker, 'click', function (e) {
            abrirInfoBox(number, this);
        });*/
        
        infoWindow = new google.maps.InfoWindow();
        /*infoBoxes[number].marker = marker;
        infoBoxes[number].listener =*/ 
       	google.maps.event.addListener(marker, 'click', function () {
       		infoWindow.setContent(this.html);
       		infoWindow.open(map, this);
            //abrirInfoBox(number, this);
        });
        
        markers.push(marker);
        <%-- Extendendo a area do mapa que ira aparecer na tela --%>
        latlngbounds.extend(marker.position);
    }
    
    <%-- Agrupando pontos próximos --%>
    markerCluster = new MarkerClusterer(map, markers);

	<%-- Ajustando o zoom do mapa para caber todos os pontos na tela --%>
    map.fitBounds(latlngbounds);
}

function getInfoWindowContent(idLocal, urlPathLocal, nomeLocal, urlFotoLocal, endereco, notaTripAdivisor, qtdAvaliacoesTripAdvisor) {
	return '<div class="media" style="" data-local-id="' + idLocal + '" data-local-url="' + urlPathLocal + '">' + 
				'<a class="media-left" href="#"> ' + 
					'<div> ' + 
						'<img src="' + urlFotoLocal + '" style="width: 64px; height: 64px;"> ' + 
					'</div>  ' +
				'</a>  ' +
				'<div class="media-body"> ' +
					'<h4 class="media-heading"> ' + 
						'<a href="#" class="card-title" style="font-size: .8em;"> ' + nomeLocal + ' </a> ' + 
					'</h4>  ' +
					'<div> ' + 
						'<div style="padding-left: 0px; padding-top: 0;"> ' + 
							'<small> ' + endereco + '</small>  ' +
							'<div> ' +
								'<span class="tripAdvisorRating rate-' + notaTripAdivisor + '" title="Nota dos viajantes site TripAdvisor.com - ' + qtdAvaliacoesTripAdvisor  + ' avaliações"></span> ' + 
							'</div>  ' +
						'</div> ' +
					'</div> ' +
				'</div> ' +
			'</div> ';
}

function clearMarkers() {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
		markers = [];
	}
}

function recarregarPontosMapa() {
	var url = '<c:url value="/viagem/${viagem.id}/pontosMapa"/>';
	var request = $.ajax({
        url: url, 
        type: 'GET' 
    });
    request.done(function(result) {
    	clearMarkers();
    	var points = eval('[' + result + ']');
    	$('#selectDiasMapa').val('')
    	popularMapa(points);
    });
    request.fail(function(jqXHR, textStatus) {
    });
}

</script>

