<div id="div-lista">

	<div class="panel-group" role="tablist">
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="collapseListGroupHeading1">
				<h4 class="panel-title">
					<a class="" data-toggle="collapse" href="#listarViajantes" aria-expanded="true" aria-controls="listarViajantes">
						Pessoas que viajar�o com voc� <span class="glyphicon glyphicon-chevron-down btn-xs"></span>
					</a>
					<button id="btn-add-viajante" class="btn btn-xs btn-success pull-right">
						<span class="glyphicon glyphicon-plus"></span> Adicionar viajante
					</button>
				</h4>
			</div>
			<div id="listarViajantes" class="panel-collapse in" role="tabpanel"
				aria-labelledby="collapseListGroupHeading1" aria-expanded="true">
				<c:import url="/viagem/${viagem.id}/listarViajantes" />
			</div>
		</div>
		<div class="panel panel-default">
			<div class="panel-heading" role="tab" id="collapseListGroupHeading2">
				<h4 class="panel-title">
					<a class="" data-toggle="collapse" href="#listarCompartilhamentos" aria-expanded="true"
						aria-controls="listarCompartilhamentos"> Compartilhar com <span
						class="glyphicon glyphicon-chevron-down btn-xs"></span>
					</a>
					<button id="btn-compartilhar" class="btn btn-xs btn-primary pull-right">
						<span class="glyphicon glyphicon-plus"></span> Adcionar pessoa
					</button>
				</h4>
			</div>
			<div id="listarCompartilhamentos" class="panel-collapse in" role="tabpanel"
				aria-labelledby="collapseListGroupHeading2" aria-expanded="true">
				<c:import url="/viagem/${viagem.id}/listarCompartilhamentos" />
			</div>
		</div>
	</div>
</div>

<div id="div-form" style="display: none;">

    <div id="link-retornar" style="padding-bottom: 4px;">
      <div>
    	<a id="link-retornar-viajantes" href="#"><span>&laquo;</span> Ver lista de Viajantes</a>
      </div>
      <div>
    	<a id="link-retornar-compartilhar" href="#"><span>&laquo;</span> Ver compartilhamentos</a>
      </div>
    </div>

	<fieldset>

		<legend>
			<span id="legend-viajante" style="font-size: 18px;"> Incluir viajante </span> <span id="legend-compartilhar"
				style="font-size: 18px;"> Compartilhar com amigos </span>
		</legend>

		<ul id="tab-viajante" class="nav nav-tabs" role="tablist">

			<li class="active"><a href="#" id="adulto-tab" role="tab" data-toggle="tab" aria-controls="home"
				aria-expanded="true">Adulto</a></li>
			<li><a href="#" role="tab" id="crianca-tab" role="tab" data-toggle="tab" aria-controls="home"
				aria-expanded="true">Crian�a</a></li>
		</ul>

		<div style="padding-top: 15px; padding-bottom: 10px; background-color: #fff;">

			<form:form id="frm-add-viajante" method="post" action="/viagem/salvar" class="form-horizontal" role="form"
				modelAttribute="participante">

				<input type="hidden" name="viaja" value="true" />

				<div id="frm-adulto">

					<div id="group-nome" class="form-group">
						<label for="inputViajantes" class="col-sm-3 control-label required"> Nome / E-mail <span
							class="glyphicon glyphicon-question-sign" title="Escolha entre seus amigos ou informe o email"></span>
						</label>
						<div class="col-sm-7">
							<fan:autoComplete url="/amigos/consultarAmigos" name="nome" hiddenName="usuario" id="inputNomeAmigo"
								valueField="idAmigo" responsive="true" onSelect="selecionarAmigo()" onRemove="removerAmigo()"
								blockOnSelect="true" minLength="2" labelField="nomeAmigo"
								placeholder="Escolha entre seus amigos ou informe o email" index="9999" showImage="true"
								imageName="urlFotoSmall" imageStyle="width: 30px; height: 30px;" imageClass="semBorda"
								lineStyle="min-height: 36px;" cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
								showAddress="false" showNotFound="false"
								jsonFields="[{name : 'id'}, {name : 'idAmigo'}, {name : 'nomeAmigo'}, {name : 'amigo.urlFotoSmall'}, {name : 'urlFotoSmall'}, {name : 'amigo.ativo'}, {name : 'amigo.idade'}]"
								notFoundMsg="" />

						</div>
					</div>

					<%-- div id="group-permissao" class="form-group grp-permissao">
						<label for="selectPermissao" class="col-sm-3 control-label"> Permiss�o <span
							class="glyphicon glyphicon-question-sign" title="Selecione o que a pessoa pode fazer em seu roteiro."></span>
						</label>
						<div class="col-sm-7">
							<select id="selectPermissao" name="permissao" class="form-control">
								<option value="1">Pode alterar todo o roteiro</option>
								<option value="2">Pode somente incluir atividades</option>
								<option value="3">Pode apenas ver o roteiro</option>
							</select>
						</div>
					</div--%>
				</div>

				<div id="frm-crianca" style="display: none;">
					<div class="form-group">
						<label for="inputNomeCrianca" class="col-sm-3 control-label required"> Nome da crian�a <span
							class="glyphicon glyphicon-question-sign" title="Informe o nome da crian�a ou beb�"></span>
						</label>
						<div class="col-sm-7">
							<input type="text" name="nome" id="inputNomeCrianca" class="form-control" placeholder="Nome da crian�a ou beb�" />
						</div>
					</div>

					<div class="form-group grp-faixa">
						<label for="selectFaixaEtaria" class="col-sm-3 control-label required"> Faixa et�ria <span
							class="glyphicon glyphicon-question-sign" title="Selecione a faixa et�ria"></span>
						</label>
						<div class="col-sm-7">
							<select id="selectFaixaEtaria" name="faixaEtaria" class="form-control">
								<option value="2">Crian�a (2 a 11 anos)</option>
								<option value="3">Beb� de colo (0 a 23 meses)</option>
							</select>
						</div>
					</div>
				</div>

				<div class="row" style="margin-right: 0px;">
					<button id="btn-add-pessoa" class="btn btn-primary pull-right">
						<span class="glyphicon glyphicon-ok"></span> Incluir
					</button>
					<button id="btn-alterar-pessoa" class="btn btn-primary pull-right" style="display: none;">
						<span class="glyphicon glyphicon-ok"></span> Alterar
					</button>
					<button id="btn-cancelar-add-pessoa" class="btn btn-default pull-right" style="margin-right: 10px;">
						<span class="glyphicon glyphicon-remove"></span> Cancelar
					</button>
					<button id="btn-cancelar-direto" class="btn btn-default pull-right" data-dismiss="modal" style="margin-right: 10px; display: none;">
						<span class="glyphicon glyphicon-remove"></span> Cancelar
					</button>
				</div>

			</form:form>
		</div>
	</fieldset>
</div>

<script>
		
$(document).ready(function () {
	
	$('#btn-add-pessoa').click(function (e) {
		e.preventDefault();
		
		var valido = false;
		
		if ($('#inputNomeAmigo_hidden').val()) {
			valido = true;
		} else {
			valido = $('#frm-add-viajante').valid();
		}
		
		if (valido) {
			jQuery.ajax({
	            type: 'POST',
	            url: '<c:url value="/viagem/${viagem.id}/adicionarParticipante" />',
	            data: $('#frm-add-viajante').serialize(),
	            success: function(data) {
	            	var busca = ''
	            	if ($('input:hidden[name="viaja"]').val() == 'true') {
	            		busca = 'listarViajantes';
	            	} else {
	            		busca = 'listarCompartilhamentos';
	            	}
	            	if (data.success) {
	            		$('#frm-add-viajante').get(0).reset();
	    				$('#inputNomeAmigo_hidden').val('');
	                    $('#' + busca).load('<c:url value="/viagem/${viagem.id}/"/>' + busca, function() {
		    				$('#div-form').hide();
		    				$('#div-lista').show();
		    				if (!$('#' + busca).hasClass('in')) {
		    					$('#' + busca).collapse('show');
		    				}
	                    });
	                }
	            }
	        });
		}
	});
	
	$('#btn-alterar-pessoa').click(function (e) {
		jQuery.ajax({
            type: 'POST',
            url: '<c:url value="/viagem/${viagem.id}/alterarParticipante" />',
            data: {
            	permissao: $('#selectPermissao').val()
            },
            success: function(data) {
           		$('#frm-add-viajante').get(0).reset();
   				$('#inputNomeAmigo_hidden').val('');
                   $('#' + busca).load('<c:url value="/viagem/${viagem.id}/"/>' + busca, function() {
    				$('#div-form').hide();
    				$('#div-lista').show();
                   });
            }
        });
	});
	
    $('#frm-add-viajante').validate({
        onkeyup: false,
        onfocusout: false,
        errorPlacement: function(error, element) {
        	if (element.attr('id') == 'inputNomeAmigo') {
            	error.appendTo( element.parent('.input-group').parent());
        	} else {
        		error.insertAfter(element);
        	}
        }
    });

	$('#btn-add-viajante').click(function (e) {
		e.preventDefault();
		$('#div-lista').hide();
		$('#frm-add-viajante').get(0).reset();
		$('#inputNomeAmigo_hidden').val('');
		$('#div-form').show();
		$('#legend-viajante').text('Incluir Viajante');
		$('#legend-viajante').show();
		$('#legend-compartilhar').hide();
		$('#tab-viajante').show();
		$('#group-permissao').show();
		$('#btn-alterar-pessoa').hide();
		$('#btn-add-pessoa').show();
		$('input:hidden[name="viaja"]').val('true');
		$('#inputNomeAmigo').removeAttr('disabled')
		$('#inputNomeAmigo').focus();
    });

	$(document).on('click', '.btn-editar-viajante', function (e) {
		e.preventDefault();
		$('#div-lista').hide();
		$('#legend-viajante').text('Alterar permiss�o');
		$('#legend-compartilhar').hide();
		$('#tab-viajante').hide();
		$('#group-permissao').show();
		$('input:hidden[name="viaja"]').val('true');
		$('#inputNomeAmigo').val($(this).data('nome-participante'));
		$('#inputNomeAmigo_hidden').val($(this).data('id-participante'));
		$('#inputNomeAmigo').attr('disabled', 'disabled')
		$('#btn-alterar-pessoa').show();
		$('#btn-add-pessoa').hide();
		$('#div-form').show();
	})
	
	$(document).on('click', '.btn-remover-viajante', function (e) {
		e.preventDefault();
		var idParticipante = $(this).data('id-participante');
		jQuery.ajax({
            type: 'POST',
            url: '<c:url value="/viagem/${viagem.id}/removerParticipante/" />' + idParticipante,
            success: function(data) {
            	if (data.success) {
            		$('#li-participante-' + idParticipante).remove();
                }
            }
        });
	})
	
	$('#btn-compartilhar').click(function (e) {
		e.preventDefault();
		$('#div-lista').hide();
		$('#btn-alterar-pessoa').hide();
		$('#frm-add-viajante').get(0).reset();
		$('#inputNomeAmigo_hidden').val('');
		$('#btn-add-pessoa').show();
		$('#div-form').show();
		$('#legend-viajante').hide();
		$('#legend-compartilhar').show();
		$('#tab-viajante').hide();
		$('#group-permissao').hide();
		$('input:hidden[name="viaja"]').val('false');
		$('#adulto-tab').click();
		$('#inputNomeAmigo').focus();
    });
	
	$('#adulto-tab').click(function (e) {
		e.preventDefault();
		$('#frm-crianca').hide();
		$('#frm-adulto').show();
		$('#inputNomeAmigo').removeAttr('disabled');
		$('#selectPermissao').removeAttr('disabled');
		$('#inputNomeAmigo').focus();
		
		$('#inputNomeAmigo').rules('add', {
                required: true, 
             email: true,
		  messages: {
           	required : 'Por favor, escolha um amigo ou informe o email.',
               email : 'Por favor, informe um email v�lido.'
		  }
	    });
		
		$('#inputNomeCrianca').attr('disabled', 'disabled');
		$('#selectFaixaEtaria').attr('disabled', 'disabled');
	})
	
	$('#crianca-tab').click(function (e) {
		e.preventDefault();
		$('#frm-adulto').hide();
		$('#frm-crianca').show();
		$('#inputNomeCrianca').removeAttr('disabled');
		$('#selectFaixaEtaria').removeAttr('disabled');
		$('#inputNomeCrianca').focus();
		
		$('#inputNomeCrianca').rules('add', {
			required: true, 
			email: false,
			messages: {
			required : 'Por favor, informe o nome da crian�a.',
		  }
	    });
		
		$('#inputNomeAmigo').attr('disabled', 'disabled');
		$('#selectPermissao').attr('disabled', 'disabled');
	})
	
	$('#btn-cancelar-add-pessoa').click(function (e) {
		e.preventDefault();
		$('#frm-add-viajante').get(0).reset();
		$('#inputNomeAmigo_hidden').val('')
		$('#div-form').hide();
		$('#div-lista').show();
    });
	
	$('#link-retornar-viajantes').click(function (e) {
		e.preventDefault();
		$('#listarViajantes').collapse('show');
		$('#listarCompartilhamentos').collapse('show');
		$('#btn-cancelar-add-pessoa').click();
	});

	$('#link-retornar-compartilhar').click(function (e) {
		e.preventDefault();
		$('#listarCompartilhamentos').collapse('show');
		$('#listarViajantes').collapse('show');
		$('#btn-cancelar-add-pessoa').click();
	});
	
	$('#adulto-tab').click();
	
});
		
</script>