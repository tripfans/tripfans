<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
    
        <style>
            .page-header {
                padding-bottom: 9px;
                margin: 10px 0 20px;
                border-bottom: 1px solid #eeeeee;
            }
            
            @media (max-width: 767px) {
	            .main-content {
	            	padding: 0px;
	            }
	            
	            #div-bottom-bar {
	            	padding: 1px 4px 1px 4px;
	            }
	        }
	        
	        #divMainContent {
	        	padding-top: 10px;
	        }
	        
	        #divMainContent legend {
	        	text-transform: uppercase;
	        	font-size: 14px;
	        }
            
        </style>
        
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
        <script>

        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        <div class="col-md-2 left-menu-container">
        </div>
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="main-content col-md-8" style="margin-top: 20px;">
            
		    <div id="divMainContent">
				<fieldset>
				
		            <div id="div-alert" class="alert alert-dismissible fade in" role="alert" style="margin-bottom: 0;">
					  <div style="text-align: center; margin-bottom: 8px;">
						  <h3 style="color: #555;">
						  	<strong>Parabéns!</strong>
						  	Seu roteiro foi criado!
						  </h3> 
					  </div>
					  <div style="text-align: center; font-size: 14px;">
						  Agora você poderá, com mais tranquilidade, organizar seu itinerário, 
						  escolher e reservar hotéis, voos, aluguel de veículos e muito mais.
					  </div>
					</div>
				
					<div class="row">
				      <div class="col-sm-4">
				        <a href="#" data-toggle="modal" data-target="#modal-incluir-participantes">
					        <div class="media" style="background: #fff; border-bottom: solid 1px #c7c9c8; padding: 10px; min-height: 90px;">
					          <div class="row">
		                        <div class="media-img pull-left col-sm-5  col-md-4" style="margin-top: 10px;">
					              <span class="glyphicon glyphicon-user" style="color: #a7c746; font-size: 46px;"></span>
					            </div>
					            <div class="media-body col-sm-7 col-md-8" style="padding-left: 0px;">
					              <h4 class="media-heading" style="text-transform: uppercase; color: #337ab7;">
						              Incluir Viajantes
					              </h4>
					              <p class="hidden-sm" style="color: #4D4D4D;">Adicione as pessoas que irão com você nesta Viagem.</p>
					            </div>
		                      </div>
					        </div>
				        </a>
				      </div>
				      
				      <div class="col-sm-4">
				        <a href="<c:url value="/viagem/${viagem.id}"/>/hoteis">
					        <div class="media" style="background: #fff; border-bottom: solid 1px #c7c9c8; padding: 10px; min-height: 90px;">
					          <div class="row">
		                        <div class="media-img pull-left col-sm-5  col-md-4" style="margin-top: 10px;">
					              <span class="glyphicon glyphicon-home" style="color: #01b8cc; font-size: 46px;"></span>
					            </div>
					            <div class="media-body col-sm-7 col-md-8" style="padding-left: 0px;">
					              <h4 class="media-heading" style="text-transform: uppercase; color: #337ab7;">
						              Encontrar Hotéis
					              </h4>
					              <p class="hidden-sm" style="color: #4D4D4D;">Encontre as melhores opções de hotéis para você.</p>
					            </div>
		                      </div>
					        </div>
				        </a>
				      </div>

				      <div class="col-sm-4">
				        <a href="<c:url value="/viagem/${viagem.id}"/>">
					        <div class="media" style="background: #fff; border-bottom: solid 1px #c7c9c8; padding: 10px; min-height: 90px;">
					          <div class="row">
		                        <div class="media-img pull-left col-sm-5  col-md-4" style="margin-top: 10px;">
					              <span class="glyphicon glyphicon-calendar" style="color: #fbb423; font-size: 46px;"></span>
					            </div>
					            <div class="media-body col-sm-7 col-md-8" style="padding-left: 0px;">
					              <h4 class="media-heading" style="text-transform: uppercase; color: #337ab7;">
						              Ver Roteiro
					              </h4>
					              <p class="hidden-sm" style="color: #4D4D4D;">Ver e organizar o dia-a-dia do seu Roteiro.</p>
					            </div>
		                      </div>
					        </div>
				        </a>
				      </div>
				    </div>				
				</fieldset>
		    </div>
		    
		    <div>
	            <div id="div-bottom-bar" class="well well-sm affix pull-right" style="bottom: 0px; width: 100%; z-index: 99; left: 0; margin-bottom: 0;">
	                <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
	                	<div class="col-xs-3">
	                	</div>
	                	<div class="col-xs-6" style="text-align: center;">
	                		<ul class="list-inline" style="color: #ccc; padding-top: 6px;">
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                			<li style="color: #0089FF;">&#9679</li>
	                		</ul>
	                	</div>
	                </div>
	            </div>
            </div>
		</div>
		
		<div id="modal-incluir-participantes" class="modal" data-backdrop="static" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Adicionar viajantes / Compartilhar</h4>
                  </div>
                  <div class="modal-body">
                  	<%@include file="adicionarParticipantes.jsp" %>
                  </div>
                  <div class="modal-footer">
                    <button id="" class="btn btn-success" data-dismiss="modal" aria-hidden="true">
                        <span class="glyphicon glyphicon-ok"></span>
                        Concluído
                    </button>
                  </div>
                </div>
            </div>
        </div>
		
    </tiles:putAttribute>
        
</tiles:insertDefinition>