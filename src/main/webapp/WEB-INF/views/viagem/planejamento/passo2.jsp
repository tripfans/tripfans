<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
    
        <style>
            .page-header {
                padding-bottom: 9px;
                margin: 10px 0 20px;
                border-bottom: 1px solid #eeeeee;
            }
            
            @media (max-width: 767px) {
	            .main-content {
	            	padding: 0px;
	            }
	            
	            #div-bottom-bar {
	            	padding: 1px 4px 1px 4px;
	            }
	        }
            
        </style>
        
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
        <script>

        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        <div class="col-md-2 left-menu-container">
        </div>
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="main-content col-md-8">
            <div class="bs-docs-section">
                <div class="page-header" style="margin: 10px 0 0px;">
                  <h3 style="display: inline;">Lugares para visitar</h3>
                  <button class="btn btn-sm btn-primary pull-right btn-proximo" style="margin-top: -5px;">
               		Próximo
               		<span class="glyphicon glyphicon-chevron-right"></span>
           		  </button>
                </div>
            </div>
            
            <div id="div-alert" class="alert alert-info alert-dismissible fade in" role="alert" style="margin: 10 0 5; font-size: 17px;">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <c:choose>
			    <c:when test="${isMobile}">
			      Toque  
			    </c:when>
			    <c:otherwise>
			      Clique
			    </c:otherwise>
			  </c:choose>
			    nos locais que você deseja vistar para adicioná-los ao seu plano.
			    Se preferir, você pode pular este passo e escolher seus locais em outro momento.
			</div>
			
		    <div>
		
		        <!-- Nav tabs -->
		        <ul class="nav nav-tabs" role="tablist" style="display: none;">
		            <li><a id="btn-tab-lista" href="#tab-lista" role="tab" data-toggle="tab" style="font-size: 15px;">Lista</a></li>
		            <li><a id="btn-tab-mapa" href="#tab-map" role="tab" data-toggle="tab" style="font-size: 15px;">Mapa</a></li>
		        </ul>
		
		        <div id="div-main-body">
		            <!-- Tab panes -->
		            <div class="tab-content">
		                <div class="tab-pane" id="tab-lista">
		                  <div id="tab-lista-body">
		                  	<div style="margin: 30px; text-align: center;">
		                  		<img src="<c:url value="/resources/images/loading.gif"/>" alt="Carregando" /> Carregando...
		                  	</div>
		                  </div>
		                </div>
		                <div class="tab-pane" id="tab-mapa"></div>
		            </div>        
		        </div>
		
		    </div>
		    
		    <div>
	            <div id="div-bottom-bar" class="well well-sm affix pull-right" style="bottom: 0px; width: 100%; z-index: 99; left: 0; margin-bottom: 0;">
	                <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
	                	<div class="col-xs-3">
	                	</div>
	                	<div class="col-xs-6" style="text-align: center;">
	                		<ul class="list-inline" style="color: #ccc; padding-top: 6px;">
	                			<li>&#9679</li>
	                			<li style="color: #0089FF;">&#9679</li>
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                		</ul>
	                	</div>
	                	<div class="col-xs-3">
			           		<button class="btn btn-primary pull-right btn-proximo">
			               		Próximo
			               		<span class="glyphicon glyphicon-chevron-right"></span>
			           		</button>
	                	</div>
	                </div>
	            </div>
            </div>
		    
		</div>
		
		<script>
		$(document).ready(function () {
		
		    $('#btn-tab-lista').click(function (e) {
		    	
	            var height = $(window).height()// $('#tab-lista-body').css('height');
	            
		    	$('#tab-lista-body').load('<c:url value="/viagem/${viagem.id}/${urlPathCidadeSelecionada}/sugestoesLugares?tipoLocal=${tipoLocal}&diaInicio=${diaInicio}&wizard=true"/>', function(response) {
		            $('#scroll-div').css('height', $(window).height() - 200);
		        });
	            
		    });
		    
		
		    $('.btn-proximo').click(function (e) {
		    	window.location.href = '<c:url value="/viagem/criar/"/>${viagem.id}/3';
		    });
		    
		    var acao = '${acao}';
		    if (acao !== '') {
		        var $tabAtual = $('#btn-tab-' + acao);
		        if ($tabAtual.attr('id') != null) {
		            $tabAtual.click();
		        } else {
		            $('#btn-tab-lista').click();
		        }
		        
		    } else {
		        $('#btn-tab-lista').click();
		    }
		    
		    $('#div-bottom-bar').affix({
	    	  offset: {
	    	    top: 10,
	    	    bottom: function () {
	    	      return (this.bottom = $('footer.bs-footer').outerHeight(true))
	    	    }
	    	  }
	    	});
		    
		});
		</script>
    </tiles:putAttribute>
        
</tiles:insertDefinition>