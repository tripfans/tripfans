<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
    
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
	
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.lazyload.js"/>" ></script>
		
		<script>
		$(document).ready(function() {		
			
			$('.btn-proximo').click(function(e) {
				window.location.href = '<c:url value="/viagem/criar/"/>${viagem.id}/4';
			});
			
			$(document).on(
					'click',
					'div.usercard-inner.selectable',
					function(e) {
						$(this).toggleClass('selected');

						// habilitar botao mover
						var show = false;

						$('#div-amigos').find('div.usercard-inner')
								.each(
										function(index) {
											if ($(this).hasClass(
													'selected')) {
												show = true;
											}
										});
						if (show) {
							$('.btn-proximo span.txt')
									.text('Pular');
							$('.btn-enviar-solicitacoes').css(
									'display', '');
						} else {
							$('.btn-proximo span.txt').text(
									'Próximo');
							$('.btn-enviar-solicitacoes').css(
									'display', 'none');
						}
					});
			
			$('div[data-spy="affix"]').affix(
					{
						offset : {
							top : 100,
							bottom : function() {
								return (this.bottom = $(
										'footer.bs-footer')
										.outerHeight(true))
							}
						}
					});

			$('#div-bottom-bar').affix(
					{
						offset : {
							top : 10,
							bottom : function() {
								return (this.bottom = $(
										'footer.bs-footer')
										.outerHeight(true))
							}
						}
					});
		});

		</script>
	
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        <div class="col-md-2 left-menu-container">
        </div>
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="main-content col-md-8">
            <div class="bs-docs-section">
                <div class="page-header" style="margin: 10px 0 0px;">
                  <h3 style="display: inline;">Peça ajuda de seus amigos</h3>
                  <button class="btn btn-sm btn-primary pull-right btn-proximo" style="margin-top: -5px;">
               		<span class="txt">Próximo</span>
               		<span class="glyphicon glyphicon-chevron-right"></span>
           		  </button>
                  <button class="btn btn-sm btn-success pull-right btn-enviar-solicitacoes" style="margin-top: -5px; margin-right: 5px; display: none;">
               		<span class="txt">Enviar</span>
               		<span class="glyphicon glyphicon-send"></span>
           		  </button>
                </div>
            </div>
            
            <div id="div-alert" class="alert alert-info alert-dismissible fade in" role="alert" style="margin: 10 0 5; font-size: 17px;">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			  <c:if test="${possuiAmigos}">
			    Selecione dentre seus amigos que já estiveram na(s) cidade(s) do seu roteiro 
			    <br/>ou<br/>
			  </c:if>
			  Informe os emails ${possuiAmigos ? 'deles' : 'de seus amigos'}  para te ajudarem com dicas e sugestões de lugares.
			</div>
			
		    <div id="divMainContent">
				
				<%@include file="pedirAjudaAmigos.jsp" %>

		    </div>
		    
		    <div>
	            <div id="div-bottom-bar" class="well well-sm affix pull-right" style="bottom: 0px; width: 100%; z-index: 99; left: 0; margin-bottom: 0;">
	                <div class="row" style="padding-top: 3px; padding-bottom: 2px;">
	                	<div class="col-xs-3">
	                	</div>
	                	<div class="col-xs-6" style="text-align: center;">
	                		<ul class="list-inline" style="color: #ccc; padding-top: 6px;">
	                			<li>&#9679</li>
	                			<li>&#9679</li>
	                			<li style="color: #0089FF;">&#9679</li>
	                			<li>&#9679</li>
	                		</ul>
	                	</div>
	                	<div class="col-xs-3">
			           		<button class="btn btn-primary pull-right btn-proximo">
			               		<span class="txt">Próximo</span>
			               		<span class="glyphicon glyphicon-chevron-right"></span>
			           		</button>
			                <button class="btn btn-success pull-right btn-enviar-solicitacoes" style="margin-right: 5px;; display: none;">
			               		<span class="txt">Enviar</span>
			               		<span class="glyphicon glyphicon-send"></span>
		           		    </button>
	                	</div>
	                </div>
	            </div>
            </div>
		    
		</div>
		
    </tiles:putAttribute>
        
</tiles:insertDefinition>