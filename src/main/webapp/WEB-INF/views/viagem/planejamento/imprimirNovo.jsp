<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% pageContext.setAttribute("newLineChar", "\n"); %>

<c:choose>
    <c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
        <c:set var="reqScheme" value="https" />
    </c:when>
    <c:otherwise>
        <c:set var="reqScheme" value="http" />
    </c:otherwise>
</c:choose>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
	
		<script type="text/javascript" src="${reqScheme}://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<%-- script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script--%>
		<script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/src/markerclusterer.js"></script>
		<script type="text/javascript" src="${reqScheme}://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel.js"></script>
	
        <style>
        
            div.page-header {
                margin: 0px;
                padding-bottom: 0px;
            }
        
            label {
                font-weight: normal;
            }
            
            hr {
                margin-top: 10px;
                margin-bottom: 10px;
            }
            
            .label {
                font-weight: normal;
            }
            
            .label-horario-atividade {
                margin-left: -81px; margin-right: -20px;
            }
            
            .icone-atividade {
                position: absolute; 
                z-index: 1;
                top: 5px; 
                left: 5px;
            }

            .body-impressao {
            }
            
            .box-detalhe-atividade {
                border-left: 1px solid #EEE;
                
            }

            a[href]:after { display:none; } 
            
            span.label-time {
                color: black;
                padding: 0;
            }

            span.label-time.start {
                color: #5cb85c;
            }

            span.label-time.end {
                color: #f0ad4e;
            }
            
            img.img-local-atividade {
                max-width: 50px; 
                max-height: 50px; 
            }

            h4 {
                margin: 0px;
            }
            
            @media print {
                div {
                    font-family: 'PT Sans Narrow', "Helvetica Neue", Helvetica, Arial, sans-serif;
                }
                div.div-atividade, div.media.card-atividade {
                    page-break-inside: avoid;
                }
                
                div.page-header {
                    margin: 0px;
                    padding-bottom: 0px;
                }
            
                span.label.label-time {
                    border: 0px;
                }
                
                a.media-left {
                    display: table-cell;
                }
                
                a[href].media-left :after  {
                    display: table-cell;
                }
                
                div.page-container {
                    border: 0px;
                }
                
                h1, h3, h4 {
                	margin: 0px;
                }
                
                .label {
				    
				}
                
            }
            
            .number-label {
				font-family: "Arial", sans-serif;
            	font-size: 10px; 
            	font-weight: bold!important; 
            	line-height: 2;
            	margin-left: 5px;
            	padding: .3em .6em .2em;
			}
			
			/*
			.label-restaurante {
			   background-color: #e74f4e; 
			}

			.label-hotel {
				background-color: #01b8cc; 
			}

			.label-personalizado {
				background-color: #5cb85c; 
			}

			.label-atracao {
				background-color: #fbb423; 
			}
			*/
			.checkin-label {
				margin-top: 5px;
            	margin-left: -10px;
			}
			
        </style>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
        
        <script>
            jQuery(document).ready(function() {
                $('#btn-imprimir').click(function (e) {
                    e.preventDefault();
                    window.print();
                });
                
                $('#btn-enviar-email').click(function (e) {
                    e.preventDefault();
                    var btn = $(this);
                    btn.button('loading');
                    $.ajax({
    					type: 'POST',
    					url: '<c:url value="/viagem/${viagem.id}/planejamento/enviarRoteiroPorEmail" />',
    					success: function(data, status, xhr) {
    						if (data.success) {
    							$.gritter.add({
	                                title: 'Informação',
	                                text: data.message,
	                                time: 4000,
	                                image: '<c:url value="/resources/images/info.png" />'
	                            });
    						} else {
    							$.gritter.add({
	                                title: 'Erro',
	                                text: data.message,
	                                time: 4000,
	                                image: '<c:url value="/resources/images/error.png" />'
	                            });
    						}
    						btn.button('reset');
    					}
    				});
                });
                
                $('#imprimir-fotos').change(function (e) {
                    e.preventDefault();
                    exibirElementos($(this), 'img', 'img-local-atividade');
                });
                
                $('#imprimir-mapas').change(function (e) {
                	e.preventDefault();
                	exibirElementos($(this), 'img', 'map-canvas-dia');
                });
                
                $('#imprimir-icones').change(function (e) {
                	e.preventDefault();
                	exibirElementos($(this), 'div', 'div-icone-atividade');
                });
                
                $('#imprimir-enderecos').change(function (e) {
                	e.preventDefault();
                	exibirElementos($(this), 'div', 'div-mais-detalhes');
                });
                
                $('img.map-canvas-dia').each(function(index) {
                	var urlMarkers = getUrlMarkers($(this).data('dia'));
                	if (urlMarkers != '') {
                    	var src = '${reqScheme}://maps.googleapis.com/maps/api/staticmap?&size=1024x200&maptype=roadmap' +
                				  urlMarkers +
                				  '&sensor=false';
	                    $(this).attr('src', src);
                	}
                });

            });
            
            function exibirElementos($check, element, className) {
            	if ($check.is(':checked')) {
                	$(element + '.' + className).each(function(index) {
                		$(this).removeClass('hidden-print');
                		$(this).show();
                	});
                } else {
                	$(element + '.' + className).each(function(index) {
                		$(this).addClass('hidden-print');
                		$(this).hide();
                	});
                }
            }
            
            function getUrlMarkers(idDia) {
            	var $painelAtividades = $('#panel-' + idDia);
            	var url = '';
            	
            	var indice = 0;
            	
            	$painelAtividades.find('div.div-atividade').each(function(index) {
    	   			$painelAtividade = $(this);
    	   			var tipo, color, label, latitude, longitude;
    	   			
    				if ($painelAtividade.data('latitude') && $painelAtividade.data('tipo-atividade') != 'HOSPEDAGEM') {
    					indice++;
    					if (!$painelAtividade.data('fim-atividade')) {
	    					tipo = $painelAtividade.data('tipo-atividade');
	    					color = '0xa7c746';
			       	        latitude = $painelAtividade.data('latitude');
			       	        longitude = $painelAtividade.data('longitude');
	    					label = indice;
	    					if (tipo == 'ALIMENTACAO') {
	    						color = '0xe74f4e';
	    					} else if (tipo == 'HOSPEDAGEM') {
	    						color = '0x01b8cc';
	    					} else if (tipo == 'TRANSPORTE') {
	    						color = '0x444';
	    					} else if (tipo == 'VISITA_ATRACAO') {
	    						color = '0xfbb423';
	    					}
	    					url += '&markers=color:' + color + '%7Clabel:' + label +'%7C' + latitude + ',' + longitude;
    					} else {
        					indice--;
        				}
    				} 
    	        });
            	return url;
            }
            
            function carregarMapaDia(idDia) {
            	
           	    var latlngbounds = new google.maps.LatLngBounds();
           	    var infoWindow;
           	 	var markers = [];
           	    
           	    var mapOptions = {
           	 		center: new google.maps.LatLng(${viagem.primeiroDestino.destino.latitude}, ${viagem.primeiroDestino.destino.longitude}),
    	       		zoom: 4,
    	       		maxZoom: 15,
    	     	    mapTypeId: google.maps.MapTypeId.ROADMAP,
           	 		scrollwheel: false,
           	  	};
            	
        		var mapDia = new google.maps.Map(document.getElementById('map-canvas-dia-' + idDia), mapOptions);
    			var points = [];
    			var $painelAtividades = $('#panel-' + idDia);
    			
    			$painelAtividades.find('div.div-atividade').each(function(index) {
    	   			$painelAtividade = $(this);
    				if ($painelAtividade.data('latitude')) {
    					var index = $painelAtividade.data('tipo-atividade') != 'HOSPEDAGEM' ? $painelAtividade.index() : null;
    		   			points.push({
    		   				'index': $painelAtividade.index(),
    		       	        'id': $painelAtividade.data('atividade-id'),
    		       	        'nome': $painelAtividade.data('nome-local'),
    		       	        'urlPath': $painelAtividade.data('urlpath-local'),
    		       	        'latitude': $painelAtividade.data('latitude'),
    		       	        'longitude': $painelAtividade.data('longitude'),
    		       	        'tipo': $painelAtividade.data('tipo-atividade'), 
    		       	        'icone': '<c:url value="/resources/images/markers"/>/' + $painelAtividade.data('icone-atividade') + '-medium-pin.png',
    		       	        'fimAtividade' : $painelAtividade.data('fim-atividade')
    		   			});
    		   			
    				}
    	        });
    			if (points.length > 0) {
    				var index = 0;
    	       		for (var key in points) {
    	       			var showIndex = points[key].index != null ? true : false;
    	       			if (!points[key].fimAtividade) {
	    	       	        var marker = new MarkerWithLabel({
	    	       	            position: new google.maps.LatLng(points[key].latitude, points[key].longitude),
	    	       	         	labelContent: points[key].index + 1,
	    	       	         	labelAnchor: new google.maps.Point(25, 45),
	    	       	         	labelClass: 'marker-label',
	    	       	         	labelInBackground: false,
	    	       	            title: points[key].nome,
	    	       	            icon: points[key].icone,
	    	       	            map: mapDia,
	    	       	        });
	    	       	        markers.push(marker);
	    	       	        <%-- Extendendo a area do mapa que ira aparecer na tela --%>
	    	       	        latlngbounds.extend(marker.position);
	    	       	        if (showIndex) {
			       	        	index++;
			       	        }
    	       			} else {
    	       				if (index > 0) {
    	       					index--;
    	       				}
    	       			}
    	       	    }
    	       	    
    	       	    <%-- Agrupando pontos próximos --%>
    	       	    //markerCluster = new MarkerClusterer(map, markers);
    	
    	       		<%-- Ajustando o zoom do mapa para caber todos os pontos na tela --%>
    	       		mapDia.fitBounds(latlngbounds);
    	       		return true;
    			} else {
    				return false;
    			}
            }
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
        <div class="col-md-2">
        </div>

        <div class="col-md-8">
            <div class="bs-docs-section">
                <div class="page-header">
                  <h1>
                    <img class="img" src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" height="50" />
                    <small style="float: right; margin-top: 5px;">
                        Plano de Viagem
                        <br/>
                        <span style="font-size: 45%; margin-top: 10px; float: right;">por ${viagem.usuarioCriador.displayName}</span>
                    </small>
                  </h1>
                </div>
                <h3 style="margin-top: 10px;">
                    <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
                    <span style="font-weight: normal;">
                        ${viagem.titulo}
                    </span>
                </h3>
                <span style="font-size: 24px; color: #707070; margin: 0; padding-top: 4px; text-rendering: optimizelegibility;  display: block;">
                    <small style="font-size: 65%;">
                        ${periodoFormatado}
                        <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                          - 
                        </c:if>
                        <c:if test="${not empty viagem.quantidadeDias}">
                          ${viagem.quantidadeDias} dias
                        </c:if>
                        <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                          Sem data definida 
                        </c:if>
                    </small>
                </span>
                <span style="font-size: 18px; font-weight: normal; color: #707070; margin-top: 4px; display: block;">
                    <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                      ${not status.first and not status.last ? ', ' : ''}${status.last ? ' e ' : ''}${destinoViagem.destino.nome}
                    </c:forEach>
                </span>                

				<c:if test="${empty impressaoPDF or not impressaoPDF}">
					<div class="hidden-print"> 
		                <hr/>
		                
		                <div style="text-align: center">
		                
			                <label class="hidden-print">
					          <input id="imprimir-icones" type="checkbox" checked="checked" /> Imprimir icones 
					        </label>
			                <label class="hidden-print">
					          <input id="imprimir-fotos" type="checkbox" checked="checked" style="margin-left: 6px;"> Imprimir fotos </input>
					        </label>
			                <label class="hidden-print">
					          <input id="imprimir-mapas" type="checkbox" checked="checked" style="margin-left: 6px;"> Imprimir mapas </input>
					        </label>
			                <label class="hidden-print">
					          <input id="imprimir-enderecos" type="checkbox" checked="checked" style="margin-left: 6px;"> Imprimir endereços </input>
					        </label>
			                
			                <p class="hidden-print">
			                  <div class="row centerAligned" style="  margin-left: 30%; margin-right: 30%; width: 100%;">
			                    <a id="btn-imprimir" href="#" class="btn btn-success col-md-2 hidden-print">
			                        <span class="glyphicon glyphicon-print"></span> Imprimir
			                    </a>
			                    <a id="btn-enviar-email" href="#" class="btn btn-info col-md-2 hidden-print" style="margin-left: 10px;" data-loading-text="Aguarde...">
			                        <span class="glyphicon glyphicon-send"></span> Enviar por email
			                    </a>
			                  </div>
			                </p>
			                
		                </div>
		
		            </div>
		        </c:if>
                <hr/>
	                
                <c:forEach var="dia" items="${viagem.dias}" varStatus="statusDia">
                    <c:set var="atividadesOrdenadas" value="${dia.itensOrdenados}" />
                    
                    <h3>
                      <span class="label label-info" style="width: 100%; display: inline-block; text-align: left; ${not statusDia.first ? 'margin-top: 15px;' : ''}">
                        ${dia.numero}º Dia
                        <c:if test="${not empty dia.dataViagem}">
                            - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                        </c:if>
                        <small style="color: #CEFAFA;">
                          <c:forEach var="destino" items="${viagem.getDia(dia.numero).destinos}" varStatus="status">
                            <c:if test="${empty pais}">
                              <c:set var="pais" value="${destino.pais}"/>
                            </c:if>
                            <c:if test="${status.first}"> - </c:if>
                            <c:if test="${not status.first}"> / </c:if>
                            ${destino.nome}
                            <c:if test="${pais.id != destino.pais.id or status.last}">
                              <i class="flag flag-${fn:toLowerCase(destino.pais.siglaIso)}" title="${destino.pais.nome}"></i>
                            </c:if>
                          </c:forEach>
                        </small>
                      </span>
                    </h3>
                    
                    <c:if test="${empty atividadesOrdenadas}">
                        <br/>
                        <div align="center">
                            Nenhuma atividade programada
                        </div>
                    </c:if>
                    
                    <c:if test="${not empty atividadesOrdenadas}">
	                    <%--div id="map-canvas-dia-${dia.id}" class="map-canvas-dia" style="width: 100%; height: 100%; min-height: 200px; max-height: 200px; margin-bottom: 2px;" data-dia="${dia.id}"></div--%>
	                    <img id="map-canvas-dia-${dia.id}" src="" class="map-canvas-dia" 
	                         style="width: 100%; height: 100%; min-height: 200px; max-height: 200px; margin-bottom: 2px; margin-top: 15px;" data-dia="${dia.id}" />
	                    
	                    <div id="panel-${dia.id}">
	                        <c:set var="index" value="0" />
		                    <c:forEach items="${atividadesOrdenadas}" var="item" varStatus="itemStatus">
		                        <c:set var="diaID" value="${dia.id}"/>
		                        
							    <c:if test="${not item.atividadeHospedagem}">
									<c:set var="index" value="${index + 1}" />
								</c:if>
		                        
		                        <%@include file="atividadeNovo.jsp" %>
		                    </c:forEach>
		                </div>
	                </c:if>
                    
                </c:forEach>
                
            </div>
        </div>
    </tiles:putAttribute>
        
</tiles:insertDefinition>