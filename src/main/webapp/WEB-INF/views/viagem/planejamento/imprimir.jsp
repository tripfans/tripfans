<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.responsive.default">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
        <style>
            label {
                font-weight: normal;
            }
            
            .label {
                font-weight: normal;
            }
            
            .label-horario-atividade {
                margin-left: -81px; margin-right: -20px;
            }
            
            .icone-atividade {
                position: absolute; 
                z-index: 1;
                top: 5px; 
                left: 5px;
            }
            
            .box-detalhe-atividade {
                border-left: 1px solid #EEE;
                
            }

            a[href]:after { display:none; } 
            
            @font-face {
                font-family: 'digi';
                src: url('<c:url value="/resources/fonts/SFDigitalReadout-Heavy.ttf" />');
            }

            span.label-time {
                color: black;
                padding: 0;
            }

            span.label-time.start {
                color: #5cb85c;
            }

            span.label-time.end {
                color: #f0ad4e;
            }

            span.digital-clock {
                color: #555;
                font-family: 'digi';
                font-size: 23px;
                font-weight: normal;
                margin-left: 4px;
                margin-top: -1px;
                position: absolute;
            }
            
            h4 {
                margin: 0px;
            }
            
            img.img-local-atividade {
                width: 80px; 
                height: 80px; 
                margin-left: -12px; 
                margin-right: -5px;
            }
            
            @media (max-width: 767px) {
                .img-local-atividade {
                    width: 60px; 
                    height: 60px; 
                }
            }
            
            @media print {
                div {
                    font-family: 'PT Sans Narrow', "Helvetica Neue", Helvetica, Arial, sans-serif;
                }
                div.panel-default {
                    page-break-inside: avoid;
                }
                span.label.label-time {
                    border: 0px;
                }
            }
        </style>
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
        
        <script>
            jQuery(document).ready(function() {
                $('#btn-imprimir').click(function (e) {
                    e.preventDefault();
                    window.print();
                });
            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
        
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
        <div class="col-md-8">
            <div class="bs-docs-section">
                <div class="page-header">
                  <h1>
                    <img class="img" src="<c:url value="/resources/images/logos/tripfanslogo.png"/>" height="50">
                    <small style="float: right; margin-top: 5px;">
                        Plano de Viagem
                        <br/>
                        <span style="font-size: 45%; margin-top: 10px; float: right;">por ${viagem.usuarioCriador.displayName}</span>
                    </small>
                  </h1>
                </div>
                <p class="lead">
                    <h3>
                        <c:set var="periodoFormatado" value="${viagem.periodoFormatado}"/>
                        <span style="font-weight: normal;">
                            ${viagem.titulo}
                            <small>
                                ${periodoFormatado}
                                <c:if test="${not empty periodoFormatado and not empty viagem.quantidadeDias}">
                                  - 
                                </c:if>
                                <c:if test="${not empty viagem.quantidadeDias}">
                                  ${viagem.quantidadeDias} dias
                                </c:if>
                                <c:if test="${empty periodoFormatado and empty viagem.quantidadeDias}">
                                  Sem data definida 
                                </c:if>
                            </small>
                        </span>
                    </h3>
                    <h4 style="font-weight: normal;">
                        <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                          ${destinoViagem.destino.nome}${not status.last ? ',' : ''}
                        </c:forEach>
                    </h4>                
                </p>
                
                <div class="row centerAligned">
                    <a id="btn-imprimir" href="#" class="btn btn-success col-md-2 hidden-print">
                        <span class="glyphicon glyphicon-print"></span> Imprimir
                    </a>
                </div>
                
                <hr/>
                
                <c:forEach var="dia" items="${viagem.dias}">
                    <c:set var="atividadesOrdenadas" value="${dia.itensOrdenados}" />
                    
                    <h3>
                      <span class="label label-info">
                        ${dia.numero}º Dia
                        <c:if test="${not empty dia.dataViagem}">
                            - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                        </c:if>
                      </span>
                    </h3>
                    
                    <br/>
                    
                    <c:if test="${empty atividadesOrdenadas}">
                        <div align="center">
                            Nenhuma atividade programada
                        </div>
                    </c:if>
                    <c:forEach items="${atividadesOrdenadas}" var="item" varStatus="itemStatus">
                        <c:set var="diaID" value="${dia.id}"/>
                        <%@include file="atividade.jsp" %>
                    </c:forEach>
                    
                </c:forEach>
                
                
            </div>
        </div>
    </tiles:putAttribute>
        
</tiles:insertDefinition>