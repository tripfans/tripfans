<%@page import="br.com.fanaticosporviagens.model.entity.TipoTransporte" trimDirectiveWhitespaces="true" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoAtividade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:url var="url" value="/viagem/${viagem.id}/salvarAtividade" />
<c:if test="${not empty atividade.id}">
    <c:set var="formId" value="formAlterarAtividade"/>
</c:if>
<c:if test="${empty atividade.id}">
    <c:set var="formId" value="formIncluirAtividade"/>
</c:if>

<c:if test="${not isMobile}">
    <c:set var="typeTime" value="text" />
</c:if>
<c:if test="${isMobile}">
    <c:set var="typeTime" value="time" />
</c:if>

<c:set var="textoOrigem" value="${atividade.transporte == 'ALUGUEL_VEICULO' ? 'Retirada' : 'Origem'}"/>
<c:set var="textoDiaOrigem" value="${atividade.transporte == 'ALUGUEL_VEICULO' ? 'Dia' : 'Partida'}"/>
<c:set var="textoDestino" value="${atividade.transporte == 'ALUGUEL_VEICULO' ? 'Entrega' : 'Destino'}"/>
<c:set var="textoDiaDestino" value="${atividade.transporte == 'ALUGUEL_VEICULO' ? 'Dia' : 'Chegada'}"/>
<c:if test="${atividade.transporte == 'ALUGUEL_VEICULO'}">
  <c:choose>
    <c:when test="${not empty atividade.transporteLocalOrigem and not empty atividade.transporteLocalDestino}">
        <c:set var="mesmoLocalOrigemDestino" value="${atividade.transporteLocalDestino.id eq atividade.transporteLocalOrigem.id}"/>
    </c:when>
    <c:when test="${not empty atividade.nomeLocalOrigem}">
        <c:set var="mesmoLocalOrigemDestino" value="${atividade.nomeLocalOrigem eq atividade.nomeLocalDestino}"/>
    </c:when>
  </c:choose>
</c:if>

<compress:html enabled="true" compressJavaScript="true" jsCompressor="closure" compressCss="true">
<form:form id="${formId}" action="${url}" modelAttribute="atividade" class="form-horizontal formEditarAtividade" method="post" role="form">

  <c:if test="${not empty viagem.quantidadeDias}">
    <fieldset id="fieldset-basico" style="${tipo.atividadeHospedagem or tipo.atividadeTransporte ? 'display: none' : '' }">
      <legend>Dia</legend>
      <input type="hidden" name="atualizarQuandoFechar" id="inputAtualizarQuandoFechar" value="nao" class="permanente" />
      <input type="hidden" name="idItem" id="inputIdAtividade" value="${atividade.id}" />
      <input type="hidden" name="itemDonoOriginal" id="inputDiaOriginal" value="${(atividade.id != null && atividade.dia != null) ? atividade.dia.id : 0}" />
      <input type="hidden" name="diaFimOriginal" id="inputDiaFimOriginal" value="${(atividade.id != null && atividade.diaFim != null) ? atividade.diaFim.id : 0}" />
      <div class="form-group">
          <label for="inputDia" class="col-xs-2 col-sm-2 control-label">Incluir no dia</label>
          <div class="col-xs-9 col-sm-6">
            <select name="itemDono" id="inputDia" class="form-control permanente">
              <option value="0">Sem dia definido</option>
              <c:forEach var="dia" items="${viagem.dias}">
                <option value="${dia.id}" ${dia.id eq atividade.dia.id ? 'selected' : ''} data-numero-dia="${dia.numero}">
                    ${dia.numero}º dia 
                    <c:if test="${not empty dia.dataViagem}"> 
                      - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                    </c:if>
                </option>
              </c:forEach>
            </select>
          </div>
      </div>
    </fieldset>
  </c:if>
  <c:if test="${empty viagem.quantidadeDias}">
    <input type="hidden" name="itemDono" value="0" />
    <input type="hidden" name="itemDonoOriginal" id="inputDiaOriginal" value="${(atividade.id != null && atividade.dia != null) ? atividade.dia.id : 0}" />
    <input type="hidden" name="idItem" id="inputIdAtividade" value="${atividade.id}" />
  </c:if>
  
  <%
  Map<TipoAtividade, String> descricoesAtividades = new HashMap<TipoAtividade, String>();
  descricoesAtividades.put(TipoAtividade.ALIMENTACAO, "Escolha restaurantes, lanchonentes, bares, etc.");
  descricoesAtividades.put(TipoAtividade.HOSPEDAGEM, "Escolha hotéis, pousadas, resorts, etc.");
  descricoesAtividades.put(TipoAtividade.PERSONALIZADO, "Personalize sua atividade");
  descricoesAtividades.put(TipoAtividade.TRANSPORTE, "Escolha o meio de transporte");
  descricoesAtividades.put(TipoAtividade.VISITA_ATRACAO, "Escolha coisas para fazer, passeios, etc.");
  pageContext.setAttribute("descricoesAtividades", descricoesAtividades);
  %>

  <fieldset id="fieldset-atividade">  
      <legend>
        <c:choose>
            <c:when test="${tipo.atividadeHospedagem}">
                Hospedagem
            </c:when>
            <c:when test="${tipo.atividadeTransporte}">
                Transporte
            </c:when>
            <c:otherwise>
                Atividade
            </c:otherwise>
        </c:choose>
      </legend>  
      <div class="form-group" style="${tipo.atividadeHospedagem or tipo.atividadeTransporte ? 'display: none' : '' }">
          <label for="selectTipoAtividade" class="col-xs-2 col-sm-2 control-label">Tipo</label>
          <div class="col-xs-9 col-sm-6">
            <form:hidden path="tipo" cssClass="permanente" />
            <form:select path="tipo" id="selectTipoAtividade" class="form-control ddslick-atividade permanente">
                <c:choose>
                  <c:when test="${tipo.atividadeHospedagem or tipo.atividadeTransporte}">
                    <c:forEach var="tipoAtividade" items="${tiposAtividade}">
                      <c:url var="imageUrl" value="/resources/images/${tipoAtividade.icone}-medium.png"/>
                      <form:option value="${tipoAtividade}" data-imagesrc="${imageUrl}" 
                                 data-description="${descricoesAtividades[tipoAtividade]}">
                        ${tipoAtividade.descricao}
                      </form:option>
                    </c:forEach>
                  </c:when>
                  <c:otherwise>
                    <c:url var="atracaoRestImageUrl" value="/resources/images/atracao-restaurante-medium.png"/>
                    <form:option value="ATRACAO_ALIMENTACAO" data-imagesrc="${atracaoRestImageUrl}" 
                                 data-description="Escolha coisas para fazer, passeios, restaurantes, lanchonetes">
                        O que fazer / Onde comer
                    </form:option>
                    <c:url var="imageUrl" value="/resources/images/personalizado-medium.png"/>
                    <form:option value="PERSONALIZADO" data-imagesrc="${imageUrl}" 
                                 data-description="Personalize sua atividade">
                        Personalizado
                    </form:option>
                  </c:otherwise>
                </c:choose>
            </form:select>
          </div>
      </div>
      
      <c:set var="tipoAtividadeAtracao" value="<%=TipoAtividade.VISITA_ATRACAO%>"/>
      <c:set var="tipoAtividadeAlimentacao" value="<%=TipoAtividade.ALIMENTACAO%>"/>
      <c:set var="tipoAtividade" value="ATRACAO_ALIMENTACAO"/>
      <div id="fieldset-atracao-alimentacao" rel="fieldset-tipo-atividade" class="fieldset-ATRACAO_ALIMENTACAO" style="${((atividade.tipo eq tipoAtividadeAtracao or atividade.tipo eq tipoAtividadeAlimentacao) or empty atividade.tipo) ? '' : 'display: none;'}">
          <div class="form-group">
            <label for="inputLocal" class="col-xs-2 col-sm-2 control-label required">Local</label>
            <div class="col-xs-9 col-sm-9">
                <fan:autoComplete url="/pesquisaTextual/consultar?ll=${latLongDestinos}&sortByDistance=true&radius=250000&sortByRanking=true&types=7,10,12" name="nomeLocal" hiddenName="local" id="input${tipoAtividade}${atividade.id}" valueField="id"  
                                  value="${atividade.atividadeVisitaAtracao or atividade.atividadeAlimentacao ? atividade.nomeLocal : ''}" hiddenValue="${atividade.atividadeVisitaAtracao or atividade.atividadeAlimentacao ? atividade.local.id : ''}"
                                  blockOnSelect="false" minLength="3" labelField="nomeComSiglaEstadoPais" placeholder="Escreva algo para receber sugestões" rel="local" showImage="true" imageName="urlFotoSmall"
                                  cssClass="form-control focus-${tipoAtividade} consultaLocal" showActions="true" useDocumentReady="false" showAddress="true" showNotFound="true"  
                                  notFoundMsg="Esse local não foi encontrado.<br/> Mesmo assim você pode adicionar esse local ao seu roteiro clicando em Salvar" 
                                  resultListClassWidth="col-xs-9 col-sm-9" resultListStyle="z-index:1000000; overflow-y: auto; overflow-x: hidden;  max-height: 300px;" />
            </div>
          </div>
          <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocal" />
                <c:set var="inputName" value="enderecoLocal" />
                <c:set var="inputValue" value="${atividade.atividadeVisitaAtracao ? atividade.enderecoLocal : ''}" />
                <c:set var="inputDisabled" value="${not empty atividade.local}" />
                <%@include file="endereco.jsp" %>
              </div>
          </div>
      </div>
      
      <c:set var="tipoAtividade" value="<%=TipoAtividade.VISITA_ATRACAO%>"/>
      <div id="fieldset-atracao" rel="fieldset-tipo-atividade" class="fieldset-<%=TipoAtividade.VISITA_ATRACAO%>" style="${(atividade.tipo eq tipoAtividade or empty atividade.tipo) ? '' : 'display: none;'}">
          <div class="form-group">
            <label for="inputLocal" class="col-xs-2 col-sm-2 control-label required">Local</label>
            <div class="col-xs-9 col-sm-9">
                <fan:autoComplete url="/pesquisaTextual/consultar?ll=${latLongDestinos}&sortByDistance=true&radius=250000&types=7,12" name="nomeLocal" hiddenName="local" id="input${tipoAtividade}${atividade.id}" valueField="id"  
                                  value="${atividade.atividadeVisitaAtracao ? atividade.nomeLocal : ''}" hiddenValue="${atividade.atividadeVisitaAtracao ? atividade.local.id : ''}"
                                  blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" showImage="true" imageName="urlFotoSmall"
                                  cssClass="form-control focus-${tipoAtividade} consultaLocal" cssStyle="" showActions="true" useDocumentReady="false" resultListClassWidth="col-xs-9 col-sm-9" 
                                  showAddress="true" showNotFound="true" />
            </div>
          </div>
          <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocal" />
                <c:set var="inputName" value="enderecoLocal" />
                <c:set var="inputValue" value="${atividade.atividadeVisitaAtracao ? atividade.enderecoLocal : ''}" />
                <c:set var="inputDisabled" value="${not empty atividade.local}" />
                <%@include file="endereco.jsp" %>
              </div>
          </div>
      </div>
      
      <c:set var="tipoAtividade" value="<%=TipoAtividade.ALIMENTACAO%>"/>
      <div id="fieldset-restaurante" rel="fieldset-tipo-atividade" class="fieldset-<%=TipoAtividade.ALIMENTACAO%>" style="${(atividade.tipo eq tipoAtividade) ? '' : 'display: none;'}">
          <div class="form-group">
            <label for="inputLocal" class="col-xs-2 col-sm-2 control-label required">Local</label>
            <div class="col-xs-9 col-sm-9">
                <fan:autoComplete url="/pesquisaTextual/consultar?ll=${latLongDestinos}&sortByDistance=true&radius=250000&types=10" name="nomeLocal" hiddenName="local" id="input${tipoAtividade}${atividade.id}" valueField="id"  
                                  value="${atividade.atividadeAlimentacao ? atividade.nomeLocal : ''}" hiddenValue="${atividade.atividadeAlimentacao ? atividade.local.id : ''}"
                                  blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" showImage="true" imageName="urlFotoSmall"
                                  showAddress="true" cssClass="form-control focus-${tipoAtividade} consultaLocal" cssStyle="" showActions="true" useDocumentReady="false" resultListClassWidth="col-xs-9 col-sm-9" />
            </div>
          </div>
          
          <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocal" />
                <c:set var="inputName" value="enderecoLocal" />
                <c:set var="inputValue" value="${atividade.atividadeAlimentacao ? atividade.enderecoLocal : ''}" />
                <c:set var="inputDisabled" value="${not empty atividade.local}" />
                <%@include file="endereco.jsp" %>
              </div>
          </div>
      </div>

      <c:set var="tipoAtividade" value="<%=TipoAtividade.HOSPEDAGEM%>"/>
      <div id="fieldset-hotel" rel="fieldset-tipo-atividade" class="fieldset-<%=TipoAtividade.HOSPEDAGEM%>" style="${(atividade.tipo eq tipoAtividade) ? '' : 'display: none;'}">
          <div class="form-group">
            <label for="inputLocal" class="col-xs-2 col-sm-2 control-label required">Hotel</label>
            <div class="col-xs-10 col-sm-9">
                <fan:autoComplete url="/pesquisaTextual/consultar?ll=${latLongDestinos}&sortByDistance=true&radius=250000&sortByRanking=true&types=8" name="nomeLocal" hiddenName="local" id="input${tipoAtividade}${atividade.id}" valueField="id"  
                                  value="${atividade.atividadeHospedagem ? atividade.nomeLocal : ''}" hiddenValue="${atividade.atividadeHospedagem ? atividade.local.id : ''}"
                                  blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" showImage="true" imageName="urlFotoSmall"
                                  showAddress="true" cssClass="form-control focus-${tipoAtividade} consultaLocal" cssStyle="" showActions="true" useDocumentReady="false" showNotFound="true"
                                  notFoundMsg="Esse local não foi encontrado.<br/> Mesmo assim você pode adicionar esse local ao seu roteiro clicando em Salvar" 
                                  resultListClassWidth="col-xs-9 col-sm-9" resultListStyle="overflow-y: auto; overflow-x: hidden;  max-height: 300px;" />  
            </div>
          </div>
          
          <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocal" />
                <c:set var="inputName" value="enderecoLocal" />
                <c:set var="inputValue" value="${atividade.atividadeHospedagem ? atividade.enderecoLocal : ''}" />
                <c:set var="inputDisabled" value="${not empty atividade.local}" />
                <%@include file="endereco.jsp" %>
              </div>
          </div>
          
          <c:if test="${not empty viagem.quantidadeDias}">
              <div class="form-group">
                <label for="diaInicio" class="col-xs-2 col-sm-2 control-label required">Chegada (Check-in)</label>
                <div class="col-xs-5 col-sm-4">
                    <select name="diaInicio" rel="<%=TipoAtividade.HOSPEDAGEM%>" class="form-control">
                      <option value="0">Sem dia definido</option>
                      <c:forEach var="dia" items="${viagem.dias}">
                        <option value="${dia.id}" ${dia.id eq atividade.dia.id ? 'selected' : ''} data-numero-dia="${dia.numero}">
                            ${dia.numero}º dia 
                            <c:if test="${not empty dia.dataViagem}"> 
                              - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                            </c:if>
                        </option>
                      </c:forEach>
                    </select>
                </div>
                <c:if test="${viagem.controlarTempo}">
                    <label for="inputDataInicio" class="col-xs-1 col-sm-1 control-label dependeDiaInicio">Hora</label>
                    <div class="col-xs-4 col-sm-3">
                        <c:choose>
                          <c:when test="${not empty atividade.dataHoraInicio}">
                            <fmt:formatDate var="horaInicio" value="${atividade.dataHoraInicio}" pattern="HH:mm" />
                          </c:when>
                          <c:otherwise>
                            <c:set var="horaInicio" value="12:00"></c:set>
                          </c:otherwise>
                        </c:choose>
                        <input type="${typeTime}" name="dataInicio" id="inputDataInicio" rel="<%=TipoAtividade.HOSPEDAGEM%>" class="form-control dependeDiaInicio" placeholder="HH:mm" value="${horaInicio}" />
                    </div>
                </c:if>
              </div>
              <%-- div class="form-group">
                <label for="inputLocal" class="col-xs-2 col-sm-2 control-label">Diárias</label>
                <div class="col-xs-4 col-sm-4">
                    <input type="text" name="quantidadeDiarias" id="inputQuantidadeDiarias" class="form-control" placeholder="Número de diárias" />
                </div>
              </div--%>
              <div class="form-group dependeDiaInicio">
                <label for="inputDataFimViagem" class="col-xs-2 col-sm-2 control-label">Partida (Check-out)</label>
                <div class="col-xs-5 col-sm-4">
                    <select name="diaFim" rel="<%=TipoAtividade.HOSPEDAGEM%>" class="form-control">
                      <option value="0">Sem dia definido</option>
                      <c:forEach var="dia" items="${viagem.dias}">
                        <option value="${dia.id}" ${dia.id eq atividade.diaFim.id ? 'selected' : ''} data-numero-dia="${dia.numero}">
                            ${dia.numero}º dia 
                            <c:if test="${not empty dia.dataViagem}"> 
                              - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                            </c:if>
                        </option>
                      </c:forEach>
                    </select>
                </div>
                <c:if test="${viagem.controlarTempo}">
                    <label for="inputDataFim" class="col-xs-1 col-sm-1 control-label">Hora</label>
                    <div class="col-xs-4 col-sm-3">
                         <c:choose>
                          <c:when test="${not empty atividade.dataHoraFim}">
                            <fmt:formatDate var="horaFim" value="${atividade.dataHoraFim}" pattern="HH:mm" />
                          </c:when>
                          <c:otherwise>
                            <c:set var="horaFim" value="12:00"></c:set>
                          </c:otherwise>
                        </c:choose>
                        <input type="${typeTime}" name="dataFim" id="inputDataFim" rel="<%=TipoAtividade.HOSPEDAGEM%>" class="form-control" placeholder="HH:mm" value="${horaFim}" />
                    </div>
                </c:if>
              </div>
              <div class="form-group dependeDiaInicio">
                <label class="col-xs-2 col-sm-2 control-label">Reservado</label>
                <div class="col-xs-4 col-sm-4" style="margin-top: -4px;">
                  <label class="radio-inline">
                    <input type="radio" name="reservada" value="true" ${atividade.reservada ? 'checked="checked"' : ''} style="margin-top: 0px;">
                    Sim
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="reservada" value="false" ${not atividade.reservada ? 'checked="checked"' : ''} style="margin-top: 0px;">
                    Não
                  </label>
                </div>
              </div>
          </c:if>
      </div>

      <c:set var="tipoAtividade" value="<%=TipoAtividade.TRANSPORTE%>"/>
      <div id="fieldset-transporte" rel="fieldset-tipo-atividade" class="fieldset-<%=TipoAtividade.TRANSPORTE%>" style="${(atividade.tipo eq tipoAtividade) ? '' : 'display: none;'}">
        <div class="form-group">
            <label for="selectTransporte" class="col-xs-2 col-sm-2 control-label">Meio</label>
            <div class="col-xs-10 col-sm-6">
                <form:hidden path="transporte" />
                <form:select path="transporte" id="selectTransporte" class="form-control ddslick-transporte focus-${tipoAtividade}">
                  <c:forEach var="transporte" items="${transportes}">
                    <c:url var="imageUrl" value="/resources/images/${transporte.icone}-medium.png"/>
                    <form:option value="${transporte}" data-imagesrc="${imageUrl}" data-description="&nbsp;">
                        ${transporte.descricao}
                    </form:option>
                  </c:forEach>
                </form:select>
            </div>
        </div>
        
        <fieldset style="background-color: white; margin-bottom: 5px;">
            <legend>
                <small id="legend-origem-transporte">${textoOrigem}</small>
            </legend>
            <div class="form-group">
                <label id="label-dia-inicio-tranporte" for="diaInicio" class="col-xs-2 col-sm-2 control-label">${textoDiaOrigem}</label>
                <div class="col-xs-4 col-sm-4">
                    <select name="diaInicio" rel="<%=TipoAtividade.TRANSPORTE%>" class="form-control">
                      <option value="0">Sem dia definido</option>
                      <c:forEach var="dia" items="${viagem.dias}">
                        <option value="${dia.id}" ${dia.id eq atividade.dia.id ? 'selected' : ''} data-numero-dia="${dia.numero}">
                            ${dia.numero}º dia 
                            <c:if test="${not empty dia.dataViagem}"> 
                              - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                            </c:if>
                        </option>
                      </c:forEach>
                    </select>
                </div>
                <c:if test="${viagem.controlarTempo}">
                    <label for="inputDataInicio" class="col-xs-1 col-sm-1 control-label dependeDiaInicio">Hora</label>
                    <div class="col-xs-4 col-sm-3">
                        <input type="${typeTime}" name="dataInicio" id="inputDataInicio" rel="<%=TipoAtividade.TRANSPORTE%>" class="form-control dependeDiaInicio" placeholder="HH:mm" value="<fmt:formatDate value="${atividade.dataHoraInicio}" pattern="HH:mm" />" />
                    </div>
                </c:if>
            </div>
            <c:set var="isAviao" value="${atividade.transporte eq 'AVIAO'}"/>
            <c:set var="isAluguel" value="${atividade.transporte eq 'ALUGUEL_VEICULO'}"/>
            <c:set var="isOutros" value="${not isAviao and not isAluguel}"/>
            <div class="form-group" rel="OUTROS" style="${isOutros ? '' : 'display: none;'}">
                <label id="label-origem-transporte" for="inputOrigemTransporte" class="col-xs-2 col-sm-2 control-label required">Local</label>
                <div class="col-xs-7 col-sm-7">
                    <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocalTransporteOrigem" hiddenName="localTransporteOrigem" id="inputOrigemTransporte${atividade.id}" valueField="id"  
                                      value="${atividade.nomeLocalOrigem}" hiddenValue="${atividade.transporteLocalOrigem.id}"
                                      blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                      cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
                                      disabled="${not isOutros}" />
                </div>
            </div>
            <div class="form-group" rel="AVIAO" style="${isAviao ? 'display: none;' : ''}">
                <label id="label-origem-transporte" for="inputOrigemTransporte" class="col-xs-2 col-sm-2 control-label required">Local / Aeroporto</label>
                <div class="col-xs-7 col-sm-7">
                    <fan:autoComplete url="/pesquisaTextual/consultarCidadesEAeroportos" name="nomeLocalTransporteOrigem" hiddenName="localTransporteOrigem" id="inputOrigemVoo${atividade.id}" valueField="id"  
                                      value="${atividade.nomeLocalOrigem}" hiddenValue="${atividade.transporteLocalOrigem.id}"
                                      blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                      cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
                                      disabled="${not isAviao}" />
                </div>
            </div>
            <div class="form-group" rel="ALUGUEL_VEICULO" style="${isAluguel ? 'display: none;' : ''}">
                <label id="label-origem-transporte" for="inputOrigemTransporte" class="col-xs-2 col-sm-2 control-label required">Local / Locadora</label>
                <div class="col-xs-7 col-sm-7">
                    <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocalTransporteOrigem" hiddenName="localTransporteOrigem" id="inputOrigemLocadora${atividade.id}" valueField="id"  
                                      value="${atividade.nomeLocalOrigem}" hiddenValue="${atividade.transporteLocalOrigem.id}"
                                      blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                      cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
                                      disabled="${not isAluguel}" />
                </div>
            </div>
            
            <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocalTransporteOrigem" />
                <c:set var="inputName" value="enderecoLocalTransporteOrigem" />
                <c:set var="inputValue" value="${atividade.enderecoLocalOrigem}" />
                <c:set var="inputDisabled" value="${not empty atividade.transporteLocalOrigem}" />
                <%@include file="endereco.jsp" %>
              </div>
            </div>
            
        </fieldset>
        
        <fieldset style="background-color: white; margin-bottom: 5px;">
            <legend>
                <small id="legend-destino-transporte">${textoDestino}</small>
            </legend>

            <div class="form-group dependeDiaInicio">
                <label  id="label-dia-fim-tranporte" for="inputDataFimViagem" class="col-xs-2 col-sm-2 control-label">${textoDiaDestino}</label>
                <div class="col-xs-4 col-sm-4">
                    <select name="diaFim" rel="<%=TipoAtividade.TRANSPORTE%>" class="form-control">
                      <option value="0">Sem dia definido</option>
                      <c:forEach var="dia" items="${viagem.dias}">
                        <option value="${dia.id}" ${dia.id eq atividade.diaFim.id ? 'selected' : ''} data-numero-dia="${dia.numero}">
                            ${dia.numero}º dia 
                            <c:if test="${not empty dia.dataViagem}"> 
                              - <fmt:formatDate value="${dia.dataViagem}" pattern="EEE, dd MMM" />
                            </c:if>
                        </option>
                      </c:forEach>
                    </select>
                </div>
                <c:if test="${viagem.controlarTempo}">
                    <label for="inputDataFim" class="col-xs-1 col-sm-1 control-label">Hora</label>
                    <div class="col-xs-3 col-sm-3">
                        <input type="${typeTime}" name="dataFim" id="inputDataFim" rel="<%=TipoAtividade.TRANSPORTE%>" class="form-control" placeholder="HH:mm" value="<fmt:formatDate value="${atividade.dataHoraFim}" pattern="HH:mm" />" />
                    </div>
                </c:if>
            </div>

            <div id="div-entrega-mesmo-local" class="form-group" style="${atividade.transporte == 'ALUGUEL_VEICULO' ? '' : 'display: none;'}">
              <div class="checkbox">
                <label class="col-xs-2 col-sm-2 control-label" style="margin-right: 18px;"></label>
                <label for="check-entrega-mesmo-local" class="">
                    <input type="checkbox" id="check-entrega-mesmo-local" name="entregaMesmoLocal" style="margin-top: -2px;" ${mesmoLocalOrigemDestino ? 'checked="checked"' : ''} >
                    Entrega no mesmo local de retirada
                </label>
              </div>
            </div>
            
            <div id="div-destino-transporte" style="${mesmoLocalOrigemDestino ? 'display:none' : ''}">
                <div class="form-group" rel="OUTROS" style="${isOutros ? '' : 'display: none;'}">
                    <label id="label-destino-transporte" for="inputDestinoTransporte" class="col-xs-2 col-sm-2 control-label required">Local</label>
                    <div class="col-xs-7 col-sm-7">
                        <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocalTransporteDestino" hiddenName="localTransporteDestino" id="inputDestinoTransporte${atividade.id}" valueField="id"  
                                          value="${atividade.nomeLocalDestino}" hiddenValue="${atividade.transporteLocalDestino.id}"
                                          blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                          cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"/>
                    </div>
                </div>
                <div class="form-group" rel="AVIAO" style="${isAviao ? 'display: none;' : ''}">
                    <label id="label-origem-transporte" for="inputOrigemTransporte" class="col-xs-2 col-sm-2 control-label required">Local / Aeroporto</label>
                    <div class="col-xs-7 col-sm-7">
                        <fan:autoComplete url="/pesquisaTextual/consultarCidadesEAeroportos" name="nomeLocalTransporteDestino" hiddenName="localTransporteDestino" id="inputDestinoVoo${atividade.id}" valueField="id"  
                                          value="${atividade.nomeLocalDestino}" hiddenValue="${atividade.transporteLocalDestino.id}"
                                          blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                          cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
                                          disabled="${not isAviao}" />
                    </div>
                </div>
                <div class="form-group" rel="ALUGUEL_VEICULO" style="${isAluguel ? 'display: none;' : ''}">
                    <label id="label-origem-transporte" for="inputOrigemTransporte" class="col-xs-2 col-sm-2 control-label required">Local / Locadora</label>
                    <div class="col-xs-7 col-sm-7">
                        <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocalTransporteDestino" hiddenName="localTransporteDestino" id="inputDestinoAluguel${atividade.id}" valueField="id"  
                                          value="${atividade.nomeLocalDestino}" hiddenValue="${atividade.transporteLocalDestino.id}"
                                          blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Escreva algo para receber sugestões" rel="local" 
                                          cssClass="form-control" cssStyle="" showActions="true" useDocumentReady="false"
                                          disabled="${not isAluguel}" />
                    </div>
                </div>
            </div>
            
            <div class="div-mais-detalhes">
              <div class="form-group" style="margin-bottom: 2px; margin-top: -6px;">
                <label class="col-xs-2 col-sm-2 control-label"></label>
                <div class="col-xs-9 col-sm-9">
                    <a href="#" class="btn-mais-detalhes">+ Mais detalhes</a>
                </div>
              </div>
              <div class="form-group campos-mais-detalhes" style="display: none;">
                <c:set var="inputId" value="enderecoLocalTransporteDestino" />
                <c:set var="inputName" value="enderecoLocalTransporteDestino" />
                <c:set var="inputValue" value="${atividade.enderecoLocalDestino}" />
                <c:set var="inputDisabled" value="${not empty atividade.transporteLocalDestino}" />
                <%@include file="endereco.jsp" %>
              </div>
            </div>
            
      </div>
        
      <c:set var="tipoAtividade" value="<%=TipoAtividade.PERSONALIZADO%>"/>
      <div id="fieldset-personalizado" rel="fieldset-tipo-atividade" class="fieldset-<%=TipoAtividade.PERSONALIZADO%>" style="${(atividade.tipo eq tipoAtividade) ? '' : 'display: none;'}">
        <div class="form-group">
            <label for="input${tipoAtividade}" class="col-xs-2 col-sm-2 control-label required">Título da atividade </label>
            <div class="col-xs-9 col-sm-9">
                <form:input type="text" path="titulo" id="input${tipoAtividade}" class="form-control focus-${tipoAtividade}" />
            </div>
        </div>
        <div class="form-group">
            <label for="inputDescricaoPersonalizada" class="col-xs-2 col-sm-2 control-label">Descrição da atividade</label>
            <div class="col-xs-9 col-sm-9">
                <textarea id="inputDescricaoPersonalizada" name="descricao" class="form-control" rows="2">${atividade.descricao}</textarea>
            </div>
        </div>
      </div>
      
  </fieldset>

  <c:if test="${viagem.controlarTempo}">
    <fieldset id="fieldset-horarios" rel="fieldset-horarios">
      <legend>Horário</legend>
      <div class="form-group">
        <label for="inputDataInicio" class="col-xs-2 col-sm-2 control-label">Início</label>
        <div class="col-xs-4 col-sm-3">
            <input type="${typeTime}" name="dataInicio" id="inputDataInicio" class="form-control inputHorario" placeholder="HH:mm" value="<fmt:formatDate value="${atividade.dataHoraInicio}" pattern="HH:mm" />" />
        </div>
        <label for="inputDataFim" class="col-xs-2 col-sm-2 control-label">Término</label>
        <div class="col-xs-4 col-sm-3">
            <input type="${typeTime}" name="dataFim" id="inputDataFim" class="form-control inputHorario" placeholder="HH:mm" value="<fmt:formatDate value="${atividade.dataHoraFim}" pattern="HH:mm" />" />
        </div>
      </div>
    </fieldset>
  </c:if>
  
  <c:if test="${viagem.controlarGastos}">
    <fieldset id="fieldset-valor">
      <legend>Custo</legend>
      <div class="form-group">
        <label for="inputValor" class="col-xs-2 col-sm-2 control-label">Valor</label>
        <div class="col-xs-3 col-sm-3">
            <input type="text" name="valor" id="inputValor" class="form-control" placeholder="Valor" value="${atividade.valor}" />
        </div>
        <div class="col-xs-3 col-sm-3">
          <select name="moeda" id="selectMoeda" class="form-control">
            <option value="1">R$</option>
            <!-- option value="1">US$</option>
            <option value="1">£</option-->
          </select>
        </div>
        <div class="col-xs-2 col-sm-3">
          <div class="checkbox">
            <label for="checkPago">
              <form:checkbox id="checkPago" path="paga" value="true" cssStyle="margin-top: -2px;" /> Pago 
            </label>
          </div>
        </div>
      </div>
    </fieldset>
  </c:if>
  
  <fieldset id="fieldset-anotacoes">
      <div class="form-group">
        <label for="inputDescricao" class="col-xs-2 col-sm-2 control-label">Anotações</label>
        <div class="col-xs-9 col-sm-9">
            <form:textarea id="inputAnotacoes" path="anotacoes" class="form-control" rows="3"></form:textarea>
        </div>
      </div>
  </fieldset>
</form:form>

<script>

    $('.ddslick-atividade').ddslick({
        width: '100%',
        onSelected: function(data) {
            atualizarFieldset(data.selectedData.value);
            $('input[name="tipo"]').val(data.selectedData.value);
            if (data.selectedData.value == '<%=TipoAtividade.TRANSPORTE%>') {
                atualizarCamposTransporte($('input[name="transporte"]').val());
            }
        }
    });
    
    $('.ddslick-transporte').ddslick({
        width: '100%',
        onSelected: function(data) {
            $('input[name="transporte"]').val(data.selectedData.value);
            if ($('input[name="tipo"]').val() == '<%=TipoAtividade.TRANSPORTE%>') {
                atualizarCamposTransporte(data.selectedData.value);
            }
        }
    });
    
    $.validator.addMethod(
        'brazilianDate',
        function(value, element) {
            // put your own logic here, this is just a (crappy) example
            return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
        },
        "Informe uma data no formato 'dd/mm/yyyy'"
    );
    
    $.validator.addMethod("time24", function(value, element) {
        if (value == '') {
            return true;
        }
        if (!/^\d{2}:\d{2}$/.test(value)) return false;
        var parts = value.split(':');
        if (parts[0] > 23 || parts[1] > 59) return false;
        return true;
    }, "Informe a hora no formato 'HH:mm'");
    
    //custom validator for money fields
    $.validator.addMethod("brazilianMoney", function (value, element) {
      return this.optional(element) || value.match(/^-?(?:0|[1-9]\d{0,2}(?:,?\d{3})*)(?:\.\d+)?$/);
    }, "Please provide a valid dollar amount (up to 2 decimal places) and do not include a dollar sign.");
    
    $('#${formId}').validate({
        rules: {
            titulo: 'required',
            nomeLocal: 'required',
            nomeLocalTransporteOrigem: 'required',
            nomeLocalTransporteDestino: 'required',
            dataInicio: {
                time24 : true,
                required : false
            },
            dataFim: {
                time24 : true,
                required : false
            }/*,
            valor: {
                brazilianMoney: true
            }*/
        },
        messages: { 
            titulo: 'Informe um título para sua atividade',
            nomeLocal: 'Informe o nome do local',
            nomeLocalTransporteOrigem: 'Informe o nome do local',
            nomeLocalTransporteDestinos: 'Informe o nome do local',
            dataInicio: 'Informe uma hora válida',
            dataFim: 'Informe uma hora válida',
            //valor: 'Informe um valor válido'
        },
        errorPlacement: function(error, element) {
            var parent = element.closest('div')[0];
            error.appendTo(parent);
        },
        errorContainer: "#errorBox",
        onfocusout: false
    });
    
    function atualizarFieldset(nomeFieldset) {
        $('div[rel="fieldset-tipo-atividade"]').hide();
        $('div[rel="fieldset-tipo-atividade"]').attr('disabled', 'true');
        $('div[rel="fieldset-tipo-atividade"]').find('input, select').hide();
        $('div[rel="fieldset-tipo-atividade"]').find('input, select').attr('disabled', 'true')
        $('.fieldset-' + nomeFieldset).removeAttr('disabled');
        $('.fieldset-' + nomeFieldset).show();
        $('.fieldset-' + nomeFieldset).find('input, select').show();
        $('.fieldset-' + nomeFieldset).find('input, select').removeAttr('disabled');
        $('input[name="local"]').attr('disabled', 'disabled');
        $('.consultaLocal').attr('disabled', 'disabled');
        atualizarFieldsetEspecifico(nomeFieldset);
        var inputId = 'input' + nomeFieldset + '${atividade.id}';
        $('#' + inputId).removeAttr('disabled');
        var hiddenId = $('#' + inputId).data('hidden-id');
        $('#' + hiddenId).removeAttr('disabled');
        $('#' + inputId).focus();
    }
    
    function atualizarFieldsetEspecifico(nomeFieldset) {
        $('input[rel="<%=TipoAtividade.HOSPEDAGEM%>"], select[rel="<%=TipoAtividade.HOSPEDAGEM%>"]').attr('disabled', 'disabled');
        $('input[rel="<%=TipoAtividade.TRANSPORTE%>"], select[rel="<%=TipoAtividade.TRANSPORTE%>"]').attr('disabled', 'disabled');
        if (nomeFieldset == '<%=TipoAtividade.HOSPEDAGEM%>' || nomeFieldset == '<%=TipoAtividade.TRANSPORTE%>') {
            $('input[rel="' + nomeFieldset + '"], select[rel="' + nomeFieldset + '"]').removeAttr('disabled');
            // esconder fieldset horario
            $('fieldset[rel="fieldset-horarios"').attr('disabled', 'true');
            $('.inputHorario').attr('disabled', 'true');
            $('fieldset[rel="fieldset-horarios"').hide();
            if (nomeFieldset == '<%=TipoAtividade.TRANSPORTE%>') {
                
            }
        } else {
            $('fieldset[rel="fieldset-horarios"').removeAttr('disabled');
            $('.inputHorario').removeAttr('disabled');
            $('fieldset[rel="fieldset-horarios"').show();
        }
    }
    
    function atualizarCamposTransporte(tipoTransporte) {
        if (tipoTransporte == '<%=TipoTransporte.AVIAO%>') { 
            $('#legend-origem-transporte').html('Origem');
            $('#legend-destino-transporte').html('Destino');
            $('#label-dia-inicio-tranporte').html('Partida');
            $('#label-dia-fim-tranporte').html('Chegada');
            $('fieldset[rel="fieldset-horarios"').attr('disabled', 'true');
            // habilitar consulta
            $('div[rel="OUTROS"]').hide();
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').hide();
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').show();
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').find('input').removeAttr('disabled');
            $('div[rel="OUTROS"]').find('input').attr('disabled', 'disabled');
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').find('input').attr('disabled', 'disabled');
        } else if (tipoTransporte == '<%=TipoTransporte.ALUGUEL_VEICULO%>') {
            $('#legend-origem-transporte').html('Retirada');
            $('#legend-destino-transporte').html('Entrega');
            $('#label-dia-inicio-tranporte').html('Dia');
            $('#label-dia-fim-tranporte').html('Dia');
            $('#div-entrega-mesmo-local').show();
            // habilitar consulta
            $('div[rel="OUTROS"]').hide();
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').hide();
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').show();
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').find('input').removeAttr('disabled');
            $('div[rel="OUTROS"]').find('input').attr('disabled', 'disabled');
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').find('input').attr('disabled', 'disabled');
        } else {
            $('#legend-origem-transporte').html('Origem');
            $('#legend-destino-transporte').html('Destino');
            $('#label-dia-inicio-tranporte').html('Partida');
            $('#label-dia-fim-tranporte').html('Chegada');
            $('#div-entrega-mesmo-local').hide();
            // habilitar consulta
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').hide();
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').hide();
            $('div[rel="OUTROS"]').show();
            $('div[rel="OUTROS"]').find('input').removeAttr('disabled');
            $('div[rel="<%=TipoTransporte.AVIAO%>"]').find('input').attr('disabled', 'disabled');
            $('div[rel="<%=TipoTransporte.ALUGUEL_VEICULO%>"]').find('input').attr('disabled', 'disabled');
            //campo_inputOrigemTransporte.setExtraParams('aaaa');

        }
    }
    
    function atualizarOpcoesDataFimAtividade(rel) {
        $('select[name="diaFim"][rel="' + rel + '"]').empty();
        var dia = $('select[name="diaInicio"][rel="' + rel + '"]').val();
        dia = parseInt(dia);
        if (dia > 0) {
            $('select[name="diaInicio"][rel="' + rel + '"] option').each(function(i) {
                var val = $(this).attr('value');
                val = parseInt(val);
                if (rel == '<%=TipoAtividade.HOSPEDAGEM%>') {
                    if (val > dia) {
                        $('select[name="diaFim"][rel="' + rel + '"]').append('<option value="' + val + '">' + $(this).html() + '</option>');
                    }
                } else if (rel == '<%=TipoAtividade.TRANSPORTE%>') {
                    if (val >= dia) {
                        $('select[name="diaFim"][rel="' + rel + '"]').append('<option value="' + val + '">' + $(this).html() + '</option>');
                    }
                }
            });
            $('.dependeDiaInicio').show();
        } else {
            $('.dependeDiaInicio').hide();
        }
    }
    
    $('input[name="dataInicio"]').mask('00:00', {
        onComplete: function(data) {
            //$('#inputDataFimViagem').focus();
        }
    });

    $('input[name="dataFim"]').mask('00:00', {
        onComplete: function(data) {
            //$('#inputDataFimViagem').focus();
        }
    });
    
    $('input[name="valor"]').mask('000.000.000,00', {reverse: true});

</script>
</compress:html>