<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.page-header {
	padding-bottom: 9px;
	margin: 10px 0 20px;
	border-bottom: 1px solid #eeeeee;
}

@media ( min-width : 992px) {
	div.fixed-top-bar.affix {
		top: 70px;
	}
	div.well.affix {
		padding-left: 20% !important;
		padding-right: 20% !important;
	}
}

@media ( max-width : 767px) {
	div.fixed-top-bar.affix {
		top: 40px;
	}
	.main-content {
		padding: 0px;
	}
	#div-bottom-bar {
		padding: 1px 4px 1px 4px;
	}
}

#divMainContent {
	padding-top: 10px;
}

#divMainContent legend {
	text-transform: uppercase;
	font-size: 14px;
}

.row-eq-height {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: flex;
}

div.usercard-inner {
	background-color: #fff;
}

div.usercard-inner.selectable:hover {
	cursor: pointer;
	border: 1px solid #A3D6A3;
	background-color: #F0FFF0;
}

div.usercard-inner.selected {
	border: 1px solid #42D642;
	background-color: #E9FAE9;
}

div.usercard-inner.selected:hover {
	border: 1px solid #42D642;
	background-color: #E9FAE9;
}

.icon-selectable {
	float: right;
	color: #D3D2D2;
	padding-right: 15px;
}

div.usercard-inner.selectable:hover .icon-selectable {
	color: #83CD83;
}

div.usercard-inner.selected .icon-selectable, .card-atividade.selected:hover .icon-selectable {
	color: #5cb85c;
	/*padding-right: 17px;*/
}
</style>

<script type="text/javascript" src="<c:url value="/resources/scripts/jquery.lazyload.js"/>" ></script>

<fieldset>

	<div class="btn-group" data-toggle="buttons">
		<c:if test="${possuiAmigos}">
			<label id="btn-amigos" class="btn btn-info active"> <input type="radio" name="options" autocomplete="off"
				checked> <span class="glyphicon glyphicon-user"></span> Amigos que j� foram
			</label>
		</c:if>
		<label id="btn-email" class="btn btn-primary ${not possuiAmigos ? 'active' : ''}"> <input type="radio"
			name="options" autocomplete="off"> <span class="glyphicon glyphicon-envelope"></span> Pedir ajuda por email<span
			class="hidden-xs"> de amigos</span>
		</label>
	</div>

	<hr style="margin: 4px 0px 4px 0px;" />

	<c:if test="${possuiAmigos}">
		<div id="div-amigos">

			<div>
				<div id=fixed-bar " class="well well-sm fixed-top-bar" data-spy="affix" data-offset-top="10" data-offset-bottom="10"
					style="width: 100%; margin-left: 1px; margin-bottom: 0; padding: 0px 6px; right: 0; z-index: 99;">
					<input type="hidden" id="urlCidadeAtual" value="${destino.urlPath}" />
					<div class="row" style="padding-top: 3px; padding-bottom: 2px;">
						<div class="col-md-1"></div>
						<div class="col-md-10">
							<form>
								<div class="form-group" style="margin-bottom: 0px;">
									<label class="sr-only" for="filtro-nome-amigos">Procurar amigos</label>
									<div class="input-group">
										<input type="text" class="form-control" id="filtro-nome-amigos" placeholder="Procurar amigos">
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-search"></span>
										</div>
									</div>
								</div>
							</form>
						</div>
						<div class="col-md-1"></div>
					</div>

				</div>
			</div>
			
			<div style="margin-left: -8px; margin-right: -8px;">
				<c:forEach items="${amigosJaForam}" var="amigo" varStatus="varStatus">
					<fan:cardResponsive type="user" user="${amigo.key}" cardClass="well well-sm usercard-inner selectable"
						cardStyle="min-height: 68px; margin: 0 0 5px; 0" useLazyLoadImg="true">
						<jsp:attribute name="info">
						    <div style="display: ${idsUsuariosJaEnviados.contains(amigo.key.id) ? '' : 'none'}" class="div-enviado">
						  	  <span class="label label-success">
						  	    <span class="glyphicon glyphicon-ok"></span>
						  	  	Pedido j� enviado
						  	  </span>
						    </div>
						  <div>
						    <small>J� esteve em: </small> 
							<c:forEach items="${amigo.value}" var="local" varStatus="status">
							    <c:if test="${not status.first}">, </c:if>
								${local.nome}
							</c:forEach>
						  </div>
						</jsp:attribute>
					</fan:cardResponsive>

					<c:if test="${not varStatus.first && (varStatus.index +1) % 3 == 0}">
						<div class="clearfix"></div>
					</c:if>

				</c:forEach>
			</div>

			<div class="hide nenhumEncontrado">Nenhum amigo encontrado com o nome informado.</div>

		</div>
	</c:if>

	<div id="div-email" style="${possuiAmigos ? 'display: none;' : ''}">
		<div>
			<div class="col-md-12">
				<p>Informe abaixo os emails de seus amigos ( separados por v��rgulas )</p>
				<textarea name="emails-amigos" class="form-control" rows="5"></textarea>
			</div>

			<div class="col-md-12" style="margin-left: 0px; margin-top: 10px;">
				<a id="btn-enviar-convites-email" href="#" class="btn btn-success pull-right"> 
					<span class="glyphicon glyphicon-send"></span> Enviar pedido de ajuda
				</a>
			</div>
		</div>
	</div>

</fieldset>

<div id="fb-root"></div>

<script>
	
	accentsTidy = function(s) {
		var r = s.toLowerCase();
		r = r.replace(new RegExp("\\s", 'g'), "");
		r = r.replace(new RegExp("[������]", 'g'), "a");
		r = r.replace(new RegExp("�", 'g'), "ae");
		r = r.replace(new RegExp("�", 'g'), "c");
		r = r.replace(new RegExp("[����]", 'g'), "e");
		r = r.replace(new RegExp("[����]", 'g'), "i");
		r = r.replace(new RegExp("�", 'g'), "n");
		r = r.replace(new RegExp("[�����]", 'g'), "o");
		r = r.replace(new RegExp("[����]", 'g'), "u");
		r = r.replace(new RegExp("[��]", 'g'), "y");
		r = r.replace(new RegExp("\\W", 'g'), "");
		return r;
	};

	var imgLazyLoad;

	$(document).ready(function() {

		$('#filtro-nome-amigos').focus();

		imgLazyLoad = $('img.lazy').lazyload({
			effect : 'fadeIn',
			placeholder : '<c:url value="/resources/images/anonymous.png"/>'
		});

		$('#btn-enviar-convites-email').click(function(e) {
			e.preventDefault();
			var emails = $('textarea[name="emails-amigos"]').val();

			if (emails != '') {
				jQuery.ajax({
					type : 'POST',
					url : '<c:url value="/viagem/${viagem.id}/pedirDicasAmigosPorEnderecosDeEmail" />',
					data : {
						enderecosEmail : emails
					},
					success : function(data) {
						if (data.success) {
							$('textarea[name="emails-amigos"]').val('');
							$.gritter.add({
								title : data.title,
								text : data.message,
								time : 4000,
								image : '<c:url value="/resources/images/' + data.imageName + '.png" />'
							});
						}
					}
				});
			}
		});

		$('.btn-enviar-solicitacoes').click(function(e) {
			/*jQuery.ajax({
			    type: 'POST',
			    url: '<c:url value="/dicas/pedidoDica/enviarRequisicaoAplicativo" />',
			    success: function(data) {
			    }
			});*/

			var ativo = null;
			var usrId = null;
			var idsUsuariosTripFans = '';
			var idsUsuariosFacebook = '';
			$('#div-amigos').find('div.usercard-inner.selected').each(function(index) {

				ativo = $(this).data('usuario-ativo-tripfans');

				/*if (ativo) {
					usrId = $(this).data('usuario-id');
				} else {*/
					usrId = $(this).data('fb-id');
				//}
				if (usrId != null && usrId != '') {
					/*if (ativo) {
						if (idsUsuariosTripFans != '') {
							idsUsuariosTripFans += ','
						}
						idsUsuariosTripFans += usrId;
					} else {*/
						if (idsUsuariosFacebook != '') {
							idsUsuariosFacebook += ','
						}
						idsUsuariosFacebook += usrId;
					//}
				}

			});

			if (idsUsuariosFacebook != '' || idsUsuariosTripFans != '') {
				enviarConviteViaRequisaoAplicativo(idsUsuariosTripFans, idsUsuariosFacebook);
			}
		})

		$('#btn-amigos').click(function(e) {
			e.preventDefault();
			$('#div-email').hide();
			$('#div-amigos').show();
			$('#filtro-nome-amigos').focus();
		});

		$('#btn-email').click(function(e) {
			e.preventDefault();
			$('#div-amigos').hide();
			$('#div-email').show();
			$('textarea[name="emails-amigos"]').focus();
		});

		$('#filtro-nome-amigos').keyup(function(e) {
			//if (e.keyCode == 13) {}
			var text = accentsTidy($(this).val());
			var regex = new RegExp(text, "ig");
			var qtd = 0
			$('#div-amigos div.usercard').each(function() {
				var $card = $(this);
				if (!accentsTidy($card.attr('data-name')).match(regex)) {
					$card.hide();
				} else {
					qtd += 1;
					$card.show();
					// For�ar lazyload das imagens
					$(window).trigger('scroll');
				}
			});
			if (qtd == 0) {
				$('#div-amigos').find('.nenhumEncontrado').show();
			} else {
				$('#div-amigos').find('.nenhumEncontrado').hide();
			}
		});

	});

	window.fbAsyncInit = function() {
		FB.init({
			appId : '${tripFansEnviroment.facebookClientId}',
			status : true,
			cookie : true
		});
	}

	function enviarConviteViaRequisaoAplicativo(idsUsuariosTripFans, idsUsuariosFacebook, tripfansCallback) {
		if (idsUsuariosFacebook != '') {
			FB.ui({
				method : 'apprequests',
				message : 'Ol�! Por favor, me ajude a montar meu roteiro de Viagem.',
				to : idsUsuariosFacebook,
				//action_type:'askfor',
				//object_id: '253813521323850',
				title : 'Enviar solicita��es'
			//data: 'Friend Smash Custom Tracking 1'
			//data: '${usuario.urlPath}'
			}, function(response) {
				if (response !== null && response !== undefined && response.request !== null && response.request !== undefined) {
					var usersFacebookIds = new Array();
					var requestId = response.request;
					$(response.to).each(function(index, el) {
						usersFacebookIds.push(el);
					});
					// registrar pedido no TripFans
					$.post('<c:url value="/viagem/${viagem.id}/pedirDicas" />', {
						'idsAmigosTripFans' : idsUsuariosTripFans,
						'requestId' : requestId,
						'idsAmigosFacebook' : idsUsuariosFacebook
					}, function(response) {
						concluirEnvioPedidoDicas();
					});
				}
			});
		} else {
			// registrar pedido no TripFans
			jQuery.ajax({
				type : 'POST',
				url : '<c:url value="/viagem/${viagem.id}/pedirDicas" />',
				data : {
					'idsAmigosTripFans' : idsUsuariosTripFans,
				},
				success : function(data) {
					if (data.success) {
						concluirEnvioPedidoDicas(data);
					}
				}
			});
		}
	}

	function concluirEnvioPedidoDicas(data) {
		$('#filtro-nome-amigos').val('');
		$('#filtro-nome-amigos').trigger('change');
		$('#div-amigos').find('div.usercard-inner.selected').each(function(index) {
			$(this).removeClass('selected');
			$(this).find('.div-enviado').show();
		});
		
		
		
		$('.btn-proximo span.txt').text('Pr�ximo');
		$('.btn-enviar-solicitacoes').css('display', 'none');

		if (data) {
			$.gritter.add({
				title : data.title,
				text : data.message,
				time : 4000,
				image : '<c:url value="/resources/images/' + data.imageName + '.png" />'
			});
		}

	}

	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>