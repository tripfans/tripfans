<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="TripFans - ${viagem.titulo}" />

	<tiles:putAttribute name="stylesheets">
        
	</tiles:putAttribute>
	
    <tiles:putAttribute name="leftMenu">
    
    </tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <script>
        $(document).ready(function () {
            $('#linkAmizade').bind('click', function(event) {
                event.preventDefault();
                $.post('<c:url value="/convidarParaAmizade/${autorViagem.urlPath}" />',{}, function(response) {
                    //$('#btnAmizade_${perfil.urlPath}').remove();
                    $('#mensagemAmizade').remove();
                    $('#spanConviteEnviado').show();
                });
            });        
        });        
        </script>

        <div class="col-md-2" style="margin-top: 80px;">
        </div>

        <div id="main-content" class="col-md-8" style="margin-top: 80px;">
        
            <c:choose>
                <c:when test="${necessarioAutenticar}">
                        
                    <h2>É necessário entrar no TripFans para ver este Roteiro de Viagem</h2>
                    
                    <br/>
                    <span><%-- ainda não está no TripFans, mas se voce convidá-la vocês poderão compartilhar dicas, recomendações, fotos, planos de viagem e muito mais.--%></span>
                    <br/>
                    <br/>
                    <div class="row" style="text-align: center; margin-top: 15px;">
                        <p>
                            <a href="<c:url value="/entrarPop"/>" class="btn btn-lg btn-success login-modal-link" style="font-weight: bold; text-transform: uppercase;">Entrar </a>
                        </p>
                    </div>
                    
                </c:when>
                <c:when test="${necessarioAmizade}">
                    <div id="mensagemAmizade">
                      <h4>
                        <p><strong>${autorViagem.primeiroNome}</strong> compartilhou este roteiro somente com os amigos.
                        <sec:authorize access="isAuthenticated()">
                            <%--c:if test="${perfilView.foiConvidadoPorQuemEstaVendo == false && perfilView.convidouQuemEstaVendo == false}"--%>
                            <span id="spanConviteAmizade"> 
                                Se você conhece ${autorViagem.primeiroNome}, <a id="linkAmizade" href="#">envie um convite de amizade agora mesmo.</a></p>
                            </span>
                            <%--/c:if--%>
                        </sec:authorize>
                      </h4>
                    </div>
                    <div id="spanConviteEnviado" style="display: none;">
                      <h4> 
                        O Convite foi enviado com sucesso! ${autorViagem.primeiroNome} será notificado em breve.
                      </h4>
                    </span>
                </c:when>
                <c:otherwise>
                </c:otherwise>
            </c:choose>        
        </div>
        
    </tiles:putAttribute>
        
</tiles:insertDefinition>