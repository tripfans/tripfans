<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="br.com.fanaticosporviagens.model.entity.Dica"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">

			$(document).ready(function() {

				$('#btnFormInclusaoViagem').click(function(e) {
					e.preventDefault();

					var varData = $('#formInclusaoViagem').serialize();
					var varUrl = $('#formInclusaoViagem').attr('action');

					var request = $.ajax({
						url : varUrl,
						type : "POST",
						data : varData,
						dataType : "html"
					});

					request.done(function(mensage) {
						location.replace('${pageContext.request.contextPath}/viagem/'+mensage+'/exibir/${modoVisaoCalendario}');
					});

					request.fail(function(jqXHR, textStatus) {
					});

				});
			});
		</script>			

	</tiles:putAttribute>

	<tiles:putAttribute name="body">
 			
        <div class="container-fluid">
            <div class="block">         
				<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
		            <h2>Planejamento de Viagem</h2>
    			</div>
			
				<div class="block_content">
					<ul class="nav nav-tabs">
						<c:forEach var="visao" items="${visoesListar}">
							<c:choose>
								<c:when test="${visao eq visaoListar}">
								<li class="active"><a href="#">${visao.descricao}</a></li>
								</c:when>
								<c:otherwise>
								<li><a href="${pageContext.request.contextPath}${visao.url}">${visao.descricao}</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</ul>
					
					<c:choose>
						<c:when test="${visaoListar == 'MINHAS_VIAGENS'}">
							<fan:viagemListarMinhasViagens visibilidades="${visibilidades}" visoesViagem="${visoesViagem}" viagensProximas="${viagensProximas}" viagensRealizadas="${viagensRealizadas}" />
						</c:when>
						<c:when test="${visaoListar == 'VIAGENS_CONVIDADO'}">
							<fan:viagemListarConvidado visibilidades="${visibilidades}" visoesViagem="${visoesViagem}" viagensProximas="${viagensProximas}" viagensRealizadas="${viagensRealizadas}" />
						</c:when>
						<c:when test="${visaoListar == 'VIAGENS_AMIGOS'}">
							<fan:viagemListarAmigos visibilidades="${visibilidades}" visoesViagem="${visoesViagem}" viagensProximas="${viagensProximas}" viagensRealizadas="${viagensRealizadas}" />
						</c:when>
						<c:when test="${visaoListar == 'VIAGENS_TRIPFANS'}">
							<fan:viagemListarTripFans viagensTripFans="${viagensTripFans}" visoesViagem="${visoesViagem}" />
						</c:when>
						<c:when test="${visaoListar == 'VIAGENS_PUBLICAS'}">
							<fan:viagemListarPublico viagensPublicas="${viagensPublicas}" visoesViagem="${visoesViagem}" />
						</c:when>
						<c:when test="${visaoListar == 'VIAGENS_AGENCIAS'}">
							<fan:viagemListarAgencia viagensAgencias="${viagensAgencias}" visoesViagem="${visoesViagem}" />
						</c:when>
					</c:choose>
				</div>
			</div>
		</div>

	</tiles:putAttribute>

</tiles:insertDefinition>