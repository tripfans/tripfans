<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="suggestions-div" style="position: relative; min-height: 100%;">
            
                <div id=fixed-bar" class="well well-sm" data-spy="affix" data-offset-top="60" data-offset-bottom="200" style="width: 100%; margin-left: 0px; margin-top: -2px; margin-bottom: 0; padding: 0px 6px 6px; z-index: 99;">
                  <input type="hidden" id="urlCidadeAtual" value="${destino.urlPath}"/>
                  <input type="hidden" id="tipoLocal" value="${tipoLocal}"/>
                  <div class="row" style="padding-top: 5px;">
                      <c:if test="${not empty camposAgrupados or reiniciar}">
                        <div class="col-xs-1" style="margin-right: 10px;">
                          <a id="btn-options" class="btn btn-default btn-sm" href="#options-menu" style="padding-left: 10px; padding-right: 10px; margin-top: 2px;">
                              <span class="glyphicon glyphicon-filter"></span>
                              <span>${isMobile ? '' : 'Filtrar'}</span>
                          </a>
                        </div>
                      </c:if>
                      
                      <c:if test="${tipoLocal eq 'ATRACAO' or tipoLocal eq 'RESTAURANTE'}">
                      <div class="col-xs-2" style="margin-right: 10px;">
                            <div class="btn-group">
                              <button id="btn-filtro-tipo-local" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="padding: 3px;">
                                 <span id="img-filtro-tipo-local">
                                   <img src="${pageContext.request.contextPath}/resources/images/planejamento/${tipoLocal eq 'ATRACAO' ? 'atracao' : 'restaurante'}-small.png" class="icone-atividade img-circle" title="${tipoLocal.tituloListaLocais}" width="25" height="25">
                                 </span>
                                 <span id="txt-filtro-tipo-local" style="${isMobile ? 'display: none;' : ''}">
                                   <span>${tipoLocal.tituloListaLocais}</span>
                                 </span>
                                 <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a id="filtro-atracao" href="#">
                                        <img id="img-filtro-atracao" src="${pageContext.request.contextPath}/resources/images/planejamento/atracao-small.png" class="icone-atividade img-circle" title="O que fazer">
                                        <span id="txt-filtro-atracao">O que fazer</span>
                                    </a>
                                </li>
                                <li>
                                    <a id="filtro-restaurante" href="#">
                                        <img id="img-filtro-restaurante" src="${pageContext.request.contextPath}/resources/images/planejamento/restaurante-small.png" class="icone-atividade img-circle" title="Onde comer">
                                        <span id="txt-filtro-restaurante">Onde comer</span>
                                    </a>
                                </li>
                                <!-- li>
                                    <a href="#">
                                        <img src="/tripfans/resources/images/planejamento/restaurante-small.png" class="icone-atividade img-circle" title="Tours">
                                        Tours
                                    </a>
                                </li-->
                              </ul>
                            </div>           
                      </div>
                      </c:if>
                      <div class="col-xs-5">
                        <select id="selectCidades" class="form-control ddslick-cidades">
                            <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                                <option value="${destinoViagem.destino.urlPath}" data-local-id="${destinoViagem.destino.id}" ${destino.urlPath eq destinoViagem.destino.urlPath ? 'selected="selected"' : ''}>
                                    ${destinoViagem.destino.nome}
                                </option>
                            </c:forEach>
                        </select>
                      </div>
                      
                      <div class="col-sm-3">
                          <button id="btn-concluir-selecao" type="button" class="btn btn-success pull-right btn-large" data-dismiss="modal">
                              <span class="glyphicon glyphicon-send"></span>
                              <span title="Enviar recomenda��es">${isMobile ? '' : 'Enviar recomenda��es'}</span>
                          </button>
                      </div>
                  </div>
                  
                </div>
                
                <div id="scroll-div" class="row-fluid" style="${wizard ? '' : 'overflow-x: hidden; overflow-y: scroll; min-height: 500px;"'} >
                    <div class="borda-arredondada" style="margin-bottom: 20px;">
                        <div>
                            <div class="lista-locais" style="padding: 4px;">
                              <c:choose>
                                <c:when test="${not empty sugestoesLocais}">
                                    <div id="lista-locais-scroll" class="col-md-12 infinite-scroll" style="padding-right: 0px; margin-bottom: 80px; padding-top: 6px;">
                                        <c:set var="recomendacao" value="true"/>
                                        <c:set var="columns" value="2"/>
                                        <c:set var="columnSize" value="6"/>
                                        <%@include file="../planejamento/listaSugestoesLugares.jsp" %>
                                    </div>
                                </c:when>
                                <c:otherwise>
                                    No momento, n�o temos sugest�es para esta Cidade.
                                </c:otherwise>
                              </c:choose>
                            </div>
                        </div>
                    </div>
                </div>
                
                <c:if test="${not empty camposAgrupados}">
                
                <form id="optionsForm" style="display: none;">
                  <input type="hidden" name="nome" />
                  <input type="hidden" name="idsTags" />
                  <input type="hidden" name="idsClasses" />
                  <input type="hidden" name="estrelas" />
                  <input type="hidden" name="bairro" />
                  <input type="hidden" name="ordem" />
                  <input type="hidden" name="direcaoOrdem" />
                
                  <nav id="options-menu">
                    <ul id="settings">
                        <%-- li id="setting-location">
                            <em class="Counter">
                                ${destino.nome}
                            </em>
                            <input type="hidden" name="idCidade" value="${destino.id}" />
                            <span>Cidade</span>
                            <ul id="locations">
                                <c:forEach var="destinoViagem" items="${viagem.destinosViagem}" varStatus="status">
                                    <li data-local-id="${destinoViagem.destino.id}"><span>${destinoViagem.destino.nome}</span></li>
                                </c:forEach>
                            </ul>
                        </li--%>
                        
                        <c:forEach items="${camposAgrupados}" var="campoAgrupado">
                          
                          <c:if test="${not empty campoAgrupado.values and not empty campoAgrupado.name}">
        
                                <%-->em class="Counter">5 Km</em--%>
                                <c:if test="${campoAgrupado.name == 'estrelas'}">
                                    <c:set var="nomeAgrupamento" value="Estrelas" />
                                </c:if>
                                <c:if test="${campoAgrupado.name == 'idsTags'}">
                                    <c:set var="nomeAgrupamento" value="Categorias" />
                                </c:if>
                                <c:if test="${campoAgrupado.name == 'idsClasses'}">
                                    <c:set var="nomeAgrupamento" value="Classifica��es" />
                                </c:if>
                                <c:if test="${campoAgrupado.name == 'bairro'}">
                                    <c:set var="nomeAgrupamento" value="Bairros" />
                                </c:if>
                                
                                <li class="Label Spacer" data-toggle="collapse" data-target="#lista_${nomeAgrupamento}" style="cursor: pointer;">
                                    ${nomeAgrupamento}
                                    <span class="glyphicon glyphicon-chevron-down" style="font-family: 'Glyphicons Halflings'; display: inline-block; margin-left: -15px; top: 5px;"></span>
                                </li>
                                
                                <ul id="lista_${nomeAgrupamento}" class="in">
                                  <li>
                                    <span>Todos</span>
                                    <input type="checkbox" id="chkTodos-${campoAgrupado.name}" name="${campoAgrupado.name}_todos" value="TODOS" rel="CHK_TODOS" data-grupo="${campoAgrupado.name}" style="display: inline-block;" checked="checked" class="Toggle" />
                                  </li>
                                  <c:forEach items="${campoAgrupado.values}" var="valorCampoAgrupado">
                                    <li>
                                       <c:choose>
                                        <c:when test="${valorCampoAgrupado.name == '0'}">
                                          <c:set var="semClassificacao" value="${valorCampoAgrupado.count}" />
                                        </c:when>
                                        <c:otherwise>
                                          <span>
                                            <c:if test="${campoAgrupado.name == 'estrelas'}">
                                              <img src="<c:url value="/resources/images/star${valorCampoAgrupado.name}.png"/>"/> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                              <c:set var="campoSelecionado" value="${estrelasList.contains(valorCampoAgrupado.name)}"/>
                                            </c:if>
                                            <c:if test="${campoAgrupado.name != 'estrelas'}">
                                              <c:if test="${campoAgrupado.name == 'idsTags'}">
                                                <c:set var="codigoTag" value="${valorCampoAgrupado.name}" scope="page" />
                                                <span>${tagsLocal.getPorCodigo(codigoTag)}</span> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                <c:set var="campoSelecionado" value="${tagsList.contains(codigoTag)}"/>
                                              </c:if>
                                              <c:if test="${campoAgrupado.name == 'idsClasses'}">
                                                <c:set var="codigoClasse" value="${valorCampoAgrupado.name}" scope="page" />
                                                <span>${todasClassesLocal.getPorCodigo(codigoClasse)}</span> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                              </c:if>
                                              <c:if test="${campoAgrupado.name == 'bairro'}">
                                                 <span>${valorCampoAgrupado.name}</span> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                <c:set var="campoSelecionado" value="${bairrosList.contains(valorCampoAgrupado.name)}"/>
                                              </c:if>
                                            </c:if>
                                          </span>
                                          <input type="checkbox" name="${campoAgrupado.name}" value="${valorCampoAgrupado.name}" ${campoSelecionado ? 'checked="checked"' : ''}" rel="CHK" data-grupo="${campoAgrupado.name}" style="display: inline-block;" class="Toggle" />
                                        </c:otherwise>
                                       </c:choose>
                                    </li>
                                  </c:forEach>
                                </ul>
                          </c:if>
                                
                        </c:forEach>                
                        <%--li class="Label Spacer">Tipo da atra��o</li>
                        <li>
                            <span>Praia</span>
                            <input type="checkbox" class="Toggle" />
                        </li>
                        <li>
                            <span>Museu</span>
                            <input type="checkbox" class="Toggle" />
                        </li>
                        <li>
                            <span>Parques</span>
                            <input type="checkbox" class="Toggle" />
                        </li>
        
                        <!-- footer info -->
                        <li class="Footer">
                            <a class="button" href="#">Buscar Locais</a>
                        </li--%>
                    </ul>
        
                    <!-- header info -->
                    <!-- span class="Header">Filtrar Lugares</span-->
                  </nav>
                </form>
                
                </c:if>
                
            </div>
            
            
            
<script>
jQuery(document).ready(function() {
    $("#options-menu").mmenu({
        //offCanvas   : false,
        classes     : 'mm-white',
        header      : {
        	add			: true,
			update		: true,
			title		: 'Filtros'
        },
        searchfield : {
            add         : true,
            addTo       : '#locations',
            placeholder : 'Address, Suburbs or Regions '
        },
        footer: {
            add: true,
            update: true
        },
        onClick     : {
            setSelected : false
        }
    })
    //.on("click", "a[href=\"#\"]",
    //    function() {
    //        //alert("Thank you for clicking, but that's a demo link.");
    //        return false;
    //    })
    .on( "closing.mm", function() {
        var somethingChanged = false;
        
        $(':input').each(function () {
            if ($(this).attr('type') == 'checkbox') {
                if ($(this).data('initialValue') != $(this).attr("checked")) {
                    somethingChanged = true;
                    $(this).data('initialValue', $(this).attr("checked"));
                }
            } else {
                if ($(this).data('initialValue') != $(this).val()) {
                    somethingChanged = true;
                }
            }
        });
        
        if (somethingChanged) {
        	filtrarSugestoes();
        }
    });
	
    $(document).off('click', '#scroll-div div.card.selectable');

    $(document).off('click', '#scroll-div div.card.selectable button.btn-imperdivel');
    
    $(document).off('click', '#scroll-div div.card.selectable label.btn-imperdivel');
    
    $(document).on('click', '#scroll-div div.card.selectable label.btn-imperdivel', function (e) {
    	e.preventDefault();
    	e.stopPropagation();
    	var $btn = $(this);
        var idLocal = $(this).data('local-id');
    	
    	var $btnRecomendo = $btn.parent().find('.btn-recomendo');
    	if ($btn.hasClass('active')) {
    		$btn.removeClass('active');
    		$btn.removeClass('btn-danger');
    		var i = idsLocaisSelecionadosImperdiveis.indexOf(idLocal);
        	if (i != -1) {
        		idsLocaisSelecionadosImperdiveis.splice(i, 1);
        	}
    	} else {
    		$btn.addClass('active');
    		$btn.addClass('btn-danger');
    		if (!$btnRecomendo.hasClass('active')) {
    			$btnRecomendo.click();
    		}
    		idsLocaisSelecionadosImperdiveis.push(idLocal);
    	}
    });

    $(document).on('click', '#scroll-div div.card.selectable', function (e) {
    	
        e.preventDefault();
        var idLocal = $(this).data('local-id');
        var urlLocal = $(this).data('local-url');
        var url = '';
        var add = true;
        var $card = $(this);
        
        if ($(this).hasClass('selected')) {
            var idAtividade = $(this).data('atividade-id');
            if (idAtividade) {
            	url = '<c:url value="/viagem/removerAtividadeViagem/${viagem.id}"/>/' + idAtividade;
            } else {
            	url = '<c:url value="/viagem/removerLocalViagem/${viagem.id}"/>/' + idLocal;
            }
            add = false;
        } else {
            url = '<c:url value="/viagem/adicionarLocalEmViagem/${viagem.id}/${not empty diaInicio ? diaInicio : 0}"/>/' + urlLocal;
        }
        
        var $btnRecomendo = $card.find('.btn-recomendo');
        var $btnImperdivel = $card.find('.btn-imperdivel');
        if (add) {
            $btnRecomendo.addClass('btn-warning');
            $btnRecomendo.addClass('active');
            $card.addClass('selected');
            idsLocaisSelecionados.push(idLocal);
        } else {
            $card.removeClass('selected');
            $btnRecomendo.removeClass('btn-warning');
            $btnRecomendo.removeClass('active');
            $btnImperdivel.removeClass('btn-danger');
            $btnImperdivel.removeClass('active');
            var i = idsLocaisSelecionados.indexOf(idLocal);
        	if (i != -1) {
        		idsLocaisSelecionados.splice(i, 1);
        	}
        }
    
    });
    
    $('#filtro-atracao').unbind('click');
    
    $('#filtro-atracao').click(function (e) {
        e.preventDefault();
        if ($('#tipoLocal').val() != 'ATRACAO') {
            $('#img-filtro-tipo-local img').remove();
            $('#img-filtro-atracao').clone().css({'width' : '25px', 'height' : '25px'}).appendTo('#img-filtro-tipo-local');
            $('#txt-filtro-tipo-local span').text('');
            $('#txt-filtro-tipo-local span').append($('#txt-filtro-atracao').text());
            $('#tipoLocal').val('ATRACAO');
            atualizarSugestoes();
        }
    });
    
    $('#filtro-restaurante').unbind('click');
    	
    $('#filtro-restaurante').click(function (e) {
        e.preventDefault();
        if ($('#tipoLocal').val() != 'RESTAURANTE') {
            $('#img-filtro-tipo-local img').remove();
            $('#img-filtro-restaurante').clone().css({'width' : '25px', 'height' : '25px'}).appendTo('#img-filtro-tipo-local');
            $('#txt-filtro-tipo-local span').text('');
            $('#txt-filtro-tipo-local span').append($('#txt-filtro-restaurante').text());
            $('#tipoLocal').val('RESTAURANTE');
            atualizarSugestoes();
        }
    });
    
    $('#btn-concluir-selecao').unbind('click');
    
    $('#btn-concluir-selecao').click(function (e) {
        e.preventDefault();
        
        var idsLocais = '';
        var idsLocaisImperdiveis = '';
        $('#scroll-div div.card.selected').each(function() {
        	var $card = $(this);
            if (idsLocais != '') {
                idsLocais += ',';
            }
            idsLocais += $card.data('local-id');
            if ($card.find('label.btn-imperdivel').hasClass('active')) {
                if (idsLocaisImperdiveis != '') {
                	idsLocaisImperdiveis += ',';
                }
            	idsLocaisImperdiveis += $card.data('local-id');
            }
        });
        
        if (idsLocais != '') {
            var url = '<c:url value="/viagem/${viagem.id}/ajudar/${not empty pedido ? pedido.id : ''}/enviarSugestoes"/>';
            var request = $.ajax({
                url: url,
                type: 'GET',
                data: {
                    idsLocais: idsLocais,
                    idsLocaisImperdiveis: idsLocaisImperdiveis
                }
            });
            request.done(function(data) {
            });
            request.fail(function(jqXHR, textStatus){
            });
            
        } else {
        }
    });
    
    $(':input').each(function() {
        if ($(this).attr('type') == 'checkbox') {
            $(this).data('initialValue', $(this).attr('checked')); 
        } else {
            $(this).data('initialValue', $(this).val()); 
        }
    });
    
    $('input[rel="CHK_TODOS"]').click(function(e) {
        var grupo = $(this).data('grupo');
        $('input[rel="CHK"][data-grupo="' + grupo +'"]').removeAttr('checked');
        $(this).attr('checked', 'checked');
    });
    
    $('input[rel="CHK"]').click(function(e) {
        var grupo = $(this).data('grupo');
       $('#chkTodos-' + grupo).removeAttr('checked');
    })
    
    var $settings = $('#settings');

    //  Choose location
    var $set_location = $('#setting-location .mm-counter');
    $('#locations').find( 'li span' ).click(function() {
        $set_location.text( $(this).text() );
        $('input[name="idCidade"]').val($(this).data('local-id'));
        $settings.trigger( 'open.mm' );
    });
    
    //  Choose radius
    /*var $set_radius = $('#setting-radius .mm-counter');
    $('#radius').find( 'li span' ).click(function() {
        $set_radius.text( $(this).text() );
        $settings.trigger( 'open.mm' );
    });*/
    
    //  Show/hide searchresults
    var $results = $('.searchresult');
    $('#locations input').keyup(function() {
        $results[ ( $(this).val() == "" ) ? "hide" : "show" ]();
    });

    //  Choose pricerange
    var $set_range = $('#setting-pricerange .mm-counter'),
        $range_from = $('#price-from'),
        $range_till = $('#price-till');

    $('#pricerange').find( '.button' ).click(function() {
        $set_range.text( $range_from.val() + ' - ' + $range_till.val() );
    });
    
    $("#btn-options").click(function() {
        $("#options-menu").trigger("open.mm");
    });
    
    $('.ddslick-cidades').change(function (e) {
    	
    	var tipoLocal = $('#tipoLocal').val();
    	var cidade = $(this).val();
    	
    	atualizarSugestoes(cidade);
    	
        /*var url = '<c:url value="/viagem/${viagem.id}/' + $(this).val() + '/sugestoesLugares?filtro=false&tipoLocal=' + tipoLocal +'&locaisJaSelecionados="/>' + idsLocaisSelecionados + '&wizard=${wizard}';
        $('#options-menu').remove();
        <c:if test="${not wizard}">
        $('#tab-selecionar-atividades').load(url, function() {
            parent.$('#sugestoes-modal').find('#scroll-div').css('height', $(window).height());
            $('#scroll-div').css('height', $(window).height());
            $('#scroll-div').find('div.lista-locais').css('min-height', $(window).height() + 200);
        });
        </c:if>
        <c:if test="${wizard}">
        $('#tab-lista').load(url, function() {});
        </c:if>*/
    })
    
    /*$('.ddslick-cidades').ddslick({
        width: '100%',
        //<c:if test="${isMobile}">height: '36px',</c:if>
        //height: $(window).height()/2,
        height: 30px,
        onSelected: function(data) {
            if (data.selectedData.value != $('#urlCidadeAtual').val()) {
                $('#urlCidadeAtual').val(data.selectedData.value);
                var url = '<c:url value="/viagem/${viagem.id}/' + data.selectedData.value + '/sugestoesLugares?filtro=false&tipoLocal=${tipoLocal}"/>';
                $('#options-menu').remove();
                $('#tab-selecionar-atividades').load(url, function() {
                    parent.$('#sugestoes-modal').find('#scroll-div').css('height', $(window).height());
                    $('#scroll-div').css('height', $(window).height());
                    $('#scroll-div').find('div.lista-locais').css('min-height', $(window).height() + 200);
                });
            }
        }
    });*/
    
});

$('.infinite-scroll').jscroll({
    loadingHtml: '<img src="<c:url value="/resources/images/loading.gif"/>" alt="Carregando" /> Carregando...',
    padding: 0,
    nextSelector: 'a.jscroll-next:last',
    autoTrigger: true,
    avoidDestroy: true,
    debug: false
    //contentSelector: 'li'
});

$('div[data-spy="affix"]').affix({
    offset: {
        top: 100, 
        bottom: function () {
            return (this.bottom = $('.footer').outerHeight(true))
        }
    }
});
</script>            