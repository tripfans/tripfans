<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="br.com.fanaticosporviagens.model.entity.Dica"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
			 	$('#btnFormAlterarDadosDaViagem').click(function(e){
					e.preventDefault();
					
					var varData = $('#formAlterarViagem').serialize();
					var varUrl = $('#formAlterarViagem').attr('action');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: varData
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
			 	});
			 	
			 	$('#btnFormIncluirItemCancelar').click(function(e){
			 		if($('#formViagemItemIncluirAtualizarQuandoFechar').val() == 'sim'){
			 			$('#formViagemItemIncluirAtualizarQuandoFechar').val('nao');
						location.reload();
			 		}
			 	});
			 	
			 	$('#btnFormAlterarItemIncluir').click(function(e){
					e.preventDefault();
					
					var varData = $('#formViagemItemAlterar').serialize();
					var varUrl = $('#formViagemItemAlterar').attr('action');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: varData
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
			 	});
			 	
			 	$('#btnFormIncluirItemIncluir').click(function(e){
					e.preventDefault();
					
					var varData = $('#formViagemItemIncluir').serialize();
					var varUrl = $('#formViagemItemIncluir').attr('action');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: varData
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
			 	});
			 	
			 	$('#btnFormIncluirItemIncluirVarios').click(function(e){
					e.preventDefault();
					
					var varData = $('#formViagemItemIncluir').serialize();
					var varUrl = $('#formViagemItemIncluir').attr('action');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: varData
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						$('#formViagemItemIncluirLocal').val('');
						$('#formViagemItemIncluirLocalTransporteOrigem').val('');
						$('#formViagemItemIncluirLocalTransporteDestino').val('');
						$('#formViagemItemIncluirTitulo').val('');
						$('#formViagemItemIncluirDataInicio').val('');
						$('#formViagemItemIncluirDataFim').val('');
						$('#formViagemItemIncluirValor').val('');
						$('#formViagemItemIncluirDescricao').val('');
						$('#formViagemItemIncluirAtualizarQuandoFechar').val('sim');
					});
					
					request.fail(function(jqXHR, textStatus){
					});
			 	});
			 	
			 	$('#formViagemItemIncluirRadioItemTypeLocal').change(function(e){
				 	$('#formViagemItemIncluirFieldsetItemTypeLocal').show(); 
		 			$('#formViagemItemIncluirFieldsetItemTypeTransporte').hide(); 
					$('#formViagemItemIncluirFieldsetItemTypeAvulso').hide();
			 	});
			 	
			 	$('#formViagemItemIncluirRadioItemTypeTransporte').change(function(e){
				 	$('#formViagemItemIncluirFieldsetItemTypeLocal').hide(); 
		 			$('#formViagemItemIncluirFieldsetItemTypeTransporte').show(); 
					$('#formViagemItemIncluirFieldsetItemTypeAvulso').hide();
			 	});
			 	
			 	$('#formViagemItemIncluirRadioItemTypeAvulso').change(function(e){
				 	$('#formViagemItemIncluirFieldsetItemTypeLocal').hide(); 
		 			$('#formViagemItemIncluirFieldsetItemTypeTransporte').hide(); 
					$('#formViagemItemIncluirFieldsetItemTypeAvulso').show();
			 	});
			 	
			 	$('#formViagemItemIncluirFieldsetItemTypeLocal').show(); 
	 			$('#formViagemItemIncluirFieldsetItemTypeTransporte').hide(); 
				$('#formViagemItemIncluirFieldsetItemTypeAvulso').hide();
			 	
			 	$('#formViagemItemAlterarRadioItemTypeLocal').change(function(e){
				 	$('#formViagemItemAlterarFieldsetItemTypeLocal').show(); 
		 			$('#formViagemItemAlterarFieldsetItemTypeTransporte').hide(); 
					$('#formViagemItemAlterarFieldsetItemTypeAvulso').hide();
			 	});
			 	
			 	$('#formViagemItemAlterarRadioItemTypeTransporte').change(function(e){
				 	$('#formViagemItemAlterarFieldsetItemTypeLocal').hide(); 
		 			$('#formViagemItemAlterarFieldsetItemTypeTransporte').show(); 
					$('#formViagemItemAlterarFieldsetItemTypeAvulso').hide();
			 	});
			 	
			 	$('#formViagemItemAlterarRadioItemTypeAvulso').change(function(e){
				 	$('#formViagemItemAlterarFieldsetItemTypeLocal').hide(); 
		 			$('#formViagemItemAlterarFieldsetItemTypeTransporte').hide(); 
					$('#formViagemItemAlterarFieldsetItemTypeAvulso').show();
			 	});
				
				$('.btnAdicionarItem').click(function(e){
					var valorSelect = $(this).attr('idDia');
					$('#formViagemItemIncluirItemDono option[value="'+valorSelect+'"]').attr("selected", "selected");
			 	});
				
				$('.btnAlterarItem').click(function(e){
					e.preventDefault();
					
					var varIdItem = $(this).attr('idItem');
					var varUrl = '${pageContext.request.contextPath}/viagem/${viagem.id}/consultarItem';
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: {idItem: varIdItem}
					    , dataType: "html"
					});
					
					request.done(function(data){
						var item = JSON.parse(data);

						$('#formViagemItemAlterarIdItem').val(item.params.idItem);
						
						if(item.params.tipo == 'LOCAL'){
							$('#formViagemItemAlterarFieldsetItemTypeLocal').show();
							$('#formViagemItemAlterarFieldsetItemTypeTransporte').hide();
							$('#formViagemItemAlterarFieldsetItemTypeAvulso').hide();
							
							$('#formViagemItemAlterarRadioItemTypeLocal').attr('checked', true);
							
							$('#formViagemItemAlterarLocal').val(item.params.localUrlPath);
							$('#formViagemItemAlterarLocalTransporteOrigem').val('');
							$('#formViagemItemAlterarLocalTransporteDestino').val('');
							$('#formViagemItemAlterarTitulo').val('');
						} else if(item.params.tipo == 'TRANSPORTE'){
							$('#formViagemItemAlterarFieldsetItemTypeLocal').hide();
							$('#formViagemItemAlterarFieldsetItemTypeTransporte').show();
							$('#formViagemItemAlterarFieldsetItemTypeAvulso').hide();
							
							$('#formViagemItemAlterarRadioItemTypeTransporte').attr('checked', true);
							
							$('#formViagemItemAlterarLocal').val('');
							$('#formViagemItemAlterarTransporte').val(item.params.transporte);
							$('#formViagemItemAlterarLocalTransporteOrigem').val(item.params.transporteLocalOrigemUrlPath);
							$('#formViagemItemAlterarLocalTransporteDestino').val(item.params.transporteLocalDestinoUrlPath);
							$('#formViagemItemAlterarTitulo').val('');
						} else if(item.params.tipo == 'AVULSO') {
							$('#formViagemItemAlterarFieldsetItemTypeLocal').hide();
							$('#formViagemItemAlterarFieldsetItemTypeTransporte').hide();
							$('#formViagemItemAlterarFieldsetItemTypeAvulso').show();
							
							$('#formViagemItemAlterarRadioItemTypeAvulso').attr('checked', true);
							
							$('#formViagemItemAlterarLocal').val('');
							$('#formViagemItemAlterarLocalTransporteOrigem').val('');
							$('#formViagemItemAlterarLocalTransporteDestino').val('');
							$('#formViagemItemAlterarTitulo').val(item.params.titulo);
						} else {
							alert('ERRO - Tipo não tratado - '+item.params.tipo);
						}
						$('#formViagemItemAlterarDataInicio').val(item.params.dataInicio);
						$('#formViagemItemAlterarDataFim').val(item.params.dataFim);
						$('#formViagemItemAlterarValor').val(item.params.valor);
						$('#formViagemItemAlterarDescricao').val(item.params.descricao);
					});
					
					request.fail(function(jqXHR, textStatus){
					});
				});
				
				$('#btnMenuCopiarViagem').click(function(e){
					e.preventDefault();
					
					var varUrl = $(this).attr('href');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						$('<div></div>')
						.appendTo('body')
						.html('<div><h6>Cópia realizada com sucesso. Caso queira ir para a cópia click <a href="${pageContext.request.contextPath}/viagem/'+mensage+'/exibir/${modoVisao}">aqui</a>. </h6></div>')
						.dialog({
							modal: true
							, title: 'Cópia realizada com sucesso'
							, zIndex: 10000
							, autoOpen: true
							, width: 'auto'
							, resizable: false
							, buttons: {
								'OK' : function() {
									$(this).dialog('close');
								}
							}
						    , close: function(event, ui) {
						    	$(this).remove();
						    }
						});
					});
					
					request.fail(function(jqXHR, textStatus){
					});
										
				});
					
				$('.btnMoverItem').click(function(e){
					e.preventDefault();
					
					var varIdItem = $(this).attr('idItem');
					var varUrl = $(this).attr('href');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: {idItem: varIdItem}
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
					
				});
				
				$('.btnMovimentarParaViagem').click(function(e){
					e.preventDefault();
					
					var varIdItem = $(this).attr('idItem');
					var varUrl = $(this).attr('href');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: {idItem: varIdItem}
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
					
				});
				
				$('.btnMovimentarParaDia').click(function(e){
					e.preventDefault();
					
					var varIdItem = $(this).attr('idItem');
					var varIdDiaDestino = $(this).attr('idDiaDestino');
					var varUrl = $(this).attr('href');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
						, data: {idItem: varIdItem, idDiaDestino: varIdDiaDestino}
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
					
				});
				
				$('.btnExcluirItem').click(function(e){
					e.preventDefault();
					
					var varIdItem = $(this).attr('idItem');
					var varUrl = $(this).attr('href');
					
					$('<div></div>')
					.appendTo('body')
					.html('<div><h6>Tem certeza que deseja excluir o item?</h6></div>')
					.dialog({
						modal: true
						, title: 'Confirmação de exclusão de item'
						, zIndex: 10000
						, autoOpen: true
						, width: 'auto'
						, resizable: false
						, buttons: {
							'Sim' : function() {
								var request = $.ajax({
									url: varUrl
									, type: "POST"
									, data: {idItem: varIdItem}
								    , dataType: "html"
								});
								
								request.done(function(mensage){
									location.reload();
								});
								
								request.fail(function(jqXHR, textStatus){
								});
								
								$(this).dialog('close');
							}
							, 'Não': function(){
								$(this).dialog('close');
							}
						}
					    , close: function(event, ui) {
					    	$(this).remove();
					    }
					});
				});
				
				$('#btnAdicionarDia').click(function(e){
					e.preventDefault();
					
					var varUrl = $(this).attr('href');
					
					var request = $.ajax({
						url: varUrl
						, type: "POST"
					    , dataType: "html"
					});
					
					request.done(function(mensage){
						location.reload();
					});
					
					request.fail(function(jqXHR, textStatus){
					});
					
				});

				$('.btnExcluirDia').click(function(e){
					e.preventDefault();
					
					var varIdDia = $(this).attr('idDia');
					var varUrl = $(this).attr('href');
					
					$('<div></div>')
							.appendTo('body')
							.html('<div><h6>Tem certeza que deseja excluir o dia?</h6></div>')
							.dialog({
								modal: true
								, title: 'Confirmação de exclusão de dia'
								, zIndex: 10000
								, autoOpen: true
								, width: 'auto'
								, resizable: false
								, buttons: {
									'Sim' : function() {
										var request = $.ajax({
											url: varUrl
											, type: "POST"
											, data: {idDia: varIdDia}
										    , dataType: "html"
										});
										
										request.done(function(mensage){
											location.reload();
										});
										
										request.fail(function(jqXHR, textStatus){
										});
										
										$(this).dialog('close');
									}
									, 'Não': function(){
										$(this).dialog('close');
									}
								}
							    , close: function(event, ui) {
							    	$(this).remove();
							    }
							});
					
				});
				
				$('#btnExcluirViagem').click(function(e){
					e.preventDefault();
					
					var varUrl = $(this).attr('href');
					
					$('<div></div>')
							.appendTo('body')
							.html('<div><h6>Tem certeza que deseja excluir a viagem?</h6></div>')
							.dialog({
								modal: true
								, title: 'Confirmação de exclusão de viagem'
								, zIndex: 10000
								, autoOpen: true
								, width: 'auto'
								, resizable: false
								, buttons: {
									'Sim' : function() {
										var request = $.ajax({
											url: varUrl
											, type: "POST"
										    , dataType: "html"
										});
										
										request.done(function(mensage){
											location.replace('${pageContext.request.contextPath}/viagem/listar');
										});
										
										request.fail(function(jqXHR, textStatus){
										});
										
										$(this).dialog('close');
									}
									, 'Não': function(){
										$(this).dialog('close');
									}
								}
							    , close: function(event, ui) {
							    	$(this).remove();
							    }
							});
					
				});
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
 			
   <div class="container-fluid">
		<div class="block">         
			<div class="block_head">
                  <div class="bheadl">
                  </div>
                  <div class="bheadr">
                  </div>
            <h2>Planejamento de Viagem</h2>
        	</div>
			
			<div class="block_content">
			
			<ul class="pager">
			  <li class="previous">
				<a href="${pageContext.request.contextPath}/viagem/listar" >
	            &larr; Voltar para lista de viagens
				</a>
			  </li>
			  <c:forEach var="visao" items="${visoesViagem}">
			  	<c:choose>
					<c:when test="${modoVisao != visao}">
					<li class="next">
					</c:when>
					<c:otherwise>
					<li class="next disabled">
					</c:otherwise>
				</c:choose>
				<a href="${pageContext.request.contextPath}/viagem/${viagem.id}/exibir/${visao}" >
				<img alt="Modo de visão ${visao.nome}" src="${pageContext.request.contextPath}/resources/images/${visao.icone}" />
	            Trocar para ${visao.nome}
				</a>
				</li>
			  </c:forEach>
			</ul>

			<div class="alert alert-info">
			<h2>${viagem.titulo} (${viagem.quantidadeDias} dias)</h2>

			<div class="btn-toolbar">
				<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem 
						|| permissao.usuarioPodeExcluirViagem 
						|| permissao.usuarioPodeAdicionarDia 
						|| permissao.usuarioPodeCopiarViagem}">
				<div class="btn-group">
					<c:if test="${permissao.usuarioPodeCopiarViagem}">
					<a id="btnMenuCopiarViagem" class="btn btn-mini" href="${pageContext.request.contextPath}/viagem/${viagem.id}/copiar" >
					<img alt="Copiar para minhas viagens" src="${pageContext.request.contextPath}/resources/images/icons/page_copy.png" />
						<c:choose>
							<c:when test="${permissao.usuarioEOCriadorDaViagem}">Duplicar viagem</c:when>
							<c:otherwise>Copiar para minhas viagens</c:otherwise>
						</c:choose>
					</a>
					</c:if>
					<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
					<a href="#formAlterarViagemModal" role="button" class="btn btn-mini" data-toggle="modal">
					<img alt="Alterar viagem" src="${pageContext.request.contextPath}/resources/images/icons/application_form_edit.png" />
		            Alterar dados da viagem
					</a>
					</c:if>
					<c:if test="${permissao.usuarioPodeExcluirViagem}">
					<a id="btnExcluirViagem" class="btn btn-mini" href="${pageContext.request.contextPath}/viagem/${viagem.id}/excluir" >
					<img alt="Excluir viagem" src="${pageContext.request.contextPath}/resources/images/icons/application_form_delete.png" />
		            Excluir viagem
					</a>
					</c:if>
				</div>
				</c:if>
			</div>

			</div>
			
			<ul class="unstyled">
				<li><strong>Criada por: </strong>${viagem.usuarioCriador.nomeExibicao}</li>
				<li><strong>Data da criação: </strong><%-- >fmt:formatDate value="${viagem.dataInclusao}" pattern="dd/MM/yyyy" /--%></li>
				<li><strong>Quem pode ver: </strong><i class="${viagem.visibilidade.icone}"></i> ${viagem.visibilidade.descricao}</li>
				<c:if test="${not empty viagem.dataInicio}">
				<li><strong>Data da viagem: </strong><%--fmt:formatDate value="${viagem.dataInicio}" pattern="dd/MM/yyyy" /--%></li>
				</c:if>
				<c:if test="${not empty viagem.descricao}">
				<li><strong>Descrição da viagem: </strong>${viagem.descricao}</li>
				</c:if>
			</ul>
			
			<fan:viagemExibirCalendario viagem="${viagem}" modoVisao="${modoVisao}" permissao="${permissao}" />
			<fan:viagemExibirListaItens viagem="${viagem}" modoVisao="${modoVisao}" permissao="${permissao}" />

			<br/>
			<div class="alert alert-info">
			<h3>Resenha da Viagem</h3>
			</div>
			<fan:viagemResenha viagem="${viagem}" />
					
			<div class="alert alert-info">
			<h3>Locais Visitados</h3>
			</div>
			
			<fan:viagemLocaisVisitadosCompleto viagem="${viagem}" />
			
			<c:if test="${permissao.usuarioPodeAlterarDadosDaViagem}">
			
			<div id="formAlterarViagemModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formAlterarViagemModalLabel" aria-hidden="true">
			<form id="formAlterarViagem" action="${pageContext.request.contextPath}/viagem/${viagem.id}/alterar" method="post">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			    <h3 id="formAlterarViagemModalLabel">Alteração de dados da viagem</h3>
			  </div>
			  <div class="modal-body">
				<label for="titulo">Título*: </label><input type="text" name="titulo" value="${viagem.titulo}" />
				<label for="visibilidade">Visibilidade*: </label>
				<select name="visibilidade" >
					<c:forEach var="visibilidade" items="${visibilidades}" >
						<c:choose>
							<c:when test="${visibilidade eq viagem.visibilidade }">
								<option value="${visibilidade}" selected="selected">${visibilidade.descricao}</option>
							</c:when>
							<c:otherwise>
								<option value="${visibilidade}">${visibilidade.descricao}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
				<label for="dataInicio">Data de início: </label>
				<input type="date" name="dataInicio" value="<%--fmt:formatDate value="${viagem.dataInicio}" pattern="yyyy-MM-dd" /--%>"  />
				<label for="descricao">Descrição: </label>
				<textarea name="descricao" rows="5" cols="500">${viagem.descricao}</textarea>
			  </div>
			  <div class="modal-footer">
			    <button class="btn" data-dismiss="modal" aria-hidden="true">
			    <img alt="Cancelar" src="${pageContext.request.contextPath}/resources/images/icons/cancel.png" />
				Cancelar
				</button>
			    <button id="btnFormAlterarDadosDaViagem" class="btn btn-primary">
			    <img alt="Salvar" src="${pageContext.request.contextPath}/resources/images/icons/table_save.png" />
			    Salvar
			    </button>
			  </div>
			</form>  
			</div>
			
			</c:if>
			
			<c:if test="${permissao.usuarioPodeIncluirItem}">
			<div id="formViagemItemIncluirModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formViagemItemIncluirModalLabel" aria-hidden="true">
			<form id="formViagemItemIncluir" action="${pageContext.request.contextPath}/viagem/${viagem.id}/adicionarItem" method="post">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			    <h3 id="formViagemItemIncluirModalLabel">Inclusão de item</h3>
			  </div>
			  <div class="modal-body">
			   	<input type="hidden" name="atualizarQuandoFechar" id="formViagemItemIncluirAtualizarQuandoFechar" value="nao"/>
			  
			    <label for="itemDono">Incluir o item como*:</label>
			    <select name="itemDono" id="formViagemItemIncluirItemDono" >
			    	<option value="viagem">Item da viagem</option>
			    	<c:forEach var="dia" items="${viagem.dias}">
			    		<option value="${dia.id}">Item do Dia ${dia.numero}</option>
			    	</c:forEach>
			    </select>
			    
			    <label class="radio">
			    <input type="radio" id="formViagemItemIncluirRadioItemTypeLocal" name="itemType" value="local" checked="checked" />
			    Local a ser visitado
			    </label>
			    <label class="radio">
			    <input type="radio" id="formViagemItemIncluirRadioItemTypeTransporte" name="itemType" value="transporte" />
			    Transporte
			    </label>
			    <label class="radio">
			    <input type="radio" id="formViagemItemIncluirRadioItemTypeAvulso" name="itemType" value="avulso" />
			    Item pessoal
			    </label>
			    
			    <fieldset id="formViagemItemIncluirFieldsetItemTypeLocal">
			    <label for="local">Local a ser visitado*: </label><input class="search-query" type="text" name="local" id="formViagemItemIncluirLocal" />
			    </fieldset>
			    
			    <fieldset id="formViagemItemIncluirFieldsetItemTypeTransporte">
			    <label for="local">Transporte*: </label>
			    <select name="transporte" id="formViagemItemIncluirTransporte">
			    	<c:forEach var="transporte" items="${transportes}">
			    	<option value="${transporte}">${transporte.descricao}</option>
			    	</c:forEach>
			    </select>
			    <label for="local">Origem: </label><input class="search-query" type="text" name="localTransporteOrigem" id="formViagemItemIncluirLocalTransporteOrigem" />
			    <label for="local">Destino: </label><input class="search-query" type="text" name="localTransporteDestino" id="formViagemItemIncluirLocalTransporteDestino" />
			    </fieldset>
			    
			    <fieldset  id="formViagemItemIncluirFieldsetItemTypeAvulso">
			    <label for="local">Item pessoal*: </label>
			    <input type="text" name="titulo" id="formViagemItemIncluirTitulo" />
			    </fieldset>

				<label for="dataInicio">Horário de início: </label><input type="time" name="dataInicio" id="formViagemItemIncluirDataInicio" />
				<label for="dataInicio">Horário de fim: </label><input type="time" name="dataFim" id="formViagemItemIncluirDataFim" />
				<label for="Valor">Valor: </label><input type="number" step="0.01" min="0" name="valor" id="formViagemItemIncluirValor" />
				<label for="descricao">Descrição: </label><textarea name="descricao" rows="5" cols="500" id="formViagemItemIncluirDescricao" ></textarea>
			  </div>
			  <div class="modal-footer">
			    <button id="btnFormIncluirItemCancelar"  class="btn" data-dismiss="modal" aria-hidden="true">
			    <img alt="Cancelar" src="${pageContext.request.contextPath}/resources/images/icons/cancel.png" />
				Cancelar
				</button>
			    <button id="btnFormIncluirItemIncluir" class="btn btn-primary">
			    <img alt="Salvar" src="${pageContext.request.contextPath}/resources/images/icons/table_save.png" />
			    Salvar
			    </button>
			    <button id="btnFormIncluirItemIncluirVarios" class="btn btn-primary">
			    <img alt="Salvar e adicionar outro item" src="${pageContext.request.contextPath}/resources/images/icons/table_save.png" />
			    Salvar e adicionar outro item 
			    </button>
			  </div>
			</form>
			</div>
			</c:if>

			<c:if test="${permissao.usuarioPodeAlterarItem}">
			<div id="formViagemItemAlterarModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="formViagemItemAlterarModalLabel" aria-hidden="true">
			<form id="formViagemItemAlterar" action="${pageContext.request.contextPath}/viagem/${viagem.id}/alterarItem" method="post">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			    <h3 id="formViagemItemAlterarModalLabel">Alteração de item</h3>
			  </div>
			  <div class="modal-body">
			   	<input type="hidden" name="idItem" id="formViagemItemAlterarIdItem" />
			  
			    <label class="radio">
			    <input type="radio" id="formViagemItemAlterarRadioItemTypeLocal" name="itemType" value="local" checked="checked" />
			    Local a ser visitado
			    </label>
			    <label class="radio">
			    <input type="radio" id="formViagemItemAlterarRadioItemTypeTransporte" name="itemType" value="transporte" />
			    Transporte
			    </label>
			    <label class="radio">
			    <input type="radio" id="formViagemItemAlterarRadioItemTypeAvulso" name="itemType" value="avulso" />
			    Item pessoal
			    </label>
			     
			    <fieldset id="formViagemItemAlterarFieldsetItemTypeLocal">
			    <label for="local">Local a ser visitado*: </label><input class="search-query" type="text" name="local" id="formViagemItemAlterarLocal" />
			    </fieldset>
			    
			    <fieldset id="formViagemItemAlterarFieldsetItemTypeTransporte">
			    <label for="local">Transporte*: </label>
			    <select name="transporte" id="formViagemItemAlterarTransporte">
			    	<c:forEach var="transporte" items="${transportes}">
			    	<option value="${transporte}">${transporte.descricao}</option>
			    	</c:forEach>
			    </select>
			    <label for="local">Origem: </label><input class="search-query" type="text" name="localTransporteOrigem" id="formViagemItemAlterarLocalTransporteOrigem" />
			    <label for="local">Destino: </label><input class="search-query" type="text" name="localTransporteDestino" id="formViagemItemAlterarLocalTransporteDestino" />
			    </fieldset>
			    
			    <fieldset  id="formViagemItemAlterarFieldsetItemTypeAvulso">
			    <label for="local">Item pessoal*: </label>
			    <input type="text" name="titulo" id="formViagemItemAlterarTitulo" />
			    </fieldset>

				<label for="dataInicio">Horário de início: </label><input type="time" name="dataInicio" id="formViagemItemAlterarDataInicio" />
				<label for="dataInicio">Horário de fim: </label><input type="time" name="dataFim" id="formViagemItemAlterarDataFim" />
				<label for="Valor">Valor: </label><input type="number" step="0.01" min="0" name="valor" id="formViagemItemAlterarValor" />
				<label for="descricao">Descrição: </label><textarea name="descricao" rows="5" cols="500" id="formViagemItemAlterarDescricao" ></textarea>
			  </div>
			  <div class="modal-footer">
			    <button id="btnFormAlterarItemCancelar"  class="btn" data-dismiss="modal" aria-hidden="true">
			    <img alt="Cancelar" src="${pageContext.request.contextPath}/resources/images/icons/cancel.png" />
				Cancelar
				</button>
			    <button id="btnFormAlterarItemIncluir" class="btn btn-primary">
			    <img alt="Salvar" src="${pageContext.request.contextPath}/resources/images/icons/table_save.png" />
			    Salvar
			    </button>
			  </div>
			</form>
			</div>
			</c:if>
			
			</div>
					
		</div>
	</div>
			
	</tiles:putAttribute>

</tiles:insertDefinition>