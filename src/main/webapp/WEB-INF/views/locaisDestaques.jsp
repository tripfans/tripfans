<%@ page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

 <c:forEach items="${locaisEmDestaque}" var="localEmDestaque">
    
   <fan:card type="local" local="${localEmDestaque.local}" spanSize="10" imgSize="10" imgClass="span19" panoramioWidth="205">
       <jsp:attribute name="subtitle">
       <c:if test="${localEmDestaque.quantidadeAvaliacoes > 0}">
		<c:choose>
		  	<c:when test="${localEmDestaque.local.quantidadeAvaliacoes == 1}"><c:set var="textoAvaliacoes" value="${localEmDestaque.local.quantidadeAvaliacoes} avalia��o" /></c:when>
		  	<c:otherwise><c:set var="textoAvaliacoes" value="${localEmDestaque.local.quantidadeAvaliacoes} avalia��es" /></c:otherwise>
		</c:choose>
         <p style="margin: 0">
             <span title="Classificado como <strong>${localEmDestaque.local.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}</strong> em um total de ${textoAvaliacoes}" class="starsCategoryScaled cat${localEmDestaque.classificacaoNota}" style="display: inline-block;"></span>
         </p>
       </c:if>
       <c:if test="${localEmDestaque.possuiInteresseJaFoi}">
         <p style="margin: 0;">
             <span class="label label-info">${localEmDestaque.descricaoQuantidadeJaForam}</span>
         </p>
        </c:if>
       </jsp:attribute>
       <jsp:attribute name="info">
       <c:if test="${localEmDestaque.possuiInteresseJaFoi}">
        <br/>
         <ul class="friend-list clearfix" style="overflow: hidden;">
           <c:forEach items="${localEmDestaque.amigosJaForam}" var="usuario" varStatus="varStatus" >
            <li>
               <fan:userAvatar displayName="false" user="${usuario}" idUsuarioFacebook="${usuario.idFacebook}"
                                    marginLeft="0" marginRight="0" showFrame="false" imgSize="mini-avatar" 
                                    showShadow="false" imgStyle="width: 32px; height: 32px;" displayNameTooltip="true" showFacebookMark="false" />
            </li>
           </c:forEach>
         </ul>
       </c:if>
       </jsp:attribute>
       <jsp:attribute name="actions">
           <div style="border-top: 1px solid #eaeaea; height: 1px; width: 100%;"></div>
           <div class="pull-right" style="padding-right: 6px; padding-top: 3px;">
               <div class="btn-group pull-right">
                   <a href="<c:url value="/avaliacao/avaliar/${localEmDestaque.local.urlPath}"/>" id="btn-avaliar-<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>.id}" class="btn btn-mini" title="Escreva uma avalia��o">
                     <img src="<c:url value="/resources/images/icons/award_star_gold_3.png" />">
                   </a>
                   <a href="<c:url value="/dicas/escrever/${localEmDestaque.local.urlPath}"/>" id="btn-dica-${localEmDestaque.local.id}" class="btn btn-mini" title="Escreva uma dica">
                     <img src="<c:url value="/resources/images/icons/lightbulb.png" />">
                   </a>
               </div>
             
               <sec:authorize access="@securityService.isAdmin()">
                 <fan:addToPlanButton local="${localEmDestaque.local}" planosViagem="${planosViagem}" style="margin-right: 15px;" />
               </sec:authorize>
                
               <div class="btn-group pull-right" style="margin-right: 6px;">
                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${localEmDestaque.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" 
                                          usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                                          
                    <sec:authorize access="!@securityService.isAdmin()">
                       <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${localEmDestaque.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" 
                                              usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                       <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${localEmDestaque.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" 
                                              usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                       <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${localEmDestaque.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" 
                                              usarBotoes="false" exibirLabel="true" cssButtonClassSize="mini" marginLeftBotao="10px" marginRightBotao="10px" />
                    </sec:authorize>
               </div>
                
           </div>
       </jsp:attribute>
   </fan:card>
   
</c:forEach>
    
<div class="row" id=terms-of-service style="margin-left: 0px;"></div>
<c:if test="${not unsuportedIEVersion}">
   <script>
       var termsWidget = new panoramio.TermsOfServiceWidget('terms-of-service', {'width': '100%'});
   </script>
</c:if>