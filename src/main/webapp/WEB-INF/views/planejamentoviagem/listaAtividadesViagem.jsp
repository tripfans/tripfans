<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Planejamento ${viagem.nomeViagem}" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
	<script type="text/javascript">
			jQuery(document).ready(function() {
				$("#escolhaAtividades").fancybox({
					width:600,
					height:300
				});
			});
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="row">
			<div class="span8">
				<div class="page-header">
					<h1>${viagem.nomeViagem}</h1>
				</div>
				<div class="row">
					<div class="span1 offset1">
						<h6>Período</h6>
					</div>
					<div class="span4">
						${viagem.dataInicio} à ${viagem.dataFim}
					</div>
				</div>
				
				<div class="row">
					<div class="span1 offset1">
						<h6>Descrição</h6>
					</div>
					<div class="span4">
						${viagem.descricao}
					</div>
				</div>				
				<div class="row">
					<div class="span1 offset1">
						<h6>Locais</h6>
					</div>
					<div class="span5">
						<a href="#">{Rio de Janeiro, Brasil</a>
						<span>;</span>
						<a href="#">Buzios, Brasil</a>									
						<span>; </span>
						<a href="#">Fortaleza, Brasil</a>
						<span>; </span>
						<a href="#">Natal, Brasil</a>
						<span>; </span>
						<a href="#">Manaus, Brasil</a>
						<span>; </span>
						<a href="#">São Paulo, Brasil}</a>
					</div>
				</div>
				
				<div class="horizontalSpacer"></div>
					<div class="panel span9 offset1">
						<div class="panelHeader collapsible">
							<div class="row">
							<div class="left span1"> Dia 1</div>
							<div class="imageSideText bkImgSun left"> Rio de Janeiro, Brasil - Temp. 28ºC/25ºC</div>
                   			<div class="headerActions span2">
                   				<div class="imageSideText bkImgAdd right">
                   					<a id="escolhaAtividades" href="${pageContext.request.contextPath}/planejar/escolherTipoAtividade/planoViagem/${viagem.id}">Inclua Atividades</a>
                   				</div>
                   			</div>
							</div>

<!--							<div class="span1">Dia 1</div>-->
<!--							<div class="imageSideText bkImgSun">rio</div>-->
                   		</div>
                   		<div class="panelContent">
	                   		<div class="row">lalal</div>
	                   		<div class="row">
	                   			lalal
	                   		</div>
	                   		<div class="row">
	                   			lalal
	                   		</div>
	                   		<div class="row">
	                   			lalal
	                   		</div>
               			</div>
               			
               		</div>
               	
					<!-- 
					<div class="span8">
					<div class="horizontalSpacer"></div>
					<div class="borderFullPanel scroll" style="height: 250px">
						<ul class="borderBottomList">
							<li>
								<a href="#">	
									<img class="userPhotoSmall" src="${pageContext.request.contextPath}/resources/images/anonymous.png" />
								</a>
								<span>Participante 1</span>
							</li>
							<li>
								<a href="#">	
									<img class="userPhotoSmall" src="${pageContext.request.contextPath}/resources/images/anonymous.png" />
								</a>
								<span>Participante 1</span>
							</li>
							<li>
								<a href="#">	
									<img class="userPhotoSmall" src="${pageContext.request.contextPath}/resources/images/anonymous.png" />
								</a>
								<span>Participante 1</span>
							</li>
							<li>
								<a href="#">	
									<img class="userPhotoSmall" src="${pageContext.request.contextPath}/resources/images/anonymous.png" />
								</a>
								<span>Participante 1</span>
							</li>
						</ul>
					</div>-->
			</div>
		</div>
		
	</tiles:putAttribute>

</tiles:insertDefinition>