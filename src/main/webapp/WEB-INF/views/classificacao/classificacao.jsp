<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">

        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
        	<div class="page-header">
                <h3>Classifica��es para ${localType.descricaoPlural}</h3>
            </div>
    
    		<ul>
            <c:forEach items="${categorias}" var="categoria">
            	<li><strong>Categoria:</strong> ${categoria.descricao}
            	<br/><strong>Classes:</strong>
            	<ul>
            	<c:forEach items="${categoria.classes}" var="classe">
            		<li>${classe.descricao}</li>
            	</c:forEach>
            	</ul>
            	</li>
            </c:forEach>
            </ul>
            
            <p>
            Para adicionar novos �tens de classificaca��o para ${localType.descricaoPlural}, por favor, 
            entre em contado pelo email parceiros@tripfans.com.br informe a descri��o do �tem pretendido, 
            em qual categoria a nova classe se enquadra e de exemplos de empreendimentos que poderiam 
            ser classificados com essa nova classe. Obrigado.
            </p>
            
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>