<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:forEach var="notificacao" items="${notificacoes}" >
   	<c:set var="notificacao" value="${notificacao}" scope="request" />
   	<div class="row">
    	<div class="span12" style="margin-left: 0px;">
  	        <jsp:include page="notificacao.jsp">
                  <jsp:param value="true" name="displayDetails"/>
              </jsp:include>
        </div>
   </div>
    <hr style="margin:2px;" />
</c:forEach>

<div style="text-align: center; padding: 8px;">
    <c:url var="urlNotificacoes" value="/perfil/maisNotificacoes/${perfil.urlPath}" />
    <fan:pagingButton targetId="listaNotificacoes" limit="10" title="Ver Mais Notificações" noMoreItensText="" cssStyle="width: 95%;" url="${urlNotificacoes}" />
</div>
