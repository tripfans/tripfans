<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<form id="filters" style="margin: 0px;">
    <table cellspacing="0" cellpadding="6" border="0" width="100%" align="center">
        <tbody>
            <tr>
                <td width="25%" align="center">
                    <img border="0" alt="J� foi" src="<c:url value="/resources/images/markers/est-pin.png" />">
                    <div>
                        <input id="filtroJaFoi" type="checkbox" onclick="filtrarMarcadores();" checked="checked">
                        J� foi 
                        <span id="numeroJaFoi" class="badge badge-success">
                            ${perfil.quantidadeCidadesJaFoi}
                        </span>
                    </div>
                </td>
                <td width="25%" align="center">
                    <img border="0" alt="Deseja ir" src="<c:url value="/resources/images/markers/des-pin.png" />">
                    <div>
                        <input id="filtroDesejaIr" type="checkbox" onclick="filtrarMarcadores();" checked="checked">
                        Deseja ir
                        <span id="numeroDesejaIr" class="badge badge-info">
                            ${perfil.quantidadeCidadesDesejaIr}
                        </span>
                    </div>
                </td>
                <td width="25%" align="center">
                    <img border="0" alt="Favorito" src="<c:url value="/resources/images/markers/est-fav-pin.png" />">
                    <div>
                        <input id="filtroFavorito" type="checkbox" onclick="filtrarMarcadores();" checked="checked">
                        Favorito 
                        <span id="numeroFavorito"class="badge badge-important">
                            ${perfil.quantidadeCidadesFavorito}
                        </span>
                    </div>
                </td>
                <td width="25%" align="center">
                    <img border="0" alt="Indica" src="<c:url value="/resources/images/markers/est-ind-pin.png" />">
                    <div>
                        <input id="filtroIndica" type="checkbox" onclick="filtrarMarcadores();" checked="checked">
                        Indica 
                        <span id="numeroIndica" class="badge badge-warning">
                            ${perfil.quantidadeCidadesIndica}
                        </span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</form>