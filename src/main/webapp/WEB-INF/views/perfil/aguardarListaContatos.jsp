<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.popup">
    <tiles:putAttribute name="body">
    
    <div id="conteudo" style="width: 100%">
    
      <div style="margin-top: 150px;" align="center">
        <p>
            Por favor, aguarde...
            <br/>
            <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
        </p>
      </div>
    </div>
    
    <script>
        $(document).ready(function() {
             $('#conteudo').load('<c:url value="/perfil/consultarContatosGoogle" />');
        });
    </script>
    
    </tiles:putAttribute>
</tiles:insertDefinition>