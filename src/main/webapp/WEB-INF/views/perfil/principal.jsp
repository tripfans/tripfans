<%@ page import="br.com.fanaticosporviagens.model.entity.TipoInteressesViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib prefix="tf" uri="/WEB-INF/tripfansFunctions.tld" %>

<c:set var="tituloMapaViagens" value="Mapa de Viagens de ${perfil.displayName}" />
<c:set var="tituloViagens" value="�ltimas Viagens de ${perfil.displayName}" />
<c:set var="tituloFotos" value="�ltimas Fotos de ${perfil.displayName}" />
<c:set var="tituloAtividades" value="Atividades Recentes de ${perfil.displayName}" />
<c:set var="tituloComentarios" value="Coment�rios" />
<c:set var="tituloSobre" value="Sobre ${perfil.displayName}" />
<c:set var="tituloInteresses" value="Interesses de Viagem de ${perfil.displayName}" />
<c:set var="tituloAmigos" value="Amigos de ${perfil.displayName}" />

<c:if test="${perfil.id == usuario.id}">
	<c:set var="tituloMapaViagens" value="Meu Mapa de Viagens" />
	<c:set var="tituloViagens" value="Minhas �ltimas Viagens" />
	<c:set var="tituloFotos" value="Minhas �ltimas Fotos" />
	<c:set var="tituloAtividades" value="Minhas Atividades Recentes" />
	<c:set var="tituloComentarios" value="Coment�rios" />
	<c:set var="tituloSobre" value="Sobre Mim" />
	<c:set var="tituloInteresses" value="Meus Interesses de Viagem" />
	<c:set var="tituloAmigos" value="Meus Amigos" />
</c:if>

<div class="span12" >
    
    		<c:if test="${not empty viagens}">
				<div class="page-header" style="margin: 10px 20px 20px 0;">
					<h3>
					<span style="background-image: url('<c:url value="/resources/images/icons/briefcase.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
					${tituloViagens}
					</h3>
				</div>	
				<div class="borda-arredondada" style="padding: 15px; display: inline-block; margin-left: 0px; position: relative; width: 90%;">
			    
					<div>
    			
			        	<div id="roteiros-container" class="showbiz-container whitebg sb-retro-skin" style="padding-bottom: 0px;">
			
				            <!-- THE NAVIGATION -->
				            <div class="showbiz-navigation sb-nav-retro hidden-xs">
				                <a id="showbiz_left" class="sb-navigation-left"><i class="sb-icon-left-open"></i></a>
				                <a id="showbiz_right" class="sb-navigation-right"><i class="sb-icon-right-open"></i></a>
				                <div class="sbclear"></div>
				            </div> <!-- END OF THE NAVIGATION -->
				
				            <div class="divide10"></div>
				
				            <!--    THE PORTFOLIO ENTRIES   -->
				            <div class="showbiz" data-left="#showbiz_left" data-right="#showbiz_right" data-play="#showbiz_play">
				
				                <!-- THE OVERFLOW HOLDER CONTAINER, DONT REMOVE IT !! -->
				                <div class="overflowholder">
				                    <!-- LIST OF THE ENTRIES -->
				                    <ul id="ul-list-roteiros">
				                    
				                      <c:forEach items="${viagens}" var="viagem">
				
				
				                        <li class="sb-retro-skin">
				                            <!-- THE MEDIA HOLDER -->
				                            <div class="mediaholder conteudoRoteiro" style="border: 0px;" data-urlroteiro="<c:url value="/viagem/${viagem.id}" />">
				                                <div class="mediaholder_innerwrap borda-arredondada">
				                                    <i class="carrossel-thumb carrossel-home-img" data-foto-id="${viagem.id}" data-foto-url-big="<c:url value="${viagem.urlFotoPrincipal}"/>"
				                                       style="background-image: url('<c:url value="${viagem.urlFotoPrincipal}"/>'); min-height: 200px;"></i>
				                                    <!-- HOVER COVER CONAINER -->
				                                    <div class="hovercover borda-arredondada" style="z-index: 21;">
				                                        <a rel="group">
				                                            <div class="linkicon" style="cursor: pointer;"><i class="sb-icon-plus morebutton" data-urlroteiro="<c:url value="/viagem/${viagem.id}" />"></i></div>
				                                        </a>
				                                    </div><!-- END OF HOVER COVER -->
				                                </div>
				                            </div><!-- END OF MEDIA HOLDER CONTAINER -->
				
				                            <!-- DETAIL CONTAINER -->
				                            <div class="detailholder" style="margin-top: 10px;">
				                                <p style="min-height: 50px; font-family: 'PT Sans Narrow', sans-serif;">
				                                    <strong class="local-card-title" style="padding-top: 0px; margin-bottom: 0px;">
				                                      <a href="<c:url value="/viagem/${viagem.id}" />" style="text-decoration: none;">
				                                        ${viagem.titulo}
				                                      </a>
				                                    </strong>
				                                    
				                                </p>
				                            </div><!-- END OF DETAIL CONTAINER -->
				                            
				                        </li>
				                        
				                      </c:forEach>
				                    </ul>
				                    <div class="sbclear"></div>
				                </div> <!-- END OF OVERFLOWHOLDER -->
				                <div class="sbclear"></div>
				            </div>
			        	</div>
	        
				    </div>
				    
				</div>
    		</c:if>
    		
	
	
    <sec:authorize access="@securityService.isFriend('${perfil.id}')">
	 <c:if test="${perfil.quantidadeAtividades != 0}">
	  <div class="page-header" style="margin: 0 20px 20px 0;">
		<h3>
		<span style="background-image: url('<c:url value="/resources/images/icons/page_white_text.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		${tituloAtividades}
		</h3>
	  </div>
	  <div>
		<jsp:include page="listaAtividades.jsp" >
			<jsp:param value="span9" name="cssSpan"/>
		</jsp:include>
	  </div>
	  <div class="row right">
		<a class="seeMore" href="<c:url value="/perfil/${perfil.urlPath}/atividades" />" >Ver todas</a>
	  </div>
	
	  <div class="horizontalSpacer"></div>
	 </c:if>
    </sec:authorize>
	
	<div class="page-header" >
		<h3>
		<span style="background-image: url('<c:url value="/resources/images/icons/comments.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		${tituloComentarios}<span class="badge" style="margin-left: 5px;">${perfil.quantidadeComentarios}</span>
		</h3>
	</div>
	<div>
		<fan:comments inputClass="span8" tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.PERFIL.name()%>" id="painel-comentarios-perfil" idAlvo="${perfil.id}" buttonOnSameLine="true" />
	</div>
	
	<div class="horizontalSpacer"></div>
</div>

<div class="span5 right" style="margin-left: 0px;">
	 <sec:authorize access="isAuthenticated()">
		<c:if test="${usuario.id == perfil.id and usuario.conectadoFacebook}">
			<jsp:include page="/WEB-INF/views/convite/caixaConvidarAmigosFacebook.jsp" />
		</c:if>
	</sec:authorize>

	<div class="page-container" style="padding: 5px; margin-top: 12px;">
    	<div class="page-header" style="margin: 0 0px 20px 0;" style="margin-top: 0px;">
			<h3>${tituloSobre}</h3>
		</div>
		<c:if test="${not empty perfil.frasePerfil}">
            <small><i>"${perfil.frasePerfil}"</i></small>
            <div class="horizontalSpacer"></div>
        </c:if>
        <c:if test="${not empty perfil.nomeCompleto}">
        	<div><strong>Nome completo: </strong><div class="right">${perfil.nomeCompleto}</div><hr style="margin-top: 2px;"/></div>
        </c:if>

        <div><strong>Membro desde</strong><div class="right"><fan:date name="" id="" textOnly="true" value="${perfil.dataCadastro}"/></div><hr style="margin-top: 2px;"/></div>
        <c:if test="${not empty perfil.cidadeNatal or (not empty perfil.dataNascimento and perfil.exibirDataNascimento)}">
        	<div>
                <strong>
                    Nasceu 
                </strong>
                <c:if test="${not empty perfil.cidadeNatal}">
                  <strong>em</strong>
                  <div class="right">
                    <fan:localLink local="${perfil.cidadeNatal}" nomeCompleto="true" />
                  </div>
                </c:if>
                
                <c:if test="${perfil.exibirDataNascimento and not empty perfil.dataNascimento}">
                  <c:set var="formatoDataNascimento" value="${perfil.exibirDataNascimentoCompleta ? 'dd/MM/yyyy' : 'dd, MMMM' }"></c:set>
                  <div>
                    <strong>&nbsp;</strong>
                    <div class="right">
                        em
                        <fmt:formatDate value="${perfil.dataNascimento}" pattern="${formatoDataNascimento}" />
                    </div>
                  </div>
                </c:if>                
                <hr style="margin-top: 2px;"/>
            </div>
        </c:if>
        <c:if test="${not empty perfil.cidadeResidencia}">
        	<div>
                <strong>Mora em</strong>
                <div class="right">
                    <fan:localLink local="${perfil.cidadeResidencia}" nomeCompleto="true" />
                </div>
                <c:if test="${not empty perfil.dataResidencia}">
                  <div>
                    <strong>&nbsp;</strong>
                    <div class="right">
                        <span>
                          desde <fmt:formatDate value="${perfil.dataResidencia}" pattern="MMMM yy" />
                        </span>
                    </div>
                  </div>
                </c:if>                
                <hr style="margin-top: 2px;"/>
            </div>
        </c:if>
        <c:if test="${perfil.exibirGenero}">
			<div><strong>G�nero </strong>
			<div class="right">
				<c:choose>
        		<c:when test="${perfil.genero == 'M'}"><s:message code="label.usuario.perfil.genero.masculino" /></c:when>
        		<c:otherwise><s:message code="label.usuario.perfil.genero.feminino" /></c:otherwise>
        		</c:choose>
			</div><hr style="margin-top: 2px;"/></div>        
        </c:if>
        
        <sec:authorize access="isAuthenticated()">
	        <c:if test="${usuario.id == perfil.id}">
	            <div class="servicos">
	            
	                <ul class="servicos centerAligned" style="margin-bottom: 0;">
	            
	                    <li class="facebook">
	                        <c:choose>
	                          <c:when test="${perfil.conectadoFacebook}">
	                            <b title="Voc� est� conectado com Facebook">
	                            </b>
	                          </c:when>
	                          <c:otherwise>
	                            <form name="fb_signin_connect" action="<c:url value="/connect/facebook"/>" method="POST">
	                                <input type="hidden" name="scope" value="email,user_hometown,public_profile,user_friends" />
	                                <input type="hidden" name="redirectUrl" value="/perfil/${usuario.urlPath}" />
	                            </form>
	                            <a href="#" onclick="document.forms['fb_signin_connect'].submit()" title="Conectar-se ao Facebook">
	                            </a>
	                          </c:otherwise>
	                        </c:choose>
	                    </li>
	        
	                    <li class="foursquare">
	                        <c:choose>
	                          <c:when test="${perfil.conectadoFoursquare}">
	                            <b title="Voc� est� conectado com Foursquare">
	                            </b>
	                          </c:when>
	                          <c:otherwise>
	                            <form name="form_foursquare_connect" action="<c:url value="/connect/foursquare"/>" method="POST">
	                            	<input type="hidden" name="redirectUrl" value="/perfil/${usuario.urlPath}" />
	                            </form>
	                            <a href="#" onclick="document.forms['form_foursquare_connect'].submit()" title="Conectar-se ao Foursquare">
	                            </a>
	                          </c:otherwise>
	                        </c:choose>
	                    </li>
	        
	                    <li class="twitter">
	                        <c:choose>
	                          <c:when test="${perfil.conectadoTwitter}">
	                            <b title="Voc� est� conectado com Twitter">
	                            </b>
	                          </c:when>
	                          <c:otherwise>
	                            <form name="form_twitter_connect" action="<c:url value="/connect/twitter"/>" method="POST">
	                            	<input type="hidden" name="redirectUrl" value="/perfil/${usuario.urlPath}" />
	                            </form>
	                            <a href="#" onclick="document.forms['form_twitter_connect'].submit()" title="Conectar-se ao Twitter">
	                            </a>
	                          </c:otherwise>
	                        </c:choose>
	                    </li>
	                    
	                </ul>
	                
	            </div>
	            
	            <c:if test="${not perfil.conectadoFacebook or not perfil.conectadoFoursquare or not perfil.conectadoTwitter}">
	                <ul style="list-style: none;" class="centerAligned">
	                    <li>
	                        <a href="<c:url value="/conta/minhaConta/config" />" title="Clique nos bot�es acima para criar uma conex�o entre sua conta no TripFans e outras contas. Para saber mais, clique neste link.">Conecte-se a mais servi�os</a>
	                    </li>
	                </ul>
	            </c:if>
	        </c:if>
		</sec:authorize>
	</div>
	<div class="horizontalSpacer"></div>
    
    <c:if test="${not empty interessesView.interessesViagem.listaInteressesViagem}">
		<div class="page-container" style="padding: 5px;">
		    	<div class="page-header" style="margin-top: 0px;">
					<h3>${tituloInteresses}</h3>
				</div>
				
				<div style="margin-left: 9px;" align="center">
					<c:set var="quantidade" value="0"  scope="page"/>
				 	<c:forEach items="<%=TipoInteressesViagem.values() %>"  var="tipoInteresse"  varStatus="count">
				   		<c:set var="valorInteresse" value="${tf:getPropertyValue(interessesView.interessesViagem, tipoInteresse.nome)}"/>
				  		 <c:if test="${valorInteresse && quantidade < 9}">
				       		<i class="interesse ${tipoInteresse.nome} borda-arredondada" title="${tipoInteresse.descricao}"> </i>
				       		<c:set var="quantidade" value="${quantidade + 1}"  scope="page"/>
				  		 </c:if>
				 	</c:forEach>
				</div>
				<div class="row right">
					<a class="seeMore" href="<c:url value="/perfil/${perfil.urlPath}/interesses" />" >Ver todos</a>
				</div>
		        <br/>
		</div>
	  	<div class="horizontalSpacer"></div>
    </c:if>
	
	<c:if test="${perfil.quantidadeAmigos > 0 && not empty amizades}">
	<div class="page-container" style="padding: 5px;">
    	<div class="page-header" style="margin-top: 0px;">
			<h3>${tituloAmigos} <span class="badge" style="padding-bottom: 3px;">${perfil.quantidadeAmigos}</span></h3>
		</div>
		<div style="margin-left: -5px; margin-bottom: 3px;" align="center">
			<c:forEach items="${amizades}" var="amizade" varStatus="count">
				<%-- Comentamos esse trecho pois, nao vimos sentidos em nao habilitar o link. ver com Andre 
                <c:set var="enableLink" value="false" />
				<c:if test="${perfil.id == usuario.id}"--%>
					<c:set var="enableLink" value="true" />
				<%--</c:if> --%>
				<div style="display: inline-table;">
					<fan:userAvatar imgSize="mini-small" friendship="${amizade}" displayName="false" displayNameTooltip="true" enableLink="${enableLink}" marginRight="0" />
				</div>
			</c:forEach>
		</div>
		<div class="row right">
		<a class="seeMore" href="<c:url value="/perfil/${perfil.urlPath}/amigos" />" >Ver todos</a>
		</div>
		<br/>
	</div>
	</c:if>
</div>

<c:choose>
<c:when test="${not empty viagens and viagens.size() > 3}">
	<c:set var="slideCarrossel" value="on" />
</c:when>
<c:otherwise>
	<c:set var="slideCarrossel" value="off" />
</c:otherwise>
</c:choose>

<script>

$(document).ready(function () {
	$('.nano').nanoScroller({
		preventPageScrolling: true
	});
	
	jQuery('#roteiros-container').showbizpro(
        {
            dragAndScroll: 'on',
            slideAmount : 3, 
            visibleElementsArray : [ 3, 2, 2, 1 ], 
            mediaMaxHeight : [ 0, 0, 0, 0 ],
            carousel : '${slideCarrossel}', 
            entrySizeOffset : 0,
            autoPlay : 'on', 
            delay : 5000, 
            speed : 300, 
            rewindFromEnd : 'on'
        }
     );
	
	$(document).on('click', '.conteudoRoteiro', function(e) {
        e.preventDefault();
        var urlRoteiro = $(this).data('urlroteiro');
        window.location.href = urlRoteiro;
    });
	
	
});

</script>
		
