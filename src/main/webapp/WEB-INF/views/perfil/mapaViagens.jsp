<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="titulo" value="Mapa de Viagens de ${perfil.primeiroNome}" />
<c:if test="${usuario.id == perfil.id }" >
	<c:set var="titulo" value="Meu Mapa de Viagens" />
</c:if>

<style>
td.center {
    text-align: center;
    padding-top: 5px;
}

td.small {
    font-size: 10px;
    padding: 4px 2px 2px;
}

#map_canvas { 
    height: 100%; 
}

#locais-box {
    float: left;
    border: 1px solid #CCC;
    min-height: 489px;
    max-height: 489px;
    /*min-width: 296px;*/
    background-color: #EEE;
}

#locais-box ul {
    list-style: none;
    margin: 0 0 0 0;
    max-height: 434px;
}

#menu-locais {
    overflow-y: scroll;
}

.local-title {
    background-color: #EEE;
    border-bottom: 1px solid #CCC;
    color: #777;
    font-weight: bold;
    padding: 5px;
    padding-bottom: 8px;
}

.local-title-small {
    font-size: 9px;
    font-weight: normal;
}

.local-item {
    padding: 2px;
    cursor: pointer;
    cursor: hand;
    background-color: #FFFFFF;
}

li .local-item {
    padding: 5px;
    overflow: hidden;
    border-bottom: 1px solid #CCC;
}

.local-item-name {
    font-size: 11px;
    vertical-align: middle;
    max-width: 130px;
}

.local-item-active {
    background-color: #d9edf7;
}

#map-box {
    /*float: right;
    width: 630px;*/
    border-top: 1px solid #CCC;
    border-right: 1px solid #CCC;
}

#map-header {
    background-color: #EEE;
    padding-top: 2px;
    padding-bottom: 2px;
    border-bottom: 1px solid #CCC;
    min-height: 33px;
}

#map-footer {
    background-color: #EEE;
    padding-top: 2px;
    padding-bottom: 3px;
    border-top: 1px solid #CCC;
    border-bottom: 1px solid #CCC;
}

.borda-foto {
    /*padding: 4px;
    border: 1px solid #C0D0A1;*/
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
}

</style>

<div class="span16">
    <div>

		<div class="page-header">    
            <h3>
               ${titulo}
            </h3>
        </div>
        
        <c:if test="${usuario.id == perfil.id}">
	        <s:message var="texto1" code="textoAjuda.mapaViagens1" />
			<s:message var="texto2" code="textoAjuda.mapaViagens2" />
			<fan:helpText text="${texto1}${texto2 }"  showQuestion="false"/>
		</c:if>
        
        <div>
        
            <div class="compartilharMapa" style="margin-bottom: 10px;">
                <%@include file="compartilharMapa.jsp" %>
            </div>
        
            <div id="locais-box" class="span5" style="margin: 0px;">
            
              <c:choose>

                  <c:when test="${usuario.id == perfil.id}">
                
                    <%-- LISTA DE CIDADES --%>
                    <div id="cityList">
                        <div style="max-width: 296px;">
                           N�o existem destinos a exibir pr�ximos ao local. Por favor mova para outra �rea do mapa para selecionar os locais.
                       </div>
                    </div>            
    
                  </c:when>

                  <c:otherwise>
            
                    <ul>
                        <li>
                            <div class="local-title">
                                <table style="width: 96%;">
                                    <tr>
                                        <td width="45%">
                                        </td>
                                        <td width="13%" class="center local-title-small">
                                            J� foi
                                        </td>
                                        <td width="13%" class="center local-title-small">
                                            Deseja ir
                                        </td>
                                        <td width="13%" class="center local-title-small">
                                            Favorito
                                        </td>
                                        <td width="13%" class="center local-title-small">
                                            Indica
                                        </td>
                                    </tr>
                                </table>                        
                            </div>
                            
                            <ul id="menu-locais">
                            
                                <c:forEach items="${interessesUsuarioEmCidades}" var="item" varStatus="count">
                                  <c:if test="${item.local.latitude != null && item.local.longitude != null}">

                                    <li id="local-item-${item.local.id}" class="local-item selectable" data-local-id="${item.local.id}">
                                        <table>
                                            <tr>
                                                <td width="150">
                                                    <i class="country-flag ${item.local.paisUrlPath}"> </i>
                                                    <span class="local-item-name">${item.local.nome}</span>
                                                </td>
                                                <td width="30" class="center local-title-small">
                                                    <i class="map-marker est-pin ${item.jaFoi ? '' : 'disabled'}" title="J� foi"> </i>
                                                </td>
                                                <td width="30" class="center local-title-small">
                                                    <i class="map-marker des-pin ${item.desejaIr ? '' : 'disabled'}" title="Deseja ir"> </i>
                                                </td>
                                                <td width="30" class="center local-title-small">
                                                    <i class="map-marker est-fav-pin ${item.favorito ? '' : 'disabled'}" title="Favorito"> </i>
                                                </td>
                                                <td width="30" class="center local-title-small">
                                                    <i class="map-marker est-ind-pin ${item.indica ? '' : 'disabled'}" title="Indica"> </i>
                                                </td>
                                            </tr>
                                        </table>                            
                                    </li>
                                    
                                  </c:if>
                                </c:forEach>
                                    
                            </ul>
                        </li>
                    </ul>
                    
                </c:otherwise>
                
              </c:choose>
            
            </div>
            
            <div id="map-box" class="span10" style="margin: 0px;">
            
                <div id="map-header">
                  <c:if test="${usuario.id == perfil.id}">
                  
                    <c:url value="/perfil/salvarInteressesUsuarioEmCidade" var="url_adicionar" />
                    <form id="mapaViagensForm" action="${url_adicionar}" class="form-horizontal" style="margin: 0 0 0px;" onsubmit="return false;">
                    
                        <div class="control-group" style="margin-bottom: 0px;">
                            <label for="novaCidade" class="control-label" style="color: #888">
                                Adicionar cidade
                            </label>
                            <div class="controls">
                                <fan:autoComplete url="/pesquisaTextual/consultarCidades" hiddenName="local" id="novaCidade" 
                                                  valueField="id" labelField="nomeExibicao" 
                                                  cssClass="span6" showActions="false" onSelect="adicionarCidade()"
                                                  jsonFields="[{name : 'id'}, {name : 'nome'}, {name : 'nomeExibicao'}]"/>
                                <!-- input type="submit" value="Adicionar" class="btn-success" style="width: 120px;" /-->
                            </div>
                        </div>
                    
                        <input type="hidden" name="usuario" value="${perfil.id}" />
                        <input type="hidden" name="jaFoi" value="true" />
                    </form>
                  </c:if>
                </div>
            
                <%-- MAPA --%>
                <div style="height: 390px; border: 0px; margin: 0px;" class="span10">
                    <%@include file="mapaGoogle.jsp" %>
                </div>
                
                <%-- FILTROS --%>
                <div id="map-footer" class="span10" style="margin: 0px;">
                    <%@include file="mapaGoogleFooter.jsp" %>
                </div>
                
            </div>
            
            <%-- div class="compartilharMapa" style="margin-top: 5px; display: inline-table;width: 100%; margin-bottom: 20px;">
                <%@include file="compartilharMapa.jsp" %>
            </div--%>
            
        </div>
        
    </div>
</div>

<div class="horizontalSpacer"></div>

<%-- div id="mapMessage" style="display: none; position: absolute; top: -390px; left: 0px; z-index: 300;">
    <div id="mapMessageText" style="margin: 0pt 100px; text-align: center; color: rgb(255, 255, 255); border: 2px solid rgb(255, 255, 255); background-color: rgb(58, 142, 27); padding: 2px; opacity: 0.85; width: 300px;">
        Jericoacoara, Brasil:<br>"J� estive l�" status removido
    </div>
</div--%>

<script>
$(document).ready(function () {
    
	$('#mapaViagensForm').unbind('submit');

	$(document).on('click', '.local-item.selectable', function (e) {
        $('.local-item').removeClass('local-item-active');
        $(this).addClass('local-item-active');
        mostrarInfoWindowLocal($(this).attr('data-local-id'));
    });
    
	$(document).on('click', '.marker-check', function (e) {
	    var $check = $(this);
	    var idLocal = $check.attr('data-local-id'); 
	    var idIteresse = $check.attr('data-interesse-id'); 
	    var tipoInteresse = $check.attr('data-tipo-interesse');
	    
	    var $marker = $check.next('i.map-marker');
	    if ($marker.hasClass('inactive')) {
	        $marker.removeClass('inactive');
	        configurarCidade(idIteresse, idLocal, $check, tipoInteresse)
	    } else {
	        $marker.addClass('inactive');
	        configurarCidade(idIteresse, idLocal, $check, tipoInteresse)
	    }
	});

    initializeMap();
	
});

function adicionarCidade() {
    var idCidade = $('#novaCidade_hidden').val();
	// Verificar se a cidade j� est� no mapa
	if (mapManager.getLocais()[idCidade] != null) {
        $('#novaCidade').val(null);
        $('#novaCidade_hidden').val(null);
        mostrarInfoWindowLocal(idCidade);
        //placeMarkers(data.records, true);
	} else if (idCidade != null && idCidade != '') {
    	$.ajax({
            type: 'POST',
            url: document.forms['mapaViagensForm'].action,
            data: {
                local : idCidade,
                usuario : $('input[name="usuario"]').val(),
                jaFoi : 'true',
                jsonFields: $.toJSON([
                    {name: 'id', type: 'int'},
                    {name: 'usuario.id', type: 'int'},
                    {name: 'local.id', type: 'int'},
                    {name: 'local.nome', type: 'string'},
                    {name: 'local.nomeComSiglaEstado', type: 'string'},
                    {name: 'local.nomeComSiglaEstadoNomePais', type: 'string'},
                    {name: 'local.latitude', type: 'string'},
                    {name: 'local.longitude', type: 'string'},
                    {name: 'local.urlPath', type: 'string'},
                    {name: 'local.paisUrlPath', type: 'string'},
                    {name: 'jaFoi', type: 'string'},
                    {name: 'desejaIr', type: 'string'},
                    {name: 'morou', type: 'string'},
                    {name: 'favorito', type: 'string'},
                    {name: 'indica', type: 'string'}
                ])
            },
            success: function(data) {
                $('#novaCidade').val(null);
                $('#novaCidade_hidden').val(null);
                placeMarkers(data.records, true);
                // refazer a contagem de locais
                recontarLocais(null, data.records[0]);
            }
        });
	}
}
</script>