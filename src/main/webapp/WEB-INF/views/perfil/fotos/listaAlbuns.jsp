<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <c:url var="urlFotosSemAlbum" value="/perfil/${perfil.urlPath}/fotos/detalharFotosSemAlbum" />

    <c:if test="${not empty albuns}">
    
        <ul class="thumbnails">
        
          <c:forEach items="${albuns}" var="album">
          
            <c:choose>
              <c:when test="${permitirSelecaoFotos}">
                <c:url var="urlAlbum" value="/albuns/${album.urlPath}/listarFotos${permitirSelecaoFotos ? '?selecionar=true' : ''}" />
              </c:when>
              <c:otherwise>
                <c:url var="urlAlbum" value="/perfil/${perfil.urlPath}/albuns/${album.urlPath}/${item.urlOriginal}" />
              </c:otherwise>
            </c:choose>
          
            <li style="min-height: 200px;">
                <a href="${urlAlbum}" title="" class="box-album" data-album-id="${album.id}">
                    <c:choose>
                        <c:when test="${not empty album.fotoPrincipal}">
                            <%-- <img src="${album.fotoPrincipal.urlAlbum}" class="thumbnail" width="128" height="128"> --%>
                            
                            <i class="carrossel-thumb borda-arredondada thumbnail"
                               style="background-image: url('<c:url value="${album.fotoPrincipal.urlAlbum}"/>'); height: 175px; width: 175px;"></i>
                        </c:when>
                        <c:otherwise>
                          <div class="thumbnail" style="width: 175px; min-height: 175px;">
                            <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="128" height="128" style="margin-top: 18px;">
                          </div>
                        </c:otherwise>
                    </c:choose>
                </a>
                <div style="margin-top: 3px; max-width: 150px;">
                    <a href="${urlAlbum}" class="box-album" album-id="${album.id}">
                        <span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; word-wrap: break-word; max-width: 100%;">
                            <strong>${album.titulo}</strong>
                        </span>
                    </a>
                    <br/>
                    <span class="badge">${album.quantidadeFotos}</span> 
                    <span class="muted">${album.quantidadeFotos == 1 ? 'foto ' : 'fotos' }</span>
                </div> 
            </li>
            
          </c:forEach>
          
          <c:if test="${quantidadeFotosSemAlbum != 0}">
            <li style="min-height: 200px;">
                 <a href="${urlFotosSemAlbum}">
                   <div class="thumbnail" style="width: 175px; min-height: 175px;">
                     <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="128" height="128" style="margin-top: 18px;">
                   </div>
                 </a>
                 <div style="margin-top: 3px; max-width: 150px;">
                    <a href="${urlFotosSemAlbum}">
                        <span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; word-wrap: break-word; max-width: 100%;">
                            <strong>Fotos sem �lbuns</strong>
                        </span>
                    </a>
                    <br/>
                    <span class="badge">${quantidadeFotosSemAlbum}</span> 
                    <span class="muted">${quantidadeFotosSemAlbum == 1 ? 'foto ' : 'fotos' }</span>
                 </div> 
            </li>
          </c:if>
        
        </ul>
        
    </c:if>
    
    <c:if test="${empty albuns and quantidadeFotosSemAlbum != 0}">
    
        <ul class="thumbnails">
            <li>
                 <a href="${urlFotosSemAlbum}">
                   <div class="thumbnail" style="width: 175px;">
                     <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="128" height="128">
                   </div>
                 </a>
                 <div style="margin-top: 3px; max-width: 150px;">
                    <a href="${urlFotosSemAlbum}">
                        <span style="display: inline-block; overflow: hidden; text-overflow: ellipsis; word-wrap: break-word; max-width: 100%;">
                            <strong>Fotos sem �lbuns</strong>
                        </span>
                    </a>
                    <br/>
                    <span class="badge">${quantidadeFotosSemAlbum}</span> 
                    <span class="muted">${quantidadeFotosSemAlbum == 1 ? 'foto ' : 'fotos' }</span>
                 </div> 
            </li>
        </ul>
        
    </c:if>
    