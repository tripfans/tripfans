<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="tituloAlbuns" value="�lbuns de ${perfil.primeiroNome}" />
<c:if test="${perfil.id == usuario.id}">
	<c:set var="tituloAlbuns" value="Meus �lbuns" />
	<c:set var="uploadUrl" value="/fotos/albuns/adicionarFoto&idAlbum=${albumId}" />
</c:if>

<div class="span16" style="margin-left: 5px;">

	<div class="page-header" style="margin-top: 5px;">
        <h3>
            Selecionar fotos
        </h3>
        
    </div>
    
    <c:if test="${empty albuns and quantidadeFotosSemAlbum == 0}">
        <div style="height: 50px;"></div>
        <div class="blank-state">
            <div class="row">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                </div>
                <div class="span8">
                    <h3>N�o h� fotos para exibir</h3>
                    <p>
                        <c:choose>
                          <c:when test="${usuario.id == perfil.id}">
                              Voc�
                          </c:when>
                          <c:otherwise>
                              Este usu�rio
                          </c:otherwise>
                        </c:choose>
                        ainda n�o adicionou fotos ao seu perfil.
                    </p>
                    <c:if test="${usuario.id == perfil.id}">
                      <p>
                        <fan:addPhotosButton modalId="modal-photo" buttonId="btn-add-photos" id="add-photos" uploadUrl="${uploadUrl}" onlyUpload="true" />
                      </p>
                    </c:if>
                </div>
            </div>
        </div>
    </c:if>
    
    <div id="fotos-content">
        <c:import url="listaAlbuns.jsp"></c:import>
    </div>
    
</div>

<script>
    $('a.fancybox').fancybox({
        hideOnContentClick: false,
        hideOnOverlayClick: false,
        centerOnScroll: true,
        onComplete   : function() {
            $.fancybox.center();
        },
        onClosed: function() {
        	parent.location = '<c:url value="/perfil/${usuario.urlPath}/fotos" />';
        }
    });
    
    $(document).ready(function() {
        $('.box-album').click(function(e) {
            event.preventDefault();
            var idAlbum = $(this).data('album-id')
            var url = '<c:url value="/fotos/albuns/' + idAlbum + '/selecionarFotos?atividade=${atividade.id}" />'
            $('#fotos-content').load(url);
        });
    });
    
</script>
