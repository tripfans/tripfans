<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="tituloAlbuns" value="�lbuns de ${perfil.primeiroNome}" />
<c:if test="${perfil.id == usuario.id}">
	<c:set var="tituloAlbuns" value="Meus �lbuns" />
	<c:set var="uploadUrl" value="/fotos/albuns/adicionarFoto&idAlbum=${albumId}" />
</c:if>

<div class="span16">

	<div class="page-header">
        <h3>
            ${tituloAlbuns}
            
            <c:if test="${usuario.id == perfil.id}">
        
                <div class="right" style="vertical-align: top;">
                    <div class="btn-toolbar" style="margin-top:1px; margin-bottom: 9px">
                    	<c:choose>
							<c:when test="${usuario.conectadoFacebook}">
			                 	<c:url var="urlImportarAlbum" value="/fotos/albuns/listar/facebook" />
			                </c:when>
			                <c:otherwise>
			                	<c:url var="urlImportarAlbum" value="#" />
			                	<form name="fb_signin_connect" action="<c:url value="/connect/facebook"/>" method="POST" class="hidden">
			                         <input type="hidden" name="scope" value="email,user_status,user_hometown,user_checkins,user_location,user_photos,publish_stream,friends_hometown,friends_location,friends_photos" />
			                         <input type="hidden" name="redirectUrl" value="${tripFansEnviroment.serverUrl}/fotos/albuns/listar/facebook" />
			                         <input type="hidden" name="redirectUrlOnError" value="${tripFansEnviroment.serverUrl}/perfil/${usuario.urlPath}/fotos" />
			                    </form>
			                    <c:set var="onclick" value="onclick=\"document.forms['fb_signin_connect'].submit()\"" />
			                </c:otherwise>
						</c:choose>
                    
                    
                        <div class="btn-group">
                            <a class="btn action-button dropdown-toggle" data-toggle="dropdown" href="#">
                                Importar
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu clearfix" style="font-size: 13px;">
                                <li>
                                    <a href="${urlImportarAlbum}" ${onclick}>
                                        Facebook
                                    </a> 
                                </li>
                            </ul>
                        </div>
                        <div class="btn-group">
                            <a id="${buttonId}" href="#" class="btn dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-plus"></i>
                                Adicionar fotos
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu clearfix" style="font-size: 13px;">
                                <li>
                                    <a href="<c:url value="/fotos/upload?url=${uploadUrl}&novoAlbum=true"/>" class="fancybox">
                                        <i class="icon-book"></i>
                                        <i>Criar um novo album</i>
                                    </a> 
                                </li>
                                <c:forEach items="${albuns}" var="album">
                                    <li>
                                        <a href="<c:url value="/fotos/upload?idAlbum=${album.id}"/>" class="fancybox" 
                                           style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap; max-width: 100%;">
                                            ${album.titulo}
                                        </a> 
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            
            </c:if>
        </h3>
        
    </div>
    
    <c:if test="${empty albuns and quantidadeFotosSemAlbum == 0}">
        <div style="height: 50px;"></div>
        <div class="blank-state">
            <div class="row">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                </div>
                <div class="span8">
                    <h3>N�o h� fotos para exibir</h3>
                    <p>
                        <c:choose>
                          <c:when test="${usuario.id == perfil.id}">
                              Voc�
                          </c:when>
                          <c:otherwise>
                              Este usu�rio
                          </c:otherwise>
                        </c:choose>
                        ainda n�o adicionou fotos ao seu perfil.
                    </p>
                    <c:if test="${usuario.id == perfil.id}">
                      <p>
                        <fan:addPhotosButton modalId="modal-photo" buttonId="btn-add-photos" id="add-photos" uploadUrl="${uploadUrl}" onlyUpload="true" />
                      </p>
                    </c:if>
                </div>
            </div>
        </div>
    </c:if>

    <c:import url="listaAlbuns.jsp"></c:import>
    
</div>

<script>
    $('a.fancybox').fancybox({
        hideOnContentClick: false,
        hideOnOverlayClick: false,
        centerOnScroll: true,
        onComplete   : function() {
            $.fancybox.center();
        },
        onClosed: function() {
        	parent.location = '<c:url value="/perfil/${usuario.urlPath}/fotos" />';
        }
    });
</script>
