<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="tituloFotos" value="Fotos de ${perfil.primeiroNome}" />
<c:if test="${perfil.id == usuario.id}">
	<c:set var="tituloFotos" value="Minhas Fotos" />
</c:if>

<c:if test="${empty fotos}">
    <div style="height: 50px;"></div>
    <div class="blank-state">
        <div class="row">
            <div class="span1 offset1">
                <img src="<c:url value="/resources/images/no_photo.png" />" />
            </div>
            <div class="span3 offset1">
                <h3>N�o h� fotos para exibir</h3>
                <p>
                    Este usu�rio ainda n�o adicionou fotos ao seu perfil.
                </p>
            </div>
        </div>
    </div>
</c:if>

<c:if test="${not empty fotos}">
    <div>
    
    	<div class="page-header">
            <h3>${tituloFotos}</h3>
        </div>
        
        <ul class="thumbnails">
        
        <c:forEach items="${fotos}" var="item">
        
            <li>
                <a href="${item.urlOriginal}" class="fancybox" rel="slideShow" title="">
                    <img src="${item.urlCarrosel}" class="thumbnail" width="134" height="94">
                </a>
            </li>
            
        </c:forEach>
        
        </ul>
    </div>
    
    <script>
        $('a[rel="slideShow"]').fancybox({
            transitionIn      : 'elastic',
            transitionOut     : 'elastic',
            type              : 'image',
            speedIn: 400, 
            speedOut: 200, 
            overlayShow: true,
            overlayOpacity: 0.1,
            hideOnContentClick: true,
            'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
            },
            titlePosition: 'inside'
        });
    </script>
    
</c:if>