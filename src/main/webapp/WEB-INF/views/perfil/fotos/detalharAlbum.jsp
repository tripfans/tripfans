<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="body">

        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
            <div>
        
            <c:url value="/fotos/album/salvar" var="salvar_url" />
            <form:form id="albunsForm" action="${salvar_url}" modelAttribute="album" cssStyle="margin: 0;">
                <input type="hidden" name="album" value="${album.id}">

                <c:choose>
                    <c:when test="${usuario.id == album.autor.id}">
                    	<div class="page-header">
                            <h3>Editar album de fotos</h3>
                        </div>
                        
                        <div class="subnav" style="padding-left: 0px; z-index: 500; z-index: 500; width: 100%;">
                            <ul class="nav nav-pills" >
                                <li style="padding-left: 15px; margin-top: 9px;">
                                    T�tulo do album
                                </li>
                                <li style="padding-left: 15px; margin-top: 4px;">
                                    <input id="titulo-album" name="titulo" value="${album.titulo}" class="span10" style="font-size: 16px; font-weight: bold;" placeholder="Adicionar uma t�tulo" />
                                </li>
                                <li style="padding-left: 15px;">
                                    <div class="pull-right">
                                        <div class="btn-toolbar" style="margin-top: 4px;">
                                            <div class="btn-group">
                                                <fan:addPhotosButton modalId="modal-photo" buttonId="btn-add-photos" id="add-photos" 
                                                                     uploadUrl="/fotos/albuns/adicionarFoto"  onlyUpload="true"
                                                                     reloadUrl="/perfil/${usuario.urlPath}/albuns/${album.urlPath}" albumId="${album.id}" />
                                                <a class="btn action-button btn-remover-album" href="#">
                                                    <i class="icon-trash"></i>
                                                    Excluir
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li style="padding-left: 15px;">
                                    <div class="btn-toolbar" style="margin-top: 4px;">
                                        <div class="btn-group">
                                            <a id="btn-concluido" class="btn btn-primary" href="<c:url value="/perfil/${usuario.urlPath}/fotos"/>">
                                                <i class="icon-ok icon-white"></i>
                                                Conclu�do
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                        <div>
                        &nbsp;
                        </div>
                        
                        <div>
                            <textarea id="descricao-album" name="descricao" class="span15" rows="5" placeholder="Adicionar uma descri��o para o album">${album.descricao}</textarea>
                        </div>
                        
                    </c:when>
                    <c:otherwise>
                        <div class="page-header">
                            <h3 style="display: inline;">
                                �lbum de fotos
                            </h3>
                            
                            <a href="<c:url value="/perfil/${album.autor.urlPath}/fotos"/>" class="btn btn-primary">
                                <i class="icon-book icon-white"></i>
                                Voltar para �lbuns
                            </a>
                            
                            <div class="span4 pull-right">
                                <fan:userAvatar user="${album.autor}" displayName="false" width="30" height="30" showFrame="false" />
                                <h5>
                                    Por <strong><a href="<c:url value="/perfil/${v.urlPath}" />">${album.autor.displayName}</a></strong>
                                    <p>
                                      <c:if test="${not empty album.dataPublicacao}">
                                        <small>Em <joda:format value="${album.dataPublicacao}" pattern="dd 'de' MMMM 'de' yyyy" /></small>
                                      </c:if>
                                    </p>
                                </h5>
                            </div>                        
                            
                        </div>
                        
                        <h2 class="blue">${album.titulo}</h2>
                        
                        <joda:format value="${diario.dataPublicacao}" pattern="dd 'de' MMMM 'de' yyyy" />
                        
                        <span>${album.descricao}</span>
                        
                    </c:otherwise>
                </c:choose>
        
            </form:form>
            
            <c:set var="fotos" value="${album.fotos}"></c:set>
            
            <div class="row" style="padding-top: 15px;">
            
              <%--@include file="../../fotos/listarFotos.jsp" --%>
              <c:choose>
                <c:when test="${usuario.id == album.autor.id}">
                    <%@include file="../../fotos/adicionarFotos.jsp" %>
                </c:when>
                <c:otherwise>
                    <%@include file="../../fotos/listarFotos.jsp" %>
                </c:otherwise>
              </c:choose>
            
            </div>
            
            <div id="modal-remover-album" class="modal hide fade">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Excluir album de fotos</h3>
                </div>
                <div class="modal-body">
                    <p>Deseja realmente excluir este album?</p>
                    <p><span class="red"><strong>ATEN��O:</strong></span> Todas as fotos e coment�rios ser�o perdidos!</p>
                </div>
                <div class="modal-footer">
                    <a id="confirmar-remover-album" href="<c:url value="/fotos/albuns/excluir/${album.id}" />" class="btn btn-primary">
                        <i class="icon-ok icon-white"></i> 
                        Sim
                    </a>
                    <a href="#" class="btn" data-dismiss="modal">
                        <i class="icon-remove"></i> 
                        N�o
                    </a>
                </div>
            </div>
            
            <div id="modal-foto" class="modal hide fade" role="dialog" >
                <div class="modal-header" style="border-bottom: 0px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body" style="overflow: hidden; max-height: 800px; min-width: 960px;">
                    <div id="modal-foto-body"></div>
                </div>
            </div>
            
            <div class="row"></div>
    
            <div class="page-header" style="margin-top: 4px; margin-bottom: 4px;">
                <h3>
                  <a name="comentarios"></a>
                  Coment�rios
                </h3>
            </div>
    
            <div>
                <fan:comments inputClass="span16" tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.ALBUM.name()%>" id="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.ALBUM.name()%>" idAlvo="${album.id}"/>
            </div>
                
        </div>
        
        </div>
        
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
        <script>
        
        $(document).ready(function () {
            /*$('a.fancybox').fancybox({
                //transitionIn      : 'elastic',
                //transitionOut     : 'elastic',
                type              : 'ajax',
                width             : '85%',
                height            : '85%',
                speedIn: 400, 
                speedOut: 200, 
                overlayShow: true,
                centerOnScroll: true,
                //showCloseButton: false,
                hideOnContentClick: false,
                /*'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                    return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                },*/
                /*titlePosition: 'inside',
                onComplete   : function() {
                    //$.fancybox.center();
                    //$('#fancybox-outer').css('width', '60%');
                    //$('#fancybox-frame').attr('scrolling', 'no');
                }
            });*/
            
            $('a.fancybox').click(function(e) {
                e.preventDefault();
                var idFoto = $(this).data('id-foto');
                // pegar a url da foto
                var url = $('#box-foto-link-' + idFoto).attr('href');
                $('#modal-foto-body').load(url, function() {
                    $('#modal-foto').modal().css({
                        /*'width': function () { 
                             return ($(document).width() * .9) + 'px';  
                        },
                        'margin-left': function () { 
                            return -($(this).width() / 2); 
                        }*/
                        width: 'auto',
                        'margin-left': function () {
                            return -($(this).width() / 2);
                        }
                    });                 
                    $('#modal-foto').modal('show');
                	
                });
            });
            
            $('#btn-concluido').click(function(e) {
            	 e.preventDefault();
            	 var $botao = $(this);
            	 var $form = $(this).closest('form');
            	 $.ajax({
                     type: 'POST',
                     url: $form.attr('action'),
                     data: $form.serialize(),
                     success: function(data, status, xhr) {
                         if (data.success) {
                             document.location.href = $botao.attr('href');
                         }
                     }
                 });
            });
            
            $('.btn-remover-album').click(function(e) {
                e.preventDefault();
                $('#confirmar-remover-album').attr('data-album-id', $(this).attr('data-album-id'));
                $('#modal-remover-album').modal('show');
            });
            
            $('#titulo-album, #descricao-album').change(function() {
                var $form = $(this).closest('form');
                
                // Apos a alteracao de algum campo, o form ser� submitido ap�s 0,5 seg
                setTimeout(function() {
                    $.ajax({
                        type: 'POST',
                        url: $form.attr('action'),
                        data: $form.serialize(),
                        success: function(data, status, xhr) {
                            if (data.success) {
                                //carregarPagina('${param.pageToLoad}');
                            }
                        }
                    });
                }, 500);
            });
            
            // fix sub nav on scroll
            var $win = $(window)
                , $nav = $('.subnav')
                , navTop = $('.subnav').length && $('.subnav').offset().top - 40
                , isFixed = 0;
                
            processScroll();
            
            // hack sad times - holdover until rewrite for 2.1
            $nav.on('click', function () {
                if (!isFixed) setTimeout(function () { $win.scrollTop($win.scrollTop() - 47) }, 10)
            });
            
            $win.on('scroll', processScroll);
            
            function processScroll() {
                var i, scrollTop = $win.scrollTop();
                if (scrollTop >= navTop && !isFixed) {
                    isFixed = 1
                    $nav.addClass('subnav-fixed');
                } else if (scrollTop <= navTop && isFixed) {
                    isFixed = 0;
                    $nav.removeClass('subnav-fixed');
                }
            }
            
            $('.sortable').sortable({
                //revert: true
            });
            
            $('.sortable').disableSelection();

        });
            
        </script>    
    </tiles:putAttribute>    
    
</tiles:insertDefinition>