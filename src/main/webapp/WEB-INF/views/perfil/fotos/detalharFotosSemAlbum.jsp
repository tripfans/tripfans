<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="body">

        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
            <div>
            
                <div class="page-header">
                    <h3 style="display: inline;">
                        Outras fotos
                    </h3>
                    
                    <a href="<c:url value="/perfil/${perfil.urlPath}/fotos"/>" class="btn btn-primary">
                        <i class="icon-book icon-white"></i>
                        Voltar para Álbuns
                    </a>
                    
                    <div class="span4 pull-right">
                        <fan:userAvatar user="${perfil}" displayName="false" width="30" height="30" showFrame="false" />
                        <h5>
                            Por <strong><a href="<c:url value="/perfil/${perfil.urlPath}" />">${perfil.displayName}</a></strong>
                        </h5>
                    </div>                        
                    
                </div>                
        
                <div class="row" style="padding-top: 15px;">
                
                  <%@include file="../../fotos/listarFotos.jsp" %>
                  
                </div>
                
                <div id="modal-foto" class="modal hide fade" tabindex="-1" role="dialog">
                    <div class="modal-header" style="border-bottom: 0px;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body" style="overflow: hidden; max-height: 800px; min-width: 960px;">
                        <div id="modal-foto-body">
                            <div class=""></div>
                        </div>
                    </div>
                </div>
                
                <div class="row"></div>
<%--         
                <div class="page-header" style="margin-top: 4px; margin-bottom: 4px;">
                    <h3>Comentários</h3>
                </div>
        
                <div>
                    <fan:comments inputClass="span18" tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.ALBUM.name()%>" id="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.ALBUM.name()%>" idAlvo="${album.id}"/>
                </div>
                    
 --%>
            </div>
        
        </div>
        
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
        <script>
        
        $(document).ready(function () {
            /*$('a.fancybox').fancybox({
                //transitionIn      : 'elastic',
                //transitionOut     : 'elastic',
                type              : 'ajax',
                width             : '85%',
                height            : '85%',
                speedIn: 400, 
                speedOut: 200, 
                overlayShow: true,
                centerOnScroll: true,
                //showCloseButton: false,
                hideOnContentClick: false,
                /*'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                    return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                },*/
                /*titlePosition: 'inside',
                onComplete   : function() {
                    //$.fancybox.center();
                    //$('#fancybox-outer').css('width', '60%');
                    //$('#fancybox-frame').attr('scrolling', 'no');
                }
            });*/
            
            $('a.fancybox').click(function(e) {
                e.preventDefault();
                var idFoto = $(this).data('id-foto');
                // pegar a url da foto
                var url = $('#box-foto-link-' + idFoto).attr('href');
                $('#modal-foto-body').load(url, function() {
                	$('#modal-foto').modal().css({
                        /*'width': function () { 
                             return ($(document).width() * .9) + 'px';  
                        },
                        'margin-left': function () { 
                            return -($(this).width() / 2); 
                        }*/
                        width: 'auto',
                        'margin-left': function () {
                            return -($(this).width() / 2);
                        }
                    });                	
                	$('#modal-foto').modal('show');
                });
            
            $('#feedback-modal').modal({
                backdrop: true,
                keyboard: true
            }).css({
               'width': function () { 
                   return ($(document).width() * .9) + 'px';  
               },
               'margin-left': function () { 
                   return -($(this).width() / 2); 
               }
        });
            });
            
            $('.sortable').sortable({
                //revert: true
            });
            
            $('.sortable').disableSelection();

        });
            
        </script>    
    </tiles:putAttribute>    
    
</tiles:insertDefinition>