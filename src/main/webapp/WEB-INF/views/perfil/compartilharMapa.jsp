<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${usuario.id == perfil.id and perfil.possuiInteressesEmCidades}">
  <div class="row" align="center" style="margin-top: 6px;">
    <c:set var="markers" value="" />
    <c:set var="markersJaFoi" value="&markers=icon:http://www.tripfans.com.br/resources/images/markers/est-pin.png" />
    <c:set var="markersDesejaIr" value="&markers=icon:http://www.tripfans.com.br/resources/images/markers/des-pin.png" />
    <c:forEach var="interesseUsuario" items="${interessesUsuarioEmCidades}" begin="1" end="10" step="1">
      <c:set var="latLng" value="${interesseUsuario.cidade.latitude},${interesseUsuario.cidade.longitude}" />
      <c:if test="${interesseUsuario.desejaIr}">
          <c:set var="color" value="blue" />
          <c:set var="icon" value="blue" />
          <c:set var="markersDesejaIr" value="${markersDesejaIr}%7C${latLng}" />
      </c:if>
      <c:if test="${interesseUsuario.jaFoi}">
          <c:set var="color" value="green" />
          <c:set var="icon" value="blue" />
          <c:set var="markersJaFoi" value="${markersJaFoi}%7C${latLng}" />
      </c:if>
      <c:set var="markers" value="${markers}&markers=color:${color}%7C${latLng}" />
    </c:forEach>
    
    <!--http://www.tripfans.com.br/resources/images/markers/est-pin.png
    ${tripFansEnviroment.serverUrl}-->
    
    <fan:shareButton facebook="true" pictureUrl="${tripFansEnviroment.serverUrl}/staticmap?size=400x260&center=-15.800513,-55.91378${markersJaFoi}${markersDesejaIr}&sensor=false" url="${tripFansEnviroment.serverUrl}/perfil/${perfil.urlPath}/mapa" usarBotaoEspecial="false" 
                     titulo="${perfil.primeiroNome} compartilhou o Mapa de Viagens dele no TripFans" subTitulo="www.tripfans.com.br" texto="${perfil.primeiroNome} atualizou seu mapa de viagens no TripFans." 
                     labelBotaoFacebook="Compartilhar Mapa no Facebook" hint="Compartilhe o seu mapa de viagens com seus amigos no Facebook" />
                     
  </div>
</c:if>