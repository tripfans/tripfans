<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="${perfilFB.nome} - TripFans" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">

    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">

        <style>
            .sidebar-box {
                border-width: 1px;
            }        
        </style>
        
        <div class="container-fluid">
        	<div class="block withsidebar">
        	
        		<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
                    <h2>${perfilFB.nome}</h2>
					<div class="pull-right">
                        
					</div>
                </div>
                
                <div class="block_content">
		            <div class="sidebar">
	                    <%-- Foto do usuario --%>
                        <div class="fotoPerfil centerAligned">
                            <img id="profile_pic" class="moldura profile" src="http://graph.facebook.com/${perfilFB.id}/picture?type=large"  />
                        </div>
		            </div>
		            
		            <div class="sidebar_content lead" id="conteudo" style="padding-left: 0px; padding-top: 7px; margin-left: 0px;">
                    
                    <c:choose>
	                    <c:when test="${tripFansEnviroment.enableSocialNetworkActions}">
	                    
	                        <h2>Convide ${perfilFB.nome} para entrar no TripFans</h2>
	                        
	                        <br/>
	
	                        <strong>${perfilFB.nome}</strong> ainda n�o est� no TripFans, mas se voce convid�-la voc�s poder�o compartilhar dicas, recomenda��es, fotos, planos de viagem e muito mais.
	                        
	                        <br/>
	                        <br/>
	                        
	                        <a class="btn btn-large btn-primary post-to-feed"
	                        				data-from="${perfil.idFacebook}" data-to="${perfilFB.id}" 
                                            data-name="Junte-se ao TripFans - A sua rede social de viagens" 
                                            data-caption="O TripFans � a sua rede social de viagens, feita com a ajuda de seus amigos!" 
                                            data-description="Por meio das recomenda��es de seus amigos e dos interesses que voc� possui, o TripFans proporcionar� uma experi�ncia �nica em suas viagens, atrav�s de dicas, avalia��es, respostas as suas perguntas, ferramenta para a elabora��o de planos de viagens, recomenda��es de locais e promo��es." 
                                            data-link="${tripFansEnviroment.serverUrl}/entrar?ucv=${idUsuarioCriptografado}" 
                                            data-picture="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans-vertical.png">
	                        Convidar para o TripFans</a>
	                        
	                    </c:when>
	                    <c:otherwise>
	                    	<strong>${perfilFB.nome}</strong> ainda n�o est� no TripFans.
	                    </c:otherwise>
                    </c:choose>
                        
                        
		            </div>
                </div> <!-- block_content -->
                
                <div class="bendl">
                </div>
                <div class="bendr">
                </div>
        	
        	</div> <!-- block withsidebar -->
            
        </div>
    <div id="fb-root"></div>
    
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        
        $(document).ready(function(){
        	$('.post-to-feed').click(function (e) {
                e.preventDefault();

                //var idFacebook = $(this).attr('data-user-facebook-id');
                facebookPost($(this), 'feed');
            });
        });
        
	</script>
	
	<jsp:include page="/WEB-INF/includes/facebookJSFuncs.jsp" />
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>