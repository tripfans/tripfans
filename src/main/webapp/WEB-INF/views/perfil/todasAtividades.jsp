<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div class="page-header">
        <h3>Atividades</h3>
    </div>
    
    <div id="listaAtividades">
        <jsp:include page="listaAtividades.jsp" >
        	<jsp:param value="span16" name="cssSpan"/>
        </jsp:include>
    </div>
    
</div>