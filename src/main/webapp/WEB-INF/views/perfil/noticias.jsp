<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
	<div class="page-header">
	    <h3>Not�cias</h3>
	</div>
	  
	<c:choose>
		<c:when test="${not empty noticias}">
			<c:forEach var="noticia" items="${noticias}" >
			    <div class="row">
			        <fan:noticia noticia="${noticia}"/>
			    </div>
			</c:forEach>
		</c:when>
		<c:otherwise>N�o existem not�cias</c:otherwise>
	</c:choose>  
</div>