<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
	<div class="page-header">
	    <h3>Notifica��es</h3>
	</div>
	
	<c:choose>
	  <c:when test="${not empty notificacoes}">
	    <div id="listaNotificacoes" class="span15">
	      <jsp:include page="listaNotificacoes.jsp" />
	    </div>
	  </c:when>
	<c:otherwise>
	  <div class="blank-state">
	    <div style="height: 50px;"></div>
		<div class="row">
	        <div class="span2 offset1">
	            <img src="<c:url value="/resources/images/icons/mini/128/Communication-73.png" />" width="90" height="90" />
	        </div>
	        <div class="span6">
	            <h3>Sem notifica��es</h3>
	            <p>
	                No momento n�o h� nenhuma notifica��o.
	            </p>
	        </div>
	    </div>
	  </div>
	</c:otherwise>
	</c:choose>
</div>