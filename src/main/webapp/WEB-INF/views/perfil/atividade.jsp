<%@page import="br.com.fanaticosporviagens.model.entity.TipoAcao"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div style="padding-top: 6px; padding-bottom: 6px;">
    <c:if test="${showUserPhoto == true}">
        <fan:userAvatar user="${atividade.autor}" enableLink="false" displayName="false" displayDetails="${param.displayDetails}" marginLeft="10" imgSize="avatar"/>
    </c:if>
    <div class="span7" style="margin-left: 4px;">
        <div style="float:left; margin-right: 4px;">
            <img src="<c:url value="/resources/images/icons/mini/32/${atividade.tipoAcao.icone}" />" />
        </div>
        <div>
            <c:set var="autorAtividade" value="${atividade.autor}" />
            <c:set var="destinatarioAtividade" value="${atividade.destinatario}" />
            <c:set var="tipoAtividade" value="${atividade.tipoAcao}" />
            <c:set var="alvoAtividade" value="${atividade.alvo}" />
            <c:set var="codigo" value="${tipoAtividade.codigo}" />
            
            <c:choose>
              <c:when test="${codigo == 1}">
                <c:set var="autorAlvoAtividade" value="${atividade.acao.autor}" />
              </c:when>
              <c:otherwise>
                <c:if test="${tipoAtividade.classeDoAlvo.simpleName != 'Local' and not empty atividade.alvo.autor}">
                  <c:set var="autorAlvoAtividade" value="${alvoAtividade.autor}" />
                </c:if>
              </c:otherwise>
            </c:choose>
            
            <b style="vertical-align: top;">
                ${autorAtividade.primeiroNome}
            </b>
            <span style="vertical-align: top;">
                ${tipoAtividade.descricaoAcaoNoPassado}
                
                <%-- TODO melhorar isso --%>
                <c:if test="${tipoAtividade.codigo == 1 or tipoAtividade.codigo == 3 or tipoAtividade.codigo == 28}">
                    <b>${destinatarioAtividade.displayName}</b>
                </c:if> 
                
                ${atividade.quantidade != null ? atividade.quantidade : ''}
                ${tipoAtividade.preposicaoPosAcao}
                ${tipoAtividade.descricaoAlvo}
                
                <c:if test="${tipoAtividade != null}">
                  <c:if test="${autorAlvoAtividade.id == perfil.id}">
                      
                  </c:if>
                  <c:if test="${autorAlvoAtividade.id != null and autorAlvoAtividade.id != perfil.id}">
                    <c:if test="${not (tipoAtividade.codigo == 1 or tipoAtividade.codigo == 3 or tipoAtividade.codigo == 28)}">
                      de ${autorAlvoAtividade.displayName}
                    </c:if>
                  </c:if>
                  
                  <c:if test="${tipoAtividade.preposicaoPosAlvo != ''}">
                    ${tipoAtividade.preposicaoPosAlvo}
                    ${alvoAtividade.descricaoAlvo}
                  </c:if>
                  <c:if test="${codigo == 41 or codigo == 42 or codigo == 43 or codigo == 44 or codigo == 45 or codigo == 46}">
                    <c:choose>
                      <c:when test="${not empty atividade.localEnderecavel}">
                        ${atividade.localEnderecavel.nome}
                      </c:when>
                      <c:otherwise>
                        ${atividade.localCidade.nome}
                      </c:otherwise>
                    </c:choose>
                  </c:if>
                </c:if>
<%--                 
                <c:choose>
                    <c:when test="${atividade.autor.id == atividade.destinatario.id}">
                    </c:when>
                    <c:otherwise>
                        ${tipoAtividade.preposicaoPosAlvo}
                        ${alvoAtividade.descricaoAlvo}
                    </c:otherwise>
                </c:choose>
                
 --%>                <br/>
                <span style="font-size: 11px; color: #999;">
                    <fan:passedTime date="${atividade.data}"/>
                </span>
            </span>
        </div>
    </div>
    <br/>
    
</div>
<c:if test="${param.showSeparator}">
    <hr style="margin:0px; border-bottom: 0px; "/>
</c:if>
