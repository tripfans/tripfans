<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteressesViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="tf" uri="/WEB-INF/tripfansFunctions.tld" %>

<c:set var="titulo" value="Interesses de Viagem de ${perfil.primeiroNome}" />
<c:if test="${usuario.id == perfil.id }" >
	<c:set var="titulo" value="Meus Interesses de Viagem" />
</c:if>

<div class="span16">

    <div class="page-header">
    	<h3>
            ${titulo}
            
            <c:if test="${usuario.id == perfil.id }" >
              <div class="right" style="vertical-align: top;">
                <div class="btn-toolbar" style="margin-top:1px; margin-bottom: 9px">
                    <a class="btn btn-mini" href="<c:url value="/conta/interesses"/>">
                        <i class="icon-pencil"></i> Alterar meus interesses
                    </a>
                </div>
              </div>
            </c:if>             
        </h3>
    </div>
    
    <div>
    	
      <c:forEach items="<%=TipoInteressesViagem.values() %>"  var="tipoInteresse"  varStatus="count">
    	<c:set var="valorInteresse" value="${tf:getPropertyValue(interessesView.interessesViagem, tipoInteresse.nome)}"/>
    	<c:if test="${valorInteresse}">
    		<div class="span2" style="text-align: center; min-height: 120px;">
    			<label><i class="interesse ${tipoInteresse.nome} borda-arredondada" ></i></label>
    			<h6 class="titulo-interesse">${tipoInteresse.descricao}</h6>
    		</div>
    	</c:if>
      </c:forEach>
    
    </div>

</div>