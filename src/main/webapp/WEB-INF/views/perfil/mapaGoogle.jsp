<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="map_canvas" style="width:100%; height:100%"></div>

<script>
    // Locais do usuario
    
    /**
     * Data for the markers consisting of a name, a LatLng and a zIndex for
     * the order in which these markers should display on top of each
     * other.
     */
    var locais = [
        <c:forEach items="${interessesUsuarioEmCidades}" var="item" varStatus="count">
          <c:if test="${item.local.latitude != null && item.local.longitude != null}">
            {
                'id' : ${item.id},
                'local' : {
                    'id' : '${item.local.id}',
                    'nome' : "${item.local.nome}",
                    'nomeComSiglaEstado' : "${item.local.nomeComSiglaEstado}",
                    'nomeComSiglaEstadoNomePais' : "${item.local.nomeComSiglaEstadoNomePais}",
                    'siglaPais' : '${item.local.siglaPais}',
                    'urlPath' : '${item.local.urlPath}',
                    'paisUrlPath' : '${item.local.paisUrlPath}',
                    'latitude' : ${item.local.latitude},
                    'longitude' : ${item.local.longitude},
                    'urlFotoSmall' : '${item.local.urlFotoAvatar}',
                    'urlFotoAlbum' : '${item.local.urlFotoAlbum}'
                },
                'jaFoi' : ${item.jaFoi}, 
                'desejaIr' : ${item.desejaIr}, 
                'favorito' : ${item.favorito}, 
                'indica' : ${item.indica}
            }
            <c:if test="${not count.last}">
            ,
            </c:if>
          </c:if>
        </c:forEach>
    ];
    
    //TODO recuperar dinamicamente de acordo com a posicao do usuario
    var latitudeCentro = -15.45368;  //-22.95;
    var longitudeCentro = -52.396545; // -46.60;

    // ------- MAP - MANAGER
    function MapManager(map) {
	    this.map = map;
	    this.markers = new Array();
        this.locais = new Array();
	    
	    var helper = new google.maps.OverlayView();
        helper.setMap(map);
        helper.draw = function () { 
            if (!this.ready) { 
                this.ready = true; 
                google.maps.event.trigger(this, 'ready'); 
            } 
        }; 
        
        // Quando houver um 'click' na janela de visualiza��o
        google.maps.event.addListener(map, 'click', function(event) {

            closeInfowindow();
            
            // TODO ver se iremos utilizar ou nao esse recurso - comentado por enquanto
            /*window.setTimeout(function() {

                // Recupera as coordenadas do ponto clicado no mapa
                retangleCenter = event.latLng;
                
                // Dispon�vel apenas ap�s "draw" ser chamado.
                var projection = helper.getProjection();
    
                // Transforma as coordenadas para o objeto Point (em pixels)
                var clickPoint = projection.fromLatLngToDivPixel(retangleCenter);
                
                // Monta dois novos pontos NE/SW a uma distancia de 20 pixel para cada dire��o, 
                // formando um quadrado ao redor do ponto clicado
                var pointNE = new google.maps.Point(clickPoint.x + 20, clickPoint.y - 20);
                var pointSW = new google.maps.Point(clickPoint.x - 20, clickPoint.y + 20);
                
                // Transforma novamente para coordenadas Lat/Long para desenhar o quadrado no mapa
                var latlogNE = projection.fromDivPixelToLatLng(pointNE);
                var latlogSW = projection.fromDivPixelToLatLng(pointSW);
                
                // Redefine a configura��o do quadrado, fazendo com que ele reapare�a no local clicado
                rectangle.setOptions({
                    strokeColor: "#FF0000",
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: "#FF0000",
                    fillOpacity: 0.35,
                    map: map,
                    bounds: new google.maps.LatLngBounds(
                        latlogSW,
                        latlogNE
                    )
                });
                
                // Abrir popup contendo cidades dentro do retangulo
                consultarCidadesPerimetro(latlogNE, latlogSW, true, 4);
            }, 1000);
            */

        });
        
        <%-- Estre trecho so deve ser executado quando for o usuario dono do perfil --%>
        <c:if test="${usuario.id == perfil.id}">
        
        // Quando houver um movimento na janela de visualiza��o
        // * dragend - Quando o usu�rio parar de arrastar o mapa
        google.maps.event.addListener(map, 'dragend', function() {
            clearMap();
            // Recupera as coordenadas que representam a �rea que o mapa est� exibindo
            consultarCidadesPerimetro(map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
        });        
        // * zoom_changed - Quando o zoom � alterado
        google.maps.event.addListener(map, 'zoom_changed', function() {
            clearMap();
            // Recupera as coordenadas que representam a �rea que o mapa est� exibindo
            consultarCidadesPerimetro(map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
        });
        
        </c:if>
        
	}

    MapManager.prototype.getMap = function() {
    	return this.map;
    }

    MapManager.prototype.getMarkers = function() {
    	return this.markers;
    }

    MapManager.prototype.getLocais = function() {
    	return this.locais;
    }
    
    MapManager.prototype.checkOption = function(opcaoLocal, opcaoFiltro) {
    	if (opcaoFiltro != null) {
    	    return opcaoLocal && opcaoFiltro;
    	}
    	return opcaoLocal;
    }

    MapManager.prototype.getMarkerImage = function(localUsuario, opcoesFiltro) {
    	
        var urlImage = '<c:url value="/resources/images/markers/" />' + this.getMarkerImageName(localUsuario, opcoesFiltro);
        
        return  new google.maps.MarkerImage(urlImage,
                // This marker is 28 pixels wide by 27 pixels tall.
                new google.maps.Size(28, 27),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at 0,32.
                new google.maps.Point(9, 23));
    }
    
    MapManager.prototype.getMarkerImageName = function(localUsuario, opcoesFiltro) {
    	
        var image = '';
        
        // 'J� fui'
        if (this.checkOption(localUsuario.jaFoi, opcoesFiltro.jaFoi)) {
        	image += 'est-';
        }
        // 'Desejo ir'
        if (this.checkOption(localUsuario.desejaIr, opcoesFiltro.desejaIr)) {
        	image += 'des-';
        }
        // Favorito
        if (this.checkOption(localUsuario.favorito, opcoesFiltro.favorito)) {
            image += 'fav-';
        }
        // Indica
        if (this.checkOption(localUsuario.indica, opcoesFiltro.indica)) {
            image += 'ind-';
        }
        
        image += 'pin.png';
        
        return image;
    }

    MapManager.prototype.addMarker = function(localUsuario, showInfo) {
        var id = localUsuario.local.id;
        
        // Recupera o marcador caso j� esteja no mapa
        var marker = this.markers[id];
        var urlImage = '<c:url value="/resources/images/markers/" />' + this.getMarkerImageName(localUsuario, {});
        // O tamanho do marcador � expressado como 'Size' de X, Y
        // onde a origem da imagem (0,0) � localizada no topo esquerdo da imagem
        
        // Origins, anchor positions and coordinates of the marker
        // increase in the X direction to the right and in
        // the Y direction down.
        var image = new google.maps.MarkerImage(urlImage,
                // This marker is 27 pixels wide by 28 pixels tall.
                new google.maps.Size(27, 28),
                // The origin for this image is 0,0.
                new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at 12,24.
                new google.maps.Point(9, 23));
        
        // Se n�o existir
        if (marker == null) {
            
            var shadow = new google.maps.MarkerImage('images/beachflag_shadow.png',
                // The shadow image is larger in the horizontal dimension
                // while the position and offset are the same as for the main image.
                new google.maps.Size(37, 32),
                new google.maps.Point(0,0),
                new google.maps.Point(0, 32));
                // Shapes define the clickable region of the icon.
                // The type defines an HTML &lt;area&gt; element 'poly' which
                // traces out a polygon as a series of X,Y points. The final
                // coordinate closes the poly by connecting to the first
                // coordinate.
                
            var shape = {
                coord: [1, 1, 1, 20, 18, 20, 18 , 1],
                type: 'poly'
            };
            
        	marker = new google.maps.Marker({
        		map : this.map,
                position: new google.maps.LatLng(localUsuario.local.latitude, localUsuario.local.longitude),
                //shadow: shadow,
                //icon: urlImage
                icon: image,
                //shape: shape,
                //title: location[0],
                //zIndex: location[3]
            });

            if (showInfo != null && showInfo == true) {
                closeInfowindow();
                infowindow = new google.maps.InfoWindow({
                    content : criarTabelaCidades([localUsuario], true)
                });
                infowindow.open(map, marker);
            }
            
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    mostrarInfoWindowLocal(id);
                }
            })(marker, id));

            this.markers[id] = marker;
            this.locais[id] = localUsuario;
        } else {

        	// Atualizar as op��es selecionadas
        	this.locais[id] = localUsuario;
        	// Se j� existir, apenas altera a imagem e exibe caso esteja oculto
        	marker.setIcon(image);
        	marker.setVisible(true);
        	
            if (showInfo != null && showInfo == true) {
                closeInfowindow();
                infowindow = new google.maps.InfoWindow({
                    content : criarTabelaCidades([localUsuario], true)
                });
                infowindow.open(map, marker);
            }
        }
        return marker;
    }
    
    MapManager.prototype.removeMarker = function(idLocal) {
        this.markers[idLocal].setVisible(false);
        //this.markers.splice(idLocal, 1);
        //this.locais.splice(idLocal, 1);
        this.locais[idLocal] = null;
    }

    MapManager.prototype.updateMarkers = function(opcoes) {
    	for (i in this.markers) {
		    if (this.markers[i] != null) {
		    	this.markers[i].setIcon(this.getMarkerImage(this.locais[i], opcoes));
		    }
    	}
    }
    
    // -------

    function getMarkerIconURL(local) {
    	// TODO retornar o icone de acordo com o local
    	//var url = '<c:url value="/resources/images/icons/marker.png" />';
    	var url = '/fanaticosporviagens/resources/images/icons/marker.png';
    	// Subsituindo '/' por '%2F'
    	url = url.split('/');
    	url = url.join('%2F');
    	return url;
    }
    
    <c:choose>
    
        <%-- Se for do tipo compacto, exibir apenas a imagem estatica do mapa --%>
        <c:when test="${param.compacto}">
        
            var paramMarkers = ''; 
            for (var i = 0; i < locais.length; i++) {
            	paramMarkers += '&markers=icon:' + getMarkerIconURL(locais[i]) + '%7C' + locais[i].local.latitude + ',' + locais[i].local.longitude;
            }
            
            var htmlMapa = '<img style="height: 200px; width: 295px;" src="http://maps.google.com/maps/api/staticmap?&size=300x200&zoom=1&sensor=false' + paramMarkers + '">';
            
            $('#map_canvas').html(htmlMapa);            		     
            		     
        </c:when>
        
        <%-- Caso contrario, exibir o mapa completo --%>
        <c:otherwise>
        
            var mapManager = null;
            var map = null;
            // Criar um retangulo para delimitar as cidades que ser�o exibidas no popup
            var rectangle = new google.maps.Rectangle();
            var rectangleCenter = null;
            var rectangleMarker = new google.maps.Marker();
            var infowindow = null;
            //var infowindowClick = null;
    
            function initializeMap() {
                
                // Ponto central do mapa
                var center = new google.maps.LatLng(latitudeCentro, longitudeCentro);
                var myOptions = {
                    zoom: 3, //1,
                    center: center,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false,
                    scrollwheel: ${param.mapaCompleto ? 'true' : 'false'},
                    /*mapTypeControl: false,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.BOTTOM_CENTER
                    },
                    /*panControl: true,
                    panControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.DEFAULT,
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    scaleControl: true,
                    scaleControlOptions: {
                        position: google.maps.ControlPosition.TOP_LEFT
                    }*/
                };
                map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
                mapManager = new MapManager(map);
                
                // Colocar os marcadores no mapa
                placeMarkers(locais);
                // Fazer a consulta das cidades que aparecer�o na lista
                
                if (map.getBounds() != null) {
                    consultarCidadesPerimetro(map.getBounds().getNorthEast(), map.getBounds().getSouthWest());
                }
            }
            
            /**
             * Recuperar as cidades que est�o dentro do perimeitro especificado pela latitude/longitude NE e SW 
             */
            function consultarCidadesPerimetro(latlongNE, latlongSW, atualizarInfowindow, registrosPorPagina) {
                closeInfowindow();
                
                // Realiza a consulta de cidades dentro do perimetro         
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/perfil/consultarLocaisPerimetro" />',
                    data: {
                        'perfil' : ${perfil.id},
                        'latitudeNE' : latlongNE.lat(),
                        'longitudeNE' : latlongNE.lng(),
                        'latitudeSW' : latlongSW.lat(),
                        'longitudeSW' : latlongSW.lng(),
                        'jsonFields' : $.toJSON([
                              {name: 'id'},
                              {name: 'usuario.id'},
                              {name: 'local.id'},
                              {name: 'local.nome'},
                              {name: 'local.nomeComSiglaEstado'},
                              {name: 'local.nomeComSiglaEstadoNomePais'},
                              {name: 'local.siglaPais'},
                              {name: 'local.urlPath'},
                              {name: 'local.paisUrlPath'},
                              {name: 'local.latitude'},
                              {name: 'local.longitude'},
                              {name: 'local.urlFotoAvatar'},
                              {name: 'local.urlFotoAlbum'},
                              {name: 'jaFoi'},
                              {name: 'desejaIr'},
                              {name: 'favorito'},
                              {name: 'indica'}
                        ]),
                        'start' : 0,
                        'limit' : (registrosPorPagina != null ? registrosPorPagina : 10)
                    },
                    success: function(data) {
                        if (atualizarInfowindow) {
                            popularInfowindow(data.records);
                        } else {
                            popularListaCidades(data.records)
                        }
                    }
                });
            }
    
            function popularInfowindow(cidadesJson) {
                if (cidadesJson != null) {
                    // Remontar a popup de cidades no mapa
                    var tabela = criarTabelaCidades(cidadesJson, true);
                    infowindow = new google.maps.InfoWindow({
                        content : tabela
                    });
                    // Remover marcador
                    rectangleMarker.setPosition(retangleCenter);
                    infowindow.open(map, rectangleMarker);
                }
            }
             
            function popularListaCidades(cidadesJson) {
                if (cidadesJson != null) {
                    var tabela = criarTabelaCidades(cidadesJson, false);
                    // Remontar a lista de cidades
                    $('#cityList').html(tabela);
                }
            }
            
            /**
             * Retira os ret�ngulos e os 'infowindows' da visualiza��o do mapa
             */
            function clearMap() {
                clearRectangle();
                closeInfowindow();
            }
             
            function clearRectangle() {
                 rectangle.setOptions({
                     map: null,
                 });
            }
    
            function closeInfowindow() {
                if (infowindow != null) {
                    infowindow.close();
                	infowindow = null;
                	//event.preventDefault();
                }
            }
            
            /**
             * Retorna uma string contendo os atributos do local separados por virgula
             */
            function getParamsLocal(local) {
            	 var json = $.toJSON(local)
            	 // Substituir por aspas simples
                 json = json.split('"');
            	 json = json.join('\'');
            	 return json;
            }
             
            function mostrarInfoWindowLocal(id) {
                closeInfowindow();
                infowindow = new google.maps.InfoWindow();
                
                <c:choose>
                  <%-- Estre trecho so deve ser executado quando for o usuario dono do perfil --%>
                  <c:when test="${usuario.id == perfil.id}">
                    infowindow.setContent(criarTabelaCidades([mapManager.getLocais()[id]]));
                  </c:when>
                  <c:otherwise>
                    infowindow.setContent(criarPopupCidade([mapManager.getLocais()[id]]));
                    selecionarCidadeNoMenu(id);
                  </c:otherwise>
                </c:choose>
                
                infowindow.open(mapManager.getMap(), mapManager.getMarkers()[id]);
            }
             
            function selecionarCidadeNoMenu(idLocal) {
                $('.local-item').removeClass('local-item-active');
                
                var $localItem = $('#menu-locais').find('li[data-local-id="' + idLocal + '"]');

                $localItem.addClass('local-item-active')
                $localItem.parent().scrollTo($('#' + $localItem.attr('id')), 800);
            }

            function criarPopupCidade(locations) {
                var urlPathPais = locations[0].local.paisUrlPath;
                var flagClass = urlPathPais != null ? 'country-flag ' + urlPathPais : '';
                var divPopup = 
                    '<div>' +
                    	'<a href="<c:url value="/locais/cidade/" />' + locations[0].local.urlPath + '" target="_blank">' + 
                    	    '<i class="' + flagClass + '"> </i>' +
                    	    '<b style="padding-left: 4px; font-size: 120%;">' +
                    	         locations[0].local.nome +
                     	    '</b>' +
                        '</a>' +
                        '<table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%;">' + 
	                        '<tbody>' +
	                        	'<tr>' +
	                        		'<td style="vertical-align: top; padding-right: 5px;">' +
    	                        		'<span>' +
    	                        		    /*'${perfil.primeiroNome} ' + 
    	                        		    (locations[0].jaFoi ? ' j� esteve ' : '') +  
    	                        		    (locations[0].desejaIr ? ' deseja ir ' : '') +  
    	                        		    (locations[0].favorito ? ' tem como favorito ' : '') +  
    	                        		    (locations[0].indica ? ' indica ' : '') +  
    	                        		    ' este local ' +*/
    	                        		    "${perfil.displayName} neste local: <br/>";
    	                        		    
    	                        		    if (locations[0].jaFoi) {
    	                        		        divPopup += '<i class="map-marker est-pin " title="J� foi"> </i>';
    	                        		    }
    	                        		    if (locations[0].desejaIr) {
    	                        		        divPopup += '<i class="map-marker des-pin " title="Deseja ir"> </i>';
    	                        		    }
    	                        		    if (locations[0].favorito) {
    	                        		        divPopup += '<i class="map-marker fav-pin " title="Favorito"> </i>';
    	                        		    }
    	                        		    if (locations[0].indica) {
    	                        		        divPopup += '<i class="map-marker ind-pin " title="Indica"> </i>';
    	                        		    }
    	                        		    
    	                        		    divPopup +=
    	                        		'</span>' +
			                        '</td>' +
	                        		'<td>' +
	                        			'<div style="float:right; margin: 0px;">' +
	                                        '<img style="width: 90px; height: 70px;" src="' + locations[0].local.urlFotoSmall + '" class="borda-foto borda-arredondada">' +
	                                    '</div>' + 
			                        '</td>' +
		                        '</tr>' +
	                        '</tbody>' +
                        '</table>' +
                    '</div>';
                return divPopup;
            }

            function criarTabelaCidades(locations, popup) {
                if (locations == null || locations.length == 0) {
                    return '<div style="max-width: 296px;">' + 
                               'N�o existem destinos a exibir pr�ximos ao local. Por favor mova para outra �rea do mapa para selecionar os locais.' +
                           '</div>'
                }
                var styleTD = (popup == true ? '' : 'border-top: 0px solid #DDD; padding: 0px; line-height: 37px;');
                var tabela = 
                    '<div>' +

                          '<form id="cidadesForm" style="margin: 0px; max-width: 300px;">' +
                            '<table class="table">' +
                              <c:if test="${usuario.id == perfil.id}">
                                '<tr style="color: #999; ">' +
                                    '<td width="45%" style="max-width: 130px;' + styleTD + '">' +
                                        
                                    '</td>' +
                                    '<td width="45%" class="center small" style="' + styleTD + '">' +
                                        // '<i class="map-marker est-pin" title="J� fui"> </i>' + //
                                        'J� fui' +
                                    '</td>' +
                                    '<td width="13%" class="center small" style="' + styleTD + '">' +
                                        //'<i class="map-marker des-pin" title="Desejo ir"> </i>' + //
                                        'Desejo ir' +
                                        '&nbsp;' + 
                                    '</td>' +
                                    /*'<td>' +
                                        'J� morei' +
                                    '</td>' +*/
                                    '<td width="13%" class="center small" style="' + styleTD + '">' +
                                        //'<i class="map-marker est-fav-pin" title="Favorito"> </i>' + //
                                        'Favorito' +
                                    '</td>' +
                                    '<td width="13%" class="center small" style="' + styleTD + '">' +
                                        //'<i class="map-marker est-ind-pin" title="Indico"> </i>' + //
                                        'Indico' +
                                    '</td>' +
                                '</tr>';
                              </c:if>
                              <c:if test="${usuario.id != perfil.id}">
                                '';
                              </c:if>
                // Varre os locais e monta a lista de cidades com as op��es para marcar
                for (i = 0; i < locations.length; i++) {
                    tabela +=
                        '<tr class="local-item ' + (popup == false ? 'selectable' : '') + '" style="background-color: #FFFFFF">' +
                            '<td class="local-item-name" style="width: 80%; padding: 12px 7px 4px 8px; ">' +
                                '<i class="country-flag ' + locations[i].local.paisUrlPath + '"> </i>' +
                                '<a href="<c:url value="/locais/cidade/" />' + locations[i].local.urlPath + ' " target="_blank" style="padding-left: 4px;">' + locations[i].local.nomeComSiglaEstado + '</a>' +
                                '<input type="hidden" name="list[' + i + '].id" value="' + locations[i].id + '">' +
                                '<input type="hidden" name="list[' + i + '].local" value="' + locations[i].local.id + '">' +
                                '<input type="hidden" name="list[' + i + '].usuario" value="' + ${perfil.id} + '">' + 
                            '</td>' +
                            '<td class="center" style="width: 13%; padding: 12px 7px 4px 8px;">' +
                                '<label style="cursor: pointer; margin-bottom: 0px;">' + 
                                    '<input type="checkbox" id="jaFoi_' + locations[i].local.id + '" name="list[' + i + '].jaFoi" value="true" ' + (locations[i].jaFoi == true ? 'checked="checked"' : '') +
                                    //'       onchange="configurarCidade(' + locations[i].id + ',' + locations[i].local.id + ', this, \'jaFoi\')"' +
                                    '       data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="jaFoi"' + 
                                    '       class="marker-check marker-check-jaFoi" style="display: none;" />' +
                                    '<i class="map-marker marker-jaFoi checkable est-pin ' + (locations[i].jaFoi == true ? '' : 'inactive') + '" title="J� foi" data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="jaFoi"> </i> ' +
                                '</label>' + 
                            '</td>' +
                            '<td class="center" style="width: 13%; padding: 12px 7px 4px 8px;">' +
	                            '<label style="cursor: pointer; margin-bottom: 0px;">' + 
                                    '<input type="checkbox" id="desejaIr_' + locations[i].local.id + '" name="list[' + i + '].desejaIr" value="true" ' + (locations[i].desejaIr == true ? 'checked="checked"' : '') +
                                    //'       onchange="configurarCidade(' + locations[i].id + ',' + locations[i].local.id + ', this, \'desejaIr\')"' +
                                    '       data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="desejaIr"' + 
                                    '       class="marker-check marker-check-desejaIr" style="display: none;" />' +
                                    '<i class="map-marker marker-desejaIr checkable des-pin ' + (locations[i].desejaIr == true ? '' : 'inactive') + '" title="Deseja ir" data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="desejaIr"> </i> ' +
                                '</label>' + 
                            '</td>' +
                            /*'<td>' +
                                '<input type="checkbox" name="list[' + i + '].morou" value="true" ' + (locations[i].morou == true ? 'checked="true"' : '') +
                                '       onchange="configurarCidade(' + locations[i].id + ',' + locations[i].local.id + ', this)" />' +
                            '</td>' +*/
                            '<td class="center" style="width: 13%; padding: 12px 7px 4px 8px;">' +
                                '<label style="cursor: pointer; margin-bottom: 0px;">' + 
                                    '<input type="checkbox" id="favorito_' + locations[i].local.id + '" name="list[' + i + '].favorito" value="true" ' + (locations[i].favorito == true ? 'checked="checked"' : '') +
                                    //'       onchange="configurarCidade(' + locations[i].id + ',' + locations[i].local.id + ', this, \'favorito\')"' +
                                    '       data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="favorito"' + 
                                    '       class="marker-check marker-check-favorito" style="display: none;" />' +
                                    '<i class="map-marker marker-favorito checkable est-fav-pin ' + (locations[i].favorito == true ? '' : 'inactive') + '" title="Favorito" data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="favorito"> </i> ' +
                                '</label>' + 
                            '</td>' +
                            '<td class="center" style="width:13%; padding: 12px 7px 4px 8px;">' +
                                '<label style="cursor: pointer; margin-bottom: 0px;">' + 
                                    '<input type="checkbox" id="indica_' + locations[i].local.id + '" name="list[' + i + '].indica" value="true" ' + (locations[i].indica == true ? 'checked="checked"' : '') +
                                    //'       onchange="configurarCidade(' + locations[i].id + ',' + locations[i].local.id + ', this, \'indica\')"' +
                                    '       data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="indica"' + 
                                    '       class="marker-check marker-check-indica" style="display: none;" />' +
                                    '<i class="map-marker marker-indica checkable est-ind-pin ' + (locations[i].indica == true ? '' : 'inactive') + '" title="Indica" data-local-id="' + locations[i].local.id + '" data-interesse-id="' + locations[i].id + '" data-tipo-interesse="indica"> </i> ' +
                                '</label>' + 
                            '</td>' +
                        '</tr>';
                    
                }
                tabela +=
                	      '</table>' +
                      '</form>';
                      
                if (popup) {
                    tabela += '<div class="right">' +
                                  '<input type="button" value="Conclu�do" class="btn-mini btn-success" onclick="closeInfowindow(); return false;" />' +                            
                              '</div>';
                }

                tabela += '</div>';
                return tabela;
            }
            
            /**
             * Adiciona os marcadores ao mapa
             */
            function placeMarkers(locations, showInfowindow) {
                for (var i = 0; i < locations.length; i++) {
                    mapManager.addMarker(locations[i], showInfowindow);
                }
            }
            
            function somarOuSubtrairContagemLocais(nomeCampo, somar) {
            	var total = $('#' + nomeCampo).html();
                total = parseInt(total);
            	if (somar) {
            		total += 1;
            	} else {
            		total -= 1;
            	}
            	$('#' + nomeCampo).html(total);
            }
            
            function exibirCompartilharMapa() {
                // exibir os botoes 'compartilhar mapa'
                $('.compartilharMapa').each(function(index, el) {
                    $(this).load('<c:url value="/perfil/mapa/compartilhar/${perfil.urlPath}"/>');
                });
            }
            
            /**
             * Refazer a contagem dos filtros ap�s a altera��o de alguma cidade
             */
            function recontarLocais(localUsuarioAntes, localUsuarioAlterado) {
            	if (localUsuarioAntes == null) {
            		localUsuarioAntes = {
            				'jaFoi' : false, 
            				'desejaIr' : false, 
            				'favorito' : false, 
            				'indica' : false
            				}
            	}
            	
            	if (localUsuarioAntes != null && localUsuarioAlterado != null) {
            	    var total = null;
            		// Cada 'if' verifica se a op��o selecionada difere do que j� estava 
            		// selecionado para o local, caso seja diferente e a op��o estiver marcada,
            		// somar/subtrair 1 ao total
            		if (localUsuarioAlterado.jaFoi != localUsuarioAntes.jaFoi) {
            		    somarOuSubtrairContagemLocais('numeroJaFoi', localUsuarioAlterado.jaFoi);
            		}
                    if (localUsuarioAlterado.desejaIr != localUsuarioAntes.desejaIr) {
                        somarOuSubtrairContagemLocais('numeroDesejaIr', localUsuarioAlterado.desejaIr);
                    }
                    if (localUsuarioAlterado.favorito != localUsuarioAntes.favorito) {
                        somarOuSubtrairContagemLocais('numeroFavorito', localUsuarioAlterado.favorito);
                    }
                    if (localUsuarioAlterado.indica != localUsuarioAntes.indica) {
                        somarOuSubtrairContagemLocais('numeroIndica', localUsuarioAlterado.indica);
                    }
            	 }
            }
            
            function configurarAcao(idCidade, $checkbox, acao) {
                // marcar checks correspondentes em todos os lugares caso seja necessario
                var checked = $checkbox.is(':checked');
                $('input[data-local-id="' + idCidade + '"]').filter('.marker-check-' + acao).attr('checked', checked);
                var $marker = $('i[data-local-id="' + idCidade + '"]').filter('.marker-' + acao)
                if (checked) {
                    $marker.removeClass('inactive');
                } else {
                    $marker.addClass('inactive');
                }
                
                var checkJaFoi = $checkbox.closest('table').find('#jaFoi_' + idCidade);
                var checkDesejaIr = $checkbox.closest('table').find('#desejaIr_' + idCidade);
                var checkFavorito = $checkbox.closest('table').find('#favorito_' + idCidade);
                var checkIndica = $checkbox.closest('table').find('#indica_' + idCidade);
                // se clicou em favorito ou indica, marca jaFoi tb
                if ((acao == 'favorito' || acao == 'indica') && checked) {
                	$('input[data-local-id="' + idCidade + '"]').filter('.marker-check-jaFoi').attr('checked', 'checked');
                	$('i[data-local-id="' + idCidade + '"]').filter('.marker-jaFoi').removeClass('inactive');
                }
                // se desmarcou jaFoi, desmarcar favorito e indica
                else if (acao == 'jaFoi' && !checked) {
                    $('input[data-local-id="' + idCidade + '"]').filter('.marker-check-favorito').removeAttr('checked');
                	$('i[data-local-id="' + idCidade + '"]').filter('.marker-favorito').addClass('inactive');
                    $('input[data-local-id="' + idCidade + '"]').filter('.marker-check-indica').removeAttr('checked');
                	$('i[data-local-id="' + idCidade + '"]').filter('.marker-indica').addClass('inactive');
                }
                return 'jaFoi=' + checkJaFoi.is(':checked') + '&desejaIr=' + checkDesejaIr.is(':checked') + '&favorito=' + checkFavorito.is(':checked') + '&indica=' + checkIndica.is(':checked');   
            }
            
            function configurarCidade(idLocalUsuario, idCidade, $checkbox, acao) {
                var data = '';
                if (idLocalUsuario != null) {
                	data += 'localUsuario=' + idLocalUsuario + '&';
                }
                var acaoFinal = configurarAcao(idCidade, $checkbox, acao);
                
                data += '&usuario=${perfil.id}&local=' + idCidade + '&' + acaoFinal + '&jsonFields=' +
                      $.toJSON([
                          {name: 'id'},
                          {name: 'usuario.id'},
                          {name: 'local.id'},
                          {name: 'local.nome'},
                          {name: 'local.nomeComSiglaEstado'},
                          {name: 'local.nomeComSiglaEstadoNomePais'},
                          {name: 'local.latitude'},
                          {name: 'local.longitude'},
                          {name: 'jaFoi'},
                          {name: 'desejaIr'},
                          {name: 'favorito'},
                          {name: 'indica'}
                      ]);

                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/perfil/salvarInteressesUsuarioEmCidade" />',
                    data: data,
                    success: function(data) {
                        if (data.records != null) {
                            var localUsuario = data.records[0];
                            if (localUsuario.local != null) {
                            	// refazer a contagem de locais
                                recontarLocais(mapManager.getLocais()[localUsuario.local.id], localUsuario);
                            	placeMarkers([localUsuario]);
                            	exibirCompartilharMapa();
                            } else {
                            	var localUsuarioARemover = mapManager.getLocais()[localUsuario];
                            	// remover marcador
                                mapManager.removeMarker(localUsuario);
                            	
                                // refazer a contagem de locais
                                recontarLocais(localUsuarioARemover, {
                                    'jaFoi': false,
                                    'desejaIr': false,
                                    'favorito': false,
                                    'indica': false
                                });
                                exibirCompartilharMapa();
                            }
                        } else {
                            // TODO
                        }
                    }
                });
            }
            
            function filtrarMarcadores() {
            	var opcoes = {
            	    'jaFoi' : $('#filtroJaFoi').is(':checked'),
            	    'desejaIr' : $('#filtroDesejaIr').is(':checked'),
            	    'favorito' : $('#filtroFavorito').is(':checked'),
            	    'indica' : $('#filtroIndica').is(':checked')
            	}
            	mapManager.updateMarkers(opcoes);
            }
            
        </c:otherwise>
    </c:choose>
    
</script>