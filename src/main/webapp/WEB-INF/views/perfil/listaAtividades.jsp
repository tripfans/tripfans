<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="cssSpan" value="${not empty param.cssSpan ? param.cssSpan : 'span16'}"/>
<c:if test="${not empty atividades}">
	<c:forEach var="atividade" items="${atividades}" >
    	<c:set var="atividade" value="${atividade}" scope="request" />
        <div>
            <div class="${cssSpan}" style="margin-left: 0px;">
                <%--jsp:include page="atividade.jsp">
                    <jsp:param name="showUserPhoto" value="true" />
                    <jsp:param name="displayDetails" value="true" />
                </jsp:include --%>
                
                <fan:atividade acaoUsuario="${atividade}" mostrarFotoAutor="false" mostrarIcone="true" mostrarData="true" />
                
            </div>
        </div>
	    <hr class="${cssSpan}" style="margin:2px;" />
	</c:forEach>
</c:if>

<div style="text-align: center; padding: 8px;">
    <c:url var="urlAtividades" value="/perfil/maisAtividades/${perfil.urlPath}" />
    <fan:pagingButton limit="10" targetId="listaAtividades" title="Ver mais atividades" noMoreItensText="" url="${urlAtividades}" cssStyle="width: 70%;" />
</div>
