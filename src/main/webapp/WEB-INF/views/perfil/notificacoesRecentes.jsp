<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div>
    <c:forEach items="${notificacoes}" var="notificacao">
    	<c:set var="notificacao" value="${notificacao}" scope="request" />
    	<div class="notificacao ${not isMobile ? 'row' : ''} ${notificacao.pendente ? 'pendente' : ''}" style="margin-left: 0px; margin-right: 0px;">
        	<div class="${not isMobile ? 'span6' : ''}" style="margin-left: 0px; margin-right: 0px;">
                <jsp:include page="notificacao.jsp">
                    <jsp:param name="displayDetails" value="false" />
                    <jsp:param name="showSeparator" value="true" />
                </jsp:include>
        	</div>
    	</div>
    </c:forEach>
    
    <a href="<c:url value="/perfil/${usuario.urlPath}/notificacoes" />">
        <div class="row centerAligned" style="width: 100%; height: 30px; padding-top: 6px;">
            <b>Ver todas as notificações</b>
        </div>
    </a>
</div>

<script>

jQuery(function ($) {
	$('.link-notificacao').click(function (e) {
        e.preventDefault();
		var boxNotificacao = $(this);
		var url = boxNotificacao.attr('href');
		if (boxNotificacao.hasClass('pendente')) {
    		var id = boxNotificacao.attr('data-notificacao-id');
    		$.post('<c:url value="/atualizarNotificacao" />/' + id, function(data) {
    			$('#btnNotificacoes').click();
    			$(location).attr('href', url);
    		});
		} else {
			$(location).attr('href', url);
			$('#btnNotificacoes').click();
		}
    });
});
</script>