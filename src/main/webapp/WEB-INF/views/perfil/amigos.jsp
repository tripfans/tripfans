<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="titulo" value="Amigos de ${perfil.primeiroNome}" />
<c:if test="${usuario.id == perfil.id }" >
	<c:set var="titulo" value="Meus Amigos" />
</c:if>

<c:set var="acao" value="${not empty param.acao ? param.acao : acao}" />

<c:set var="convidar" value="${not empty param.convidar ? param.convidar : convidar}" />

<style>
.ordenacao-alfabetica-tab {
    min-width: 20px; 
    max-width: 20px; 
    padding-top: 0px; 
    padding-right: 0px;
    padding-bottom: 0px; 
    margin-bottom: 0px;
}

.ordenacao-alfabetica-fixed {
    position: fixed;
    top: 120px;
    left: 1180px;
    right: 0;
}
</style>

<script src="http://connect.facebook.net/en_US/all.js"></script>

<script type="text/javascript">

accentsTidy = function(s){
    var r=s.toLowerCase();
    r = r.replace(new RegExp("\\s", 'g'),"");
    r = r.replace(new RegExp("[������]", 'g'),"a");
    r = r.replace(new RegExp("�", 'g'),"ae");
    r = r.replace(new RegExp("�", 'g'),"c");
    r = r.replace(new RegExp("[����]", 'g'),"e");
    r = r.replace(new RegExp("[����]", 'g'),"i");
    r = r.replace(new RegExp("�", 'g'),"n");                            
    r = r.replace(new RegExp("[�����]", 'g'),"o");
    r = r.replace(new RegExp("[����]", 'g'),"u");
    r = r.replace(new RegExp("[��]", 'g'),"y");
    r = r.replace(new RegExp("\\W", 'g'),"");
    return r;
};

$(document).ready(function() {
    $.fancybox.init();
    
    var acao = '${acao}';
    var provider = null;
    if (acao !== '') {
    	acao = acao.split('-');
    	
    	var tab = acao[0];
    	provider = acao[1];
    	
        var $tabAtual = $('#' + tab);
        if ($tabAtual.attr('id') != null) {
        	$tabAtual.click();
        } else {
        	$('#meusAmigos').click();
        }
        
    } else {
        $('#meusAmigos').click();
    }
	
	$('.send-message').click(function (e) {
        e.preventDefault();
        var idFacebook = $(this).attr('data-user-facebook-id');
        facebookSendMessage(idFacebook);
    });

	$('.send-message-all').click(function (e) {
        e.preventDefault();
        
        var userIds = '';
        $('.card-selected').each(function(index) {
            usrId = $(this).attr('data-fb-id');
            if (usrId != null && usrId != '') {
                if (userIds != '') {
                    userIds += ','
    	        } 
                userIds += usrId;
        	}
        });
        
        if (userIds != '') {
            jQuery.ajax({
                type: 'POST',
                url: '<c:url value="/perfil/convidarAmigosFacebook" />',
                data: {
                    userIds : userIds
                },
                success: function(data) {
                	if (data.success) {
                    }
                }
            });
        }
    });

	$('.post-to-feed').click(function (e) {
        e.preventDefault();

        //var idFacebook = $(this).attr('data-user-facebook-id');
        facebookPost($(this), 'feed');
    });
	
	
    $('.send-request').click(function (e) {
    	e.preventDefault();
    	var idFacebookUsuario = $(this).attr('data-to');
    	enivarConviteViaRequisaoAplicativo(idFacebookUsuario);
    });
	
    $('.send-request-all').click(function (e) {
    	e.preventDefault();
    	enviarConviteAmigosSelecionados();
    });
    
    $('#btn-enviar-convites-email').click(function (e) {
    	e.preventDefault();
    	enviarConvitesEmails($('textarea[name="emails-amigos"]').val(), '<c:url value="/perfil/convidarContatosPorEnderecosDeEmail" />', function() {
    	    
    	});
    });

	$('.card-content').click(function (e) {
	    var $card = $(this).parents('.card');
	    var $check = $card.find('.card-check');
	    if ($card.hasClass('card-selected')) {
	        $check.removeAttr('checked');
	        $card.removeClass('card-selected');
	    } else {
	        $check.attr('checked', 'checked');
	        $card.addClass('card-selected');
	    }	    
	});
	
	$('#select-all').click(function (e) {
		if ($(this).attr('class').indexOf('active') === -1) {
	        $('.card-check:visible').attr('checked', true).parents('div.card').addClass('card-selected');
        } else {
            $('.card-check').attr('checked', false).parents('div.card').removeClass('card-selected');
        }
	});

	$('.link-encontrar-amigos').click(function (e) {
	    e.preventDefault();
	    $('#tabs-amigos a[href="#encontrar-amigos"]').tab('show');
	});

	$('#filtro-nome-amigos').keyup(function(e) {
        //if (e.keyCode == 13) {}
        var text = accentsTidy($(this).val());
        var regex = new RegExp(text, "ig");
        var qtdTripFans = 0
        var qtdFacebook = 0;
        $('#listaAmigosTripFans > .card').each(function() {
			var $card = $(this);
		    if (! accentsTidy($card.attr('data-name')).match(regex)) {
		        $card.hide();
		    } else {
		        qtdTripFans += 1;
		        $card.show();
		    }
		});
        $('#listaAmigosFacebook > .card').each(function() {
			var $card = $(this);
		    if (! accentsTidy($card.attr('data-name')).match(regex)) {
		        $card.hide();
		    } else {
		        qtdFacebook += 1;
		        $card.show();
		    }
		});
        if (qtdTripFans == 0) {
            $('#listaAmigosTripFans').find('.nenhumEncontrado').show();
        } else {
            $('#listaAmigosTripFans').find('.nenhumEncontrado').hide();
        }
        if (qtdFacebook == 0) {
            $('#listaAmigosFacebook').find('.nenhumEncontrado').show();
        } else {
            $('#listaAmigosFacebook').find('.nenhumEncontrado').hide();
        }
    });
	
	
	$('img.avatar').lazyload({
		effect : 'fadeIn'
	});
	
	$('#consultarContatosGoogle').fancybox({
	    width: 800,
	    height: 500,
	    autoDimensions: false,
	    hideOnOverlayClick: false,
	    centerOnScroll : true,
	    type: 'iframe'
	});
	
    if (provider == 'google') {
        $('#consultarContatosGoogle').click();          
    }
	
});

// assume we are already logged in

</script>

<jsp:include page="/WEB-INF/includes/facebookJSFuncs.jsp" />

<div id="fb-root"></div>

<div class="span16">
	<div class="page-header">
	    <h3>${titulo}</h3>
	</div>
	
	<c:if test="${usuario.id == perfil.id}">
		<s:message var="texto1" code="textoAjuda.amigos1" />
		<s:message var="texto2" code="textoAjuda.amigos2" />
		<s:message var="texto3" code="textoAjuda.amigos3" />
		<fan:helpText text="${texto2}${texto3}"  showQuestion="false"/>
	  <ul id="tabs-amigos" class="nav nav-tabs" style="height: 35px; max-height: 35px;">
	    <li>
	        <a id="meusAmigos" href="#meus-amigos" data-toggle="tab">Meus amigos</a>
	    </li>
	    <li>
	        <a id="solicitacoesAmizade" href="#solicitacoes-amizade" data-toggle="tab">Solicita��es de Amizade</a>
	    </li>
        <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	      <li>
	        <a id="encontrarAmigos" href="#encontrar-amigos" data-toggle="tab">Encontrar amigos</a>
	      </li>
        </c:if>
	  </ul>
	</c:if>

	<div class="tab-content" style="overflow-x: hidden;">
	
	    <div class="tab-pane active" id="meus-amigos">
	    
	        <c:if test="${empty amizadesTripFans and empty amizadesFacebook}">
	            <div style="height: 50px;"></div>
	            <div class="blank-state">
	                <div class="row">
	                    <div class="span2 offset1">
	                        <img src="<c:url value="/resources/images/icons/mini/128/Users-22.png" />" width="90" height="90" />
	                    </div>
	                    <div class="span8">
	        
	                        <h3>N�o h� amigos para exibir</h3>
	                        <p>
	                            <c:choose>
	                              <c:when test="${usuario.id == perfil.id}">
	                                  Voc�
	                              </c:when>
	                              <c:otherwise>
	                                  Este usu�rio
	                              </c:otherwise>
	                            </c:choose>
	                            ainda n�o adicionou nenhum amigo.
	                            <br/>
                                <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	                              <a href="#" class="link-encontrar-amigos">
	                                <c:if test="${usuario.id == perfil.id}">
	                                Encontre amigos
	                                </c:if>
	                              </a>
                                </c:if>
	                        </p>
	                        
	                    </div>
	                </div>
	            </div>
	        </c:if>
	                            
	        <c:if test="${not empty amizadesFacebook or not empty amizadesTripFans}">
	        
	         <c:set var="primeiraListaAmizade" value="${amizadesTripFans}" />
	         <c:set var="segundaListaAmizade" value="${amizadesFacebook}" />
	         
	         <c:if test="${not empty convite && convite == 'true' }">
	         		<c:set var="primeiraListaAmizade" value="${amizadesFacebook}" />
	         		<c:set var="segundaListaAmizade" value="${amizadesTripFans}" />
	         </c:if>
	        
	          <c:if test="${usuario.id == perfil.id}">
	          
	            <div class="subnav">
	            
	                <ul class="nav nav-pills">
	            
	                    <li>
	                    
	                        <div class="left" style="margin-bottom: 9px; margin-top: 5px;">
	                            <div class="input-append">
	                                <input id="filtro-nome-amigos" class="filtro-nome-local span4" placeholder="Procure por seus amigos" />
	                                <span class="add-on"><i class="icon-search"></i></span>
	                            </div>
	                        </div>
	                    </li>
                        
                        <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	            
	                      <li>
	                        <div class="left" style="margin: 10px; font-size: 11px;">
	                            <a id="select-all" class="btn btn-mini" data-toggle="button" href="#">
	                                Selecionar todos
	                            </a>
	                        </div>
	                      </li>
	                    
	                      <li>
	                        <div class="left" style="margin: 10px; font-size: 11px;">
	                            Com selecionados:
	                        </div>
	                      </li>
	            
	                      <li>
	                        <div class="btn-toolbar left" style="margin-bottom: 9px">
	                            <div class="btn-group">
	                                <a class="btn btn-mini btn-info action-button dropdown-toggle" data-toggle="dropdown" href="#">
	                                    Convidar para o TripFans
	                                    <span class="caret"></span>
	                                </a>
	                                <ul class="dropdown-menu">
	                                    <li>
	                                        <a href="#" class="send-request-all">
	                                            Enviar requisi��o de aplicativo
	                                        </a> 
	                                        <!-- a href="#" class="send-message-all">
	                                            Enviar mensagem privada
	                                        </a-->
	                                    </li>
	                                </ul>
	                            </div>
	                        </div>
	                      </li>
                        </c:if>
	            
	                </ul>
	                
	            </div>
	            
	          </c:if>
	        
	        </c:if>
	        
	        <div>
	        
	        <c:if test="${not empty primeiraListaAmizade}">
            
                <div class="span16" style="margin-top: -15px;">
                
                    <div class="page-header">
                        <h3>
                          Amigos
                          <c:if test="${usuario.id != perfil.id}">
                          de ${perfil.displayName}
                          </c:if>
                          no
                          <img src="<c:url value="/resources/images/logos/tripfans-t.png" />" width="110" style="margin-top: -3px;"> 
                        </h3>
                    </div>
                    
    	            <div class="row" style="margin-bottom: 20px;">
    	            
    	                <div id="listaAmigosTripFans" class="span15" class="listaAmigos">
    	                
    	                    <c:forEach items="${primeiraListaAmizade}" var="amizadeView" varStatus="contador">
    	
    	                        <c:set var="amizade" value="${amizadeView.amizade}" />
    	                        
    	                        <%@include file="amigo.jsp" %>
    	                                
    	                    </c:forEach>
                            
                            <div class="hide nenhumEncontrado">
                                Nenhum amigo encontrado com o nome informado.
                            </div>
    	                    
    	                </div>
    	                
    	            </div>
	            </div>
	        </c:if>
	        
	        </div>
            
            <div>
            
            <c:if test="${not empty segundaListaAmizade}">
            
                <div class="span16" style="margin-top: -15px;">
                
                    <div class="page-header">
                        <h3>
                          Amigos
                          <c:if test="${usuario.id != perfil.id}">
                          de ${perfil.displayName}
                          </c:if>
                          no
                          <img src="<c:url value="/resources/images/facebook-logo.jpg" />" width="110" style="margin-top: -3px;"> 
                        </h3>
                    </div>
                    
                    <div class="row" style="margin-bottom: 100px;">
                    
                        <div id="listaAmigosFacebook" class="span15" class="listaAmigos">
                        
                            <c:forEach items="${segundaListaAmizade}" var="amizadeView" varStatus="contador">
        
                                <c:set var="amizade" value="${amizadeView.amizade}" />
                                
                                <%@include file="amigo.jsp" %>
                                        
                            </c:forEach>
                            
                            <div class="hide nenhumEncontrado">
                                Nenhum amigo encontrado com o nome informado.
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </c:if>
            
            </div>             
	    
	    </div>
	    
	    <c:if test="${usuario.id == perfil.id}">
	    
	      <div class="tab-pane" id="solicitacoes-amizade">
	
	        <c:if test="${empty convitesRecebidosPendentes && empty convitesEnviadosPendentes}">
	
	            <div style="height: 50px;"></div>
	            <div class="blank-state">
	                <div class="row">
	                    <div class="span2 offset1">
	                        <img src="<c:url value="/resources/images/icons/mini/128/Communication-55.png" />" width="90" height="90" />
	                    </div>
	                    <div class="span6">
	        
	                        <h3>N�o h� convites pendentes</h3>
	                        <p>
	                            <c:choose>
	                              <c:when test="${usuario.id == perfil.id}">
	                                  Voc�
	                              </c:when>
	                              <c:otherwise>
	                                  Este usu�rio
	                              </c:otherwise>
	                            </c:choose>
	                            n�o possui nenhuma solicita��o de amizade pendente.
	                            
	                            <br/>
                                
                                <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	                              <a href="#" class="link-encontrar-amigos">
	                                <c:if test="${usuario.id == perfil.id}">
	                                Encontre novos amigos
	                                </c:if>
	                              </a>
                                </c:if>
	                        </p>
	                        
	                    </div>
	                </div>
	            </div>
	            
	        </c:if>
	
	        <c:if test="${not empty convitesRecebidosPendentes}">
            
              <div class="span14">
              
                <fieldset>
	    
	            <legend>Solicita��es de amizade</legend>
	        
	              <c:forEach items="${convitesRecebidosPendentes}" var="convite">
	            
                    <c:url var="urlAceita" value="/aceitarConviteAmizade/${convite.autor.urlPath}" />
                    <c:url var="urlIgnora" value="/ignorarConvite/${convite.id}" />
                    
	                <fan:card type="user" user="${convite.remetente}" showActions="true" selectable="false" spanSize="7" cardStyle="margin: 8px;">
	                    <jsp:attribute name="actions">
	                        <div class="btn-group right">
                                <fan:inviteButtons urlAccept="${urlAceita}" cssClassAccept="btn btn-mini btn-primary" titleAccept="Aceitar" iconClassAccept="icon-ok icon-white" 
                                                   urlIgnore="${urlIgnora}" cssClassIgnore="btn btn-mini" titleIgnore="Ignorar" iconClassIgnore="icon-remove" />
	                        </div>
	                    </jsp:attribute>
	                </fan:card>
	            
	              </c:forEach>
                
                </fieldset>
                
              </div>
	            
	        </c:if>
	        
	        <c:if test="${not empty convitesEnviadosPendentes}">
            
              <div class="span14">
              
                <fieldset>
	
	            <legend>Convites enviados por mim (aguardando confirma��o)</legend>
	    
	              <c:forEach items="${convitesEnviadosPendentes}" var="convite">
                
	                <fan:card type="user" user="${convite.convidado}" showActions="true" selectable="false" spanSize="7" cardStyle="margin: 8px;">
	                    <jsp:attribute name="actions">
	                    </jsp:attribute>
	                </fan:card>
	            
	              </c:forEach>
                
                </fieldset>
                
              </div>
	            
	        </c:if>
	    
	      </div>
	    
	    </c:if>
	    
	    <c:if test="${usuario.id == perfil.id}">
        
         <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	    
	      <div class="tab-pane" id="encontrar-amigos">
	    
	        <p>
	            Encontre pessoas que voc� conhece para fazerem parte da sua rede de amigos no TripFans. 
	            Para isso utilize suas contas do Facebook ou Gmail ou envie o convite diretamente para os emails de seus amigos.
	        </p>
	        <br/>
	        
	        <fieldset>
	            
	            <legend>
	                Pesquise seus contatos utilizando suas contas abaixo
	            </legend>        
	        
	            <div>
	            	<c:choose>
	                  <c:when test="${perfil.conectadoGoogle}">
	                    <c:url var="urlGoogle" value="/perfil/preConsultaContatosGoogle"/>
                        <a id="consultarContatosGoogle" href="${urlGoogle}" class="btn btn-large">
                            <img src="<c:url value="/resources/images/gmail.png" /> "/>
                        </a>
	                  </c:when>
	                  <c:otherwise>
	                  	<c:url var="urlGoogle" value="/connect/google?scope=https://www.googleapis.com/auth/userinfo.profile&redirectUrl=/perfil/consultarContatosGoogle&redirectUrlOnError=/perfil/erroConexaoGoogle"/>
	                    <form id="form_google_connect" action="<c:url value="/connect/google"/>" method="POST">
	                        <input type="hidden" name="scope" value="https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo#email https://www.googleapis.com/auth/plus.me https://www.google.com/m8/feeds" />
	                        <input type="hidden" name="redirectUrl" value="/perfil/${perfil.urlPath}?tab=amigos&acao=encontrarAmigos-google" />
                            <input type="hidden" name="redirectUrlOnError" value="/perfil/erroConexaoGoogle" />
	                    </form>
                        <a href="#" class="btn btn-large" onclick="document.forms['form_google_connect'].submit();">
                            <img src="<c:url value="/resources/images/gmail.png" /> "/>
                        </a>
	                  </c:otherwise>
	                </c:choose>
	            	
					<%--c:if test="${perfil.conectadoFacebook}"--%>
	            	<a class="btn btn-large send-request-all" style="height: 60px;">
	                    <img src="<c:url value="/resources/images/facebook-logo.jpg" /> "/>
	                </a>
	                <%--/c:if--%>


	            </div>
	
	        </fieldset>            
	
	        <fieldset>
	            
	            <legend>
	                Ou convite pelo endere�o de email
	            </legend>
	            
	            <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
	                
	                <li>
	                    <p> 
	                    Informe abaixo os emails de seus amigos ( separados por v��rgulas )
	                    </p>
	                    <textarea name="emails-amigos" class="span10" rows="5" />
	                </li>
	
	                <li>
	                    <div class="span10" align="right" style="margin-left: 0px;">
	                        <a id="btn-enviar-convites-email" href="#" class="btn">
	                            <i class="icon-envelope" />
	                            Enviar convites
	                        </a>
	                    </div>
	                </li>
	                    
	            </ul>            
	            
	
	        </fieldset>            
	
	        <fieldset>
	            
	            <legend>
	                Ou ainda, compartilhe no seu mural no Facebook ou no Twitter
	            </legend>
	            
	            <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
	                
	                <%--li>
	                    <p> 
	                        Escreva uma mensagem para seus amigos ou utilize o texto sugerido
	                    </p>
	                    <textarea name="texto-compartilhamento" class="span10" rows="5" />
	                </li--%>
	
	                <li>
	                    <div class="span10" align="left" style="margin-left: 0px;">
	                        <a href="#" class="btn post-to-feed" style="margin-top: -20px;"
	                                    data-from="${perfil.idFacebook}" 
	                                    data-name="Junte-se ao TripFans - A sua rede social de viagens" 
	                                    data-caption="O TripFans � a sua rede social de viagens, feita com a ajuda de seus amigos!" 
	                                    data-description="Por meio das recomenda��es de seus amigos e dos interesses que voc� possui, o TripFans proporcionar� uma experi�ncia �nica em suas viagens, atrav�s de dicas, avalia��es, respostas as suas perguntas, ferramenta para a elabora��o de planos de viagens, recomenda��es de locais e promo��es." 
	                                    data-link="${tripFansEnviroment.serverUrl}" 
	                                    data-picture="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans.png">
	                            <img src="${tripFansEnviroment.serverUrl}/resources/images/logos/facebook.png" />
                                Compartilhe no Facebook
	                        </a>
	
	                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="${tripFansEnviroment.serverUrl}" data-text="Junte-se ao TripFans - A sua rede social de viagens" data-lang="pt" data-size="large" data-count="none">Tweetar</a>
	                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	
	                    </div>
	                </li>
	                    
	            </ul>            
	
	        </fieldset>
	        
	      </div>
	    
	     </c:if>
        
        </c:if>
	    
	</div>
</div>

