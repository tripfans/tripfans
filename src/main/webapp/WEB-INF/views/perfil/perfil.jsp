<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="fanaticos.default">

	<c:set var="mostrarPerfil" value="${perfilView.visivel}" />
    
    <c:set var="tab" value="${not empty param.tab ? param.tab : tab}" />

	<tiles:putAttribute name="title" value="${perfil.displayName} - TripFans" />

    <tiles:putAttribute name="stylesheets">
    	<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/services-plugin/css/settings.css" />" media="screen" />
        <c:url var="urlFotoDoPerfil" value="${not empty perfil.foto ? perfil.urlFotoPerfil : (tripFansEnviroment.serverUrl + '/resources/logos/images/tripfans-vertical.png')}"/>
        <meta property="og:title" content="Perfil de ${perfil.displayName} - TripFans" />
        <meta property="og:url" content="${tripFansEnviroment.serverUrl}/perfil/${perfil.urlPath}" />
        <meta property="og:image" content="${urlFotoDoPerfil}" />
        <meta property="og:site_name" content="TripFans" />
        <meta property="og:description" content="Perfil de ${perfil.displayName}" />
        
        <!-- THE BASIC STYLE CLASSES -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/style.css" />" media="screen" />
		<!-- THE SHOWBIZ STLYES -->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/showbizpro/css/settings.css" />" media="screen" />
        
		<style>
		.reveal_container.tofullwidth {
		    visibility: hidden;
		}
		
		.reveal_container .reveal_wrapper {
		    padding: 0px 20px 20px;
		}
		</style>
        
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    
    	<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
        		<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>
    	
        <script type="text/javascript" src="${reqScheme}://maps.googleapis.com/maps/api/js?sensor=false"></script>
        
        <c:if test="${not unsuportedIEVersion}">
          <script src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR" type="text/javascript"></script>
        </c:if>
        
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.touchwipe.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.mousewheel.min.js" />"></script>
        
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.themepunch.services.js" />"></script> 
        
        <script type="text/javascript" src="<c:url value="/resources/components/showbizpro/js/jquery.themepunch.showbizpro.min.js" />"></script>
    
        <script type="text/javascript">
        
            <c:set var="tab" value="${pagina != null ? pagina : param.tab}" />
            
            var paramDestaque = '${param.destaque}';
            
            $(document).ready(function () {
                $('a.menu-item').bind('click',function(event) {
                    $('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    var params = $(this).attr('data-static-params');
                    if (paramDestaque != '') {
                        params += '&destaque=' + paramDestaque;
                        paramDestaque = '';
                    }
                    carregarPagina(this.id, params);
                });
                
            	var tab = '${tab}';
                if (tab !== '' ) {
                    var $tabAtual = $('#' + tab);
                    if ($tabAtual.attr('id') != null) {
                    	$tabAtual.click();
                    } else {
                    	$('#principal').click();
                    }
                } else {
	                $('#principal').click();
                }
                
                <sec:authorize access="isAuthenticated()">
                $('#linkAmizade').bind('click', function(event) {
                	event.preventDefault();
                	$.post('<c:url value="/convidarParaAmizade/${perfil.urlPath}" />',{},function(response){
               			$('#btnAmizade_${perfil.urlPath}').remove();
               			$('#spanConviteAmizade').remove();
               	 	});
                });
                </sec:authorize>
                <sec:authorize access="isAnonymous()">
                $('#linkAmizade').bind('click', function(event) {
                	event.preventDefault();
                	$("#login-dialog-link").trigger('click');
                });
    			</sec:authorize>
            });
            
            function carregarPagina(pagina, params) {
                var url = '<c:url value="/perfil/${perfil.urlPath}" />/' + pagina
               	$('#'+pagina).parent().addClass('active'); //Set clicked link class to active
               	$(document).off('click', '**');
               	$('body').scrollTop(0);
            	$("#conteudo").load('<c:url value="/perfil/" />' + pagina + '/${perfil.urlPath}' + (params != null ? '?' + params : ''),  
        			function(response) {
        				$('#conteudo').initToolTips();
        				redimensionarMenu();
        				//window.history.pushState( url, window.title, url );
        				History.pushState( url, window.title, url );
        				$.scrollTo($('#tituloPagina'), {offset: -70, duration: 100} );
        				$.lockfixed('#menuLateral', { offset: { top: 70, bottom: 386 } });
        			}
            	);
            }
            
            function redimensionarMenu() {
                /*$('#ulSidenav').css({'height': '420px'});
                $('#ulSidenav').css({'height': $('#footer-content').offset().top - 250});*/
            }
            
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <%-- Se for o perfil do usuario TripFans, retornar para a tela inicial (home) --%>
        <c:choose>
            <c:when test="${perfil.id == 1}">
                <script>
                    document.location.href = '<c:url value="/home" />';
                </script>
            </c:when>
            <c:otherwise>
        
                <c:if test="${mostrarPerfil == true}">    
                    <div id="fb-root"></div>
                    <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=${tripFansEnviroment.facebookClientId}";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));</script>
                </c:if>
            
                <meta http-equiv="cache-control" content="no-cache,must-revalidate">
                <meta http-equiv="expires" content="0">
                <meta http-equiv="pragma" content="no-cache">
        
                <style>
                    .sidebar-box {
                        border-width: 1px;
                    }        
                </style>
                
                <div class="container-fluid page-container">
                	<div id="tituloPagina">
        				<div class="title gradient-background">
            				<span class="title">${perfil.displayName}</span>
            				<div class="pull-right">
                              <div class="btn-toolbar">
            					<sec:authorize access="isAuthenticated()">
                                    <c:if test="${usuario.id != perfil.id}">
                                      	<c:if test="${perfilView.conviteAmizadePendente == false and perfilView.amigo == false}">
                                      		<fan:addFriendButton usuario="${perfil.urlPath}" />
                                      	</c:if>
                                      	<c:if test="${perfilView.convidouQuemEstaVendo == true}">
                                      		<c:url var="urlAccept" value="/aceitarConviteAmizade/${perfil.urlPath}" />
                                      		<c:url var="urlIgnore" value="/ignorarConviteAmizade/${perfil.urlPath}" />
            							    <fan:inviteButtons urlAccept="${urlAccept}" cssClassAccept="btn btn-mini btn-primary" titleIgnore="Ignorar Convite de Amizade" urlIgnore="${urlIgnore}" cssClassIgnore="btn btn-mini" titleAccept="Aceitar Convite de Amizade"/>
                                      	</c:if>
                                      	<c:if test="${perfilView.foiConvidadoPorQuemEstaVendo == true}">
            							    <a class="btn btn-mini btn-primary" title="Voc� convidou ${perfil.displayName} para ser seu amigo"><i class="icon-info-sign icon-white"></i><b>Solicita��o de amizade enviada</b></a>
                                      	</c:if>
                                      	<c:if test="${perfilView.amigo == true}">
                                      		<c:url var="urlReload" value="/perfil/${perfil.urlPath}" />
                                      		<fan:isFriendButton cssButtonClassSize="mini" somenteFacebook="${perfilView.amigoSomenteFacebook}" idUsuarioAmigo="${perfil.id}"
                                      		importadaFacebook="${perfilView.amigoImportadoFacebook}" nomeAmigo="${perfil.displayName}" urlReload="${urlReload}" buttonGroupClass="right" buttonGroupStyle="margin-left: 3px;" />
                                      	</c:if>
                                    </c:if>
                                    <c:if test="${usuario.id == perfil.id}">
                                    	<a href="<c:url value="/conta/minhaConta/perfil" />" class="btn btn-mini btn-info" id="btnEditarPerfil">
            						      <i class="icon-edit icon-white"></i>
            						      <b>Editar perfil</b>
            							</a>
                                    </c:if>
                                </sec:authorize>
                              </div>
            				</div>
                            <c:if test="${mostrarPerfil == true and tripFansEnviroment.enableSocialNetworkActions}">
                                <div class="fb-like pull-right" style="margin: 10px;" data-href="${tripFansEnviroment.serverUrl}/perfil/${perfil.urlPath}" data-send="false" data-layout="button_count" data-width="450" data-show-faces="false"></div>
                            </c:if>
        				</div>
        			</div>
        			
        			<div class="row">
        				<div class="span3 menu-lateral" id="menuLateral">
        					<%@include file="menuLateral.jsp" %>
        				</div>
        				
        				<div id="pageContent" class="sidebar_content" style="padding-right: 6px;">
                           <c:choose>
                        	<c:when test="${mostrarPerfil == true}">
        			            <div id="conteudo"></div>
        		            </c:when>
        		            <c:otherwise>
        		            	<div id="conteudo">
        			            	<div class="offset2 span9" style="margin-top: 150px;">
        			            		<div class="well">
        			            		<div class="span1"><img src="<c:url value="/resources/images/warn.png" />" /></div>
        			            		<div><p><strong>${perfil.primeiroNome}</strong> compartilha suas informa��es somente com os amigos.
        			            		<sec:authorize access="isAuthenticated()">
        			            			<c:if test="${perfilView.foiConvidadoPorQuemEstaVendo == false && perfilView.convidouQuemEstaVendo == false}">
        			            			<span id="spanConviteAmizade"> 
        			            				Se voc� conhece ${perfil.primeiroNome},	<a id="linkAmizade" href="#">envie um convite de amizade agora mesmo.</a></p>
        			            			</span>
        			            			</c:if>
        			            		</sec:authorize>
        			            		<sec:authorize access="isAnonymous()">
        			            			Para se tornar amigo de ${perfil.primeiroNome}, <a id="linkAmizade" href="#">entre</a> agora mesmo no TripFans.
        			            		</sec:authorize>
        			            		</div>
        			            		</div>
        			            	</div>
        		            	</div>
        		            </c:otherwise>
        		            </c:choose>
                        </div>
        			</div>
                </div>
            </c:otherwise>
        </c:choose>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>