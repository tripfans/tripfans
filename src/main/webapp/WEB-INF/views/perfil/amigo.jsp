<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="selectable" value="${usuario.id == perfil.id and not amizade.tripfans}" />
<c:if test="${not tripFansEnviroment.enableSocialNetworkActions}">
  <c:set var="selectable" value="false" />
</c:if>

<fan:card type="user" friendship="${amizade}" showActions="true" selectable="${selectable}" spanSize="7" cardStyle="margin: 8px;">
    <jsp:attribute name="info">
      <c:if test="${amizade.tripfans}">
        <c:if test="${not empty amizade.amigo.cidadeResidencia}">
          <div class="row" style="margin-left: 0px;">
            <fan:localLink local="${amizade.amigo.cidadeResidencia}" nomeCompleto="true" />
          </div>
        </c:if>
      </c:if>
    </jsp:attribute>
    <jsp:attribute name="aditionalInfo">

      <c:if test="${amizade.tripfans}">
        <div class="row" style="margin-left: 0px;">
            <c:if test="${amizade.amigo.quantidadeViagens != null && amizade.amigo.quantidadeViagens != 0}">
                <c:set var="texto" value="${amizade.amigo.quantidadeViagens == 1 ? 'viagem' : 'viagens'}" /> 
                <span style="margin-bottom: 10px;" title="J� publicou ${amizade.amigo.quantidadeViagens} ${texto}">
                    <span class="luggage" style="display: inline-block; height: 16px; width: 16px;" ></span>
                    <a href="<c:url value="/perfil/${amizade.amigo.urlPath}"/>/viagens">
                        ${amizade.amigo.quantidadeViagens} ${texto}
                    </a>
                </span>
                <span style="width: 5px;">&nbsp;</span>
            </c:if>
        </div>
      </c:if>
    </jsp:attribute>

    <jsp:attribute name="actions">
        <c:choose>
          <c:when test="${amizade.somenteFacebook}">
            <c:if test="${usuario.id == perfil.id}">
              <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
                <div class="btn-group right">
                    <a class="btn btn-mini action-button btn-info dropdown-toggle" data-toggle="dropdown" href="#" title="Voc� e ${amizade.nomeAmigo} j� s�o amigos no Facebook, mas se voce convid�-lo(a) para o TripFans voc�s poder�o compartilhar dicas, recomenda��es, fotos, planos de viagem e muito mais. ">
                        Convidar para o TripFans
                        <span class="caret"></span>
                    </a>
                    
                        <ul class="dropdown-menu">
                            <li>
                                <c:url var="urlReload" value="/perfil/amigos/${usuario.urlPath}" />
                                <a href="#" class="post-to-feed"
                                            data-url-reload="${urlReload}"
                                            data-url-loadto="conteudo"
                                            data-from="${perfil.idFacebook}" data-to="${amizade.idAmigoFacebook}" 
                                            data-name="Junte-se ao TripFans - A sua rede social de viagens" 
                                            data-description="O TripFans vai permitir a voc� fazer melhores escolhas na hora de viajar. Tudo feito com a ajuda de seus amigos." 
                                            data-link="${tripFansEnviroment.serverUrl}/entrar?ucv=${idUsuarioCriptografado}&idf=${amizade.idAmigoFacebook}" 
                                            data-picture="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfans-vertical.png">
                                    Postar convite no mural
                                </a> 
                            </li>
                            <li>
                                <a href="#" class="send-request" data-to="${amizade.idAmigoFacebook}">
                                    Enviar requisi��o de aplicativo
                                </a> 
                            </li>
                        </ul>
                    
                </div>
              </c:if>
            </c:if>
          </c:when>
          <c:otherwise>
             <div class="right">
               <c:set var="isAmigo" value="false" />
               <sec:authorize access="@securityService.isFriend('${amizade.idAmigo}')">
                 <c:set var="isAmigo" value="true" />
               </sec:authorize>
               <c:if test="${isAmigo}">
                 <c:url var="urlReloadIsFriend" value="/perfil/${usuario.urlPath}/amigos" />
                 <fan:isFriendButton cssButtonClassSize="mini" urlReload="${urlReloadIsFriend}" idUsuarioAmigo="${amizade.idAmigo}" 
                                     importadaFacebook="${amizade.importadaFacebook}" somenteFacebook="${amizade.somenteFacebook}" nomeAmigo="${amizade.nomeAmigo}"/>
               </c:if>
               <c:if test="${not isAmigo}">
                   <fan:addFriendButton usuario="${amizade.amigo.urlPath}" />
               </c:if>
             </div>
          </c:otherwise>
        </c:choose>
    </jsp:attribute>
</fan:card>