<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<%-- <c:set var="notificacao" value="${param.notificacao}" /> --%>

<c:if test="${notificacao.notificacaoAmizade == true}">
	<c:url var="urlNotificacao" value="/perfil/${notificacao.originador.urlPath}" />
</c:if>

<c:if test="${notificacao.notificacaoComentario == true}">
 
  <c:choose>
    <c:when test="${notificacao.acaoPrincipal.respostaComentario}">
      <c:set var="comentario" value="${notificacao.acaoPrincipal.comentarioPai}" />
    </c:when>
    <c:otherwise>
      <c:set var="comentario" value="${notificacao.acaoPrincipal}" />
    </c:otherwise>
  </c:choose>

  <c:if test="${comentario.comentarioAlbum}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.album.autor.urlPath}/albuns/${comentario.album.urlPath}#comentarios" />
  </c:if>
  <c:if test="${comentario.comentarioAvaliacao}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.avaliacao.autor.urlPath}/avaliacoes?destaque=${comentario.idObjetoComentado}" />
  </c:if>
  <c:if test="${comentario.comentarioDica}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.dica.autor.urlPath}/dicas?destaque=${comentario.idObjetoComentado}" />
  </c:if>
  <c:if test="${comentario.comentarioDiarioViagem}">
    <c:url var="urlNotificacao" value="/planoViagem/diario/${comentario.idObjetoComentado}" />
  </c:if>
  <c:if test="${comentario.comentarioFoto}">
    <c:url var="urlNotificacao" value="/fotos/${comentario.foto.urlPath}" />
  </c:if>
  <c:if test="${comentario.comentarioPerfil}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.usuario.urlPath}?tab=principal#${comentario.urlPath}" />
  </c:if>
  <c:if test="${comentario.comentarioRelatoViagem}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.urlPath}" />
  </c:if>
  <c:if test="${comentario.comentarioViagem}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.urlPath}" />
  </c:if>
</c:if>

<c:if test="${notificacao.notificacaoConvite == true}">
	<c:if test="${notificacao.acaoPrincipal.amizade == true}">
		<c:url var="urlNotificacao" value="/perfil/${notificacao.originador.urlPath}" />
		<c:url var="urlAceita" value="/aceitarConviteAmizade/${notificacao.originador.urlPath}" />
		<c:url var="urlIgnora" value="/ignorarConvite/${notificacao.acaoPrincipal.id}" />
	</c:if>
	<c:if test="${notificacao.acaoPrincipal.participacaoViagem == true}">
		<!-- definir para onde vai; provavelmente ir� para a p�gina de viagem, com a op��o de aceitar o convite" -->
		<c:url var="urlAceita" value="" />
		<c:url var="urlIgnora" value="" />
	</c:if>
</c:if>

<c:if test="${notificacao.notificacaoVotoUtil == true}">
  <c:set var="votoUtil" value="${notificacao.acaoPrincipal}" />
  <c:if test="${votoUtil.votoAvaliacao}">
    <c:url var="urlNotificacao" value="/perfil/${votoUtil.objetoVotado.autor.urlPath}/avaliacoes?destaque=${votoUtil.idObjetoVotado}" />
  </c:if>
  <c:if test="${votoUtil.votoDica}">
    <c:url var="urlNotificacao" value="/perfil/${votoUtil.objetoVotado.autor.urlPath}/dicas?destaque=${votoUtil.idObjetoVotado}" />
  </c:if>
  <c:if test="${votoUtil.votoRelatoViagem}">
    <c:url var="urlNotificacao" value="/perfil/${comentario.urlPath}" />
  </c:if>
  <c:if test="${votoUtil.votoResposta}">
    <c:url var="urlNotificacao" value="/perguntas/verPergunta/${votoUtil.resposta.pergunta.urlPath}/#respostas" />
  </c:if>
</c:if>

<c:if test="${notificacao.notificacaoResposta == true}">
    <c:url var="urlNotificacao" value="/perguntas/verPergunta/${notificacao.acaoPrincipal.pergunta.urlPath}/#respostas" />
</c:if>

<c:if test="${notificacao.notificacaoPedidoDica == true}">
    <%-- c:url var="urlNotificacao" value="/dicas/pedidoDica/listagemPedidosDicaAmigos" /--%>
    <c:url var="urlNotificacao" value="/viagem/${notificacao.acaoPrincipal.viagem.id}/ajudar/${notificacao.acaoPrincipal.id}/sugerirLocais" />
    <s:eval expression="notificacao.tipoAcao == T(br.com.fanaticosporviagens.model.entity.TipoAcao).RESPONDER_PEDIDO_DICA" var="respostaPedido" />
    <c:if test="${respostaPedido == true}">
    	<%-- c:url var="urlNotificacao" value="/dicas/pedidoDica/verResposta/${notificacao.acaoPrincipal.id}" /--%>
    	<c:url var="urlNotificacao" value="/viagem/${notificacao.acaoPrincipal.id}/planejamento/sugestoesAmigos" />
    </c:if>
</c:if>

<a href="${urlNotificacao}" class="link-notificacao ${notificacao.pendente ? 'pendente' : ''}" style="text-decoration: none;" data-notificacao-id="${notificacao.id}">
    <div class="notificacao-inner">
        <fan:userAvatar user="${notificacao.originador}" enableLink="false" displayName="false" displayDetails="${param.displayDetails}" marginLeft="10" imgSize="avatar"/>
        <span style="padding-left: 0px; color: #4D4D4D;">
            <b>${notificacao.originador.displayName}</b>
            ${notificacao.tipoAcao.descricaoAcaoNoPassado}
            <%-- ${notificacao.tipoAcao.preposicaoPosAcao} --%>
            ${notificacao.tipoAcao.pronome}
            ${notificacao.tipoAcao.descricaoAlvo}
            
            <c:if test="${notificacao.tipoAcao.preposicaoPosAlvo != ''}">
              ${notificacao.tipoAcao.preposicaoPosAlvo}
              <c:if test="${notificacao.acaoUsuario != null}">
                ${notificacao.acaoUsuario.alvo.descricaoAlvo}
              </c:if>
            </c:if>
        </span>
        <p>
            <small>
                <fan:passedTime date="${notificacao.dataNotificacao}"/>
            </small>
        </p>
        <c:if test="${notificacao.pendente and notificacao.notificacaoConvite and notificacao.acaoPrincipal.autor.id != usuario.id and not notificacao.acaoPrincipal.aceito and not notificacao.acaoPrincipal.ignorado}">
            <div style="float: right; padding-right: 3px;">
                <fan:inviteButtons urlAccept="${urlAceita}" cssClassAccept="btn btn-small btn-primary" titleAccept="Aceitar" urlIgnore="${urlIgnora}" cssClassIgnore="btn btn-small" titleIgnore="Ignorar" />
            </div>
            <div style="clear: both;"></div>
        </c:if>
    </div>
    <c:if test="${param.showSeparator}">
        <hr style="margin:0px; border-bottom: 0px; "/>
    </c:if>
</a>
