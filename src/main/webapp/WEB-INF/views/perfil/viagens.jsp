<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="tituloViagens" value="Viagens de ${perfil.primeiroNome}" />
<c:if test="${perfil.id == usuario.id}">
	<c:set var="tituloViagens" value="Minhas Viagens" />
</c:if>


    <script>
        jQuery(document).ready(function() {
            /*$('#criar-viagem-dialog-link').fancybox({
                'scrolling'     : 'no',
                'titleShow'     : false,
                'width'         : 360,
                'height'        : 800,
                'hideOnOverlayClick' : false,
                'onClosed'      : function() {
                    
                }
            });*/
            
            $('.excluir-viagem').click(function (e) {
                e.preventDefault();
                
                var idViagem = $(this).attr('data-id-viagem');
                
                var url = '<c:url value="/viagem/"/>' + idViagem + '/excluir/';
                
                $.ajax({
                    type: 'POST',
                    url: url,
                    success: function(data) {
                        if (data.error != null) {

                        } else {
                            var params = '';
                            if (data.params != null) {
                                params = data.params;
                            }
                            
                            if (params.success) {
                            	$('#conteudo').load('<c:url value="/perfil/" />viagens/${perfil.urlPath}');
                            }
                            
                        }
                    }
                });
            });
        });
    </script>

	<div class="span16">
	    <div class="page-header">
	    <c:choose>
	        <c:when test="${usuario != null and usuario.id == perfil.id}">
	            <h3>${tituloViagens}
	            
	              <div class="pull-right" style="align: top;">
<%--                   
	                <a id="criar-viagem-dialog-link" href="<c:url value="/planoViagem/wizard"/>" class="btn btn-mini btn-warning">
	                    <i class="icon-star icon-white"></i>
	                    <b>Criar um plano de viagem</b>
	                </a>

	                <a href="#" class="btn btn-mini btn-warning" title="Importar plano de viagens">
	                    <i class="icon-star icon-white"></i>
	                    <b>Importar plano de viagem</b>
	                </a>
                    <a id="criar-viagem-dialog-link" href="<c:url value="/planoViagem/diario/wizard"/>" class="btn btn-mini btn-warning">
                        <i class="icon-star icon-white"></i>
                        <b>Criar um di�rio de viagem</b>
                    </a>
--%>
 
	              </div>
	            </h3>
	        </c:when>
	        <c:otherwise>
	            <h3>Viagens de ${perfil.displayName}</h2>
	        </c:otherwise>
	    </c:choose>
	    
	    </div>
		<c:if test="${not empty viagens}">
            <c:set var="marginLeft" value="0"/>
            <c:set var="marginRight" value="10"/>
		    <%@include file="../planoViagem/listaViagens.jsp" %>
		</c:if>
		<c:if test="${empty viagens}">
		    <div style="height: 50px;"></div>
		    <div class="blank-state">
		        <div class="row">
		            <div class="span2 offset1">
		                <img src="<c:url value="/resources/images/icons/mini/128/Leisure-1.png" />" width="90" height="90" />
		            </div>
		            <div class="span8">
		                <h3>N�o h� viagens para exibir</h3>
		                <p>
		                    <c:choose>
		                      <c:when test="${usuario.id == perfil.id}">
		                          Voc�
		                      </c:when>
		                      <c:otherwise>
		                          Este usu�rio
		                      </c:otherwise>
		                    </c:choose>
		                    ainda n�o adicionou nenhuma viagem.
		                </p>
		                <c:if test="${usuario.id == perfil.id}">
		                  <p>
<%-- 		                    <a id="criar-viagem-dialog-link" href="<c:url value="/planoViagem/wizard"/>" class="btn btn-warning">
		                        <i class="icon-star icon-white"></i>
		                        <b>Criar viagem</b>
		                    </a> --%>
                            
                            <%-- a id="criar-viagem-dialog-link" href="<c:url value="/planoViagem/diario/wizard"/>" class="btn btn-warning">
                                <i class="icon-star icon-white"></i>
                                <b>Criar um di�rio de viagem</b>
                            </a--%>
                            
		                  </p>
		                </c:if>
		            </div>
		        </div>
		    </div>
		</c:if>
	</div>