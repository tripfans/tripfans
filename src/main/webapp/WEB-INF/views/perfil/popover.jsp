<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:choose>
  <c:when test="${not empty userPopover.urlFotoPerfil}">
    <c:set var="urlFotoPerfil" value="${userPopover.urlFotoPerfil}" />
  </c:when>
  <c:otherwise>
    <%--c:set var="urlFotoPerfil" value="${tripFansEnviroment.imagesServerUrl}/usuarios/${perfil.id}/FotoPerfil_album.jpg" /--%>
    <c:set var="urlFotoPerfil" value="${perfil.urlFotoAlbum}" />
  </c:otherwise>
</c:choose>

<div class='row'>
    <div class='span3 thumbnail'>
        <%-- img class='moldura' src='${urlFotoPerfil}' style='max-width: 94%; min-height: 158px;' /--%>
        <i class='carrossel-thumb' style='background-image: url(<c:url value="${urlFotoPerfil}"/>); max-width: 100%; min-height: 158px;'></i>
    </div>
    <div class='span3' style="margin-left: 3px; min-height: 160px;">
        <c:if test="${not empty userPopover.nomeCompleto}">
        	<div>
                Nome completo:<br/>
                <strong>${userPopover.nomeCompleto}</strong>
            </div>
        </c:if>
<%--         <c:if test="${not empty userPopover.cidadeNatal}">
            <hr style="margin: 2px 0;"/>
        	<div>
                Nasceu em
                <strong><fan:localLink local="${userPopover.cidadeNatal}" nomeCompleto="true" /></strong>
            </div>
        </c:if> --%>
        <c:if test="${not empty userPopover.cidadeResidencia}">
            <hr style="margin: 2px 0;"/>
        	<div>
                Mora em:<br/>
                <strong><fan:localLink local="${userPopover.cidadeResidencia}" nomeCompleto="true" /></strong>
            </div>
        </c:if>
        <c:if test="${userPopover.exibirGenero}">
            <hr style="margin: 2px 0;"/>
			<div>
                G�nero:<br/>
                <strong>
                    <c:choose>
                        <c:when test="${userPopover.genero == 'M'}"><s:message code="label.usuario.perfil.genero.masculino" /></c:when>
                        <c:otherwise><s:message code="label.usuario.perfil.genero.feminino" /></c:otherwise>
                    </c:choose>
                </strong>
			 </div>        
        </c:if>
        <div>
            <hr style="margin: 2px 0;"/>
            Membro desde:<br/>
            <strong><fan:date name="" id="" textOnly="true" value="${userPopover.dataCadastro}"/></strong>
        </div>
        
    </div>
    
</div>
        
<hr style="margin: 2px 0;"/>

<div class="row" style="margin-left: 0px;">
    <c:if test="${userPopover.quantidadeAvaliacoes != null && userPopover.quantidadeAvaliacoes != 0}">
        <c:set var="texto" value="${perfil.quantidadeAvaliacoes == 1 ? 'avalia��o' : 'avalia��es'}" />
        <div class="span2" style="margin-bottom: 10px;" title="J� escreveu ${userPopover.quantidadeAvaliacoes} ${texto}">
        <span class="award_star_gold_3" style="display: inline-block; height: 16px; width: 16px;" ></span>
        <a href="<c:url value="/perfil/${userPopover.urlPath}"/>/avaliacoes"><strong>${userPopover.quantidadeAvaliacoes} ${texto}</strong></a>
        </div>
    </c:if>
    <c:if test="${userPopover.quantidadeDicas != null && userPopover.quantidadeDicas != 0}">
        <c:set var="texto" value="${perfil.quantidadeDicas == 1 ? 'dica' : 'dicas'}" />
        <div class="span2" style="margin-bottom: 10px;" title="J� escreveu ${userPopover.quantidadeDicas} ${texto}">
        <span class="lightbulb" style="display: inline-block; height: 16px; width: 16px;" ></span>
        <a href="<c:url value="/perfil/${userPopover.urlPath}"/>/dicas"><strong>${userPopover.quantidadeDicas} ${texto}</strong></a>
        </div>
    </c:if>
    <c:if test="${userPopover.quantidadeViagens != null && userPopover.quantidadeViagens != 0}">
        <c:set var="texto" value="${perfil.quantidadeViagens == 1 ? 'viagem' : 'viagens'}" /> 
        <div class="span2" style="margin-bottom: 10px;" title="J� publicou ${userPopover.quantidadeViagens} ${texto}">
        <span class="luggage" style="display: inline-block; height: 16px; width: 16px;" ></span>
        <a href="<c:url value="/perfil/${userPopover.urlPath}"/>/viagens"><strong>${userPopover.quantidadeViagens} ${texto}</strong></a>
        </div>
    </c:if>
    <c:if test="${userPopover.quantidadeFotos != null && userPopover.quantidadeFotos != 0}">
        <c:set var="texto" value="${perfil.quantidadeFotos == 1 ? 'foto' : 'fotos'}" /> 
        <div class="span2" style="margin-bottom: 10px;" title="J� postou ${userPopover.quantidadeFotos} ${texto}">
        <span class="camera" style="display: inline-block; height: 16px; width: 16px;" ></span>
        <a href="<c:url value="/perfil/${userPopover.urlPath}"/>/fotos"><strong>${userPopover.quantidadeFotos} ${texto}</strong></a>
        </div>
	</c:if>
</div>
