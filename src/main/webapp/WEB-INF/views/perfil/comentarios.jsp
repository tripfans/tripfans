<%@page import="br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="page-header">
<h3>Comentários sobre meu perfil</h3>
</div>

<fan:comments tipo="<%=br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario.PERFIL.name()%>" id="perfil" idAlvo="${perfil.id}"/>