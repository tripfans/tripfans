<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>


html {
    margin: 0;
    overflow-x: hidden;
    padding: 0;
    width: 100%;
}
ol, ul {
    list-style: none outside none;
}
a {
    text-decoration: none;
}
#banner1-bg {
    background-image: url("../images/website_bg_tile.gif");
    color: #BBBBBB;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    font-weight: bold;
    height: 600px;
    left: 0;
    line-height: 20px;
    position: absolute;
    text-align: center;
    top: 0;
    width: 100%;
}
#banner1-bg h1 {
    color: #BBBBBB;
    font-family: 'PT Sans Narrow',sans-serif;
    font-size: 35px;
    margin-bottom: 15px;
}

#banner2-bg {
    background-image: url("../images/website_bg_tile_dark.gif");
    color: #DDDDDD;
    font-family: Arial,Helvetica,sans-serif;
    font-size: 12px;
    font-weight: bold;
    height: 580px;
    left: 0;
    line-height: 20px;
    padding-top: 20px;
    position: absolute;
    text-align: center;
    top: 600px;
    width: 100%;
}
#banner2-bg h1 {
    color: #DDDDDD;
    font-family: 'PT Sans Narrow',sans-serif;
    font-size: 35px;
    margin-bottom: 15px;
}
#banner2-bg p, #banner1-bg p {
    font-family: 'PT Sans Narrow',sans-serif;
    font-size: 17px;
}
.example-wrapper {
    height: 400px;
    margin: 50px auto auto;
    position: relative;
    width: 980px;
}
.example-wrapper2 {
    height: 400px;
    margin: 50px auto auto;
    position: relative;
    width: 761px;
}
#small_divider {
    margin-top: 8px;
}
#big_divider {
    margin-top: 150px;
}


.theme1 .main-container {
    background-color: #FFFFFF;
    border: 1px solid #E6E6E6;
    box-shadow: 0 0 5px 1px rgba(100, 100, 100, 0.1);
    padding: 30px;
}
.theme1 {
    color: #777777;
    font-family: Arial,sans-serif;
    font-size: 12px;
    line-height: 20px;
    text-shadow: 1px 1px 1px #FFFFFF;
}
.theme1 h2 {
    color: #999999;
    font-family: 'PT Sans Narrow',sans-serif;
    font-size: 18px;
    font-weight: normal;
    margin: 0;
    text-shadow: 1px 1px 1px #FFFFFF;
    width: 100%;
}
.theme1 .thumb {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #DDDDDD;
    height: auto;
    padding: 4px;
    position: relative;
    z-index: 50;
}
.theme1 .page-more {
    visibility: hidden;
}
.theme1 > ul {
    visibility: hidden;
}
.theme1 .big-image {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #DDDDDD;
    float: left;
    height: auto;
    margin-right: 30px;
    padding: 4px;
    position: relative;
    z-index: 50;
}
.theme1 .details {
    float: left;
    margin-right: 30px;
    position: relative;
    width: 160px;
}
.theme1 .details_double {
    float: left;
    margin-right: 30px;
    position: relative;
    width: 320px;
}
.theme1 .video_clip {
    border: 1px solid #DDDDDD;
    float: left;
    height: 280px;
    margin-right: 30px;
    padding: 4px;
    position: relative;
    width: 498px;
}
.theme1 .check {
    list-style: none outside none;
    margin-left: 0;
    padding-left: 0;
}
.theme1 .check li {
    background: url("../assets/check.png") no-repeat scroll left center transparent;
    line-height: 27px;
    list-style: none outside none;
    margin: 0;
    padding-left: 25px;
}
.theme1 .closer {
    background: url("../assets/button/close.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 30px;
    margin-right: -20px;
    margin-top: -20px;
    position: absolute;
    right: 0;
    top: 0;
    width: 30px;
    z-index: 10000;
}
.theme1 .closer:hover {
    background-position: center bottom;
    cursor: pointer;
    opacity: 0.5;
}
.theme2 .main-container {
    background-color: #222222;
    border: 1px solid #1E1E1E;
    border-radius: 5px 5px 5px 5px;
    padding: 20px;
}
.theme2 {
    color: #999999;
    font-family: Arial,sans-serif;
    font-size: 12px;
    line-height: 20px;
    text-shadow: 1px 1px 1px #000000;
}
.theme2 h2 {
    color: #DDDDDD;
    font-family: 'PT Sans Narrow',sans-serif;
    font-size: 17px;
    font-weight: bold;
    margin: 0;
    text-shadow: 1px 1px 1px #000000;
    width: 100%;
}
.theme2 .thumb {
    background: none repeat scroll 0 0 #FFFFFF;
    border: 1px solid #111111;
    height: auto;
    padding: 0;
    position: relative;
    z-index: 50;
}
.theme2 .page-more {
    visibility: hidden;
}
.theme2 > ul {
    visibility: hidden;
}
.theme2 .big-image {
    background: none repeat scroll 0 0 #222222;
    border: 1px solid #111111;
    float: left;
    height: auto;
    margin-right: 30px;
    position: relative;
    z-index: 50;
}
.theme2 .details {
    float: left;
    margin-right: 20px;
    position: relative;
    width: 140px;
}
.theme2 .details_double {
    float: left;
    margin-right: 20px;
    position: relative;
    width: 300px;
}
.theme2 .video_clip {
    border: 1px solid #111111;
    float: left;
    height: 300px;
    margin-right: 20px;
    position: relative;
    width: 533px;
}
.theme2 .check {
    list-style: none outside none;
    margin-left: 0;
    padding-left: 0;
}
.theme2 .check li {
    background: url("../assets/check.png") no-repeat scroll left center transparent;
    line-height: 27px;
    list-style: none outside none;
    margin: 0;
    padding-left: 25px;
}
.theme2 .closer {
    background: url("../assets/button/close_dark.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 30px;
    margin-right: -10px;
    margin-top: -10px;
    position: absolute;
    right: 0;
    top: 0;
    width: 30px;
    z-index: 10000;
}
.theme2 .closer:hover {
    background-position: center bottom;
    cursor: pointer;
    opacity: 0.5;
}
.buttonlight {
    border-radius: 5px 5px 5px 5px;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    margin-top: 10px;
}
.buttonlight:link, .buttonlight:visited {
    background: url("../assets/button/btn_light.png") repeat-x scroll center top #999999;
    border: 1px solid #CDCDCD;
    color: #777777;
    font-weight: bold;
    padding: 5px 20px;
    text-align: center;
    text-decoration: none;
    text-shadow: 1px 1px 0 #FFFFFF;
}
.buttonlight:hover, .comment-reply-link:hover {
    background: none repeat scroll 0 0 #333333;
    border: 1px solid #555555;
    color: #FFFFFF;
    text-decoration: none;
    text-shadow: 1px 1px 0 #000000;
}
.buttondark {
    border-radius: 5px 5px 5px 5px;
    float: left;
    font-size: 12px;
    height: 30px;
    line-height: 30px;
    margin-top: 10px;
}
.buttondark:link, .buttondark:visited {
    background: url("../assets/button/btn_dark.png") repeat-x scroll center top #999999;
    border: 1px solid #151515;
    color: #FFFFFF;
    font-weight: bold;
    padding: 0 20px;
    text-align: center;
    text-decoration: none;
    text-shadow: 1px 1px 0 #000000;
}
.buttondark:hover, .comment-reply-link:hover {
    background: none repeat scroll 0 0 #333333;
    border: 1px solid #555555;
    color: #FFFFFF;
    text-decoration: none;
    text-shadow: 1px 1px 0 #000000;
}
.toolbar {
    visibility: hidden;
}
.toolbar .left {
    background: url("../assets/button/left.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 45px;
    left: 0;
    margin-left: -29px;
    position: absolute;
    top: 60px;
    width: 30px;
    z-index: 100;
}

.theme2 .toolbar .left {
    background: url("../assets/button/left_dark.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 45px;
    left: 0;
    margin-left: -29px;
    position: absolute;
    top: 60px;
    width: 30px;
    z-index: 100;
}
.toolbar .left:hover {
    background-position: center bottom;
}
.toolbar .right {
    background: url("../assets/button/right.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 45px;
    margin-right: -30px;
    position: absolute;
    right: 0;
    top: 60px;
    width: 30px;
    z-index: 100;
}
.theme2 .toolbar .right {
    background: url("../assets/button/right_dark.png") no-repeat scroll center top transparent;
    cursor: pointer;
    height: 45px;
    margin-right: -30px;
    position: absolute;
    right: 0;
    top: 60px;
    width: 30px;
    z-index: 100;
}
.toolbar .right:hover {
    background-position: center bottom;
}
.hover-more-sign {
    background: url("../assets/button/more.png") no-repeat scroll 0 0 transparent;
    cursor: pointer;
    height: 50px;
    margin-left: -25px;
    margin-top: -25px;
    position: absolute;
    width: 50px;
}
.hover-blog-link-sign {
    background: url("../assets/button/link.png") no-repeat scroll 0 0 transparent;
    cursor: pointer;
    height: 50px;
    position: absolute;
    width: 50px;
}


</style>


<!-- ANIMATE AND EASING LIBRARIES -->
<script type="text/javascript" src="<c:url value="/resources/scripts/showbiz/jquery.easing.1.3.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/showbiz/jquery.cssAnimate.mini.js" />"></script>
<!-- TOUCH AND MOUSE WHEEL SETTINGS -->
<script type="text/javascript" src="<c:url value="/resources/scripts/showbiz/jquery.touchwipe.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/showbiz/jquery.mousewheel.min.js" />"></script>
<!-- jQuery SERVICES Slider -->
<script type="text/javascript" src="<c:url value="/resources/scripts/showbiz/jquery.themepunch.services.mini.js" />"></script> 


<div id="services-example-1" class="theme1">
<ul>
<!-- ############### - SLIDE 1 - ############### -->
<li>
<img class="thumb" src="images/services/service1.jpg" data-bw="images/services/service1_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Slide With Large Image</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<img class="big-image" width="498" height="280" src="images/services/large1.jpg">
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 2 - ############### -->
<li>
<img class="thumb" src="images/services/service2.jpg" data-bw="images/services/service2_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Youtube Video</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<iframe class="video_clip" src="http://www.youtube.com/embed/kjX-8kQmakk?hd=1&amp;wmode=opaque&amp;autohide=1&amp;showinfo=0" height="280" width="498" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 3 - ############### -->
<li>
<img class="thumb" src="images/services/service3.jpg" data-bw="images/services/service3_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Vimeo Video</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<iframe class="video_clip" src="http://player.vimeo.com/video/24456787?title=0&amp;byline=0&amp;portrait=0" width="498" height="280" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 4 - ############### -->
<li>
<img class="thumb" src="images/services/service4.jpg" data-bw="images/services/service4_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Design Your Layout</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<img class="big-image" width="498" height="280" src="images/services/large2.jpg">
<div class="details_double">
<h2>Just An Image & Text</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 5 - ############### -->
<li>
<img class="thumb" src="images/services/service5.jpg" data-bw="images/services/service5_bw.jpg">
<div style="margin-top:16px"></div>
<h2>No Image Just Text</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<div class="details">
<h2>Diverse Layouts</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="details_double">
<h2>Wide Text Block</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Short Text Block</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 6 - ############### -->
<li>
<img class="thumb" src="images/services/service1.jpg" data-bw="images/services/service1_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Slide With Large Image</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<img class="big-image" width="498" height="280" src="images/services/large1.jpg">
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 7 - ############### -->
<li>
<img class="thumb" src="images/services/service2.jpg" data-bw="images/services/service2_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Youtube Video</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<iframe class="video_clip" src="http://www.youtube.com/embed/kjX-8kQmakk?hd=1&amp;wmode=opaque&amp;autohide=1&amp;showinfo=0" height="280" width="498" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 8 - ############### -->
<li>
<img class="thumb" src="images/services/service3.jpg" data-bw="images/services/service3_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Vimeo Video</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<div class="details">
<h2>Our Strategy</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<iframe class="video_clip" src="http://player.vimeo.com/video/24456787?title=0&amp;byline=0&amp;portrait=0" width="498" height="280" frameborder="0" webkitAllowFullScreen allowFullScreen></iframe>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 9 - ############### -->
<li>
<img class="thumb" src="images/services/service4.jpg" data-bw="images/services/service4_bw.jpg">
<div style="margin-top:16px"></div>
<h2>Design Your Layout</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<img class="big-image" width="498" height="280" src="images/services/large2.jpg">
<div class="details_double">
<h2>Just An Image & Text</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="closer"></div>
</div>
</li>
<!-- ############### - SLIDE 10 - ############### -->
<li>
<img class="thumb" src="images/services/service5.jpg" data-bw="images/services/service5_bw.jpg">
<div style="margin-top:16px"></div>
<h2>No Image Just Text</h2>
<p>Lorem ipsum dolor sit amet, conseteetur sadipscing elitr,<br> sed diam monumy eirmod...</p>
<a class="buttonlight morebutton" href="#">View More</a>
<!--
***********************************************************************************************************
- HERE YOU CAN DEFINE THE EXTRA PAGE WHICH SHOULD BE SHOWN IN CASE THE "BUTTON" HAS BEED PRESSED -
***********************************************************************************************************
-->
<div class="page-more">
<div class="details">
<h2>Diverse Layouts</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
</div>
<div class="details">
<h2>Step Towards Success</h2>
<ul class="check">
<li>List Item Number One</li>
<li>List Item Number Two</li>
<li>List Item Number Three</li>
<li>List Item Number Four</li>
</ul>
<img src="images/certified.png">
</div>
<div class="details_double">
<h2>Wide Text Block</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
<a class="buttonlight" href="#">Visit Website</a>
</div>
<div class="details">
<h2>Short Text Block</h2>
<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labora et dolore magna.</p>
<p>At vero eou et accusam et justo duo dolores et ea rebum.<br>Stet clita kasd gubergen, no sea takimata sanctus est Lorem ipsum dolor.</p>
</div>
<div class="closer"></div>
</div>
</li>
</ul>
<!-- ############### - TOOLBAR (LEFT/RIGHT) BUTTONS - ############### -->
<div class="toolbar">
<div class="left"></div><div class="right"></div>
</div>
</div> 


<!--
##############################
- ACTIVATE THE BANNER HERE -
##############################
-->
<script type="text/javascript">
$(document).ready(function() {
	$.noConflict();
	jQuery('#services-example-1').services({
        width:920,
        height:290,
        slideAmount:4,
        slideSpacing:30,
        touchenabled:'off',
        mouseWheel:'off',
        hoverAlpha:'off',
        slideshow:3000,
        hovereffect:'off',
        transition : 5,
        callBack:function() { }
    });
});
</script> 