<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="span16">
    <div class="page-header">
        <h3>Pedidos de Dica</h3>
    </div>
    
    <c:if test="${quantidadePedidos == 0}">
        <div style="height: 50px;"></div>
        <div class="blank-state">
            <div class="row">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                </div>
                <div class="span8">
                    <h3>N�o h� pedidos de dica</h3>
                </div>
            </div>
        </div>
    </c:if>
    
    <c:if test="${quantidadePedidos > 0}">
    
		<div class="span12" style="margin-left: 0">
		  
			<c:forEach items="${pedidos}" var="pedido" varStatus="contador" >
				<div id="box-pedido-${pedido.id}" class="span12 page-container" style="background-color: #FFF; margin: 10px 0 10px; ">
					<div class="span2" style="margin-left: 0px; padding-top: 10px; padding-right: 10px;" align="center">
						<fan:userAvatar user="${pedido.autor}" displayDetails="true" displayName="false" orientation="vertical" imgFloat="center" imgSize="avatar" />
					</div>
					<div class="span10" style="margin: 0px 2px 0px 0px; padding: 10px 0 10px 7px;">
						<p class="lead">
							${pedido.autor.displayName} pediu dicas de viagem para voc� sobre <fan:localLink local="${pedido.localDica}" nomeCompleto="true" destaqueNomeLocal="true" />
						</p>
						
						<c:if test="${not empty pedido.mensagem}">
							<p class="textoUsuario">${fn:replace(pedido.mensagem, newLineChar, "<br />")}</p>
						</c:if>
						
						<c:if test="${not empty pedido.tiposPedidoDica}">
							<p>Pediu dicas sobre:
							<c:forEach items="${pedido.tiposPedidoDica}" var="tipoPedido">
							<span class="label label-info">${tipoPedido.descricao}&nbsp;</span>
							</c:forEach>
							</p>
						</c:if>
						
						<p><small>Pedido feito <fan:passedTime date="${pedido.dataCriacao}"/></small></p>
						
						<c:choose>
							<c:when test="${usuario.id == pedido.autor.id and pedido.respondido == false}">Ningu�m respondeu</c:when>
							<c:otherwise></c:otherwise>
						</c:choose>
					          
					</div>
					      
					<div class="span12" style="margin-left: 0px; background-color: rgb(244, 245, 244);" id="pedidoActions-${pedido.id}">
						<div style="border-bottom: 1px solid #eee; width: 100%;"></div>
						<div class="pull-right" style="padding-right: 6px; margin: 6px 0 6px;">
							<a href="<c:url value="/dicas/formRespostaPedidoDica/${pedido.id}" />" class="btn btn-primary">Responder</a>
						</div>
					</div>
				</div>
			</c:forEach>
		
		</div>
      
      <div class="span4">
    	<fan:adsBlock adType="1" />
      </div>
      
    </c:if>  
    
</div>

<script type="text/javascript">
jQuery(document).ready(function() {


});
</script>