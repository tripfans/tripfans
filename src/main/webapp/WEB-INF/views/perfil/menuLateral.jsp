<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.menu-fixo {
    position: fixed; 
    top: 70px; 
}
</style>

<script type="text/javascript">
        
    $(document).ready(function () {
        $("#sidebar-box").initToolTips();
        /*$('.sidebar-nav').stickyMojo({footerID: '#footer-content', contentID: '#pageContent', topSpacing: '70px'});*/
    });
    
</script>

    <c:set var="acao" value="${not empty param.acao ? param.acao : acao}" />

    <c:if test="${mostrarPerfil == true}">
    
    <div class="sidebar-nav pull-left" style="margin: 0 0 0px 0;">
      <div> 
        <div class="fotoPerfil">
            <c:choose>
                <c:when test="${perfil.fotoExterna}">
                    <c:if test="${perfil.conectadoFacebook}">
                        <c:url value="http://graph.facebook.com/${perfil.idFacebook}/picture?type=large" var="urlFotoPerfil" />
                    </c:if>
                    <c:if test="${perfil.importadoGoogle}">
                        <c:url value="${perfil.urlFotoAvatar}" var="urlFotoPerfil" />
                    </c:if>
                </c:when>
                <c:otherwise>
                    <c:url var="urlFotoPerfil" value="${perfil.urlFotoPerfil}" />
                </c:otherwise>
            </c:choose>        
            <img id="profile_pic" class="span3" src="${urlFotoPerfil}" />
        </div>
       	<div class="tabbable tabs-left" style="background-color: rgb(240, 244, 245); border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
       		<ul id="ulSidenav" class="nav nav-tabs span3" style="overflow: hidden; margin-left: 0px; margin-right: 0px; border-right: 0px; ">
		        <li class="blackAndWhite colorOnHover">
		            <a id="principal" href="#" class="menu-item">
		                <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
		                Informações
		            </a>
		        </li>
                <sec:authorize access="@securityService.isFriend('${perfil.id}')">
		          <li class="blackAndWhite colorOnHover">
		            <a id="viagens" href="#" class="menu-item"> 
		                <span style="background-image: url('<c:url value="/resources/images/icons/luggage.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		                Viagens <fan:menuCounter value="${perfil.quantidadeViagens}" />
		            </a>
		          </li>
                </sec:authorize>
		        <c:if test="${not empty interessesView.interessesViagem.listaInteressesViagem}">
		          <li class="blackAndWhite colorOnHover">
		            <a id="interesses" href="#" class="menu-item">
		                <span style="background-image: url('<c:url value="/resources/images/icons/world.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		                Interesses
		            </a>
		          </li>
		        </c:if>
                <sec:authorize access="@securityService.isFriend('${perfil.id}')">
		          <li class="blackAndWhite colorOnHover">
		            <a id="amigos" href="#" class="menu-item" data-static-params="acao=${acao}">
		                <span style="background-image: url('<c:url value="/resources/images/icons/group.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		                Amigos <fan:menuCounter value="${perfil.quantidadeAmigos}" badgeType="info" />
		            </a>
		          </li>
                </sec:authorize>
		        <sec:authorize access="@securityService.isFriend('${perfil.id}')">
		          <li class="blackAndWhite colorOnHover">
		            <a id="atividades" href="#" class="menu-item" data-static-params="start=0&limit=10"> 
		                <span style="background-image: url('<c:url value="/resources/images/icons/page_white_text.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		                Atividades <fan:menuCounter value="${perfil.quantidadeAtividades}" badgeType="info" />
		            </a>
		          </li>
                </sec:authorize>
		        <c:if test="${usuario.id == perfil.id}">
		          <li class="blackAndWhite colorOnHover">
		            <a id="notificacoes" href="#" class="menu-item" data-static-params="start=0&limit=10"> 
		                <span style="background-image: url('<c:url value="/resources/images/icons/exclamation.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
		                Notificações
		            </a>
		          </li>
		        </c:if>
		    </ul>
    		
    	</div>
      </div>
    </div>
    </c:if>
