<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div class="page-header">
        <h3>Minhas Avalia��es</h3>
    </div>
    
    <c:if test="${perfil.quantidadeAvaliacoes == 0}">
        <div style="height: 50px;"></div>
        <div class="blank-state">
            <div class="row">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                </div>
                <div class="span8">
                    <h3>N�o h� avalia��es para exibir</h3>
                    <p>
                        <c:choose>
                          <c:when test="${usuario.id == perfil.id}">
                              Voc�
                          </c:when>
                          <c:otherwise>
                              Este usu�rio
                          </c:otherwise>
                        </c:choose>
                        ainda n�o avaliou nenhum local.
                    </p>
                    <c:if test="${usuario.id == perfil.id}">
                      <p>
                        
                      </p>
                    </c:if>
                </div>
            </div>
        </div>
    </c:if>
    
    <c:if test="${perfil.quantidadeAvaliacoes > 0}">
    
      <div class="span12" style="margin-left: 0">
        <jsp:include page="/WEB-INF/views/perfil/listaAvaliacoes.jsp" />
      </div>
      
      <div class="span4">
    	<fan:adsBlock adType="1" />
      </div>
      
      <c:if test="${not unsuportedIEVersion}">
          <div id="terms-of-service" class="span12" style="margin-left: 0px;"></div>
          <script>
            var terms_widget = new panoramio.TermsOfServiceWidget('terms-of-service', {'width': '100%'});
          </script>
      </c:if>
      
    </c:if>  
    
</div>