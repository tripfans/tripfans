<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.joda.org/joda/time/tags" prefix="joda" %>

<div class="span16">
    <div class="page-header">
        <h3>Meus Locais pendentes de aprova��o</h3>
    </div>
    
    <c:if test="${empty locaisPendentes}">
        <div style="height: 50px;"></div>
        <div class="blank-state">
            <div class="row">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Places-18.png" />" width="90" height="90" />
                </div>
                <div class="span8">
                    <h3>N�o h� locais pendentes para exibir</h3>
                    <p>
                        <c:choose>
                          <c:when test="${usuario.id == perfil.id}">
                              Voc�
                          </c:when>
                          <c:otherwise>
                              Este usu�rio
                          </c:otherwise>
                        </c:choose>
                        ainda n�o cadastrou nenhum local.
                    </p>
                    <c:if test="${usuario.id == perfil.id}">
                      <p>
                        <a href="<c:url value="/locais/sugerirLocal"/>" class="btn btn-secondary">
                            <i class="icon-plus"></i> Sugira novos Locais
                        </a>
                      </p>
                    </c:if>
                </div>
            </div>
        </div>
    </c:if>
    
    <c:if test="${not empty locaisPendentes}">
    
      <div class="span12" style="margin-left: 0">
        <table class="table table-striped" style="width: 100%">
            <thead>
                <tr>
                  <th>#</th>
                  <th>Nome do Local</th>
                  <th>Tipo</th>
                  <th style="text-align: center;">Data do cadastro</th>
                  <%--th style="text-align: center;">Avalia��es</th>
                  <th style="text-align: center;">Dicas</th--%>
                  <th style="text-align: center;"></th>
                </tr>
            </thead>        
            <tbody>
                <c:forEach items="${locaisPendentes}" var="localPendente" varStatus="count">
                    <tr>
                        <td valign="middle">
                            <span>
                                ${count.index + 1} 
                            </span>
                        </td>
                        <td>
                            <span>
                                ${localPendente.nome}
                            </span>
                        </td>
                        <td>
                            <span>
                                ${localPendente.tipoLocal.descricao}
                            </span>
                        </td>
                        <td style="text-align: center;">
                            <span>
                                <joda:format value="${localPendente.dataCadastro}" pattern="dd/MM/yyyy" />
                            </span>
                        </td>
                        <%--td>
                            <span>
                                ${localPendente.nome}
                            </span>
                        </td>
                        <td>
                            <span>
                                ${localPendente.nome}
                            </span>
                        </td--%>
                        <td>
                            <div class="btn-group pull-right">
                                <a href="<c:url value="/avaliacao/avaliar/${localPendente.id}" />" class="btn btn-mini" title="Avaliar">
                                    <img src="<c:url value="/resources/images/icons/award_star_gold_3.png"/>"/>
                                </a>
                                <a href="<c:url value="/dicas/escrever/${localPendente.id}" />" class="btn btn-mini" title="Escrever Dica">
                                    <img src="<c:url value="/resources/images/icons/lightbulb.png"/>"/>
                                </a>
                            </div> 
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        
        <p>
            <a href="<c:url value="/locais/sugerirLocal"/>" class="btn btn-secondary">
                <i class="icon-plus"></i> Sugira novos Locais
            </a>
        </p>
        
      </div>
      
    </c:if>  
    
</div>