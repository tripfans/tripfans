<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.popup">
    <tiles:putAttribute name="body">
    <form id="contatosGmailForm" action="#">
    
    <c:if test="${not empty contatos}">
    
    <div class="page-header" style="margin: 3px 0;">
        <h3>Escolher contatos</h3>
    </div>
    <p>
        Encontramos <strong>${quantidadeContatos}</strong> contatos em sua conta.
        <br/>
        Selecione os contatos que voc� deseja convidar para fazerem parte da sua rede de amigos no TripFans.
    </p>
        
    <div class="subnav" style="width: 96%">
    
        <ul class="nav nav-pills">
    
            <li>
                <div class="left" style="margin-bottom: 9px; margin-top: 5px;">
                    <div class="input-append">
                        <input id="filtro-nome-contatos" class="filtro-nome-local" placeholder="Procure por seus contatos" />
                        <span class="add-on"><i class="icon-search"></i></span>
                    </div>
                </div>
            </li>
    
            <li>
                <div class="left" style="margin: 10px; font-size: 11px;">
                    <a id="contato-select-all" class="btn btn-mini" data-toggle="button">
                        Selecionar todos
                    </a>
                </div>
            </li>
            
            <li>
                <div class="left" style="margin: 10px; font-size: 11px;">
                    Com selecionados:
                </div>
            </li>
    
            <li>
                <div class="btn-toolbar left" style="margin-bottom: 9px">
                    <div class="btn-group">
                        <a class="btn btn-mini btn-info convidar-amigos-all" href="#">
                            <i class="icon-envelope icon-white"></i>
                            Convidar
                        </a>
                    </div>
                </div>
            </li>
    
        </ul>
        
    </div>
    <div style="height: 250px; clear:both; overflow: auto; border: 1px solid #DDD;">
    
        <table class="table" style="max-height:400px; width: 100%">
            <tbody>
                <c:forEach items="${contatos}" var="contato" varStatus="status">
                    <tr class="tr_contato" id="tr_contato">
                    
                        <td width="20">
                            <c:choose>
                                <c:when test="${contato.cadastradoTripfans}">
                                    <img src="<c:url value="/resources/images/tripfans-small.png" />" style="width: 16px; height: 16px;" title="${contato.nome} j� est� no TripFans!" />
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" data-index="${status.index}" id="check-contato-${status.index}" class="check-contato" data-email="${contato.email}" data-nome="${contato.nome}" />
                                    <input type="hidden" id="email-contato-${status.index}" name="contatos[0].email" value="${contato.email}" disabled="disabled">
                                    <input type="hidden" id="nome-contato-${status.index}" name="contatos[0].nome" value="${contato.nome}" disabled="disabled">
                                </c:otherwise>
                            </c:choose>
                        </td>
                       
                        <td valign="middle" width="40%">
                            <span>
                                ${contato.nome}
                            </span>
                        </td>
                        <td>
                            <span>
                                ${contato.email}
                            </span>
                        </td>
                        <td style="text-align: right;" width="15%">
                             <c:choose>
                                <c:when test="${contato.amigo}">
                                    <button class="btn btn-mini btn-success disabled" data-email-contato="${contato.email}">
                                        <i class="icon-ok icon-white"></i>
                                        Amigo
                                    </button>
                                </c:when>
                                <c:when test="${contato.cadastradoTripfans and not contato.amigo}">
                                    <button id="convite-amigo-${status.index}" class="btn btn-mini btn-primary convite-amigo" data-loading-text="Aguarde..." data-complete-text="Solicita��o enviada" data-index="${status.index}" data-email-contato="${contato.email}">
                                        <i class="icon-plus icon-white"></i>
                                        Adicionar como amigo
                                    </button>
                                </c:when>
                                <c:otherwise>
                                    <button id="convite-contato-tf-${status.index}" class="btn btn-mini convite-contato-tf" data-loading-text="Aguarde..." data-complete-text="Convite enviado" data-index="${status.index}" data-email-contato="${contato.email}">
                                        <i class="icon-envelope"></i>
                                        Convidar
                                    </button>
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
    
    <div style="width: 100%">
        <div align="right">
            <div class="form-actions">
                <a href="#" class="btn btn-primary fancy-box-close  convidar-amigos-all">
                    <i class="icon-ok icon-white"> </i>
                    Conclu�do
                </a>
                
                <div class="left">
                    <img src="<c:url value="/resources/images/tripfans-small.png" />" style="width: 16px; height: 16px;" />
                    Usu�rio do TripFans
                </div>
                
            </div>
        </div>
    </div>
    
    </c:if>
    </form>
    
    <script>
    $(document).ready(function() {
    
         
    });
    
    $('#filtro-nome-contatos').keyup(function(e) {
        
        var text = $(this).val();
        var regex = new RegExp(text, "i");
        $('table tr').each(function() {
            var $tr = $(this);
            if(!$tr.find('td').text().match(regex)){
                $tr.hide();
            } else {
                $tr.show();
            }
        });
    });
     
    $('.convidar-amigos-all').click(function (e) {
        e.preventDefault();
        enviarConviteContatosSelecionados();
    });
     
    $('.check-contato').click(function (e) {
        //$(#contatos[$(this).attr('data-index')].email).removeAttr('disabled');
        //$(contatos[$(this).attr('data-index')].nome).removeAttr('disabled');
    });
     
    $('#contato-select-all').click(function (e) {
        if ($(this).attr('class').indexOf('active') === -1) {
            $('.check-contato:visible:not(:disabled)').attr('checked', true);
        // verificar se passa pelo check-contato.click!
        } else {
            $('.check-contato:not(:disabled)').attr('checked', false);
        }
    });
     
    $('.convite-contato-tf').click(function(e) {
        e.preventDefault();
        var $botao = $(this);
        $botao.button('loading');
        
        var enderecosEmail = $botao.attr('data-email-contato');
        
        enviarConvitesEmails(enderecosEmail, '<c:url value="/perfil/convidarContatosPorEnderecosDeEmail" />', function() {
            $botao.addClass('btn-success');
            $botao.html('<i class="icon-ok icon-white"></i> ' + $botao.attr('data-complete-text'));
            // Desabilitando checkbox correspondente
            $('#check-contato-' + $botao.attr('data-index')).attr('disabled', true).attr('checked', true);
        });
    });

    $('.convite-amigo').click(function(e) {
        e.preventDefault();
        var $botao = $(this);
        $botao.button('loading');
        var email = $botao.data('email-contato');
        $.ajax({
            type: 'POST',
            url: '<c:url value="/perfil/solicitarAmizadeContato"/>',
            data: {
                enderecoEmail : email
            },
            success: function(data, status, xhr) {
                $botao.addClass('btn-success');
                $botao.html('<i class="icon-ok icon-white"></i> ' + $botao.attr('data-complete-text'));
                // Desabilitando checkbox correspondente
                $('#check-contato-' + $botao.attr('data-index')).attr('disabled', true).attr('checked', true);
            }
        });
    });
    
    $('.fancy-box-close').click(function(e) {
        e.preventDefault();
        parent.$.fancybox.close();
    });
    
    function enviarConviteContatosSelecionados() {
        var enderecosEmail = '';
        var indexesContatosSelecionados = new Array();
        var botaoContato = null;
        $('.check-contato:checked:not(:disabled)').each(function(i) {
            var index = $(this).attr('data-index');
            
            botaoContato = $('#convite-contato-tf-' + index);
            botaoContato.button('loading');
            
            indexesContatosSelecionados.push(index);
            enderecosEmail += $('#email-contato-' + index).val() + ',';
        });
        
        if(enderecosEmail != '') {
	        enviarConvitesEmails(enderecosEmail, '<c:url value="/perfil/convidarContatosPorEnderecosDeEmail" />', function() {
	            for (var i = 0; i < indexesContatosSelecionados.length; i++) {
	                botaoContato = $('#convite-contato-tf-' + indexesContatosSelecionados[i]);
	                botaoContato.addClass('btn-success');
	                botaoContato.html('<i class="icon-ok icon-white"></i> ' + botaoContato.attr('data-complete-text'));
	                $('#check-contato-' + botaoContato.data('index')).attr('disabled', true).attr('checked', true);
	            }
	        });
        }
        
        /*jQuery.ajax({
            type: 'POST',
            url: '<c:url value="/perfil/convidarContatosPorEnderecosDeEmail" />',
            //data: $.toJSON(contatosSelecionados),
            //data: formData,
            data: {
                enderecosEmail : enderecosEmail
            },
            success: function(data) {
                if (data.error != null) {
                } else {
                    for (var i = 0; i < indexesContatosSelecionados.length; i++) {
                        botaoContato = $('#convite-contato-tf-' + indexesContatosSelecionados[i]);
                        // desabilitar botao
                        botaoContato.addClass('btn-success');
                        botaoContato.addAttr('disabled', 'disabled');
                        botaoContato.button('complete');
                    }
                }
            }
        });*/
    }
    
    
    </script>
    </tiles:putAttribute>
</tiles:insertDefinition>