<%@page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://htmlcompressor.googlecode.com/taglib/compressor" prefix="compress" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>


<tiles:insertDefinition name="tripfans.responsive.new">
	<tiles:putAttribute name="title" value="Destinos para sua viagem - TripFans" />

	<tiles:putAttribute name="stylesheets">
		<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
				<c:set var="reqScheme" value="https" />
			</c:when>
			<c:otherwise>
				<c:set var="reqScheme" value="http" />
			</c:otherwise>
		</c:choose>

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>


		<meta property="og:title" content="TripFans | Fan�ticos por Viagens" />
		<meta property="og:url" content="http://www.tripfans.com.br" />
		<meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
		<meta property="og:site_name" content="TripFans" />
		<meta property="og:description" content="Encontre o destino para a sua pr�xima viagem!" />

	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="horizontalSpacer"></div>
		<div class="col-md-12">
			<p style="font-size: 19px;">
			Abaixo est� a lista de destinos atualmente suportados pelo TripFans. S�o para estes destinos que voc� pode criar seu roteiro de viagem.
			Um destino torna-se suportado quando ele foi validado por nossa equipe. Para estes destinos voc� encontra lugares para visitar, restaurantes e
			locais para ficar. Voc� recebe, ainda, lista de recomenda��es de locais para visitar. Esta lista est� sempre em constante atualiza��o.
			A nossa equipe est� sempre trabalhando para adicionar novos destinos.
			</p>
		</div>
		
		<div class="horizontalSpacer"></div>
		
		<div class="col-md-12">
			<c:forEach items="${destinos}" var="destino">
				<c:url var="linkDestino" value="/destino/${destino.urlPath}"/>
				<div class="col-md-3">
					<div class="card borda-arredondada module-box-content-list">
						<div class="card-header"> 
							<h3> <a href="<c:url value="/destino/${destino.urlPath}" />" class="card-title"> ${destino.nome} </a> </h3> 
						</div> 
						<div class="card-content" style="height: 180px;">
							<a href="<c:url value="/destino/${destino.urlPath}" />">
							<img  style="width: 100%; height: 100%" src="${destino.urlFotoAlbum}" />
							</a>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		
		<compress:js enabled="true" jsCompressor="closure" >
		<script type="text/javascript">
            $(document).ready(function() {


            })

            function scrollToTop() {
	            $('html, body').animate({
		            scrollTop : 0
	            }, 'slow');
            }

		</script>
		</compress:js>
	</tiles:putAttribute>

</tiles:insertDefinition>