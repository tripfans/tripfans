<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Escreva uma Avaliação - ${local.nome} - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				$("#avaliacaoForm").validate({
			        rules: {
			            titulo: "required",
			            mesVisita: "required",
			            anoVisita: "required",
			            descricao: {
			            	required: true,
			            	minlength: 100
			            }
			        },
			        messages: { 
			            titulo: "<s:message code="validation.avaliacao.titulo.required" />",
			            mesVisita: "<s:message code="validation.avaliacao.mesVisita.required" />",
			            anoVisita: "<s:message code="validation.avaliacao.anoVisita.required" />",
			            descricao: {
			            	required: "<s:message code="validation.avaliacao.descricao.required" />",
			            	minlength: "<s:message code="validation.avaliacao.descricao.minlength" />"
			            }
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.control-group")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });

			 	$("#avaliacaoForm").initToolTips();

			 	$('#observacoes').stickySidebar();

			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
		<fan:breadCrumb generator="${local}" />

	
        <div class="container-fluid">
            <div class="block">         
				<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
		            <h2><s:message code="avaliacao.titulo" arguments="${local.nome}" /></h2>
		        </div>
		        
				<div class="block_content">
					<div class="row">
						<div class="span12">
						
								<form:form modelAttribute="avaliacao" id="avaliacaoForm" action="${pageContext.request.contextPath}/avaliacao/avaliar/incluir" method="post" enctype="multipart/form-data">
									<s:hasBindErrors name="avaliacao">
										<div id="errorBox" class="alert alert-error centerAligned"> 
										  <p><s:message code="validation.containErrors" /></p> 
										</div>
									</s:hasBindErrors>
									<div id="errorBox" style="display: none;" class="alert alert-error centerAligned"> 
										 <p id="errorMsg">
										<s:message code="validation.containErrors" />
										</p>
									</div>
									
									<input type="hidden" name="localAvaliado" value="${local.id}" />
									
									<div class="accordion-group">
										<div class="accordion-heading collapsible">
										    <span class="accordion-toggle">
										        Dados básicos de sua avaliação 
										    </span>
										</div>
										
										<div id="group-avaliacao" class="accordion-body collapse in" style="">
										<div class="accordion-inner">
										
										<div class="control-group">
										<label for="tituloAvaliacao">
										<span class="required">
										<s:message code="avaliacao.label.titulo" />
										</span>
										<s:message code="avaliacao.tooltip.titulo" var="tooltipTitulo" />
										<i class="icon-question-sign helper" title="${tooltipTitulo}"></i>
										</label>
										<form:errors cssClass="error" path="titulo" element="label"/> 
										<div class="controls">
										<form:input id="tituloAvaliacao" path="titulo" cssClass="span8" />
										</div>
										</div>
										
										<div class="control-group">
										<label>
										<span  class="required"><s:message code="avaliacao.label.dataVisita" /></span>
										<i class="icon-question-sign helper" title="Informe o mês e ano de sua visita"></i>
										</label> 
										<form:select cssClass="span2" path="mesVisita" id="mesVisita" >
											<form:option value="" label="" />
											<c:forEach var="mes" items="<%= Mes.getMesesOrdenados() %>">
												<form:option value="${mes}" label="${mes.descricao}">${mes.descricao}</form:option>
											</c:forEach>
										</form:select>
										<form:select cssClass="span2" path="anoVisita" id="anoVisita">
											<form:option value="" label="" />
											<c:forEach var="ano" items="${anosVisita}">
												<form:option value="${ano}" label="${ano}" />
											</c:forEach>
										</form:select>
										</div>
										
										<div class="control-group">
										<label for="textarea">
										<span class="required"><s:message code="avaliacao.label.descricao" /></span>
										<s:message code="avaliacao.tooltip.descricao" var="tooltipDescricao" />
										<i class="icon-question-sign helper" title="${tooltipDescricao}"></i>
										</label>
										<form:errors cssClass="error" path="descricao" element="label"/> 
										<div class="controls">
										<form:textarea cssClass="input-xxlarge" id="textarea" path="descricao"  ></form:textarea>
										</div>
										</div>
										</div>
										</div>
									</div>

									<div class="horizontalSpacer"></div>
									<h4><s:message code="avaliacao.textoExplicativoCamposOpcionais" /></h4>
									<div class="horizontalSpacer"></div>
									
									 <div class="accordion-group">
									 		<div class="accordion-heading collapsible">
											    <span class="accordion-toggle">
											        <s:message code="avaliacao.legend.classificacaoItens" />
											        <div class="header-actions">
											            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-criterios"></i>
											        </div>
											    </span>
											</div>
											
											<div id="group-criterios" class="accordion-body collapse in" style="height: auto;">
												 <div class="accordion-inner">
												 		<c:forEach var="criterio" items="${criteriosAvaliacao}">
															<div class="control-group">
															<label for="classificacaoServicos" class="large">${criterio.descricao}</label>
															<form:errors cssClass="error" path="${criterio.nomeAtributo}" element="label"/> 
															<c:forEach var="qualidade" items="${avaliacoesQualidade}">
																<form:radiobutton path="${criterio.nomeAtributo}" id="${criterio.nomeAtributo}" value="${qualidade}" />${qualidade.descricao}&nbsp;&nbsp;
															</c:forEach>
															</div>
														</c:forEach>
												 </div>
											</div>
									</div>
									
									<div class="horizontalSpacer"></div>
									
									 <div class="accordion-group">
									 		<div class="accordion-heading collapsible">
											    <span class="accordion-toggle">
											        <s:message code="avaliacao.legend.fotos" />
											        <div class="header-actions">
											            <i class="icon-chevron-down icon-white" data-toggle="collapse" data-target="#group-fotos"></i>
											        </div>
											    </span>
											</div>
											
											<div id="group-fotos" class="accordion-body collapse in" style="height: auto;">
												 <div class="accordion-inner">
												 		<c:forEach  var="i" begin="0" end="4" step="1">
														 	<div class="control-group">
																<label for="fotos${i}.arquivoFoto" class="large"><s:message code="avaliacao.label.foto" /></label>
																<form:errors path="fotos[${i}].arquivoFoto" cssClass="error" element="label" />
																 
																<s:message code="avaliacao.tooltip.foto"  var="tooltipFoto"/>
																<s:message code="avaliacao.tooltip.descricaoFoto" var="tooltipDescricao" />
																<input id="fotos${i}.arquivoFoto"  name="fotos[${i}].arquivoFoto" type="file"  onchange="enableFields('descricaoFoto_${i}');">
															</div>
															<div class="control-group">
																<label for="descricaoFoto_${i}" class="large"><s:message code="avaliacao.label.descricaoFoto" /></label>  
																<form:input cssClass="span9" disabled="true" id="descricaoFoto_${i}" path="fotos[${i}].descricao"  />
															</div>
														 </c:forEach>
												 </div>
											</div>
											
									</div>
									
									<div class="control-group">
									<input type="checkbox"  checked="checked"  id="avaliacaoSegura" onclick="enableDisableField('botaoSalvar');"/>
									<span class="required"> 
									<s:message code="avaliacao.termoDeCompromisso" />
									</span>
									</div>
									
									<div class="form-actions rightAligned">
									<s:message code="avaliacao.botaoSalvar" var="labelBotaoSalvar" />
									<input type="submit" id="botaoSalvar" class="btn btn-large btn-primary" value="${labelBotaoSalvar}" >
									</div>
								</form:form>
						
						</div>
						
						<div class="span7">
							<div class="alert block-message alert-info">
						 		<strong class="large"><s:message code="fanaticos.compromissoInformacoesUteis.titulo" /></strong>
						 		<div class="horizontalSpacer"></div>
								<p><s:message code="fanaticos.compromissoInformacoesUteis.parte1" /></p>
								<p><s:message code="fanaticos.compromissoInformacoesUteis.parte2" /></p>
								<strong class="large"><s:message code="avaliacao.dicasPreenchimento" /></strong>
								<ul>
									<li><s:message code="formulario.camposObrigatorios" /></li> 
									<li><s:message code="foto.texto.tamanhoMaximo" arguments="${tamanhoMaximoFoto}" /></li>
									<li><s:message code="foto.texto.resolucaoMaxima" arguments="${resolucaoMaximaFoto}" /></li>
								</ul>
							</div>
						</div>
			 		</div>
		 		</div>
 		</div>
	</div>
	</tiles:putAttribute>

</tiles:insertDefinition>