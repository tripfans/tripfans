<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16" style="margin-top: -15px;">
	<div class="page-header">
		<c:if test="${not empty avaliacoes && local.quantidadeAvaliacoes > 1}">
		<c:url var="urlAvaliacao" value="/avaliacao/listar/${local.urlPath}" />
		<fan:groupOrderButtons url="${urlAvaliacao}" targetId="listaAvaliacoes" cssClass="pull-right span7">
			<fan:orderButton dir="desc" orderAttr="avaliacao.dataPublicacao" title="Mais recentes" tooltip="Ordenar pelas avaliações mais recentes" />
			<fan:orderButton dir="desc" orderAttr="avaliacao.quantidadeVotoUtil" title="Mais Úteis" tooltip="Ordenar pelas avaliações mais úteis" />
			<fan:orderButton dir="desc" orderAttr="avaliacao.classificacaoGeral" title="Classificação Geral" tooltip="Ordenar pela classificação geral" />
		</fan:groupOrderButtons>
		</c:if>
		<h3>
          Avaliações no <img src="<c:url value="/resources/images/logos/tripfans-t.png"/>" width="110" style="margin-top: -3px;" />
        </h3>
	</div>
	<div class="span12" style="margin-left: 0px;">
    
      <c:if test="${avaliacaoDestaque != null}">
          <fan:avaliacao fullSize="12" userSize="2" dataSize="10" avaliacao="${avaliacaoDestaque}" mostraTextoAvaliacaoSobre="true" mostraUsuarioAvaliador="true" mostraAcoes="true" />
      </c:if>
    
	  <c:choose>
		<c:when test="${not empty avaliacoes}">
			<jsp:include page="listaAvaliacoes.jsp" />
		</c:when>
		<c:otherwise>
    
            <div class="blank-state">
                <div class="span2 offset2">
                    <img src="<c:url value="/resources/images/icons/mini/128/Vote-50.png" />" width="90" height="90" />
                </div>    
                <div class="span6" style="margin-top: 20px;">
                    <h3>Não há avaliações para exibir</h3>
                    <p>
                        Até o momento, não foram feitas avaliações para este local.
                    </p>
                    <a class="btn btn-secondary" href="${pageContext.request.contextPath}/avaliacao/avaliar/${local.urlPath}">Seja o primeiro a avaliar</a>
                </div>
            </div>    

            <div class="horizontalSpacer"></div>

		</c:otherwise>
	  </c:choose>
	  
	  <c:if test="${not empty avaliacoes}">
		<jsp:include page="/WEB-INF/views/locais/botoesAcaoLocais.jsp">
			<jsp:param name="span" value="span12" />
		</jsp:include>
	  </c:if>
      
      <c:if test="${not empty local.idGooglePlaces}">
        <div class="span12" style="margin: -10px 10px 20px 0px;">
            <div class="page-header">
                <h3>
                  Avaliações do 
                  <img src="<c:url value="/resources/images/logos/google.png"/>" width="110" style="margin-top: -3px;" />
                </h3>
            </div>
            
            <div id="lista-google-reviews" class="span12" style="margin-left: 0px;">
                <div class="span6" style="margin-top: 20px;">
                    <p>
                        <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
                        Carregando avaliações do <img src="<c:url value="/resources/images/logos/google.png"/>" width="75" style="margin-top: -3px;" /> ...
                    </p>
                </div>
            </div>
            <div class="pull-right">
                <img src="<c:url value="/resources/images/logos/powered-by-google-on-white.png" />" width="104" height="16" />
            </div>
            
        </div>
        
        <script>
            jQuery(document).ready(function() {
                $.get('<c:url value="/avaliacao/google/listar/${local.urlPath}"/> ',{},function(response) {
                    $('#lista-google-reviews').html(response);
                    $('#lista-google-reviews').initToolTips();
                });
            });
        </script>
    </c:if>
	  
	</div>
	
    
	<div class="span4">
      <c:if test="${not empty avaliacoes}">
		<div style="text-align: center;">
		    <a class="btn btn-secondary btn-large" href="${pageContext.request.contextPath}/avaliacao/avaliar/${local.urlPath}">Escreva uma Avaliação</a>
		</div>
      </c:if>
      <div class="horizontalSpacer"></div>
      <fan:adsBlock adType="1" />
  	</div>
</div>