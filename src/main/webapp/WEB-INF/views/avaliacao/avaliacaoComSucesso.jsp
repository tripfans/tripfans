<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Muito obrigado por sua avaliação" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$("#breadCrumb").jBreadCrumb({
					beginingElementsToLeaveOpen : 7
				});
				
			});
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<fan:breadCrumb generator="${local}" />
 			
        <div class="container-fluid">
            <div class="block">         
				<div class="block_content" style="border-top: 1px solid #CCC;">
					<jsp:include page="/WEB-INF/views/agradecimentoCompartilhamentoContribuicao.jsp">
           				<jsp:param name="texto" value="avaliação" />
           				<jsp:param name="local" value="${local}" />
           				<jsp:param name="textoPromocao" value="Obrigado! A avaliação que você acabou de escrever conta pontos para a promoção do Tablet. Avalie mais locais e ganhe mais pontos!" />
           				<jsp:param name="redirectUrlPosFacebookConnection" value="${tripFansEnviroment.serverUrl}/avaliacao/avaliacaoComSucesso?postFacebook=true&avaliacao=${avaliacao.urlPath}&urlPathLocal=${local.urlPath}&nomeLocal=${local.nome}" />
           				<jsp:param name="postFacebookUrl" value="${tripFansEnviroment.serverUrl}/avaliacao/compartilharFacebook/${avaliacao.urlPath}" />
           				<jsp:param name="redirectUrlPosTwitterConnection" value="${tripFansEnviroment.serverUrl}/avaliacao/avaliacaoComSucesso?postTwitter=true&avaliacao=${avaliacao.urlPath}&urlPathLocal=${local.urlPath}&nomeLocal=${local.nome}" />
           				<jsp:param name="postTwitterUrl" value="${tripFansEnviroment.serverUrl}/avaliacao/compartilharTwitter/${avaliacao.urlPath}" />
           			</jsp:include>
				</div>
			</div>
		</div>
			
			
	</tiles:putAttribute>

</tiles:insertDefinition>