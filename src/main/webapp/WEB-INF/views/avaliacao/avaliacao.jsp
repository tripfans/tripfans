<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Escreva uma Avaliação - ${local.nome} - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script src="<c:url value="/resources/scripts/readmore.min.js" />" type="text/javascript"></script>
	
		<script type="text/javascript">
			function mostraPopUpNaoPodeAvaliar() {
				  $('#modal-avaliacao-proibida').modal({
					  keyboard: false,
					  backdrop: 'static'
				  });
			}
		
			jQuery(document).ready(function() {
				var naoPodeAvaliar = ${naoPodeAvaliar}; 
				if(naoPodeAvaliar) {
					mostraPopUpNaoPodeAvaliar();
				}
				else {
					$("#avaliacaoForm").validate({
						ignore: [], // para não ignorar hidden
				        rules: {
				        	classificacaoGeral: "required",
				            titulo: "required",
				            mesVisita: "required",
				            anoVisita: "required",
				            descricao: "required"
				            <c:if test="${local.tipoLocalEnderecavel}">
				            	,avaliacaoSegura: "required"
				            </c:if>
				        },
				        messages: { 
				        	classificacaoGeral: "Dê uma nota",
				            titulo: "<s:message code="validation.avaliacao.titulo.required" />",
				            mesVisita: "<s:message code="validation.avaliacao.mesVisita.required" />",
				            anoVisita: "<s:message code="validation.avaliacao.anoVisita.required" />",
				            descricao: "Coloque um texto que descreva sua avaliação"
				            <c:if test="${local.tipoLocalEnderecavel}">
				            ,avaliacaoSegura: "Favor marcar o termo de compromisso "
				            </c:if>
				        },
				        errorPlacement: function(error, element) {
				        	if($(element).attr('name') == 'avaliacaoSegura') {
				        		var parent = element.parents("div")[0];
				        		$(parent).css('border', '2px solid red');
				        		error.appendTo(parent);
				        	} else {
				        		var parent = element.parents("li")[0];
				        		error.appendTo(parent);
				        	}
				        },
				        invalidHandler: function(form, validator) {
				        	$.scrollTo($('.block_head'), {offset: -50, duration: 500} );
				        },
				        errorContainer: "#errorBox"
				    });
					
					<c:if test="${not empty avaliacoesRecentes}">
					var divs = $('div#avaliacoesRecentes').find('div.avaliacao-descricao');
					for(var i = 0; i < divs.length; i++) {
						$(divs[i]).readmore({
							maxHeight: 50,
							speed: 200,
							moreLink: '<strong><a href="#">Ler mais</a></strong>',
							lessLink: '<strong><a href="#">Fechar</a></strong>'
						});
					}
					</c:if>
					
					$("#btnComoEscreverAvaliacoes").fancybox({
				 		'scrolling'		: 'no',
				 		'titleShow'		: false
				 	});
				 	
				 	$('#botao-cancelar-avaliacao').click(function(e) {
				 		$('#modal-cancelar-avaliacao').modal('show');
				 	})
				}
				
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	
        <div class="container-fluid">
            <div class="block">         
				<div class="block_head">
                    <div class="bheadl">
                    </div>
                    <div class="bheadr">
                    </div>
		            <h2><s:message code="avaliacao.titulo" arguments="${local.nome}" /></h2>
		        </div>
		        
	          	<div id="modal-avaliacao-proibida" class="modal hide fade">
	                <div class="modal-header">
	                    <h2>Atenção</h2>
	                </div>
	                <div class="modal-body">
	                    <p class="lead red">Você avaliou <strong>${local.nome}</strong> há menos de 30 dias. 
	                    <br/><strong>Não é permitido</strong> que o usuário avalie o mesmo local <strong>em menos de 30 dias.</strong></p>
	                </div>
	                <div class="modal-footer">
	                	<c:url var="urlVoltar" value="/locais/${local.urlPath}" />
	                	<c:if test="${urlRedirect != null}"><c:set var="urlVoltar" value="${urlRedirect}" /></c:if>
	                    <a id="confirmar-nao-pode-avaliacao" href="${urlVoltar}" class="btn btn-primary">
	                        <i class="icon-ok icon-white"></i> 
	                        Ok
	                    </a>
	                </div>
				</div>
		        
				<div class="block_content">
					<div class="row">
						<div class="span13">
							<c:url var="action" value="/avaliacao/avaliar/incluir" />
							<form  id="avaliacaoForm" action="${action}" method="post" enctype="multipart/form-data">
								<s:hasBindErrors name="${modelAttribute}">
									<div id="errorBox" class="alert alert-error centerAligned"> 
									  <p><s:message code="validation.containErrors" /></p> 
									</div>
								</s:hasBindErrors>
								<fan:bodyFormAvaliacao criteriosAvaliacao="${criteriosAvaliacao}" local="${local}" />
							</form>
						</div>
						
						<div class="span6">
							<div style="text-align: center;">
								<a id="btnComoEscreverAvaliacoes" class="btn btn-large btn-secondary" href="#comoEscreverAvaliacoes">Como escrever uma boa avaliação?</a>
							</div>
							<div class="horizontalSpacer"></div>
							<c:if test="${not empty avaliacoesRecentes}">
							 	<div id="avaliacoesRecentes" class="borda-arredondada page-container" style="padding: 5px;">
							 			<div class="page-header" style="margin: 0px;">
											<h3>
												Últimas Avaliações sobre este local
											</h3>
										</div>
										<div class="module-box-content-list">
											<ul class="list">
												<c:forEach items="${avaliacoesRecentes}" var="avaliacao">
													<li style="min-height: 80px;">
														<div style="padding: 3px 6px 1px 6px;">
															<div class="left">
																<c:url var="urlPerfil" value="/perfil/${avaliacao.autor.urlPath}" />
																<fan:userAvatar showFrame="false" imgSize="avatar" displayDetails="true"
																	user="${avaliacao.autor}" displayName="false" marginLeft="0"
																	marginRight="8" />
															</div>
															
															<div style="overflow: hidden; word-wrap: break-word; max-width: 100%; margin-bottom: 0px; text-align: justify;">
																<p style="margin-bottom: 0px;">
																	<strong>${avaliacao.autor.nome}</strong> avaliou <strong>${avaliacao.localAvaliado.nome}</strong>
																	<c:if test="${not empty avaliacao.avaliacoesClassificacaoAplicaveis}"><span title="${avaliacao.classificacaoGeral.descricao}" class="starsCategoryScaled cat${avaliacao.classificacaoGeral.codigo}"></span></c:if>
																	<small>
																		Visitou em ${avaliacao.mesVisita.descricao} de ${avaliacao.anoVisita} e avaliou <fan:passedTime date="${avaliacao.dataAvaliacao}"/>
																	</small>
																	<div class="avaliacao-descricao" id="${avaliacao.urlPath}">
																		<p style="margin-bottom: 0px; margin-top: 5px; overflow: hidden; line-height: 14px;">
																			${avaliacao.descricao}
																		</p>
																	</div>
																</p>
															</div>
														</div>
													</li>
												</c:forEach>
											</ul>
										</div>
							 	</div>
							</c:if>
						</div>
						
				<div style="display: none;">
					<div class="span9 lead" id="comoEscreverAvaliacoes">
						<h2>Algumas dicas para escrever uma boa avaliação</h2>
						<div style="padding: 15px; font-size: 18px;">
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
							 	Deixe sua <strong>opinião</strong> sobre o local, a <strong>experiência vivida</strong>, o que gostou e o que não gostou.
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								<strong>Dê uma nota</strong> baseado na sua experiência geral
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								<strong>Dê um título</strong> que descreva, de forma clara e objetiva, a sua experiência nesse local
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								<strong>Tente descrever bem o local.</strong> Fale sobre a localização, sobre a estrutura, sobre serviços oferecidos
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Você pode dizer se esse local é <strong>caro ou barato</strong>.
							</span>
							<br/>
							<c:if test="${local.tipoRestaurante}">
								<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
									Diga se gostou da comida.
								</span>
							</c:if>
							<c:if test="${local.tipoHotel}">
								<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
									Diga se os quartos são bem equipados.
								</span>
							</c:if>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Se possível, <strong>compartilhe fotos</strong>. Elas deixam suas avaliações ainda melhores!
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								Quanto mais informações você colocar, mais você estará ajudando os seus amigos e a comunidade de viajantes.
							</span>
							<br/>
							<span class="red"><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								<strong>Evite palavras ofensivas</strong>
							</span>
							<br/>
							<span><i class="icon-ok" style="margin-top: 5px; padding: 2px;"></i>
								<strong class="red">Evite avaliações promocionais</strong>, como por exemplo, que anunciem descontos ou contenham URLs, sites e telefones. 
								<strong class="red">Nos reservamos o direito de remover avaliações com essas características.</strong>
							</span>
						</div>
				</div> 
			</div>
			
			 <div id="modal-cancelar-avaliacao" class="modal hide fade">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Confirma</h3>
                </div>
                <div class="modal-body">
                    <p>Deseja realmente sair desta página e não escrever uma avaliação?</p>
                </div>
                <div class="modal-footer">
                    <a id="confirmar-cancelar_dica" href="<c:url value="/locais/${local.urlPath}" />" class="btn btn-primary">
                        <i class="icon-ok icon-white"></i> 
                        Sim
                    </a>
                    <a href="#" class="btn" data-dismiss="modal">
                        <i class="icon-remove"></i> 
                        Não
                    </a>
                </div>
            </div>
						
			 		</div>
		 		</div>
 		</div>
	</div>
	</tiles:putAttribute>

</tiles:insertDefinition>