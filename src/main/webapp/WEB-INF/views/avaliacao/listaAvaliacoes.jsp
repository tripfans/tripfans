<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="listaAvaliacoes">

    <c:forEach items="${avaliacoes}" var="varAvaliacao" varStatus="contador">
      <c:if test="${avaliacaoDestaque == null or avaliacaoDestaque.id != varAvaliacao.id}">
    	<fan:avaliacao fullSize="12" userSize="2" dataSize="10" avaliacao="${varAvaliacao}" mostraTextoAvaliacaoSobre="true" mostraUsuarioAvaliador="true" mostraAcoes="true" />
      </c:if>	
    </c:forEach>
    
    <div class="row">
    	<c:url var="urlAvaliacao" value="/avaliacao/listar/${local.urlPath}?item=${avaliacaoDestaque.id}" />
    	<fan:pagingButton targetId="listaAvaliacoes" title="Ver Mais Avaliações" noMoreItensText="" cssClass="span12" url="${urlAvaliacao}" />
    </div>

</div>