<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div>
	<fan:avaliacao avaliacao="${avaliacao}"/>	
</div>

<script type="text/javascript">
	$('a.votoUtil').click(function(event){
		event.preventDefault();
		
		var urlPath = $(this).attr('id');
		
		var parent = $(this).parent();
		
		$.post(this.href, function(response){
			if(response.success) {
				$(parent).html(response.params.message);

				var quantidadeVotosUteis = response.params.quantidadeVotoUtil;
				var mensagemQuantidadeVotosUteis = 'Esta avaliação foi considerada útil por ';

				if(quantidadeVotosUteis == 1){
					mensagemQuantidadeVotosUteis += quantidadeVotosUteis + ' pessoa.';
				} else {
					mensagemQuantidadeVotosUteis += quantidadeVotosUteis + ' pessoas.';
				}
				$('#quantidadeVotoUtil-'+urlPath).html(mensagemQuantidadeVotosUteis);
			}
		});
	 });

	$('a.verFotos').click(function(event){
		event.preventDefault();
		var items = [];
		$.getJSON(this.href, {
			jsonFields: $.toJSON([
               		{name: 'urlOriginal', type: 'string'},
               		{name: 'descricao', type: 'string'}
            ])
		}, function(response){
		    $.each(response.records, function(key, item) {
		    	var urlImg = item.urlOriginal;
		    	var title = item.descricao !== null ? item.descricao : ''; 
		        items.push({'href' : urlImg, 'title' : title});
		    });
		    $.fancybox(items, {
		        transitionIn      : 'elastic',
		        transitionOut     : 'elastic',
		        type              : 'image',
		        speedIn: 400, 
				speedOut: 200, 
				overlayShow: true,
				overlayOpacity: 0.1,
		        hideOnContentClick: true,
				titlePosition: 'inside'
		    });
		});
	 });
	
</script>
