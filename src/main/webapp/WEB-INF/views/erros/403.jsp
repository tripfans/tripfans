<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,must-revalidate"/>
        <meta http-equiv="expires" content="0"/>
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="slurp" content="noindex">
        <meta name="msnbot" content="noindex">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container-fluid page-container">
            
            <div class="page-header" style="margin: 0px;">
                <h1 style="padding: 10px;">
                    Sem permiss�o para acessar esse recurso
                </h1>
            </div>
        
            <div align="center">
                <br/>
            
                <div class="alert alert-info" style="width: 80%;">
                  <p>
                    <strong>
                        Parece que voc� est� tentando acessar uma p�gina protegida.
                    </strong>
                    <br/>
                    <strong>
                        Pode ser que o usu�rio tenha restringido o acesso a este recurso apenas para amigos.
                    </strong>
                  </p>
                  <p>
                    <a href="<c:url value="/home"/> " style="text-decoration: none; color: #63432A;">
                        Retornar � p�gina inicial
                    </a>
                    <br/>
                    <c:set var="urlAnterior" value="${not empty header['Referer'] ? header['Referer'] : '#'}" />
                    <a href="${urlAnterior}" style="text-decoration: none; color: #63432A;" onclick="${urlAnterior == '#' ? 'event.preventDefault(); history.go(-1)' : ''}">
                        Retornar � p�gina anterior
                    </a>
                  </p>
                </div>
                
                <div>
                    <div></div>
                    <img src="<c:url value="/resources/images/forbidden.jpg"/> "/>
                </div>
            </div>
            
        </div>        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>