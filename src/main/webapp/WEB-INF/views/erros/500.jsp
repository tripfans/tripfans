<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="tripfans.responsive.new">

    <tiles:putAttribute name="stylesheets">
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,must-revalidate"/>
        <meta http-equiv="expires" content="0"/>
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="slurp" content="noindex">
        <meta name="msnbot" content="noindex">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container-fluid page-container">
            
            <div class="title module-box-header">
                <span class="title">Ocorreu um erro</span>
            </div>
        
            <div align="center">
                <div style="position: relative; width: 470px;">
                    <c:set var="urlAnterior" value="${not empty header['Referer'] ? header['Referer'] : '#'}" />
                    <div>
                        <img src="<c:url value="/resources/images/internal-error.jpg"/> "/>
                    </div>
                    <div style="left: 83px; overflow: hidden; position: absolute; top: 27px;">
                      <p style="color: #8c090d;">
                        <strong style="font-size: 18px;">Ah n�o... Ocorreu um erro nos nossos servidores!</strong>
                        <br/>
                        A p�gina solicitada n�o est� dispon�vel neste momento. 
                      </p>
                    </div>
                    <div style="left: 110px; overflow: hidden; position: absolute; top: 180px;">
                      <p>
                        Por favor, tente novamente mais tarde.
                        <br/>
                        Pedimos desculpas pelo inconveniente.
                        <br/>
                        Se o problema persistir, entre em contato conosco:
                        <br/> 
                        <a href="mailTo:suporte@tripfans.com.br">suporte@tripfans.com.br</a>
                      </p>
                    </div>
                    <div style="left: 200px; overflow: hidden; position: absolute; top: 280px;">
                      <a href="${urlAnterior}" style="text-decoration: none; color: #63432A;" onclick="${urlAnterior == '#' ? 'event.preventDefault(); history.go(-1)' : ''}">
                        P�gina anterior
                      </a>
                    </div>
                    <div style="left: 203px; overflow: hidden; position: absolute; top: 300px;">
                      <a href="<c:url value="/home"/> " style="text-decoration: none; color: #63432A;">
                        P�gina inicial
                      </a>
                    </div>
                </div>
                
            </div>
            
            <!-- 
                Error: ${pageContext.exception} 
                URI: ${pageContext.errorData.requestURI}
                <c:forEach var="trace" items="${pageContext.exception.stackTrace}">
                  ${trace}
                </c:forEach>            
             -->
            
        </div>        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>