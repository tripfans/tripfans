<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,must-revalidate"/>
        <meta http-equiv="expires" content="0"/>
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="slurp" content="noindex">
        <meta name="msnbot" content="noindex">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            
            <div class="page-header">
                <h1>
                Endere�o inv�lido
                </h1>
            </div>
        
            <div>
            
                Voc� pode ter clicado em um link expirado ou digitado o endere�o errado. Alguns endere�os de internet diferenciam letras mai�sculas de min�sculas.
                
                <br/>
            
                    Retornar � p�gina inicial
                    <br/>
                    Retornar � p�gina anterior
            </div>
            
        </div>        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>