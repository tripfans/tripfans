<%@ page language="java" errorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="tripfans.responsive.new">

	<c:choose>
		<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
			<c:set var="reqScheme" value="https" />
		</c:when>
		<c:otherwise>
			<c:set var="reqScheme" value="http" />
		</c:otherwise>
	</c:choose>

    <tiles:putAttribute name="stylesheets">
        <meta http-equiv="pragma" content="no-cache"/>
        <meta http-equiv="cache-control" content="no-cache,must-revalidate"/>
        <meta http-equiv="expires" content="0"/>
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta name="slurp" content="noindex">
        <meta name="msnbot" content="noindex">
        
        <link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>


		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/bootstrap/bootstrap3/css/bootstrap.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/pageResponsive.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/baseResponsive.css" />" />

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/components/mmenu/jquery.mmenu.all.css" type="text/css" media="screen, projection" />

		<style>
.page-header {
	padding-bottom: 9px;
	margin: 10px 0 20px;
	border-bottom: 1px solid #eeeeee;
}
</style>

		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-1.7.1.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-ui-1.8.16.custom.min.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/components/bootstrap/bootstrap3/js/bootstrap.js"/>"></script>

		<script type="text/javascript" src="<c:url value="/resources/scripts/fanaticos.js"/>"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-validation-1.9.js"></script>
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="">
            
        
            <div align="center">
                <br/>
            
                <div class="alert alert-warning" style="width: 80%;">
                  <p>
                    <strong style="font-size: 18px;">Página não encontrada!</strong>
                    <br/>
                    Você pode ter clicado em um link expirado ou digitado o endereço errado. 
                    <br/>
                    Alguns endereços de internet diferenciam letras maiúsculas de minúsculas.
                  </p>
                </div>
                
                <div style="position: relative; width: 470px;">
                    <c:set var="urlAnterior" value="${not empty header['Referer'] ? header['Referer'] : '#'}" />
                    <div>
                        <img src="<c:url value="/resources/images/not-found.jpg"/> "/>
                    </div>
                    <div style="left: 200px; overflow: hidden; position: absolute; top: 11px;">
                      <a href="${urlAnterior}" style="text-decoration: none; color: #63432A;" onclick="${urlAnterior == '#' ? 'event.preventDefault(); history.go(-1)' : ''}">
                        <strong>Página anterior</strong>
                      </a>
                    </div>
                    <div style="left: 203px; overflow: hidden; position: absolute; top: 54px;">
                      <a href="<c:url value="/home"/> " style="text-decoration: none; color: #63432A;">
                        <strong>Página inicial</strong>
                      </a>
                    </div>
                </div>
                
            </div>
            
        </div>        
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>