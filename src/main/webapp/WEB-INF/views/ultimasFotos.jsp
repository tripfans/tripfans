<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="borda-arredondada" style="padding: 15px; display: inline-block; margin-left: 0px; position: relative; width: 89%; height: 450px;">
    
    <div id="fotos-viajantes" class="theme1" style="font-family: 'PT Sans Narrow', sans-serif;">
        
        <ul style="list-style: none;">
        
            <c:forEach items="${fotosViajantes}" var="foto">
            
                <c:set var="urlAutor" value="/perfil/${foto.autor.urlPath}" />
                <%-- Se for o perfil do usuario TripFans, remover link --%>
                <c:if test="${foto.autor.id == 1}">
                  <c:set var="urlAutor" value="#" />
                </c:if>
                <li>
                    <%-- <img class="borda-arredondada" src="<c:url value="${foto.urlAlbum}"/>" style="max-height: 200px;"> --%>
                    <i class="carrossel-thumb borda-arredondada " data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>"
                       style="background-image: url('<c:url value="${foto.urlAlbum}"/>'); height: 200px; width: 242px;"></i>
                    <div style="margin-top:16px"></div>
                    <fan:userAvatar displayName="false" user="${foto.autor}" marginLeft="0" showFrame="false" />
                    <p style="min-height: 50px;">
                        <strong class="local-card-title">
                          <a href="<c:url value="/locais/${foto.local.urlPath}" />">
                            ${foto.local.nome}
                          </a>
                        </strong>
                        <br/>
                        <small>
                            por
                            <a href="<c:url value="${urlAutor}" />">
                                ${foto.autor.displayName}
                            </a>
                        </small>
                    </p>
                    <p>
                        <a id="btn-detalhar-foto-${foto.id}" class="btn morebutton" data-foto-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>" href="#">Mais detalhes</a>
                    </p>
                                            
                    <div class="page-more">
                        <%-- <img class="big-image" width="480" height="300" src="<c:url value="${foto.urlBig}"/>"> --%>
                        <div class="span13" style="border: 1px solid #DDD; padding: 4px;">
                            <i class="carrossel-big" data-foto-big-id="${foto.id}" data-foto-url-big="<c:url value="${foto.urlBig}"/>" style="height: 370px;"></i>
                        </div>
                        <div style="position: relative; float: left; margin-right: 30px;" class="span6">
                            <p>
                                <strong class="card-title" style="font-size: 26px;">
                                  <a href="<c:url value="/locais/${foto.local.urlPath}" />">
                                    ${foto.local.nome}
                                  </a>
                                </strong>
                            </p>
                            <div style="min-height: 60px;">
                                <fan:userAvatar displayName="false" user="${foto.autor}" marginLeft="0" showFrame="false" />
                                <small>
                                    por
                                    <a href="<c:url value="${urlAutor}" />">${foto.autor.displayName}</a>
                                </small>
                                <p>
                                    <small>
                                        <c:choose>
                                            <c:when test="${not empty foto.data}">
                                                <fan:passedTime date="${foto.data}"/>
                                            </c:when>
                                            <c:otherwise>
                                                <fan:passedTime date="${foto.dataPostagem}"/>
                                            </c:otherwise>
                                        </c:choose>
                                    </small>
                                </p>
                            </div>
                            <p>
                                ${foto.descricao}
                            </p>
                            <p>
                                <div class="btn-group pull-left">
                                    <a href="<c:url value="/avaliacao/avaliar/${foto.local.urlPath}"/>" id="btn-avaliar-<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>.id}" class="btn btn-mini" title="Escreva uma avalia��o">
                                      <img src="<c:url value="/resources/images/icons/award_star_gold_3.png" />">
                                    </a>
                                    <a href="<c:url value="/dicas/escrever/${foto.local.urlPath}"/>" id="btn-dica-${foto.local.id}" class="btn btn-mini" title="Escreva uma dica">
                                      <img src="<c:url value="/resources/images/icons/lightbulb.png" />">
                                    </a>
                                </div>
                                <c:if test="${usuario != null}">
                                  <div class="btn-group pull-left" style="margin-right: 6px;">
                                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" cssButtonClassSize="mini" />
                                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" cssButtonClassSize="mini" />
                                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" cssButtonClassSize="mini" />
                                    <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${foto.local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" cssButtonClassSize="mini" />
                                  </div>
                                </c:if>
                            </p>
                            <p style="margin-top: 12px;">&nbsp;</p>
                            <p>
                                <a class="btn" href="<c:url value="/locais/${foto.local.urlPath}/avaliacoes" />" style="width: 159px;">Ver avalia��es deste local</a>
                            </p>
                            <p>
                                <a class="btn" href="<c:url value="/locais/${foto.local.urlPath}/dicas" />" style="width: 159px;">Ver dicas deste local</a>
                            </p>
                            <p>
                                <a class="btn" href="<c:url value="/locais/${foto.local.urlPath}/fotos" />" style="width: 159px;">Ver mais fotos deste local</a>
                            </p>
                            <!-- <p>
                                <a class="btn">Ver mais fotos deste usu�rio</a>
                            </p> -->
                        </div>
                        <div  class="closer"></div>
                    </div>
                </li>
            
            </c:forEach>
        
        </ul>
        
        <div class="toolbar">
            <div class="left"></div><div class="right"></div>
        </div>
        
    </div>
    
</div>