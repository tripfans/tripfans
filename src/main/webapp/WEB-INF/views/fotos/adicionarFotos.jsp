<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div>
    <ul id="lista-box-fotos" style="list-style-type: none;">
      <c:forEach items="${fotos}" var="foto" varStatus="status">
        <li>            
          <%@include file="boxFoto.jsp" %>
        </li>
      </c:forEach>
    </ul>
</div>

<%@include file="acoesSobreFoto.jsp" %>