<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">

	<div class="page-header">
        <h3>
        	Selecionar fotos
        </h3>
    </div>
    
    <div id="fotos-content">
        <c:import url="listaAlbunsFacebook.jsp"></c:import>
    </div>
    
</div>

<script>
    $(document).ready(function() {
        $('.box-album').click(function(e) {
            event.preventDefault();
            var idAlbum = $(this).data('facebook-album-id')
            var url = '<c:url value="/fotos/albuns/selecionarFotos/facebook/' + idAlbum + '?atividade=${atividade.id}" />'
            $('#fotos-content').load(url);
        });
    });
    
</script>
