<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script>
            $('a[rel="slideShow"]').fancybox({
                transitionIn      : 'elastic',
                transitionOut     : 'elastic',
                type              : 'image',
                speedIn: 400, 
                speedOut: 200, 
                overlayShow: true,
                overlayOpacity: 0.1,
                hideOnContentClick: true,
                'titleFormat' : function(title, currentArray, currentIndex, currentOpts) {
                    return '<span>Foto ' + (currentIndex + 1) + ' / ' + currentArray.length + ' ' + title + '</span>';
                },
                titlePosition: 'inside'
            });
            
        </script>    
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">

        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
        
        	<div class="page-header">
                <h3>
                	Seus albuns do Facebook
                </h3>
            </div>
            
            <c:import url="listaAlbunsFacebook.jsp"></c:import>
            
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>