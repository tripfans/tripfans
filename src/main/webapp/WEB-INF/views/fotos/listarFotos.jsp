<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<c:choose>
	<c:when test="${not empty param.fotoClass}">
		<c:set var="paramsImg" value="class=\"${param.fotoClass}\"" />
	</c:when>
	<c:otherwise>
		<c:set var="paramsImg" value="class=\"thumbnail\" width=\"134\" height=\"94\"" />
	</c:otherwise>
</c:choose>

<c:if test="${selecionar}">

  <form id="formSelecionarFotos" action="<c:url value="/fotos/albuns/importarFotosAtividade"/>" method="post">
  
    <input type="hidden" name="atividade" value="${atividade.id}">
  
    <div class="subnav" style="padding-right: 20px;">
                    
        <ul class="nav nav-pills right">
    
            <li>
                <div style="margin: 10px; font-size: 11px;">
                    <a id="fotos-select-all" class="btn btn-mini" data-toggle="button" href="#">
                        Selecionar todas
                    </a>
                </div>
            </li>
    
            <li>
                <div class="btn-toolbar" style="margin-bottom: 9px">
                    <input type="submit" value="Importar selecionadas" class="btn btn-mini btn-info" />
                </div>
            </li>
    
        </ul>
        
    </div>
    
</c:if>

<div style="padding-left: 18px;">
    <ul class="thumbnails">
      <c:forEach items="${fotos}" var="foto" varStatus="status">
        <li>
            
            <c:if test="${selecionar}">
              <input type="checkbox" name="idsFotos" value="${foto.id}" class="check-foto" />
              <a href="#" title="">
                <%-- <img src="${foto.urlCarrosel}" ${paramsImg} title="${foto.descricao}"> --%>
                <i class="carrossel-thumb borda-arredondada" title="${foto.descricao}" ${paramsImg}
                               style="background-image: url('<c:url value="${foto.urlCarrosel}"/>'); height: 175px; width: 175px;"></i>                
              </a>
            </c:if>
            
            <c:if test="${not selecionar}">
              <div class="box-foto-div">
                  <%--div class="btn-group opcoes">
                    <a href="#" class="btn btn-remover-foto" data-foto-id="${foto.id}" title="Remover esta foto">
                        <i class="icon-trash"></i>
                    </a>
                  </div--%>
                  <a id="box-foto-link-${foto.id}" href="<c:url value="/fotos/exibir/${foto.id}?i=${status.index}&u=${status.last}"/>" class="box-foto fancybox" title="" 
                     data-id-foto="${foto.id}" data-index-foto="${status.index}"
                     data-prev-index="${status.index - 1}" data-next-index="${status.index + 1}">
                       <%-- <img src="${foto.urlCarrosel}" ${paramsImg} title="${foto.descricao}"> --%>
                    <i class="carrossel-thumb borda-arredondada" title="${foto.descricao}" ${paramsImg}
                                   style="background-image: url('<c:url value="${foto.urlCarrosel}"/>'); height: 175px; width: 175px;"></i>                
                  </a>
              </div>
            </c:if>
        </li>
      </c:forEach>
    </ul>
</div>

<%@include file="acoesSobreFoto.jsp" %>

<c:if test="${selecionar}">

</form>

  <script>

    $(document).ready(function() {
        $('#fotos-select-all').click(function (e) {
            if ($(this).attr('class').indexOf('active') === -1) {
                $('.check-foto').attr('checked', true);
            } else {
                $('.check-foto').attr('checked', false);
            }
        });
        
        var $formFotos = $('#formSelecionarFotos');
        $formFotos.unbind('submit');
        
        $formFotos.submit(function(event) {
            event.preventDefault();
            
            $.ajax({
                type: 'POST',
                url: $formFotos.attr('action'),
                data: $formFotos.serialize(),
                success: function(data, status, xhr) {
                    if (data.success) {
                        if ($.fancybox) {
                        	$.fancybox.close();
                        }
                    }
                }
            });
        });        
    });

  </script>
</c:if>