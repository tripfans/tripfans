<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <c:if test="${not selecionar}">
        <c:url var="urlSelecionarFotos" value="/fotos/albuns/importarFotos/facebook"/>
    </c:if>
    <c:if test="${selecionar}">
        <c:url var="urlSelecionarFotos" value="/fotos/albuns/facebook/${idAlbumFacebook}/importarFotosAtividade/${atividade.id}"/>
    </c:if>

    <form id="formFotos" action="${urlSelecionarFotos}" method="post">
    
        <input type="hidden" name="idAlbumFacebook" value="${idAlbumFacebook}" />
    
        <div class="subnav" style="padding-right: 20px;">
        
            <ul class="nav nav-pills right">
        
                <li>
                    <div style="margin: 10px; font-size: 11px;">
                        <a id="fotos-select-all" class="btn btn-mini ${not selecionar ? 'active' : ''}" data-toggle="button" href="#">
                            Selecionar todas
                        </a>
                    </div>
                </li>
                
                <c:if test="${not selecionar}">
                    <li style="margin-top: 10px; font-size: 11px;">
                        Importar para o album
                    </li>
                    
                    <li style="font-size: 11px; margin: 5px 10px 0 10px;">
                        <select id="album" name="idAlbum" class="span3">
                            <option>- Criar um novo album -</option>
                            <c:forEach items="${albuns}" var="album">
                                <option value="${album.id}">${album.titulo}</option>
                            </c:forEach>
                        </select>
                    </li>
                </c:if>
                
                <li>
                    <div class="btn-toolbar" style="margin-bottom: 9px">
                        <input type="submit" value="Importar selecionadas" class="btn btn-mini btn-info" />
                    </div>
                </li>
        
            </ul>
            
        </div>
    
        <div class="horizontalSpacer"></div>
        
        <ul class="thumbnails">
    
          <c:forEach items="${fotos}" var="foto" varStatus="status">
        
            <li>
                <input type="checkbox" name="idsFotosFacebook" value="${foto.id}" ${not selecionar ? 'checked="checked"' : '' } class="check-foto" />
                <a href="${foto.sourceImage.source}" class="fancybox" rel="slideShow" title="">
                    <img src="${foto.tinyImage.source}" width="134" height="94" style="padding-bottom: 3px;">
                </a>              
            </li>
            
          </c:forEach>
        
        </ul>

    </form>
    
    <script>

    $(document).ready(function() {
        $('#fotos-select-all').click(function (e) {
            if ($(this).attr('class').indexOf('active') === -1) {
                $('.check-foto').attr('checked', true);
            } else {
                $('.check-foto').attr('checked', false);
            }
        });
        
        var $formFotos = $('#formFotos');
        $formFotos.unbind('submit');
        
        $formFotos.submit(function(event) {
            event.preventDefault();
            $.ajax({
                type: 'POST',
                url: $formFotos.attr('action'),
                data: $formFotos.serialize(),
                success: function(data, status, xhr) {
                    if (data.success) {
                        if ($.fancybox) {
                            $.fancybox.close();
                        }
                    }
                }
            });
        });        
    });

    </script>    