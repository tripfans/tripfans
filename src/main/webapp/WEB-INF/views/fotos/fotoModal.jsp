<%@page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade"%>
<%@page import="br.com.fanaticosporviagens.model.entity.Comentario"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="showControls" value="${param.showControls}"/>
<c:if test="${param.showControls == null}">
  <c:set var="showControls" value="true"/>
</c:if>

<div class="span20" style="margin-left: 0px;">
    
    <div id="modal-foto-content">
    
        <div class="span11" align="center">
        
            <div class="carousel slide" class="span12">
                <div class="carousel-inner">
                                
                    <div class="item active">
                        <div>
                            
                            <i class="carrossel-thumb span5 box-foto-img" 
                               style="background-image: url('${foto.urlOriginal}'); height: 480px; max-height: 480px; width: 100%;"></i>                            
                            
                        </div>
                        <c:if test="${not empty foto.descricao}">
                          <div class="carousel-caption">
                            <h4></h4>
                            <p>${foto.descricao}</p>
                          </div>
                        </c:if>
                    </div>
                  
                </div>
                <c:if test="${showControls}">
                  <a class="left carousel-control" href="#" data-slide="prev" data-id-foto="${foto.id}" data-prev-index="${indexFoto - 1}" style="margin-left: 20px; ${indexFoto == 0 ? 'display: none;' : ''}">&lsaquo;</a>
                  <a class="right carousel-control" href="#" data-slide="next" data-id-foto="${foto.id}" data-next-index="${indexFoto + 1}" style="${ultimaFoto ? 'display: none;' : ''}">&rsaquo;</a>
                </c:if>
            </div>    
            
        </div>
        
        <div class="span8" align="left" style="height: 100%; border: 1px solid #ddd; max-height: 500px;">
        
          <div class="nano" style="min-height: 480px;">
        
              <div class="content">
                <div style="padding-top: 15px;"></div>
        
                <fan:userAvatar user="${foto.autor}" displayName="true" orientation="horizontal" imgSize="avatar" marginLeft="15" />
                
                <div class="horizontalSpacer"></div>
                
                <div style="padding-left: 15px;">
                
                  <c:choose>
                      <c:when test="${usuario.id == foto.autor.id}">
                      
                        <form id="fotoForm" action="<c:url value="/fotos/salvarFoto"/>">
                            <input type="hidden" name="foto" value="${foto.id}">
                            <fan:autoComplete url="/pesquisaTextual/consultarCidadesEAtracoes" hiddenName="local" hiddenValue="${foto.local.id}" 
                                              id="local-foto" valueField="id" value="${foto.local.nomeCompleto}"  
                                              blockOnSelect="true" minLength="3" labelField="nomeExibicao" placeholder="Informe o local da foto" 
                                              cssClass="span6" showActions="true" cssStyle="position: relative; z-index: 8000;" />
                                    
                            <fan:date id="data-foto" name="dataFoto" placeholder="Informe a data" value="${foto.data}"
                                      maxDate="<%= new java.util.Date() %>" cssClass="span3" cssStyle="position: relative; z-index: 8000;" />
                                      
                            <textarea id="descricao-foto" name="descricao" rows="3" class="span7" placeholder="Escreva algo sobre esta foto">${foto.descricao}</textarea>
                            
                            <%-- div class="row" style="margin-left: 0px;">
                              <div class="span3" style="margin-left: 0px;">
                                  <fan:groupRadioButtons size="normal" name="visibilidade" id="visibilidade-foto-${foto.id}" selectedValue="${foto.visibilidade}" cssClass="botaoVisibilidade">
                                    <fan:radioButton id="visibilidade-amigos-${foto.id}" value="<%= TipoVisibilidade.AMIGOS %>" 
                                                     label="<%= TipoVisibilidade.AMIGOS.getDescricao() %>" icon="<%= TipoVisibilidade.AMIGOS.getIcone() %>" 
                                                     title="Selecione se deseja que somente amigos vejam esta foto" style="width : 50%;" />
                                    <fan:radioButton id="visibilidade-publico-${foto.id}" value="<%= TipoVisibilidade.PUBLICO %>" 
                                                     label="<%= TipoVisibilidade.PUBLICO.getDescricao() %>" icon="<%= TipoVisibilidade.PUBLICO.getIcone() %>" 
                                                     title="Selecione se deseja que sua foto seja vista pela comunidade nas principais p�ginas de locais" style="width : 50%;" />
                                  </fan:groupRadioButtons>
                              </div>
                            </div--%>
                                                
                            <hr  style="margin: 5px 20px 5px 0;" />
                            <div align="right" style="margin-right: 20px; margin-bottom: 70px;">
                                <input id="btn-salvar" type="submit" value="Salvar altera��es" class="btn btn-primary right" data-loading-text="Salvando..." />
                                <a id="btn-fechar" value="Fechar" class="btn span1 right" href="#" data-dismiss="modal" style="margin-right: 10px;">
                                    Fechar
                                </a>
                            </div>
                        
                        </form>
                        
                      </c:when>
                      <c:otherwise>
                      
                        <c:if test="${foto.local != null}">
                            <div>
                                Em ${foto.local.nome}
                            </div>
                        </c:if>
                        <c:if test="${foto.data != null}">
                            <div>
                                <fan:passedTime date="${foto.data}"/>
                            </div>
                        </c:if>
                            <div>
                                ${foto.descricao}
                            </div>
                            
                      </c:otherwise>
                    
                  </c:choose>
                    
                </div>
        
                <div style="padding: 0 8px 4px 4px; background-color: #FAFAFA; height: 100%; margin-left: 10px;">
                    <fan:comments imgSize="mini-avatar" inputType="text" inputClass="span5" actionsClass="span7" tipo="<%=Comentario.TipoComentario.FOTO.name()%>" id="foto-${foto.id}" idAlvo="${foto.id}" />
                	<div style="clear: both;"></div>
                </div>
              </div>
              
            </div>
            
        </div>
        
        <script>
    
            $(document).ready(function() {
                
                $('.nano').nanoScroller({ flash: true });

                $('.carousel-control').click(function(e) {
                    
                    var idFoto = $(this).data('id-foto');
                    // prev ou next
                    var direcao = $(this).data('slide');
                    // pegar o indice da 'proxima' foto a ser exibida
                    var indexFoto = $(this).attr('data-' + direcao + '-index');
                    var url = $('#box-foto-link-' + idFoto).parents('ul').find('a[data-index-foto="' + indexFoto +'"]').attr('href');
                	
                    $('#modal-foto-content').parent().load(url,
                        function() {
                            $('#modal-foto-content').initToolTips();
                        }
                    );
                });
                
                
                var $fotoForm = $('#fotoForm');
                $fotoForm.unbind('submit');
                
                $fotoForm.submit(function(event) {
                    event.preventDefault();
                    $('#btn-salvar').button('loading');
                    
                    if ($fotoForm.valid()) {
                        $.ajax({
                            type: 'POST',
                            url: $fotoForm.attr('action'),
                            data: $fotoForm.serialize(),
                            success: function(data, status, xhr) {
                                setTimeout(function() {
                                	$('#btn-salvar').button('reset');
                                }, 1000);
                            }
                        });
                    }
                    
                });            
        	});
                
        </script>
        
    </div>

</div>