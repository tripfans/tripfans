<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

    <ul class="thumbnails">

      <c:forEach items="${albuns}" var="album">
    
        <li class="box-album" data-facebook-album-id="${album.id}">
            <img src="<c:url value="/resources/images/no_photo.png" />" class="thumbnail" width="134" height="94">
            <div style="margin-top: 3px; word-wrap: break-word; width: 134px">
                <a href="#" style="display: inline-block; overflow: hidden; text-overflow: ellipsis; white-space: nowrap; max-width: 100%;">
                    <strong>${album.name}</strong>
                </a>
                <br/>
                <span class="badge">${album.count}</span> 
                <span class="muted">${album.count == 1 ? 'foto ' : 'fotos' }</span>
            </div>
            <div class="btn-toolbar left">
                <div class="btn-group">
                    <c:if test="${not selecionar}">
                      <a href="<c:url value="/fotos/albuns/importar/facebook/${album.id}" />" class="btn btn-mini">
                        Importar
                      </a>
                    </c:if>
                    <a href="<c:url value="/fotos/albuns/listarFotos/facebook/${album.id}" />" class="btn btn-mini box-album" data-facebook-album-id="${album.id}">
                        Selecionar fotos
                    </a>
                </div>
            </div>                    
        </li>
        
      </c:forEach>
    
    </ul>