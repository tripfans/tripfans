<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:if test="${uploadUrl == null}">
  <c:set var="uploadUrl" value="/fotos/albuns/adicionarFoto"></c:set>
</c:if>

<script>
    jQuery(function() {
        
        var extraParams = {
            <c:if test="${not empty idAlbum}">
            'album' : '${idAlbum}',
            </c:if>
            <c:if test="${not empty idAtividade}">
            'atividade' : '${idAtividade}'
            </c:if>
        };
        
        $('[rel="uploadable"]').each(function() {
            initializeUploader($(this).attr('id'), '<c:url value="${uploadUrl}"/>', extraParams);
            $('#file-uploader').find('input[name="file"]').click();
        });
        
        $('#btn-publicar-fotos').click(function(e) {
            e.preventDefault();
            if (!$(this).is(':disabled')) {
                $(this).button('loading');
                <c:if test="${not empty callback}">
                    ${callback}();
                </c:if>
                // Verifica se o painel existe
                if ($('#lista-box-fotos').length > 0) {
                    // Pegar o conteudo do painel com as fotos 'subidas' e carregar no painel do album na tela de tr�s, caso esteja aberta
                	$('#lista-box-fotos').append($('#uploaded-list').html());
                }
                if($('#albumHidden').val() !== null) {
	                var $form = $('#albunsForm');
	                $.ajax({
	                    type: 'POST',
	                    url: $form.attr('action'),
	                    data: $form.serialize(),
	                    success: function(data, status, xhr) {
	                        if (data.success) {
	                        	$.fancybox.close();
	                        }
	                    }
	                });
                	
                }
              	// fechar a janela
              	$.fancybox.close();
            }
        });
        
        $('.btn-fechar-janela').click(function(e) {
        	var idsFotos = '';
        	
        	$('#uploaded-list').find('input[name="foto"]').each(function(index) {
        		if (idsFotos != '') {
        			idsFotos += ',';
        		}
        		idsFotos += $(this).val();
        	});
        	
        	// cancelar publicacao
        	$.ajax({
                type: 'POST',
                url: '<c:url value="/fotos/albuns/cancelarPublicacaoFotos"/>',
                data: {
                	'idsFotos' : idsFotos,
                },
                success: function(data, status, xhr) {
                    if (data.success) {
                        $.fancybox.close();
                    }
                }
            });
            $.fancybox.close();
        });
        
        $('#btn-adicionar-fotos').click(function(e) {
        	e.preventDefault();
        	$('#file-uploader').find('input[name="file"]').click();
        });
        
        $('#btn-publicar-fotos').attr('disabled', true);
        
    });
    
    function initializeUploader(idFileUploader, url, extraParams, callback) {
    	var element = document.getElementById(idFileUploader);
    	var uploader = new qq.FileUploader({
            element: element,
            //listElement : document.getElementById(id + '-list'),
            action: url,
            params : extraParams,
            allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
            multiple: true,
            maxConnections: 999,
            debug: false,
            onSubmit: function(id, fileName) {
            	//$('#file-upload-list').html();
                $('#btn-publicar-fotos').attr('disabled', true);
            	/*console.log ($(element))
                $(element).append("<span class='image'><div id='progress-" + id + "'></div></span>")*/
            },
            onProgress: function(id, fileName, loaded, total){
            	/*var progress = (loaded / total) * 100;
                $("#progress-" + id).progressbar({value: progress})*/
            },
            onComplete: function(id, fileName, responseJSON) {
            	urlPathFoto = '';
            	//urlFotoOriginal = '';
            	//urlFotoOriginal = '';
                $.each(responseJSON, function(key, item) {
                    if (key == "params") {
                        urlPathFoto = item.urlPathFoto;
                        <c:choose>
	                        <c:when test="${not empty idAlbum}">
	                        	urlExibir = '<c:url value="/fotos/exibirBoxFoto/" />' + urlPathFoto + '?idAlbum=${idAlbum}';
	                        </c:when>
	                        <c:otherwise>
	                        	if(item.album !== null && item.album !== undefined) {
	                        		urlExibir = '<c:url value="/fotos/exibirBoxFoto/" />' + urlPathFoto + '?idAlbum='+item.album;
                        			extraParams["album"] = item.album;
                        			$('#albumHidden').val(item.album);
	                        	} 
	                        </c:otherwise>
                        </c:choose>
                    }
                    if(extraParams["atividade"] !== null) {
                    	urlExibir = '<c:url value="/fotos/exibirBoxFoto/" />' + urlPathFoto + '?atividade='+extraParams["atividade"];
                    }
                });
                
                //$("#progress-" + id).remove();
                //console.log(urlFotoCarrosel);
                
                //$('#uploaded-list-' + idAtividade).append($('<div></div>').css({'width': '280px', 'height': '210px', 'background-color': '', 'background-image': 'url(' + image_url + ')'}));
                //$('#uploaded-list-' + idAtividade).append('<div class="span5"><img src="' + urlFotoCarrosel + '" width="280" height="210" /></div>');
                //$('.qq-upload-list').html('');
                
                $.get(urlExibir, {}, function(response) {
                    $('#uploaded-list').append(response);
                    // atualiza o z-index dos campos autoComplete na m�o
                    $('#uploaded-list').find('.autoCompleteFanaticos').css('z-index', '9999');
                    $('#btn-publicar-fotos').attr('disabled', false);
                });

                var htmlImg = '<a href="' + urlFotoOriginal + '" class="fancybox" rel="slideShow">' +
                '<img src=' + urlFotoOriginal + '" width="134" height="94"></a>';
              
                // callback
            },
            onCancel: function(id, fileName) {
            	
            },
            template: '<div class="qq-uploader">' +
                '<div class="qq-upload-drop-area"><span></span></div>' +
                
                '<ul id="file-upload-list" class="qq-upload-list unstyled"></ul>' +
            '</div>' +
            '<div class="qq-upload-button btn btn-primary" align="center" style="width: 150px; display: none"><i class="icon-picture icon-white"></i> Adicionar Fotos</div>',
            fileTemplate:  '<li>' +
                '<span class="qq-upload-file"></span>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-size"></span>' +
                '<a class="qq-upload-cancel label important" href="#">Cancelar</a>' +
                '<span class="qq-upload-failed-text">Falhou</span>' +
             '</li>',
            messages: {
                typeError: "{file} has invalid extension. Only {extensions} are allowed.",
                sizeError: "{file} is too large, maximum file size is {sizeLimit}.",
                minSizeError: "{file} is too small, minimum file size is {minSizeLimit}.",
                emptyError: "{file} is empty, please select files again without it.",
                onLeave: "The files are being uploaded, if you leave now the upload will be cancelled."            
            },
            showMessage: function(message){
                alert(message);
            } 
        });
    	
    }
</script>

<div>
    <div>
    <c:if test="${(idAlbum == null && idAtividade == null) || param.novoAlbum == true}">
	    <c:url value="/fotos/album/salvar" var="salvar_url" />
		<form id="albunsForm" action="${salvar_url}" style="margin: 0;">
			<input id="albumHidden" type="hidden" name="album" value="${idAlbum}">
			<div style="padding-left: 0px;">
				<label>T�tulo do �lbum</label>
				<input id="titulo-album" name="titulo"
						class="span10" style="font-size: 16px; font-weight: bold;"
						placeholder="Adicionar um t�tulo para o �lbum" />
			</div>
			<div>
				<label>Descri��o do �lbum</label>
				<textarea id="descricao-album" name="descricao" class="span15"
					rows="2" placeholder="Adicionar uma descri��o para o album"></textarea>
			</div>
		</form>
	</c:if>
    
        <div class="modal-header">
            <h3>Adicionar fotos</h3>
        </div>
        <div class="modal-body" style="display: inline-block; min-height: 300px;">
        
            <%-- <div class="subnav" style="padding-right: 20px;">
                
                <ul class="nav nav-pills right">
                    <li style="margin-top: 10px; font-size: 11px;">
                        Adicionar ao album
                    </li>
                    
                    <li style="font-size: 11px; margin: 5px 10px 0 10px;">
                        <select id="album" name="idAlbum" class="span3">
                            <option>- Criar um novo album -</option>
                            <c:forEach items="${albuns}" var="album">
                                <option value="${album.id}" ${idAlbum == album.id ? 'selected="selected"' : ''}>${album.titulo}</option>
                            </c:forEach>
                        </select>
                    </li>
                    
                    <li>
                        <div class="btn-toolbar" style="margin-bottom: 9px">
                            <input type="submit" value="Importar selecionadas" class="btn btn-mini btn-info" />
                        </div>
                    </li>
                </ul>
                    
            </div>        
 --%>
              <div class="row">
                <div id="uploaded-list" class="uploaded-list span16">
                
                </div>
              </div>
            
              <p>
                  <div id="file-uploader" rel="uploadable" data-id="${id}">
                  </div>
              </p>
        </div>
        <div class="modal-footer">
            <a id="btn-adicionar-fotos" href="#" class="btn btn-primary" style="float: left">
                <i class="icon-picture icon-white"></i> Adicionar Fotos
            </a>
            
            <a href="#" class="btn btn-fechar-janela">
                <i class="icon-remove"></i> Cancelar
            </a>
            
            <a href="#" id="btn-publicar-fotos" class="btn btn-primary" data-loading-text="Aguarde..." data-complete-text="Publicado">
                <i class="icon-ok icon-white"></i>
                Publicar fotos
            </a>            
        </div>
    </div>
</div>

<c:import url="acoesSobreFoto.jsp">
  <c:param name="modal" value="true"></c:param>
</c:import>

