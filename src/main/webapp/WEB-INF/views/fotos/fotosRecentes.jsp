<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="width" value="${not empty param.width ? param.width : 550}"></c:set>

<c:if test="${not empty fotos}">
  <div style="width: 100%" align="center">
	<div class="borda-arredondada" style="padding: 30px; margin-left: 10px; margin-top: 10px; display: inline-block; position: relative; width: ${width}px; height: 170px;">
	    <div id="fotosCarrossel" class="theme1">
	
	        <ul id="carrosel-fotos" style="list-style: none;">
	            <c:forEach items="${fotos}" var="foto" varStatus="status" >
	            	<li>
    	            	<%--a rel="slideShow" data-tooltip="false" href="${foto.urlBig}">
                            <i class="carrossel-thumb borda-arredondada " data-viagem-id="${viagem.id}" title="${foto.descricao}"
                               style="background-image: url('<c:url value="${foto.urlAlbum}"/>'); height: 170px; width: 175px;"></i>
    	            	</a--%>
                        
                        <a id="box-foto-link-${foto.id}" href="<c:url value="/fotos/exibir/${foto.id}?i=${status.index}&u=${status.last}"/>" class="box-foto fancybox" title="" 
                           data-id-foto="${foto.id}" data-index-foto="${status.index}"
                           data-prev-index="${status.index - 1}" data-next-index="${status.index + 1}">                        
                            <i class="carrossel-thumb borda-arredondada " title="${foto.descricao}"
                               style="background-image: url('<c:url value="${foto.urlAlbum}"/>'); height: 170px; width: 175px;"></i>
                        </a>
	            	</li>
	            </c:forEach>
                <c:if test="${not empty quantidadeFotos and quantidadeFotos < 3}">
                  <c:if test="${(not empty perfil and perfil.id == usuario.id) or not empty local}">
                    <c:forEach begin="${quantidadeFotos}" end="2" varStatus="status">
                      <li style="font-family: 'PT Sans Narrow', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                        <a href="<c:url value="/fotos/publicarFotos" />" style="text-decoration: none; color: #929292;">
                            <div class="borda-arredondada" style="height: 170px; width: 175px; background-color: #eee;">
                              <div style="padding-top: 25px; font-size: 14px;">
                                  <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                                  Clique para adicionar
                              </div>
                            </div>
                        </a>
                      </li>
                    </c:forEach>
                  </c:if>
                </c:if>
	        </ul>
	        
	        <div class="toolbar">
	             <div class="left"></div><div class="right"></div>
	         </div>
	
	    </div>    
    </div>
    
    <div id="modal-foto" class="modal hide fade" role="dialog" >
        <div class="modal-header" style="border-bottom: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body" style="overflow: hidden; max-height: 800px; min-width: 960px;">
            <div id="modal-foto-body"></div>
        </div>
    </div>
  </div>
    
    <script>
        jQuery(document).ready(function() {
        	
        	jQuery('#fotosCarrossel').services({
				width: ${param.width},
				height: 170,
				slideAmount: 3,
				slideSpacing: 5,
				touchenabled: 'off',
				mouseWheel: 'off',
				transition: 1,
				carousel: 'on',
				hoverAlpha: 'on',
				slideshow: 5000
			});
        	
        	if (window.innerWidth < 1199) {
        	    $('.main-container').css('padding-left', '10px').css('padding-right', '6px')
        	    $('.main-container').parent().find('.toolbar').find('.right').css('right', '47px');
            }
            
            /*$('a[rel="slideShow"]').fancybox({
                transitionIn      : 'elastic',
                transitionOut     : 'elastic',
                type              : 'image',
                speedIn: 400, 
                speedOut: 200, 
                overlayShow: true,
                overlayOpacity: 0.1,
                hideOnContentClick: true,
                titleFormat : function(title, currentArray, currentIndex, currentOpts) {
                	return title;
                },
                titlePosition: 'inside'
            });*/
            
        	$('a.fancybox').click(function(e) {
                e.preventDefault();
                var idFoto = $(this).data('id-foto');
                // pegar a url da foto
                var url = $('#box-foto-link-' + idFoto).attr('href');
                $('#modal-foto-body').load(url, function() {
                    $('#modal-foto').modal().css({
                        /*'width': function () { 
                             return ($(document).width() * .9) + 'px';  
                        },
                        'margin-left': function () { 
                            return -($(this).width() / 2); 
                        }*/
                        width: 'auto',
                        'margin-left': function () {
                            return -($(this).width() / 2);
                        }
                    });                 
                    $('#modal-foto').modal('show');
                    
                });
            });
        });
    </script>
    
</c:if>