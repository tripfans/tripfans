<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
.box-foto-div {
    position: relative;
}
.box-foto-div div.opcoes {
    display: none;
    position: absolute;
    left: 10px;
    top: 10px;
    overflow: hidden;
}
.box-foto-div div.opcoes:hover {
    display: block;
}
.box-foto-div:hover div.opcoes {
    display: block;
}
</style>

<div id="modal-remover-foto" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Excluir foto</h3>
    </div>
    <div class="modal-body">
        <p>Deseja realmente excluir esta foto?</p>
    </div>
    <div class="modal-footer">
        <a id="confirmar-remover-foto" href="#" class="btn btn-primary" data-foto-id="" data-atividade-id="">
            <i class="icon-ok icon-white"></i> 
            Sim
        </a>
        <a href="#" class="btn" data-dismiss="modal">
            <i class="icon-remove"></i> 
            N�o
        </a>
    </div>
</div>

<script>

function removerFoto($botao) {
    $('#modal-remover-foto').modal('show');
    $('#confirmar-remover-foto').data('foto-id', $botao.data('foto-id'));
    $('#confirmar-remover-foto').data('atividade-id', $botao.data('atividade-id'));
}

function confirmarRemoverFoto($botao) {
    var idFoto = $botao.data('foto-id');
    var idAtividade = $botao.data('atividade-id');
    
    var url = ''
    if (idAtividade != null && idAtividade != '') {
        url = '<c:url value="/fotos/excluirDaAtividade/" />' + idAtividade + '/' + idFoto;
    } else {
        url = '<c:url value="/fotos/excluir/" />' + idFoto;
    }
    $.ajax({
        type: 'POST',
        url: url,
        success: function(data, status, xhr) {
            if (data.success) {
            	
                // fechar modal
                $('#modal-remover-foto').modal('hide');
                // remover o box da foto excluida
                $('#form-edit-foto-' + idFoto).hide('explode', {}, 1000);
                $('#form-edit-foto-' + idFoto).remove();
                
                try {
                    if (aposExclucaoFotos) {
                        aposExclucaoFotos();
                    }
                } catch(err) {
                }
            }
        }
    });
}

$(document).ready(function() {
	
	var modal = '${param.modal}';
	
	<c:choose> 
	  <c:when test="${param.modal}">
	    $(document).on('click', '.btn-remover-foto', function (e) {
	  </c:when>
	  <c:otherwise>
	    $('.btn-remover-foto').click(function (e) {
	  </c:otherwise>
	</c:choose>
		e.preventDefault();
        if ('${param.modal}' === 'true') {
        	confirmarRemoverFoto($(this));
        } else {
        	removerFoto($(this));
        }
    });
    
    //$(document).on('click', '#confirmar-remover-foto', function (e) {
    $('#confirmar-remover-foto').click(function (e) {
        e.preventDefault();
        confirmarRemoverFoto($(this));
    });
    
    //$(document).on('click', '.btn-escolher-capa', function (e) {
    $('.btn-escolher-capa').click(function (e) {
        e.preventDefault();
        var $botao = $(this);
        var idFoto = $botao.attr('data-foto-id')
        $.ajax({
            type: 'POST',
            url: '<c:url value="/fotos/selecionarCapa/" />' + idFoto,
            success: function(data, status, xhr) {
                if (data.success) {
                    $('.btn-escolher-capa').removeClass('btn-info');
                    $('.btn-escolher-capa').find('i').attr('class', 'icon-star-empty');
                    $botao.find('i').attr('class', 'icon-star icon-white');
                    //$botao.attr('title', 'Esta � a capa do album');
                    $botao.addClass('btn-info');
                }
            }
        });
    });

});

</script>