<%@ page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<form id="form-edit-foto-${foto.id}" action="<c:url value="/fotos/salvarFoto" />" method="POST" class="box-edit-foto-form" style="margin: 0px;">
    <input type="hidden" name="foto" value="${foto.id}">
    <c:if test="${idAlbum !=null}">
        <input type="hidden" name="album" value="${idAlbum}">
    </c:if>
    <div class="span5 thumbnail box-foto-div" align="center" style="margin-bottom: 20px; position: relative;">
        <div class="btn-group opcoes">
            <a href="#" class="btn btn-remover-foto" data-foto-id="${foto.id}" title="Remover esta foto">
                <i class="icon-trash"></i>
            </a>
            <c:choose>
              <c:when test="${album.idFotoPrincipal == foto.id}">
                <a href="#" class="btn btn-info btn-escolher-capa" data-foto-id="${foto.id}" title="Esta � a capa do album">
                    <i class="icon-star icon-white"></i>
                </a>
              </c:when>
              <c:otherwise>
                <a href="#" class="btn btn-escolher-capa" data-foto-id="${foto.id}" title="Escolher como capa do album">
                    <i class="icon-star-empty"></i>
                </a>
              </c:otherwise>
            </c:choose>
        </div>

        <a id="box-foto-link-${foto.id}" href="<c:url value="/fotos/exibir/${foto.urlPath}?i=${status.index}&u=${status.last}"/>" class="fancybox" title="" 
           data-id-foto="${foto.id}" data-index-foto="${status.index}"
           data-prev-index="${status.index - 1}" data-next-index="${status.index + 1}">
              <i class="carrossel-thumb span5 box-foto-img" style="background-image: url('<c:url value="${foto.urlCarrosel}"/>'); height: 210px; margin-left: 0px; margin-bottom: 4px;"></i>
        </a>
        
        <fan:autoComplete url="/pesquisaTextual/consultar" hiddenName="local" hiddenValue="${foto.local.id}" 
                          id="local_foto_${foto.id}" valueField="id" value="${foto.local.nomeCompleto}"  
                          blockOnSelect="true" minLength="3" labelField="nomeExibicao"  
                          placeholder="Informe o local da foto" cssClass="span4" cssStyle="position: relative; z-index: 999; " showActions="true"/>
                          
        <fan:date id="data-foto-${foto.id}" name="data" placeholder="Informe a data" value="${foto.data}"
                  maxDate="<%= new java.util.Date() %>" cssClass="span5" />
                  
        <textarea id="descricao-foto-${foto.id}" name="descricao" rows="3" class="span5" placeholder="Escreva algo sobre esta foto">${foto.descricao}</textarea>
        
        <div class="row" style="margin-left: 0px;">
          <div class="span2" style="margin-left: 3px; margin-top: 4px; text-align: left;">
            Visibilidade <i class="icon-question-sign" title="Selecione 'P�blico', se voc� deseja que esta foto seja exibida para toda a comunidade nas p�ginas do local que voc� informou ou, selecione 'Amigos', se quiser restringir as pessoas que ver�o esta foto."></i>
          </div>
          <div class="span3" style="margin-left: 0px; float: right;">
              <fan:groupRadioButtons size="normal" name="visibilidade" id="visibilidade-foto-${foto.id}" selectedValue="${foto.visibilidade}" cssClass="botaoVisibilidade">
                <fan:radioButton id="visibilidade-amigos-${foto.id}" value="<%= TipoVisibilidade.AMIGOS %>" 
                                 label="<%= TipoVisibilidade.AMIGOS.getDescricao() %>" icon="<%= TipoVisibilidade.AMIGOS.getIcone() %>" 
                                 title="Selecione se deseja que somente amigos vejam esta foto" style="width : 50%;" />
                <fan:radioButton id="visibilidade-publico-${foto.id}" value="<%= TipoVisibilidade.PUBLICO %>" 
                                 label="<%= TipoVisibilidade.PUBLICO.getDescricao() %>" icon="<%= TipoVisibilidade.PUBLICO.getIcone() %>" 
                                 title="Selecione se deseja que sua foto seja vista pela comunidade nas principais p�ginas de locais" style="width : 50%;" />
              </fan:groupRadioButtons>
          </div>
        </div>
        
    </div>
</form>

<script>
    //$('form :input').change(function() {
    $('#local_foto_${foto.id}, #descricao-foto-${foto.id}, #data-foto-${foto.id}').change(function() {
        //$(this).closest('form').data('changed', true);
    	var $form = $(this).closest('form');
    	atualizarFoto($form);
  	});
    
    /*$('#visibilidade-amigos-${foto.id}').click(function (e) {
        atualizarFoto($(this).closest('form'));
    });*/
    
    $('#visibilidade-amigos-${foto.id}, #visibilidade-publico-${foto.id}').click(function (e) {
    	atualizarFoto($(this).closest('form'));
    });
    
    function atualizarFoto($form) {
        
        // Apos a alteracao de algum campo, o form ser� submitido ap�s 0,5 seg
        setTimeout(function() {
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function(data, status, xhr) {
                    if (data.success) {
                        //carregarPagina('${param.pageToLoad}');
                    }
                }
            });
        }, 500);
    }
</script>