<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="row" style="text-align: center;">

    <c:url var="urlPlano" value="/viagem/planejamento/inicio"/>
    <div class="col-md-3">
      
        <div class="homeOpcaoAcao">
            <img src="<c:url value="/resources/images/planejamento.png" />" width="64" class="transparencia-75" />
            <h3 class="plano">1. Crie sua Viagem</h3>
            <p class="muted">
                Escolha os destinos e defina o período de sua viagem.
            </p>
        </div>
      
    </div>

    <c:url var="urlMapa" value="/perfil/meuPerfil?tab=mapa"/>
    <c:if test="${usuario != null}">
        <c:url var="urlMapa" value="/perfil/${usuario.urlPath}/mapa"/>
    </c:if>
    <div class="col-md-3">
      
        <div class="homeOpcaoAcao">
            <img src="<c:url value="/resources/images/mapa.png" />" width="64" class="transparencia-75 img-circle" />
            <h3 class="mapa">2. Escolha as atividades</h3>
            <p class="muted">
                Escolha os restaurantes e atrações que deseja visitar.
            </p>
        </div>
      
    </div>
    
    <div class="col-md-3">
	  
		<div class="homeOpcaoAcao">
			<img src="<c:url value="/resources/images/icons/mini/64/avaliacao.png" />" height="64" class="transparencia-75" />
			<h3 class="avaliacao">3. Receba Sugestões</h3>
			<p class="muted">
				Receba sugestões personalizadas de lugares para visitar.
			</p>
		</div>
	  
    </div>
    
    <div class="col-md-3">
	  
		<div class="homeOpcaoAcao">
			<img src="<c:url value="/resources/images/icons/mini/64/avaliacao.png" />" height="64" class="transparencia-75" />
			<h3 class="avaliacao">4. Pronto!</h3>
			<p class="muted">
				Sua roteiro de viagem está pronto.
			</p>
		</div>
	  
    </div>
    
</div>