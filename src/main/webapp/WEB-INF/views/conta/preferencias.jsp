<%@page import="br.com.fanaticosporviagens.model.entity.CategoriaPreferenciasViagem"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoOrganizacaoViagem"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PreferenciasViagem" %>
<%@ page import="br.com.fanaticosporviagens.model.entity.InteressesViagem" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div>
    
    	<div class="page-header">
          	<h3><s:message code="label.usuario.perfil.preferenciasViagem.titulo" /></h3>
          </div>
          
         <s:message var="texto" code="textoAjuda.preferenciasViagem" />
		<fan:helpText text="${texto}" showQuestion="false"/>
    
		<c:url value="/conta/salvarPreferenciasInteressesViagem" var="salvarUrl" />

        <jsp:include page="formPreferencias.jsp">
        	<jsp:param name="salvarUrl" value="${salvarUrl}"/>
        	<jsp:param name="mostraBarraAcoes" value="true"/>
        </jsp:include>
                
    </div>
</div>