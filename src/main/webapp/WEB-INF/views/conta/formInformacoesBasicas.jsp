<%@page import="java.util.Date"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoExibicaoDataNascimento"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<form:form id="usuarioForm" action="${param.salvar_url}" modelAttribute="contaUsuario">
	
		<div id="errorBox" class="alert alert-error centerAligned hide"> 
		  <p><s:message code="validation.containErrors" /></p> 
		</div>
	
	<fieldset>
	    <legend>
	        <s:message code="label.usuario.perfil.dadosPessoais.titulo" />
	    </legend>
	    
	    <div>
	        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
	            <li>
	                <label class="span3 fancy-text">
	                    <span class="required">
	                        <s:message code="label.usuario.perfil.primeiroNome" />
	                    </span>
	                </label>
	                <form:input path="primeiroNome" cssClass="span6" />
	            </li>                                         
	                                                        
	            <li>
	                <label>
	                    <span class="span3 fancy-text">
	                        <s:message code="label.usuario.perfil.nomeMeio" />
	                    </span>
	                </label>
	                <form:input path="nomeMeio"  cssClass="span6" />
	            </li>
	            
	            <li>
	                <label class="span3 fancy-text">
	                    <span class="required">
	                        <s:message code="label.usuario.perfil.ultimoNome" />
	                    </span>
	                </label>
	                <form:input path="ultimoNome" cssClass="span6" />
	            </li>
	
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.displayName" />
	                        <i id="help-nome" class="icon-question-sign helper" title="<s:message code="tooltip.usuario.perfil.displayName" />"></i>
	                    </span>
	                </label>
	                <form:input path="displayName" cssClass="span6" />
	            </li>
	            
	            <li>
	                <label class="span3 fancy-text">
	                    <span class="required">
	                        <s:message code="label.usuario.perfil.nomeUsuario" />
	                        <i id="help-nome" class="icon-question-sign helper" title="<s:message code="tooltip.usuario.perfil.nomeUsuario" arguments="${usuario.urlPath}" />"></i>
	                    </span>
	                </label>
	                <form:input path="urlPath" id="nomeUsuario" cssClass="span6" />
	            </li>
	
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.genero" />
	                    </span>
	                </label>
	                <div class="span3" style="margin-left: 0px;">
	                    <form:select id="genero" path="genero" cssClass="span3 fancy">
	                        <form:option value="M">
	                            <s:message code="label.usuario.perfil.genero.masculino" />
	                        </form:option>
	                        <form:option value="F">
	                            <s:message code="label.usuario.perfil.genero.feminino" />
	                        </form:option>
	                    </form:select>
	                </div>
	                <div class="span7">
	                	<fan:groupRadioButtons size="normal" name="exibirGenero" id="exibirGenero" selectedValue="${contaUsuario.exibirGenero}">
	                		<fan:radioButton value="true" label="Exibir no meu perfil"></fan:radioButton>
	                		<fan:radioButton value="false" label="N�o exibir no meu perfil"></fan:radioButton>
	                	</fan:groupRadioButtons>
	                    
	                </div>
	            </li>
	            
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.dataNascimento" />
	                    </span>
	                </label>
	                <div class="span2" style="margin-left: 0px;">
	                	<fan:date id="dataNascimento" name="dataNascimento" value="${contaUsuario.dataNascimento}"
	                	maxDate="<%= new Date() %>" cssClass="span2" cssStyle="position: relative; z-index: 100;" />
	                </div>
	                <div class="span7" style="margin-left: 5px;"> 
	                    <form:select id="exibirDataNascimento" cssClass="span6 fancy" path="tipoExibicaoDataNascimento">
	                        <form:option value="<%=TipoExibicaoDataNascimento.COMPLETA %>">
	                            <%=TipoExibicaoDataNascimento.COMPLETA.getDescricao() %>
	                        </form:option>
	                        <form:option value="<%=TipoExibicaoDataNascimento.APENAS_DIA_MES %>">
	                            <%=TipoExibicaoDataNascimento.APENAS_DIA_MES.getDescricao() %>
	                        </form:option>
	                        <form:option value="<%=TipoExibicaoDataNascimento.OCULTAR %>">
	                            <%=TipoExibicaoDataNascimento.OCULTAR.getDescricao() %>
	                        </form:option>
	                    </form:select>
	                </div>
	            </li>
	        </ul>                
	    </div>
	</fieldset>
	
	<fieldset>
	
	    <legend>
	        <s:message code="label.usuario.perfil.cidades.titulo" />
	    </legend>
	    
	    <div>
	        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.cidadeAtual" />
	                    </span>
	                </label>
	                <fan:autoComplete url="/pesquisaTextual/consultarCidades" hiddenName="cidadeResidencia" hiddenValue="${contaUsuario.cidadeResidencia.id}" id="caixaCidadeResidencia" valueField="id" value="${contaUsuario.cidadeResidencia.nomeComEstadoPais}"  
	                blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Sua cidade atual" cssClass="span6" cssStyle=""/>
	                <span><s:message code="label.usuario.perfil.dataResidencia" /></span>
	                <span>
	                	<fan:date id="dataResidencia" name="dataResidencia" value="${contaUsuario.dataResidencia}"
	                	maxDate="<%= new Date() %>" cssClass="span2" cssStyle="position: relative; z-index: 100;" />
	                </span>
	                
	            </li> 
	            
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.cidadeNatal" />
	                    </span>
	                </label>
	                <fan:autoComplete url="/pesquisaTextual/consultarCidades" hiddenName="cidadeNatal" hiddenValue="${contaUsuario.cidadeNatal.id}" id="caixaCidadeNatal" valueField="id" value="${contaUsuario.cidadeNatal.nomeComEstadoPais}"  
	                blockOnSelect="false" minLength="3" labelField="nomeExibicao" placeholder="Sua cidade natal" cssClass="span6" cssStyle="" />
	            </li> 
	        </ul>
	    </div>
	</fieldset>    
	    
	
	<fieldset>
	    <legend>
	        <s:message code="label.usuario.perfil.sobreVoce.titulo" />
	    </legend>
	    
	    <div>
	    
	        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
	                
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.frasePerfil" />
	                    </span>
	                </label>
	                <form:input path="frasePerfil" cssClass="span10" maxlength="255" />
	            </li>
	            
	            <li>
	                <label class="span3 fancy-text">
	                    <span>
	                        <s:message code="label.usuario.perfil.informacoesAdicionais" />
	                    </span>
	                </label>
	                <form:textarea path="informacoesAdicionais" cssClass="span10" rows="5" />
                </li>
	                     
            </ul>
	                 
		</div>
	</fieldset>	
		
	<c:if test="${param.mostraBarraAcoes}">
		<jsp:include page="barraAcoes.jsp"><jsp:param value="informacoesBasicas" name="pageToLoad"/></jsp:include>
	</c:if>

</form:form>

    
<script type="text/javascript">

(function($){
	// call setMask function on the document.ready event
    //$('input:text').setMask();
})(jQuery);
    
$(document).ready(function () {
    
    $('.fancy').fancy();
	
    var language = "<c:out value="<%=LocaleContextHolder.getLocale().getLanguage()%>"></c:out>";
    var country = "<c:out value="<%=LocaleContextHolder.getLocale().getCountry()%>"></c:out>";
    $.datepicker.setDefaults( $.datepicker.regional[ language + "-" + country ] );
	
    $.validator.addMethod('nomeDeUsuarioValido', function(value, element) {
    	var span = $('#responseStatus');
    	if(span !== undefined) {
    		var error = ($('#responseStatus span').attr("class") == "cross");
    		return !error;
    	}
    	return true;
    });
    
    $("#usuarioForm").validate({ 
        rules: {
        	primeiroNome: { 
                required: true, 
                maxlength : 30
            }, 
            ultimoNome: { 
                required: true,
                maxlength : 30
            },
            urlPath: { 
                required: true,
                minlength : 5,
                nomeDeUsuarioValido: true
            },
            frasePerfil: {
            	maxlength: 255
            }
        }, 
        messages: { 
        	primeiroNome: {
                required : "<s:message code="validation.usuario.nome.required" />",
                maxlength: 'A quantidade m�xima de caracteres � 30'
            },
            ultimoNome: {
                required : "<s:message code="validation.usuario.ultimonome.required" />",
                maxlength: 'A quantidade m�xima de caracteres � 30'
            },
            urlPath: {
                required : "<s:message code="validation.usuario.nomeusuario.required" />",
                minlength: "<s:message code="validation.usuario.nomeusuario.minlength" />",
                nomeDeUsuarioValido: 'O nome de usu�rio deve ser v�lido'
            },
            frasePerfil: {
            	maxlength: "A quantidade m�xima de caracteres permitidos � 255"
            }
        },
        invalidHandler: function(form, validator) {
        	$.scrollTo($('#usuarioForm').closest('div'), {offset: -50, duration: 500} );
        },
        errorPlacement: function(error, element) {
       		var parent = element.parents("li")[0];
       		error.appendTo(parent);
        },
        errorContainer: "#errorBox"
    });
    
    $("#nomeUsuario").typeWatch({
    	callback: consultarDisponibilidade,
        wait: 750,
        highlight: true,
        captureLength: 5
    });
    
    function consultarDisponibilidade(){
    	$('#responseStatus').remove();
    	jQuery(':submit').removeAttr('disabled');
	    var nomeUsuario = $('#nomeUsuario').val();
        if(nomeUsuario !== '${usuario.urlPath}') {
    		var regex = new RegExp(/^[a-zA-Z0-9\-_]+$/);
        	if(nomeUsuario.match(regex)) {
		        $('#nomeUsuario').parent().append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="20" style="margin-left: 8px;" >');
		    	$.post('<c:url value="/conta/consultarDisponibilidadeNome" />', {'nomeUsuario' : nomeUsuario}, 
		    		function(data) {
		    			var resposta = data.params.resposta;
		    			var iconClass = "cross";
		    			if(data.params.disponivel) {
		    				iconClass = "accept";
		    			}
		    			var markup = '<span id="responseStatus"><span style="padding: 2px;" class="'+iconClass+'">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>'+resposta+'</em></span>';
		    			$('#loadingImg').remove();
		    			$('#nomeUsuario').parent().append(markup);
		    		}
		    	);
        	} else {
        		$('#nomeUsuario').parent().append('<span id="responseStatus"><span style="padding: 2px;" class="cross">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>Cont�m caracteres inv�lidos</em></span>');
        		jQuery('input[type="submit"]').attr('disabled', 'disabled');
        		return false;
        	}
        }
    }
    
});

</script>