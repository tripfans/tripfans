<%@page import="br.com.fanaticosporviagens.model.entity.FormatoTemperatura"%>
<%@page import="br.com.fanaticosporviagens.model.entity.FormatoHora"%>
<%@page import="br.com.fanaticosporviagens.model.entity.FormatoDistancia"%>
<%@page import="br.com.fanaticosporviagens.model.entity.FormatoData"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div>
    <div>
    
        <c:url value="/conta/salvarPreferenciasUsuario" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="view" class="form-stacked">
        
        	<c:if test="${view.preferencias.id != null}"><input type="hidden" name="preferencias" value="${view.preferencias.id}" /></c:if>

			<div class="page-header">    
            <h3>Geral</h3>
            </div>
            
            <div class="span-8">
                <span class="help-block">
                </span>
            </div>
            
            <fieldset>

                <div class="control-group">
                    <label>Data</label>
                    <div class="controls">
                        <form:select path="preferencias.formatoData" cssClass="span3">
                            <form:option value="<%=FormatoData.DD_MM_YYYY%>">Dia/M�s/Ano</form:option>
                            <form:option value="<%=FormatoData.MM_DD_YYYY%>">M�s/Dia/Ano</form:option>
                        </form:select>
                    </div>
                </div>            
            
                <div class="control-group">
                    <label>Hora</label>
                    <div class="controls">
                        <form:select path="preferencias.formatoHora" cssClass="span3">
                            <form:option value="<%=FormatoHora.FORMATO_24_HORAS%>">HH:mm</form:option>
                            <form:option value="<%=FormatoHora.FORMATO_AM_PM%>">hh:mm AM/PM</form:option>
                        </form:select>
                    </div>
                </div>            

                <div class="control-group">
                    <label>Dist�ncia</label>
                    <div class="controls">
                        <form:select path="preferencias.formatoDistancia" cssClass="span3">
                            <form:option value="<%=FormatoDistancia.KM%>">KM</form:option>
                            <form:option value="<%=FormatoDistancia.MILHAS%>">Milhas</form:option>
                        </form:select>
                    </div>
                </div>            

                <div class="control-group">
                    <label>Temperatura</label>
                    <div class="controls">
                        <form:select path="preferencias.formatoTemperatura" cssClass="span3">
                            <form:option value="<%=FormatoTemperatura.CELSIUS%>">Celsius (�C)</form:option>
                            <form:option value="<%=FormatoTemperatura.FAHRENHEIT%>">Fahrenheit (�F)</form:option>
                        </form:select>
                    </div>
                </div>            

            </fieldset>

            <jsp:include page="barraAcoes.jsp"><jsp:param value="interesses" name="geral"/></jsp:include>
            
        </form:form>
        
    </div>
</div>