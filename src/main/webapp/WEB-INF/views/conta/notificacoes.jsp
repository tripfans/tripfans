<%@page import="br.com.fanaticosporviagens.model.entity.CategoriaNotificacaoPorEmail"%>
<%@page import="br.com.fanaticosporviagens.model.entity.ConfiguracaoNotificacoesPorEmail"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoAcao"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="tf" uri="/WEB-INF/tripfansFunctions.tld" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div>
    
        <c:url value="/conta/salvarConfiguracoesEmails" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="view" class="form-stacked">
        
        	<c:if test="${view.configuracaoNotificacoesPorEmail.id != null}">
                <input type="hidden" name="configuracaoNotificacoesPorEmail" value="${view.configuracaoNotificacoesPorEmail.id}" />
            </c:if>
    
            <div class="page-header">
                <h3>
                    <s:message code="label.usuario.perfil.notificacoes.titulo" />
                </h3>
            </div>
            
            <s:message var="texto" code="label.usuario.perfil.notificacoes.ajuda" />
            <fan:helpText text="${texto}" showQuestion="false" />

<%--             
            <fieldset>
            
                <legend>
                    Destino das notifica��es
                </legend>
            
                <div>
                
                    <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">            
				  
                        <li>
                            <label class="span5 fancy-text">
                                <span>
                                    <s:message code="label.usuario.perfil.notificacoes.emailNotificacoes" />
                                </span>
                            </label>
                            <select id="emailSubscricao" name="configuracaoNotificacoesPorEmail.usuario.email" class="span7 fancy">
                                <option value="${view.usuario.username}">
                                    <s:message code="label.usuario.perfil.email.principal" /> (${view.usuario.username})
                                </option>
                                <c:forEach items="${view.usuario.contasEmServicosExternos}" var="conta">
                                   <option value="${conta.username}">${conta.username}</option>
                                </c:forEach>
                            </select>
                        </li>
                    
                    </ul>

				</div>
                
            </fieldset>
            
 --%>
             <span class="help-block">
                <strong>
                    <s:message code="label.usuario.perfil.notificacoes.emailNotificacoes.ajuda" />
                </strong>
            </span>
            
            <fieldset>
            
                <legend>
                    Quando receber
                </legend>
                
                <c:forEach items="<%=CategoriaNotificacaoPorEmail.values()%>" var="categoriaNotificacao">
                  <div style="padding: 10px;">
                
                    
                    <h4>
                        <p>${categoriaNotificacao.descricao}</p>
                    </h4>
                
                    <table class="table table-striped" style="width: 100%">
                        <tbody>
                            <c:forEach items="${categoriaNotificacao.tiposNotificoesPorEmail}" var="tipoNotificacao" varStatus="count">
                                <tr>
                                    <td valign="middle" width="60%">
                                        <span>
                                            ${tipoNotificacao.descricao}
                                        </span>
                                    </td>
                                    <td>
                                        <c:set var="valorSelecionado" value="${tf:getStatusTipoNotificacao(view.configuracaoNotificacoesPorEmail, tipoNotificacao.atributoDaClasse)}"/>
                                        
                                        <fan:groupRadioButtons size="normal" name="configuracaoNotificacoesPorEmail.${tipoNotificacao.atributoDaClasse}" id="tipo-notificacao-${tipoNotificacao.codigo}" selectedValue="${valorSelecionado}">
                                            <fan:radioButton value="true" label="Sim"></fan:radioButton>
                                            <fan:radioButton value="false" label="N�o"></fan:radioButton>
                                        </fan:groupRadioButtons>
                                        
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    
                  </div>
                </c:forEach>             
	    
            </fieldset>
				  
            <jsp:include page="barraAcoes.jsp">
                <jsp:param value="notificacoes" name="pageToLoad"/>
            </jsp:include>
            
        </form:form>
        
    </div>
</div>

<script>

function checkRegexp( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        //updateTips( n );
        return false;
    } else {
        return true;
    }
}

function executarPOST(url, data) {
	$.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(data) {
            if (data.error != null) {
                $("#errorMsg").empty().append(data.error);
                $("#errors").show();
            } else {
                var params = '';
                if (data.params != null) {
                    params = data.params;
                }
                $("#successMsg").empty().append(data.success);
                $("#success").fadeIn(1000);
                carregarPagina('notificacoes');
            }
        }
    });
}

$(document).ready(function () {
	
	$('.fancy').fancy();
	
});

</script>