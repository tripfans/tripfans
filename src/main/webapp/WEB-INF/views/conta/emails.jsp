<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div>
    <div>
    
        <c:url value="/conta/salvarConfiguracoesEmails" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="view" class="form-stacked">
        
        	<c:if test="${view.configuracaoNotificacoesPorEmail.id != null}"><input type="hidden" name="configuracaoNotificacoesPorEmail" value="${view.configuracaoNotificacoesPorEmail.id}" /></c:if>
    
            <div class="page-header">
                <h3>
                    <s:message code="label.usuario.perfil.email.titulo" />
                </h3>
            </div>
            
            <span class="help-block">
                <h3>
                    <small>
                        <p>
                            <s:message code="label.usuario.perfil.email.ajuda" />
                        </p>
                    </small>
                </h3>
            </span>
            
            <fieldset>
            
                <legend>
                    <s:message code="label.usuario.perfil.email.meusEmails" />
                </legend>
            
                <div>
                
                    <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
                    
                        <li>
                            <label class="span3 fancy-text">
                                <span>
                                    <s:message code="label.usuario.perfil.email.principal" />
                                </span>
                            </label>
                            <form:input type="text" path="usuario.username" disabled="true" cssClass="span6" />
                        </li>                                
            
                        <li>
                            <label class="span3 fancy-text">
                                <span>
                                    <s:message code="label.usuario.perfil.email.outros" />
                                </span>
                            </label>
                            <c:forEach items="${view.usuario.contasEmServicosExternos}" var="conta">
                                <input type="text" disabled="true" value="${conta.username}" class="span6" />
                                <div class="span5"></div>
                                <a class="menu-item" onclick="excluirEmail(${conta.id})"><img src="<c:url value="/resources/images/icons/delete.png"/>"><s:message code="label.acoes.remover" /></a>
                                <a class="menu-item" onclick="verificarEmail(${conta.id})"><img src="<c:url value="/resources/images/icons/accept.png"/>"><s:message code="label.acoes.verificar" /></a>
                            </c:forEach>
                            <div>
                                <input type="text" id="novoEmail" placeholder="<s:message code="placeholder.usuario.email.adicionar" />" class="span6" />
                                <a class="menu-item" id="incluirEmail">
                                <img src="<c:url value="/resources/images/icons/add.png"/>"><s:message code="label.acoes.adicionar" />
                                </a>
                            </div>
                        </li>
                    
                    </ul>

				</div>
					
            </fieldset>
            
            <jsp:include page="barraAcoes.jsp"><jsp:param value="emails" name="pageToLoad"/></jsp:include>
            
        </form:form>
        
    </div>
</div>

<script>

function checkRegexp( o, regexp, n ) {
    if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        //updateTips( n );
        return false;
    } else {
        return true;
    }
}

function excluirEmail(idConta) {
    var data = {
        contaServicoExterno : idConta
    };
    executarPOST('<c:url value="/conta/excluirEmail" />', data);
}

function verificarEmail(idConta) {
    var data = {
        contaServicoExterno : idConta
    };
    executarPOST('<c:url value="/conta/verificarEmail" />', data);
}

function executarPOST(url, data) {
	$.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(data) {
            if (data.error != null) {
                $("#errorMsg").empty().append(data.error);
                $("#errors").show();
            } else {
                var params = '';
                if (data.params != null) {
                    params = data.params;
                }
                $("#successMsg").empty().append(data.success);
                $("#success").fadeIn(1000);
                carregarPagina('emails');
            }
        }
    });
}

$(document).ready(function () {
	
	$('.fancy').fancy();
	
	$("#incluirEmail").click(function(e) {
        e.preventDefault();
        
        var valid = true;
        
        valid = valid && checkRegexp( $("#novoEmail"), /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
        
        if (!valid) {
        	$("#novoEmail").focus();
        } else {
        	var data = {
                email : $("#novoEmail").val()
            };
        	executarPOST('<c:url value="/conta/incluirEmail" />', data);
        }
        
    });

    $("#excluirEmail").click(function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '<c:url value="/conta/incluirEmail" />',
            data: {
                email : $("#novoEmail").val()
            },
            success: function(data) {
                if (data.error != null) {
                    $("#errorMsg").empty().append(data.error);
                    $("#errors").show();
                } else {
                    var params = '';
                    if (data.params != null) {
                        params = data.params;
                    }
                    
                    $("#successMsg").empty().append(data.success);
                    
                    $("#success").fadeIn(1000);
                }
            }
        });
    });

    $("#verificarEmail").click(function(e) {
        e.preventDefault();
    });
    
    /*$("#usuarioForm").validate({ 
        rules: {
            primeiroNome: { 
                required: true, 
                maxlength : 30
            }, 
            ultimoNome: { 
                required: true,
                maxlength : 30
            }, 
            dataNascimento: { 
                date: true
            },
        }, 
        messages: { 
            primeiroNome: {
                required : "<s:message code="validation.usuario.nome.required" />",
            },
            ultimoNome: {
                required : "<s:message code="validation.usuario.ultimonome.required" />",
            },
            dataNascimento: {
                date : "<s:message code="validation.date.invalid" />",
            }
        }
    });*/
    
});

</script>