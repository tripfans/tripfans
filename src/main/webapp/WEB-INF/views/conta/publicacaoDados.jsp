<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div>
    <div>
    
        <c:url value="/conta/alterarSenha" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="contaUsuario">
    
    		<div class="page-header">
            <h3><s:message code="label.usuario.publicarDados.titulo" /></h3>
            </div>
            
            <span class="help-block">
                <s:message code="label.usuario.publicarDados.situacoes" />
            </span>
            
            <fieldset>
            
                <table class="table table-striped" style="width: 70%">
                    <thead>
                      <tr>
                        <th></th>
                        <th style="text-align: center;" class="purple"><img src="<c:url value="/resources/images/logos/facebook-small.png" />"/><br/>Facebook</th>
                        <th style="text-align: center;" class="blue"><img src="<c:url value="/resources/images/logos/twitter-small.png" />"/><br/>Twitter</th>
                        <th style="text-align: center;" class="green"><img src="<c:url value="/resources/images/logos/google-small.png" />"/><br/>Google</th>
                      </tr>
                    </thead>
            
                    <tbody>
                        <tr>
                            <td><s:message code="label.usuario.publicarDados.tornarAmigo" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.publicarDados.compartilharViagem" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.publicarDados.entrarEmGrupo" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.publicarDados.comentar" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.publicarDados.alteracaoViagem" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                            <td style="text-align: center;"><input type="checkbox" /></td>
                        </tr>
                    </tbody>
                </table>                
                
            </fieldset>
            
            <jsp:include page="barraAcoes.jsp"><jsp:param value="publicacaoDados" name="pageToLoad"/></jsp:include>
            
        </form:form>
        
    </div>
</div>