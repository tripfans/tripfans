<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Complete seu Cadastro - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-title">
	            		<h2>Conex�es com contas externas</h2>
	            		<i>
        				Para compartilhar suas atividades em outras redes sociais, conecte suas contas abaixo
    					</i>
	            	</div>
	            	<div class="wizard-step-content">
						<s:message var="texto1" code="textoAjuda.conexoesExternas1" />
						<s:message var="texto2" code="textoAjuda.conexoesExternas2" />
						<s:message var="texto3" code="textoAjuda.conexoesExternas3" />
						<s:message var="texto4" code="textoAjuda.conexoesExternas4" />
	            		<fan:helpText text="${texto1}${texto2}${texto3}${texto4}" />
	            	
			            <jsp:include page="../formConexoesExternas.jsp">
			            	<jsp:param  name="redirectUrl" value="/confirmacaoPassoConexoesExternas/${usuario.urlPath}"/>
			            	<jsp:param  name="redirectUrlOnError" value="/confirmacaoPassoConexoesExternas/${usuario.urlPath}"/>
			            </jsp:include>
		            </div>
		            <div class="wizard-step-buttons form-actions">
		            	<c:url var="urlPular" value="/confirmacaoPassoFinal/${usuario.urlPath}" />
		            	<jsp:include page="barraAcoesConfirmacao.jsp">
		            		<jsp:param name="mostraBotaoPular" value="true"/>
		            		<jsp:param name="urlPular" value="${urlPular}"/>
		            		<jsp:param name="acaoBotaoContinuar" value="/confirmacaoPassoFinal/${usuario.urlPath}"/>
		            	</jsp:include>
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>