<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">
	
	<tiles:putAttribute name="title" value="Complete seu Cadastro - TripFans | Fanáticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-title">
	            		<h2>Seus Interesses de Viagem</h2>
	            		<i>
        				Escolha seus interesses de viagem
    					</i>
	            	</div>
	            	<div class="wizard-step-content">
	            		<s:message var="texto1" code="textoAjuda.interessesViagem1" />
	            		<s:message var="texto2" code="textoAjuda.interessesViagem2" arguments="Continuar" />
	            		<fan:helpText text="${texto1}${texto2 }" />
	            		
	            		<c:url var="salvarUrl" value="/conta/salvarPreferenciasInteressesViagem" />
			            <jsp:include page="../formInteresses.jsp">
			            	<jsp:param name="salvarUrl" value="${salvarUrl}"/>
			            	<jsp:param name="mostraBarraAcoes" value="false" />
			            </jsp:include>
		            </div>
		            <div class="wizard-step-buttons form-actions">
		            	<c:url var="urlPular" value="/confirmacaoPassoPreferenciasViagem/${usuario.urlPath}" />
		            	<jsp:include page="barraAcoesConfirmacao.jsp">
		            		<jsp:param name="mostraBotaoPular" value="true"/>
		            		<jsp:param name="urlPular" value="${urlPular}"/>
		            		<jsp:param name="submitForm" value="true"/>
		            		<jsp:param name="acaoBotaoContinuar" value="/confirmacaoPassoPreferenciasViagem/${usuario.urlPath}"/>
		            	</jsp:include>
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>