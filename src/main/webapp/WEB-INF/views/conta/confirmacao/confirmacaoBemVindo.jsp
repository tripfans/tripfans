<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Complete seu Cadastro - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-title">
	            		<h2>Agora vamos completar seu cadastro...</h2>
	            	</div>
	            	<div class="wizard-step-content" style="text-align: center;">
			            <p class="lead">
			            Ol�! Seja bem-vindo. Muito obrigado por usar o <strong>TripFans</strong>.
			            <br/>
			            Ser�o pedidas algumas informa��es para completar seu cadastro. Essas informa��es s�o importantes para que voc� usufrua da
			            melhor maneira poss�vel da nossa rede social.
			            <br/><br/>
			            Voc� pode pular qualquer um dos passos, se desejar.
			            </p>
		            </div>
		            <div class="wizard-step-buttons form-actions">
		            	<jsp:include page="barraAcoesConfirmacao.jsp">
		            		<jsp:param name="mostraBotaoPular" value="false"/>
		            		<jsp:param name="acaoBotaoContinuar" value="/confirmacaoPassoInformacoesBasicas/${usuario.urlPath}"/>
		            	</jsp:include>
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>