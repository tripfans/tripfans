<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Complete seu Cadastro - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-title">
	            		<h2>Suas Informa��es B�sicas</h2>
	            		<i>
        				Essas informa��es ser�o exibidas no seu perfil
    					</i>
	            	</div>
	            	<div class="wizard-step-content">
	            		<s:message var="texto" code="textoAjuda.informacoesBasicas" />
	            		<fan:helpText text="${texto}" />
	            		<c:url var="salvar_url" value="/conta/salvarAlteracoes" />
			            <jsp:include page="../formInformacoesBasicas.jsp">
			            	<jsp:param name="salvar_url" value="${salvar_url}"/>
			            	<jsp:param name="mostraBarraAcoes" value="false" />
			            </jsp:include>
		            </div>
		            <div class="wizard-step-buttons form-actions">
		            	<c:url var="urlPular" value="/confirmacaoPassoInteressesViagem/${usuario.id}" />
		            	<jsp:include page="barraAcoesConfirmacao.jsp">
		            		<jsp:param name="mostraBotaoPular" value="true"/>
		            		<jsp:param name="urlPular" value="${urlPular}"/>
		            		<jsp:param name="submitForm" value="true"/>
		            		<jsp:param name="acaoBotaoContinuar" value="/confirmacaoPassoInteressesViagem/${usuario.id}"/>
		            	</jsp:include>
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>