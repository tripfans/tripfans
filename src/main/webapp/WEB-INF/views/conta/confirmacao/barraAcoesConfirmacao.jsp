<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div style="width: 100%">
    <div class="right">
    	<c:if test="${param.mostraBotaoPular}">
	        <a id="jump-button" class="btn btn-info" href="${param.urlPular}">
	            <i class="icon-share-alt icon-white"></i>
	            Pular esse passo
	        </a>    
        </c:if>
        <a id="botaoContinuar" class="btn btn-success" data-loading-text="Aguarde..."  href="<c:url value="${param.acaoBotaoContinuar}"/>">
            <i class="icon-arrow-right icon-white"></i>
            Continuar
        </a>
    </div>
</div>

<script>

$(document).ready(function () {
<c:if test="${param.submitForm == true}" >
		$('#botaoContinuar').click(function(event) {
			event.preventDefault();
			var urlPosSubmit = $(this).attr('href');
			var formData = $('#usuarioForm').serialize();
			if ($("#usuarioForm").valid()) {
				$.ajax({
					type: 'POST',
					url: document.forms['usuarioForm'].action,
					data: formData,
					success: function(data, status, xhr) {
						if (data.error != null) {
							$("#errorMsg").empty().append(data.error);
							$("#errors").show();
						} else {
							document.location.href = urlPosSubmit;
						}
					}
				});
			}
		});
</c:if>
});
</script>
