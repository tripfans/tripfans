<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Complete seu Cadastro - TripFans | Fan�ticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
    	<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=447140778670288";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
    
        <div class="container">
            <div class="span20">
	            <div class="page-container" style="margin-top: 50px;">
	            	<div class="wizard-step-content" style="text-align: center;">
	            		<img src="<c:url value="/resources/images/tripfans-mala.png" />" />
			            <h1>Pronto! Voc� configurou sua conta</h1>
		            	<p class="lead">Agora voc� pode come�ar a utilizar o TripFans para compartilhar suas experi�ncias de viagens.</p>
		            	
		            	<jsp:include page="/WEB-INF/views/listaAcoesTripFans.jsp" />
		            	
						<c:if test="${tripFansEnviroment.enableSocialNetworkActions}">		            	
			            	<p class="lead">Aproveite e avise tamb�m seus amigos.</p>
			            	<div class="row">
			            		<div class="fb-like" data-href="http://www.tripfans.com.br" data-width="550" data-layout="box_count" data-action="recommend" data-show-faces="true" data-send="false"></div>
	
			            		<a href="https://twitter.com/share" data-url="http://www.tripfans.com.br" data-count="none" data-size="large" data-related="tripfans" data-text="Estou usando a melhor rede social de viagens" class="twitter-share-button" data-lang="pt-BR">Tweet</a>
			            		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			            		
			            	</div>
		            	</c:if>
		            </div>
		            <div>
		            </div>
	            </div>
            </div>        
        </div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>