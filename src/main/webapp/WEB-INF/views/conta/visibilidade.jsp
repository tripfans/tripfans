<%@ page import="br.com.fanaticosporviagens.model.entity.TipoVisibilidade" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div>
    
        <c:url value="/conta/salvarAlteracoes" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="contaUsuario">
        
			<div class="page-header">    
            <h3><s:message code="label.usuario.visibilidade.titulo" /></h3>
            </div>
            
            <s:message var="texto" code="label.usuario.visibilidade.ajuda" />
            <fan:helpText text="${texto}" showQuestion="false"/>
            
                <table class="table table-striped table-bordered">
            		<c:set var="tiposVisibilidade" value="<%= TipoVisibilidade.valuesOrdenadosPorCodigo() %>" />        
                    <tbody>
                        <tr>
                            <td><s:message code="label.usuario.visibilidade.perfil" /></td>
                            <td align="right">
                             <fan:groupRadioButtons name="configuracoesVisibilidade.mostrarInformacoesPerfil" selectedValue="${contaUsuario.configuracoesVisibilidade.mostrarInformacoesPerfil}" id="mostrarInformacoesPerfil">
                           		<fan:radioButton value="<%= TipoVisibilidade.PUBLICO %>" label="<%= TipoVisibilidade.PUBLICO.getDescricao() %>" icon="<%= TipoVisibilidade.PUBLICO.getIcone() %>" />
                           		<fan:radioButton value="<%= TipoVisibilidade.AMIGOS %>" label="<%= TipoVisibilidade.AMIGOS.getDescricao() %>" icon="<%= TipoVisibilidade.AMIGOS.getIcone() %>" />
                             </fan:groupRadioButtons>
						    </td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.visibilidade.albunsFoto" /></td>
                            <td>
                            <fan:groupRadioButtons name="configuracoesVisibilidade.mostrarAlbunsFoto" selectedValue="${contaUsuario.configuracoesVisibilidade.mostrarAlbunsFoto}" id="mostrarAlbunsFoto">
                             	<c:forEach var="tipoVisibilidade"  items="${tiposVisibilidade}">
                             		<fan:radioButton value="${tipoVisibilidade}" label="${tipoVisibilidade.descricao}" icon="${tipoVisibilidade.icone}" />
                             	</c:forEach>
                             </fan:groupRadioButtons>
                            </td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.visibilidade.foto" /></td>
                            <td>
                            <fan:groupRadioButtons name="configuracoesVisibilidade.mostrarFotos" selectedValue="${contaUsuario.configuracoesVisibilidade.mostrarFotos}" id="mostrarFotos">
                             	<c:forEach var="tipoVisibilidade"  items="${tiposVisibilidade}">
                             		<fan:radioButton value="${tipoVisibilidade}" label="${tipoVisibilidade.descricao}" icon="${tipoVisibilidade.icone}" />
                             	</c:forEach>
                             </fan:groupRadioButtons>
                            </td>
                        </tr>
                        <tr>
                            <td><s:message code="label.usuario.visibilidade.diariosViagens" /></td>
                            <td>
                            <fan:groupRadioButtons name="configuracoesVisibilidade.mostrarDiariosViagens" selectedValue="${contaUsuario.configuracoesVisibilidade.mostrarDiariosViagens}" id="mostrarDiariosViagens">
                             	<c:forEach var="tipoVisibilidade"  items="${tiposVisibilidade}">
                             		<fan:radioButton value="${tipoVisibilidade}" label="${tipoVisibilidade.descricao}" icon="${tipoVisibilidade.icone}" />
                             	</c:forEach>
                             </fan:groupRadioButtons>
                            </td>
                        </tr>
                    </tbody>
                </table>              
                                                                
            <jsp:include page="barraAcoes.jsp"><jsp:param value="visibilidade" name="pageToLoad"/></jsp:include>
            
        </form:form>
        
    </div>
</div>