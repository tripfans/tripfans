<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteressesViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<form:form id="usuarioForm" action="${param.salvarUrl}" modelAttribute="view" cssClass="form-stacked">

	<c:if test="${view.interessesViagem.id != null}"><input type="hidden" name="interessesViagem" value="${view.interessesViagem.id}" /></c:if>

    <fieldset>
          <c:forEach items="<%=TipoInteressesViagem.values() %>" var="interesse" varStatus="count">
          
          <div class="span2" style="min-height: 140px;">
        
                <label class="checkbox label-interesse" style="cursor: pointer">
                    <form:checkbox path="interessesViagem.${interesse.nome}" style="display: none;" id="interesse-${interesse.codigo}" class="check-interesse" />
                    <i class="interesse ${interesse.nome} borda-arredondada" style="display: none;" data-check-interesse-id="interesse-${interesse.codigo}"> </i>
                    <h6 class="titulo-interesse" style="text-align: center; max-width: 70px;">${interesse.descricao}</h6>
                </label>
            
          </div>
            
          </c:forEach>
            
    </fieldset>
    
    <c:if test="${param.mostraBarraAcoes}">
    	<jsp:include page="barraAcoes.jsp">
            <jsp:param name="pageToLoad" value="interesses" />
            <jsp:param name="showShareButton" value="true" />
            <jsp:param name="urlToShare" value="/perfil/${contaUsuario.urlPath}/interesses" />
            <jsp:param name="tituloToShare" value="${contaUsuario.displayName} alterou os interesses de viagem" />
            <jsp:param name="subTituloToShare" value="" />
            <jsp:param name="pictureUrlToShare" value="" />
            <jsp:param name="textoToShare" value="${contaUsuario.displayName} alterou os interesses de viagem" />
        </jsp:include>
    </c:if>

</form:form>
                
<script>

$(document).ready(function () {

    $('.interesse').each(function() {
		var $interesse = $(this);
		var $checkInteresse = $('#' + $interesse.attr('data-check-interesse-id'));
		if (!$checkInteresse.is(':checked')) {
		    $interesse.addClass('inactive');
		    $interesse.closest('div').find('.titulo-interesse').css('color', '#999');
		} else {
			$interesse.closest('div').find('.titulo-interesse').css('color', '#049cdb');
		}
		$interesse.show();
    });
    
    $('.check-interesse').click(function (e) {
    	var $check = $(this);
        var $interesse = $('i[data-check-interesse-id="' + $check.attr('id') + '"]');
        if ($interesse.hasClass('inactive')) {
            $interesse.removeClass('inactive');
            $interesse.closest('div').find('.titulo-interesse').css('color', '#049cdb');
        } else {
            $interesse.addClass('inactive');
            $interesse.closest('div').find('.titulo-interesse').css('color', '#999')
        }
    });

});

</script>