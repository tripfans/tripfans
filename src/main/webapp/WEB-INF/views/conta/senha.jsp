<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<style>
    //input.error { border: 1px solid red; width: auto; }
    label.error {
        background: url('http://dev.jquery.com/view/trunk/plugins/validate/demo/images/unchecked.gif') no-repeat;
        padding-left: 16px;
        margin-left: .3em;
    }
    label.valid {
        background: url('http://dev.jquery.com/view/trunk/plugins/validate/demo/images/checked.gif') no-repeat;
        display: block;
        width: 16px;
        height: 16px;
    }
</style>

<div class="span16">
    <div>

        <c:url value="/conta/alterarSenha" var="salvar_url" />
        <c:if test="${usuario.password == null}">
          <c:url value="/conta/criarSenha" var="salvar_url" />
        </c:if>
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="usuario" class="form-stacked">
    
            <div class="page-header">
                <h3>
                  <c:if test="${usuario.password == null}">
                    <s:message code="label.usuario.senha.criar.titulo" />
                  </c:if>
                  <c:if test="${usuario.password != null}">
                    <s:message code="label.usuario.senha.alterar.titulo" />
                  </c:if>
                </h3>
            </div>
            
            <span class="help-block">
                <h3>
                    <small>
                        <p>
                          <c:if test="${usuario.password == null}">
                            <s:message code="label.usuario.senha.criar.ajuda" />
                          </c:if>
                          <c:if test="${usuario.password != null}">
                            <s:message code="label.usuario.senha.alterar.ajuda" />
                          </c:if>
                        </p>
                    </small>
                </h3>
            </span>
            
            <fieldset>
            
                <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
              
                <c:if test="${usuario.password != null}">

                <li>
                    <label class="span4 fancy-text">
                        <span class="required">
                            <s:message code="label.usuario.senha.atual" />
                        </span>
                    </label>
                    <input type="password" id="senhaAtual" name="senhaAtual" value="" class="span4" />
                </li>             
            
                </c:if>

                <li>
                    <label class="span4 fancy-text">
                        <span class="required">
                            <s:message code="label.usuario.senha.nova" />
                        </span>
                    </label>
                    <input type="password" id="senhaNova" name="senhaNova" class="span4" />
                </li>             

                <li>
                    <label class="span4 fancy-text">
                        <span class="required">
                            <s:message code="label.usuario.senha.confirmar" />
                        </span>
                    </label>
                    <input type="password" id="confirmaSenha" name="confirmaSenha" class="span4" />
                </li>             

            </fieldset>
            
            <jsp:include page="barraAcoes.jsp"><jsp:param value="senha" name="pageToLoad"/></jsp:include>
            
        </form:form>
        
    </div>
</div>

<script>

$(document).ready(function () {
    
    $("#usuarioForm").validate({ 
        rules: {
        	senhaAtual: { 
                required: true,
                minlength : 6,
                maxlength : 20
            }, 
            senhaNova: { 
                required: true,
                minlength : 6,
                maxlength : 20
            }, 
            confirmaSenha: {
                required: true,
                equalTo: "#senhaNova",
                minlength : 6,
                maxlength : 20
            }, 
        }, 
        messages: { 
        	senhaAtual: {
                required : "<s:message code="validation.password.required" />",
                minlength : "<s:message code="validation.password.minlength" />",
                maxlength : "<s:message code="validation.password.maxlength" />"
            },
            senhaNova: {
                required : "<s:message code="validation.password.required" />",
                minlength : "<s:message code="validation.password.minlength" />",
                maxlength : "<s:message code="validation.password.maxlength" />"
            },
            confirmaSenha: {
            	required : "<s:message code="validation.password.required" />",
            	equalTo : "<s:message code="validation.confirmPassword.required" />"
            }
        } 
    });
    
});

</script>