<%@page import="br.com.fanaticosporviagens.model.entity.TipoOrganizacaoViagem"%>
<%@page import="br.com.fanaticosporviagens.model.entity.CategoriaPreferenciasViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<form:form id="usuarioForm" action="${param.salvarUrl}" modelAttribute="view" class="form-stacked">
        
	<c:if test="${view.preferenciasViagem.id != null}"><input type="hidden" name="preferenciasViagem" value="${view.preferenciasViagem.id}" /></c:if>

				<c:forEach items="<%=CategoriaPreferenciasViagem.values() %>" var="categoria" varStatus="count">
					<c:if test="${count.first || count.count % 2 == 1}">
	                  	<div class="row">
	               	</c:if>
					<div class="span7">
						<c:choose>
							<c:when test="${categoria.codigo == 3 || categoria.codigo == 4}">
								<c:set var="styleHeight" value="style='min-height: 180px;'" />
							</c:when>
							<c:otherwise><c:set var="styleHeight" value="" /></c:otherwise>
						</c:choose>
						<fieldset ${styleHeight}>
							<legend>
							    <s:message code="label.usuario.perfil.preferenciasViagem.${categoria.nome}" />
							</legend>
							<div class="controls">
								<c:forEach items="${categoria.tiposPreferencias}" var="tipo">
									<label class="checkbox">
										<form:checkbox path="preferenciasViagem.${categoria.nome}.${tipo.nome}" />
										<span>${tipo.descricao}</span>
									</label>
								</c:forEach>
							</div>
						</fieldset>
					</div>
		                     
					<c:if test="${count.count % 2 == 0 && count.last == false}">
						</div> <!-- row -->
					</c:if>
		                  
					<c:if test="${count.last}">
						<div class="span7">
						    <fieldset>
						    	<legend><s:message code="label.usuario.perfil.preferenciasViagem.tipoOrganizacao" /></legend>
						    	<div class="controls">
						    		<label class="radio">
						    			<form:radiobutton path="preferenciasViagem.tipoOrganizacao" value="<%=TipoOrganizacaoViagem.POR_CONTA_PROPRIA %>" />
						    			<span><%=TipoOrganizacaoViagem.POR_CONTA_PROPRIA.getDescricao() %></span>
					    			</label>
						           	<label class="radio">
						               	<form:radiobutton path="preferenciasViagem.tipoOrganizacao" value="<%=TipoOrganizacaoViagem.PACOTE_TURISTICO %>" />
						               	<span><%=TipoOrganizacaoViagem.PACOTE_TURISTICO.getDescricao() %></span>
						           	</label>
						           	<label class="radio">
						               	<form:radiobutton path="preferenciasViagem.tipoOrganizacao" value="<%=TipoOrganizacaoViagem.EXCURSAO %>" />
						               	<span><%=TipoOrganizacaoViagem.EXCURSAO.getDescricao() %></span>
						           	</label>
						      	</div>
						    </fieldset>
					    </div>
					    
					    </div> <!-- row -->
					</c:if>
		                  
				</c:forEach>
              
    <c:if test="${param.mostraBarraAcoes}">
	    <div class="span15">            
	    	<jsp:include page="barraAcoes.jsp"><jsp:param value="preferencias" name="pageToLoad"/></jsp:include>
	    </div>
    </c:if>

</form:form>