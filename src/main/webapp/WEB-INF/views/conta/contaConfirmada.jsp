<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="tripfans.responsive.new">

	<c:choose>
		<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
			<c:set var="reqScheme" value="https" />
		</c:when>
		<c:otherwise>
			<c:set var="reqScheme" value="http" />
		</c:otherwise>
	</c:choose>

	<tiles:putAttribute name="title" value="Sua conta foi confirmada - TripFans | Fanáticos Por Viagens" />

	<tiles:putAttribute name="stylesheets">

		<link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
		<link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>


		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/bootstrap/bootstrap3/css/bootstrap.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/pageResponsive.css" />" />
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/baseResponsive.css" />" />

		<c:choose>
			<c:when test="${isMobile}">
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection" />
			</c:when>
			<c:otherwise>
				<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection" />
			</c:otherwise>
		</c:choose>

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/components/mmenu/jquery.mmenu.all.css" type="text/css" media="screen, projection" />

		<style>
.page-header {
	padding-bottom: 9px;
	margin: 10px 0 20px;
	border-bottom: 1px solid #eeeeee;
}
</style>

		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-1.7.1.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/scripts/jquery-ui-1.8.16.custom.min.js"/>"></script>
		<script type="text/javascript" src="<c:url value="/resources/components/bootstrap/bootstrap3/js/bootstrap.js"/>"></script>

		<script type="text/javascript" src="<c:url value="/resources/scripts/fanaticos.js"/>"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery-validation-1.9.js"></script>


	</tiles:putAttribute>

	<tiles:putAttribute name="footer">
		<script type="text/javascript">
            $(document).ready(function () {
            	 $('#fazer-login').click( function(event) {
                 	event.preventDefault();
                 	$(".login-modal-link").trigger('click');
                 });
            });
        </script>
	</tiles:putAttribute>

	<tiles:putAttribute name="leftMenu">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">

		<div class="col-md-12">

			<div class="" style="text-align: center; padding: 5% 0;">
				<img src="<c:url value="/resources/images/tripfans-mala.png" />" />
				<h1>Muito obrigado por confirmar sua conta!</h1>
				<p class="lead">Seu e-mail foi confirmado com sucesso.</p>
				
					<a href="<c:url value="/viagem/planejamento/inicio"/>" class="btn btn-lg btn-success" style="font-weight: bold; text-transform: uppercase;"> Quero criar meu roteiro
									agora! </a>
				
			</div>

		</div>

	</tiles:putAttribute>

</tiles:insertDefinition>