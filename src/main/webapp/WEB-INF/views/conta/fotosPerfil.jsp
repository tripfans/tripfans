<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% long dataAtual = System.currentTimeMillis(); %>

<div class="span16">
    <div>
    
        <c:url value="/conta/salvarFoto" var="form_url" />
        <c:url value="/conta/cortarFoto" var="urlCortarFoto" />
    
        <form:form id="usuarioForm" action="${form_url}" modelAttribute="contaUsuario" enctype="multipart/form-data" class="form-stacked">
         
            <div class="page-header">
                <h3><s:message code="label.usuario.perfil.foto.titulo" /></h3>
            </div>
            
            <div class="span16" >
            	<fan:helpText text="Sua foto deve estar nos formatos .jpg, .gif, ou .png, com tamanho m�ximo de 4 MB e de prefer�ncia no formato paisagem com 
                    resolu��o at� 1024 x 768.
                    <br>
                    Fotos maiores ser�o redimensionadas e podem ficar granuladas ou distorcidas.
                    <br>
                    Por favor, n�o fa�a upload de imagens contendo celebridades, nudez, trabalhos art�sticos e imagens protegidas por direitos autorais." showQuestion="false"/>
            </div>
            
            <div class="horizontalSpacer"></div>
            
            <div class="row">
                                    
                <div class="span4">&nbsp;</div>
                
                <div class="span5" style="min-height: 215px; min-width: 280px; background-color: #eee;">
                    <%-- Foto do usuario --%>
                    <div class="fotoPerfil" data-h="0" data-w="0" data-y="0" data-x="0" data-editheight="360" data-editwidth="480" data-editratio="0.43956044">
                        <c:choose>
                            <c:when test="${contaUsuario.fotoExterna}">
                                <img id="profile_pic" class="moldura original" src="<c:url value="${contaUsuario.urlFotoPerfil}"/>" />
                            </c:when>
                            <c:otherwise>
                                <img id="profile_pic" class="moldura original" src="<c:url value="${contaUsuario.urlFotoAlbum}?d=<%=dataAtual%>"/>" />                            
                            </c:otherwise>
                        </c:choose>
                    </div>
                    
                </div>
                
                <div class="span6" style="margin-top: 90px;">

                    <div class="span6" style="margin-left: 0px;">
                        Selecione uma foto de seu computador (M�ximo de 4 MB):
                    </div>

                    <%-- Div necess�ria para funcionar o file-uploader (o bot�o de selecionar arquivo � renderizado aqui) --%>
                    <div id="file-uploader"></div>

                    <div class="horizontalSpacer"></div>
                
                </div>
                
            </div>
            
            <div class="row" style="padding-top: 6px;">
            
                <div class="span4">&nbsp;</div>

                <div class="span5">
                    <%-- Verifica se a foto est� armazenada do servidor de imagens --%>
                    <c:if test="${contaUsuario.foto.tipoArmazenamentoLocal && !contaUsuario.foto.anonima}">
                    
                      <div class="row">
                    
                        <div class="span2"  style="padding-right: 8px;">
                            <a id="editarFoto-dialog-link" href="#editarFoto-dialog" class="btn btn-info edicaoFoto span2">
                                <i class="icon-pencil icon-white"></i> Editar foto
                            </a>
                        </div>
                        <div class="span2">
                            <a href="#" class="btn btn-danger remocaoFoto span2" style="width: 100px">
                                <i class="icon-remove icon-white"></i> Remover foto
                            </a>
                        </div>
                        
                      </div>
                    
                    </c:if>
                    <%-- Verifica se a foto � externa e se a foto perfil nao � anonima --%>
                    <c:if test="${contaUsuario.foto.tipoArmazenamentoExterno}">
                        <a href="#" class="btn btn-danger remocaoFoto" style="width: 100px">Remover foto</a>
                    </c:if>
                </div>

            </div>
                
            <script type="text/javascript">
            
                $('#editarFoto-dialog-link').fancybox({
                    'modal' : true,
                    'hideOnContentClick': false,
                    'scrolling'     : 'no',
                    'titleShow'     : false,
                    'width'         : 460,
                    'height'        : 500,
                    'hideOnOverlayClick' : false, 
                    'onClosed'      : function() {
                        
                    },
                    'onComplete': function() {
                    	$('#modal-body-content').load('<c:url value="/conta/carregarFotoCrop" />');
                    }
                });
                
                $('.remocaoFoto').click(function(event) {
                	event.preventDefault();
                	jQuery.ajax({
                        type: 'POST',
                        url: '<c:url value="/conta/removerFotoPerfil" />',
                        data: { 
                            'usuario' : '${usuario.urlPath}' 
                        },
                        success: function(data) {
                            if (data.success) {
                            	$('#conteudo').load('<c:url value="/conta/fotosPerfil" />?d=' + new Date().getTime());
                            }
                        }
                    });
                });
                
                function carregarFoto() {
                    jQuery.ajax({
                        type: 'POST',
                        url: '${urlCortarFoto}',
                        data: { 
                            'x' : $('#x').val(), 
                            'y' : $('#y').val(),
                            'width' : $('#w').val(), 
                            'height' : $('#h').val(),
                        },
                        success: function(data) {
                            if (data.success) {
                            	setTimeout($('#conteudo').load('<c:url value="/conta/fotosPerfil" />?d=' + new Date().getTime()), 3000);
                            }
                        }
                    });
                }
            
            </script>
            
            <script>
            
                // Configurar o File-uploader 
                var uploader = new qq.FileUploader({
                    element: document.getElementById('file-uploader'),
                    action: '${form_url}',
                    allowedExtensions: ['jpg', 'jpeg', 'png'],
                    multiple: false,
                    onSubmit: function(id, fileName) {
                        $(".edicaoFoto").css("display", "none");
                        $(".remocaoFoto").css("display", "none");
                    },
                    onProgress: function(id, fileName, loaded, total) {
                    	$(".fotoPerfil").html('<img id="profile_pic" src="<c:url value="/resources/images/loading.gif" />" style="margin-left: 124px; margin-top: 80px;" />');
                    },
                    onComplete: function(id, fileName, responseJSON) {
                        $(".qq-upload-list").html("");
                        $('#conteudo').load('<c:url value="/conta/fotosPerfil" />');
		            },
                    onCancel: function(id, fileName) {
                        $(".edicaoFoto").css("display", "");
                        $(".remocaoFoto").css("display", "");
                        $(".qq-upload-list").html("");
                    },
                    template: '<div class="qq-uploader">' +
                                  '<div class="qq-upload-drop-area"><span></span></div>' +
                                  '<div class="qq-upload-button btn btn-primary span2" style="margin-left: 0px;">Selecionar foto</div>' +
                                  '<ul class="qq-upload-list unstyled span6"></ul>' +
                              '</div>',
                    fileTemplate:  '<li>' +
                                      '<span class="qq-upload-file"></span>' +
                                      '<span class="qq-upload-spinner"></span>' +
                                      '<span class="qq-upload-size"></span>' +
                                      '<a class="qq-upload-cancel label important" href="#">Cancelar</a>' +
                                      '<span class="qq-upload-failed-text">Falhou</span>' +
                                   '</li>'
                });
                                        
                
            </script> 
            
            <%--jsp:include page="barraAcoes.jsp">
                <jsp:param name="buttonTitle" value="Conclu�do" />
                <jsp:param name="pageToLoad" value="fotosPerfil" />
                <jsp:param name="showShareButton" value="true" />
            </jsp:include--%>            
                
        </form:form>
        
    </div>
</div>

<%-- Janela modal para exibir a foto que ser� feita o 'Crop' --%>

<div style="display:none">
    <div id="editarFoto-dialog" title="" style="width: 820px;">
    <div class="modal-header">
        <h3>Editar foto</h3>
    </div>
    
    <div class="modal-body">
        <div style="min-height: 300px;">
            <div id="modal-body-content" style="width: 100%">
              <div style="margin-top: 150px;" align="center">
                <p>
                    Por favor, aguarde...
                    <br/>
                    <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
                </p>
              </div>
            </div>        
        </div>
    </div>
    
    <div class="modal-footer">
        <div align="right" class="right">
            <a id="btnCancel" class="btn btn-danger span2" href="#" onclick="$.fancybox.close();">
                <i class="icon-remove icon-white"></i> Cancelar
            </a>
            <a id="btnCrop" class="btn btn-primary span2" href="#" onclick="$.fancybox.close(); carregarFoto();">
                <i class="icon-ok icon-white"></i> Salvar
            </a>
        </div>    
    </div>
        
</div>
</div>