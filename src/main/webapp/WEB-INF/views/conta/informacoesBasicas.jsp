<%@page import="java.util.Date"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoExibicaoDataNascimento"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div>
        <c:url value="/conta/salvarAlteracoes" var="salvar_url" />
        
        <div class="page-header">
	         <h3>
		        <s:message code="label.usuario.perfil.informacoesBasicas.titulo" />
		    </h3>
		</div>
        
       <s:message var="texto" code="textoAjuda.informacoesBasicas" />
  		<fan:helpText text="${texto}" showQuestion="false"/>

		<jsp:include page="formInformacoesBasicas.jsp">
        	<jsp:param name="salvar_url" value="${salvar_url}"/>
        	<jsp:param name="mostraBarraAcoes" value="true"/>
        </jsp:include>

    </div>
</div>