<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {

            })
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div class="container">
        
            <div class="span-24">
            </div>        
        
        </div>
    
    
        User : ${newUser}
        
        <c:if test="${newUser}">
            <c:url value="/signin" var="form_url_login" />
            <s:message code="label.login"/>
        </c:if>

        <div id="cadastro-dialog" title="Vamos completar seu cadastro...">
            <form>
                <fieldset>
                
                    <label>
                        <strong class="email-label">
                            <s:message code="label.username" />
                        </strong>
                        <input name="j_username" id="confirmacao_email" value="" type="text">
                    </label>
                    <label>
                        <strong class="passwd-label">
                            <s:message code="label.password" />
                        </strong>
                        <input name="j_password" id="confirmacao_senha" type="password" autocomplete="off" autocorrect="off" autocapitalize="off">
                    </label>
                
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all" />
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" />
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
                </fieldset>
            </form>
        </div>

    </tiles:putAttribute>
    
</tiles:insertDefinition>