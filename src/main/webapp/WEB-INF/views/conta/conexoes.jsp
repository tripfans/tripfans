<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">
    <div>
    
		<div class="page-header">
            <h3>
                Conex�es com outras redes
            </h3>
        </div>
        
		<s:message var="texto1" code="textoAjuda.conexoesExternas1" />
		<s:message var="texto2" code="textoAjuda.conexoesExternas2" />
		<s:message var="texto3" code="textoAjuda.conexoesExternas3" />
		<s:message var="texto4" code="textoAjuda.conexoesExternas4" />
		<fan:helpText text="${texto1}${texto2}${texto3}${texto4}" showQuestion="false"/>
        
        <jsp:include page="formConexoesExternas.jsp" />
        
    </div>
</div>