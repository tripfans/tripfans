<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteressesViagem"%>
<%@page import="br.com.fanaticosporviagens.model.entity.TipoOrganizacaoViagem"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.PreferenciasViagem" %>
<%@ page import="br.com.fanaticosporviagens.model.entity.InteressesViagem" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.borda-arredondada {
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    -ms-border-radius: 4px;
    -o-border-radius: 4px;
    border-radius: 4px;
}

.titulo-interesse .active, .titulo-interesse:hover {
    color : #049cdb;
}

</style>

<div class="span16">
    <div>
    
        <div class="page-header">
            <h3>
                <s:message code="label.usuario.perfil.interesses.titulo" />
            </h3>
        </div>
        
        <c:url value="/conta/salvarPreferenciasInteressesViagem" var="salvarUrl" />

		<s:message var="texto1" code="textoAjuda.interessesViagem1" />
		<s:message var="texto2" code="textoAjuda.interessesViagem2" arguments="Salvar alterações"/>
		<fan:helpText text="${texto1}${texto2 }"  showQuestion="false"/>

		<jsp:include page="formInteresses.jsp">
        	<jsp:param name="salvarUrl" value="${salvarUrl}"/>
        	<jsp:param name="mostraBarraAcoes" value="true"/>
        </jsp:include>
                        
    </div>
</div>