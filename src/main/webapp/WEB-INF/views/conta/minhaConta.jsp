<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Configura��es de Conta - TripFans" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>

    <tiles:putAttribute name="footer">
    
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/bday-picker-pt_BR.js"></script>
        
        <script type="text/javascript">
        
            <c:set var="tab" value="${pagina != null ? pagina : param.tab}" />
        
            $(document).ready(function () {
                
                var tab = '${tab}';
                if (tab == 'perfil' || tab == null) {
                    tab = 'informacoesBasicas';
                } else if (tab == 'config') {
                    tab = 'conexoes';
                } else if (tab == 'privacidade') {
                    tab = 'visibilidade';
                }
                
                carregarPagina(tab);
                $('#' + tab).parent().addClass('active');
                
                $('a.menu-item').bind('click',function(event) { //When any link is clicked
                	$('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    $(this).parent().addClass('active'); //Set clicked link class to active
                    carregarPagina(this.id);
                });
                
                //$('.sidebar-nav').stickyMojo({footerID: '#footer-content', contentID: '#conteudo', topSpacing: '70px'});
                $.lockfixed('#menuLateral', { offset: { top: 70, bottom: 386 } });
            });
            
            function carregarPagina(pagina) {
                var url = '<c:url value="/conta/" />' + pagina;
                $('body').scrollTop(0);
            	$('#conteudo').load(url, function () {
	            	$('#conteudo').initToolTips();
	            	redimensionarMenu();
            	    //window.history.pushState( url, window.title, url );
            	    History.pushState(url, window.title, url);
            	    $.lockfixed('#menuLateral', { offset: { top: 70, bottom: 386 } });
            	});
            }
            
            function redimensionarMenu() {
                /*$('#ulSidenav').css({'height': '420px'});
                $('#ulSidenav').css({'height': $('#footer-content').offset().top - 145});*/
            }
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <meta http-equiv="cache-control" content="no-cache,must-revalidate">
        <meta http-equiv="expires" content="0">
        <meta http-equiv="pragma" content="no-cache">

        <style>
            .sidebar-box {
                padding: 8px 11px 6px;
                border-width: 1px;
                border-style: solid;
                border-color: #E3E3E3;
            }        
        </style>
        
        <div class="container-fluid page-container">
            	<div id="tituloPagina">
					<div class="title gradient-background">
    					<span class="title">
    					${usuario.displayName} - Configura��es
    					</span>
    					<div class="pull-right" style="margin-top: 10px;">
    						<a href="<c:url value="/perfil/${usuario.urlPath}" />" class="btn btn-mini btn-info" id="btnVerPerfil">
    						     <i class="icon-eye-open icon-white"></i>
    						     <b>Ver perfil p�blico</b>
    						</a>
    					</div>
					</div>
				</div>
                
                <div class="row">
                	<div class="span3 menu-lateral" id="menuLateral">
                		<div class="sidebar-nav pull-left" style="margin: 0px;">
	                    	<div class="tabbable tabs-left" style="background-color: rgb(240, 244, 245); border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	                          <ul id="ulSidenav" class="nav nav-tabs span3" style="margin-left: 0px; border-right: 0px;">
	                            <li class="nav-header">
	                                &nbsp;&nbsp;<s:message code="label.usuario.perfil" />
	                            </li>
	                        
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="informacoesBasicas" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/vcard.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
	                                    <span><s:message code="label.usuario.perfil.informacoesBasicas.titulo" /></span>
	                                </a>
	                            </li>
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="fotosPerfil" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/camera.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
	                                    <span><s:message code="label.usuario.perfil.foto.titulo" /></span>
	                                </a>
	                            </li>
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="interesses" href="#" class="menu-item"> 
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/world.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
	                                    <span><s:message code="label.usuario.perfil.interesses.titulo" /></span>
	                                </a>
	                            </li>
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="preferencias" href="#" class="menu-item"> 
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/luggage.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
	                                    <span><s:message code="label.usuario.perfil.preferenciasViagem.titulo" /></span>
	                                </a>
	                            </li>
	                            
	                            <li class="nav-header">
	                                &nbsp;&nbsp;<s:message code="label.usuario.perfil.configuracoes" />
	                            </li>
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="conexoes" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/disconnect.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
	                                    <span class="menu_item_text">Conex�es com outras redes</span>
	                                </a>
	                            </li>
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="notificacoes" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/email.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;"></span>
	                                    <span><s:message code="label.usuario.perfil.notificacoes" /></span>
	                                </a>
	                            </li>
	                            <%--c:if test="${usuario.importado == false}"--%>
	                              <li class="blackAndWhite colorOnHover">
	                                <a id="senha" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/key.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
	                                    <span class="menu_item_text"><s:message code="label.usuario.senha" /></span>
	                                </a>
	                              </li>
	                            <%--/c:if--%>
	                            
	                            <%--li class="nav-header">
	                                &nbsp;&nbsp;<s:message code="label.usuario.perfil.privacidade" />
	                            </li>
	                            
	                            <li class="blackAndWhite colorOnHover">
	                                <a id="visibilidade" href="#" class="menu-item">
	                                    <span style="background-image: url('<c:url value="/resources/images/icons/lock.png" />'); background-repeat: no-repeat; display: inline-block; height: 16px; width: 16px;" ></span>
	                                    <span class="menu_item_text"><s:message code="label.usuario.visibilidade.titulo" /></span>
	                                </a>
	                            </li--%>                            
	                            
	                           </ul>
	                    	</div>
                        
                    	</div>
                	</div>
                    
                    <div id="pageContent" class="sidebar_content" style="padding-right: 15px;">
                        <div id="conteudo">
                        </div>
                    </div>
                </div>
            
        </div>
        
    <script>
        var tab = '${param.tab}';
        tab = (tab == 'perfil' ? 0 : (tab == 'config' ? 1 : 2));
	</script>        
    </tiles:putAttribute>
    
</tiles:insertDefinition>