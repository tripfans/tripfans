<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteressesViagem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<fieldset>
        
     <table class="table table-striped" style="width: 100%">
 
         <tbody>
         
             <tr>
                 <td>
                     <div class="servicos">
                         <ul class="servicos">
                             <li class="facebook" style="vertical-align: middle;">
                                 <b></b>
                             </li>
                             <li style="vertical-align: middle;">
                                 Facebook
                             </li>
                         </ul>
                     </div>
                 </td>
                 <td style="text-align: right;">
                     <c:choose>
                       <c:when test="${contaUsuario.conectadoFacebook}">
                         <c:choose>
                           <c:when test="${contaUsuario.importadoFacebook}">
                             <a class="btn btn-success btn-large span3 disabled" href="#">Conectado</a>
                           </c:when>
                           <c:otherwise>
                             <form name="fb_signin_disconnect" action="<c:url value="/connect/facebook"/>" method="POST" style="margin: 0px;">
                                 <input type="hidden" name="_method" value="delete" />
                                 <input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             	 <input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                             </form>
                             <a class="btn btn-warning btn-large span3" href="#" onclick="document.forms['fb_signin_disconnect'].submit()" title="Desconectar-se do Facebook">Desconectar</a>
                           </c:otherwise>
                         </c:choose>
                       </c:when>
                       <c:otherwise>
                         <form name="fb_signin_connect" action="<c:url value="/connect/facebook"/>" method="POST" style="margin: 0px;">
                             <input type="hidden" name="scope" value="email,user_status,user_hometown,user_checkins,user_location,user_photos,publish_stream,friends_hometown,friends_location,friends_photos" />
                             <input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             <input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                         </form>
                         <a class="btn btn-primary btn-large span3" href="#" onclick="document.forms['fb_signin_connect'].submit()" title="Conectar-se ao Facebook">
                             Conectar
                         </a>
                       </c:otherwise>
                     </c:choose>                                
                 </td>
             </tr>
             
             <tr>
                 <td>
                     <div class="servicos">
                         <ul class="servicos">
                             <li class="foursquare" style="vertical-align: middle;">
                                 <b></b>
                             </li>
                             <li style="vertical-align: middle;">
                                 Foursquare
                             </li>
                         </ul>
                     </div>
                 </td>
                 <td style="text-align: right;">
                     <c:choose>
                       <c:when test="${contaUsuario.conectadoFoursquare}">
                         <form name="form_foursquare_disconnect" action="<c:url value="/connect/foursquare"/>" method="POST" style="margin: 0px;">
                             <input type="hidden" name="_method" value="delete" />
                             <input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             <input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                         </form>
                         <a class="btn btn-warning btn-large span3" href="#" onclick="document.forms['form_foursquare_disconnect'].submit()" title="Desconectar-se do Foursquare">Desconectar</a>
                       </c:when>
                       <c:otherwise>
                         <form name="form_foursquare_connect" action="<c:url value="/connect/foursquare"/>" method="POST" style="margin: 0px;">
                         		<input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             	<input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                         </form>
                         <a class="btn btn-primary btn-large span3" href="#" onclick="document.forms['form_foursquare_connect'].submit()" title="Conectar-se ao Foursquare">
                             Conectar
                         </a>
                       </c:otherwise>
                     </c:choose>                                
                 </td>
             </tr>
             
             <tr>
                 <td>
                     <div class="servicos">
                         <ul class="servicos">
                             <li class="twitter" style="vertical-align: middle;">
                                 <b></b>
                             </li>
                             <li style="vertical-align: middle;">
                                 Twitter
                             </li>
                         </ul>
                     </div>
                 </td>
                 <td style="text-align: right;">
                     <c:choose>
                       <c:when test="${contaUsuario.conectadoTwitter}">
                         <form name="form_twitter_disconnect" action="<c:url value="/connect/twitter"/>" method="POST" style="margin: 0px;">
                             <input type="hidden" name="_method" value="delete" />
                             <input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             <input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                         </form>
                         <a class="btn btn-warning btn-large span3" href="#" onclick="document.forms['form_twitter_disconnect'].submit()" title="Desconectar-se do Twitter">Desconectar</a>
                       </c:when>
                       <c:otherwise>
                         <form name="form_twitter_connect" action="<c:url value="/connect/twitter"/>" method="POST" style="margin: 0px;">
                         		<input type="hidden" name="redirectUrl" value="${param.redirectUrl}" />
                             	<input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlOnError}" />
                         </form>
                         <a class="btn btn-primary btn-large span3" href="#" onclick="document.forms['form_twitter_connect'].submit()" title="Conectar-se ao Twitter">
                             Conectar
                         </a>
                       </c:otherwise>
                     </c:choose>                                
                 </td>
             </tr>

         </tbody>
         
     </table>                
            
</fieldset>