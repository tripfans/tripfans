<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="mainButtonTitle" value="${not empty param.buttonTitle ? param.buttonTitle : 'Salvar alterações'}" />
<c:url var="urlToShare" value="${param.urlToShare}" />
<c:if test="${empty urlToShare}">
  <c:url var="urlToShare" value="/perfil/${contaUsuario.urlPath}" />
</c:if>

<c:set var="titulo" value="${param.tituloToShare}" />
<c:set var="subTitulo" value="${param.subTituloToShare}" />
<c:set var="pictureUrl" value="${param.pictureUrlToShare}" />
<c:set var="texto" value="${param.textoToShare}" />

<div style="width: 100%">
    <div align="right">
        <div class="form-actions">
            <c:if test="${param.showShareButton}">
                <fan:shareButton id="btn_fb_share" facebook="true" esconderBotao="true" usarComoCheckbox="true"
                     url="${urlToShare}" pictureUrl="${pictureUrl}" titulo="${titulo}" subTitulo="${subTitulo}" texto="${texto}" />
            </c:if>
            <input id="btn-form-submit" type="submit" value="${mainButtonTitle}" class="btn btn-primary" disabled="disabled" />
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        $('#usuarioForm').unbind('submit');
        
        $('#usuarioForm :input').change(function() {
        	$('#btn-form-submit').enable();
        	<c:if test="${param.showShareButton}">
        	    $('#btn_fb_share').show();
        	</c:if>
        });
        
        $('#usuarioForm').submit(function(event) {
            event.preventDefault();
            var formData = $('#usuarioForm').serialize();
            
            if ($('#usuarioForm').valid()) {
                $.ajax({
                    type: 'POST',
                    url: document.forms['usuarioForm'].action,
                    data: formData,
                    success: function(data, status, xhr) {
                        if (data.error != null) {
                            $('#errorMsg').empty().append(data.error);
                            $('#errors').show();
                        } else {
                            <c:if test="${param.showShareButton}">
                            var selectedFBShare = $('#btn_fb_share').data('selected');
                            if (selectedFBShare) { 
                                var nomeDaFuncaoShare = $('#btn_fb_share').data('function-name');
                                //$.fn[nomeDaFuncaoShare]();
                                //window[nomeDaFuncaoShare]();
                            }
                            </c:if>
    				    	carregarPagina('${param.pageToLoad}');
                        }
                    }
                });
            }
        });
        
    });
</script>
