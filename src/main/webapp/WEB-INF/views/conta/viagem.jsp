<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div style="padding-left: 20px;">
    <div>
    
        <c:url value="/conta/salvarAlteracoes" var="salvar_url" />
    
        <form:form id="usuarioForm" action="${salvar_url}" modelAttribute="contaUsuario">
    
            <div class="popup-subtitle">Prefer�ncias de Viagem</div>
            
            <fieldset style="border-left: medium none; border-right: 0 none; border-style: none none solid; border-width: 0 0 1px medium; border-color: #CCCCCC">
                <legend>Interesses</legend>
                
                <hr class="space"/>
                
                <div class="span-18 last">
                    <label>
                        Estilo de viagem
                        <br/>
                        <form:select id="genero" path="genero" style="width: 150px;">
                            <option>-- Selecione --</option>
                            <c:forEach items="${tiposViagem}" var="tipo">
                                <option value="${tipo.id}">${tipo.descricao}</option>
                            </c:forEach>
                        </form:select>
                    </label>
               </div>
                
            </fieldset>
            
            <fieldset style="border-left: medium none; border-right: 0 none; border-style: none none solid; border-width: 0 0 1px medium; border-color: #CCCCCC">
                <legend>Formatos dos dados</legend>
                
                <hr class="space"/>
                
                <div class="span-18 last">
                    <label>
                        Data
                        <form:select path="preferencias.formatoData" style="width: 150px;">
                            <option value="0">Dia/M�s/Ano</option>
                            <option value="1">M�s/Dia/Ano</option>
                            <option value="2">Ano/M�s/Dia</option>
                        </form:select>
                    </label>
                    <label>
                        <strong>
                            Hora
                        </strong>
                        <form:select path="preferencias.formatoHora" style="width: 150px;">
                            <option value="0">HH:mm</option>
                            <option value="1">hh:mm AM/PM</option>
                        </form:select>
                    </label>                                
                </div>
                
                <div class="span-18 last">
                    <label>
                        Dist�ncia
                        <form:select path="preferencias.formatoDistancia" style="width: 150px;">
                            <option value="0">KM</option>
                            <option value="1">Milhas</option>
                        </form:select>
                    </label>
                </div>

                <div class="span-18 last">
                    <label>
                        Temperatura
                        <form:select path="preferencias.formatoTemperatura" style="width: 150px;">
                            <option value="0">Celsius (�C)</option>
                            <option value="0">Fahrenheit (�F)</option>
                        </form:select>
                    </label>
                </div>
                
            </fieldset>
            
            <div align="right">
                <input type="submit" value="Salvar altera��es" />
            </div>
            
        </form:form>
        
    </div>
</div>