<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<% long dataAtual = System.currentTimeMillis(); %>

<script type="text/javascript">

    // Script que recupera as dimens�es da foto do usu�rio e a posicao do "Crop"
    
    var x = Math.round($(".fotoPerfil").attr("data-x")
            * $(".fotoPerfil").attr("data-editRatio"));
    var y = Math.round($(".fotoPerfil").attr("data-y")
            * $(".fotoPerfil").attr("data-editRatio"));
    var x2 = Math.round(($(".fotoPerfil").attr("data-x") * $(".fotoPerfil").attr(
            "data-editRatio"))
            + ($(".fotoPerfil").attr("data-w") * $(".fotoPerfil").attr(
                    "data-editRatio")));
    var y2 = Math.round(($(".fotoPerfil").attr("data-y") * $(".fotoPerfil").attr(
            "data-editRatio"))
            + ($(".fotoPerfil").attr("data-h") * $(".fotoPerfil").attr(
                    "data-editRatio")));
    if (x == 0 && x2 == 0 && y == 0 && y2 == 0) {
        var a = parseInt($(".fotoPerfil").attr("data-editWidth"));
        var c = parseInt($(".fotoPerfil").attr("data-editHeight"));
        if (a >= c) {
            x = a/2 - 20;// Math.round($(".fotoPerfil").attr("data-editWidth") / 6);
            y = c/2 - 20;
            x2 = x + 40;
            y2 = y + 40;
        } else {
            x = 0;
            y = Math.round($(".fotoPerfil").attr("data-editHeight") / 6);
            x2 = a;
            y2 = y + a
        }
    }
    
    // Create variables (in this scope) to hold the API and image size
    var jcrop_api, boundx, boundy;
    
    function initJcrop() {
        
        jQuery('#target').Jcrop({
            aspectRatio: 1,
            minSize : [ 200 * $(".fotoPerfil").attr("data-editRatio"),
                    200 * $(".fotoPerfil").attr("data-editRatio") ],
            setSelect : [ parseInt(x), parseInt(y), parseInt(x2), parseInt(y2) ],
            allowSelect: false,
            onChange: showPreview,
            onSelect: showPreview
        }, function() {
            // Use the API to get the real image size
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];
            // Store the API in the jcrop_api variable
            jcrop_api = this;
        });
        
    }
    
    /**
     * Renderiza a janela com a pre-visualizacao
     */
    function showPreview(coords) {
        if (parseInt(coords.w) > 0) {
            var rx = 280 / coords.w;
            var ry = 280 / coords.h;

            $('#preview').css({
                width: Math.round(rx * boundx) + 'px',
                height: Math.round(ry * boundy) + 'px',
                marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                marginTop: '-' + Math.round(ry * coords.y) + 'px'
            });
        }
        // Atualizar os campos com as coordenadas para fazer o 'crop'
        updateCoords(coords);
    }
    
    /**
     * Atualiza os campos do formulario com as coordenadas
     */
    function updateCoords(coords) {
        jQuery('#x').val(Math.round(coords.x));
        jQuery('#y').val(Math.round(coords.y));
        jQuery('#w').val(Math.round(coords.w));
        jQuery('#h').val(Math.round(coords.h));
    };
    
    /**
     * Verifica se o usuario selecionou uma parte da foto para o 'crop'
     */
    function checkCoords() {
        if (parseInt(jQuery('#w').val())>0) return true;
        alert('Please select a crop region then press submit.');
        return false;
    };
    
    // Criar o componente Jcrop
    initJcrop();
    
</script>

<!-- Formulario que enviar� as coordenadas para cortar a foto -->
<form id="cropForm" method="post" onsubmit="return checkCoords();" style="margin: 0px;">
    <input type="hidden" id="x" name="x" value="100" />
    <input type="hidden" id="y" name="y" value="100" />
    <input type="hidden" id="w" name="w" value="100" />
    <input type="hidden" id="h" name="h" value="100" />
</form>

<table>
    <tr>
        <td>
            Foto enviada
        </td>
        <td width="10px">
            
        </td>
        <td>
            Pr�-visualiza��o
        </td>
    </tr>
    <tr>
        <td>
            <div id="cropBox" class="cropBox" style="border: 1px solid #E3E3E3;">
                <img id=target src="${contaUsuario.urlFotoCrop}?d=<%=dataAtual%>" width="480"/>
            </div>
        </td>
        <td>
        </td>
        <td valign="top">
            <div style="width: 280px; height: 280px; overflow: hidden; border: 1px solid #E3E3E3;">
                <img src="${contaUsuario.urlFotoCrop}?d=<%=dataAtual%>" id="preview" alt="Preview" class="jcrop-preview" />
            </div>
        </td>
    </tr>
</table>