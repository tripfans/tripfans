<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="fanaticos.default">

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
        <script type="text/javascript">
            $(document).ready(function () {
            	
                $( "#cadastro-dialog" ).dialog({
                            autoOpen: true,
                            height: 300,
                            width: 350,
                            modal: true,
                            closeOnEscape : false,
                            draggable : false,
                            resizable : false,
                            buttons: {
                                "<s:message code="label.continuar" />": function() {
                                    var bValid = true;
                                    allFields.removeClass( "ui-state-error" );
                                    
                                    bValid = true;
                
                                    /*bValid = bValid && checkLength( name, "username", 3, 16 );
                                    bValid = bValid && checkLength( email, "email", 6, 80 );
                                    bValid = bValid && checkLength( password, "password", 5, 16 );
                
                                    bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
                                    // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                                    bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
                                    bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );*/
                
                                    /*if ( bValid ) {
                                        $( "#users tbody" ).append( "<tr>" +
                                            "<td>" + name.val() + "</td>" + 
                                            "<td>" + email.val() + "</td>" + 
                                            "<td>" + password.val() + "</td>" +
                                        "</tr>" ); 
                                        $( this ).dialog( "close" );
                                    }*/
                                }/*,
                                Cancel: function() {
                                    $( this ).dialog( "close" );
                                }*/
                            },
                            beforeClose: function() {
                                //allFields.val( "" ).removeClass( "ui-state-error" );
                            	//$( "#dialog-form" ).dialog( "open" );
                            	return false;
                            }
                        });
                
                        /*$( "#create-user" )
                            .button()
                            .click(function() {
                                $( "#dialog-form" ).dialog( "open" );
                            });*/            	

            })
        </script>
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <style>
            body { font-size: 62.5%; }
            label, input { display:block; text-align : left; }
            input.text { margin-bottom:12px; width:95%; padding: .4em; }
            fieldset { padding:0; border:0; margin-top:25px; }
            h1 { font-size: 1.2em; margin: .6em 0; }
            .ui-dialog .ui-state-error { padding: .3em; }
            .validateTips { border: 1px solid transparent; padding: 0.3em; }
        </style>    
    
    
        User : ${newUser}
        
        <c:if test="${newUser}">
            <c:url value="/signin" var="form_url_login" />
            <s:message code="label.login"/>
        </c:if>

        <div id="cadastro-dialog" title="Vamos completar seu cadastro...">
        
            <form:form id="cadastroForm" action="${signinUrl}" method="post" modelAttribute="cadastroForm">
            
                <fieldset>
                
                    <label>
                        <strong>
                            <s:message code="label.nome" />
                        </strong>
                        <form:input path="nome" id="nome" type="text" style="width: 100%;" />
                    </label>
                    
                    <label>
                        <strong>
                            <s:message code="label.sobreNome" />
                        </strong>
                        <form:input path="sobreNome" id="sobreNome" type="text" style="width: 100%;" />
                    </label>

                    <label>
                        <strong>
                            <s:message code="label.cidade" />
                        </strong>
                        <form:input path="cidade" id="cidade" type="text" style="width: 100%;" />
                    </label>
                    
                </fieldset>
            </form:form>
        </div>

    </tiles:putAttribute>
    
</tiles:insertDefinition>