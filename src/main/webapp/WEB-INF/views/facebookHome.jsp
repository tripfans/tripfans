<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

	<tiles:putAttribute name="title" value="TripFans" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
        <div id="fb-root"></div>
    
        <script src="//connect.facebook.net/pt_BR/all.js"></script>

        <form name="signin_form" id="signin_form" action="<c:url value="/signin/facebook"/>" method="POST">
            <input type="hidden" name="scope" value="email,user_hometown,public_profile,user_friends" />
        </form>    
        <script type="text/javascript">
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '${tripFansEnviroment.facebookClientId}', // App ID
                    channelUrl : '${tripFansEnviroment.serverUrl}/channel.html', // Channel File
                    status     : true, // check login status
                    cookie     : true, // enable cookies to allow the server to access the session
                    xfbml      : true  // parse XFBML
                });
                
                FB.getLoginStatus(checkLoginStatus);
                
                var requestIds = '${param.request_ids}';
                if (requestIds) {
                	requestIds = requestIds.split(',');
                }
                
                function checkLoginStatus(response) {
                    <%--
                        Se o usuario estiver logado no facebook e no aplicativo
                    --%>
                    if (response && response.status == 'connected') {
                    	if ('${usuario.id}' == '') {
                    		entrar();
                    	} else {
	                        var uid = response.authResponse.userID;
	                        var accessToken = response.authResponse.accessToken;
	                        
	                        if (requestIds) {
	                        	//aceitarConvite(requestIds);
	                        	
	                        } else {
	                            home();
	                        }
                    	}
                    }
                    <%--
                        Se o usuario estiver logado no facebook mas n�o estiver autorizado o app TripFans
                    --%>
                    /*else if (response.status === 'not_authorized') {
                        FB.login();
                    }*/ 
                    <%--
                        Se o usuario n�o estiver logado no facebook e nem no aplicativo
                    --%>
                    else {
                        FB.login(function(response) {
                                // handle the response
                                if (response.status === 'not_authorized') {
                                    
                                } else {
                                    if (requestIds) {
                                        //jsDump(response.authResponse, 3)
                                        var userId;
                                        if (response.authResponse) {
                                            userId = response.authResponse.userID;
                                        }
                                    	//aceitarConvite(requestIds, response.authResponse.userID, true);
                                	}
                                }
                            }, 
                            {	
                                scope: 'email,user_hometown,public_profile,user_friends',
                                access_type: 'offline'
                            }
                        );                        
                    }
                }
            }

        	function aceitarConvite(requestIds, userId, signIn) {
        	    $.post('<c:url value="/aceitarConviteTripFansViaFacebook" />', {'requestIds' : requestIds, 'userId' : userId}, function(response) {
        			for (var i = 0; i < requestIds.length; i++) {
                        FB.api(requestIds[i], 'delete', function(response) {
                            //console.log(response);
                        });
        			}
        			if (signIn && userId != null) {
        			    entrar();
        			} else {
        			    home();
        			}
        			/*$.ajax({
        			    url: 'https://graph.facebook.com/${param.request_ids}_' + uid + '?access_token=' + accessToken,
        			    type: 'DELETE',
        			    success: function(result) {
        			    	alert('1')
        			    	home();
        			    },
        			    fail: function(result) {
        			    	alert('2')
        			    	home();
        			    }
        			})always(function() { alert("complete"); });*/
        			
                });
        	}

        	function entrar() {
        	    /*$.ajax({
                  type: "POST",
                  url: '<c:url value="/connect/facebook" />',
                  data: {
                      'scope' : 'email,user_hometown,public_profile,user_friends' 
                  },
                  success: function(data) {
                      alert('1.2')
                      jsDump(data, 3)
                  }
                });*/
                document.forms['signin_form'].submit();
        	}

        	function home() {
        		top.location.href= '${tripFansEnviroment.serverUrl}/fb/home';
        	}
        	
        </script>
        
    	<div id="content">
    		
            
            <div class="row" style="margin-top: 50px; margin-right: 0px; margin-left: 0px;">
            
              <hr style="margin:0px; border-bottom: 0px; "/>
            
              <div class="col-md-3">&nbsp;</div>
            
                <c:forEach items="${convites}" var="convite">
                    
                    <div class="col-md-4">
                        <a href="${urlNotificacao}" class="link-notificacao ${notificacao.pendente ? 'pendente' : ''}" style="text-decoration: none;" data-notificacao-id="${notificacao.id}">
                            <div class="notificacao-inner">
                                <fan:userAvatar user="${notificacao.originador}" enableLink="false" displayName="false" displayDetails="${param.displayDetails}" marginLeft="10" imgSize="avatar"/>
                                <span style="padding-left: 0px; color: #4D4D4D;">
                                    <b>${notificacao.originador.displayName}</b>
                                    ${notificacao.tipoAcao.descricaoAcaoNoPassado}
                                    <%-- ${notificacao.tipoAcao.preposicaoPosAcao} --%>
                                    ${notificacao.tipoAcao.pronome}
                                    ${notificacao.tipoAcao.descricaoAlvo}
                                    
                                    <c:if test="${notificacao.tipoAcao.preposicaoPosAlvo != ''}">
                                      ${notificacao.tipoAcao.preposicaoPosAlvo}
                                      <c:if test="${notificacao.acaoUsuario != null}">
                                        ${notificacao.acaoUsuario.alvo.descricaoAlvo}
                                      </c:if>
                                    </c:if>
                                </span>
                                <p>
                                    <small>
                                        <fan:passedTime date="${notificacao.dataNotificacao}"/>
                                    </small>
                                </p>
                                <c:if test="${notificacao.pendente and notificacao.notificacaoConvite and notificacao.acaoPrincipal.autor.id != usuario.id and not notificacao.acaoPrincipal.aceito and not notificacao.acaoPrincipal.ignorado}">
                                    <div style="float: right; padding-right: 3px;">
                                        <fan:inviteButtons urlAccept="${urlAceita}" cssClassAccept="btn btn-small btn-primary" titleAccept="Aceitar" urlIgnore="${urlIgnora}" cssClassIgnore="btn btn-small" titleIgnore="Ignorar" />
                                    </div>
                                    <div style="clear: both;"></div>
                                </c:if>
                            </div>
                            <c:if test="${param.showSeparator}">
                                <hr style="margin:0px; border-bottom: 0px; "/>
                            </c:if>
                        </a>
                    </div>
                </c:forEach>
                
                <c:forEach items="${pedidosDicas}" var="pedido">
                    <div>
                        <a href="<c:url value="/viagem/${pedido.viagem.id}/ajudar/${pedido.id}/sugerirLocais"/>" class="link-notificacao" style="text-decoration: none;">
                            <div class="notificacao-inner">
                                <fan:userAvatar user="${pedido.autor}" enableLink="false" displayName="false"  marginLeft="10" imgSize="avatar"/>
                                <span style="padding-left: 0px; color: #4D4D4D;">
                                    <b>${pedido.autor.displayName}</b>
                                    
                                    convidou voc� para ajud�-lo(a) a montar um Roteiro de Viagem.
                                </span>
                                <p>
                                    <small>
                                        <fan:passedTime date="${pedido.dataCriacao}"/>
                                    </small>
                                </p>
                                
                                <div style="padding-right: 3px;">
                                    <a class="btn btn-success btn-lg" href="<c:url value="/viagem/${pedido.viagem.id}/ajudar/${pedido.id}/sugerirLocais"/>">
                                        Clique para ajudar agora
                                    </a>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <hr style="margin:0px; border-bottom: 0px; "/>
                        </a>
                    </div>
                </c:forEach>
                
                <div class="col-md-3">&nbsp;</div>
                
            </div>
            
    	</div>

    </tiles:putAttribute>
    
</tiles:insertDefinition>
 