<%@page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<c:set var="span" value="span16" />
<c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	<c:set var="span" value="span8" />
</c:if>

<c:if test="${promocaoTabletValida == true}">
<div class="row" style="width: 80%; margin-left: auto; margin-right: auto;">
		<div class="well centerAligned">
			<h2>${param.textoPromocao}</h2>
			<a class="lead" href="<c:url value="/promocaoTablet#regulamentoPromocao" />" target="_blank">Saiba mais</a>
		</div>
</div>
</c:if>

<div class="row">
<div class="horizontalSpacer"></div>
<div class="centerAligned">
	<div class="row">
		<div class="offset1 ${span} well">
			<h2>Muito obrigado por sua ${param.texto}!</h2>
			<p style="font-size: 16px;">
			O TripFans agradece sua colaboração. Continue contribuindo com a comunidade. Ajude outros viajantes a planejar suas viagens da melhor maneira.
			</p>
			<a  class="btn btn-large btn-secondary" href="<c:url  value= "/avaliacao/oQueAvaliar" />">Escreva avaliações</a>
			<a class="btn btn-large btn-secondary" href="<c:url value= "/dicas/sobreOQueEscreverDica" />">Deixe dicas</a>
		</div>
        
        <c:if test="${tripFansEnviroment.enableSocialNetworkActions}">
	
		  <div class="${span} well">
			<h2>Compartilhe sua ${param.texto}!</h2>
			<p style="font-size: 16px;">
			Compartilhe a ${param.texto} que você escreveu com seus amigos das redes sociais. Compartilhe suas experiências.	
			</p>
			
			<c:choose>
				<c:when test="${usuario.conectadoFacebook}">
                 	<c:set var="onclick" value="onclick=\"compartilharFacebook()\"" />
                </c:when>
                <c:otherwise>
                	<form name="fb_signin_connect" action="<c:url value="/connect/facebook"/>" method="POST" class="hidden">
                         <input type="hidden" name="scope" value="email,user_status,user_hometown,user_checkins,user_location,user_photos,publish_stream,friends_hometown,friends_location,friends_photos" />
                         <input type="hidden" name="redirectUrl" value="${param.redirectUrlPosFacebookConnection}" />
                         <input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlPosFacebookConnection}" />
                    </form>
                    <c:set var="onclick" value="onclick=\"document.forms['fb_signin_connect'].submit()\"" />
                </c:otherwise>
			</c:choose>
			<button id="botaoFacebook" class="btn btn-large btn-primary" ${onclick}>Compartilhar no Facebook</button>
			
			<c:choose>
				<c:when test="${usuario.conectadoTwitter }">
					<c:set var="onclick" value="onclick=\"compartilharTwitter()\"" />
				</c:when>
				<c:otherwise>
					<form name="form_twitter_connect" action="<c:url value="/connect/twitter"/>" method="POST" class="hidden">
                     		<input type="hidden" name="redirectUrl" value="${param.redirectUrlPosTwitterConnection}" />
                         	<input type="hidden" name="redirectUrlOnError" value="${param.redirectUrlPosTwitterConnection}" />
                     </form>
                     <c:set var="onclick" value="onclick=\"document.forms['form_twitter_connect'].submit()\"" />
				</c:otherwise>
			</c:choose>
			<button id="botaoTwitter" class="btn btn-large btn-primary" ${onclick}>Compartilhar no Twitter</button>
		  </div>
        </c:if>
	</div>
	<div class="horizontalSpacer"></div>
	<div class="row">
		<c:choose>
			<c:when test="${local != null}">
				<c:set var="urlPath" value="${local.urlPath}" />
				<c:set var="nomeLocal" value="${local.nome}" />
			</c:when>
			<c:otherwise>
				<c:set var="urlPath" value="${param.urlPathLocal}" />
				<c:set var="nomeLocal" value="${param.nomeLocal}" />
			</c:otherwise>
		</c:choose>
		Voltar para a página de <a href="<c:url value="/locais/${urlPath}" />">${nomeLocal}</a>
		</div>
	</div>
</div>

<script type="text/javascript">

	var postFacebook = '${param.postFacebook}';
	var postTwitter = '${param.postFacebook}';
	
	if(postFacebook !== '') {
		compartilharFacebook();
	}
	
	if(postTwitter !== '') {
		compartilharTwitter();
	}
	
<c:if test="${usuario.conectadoFacebook}" >
	function compartilharFacebook() {
		var url = '${param.postFacebookUrl}'+ '${param.objeto}';
		jQuery.get(
			url,
			function(data) {
				if(data.success == false) {
					console.log('deu erro ao postar');
				} else {
					$('#botaoFacebook').addClass('disabled');
					$('#botaoFacebook').attr('disabled', true);
				}
			}
		);
	}
</c:if>

<c:if test="${usuario.conectadoTwitter}" >
function compartilharTwitter() {
	var url = '${param.postTwitterUrl}'+ '${param.objeto}';
	jQuery.get(
		url,
		function(data) {
			if(data.success == false) {
				console.log('deu erro ao postar');
			} else {
				$('#botaoTwitter').addClass('disabled');
				$('#botaoTwitter').attr('disabled', true);
			}
		}
	);
}
</c:if>

</script>