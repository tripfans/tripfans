<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<a id="complemento-dialog-link" href="#complemento-dialog"></a>

<div id="complemento-dialog">

    <div class="modal-header">
        <h3>Bem-vindo!</h3>
    </div>

    <div class="modal-body">
    
        <div class="alert alert-success">
            <p>Voc� confirmou seu email <strong>${usuario.username}</strong> com sucesso!</p>
        </div>

        <div class="alert block-message success">
            <p> 
                <strong>Forne�a mais alguns dados para melhorar ainda mais sua experi�ncia no TripFans.</strong>
                <br/>
                Lembramos que se voc� fizer o cadastro completo, voc� ganhar� X pontos que poder�o
                ser trocados por viagens e outros pr�mios. 
                <br/>
                <a href="#" style="font-size: 10px;">Saiba mais</a>
                �
                <a href="#" style="font-size: 10px;">Ir para o cadastro completo</a>
            </p>
        </div>
            
        <c:url value="/completar" var="completar_url" />
    
        <form:form id="cadastroForm" action="${completar_url}" method="post" modelAttribute="cadastroForm" class="form-stacked">
        
            <fieldset>
            
                <div class="row">
                
                    <div class="span6">
                        <div class="control-group">
                            <label class="required">
                                <s:message code="label.usuario.perfil.primeiroNome" />
                            </label>
                            <div class="controls">
                                <form:input path="nome" cssClass="span5" />
                            </div>
                        </div>
                    </div>

                    <div class="span6">
                        <div class="control-group">
                            <label class="required">
                                <s:message code="label.usuario.perfil.ultimoNome" />
                            </label>
                            <div class="controls">
                                <form:input path="sobreNome" cssClass="span5" />
                            </div>
                        </div>
                    </div>           
                
                </div>           
                
                <div class="control-group">
                    <label>
                        <s:message code="label.usuario.perfil.genero" />
                    </label>
                    <div class="controls">
                        <div class="inline-inputs">
                            <form:select id="genero" path="genero" cssClass="span3">
                                <form:option value="M">
                                    <s:message code="label.usuario.perfil.genero.masculino" />
                                </form:option>
                                <form:option value="F">
                                    <s:message code="label.usuario.perfil.genero.feminino" />
                                </form:option>
                            </form:select>
                            &nbsp;&nbsp; 
                            <form:checkbox id="exibirGenero" path="exibirGenero" />
                            <span><s:message code="label.usuario.perfil.exibirGenero" /></span>
                        </div>
                    </div>
                </div>
                
                <div class="control-group">
                    <label>
                        <s:message code="label.usuario.perfil.cidadeAtual" />
                    </label>
                    <div class="controls">
                        <input type="text" name="cidade" id="cidadeAtual" class="span8" />
                    </div>
                </div>
                
                <form:hidden path="id" />
                
            </fieldset>
        </form:form>
        
    </div>
    
    <div class="modal-footer">
        <a class="btn small success" href="#" onclick="$('#cadastroForm').submit(); $.fancybox.close();">Continuar</a>
        <a class="btn small info" href="#" onclick="$.fancybox.close();">Mais tarde...</a>
    </div>
    
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#complemento-dialog-link").fancybox({
            'scrolling'     : 'no',
            'titleShow'     : false,
            'width'         : 300,
            'height'        : 500,
            'hideOnOverlayClick' : false, 
            'onClosed'      : function() {
                
            }
        });
        
        $.fancybox.init();
        
        $("#complemento-dialog-link").trigger('click');
        
        $( "#cidadeAtual" ).autocompleteFanaticos({
            source: '<c:url value="/locais/cidade/consultarCidades"/>',
            hiddenName : 'cidade',
            valueField: 'id',
            labelField: 'nome',
            minLength: 2
        });        
        
        $("#cadastroForm").validate({ 
            rules: {
            	primeiroNome: { 
                    required: true, 
                    maxlength : 30
                }, 
                ultimoNome: { 
                    required: true,
                    maxlength : 30
                }
            }, 
            messages: { 
            	primeiroNome: {
                    required : "<s:message code="validation.usuario.nome.required" />",
                },
                ultimoNome: {
                    required : "<s:message code="validation.usuario.ultimonome.required" />",
                }
            }
        });
        
        $("#cadastroForm").unbind("submit");
        
        $("#cadastroForm").submit(function(event) {
            
            $("#errors").hide();
            $("#success").hide();
            
            /* stop form from submitting normally */
            event.preventDefault();
            
            var formData = $('#cadastroForm').serialize();
            
            if ($("#cadastroForm").valid()) {
            
                $.ajax({
                    type: 'POST',
                    url: document.forms['cadastroForm'].action,
                    data: formData,
                    success: function(data) {
                        if (data.error != null) {
                            $("#errorMsg").empty().append(data.error);
                            $("#errors").show();
                        } else {
                            $("#successMsg").empty().append(data.success);
                            $("#success").fadeIn(1000);
                        }
                    }
                });
            }
            
        });        
        
    });
</script>
