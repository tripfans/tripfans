<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<a id="confirmacao-dialog-link" href="#confirmacao-dialog"></a>

<div id="confirmacao-dialog" title="<s:message code="label.confirmarUsuario.titulo" />"">

    <div class="modal-header">
        <h3>Bem-vindo!</h3>
    </div>
    
    <div class="modal-body">

        <div class="alert alert-success">
            <p><strong>Ol� ${usuario.username}!</strong> Seu cadastro foi realizado com sucesso!</p>
        </div>
            
        <div class="alert block-message success">
            <p>
                <s:message code="label.confirmarUsuario.parte2" />
                <br/>
                <s:message code="label.confirmarUsuario.parte3" />
                <br/>
                <s:message code="label.confirmarUsuario.parte4" />
            </p>
            <br/>
        </div>    

        <div class="modal-footer">
            <a class="btn small info" href="#" onclick="$.fancybox.close();">Conectar com Facebook</a>
            <a class="btn small success" href="#" onclick="$.fancybox.close();">Continuar</a>
        </div>
    
    </div>
    
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#confirmacao-dialog-link").fancybox({
            'scrolling'     : 'no',
            'titleShow'     : false,
            'width'         : 500,
            'height'        : 500,
            'hideOnOverlayClick' : false, 
            'onClosed'      : function() {
            }
        });
        
        $.fancybox.init();
        
        $("#confirmacao-dialog-link").trigger('click');
    	
    });
</script>