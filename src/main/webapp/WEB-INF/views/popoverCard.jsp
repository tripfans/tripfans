<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
}

li {
    float : left;
}
</style>

<div class='row'>
    
    <div class='span1'>
        <img src='<c:url value="/resources/images/no_photo.png" />' width='50' height='50' />
    </div>
    
    <div class='span5'>
        ${popoverCard.titulo}
        ${popoverCard.subTitulo}
        ${popoverCard.quantidadeTotal}
        ${popoverCard.quantidadeAmigos}
        <ul>
            <li>
                <a>
                    <img src='http://graph.facebook.com/100001996383236/picture' width='35' height='35' />
                </a>            
            </li>
            
            <li>
                <a>
                    <img src='http://graph.facebook.com/100000726931592/picture' width='35' height='35' />
                </a>            
            </li>
        </ul>
    </div>
</div>