<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>


<div id="listaFotos4sq">
  <c:if test="${not empty fotosFoursquare}">
    <c:forEach items="${fotosFoursquare}" var="photo">
        <c:forEach items="${photo.sizes.items}" var="size">
            <c:if test="${size.width == 300}">
                <img class="borda-arredondada sombra span4 album" src="${size.url}" style="margin-bottom: 20px;" title="">
            </c:if>
        </c:forEach>
    </c:forEach>
  </c:if>
    
  <c:if test="${empty fotosFoursquare}">
    <div class="blank-state">
    
        <div class="span2 offset1">
            <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
        </div>    

        <div class="span6" style="margin-top: 20px;">
            <h3>Não há fotos para exibir</h3>
            <p>
                Não foram encontradas fotos no <img src="<c:url value="/resources/images/logos/foursquare.png"/>" width="75" style="margin-top: -3px;" /> para este local.
            </p>
        </div>
    </div>    
  </c:if>
    
</div>