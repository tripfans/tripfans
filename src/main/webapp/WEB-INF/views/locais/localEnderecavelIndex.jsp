<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% pageContext.setAttribute("newLineChar", "\r\n"); %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">
	<tiles:putAttribute name="title" value="${local.nome} - TripFans" />
	<tiles:putAttribute name="keywords" value="${local.nome}, ${local.pai.nome}, ${local.tipoLocal.descricao}, dicas, avaliações, viagens" />

	<tiles:putAttribute name="stylesheets">
		<meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
		<meta property="og:url" content="http://www.tripfans.com.br/locais/${local.urlPath}" />
		<meta property="og:description" content="${(not empty local.descricao and local.descricao != '') ? local.descricao : local.nomeCompleto}" />
		
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/services-plugin/css/settings.css" />" media="screen" />
		
		<style type="text/css">
			.nav-tabs > li > a {
				padding-right: 5px;
				padding-left: 5px;
			}
			
			#alertFooter {
				width: 100%;
				height: 160px;
				position: fixed;
				bottom: 0;
				left: 0;
				z-index: 99999;
				border-color: #f0c36d;
				background-color: #f9edbe;
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
		
	<tiles:putAttribute name="footer">
	
		<c:choose>
		<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
		</c:when>
		<c:otherwise>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		</c:otherwise>
		</c:choose>
	
		<script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.touchwipe.min.js" />"></script> 
        <script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.mousewheel.min.js" />"></script>
		<script type="text/javascript" src="<c:url value="/resources/components/services-plugin/js/jquery.themepunch.services.js" />"></script>
	
		<script type="text/javascript">
		
		    <c:set var="tab" value="${pagina != null ? pagina : param.tab}" />
		    
			jQuery(document).ready(function() {
				$('a.menu-item').bind('click',function(event) {
					event.preventDefault();
					var id = $(this).attr('id');
					var params = $(this).attr('data-params');
					var url = $(this).data('href');
                    if (params != null && params != '') {
                        url += ((url.indexOf('?') != -1) ? '&' : '?') + params;
                        $(this).attr('data-params', '');
                    }
					$('.menu-item').parent().removeClass('active'); // Remove active class from all links
                    $(this).parent().addClass('active'); //Set clicked link class to active
                    $('body').scrollTop(0);
				    $.get(url, {}, function(response) {
				    	$('#pageContent').html(response);
				 	  	$('#pageContent').initToolTips();
				 	  	//window.history.pushState('<c:url value="/locais/${local.urlPath}/"/>' + id, window.title, '<c:url value="/locais/${local.urlPath}/"/>' + id);
				 	  	History.pushState('<c:url value="/locais/${local.urlPath}/"/>' + id, window.title, '<c:url value="/locais/${local.urlPath}/"/>' + id);
				 	  	$.lockfixed('.sidebar-nav', { offset: { top: 70, bottom: 386 } });
				    });
				});
				
                var tab = '${tab}';
                if (tab !== '' ) {
                    var $tabAtual = $('#' + tab);
                    if ($tabAtual.attr('id') != null) {
                        var destaque = '${param.item}';
                        if (destaque != null) {
                            $('#' + tab).attr('data-params', 'item=' + destaque);
                        }
                        $('#' + tab).click();
                    } else {
                        $('#informacoes').click();
                    }                        
                }
				
				/*var height = $('#footer-content').offset().top - 300;
				$('#ulSidenav').css({'height': height});*/
				
				//$('.sidebar-nav').stickyMojo({footerID: '#footer-content', contentID: '#pageContent', topSpacing: '70px'});
                $.lockfixed('.sidebar-nav', { offset: { top: 70, bottom: 386 } });
				
			});
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div itemscope itemtype="${local.seoItemScopeType}">
			<fan:breadCrumb generator="${local}" />
			<div class="container-fluid page-container">
				<div id="tituloLocal">
					<div class="title module-box-header">
    					<span class="title"><span itemprop="name">${local.nome}</span></span>
    					<div class="pull-right buttonToolBar btn-toolbar">
    						<div class="btn-group">
    							<fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" />
    							<fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>"  />
    							<fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>"  />
    							<fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${local}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>"  />
    						</div>
    						
    						<div class="btn-group">
    							<a href="<c:url value="/avaliacao/avaliar/${local.urlPath}" />" id="btnAvalia" class="btn btn-small" title="Escreva uma avaliação sobre ${local.nome}">
                                    <img src="<c:url value="/resources/images/icons/award_star_gold_3.png" />" />
                                    <span>Avaliar</span>
                                </a>
    							<a href="<c:url value="/dicas/escrever/${local.urlPath}" />" id="btnDica" class="btn btn-small" title="Escreva uma dica sobre ${local.nome}">
                                    <img src="<c:url value="/resources/images/icons/lightbulb.png" />" />
                                    <span>Escrever Dicas</span>
                                </a>
    							<a href="<c:url value="/perguntas/perguntar?local=${local.urlPath}" />" id="btnPergunta" class="btn btn-small" title="Faça uma pergunta sobre ${local.nome}">
                                    <img src="<c:url value="/resources/images/icons/help.png" />" />
                                    <span>Fazer Perguntas</span>
                                </a>
    						</div>
    					</div>
					</div>
				</div>			
			   <div class="row">
					<div class="span3 menu-lateral" id="menuLocal">
						<%@include file="localEnderecavelMenu.jsp" %>
					</div>
					<div id="pageContent" class="sidebar_content" style="padding-right: 6px;">
                      <c:if test="${empty tab}">
						<%@include file="localEnderecavelInformacoes.jsp" %>
                      </c:if>
					</div>
			   </div>
			</div>
		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>