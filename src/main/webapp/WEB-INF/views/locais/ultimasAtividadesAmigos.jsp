<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

  <c:set var="cssSpan" value="${not empty param.cssSpan ? param.cssSpan : 'span16'}"/>
  <c:if test="${not empty atividades}">
    <c:forEach var="atividade" items="${atividades}" >
        <c:set var="atividade" value="${atividade}" scope="request" />
        <div>
            <div class="${cssSpan}" style="margin-left: 0px;">
                <fan:atividade acaoUsuario="${atividade}" mostrarFotoAutor="true" mostrarIcone="true" mostrarData="true" />
            </div>
        </div>
        <hr class="${cssSpan}" style="margin:2px;" />
    </c:forEach>
  </c:if>