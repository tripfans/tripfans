<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">
    <tiles:putAttribute name="title" value="TripFans" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
        
    <tiles:putAttribute name="footer">
        <script type="text/javascript">
        $(document).ready(function() {
            $('#btn-continuar').click(function (e) {
                e.preventDefault();
                
                var destino = $('input[name="localPai"]').val();
                var tipoLocal = $('input[name="tipoLocal"]').val();
                var nomeLocal = $('input[name="nome"]').val();
                
                if ($('#novoLocalForm').valid()) {
                    var url = '<c:url value="/locais/sugerirLocal/consultarLocaisPossiveis"/>/' + destino + '/' + tipoLocal + '/' + nomeLocal;
                    if ('${acao}' != null) {
                        url += '?acao=${acao}'
                    }
                    $('#div-locais-possiveis').load(url,  
                        function(response) {
                            $('#div-locais-possiveis').html(response);
                            desabilitarCampos();
                            $('#div-acoes-passo1').hide();
                            $('#div-acoes-corrigir').show();
                        }
                    );
                }
            });
            
            $('#cb-concordo').click(function (e) {
                $('#btn-conluir').attr('disabled', !$(this).is(':checked'));
            });
            
            $('#btn-corrigir').click(function (e) {
                habilitarCampos();
                $('#div-acoes-corrigir').hide();
                $('#div-info-complementar').hide();
                $('#div-acoes-passo1').show();
                $('input[name="nome"]').focus();
            });
            
            $('#novoLocalForm').validate({
                rules: {
                    localPai: { 
                        required: true,
                    }, 
                    nome: {
                        required: true,
                    }, 
                }, 
                messages: { 
                	localPai: {
                        required : 'Informe o destino para o qual você sugerir um local'
                    },
                    nome: {
                        required : 'Informe o nome do local sugerido" />'
                    }
                } 
            });
            
            verificarCamposPreenchidos();
            
            $('#novoLocalForm :input').change(function() {
                verificarCamposPreenchidos();
            });
        })
        
        function verificarCamposPreenchidos() {
            var cidade = $('input[name="localPai"]').val();
            var tipoLocal = $('input[name="tipoLocal"]').val();
            var nomeLocal = $('input[name="nome"]').val();
            
            if (cidade != null && cidade != '') {
                if (tipoLocal != null && tipoLocal != '') {
                    if (nomeLocal != null && nomeLocal != '') {
                        if (!$('#form-item-nome').is(':visible')) {
                            $('#form-item-nome').show();
                            $('input[name="nome"]').focus();
                        }
                        $('#btn-continuar').enable();
                    } else {
                        $('#btn-continuar').attr('disabled', 'disabled');
                        $('#form-item-nome').show();
                        $('input[name="nome"]').focus();
                    }
                } else {
                    $('#form-item-nome').hide();
                    $('#btn-continuar').attr('disabled', 'disabled');
                    $('input[name="tipoLocal"]').focus();
                }
            } else {
                $('#form-item-nome').hide();
                $('#btn-continuar').attr('disabled', 'disabled');
                $('#caixaPesquisaCidade').focus();
            }
        }

        function habilitarCampos() {
            $('#addOnRemove_caixaPesquisaCidade').show();
            $('#caixaPesquisaCidade').enable();
            $('.radioBtn').each(function() {
                $(this).enable();
                if (!$(this).hasClass('active')) {
                    $(this).show();
                }
            });
            $('input[name="nome"]').enable();
            $('input[name="nome"]').removeAttr('readonly');
        }
        
        function desabilitarCampos() {
            $('#addOnRemove_caixaPesquisaCidade').hide();
            $('.radioBtn').each(function() {
                $(this).attr('disabled', 'disabled');
                if (!$(this).hasClass('active')) {
                    $(this).hide();
                }
            });
            $('input[name="nome"]').attr('readonly', 'true');
        }
        
        $('#nomeLocal').keyup(function() {
            verificarCamposPreenchidos();
        });
        </script>
    </tiles:putAttribute>

    <tiles:putAttribute name="body">
    
        <div class="container-fluid page-container" style="min-height: 516px;">
        
          <form id="novoLocalForm" action="<c:url value="/locais/sugerirLocal/incluir"/>" method="post">
            
            <div class="title module-box-header">
                <span class="title">Sugerir um novo Local</span>
            </div>
            
            <div class="span15" style="margin-top: 15px;">
            
              <div class="alert alert-info alert-block" style="text-align: justify; font-size: 13pt;">
                <button type="button" class="close" data-dismiss="alert">x</button>
                
                <ol>
                  <li> 
                  Ao cadastrar um novo local, recomendamos que você faça uma pesquisa na Internet, para obter algumas informações sobre o local, 
                  como por exemplo: nome completo e endereço.<br/> 
                  Quanto mais informações forem fornecidas, mais fácil e rápido será para a nossa equipe validar o local e disponibilizá-lo para toda a comunidade. 
                </li>
                	<li>
                  
                  Preencha as informações obrigatórias abaixo e clique em 'Continuar'. 
                  Em seguida, poderá ser exibida uma lista de locais com nomes iguais ou semelhantes ao que foi informado. 
                  Caso seu local esteja nesta lista de sugestão, favor selecioná-lo.
                </li>
                </ol>
              </div>            
            
              <fieldset>
                <legend>
                    Informações obrigatórias sobre o Local
                </legend>
                
                <div>
                    <input type="hidden" name="acao" value="${acao}" />
                    <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
                        <li>
                            <label class="span4 fancy-text">
                                <span class="required">
                                    Cidade
                                </span>
                            </label>
                            <fan:autoComplete url="/pesquisaTextual/consultarCidades" hiddenName="localPai" id="caixaPesquisaCidade"
                                              hiddenValue="${cidade.id}" value="${cidade.nome}" onSelect="verificarCamposPreenchidos()"
                                              labelField="nomeExibicao" minLength="3" valueField="id" group="false"
                                              blockOnSelect="true" showActions="true" addOnLarge="true"
                                              position="collision: 'flip'" placeholder="Escolha a cidade ao qual o local pertence" showNotFound="false" cssClass="span6"
                                              limit="15" />
                        </li>
                        <li>
                            <label class="span4 fancy-text">
                                <span class="required">
                                    Tipo do Local
                                </span>
                            </label>
                            <c:choose>
                              <c:when test="${not empty tipoLocal}">
                                <c:set var="tipoLocalSelecionado" value="${tipoLocal.urlPath}"></c:set>
                              </c:when>
                              <c:otherwise>
                                <%--c:set var="tipoLocalSelecionado" value="<%=LocalType.ATRACAO.getUrlPath()%>"></c:set--%>
                              </c:otherwise>
                            </c:choose>
                            <fan:groupRadioButtons size="normal" name="tipoLocal" id="tipoLocal" selectedValue="${tipoLocalSelecionado}">
                                <fan:radioButton value="<%=LocalType.ATRACAO.getUrlPath()%>" label="Atração"></fan:radioButton>
                                <fan:radioButton value="<%=LocalType.HOTEL.getUrlPath()%>" label="Hotel"></fan:radioButton>
                                <fan:radioButton value="<%=LocalType.RESTAURANTE.getUrlPath()%>" label="Restaurante"></fan:radioButton>
                            </fan:groupRadioButtons>
                        </li>
                        <li id="form-item-nome" class="${empty cidade.id and empty tipoLocal ? 'hide' : ''}">
                            <label class="span4 fancy-text">
                                <span class="required">
                                    Nome completo do Local
                                    <!-- i id="help-nome" class="icon-question-sign helper" title=""></i-->
                                </span>
                            </label>
                            <input id="nomeLocal" name="nome" value="${nomeLocal}" class="span6" maxlength="100" />
                        </li>
                    </ul>
                    
                    <div id="div-acoes-corrigir" align="center" class="hide">
                        <input type="button" id="btn-corrigir" class="btn btn-warning" value="Corrigir informações"/>
                    </div>
                    
                </div>
                
              </fieldset>
              
              <div id="div-acoes-passo1" style="width: 100%">
                <div align="right">
                    <div class="form-actions">
                        <input type="button" id="btn-continuar" class="btn btn-primary" disabled="disabled" value="Continuar"/>
                    </div>
                </div>
              </div>
              
              <div id="div-locais-possiveis">
              </div>         
              
              <div id="div-info-complementar" class="hide">
                  <fieldset>
                    <legend>
                        Informações complementares sobre o Local
                    </legend>
                    
                    <div>
                        <ul class="options-list label-right" style="min-height: 34px; margin: 5px;">
                            <li>
                                <label class="span4 fancy-text">
                                    <span>
                                        Endereço
                                    </span>
                                </label>
                                <input id="endereco" name="endereco" class="span6" maxlength="255" />
                            </li>
                            <li>
                                <label class="span4 fancy-text">
                                    <span>
                                        Bairro
                                    </span>
                                </label>
                                <input id="bairro" name="bairro" class="span6" maxlength="100" />
                            </li>
                            <li>
                                <label class="span4 fancy-text">
                                    <span>
                                        Ponto de referência/outras informações
                                    </span>
                                </label>
                                <textarea name="informacoesAdicionais" class="span6" rows="5"></textarea>
                            </li>
                        </ul>
                    </div>
                  </fieldset>
                  
                  <div class="alert alert-warning alert-block" style="text-align: justify; font-size: 12pt;">
                    <strong>Política de Validação</strong>
                    <br/><br/>
                      As informações fornecidas neste formulário, serão avaliadas e validadas pela equipe TripFans.
                      <ul>
                        <li>
                          Caso o local <strong>NÃO</strong> esteja em conformidade com a política do TripFans, este será recusado.
                          </li>
                        <li>
                          Se detectarmos que o local informado já esteja em nossa base, suas avaliações e/ou dicas serão vinculadas à este local e o seu cadastro será descartado.
                        </li>
                      </ul>
                    
                    
                      <strong>Enquanto o local não for validado, ele não será exibido no TripFans.</strong> 
                    
                  </div>
                  
                  <div id="div-acoes-passo2" style="width: 100%">
                    <div align="right">
                        <div class="form-actions">
                            <input type="checkbox" id="cb-concordo" name="concordo"/>
                            <span style="margin-right: 8px;">Concordo com os termos de uso acima</span>
                            <input type="submit" id="btn-conluir" class="btn btn-primary" value="Concluir" disabled="disabled" />
                        </div>
                    </div>
                  </div>
              </div>
              
            </div>
            
            <div class="span4" style="margin-left: 0px;">
              <div class="horizontalSpacer"></div>
              <fan:adsBlock adType="0" />
            </div>
            
          </form>
        </div>
    </tiles:putAttribute>

</tiles:insertDefinition>        