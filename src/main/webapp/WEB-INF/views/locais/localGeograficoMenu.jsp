<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
	<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
      		<c:set var="reqScheme" value="https" />
	</c:when>
	<c:otherwise>
		<c:set var="reqScheme" value="http" />
	</c:otherwise>
</c:choose>

<c:if test="${not unsuportedIEVersion}">
  <script src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR" type="text/javascript"></script>
</c:if>

<%@include file="localMenu.jsp" %>