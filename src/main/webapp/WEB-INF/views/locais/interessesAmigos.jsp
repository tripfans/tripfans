<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span13" style="margin-top: -15px;">

	<div class="span13" style="margin-left: 0px;">
		<c:choose>
			<c:when test="${not empty interessesAmigos}">
			
				<c:forEach items="${interessesAmigos}" var="interesseAmigo">
                
                    <c:if test="${interesseAmigo.tipo.interesseJaFoi}">
                        <c:set var="badge" value="success"/>
                    </c:if>
                    
                    <c:if test="${interesseAmigo.tipo.interesseDesejaIr}">
                        <c:set var="badge" value="info"/>
                    </c:if>
                    
                    <c:if test="${interesseAmigo.tipo.interesseFavorito}">
                        <c:set var="badge" value="important"/>
                    </c:if>                

                    <c:if test="${interesseAmigo.tipo.interesseIndica}">
                        <c:set var="badge" value="warning"/>
                    </c:if>                
                    
					<div class="page-header">
						<h3>
                          Amigos que ${interesseAmigo.titulo}
                          <span class="badge badge-${badge}">${interesseAmigo.quantidadeAmigos}</span>
                        </h3> 
					</div>
					<div class="span13">
						<c:forEach items="${interesseAmigo.amostraInteresses}" var="interesse">
							<c:choose>
						        <c:when test="${interesse.usuario != null}">
						            <div><fan:userAvatar user="${interesse.usuario}" imgStyle="width: 50px; height: 50px;" displayName="false" displayDetails="true" displayNameTooltip="true"/></div>
						        </c:when>
							    <c:otherwise>
						            <div>
					            		<fan:userAvatar displayName="false" imgStyle="width: 50px; height: 50px;" showFacebookMark="true" idUsuarioFacebook="${interesse.idUsuarioFacebook}" displayNameTooltip="true" nomeUsuario="${interesse.nomeUsuario}" />
						            </div>
						        </c:otherwise>
						    </c:choose>
						</c:forEach>
					</div>
				</c:forEach>
			
			</c:when>
			<c:otherwise>

                <div class="blank-state">
                    <div class="span2 offset1">
                        <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
                    </div>    
                    <div class="span6" style="margin-top: 20px;">
                        <h3>Seus amigos ainda não registraram nenhuma atividade em ${local.nome}</h3>
                    </div>
                </div>    

                <div class="horizontalSpacer">
                </div>

			</c:otherwise>
		</c:choose>
	</div>
	
</div>