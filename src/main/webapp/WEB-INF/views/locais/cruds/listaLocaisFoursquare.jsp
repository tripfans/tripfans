<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">

    $(document).ready(function() {
    	
    });

</script>

<div id="dados4sq-${local.id}">
<c:choose>
    <c:when test="${not empty venues}">
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="2%">#</th>
                    <th width="98%">Local no 4SQ</th>
                </tr>
            </thead>
            <c:forEach items="${venues}" var="venue" varStatus="count">
                <tr>
                    <td width="2%" style="text-align: center;">
                      <p>
                        ${count.count + (limit * (paginaAtual - 1))}
                      </p>
                      <p>
                        <a href="#" class="btn btn-success botao-associar-4sq" data-local-id="${local.id}" data-foursquare-id="${venue.id}">
                            <i class="icon-chevron-left icon-white"></i> Associar ID do 4SQ
                        </a>
                      </p>
                      <p>
                        <%--a id="botao-associar-copiar-4sq" href="#" class="btn btn-primary" >
                            <i class="icon-chevron-left icon-white"></i> Associar e copiar dados
                        </a--%>
                      </p>
                    </td>
                    <td width="98%">
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Nome: <strong>${venue.name}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${venue.name != local.nome}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Nome'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Endere�o: <strong>${venue.location.address}</strong> 
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${not empty venue.location.address and venue.location.address != local.endereco.descricao}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Endere�o'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                CEP: <strong>${venue.location.postalCode}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${not empty venue.location.postalCode and venue.location.postalCode != local.endereco.cep}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'CEP'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div>
                          <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Latitude: <strong>${venue.location.latitude}</strong>
                                Longitude: <strong>${venue.location.longitude}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${venue.location.latitude != local.latitude or venue.location.longitude != local.longitude}">
                                <a href="#" class="btn btn-primary btn-large btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Lat/Lng'
                                </a>
                              </c:if--%>
                            </div>
                          </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Categoria(s): 
                                <strong>
                                  <c:forEach items="${venue.categories}" var="category">
                                    ${category.name}, 
                                  </c:forEach>
                                </strong>
                            </div>
                            <div style="width: 50%; float: left;">
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Qtd. checkins: <strong>${venue.stats.usersCount}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                            </div>
                        </div>
                    </td>
                </tr>
            </c:forEach>
        </table>    
    </c:when>
    <c:otherwise>
        <div class="horizontalSpacer"></div>
        <div class="centerAligned"><h4>N�o foram encontrados locais semelhantes no Foursquare.</h4></div>
    </c:otherwise>
</c:choose>

</div>