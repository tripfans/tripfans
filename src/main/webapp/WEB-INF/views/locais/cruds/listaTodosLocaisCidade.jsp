<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

        <script type="text/javascript">
        
        $(document).ready(function() {
        	
            $(".paginacao").click(function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                jQuery.get(
                        url,
                        function(response) {
                            $('#listaPaginada').html(response);
                        }
                    );
            });
            
            $(document).on('click', '.botao-carregar-4sq', function (event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                $('#coluna-dados-externos-4sq-' + idLocal).load('<c:url value="/locais/cruds/listarLocaisFoursquare" />/' + idLocal);
                //$(this).hide();
                $('.btn-comparar-mapas').show();
            });

            $(document).on('click', '.botao-carregar-google', function (event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                $('#coluna-dados-externos-google-' + idLocal).load('<c:url value="/locais/cruds/listarLocaisGoogle" />/' + idLocal);
                //$(this).hide();
                $('.btn-comparar-mapas').show();
            });
            
            $('.btn-recarregar').click(function(event) {
                event.preventDefault();
                var tabPane = $('#tabs-cidade li.active a').attr('href').substring(1);
		        $(document).off('click', '**');
		        $('#' + tabPane).load('<c:url value="/locais/cruds/listar" />' + tabPane + 'Cidade/' + ${cidade.id});
            });
            
            $('.btn-carregar-proximos').click(function(event) {
                event.preventDefault();
                var tabPane = $('#tabs-cidade li.active a').attr('href').substring(1);
                
                var url = '<c:url value="/locais/cruds/listar" />' + tabPane + 'Cidade/' + ${cidade.id};
                var start = $(this).attr('data-start');
                var limit = $(this).attr('data-limit');
                
                $(this).attr('data-start', (50 + parseInt(start)));
                
                $.get(url, {start: start, limit: limit}, function(response) {
                    $('#div-locais').append(response);
                });
            });
            
            $('.botao-carregar-todos-4sq').click(function(event) {
                event.preventDefault();
                $('.tabela-locais').find('.coluna-dados-externos-4sq').each(function(index, el) {
                    $(this).load('<c:url value="/locais/cruds/listarLocaisFoursquare" />/' + $(this).data('id-local'));
                });
                $(this).hide();
                $('.btn-comparar-mapas').show();
            });

            $('.botao-carregar-todos-google').click(function(event) {
                event.preventDefault();
                $('.tabela-locais').find('.coluna-dados-externos-google').each(function(index, el) {
                    $(this).load('<c:url value="/locais/cruds/listarLocaisGoogle" />/' + $(this).data('id-local'));
                });
                $(this).hide();
                $('.btn-comparar-mapas').show();
            });
            
            $(document).on('click', '.visitar', function(event) {
                var $input = $('#' + $(this).data('input'));
                if ($input.val() != '') {
                	$(this).attr('href', $input.val());
                } else {
                    event.preventDefault();
                }
            })
            
            $(document).on('click', '.botao-associar-4sq', function(event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                var idFoursquare = $(this).data('foursquare-id');
                var $botao = $(this);
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/locais/cruds/associarComFoursquare/" />' + idLocal + '/' + idFoursquare,
                    success: function(data) {
                        $botao.hide();
                        $('#dados4sq-' + idLocal).load('<c:url value="/locais/cruds/dadosLocalFoursquare/" />' + idLocal);
                    }
                });
            });

            $(document).on('click', '.botao-associar-google', function(event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                var idGoogle = $(this).data('google-id');
                var googleReference = $(this).data('google-reference');
                var $botao = $(this);
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/locais/cruds/associarComGoogle/" />' + idLocal + '/' + idGoogle + '/' + googleReference,
                    success: function(data) {
                        $botao.hide();
                        $('#dadosGoogle-' + idLocal).load('<c:url value="/locais/cruds/dadosLocalGoogle/" />' + idLocal);
                    }
                });
            });
            
            $(document).on('click', '.btn-mostrar-foto', function(event) {
                event.preventDefault();
                mostrarFotosProximas(this);
                $(this).text('Recarregar Foto(s)');
                var idLocal = $(this).data('local-id');
                $('#secao-usar-foto-' + idLocal).show();
            });
            
            $('.btn-salvar').click(function(event) {
            	event.preventDefault();
                var idLocal = $(this).data('local-id');
                var $botao = $(this);
                
                $('#mostrarOutrasFotosPanoramio-' + idLocal).val($('input[name="radioMostrarOutrasFotos-' + idLocal + '"]:checked').val());
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/locais/cruds/alterarLocal" />',
                    data: $('#form-local-' + idLocal).serialize(),
                    success: function(data) {
                        if (data.success && data.params) {
                            var $botao = $('#btn-mostrar-foto-' + idLocal);
                            $botao.attr('data-latsw', data.params.latSW);
                            $botao.attr('data-lngsw', data.params.lngSW);
                            $botao.attr('data-latne', data.params.latNE);
                            $botao.attr('data-lngne', data.params.lngNE);
                        }
                    }
                });
                $botao.removeClass('btn-success');
            });
            
            $(document).on('click', '.btn-copiar', function (event) {
                event.preventDefault();
                var $botao = $(this);
                var nomeCampoDestino = $botao.data('input-destino');
                var nomeCampoOrigem = $botao.data('input-origem');
                var siteOrigem = $botao.data('site-origem');
                if (nomeCampoOrigem == null) {
                    nomeCampoOrigem = siteOrigem + '-' + nomeCampoDestino;
                }
                var replace = $botao.data('replace');
                var novoValor = $('#' + nomeCampoOrigem).text();
                if (replace != null && replace == false) {
                    var valorAnterior = $('#input-' + nomeCampoDestino).val();
                    $('#input-' + nomeCampoDestino).val((valorAnterior != '' ? (valorAnterior + ',') : '') + novoValor);
                } else {
                    $('#input-' + nomeCampoDestino).val(novoValor);
                }
                $('#input-' + nomeCampoDestino).trigger('change')
                $botao.hide();
            });
            
            $(document).on('click', '.btn-copiar-lat-lon', function (event) {
                event.preventDefault();
                var $botao = $(this);
                var idLocal = $botao.data('local-id');
                var origem = $botao.data('site-origem');
                $('#input-latitude-' + idLocal).val($('#' + origem + '-lat-' + idLocal).text());
                $('#input-longitude-' + idLocal).val($('#' + origem + '-lon-' + idLocal).text());
                $('#input-latitude-' + idLocal).trigger('change')
                $botao.hide();
            });
            
            $(document).on('click', '.btn-comparar-mapas', function(event) {
            	event.preventDefault();
            	var $botao = $(this);
            	var idLocal = $botao.data('local-id');
            	var $painelMapaTF = $('#mapa-tf-' + idLocal);
            	
            	var latitudeTF = $painelMapaTF.data('latitude');
            	var longitudeTF = $painelMapaTF.data('longitude');
            	
            	var $painelMapa4SQ = $('#mapa-4sq-' + idLocal);
                var latitude4SQ = $painelMapa4SQ.data('latitude');
                var longitude4SQ = $painelMapa4SQ.data('longitude');

            	var $painelMapaGoogle = $('#mapa-google-' + idLocal);
                var latitudeGoogle = $painelMapaGoogle.data('latitude');
                var longitudeGoogle = $painelMapaGoogle.data('longitude');
                
            	mostraMapaLocal(null, 'mapa-tf-' + idLocal, latitudeTF, longitudeTF, '${pageContext.request.contextPath}/resources/images/marcador.png', google.maps.MapTypeId.SATELLITE);
                mostraMapaLocal(null, 'mapa-4sq-' + idLocal, latitude4SQ, longitude4SQ,'${pageContext.request.contextPath}/resources/images/marcador.png', google.maps.MapTypeId.SATELLITE);
                mostraMapaLocal(null, 'mapa-google-' + idLocal, latitude4SQ, longitude4SQ,'${pageContext.request.contextPath}/resources/images/marcador.png', google.maps.MapTypeId.SATELLITE);
            });
            
            $(document).on('click', '.btn-usar-foto', function(event) {
                event.preventDefault();
                
                var idLocal = $(this).data('local-id');
                var idFotoPanoramio = $(this).data('panoramio-id');
                var idUsuarioFotoPanoramio = $(this).data('usuario-panoramio-id');
                var $botao = $(this);
                var $secao = $('#secao-usar-foto-' + idLocal);
                
                $.ajax({
                    type: 'POST',
                    url: '<c:url value="/locais/cruds/usarFotoPanoramio/" />' + idLocal + '/' + idFotoPanoramio + '/' + idUsuarioFotoPanoramio,
                    success: function(data) {
                        $secao.hide();
                    }
                });
            });
            
            $(document).on('click', '.radioMostrarOutras', function(event) {
                var idLocal = $(this).data('local-id');
                var $botao = $('#btn-salvar-' + idLocal);
                $botao.addClass('btn-success');
            });
            
            $(document).on('click', '.btn-concluir', function(event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                
                //$(this).disable();
                $('#btn-salvar-' + idLocal).click();
                window.setTimeout(function() {
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/locais/cruds/concluirLocal/" />' + idLocal,
                        success: function(data) {
                            $('#linhaLocal_' + idLocal).hide();
                        }
                    })
                }, 500);
            });
            
            $(document).on('click', '.btn-preexcluir', function(event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                if (confirm('Deseja marcar este local como candidato � exclus�o?')) {
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/locais/cruds/marcarLocalCandidatoExclusao/" />' + idLocal,
                        success: function(data) {
                            $('#linhaLocal_' + idLocal).hide();
                        }
                    });
                }
            });
            
            $(document).on('click', '.btn-excluir', function(event) {
                event.preventDefault();
                var idLocal = $(this).data('local-id');
                if (confirm('Deseja realmente excluir este local? <br/> ATEN��O: S� confirme se tiver certeza, caso contr�rio, utilize o bot�o "Canditado � exclus�o" ')) {
                    $.ajax({
                        type: 'POST',
                        url: '<c:url value="/locais/cruds/marcarLocalExcluido/" />' + idLocal,
                        success: function(data) {
                            $('#linhaLocal_' + idLocal).hide();
                        }
                    });
                }
            });
            
            $('form.form-local :input').change(function() {
                var $form = $(this).closest('form');
            	var idLocal = $form.data('local-id');
            	$('#btn-salvar-' + idLocal).addClass('btn-success');
            });
        });
        
        function mostrarFotoLocal(idLocal, idFotoPanoramio, idUsuarioFotoPanoramio) {
            var requestOptions = {
                'ids': [{'userId': idUsuarioFotoPanoramio, 'photoId': idFotoPanoramio}]
            };
            mostrarFoto(idLocal, requestOptions)
        }
        
        function mostrarFotosProximas(link) {
            var idLocal = $(link).data('local-id')
            var latSW = $(link).attr('data-latsw');
            var lngSW = $(link).attr('data-lngsw');
            var latNE = $(link).attr('data-latne');
            var lngNE = $(link).attr('data-lngne');
            
            if (event) {
                event.preventDefault();
            }
            var requestOptions = {
                'set' : 'public',
                'rect' : {'sw': {'lat': latSW, 'lng': lngSW}, 'ne': {'lat': latNE, 'lng': lngNE}},
            };
            mostrarFoto(idLocal, requestOptions);
            $('#secao-usar-foto-' + idLocal).show();
            $('#usar-outras-panoramio-' + idLocal).show();
        }
        
        function mostrarFoto(idLocal, requestOptions) {
            var request = new panoramio.PhotoRequest(requestOptions);
            var widgetOptions = {
                'width': 150,
                'height': 150,
                'bgcolor' : 'white',
                'croppedPhotos' : panoramio.Cropping.TO_FILL,
                'attributionStyle': panoramio.tos.Style.HIDDEN,
                //'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
            };
            var photoWidget = new panoramio.PhotoWidget('panoramio-image-' + idLocal, request, widgetOptions);
            function onPhotoChanged(event) {
                if (event.target.getPhoto() != null) {
                    $('#btn-usar-foto-' + idLocal).attr('data-panoramio-id', event.target.getPhoto().getPhotoId());
                    $('#btn-usar-foto-' + idLocal).attr('data-usuario-panoramio-id', event.target.getPhoto().getOwnerId());
                }
            }
            panoramio.events.listen(photoWidget, panoramio.events.EventType.PHOTO_CHANGED, onPhotoChanged);
            photoWidget.setPosition(0);
        }

        </script>
            <c:choose>
                <c:when test="${not empty locais}">
                
                    <div align="center" style="font-size: 20px; padding: 5px;">
                        Mostrando: <strong>${quantidadeLocais}</strong> de <strong>${searchData.total}</strong>
                        <p>
                            <a href="#" class="btn btn-recarregar" style="margin-top: 5px;">Recarregar p�gina</a>
                        </p>
                    </div>
                    
                    <div id="div-locais">
                
                        <%@include file="listaTodosLocaisCidadePagina.jsp" %>
                    
                    </div>
                    
                    <div align="center" style="font-size: 20px; padding: 5px;">
                        <p>
                            <c:if test="${searchData.total > 50}">
                                <a href="#" class="btn btn-carregar-proximos" data-start="50" data-limit="50" style="margin-top: 5px;">Carregar pr�ximos</a>
                            </c:if>
                        </p>
                    </div>

                </c:when>
                <c:otherwise>
                    <div class="horizontalSpacer"></div>
                    <div class="centerAligned"><h4>N�o existem atra��es para essa sele��o.</h4></div>
                </c:otherwise>
            </c:choose>