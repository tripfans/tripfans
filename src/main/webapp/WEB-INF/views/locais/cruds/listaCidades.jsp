<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Gerenciamento de Cidades - TripFans" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		jQuery(document).ready(function() {
			$(".linkPopup").click(function(event){
				event.preventDefault();
				openPopupWindow(event.originalEvent.currentTarget,'formEdit',1000,600);
			});
		});
		$(".paginacao").click(function(event){
			var url = $(this).attr('href');
			jQuery.get(
		            url,
		            function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
		});
// 		Para montar a url da paginação{
// 			class='x' href=--------------------?${start}&${pais}
// 			$('#start').val()
// 			faz a function
// 			$('.x').click(function(){
// 				var url = $(this).attr('href')
// 				jQuery.get(
// 			            url,
// 			             function(response) {
// 			            	$('#listaPaginada').html(response);
// 			            }
// 			        );
// 			}
// 					)
// 		}
		function filtraCidades() {
			if(!$('input[name="pais"]').val()) {
				alert('Escolha um País.');
			 } else {
			    jQuery.get(
		            '${pageContext.request.contextPath}/locais/cruds/listaCidades',
		            {
		            	start:0,
		            	pais: $('input[name="pais"]').val()
		            }, function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
			}	
		}

		function selecionaCidade(event, ui) {
			var url = '${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoCidade/' + ui.item.id;
			window.open(url);
		}
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	<form:form id="destinosForm">
 							
			<div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
				<div class="page-header"><h2>Gerenciamento de Cidades</h2></div>
				<!--div class="control-group">
    				<label>Escolha o País</label>
    				<div class="pesquisa">
    	               <fan:autoComplete url="/pesquisaTextual/consultarPaises" name="nomeLocal" hiddenName="pais" id="nomePais" 
    	                     valueField="id" labelField="nome" cssClass="span7" 
    	                     value="${pais.nome}"
    	                     hiddenValue="${pais.id}"
    	                     placeholder="Informe o Pais..." 
    	                     blockOnSelect="false"/>
    	               	<input type="button" value="Pesquisar Cidades" class="btn"  onclick="filtraCidades()" />
    	            </div>
				</div-->
				
				<div class="control-group">
				<label>Escolha a Cidade</label>
				<div class="pesquisa">
	               <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocal" hiddenName="cidade" id="nomeCidade" 
	                     valueField="id" onSelect="selecionaCidade()" labelField="nomeExibicao"  cssClass="span7" 
	                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
	                     placeholder="Informe a Cidade..." 
	                     blockOnSelect="false"/>
	            </div>
				</div>
				<div id="listaPaginada">
					<c:if test="${cidades != null}">
						<%@include file="listaCidadesPaginada.jsp" %>
					</c:if>
				</div>
			</div>
			
			
		</form:form>
		
		
	</tiles:putAttribute>

</tiles:insertDefinition>