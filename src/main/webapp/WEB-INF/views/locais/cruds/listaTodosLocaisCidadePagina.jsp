<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%--ul class="pager">
    <c:choose>
        <c:when test="${paginaAtual != 1}">
            <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=0&cidade=${cidade.id}">Primeira</a></li>
            <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${param.start - limit}&cidade=${cidade.id}">Anterior</a></li>
        </c:when>
        <c:otherwise>
            <li class="disabled"><a href="#">Primeira</a></li>
            <li class="disabled"><a href="#">Anterior</a></li>
        </c:otherwise>
    </c:choose>
    
    <c:choose>
        <c:when test="${temMaisPaginas == true}">
            <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${param.start + limit}&cidade=${cidade.id}">Pr�xima</a></li>
            <li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${startUltimaPagina}&cidade=${cidade.id}">�ltima</a></li>
        </c:when>
        <c:otherwise>
            <li class="disabled"><a href="#">Pr�ximo</a></li>
            <li class="disabled"><a href="#">�ltima</a></li>
        </c:otherwise>
    </c:choose>
</ul--%>

<table id="tabela-locais" class="table table-striped table-bordered span19 tabela-locais">
<thead>
    <tr>
        <th width="10%">#</th>
        <th width="28%">Local no TripFans</th>
        <th width="29%">
            Local no Foursquare
            <!-- a id="botao-carregar-todos-4sq" href="#" class="btn btn-primary botao-carregar-todos-4sq" >
                Carregar todos os no Foursquare
            </a-->
        </th>
        <th width="29%">
            Local no Google
            <!-- a id="botao-carregar-todos-google" href="#" class="btn btn-success botao-carregar-todos-google" >
                Carregar todos os no Google
            </a-->
        </th>
        <th>
            Concluir
        </th>
    </tr>
</thead>

<c:forEach items="${locais}" var="local" varStatus="count">

<c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
<c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
<c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
<c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />

<tr id="linhaLocal_${local.id}">
    <td width="10%">
        <p align="center">
            ${count.count + (limit * (paginaAtual - 1))}
        </p>
        <p align="center">
          <a id="btn-mostrar-foto-${local.id}" href="#" class="btn btn-info btn-mostrar-foto" data-local-id="${local.id}" data-latsw="${latSW}" data-lngsw="${lngSW}" data-latne="${latNE}" data-lngne="${lngNE}">
            Carregar Foto(s)
          </a>
        </p>
        <c:if test="${not empty local.idFotoPanoramio}">
            <script>
                $(document).ready(function() {
                    mostrarFotoLocal(${local.id}, '${local.idFotoPanoramio}', '${local.idUsuarioFotoPanoramio}');
                });
            </script>
        </c:if>
        <div id="panoramio-image-${local.id}" style="max-height: 150px;">
        </div>
        <p id="secao-usar-foto-${local.id}" class="hide" align="center" style="margin-top: 10px;">
          'Usar' a foto acima como principal do local?
          <br/>
          <a id="btn-usar-foto-${local.id}" href="#" class="btn btn-info btn-usar-foto" data-local-id="${local.id}" data-panoramio-id="" data-usuario-panoramio-id="">
            Usar
          </a>
        </p>
        <p id="usar-outras-panoramio-${local.id}" class="${empty local.idFotoPanoramio ? 'hide' : ''}" align="center" style="margin-top: 10px;">
             Usar as outras fotos do carrossel?
             <br/>
             <span style="font-size: 10px">Marcar sim, caso as outras fotos tamb�m sejam deste local espec�fico ou das proximidades</span>
             <br/>
             <br/>
             <input type="radio" name="radioMostrarOutrasFotos-${local.id}" value="true" ${local.mostrarOutrasFotosPanoramio ? 'checked="checked"' : ''} data-local-id="${local.id}" class="radioMostrarOutras">Sim</input>
             <input type="radio" name="radioMostrarOutrasFotos-${local.id}" value="false" ${not local.mostrarOutrasFotosPanoramio ? 'checked="checked"' : ''} data-local-id="${local.id}" class="radioMostrarOutras">N�o</input>
        </p>
    </td>
    <td width="28%">
      <form id="form-local-${local.id}" class="form-local" data-local-id="${local.id}">
        <input type="hidden" name="idLocal" value="${local.id}" />
        <input type="hidden" id="mostrarOutrasFotosPanoramio-${local.id}" name="mostrarOutrasFotosPanoramio" />
        <div style="width: 15%; float: left;">
            Nome:
        </div>
        <div style="width: 85%; float: left;">
            <input id="input-nome-${local.id}" type="text" name="nome" value="${local.nome}" style="width: 98%;" />
        </div>
        <div style="width: 15%; float: left;">
            Endere�o: 
        </div>
        <div style="width: 85%; float: left;">
            <input id="input-endereco-${local.id}" type="text" name="endereco" value="${local.endereco.descricao}" style="width: 98%;" />
        </div>
        <div style="width: 15%; float: left;">
            Bairro: 
        </div>
        <div style="width: 85%; float: left;">
            <input id="input-bairro-${local.id}" type="text" name="bairro" value="${local.endereco.bairro}" style="width: 76%;" />
            <a href="http://www.buscacep.correios.com.br" target="busca_cep" title="Para confirmar o endere�o e tentar descobrir o bairro">Consultar</a>
        </div>
        <div style="width: 15%; float: left;">
            CEP:
        </div> 
        <div style="width: 85%; float: left;">
            <input id="input-cep-${local.id}" type="text" name="cep" value="${local.endereco.cep}" style="width: 70%;" />
        </div>
        <div style="width: 15%; float: left;">
            Latitude: 
        </div>
        <div style="width: 85%; float: left;">
            <input id="input-latitude-${local.id}" type="text" name="latitude" value="${local.latitude}" style="width: 70%;" />
        </div>
            
        <div style="width: 15%; float: left;">
            Longitude:
        </div>
        <div style="width: 85%; float: left;"> 
            <input id="input-longitude-${local.id}" type="text" name="longitude" value="${local.longitude}" style="width: 70%;" />
        </div>

        <div style="width: 15%; float: left;">
            Telefone:
        </div> 
        <div style="width: 85%; float: left;">
            <input id="input-tel-${local.id}" type="text" name="telefone" value="${local.telefone}" style="width: 70%;" />
        </div>
        
        <div style="width: 15%; float: left;">
            Email:
        </div> 
        <div style="width: 85%; float: left;">
            <input id="input-email-${local.id}" type="text" name="email" value="${local.email}" style="width: 98%;" />
        </div>
        
        <div style="width: 15%; float: left;">
            Site:
        </div> 
        <div style="width: 85%; float: left;">
            <input id="input-site-${local.id}" type="text" name="site" value="${local.site}" style="width: 98%;" />
            <a href="#" class="visitar" data-input="input-site-${local.id}" target="_blank" >Visitar</a>
        </div>
        
        <div style="width: 15%; float: left;">
            Categoria(s):
            <br/>
        </div> 
        <div style="width: 85%; float: left;">
            <br/>
            <c:forEach items="${local.classificacoes}" var="classificacao" varStatus="status">
                ${status.index > 0 ? ',' : ''}${classificacao.classe.descricao}
            </c:forEach>
        </div>

        <div style="width: 15%; float: left;">
            Outras categoria(s):
            <br/>
        </div> 
        <div style="width: 85%; float: left;">
            <br/>
            <input id="input-outras-categorias-${local.id}" type="text" name="outrasCategorias" value="${local.outrasCategorias}" style="width: 98%;" />
        </div>
      </form>
      <p align="center">
        <a id="btn-salvar-${local.id}" href="#" class="btn btn-salvar btn-large" data-local-id="${local.id}">
            Salvar
        </a>
        <a href="#" class="btn btn-warning btn-comparar-mapas hide" data-local-id="${local.id}">
            Comparar mapas
        </a>
        <div class="span6 coordendada" id="mapa-tf-${local.id}" style="border: 1px solid #CCC; height: 150px; margin-top: 15px; display: none;" data-latitude="${local.latitude}" data-longitude="${local.longitude}">
      </p>
    </td>
    <td width="29%">
      <div>
        <p align="center">
          <a href="#" class="btn btn-primary botao-carregar-4sq" data-local-id="${local.id}">
            Carregar do Foursquare
          </a>
        </p>
      </div>
      <div id="coluna-dados-externos-4sq-${local.id}" class="coluna-dados-externos-4sq" data-id-local="${local.id}">
      
      </div>
    </td>
    <td width="29%" style="max-width: 250px; overflow-x: hidden;">
      <div>
        <p align="center">
          <a href="#" class="btn btn-success botao-carregar-google" data-local-id="${local.id}">
            Carregar do Google
          </a>
        </p>
      </div>
      <div id="coluna-dados-externos-google-${local.id}" class="coluna-dados-externos-google" data-id-local="${local.id}">
      
      </div>
    </td>
    <td width="4%" style="vertical-align: middle;">
        <p align="center">
          <c:set var="tipoDoLocal" value="${local.tipoAtracao ? 'Atracao' : (local.tipoHotel ? 'Hotel' : 'Restaurante')}"></c:set>
          <a href="<c:url value="/locais/cruds/exibirAlteracao${tipoDoLocal}/${local.id}"/>" target="editar_local" class="btn btn-editar" style="height: 65px;">
            <br/>
            Editar mais dados
          </a>
        </p>
        <p align="center">
          <a href="#" class="btn btn-success btn-concluir" data-local-id="${local.id}" style="height: 65px;">
            <br/>
            Concluir
          </a>
        </p>
        <p align="center">
          <a href="#" class="btn btn-warning btn-preexcluir" data-local-id="${local.id}" style="height: 65px;">
            Candidato a Exclus�o
          </a>
        </p>
        <p align="center">
          <a href="#" class="btn btn-danger btn-excluir" data-local-id="${local.id}" style="height: 65px;">
            <br/>
            Excluir
          </a>
        </p>
    </td>
</tr>

</c:forEach>
</table>
<%--ul class="pager">
    <c:choose>
    <c:when test="${paginaAtual != 1}">
        <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=0&cidade=${cidade.id}">Primeira</a></li>
        <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${param.start - limit}&cidade=${cidade.id}">Anterior</a></li>
    </c:when>
    <c:otherwise>
        <li class="disabled"><a href="#">Primeira</a></li>
        <li class="disabled"><a href="#">Anterior</a></li>
    </c:otherwise>
    </c:choose>
    
    <c:choose>
    <c:when test="${temMaisPaginas == true}">
        <li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${param.start + limit}&cidade=${cidade.id}">Pr�xima</a></li>
        <li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaAtracoes?start=${startUltimaPagina}&cidade=${cidade.id}">�ltima</a></li>
    </c:when>
    <c:otherwise>
        <li class="disabled"><a href="#">Pr�ximo</a></li>
        <li class="disabled"><a href="#">�ltima</a></li>
    </c:otherwise>
    </c:choose>
</ul--%>