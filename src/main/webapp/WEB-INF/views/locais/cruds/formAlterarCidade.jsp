<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="TripFans - ${cidade.nome}" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
    
		<script type="text/javascript">
			jQuery(document).ready(function() {
				
				$('#tabs-cidade a[href="#informacoes-basicas"]').tab('show');
				
				$("#cidadeForm").validate({
			        rules: {
			            urlPath: "required"
			        },
			        messages: { 
			            urlPath: "Você deve informar uma URL"
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.control-group")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });

				
			    $("#cidadeForm").initToolTips();

			    $("#urlPath").typeWatch({
			    	callback: consultarDisponibilidade,
			        wait: 750,
			        highlight: true,
			        captureLength: 5
			    });

			    function consultarDisponibilidade(){
			    	$('#responseStatus').remove();
			    	jQuery(':submit').removeAttr('disabled');
				    var nomeLocal = $('#urlPath').val();
			        if(nomeLocal !== '${cidade.urlPath}') {
			    		var regex = new RegExp(/^[a-zA-Z0-9\-_]+$/);
			        	if(nomeLocal.match(regex)) {
					        $('#urlPath').parent().append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="20" style="margin-left: 8px;" >');
					    	$.post('<c:url value="/locais/cruds/consultarDisponibilidadeNome" />', {'nomeLocal' : nomeLocal}, 
					    		function(data) {
					    			var resposta = data.params.resposta;
					    			var iconClass = "cross";
					    			if(data.params.disponivel) {
					    				iconClass = "accept";
					    			} else {
					    				jQuery(':submit').attr('disabled', 'disabled');
					    			}
					    			var markup = '<span id="responseStatus"><span style="padding: 2px;" class="'+iconClass+'">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>'+resposta+'</em></span>';
					    			$('#loadingImg').remove();
					    			$('#urlPath').parent().append(markup);
					    		}
					    	);
			        	} else {
			        		$('#urlPath').parent().append('<span id="responseStatus"><span style="padding: 2px;" class="cross">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>Contém caracteres inválidos</em></span>');
			        		jQuery(':submit').attr('disabled', 'disabled');
			        	}
			        }
			    }
			});
			
            $('#tabs-cidade a').click(function (e) {
                e.preventDefault();
                var tabPane = $(this).attr('href').substring(1);
                
                $(document).off('click', '**');
                
                $('#' + tabPane).load('<c:url value="/locais/cruds/listar" />' + tabPane + 'Cidade/' + ${cidade.id});
                $(this).tab('show');
            });
			
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
    	<c:choose>
			<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
        		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        		<script src="https://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR" type="text/javascript"></script>
			</c:when>
			<c:otherwise>
				<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        		<script src="http://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR" type="text/javascript"></script>
			</c:otherwise>
		</c:choose>
    
		<div class="container">
		  <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
          
              <div>
              
                <h2 class="centerAligned">${cidade.nomeCompleto}</h2> 
                <div class="right"><a href="<c:url value="/locais/${cidade.urlPath}" />" target="_blank">Preview</a></div>
              
              </div>
          
              <ul id="tabs-cidade" class="nav nav-tabs" style="height: 35px; max-height: 35px;">
                <li>
                    <a id="link-informacoesBasicas" href="#informacoes-basicas" data-toggle="tab">Informações Básicas</a>
                </li>
                <li>
                    <a id="link-hoteis" href="#Hoteis" data-toggle="tab">Hotéis</a>
                </li>
                <li>
                    <a id="link-restaurantes" href="#Restaurantes" data-toggle="tab">Restaurantes</a>
                </li>
                <li>
                    <a id="link-atracoes" href="#Atracoes" data-toggle="tab">Atrações</a>
                </li>
              </ul>
              
              <div class="tab-content">
    
                <div class="tab-pane active" id="informacoes-basicas">
              
        			<form:form modelAttribute="cidade" id="cidadeForm" action="${pageContext.request.contextPath}/locais/cruds/alterarCidade" method="post" enctype="multipart/form-data">
        				<s:hasBindErrors name="cidade">
        					<div id="errorBox" class="alert alert-error centerAligned"> 
        					  <p><s:message code="validation.containErrors" /></p> 
        					</div>
        				</s:hasBindErrors>
        				<div id="errorBox" style="display: none;" class="alert alert-error centerAligned"> 
        					 <p id="errorMsg">
        					<s:message code="validation.containErrors" />
        					</p>
        				</div>
        				
        				<input type="hidden" name="cidade" value="${cidade.id}" />
        
        				<div class="control-group">
            				<label for="nome"  >Nome</label>
            				<form:input cssClass="span7" id="nome" path="nome" disabled="true" readonly="true" />
        				</div>
        				
        			   <div class="control-group">
                               <label>
                                   <span class="required">URL</span>
                                   <i id="help-nome" class="icon-question-sign helper" title="Esse é o caminho para acessar o local na aplicação. Exemplo: tripfans.com.br/${cidade.urlPath}"></i>
                               </label>
                               <div class="controls">
                                   <form:input path="urlPath" id="urlPath" cssClass="span7" />
                               </div>
                        </div>
        				
        				<div class="control-group">
            				<label for="latitude">Latitude</label>
            				<form:input id="coordenadaGeografica.latitude" path="coordenadaGeografica.latitude" />
        				</div>
        				
        				<div class="control-group">
            				<label for="longitude">Longitude</label>
            				<form:input id="coordenadaGeografica.longitude" path="coordenadaGeografica.longitude" />
        				</div>
                        
                        <div class="control-group">
                            <label for="latitude">Latitude para Fotos (Ponto central para exibição de fotos do Panoramio)</label>
                            <form:input id="coordenadaGeograficaFotos.latitude" path="coordenadaGeograficaFotos.latitude" />
                        </div>
                        
                        <div class="control-group">
                            <label for="longitude">Longitude para Fotos (Ponto central para exibição de fotos do Panoramio)</label>
                            <form:input id="coordenadaGeograficaFotos.longitude" path="coordenadaGeograficaFotos.longitude" />
                        </div>
                        
        				<c:if test="${not cidade.tipoLocalInteresseTuristico}">
            				<div class="control-group">
                				<label for="capitalEstado" class="checkbox  inline" >
                				<form:checkbox id="capitalEstado" path="capitalEstado" />
                				É a capital do Estado
            				</label>
            				
            				<label for="capitalPais" class="checkbox inline" >
                				<form:checkbox id="capitalPais" path="capitalPais" />
                				É a capital do País
                				</label>
            				</div>
                        </c:if>
        				
        				<div class="control-group">
            				<label for="idFacebook"  >Identificador do Facebook
            				<i class="icon-question-sign helper" title="Identificador desse local no Facebook (exemplo: facebook.com/108142565873758)"></i>
            				</label>
            				<form:input id="idFacebook" path="idFacebook" />
        				</div>
        				
        				<div class="control-group">
            				<label for="idFoursquare"  >Identificador do FourSquare
            				<i class="icon-question-sign helper" title="Identificador desse local no FourSquare (exemplo: foursquare.com/v/veredas-grill/4baa4086f964a520ce573ae3)"></i>
            				</label>
            				<form:input id="idFoursquare" path="idFoursquare" />
        				</div>
        				
        				<div class="control-group">
            				<label for="textarea" >
                				Texto descritivo da Cidade
                				<i class="icon-question-sign helper" title="Descrição turística da cidade. Você pode usar como fonte de informação os sites Wikitravel, Wikipedia, Ministério do Turismo ou mesmo o site da prefeitura da Cidade"></i>
            				</label>
            				<form:textarea cssClass="span10"  id="textarea" path="descricao" cssStyle="height: 150px;"></form:textarea>
        				</div>
        				
        				<!-- Colocar autocomplete -->
        				<!-- <div> quando popular a base de continentes colocar esse campo aqui.
        				<label for="continente"  >Continente</label><br/>
        				<%--form:input id="continente" path="continenteNome" /--%>
        				</div> -->
        				
        				<div class="control-group">
        					<label>País</label>
        					<div class="pesquisaPais">
        		               <fan:autoComplete url="/pesquisaTextual/consultarPaises" name="nomePais" hiddenName="pais" id="nomePais" 
        		                     valueField="id" labelField="nome" cssClass="span7" 
        		                     value="${cidade.pais.nome}"
        		                     hiddenValue="${cidade.pais.id}"
        		                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
        		                     placeholder="Informe o Pais..." 
        		                     blockOnSelect="false"/>
        		            </div>
        	            </div>
        				
        				<div class="control-group">
        					<label>Estado</label>
        					<div class="pesquisaEstado">
        		               <fan:autoComplete url="//pesquisaTextual/consultarEstados" name="nomeEstado" hiddenName="estado" id="nomeEstado" 
        		                     valueField="id" labelField="nome" cssClass="span7" 
        		                     value="${cidade.estado.nome}"
        		                     hiddenValue="${cidade.estado.id}"
        		                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
        		                     placeholder="Informe o Estado..." 
        		                     blockOnSelect="false"
        		                     extraParams="{pais: $('#nomePais_hidden').val()}"/>
        	            	</div>
        	            </div>
        				
        				<fieldset>
                            <legend>Fotos</legend>
                            <c:set var="fotoPrincipal" value="${cidade.fotoPadrao}"/>
                            <c:set var="urlFotoPrincipal" value="${cidade.urlFotoAlbum}"/>
                            <%@include file="camposFotoPrincipal.jsp" %>
                            
                            <%@include file="camposIncluirFotos.jsp" %>
                        </fieldset>
        				
        				<div class="form-actions centerAligned">
        				    <input type="submit" id="botaoSalvar" class="btn btn-primary btn-large" value="Salvar" >
        				</div>
        			</form:form>
                </div>
                
                <div class="tab-pane" id="Hoteis">
                    
                </div>

                <div class="tab-pane" id="Restaurantes">
                    
                </div>
                
                <div class="tab-pane" id="Atracoes">
                    
                </div>
                
                
            </div>
            
            <div id="terms-of-service"></div>
            <script>
                var terms_widget = new panoramio.TermsOfServiceWidget('terms-of-service', {'width': '100%'});
            </script>
                
		  </div>
 		</div>
	</tiles:putAttribute>


</tiles:insertDefinition>