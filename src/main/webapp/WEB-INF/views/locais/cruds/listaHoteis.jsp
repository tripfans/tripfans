<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Gerenciamento de Hotéis - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		jQuery(document).ready(function() {
			$(".linkPopup").click(function(event){
				event.preventDefault();
				openPopupWindow(event.originalEvent.currentTarget,'formEdit',1000,600);
			});
		});
		
		$(".paginacao").click(function(event){
            var url = $(this).attr('href');
            jQuery.get(
                    url,
                    function(response) {
                        $('#listaPaginada').html(response);
                    }
                );
        });
		
		function filtraHoteis() {
            if(!$('input[name="cidade"]').val()) {
                alert('Escolha uma cidade.');
             } else {
                jQuery.get(
                    '${pageContext.request.contextPath}/locais/cruds/listaHoteis',
                    {
                        start:0,
                        cidade: $('input[name="cidade"]').val()
                    }, function(response) {
                        $('#listaPaginada').html(response);
                    }
                );
            }   
        }

        function selecionaCidade(event, ui) {
            var id = ui.item.id;
            $('input[name="cidade"]').val(id);
        }
        
        function selecionaHotel(event, ui) {
            var url = '${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoHotel/' + ui.item.id;
            window.open(url);
        }
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
          
            <form:form id="destinosForm">
              <div class="span19">
                <div class="page-header"><h2>Gerenciamento de Hotéis</h2></div>
                <div class="control-group">
                <label>Escolha a cidade</label>
                <div class="pesquisa">
                   <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeCidade" hiddenName="cidade" id="nomeCidade" 
                         valueField="id" labelField="nomeExibicao" onSelect="selecionaCidade()" cssClass="span7" 
                         value="${cidade.nome}"
                         hiddenValue="${cidade.id}"
                         jsonFields="[{name : 'id'}, {name : 'nomeExibicao'}]"
                         placeholder="Informe a Cidade..." 
                         blockOnSelect="false"/>
                    <input type="button" value="Pesquisar Hotéis" class="btn" onclick="filtraHoteis()" />
                </div>
                </div>
                
                <div class="control-group">
                <label>Ou escolha diretamente o Hotel</label>
                <div class="pesquisa">
                   <fan:autoComplete url="/pesquisaTextual/consultarHoteis" name="nomeLocal" hiddenName="hotel" id="nomeHotel" 
                         valueField="id" onSelect="selecionaHotel()" labelField="nomeExibicao"  cssClass="span7" 
                         jsonFields="[{name : 'id'}, {name : 'nome'}]"
                         placeholder="Informe a Hotel..." 
                         blockOnSelect="false"/>
                </div>
                </div>
                <div id="listaPaginada">
                    <c:if test="${hoteis != null}">
                        <%@include file="listaHoteisPaginada.jsp" %>
                    </c:if>
                </div>
              </div>
            </form:form>
 							
			<%-- div class="span-24">
			<h1>Lista de Hotéis</h1>
			<c:choose>
				<c:when test="${not empty hoteis}">
				
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
					
					<table  style="border: 1px solid #C3C3C3;" class="zebra-striped">
					<thead>
						<tr>
						<th width="2%">#</th>
						<th width="20%">Nome</th>
						<th width="2%">Estrelas/Quartos</th>
						<th width="2%">Endereço</th>
						<th width="2%">Contato</th>
						<th>Descricao</th>
						<th width="15%">URL-Path</th>
						<th width="12%">Ações</th>
						</tr>
					</thead>
					<c:forEach items="${hoteis}" var="hotel" varStatus="count">
					<tr>
						<td width="2%">${count.count + (30 * (paginaAtual - 1))}</td>
						<td width="20%"><a href="${pageContext.request.contextPath}/locais/hotel/${hotel.urlPath}" target="_blank">${hotel.nome}</a></td>
						<td width="2%">
						<strong>Estrelas:</strong> ${hotel.estrelas}
						<br/><strong>Quartos:</strong> ${hotel.numeroQuartos}
						</td>
						<td width="20%">
						<strong>Endereço:</strong> ${hotel.endereco.descricao}
						<br/><strong>CEP:</strong> ${hotel.endereco.cep}
						<br/><strong>Cidade:</strong> ${hotel.cidade.nome}
						<br/><strong>Estado:</strong> ${hotel.estado.nome}  
						</td>
						<td width="20%">
						<c:if test="${not empty hotel.email}">
						<strong>Email:</strong> ${hotel.email} <br/>
						</c:if>
						<c:if test="${not empty hotel.site}">
						<strong>Site:</strong> <a href="${hotel.site}" target="_blank">${hotel.site}</a> <br/>
						</c:if>
						<c:if test="${not empty hotel.telefone}">
						<strong>Telefone:</strong> ${hotel.telefone}
						</c:if>
						</td>
						<td>${hotel.descricao}</td>
						<td width="15%">${hotel.urlPath}</td>
						<td width="12%">
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoHotel/${hotel.id}" class="linkPopup">Editar</a><br/>
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAdicaoFotosLocal?local=${hotel.id}" class="linkPopup">Adicionar Fotos</a><br/>
						<a href="${pageContext.request.contextPath}/locais/hotel/${hotel.urlPath}" target="_blank">Preview</a>
						</td>
					</tr>
					</c:forEach>
					</table>
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaHoteis/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
				</c:when>
				<c:otherwise>
					<h4>Não existem hoteis.</h4>
				</c:otherwise>
			</c:choose>
			
					
			</div--%>
			
	      </div>
		</div>
		
		
	</tiles:putAttribute>

</tiles:insertDefinition>