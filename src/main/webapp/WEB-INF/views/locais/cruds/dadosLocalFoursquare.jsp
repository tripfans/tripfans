<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
    <c:when test="${not empty dadosFoursquare}">
    
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="98%">Dados do 4SQ</th>
                </tr>
            </thead>
            <tr>
                <td width="98%">
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Nome: <strong><span id="4sq-nome-${local.id}">${dadosFoursquare.nome}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${dadosFoursquare.nome != local.nome}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-destino="nome-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Nome'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Endere�o: <strong><span id="4sq-endereco-${local.id}">${dadosFoursquare.endereco}</span></strong> 
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.endereco and dadosFoursquare.endereco != local.endereco.descricao}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-destino="endereco-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Endere�o'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            CEP: <strong><span id="4sq-cep-${local.id}">${dadosFoursquare.cep}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.cep and dadosFoursquare.cep != local.endereco.cep}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-destino="cep-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'CEP'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div>
                      <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Latitude: <strong><span id="4sq-lat-${local.id}">${dadosFoursquare.latitude}</span></strong><br/>
                            Longitude: <strong><span id="4sq-lon-${local.id}">${dadosFoursquare.longitude}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${dadosFoursquare.latitude != local.latitude or dadosFoursquare.longitude != local.longitude}">
                            <a href="#" class="btn btn-primary btn-large btn-copiar-lat-lon" data-site-origem="4sq" data-local-id="${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Lat/Lng'
                            </a>
                          </c:if>
                        </div>
                      </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Telefone: <strong><span id="4sq-tel-${local.id}">${dadosFoursquare.telefone}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.telefone and dadosFoursquare.telefone != local.telefone}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-origem="4sq-tel-${local.id}" data-input-destino="tel-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Telefone'
                            </a>
                          </c:if>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Telefone Formatado: <strong><span id="4sq-telf-${local.id}">${dadosFoursquare.telefoneFormatado}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.telefoneFormatado and dadosFoursquare.telefoneFormatado != local.telefone}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-origem="4sq-telf-${local.id}" data-input-destino="tel-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Telefone Form.'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Email: <strong><span id="4sq-email-${local.id}">${dadosFoursquare.email}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.email and dadosFoursquare.email != local.email}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-destino="email-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Email'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            URL: <strong><span id="4sq-site-${local.id}">${dadosFoursquare.url}</span></strong><br/>
                            Short URL: <strong><span id="4sq-surl-${local.id}">${dadosFoursquare.shortUrl}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty dadosFoursquare.url and dadosFoursquare.url != local.site}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-destino="site-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Site'
                            </a>
                          </c:if>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Categoria(s): 
                        </div>
                        <div style="width: 50%; float: left;">
                            <c:forEach items="${dadosFoursquare.listaNomesCategorias}" var="nomeCategoria" varStatus="status">
                                <strong><span id="4sq-categoria-${status.index}-${local.id}">${nomeCategoria}</span></strong>
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="4sq" data-input-origem="4sq-categoria-${status.index}-${local.id}" data-input-destino="outras-categorias-${local.id}" data-replace="false">
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'categoria'
                                </a>
                            </c:forEach>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Facebook: <strong><span id="4sq-fb-${local.id}">${dadosFoursquare.facebook}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Twitter: <strong><span id="4sq-tw-${local.id}">${dadosFoursquare.twitter}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                        </div>
                    </div>
                    
                    <div>
                      <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <c:if test="${not empty local.idFoursquare}">
                          <p align="center">
                            <a href="#" class="btn btn-warning btn-comparar-mapas" data-local-id="${local.id}">
                                Comparar mapas
                            </a>
                            <div class="span5" id="mapa-4sq-${local.id}" style="border: 1px solid #CCC; height: 150px; margin-top: 15px; display: none;" data-latitude="${dadosFoursquare.latitude}" data-longitude="${dadosFoursquare.longitude}">
                          </p>
                        </c:if>
                      </div>
                    </div>
                    
                </td>
            </tr>
        </table>    
    </c:when>
    <c:otherwise>
        <div class="horizontalSpacer"></div>
        <div class="centerAligned"><h4>N�o foram encontrados locais semelhantes no Foursquare.</h4></div>
    </c:otherwise>
</c:choose>