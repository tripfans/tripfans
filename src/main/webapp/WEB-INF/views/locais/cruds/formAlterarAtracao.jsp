<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="TripFans - ${atracao.nome}" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {

				$("#atracaoForm").validate({
			        rules: {
			        	<c:if test="${inclusao == true}">
			        	nome: "required",
			        	cidade: "required",
			        	pais: "required",
			        	</c:if>
			            urlPath: "required"
			        },
			        messages: {
			        	<c:if test="${inclusao == true}">
			        	nome: "Você deve informar um nome",
			        	cidade: "Você deve informar uma cidade",
			        	pais: "Você deve informar um país",
			        	</c:if>
			            urlPath: "Você deve informar uma URL"
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.control-group")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });

				
			    $("#atracaoForm").initToolTips();

			    $("#urlPath").typeWatch({
			    	callback: consultarDisponibilidade,
			        wait: 750,
			        highlight: true,
			        captureLength: 5
			    });
			    
			    function consultarDisponibilidade(){
			    	$('#responseStatus').remove();
			    	jQuery(':submit').removeAttr('disabled');
				    var nomeLocal = $('#urlPath').val();
			        if(nomeLocal !== '${atracao.urlPath}') {
			    		var regex = new RegExp(/^[a-zA-Z0-9\-_]+$/);
			        	if(nomeLocal.match(regex)) {
					        $('#urlPath').parent().append('<img id="loadingImg" src="${pageContext.request.contextPath}/resources/images/loading.gif" width="20" style="margin-left: 8px;" >');
					    	$.post('<c:url value="/locais/cruds/consultarDisponibilidadeNome" />', {'nomeLocal' : nomeLocal}, 
					    		function(data) {
					    			var resposta = data.params.resposta;
					    			var iconClass = "cross";
					    			if(data.params.disponivel) {
					    				iconClass = "accept";
					    			} else {
					    				jQuery(':submit').attr('disabled', 'disabled');
					    			}
					    			var markup = '<span id="responseStatus"><span style="padding: 2px;" class="'+iconClass+'">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>'+resposta+'</em></span>';
					    			$('#loadingImg').remove();
					    			$('#urlPath').parent().append(markup);
					    		}
					    	);
			        	} else {
			        		$('#urlPath').parent().append('<span id="responseStatus"><span style="padding: 2px;" class="cross">&nbsp;&nbsp;&nbsp;&nbsp;</span><em>Contém caracteres inválidos</em></span>');
			        		jQuery(':submit').attr('disabled', 'disabled');
			        	}
			        }
			    }
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
    
			<div class="span20">
			<h2 class="centerAligned">${atracao.nomeCompleto}</h2> 
			<div class="right"><a href="<c:url value="/locais/${atracao.urlPath}" />" target="_blank">Preview</a></div>
			
			<form:form modelAttribute="atracao" id="cidadeForm" action="${pageContext.request.contextPath}/locais/cruds/alterarAtracao" method="post" enctype="multipart/form-data">
				<s:hasBindErrors name="atracao">
					<div id="errorBox" class="alert alert-error centerAligned"> 
					  <p><s:message code="validation.containErrors" /></p> 
					</div>
				</s:hasBindErrors>
				<div id="errorBox" style="display: none;" class="alert alert-error centerAligned"> 
					 <p id="errorMsg">
					<s:message code="validation.containErrors" />
					</p>
				</div>
				
				<c:if test="${inclusao != true}">
				    <input type="hidden" name="atracao" value="${atracao.id}" />
				</c:if>

				<div class="control-group">
    				<label for="nome"  >Nome</label>
    				<c:set var="disabled" value="true" />
				    <c:if test="${inclusao == true}">
    				    <c:set var="disabled" value="false" />
    				</c:if>
    				<form:input cssClass="span7" id="nome" path="nome" />
				</div>
				
			    <div class="control-group">
                   <label>
                       <span class="required">URL Path</span>
                       <i id="help-nome" class="icon-question-sign helper" title="Esse é o caminho para acessar o local na aplicação. Exemplo: tripfans.com.br/${cidade.urlPath}"></i>
                   </label>
                   <div class="controls">
                       <form:input path="urlPath" id="urlPath" cssClass="span7" />
                   </div>
                </div>
				
				<div class="control-group">
    				<label for="latitude">Latitude</label>
    				<form:input id="coordenadaGeografica.latitude" path="coordenadaGeografica.latitude" />
				</div>
				
				<div class="control-group">
    				<label for="longitude">Longitude</label>
    				<form:input id="coordenadaGeografica.longitude" path="coordenadaGeografica.longitude" />
				</div>
				
				<div class="control-group">
    				<label for="idFacebook">Identificador do Facebook
    				    <i class="icon-question-sign helper" title="Identificador desse local no Facebook (exemplo: facebook.com/108142565873758)"></i>
    				</label>
    				<form:input id="idFacebook" path="idFacebook" />
				</div>
				
				<div class="control-group">
    				<label for="idFoursquare">Identificador do FourSquare
    				    <i class="icon-question-sign helper" title="Identificador desse local no FourSquare (exemplo: foursquare.com/v/veredas-grill/4baa4086f964a520ce573ae3)"></i>
    				</label>
    				<form:input id="idFoursquare" path="idFoursquare" />
				</div>
				
				<div class="control-group">
    				<label for="textarea" >
    				    Texto descritivo da Atração
    				    <i class="icon-question-sign helper" title="Descrição turística da atração. Você pode usar como fonte de informação os sites Wikitravel, Wikipedia, Ministério do Turismo ou mesmo o site da prefeitura da Cidade"></i>
    				</label>
    				<form:textarea cssClass="span10" cssStyle="height: 120px;" id="textarea" path="descricao" ></form:textarea>
				</div>
				
				<!-- Colocar autocomplete -->
				<!-- <div> quando popular a base de continentes colocar esse campo aqui.
				<label for="continente"  >Continente</label><br/>
				<form:input id="continente" path="continenteNome" />
				</div> -->
				
				<div class="control-group">
					<label>Cidade</label>
					<div class="pesquisaCidade">
		               <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeCidade" hiddenName="cidade" id="nomeCidade" 
		                     valueField="id" labelField="nome" cssClass="span7" 
		                     value="${atracao.cidade.nome}"
		                     hiddenValue="${atracao.cidade.id}"
		                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
		                     placeholder="Informe a Cidade..." 
		                     blockOnSelect="false"/>
		            </div>
	            </div>
	            
	            <%-- div class="control-group">
					<label>Estado</label>
					<div class="pesquisaEstado">
		               <fan:autoComplete url="/pesquisaTextual/consultarEstados" name="nomeEstado" hiddenName="estado" id="nomeEstado" 
		                     valueField="id" labelField="nome" cssClass="span7" 
		                     value="${atracao.estado.nome}"
		                     hiddenValue="${atracao.estado.id}"
		                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
		                     placeholder="Informe o Estado..." 
		                     blockOnSelect="false"
		                     />
	            	</div>
	            </div>
				
				<div class="control-group">
					<label>País</label>
					<div class="pesquisaPais">
		               <fan:autoComplete url="/pesquisaTextual/consultarPaises" name="nomePais" hiddenName="pais" id="nomePais" 
		                     valueField="id" labelField="nome" cssClass="span7" 
		                     value="${atracao.pais.nome}"
		                     hiddenValue="${atracao.pais.id}"
		                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
		                     placeholder="Informe o Pais..." 
		                     blockOnSelect="false"/>
		            </div>
	            </div--%>
				
				
			 	<fieldset>
                    <legend>Fotos</legend>
                    <c:set var="fotoPrincipal" value="${atracao.fotoPadrao}"/>
                    <c:set var="urlFotoPrincipal" value="${atracao.urlFotoAlbum}"/>
                    <%@include file="camposFotoPrincipal.jsp" %>
                    
                    <%@include file="camposIncluirFotos.jsp" %>
                </fieldset>
				
				<div class="form-actions centerAligned">
				    <input type="submit" id="botaoSalvar" class="btn btn-primary btn-large" value="Salvar" >
				</div>
			</form:form>
            
            </div>
		  </div>
 		</div>
	</tiles:putAttribute>


</tiles:insertDefinition>