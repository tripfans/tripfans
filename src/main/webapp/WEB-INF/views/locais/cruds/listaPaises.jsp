<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Lista de Países - TripFans" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="container">
 							
			<div class="span-24">
			<h1>Lista de Países</h1>
			
			<div style="text-align: center;">
				
				<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
				<c:choose>
				<c:when test="${paginaAtual != 1}">
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
				</c:when>
				<c:otherwise>
					 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
				</c:otherwise>
				</c:choose>
				
				<c:choose>
				<c:when test="${temMaisPaginas == true}">
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
				</c:when>
				<c:otherwise>
					Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
				</c:otherwise>
				</c:choose>
				
			</div>
			
			<table  style="border: 1px solid #C3C3C3;">
			<thead>
				<tr>
				<th>#</th>
				<th>Nome do País</th>
				<th>Descricao</th>
				<th>URL</th>
				<th>Ações</th>
				</tr>
			</thead>
			<c:forEach items="${paises}" var="pais" varStatus="count">
			<tr>
				<td width="2%">${count.count + (30 * (paginaAtual - 1))}</td>
				<td><a href ="#">${pais.nome}</a></td>
				<td>${pais.descricao}</td>
				<td>${pais.urlPath}</td>
				<td><a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${pais.id}">Estados</a>&nbsp;&nbsp;
				<a href ="${pageContext.request.contextPath}/locais/cruds/listaCidades/${pais.id}">Cidades</a>&nbsp;&nbsp;
				<a href ="${pageContext.request.contextPath}/locais/cruds/salvarPais/${pais.id}">Editar</a></td>
			</tr>
			</c:forEach>
			</table>
			<div style="text-align: center;">
				
				<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
				<c:choose>
				<c:when test="${paginaAtual != 1}">
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
				</c:when>
				<c:otherwise>
					 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
				</c:otherwise>
				</c:choose>
				
				<c:choose>
				<c:when test="${temMaisPaginas == true}">
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
					<a href ="${pageContext.request.contextPath}/locais/cruds/listaPaises/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
				</c:when>
				<c:otherwise>
					Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
				</c:otherwise>
				</c:choose>
				
			</div>

			</div>
			
			
		</div>
		
		
	</tiles:putAttribute>

</tiles:insertDefinition>