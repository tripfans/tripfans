<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript">

    $(document).ready(function() {
    	
    });

</script>

<div id="dadosGoogle-${local.id}">
<c:choose>
    <c:when test="${not empty places}">
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="2%">#</th>
                    <th width="98%">Local no Google</th>
                </tr>
            </thead>
            <c:forEach items="${places}" var="place" varStatus="count">
                <tr>
                    <td width="2%" style="text-align: center;">
                      <p>
                        ${count.count + (limit * (paginaAtual - 1))}
                      </p>
                      <p>
                        <a href="#" class="btn btn-success botao-associar-google" data-local-id="${local.id}" data-google-id="${place.id}" data-google-reference="${place.reference}">
                            <i class="icon-chevron-left icon-white"></i> Associar ID do Google
                        </a>
                      </p>
                      <p>
                        <%--a id="botao-associar-copiar-google" href="#" class="btn btn-primary" >
                            <i class="icon-chevron-left icon-white"></i> Associar e copiar dados
                        </a--%>
                      </p>
                    </td>
                    <td width="98%">
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Nome: <strong>${place.name}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${place.name != local.nome}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Nome'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Endere�o: <strong>${place.formattedAddress}</strong> 
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${not empty place.location.address and place.location.address != local.endereco.descricao}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Endere�o'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                CEP: <strong>${place.address.postalCode}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${not empty place.location.postalCode and place.location.postalCode != local.endereco.cep}">
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'CEP'
                                </a>
                              </c:if--%>
                            </div>
                        </div>
                        <div>
                          <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Latitude: <strong>${place.latitude}</strong>
                                Longitude: <strong>${place.longitude}</strong>
                            </div>
                            <div style="width: 50%; float: left;">
                              <%--c:if test="${place.location.latitude != local.latitude or place.location.longitude != local.longitude}">
                                <a href="#" class="btn btn-primary btn-large btn-copiar" >
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'Lat/Lng'
                                </a>
                              </c:if--%>
                            </div>
                          </div>
                        </div>
                        <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                            <div style="width: 50%; float: left;">
                                Categoria(s): 
                                <strong>
                                  <c:forEach items="${place.types}" var="type">
                                    ${type}, 
                                  </c:forEach>
                                </strong>
                            </div>
                            <div style="width: 50%; float: left;">
                            </div>
                        </div>
                        
                    </td>
                </tr>
            </c:forEach>
        </table>    
    </c:when>
    <c:otherwise>
        <div class="horizontalSpacer"></div>
        <div class="centerAligned"><h4>N�o foram encontrados locais semelhantes no Google.</h4></div>
    </c:otherwise>
</c:choose>

</div>