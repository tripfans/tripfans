<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:choose>
    <c:when test="${not empty localGoogle}">
    
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="98%">Dados do Google</th>
                </tr>
            </thead>
            <tr>
                <td width="98%">
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Nome: <strong><span id="google-nome-${local.id}">${localGoogle.name}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${localGoogle.name != local.nome}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="nome-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Nome'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Endere�o completo: <strong><span id="google-endereco-compl-${local.id}">${localGoogle.formattedAddress}</span></strong> 
                        </div>
                        <div style="width: 50%; float: left;">
                          <%--c:if test="${not empty localGoogle.formattedAddress and localGoogle.formattedAddress != local.endereco.descricao}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="endereco-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Endere�o'
                            </a>
                          </c:if--%>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Endere�o: <strong><span id="google-endereco-${local.id}">${localGoogle.route}${not empty localGoogle.streetNumber ? ', ' : ''}${localGoogle.streetNumber}</span></strong> 
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.formattedAddress and localGoogle.formattedAddress != local.endereco.descricao}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="endereco-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Endere�o'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">Bairro: <strong><span id="google-bairro-${local.id}">${localGoogle.sublocality}</span></strong> 
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.sublocality and localGoogle.sublocality != local.endereco.bairro}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="bairro-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Bairro'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Outras infos endere�o: 
                            <strong><span>
                                ${localGoogle.administrativeAreaLevel1}
                                <br/>
                                ${localGoogle.administrativeAreaLevel2}
                            </span></strong> 
                        </div>
                        <div style="width: 50%; float: left;">
                        </div>
                    </div>
                    
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            CEP: <strong><span id="google-cep-${local.id}">${localGoogle.postalCode}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.postalCode and localGoogle.postalCode != local.endereco.cep}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="cep-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'CEP'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    <div>
                      <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Latitude: <strong><span id="google-lat-${local.id}">${localGoogle.latitude}</span></strong><br/>
                            Longitude: <strong><span id="google-lon-${local.id}">${localGoogle.longitude}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${localGoogle.latitude != local.latitude or localGoogle.longitude != local.longitude}">
                            <a href="#" class="btn btn-primary btn-large btn-copiar-lat-lon" data-site-origem="google" data-local-id="${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Lat/Lng'
                            </a>
                          </c:if>
                        </div>
                      </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Telefone Internacional: <strong><span id="google-tel-${local.id}">${localGoogle.internationalPhoneNumber}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.internationalPhoneNumber and localGoogle.internationalPhoneNumber != local.telefone}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-origem="google-tel-${local.id}" data-input-destino="tel-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Telefone Inter.'
                            </a>
                          </c:if>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Telefone Formatado: <strong><span id="google-telf-${local.id}">${localGoogle.formattedPhoneNumber}</span></strong>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.formattedPhoneNumber and localGoogle.formattedPhoneNumber != local.telefone}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-origem="google-telf-${local.id}" data-input-destino="tel-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Telefone Form.'
                            </a>
                          </c:if>
                        </div>
                    </div>
                    
                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 50%; float: left;">
                            Website: <strong><span id="google-site-${local.id}" style="word-wrap: break-word;">${localGoogle.website}</span></strong><br/>
                        </div>
                        <div style="width: 50%; float: left;">
                          <c:if test="${not empty localGoogle.website and localGoogle.website != local.site}">
                            <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-destino="site-${local.id}">
                                <i class="icon-chevron-left icon-white"></i> Copiar 'Site'
                            </a>
                          </c:if>
                        </div>
                    </div>

                    <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <div style="width: 20%; float: left;">
                            Categoria(s): 
                        </div>
                        <div style="width: 50%; float: left;">
                            <c:forEach items="${localGoogle.listOfTypes}" var="nomeCategoria" varStatus="status">
                                <strong><span id="google-categoria-${status.index}-${local.id}">${nomeCategoria}</span></strong>
                                <a href="#" class="btn btn-primary btn-mini btn-copiar" data-site-origem="google" data-input-origem="google-categoria-${status.index}-${local.id}" data-input-destino="outras-categorias-${local.id}" data-replace="false">
                                    <i class="icon-chevron-left icon-white"></i> Copiar 'categoria'
                                </a>
                            </c:forEach>                        
                        </div>
                    </div>

                    <div>
                      <div class="row" style="margin-left: 0px; padding-bottom: 6px;">
                        <c:if test="${not empty local.idFoursquare}">
                          <p align="center">
                            <a href="#" class="btn btn-warning btn-comparar-mapas" data-local-id="${local.id}">
                                Comparar mapas
                            </a>
                            <div class="span5" id="mapa-google-${local.id}" style="border: 1px solid #CCC; height: 150px; margin-top: 15px; display: none;" data-latitude="${localGoogle.latitude}" data-longitude="${localGoogle.longitude}">
                          </p>
                        </c:if>
                      </div>
                    </div>
                    
                </td>
            </tr>
        </table>    
    </c:when>
    <c:otherwise>
        <div class="horizontalSpacer"></div>
        <div class="centerAligned"><h4>N�o foram encontrados locais semelhantes no Google.</h4></div>
    </c:otherwise>
</c:choose>