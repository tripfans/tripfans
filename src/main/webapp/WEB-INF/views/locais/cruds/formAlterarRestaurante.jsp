<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="TripFans - ${restaurante.nome}" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
			    $("#restauranteForm").initToolTips();


			    $("#estado_nome").autocompleteFanaticos({
					source: "${pageContext.request.contextPath}/locais/cruds/consultarEstadosPorNome",
					hiddenName : 'estado',
				    valueField: 'id',
				    labelField: 'nome',
					minLength: 3
				});

			    $("#cidade_nome").autocompleteFanaticos({
					source: function( request, response ) {
			    	jQuery.getJSON(
							'${pageContext.request.contextPath}/locais/cruds/consultarCidadesPorNome',
							 {
								term: request.term,
								estado: $("#estado").val()
							},
							function( data ) {
								response(data);
							}
						);
					},
					hiddenName : 'cidade',
				    valueField: 'id',
				    labelField: 'nomeComEstadoPais',
					minLength: 3
				});
			    $("#restauranteForm").validate({
			        rules: {
			            // anoVisita: "required",
			            email: {
			            	required: false,
			            	email: true
			            }
			            ,site: {
			            	required: false,
			            	url: true
			            }
			        },
			        messages: { 
			        	email: "<s:message code="validation.formAlterarRestaurante.email.valid" />",
			        	site: "<s:message code="validation.formAlterarRestaurante.site.valid" />",
			            descricao: {
			            	required: "<s:message code="validation.avaliacao.descricao.required" />",
			            	minlength: "<s:message code="validation.avaliacao.descricao.minlength" />"
			            }
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.clearfix")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
			<div class="span-12">
			<h2 class="centerAligned">${restaurante.nome} - ${restaurante.cidade.nome} - ${restaurante.estado.nome}</h2>
			
			<form:form modelAttribute="restaurante" id="restauranteForm" action="${pageContext.request.contextPath}/locais/cruds/alterarRestaurante" method="post" enctype="multipart/form-data">
				<s:hasBindErrors name="restaurante">
					<div id="errors" class="error centerAligned"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
					</div>
				</s:hasBindErrors>
				<div id="errors" class="error centerAligned" style="display: none;"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
				</div>
				
				<input type="hidden" name="restaurante" value="${restaurante.id}" />
				
				<fieldset>
				<legend>
				Dados Cadastrais
				</legend>

				<div>
				<label for="nome" class="large" >Nome</label><br/>
				<form:input id="nome" path="nome" disabled="true" readonly="true" />
				</div>
				
				<div>
				<label for="textarea" class="large">Texto descritivo do Restaurante</label><br/>
				<form:textarea id="textarea" cssClass="span10" path="descricao" cssStyle="height: 120px;" title="Descrição turística da restaurante. Você pode usar como fonte de informação os sites Wikitravel, Wikipedia, Ministério do Turismo, Fóruns na internet, na página do Restaurante, etc"></form:textarea>
				</div>
				
				</fieldset>
				
				<fieldset>
				<legend>
				Contato
				</legend>
				
				<div>
				<label for="email" class="large" >Email</label><br/>
				<form:input id="email" path="email" />
				</div>
				
				<div>
				<label for="site" class="large" >Site</label><br/>
				<form:input id="site" path="site" /> Exemplo: http://www.restaurante.com.br
				</div>
				
				<div>
				<label for="telefone" class="large" >Telefone</label><br/>
				<form:input id="telefone" path="telefone" />
				</div>
				
				</fieldset>

				<fieldset>
				<legend>
				Endereço
				</legend>
				
				<div>
				<label for="estado" class="large" >Estado</label><br/>
				<form:input id="estado_nome" path="estado.nome" />
				<%--form:hidden id="estado" path="estado.id" /--%>
				</div>

				<div>
				<label for="cidade" class="large" >Cidade</label><br/>
				<form:input id="cidade_nome" path="cidade.nome" />
				<%--form:hidden id="cidade" path="cidade.id" /--%>
				</div>
				
				<div>
				<label for="endereco" class="large" >Endereço</label><br/>
				<form:input id="endereco.descricao" path="endereco.descricao" />
				</div>

				<div>
				<label for="cep" class="large" >CEP</label><br/>
				<form:input id="endereco.cep" path="endereco.cep" />
				</div>
				
				</fieldset>
                
                <fieldset>
                    <legend>Fotos</legend>
                    <c:set var="fotoPrincipal" value="${restaurante.fotoPadrao}"/>
                    <c:set var="urlFotoPrincipal" value="${restaurante.urlFotoAlbum}"/>
                    <%@include file="camposFotoPrincipal.jsp" %>
                    
                    <%@include file="camposIncluirFotos.jsp" %>
                </fieldset>
				
				<div class="centerAligned">
				<input type="submit" id="botaoSalvar" class="btn btn-primary" value="Salvar" >
				</div>
			</form:form>
			
			</div>
          </div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>