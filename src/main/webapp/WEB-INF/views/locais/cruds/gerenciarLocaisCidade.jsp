<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Gerenciamento Geral de Locais de uma cidade - TripFans" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">

		function filtrarLocais() {
			if(!$('input[name="cidade"]').val()) {
				alert('Escolha uma cidade.');
			 } else {
			    jQuery.get(
		            '${pageContext.request.contextPath}/locais/cruds/listaTodosLocais',
		            {
		            	start:0,
		            	cidade: $('input[name="cidade"]').val()
		            }, function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
			}	
		}

		function selecionaAtracao(event, ui) {
			var url = '${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoAtracao/' + ui.item.id;
			window.open(url);
		}
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
	<form:form id="destinosForm">
 							
			<div class="span20">
				<div class="page-header"><h2>Gerenciamento Geral de Locais de uma Cidade</h2></div>
				<div class="control-group">
				<label>Escolha a cidade</label>
				<div class="pesquisa">
	               <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeLocal" hiddenName="cidade" id="nomeCidade" 
	                     valueField="id" labelField="nomeExibicao" cssClass="span7" 
	                     value="${cidade.nome}"
	                     hiddenValue="${cidade.id}"
	                     placeholder="Informe a Cidade..." 
	                     blockOnSelect="false"/>
	               	<input type="button" value="Pesquisar Locais" class="btn"  onclick="filtrarLocais()" />
	            </div>
				</div>
				
				<div id="listaPaginada">
					<c:if test="${exibirLocais != null}">
						<%@include file="listaTodosLocaisCidade.jsp" %>
					</c:if>
				</div>
			</div>
			
			
		</form:form>
		
		
	</tiles:putAttribute>

</tiles:insertDefinition>