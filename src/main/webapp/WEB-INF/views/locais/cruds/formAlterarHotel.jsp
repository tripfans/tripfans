<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="TripFans - ${hotel.nome}" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
			    $("#hotelForm").initToolTips();


			    $("#estado_nome").autocompleteFanaticos({
					source: "${pageContext.request.contextPath}/locais/cruds/consultarEstadosPorNome",
					hiddenName : 'estado',
				    valueField: 'id',
				    labelField: 'nome',
					minLength: 3
				});

			    $("#cidade_nome").autocompleteFanaticos({
					source: function( request, response ) {
			    	jQuery.getJSON(
							'${pageContext.request.contextPath}/locais/cruds/consultarCidadesPorNome',
							 {
								term: request.term,
								estado: $("#estado").val()
							},
							function( data ) {
								response(data);
							}
						);
					},
					hiddenName : 'cidade',
				    valueField: 'id',
				    labelField: 'nomeComEstadoPais',
					minLength: 3
				});
			    $("#hotelForm").validate({
			        rules: {
			            // anoVisita: "required",
			            email: {
			            	required: false,
			            	email: true
			            }
			            ,site: {
			            	required: false,
			            	url: true
			            }
			        },
			        messages: { 
			        	email: "<s:message code="validation.formAlterarHotel.email.valid" />",
			        	site: "<s:message code="validation.formAlterarHotel.site.valid" />",
			            descricao: {
			            	required: "<s:message code="validation.avaliacao.descricao.required" />",
			            	minlength: "<s:message code="validation.avaliacao.descricao.minlength" />"
			            }
			        },
			        errorPlacement: function(error, element) {
			        	var parent = element.parents("div.clearfix")[0];
			        	error.appendTo(parent);
			        },
			        errorContainer: "#errorBox"
			    });
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
			<div class="span12">
			<h2 class="centerAligned">${hotel.nome} - ${hotel.cidade.nome} - ${hotel.estado.nome}</h2>
			
			<form:form modelAttribute="hotel" id="hotelForm" action="${pageContext.request.contextPath}/locais/cruds/alterarHotel" method="post" enctype="multipart/form-data">
				<s:hasBindErrors name="hotel">
					<div id="errors" class="error centerAligned"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
					</div>
				</s:hasBindErrors>
				<div id="errors" class="error centerAligned" style="display: none;"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
				</div>
				
				<input type="hidden" name="hotel" value="${hotel.id}" />
				
				<fieldset>
    				<legend>
    				Dados Cadastrais
    				</legend>
    
                    <div class="control-group">
        				<label for="nome" class="large" >Nome</label><br/>
        				<form:input id="nome" path="nome" cssClass="span7"/>
    				</div>

                    <div class="control-group">
                       <label>
                           <span class="required">URL Path</span>
                           <i id="help-nome" class="icon-question-sign helper" title="Esse é o caminho para acessar o local na aplicação. Exemplo: tripfans.com.br/${cidade.urlPath}"></i>
                       </label>
                       <div class="controls">
                           <form:input path="urlPath" id="urlPath" cssClass="span7" />
                       </div>
                    </div>
    				
                    <div class="control-group">
        				<label for="estrelas" class="large" >Número de Estrelas</label><br/>
                        <div class="controls">
        				    <form:input id="estrelas" path="estrelas" cssClass="span1" maxlength="1"/>
                        </div>
    				</div>
    
                    <div class="control-group">
        				<label for="numeroQuartos" class="large" >Número de Quartos</label><br/>
                        <div class="controls">
        				    <form:input id="numeroQuartos" path="numeroQuartos" cssClass="span2" maxlength="6" />
                        </div>
    				</div>
    				
                    <div class="control-group">
                        <label for="latitude">Latitude</label>
                        <form:input id="coordenadaGeografica.latitude" path="coordenadaGeografica.latitude" />
                    </div>
                    
                    <div class="control-group">
                        <label for="longitude">Longitude</label>
                        <form:input id="coordenadaGeografica.longitude" path="coordenadaGeografica.longitude" />
                    </div>
                    
                    <div class="control-group">
                        <label for="idFacebook">Identificador do Facebook
                            <i class="icon-question-sign helper" title="Identificador desse local no Facebook (exemplo: facebook.com/108142565873758)"></i>
                        </label>
                        <form:input id="idFacebook" path="idFacebook" />
                    </div>
                    
                    <div class="control-group">
                        <label for="idFoursquare">Identificador do FourSquare
                            <i class="icon-question-sign helper" title="Identificador desse local no FourSquare (exemplo: foursquare.com/v/veredas-grill/4baa4086f964a520ce573ae3)"></i>
                        </label>
                        <form:input id="idFoursquare" path="idFoursquare" />
                    </div>
                    
                    <div class="control-group">
                        <label for="textarea" >
                            Texto descritivo do Hotel
                            <i class="icon-question-sign helper" title="Descrição turística da hotel. Você pode usar como fonte de informação os sites Wikitravel, Wikipedia, Ministério do Turismo, Fóruns na internet, na página do Hotel, etc"></i>
                        </label>
                        <form:textarea cssClass="span10" cssStyle="height: 120px;" id="textarea" path="descricao" ></form:textarea>
                    </div>
				</fieldset>
				
				<fieldset>
    				<legend>
    				Contato
    				</legend>
    				
    				<div>
        				<label for="email" class="large" >Email</label><br/>
        				<form:input id="email" path="email" cssClass="span5" maxlength="150" />
    				</div>
    				
    				<div>
        				<label for="site" class="large" >Site</label><br/>
        				<form:input id="site" path="site" cssClass="span5" maxlength="255" /> Exemplo: http://www.hotel.com.br
    				</div>
    				
    				<div>
        				<label for="telefone" class="large" >Telefone</label><br/>
        				<form:input id="telefone" path="telefone" cssClass="span3" />
    				</div>
				</fieldset>

				<fieldset>
    				<legend>
    				Endereço
    				</legend>
    				
                    <div>
                        <label for="endereco" class="large" >Endereço</label><br/>
                        <form:input id="endereco.descricao" path="endereco.descricao" cssClass="span6" />
                    </div>

                    <div>
                    <label for="endereco" class="large" >Bairro</label><br/>
                        <form:input id="endereco.bairro" path="endereco.bairro" cssClass="span4" />
                    </div>
    
                    <div>
                        <label for="cep" class="large" >CEP</label><br/>
                        <form:input id="endereco.cep" path="endereco.cep" cssClass="span3" />
                    </div>

    				<div>
        				<label for="estado" class="large" >Estado</label><br/>
        				<form:input id="estado_nome" path="estado.nome" cssClass="span2" disabled="true" />
        				<%--form:hidden id="estado" path="estado.id" /--%>
    				</div>
    
    				<div>
        				<label for="cidade" class="large" >Cidade</label><br/>
        				<form:input id="cidade_nome" path="cidade.nome" cssClass="span2" disabled="true" />
        				<%--form:hidden id="cidade" path="cidade.id" /--%>
    				</div>
    				
				</fieldset>
                
                <fieldset>
                    <legend>Fotos</legend>
                    <c:set var="fotoPrincipal" value="${hotel.fotoPadrao}"/>
                    <c:set var="urlFotoPrincipal" value="${hotel.urlFotoAlbum}"/>
                    <%@include file="camposFotoPrincipal.jsp" %>
                    
                    <%@include file="camposIncluirFotos.jsp" %>
                </fieldset>
				
				<div class="centerAligned form-actions">
				    <input type="submit" id="botaoSalvar" class="btn btn-primary" value="Salvar" >
				</div>
			</form:form>
			
			</div>
          </div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>