<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Gerenciamento de Restaurantes - TripFans" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		jQuery(document).ready(function() {
			$(".linkPopup").click(function(event){
				event.preventDefault();
				openPopupWindow(event.originalEvent.currentTarget,'formEdit',1000,600);
			});
		});
		
		$(".paginacao").click(function(event){
            var url = $(this).attr('href');
            jQuery.get(
                    url,
                    function(response) {
                        $('#listaPaginada').html(response);
                    }
                );
        });
		
		function filtraRestaurantes() {
            if(!$('input[name="cidade"]').val()) {
                alert('Escolha uma cidade.');
             } else {
                jQuery.get(
                    '${pageContext.request.contextPath}/locais/cruds/listaRestaurantes',
                    {
                        start:0,
                        cidade: $('input[name="cidade"]').val()
                    }, function(response) {
                        $('#listaPaginada').html(response);
                    }
                );
            }   
        }

        function selecionaCidade(event, ui) {
            var id = ui.item.id;
            $('input[name="cidade"]').val(id);
        }
        
        function selecionaRestaurante(event, ui) {
            var url = '${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoRestaurante/' + ui.item.id;
            window.open(url);
        }
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
            <form:form id="destinosForm">
              <div class="span19">
                <div class="page-header"><h2>Gerenciamento de Restaurantes</h2></div>
                <div class="control-group">
                <label>Escolha a cidade</label>
                <div class="pesquisa">
                   <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeCidade" hiddenName="cidade" id="nomeCidade" 
                         valueField="id" labelField="nomeExibicao" onSelect="selecionaCidade()" cssClass="span7" 
                         value="${cidade.nome}"
                         hiddenValue="${cidade.id}"
                         jsonFields="[{name : 'id'}, {name : 'nomeExibicao'}]"
                         placeholder="Informe a Cidade..." 
                         blockOnSelect="false"/>
                    <input type="button" value="Pesquisar Restaurantes" class="btn" onclick="filtraRestaurantes()" />
                </div>
                </div>
                
                <div class="control-group">
                <label>Ou escolha diretamente o Restaurante</label>
                <div class="pesquisa">
                   <fan:autoComplete url="/pesquisaTextual/consultarRestaurantes" name="nomeLocal" hiddenName="restaurante" id="nomeRestaurante" 
                         valueField="id" onSelect="selecionaRestaurante()" labelField="nomeExibicao"  cssClass="span7" 
                         jsonFields="[{name : 'id'}, {name : 'nome'}]"
                         placeholder="Informe a Restaurante..." 
                         blockOnSelect="false"/>
                </div>
                </div>
                <div id="listaPaginada">
                    <c:if test="${restaurantes != null}">
                        <%@include file="listaRestaurantesPaginada.jsp" %>
                    </c:if>
                </div>
              </div>
            </form:form>
 							
			<%--div class="span-24">
			<h1>Lista de Restaurantes</h1>
			<c:choose>
				<c:when test="${not empty restaurantes}">
				
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
					
					<table  style="border: 1px solid #C3C3C3;">
					<thead>
						<tr>
						<th width="2%">#</th>
						<th width="20%">Nome</th>
						<th width="2%">Endereço</th>
						<th width="2%">Contato</th>
						<th>Descricao</th>
						<th width="15%">URL-Path</th>
						<th width="12%">Ações</th>
						</tr>
					</thead>
					<c:forEach items="${restaurantes}" var="restaurante" varStatus="count">
					<tr>
						<td width="2%">${count.count + (30 * (paginaAtual - 1))}</td>
						<td width="20%"><a href="${pageContext.request.contextPath}/locais/restaurante/${restaurante.urlPath}" target="_blank">${restaurante.nome}</a></td>
						<td width="20%">
						<strong>Endereço:</strong> ${restaurante.endereco.descricao}
						<br/><strong>CEP:</strong> ${restaurante.endereco.cep}
						<br/><strong>Cidade:</strong> ${restaurante.cidade.nome}
						<br/><strong>Estado:</strong> ${restaurante.estado.nome}  
						</td>
						<td width="20%">
						<c:if test="${not empty restaurante.email}">
						<strong>Email:</strong> ${restaurante.email} <br/>
						</c:if>
						<c:if test="${not empty restaurante.site}">
						<strong>Site:</strong> <a href="${restaurante.site}" target="_blank">${restaurante.site}</a> <br/>
						</c:if>
						<c:if test="${not empty restaurante.telefone}">
						<strong>Telefone:</strong> ${restaurante.telefone}
						</c:if>
						</td>
						<td>${restaurante.descricao}</td>
						<td width="15%">${restaurante.urlPath}</td>
						<td width="12%">
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoRestaurante/${restaurante.id}" class="linkPopup">Editar</a><br/>
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAdicaoFotosLocal?local=${restaurante.id}" class="linkPopup">Adicionar Fotos</a><br/>
						<a href="${pageContext.request.contextPath}/locais/restaurante/${restaurante.urlPath}" target="_blank">Preview</a>
						</td>
					</tr>
					</c:forEach>
					</table>
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
				</c:when>
				<c:otherwise>
					<h4>Não existem restaurantes.</h4>
				</c:otherwise>
			</c:choose--%>
		  </div>
        </div>
		
	</tiles:putAttribute>

</tiles:insertDefinition>