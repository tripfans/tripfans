<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<fieldset>
 <legend>Adicione outras fotos</legend>
 <c:forEach  var="i" begin="0" end="9" step="1">
 	<p>
 	<div class="control-group" style="max-width: 400px;">
		<label for="fotos${i}.arquivoFoto" class="large">Foto ${i+1}</label>
		<form:errors path="fotos[${i}].arquivoFoto" cssClass="error" element="label" />
		<input id="fotos${i}.arquivoFoto" name="fotos[${i}].arquivoFoto" type="file"  title="Escolha uma foto" onchange="enableFields('descricaoFoto_${i}','fonte_${i}', 'url_fonte_${i}', 'autor_fonte_${i}');">
	</div>
	<div class="control-group">
		<label for="descricaoFoto_${i}" class="large">Descrição da foto</label>
		<form:input cssStyle="width: 400px;" disabled="true" id="descricaoFoto_${i}" path="fotos[${i}].descricao" title="Adicione um título/descrição para as fotos" />
	</div>
	<div class="control-group">
		<label for="fonte_${i}" class="large">Origem da Foto</label>
		<form:input cssStyle="width: 400px;" disabled="true" id="fonte_${i}" path="fotos[${i}].fonteImagem" title="Coloque a fonte original da foto. Exemplos: Prefeitura do Rio de Janeiro, Wikitravel, Embratur" />
	</div>
    
    <div class="control-group">
        <label for="url_fonte_Padrao" >URL da origem da Foto</label> 
        <form:input cssStyle="width: 400px;" disabled="true" id="url_fonte_${i}" path="fotos[${i}].urlFonteImagem" title="Coloque a URL da fonte original da foto. Exemplos: www.embratur.gov.br, http://500px.com/photo/48537232" />
    </div>
    <div class="control-group">
        <label for="autor_fonte_padrao" >Autor da foto na origem</label> 
        <form:input cssStyle="width: 400px;" disabled="true" id="autor_fonte_${i}" path="fotos[${i}].autorFonteImagem" title="Coloque o nome do Autor/Usuario que possui o crédito/autoria da foto. Exemplos: Nicko McBrain, Cristina Wilson, etc" />
    </div>
    
	</p>
 </c:forEach>
</fieldset>
				
