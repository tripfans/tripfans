<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Crud de Locais - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		function showPopup() {
			$( "#dialog-confirm" ).dialog({
				resizable: false,
				modal: true,
				buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}
				}
			});
		}
		
		function configuraEventoClickx(divFiltro, event) {
			event.preventDefault();
			$(".filtrosLocais").attr("style", "display: none");
			var div = $("#"+divFiltro);
			div.attr("style", "display: block");
			div.appendTo($(event.originalEvent.target).parent());
			var button = $("#"+divFiltro+" > button")[0];
			var url = event.target;
			$(button).click(function(event) {
				var value =  $(div.find(":hidden")[0]).val();
				if(value !== null && value !== '') {
					window.open(url+value);
				} else {
					showPopup();
				}
			});
		}
		
		jQuery(document).ready(function() {
			$("#cidades").autocomplete({
				source: "${pageContext.request.contextPath}/locais/cruds/consultarCidadesPorNome",
				minLength: 3,
				 select: function(event, ui) { 
					 $("#cidadesHidden").val(ui.item.id);
				 }
			});
			
			$("#estados").autocomplete({
				source: "${pageContext.request.contextPath}/locais/cruds/consultarEstadosPorNome",
				minLength: 3,
				 select: function(event, ui) { 
					 $("#estadosHidden").val(ui.item.id);
				 }
			});
			
			$("#paises").autocomplete({
				source: "${pageContext.request.contextPath}/locais/cruds/consultarPaisesPorNome",
				minLength: 3,
				 select: function(event, ui) { 
				     $("#paisesHidden").val(ui.item.id); 
				 }
			});
			
			$("#locais").autocomplete({
				source: "${pageContext.request.contextPath}/locais/cruds/consultarLocaisPorNome",
				minLength: 3,
				position: {
					my: "right bottom",
				    at: "right top"
				},
				 select: function(event, ui) { 
				     $("#locaisHidden").val(ui.item.id); 
				 }
			});
			
			//$("#cidadeListaPorEstado").click(function(event){
			//	configuraEventoClickx("filtroEstado", event);
			//});
			
			$("#cidadeListaPorPais").click(function(event){
				configuraEventoClickx("filtroPais", event);
			});
			
			$("#estadoLista").click(function(event){
				configuraEventoClickx("filtroPais", event);
			});
			
			$("#editarCidade").click(function(event){
				configuraEventoClickx("filtroCidade", event);
			});
			
			$("#adicionarFotos").click(function(event){
				configuraEventoClickx("filtroLocal", event);
			});
		});
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
      <div class="container">
        <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
          
    		<div id="dialog-confirm" title="Atenção" style="display: none;">
    			<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Selecione um valor no campo ao lado</p>
    		</div>
	
			<div class="span20">
			<h1>Escolha uma opção a seguir</h1>
			<div class="horizontalSpacer"></div>
			<div class="row">
				<div class="offset1">
				<a href="#" class="btn btn-large btn-primary span5 disabled">Gerenciar Países</a>
				<a href="#" class="btn btn-large btn-primary span5 disabled">Gerenciar Estados</a>
				<a href="<c:url value="/locais/cruds/listaCidades" />" class="btn btn-large btn-primary span5" target="_blank">Gerenciar Cidades</a>
				</div>
			</div>
			<div class="horizontalSpacer"></div>
			<div class="row">
				<div class="offset1">
				<a href="<c:url value="/locais/cruds/listaAtracoes" />" class="btn btn-large btn-primary span5" target="_blank">Gerenciar Atrações</a>
				<a href="<c:url value="/locais/cruds/listaHoteis" />" class="btn btn-large btn-primary span5" target="_blank">Gerenciar Hotéis</a>
				<a href="<c:url value="/locais/cruds/listaRestaurantes" />" class="btn btn-large btn-primary span5" target="_blank">Gerenciar Restaurantes</a>
				</div>
			</div>
			<div class="horizontalSpacer"></div>
			<div class="row">
				<div class="offset1">
					<a href="<c:url value="/locais/cruds/exibirAdicaoFotosLocal" />" class="btn btn-large btn-primary span5 disabled" id="adicionarFotos">Adicionar Fotos a um local</a>
				</div>
			</div>
			
			<ul class="hidden">
				<p>
				<li><a>Listar Continentes</a> - Pendente</li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaPaises" target="_blank">Listar Países</a></li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaEstados/" id="estadoLista">Listar Estados</a></li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaCidades" id="cidadeLista">Listar Cidades - Em execução</a></li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaCidadesPorPais/" id="cidadeListaPorPais">Listar Cidades Por Pais</a></li>
				</p>
				<p>
				<li><a href="#">Editar País</a> - Pendente</li>
				<li><a href="#">Editar Estado</a> - Pendente</li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoCidade/" id="editarCidade">Editar Cidade</a></li>
				</p>
				<p>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaHoteis/" id="hotelLista">Listar Hotéis</a></li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaRestaurantes/" id="restauranteLista">Listar Restaurantes</a></li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/listaAtracoes/" id="atracaoLista">Listar Atrações</a></li>
				</p>
				<p>
				<li><a href="#">Editar Hotel</a> - Pendente</li>
				<li><a href="#">Editar Restaurante</a> - Pendente</li>
				<li><a href="#">Editar Atração</a> - Pendente</li>
				</p>
				<p>
				<li><a href="#">Adicionar Hotel</a> - Pendente</li>
				<li><a href="#">Adicionar Restaurante</a> - Pendente</li>
				<li><a href="#">Adicionar Atração</a> - Pendente</li>
				<li><a href="${pageContext.request.contextPath}/locais/cruds/exibirAdicaoFotosLocal?local=" id="adicionarFotos">Adicionar Fotos a um local</a></li>
				</p>
			</ul>
			</div>
			
			<div id="filtroPais" style="display: none;" class="filtrosLocais">
				<label for="paises">Escolha o País:</label>
				<fan:autoComplete url="/locais/cruds/consultarPaisesPorNome" name="nomeLocal" hiddenName="paisesHidden" id="paises" 
		                valueField="id" labelField="nome" cssClass="span5" 
		                hiddenValue="${idPais}"
		                showActions="false" blockOnSelect="false" placeholder="Informe o país..." />
				<button>Vai</button>
			</div>
			
			<div id="filtroEstado" style="display: none;" class="filtrosLocais">
				<label for="estados">Escolha o estado:</label>
				<input id="estadosHidden" type="hidden">
				<input id="estados" type="text">
				<button>Vai</button>
			</div>
			
			<div id="filtroCidade" style="display: none;" class="filtrosLocais">
				<label for="cidades">Escolha a cidade:</label>
				<input id="cidadesHidden" type="hidden">
				<input id="cidades" type="text">
				<button>Vai</button>
			</div>
			
			<div id="filtroHotel" style="display: none;" class="filtrosLocais"></div>
			
			<div id="filtroRestaurante" style="display: none;" class="filtrosLocais"></div>
			
			<div id="filtroAtracao" style="display: none;" class="filtrosLocais"></div>
			
			<div id="filtroLocal" style="display: none;" class="filtrosLocais">
				<label for="locais">Escolha o local:</label>
				<input id="locaisHidden" type="hidden">
				<input id="locais" type="text">
				<button>Vai</button>
			</div>
        </div>
      </div>
	</tiles:putAttribute>

</tiles:insertDefinition>