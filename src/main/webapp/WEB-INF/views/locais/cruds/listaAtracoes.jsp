<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Gerenciamento de Atrações - TripFans" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		jQuery(document).ready(function() {
			$(".linkPopup").click(function(event){
				event.preventDefault();
				openPopupWindow(event.originalEvent.currentTarget,'formEdit',1000,600);
			});
		});
		$(".paginacao").click(function(event){
			var url = $(this).attr('href');
			jQuery.get(
		            url,
		            function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
		});
// 		Para montar a url da paginação{
// 			class='x' href=--------------------?${start}&${pais}
// 			$('#start').val()
// 			faz a function
// 			$('.x').click(function(){
// 				var url = $(this).attr('href')
// 				jQuery.get(
// 			            url,
// 			             function(response) {
// 			            	$('#listaPaginada').html(response);
// 			            }
// 			        );
// 			}
// 					)
// 		}
		function filtraAtracoes() {
			if(!$('input[name="cidade"]').val()) {
				alert('Escolha uma cidade.');
			 } else {
			    jQuery.get(
		            '${pageContext.request.contextPath}/locais/cruds/listaAtracoes',
		            {
		            	start:0,
		            	cidade: $('input[name="cidade"]').val()
		            }, function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
			}	
		}

        function selecionaCidade(event, ui) {
            var id = ui.item.id;
            $('input[name="cidade"]').val(id);
        }
		
		function selecionaAtracao(event, ui) {
			var url = '${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoAtracao/' + ui.item.id;
			window.open(url);
		}
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
    
        <div class="container">
          <div class="container-fluid page-container" style="position: relative; min-height: 100%; padding: 0px 14px 15px; margin: 0 auto;">
    
        	<form:form id="destinosForm">
			  <div class="span19">
				<div class="page-header"><h2>Gerenciamento de Atrações</h2></div>
				<div class="control-group">
				<label>Escolha a cidade</label>
				<div class="pesquisa">
	               <fan:autoComplete url="/pesquisaTextual/consultarCidades" name="nomeCidade" hiddenName="cidade" id="nomeCidade" 
	                     valueField="id" labelField="nomeExibicao" onSelect="selecionaCidade()" cssClass="span7" 
	                     value="${cidade.nome}"
	                     hiddenValue="${cidade.id}"
                         jsonFields="[{name : 'id'}, {name : 'nomeExibicao'}]"
	                     placeholder="Informe a Cidade..." 
	                     blockOnSelect="false"/>
	               	<input type="button" value="Pesquisar Atrações" class="btn" onclick="filtraAtracoes()" />
	            </div>
				</div>
				
				<div class="control-group">
				<label>Ou escolha diretamente a Atração</label>
				<div class="pesquisa">
	               <fan:autoComplete url="/pesquisaTextual/consultarAtracoes" name="nomeLocal" hiddenName="atracao" id="nomeAtracao" 
	                     valueField="id" onSelect="selecionaAtracao()" labelField="nomeExibicao"  cssClass="span7" 
	                     jsonFields="[{name : 'id'}, {name : 'nome'}]"
	                     placeholder="Informe a Atração..." 
	                     blockOnSelect="false"/>
	            </div>
				</div>
				<div id="listaPaginada">
					<c:if test="${atracoes != null}">
						<%@include file="listaAtracoesPaginada.jsp" %>
					</c:if>
				</div>
			  </div>
		    </form:form>
        
        </div>
        
      </div>
		
	</tiles:putAttribute>

</tiles:insertDefinition>