<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<script type="text/javascript">
		$(".paginacao").click(function(event){
			event.preventDefault();
			var url = $(this).attr('href');
			jQuery.get(
		            url,
		            function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
		});
// 		Para montar a url da pagina��o{
// 			class='x' href=--------------------?${start}&${cidade}
// 			$('#start').val()
// 			faz a function
// 			$('.x').click(function(){
// 				var url = $(this).attr('href')
// 				jQuery.get(
// 			            url,
// 			             function(response) {
// 			            	$('#listaPaginada').html(response);
// 			            }
// 			        );
// 			}
// 					)
// 		}
		</script>
			<c:choose>
				<c:when test="${not empty hoteis}">
				
					<ul class="pager">
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=0&cidade=${cidade.id}">Primeira</a></li>
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${param.start - limit}&cidade=${cidade.id}">Anterior</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Primeira</a></li>
							<li class="disabled"><a href="#">Anterior</a></li>
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${param.start + limit}&cidade=${cidade.id}">Pr�xima</a></li>
							<li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${startUltimaPagina}&cidade=${cidade.id}">�ltima</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Pr�ximo</a></li>
							<li class="disabled"><a href="#">�ltima</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
				
					<div class="right">
						<strong>P�gina:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
					</div>
					<table class="table table-striped table-bordered span19" style="margin-left: 0;">
					<thead>
						<tr>
						<th width="2%">#</th>
						<th width="20%">Nome</th>
						<th>Descricao</th>
						<th width="15%">URL-Path</th>
						<th width="12%">A��es</th>
						</tr>
					</thead>
					<c:forEach items="${hoteis}" var="hotel" varStatus="count">
					<tr>
						<td width="2%">${count.count + (limit * (paginaAtual - 1))}</td>
						<td width="20%"><a href="${pageContext.request.contextPath}/locais/hotel/${hotel.urlPath}" target="_blank">${hotel.nome}</a></td>
						<td>${hotel.descricao}</td>
						<td width="15%">${hotel.urlPath}</td>
						<td width="12%">
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoHotel/${hotel.id}" target="_blank">Editar</a><br/>
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAdicaoFotosLocal?local=${hotel.id}" target="_blank">Adicionar Fotos</a><br/>
						<a href="${pageContext.request.contextPath}/locais/hotel/${hotel.urlPath}" target="_blank">Preview</a>
						</td>
					</tr>
					</c:forEach>
					</table>
					<ul class="pager">
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=0&cidade=${cidade.id}">Primeira</a></li>
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${param.start - limit}&cidade=${cidade.id}">Anterior</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Primeira</a></li>
							<li class="disabled"><a href="#">Anterior</a></li>
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${param.start + limit}&cidade=${cidade.id}">Pr�xima</a></li>
							<li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaHoteis?start=${startUltimaPagina}&cidade=${cidade.id}">�ltima</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Pr�ximo</a></li>
							<li class="disabled"><a href="#">�ltima</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
				</c:when>
				<c:otherwise>
					<div class="horizontalSpacer"></div>
					<div class="centerAligned"><h4>N�o existem hot�is para essa sele��o.</h4></div>
				</c:otherwise>
			</c:choose>