<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="Lista de Estados - TripFans" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="container">
 							
			<div class="span-24">
			<h1>Lista de Estados - ${pais.nome}</h1>
			<c:choose>
				<c:when test="${not empty estados}">
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
				
					<table  style="border: 1px solid #C3C3C3;">
					<thead>
						<tr>
						<th width="2%">#</th>
						<th width="15%">Nome</th>
						<th width="5%">Sigla</th>
						<th>Descricao</th>
						<th width="15%">URL</th>
						<th width="15%">Ações</th>
						</tr>
					</thead>
					<c:forEach items="${estados}" var="estado" varStatus="count">
					<tr>
						<td width="2%">${count.count + (30 * (paginaAtual - 1))}</td>
						<td width="15%"><a href ="#">${estado.nome}</a></td>
						<td width="5%">${estado.sigla}</td>
						<td>${estado.descricao}</td>
						<td width="15%">${estado.urlPath}</td>
						<td width="15%">
						<a href ="${pageContext.request.contextPath}/locais/cruds/listaCidades/${estado.id}">Cidades</a>&nbsp;&nbsp;
						<a href ="${pageContext.request.contextPath}/locais/cruds/salvarPais/${estado.id}">Editar</a></td>
					</tr>
					</c:forEach>
					</table>
					<div style="text-align: center;">
						
						<strong>Página:</strong> ${paginaAtual} / ${quantidadePaginas} &nbsp;&nbsp;
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=1"> &lt;&lt; Primeiro</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${paginaAtual - 1}">&lt; Anterior</a>&nbsp;&nbsp;
						</c:when>
						<c:otherwise>
							 &lt;&lt; Primeiro &lt; Anterior&nbsp;&nbsp;
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${paginaAtual + 1}">Próximo &gt;</a>&nbsp;&nbsp;
							<a href ="${pageContext.request.contextPath}/locais/cruds/listaEstados/${idEstado}?page=${quantidadePaginas}">Último &gt;&gt;</a>
						</c:when>
						<c:otherwise>
							Próximo &gt;&nbsp;&nbsp; Último &gt;&gt;
						</c:otherwise>
						</c:choose>
						
					</div>
				</c:when>
				<c:otherwise>
					<h4>Não existem estados nesse país.</h4>
				</c:otherwise>
			</c:choose>
			
					
			</div>
			
			
		</div>
		
		
	</tiles:putAttribute>

</tiles:insertDefinition>