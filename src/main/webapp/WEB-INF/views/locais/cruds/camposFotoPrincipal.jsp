<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<fieldset>
    <legend>Foto Principal</legend>
    
    <c:if test="${not empty fotoPrincipal and fotoPrincipal.tipoArmazenamentoLocal}">
        <c:set var="is500px" value="false"></c:set>
    </c:if> 
    <c:if test="${not empty fotoPrincipal and fotoPrincipal.foto500px}">
        <c:set var="is500px" value="true"></c:set>
    </c:if>
    
    <div class="control-group" style="max-width: 400px;">
        <input type="radio" name="fotoPadrao.tipoArmazenamento" id="radio-tipo-local" value="LOCAL" ${not is500px ? 'checked="checked"' : ''}>
            Usar foto Local
        </input>
        <input type="radio" name="fotoPadrao.tipoArmazenamento" id="radio-tipo-500px" value="EXTERNO" ${is500px ? 'checked="checked"' : ''}>
            Usar foto 500px
        </input>
        
        <input type="hidden" id="hidden-foto500px" name="fotoPadrao.foto500px" value="${fotoPrincipal.foto500px}" />
    </div>
    
    <div id="div-foto-local" style="${is500px ? 'display: none;' : ''}">
    
	 	<div class="control-group" style="max-width: 400px;">
			<label for="fotoPadrao.arquivoFoto">Foto Local</label>
            <c:if test="${not is500px}">
			  <img src="${urlFotoPrincipal}" class="profile moldura thumbnail" />
            </c:if>
			<form:errors path="fotoPadrao.arquivoFoto" cssClass="error" element="label" />
			<input class="span7 input-foto-local" id="fotoPadrao.arquivoFoto" name="fotoPadrao.arquivoFoto" type="file"  title="Escolha uma nova foto para ser a padrão desse local" 
                   onchange="enableFields('descricaoFotoPadrao','fonte_Padrao', 'url_fonte_Padrao', 'autor_fonte_padrao');">
		</div>
		<div class="control-group">
			<label for="descricaoFotoPadrao" >Descrição da foto</label>
			<form:input cssStyle="width: 400px;" disabled="true" id="descricaoFotoPadrao" path="fotoPadrao.descricao" 
                        cssClass="input-foto-local"
                        title="Adicione um título/descrição para as fotos" />
		</div>
		<div class="control-group">
			<label for="fonte_Padrao" >Origem da Foto</label> 
			<form:input cssStyle="width: 400px;" disabled="true" id="fonte_Padrao" path="fotoPadrao.fonteImagem" 
                        cssClass="input-foto-local"
                        title="Coloque a fonte original da foto. Exemplos: Prefeitura do Rio de Janeiro, Wikitravel, Embratur" />
		</div>
        <div class="control-group">
            <label for="url_fonte_Padrao" >URL da origem da Foto</label> 
            <form:input cssStyle="width: 400px;" disabled="true" id="url_fonte_Padrao" path="fotoPadrao.urlFonteImagem" 
                        cssClass="input-foto-local"
                        title="Coloque a URL da fonte original da foto. Exemplos: www.embratur.gov.br, http://500px.com/photo/48537232" />
        </div>
        <div class="control-group">
            <label for="autor_fonte_padrao" >Autor da foto na origem</label> 
            <form:input cssStyle="width: 400px;" disabled="true" id="autor_fonte_padrao" path="fotoPadrao.autorFonteImagem" 
                        cssClass="input-foto-local"
                        title="Coloque o nome do Autor/Usuario que possui o crédito/autoria da foto. Exemplos: Nicko McBrain, Cristina Wilson, etc" />
        </div>
    
    </div>
    
    <div id="div-foto-500px" style="${is500px ? '' : 'display: none;'}">
        <div class="control-group" style="max-width: 400px;">
            <label for="fotoPadrao.arquivoFoto">Foto 500px</label>
            <c:if test="${is500px}">
              <img src="${urlFotoPrincipal}" class="profile moldura thumbnail" />
            </c:if>
        </div>
        <div class="control-group">
            <label for="fonte_Padrao">Origem da Foto</label> 
            <input type="text" style="width: 400px;" disabled="true" name="fotoPadrao.fonteImagem" value="500px" />
            <input type="hidden" style="width: 400px;" name="fotoPadrao.fonteImagem" value="500px" class="input-foto-500px" />
        </div>
        <div class="control-group">
            <label for="url_fonte_Padrao" class="required">Id da Foto no 500px</label> 
            <form:input cssStyle="width: 400px;" path="fotoPadrao.idFoto500px"
                        title="Coloque o ID da foto no 500px. Exemplo: 48537232" />
        </div>
        <div class="control-group">
            <label for="url_fonte_Padrao" class="required">URL da imagem no 500px</label> 
            <form:input cssStyle="width: 400px;" id="url_fonte_Padrao" path="fotoPadrao.urlFoto500px" 
                        class="input-foto-500px" title="Coloque a URL da fonte original da foto. Exemplo: http://ppcdn.500px.org/41306792/b9d65ef83fe7fb6bb6a8d9a7cba3982dab9014a1/4.jpg" />
        </div>
        <div class="control-group">
            <label for="autor_fonte_padrao" class="required">ID do Autor da foto no 500px</label> 
            <form:input cssStyle="width: 400px;" path="fotoPadrao.idUsuarioFoto500px"
                        class="input-foto-500px" title="Coloque o ID do Autor/Usuario que possui o crédito/autoria da foto. Exemplos: maxrivefotograaf" />
        </div>
        <div class="control-group">
            <label for="autor_fonte_padrao" class="required">Nome do Autor da foto no 500px</label> 
            <form:input cssStyle="width: 400px;" path="fotoPadrao.autorFonteImagem"
                        class="input-foto-500px" title="Coloque o nome do Autor/Usuario que possui o crédito/autoria da foto. Exemplos: Max Rive, Nicko McBrain, Cristina Wilson, etc" />
        </div>
        <%--div class="control-group">
            <label for="autor_fonte_padrao">URL para o perfil do Autor da foto no 500px</label> 
            <form:input cssStyle="width: 400px;" disabled="true" id="autor_fonte_padrao" path="fotoPadrao.urlAutorImagem" 
                        class="input-foto-500px" title="Coloque a URL para o perfil do Autor/Usuario que possui o crédito/autoria da foto. Exemplo: http://500px.com/maxrivefotograaf" />
        </div--%>
    </div>
    
</fieldset>

<script>
$('#radio-tipo-500px').click(function (e) {
    $('#hidden-foto500px').val('true');
    $('#div-foto-local').hide();
    $('.input-foto-local').attr('disabled', 'disabled');
    $('.input-foto-500px').removeAttr('disabled');
    $('#div-foto-500px').show();
});

$('#radio-tipo-local').click(function (e) {
    $('#hidden-foto500px').val('false');
    $('#div-foto-500px').hide();
    $('.input-foto-500px').attr('disabled', 'disabled');
    $('.input-foto-local').removeAttr('disabled');
    $('#div-foto-local').show();
});
</script>