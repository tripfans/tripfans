<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<!-- TODO vamos ter que implementar uma forma dinâmica de obter as palavras chaves, pois, essa página será gerada dinamicamente -->
	<tiles:putAttribute name="title" value="Muito obrigado por sua avaliação" />


	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
		jQuery(document).ready(function() {
			$("#closeLink").click(function(event){
				event.preventDefault();
				window.close();
				opener.location.reload();
			});
		});
		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<div class="container">
			<div class="span-24 centerAligned">
				<h2>Dados Salvos com Sucesso!</h2>
				<div class="horizontalSpacer"></div>
				<a href="#" id="closeLink">Fechar a Janela</a>
			</div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>