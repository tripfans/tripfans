<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
		<script type="text/javascript">
		$(".paginacao").click(function(event){
			event.preventDefault();
			var url = $(this).attr('href');
			jQuery.get(
		            url,
		            function(response) {
		            	$('#listaPaginada').html(response);
		            }
		        );
		});
// 		Para montar a url da pagina��o{
// 			class='x' href=--------------------?${start}&${pais}
// 			$('#start').val()
// 			faz a function
// 			$('.x').click(function(){
// 				var url = $(this).attr('href')
// 				jQuery.get(
// 			            url,
// 			             function(response) {
// 			            	$('#listaPaginada').html(response);
// 			            }
// 			        );
// 			}
// 					)
// 		}
		</script>
			<c:choose>
				<c:when test="${not empty cidades}">
					<ul class="pager">
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=0&pais=${pais.id}">Primeira</a></li>
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=${param.start - limit}&pais=${pais.id}">Anterior</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Primeira</a></li>
							<li class="disabled"><a href="#">Anterior</a></li>
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=${param.start + limit}&pais=${pais.id}">Pr�xima</a></li>
							<li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaCidades?start=${startUltimaPagina}&pais=${pais.id}">�ltima</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Pr�ximo</a></li>
							<li class="disabled"><a href="#">�ltima</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
				
					<div class="right">
						<strong>P�gina:</strong> ${paginaAtual} / ${quantidadePaginas}
					</div>
					<table class="table table-striped table-bordered span20" style="margin-left: 0px;">
					<thead>
						<tr>
						<th width="2%">#</th>
						<th width="20%">Nome</th>
						<th>Descricao</th>
						<th width="15%">URL-Path</th>
						<th width="12%">A��es</th>
						</tr>
					</thead>
					<c:forEach items="${cidades}" var="cidade" varStatus="count">
					<tr>
						<td width="2%">${count.count + (limit * (paginaAtual - 1))}</td>
						<td width="20%"><a href="${pageContext.request.contextPath}/locais/cidade/${cidade.urlPath}" target="_blank">${cidade.nome}</a></td>
						<td>${cidade.descricao}</td>
						<td width="15%">${cidade.urlPath}</td>
						<td width="12%">
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAlteracaoCidade/${cidade.id}" target="_blank">Editar</a><br/>
						<a href="${pageContext.request.contextPath}/locais/cruds/exibirAdicaoFotosLocal?local=${cidade.id}" target="_blank">Adicionar Fotos</a><br/>
						<a href="${pageContext.request.contextPath}/locais/cidade/${cidade.urlPath}" target="_blank">Preview</a>
						</td>
					</tr>
					</c:forEach>
					</table>
					<ul class="pager">
						<c:choose>
						<c:when test="${paginaAtual != 1}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=0&pais=${pais.id}">Primeira</a></li>
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=${param.start - limit}&pais=${pais.id}">Anterior</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Primeira</a></li>
							<li class="disabled"><a href="#">Anterior</a></li>
						</c:otherwise>
						</c:choose>
						
						<c:choose>
						<c:when test="${temMaisPaginas == true}">
							<li><a class="paginacao" href="${pageContext.request.contextPath}/locais/cruds/listaCidades?start=${param.start + limit}&pais=${pais.id}">Pr�xima</a></li>
							<li><a class="paginacao" href="{pageContext.request.contextPath}/locais/cruds/listaCidades?start=${startUltimaPagina}&pais=${pais.id}">�ltima</a></li>
						</c:when>
						<c:otherwise>
							<li class="disabled"><a href="#">Pr�ximo</a></li>
							<li class="disabled"><a href="#">�ltima</a></li>
						</c:otherwise>
						</c:choose>
					</ul>
				
					<div class="right">
						<strong>P�gina:</strong> ${paginaAtual} / ${quantidadePaginas}
					</div>
				</c:when>
				<c:otherwise>
					<h4>N�o existem cidades nesse para essa sele��o.</h4>
				</c:otherwise>
			</c:choose>