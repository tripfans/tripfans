<%@page import="br.com.fanaticosporviagens.model.entity.Mes"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="fanaticos.default">

	<tiles:putAttribute name="title" value="TripFans - Adicionar Fotos - ${local.nome}" />

	<tiles:putAttribute name="stylesheets">
	</tiles:putAttribute>
	
	<tiles:putAttribute name="footer">
		<script type="text/javascript">
			jQuery(document).ready(function() {
			    $("#fotosForm").initToolTips();
			});

		</script>
	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		
		<div class="container">
			<div class="span20">
			<h2 class="centerAligned">Adicionar Fotos de ${local.nome} - ${local.pai.nome}</h2>
			
			<form:form id="fotosForm" modelAttribute="local" action="${pageContext.request.contextPath}/locais/cruds/adicionarFotos" method="post" enctype="multipart/form-data">
				<s:hasBindErrors name="">
					<div id="errors" class="error centerAligned"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
					</div>
				</s:hasBindErrors>
				<div id="errors" class="error centerAligned" style="display: none;"> 
					  <span class="large"><s:message code="validation.containErrors" /></span> 
				</div>
				
				<input type="hidden" name="local" value="${local.id}" />

				<%@include file="camposIncluirFotos.jsp" %>
				
				<div class="form-actions centerAligned">
				<input type="submit" id="botaoSalvar" class="btn btn-large btn-primary" value="Salvar" >
				</div>
			</form:form>
			
			</div>
 		</div>
	</tiles:putAttribute>

</tiles:insertDefinition>