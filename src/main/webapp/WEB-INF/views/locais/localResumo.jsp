<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${deveMostrarSobre}" >
	<div class="page-container" style="margin-top: 12px;">
		<div class="page-header module-box-header" style="margin: 0 0px 0px 0;">
			<h3>Sobre <span itemprop="name">${local.nome}</span></h3>
		</div>
		<div style="padding: 5px;">
			<c:if test="${local.quantidadeAvaliacoes != 0}" >
				<div class="row" style="margin-bottom: 10px;">
				    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
						<meta itemprop="bestRating" content="5" />
						<meta itemprop="worstRating" content="1" />
						<meta itemprop="ratingValue" content="${local.avaliacaoConsolidadaGeral.classificacaoGeral.nota}" />
						<meta itemprop="reviewCount" content="${local.quantidadeAvaliacoes}" />
						<div class="offset1 span2">
						<span title="Classificado como '${local.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}' em um total de ${local.quantidadeAvaliacoes} avaliações"
						class="starsCategory cat${local.avaliacaoConsolidadaGeral.classificacaoGeral.codigo}"></span>
						</div>
					</div>
				</div>
			</c:if>
			
			<c:if test="${local.tipoLocalEnderecavel}">
			
				<c:if test="${not empty local.classificacoes}">
		    		<strong>Classificação</strong>
		                 <div class="right">
		                 	<c:forEach items="${local.classificacoes}" var="classificacao" varStatus="count">
		    	             	<span class="label label-info">${classificacao.categoria.descricao}</span>
		    	             	<c:if test="${not empty classificacao.classe}"> <span class="label label-info">${classificacao.classe.descricao}</span></c:if> 
		                 	</c:forEach>
		                 </div>
		            <hr style="margin-top: 2px;"/>
		   		</c:if>
			
				<c:if test="${local.tipoHotel && not empty local.estrelas && local.estrelas != ''}">
				 	<div class="row" style="margin-top: 10px; margin-left: 0px; border-bottom: 1px solid #eee;">
		   				<strong>Estrelas:</strong><div class="right"><span title="Categoria: ${local.estrelas} estrela(s)" class="starsRating cat${local.estrelas}" style="margin-left: 8px;">&nbsp;</span></div>
		   			</div>
		   		</c:if>
		   		
			    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<c:if test="${(not empty local.endereco.descricao || not empty localLista.endereco.bairro || not empty localLista.endereco.cep)}">
						<div class="row" style="margin-top: 10px; margin-left: 0px; border-bottom: 1px solid #eee;">
							<strong>Endereço: </strong>
							<div class="right">
								<span itemprop="streetAddress">${local.endereco.descricao}<c:if test="${not empty localLista.endereco.bairro }"> - ${local.endereco.bairro}</c:if></span>
								<c:if test="${not empty localLista.endereco.cep }"> CEP: <span itemprop="postalCode">${localLista.endereco.cep}</span></c:if>
				    		</div>
						</div>
					</c:if>
				
					<div class="row" style="margin-top: 10px; margin-left: 0px; border-bottom: 1px solid #eee;">
						<strong>Localização: </strong>
						<div class="right">
						    <c:set var="addressUrlPai"  value="" />
						    <c:choose>
						    	<c:when test="${not empty local.localizacao.localInteresseTuristicoId}"><c:set var="addressUrlPai" value="${local.localizacao.localInteresseTuristicoUrl}" /></c:when>
						    	<c:when test="${not empty local.localizacao.cidadeId}"><c:set var="addressUrlPai" value="${local.localizacao.cidadeUrl}" /></c:when>
						    	<c:when test="${not empty local.localizacao.estadoId}"><c:set var="addressUrlPai" value="${local.localizacao.estadoUrl}" /></c:when>
						    	<c:when test="${not empty local.localizacao.paisId}"><c:set var="addressUrlPai" value="${local.localizacao.paisUrl}" /></c:when>
						    </c:choose>
							<a href="<c:url value="${addressUrlPai}" />">
								<c:choose>
									<c:when test="${not empty local.localizacao.localInteresseTuristicoId}">
										<span itemprop="addressLocality">
										${local.localizacao.localInteresseTuristicoNome}
										<c:if test="${not empty local.localizacao.cidadeId}">, ${local.localizacao.cidadeNome}</c:if>
										</span>
									</c:when>
									<c:when test="${not empty local.localizacao.cidadeId}">
										<span itemprop="addressLocality">
										${local.localizacao.cidadeNome}
										</span>
									</c:when>
								</c:choose>
								<c:if test="${not empty local.localizacao.estadoId}">
								    <c:if test="${not empty local.localizacao.localInteresseTuristicoId
								                  || not empty local.localizacao.cidadeId}">
								    , 
								    </c:if>
									<span itemprop="addressRegion">
									${local.localizacao.estadoNome}
									</span>
								</c:if>							
								<c:if test="${not empty local.localizacao.paisId}">
								    <c:if test="${not empty local.localizacao.localInteresseTuristicoId
								                  || not empty local.localizacao.cidadeId
								                  || not empty local.localizacao.estadoId}">
								    , 
								    </c:if>
									<span itemprop="addressCountry">
									${local.localizacao.paisNome}
									</span>
								</c:if>							
							</a>
			    		</div>
					</div>
			   		<c:if test="${not empty local.site || not empty local.telefone || not empty local.email}">
			   			<div class="row" style="margin-top: 10px; margin-left: 0px; border-bottom: 1px solid #eee;">
							<c:if test="${not empty local.site}">
								<strong>Website: </strong><a href="${local.site}" target="_blank" itemprop="url">${local.site}</a>
							</c:if>
							<c:if test="${not empty local.email}">
								<c:if test="${not empty local.site}"><br/></c:if>
								<strong>E-mail:</strong> <a href="mailto:${local.email}" itemprop="email">${local.email}</a>
							</c:if>
							<c:if test="${not empty local.telefone}">
								<c:if test="${not empty local.site || not empty local.email}"><br/></c:if>
								<strong>Telefone: <span itemprop="telephone">${local.telefone}</span></strong>
							</c:if>
						</div>
			   		</c:if>
				</div>
		   		
			</c:if>
			
			<c:if test="${local.possuiAlgumInteresse}">
				<div class="row" style="margin-left: 10px; border-bottom: 1px solid #eee; margin-bottom: 10px; margin-top: 10px;">
				    <c:if test="${local.quantidadeInteressesJaFoi != 0}">
				           <c:set var="texto" value="${local.quantidadeInteressesJaFoi == 1 ? 'já foi' : 'já foram'}" /> 
				     <div class="span4" style="margin-left: 0px; margin-bottom: 5px; text-align: center;">
						<span style="display: inline-block;" ><img style=" height: 16px; width: 16px;" src="<c:url value="/resources/images/markers/est-pin.png" />" /></span>
				        <span>${local.quantidadeInteressesJaFoi} ${texto}</span>
				     </div>
				    </c:if>
			    
				    <c:if test="${local.quantidadeInteressesDesejaIr != 0}">
				           <c:set var="texto" value="${local.quantidadeInteressesDesejaIr == 1 ? 'deseja ir' : 'desejam ir'}" /> 
				     <div class="span4" style="margin-left: 0px; margin-bottom: 5px; text-align: center;">
				      <span style="display: inline-block;" ><img style=" height: 16px; width: 16px;" src="<c:url value="/resources/images/markers/des-pin.png" />" /></span>
	                  <span>${local.quantidadeInteressesDesejaIr} ${texto}</span>
				     </div>
				    </c:if>
			    
				    <c:if test="${local.quantidadeInteressesLocalFavorito != 0}">
				           <c:set var="texto" value="${local.quantidadeInteressesLocalFavorito == 1 ? 'marcou como favorito' : 'marcarcam como favorito'}" /> 
				     <div class="span4" style="margin-left: 0px; margin-bottom: 5px; text-align: center;">
				      <span style="display: inline-block;" ><img style=" height: 16px; width: 16px;" src="<c:url value="/resources/images/markers/est-fav-pin.png" />" /></span>
	                  <span>${local.quantidadeInteressesLocalFavorito} ${texto}</span>
				     </div>
				    </c:if>
			    
				    <c:if test="${local.quantidadeInteressesSeguir != 0}">
				           <c:set var="texto" value="${local.quantidadeInteressesSeguir == 1 ? 'segue' : 'seguem'}" /> 
				     <div class="span4" style="margin-left: 0px; margin-bottom: 5px; text-align: center;">
				      <span style="display: inline-block;" ><img style=" height: 16px; width: 16px;" src="<c:url value="/resources/images/icons/gps_arrow.png" />" /></span>
	                  <span>${local.quantidadeInteressesSeguir} ${texto}</span>
				     </div>
				    </c:if>
		 		</div>
			</c:if>
			
			<div class="row" style="margin-left: 10px; margin-bottom: 10px;">
			   <c:if test="${local.quantidadeAvaliacoes != 0}">
				   <c:set var="texto" value="${local.quantidadeAvaliacoes == 1 ? 'avaliação' : 'avaliações'}" /> 
				    <div class="span2" style="margin-left: 0px; margin-bottom: 5px; ">
				    <span class="award_star_gold_3" style="display: inline-block; height: 16px; width: 16px;" ></span>
				    <a href="<c:url value="/destino/${local.urlPath}/avaliacoes" />">
				              <strong>${local.quantidadeAvaliacoes} ${texto}</strong>
				          </a>
				    </div>
			   </c:if>
			   <c:if test="${local.quantidadeDicas != 0}">
			       <c:set var="texto" value="${local.quantidadeDicas == 1 ? 'dica' : 'dicas'}" /> 
				    <div class="span2" style="margin-left: 0px; margin-bottom: 5px;">
				    <span class="lightbulb" style="display: inline-block; height: 16px; width: 16px;" ></span>
				   <a href="<c:url value="/destino/${local.urlPath}/dicas" />">
				              <strong>${local.quantidadeDicas} ${texto}</strong>
				          </a>
				    </div>
			   </c:if>
			   <c:if test="${local.quantidadeFotos !=0 && not empty fotos}">
				    <c:set var="texto" value="${local.quantidadeFotos == 1 ? 'foto' : 'fotos'}" /> 
				    <div class="span2" style="margin-left: 0px; margin-bottom: 5px;">
				    <span class="camera" style="display: inline-block; height: 16px; width: 16px;" ></span>
				    <a href="<c:url value="/destino/${local.urlPath}/fotos" />">
				              <strong>${local.quantidadeFotos} ${texto}</strong>
				          </a>
				    </div>
			  	</c:if>
			  	<c:if test="${local.quantidadePerguntas != 0}">
				    <c:set var="texto" value="${local.quantidadePerguntas == 1 ? 'pergunta' : 'perguntas'}" /> 
				    <div class="span2" style="margin-left: 0px; margin-bottom: 5px;">
				    <span class="help" style="display: inline-block; height: 16px; width: 16px;" ></span>
				    <a href="<c:url value="/destino/${local.urlPath}/perguntas" />">
			              <strong>${local.quantidadePerguntas} ${texto}</strong>
			          </a>
				    </div>
			   </c:if>	
		 	</div>
		</div>		
	</div>
</c:if>