<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16">

    <div id="modal-foto" class="modal hide fade" role="dialog" >
        <div class="modal-header" style="border-bottom: 0px;">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body" style="overflow: hidden; max-height: 600px; min-width: 800px;">
            <div id="modal-foto-body"></div>
        </div>
    </div>

    <div class="span12" style="margin-left: 0px; margin-top: -15px;">

        <div class="page-header">
            <h3>
              Fotos de ${local.nome} no
              <img src="<c:url value="/resources/images/logos/tripfans-t.png"/>" width="110" style="margin-top: -3px;" /> 
            </h3>
        </div>
        
        <c:choose>
            <c:when test="${not empty albumFotoLocal}">
        		<fan:albumFotoLocal album="${albumFotoLocal}" />
            </c:when>
            <c:otherwise>
	            <div class="blank-state">
	                <div class="span2 offset2">
	                    <img src="<c:url value="/resources/images/icons/mini/128/Multimedia-1.png" />" width="90" height="90" />
	                </div>    
	                <div class="span6" style="margin-top: 20px;">
	                    <h3>Não há fotos para exibir</h3>
	                    <p>
	                        Não foram adicionadas fotos para este local.
	                    </p>
	                    <a class="btn btn-secondary" href="<c:url value="/fotos/publicarFotos" />">Seja o primeiro a adicionar uma Foto</a>
	                </div>
	            </div>
            </c:otherwise>
        </c:choose>
        
        <div class="horizontalSpace"></div>
        
        <c:if test="${not unsuportedIEVersion}">
          <div class="span12" style="margin: 20px 10px 20px 0px;">

            <c:if test="${local.usarFotoPanoramio and not empty local.coordenadaGeografica.latitude}">
            
                  <div class="page-header">
                    <h3>
                      Fotos de ${local.nome} no  
                      <img src="<c:url value="/resources/images/logos/panoramio.png"/>" height="20" style="margin-top: -3px;" /> 
                    </h3>
                  </div>
                
                  <div class="span12" style="margin-left: 0px;">
                    <div id="lista-panoramio-photos" class="span12" style="margin-left: 0px;">
                        <div id="wapiblock">
                          <div class="span6" style="margin-top: 20px;">
                            <p>
                                <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
                                Carregando fotos do <img src="<c:url value="/resources/images/logos/panoramio.png"/>" height="16" style="margin-top: -3px;" />...
                            </p>
                          </div>
                        </div>
                    </div>
                  </div>
                
                <c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
                <c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
                <c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
                <c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />
                            
                <script type="text/javascript">
                  var photo_options = {
                      'set' : 'public',
                      'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}},
                  };
                  var widget_options = {
                      'width' : 700, 
                      'height' : 230,
                      'columns': 3,
                      'croppedPhotos' : panoramio.Cropping.TO_FILL,
                      'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED]
                  };
                  
                  function photoClicked(event) {
                      var position = event.getPosition();
                      //var url = 'http://mw2.google.com/mw-panoramio/photos/medium/' + event.getPhoto().getPhotoId() + '.jpg';
                      var url = '<c:url value="/locais/fotos/panoramio/${local.urlPath}" />?position=' + position;
                      $('#modal-foto-body').load(url, function() {
                          $('#modal-foto').modal().css({
                              width: 'auto',
                              'margin-left': function () {
                                  return -($(this).width() / 2);
                              },
                              'top' : '35%'
                          });                 
                          $('#modal-foto').modal('show');
                          
                      });
                      return false;
                  }
                  
                  //var photoWidget = new panoramio.PhotoWidget('wapiblock', photo_options, widget_options);
                  var photoWidget = new panoramio.PhotoListWidget('wapiblock', photo_options, widget_options);
                  
                  panoramio.events.listen(photoWidget, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);                
                  
                  photoWidget.setPosition(0);
                  /*photoWidget.enableNextArrow(true);
                  photoWidget.enablePreviousArrow(true);*/
                </script>
                
            </c:if>
          </div>
        </c:if>
        
    </div>
    
    <div class="span4" style="margin-top: 10px;">
      <c:if test="${not empty fotos}">
        <div style="text-align: center;">
          <sec:authorize access="isAuthenticated()">
            <a class="btn btn-secondary btn-large" href="<c:url value="/perfil/${usuario.urlPath}/fotos" />">Adicione Fotos</a>
          </sec:authorize>
          <sec:authorize access="not isAuthenticated()">
            <a class="btn btn-secondary btn-large" href="<c:url value="/entrarPop" />">Adicione Fotos</a>
          </sec:authorize>
        </div>
      </c:if>
      <div class="horizontalSpacer"></div>
      <fan:adsBlock adType="1" />
    </div>
    
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    $('img.album').lazyload({
        effect : 'fadeIn'
    });
    
    $('a.fancybox').click(function(e) {
        e.preventDefault();
        var idFoto = $(this).data('id-foto');
        // pegar a url da foto
        var url = $('#box-foto-link-' + idFoto).attr('href');
        $('#modal-foto-body').load(url, function() {
            $('#modal-foto').modal().css({
                width: 'auto',
                'margin-left': function () {
                    return -($(this).width() / 2);
                }
            });                 
            $('#modal-foto').modal('show');
            
        });
    });
    
    $('.panoramio-wapi-photolist').css('width', '100%');
    
});
</script>