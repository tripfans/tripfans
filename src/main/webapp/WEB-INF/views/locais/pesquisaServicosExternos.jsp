<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>


<div id="componentePesquisa" class="borderFullPanel">
	<ul>
		<li><a href="#abaPesquisaHoteis">Hotéis</a></li>
		<li><a href="#abaPesquisaVoos">Voos</a></li>
	</ul>
	<div id="abaPesquisaHoteis">
		<p class="large centerAligned">
			Pesquise os preços de hotéis
		</p>
		<form>
			<div class="span5">
				<label for="local">Destino:</label>
			</div>
			
			<div class="span5">
				<input id="local" name="local" type="text" value="${cidade.nome}" > 
			</div>
			
			<div class="span5">
				<div class="span3">
					<label for="dataChegada">Chegada:<input id="dataChegada" name="dataChegada" type="text"></label>
				</div>
				
				<div class="span3">
					<label for="dataSaida">Saída: <input id="dataSaida" name="dataSaida" type="text"></label>
				</div>
				
				<div class="span2">
					<label for="pessoas">Pessoas: <select name="pessoas"
						id="pessoas">
						<option value="1">1</option>
						<option value="2" selected="selected">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
					</label>
				</div>
				
				<div class="span2">
					<label for="quartos">Quartos:<select name="quartos"
						id="quartos">
						<option value="1" selected="selected">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="4">4</option>
					</select>
					</label>
				</div>
			</div>
			
			<div class="span5 centerAligned">
				<button>Pesquisar</button>
			</div>
		</form>

	</div>
	<div id="abaPesquisaVoos">
	</div>
</div>

<script type="text/javascript">
	// O conteúdo a seguir é específico dessa página
	jQuery(document).ready(function() {
        $("#componentePesquisa").tabs();
        //criação dos campos de data
        $("#dataChegada").datepicker();
        $("#dataSaida").datepicker();
    });
</script>