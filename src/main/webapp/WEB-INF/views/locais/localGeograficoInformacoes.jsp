<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade,br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style type="text/css">
  #panoramio_box {
    position: relative;
    margin: 10px 0 20px 10px;
    width: 531px;
  }
  #panoramio_box_list {
    position: absolute;
    left: 540px;
  }
  #panoramio_box_photo_big {
    margin-left: 10px;
  }
  #panoramio_box .panoramio-wapi-images {
    background-color: #transparent;
  }
  #panoramio_box .pwanoramio-wapi-tos{
    background-color: #transparent !important;
  }
  
  #tripfans_foto_img {
    position: relative;
  }
  
</style>

	<c:set var="spanDescricao" value="span12" />
	<c:set var="spanFoto" value="span9" />
	
    <div class="span12">
    
      <div class="row">
      
    	  <c:if test="${local.fotoPadraoAnonima == false}">
    		<div id="tripfans_foto" class="${spanFoto}" style="margin-top: 10px;">
                <i id="tripfans_foto_img" class="carrossel-thumb borda-arredondada" style="background-image: url('${local.urlFotoBig}'); height: 340px; width: 520px;">
                    <c:if test="${local.fotoPadrao.tipoArmazenamentoExterno}">
                      <p class="foto-externa-info " style="opacity: .9;">
                        <span class="pull-right" style="margin-right: 8px;">
                          Por <a class="foto-externa-autor" target="_blank" href="${local.fotoPadrao.urlAutorImagem}">${local.fotoPadrao.idUsuarioFoto500px}</a>
                          em <a class="foto-externa-fonte" target="_blank" href="${local.fotoPadrao.urlFonteImagem}">${local.fotoPadrao.fonteImagem}</a>
                        </span>
                      </p>
                    </c:if>
                </i>
    			<c:if test="${not empty local.fotoPadrao.fonteImagem}">
    			    <div class="right"><h5><i>Origem da Foto: ${local.fotoPadrao.fonteImagem}</i></h5></div>
    			</c:if>
    		</div>
    	  </c:if>
            <div id="panoramio_box">
              <div id="panoramio_box_list"></div>
              <div id="panoramio_box_photo_big" class="${not local.fotoPadraoAnonima ? 'hide' : ''}">
                <div id="panoramio_box_photo"></div>
                <div id="panoramio_box_attr"></div>
              </div>
            </div>
<%--                 <div id="wapiblock">
                  <div class="span11" style="margin-top: 20px;">
                    <p>
                        <img src="<c:url value="/resources/images/ui-anim_basic_16x16.gif"/>" width="16" style="margin-top: -3px;" />
                        Carregando fotos do <img src="<c:url value="/resources/images/logos/panoramio.png"/>" height="16" style="margin-top: -3px;" />...
                    </p>
                  </div>
                </div>
 --%>           
            <c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
            <c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
            <c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
            <c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />
            
            <c:if test="${not unsuportedIEVersion}">
            <script type="text/javascript">
            
              var photo_request = {
                  //'tag': 'urban',
                  'set' : 'public',
                  'rect' : {'sw': {'lat': '${latSW}', 'lng': '${lngSW}'}, 'ne': {'lat': '${latNE}', 'lng': '${lngNE}'}}
              };
              
              var photo_widget_options = {
                  'width': 520,
                  'height': 340,
                  'attributionStyle': panoramio.tos.Style.HIDDEN,
                  'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED],
                  'croppedPhotos' : panoramio.Cropping.TO_FILL
              };

    		  var photo_widget = new panoramio.PhotoWidget(
    		      'panoramio_box_photo', photo_request, photo_widget_options);
            		  
              var list_widget_options = {
                  'width' : 50, 
                  'height' : 340,
                  'columns': 1,
                  'rows': 6,
                  'croppedPhotos' : true,
                  'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED],
                  'orientation': panoramio.PhotoListWidgetOptions.Orientation.VERTICAL,
                  'attributionStyle': panoramio.tos.Style.HIDDEN
              };
              
              var list_widget = new panoramio.PhotoListWidget(
            		    'panoramio_box_list', photo_request, list_widget_options);
              
              var terms_options = {'width': '100%'};
              var terms_widget = new panoramio.TermsOfServiceWidget(
                'panoramio_box_attr', terms_options);

              function photoClicked(event) {
            	  return false;
              }
              
              function photoChanged(event) {
              	if (event.target.getPhoto() == null) {
              		// esconder widget
              		$('#panoramio_box').hide();
              	}
              }
              
              function listPhotoClicked(event) {
            	  var position = event.getPosition();
            	  if (position !== null) {
            	      $('#tripfans_foto').hide();
            	      $('#panoramio_box_photo_big').show();
            		  photo_widget.setPosition(position);
            	  }
              }
              
              panoramio.events.listen(list_widget, panoramio.events.EventType.PHOTO_CLICKED, listPhotoClicked);
              panoramio.events.listen(photo_widget, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);
              panoramio.events.listen(photo_widget, panoramio.events.EventType.PHOTO_CHANGED, photoChanged);
              
              photo_widget.enablePreviousArrow(false);
              photo_widget.enableNextArrow(false);
              
              photo_widget.setPosition(0);
              list_widget.setPosition(0);
            </script>    
            </c:if>            
          
          <c:if test="${not empty local.descricao}">
            <div class="${spanDescricao} descricaoLocal" style="padding-top: 15px;">
                ${local.descricao}
            </div>
    	  </c:if>
    	
      </div>
	  
	  <c:if test="${not empty interessesAmigosJaForam}">
				<jsp:include page="listaAmigosJaForamLocal.jsp" />
	  			<div class="horizontalSpacer"></div>
		</c:if>

		<c:if test="${local.quantidadeFotos != 0}">
			<div class="page-header">
				<h3>
					Últimas Fotos de ${local.nome}
					<span class="badge badge-info" style="margin-top: 10px;">${local.quantidadeFotos}</span>
					<div class="pull-right">
	     				<a href="<c:url value="/locais/${local.urlPath}/fotos" />" class="seeMore">
	     				Ver todas as Fotos
	     				</a>
	   				</div>
				</h3>
			</div>
			<jsp:include page="/WEB-INF/views/fotos/fotosRecentes.jsp">
				<jsp:param value="550" name="width"/>
			</jsp:include>
			<div class="horizontalSpacer"></div>
		</c:if>
		
		<fan:localAvaliacaoAmostra local="${local}" amostraAvaliacoes="${amostraAvaliacoes}" />
		
		<c:if test="${local.quantidadeDicas != 0}">
       		<div class="span12" style="margin-left: 0px;">
				<div class="page-header">
					<h3>Últimas dicas
					<div class="row pull-right">
						<a id="listarTodasDicas" href="${pageContext.request.contextPath}/locais/${local.urlPath}?tab=dicas" class="seeMore">Ver todas</a>
					</div>
					</h3>
				</div>
			
				<c:forEach items="${amostraDicas}" var="dica" varStatus="contador" >
					<fan:dica fullSize="12" userSize="2" dataSize="10" mostraAcoes="true" mostraTextoDicaSobre="true" dica="${dica}" />
				</c:forEach>
			</div>
		</c:if>
		
		<fan:localListaComAvaliacao locais="${filhosMaisPopulares}" localGeografico="${local}" mostrarLinkVerTodos="false" titulo="Destinos mais populares"/>

		<fan:localListaComAvaliacao locais="${melhoresAtracoes}" localGeografico="${local}" mostrarLinkVerTodos="true" linkVerTodos="oque-fazer"  quantidade="${local.quantidadeAtracoes}"  tipoLocalListado="${'ATRACAO'}" />
		
	  	<fan:localListaComAvaliacao locais="${melhoresHoteis}" localGeografico="${local}" mostrarLinkVerTodos="true" linkVerTodos="hospedar" quantidade="${local.quantidadeHoteis}" tipoLocalListado="${'HOTEL'}" />
				
		<fan:localListaComAvaliacao locais="${melhoresRestaurantes}" localGeografico="${local}" mostrarLinkVerTodos="true"  linkVerTodos="onde-comer" quantidade="${local.quantidadeRestaurantes}"  tipoLocalListado="${'RESTAURANTE'}" />
		
        
<!-- 	  </div> -->
    
    </div>
    
    <div class="span5 right" style="margin-left: 0px;">
    	
    	<jsp:include page="localResumo.jsp" />
    	
    	<c:if test="${local.latitude != null}">
    		
    			<div id="map_canvas" class="hide" style="border: 1px solid #CCC; height: 180px; margin-top: 15px; display: none;">
                    <div style="padding-bottom: 10px;"></div>
    		    </div>
                <div class="pull-right" style="margin-top: 10px;"><strong>
                    <a href="#" onclick="mostraMapaLocal(this, 'map_canvas', '${local.latitude}', '${local.longitude}','${pageContext.request.contextPath}/resources/images/${local.tipoLocal.iconeMapa}')"
                       id="mostraMapa">Ocultar Mapa</a></strong>
                </div>
                <script>
                    jQuery(document).ready(function() {
                        mostraMapaLocal(null, 'map_canvas', '${local.latitude}', '${local.longitude}','${pageContext.request.contextPath}/resources/images/${local.tipoLocal.iconeMapa}')
                    });
                </script>
    		
    	</c:if>
	
    	<div class="horizontalSpacer"></div>
    	<fan:adsBlock adType="0" />
    	
        <div class="horizontalSpacer"></div>
    	
        <fan:localTopUsers local="${local}" amostraPontuacaoUsuario="${amostraPontuacaoUsuario}"/>
        
    </div>