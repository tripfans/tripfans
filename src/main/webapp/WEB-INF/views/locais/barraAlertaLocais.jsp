<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style>
    .alertFooter {
        /*background-color: rgb(58, 57, 49);*/
        background-color: rgb(56, 97, 110);
        height: 96px;
    }
    .alertFooter h2 {
        /*color: rgb(6, 212, 221);*/
        color: rgb(157, 223, 226);
    }
    .alertFooter p {
        color: rgb(228, 228, 228);
    }
    .alertFooter a.avaliacao {
        color: #46a81d;
    }    
    .alertFooter a.dica {
        color: #ecb726;
    }    
    .alertFooter a.pergunta {
        color: #43a1f2;
    }    
    #botaoFecharBarra {
        background-color: rgb(253, 253, 253);
    }
</style>

<div class="alertFooter">
	<div class="container">
	<button type="button" class="close" id="botaoFecharBarra">
		<img src="<c:url value="/resources/images/icons/mini/32/preto/Interface-83.png" />" />
	</button>
	
	<div class="offset2 span5" style="padding-top: 15px;">
		 <div class="span1">
               <img src="<c:url value="/resources/images/icons/mini/64/avaliacao.png" />" width="50" />
           </div> 
           <div class="span3">
			<p>
				Escreva uma <strong><a href="<c:url value="/avaliacao/avaliar/${local.urlPath}" />" class="avaliacao">avaliação</a></strong> relatando sua experiência aqui.
			</p>
		</div>
	</div>
	<div class="span5" style="padding-top: 15px;">
		 <div class="span1">
               <img src="<c:url value="/resources/images/icons/mini/64/dica.png" />" width="50" />
           </div> 
           <div class="span3">
			<p>
				Deixe uma <strong><a href="<c:url value="/dicas/escrever/${local.urlPath}" />" class="dica">dica</a></strong> para os outros viajantes.
			</p>
		</div>
	</div>
	<div class="span5" style="padding-top: 15px;">
		 <div class="span1">
               <img src="<c:url value="/resources/images/icons/mini/64/pergunta.png" />" width="50" />
           </div> 
           <div class="span3">
			<p>
				Quer saber mais sobre ${local.nome}? Então faça uma <strong><a href="<c:url value="/perguntas/perguntar/?local=${local.urlPath}" />" class="pergunta">pergunta</a></strong> agora mesmo.
				</p>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    $('.alertFooter').show('slow');
    
    $('#botaoFecharBarra').click(function() {
    	$('.alertFooter').hide('slow');
    });
    
});
</script>