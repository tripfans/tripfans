<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
					
<div id="botoesAcao" class="${param.span}" style="margin-left: 0px;">
	<div class="page-header">
		<h3>Já esteve em ${local.nome}? Compartilhe sua experiência aqui</h3>
	</div>
	<div style="text-align: center;">
		<a class="btn btn-secondary btn-large" href="<c:url value="/avaliacao/avaliar/${local.urlPath}" />" >Escreva uma Avaliação</a>
		<a class="btn btn-secondary btn-large" href="<c:url value="/dicas/escrever/${local.urlPath}" />">Deixe uma Dica</a>
	</div>
	
	<div class="horizontalSpacer"></div>
</div>