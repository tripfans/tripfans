<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${local.quantidadeInteressesJaFoi != 0}">
	<span style="margin-right: 5px;">
	   <strong>Já Foram</strong> <span  class="badge badge-success">${local.quantidadeInteressesJaFoi}</span>
	</span>
</c:if>

<c:if test="${local.quantidadeInteressesDesejaIr != 0}">
	<span style="margin-right: 5px;">
	   <strong>Desejam Ir</strong> <span  class="badge badge-info">${local.quantidadeInteressesDesejaIr}</span>
	</span>
</c:if>

<c:if test="${local.quantidadeInteressesLocalFavorito != 0}">
	<span style="margin-right: 5px;">
	   <strong>Favorito</strong> <span  class="badge badge-important">${local.quantidadeInteressesLocalFavorito}</span>
	</span>
</c:if>

<c:if test="${local.quantidadeInteressesSeguir != 0}">
	<span>
	   <strong>Seguem</strong> <span  class="badge badge-warning">${local.quantidadeInteressesSeguir}</span>
	</span>
</c:if>