<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16" style="margin-top: -15px;">

    <div class="page-header">
        <h3>Not�cias</h3>
    </div>
      
    <c:choose>
    	<c:when test="${not empty noticias}">
    		<c:forEach var="noticia" items="${noticias}" >
    		    <div class="row" style="margin-left: 0;">
    		        <fan:noticia noticia="${noticia}"/>
    		    </div>
    		</c:forEach>
    	</c:when>
    	<c:otherwise>
            <div class="blank-state">
                <div class="span2 offset1">
                    <img src="<c:url value="/resources/images/icons/mini/128/Communication-27.png" />" width="90" height="90" />
                </div>    
                <div class="span6" style="margin-top: 20px;">
                    <h3>N�o h� not�cias para exibir</h3>
                    <p>
                        At� o momento n�o existem novidades para este este local.
                    </p>
                </div>
            </div>    
    
            <div class="horizontalSpacer">
            </div>
            
        </c:otherwise>
    </c:choose>  

</div>