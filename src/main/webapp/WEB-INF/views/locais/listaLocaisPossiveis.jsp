<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="div-lista-locais">
    <c:if test="${not empty locais}">
      <fieldset>
        <div>
            <h4>
                O local que voc� est� procurando � algum dos listados abaixo?
                <a href="#" class="btn btn-warning" onclick="exibirComplemento();">N�o</a>
            </h4>
            <br/>
        </div>
        <table class="table table-striped" style="width: 100%">
         <tbody>
           <c:forEach var="local" items="${locais}">
             <tr>
                 <td width="35%">
                    ${local.nome}
                 </td>
                 <td width="50%">
                    ${local.endereco.descricao}
                 </td>
                 <td width="15%" style="text-align: right;">
                    <a class="btn btn-info" href="<c:url value="/locais/sugerirLocal/escolher/${local.urlPath}?acao=${acao}"/>">� este</a>
                 </td>
             </tr>
           </c:forEach>
         </tbody>
        </table>
      </fieldset>
    </c:if>
    <script>
    function exibirComplemento() {
        $('#div-lista-locais').hide();
        $('#div-info-complementar').show();
        $('#div-acoes-passo2').show();
        $('#endereco').focus();
    }
    
    <c:if test="${empty locais}">
        exibirComplemento();
    </c:if>
    </script>
</div>