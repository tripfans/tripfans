<%@page
	import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade,br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan"%>

<c:if test="${param.mostraLerMais == true}" >
	<c:set var="maxHeight" value="max-height: 43px;" />
</c:if>

<div class="horizontalSpacer"></div>
<div class="borda-arredondada ${param.spanSize} page-container">
	<div class="page-header module-box-header" style="margin: 0px;">
		<h3>
			Dicas mais recentes sobre este local
			<c:if test="${param.mostraQuantidades == true}">
				<span class="badge badge-info" style="margin-top: 10px;">${local.quantidadeDicas}</span>
			</c:if>
		</h3>
	</div>
	<div class="module-box-content-list"  style="padding: 5px;">
		<ul class="list">
			<c:forEach items="${amostraDicas}" var="dica">
				<li style="min-height: 80px;">
					<div style="padding: 3px 6px 1px 6px;">
						<div class="left">
							<c:choose>
								<c:when test="${not empty dica.autor}">
									<c:url var="urlPerfil" value="/perfil/${dica.autor.urlPath}" />
									<fan:userAvatar showFrame="false" imgSize="avatar" displayDetails="true"
										user="${dica.autor}" displayName="false" marginLeft="0"
										marginRight="8"  />
								</c:when>
								<c:otherwise>
									<c:url var="urlPerfil"
										value="/perfil/fb/${dica.idAutorFacebook}" />
									<fan:userAvatar showFrame="false" imgSize="mini-avatar"
										idUsuarioFacebook="${dica.idAutorFacebook}"
										displayName="false" marginLeft="0" marginRight="8" />
								</c:otherwise>
							</c:choose>
						</div>
						<div
							style="overflow: hidden; word-wrap: break-word; max-width: 100%; margin-bottom: 0px; text-align: justify;">
							<p style="margin-bottom: 0px;">
								<small> <a href="${urlPerfil}"> ${dica.nomeAutor} </a> <span
									class="gray">sobre</span> <a
									href="<c:url value="/locais/${dica.localDica.urlPath}" />">
										${dica.localDica.nome} </a>
								</small>
							<p style="margin-bottom: 0px; overflow: hidden; ${maxHeight} line-height: 14px;">
								<a
									href="<c:url value="/locais/${dica.localDica.urlPath}?tab=dicas&dica=${dica.urlPath}" />"
									style="color: #999;"> ${dica.descricao} </a>
							</p>
							<c:if test="${param.mostraLerMais == true}" >
								<div class="pull-right" style="margin-bottom: 0px;">
									<small> <a
										href="<c:url value="/locais/${dica.localDica.urlPath}?tab=dicas&dica=${dica.urlPath}" />">
											Ler mais... </a>
									</small>
								</div>
							</c:if>
							</p>
							<c:if test="${param.mostraBotaoMarcarUtil == true}">
								<p style="margin-top: 0px; margin-bottom: 0px;">
									 <fan:votarUtilButton target="${dica}" cssButtonClassSize="mini" />
									<c:if test="${dica.quantidadeVotoUtil != 0}">
										<small> ${dica.quantidadeVotoUtil} marcaram como útil</small>
									</c:if>
								</p>
							</c:if>
						</div>
					</div>
				</li>
			</c:forEach>
		</ul>
	</div>
	<c:if test="${param.mostraVerTodas == true}">
		<div class="row right" style="margin-top: 5px;">
			<a class="seeMore" href="<c:url value="/locais/${local.urlPath}?tab=dicas" />">Ver todas</a>
		</div>
	</c:if>
</div>