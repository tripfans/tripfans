<%@page import="br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="page-header">
	<h3>O que dizem seus amigos - ${interesseAmigo.tipo.tituloInteresseAmigos}</h3>
</div>

<fan:localInteresseAmigo local="${local}" interesseAmigo="${interesseAmigo}" />
