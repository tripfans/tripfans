<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.LocalType"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="fanaticos.default">
    <tiles:putAttribute name="title" value="TripFans" />

    <tiles:putAttribute name="stylesheets">
    </tiles:putAttribute>
        
    <tiles:putAttribute name="footer">
        <script type="text/javascript">
        $(document).ready(function() {
            <c:if test="${exibirAgradecimento}">
              $('#modal-agradecimento').modal('show');
            </c:if>
        });
        </script>
    </tiles:putAttribute>

    <tiles:putAttribute name="body">
        <div class="container-fluid page-container" style="min-height: 516px;">
          <form method="post">
            <div class="title module-box-header">
                <span class="title">${novoLocal.nome}</span>
            </div>

            <div class="span15" style="margin-top: 15px;">
              <fieldset>
                <div>
                    <div class="horizontalSpacer"></div>
                    <div class="row" align="center">
                        <h2>O que você gostaria de fazer?</h2>
                        <br/>
                        <fan:listaAcoesLocais local="${novoLocal}" tamanhoTitulo="large" exibirAcaoAvaliacao="true" exibirAcaoDica="true" />
                    </div>
                </div>
              </fieldset>
              
              <%--fan:adsBlock adType="2" /--%>
            </div>
            
            <div class="span4" style="margin-top: 15px;">
              <fan:adsBlock adType="1" />
            </div>
          </form>
        </div>
        
        <c:if test="${exibirAgradecimento}">
          <div id="modal-agradecimento" class="modal hide fade" style="font-size: 16px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Obrigado por sua colaboração!</h3>
            </div>
            <div class="modal-body" style="display: inline-block;">
              <div class="alert alert-success" style="text-align: justify;">
                <p class="lead">
                    O local <strong>${novoLocal.nome}</strong> foi pré-cadastrado e está aguardando validação.
                    Assim que ocorrer a validação ou recusa do cadastro, você será avisado. 
                </p>
                <p class="lead">
                    Para ver os locais que você cadastrou, vá até o seu perfil e clique em <strong>"Meus Locais cadastrados"</strong>.
                </p>
                <p class="lead">
                    <strong>Você já pode avaliar, deixar dicas ou fazer perguntas sobre o local cadastrado.</strong>
                </p>
              </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn" data-dismiss="modal">
                    <i class="icon-remove"></i> Fechar
                </a>
            </div>
          </div>
        </c:if>
    </tiles:putAttribute>

</tiles:insertDefinition>        