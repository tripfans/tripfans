<%@ page import="br.com.fanaticosporviagens.model.entity.ClasseLocal"%>
<%@ page import="br.com.fanaticosporviagens.model.entity.CategoriaLocal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<style>
.ui-widget-content, .ui-state-default {
    border: solid 1px #ccc;
    color: #4D4D4D;
    font-family: 'PT Sans Narrow', "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 13px;
    line-height: 18px;
    color: #4D4D4D;
    background: #fff;
}

.ui-state-default .ui-icon, .ui-state-hover .ui-icon {
    background-image: url(<c:url value="/resources/styles/jquery-ui/fanaticos/images/ui-icons_666666_256x240.png"/>);
}

.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
    color: #4D4D4D; 
    border: solid 1px #ccc;
    background: #ffffff 50% 50% repeat-x;
}

label.ui-corner-all.ui-state-hover {
    border: solid 1px #fff;
    background-color: #f6f6f6;
}


.fieldset-oculto {
    max-height: 1px;
    border: 0px;
    border-top: solid 1px #eee;
    background-color: transparent;
}

.fieldset-visivel {
    background-color: #f3f3f3;
    border: 1px solid #EEE;
    min-height: 100px;
}

</style>
<div style="margin-top: -15px;">
	<div class="span16 page-header" style="margin-top: 3px; margin-left: 15px;">
		<h3>${tipoLocalListado.tituloListaLocais}</h3>
	</div>
    
    <c:url var="urlLocaisEnderecaveis" value="/locais/todos/${tipoLocalListado.urlPath}/${local.urlPath}" />
    <form id="filter-form" action="${urlLocaisEnderecaveis}" style="margin-bottom: 5px;" onsubmit="return false;">
    <c:set var="ordemSelecionada" value="classificacaoGeral" />
    <input id="sortField" type="hidden" name="sort" value="${ordemSelecionada}" />
    <input id="dirField" type="hidden" name="dir" value="desc" />
    
	<div id="divLista" class="span12">
    
      <script>
        <c:if test="${not unsuportedIEVersion}">
	        <c:if test="${not empty listaLocais}">
		        var terms_widget = new panoramio.TermsOfServiceWidget(
		                'terms-of-service', {'width': '100%'});
	        </c:if>
        </c:if>
        
        var timeout = null;
        var termoFiltro = null;

        $(document).ready(function () {
            
            /*$('#myselect').ddslick({
                onSelected: function(selectedData) {
                    //callback function: do something with selectedData;
                	return false;
                }   
            });*/
            
            <c:if test="${not empty listaLocais}">
            $('#mostrarFiltro').click(function(event) {
                event.preventDefault();
                $('#div-filtros').show();
                $('#fieldset-filtro').removeClass('fieldset-oculto').addClass('fieldset-visivel');
                $(this).hide();
                $('#esconderFiltro').show();
            });
            
            $('#esconderFiltro').click(function(event) {
                event.preventDefault();
                $('#div-filtros').hide();
                $('#fieldset-filtro').removeClass('fieldset-visivel').addClass('fieldset-oculto');
                $(this).hide();
                $('#mostrarFiltro').show();
            });
            
            $('#select-estrelas').multiselect({
        	   //selectedText: "# of # selected"
                header: true,
                height: 200,
                minWidth: 225,
                classes: '',
                checkAllText: 'Todas',
                uncheckAllText: 'Nenhuma',
                noneSelectedText: 'Nenhuma selecionada',
                selectedText: '# selecionada(s)',
                selectedList: 0,
        	   
        	});
            
            $('#filter-form :input').change(function() {
                if ($(this).attr('id') != 'filtro-nome') {
                    clearTimeout(timeout);
                    timeout = setTimeout(function() {
                        filtrar();
                    }, 1000);
                }
            });
            
            
            $('.limpar-filtro').click(function(event) {
                $('#filter-form')[0].reset();
                filtrar();
            });
            
            $('#btn-filtrar').click(function(event) {
                event.preventDefault();
                var formData = $('#filter-form').serialize();
                $('#listaLocalEnderecavel').load('${urlLocaisEnderecaveis}', formData, function(response) {
                });                
            });
            
            $('#filtro-nome').keyup(function(event) {
                clearTimeout(timeout);
                var valorAtual = $('#filtro-nome').val();
                if (valorAtual != termoFiltro) {
                    timeout = setTimeout(function() {
                        filtrar()
                    }, 1500);
                }
                termoFiltro = $('#filtro-nome').val();
            });
            </c:if>
        
            //$('.fancy').fancy();
        });
        
        function filtrar() {
            var formData = $('#filter-form').serialize();
            $('#listaLocalEnderecavel').load('${urlLocaisEnderecaveis}', formData, function(response) {
            }); 
        }
      </script>
      
	  <c:choose>
		<c:when test="${not empty listaLocais}">
            <div style="margin-bottom: 10px; display: inline-block; float: right;">
                <fan:groupOrderButtons url="${urlLocaisEnderecaveis}" formId="filter-form" targetId="listaLocalEnderecavel" cssClass="pull-right" selectedOrder="${ordemSelecionada}">
                    <fan:orderButton dir="desc" orderAttr="classificacaoGeral" title="Mais bem avaliados" tooltip="Ordenar pela nota geral das avaliações"  />
                    <c:if test="${tipoLocalListado.tipoHotel}">
                      <fan:orderButton dir="desc" orderAttr="estrelas" title="Estrelas" tooltip="Ordenar pela classificação em estrelas" />
                    </c:if>
                    <fan:orderButton dir="desc" orderAttr="quantidadeInteressesJaFoi" title="Mais visitados" tooltip="Ordenar pelos mais visitados" />
                    <fan:orderButton dir="asc" orderAttr="nome" title="Alfabética" tooltip="Ordenar por ordem alfabética" />
                </fan:groupOrderButtons>
            </div>
        
			<%@ include file="localEnderecavelTodosPagina.jsp"  %>
            
            <div id="terms-of-service" class="${not empty listaLocais ? '' : 'hide'}"></div>
		</c:when>
		<c:otherwise>
            <div class="blank-state">
                <div class="span2 offset2">
                    <c:set var="icon" value="${(tipoLocalListado.tipoAtracao ? 'Places-18' : (tipoLocalListado.tipoHotel ? 'Places-6' : 'Food-18'))}"></c:set>
                    <img src="<c:url value="/resources/images/icons/mini/128/${icon}.png" />" width="90" height="90" />
                </div>    
                <div class="span6" style="margin-top: 5px; margin-left: 10px;">
                    <h3>Nenhum${tipoLocalListado.tipoAtracao ? 'a' : ''} ${tipoLocalListado.descricao} para exibir</h3>
                    <p>
                        Ainda não foram cadastrad${tipoLocalListado.tipoAtracao ? 'a' : 'o'}s ${tipoLocalListado.descricaoPlural} para esta cidade.
                    </p>
                    <a class="btn btn-secondary" href="<c:url value="/locais/sugerirLocal?cidade=${local.urlPath}&tipoLocal=${tipoLocalListado.urlPath}"/>">Sugira ${tipoLocalListado.descricaoPlural}</a>
                </div>
            </div>    
		</c:otherwise>
	  </c:choose>
	</div>
	
	<div class="span5 right" style="margin-left: 0px; margin-top: -9px;">
      <c:if test="${not empty listaLocais}">
      
        <div class="page-container" style="margin-top: 0px; margin-bottom: 20px; max-width: 263px; background-color: #f8f8f8;">
            <div class="page-header module-box-header" style="margin: 0 0px 0px 0;">
                <h3>
                  Filtros
                  <a href="#" class="btn btn-mini limpar-filtro pull-right" style="margin-top: 4px;">
                      Limpar filtros
                  </a>
                </h3>
            </div>
            <div style="padding: 5px;">
                  
              <div id="div-filtros">
                
                  <input type="hidden" name="filtro" value="true" />
                  
                  <ul class="label-right" style="min-height: 34px; margin: 5px; list-style: none;">
                    <li>
                        <label for="tituloAvaliacao" class="${labelSpanClass}" style="width: 80px; display: inline-table;">
                            <span>
                                Nome
                            </span>
                        </label>
                        <input id="filtro-nome" type="text" name="nome" maxlength="50" style="width: 233px;" />
                    </li>
    
                    <c:forEach items="${camposAgrupados}" var="campoAgrupado">
                      <c:choose>
                          <c:when test="${(campoAgrupado.name == 'estrelas' and not tipoLocalListado.tipoHotel) or empty campoAgrupado.values or campoAgrupado.name == 'bairro'}">
                          </c:when>
                          <c:otherwise>
                            <div class="accordion" id="accordion-${campoAgrupado.name}" style="background-color: white;">
                              <c:if test="${campoAgrupado.name == 'estrelas'}">
                                <c:set var="nomeAgrupamento" value="Estrelas" />
                              </c:if>
                              <c:if test="${campoAgrupado.name == 'idsCategorias'}">
                                <c:set var="nomeAgrupamento" value="Categorias" />
                              </c:if>
                              <c:if test="${campoAgrupado.name == 'idsClasses'}">
                                <c:set var="nomeAgrupamento" value="Classificações" />
                              </c:if>
                              <c:if test="${campoAgrupado.name == 'bairro'}">
                                <c:set var="nomeAgrupamento" value="Bairros" />
                              </c:if>
                              <div class="accordion-group">
                                <div class="accordion-heading">
                                  <a class="accordion-toggle" data-toggle="collapse" href="#collapse-${campoAgrupado.name}">
                                    <i class="icon-chevron-down"></i> ${nomeAgrupamento}
                                  </a>
                                </div>
                                  <div id="collapse-${campoAgrupado.name}" class="accordion-body collapse in">
                                      <div class="accordion-inner">
                                        <%-- ul class="nav nav-list" --%>
                                          <c:forEach items="${campoAgrupado.values}" var="valorCampoAgrupado">
                                            <li>
                                               <c:choose>
                                                <c:when test="${valorCampoAgrupado.name == '0'}">
                                                  <c:set var="semClassificacao" value="${valorCampoAgrupado.count}" />
                                                </c:when>
                                                <c:otherwise>
                                                  <label>
                                                    <input type="checkbox" name="${campoAgrupado.name}" value="${valorCampoAgrupado.name}" style="display: inline-block;" />
                                                    <c:if test="${campoAgrupado.name == 'estrelas'}">
                                                      <img src="<c:url value="/resources/images/star${valorCampoAgrupado.name}.png"/>"/> <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                    </c:if>
                                                    <c:if test="${campoAgrupado.name != 'estrelas'}">
                                                      <c:if test="${campoAgrupado.name == 'idsCategorias'}">
                                                        <c:set var="codigoCategoria" value="${valorCampoAgrupado.name}" scope="page" />
                                                        ${todasCategoriasLocal.getPorCodigo(codigoCategoria)} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                      </c:if>
                                                      <c:if test="${campoAgrupado.name == 'idsClasses'}">
                                                        <c:set var="codigoClasse" value="${valorCampoAgrupado.name}" scope="page" />
                                                        ${todasClassesLocal.getPorCodigo(codigoClasse)} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                      </c:if>
                                                      <c:if test="${campoAgrupado.name == 'bairro'}">
                                                         ${valorCampoAgrupado.name} <span class="label label-info">${valorCampoAgrupado.count}</span>
                                                      </c:if>
                                                    </c:if>
                                                  </label>
                                                </c:otherwise>
                                               </c:choose>
                                            </li>
                                          </c:forEach>
                                          <c:if test="${not empty semClassificacao and semClassificacao > 0}">
                                            <li>
                                              <label>
                                                <input type="checkbox" name="estrelas" value="0" style="display: inline-block;" />
                                                Sem classificação <span class="label label-info">${semClassificacao}</span>
                                              </label>
                                            </li>
                                          </c:if>
                                        <%-- /ul--%>
                                      </div>
                                  </div>
                              </div>
                            </div>
                          </c:otherwise>
                      </c:choose>
                    </c:forEach>
                      
                      <%-- li>
                        <label for="tituloAvaliacao" class="${labelSpanClass}" style="width: 80px; display: inline-table;">
                            <span>
                                Estrelas
                                <!-- i class="icon-question-sign helper" title=""></i-->
                            </span>
                        </label>
                        <select id="select-estrelas" name="estrelas" class="span4" multiple="multiple" style="height: 33px;">
                            <option value="5" selected="selected" image="<c:url value="/resources/images/star5.png"/>"></option>
                            <option value="4" selected="selected" image="<c:url value="/resources/images/star4.png"/>"></option>
                            <option value="3" selected="selected" image="<c:url value="/resources/images/star3.png"/>"></option>
                            <option value="2" selected="selected" image="<c:url value="/resources/images/star2.png"/>"></option>
                            <option value="1" selected="selected" image="<c:url value="/resources/images/star1.png"/>"></option>
                            <option value="0" selected="selected">Sem classificação</option>
                         </select>
                      </li--%>
                      
                      <li style="text-align: center;">
                        <a href="#" class="btn btn-mini limpar-filtro">
                          Limpar filtros
                        </a>
                      </li>
                      
                  </ul>
                  <%-- div align="center">
                    <a id="btn-filtrar" href="#" class="btn btn-info">
                      <i class="icon-filter icon-white"> </i> Filtrar
                    </a>
                  </div--%>
                  
              </div>
           </div>
         </div>
      </c:if>
      <fan:adsBlock adType="1" />
    </div>
    
    </form>
</div>
