<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="sidebar-nav pull-left" style="margin: 0 0 0px 0;">    
    <div class="tabbable tabs-left" style="background-color: rgb(240, 244, 245); border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
    <ul id="ulSidenav" class="nav nav-tabs span3" style="margin-left: 0px; margin-right: 0px; border-right: 0px;">
		<li class="${empty tab ? 'active' : ''} blackAndWhite colorOnHover">
		    <a id="informacoes" href="<c:url value="/locais/${local.urlPath}/informacoes"/>" data-href="${pageContext.request.contextPath}/locais/${local.tipoLocalGeografico ? 'localGeografico' : 'localEnderecavel'}/informacoes/${local.urlPath}" class="menu-item">
				<span class="layout_content" style="display: inline-block; height: 16px; width: 16px;" ></span>
				<span>Capa</span>
		    </a>
		</li>
		<c:if test="${usuario != null}">
		<li class="blackAndWhite colorOnHover">
		    <a id="atividade-amigos" href="<c:url value="/locais/${local.urlPath}/atividade-amigos"/>" data-href="<c:url value="/locais/atividadesAmigos/${local.urlPath}" />" class="menu-item">
				<span class="group" style="display: inline-block; height: 16px; width: 16px;" ></span>
				<span >Atividade dos Amigos</span>
		    </a>
		</li>
		</c:if>
		<li class="blackAndWhite colorOnHover">
			<a id="avaliacoes" href="<c:url value="/locais/${local.urlPath}/avaliacoes"/>" data-href="${pageContext.request.contextPath}/avaliacao/listar/${local.urlPath}?start=0" class="menu-item" data-params="">
		        <span class="award_star_gold_3" style="display: inline-block; height: 16px; width: 16px;" ></span>
		    	Avaliações <fan:menuCounter value="${local.quantidadeAvaliacoes}" badgeType="info" />
			</a>
		</li>
		<li class="blackAndWhite colorOnHover">
			<a id="dicas" href="<c:url value="/locais/${local.urlPath}/dicas"/>" data-href="${pageContext.request.contextPath}/dicas/listar/${local.urlPath}?start=0" class="menu-item" data-params="">
		    	<span class="lightbulb" style="display: inline-block; height: 16px; width: 16px;"></span>
		    	Dicas <fan:menuCounter value="${local.quantidadeDicas}" badgeType="info" />
		    </a>
		</li>                                
		<li class="blackAndWhite colorOnHover">
		    <a id="fotos" href="<c:url value="/locais/${local.urlPath}/fotos"/>" data-href="${pageContext.request.contextPath}/locais/fotos/${local.urlPath}?start=0" class="menu-item" data-params="">
		        <span class="camera" style="display: inline-block; height: 16px; width: 16px;"></span>
				Fotos <fan:menuCounter value="${local.quantidadeFotos}" badgeType="info" />
		    </a>
		</li>
		<li class="blackAndWhite colorOnHover">
		    <a id="perguntas" herf="<c:url value="/locais/${local.urlPath}/perguntas"/>" data-href="${pageContext.request.contextPath}/perguntas/listar/${local.urlPath}?start=0" class="menu-item" data-params="">
				<span class="help" style="display: inline-block; height: 16px; width: 16px;" ></span>
				Perguntas <fan:menuCounter value="${local.quantidadePerguntas}" badgeType="info" />
		    </a>
		</li>
		<c:if test="${local.tipoLocalGeografico}">
			<li class="blackAndWhite colorOnHover">
				<a id="hospedar" href="<c:url value="/locais/${local.urlPath}/hospedar"/>" data-href="${pageContext.request.contextPath}/locais/todos/<%=br.com.fanaticosporviagens.model.entity.LocalType.HOTEL.getUrlPath()%>/${local.urlPath}" class="menu-item">
			    	<span class="hotel" style="display: inline-block; height: 16px; width: 16px;"></span>
			    	Onde se Hospedar <fan:menuCounter value="${local.quantidadeHoteis}" badgeType="info" />
			    </a>
			</li>
			<li class="blackAndWhite colorOnHover">
				<a id="onde-comer" href="<c:url value="/locais/${local.urlPath}/onde-comer"/>" data-href="${pageContext.request.contextPath}/locais/todos/<%=br.com.fanaticosporviagens.model.entity.LocalType.RESTAURANTE.getUrlPath()%>/${local.urlPath}" class="menu-item">
			    	<span class="food" style="display: inline-block; height: 16px; width: 16px;"></span>
			    	Onde Comer <fan:menuCounter value="${local.quantidadeRestaurantes}" badgeType="info" />
			    </a>
			</li>
			<li class="blackAndWhite colorOnHover">
				<a id="oque-fazer" href="<c:url value="/locais/${local.urlPath}/oque-fazer"/>" data-href="${pageContext.request.contextPath}/locais/todos/<%=br.com.fanaticosporviagens.model.entity.LocalType.ATRACAO.getUrlPath()%>/${local.urlPath}" class="menu-item">
			    	<span class="picture" style="display: inline-block; height: 16px; width: 16px;"></span>
			    	O Que Fazer <fan:menuCounter value="${local.quantidadeAtracoes}" badgeType="info" />
			    </a>
			</li>
		</c:if>
    </ul>
    </div>
</div>