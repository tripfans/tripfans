<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<style type="text/css">
  #panoramio_box {
    position: relative;
    margin: 10px 0 20px 10px;
    width: 680px;
  }
  #panoramio_box_list {
    position: absolute;
    left: 655px;
  }
  #panoramio_box .panoramio-wapi-images {
    background-color: #transparent;
  }
  #panoramio_box .pwanoramio-wapi-tos{
    background-color: #transparent !important;
  }
</style>

<c:if test="${not empty local.coordenadaGeografica.latitude}">

    <c:set var="latSW" value="${local.perimetroParaConsultaNoMapa.latitudeSudoeste}" />
    <c:set var="lngSW" value="${local.perimetroParaConsultaNoMapa.longitudeSudoeste}" />
    <c:set var="latNE" value="${local.perimetroParaConsultaNoMapa.latitudeNordeste}" />
    <c:set var="lngNE" value="${local.perimetroParaConsultaNoMapa.longitudeNordeste}" />

    <div class="span16" style="margin-left: 5px;">

      <div class="page-header" style="margin: 0px;">
        <h3>
          Fotos de ${local.nome} no  
          <img src="<c:url value="/resources/images/logos/panoramio.png"/>" height="20" style="margin-top: -3px;" /> 
        </h3>
      </div>
    
      <div style="margin-left: 0px;" align="center">
        <div id="panoramio_box" align="center">
          <div id="panoramio_box_list"></div>
          <div id="panoramio_box_photo_big">
            <div id="panoramio_box_photo"></div>
            <div id="panoramio_box_attr"></div>
          </div>
        </div>
      </div>
    
    </div>
                
    <script type="text/javascript">
      var photo_request = {
          'set' : 'public',
          'rect' : {'sw': {'lat': ${latSW}, 'lng': ${lngSW}}, 'ne': {'lat': ${latNE}, 'lng': ${lngNE}}},
      };
      var photo_widget_options = {
          'width': 640,
          'height': 480,
          //'attributionStyle': panoramio.tos.Style.HIDDEN,
          'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED],
          'croppedPhotos' : panoramio.Cropping.TO_FILL
      };

      var photo_widget = new panoramio.PhotoWidget(
          'panoramio_box_photo', photo_request, photo_widget_options);
              
      var list_widget_options = {
          'width' : 70, 
          'height' : 480,
          'columns': 1,
          'rows': 9,
          'croppedPhotos' : true,
          'disableDefaultEvents': [panoramio.events.EventType.PHOTO_CLICKED],
          'orientation': panoramio.PhotoListWidgetOptions.Orientation.VERTICAL,
          'attributionStyle': panoramio.tos.Style.HIDDEN
      };
      
      var list_widget = new panoramio.PhotoListWidget(
                'panoramio_box_list', photo_request, list_widget_options);
      
      function photoClicked(event) {
          return false;
      }
      
      function listPhotoClicked(event) {
          var position = event.getPosition();
          if (position !== null) {
              $('#tripfans_foto').hide();
              $('#panoramio_box_photo_big').show();
              photo_widget.setPosition(position);
          }
      }
      
      panoramio.events.listen(list_widget, panoramio.events.EventType.PHOTO_CLICKED, listPhotoClicked);
      panoramio.events.listen(photo_widget, panoramio.events.EventType.PHOTO_CLICKED, photoClicked);
      
      photo_widget.enablePreviousArrow(false);
      photo_widget.enableNextArrow(false);
      
      photo_widget.setPosition(${not empty position ? position : 0});
      list_widget.setPosition(${not empty position ? position : 0});      
    </script>
  </div>
</c:if>

<script type="text/javascript">
jQuery(document).ready(function() {
    //$('.panoramio-wapi-photolist').css('width', '100%');
});
</script>