<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="span16" style="margin-top: -15px;">

    <div class="page-header">
        <h3>
          Atividade dos meus Amigos
        </h3> 
    </div>
    
    <c:choose>
      <c:when test="${not empty atividades}">
        <div class="span12" style="margin-left: 0">
          <jsp:include page="/WEB-INF/views/locais/ultimasAtividadesAmigos.jsp" />
        </div>
      </c:when>
      <c:otherwise>
      
        <div class="span12" style="margin-left: 0px;">
          <div class="row">
            <div class="blank-state">
                <div class="span2 offset2">
                    <img src="<c:url value="/resources/images/icons/mini/128/Time-3.png" />" width="90" height="90" />
                </div>    
                <div class="span6" style="margin-top: 20px;">
                    <h3>Não há atividades para exibir</h3>
                    <p>
                        Seus amigos ainda não registraram nenhuma atividade em ${local.nome}
                    </p>
                </div>
            </div>
          </div>
          <div class="horizontalSpacer"></div>
          <div class="row" align="center">
            <h4>Seja o primeiro a compartilhar experiências:</h4>
            <br/>
            <fan:listaAcoesLocais local="${local}" tamanhoTitulo="medium" exibirAcaoAvaliacao="true" exibirAcaoDica="true" exibirAcaoFoto="true" exibirAcaoPergunta="true" />
          </div>
        </div>
        <div class="horizontalSpacer"></div>
      </c:otherwise>
    </c:choose>
</div>