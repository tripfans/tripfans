<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div class="page-header">
	<h3>
	Amigos que já foram a ${local.nome}
	<span class="badge badge-info" style="margin-top: 10px;">${quantidadeAmigosJaForam}</span>
	<c:if test="${false and quantidadeAmigosJaForam > 5}">
	<span>
			<small><a href="<c:url value="" />" >Ver todos amigos que já foram</a></small>
	</span>
	</c:if>
	</h3>
</div>
<fan:listaAmigosJaForamLocal listaAmigos="${interessesAmigosJaForam}" itemSize="span2"/>