<%@page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<div id="listaLocalEnderecavel" style="display: inline-block;">
	<div class="row" style="margin-left: 0px;">
    
	  <c:forEach items="${listaLocais}" var="localIndexado" varStatus="status">
      
        <c:set var="localLista" value="${localIndexado.localEnderecavel}" />
    
        <fan:card type="local" local="${localLista}" spanSize="12" imgSize="4" panoramioWidth="220">
            <jsp:attribute name="subtitle">
                <span class="stylizedText"><a href="${pageContext.request.contextPath}${localLista.url}">${localLista.nome}</a></span>
            </jsp:attribute>
            <jsp:attribute name="info">
               	<p>${localLista.endereco.descricao}
               	<c:if test="${not empty localLista.endereco.cep }">, CEP: ${localLista.endereco.cep}</c:if>
               	</p>
                <c:if test="${not empty localLista.avaliacaoConsolidadaGeral}">
                    <c:if test="${localLista.quantidadeAvaliacoes > 0}">
                      <c:choose>
                        <c:when test="${localLista.quantidadeAvaliacoes == 1}"><c:set var="textoAvaliacoes" value="${localLista.quantidadeAvaliacoes} avaliação" /></c:when>
                        <c:otherwise><c:set var="textoAvaliacoes" value="${localLista.quantidadeAvaliacoes} avaliações" /></c:otherwise>
                      </c:choose>
                      <span title="Classificado como <strong>${localLista.avaliacaoConsolidadaGeral.classificacaoGeral.descricao}</strong> em um total de ${textoAvaliacoes} " class="starsCategory cat${localLista.avaliacaoConsolidadaGeral.classificacaoGeral.codigo}" style="display: inline-block;"></span>
                    </c:if>
                    <p><small>
                    <c:choose>
                        <c:when test="${localLista.quantidadeInteressesJaFoi == 1}"><span class="label label-info">${localLista.quantidadeInteressesJaFoi} já foi</span></c:when>
                        <c:when test="${localLista.quantidadeInteressesJaFoi > 1}"><span class="label label-info">${localLista.quantidadeInteressesJaFoi} já foram</span></c:when>
                    </c:choose>
                    </small></p>
                </c:if>
                 <c:if test="${localLista.tipoLocalInteresseTuristico == false and not empty localLista.classificacoes}">
                 	<strong>Classificação:</strong>
	               	<c:forEach items="${localLista.classificacoes}" var="classificacao" varStatus="count">
	  	             	<%--span class="label label-info">${classificacao.categoria.descricao}</span--%>
	  	             	<c:if test="${not empty classificacao.classe}"> <span class="label label-info">${classificacao.classe.descricao}</span></c:if> 
	               	</c:forEach>
	   			</c:if>            
            </jsp:attribute>
            <jsp:attribute name="actions">
                <div style="border-bottom: 1px solid #eee; height: 2px;"></div>
                <div class="pull-right buttonToolBar btn-toolbar" style="margin-bottom: 0px; margin-right: 5px;">
                    <div class="btn-group">
                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.jaFoi}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.JA_FOI %>" cssButtonClassSize="mini" />
                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.desejaIr}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.DESEJA_IR %>" cssButtonClassSize="mini" />
                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.favorito}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.FAVORITO %>" cssButtonClassSize="mini" />
                        <fan:marcarLocalButton listaMarcados="${marcacoesUsuarioLocal.seguir}" local="${localLista}" tipoMarcacao="<%= TipoInteresseUsuarioEmLocal.SEGUIR %>" cssButtonClassSize="mini" />
                    </div>
                    
                </div> 
            </jsp:attribute>
        </fan:card>    
    
	  </c:forEach>
      
      <c:if test="${empty listaLocais}">
         <div align="center">
             Nenhum${tipoLocalListado.tipoAtracao ? 'a' : ''} ${tipoLocalListado.descricao} econtrad${tipoLocalListado.tipoAtracao ? 'a' : 'o'} com o filtro informado.
         </div>
      </c:if>
	</div>
	
	<div class="span12 row" style="margin: auto; margin-bottom: 5px;">
		<c:url var="urlLocais" value="/locais/todos/${tipoLocalListado.urlPath}/${local.urlPath}" />
		<fan:pagingButton targetId="listaLocalEnderecavel" title="Ver mais ${tipoLocalListado.descricaoPlural}" cssClass="span11" formId="filter-form">
            <jsp:attribute name="noMoreItensActions">
                <p>Faltou algum${tipoLocalListado.tipoAtracao ? 'a' : ''} ${tipoLocalListado.descricao} que você conhece?</p>
                <a href="<c:url value="/locais/sugerirLocal?cidade=${local.urlPath}&tipoLocal=${tipoLocalListado.urlPath}"/>" class="btn btn-secondary">Sugira ${tipoLocalListado.descricaoPlural}</a>
            </jsp:attribute>
        </fan:pagingButton>
	</div>
</div>
