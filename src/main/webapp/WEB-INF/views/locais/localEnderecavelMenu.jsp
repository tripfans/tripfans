<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:choose>
	<c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
      		<c:set var="reqScheme" value="https" />
	</c:when>
	<c:otherwise>
		<c:set var="reqScheme" value="http" />
	</c:otherwise>
</c:choose>

<c:if test="${not unsuportedIEVersion}">
  <script src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR" type="text/javascript"></script>
</c:if>

<%-- Foto do local --%>
<c:if test="${local.fotoPadraoAnonima}">
  <div style="min-height: 160px; background-color: rgb(235, 235, 235);">
    <img id="profile_pic" class="span3" src="${local.fotoPadraoAnonima ? local.urlFotoCapa : local.urlFotoAlbum}" style="${local.fotoPadraoAnonima ? 'height: 120px; width: 120px; margin-top: 16px;' : 'margin-left: 0px; height: 160px;'}" />
  </div>
</c:if>
<%@include file="localMenu.jsp" %>