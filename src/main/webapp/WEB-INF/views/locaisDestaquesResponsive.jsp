<%@ page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<%@ page import="br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="fan" %>

<tiles:insertDefinition name="tripfans.responsive.new">

    <tiles:putAttribute name="stylesheets">
        <c:choose>
            <c:when test="${fn:toLowerCase(pageContext.request.scheme) == 'https'}">
                <c:set var="reqScheme" value="https" />
            </c:when>
            <c:otherwise>
                <c:set var="reqScheme" value="http" />
            </c:otherwise>
        </c:choose>

        <link href='${reqScheme}://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1' rel='stylesheet' type='text/css'>
        <link href='${reqScheme}://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
        
        <!--link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/app/common.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/app/locaisDestaque.css" />" /-->
        
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/components/bootstrap/bootstrap3/css/bootstrap.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/pageResponsive.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/styles/baseResponsive.css" />" />
        <link type="text/css" href="${pageContext.request.contextPath}/resources/styles/jquery-ui/fanaticos/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />
        
        <c:choose>
          <c:when test="${isMobile}">
            <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveMobile.css" type="text/css" media="screen, projection"/>
          </c:when>
          <c:otherwise>
            <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/styles/baseResponsiveDesktop.css" type="text/css" media="screen, projection"/>
          </c:otherwise>
        </c:choose>
        
        <c:if test="${not unsuportedIEVersion}">
          <%-- script type="text/javascript" src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR"></script--%>
        </c:if>
        
        <!-- script type="text/javascript" src="<c:url value="/resources/scripts/app/common.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/scripts/app/locaisDestaque.js"/>"></script-->

        <script type="text/javascript" src="<c:url value="/resources/scripts/jquery-1.7.1.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/scripts/jquery-ui-1.8.16.custom.min.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/components/bootstrap/bootstrap3/js/bootstrap.js"/>"></script>
        
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/scripts/jquery.json-2.3.js"></script>
        <script type="text/javascript" src="<c:url value="/resources/scripts/jquery.ui.fanaticos.autocomplete.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/scripts/fanaticos.js"/>" ></script>

        <script type="text/javascript" src="<c:url value="/resources/scripts/jquery.jscroll/jquery.jscroll.js"/>" ></script>
        
        <c:if test="${not unsuportedIEVersion}">
          <script type="text/javascript" src="${reqScheme}://www.panoramio.com/wapi/wapi.js?v=1&hl=pt_BR"></script>
        </c:if>
        
        <meta property="og:title" content="TripFans | Fan�ticos por Viagens" />
        <meta property="og:url" content="http://www.tripfans.com.br" />
        <meta property="og:image" content="http://www.tripfans.com.br/resources/images/logos/tripfans-vertical.png" />
        <meta property="og:site_name" content="TripFans" />
        <meta property="og:description" content="O TripFans � a sua rede social de viagens. Encontre destinos, planeje suas viagens, crie di�rios, aproveite ofertas e interaja com seus amigos por meio de dicas, recomenda��es e avalia��es" />
        
    </tiles:putAttribute>
    
    <tiles:putAttribute name="footer">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="leftMenu">
    </tiles:putAttribute>
    
    <tiles:putAttribute name="body">
    
         <div class="container-fluid" style="position: relative; min-height: 100%; margin: 0 auto; background: url(<c:url value="/resources/images/wash-white-30.png" />);">
            <div class="row-fluid">
                <div class="borda-arredondada module-box" style="margin-bottom: 20px;">
                    <div>
                        <div class="row lista-locais">
                            <div class="col-md-12 infinite-scroll">
                                <%@include file="listaLocaisDestaques.jsp" %>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="terms-of-service"></div>
        
    </tiles:putAttribute>
    
</tiles:insertDefinition>


   <script>
       <c:if test="${not unsuportedIEVersion}">
           var terms_widget = new panoramio.TermsOfServiceWidget('terms-of-service', {'width': '100%'});
       </c:if>
       
       $('.infinite-scroll').jscroll({
            loadingHtml: '<img src="<c:url value="/resources/images/loading.gif"/>" alt="Carregando" /> Carregando...',
            padding: 200,
            nextSelector: 'a.jscroll-next:last',
            //contentSelector: 'li'
        });
   </script>
