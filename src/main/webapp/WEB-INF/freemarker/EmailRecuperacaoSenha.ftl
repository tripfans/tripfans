<#ftl encoding="UTF-8" />
<#import "/spring.ftl" as spring />

<html>
<body>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td align="center">
          <table style="background-color: #9edbf6; border:1px solid rgb(204,204,204)" border="0" cellpadding="0" cellspacing="25" width="580">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="90%">
                    <tbody>
                      <tr>
                        <td style="min-height:70px" align="left" valign="bottom">
                          <!--img src='cid:logo'-->
                          <img src="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfanslogo.png" alt="TripFans" />
                          <br/>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="590" border="0" cellspacing="0" cellpadding="0" bgcolor="#fefefe" style="border:solid 1px #dddddd;border-radius:5px">
                            <tbody>
                              <tr>
                                <td colspan="3">
                                  <table width="1" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                      <tr>
                                        <td>
                                          <div style="min-height:15px;font-size:15px;line-height:15px">&nbsp;</div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td width="100%" style="padding: 0 15px 15px 15px;">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                          <td align="left">
                                          
                                             <div style="color:#333333;font-family:Arial,sans-serif;font-size:14px"> 
                                                 <h3>Olá ${usuario.nomeExibicao},</h3>
                                             </div>
                                             
                                             <div style="color:#333333;font-family:Arial,sans-serif;font-size:12px">
                                             
                                                Você solicitou a redefinição de sua senha no TripFans.
                                                <br/>
                                                
                                                Para completar o processo, você deve informar o código abaixo na tela de recuperação de senha do TripFans. 
                                                
                                                <br/>
                                                
                                                <h2>${codigoRecuperacao}</h2>

                                                Se você não solicitou uma nova senha, por favor entre em contato conosco: <a href="mailto:suporte@tripfans.com.br">suporte@tripfans.com.br</a>
                                                
                                                <br/>
                                                <br/>
                                                Equipe TripFans
                                                <br/>
                                                <br/>
                                            </div>
                                          </td>
                                        </tr>
                                    
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </td>
                      </tr>
                      
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
</table>

</body>

</html>