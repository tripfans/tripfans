<#ftl encoding="UTF-8" />
<#import "/spring.ftl" as spring />

<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
      <tr>
        <td align="center">
          <table style="background-color: #9edbf6; border:1px solid rgb(204,204,204)" border="0" cellpadding="0" cellspacing="25" width="580">
            <tbody>
              <tr>
                <td>
                  <table border="0" cellpadding="0" cellspacing="0" width="90%">
                    <tbody>
                      <tr>
                        <td style="min-height:70px" align="left" valign="bottom">
                          <!--img src='cid:logo'-->
                          <img src="${tripFansEnviroment.serverUrl}/resources/images/logos/tripfanslogo.png" />
                          <br/>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <table width="590" border="0" cellspacing="0" cellpadding="0" bgcolor="#fefefe" style="border:solid 1px #dddddd;border-radius:5px">
					        <tbody>
					          <tr>
					            <td colspan="3">
					              <table width="1" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
					                  <tr>
					                    <td>
					                      <div style="min-height:15px;font-size:15px;line-height:15px">&nbsp;</div>
					                    </td>
					                  </tr>
					                </tbody>
					              </table>
					            </td>
					          </tr>
					          <tr>
					            <td width="100%" style="padding-left: 15px;">
					              <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                <tbody>
						                <tr>
						                  <td align="left" style="color:#333333;font-family:Arial,sans-serif;font-size:14px">
						                  
						                     <strong>
						                       <@spring.message "mail.template.saudacao" />
						                       <#if destinatario??>
												  ${destinatario.primeiroNome!},
											   <#elseif nomeDestinatario??>
												  ${nomeDestinatario},
											   <#else>
											   ,
											   </#if>
						                     </strong>
						                     
										  </td>
						                </tr>
						            
						                <tr>
						                  <td align="left">
						                    <table width="100%" border="0" cellspacing="15" cellpadding="0">
						                      <tbody>
						                        <tr>
						                          <td valign="top" style="font-family:Arial;color:#333333">
						                            <div style="font-size:12px">
						                              <#if autor??><strong>${autor.nomeExibicao}</strong></#if> ${textoAcao}
						                            </div>
						                          </td>
						                        </tr>

						                        <tr>
						                          <td valign="top" style="font-family:Arial;color:#333333">
						                            <div style="font-size:12px">
						                              <a href="${tripFansEnviroment.serverUrl}/${url}">${textoLink!"Clique aqui para ver"}</a>
						                            </div>
						                          </td>
						                        </tr>
						                        
						                      </tbody>
						                    </table>
						                  </td>
						                </tr>
					                </tbody>
					              </table>
				                </td>
				              </tr>
					          
					          
					        </tbody>
					      </table>
                        </td>
                      </tr>
                      
                      <#if hideFooter??>
                      
                      <#else>
	                      <tr>
						          
							<table cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;width:590px">
							    <tbody>
							        <tr>
							            <td style="border-right:none;color:#666;font-size:11px;border-bottom:none;font-family:'lucida grande',tahoma,verdana,arial,sans-serif;border:none;border-top:none;padding-top: 15px;border-left:none">
							                <@spring.message "mail.template.footer.informacao.parte1" />
							                <@spring.message "mail.template.footer.informacao.parte2" /> 
							                <a href="mailto:${destinatario.username}" target="_blank">
							                    ${destinatario.username}
							                </a>
							                .
							                <br/>
							                <@spring.message "mail.template.footer.informacao.parte4" />
							                <br/>
							                <@spring.message "mail.template.footer.informacao.parte5" />
							                <a href="${tripFansEnviroment.serverUrl}/conta/minhaConta?tab=notificacoes" target="_blank">
							                    <@spring.message "mail.template.footer.informacao.parte6" />
							                </a>.
							            </td>
							        </tr>
							    </tbody>
							</table>
												          
				          </tr>
			          </#if>
			          
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
</table>