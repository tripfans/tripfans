angular.module('tripfansApp').directive('loadingContainer', function() {
    return {
        restrict : 'A',
        scope : false,
        link : function(scope, element, attrs) {
            var loadingLayer = angular.element('<div class="loading"></div>');
            
            loadingLayer.append('<div layout="row" layout-sm="column" layout-align="space-around" class="layout-sm-column layout-align-space-around-stretch layout-row"><md-progress-circular md-mode="indeterminate" md-diameter="96" aria-valuemin="0" aria-valuemax="100" role="progressbar" class="ng-scope" style="width: 96px; height: 96px;"><div class="md-scale-wrapper md-mode-indeterminate"><div class="md-spinner-wrapper"><div class="md-inner"><div class="md-gap"></div><div class="md-left"><div class="md-half-circle"></div></div><div class="md-right"><div class="md-half-circle"></div></div></div></div></div></md-progress-circular></div>')
            
            element.append(loadingLayer);
            element.addClass('loading-container');
            scope.$watch(attrs.loadingContainer, function(value) {
            	loadingLayer.toggleClass('ng-hide', !value);
            });
        }
    };
});