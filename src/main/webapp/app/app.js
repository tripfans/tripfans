var app = angular.module('tripfansApp', [
    'ngResource',
    'ngCookies',
    'ui.router',
    'ui.router.router',
    'ngStorage',
    'ngMaterial',
    'ngMessages',  
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap-slider',
    'daterangepicker'
]);

app.config(function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $mdIconProvider, $mdThemingProvider, $mdDateLocaleProvider) {
    
    $urlRouterProvider.otherwise('/');

    $stateProvider.state('main', {
        'abstract': true,
    });
    
    $mdThemingProvider.definePalette('custom-blue', {
        '50': 'ffebee',
        '100': 'ffcdd2',
        '200': 'ef9a9a',
        '300': 'e57373',
        '400': 'ef5350',
        '500': '1B86B5',
        '600': 'e53935',
        '700': 'd32f2f',
        '800': 'c62828',
        '900': 'b71c1c',
        'A100': 'ff8a80',
        'A200': 'ff5252',
        'A400': 'ff1744',
        'A700': 'd50000',
        'contrastDefaultColor': 'light',    // whether, by default, text (contrast)
                                            // on this palette should be dark or light
        'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
         '200', '300', '400', 'A100'],
        'contrastLightColors': undefined    // could also specify this if default was 'dark'
    });
    
    $mdThemingProvider.theme('default')
        .primaryPalette('custom-blue', {
          'default': '500', // by default use for primary intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
        })
        .accentPalette('teal', {
          'default': '600', // by default use for secondary intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
        })
        .warnPalette('amber', {
          'default': '300', // by default use or warn intentions
          'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
          'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
          'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
        });
        /*.primaryPalette('indigo')
        .accentPalette('orange')
        .warnPalette('red');*/
    
    $mdDateLocaleProvider.parseDate = function(dateString) {
        var m = moment(dateString, 'L', true);
        return m.isValid() ? m.toDate() : new Date(NaN);
    };
    
    $mdDateLocaleProvider.formatDate = function(date) {
        if (date) {
            return moment(date).format('L');
        }
        return '';
    };

});

app.run(function($rootScope, $location, $window, $http, $state, $urlRouter) {
    
    $rootScope.ENV = 'dev';
    $rootScope.VERSION = '1.0';
    $rootScope.EAN_WEBSITE_ID = '7944491';
    // Se esse valor for alterado, deverá ser alterado no Serviço Java também (constante na classe HotelService.java)
    $rootScope.DEFAULT_NUMBER_OF_RESULTS = 20;

    $rootScope.ANO = moment(new Date()).format('YYYY');
    
    $rootScope.$on('$locationChangeStart', function (event, toState, toStateParams) {
    });
    
    $rootScope.$on('$stateChangeStart',  function(event, toState, toParams, fromState, fromParams) {
    });
    
    $rootScope.$on('$stateNotFound', function(event, unfoldState, toParams, fromState, fromParams) {
    });
    
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, error) {
        $rootScope.previousStateName = fromState.name;
        $rootScope.previousStateParams = fromParams;
    });
    
    $rootScope.back = function() {
        // If previous state is 'activate' or do not exist go to 'home'
        if ($rootScope.previousStateName === 'activate' || $state.get($rootScope.previousStateName) === null) {
            $state.go('home');
        } else {
            $state.go($rootScope.previousStateName, $rootScope.previousStateParams);
        }
    };
    
});

app.filter('html', ['$sce', function($sce) {
    var div = document.createElement('div');
    return function(text) {
        div.innerHTML = text;
        return $sce.trustAsHtml(div.textContent);
    };
}]);