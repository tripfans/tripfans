angular.module('tripfansApp')
    .config(function ($stateProvider) {
        
        $stateProvider.state('viagem', {
            parent: 'main',
            url: '/viagem?destinos&dataInicio&dataFim',
            views: {
                'content@': {
                    templateUrl: 'app/viagem/viagem.html',
                    controller: 'ViagemController',
                    controllerAs: 'viagemCtrl'
                }
            },
            resolve: {
                /*origem: function($q) {
                    var deferred = $q.defer();
                    
                    // Informações sobre localização baseada em IP (Pago a partir de 1000 requests por dia)
                    $.get("http://ipinfo.io", function(response) {
                        console.log(response.city);
                        console.log(response.city, response.country);
                    }, "jsonp");
                    
                    // Localização a partir de Geolocation do navegador (requer permissão)
                    // check for Geolocation support
                    if (navigator.geolocation) {
                        var startPos;
                        var geoOptions = {
                            // evitar busca frequente da localizacao
                            maximumAge: 5 * 60 * 1000,
                            // timeout 
                            timeout: 10 * 1000,
                            enableHighAccuracy: false
                        }
                        var geoSuccess = function(position) {
                            deferred.resolve(position);
                        };
                        var geoError = function(error) {
                            // error.code can be:
                            //   0: unknown error
                            //   1: permission denied
                            //   2: position unavailable (error response from location provider)
                            //   3: timed out
                            console.log('Erro ao recuperar Geolocalização. Código do erro: ' + error.code);
                        }
                        navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
                        
                        return deferred.promise;
                    }
                },*/
                destinos: function($q, $stateParams, ViagemService) {
                    var deferred = $q.defer();
                    
                    ViagemService.consultarRegioesPorIds($stateParams.destinos).then(function (response) {
                        deferred.resolve(response.data.records);
                    }, function (response) {
                    });
                    
                    return deferred.promise;
                }, 
                dataInicio: function($q, $stateParams) {
                    var dataInicio = moment($stateParams.dataInicio, 'DD-MM-YYYY', true);
                    if (!dataInicio.isValid() || dataInicio.isSameOrBefore(moment())) {
                        return moment(new Date()).add(1, 'days').format('DD-MM-YYYY');
                    }
                    return $stateParams.dataInicio
                },
                dataFim: function($q, $stateParams) {
                    var dataInicio = moment($stateParams.dataInicio, 'DD-MM-YYYY', true);
                    var dataFim = moment($stateParams.dataFim, 'DD-MM-YYYY', true);
                    
                    if (!dataInicio.isValid() || dataInicio.isSameOrBefore(moment())) {
                        dataInicio = moment(new Date()).add(1, 'days');
                    }
                    
                    if (!dataFim.isValid() || dataFim.isSameOrBefore(dataInicio)) {
                        return moment(dataInicio).add(3, 'days').format('DD-MM-YYYY');
                    }
                    return $stateParams.dataFim
                },
                quartos: function($q, $stateParams) {
                    return [{
                        adultos: 2,
                        criancas: {
                            quantidade: 0,
                            idades: []
                        }
                    }]
                }
            }
        })
        
        $stateProvider.state('verOferta', {
            url: '/viagem/verOferta?url',
            views: {
                'content@': {
                    templateUrl: 'app/viagem/redirecionador.html',
                    controller: function($scope, $stateParams, $window, ViagemService) {
                        var url = decodeURIComponent($stateParams.url);
                        $scope.redirecionarParaRerservaParceiro = function() {
                            ViagemService.redirecionarParaRerservaParceiro(url).then(function (response) {
                                var url = response.data.records[0];
                                $window.location.href = url;
                            });
                        }
 
                    }
                }
            }
        });
        
        function getUrlReserva(parceiro, destino, hotelId) {
            // URL base
            var urlBase = 'http://www.dpbolvw.net/click-' + $rootScope.EAN_WEBSITE_ID +'-10479934-1420669072000';
            // URL a ser redirecionada - http://www.expedia.com/pubspec/scripts/eap.asp
            var urlEAN = 'http://www.expedia.com.br/pubspec/scripts/eap.asp';
            // Id do Hotel
            urlEAN += '?PRID=1&GOTO=hotelinfo&hotelid=' + hotelId;
            // Datas - MM/DD/YYYY
            urlEAN += '&InDate=' + moment(destino.dataInicio).format('MM/DD/YYYY') + '&OutDate=' + moment(destino.dataFim).format('MM/DD/YYYY');
            
            // Quartos
            if (self.viagem.quartos.length > 0) {
                urlEAN += '&NumRoom=' + self.viagem.quartos.length;
                
                var urlQuartos = '';
                if (self.viagem.quartos) {
                    urlQuartos = '&'
                    self.viagem.quartos.forEach(function(quarto, indice) {
                        urlQuartos += 'NumAdult' + (indice + 1) + '=' + quarto.adultos;
                        if (quarto.criancas.quantidade > 0) {
                            urlQuartos += '&NumChild' + (indice + 1) + '=' + quarto.criancas.quantidade;
                            quarto.criancas.idades.forEach(function(idade, index) {
                                urlQuartos += '&Child' + (index + 1) +'Age=' + idade;
                            })
                        }
                    })
                }
            }
            
            var urlReserva = urlBase + '?url=' + encodeURIComponent(urlEAN) + '&cjsku=' + hotelId + '#rooms-and-rates'
            
            return urlReserva;
        }
        
    })
    
    .controller('ViagemController', function ($rootScope, $scope, $state, $stateParams, $http, $q, $window, ViagemService, destinos, dataInicio, dataFim, quartos) {
    
        var self = this;
        
        //self.destinos = carregarDestinos();
        
        self.indiceDestinoSelecionado = 0;
        
        self.destinosDisponiveis = carregarDestinos();
        
        self.viagem = {
            destinos: destinos,
            dataInicio: dataInicio.replace(/-/g, '/'),
            dataFim: dataFim.replace(/-/g, '/'),
            quartos: quartos
        }
        
        self.filters = {
            
        }

        self.sortNames = [
            {
                type: 'OVERALL_VALUE',
                label: 'Recomendados'
            },
            {
                type: 'PRICE',
                label: 'Menor Preço'
            },
            {
                type: 'PRICE_REVERSE',
                label: 'Maior Preço'
            },
            {
                type: 'TRIP_ADVISOR',
                label: 'Melhores Avaliados'
            },
            {
                type: 'QUALITY',
                label: 'Estrelas (de 5 a 1)'
            },
            {
                type: 'QUALITY_REVERSE',
                label: 'Estrelas (de 1 a 5)'
            }
        ];
        
        self.filters['sortType'] = [];
        self.filters['sortType'].push(self.sortNames[0].type)
        self.ordemSelecionada = self.sortNames[0].label;
        
        self.periodoSelecionado = {
            startDate: moment(self.viagem.dataInicio, 'DD-MM-YYYY'),
            endDate: moment(self.viagem.dataFim, 'DD-MM-YYYY')
        };
        
        self.daterangeOptions = {
            autoApply: true,
            autoUpdateInput: true,
            minDate: new Date(),
            locale: {
                format: 'DD MMM YYYY',
                cancelLabel: 'Cancelar',
            }
        };
        
        $scope.$watch('viagemCtrl.periodoSelecionado', function(value) {
            atualizarDiasCadaDestino(self.getQuantidadeTotalDias());
            atualizarDatasDestinos();
        });
        
        self.atualizarDiasDestino = function(destino) {
            atualizarDatasDestinos();
        }
        
        self.getQuantidadeTotalDias = function() {
            var inicio = self.periodoSelecionado.startDate;
            var fim = self.periodoSelecionado.endDate;
            var totalDias = fim.diff(inicio, 'days') + 1;
    
            //atualizarDiasCadaDestino(totalDias);
            return totalDias;
        }
        
        function atualizarDiasCadaDestino(totalDias) {
            if (self.viagem.destinos.length > 1) {
                var qtdDiasDestinos = 0;
                self.viagem.destinos.forEach(function(destino) {
                    if (destino.quantidadeDias == null) {
                        destino.quantidadeDias = parseInt(totalDias - (totalDias / self.viagem.destinos.length));
                        qtdDiasDestinos += destino.quantidadeDias;
                    }
                })
                // Se sobrar dias livres, adicioná-los ao ultimo destino
                if (qtdDiasDestinos > 0 && qtdDiasDestinos < totalDias) {
                    self.viagem.destinos[self.viagem.destinos.length -1].quantidadeDias += totalDias - qtdDiasDestinos;
                }
                    
            } else if (self.viagem.destinos[0]) {
                self.viagem.destinos[0].quantidadeDias = totalDias;
            }
        }
        
        function atualizarDatasDestinos() {
            
            var dataInicial = null;
            var dataFinal = null;
            
            self.viagem.destinos.forEach(function(destino) {
                
                if (destino.quantidadeDias == null) {
                    atualizarDiasCadaDestino(self.getQuantidadeTotalDias());
                }
                var qtdDiasDestino = destino.quantidadeDias;
                
                if (dataInicial == null) {
                    dataInicial = self.periodoSelecionado.startDate.clone();
                    dataFinal = dataInicial.clone().add(qtdDiasDestino, 'days');
                } else {
                    dataInicial = dataFinal.clone();
                    dataFinal = dataInicial.clone().add(qtdDiasDestino, 'days');
                }
                
                destino.dataInicio = dataInicial.toDate();
                destino.dataFim = dataFinal.toDate();
                
            });
        }
        
        self.getQuantidadePessoasQuarto = function(quarto) {
            var quantidade = 0;
            if (quarto) {
                quantidade = quarto.adultos + quarto.criancas.quantidade;
            }
            return quantidade
        }
        
        self.getTotalViajantes = function() {
            var total = 0;
            self.viagem.quartos.forEach(function(quarto) {
                total += self.getQuantidadePessoasQuarto(quarto);
            })
            return total;
        }
        
        self.adicionarQuarto = function() {
            self.viagem.quartos.push({
                numero: self.viagem.quartos.length + 1,
                adultos: 2,
                criancas: {
                    quantidade: 0,
                    idades: []
                }
            })
        }
    
        self.removerQuarto = function(indice) {
            self.viagem.quartos.splice(indice, 1)
        }
        
        self.adicionarCriancas = function(indice) {
            //self.viagem.quartos[indice].criancas
        }
        
        self.adicionarDestino = function(destino) {
            //destino.city = destino.city + '1'
            self.viagem.destinos.push(destino);
        }
    
        self.selecionarFiltro =  function(nomeFiltro, valor) {
            self.filters[nomeFiltro] = valor;
        }
        
        self.selecionarOrdem =  function(sort) {
            self.selecionarFiltro('sortType', sort.type)
            self.ordemSelecionada = sort.label;
        }
        
        self.selecionarFiltros =  function(nomeFiltro, valor) {
            if (self.filters[nomeFiltro] == null || valor == 0) {
                self.filters[nomeFiltro] = [];
            }
            
            if (valor != 0) {
                var idx = self.filters[nomeFiltro].indexOf(valor);
                if (idx > -1) {
                    self.filters[nomeFiltro].splice(idx, 1);
                } else {
                    self.filters[nomeFiltro].push(valor);
                }
            }
        }
        
        self.filtroSelecionado = function (nomeFiltro, valor) {
            if (self.filters[nomeFiltro]) {
                if (angular.isArray(self.filters[nomeFiltro])) {
                    return self.filters[nomeFiltro].indexOf(valor) > -1;
                } else {
                    return self.filters[nomeFiltro] == valor;
                }
            }
            return false;
        };
        
        self.changeStarsFilter = function (slideEvt) {
            if (slideEvt) {
                $("#filter-star-min").text(slideEvt[0]);
                $("#filter-star-max").text(slideEvt[1]);
            }
        }

        self.changeRatingFilter = function (slideEvt) {
            if (slideEvt) {
                $("#filter-rating-min").text(slideEvt[0]);
                $("#filter-rating-max").text(slideEvt[1]);
            }
        }
        
        self.changePriceFilter = function (slideEvt) {
            if (slideEvt) {
                $("#filter-price-min").text(slideEvt[0]);
                $("#filter-price-max").text(slideEvt[1]);
            }
        }
        
        $scope.range = function(n) {
            return new Array(parseInt(n));
        };
        
        self.pesquisarHoteis = function(destino) {
            if (destino == null) {
                destino = self.viagem.destinos[self.indiceDestinoSelecionado];            
            }
            destino.loading = true;
            destino.hotelSummary = [];
            
            destino.roomOffers = [];
            
            ViagemService.pesquisarHoteis(montarUrlPesquisa(destino), self.filters).then(function (response) {
                
                destino.roomOffers = response.data.records[0].results;
                destino.expediaResponseData = response.data.records[0].expediaResponseData;
                destino.page = 1;
                destino.loading = false;                
            }, function (response) {
                destino.loading = false;
            });
        }
        
        self.carregarMaisHoteis = function(destino) {
            if (destino == null) {
                destino = self.viagem.destinos[self.indiceDestinoSelecionado];            
            }
            
            destino.page += 1;
            destino.loadingMore = true;
            
            self.filters['pagination.page'] = destino.page;
            self.filters['pagination.cacheKey'] = destino.expediaResponseData.cacheKey;
            self.filters['pagination.cacheLocation'] = destino.expediaResponseData.cacheLocation;
            
            ViagemService.pesquisarHoteis(montarUrlPesquisa(destino), self.filters).then(function (response) {
                destino.roomOffers = destino.roomOffers.concat(response.data.records[0].results)
                destino.expediaResponseData = response.data.records[0].expediaResponseData;
                destino.loadingMore = false;
            }, function (response) {
                destino.loadingMore = false;
            });            
        }
        
        /*self.getTotalPages = function(destino) {
            if (destino == null) {
                destino = self.viagem.destinos[self.indiceDestinoSelecionado];            
            }
            if (destino.totalResults && destino.totalResults > 0) {
                return parseInt(destino.totalResults / $rootScope.DEFAULT_NUMBER_OF_RESULTS);
            }
            return null;
        }*/
        
        self.redirecionarParaRerservaParceiro = function() {
            
            /*$scope.window = $window.open('', '_blank');
            angular.element($scope.window.document.body).append($compile($element.contents())($scope));*/
            
            ViagemService.redirecionarParaRerservaParceiro(montarUrlReserva($stateParams.parceiro, $stateParams.hotelId)).then(function (response) {
                var url = response.data.records[0];
                $window.location.href = url;
            });
        }
        
        self.getDestino = function(index) {
            return self.viagem.destinos[index];
        }
        
        self.consultarDestinos = function(query) {
            
            var deferred = $q.defer();
            
            ViagemService.pesquisarDestinos(query).then(function (response) {
                deferred.resolve(response.data.records);
            }, function (response) {
            });
            
            return deferred.promise;
            
            /*var resultados = query ? this.destinosDisponiveis.filter(createFilterFor(query)) : [];
            return resultados;*/
        }
        
        function createFilterFor(query) {
            query = angular.lowercase(query);
            return function filterFn(destino) {
                return (destino._lowerNome.indexOf(query) === 0) ||
                    (destino._lowerPais.indexOf(query) === 0);
            };
        }
        
        function carregarDestinos() {
            var destinos = [{
                            'regionID': '2693',
                            'city': 'Orlando',
                            'stateProvinceCode': 'US-FL',
                            'countryCode': 'US',
                            roomOffers: []
                        },{
                            'regionID': '2621',
                            'city': 'Nova York',
                            'stateProvinceCode': 'US-NY',
                            'countryCode': 'US',
                            roomOffers: []
                        }]
            
            return destinos.map(function (destino) {
                destino._lowerNome = destino.city.toLowerCase();
                destino._lowerPais = destino.countryCode.toLowerCase();
                return destino;
            });
        }
        
        self.getDataInicio = function(destino) {
            if (destino) {
                return '';
            }
            return self.periodoSelecionado.startDate;
        }
        
        self.getDataFim = function(destino) {
            self.viagem.dataFim = $('input[name=dataFim]').val();
            return $('input[name=dataFim]').val();
        }
        
        // Realizar a pesquisa ao carregar a pagina
        self.pesquisarHoteis(self.viagem.destinos[0]);
        
        function montarUrlPesquisa(destino) {
            var urlBase = '/tripfans/hoteis/search';
            var urlDestino = '/' + destino.id;
            
            var urlDatas = montarUrlDatas(self.getDestino(self.indiceDestinoSelecionado));
            
            var urlQuartos = (urlDatas ? '&' : '?') + montarUrlQuartos();
            
            return urlBase + urlDestino + urlDatas + urlQuartos
        }
        
        self.redirecionarParaRerserva = function(parceiro, hotelId, directLink) {
            var url = $state.href('verOferta', {url: montarUrlReserva(parceiro, hotelId, directLink)});
            window.open(url,'_blank');
        }
        
        function montarUrlReserva(parceiro, hotelId, directLink) {
            var urlBase = '/tripfans/hoteis/verOferta/' + parceiro + '/' + hotelId;
            
            var urlDatas = montarUrlDatas(self.getDestino(self.indiceDestinoSelecionado));
            
            var urlQuartos = (urlDatas ? '&' : '?') + montarUrlQuartos();
            
            var urlCompleta = urlBase + urlDatas + urlQuartos;

            if (directLink) {
                urlCompleta += '&directLink=' + directLink;
            }
            
            return urlCompleta
        }
        
        function montarUrlDatas(destino) {
            var urlDatas = '';
            
            var dataInicio = self.periodoSelecionado.startDate;
            var dataFim = self.periodoSelecionado.endDate;
            
            if (destino) {
                atualizarDatasDestinos();
                dataInicio = moment(destino.dataInicio);
                dataFim = moment(destino.dataFim);
            }
            
            if (dataInicio) {
                var m = dataInicio;
                
                urlDatas += '?arrivalDate=' + (m.isValid() ? m.format('MM/DD/YYYY') : '');
                if (dataFim) {
                    m = dataFim;
                    urlDatas += '&departureDate=' + (m.isValid() ? m.format('MM/DD/YYYY') : '');
                }
            }
            return urlDatas;
        }
        
        function montarUrlQuartos() {
            var urlQuartos = '';
            if (self.viagem.quartos) {
                self.viagem.quartos.forEach(function(quarto, indice) {
                    urlQuartos += '&rooms[' + indice + '].numberOfAdults=' + quarto.adultos;
                    if (quarto.criancas.quantidade > 0) {
                        urlQuartos += '&rooms[' + indice + '].childAges=' + quarto.criancas.idades.join(',');
                    }
                });
            }
            return urlQuartos;
        }
        
        // Chama a pesquisa de Hoteis
        //self.pesquisarHoteis();
        
    });