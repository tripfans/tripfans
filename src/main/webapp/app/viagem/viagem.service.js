angular.module('tripfansApp')
    .factory('ViagemService', function($http, $resource, $q) {
        
        return {
            
            pesquisarHoteis: function(url, filtros) {
                return $http({
                    method: 'GET',
                    url: url,
                    params: filtros
                })
            },

            carregarMaisHoteis: function(paginacao) {
                return $http({
                    method: 'GET',
                    url: 'hoteis/moreResults/' + paginacao.cacheKey + '/' + paginacao.cacheLocation + '/' + paginacao.supplierType,
                })
            },
            
            redirecionarParaRerservaParceiro: function(url) {
                return $http({
                    method: 'GET',
                    url: url
                })
            },
            
            pesquisarDestinos: function(nome) {
                return $http({
                    method: 'GET',
                    url: 'destinos/consultarRegioes',
                    params: {
                        term: nome
                    }
                        
                })
            },
            
            consultarRegioesPorIds: function(ids) {
                console.log('consultarRegioesPorIds')
                
                return $http({
                    method: 'GET',
                    url: 'destinos/consultarRegioesPorIds',
                    params: {
                        ids: ids
                    }
                        
                })
            }
            
        }
        
    });
        