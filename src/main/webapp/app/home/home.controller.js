angular.module('tripfansApp')
    .config(function ($stateProvider) {
        
        $stateProvider.state('home', {
            parent: 'main',
            url: '/',
            views: {
                'content@': {
                    templateUrl: 'app/home/home.html',
                    controller: 'HomeController',
                    controllerAs: 'homeCtrl'
                }
            },
            resolve: {
                
            }
        })
        
    })

    .controller('HomeController', HomeController);

function HomeController ($state, $stateParams, $http, $q, ViagemService) {
    
    var self = this;

    self.destinoSelecionado = null;
    self.textoBusca = null;
    
    self.destinos = [];
    
    self.destinosSelecionados = [];
    
    self.periodoSelecionado = {
        startDate: null,
        endDate: null
    };
    
    self.daterangeOptions = {
        autoApply: true,
        autoUpdateInput: true,
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Cancelar',
        }
    };    
    
    self.dataInicio = null;
        
    self.dataFim = null;
    
    self.criarViagem = function() {
        
        //console.log(formCriarViagem)
        //console.log(formCriarViagem.$valid)
        
        //if (formCriarViagem.$valid) {
        
            console.log(self.destinosSelecionados)
            console.log(self.periodoSelecionado)
            self.dataInicio = $('input[name="dataInicio"]').val();
            self.dataFim = $('input[name="dataFim"]').val();
            
            var idsRegioesTripfans = [];
            self.destinosSelecionados.forEach(function(destino) {
                idsRegioesTripfans.push(destino.id);
            });
            
            idsRegioesTripfans = idsRegioesTripfans.join(',') + '';

            console.log('vai ' + idsRegioesTripfans)
            console.log('dataInicio ' + self.periodoSelecionado.startDate.format('DD-MM-YYYY'))
            console.log('dataFim ' + self.periodoSelecionado.endDate.format('DD-MM-YYYY'))
            $state.transitionTo('viagem', {
                destinos: idsRegioesTripfans,
                //datas: self.periodoSelecionado
                dataInicio: self.periodoSelecionado.startDate.format('DD-MM-YYYY'),
                dataFim: self.periodoSelecionado.endDate.format('DD-MM-YYYY')
            });
            console.log('foi')
        
        //}
    }
    
    self.consultarDestinos = function(query) {
        
        var deferred = $q.defer();
        
        ViagemService.pesquisarDestinos(query).then(function (response) {
            deferred.resolve(response.data.records);
            console.log(response.data.records)
        }, function (response) {
        });
        
        return deferred.promise;
    }
    
    
}