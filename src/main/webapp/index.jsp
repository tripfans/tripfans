<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TripFans</title>
<link rel="shortcut icon" href="${pageContext.request.contextPath}/images/tripfans.ico">
</head>
<body style="background-image: url('${pageContext.request.contextPath}/images/ceu-bg.jpg'); background-attachment: fixed; background-repeat: no-repeat; background-color: #e2fafe;">

<div style="position: relative;padding-top: 10%;">
	<img style="display: block; margin-left: auto; margin-right: auto;" src="${pageContext.request.contextPath}/resources/images/logos/tripfans-vertical.png" />
	<div style="text-align: center; padding-top: 20px;">
		<h3 style="font-family: Trebuchet MS,Liberation Sans,DejaVu Sans,sans-serif;">
		Estamos trabalhando para fazer um TripFans ainda melhor.<br/>
		Aguardem, em breve um novo TripFans para vocês.
		</h3>
	</div>
</div>

</body>
</html>