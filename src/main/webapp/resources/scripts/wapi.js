var CPWA, CPWAa = this, CPWAaa = function() {
}, CPWAba = function(a) {
	var b = typeof a;
	if ("object" == b)
		if (a) {
			if (a instanceof Array)
				return "array";
			if (a instanceof Object)
				return b;
			var c = Object.prototype.toString.call(a);
			if ("[object Window]" == c)
				return "object";
			if ("[object Array]" == c || "number" == typeof a.length
					&& "undefined" != typeof a.splice
					&& "undefined" != typeof a.propertyIsEnumerable
					&& !a.propertyIsEnumerable("splice"))
				return "array";
			if ("[object Function]" == c || "undefined" != typeof a.call
					&& "undefined" != typeof a.propertyIsEnumerable
					&& !a.propertyIsEnumerable("call"))
				return "function"
		} else
			return "null";
	else if ("function" == b && "undefined" == typeof a.call)
		return "object";
	return b
}, CPWAb = function(a) {
	return void 0 !== a
}, CPWAc = function(a) {
	return "array" == CPWAba(a)
}, CPWAd = function(a) {
	var b = CPWAba(a);
	return "array" == b || "object" == b && "number" == typeof a.length
}, CPWAe = function(a) {
	return "string" == typeof a
}, CPWAf = function(a) {
	return "number" == typeof a
}, CPWAg = function(a) {
	return "function" == CPWAba(a)
}, CPWAca = function(a) {
	var b = typeof a;
	return "object" == b && null != a || "function" == b
}, CPWAda = "closure_uid_" + (1E9 * Math.random() >>> 0), CPWAea = 0, CPWAfa = function(
		a, b, c) {
	return a.call.apply(a.bind, arguments)
}, CPWAga = function(a, b, c) {
	if (!a)
		throw Error();
	if (2 < arguments.length) {
		var d = Array.prototype.slice.call(arguments, 2);
		return function() {
			var c = Array.prototype.slice.call(arguments);
			Array.prototype.unshift.apply(c, d);
			return a.apply(b, c)
		}
	}
	return function() {
		return a.apply(b, arguments)
	}
}, CPWAh = function(a, b, c) {
	CPWAh = Function.prototype.bind
			&& -1 != Function.prototype.bind.toString().indexOf("native code")
			? CPWAfa
			: CPWAga;
	return CPWAh.apply(null, arguments)
}, CPWAha = function(a, b) {
	var c = Array.prototype.slice.call(arguments, 1);
	return function() {
		var b = c.slice();
		b.push.apply(b, arguments);
		return a.apply(this, b)
	}
}, CPWAia = Date.now || function() {
	return +new Date
}, CPWAi = function(a, b) {
	var c = a.split("."), d = CPWAa;
	c[0] in d || !d.execScript || d.execScript("var " + c[0]);
	for (var e; c.length && (e = c.shift());)
		c.length || void 0 === b ? d = d[e] ? d[e] : d[e] = {} : d[e] = b
}, CPWAj = function(a, b) {
	function c() {
	}
	c.prototype = b.prototype;
	a.c = b.prototype;
	a.prototype = new c
};
Function.prototype.bind = Function.prototype.bind || function(a, b) {
	if (1 < arguments.length) {
		var c = Array.prototype.slice.call(arguments, 1);
		c.unshift(this, a);
		return CPWAh.apply(null, c)
	}
	return CPWAh(this, a)
};
var CPWAk = function() {
};
CPWAi("panoramio.BufferingOptions", CPWAk);
CPWAk.prototype.qe = void 0;
CPWAk.prototype.metadata = CPWAk.prototype.qe;
CPWAk.prototype.images = void 0;
CPWAk.prototype.images = CPWAk.prototype.images;
var CPWAl = function() {
};
CPWAi("panoramio.MetadataBufferingOptions", CPWAl);
CPWAl.prototype.Yc = void 0;
CPWAl.prototype.prefetchBefore = CPWAl.prototype.Yc;
CPWAl.prototype.Xc = void 0;
CPWAl.prototype.prefetchAfter = CPWAl.prototype.Xc;
CPWAl.prototype.je = void 0;
CPWAl.prototype.hysteresis = CPWAl.prototype.je;
var CPWAm = function() {
};
CPWAi("panoramio.ImageBufferingOptions", CPWAm);
CPWAm.prototype.Yc = void 0;
CPWAm.prototype.prefetchBefore = CPWAm.prototype.Yc;
CPWAm.prototype.Xc = void 0;
CPWAm.prototype.prefetchAfter = CPWAm.prototype.Xc;
var CPWAn = function() {
};
CPWAi("panoramio.Coordinates", CPWAn);
CPWAn.prototype.me = 0;
CPWAn.prototype.lat = CPWAn.prototype.me;
CPWAn.prototype.oe = 0;
CPWAn.prototype.lng = CPWAn.prototype.oe;
var CPWAo = function(a) {
	Error.captureStackTrace
			? Error.captureStackTrace(this, CPWAo)
			: this.stack = Error().stack || "";
	a && (this.message = String(a))
};
CPWAj(CPWAo, Error);
CPWAo.prototype.name = "CustomError";
var CPWAoa = function(a) {
	if (!CPWAja.test(a))
		return a;
	-1 != a.indexOf("&") && (a = a.replace(CPWAka, "&amp;"));
	-1 != a.indexOf("<") && (a = a.replace(CPWAla, "&lt;"));
	-1 != a.indexOf(">") && (a = a.replace(CPWAma, "&gt;"));
	-1 != a.indexOf('"') && (a = a.replace(CPWAna, "&quot;"));
	return a
}, CPWAka = /&/g, CPWAla = /</g, CPWAma = />/g, CPWAna = /\"/g, CPWAja = /[&<>\"]/, CPWApa = function(
		a) {
	return String(a).replace(/\-([a-z])/g, function(a, c) {
				return c.toUpperCase()
			})
}, CPWAqa = function(a) {
	var b = CPWAe(void 0)
			? "undefined".replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1")
					.replace(/\x08/g, "\\x08")
			: "\\s";
	return a.replace(
			RegExp("(^" + (b ? "|[" + b + "]+" : "") + ")([a-z])", "g"),
			function(a, b, e) {
				return b + e.toUpperCase()
			})
};
var CPWAp = Array.prototype, CPWAra = CPWAp.indexOf ? function(a, b, c) {
	return CPWAp.indexOf.call(a, b, c)
} : function(a, b, c) {
	c = null == c ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
	if (CPWAe(a))
		return CPWAe(b) && 1 == b.length ? a.indexOf(b, c) : -1;
	for (; c < a.length; c++)
		if (c in a && a[c] === b)
			return c;
	return -1
}, CPWAq = CPWAp.forEach ? function(a, b, c) {
	CPWAp.forEach.call(a, b, c)
} : function(a, b, c) {
	for (var d = a.length, e = CPWAe(a) ? a.split("") : a, f = 0; f < d; f++)
		f in e && b.call(c, e[f], f, a)
}, CPWAsa = CPWAp.filter ? function(a, b, c) {
	return CPWAp.filter.call(a, b, c)
} : function(a, b, c) {
	for (var d = a.length, e = [], f = 0, g = CPWAe(a) ? a.split("") : a, h = 0; h < d; h++)
		if (h in g) {
			var k = g[h];
			b.call(c, k, h, a) && (e[f++] = k)
		}
	return e
}, CPWAta = CPWAp.map ? function(a, b, c) {
	return CPWAp.map.call(a, b, c)
} : function(a, b, c) {
	for (var d = a.length, e = Array(d), f = CPWAe(a) ? a.split("") : a, g = 0; g < d; g++)
		g in f && (e[g] = b.call(c, f[g], g, a));
	return e
}, CPWAua = CPWAp.reduce ? function(a, b, c, d) {
	d && (b = CPWAh(b, d));
	return CPWAp.reduce.call(a, b, c)
} : function(a, b, c, d) {
	var e = c;
	CPWAq(a, function(c, g) {
				e = b.call(d, e, c, g, a)
			});
	return e
}, CPWAva = CPWAp.some ? function(a, b, c) {
	return CPWAp.some.call(a, b, c)
} : function(a, b, c) {
	for (var d = a.length, e = CPWAe(a) ? a.split("") : a, f = 0; f < d; f++)
		if (f in e && b.call(c, e[f], f, a))
			return !0;
	return !1
}, CPWAr = CPWAp.every ? function(a, b, c) {
	return CPWAp.every.call(a, b, c)
} : function(a, b, c) {
	for (var d = a.length, e = CPWAe(a) ? a.split("") : a, f = 0; f < d; f++)
		if (f in e && !b.call(c, e[f], f, a))
			return !1;
	return !0
}, CPWAya = function(a) {
	var b = CPWAwa.Ba;
	a = CPWAxa(b, a, void 0);
	return 0 > a ? null : CPWAe(b) ? b.charAt(a) : b[a]
}, CPWAxa = function(a, b, c) {
	for (var d = a.length, e = CPWAe(a) ? a.split("") : a, f = 0; f < d; f++)
		if (f in e && b.call(c, e[f], f, a))
			return f;
	return -1
}, CPWAs = function(a, b) {
	return 0 <= CPWAra(a, b)
}, CPWAza = function(a, b) {
	var c = CPWAra(a, b), d;
	(d = 0 <= c) && CPWAp.splice.call(a, c, 1);
	return d
}, CPWAAa = function(a) {
	return CPWAp.concat.apply(CPWAp, arguments)
}, CPWAt = function(a) {
	var b = a.length;
	if (0 < b) {
		for (var c = Array(b), d = 0; d < b; d++)
			c[d] = a[d];
		return c
	}
	return []
}, CPWABa = function(a, b) {
	for (var c = 1; c < arguments.length; c++) {
		var d = arguments[c], e;
		if (CPWAc(d) || (e = CPWAd(d))
				&& Object.prototype.hasOwnProperty.call(d, "callee"))
			a.push.apply(a, d);
		else if (e)
			for (var f = a.length, g = d.length, h = 0; h < g; h++)
				a[f + h] = d[h];
		else
			a.push(d)
	}
}, CPWACa = function(a, b, c) {
	return 2 >= arguments.length ? CPWAp.slice.call(a, b) : CPWAp.slice.call(a,
			b, c)
};
var CPWADa = "StopIteration" in CPWAa
		? CPWAa.StopIteration
		: Error("StopIteration"), CPWAEa = function() {
};
CPWAEa.prototype.next = function() {
	throw CPWADa;
};
CPWAEa.prototype.ja = function() {
	return this
};
var CPWAFa = function(a) {
	if (a instanceof CPWAEa)
		return a;
	if ("function" == typeof a.ja)
		return a.ja(!1);
	if (CPWAd(a)) {
		var b = 0, c = new CPWAEa;
		c.next = function() {
			for (;;) {
				if (b >= a.length)
					throw CPWADa;
				if (b in a)
					return a[b++];
				b++
			}
		};
		return c
	}
	throw Error("Not implemented");
}, CPWAGa = function(a, b, c) {
	if (CPWAd(a))
		try {
			CPWAq(a, b, c)
		} catch (d) {
			if (d !== CPWADa)
				throw d;
		}
	else {
		a = CPWAFa(a);
		try {
			for (;;)
				b.call(c, a.next(), void 0, a)
		} catch (e) {
			if (e !== CPWADa)
				throw e;
		}
	}
}, CPWAHa = function(a, b) {
	var c = CPWAFa(a), d = new CPWAEa;
	d.next = function() {
		for (;;) {
			var a = c.next();
			if (b.call(void 0, a, void 0, c))
				return a
		}
	};
	return d
};
var CPWAIa = function(a, b, c) {
	for (var d in a)
		b.call(c, a[d], d, a)
}, CPWAJa = function(a) {
	var b = [], c = 0, d;
	for (d in a)
		b[c++] = a[d];
	return b
}, CPWAKa = function(a) {
	var b = [], c = 0, d;
	for (d in a)
		b[c++] = d;
	return b
}, CPWALa = function(a, b) {
	for (var c in a)
		if (a[c] == b)
			return !0;
	return !1
}, CPWAMa = function(a) {
	for (var b in a)
		return !1;
	return !0
}, CPWAu = function(a, b, c) {
	return b in a ? a[b] : c
}, CPWANa = function(a) {
	var b = {}, c;
	for (c in a)
		b[c] = a[c];
	return b
}, CPWAOa = "constructor hasOwnProperty isPrototypeOf propertyIsEnumerable toLocaleString toString valueOf"
		.split(" "), CPWAPa = function(a, b) {
	for (var c, d, e = 1; e < arguments.length; e++) {
		d = arguments[e];
		for (c in d)
			a[c] = d[c];
		for (var f = 0; f < CPWAOa.length; f++)
			c = CPWAOa[f], Object.prototype.hasOwnProperty.call(d, c)
					&& (a[c] = d[c])
	}
};
var CPWAv = function(a, b) {
	this.j = {};
	this.l = [];
	this.mb = this.q = 0;
	var c = arguments.length;
	if (1 < c) {
		if (c % 2)
			throw Error("Uneven number of arguments");
		for (var d = 0; d < c; d += 2)
			this.set(arguments[d], arguments[d + 1])
	} else
		a && this.Jb(a)
};
CPWA = CPWAv.prototype;
CPWA.$ = function() {
	return this.q
};
CPWA.P = function() {
	CPWAQa(this);
	for (var a = [], b = 0; b < this.l.length; b++)
		a.push(this.j[this.l[b]]);
	return a
};
CPWA.N = function() {
	CPWAQa(this);
	return this.l.concat()
};
CPWA.D = function(a) {
	return CPWARa(this.j, a)
};
CPWA.clear = function() {
	this.j = {};
	this.mb = this.q = this.l.length = 0
};
CPWA.remove = function(a) {
	return CPWARa(this.j, a)
			? (delete this.j[a], this.q--, this.mb++, this.l.length > 2
					* this.q
					&& CPWAQa(this), !0)
			: !1
};
var CPWAQa = function(a) {
	if (a.q != a.l.length) {
		for (var b = 0, c = 0; b < a.l.length;) {
			var d = a.l[b];
			CPWARa(a.j, d) && (a.l[c++] = d);
			b++
		}
		a.l.length = c
	}
	if (a.q != a.l.length) {
		for (var e = {}, c = b = 0; b < a.l.length;)
			d = a.l[b], CPWARa(e, d) || (a.l[c++] = d, e[d] = 1), b++;
		a.l.length = c
	}
};
CPWA = CPWAv.prototype;
CPWA.get = function(a, b) {
	return CPWARa(this.j, a) ? this.j[a] : b
};
CPWA.set = function(a, b) {
	CPWARa(this.j, a) || (this.q++, this.l.push(a), this.mb++);
	this.j[a] = b
};
CPWA.Jb = function(a) {
	var b;
	a instanceof CPWAv
			? (b = a.N(), a = a.P())
			: (b = CPWAKa(a), a = CPWAJa(a));
	for (var c = 0; c < b.length; c++)
		this.set(b[c], a[c])
};
CPWA.U = function() {
	return new CPWAv(this)
};
CPWA.ja = function(a) {
	CPWAQa(this);
	var b = 0, c = this.l, d = this.j, e = this.mb, f = this, g = new CPWAEa;
	g.next = function() {
		for (;;) {
			if (e != f.mb)
				throw Error("The map has changed since the iterator was created");
			if (b >= c.length)
				throw CPWADa;
			var g = c[b++];
			return a ? g : d[g]
		}
	};
	return g
};
var CPWARa = function(a, b) {
	return Object.prototype.hasOwnProperty.call(a, b)
};
var CPWATa = function(a, b) {
	this.od = a || null;
	this.A = !!b;
	this.j = new CPWAv;
	this.p = new CPWASa("", void 0);
	this.p.next = this.p.Q = this.p
}, CPWAVa = function(a, b) {
	var c = a.j.get(b);
	c && a.A && (c.remove(), CPWAUa(a, c));
	return c
};
CPWA = CPWATa.prototype;
CPWA.get = function(a, b) {
	var c = CPWAVa(this, a);
	return c ? c.value : b
};
CPWA.set = function(a, b) {
	var c = CPWAVa(this, a);
	c ? c.value = b : (c = new CPWASa(a, b), this.j.set(a, c), CPWAUa(this, c))
};
CPWA.shift = function() {
	return CPWAWa(this, this.p.next)
};
CPWA.pop = function() {
	return CPWAWa(this, this.p.Q)
};
CPWA.remove = function(a) {
	return (a = this.j.get(a)) ? (this.removeNode(a), !0) : !1
};
CPWA.removeNode = function(a) {
	a.remove();
	this.j.remove(a.key)
};
CPWA.$ = function() {
	return this.j.$()
};
CPWA.N = function() {
	return this.map(function(a, b) {
				return b
			})
};
CPWA.P = function() {
	return this.map(function(a) {
				return a
			})
};
CPWA.contains = function(a) {
	return this.some(function(b) {
				return b == a
			})
};
CPWA.D = function(a) {
	return this.j.D(a)
};
CPWA.clear = function() {
	CPWAXa(this, 0)
};
CPWA.forEach = function(a, b) {
	for (var c = this.p.next; c != this.p; c = c.next)
		a.call(b, c.value, c.key, this)
};
CPWA.map = function(a, b) {
	for (var c = [], d = this.p.next; d != this.p; d = d.next)
		c.push(a.call(b, d.value, d.key, this));
	return c
};
CPWA.some = function(a, b) {
	for (var c = this.p.next; c != this.p; c = c.next)
		if (a.call(b, c.value, c.key, this))
			return !0;
	return !1
};
CPWA.every = function(a, b) {
	for (var c = this.p.next; c != this.p; c = c.next)
		if (!a.call(b, c.value, c.key, this))
			return !1;
	return !0
};
var CPWAUa = function(a, b) {
	a.A
			? (b.next = a.p.next, b.Q = a.p, a.p.next = b, b.next.Q = b)
			: (b.Q = a.p.Q, b.next = a.p, a.p.Q = b, b.Q.next = b);
	null != a.od && CPWAXa(a, a.od)
}, CPWAXa = function(a, b) {
	for (var c = a.j.$(); c > b; c--)
		a.removeNode(a.A ? a.p.Q : a.p.next)
}, CPWAWa = function(a, b) {
	a.p != b && a.removeNode(b);
	return b.value
}, CPWASa = function(a, b) {
	this.key = a;
	this.value = b
};
CPWASa.prototype.remove = function() {
	this.Q.next = this.next;
	this.next.Q = this.Q;
	delete this.Q;
	delete this.next
};
var CPWAw = function(a, b) {
	CPWATa.call(this, a, b);
	this.Ga = !1
};
CPWAj(CPWAw, CPWATa);
CPWAw.prototype.removeNode = function(a) {
	a.value.e();
	CPWAw.c.removeNode.call(this, a)
};
CPWAw.prototype.g = function() {
	return this.Ga
};
CPWAw.prototype.e = function() {
	this.Ga || (this.Ga = !0, this.clear())
};
var CPWAx = function() {
};
CPWAx.prototype.Ga = !1;
CPWAx.prototype.g = function() {
	return this.Ga
};
CPWAx.prototype.e = function() {
	this.Ga || (this.Ga = !0, this.a())
};
CPWAx.prototype.a = function() {
	if (this.Dd)
		for (; this.Dd.length;)
			this.Dd.shift()()
};
var CPWAZa = function(a, b) {
	var c = Array.prototype.slice.call(arguments), d = c.shift();
	if ("undefined" == typeof d)
		throw Error("[goog.string.format] Template required");
	return d.replace(/%([0\-\ \+]*)(\d+)?(\.(\d+))?([%sfdiu])/g, function(a, b,
					d, h, k, n, l, m) {
				if ("%" == n)
					return "%";
				var p = c.shift();
				if ("undefined" == typeof p)
					throw Error("[goog.string.format] Not enough arguments");
				arguments[0] = p;
				return CPWAYa[n].apply(null, arguments)
			})
}, CPWAYa = {
	s : function(a, b, c) {
		return isNaN(c) || "" == c || a.length >= c ? a : a = -1 < b.indexOf(
				"-", 0) ? a + Array(c - a.length + 1).join(" ") : Array(c
				- a.length + 1).join(" ")
				+ a
	},
	f : function(a, b, c, d, e) {
		d = a.toString();
		isNaN(e) || "" == e || (d = a.toFixed(e));
		var f;
		f = 0 > a ? "-" : 0 <= b.indexOf("+") ? "+" : 0 <= b.indexOf(" ")
				? " "
				: "";
		0 <= a && (d = f + d);
		if (isNaN(c) || d.length >= c)
			return d;
		d = isNaN(e) ? Math.abs(a).toString() : Math.abs(a).toFixed(e);
		a = c - d.length - f.length;
		return d = 0 <= b.indexOf("-", 0) ? f + d + Array(a + 1).join(" ") : f
				+ Array(a + 1).join(0 <= b.indexOf("0", 0) ? "0" : " ") + d
	},
	d : function(a, b, c, d, e, f, g, h) {
		return CPWAYa.f(parseInt(a, 10), b, c, d, 0, f, g, h)
	}
};
CPWAYa.i = CPWAYa.d;
CPWAYa.u = CPWAYa.d;
var CPWA_a = function(a, b, c, d) {
	this.width = a;
	this.height = b;
	this.da = c;
	this.url = d
};
CPWAj(CPWA_a, CPWAx);
CPWAi("panoramio.PhotoImageMetadata", CPWA_a);
CPWA_a.prototype.a = function() {
	delete this.width;
	delete this.height;
	delete this.da;
	delete this.url;
	CPWA_a.c.a.call(this)
};
CPWA_a.prototype.toString = function() {
	return CPWAZa("<w=%s h=%s %s url=%s>", this.width, this.height, this.da
					? "cropped"
					: "whole", this.url)
};
var CPWA0a = function(a, b) {
	this.description = a;
	this.ad = b
};
CPWAj(CPWA0a, CPWAx);
CPWA0a.prototype.a = function() {
	delete this.description;
	delete this.ad;
	CPWA0a.c.a.call(this)
};
CPWA0a.prototype.toString = function() {
	return CPWAZa("<Image %s>", this.description.toString())
};
var CPWA1a, CPWA2a = function(a) {
	a = a.className;
	return CPWAe(a) && a.match(/\S+/g) || []
}, CPWA3a = function(a, b) {
	for (var c = CPWA2a(a), d = CPWACa(arguments, 1), e = c.length + d.length, f = c, g = 0; g < d.length; g++)
		CPWAs(f, d[g]) || f.push(d[g]);
	a.className = c.join(" ");
	return c.length == e
}, CPWA5a = function(a, b) {
	var c = CPWA2a(a), d = CPWACa(arguments, 1), c = CPWA4a(c, d);
	a.className = c.join(" ")
}, CPWA4a = function(a, b) {
	return CPWAsa(a, function(a) {
				return !CPWAs(b, a)
			})
};
var CPWA6a = function(a) {
	CPWA6a[" "](a);
	return a
};
CPWA6a[" "] = CPWAaa;
var CPWA7a, CPWA8a, CPWA9a, CPWA$a, CPWAab = function() {
	return CPWAa.navigator ? CPWAa.navigator.userAgent : null
};
CPWA$a = CPWA9a = CPWA8a = CPWA7a = !1;
var CPWAbb;
if (CPWAbb = CPWAab()) {
	var CPWAcb = CPWAa.navigator;
	CPWA7a = 0 == CPWAbb.lastIndexOf("Opera", 0);
	CPWA8a = !CPWA7a
			&& (-1 != CPWAbb.indexOf("MSIE") || -1 != CPWAbb.indexOf("Trident"));
	CPWA9a = !CPWA7a && -1 != CPWAbb.indexOf("WebKit");
	CPWA$a = !CPWA7a && !CPWA9a && !CPWA8a && "Gecko" == CPWAcb.product
}
var CPWAdb = CPWA7a, CPWAy = CPWA8a, CPWAeb = CPWA$a, CPWAz = CPWA9a, CPWAfb = function() {
	var a = CPWAa.document;
	return a ? a.documentMode : void 0
}, CPWAgb;
t : {
	var CPWAhb = "", CPWAib;
	if (CPWAdb && CPWAa.opera)
		var CPWAjb = CPWAa.opera.version, CPWAhb = "function" == typeof CPWAjb
				? CPWAjb()
				: CPWAjb;
	else if (CPWAeb ? CPWAib = /rv\:([^\);]+)(\)|;)/ : CPWAy
			? CPWAib = /\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/
			: CPWAz && (CPWAib = /WebKit\/(\S+)/), CPWAib)
		var CPWAkb = CPWAib.exec(CPWAab()), CPWAhb = CPWAkb ? CPWAkb[1] : "";
	if (CPWAy) {
		var CPWAlb = CPWAfb();
		if (CPWAlb > parseFloat(CPWAhb)) {
			CPWAgb = String(CPWAlb);
			break t
		}
	}
	CPWAgb = CPWAhb
}
var CPWAmb = CPWAgb, CPWAnb = {}, CPWAA = function(a) {
	var b;
	if (!(b = CPWAnb[a])) {
		b = 0;
		for (var c = String(CPWAmb).replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
				.split("."), d = String(a)
				.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "").split("."), e = Math
				.max(c.length, d.length), f = 0; 0 == b && f < e; f++) {
			var g = c[f] || "", h = d[f] || "", k = RegExp("(\\d*)(\\D*)", "g"), n = RegExp(
					"(\\d*)(\\D*)", "g");
			do {
				var l = k.exec(g) || ["", "", ""], m = n.exec(h)
						|| ["", "", ""];
				if (0 == l[0].length && 0 == m[0].length)
					break;
				b = ((0 == l[1].length ? 0 : parseInt(l[1], 10)) < (0 == m[1].length
						? 0
						: parseInt(m[1], 10)) ? -1 : (0 == l[1].length
						? 0
						: parseInt(l[1], 10)) > (0 == m[1].length
						? 0
						: parseInt(m[1], 10)) ? 1 : 0)
						|| ((0 == l[2].length) < (0 == m[2].length)
								? -1
								: (0 == l[2].length) > (0 == m[2].length)
										? 1
										: 0)
						|| (l[2] < m[2] ? -1 : l[2] > m[2] ? 1 : 0)
			} while (0 == b)
		}
		b = CPWAnb[a] = 0 <= b
	}
	return b
}, CPWAob = CPWAa.document, CPWApb = CPWAob && CPWAy
		? CPWAfb()
				|| ("CSS1Compat" == CPWAob.compatMode
						? parseInt(CPWAmb, 10)
						: 5)
		: void 0;
var CPWAqb = !CPWAy || CPWAy && 9 <= CPWApb, CPWArb = CPWAy && !CPWAA("9");
!CPWAz || CPWAA("528");
CPWAeb && CPWAA("1.9b") || CPWAy && CPWAA("8") || CPWAdb && CPWAA("9.5")
		|| CPWAz && CPWAA("528");
CPWAeb && !CPWAA("8") || CPWAy && CPWAA("9");
var CPWAB = function(a, b) {
	this.type = a;
	this.currentTarget = this.target = b
};
CPWA = CPWAB.prototype;
CPWA.a = function() {
};
CPWA.e = function() {
};
CPWA.ua = !1;
CPWA.defaultPrevented = !1;
CPWA.qd = !0;
CPWA.stopPropagation = function() {
	this.ua = !0
};
CPWA.preventDefault = function() {
	this.defaultPrevented = !0;
	this.qd = !1
};
var CPWAsb = function(a, b) {
	if (a) {
		var c = this.type = a.type;
		CPWAB.call(this, c);
		this.target = a.target || a.srcElement;
		this.currentTarget = b;
		var d = a.relatedTarget;
		if (d) {
			if (CPWAeb) {
				var e;
				t : {
					try {
						CPWA6a(d.nodeName);
						e = !0;
						break t
					} catch (f) {
					}
					e = !1
				}
				e || (d = null)
			}
		} else
			"mouseover" == c ? d = a.fromElement : "mouseout" == c
					&& (d = a.toElement);
		this.relatedTarget = d;
		this.offsetX = CPWAz || void 0 !== a.offsetX ? a.offsetX : a.layerX;
		this.offsetY = CPWAz || void 0 !== a.offsetY ? a.offsetY : a.layerY;
		this.clientX = void 0 !== a.clientX ? a.clientX : a.pageX;
		this.clientY = void 0 !== a.clientY ? a.clientY : a.pageY;
		this.screenX = a.screenX || 0;
		this.screenY = a.screenY || 0;
		this.button = a.button;
		this.keyCode = a.keyCode || 0;
		this.charCode = a.charCode || ("keypress" == c ? a.keyCode : 0);
		this.ctrlKey = a.ctrlKey;
		this.altKey = a.altKey;
		this.shiftKey = a.shiftKey;
		this.metaKey = a.metaKey;
		this.state = a.state;
		this.vb = a;
		a.defaultPrevented && this.preventDefault();
		delete this.ua
	}
};
CPWAj(CPWAsb, CPWAB);
CPWA = CPWAsb.prototype;
CPWA.target = null;
CPWA.relatedTarget = null;
CPWA.offsetX = 0;
CPWA.offsetY = 0;
CPWA.clientX = 0;
CPWA.clientY = 0;
CPWA.screenX = 0;
CPWA.screenY = 0;
CPWA.button = 0;
CPWA.keyCode = 0;
CPWA.charCode = 0;
CPWA.ctrlKey = !1;
CPWA.altKey = !1;
CPWA.shiftKey = !1;
CPWA.metaKey = !1;
CPWA.vb = null;
CPWA.stopPropagation = function() {
	CPWAsb.c.stopPropagation.call(this);
	this.vb.stopPropagation
			? this.vb.stopPropagation()
			: this.vb.cancelBubble = !0
};
CPWA.preventDefault = function() {
	CPWAsb.c.preventDefault.call(this);
	var a = this.vb;
	if (a.preventDefault)
		a.preventDefault();
	else if (a.returnValue = !1, CPWArb)
		try {
			if (a.ctrlKey || 112 <= a.keyCode && 123 >= a.keyCode)
				a.keyCode = -1
		} catch (b) {
		}
};
CPWA.a = function() {
};
var CPWAtb = "closure_listenable_" + (1E6 * Math.random() | 0), CPWAub = function(
		a) {
	try {
		return !(!a || !a[CPWAtb])
	} catch (b) {
		return !1
	}
}, CPWAvb = 0;
var CPWAwb = function(a, b, c, d, e) {
	this.Ea = a;
	this.Xb = null;
	this.src = b;
	this.type = c;
	this.capture = !!d;
	this.Ub = e;
	this.key = ++CPWAvb;
	this.Ma = this.Tb = !1
}, CPWAxb = function(a) {
	a.Ma = !0;
	a.Ea = null;
	a.Xb = null;
	a.src = null;
	a.Ub = null
};
var CPWAyb = function(a) {
	this.src = a;
	this.I = {};
	this.ub = 0
};
CPWAyb.prototype.add = function(a, b, c, d, e) {
	var f = this.I[a];
	f || (f = this.I[a] = [], this.ub++);
	var g = CPWAzb(f, b, d, e);
	-1 < g ? (a = f[g], c || (a.Tb = !1)) : (a = new CPWAwb(b, this.src, a,
			!!d, e), a.Tb = c, f.push(a));
	return a
};
CPWAyb.prototype.remove = function(a, b, c, d) {
	if (!(a in this.I))
		return !1;
	var e = this.I[a];
	b = CPWAzb(e, b, c, d);
	return -1 < b ? (CPWAxb(e[b]), CPWAp.splice.call(e, b, 1), 0 == e.length
			&& (delete this.I[a], this.ub--), !0) : !1
};
var CPWAAb = function(a, b) {
	var c = b.type;
	if (!(c in a.I))
		return !1;
	var d = CPWAza(a.I[c], b);
	d && (CPWAxb(b), 0 == a.I[c].length && (delete a.I[c], a.ub--));
	return d
};
CPWAyb.prototype.Mb = function(a) {
	var b = 0, c;
	for (c in this.I)
		if (!a || c == a) {
			for (var d = this.I[c], e = 0; e < d.length; e++)
				++b, CPWAxb(d[e]);
			delete this.I[c];
			this.ub--
		}
	return b
};
CPWAyb.prototype.tb = function(a, b, c, d) {
	a = this.I[a];
	var e = -1;
	a && (e = CPWAzb(a, b, c, d));
	return -1 < e ? a[e] : null
};
var CPWAzb = function(a, b, c, d) {
	for (var e = 0; e < a.length; ++e) {
		var f = a[e];
		if (!f.Ma && f.Ea == b && f.capture == !!c && f.Ub == d)
			return e
	}
	return -1
};
var CPWABb = "closure_lm_" + (1E6 * Math.random() | 0), CPWACb = {}, CPWADb = 0, CPWAC = function(
		a, b, c, d, e) {
	if (CPWAc(b)) {
		for (var f = 0; f < b.length; f++)
			CPWAC(a, b[f], c, d, e);
		return null
	}
	c = CPWAEb(c);
	return CPWAub(a) ? a.Ec(b, c, d, e) : CPWAFb(a, b, c, !1, d, e)
}, CPWAFb = function(a, b, c, d, e, f) {
	if (!b)
		throw Error("Invalid event type");
	var g = !!e, h = CPWAGb(a);
	h || (a[CPWABb] = h = new CPWAyb(a));
	c = h.add(b, c, d, e, f);
	if (c.Xb)
		return c;
	d = CPWAHb();
	c.Xb = d;
	d.src = a;
	d.Ea = c;
	a.addEventListener ? a.addEventListener(b, d, g) : a.attachEvent(
			b in CPWACb ? CPWACb[b] : CPWACb[b] = "on" + b, d);
	CPWADb++;
	return c
}, CPWAHb = function() {
	var a = CPWAIb, b = CPWAqb ? function(c) {
		return a.call(b.src, b.Ea, c)
	} : function(c) {
		c = a.call(b.src, b.Ea, c);
		if (!c)
			return c
	};
	return b
}, CPWAJb = function(a, b, c, d, e) {
	if (CPWAc(b)) {
		for (var f = 0; f < b.length; f++)
			CPWAJb(a, b[f], c, d, e);
		return null
	}
	c = CPWAEb(c);
	return CPWAub(a) ? a.Jc(b, c, d, e) : CPWAFb(a, b, c, !0, d, e)
}, CPWAKb = function(a, b, c, d, e) {
	if (CPWAc(b)) {
		for (var f = 0; f < b.length; f++)
			CPWAKb(a, b[f], c, d, e);
		return null
	}
	c = CPWAEb(c);
	if (CPWAub(a))
		return a.Nb(b, c, d, e);
	if (!a)
		return !1;
	if (a = CPWAGb(a))
		if (b = a.tb(b, c, !!d, e))
			return CPWALb(b);
	return !1
}, CPWALb = function(a) {
	if (CPWAf(a) || !a || a.Ma)
		return !1;
	var b = a.src;
	if (CPWAub(b))
		return CPWAAb(b.la, a);
	var c = a.type, d = a.Xb;
	b.removeEventListener
			? b.removeEventListener(c, d, a.capture)
			: b.detachEvent
					&& b.detachEvent(c in CPWACb ? CPWACb[c] : CPWACb[c] = "on"
									+ c, d);
	CPWADb--;
	(c = CPWAGb(b)) ? (CPWAAb(c, a), 0 == c.ub
			&& (c.src = null, b[CPWABb] = null)) : CPWAxb(a);
	return !0
}, CPWANb = function(a, b, c, d) {
	var e = 1;
	if (a = CPWAGb(a))
		if (b = a.I[b])
			for (b = CPWAt(b), a = 0; a < b.length; a++) {
				var f = b[a];
				f && f.capture == c && !f.Ma && (e &= !1 !== CPWAMb(f, d))
			}
	return Boolean(e)
}, CPWAMb = function(a, b) {
	var c = a.Ea, d = a.Ub || a.src;
	a.Tb && CPWALb(a);
	return c.call(d, b)
}, CPWAOb = function(a, b) {
	a.dispatchEvent(b)
}, CPWAIb = function(a, b) {
	if (a.Ma)
		return !0;
	if (!CPWAqb) {
		var c;
		if (!(c = b))
			t : {
				c = ["window", "event"];
				for (var d = CPWAa, e; e = c.shift();)
					if (null != d[e])
						d = d[e];
					else {
						c = null;
						break t
					}
				c = d
			}
		e = c;
		c = new CPWAsb(e, this);
		d = !0;
		if (!(0 > e.keyCode || void 0 != e.returnValue)) {
			t : {
				var f = !1;
				if (0 == e.keyCode)
					try {
						e.keyCode = -1;
						break t
					} catch (g) {
						f = !0
					}
				if (f || void 0 == e.returnValue)
					e.returnValue = !0
			}
			e = [];
			for (f = c.currentTarget; f; f = f.parentNode)
				e.push(f);
			for (var f = a.type, h = e.length - 1; !c.ua && 0 <= h; h--)
				c.currentTarget = e[h], d &= CPWANb(e[h], f, !0, c);
			for (h = 0; !c.ua && h < e.length; h++)
				c.currentTarget = e[h], d &= CPWANb(e[h], f, !1, c)
		}
		return d
	}
	return CPWAMb(a, new CPWAsb(b, this))
}, CPWAGb = function(a) {
	a = a[CPWABb];
	return a instanceof CPWAyb ? a : null
}, CPWAPb = "__closure_events_fn_" + (1E9 * Math.random() >>> 0), CPWAEb = function(
		a) {
	return CPWAg(a) ? a : a[CPWAPb] || (a[CPWAPb] = function(b) {
		return a.handleEvent(b)
	})
};
var CPWAD = function() {
	this.la = new CPWAyb(this);
	this.Ud = this
};
CPWAj(CPWAD, CPWAx);
CPWAD.prototype[CPWAtb] = !0;
CPWA = CPWAD.prototype;
CPWA.zc = null;
CPWA.addEventListener = function(a, b, c, d) {
	CPWAC(this, a, b, c, d)
};
CPWA.removeEventListener = function(a, b, c, d) {
	CPWAKb(this, a, b, c, d)
};
CPWA.dispatchEvent = function(a) {
	var b, c = this.zc;
	if (c) {
		b = [];
		for (var d = 1; c; c = c.zc)
			b.push(c), ++d
	}
	c = this.Ud;
	d = a.type || a;
	if (CPWAe(a))
		a = new CPWAB(a, c);
	else if (a instanceof CPWAB)
		a.target = a.target || c;
	else {
		var e = a;
		a = new CPWAB(d, c);
		CPWAPa(a, e)
	}
	var e = !0, f;
	if (b)
		for (var g = b.length - 1; !a.ua && 0 <= g; g--)
			f = a.currentTarget = b[g], e = CPWAQb(f, d, !0, a) && e;
	a.ua
			|| (f = a.currentTarget = c, e = CPWAQb(f, d, !0, a) && e, a.ua
					|| (e = CPWAQb(f, d, !1, a) && e));
	if (b)
		for (g = 0; !a.ua && g < b.length; g++)
			f = a.currentTarget = b[g], e = CPWAQb(f, d, !1, a) && e;
	return e
};
CPWA.a = function() {
	CPWAD.c.a.call(this);
	this.la && this.la.Mb(void 0);
	this.zc = null
};
CPWA.Ec = function(a, b, c, d) {
	return this.la.add(a, b, !1, c, d)
};
CPWA.Jc = function(a, b, c, d) {
	return this.la.add(a, b, !0, c, d)
};
CPWA.Nb = function(a, b, c, d) {
	return this.la.remove(a, b, c, d)
};
var CPWAQb = function(a, b, c, d) {
	b = a.la.I[b];
	if (!b)
		return !0;
	b = CPWAt(b);
	for (var e = !0, f = 0; f < b.length; ++f) {
		var g = b[f];
		if (g && !g.Ma && g.capture == c) {
			var h = g.Ea, k = g.Ub || g.src;
			g.Tb && CPWAAb(a.la, g);
			e = !1 !== h.call(k, d) && e
		}
	}
	return e && !1 != d.qd
};
CPWAD.prototype.tb = function(a, b, c, d) {
	return this.la.tb(a, b, c, d)
};
var CPWARb = function(a) {
	return function() {
		throw a;
	}
};
var CPWAE = function(a, b) {
	this.x = CPWAb(a) ? a : 0;
	this.y = CPWAb(b) ? b : 0
};
CPWAE.prototype.U = function() {
	return new CPWAE(this.x, this.y)
};
CPWAE.prototype.floor = function() {
	this.x = Math.floor(this.x);
	this.y = Math.floor(this.y);
	return this
};
CPWAE.prototype.round = function() {
	this.x = Math.round(this.x);
	this.y = Math.round(this.y);
	return this
};
CPWAE.prototype.scale = function(a, b) {
	var c = CPWAf(b) ? b : a;
	this.x *= a;
	this.y *= c;
	return this
};
var CPWAF = function(a, b) {
	this.width = a;
	this.height = b
};
CPWAF.prototype.U = function() {
	return new CPWAF(this.width, this.height)
};
var CPWAG = function(a) {
	return a.width / a.height
};
CPWAF.prototype.floor = function() {
	this.width = Math.floor(this.width);
	this.height = Math.floor(this.height);
	return this
};
CPWAF.prototype.round = function() {
	this.width = Math.round(this.width);
	this.height = Math.round(this.height);
	return this
};
CPWAF.prototype.scale = function(a, b) {
	var c = CPWAf(b) ? b : a;
	this.width *= a;
	this.height *= c;
	return this
};
var CPWASb = !CPWAy || CPWAy && 9 <= CPWApb;
!CPWAeb && !CPWAy || CPWAy && CPWAy && 9 <= CPWApb || CPWAeb && CPWAA("1.9.1");
CPWAy && CPWAA("9");
var CPWATb = function(a, b, c, d) {
	a = d || a;
	b = b && "*" != b ? b.toUpperCase() : "";
	if (a.querySelectorAll && a.querySelector && (b || c))
		return a.querySelectorAll(b + (c ? "." + c : ""));
	if (c && a.getElementsByClassName) {
		a = a.getElementsByClassName(c);
		if (b) {
			d = {};
			for (var e = 0, f = 0, g; g = a[f]; f++)
				b == g.nodeName && (d[e++] = g);
			d.length = e;
			return d
		}
		return a
	}
	a = a.getElementsByTagName(b || "*");
	if (c) {
		d = {};
		for (f = e = 0; g = a[f]; f++)
			b = g.className, "function" == typeof b.split
					&& CPWAs(b.split(/\s+/), c) && (d[e++] = g);
		d.length = e;
		return d
	}
	return a
}, CPWAH = function(a, b) {
	CPWAIa(b, function(b, d) {
				"style" == d ? a.style.cssText = b : "class" == d
						? a.className = b
						: "for" == d ? a.htmlFor = b : d in CPWAUb ? a
								.setAttribute(CPWAUb[d], b) : 0 == d
								.lastIndexOf("aria-", 0)
								|| 0 == d.lastIndexOf("data-", 0) ? a
								.setAttribute(d, b) : a[d] = b
			})
}, CPWAUb = {
	cellpadding : "cellPadding",
	cellspacing : "cellSpacing",
	colspan : "colSpan",
	frameborder : "frameBorder",
	height : "height",
	maxlength : "maxLength",
	role : "role",
	rowspan : "rowSpan",
	type : "type",
	usemap : "useMap",
	valign : "vAlign",
	width : "width"
}, CPWAI = function(a, b, c) {
	return CPWAVb(document, arguments)
}, CPWAVb = function(a, b) {
	var c = b[0], d = b[1];
	if (!CPWASb && d && (d.name || d.type)) {
		c = ["<", c];
		d.name && c.push(' name="', CPWAoa(d.name), '"');
		if (d.type) {
			c.push(' type="', CPWAoa(d.type), '"');
			var e = {};
			CPWAPa(e, d);
			delete e.type;
			d = e
		}
		c.push(">");
		c = c.join("")
	}
	c = a.createElement(c);
	d
			&& (CPWAe(d) ? c.className = d : CPWAc(d) ? CPWA3a.apply(null, [c]
							.concat(d)) : CPWAH(c, d));
	2 < b.length && CPWAWb(a, c, b);
	return c
}, CPWAWb = function(a, b, c) {
	function d(c) {
		c && b.appendChild(CPWAe(c) ? a.createTextNode(c) : c)
	}
	for (var e = 2; e < c.length; e++) {
		var f = c[e];
		!CPWAd(f) || CPWAca(f) && 0 < f.nodeType ? d(f) : CPWAq(CPWAXb(f)
						? CPWAt(f)
						: f, d)
	}
}, CPWAYb = function(a) {
	var b = document, c = b.createElement("div");
	CPWAy
			? (c.innerHTML = "<br>" + a, c.removeChild(c.firstChild))
			: c.innerHTML = a;
	if (1 == c.childNodes.length)
		return c.removeChild(c.firstChild);
	for (a = b.createDocumentFragment(); c.firstChild;)
		a.appendChild(c.firstChild);
	return a
}, CPWAJ = function(a) {
	for (var b; b = a.firstChild;)
		a.removeChild(b)
}, CPWAZb = function(a) {
	return a && a.parentNode ? a.parentNode.removeChild(a) : null
}, CPWA_b = function(a, b) {
	if (a.contains && 1 == b.nodeType)
		return a == b || a.contains(b);
	if ("undefined" != typeof a.compareDocumentPosition)
		return a == b || Boolean(a.compareDocumentPosition(b) & 16);
	for (; b && a != b;)
		b = b.parentNode;
	return b == a
}, CPWA0b = function(a) {
	return 9 == a.nodeType ? a : a.ownerDocument || a.document
}, CPWA1b = function(a, b) {
	if ("textContent" in a)
		a.textContent = b;
	else if (a.firstChild && 3 == a.firstChild.nodeType) {
		for (; a.lastChild != a.firstChild;)
			a.removeChild(a.lastChild);
		a.firstChild.data = b
	} else
		CPWAJ(a), a.appendChild(CPWA0b(a).createTextNode(String(b)))
}, CPWAXb = function(a) {
	if (a && "number" == typeof a.length) {
		if (CPWAca(a))
			return "function" == typeof a.item || "string" == typeof a.item;
		if (CPWAg(a))
			return "function" == typeof a.item
	}
	return !1
}, CPWA2b = function(a) {
	this.Ab = a || CPWAa.document || document
};
CPWA = CPWA2b.prototype;
CPWA.Zb = function(a) {
	return CPWAe(a) ? this.Ab.getElementById(a) : a
};
CPWA.k = function(a, b, c) {
	return CPWATb(this.Ab, a, b, c)
};
CPWA.Hd = function(a, b, c) {
	return CPWAVb(this.Ab, arguments)
};
CPWA.createElement = function(a) {
	return this.Ab.createElement(a)
};
CPWA.createTextNode = function(a) {
	return this.Ab.createTextNode(String(a))
};
CPWA.appendChild = function(a, b) {
	a.appendChild(b)
};
CPWA.removeNode = CPWAZb;
CPWA.contains = CPWA_b;
var CPWA3b = function(a) {
	this.Z = a;
	this.l = {}
};
CPWAj(CPWA3b, CPWAx);
var CPWA4b = [];
CPWA = CPWA3b.prototype;
CPWA.Ec = function(a, b, c, d, e) {
	CPWAc(b) || (CPWA4b[0] = b, b = CPWA4b);
	for (var f = 0; f < b.length; f++) {
		var g = CPWAC(a, b[f], c || this, d || !1, e || this.Z || this);
		if (!g)
			break;
		this.l[g.key] = g
	}
	return this
};
CPWA.Jc = function(a, b, c, d, e) {
	if (CPWAc(b))
		for (var f = 0; f < b.length; f++)
			this.Jc(a, b[f], c, d, e);
	else {
		a = CPWAJb(a, b, c || this, d, e || this.Z || this);
		if (!a)
			return this;
		this.l[a.key] = a
	}
	return this
};
CPWA.Nb = function(a, b, c, d, e) {
	if (CPWAc(b))
		for (var f = 0; f < b.length; f++)
			this.Nb(a, b[f], c, d, e);
	else
		e = e || this.Z || this, c = CPWAEb(c || this), d = !!d, b = CPWAub(a)
				? a.tb(b, c, d, e)
				: a ? (a = CPWAGb(a)) ? a.tb(b, c, d, e) : null : null, b
				&& (CPWALb(b), delete this.l[b.key]);
	return this
};
CPWA.Mb = function() {
	CPWAIa(this.l, CPWALb);
	this.l = {}
};
CPWA.a = function() {
	CPWA3b.c.a.call(this);
	this.Mb()
};
CPWA.handleEvent = function() {
	throw Error("EventHandler.handleEvent not implemented");
};
var CPWA5b = function(a) {
	CPWAD.call(this);
	this.cb = {};
	this.bb = {};
	this.Z = new CPWA3b(this);
	this.Da = a
};
CPWAj(CPWA5b, CPWAD);
var CPWA6b = [CPWAy ? "readystatechange" : "load", "abort", "error"], CPWA7b = function(
		a, b, c) {
	(c = CPWAe(c) ? c : c.src) && (a.cb[b] = c)
};
CPWA5b.prototype.start = function() {
	var a = this.cb;
	CPWAq(CPWAKa(a), function(b) {
				var c = a[b];
				if (c && (delete a[b], !this.g())) {
					var d;
					d = this.Da
							? (this.Da ? new CPWA2b(CPWA0b(this.Da)) : CPWA1a
									|| (CPWA1a = new CPWA2b)).Hd("img")
							: new Image;
					this.Z.Ec(d, CPWA6b, this.pd);
					this.bb[b] = d;
					d.id = b;
					d.src = c
				}
			}, this)
};
CPWA5b.prototype.pd = function(a) {
	var b = a.currentTarget;
	if (b) {
		if ("readystatechange" == a.type)
			if ("complete" == b.readyState)
				a.type = "load";
			else
				return;
		"undefined" == typeof b.naturalWidth
				&& ("load" == a.type
						? (b.naturalWidth = b.width, b.naturalHeight = b.height)
						: (b.naturalWidth = 0, b.naturalHeight = 0));
		this.dispatchEvent({
					type : a.type,
					target : b
				});
		!this.g()
				&& (a = b.id, delete this.cb[a], b = this.bb[a])
				&& (delete this.bb[a], this.Z.Nb(b, CPWA6b, this.pd), CPWAMa(this.bb)
						&& CPWAMa(this.cb) && this.dispatchEvent("complete"))
	}
};
CPWA5b.prototype.a = function() {
	delete this.cb;
	delete this.bb;
	var a = this.Z;
	a && "function" == typeof a.e && a.e();
	CPWA5b.c.a.call(this)
};
var CPWA8b = function(a) {
	if ("function" == typeof a.P)
		return a.P();
	if (CPWAe(a))
		return a.split("");
	if (CPWAd(a)) {
		for (var b = [], c = a.length, d = 0; d < c; d++)
			b.push(a[d]);
		return b
	}
	return CPWAJa(a)
}, CPWA9b = function(a) {
	if ("function" == typeof a.N)
		return a.N();
	if ("function" != typeof a.P) {
		if (CPWAd(a) || CPWAe(a)) {
			var b = [];
			a = a.length;
			for (var c = 0; c < a; c++)
				b.push(c);
			return b
		}
		return CPWAKa(a)
	}
}, CPWA$b = function(a, b, c) {
	if ("function" == typeof a.forEach)
		a.forEach(b, c);
	else if (CPWAd(a) || CPWAe(a))
		CPWAq(a, b, c);
	else
		for (var d = CPWA9b(a), e = CPWA8b(a), f = e.length, g = 0; g < f; g++)
			b.call(c, e[g], d && d[g], a)
};
var CPWAac = function(a) {
	this.j = new CPWAv;
	a && this.Jb(a)
}, CPWAbc = function(a) {
	var b = typeof a;
	return "object" == b && a || "function" == b ? "o"
			+ (a[CPWAda] || (a[CPWAda] = ++CPWAea)) : b.substr(0, 1) + a
};
CPWA = CPWAac.prototype;
CPWA.$ = function() {
	return this.j.$()
};
CPWA.add = function(a) {
	this.j.set(CPWAbc(a), a)
};
CPWA.Jb = function(a) {
	a = CPWA8b(a);
	for (var b = a.length, c = 0; c < b; c++)
		this.add(a[c])
};
CPWA.Mb = function(a) {
	a = CPWA8b(a);
	for (var b = a.length, c = 0; c < b; c++)
		this.remove(a[c])
};
CPWA.remove = function(a) {
	return this.j.remove(CPWAbc(a))
};
CPWA.clear = function() {
	this.j.clear()
};
CPWA.contains = function(a) {
	return this.j.D(CPWAbc(a))
};
CPWA.P = function() {
	return this.j.P()
};
CPWA.U = function() {
	return new CPWAac(this)
};
CPWA.ja = function() {
	return this.j.ja(!1)
};
var CPWAcc = function(a) {
	var b = new CPWAac, c = [], d = function(a, f) {
		var g = f + "  ";
		try {
			if (CPWAb(a))
				if (null === a)
					c.push("NULL");
				else if (CPWAe(a))
					c.push('"' + a.replace(/\n/g, "\n" + f) + '"');
				else if (CPWAg(a))
					c.push(String(a).replace(/\n/g, "\n" + f));
				else if (CPWAca(a))
					if (b.contains(a))
						c.push("*** reference loop detected ***");
					else {
						b.add(a);
						c.push("{");
						for (var h in a)
							CPWAg(a[h])
									|| (c.push("\n"), c.push(g), c.push(h
											+ " = "), d(a[h], g));
						c.push("\n" + f + "}")
					}
				else
					c.push(a);
			else
				c.push("undefined")
		} catch (k) {
			c.push("*** " + k + " ***")
		}
	};
	d(a, "");
	return c.join("")
};
var CPWAdc = {
	panoramio_url : "http://www.panoramio.com",
	css_prefix : "panoramio\u002Dwapi",
	locale : "en_US.utf8",
	is_devappserver : "False",
	enable_gen204 : "True",
	frame_referrer : ""
}, CPWAec = function() {
	return CPWAdc.panoramio_url
}, CPWAK = function() {
	return CPWAdc.css_prefix
};
CPWAi("panoramio.StatusCode", {
			Le : 0,
			be : 1,
			He : 2,
			Ke : 3
		});
CPWAi("panoramio.StatusCode.OK", 0);
CPWAi("panoramio.StatusCode.ERROR", 1);
CPWAi("panoramio.StatusCode.NETWORK_ERROR", 2);
CPWAi("panoramio.StatusCode.NO_REQUEST", 3);
var CPWAfc = function(a) {
	CPWAD.call(this);
	this.bd = a;
	this.$a = this.ab = !1;
	this.aa = this.ka = null;
	this.hb = !1
};
CPWAj(CPWAfc, CPWAD);
var CPWAgc = 0;
CPWAfc.prototype.a = function() {
	this.aa && this.aa.e();
	delete this.bd;
	delete this.ab;
	delete this.$a;
	delete this.aa;
	delete this.ka;
	CPWAfc.c.a.call(this)
};
CPWAfc.prototype.Fc = function(a) {
	this.hb = a
};
var CPWAhc = function(a) {
	if (!(a.ka || a.ab || a.$a)) {
		a.ab = !0;
		a.aa = new CPWA5b;
		var b = "loadedImage" + CPWAgc;
		CPWAgc++;
		CPWA7b(a.aa, b, a.bd);
		var b = CPWAh(a.Qd, a), c = CPWAh(a.Pd, a);
		CPWAJb(a.aa, "load", b);
		CPWAJb(a.aa, ["error", "abort"], c);
		var d = Image;
		a.hb && (Image = function() {
			var a = new d;
			a.crossOrigin = "anonymous";
			return a
		});
		a.aa.start();
		Image = d
	}
};
CPWAfc.prototype.Qd = function(a) {
	this.g()
			|| ((a = a.target) && "naturalWidth" in a && "naturalHeight" in a
					&& (CPWA3a(a, CPWAK() + "-img"), CPWAH(a, {
								alt : ""
							}), this.ka = a), this.ab = !1, this.aa = null, this.$a = !0, this
					.dispatchEvent("load"))
};
CPWAfc.prototype.Pd = function() {
	this.g()
			|| (this.ab = !1, this.aa = null, this.$a = !0, this
					.dispatchEvent("error"))
};
var CPWAic = function(a, b) {
	a.$a ? null != a.ka ? b(a.ka, 0) : b(a.ka, 2) : (CPWAJb(a, "load", CPWAh(
					function() {
						b(this.ka, 0)
					}, a)), CPWAJb(a, "error", CPWAh(function() {
						b(this.ka, 2)
					}, a)), CPWAhc(a))
};
var CPWAjc = function(a, b, c) {
	this.width = a;
	this.height = b;
	this.da = c
};
CPWAi("panoramio.PhotoSize", CPWAjc);
CPWAjc.prototype.toString = function() {
	return CPWAZa("<w=%s h=%s %s>", this.width, this.height, this.da
					? "cropped"
					: "whole")
};
var CPWAL = function(a) {
	this.A = new CPWAw(a, !0);
	this.hb = !1
};
CPWAj(CPWAL, CPWAx);
CPWAi("panoramio.ImageStore", CPWAL);
var CPWAkc = new CPWAL(250);
CPWAL.globalStore = CPWAkc;
CPWAL.prototype.a = function() {
	this.A.e();
	delete this.A
};
CPWAL.prototype.Fc = function(a) {
	this.hb = a
};
CPWAL.prototype.setUseCors = CPWAL.prototype.Fc;
CPWAL.prototype.get = function(a, b) {
	var c = this.A.get(a);
	CPWAb(c)
			|| (c = new CPWAfc(a), c.Fc(this.hb), this.A.set(a, c), this.A.$());
	CPWAic(c, CPWAh(function(a, c) {
						if (this.g())
							b(null, 1);
						else if (0 == c) {
							var f = a.cloneNode(!0);
							b(f, 0)
						} else
							b(null, c)
					}, this))
};
CPWAL.prototype.sb = function(a) {
	a = this.A.get(a);
	return CPWAb(a) ? (a = a.ka) ? a.cloneNode(!0) : null : null
};
var CPWAM = function() {
	this.jc = null;
	this.kc = "";
	this.gc = this.lc = null;
	this.hc = "";
	this.Xa = this.Ya = this.ic = null;
	this.Ja = [];
	this.ec = [];
	this.fc = {};
	this.mc = null
};
CPWAj(CPWAM, CPWAx);
CPWAi("panoramio.Photo", CPWAM);
CPWAM.prototype.a = function() {
	CPWAq(this.Ja, function(a) {
				a.e()
			});
	delete this.jc;
	delete this.kc;
	delete this.lc;
	delete this.gc;
	delete this.hc;
	delete this.ic;
	delete this.Ya;
	delete this.Xa;
	delete this.Ja;
	delete this.ec;
	delete this.fc;
	delete this.mc;
	CPWAM.c.a.call(this)
};
var CPWAlc = function(a, b) {
	a.jc = b.photoId;
	a.kc = b.photoTitle || "";
	a.lc = b.photoUrl;
	a.gc = b.ownerId;
	a.hc = b.ownerName || "";
	a.ic = b.ownerUrl;
	a.Ya = b.width;
	a.Xa = b.height;
	CPWAq(a.Ja, function(a) {
				a.e()
			});
	a.Ja = [];
	a.wc = b.position || null;
	a.ec = CPWAt(b.extraHtml || []);
	a.mc = b.tooltipText || null;
	a.fc = b.extraInfo || {};
	CPWAq(b.photoPixelsUrls, function(a) {
				a = new CPWA_a(a.width, a.height, a.cropped, a.url);
				this.Ja.push(a)
			}, a)
};
CPWAM.prototype.ga = function() {
	return this.jc
};
CPWAM.prototype.getPhotoId = CPWAM.prototype.ga;
CPWAM.prototype.oc = function() {
	return this.kc
};
CPWAM.prototype.getPhotoTitle = CPWAM.prototype.oc;
CPWAM.prototype.rc = function() {
	return this.lc
};
CPWAM.prototype.getPhotoUrl = CPWAM.prototype.rc;
CPWAM.prototype.Ad = function() {
	return this.gc
};
CPWAM.prototype.getOwnerId = CPWAM.prototype.Ad;
CPWAM.prototype.nc = function() {
	return this.hc
};
CPWAM.prototype.getOwnerName = CPWAM.prototype.nc;
CPWAM.prototype.gd = function() {
	return this.ic
};
CPWAM.prototype.getOwnerUrl = CPWAM.prototype.gd;
CPWAM.prototype.ie = function() {
	return this.Ya
};
CPWAM.prototype.getWidth = CPWAM.prototype.ie;
CPWAM.prototype.Tc = function() {
	return this.Xa
};
CPWAM.prototype.getHeight = CPWAM.prototype.Tc;
CPWAM.prototype.W = function() {
	return this.wc
};
CPWAM.prototype.getPosition = CPWAM.prototype.W;
CPWAM.prototype.nd = function() {
	return this.mc
};
CPWAM.prototype.getTooltipText = CPWAM.prototype.nd;
CPWAM.prototype.md = function() {
	return this.ec
};
CPWAM.prototype.getExtraHtml = CPWAM.prototype.md;
CPWAM.prototype.ee = function() {
	return this.fc
};
CPWAM.prototype.getExtraInfo = CPWAM.prototype.ee;
CPWAM.prototype.vc = function(a) {
	var b = null, c = null, d = null, e = new CPWAF(a.width, a.height);
	CPWAq(this.Ja, function(f) {
				var g = new CPWAF(f.width, f.height);
				if (!f.da || a.da && CPWAG(g) == CPWAG(e)) {
					var h = CPWAG(g) < CPWAG(e) == a.da
							? e.width / g.width
							: e.height / g.height, g = g.width * g.height;
					if (null === b || 1 < c && h < c || 1 >= h && g < d)
						b = f, c = h, d = g
				}
			});
	return b
};
CPWAM.prototype.getImageDescriptionForSize = CPWAM.prototype.vc;
CPWAM.prototype.toString = function() {
	return CPWAZa("<Photo owner_id=%s id=%s>", this.Ad() || "null", this.ga()
					|| "null")
};
var CPWAmc = function(a, b, c) {
	if (CPWAg(a))
		c && (a = CPWAh(a, c));
	else if (a && "function" == typeof a.handleEvent)
		a = CPWAh(a.handleEvent, a);
	else
		throw Error("Invalid listener argument");
	return 2147483647 < b ? -1 : CPWAa.setTimeout(a, b || 0)
};
var CPWAnc = function(a, b, c) {
	this.yc = a;
	this.Td = b || 0;
	this.Z = c;
	this.Sd = CPWAh(this.Wd, this)
};
CPWAj(CPWAnc, CPWAx);
CPWA = CPWAnc.prototype;
CPWA.Fa = 0;
CPWA.a = function() {
	CPWAnc.c.a.call(this);
	this.stop();
	delete this.yc;
	delete this.Z
};
CPWA.start = function(a) {
	this.stop();
	this.Fa = CPWAmc(this.Sd, CPWAb(a) ? a : this.Td)
};
CPWA.stop = function() {
	0 != this.Fa && CPWAa.clearTimeout(this.Fa);
	this.Fa = 0
};
CPWA.Wd = function() {
	this.Fa = 0;
	this.yc && this.yc.call(this.Z)
};
var CPWAoc = {}, CPWApc = null, CPWAqc = function(a) {
	a = a[CPWAda] || (a[CPWAda] = ++CPWAea);
	delete CPWAoc[a];
	CPWAMa(CPWAoc) && CPWApc && CPWApc.stop()
}, CPWAsc = function() {
	CPWApc || (CPWApc = new CPWAnc(function() {
				CPWArc()
			}, 20));
	var a = CPWApc;
	0 != a.Fa || a.start()
}, CPWArc = function() {
	var a = CPWAia();
	CPWAIa(CPWAoc, function(b) {
				CPWAtc(b, a)
			});
	CPWAMa(CPWAoc) || CPWAsc()
};
var CPWAN = function() {
	CPWAD.call(this);
	this.r = 0;
	this.gb = this.startTime = null
};
CPWAj(CPWAN, CPWAD);
CPWAN.prototype.ra = function() {
	this.L("begin")
};
CPWAN.prototype.qa = function() {
	this.L("end")
};
CPWAN.prototype.L = function(a) {
	this.dispatchEvent(a)
};
var CPWAO = function(a, b, c, d) {
	CPWAN.call(this);
	if (!CPWAc(a) || !CPWAc(b))
		throw Error("Start and end parameters must be arrays");
	if (a.length != b.length)
		throw Error("Start and end points must be the same length");
	this.nb = a;
	this.$d = b;
	this.duration = c;
	this.zd = d;
	this.coords = [];
	this.Zd = !1
};
CPWAj(CPWAO, CPWAN);
CPWAO.prototype.R = 0;
CPWAO.prototype.play = function(a) {
	if (a || 0 == this.r)
		this.R = 0, this.coords = this.nb;
	else if (1 == this.r)
		return !1;
	CPWAqc(this);
	this.startTime = a = CPWAia();
	-1 == this.r && (this.startTime -= this.duration * this.R);
	this.gb = this.startTime + this.duration;
	this.R || this.ra();
	this.L("play");
	-1 == this.r && this.L("resume");
	this.r = 1;
	var b = this[CPWAda] || (this[CPWAda] = ++CPWAea);
	b in CPWAoc || (CPWAoc[b] = this);
	CPWAsc();
	CPWAtc(this, a);
	return !0
};
CPWAO.prototype.stop = function(a) {
	CPWAqc(this);
	this.r = 0;
	a && (this.R = 1);
	CPWAuc(this, this.R);
	this.L("stop");
	this.qa()
};
CPWAO.prototype.a = function() {
	0 == this.r || this.stop(!1);
	this.L("destroy");
	CPWAO.c.a.call(this)
};
var CPWAtc = function(a, b) {
	a.R = (b - a.startTime) / (a.gb - a.startTime);
	1 <= a.R && (a.R = 1);
	CPWAuc(a, a.R);
	1 == a.R ? (a.r = 0, CPWAqc(a), a.L("finish"), a.qa()) : 1 == a.r && a.Cc()
}, CPWAuc = function(a, b) {
	CPWAg(a.zd) && (b = a.zd(b));
	a.coords = Array(a.nb.length);
	for (var c = 0; c < a.nb.length; c++)
		a.coords[c] = (a.$d[c] - a.nb[c]) * b + a.nb[c]
};
CPWAO.prototype.Cc = function() {
	this.L("animate")
};
CPWAO.prototype.L = function(a) {
	this.dispatchEvent(new CPWAvc(a, this))
};
var CPWAvc = function(a, b) {
	CPWAB.call(this, a);
	this.coords = b.coords;
	this.x = b.coords[0];
	this.y = b.coords[1];
	this.z = b.coords[2];
	this.duration = b.duration;
	this.R = b.R;
	this.state = b.r
};
CPWAj(CPWAvc, CPWAB);
var CPWAwc = function() {
	CPWAN.call(this);
	this.G = []
};
CPWAj(CPWAwc, CPWAN);
CPWAwc.prototype.add = function(a) {
	CPWAs(this.G, a) || (this.G.push(a), CPWAC(a, "finish", this.yd, !1, this))
};
CPWAwc.prototype.remove = function(a) {
	CPWAza(this.G, a) && CPWAKb(a, "finish", this.yd, !1, this)
};
CPWAwc.prototype.a = function() {
	CPWAq(this.G, function(a) {
				a.e()
			});
	this.G.length = 0;
	CPWAwc.c.a.call(this)
};
var CPWAxc = function() {
	CPWAwc.call(this);
	this.X = 0
};
CPWAj(CPWAxc, CPWAwc);
CPWAxc.prototype.play = function(a) {
	if (0 == this.G.length)
		return !1;
	if (a || 0 == this.r)
		this.X < this.G.length && 0 != this.G[this.X].r
				&& this.G[this.X].stop(!1), this.X = 0, this.ra();
	else if (1 == this.r)
		return !1;
	this.L("play");
	-1 == this.r && this.L("resume");
	this.startTime = CPWAia();
	this.gb = null;
	this.r = 1;
	this.G[this.X].play(a);
	return !0
};
CPWAxc.prototype.stop = function(a) {
	this.r = 0;
	this.gb = CPWAia();
	if (a)
		for (a = this.X; a < this.G.length; ++a) {
			var b = this.G[a];
			0 == b.r && b.play();
			0 == b.r || b.stop(!0)
		}
	else
		this.X < this.G.length && this.G[this.X].stop(!1);
	this.L("stop");
	this.qa()
};
CPWAxc.prototype.yd = function() {
	1 == this.r
			&& (this.X++, this.X < this.G.length
					? this.G[this.X].play()
					: (this.gb = CPWAia(), this.r = 0, this.L("finish"), this
							.qa()))
};
var CPWAyc = {
	aliceblue : "#f0f8ff",
	antiquewhite : "#faebd7",
	aqua : "#00ffff",
	aquamarine : "#7fffd4",
	azure : "#f0ffff",
	beige : "#f5f5dc",
	bisque : "#ffe4c4",
	black : "#000000",
	blanchedalmond : "#ffebcd",
	blue : "#0000ff",
	blueviolet : "#8a2be2",
	brown : "#a52a2a",
	burlywood : "#deb887",
	cadetblue : "#5f9ea0",
	chartreuse : "#7fff00",
	chocolate : "#d2691e",
	coral : "#ff7f50",
	cornflowerblue : "#6495ed",
	cornsilk : "#fff8dc",
	crimson : "#dc143c",
	cyan : "#00ffff",
	darkblue : "#00008b",
	darkcyan : "#008b8b",
	darkgoldenrod : "#b8860b",
	darkgray : "#a9a9a9",
	darkgreen : "#006400",
	darkgrey : "#a9a9a9",
	darkkhaki : "#bdb76b",
	darkmagenta : "#8b008b",
	darkolivegreen : "#556b2f",
	darkorange : "#ff8c00",
	darkorchid : "#9932cc",
	darkred : "#8b0000",
	darksalmon : "#e9967a",
	darkseagreen : "#8fbc8f",
	darkslateblue : "#483d8b",
	darkslategray : "#2f4f4f",
	darkslategrey : "#2f4f4f",
	darkturquoise : "#00ced1",
	darkviolet : "#9400d3",
	deeppink : "#ff1493",
	deepskyblue : "#00bfff",
	dimgray : "#696969",
	dimgrey : "#696969",
	dodgerblue : "#1e90ff",
	firebrick : "#b22222",
	floralwhite : "#fffaf0",
	forestgreen : "#228b22",
	fuchsia : "#ff00ff",
	gainsboro : "#dcdcdc",
	ghostwhite : "#f8f8ff",
	gold : "#ffd700",
	goldenrod : "#daa520",
	gray : "#808080",
	green : "#008000",
	greenyellow : "#adff2f",
	grey : "#808080",
	honeydew : "#f0fff0",
	hotpink : "#ff69b4",
	indianred : "#cd5c5c",
	indigo : "#4b0082",
	ivory : "#fffff0",
	khaki : "#f0e68c",
	lavender : "#e6e6fa",
	lavenderblush : "#fff0f5",
	lawngreen : "#7cfc00",
	lemonchiffon : "#fffacd",
	lightblue : "#add8e6",
	lightcoral : "#f08080",
	lightcyan : "#e0ffff",
	lightgoldenrodyellow : "#fafad2",
	lightgray : "#d3d3d3",
	lightgreen : "#90ee90",
	lightgrey : "#d3d3d3",
	lightpink : "#ffb6c1",
	lightsalmon : "#ffa07a",
	lightseagreen : "#20b2aa",
	lightskyblue : "#87cefa",
	lightslategray : "#778899",
	lightslategrey : "#778899",
	lightsteelblue : "#b0c4de",
	lightyellow : "#ffffe0",
	lime : "#00ff00",
	limegreen : "#32cd32",
	linen : "#faf0e6",
	magenta : "#ff00ff",
	maroon : "#800000",
	mediumaquamarine : "#66cdaa",
	mediumblue : "#0000cd",
	mediumorchid : "#ba55d3",
	mediumpurple : "#9370db",
	mediumseagreen : "#3cb371",
	mediumslateblue : "#7b68ee",
	mediumspringgreen : "#00fa9a",
	mediumturquoise : "#48d1cc",
	mediumvioletred : "#c71585",
	midnightblue : "#191970",
	mintcream : "#f5fffa",
	mistyrose : "#ffe4e1",
	moccasin : "#ffe4b5",
	navajowhite : "#ffdead",
	navy : "#000080",
	oldlace : "#fdf5e6",
	olive : "#808000",
	olivedrab : "#6b8e23",
	orange : "#ffa500",
	orangered : "#ff4500",
	orchid : "#da70d6",
	palegoldenrod : "#eee8aa",
	palegreen : "#98fb98",
	paleturquoise : "#afeeee",
	palevioletred : "#db7093",
	papayawhip : "#ffefd5",
	peachpuff : "#ffdab9",
	peru : "#cd853f",
	pink : "#ffc0cb",
	plum : "#dda0dd",
	powderblue : "#b0e0e6",
	purple : "#800080",
	red : "#ff0000",
	rosybrown : "#bc8f8f",
	royalblue : "#4169e1",
	saddlebrown : "#8b4513",
	salmon : "#fa8072",
	sandybrown : "#f4a460",
	seagreen : "#2e8b57",
	seashell : "#fff5ee",
	sienna : "#a0522d",
	silver : "#c0c0c0",
	skyblue : "#87ceeb",
	slateblue : "#6a5acd",
	slategray : "#708090",
	slategrey : "#708090",
	snow : "#fffafa",
	springgreen : "#00ff7f",
	steelblue : "#4682b4",
	tan : "#d2b48c",
	teal : "#008080",
	thistle : "#d8bfd8",
	tomato : "#ff6347",
	turquoise : "#40e0d0",
	violet : "#ee82ee",
	wheat : "#f5deb3",
	white : "#ffffff",
	whitesmoke : "#f5f5f5",
	yellow : "#ffff00",
	yellowgreen : "#9acd32"
};
var CPWADc = function(a) {
	var b = {};
	a = String(a);
	var c = "#" == a.charAt(0) ? a : "#" + a;
	if (CPWAzc.test(c))
		return b.Ha = CPWAAc(c), b.type = "hex", b;
	t : {
		var d = a.match(CPWABc);
		if (d) {
			var c = Number(d[1]), e = Number(d[2]), d = Number(d[3]);
			if (0 <= c && 255 >= c && 0 <= e && 255 >= e && 0 <= d && 255 >= d) {
				c = [c, e, d];
				break t
			}
		}
		c = []
	}
	if (c.length)
		return b.Ha = CPWACc(c[0], c[1], c[2]), b.type = "rgb", b;
	if (CPWAyc && (c = CPWAyc[a.toLowerCase()]))
		return b.Ha = c, b.type = "named", b;
	throw Error(a + " is not a valid color string");
}, CPWAEc = /#(.)(.)(.)/, CPWAAc = function(a) {
	if (!CPWAzc.test(a))
		throw Error("'" + a + "' is not a valid hex color");
	4 == a.length && (a = a.replace(CPWAEc, "#$1$1$2$2$3$3"));
	return a.toLowerCase()
}, CPWACc = function(a, b, c) {
	a = Number(a);
	b = Number(b);
	c = Number(c);
	if (isNaN(a) || 0 > a || 255 < a || isNaN(b) || 0 > b || 255 < b
			|| isNaN(c) || 0 > c || 255 < c)
		throw Error('"(' + a + "," + b + "," + c
				+ '") is not a valid RGB color');
	a = CPWAFc(a.toString(16));
	b = CPWAFc(b.toString(16));
	c = CPWAFc(c.toString(16));
	return "#" + a + b + c
}, CPWAGc = function(a, b, c) {
	0 > c ? c += 1 : 1 < c && (c -= 1);
	return 1 > 6 * c ? a + 6 * (b - a) * c : 1 > 2 * c ? b : 2 > 3 * c ? a + 6
			* (b - a) * (2 / 3 - c) : a
}, CPWAzc = /^#(?:[0-9a-f]{3}){1,2}$/i, CPWABc = /^(?:rgb)?\((0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2})\)$/i, CPWAFc = function(
		a) {
	return 1 == a.length ? "0" + a : a
};
var CPWAP = function(a, b, c, d) {
	this.top = a;
	this.right = b;
	this.bottom = c;
	this.left = d
};
CPWA = CPWAP.prototype;
CPWA.U = function() {
	return new CPWAP(this.top, this.right, this.bottom, this.left)
};
CPWA.contains = function(a) {
	return this && a ? a instanceof CPWAP ? a.left >= this.left
			&& a.right <= this.right && a.top >= this.top
			&& a.bottom <= this.bottom : a.x >= this.left && a.x <= this.right
			&& a.y >= this.top && a.y <= this.bottom : !1
};
CPWA.floor = function() {
	this.top = Math.floor(this.top);
	this.right = Math.floor(this.right);
	this.bottom = Math.floor(this.bottom);
	this.left = Math.floor(this.left);
	return this
};
CPWA.round = function() {
	this.top = Math.round(this.top);
	this.right = Math.round(this.right);
	this.bottom = Math.round(this.bottom);
	this.left = Math.round(this.left);
	return this
};
CPWA.scale = function(a, b) {
	var c = CPWAf(b) ? b : a;
	this.left *= a;
	this.right *= a;
	this.top *= c;
	this.bottom *= c;
	return this
};
var CPWAQ = function(a, b, c) {
	CPWAe(b) ? CPWAHc(a, c, b) : CPWAIa(b, CPWAha(CPWAHc, a))
}, CPWAHc = function(a, b, c) {
	var d;
	t : if (d = CPWApa(c), void 0 === a.style[d]
			&& (c = (CPWAz ? "Webkit" : CPWAeb ? "Moz" : CPWAy ? "ms" : CPWAdb
					? "O"
					: null)
					+ CPWAqa(c), void 0 !== a.style[c])) {
		d = c;
		break t
	}
	d && (a.style[d] = b)
}, CPWAIc = function(a, b) {
	var c;
	t : {
		c = CPWA0b(a);
		if (c.defaultView && c.defaultView.getComputedStyle
				&& (c = c.defaultView.getComputedStyle(a, null))) {
			c = c[b] || c.getPropertyValue(b) || "";
			break t
		}
		c = ""
	}
	return c || (a.currentStyle ? a.currentStyle[b] : null) || a.style
			&& a.style[b]
}, CPWAR = function(a, b) {
	var c;
	if (b instanceof CPWAF)
		c = b.height, b = b.width;
	else
		throw Error("missing height argument");
	a.style.width = CPWAJc(b);
	a.style.height = CPWAJc(c)
}, CPWAJc = function(a) {
	"number" == typeof a && (a = Math.round(a) + "px");
	return a
}, CPWALc = function(a) {
	var b = CPWAKc;
	if ("none" != CPWAIc(a, "display"))
		return b(a);
	var c = a.style, d = c.display, e = c.visibility, f = c.position;
	c.visibility = "hidden";
	c.position = "absolute";
	c.display = "inline";
	a = b(a);
	c.display = d;
	c.position = f;
	c.visibility = e;
	return a
}, CPWAKc = function(a) {
	var b = a.offsetWidth, c = a.offsetHeight, d = CPWAz && !b && !c;
	if ((!CPWAb(b) || d) && a.getBoundingClientRect) {
		var e;
		t : {
			try {
				e = a.getBoundingClientRect()
			} catch (f) {
				e = {
					left : 0,
					top : 0,
					right : 0,
					bottom : 0
				};
				break t
			}
			CPWAy
					&& a.ownerDocument.body
					&& (a = a.ownerDocument, e.left -= a.documentElement.clientLeft
							+ a.body.clientLeft, e.top -= a.documentElement.clientTop
							+ a.body.clientTop)
		}
		return new CPWAF(e.right - e.left, e.bottom - e.top)
	}
	return new CPWAF(b, c)
}, CPWAMc = function(a, b) {
	var c = a.style;
	"opacity" in c ? c.opacity = b : "MozOpacity" in c
			? c.MozOpacity = b
			: "filter" in c
					&& (c.filter = "" === b ? "" : "alpha(opacity=" + 100 * b
							+ ")")
}, CPWANc = function(a, b) {
	a.style.display = b ? "" : "none"
};
var CPWAS = function(a, b, c, d, e) {
	CPWAO.call(this, b, c, d, e);
	this.element = a
};
CPWAj(CPWAS, CPWAO);
CPWAS.prototype.rb = CPWAaa;
CPWAS.prototype.Cc = function() {
	this.rb();
	CPWAS.c.Cc.call(this)
};
CPWAS.prototype.qa = function() {
	this.rb();
	CPWAS.c.qa.call(this)
};
CPWAS.prototype.ra = function() {
	this.rb();
	CPWAS.c.ra.call(this)
};
var CPWAOc = function(a, b, c, d, e) {
	if (2 != b.length || 2 != c.length)
		throw Error("Start and end points must be 2D");
	CPWAS.apply(this, arguments)
};
CPWAj(CPWAOc, CPWAS);
CPWAOc.prototype.rb = function() {
	var a;
	if (a = this.Zd)
		CPWAb(this.Bd)
				|| (this.Bd = "rtl" == CPWAIc(this.element, "direction")), a = this.Bd;
	this.element.style[a ? "right" : "left"] = Math.round(this.coords[0])
			+ "px";
	this.element.style.top = Math.round(this.coords[1]) + "px"
};
var CPWAPc = function(a, b, c, d, e) {
	CPWAf(b) && (b = [b]);
	CPWAf(c) && (c = [c]);
	CPWAS.call(this, a, b, c, d, e);
	if (1 != b.length || 1 != c.length)
		throw Error("Start and end points must be 1D");
};
CPWAj(CPWAPc, CPWAS);
CPWAPc.prototype.rb = function() {
	CPWAMc(this.element, this.coords[0])
};
CPWAPc.prototype.show = function() {
	this.element.style.display = ""
};
var CPWAQc = function(a, b, c) {
	CPWAPc.call(this, a, 1, 0, b, c)
};
CPWAj(CPWAQc, CPWAPc);
CPWAQc.prototype.ra = function() {
	this.show();
	CPWAQc.c.ra.call(this)
};
CPWAQc.prototype.qa = function() {
	this.element.style.display = "none";
	CPWAQc.c.qa.call(this)
};
var CPWARc = function(a, b, c) {
	CPWAPc.call(this, a, 0, 1, b, c)
};
CPWAj(CPWARc, CPWAPc);
CPWARc.prototype.ra = function() {
	this.show();
	CPWARc.c.ra.call(this)
};
var CPWASc = function(a) {
	return 3 * a * a - 2 * a * a * a
};
CPWAi("panoramio.Cropping", {
			Je : "no_cropping",
			Ve : "to_square",
			Ue : "to_fill"
		});
CPWAi("panoramio.Cropping.NO_CROPPING", "no_cropping");
CPWAi("panoramio.Cropping.TO_SQUARE", "to_square");
CPWAi("panoramio.Cropping.TO_FILL", "to_fill");
CPWAi("panoramio.events.EventType", {
			Me : "photo_changed",
			Pe : "previous_clicked",
			Ie : "next_clicked",
			Ne : "photo_clicked",
			Oe : "photo_html_created"
		});
CPWAi("panoramio.events.EventType.PHOTO_CHANGED", "photo_changed");
CPWAi("panoramio.events.EventType.PREVIOUS_CLICKED", "previous_clicked");
CPWAi("panoramio.events.EventType.NEXT_CLICKED", "next_clicked");
CPWAi("panoramio.events.EventType.PHOTO_CLICKED", "photo_clicked");
CPWAi("panoramio.events.EventType.PHOTO_HTML_CREATED", "photo_html_created");
var CPWAT = function(a, b, c, d) {
	CPWAB.call(this, "photo_clicked", a);
	this.Ic = b;
	this.wc = c;
	this.n = d
};
CPWAj(CPWAT, CPWAB);
CPWAi("panoramio.events.PhotoClickedEvent", CPWAT);
CPWAT.prototype.Bb = function() {
	return this.Ic
};
CPWAT.prototype.getPhoto = CPWAT.prototype.Bb;
CPWAT.prototype.W = function() {
	return this.wc
};
CPWAT.prototype.getPosition = CPWAT.prototype.W;
CPWAT.prototype.Zb = function() {
	return this.n
};
CPWAT.prototype.getElement = CPWAT.prototype.Zb;
var CPWAU = function(a, b, c, d, e) {
	CPWAB.call(this, "photo_html_created", a);
	this.Ic = b;
	this.n = c;
	this.vd = d;
	this.ud = e
};
CPWAj(CPWAU, CPWAB);
CPWAi("panoramio.events.PhotoHtmlCreatedEvent", CPWAU);
CPWAU.prototype.Bb = function() {
	return this.Ic
};
CPWAU.prototype.getPhoto = CPWAU.prototype.Bb;
CPWAU.prototype.Zb = function() {
	return this.n
};
CPWAU.prototype.getElement = CPWAU.prototype.Zb;
CPWAU.prototype.ge = function() {
	return {
		width : this.vd.width,
		height : this.vd.height
	}
};
CPWAU.prototype.getImageVisibleSize = CPWAU.prototype.ge;
CPWAU.prototype.fe = function() {
	return {
		x : this.ud.x,
		y : this.ud.y
	}
};
CPWAU.prototype.getImageVisibleOffset = CPWAU.prototype.fe;
CPWAi("panoramio.events.listen", function(a, b, c, d, e) {
			return CPWAC(a, b, c, d, e)
		});
CPWAi("panoramio.events.unlisten", function(a, b, c, d, e) {
			return CPWAKb(a, b, c, d, e)
		});
CPWAi("panoramio.events.unlistenByKey", function(a) {
			return CPWALb(a)
		});
var CPWAXc = function(a) {
	var b = {};
	a = String(a);
	var c = "#" == a.charAt(0) ? a : "#" + a;
	if (CPWATc.test(c))
		return b.Ha = CPWAUc(c), b.type = "hex", b;
	var d;
	t : {
		var e = a.match(CPWAVc);
		if (e) {
			var c = Number(e[1]), f = Number(e[2]);
			d = Number(e[3]);
			e = Number(e[4]);
			if (0 <= c && 255 >= c && 0 <= f && 255 >= f && 0 <= d && 255 >= d
					&& 0 <= e && 1 >= e) {
				d = [c, f, d, e];
				break t
			}
		}
		d = []
	}
	if (d.length) {
		a = d[0];
		c = d[1];
		f = d[2];
		d = d[3];
		e = Math.floor(255 * d);
		if (isNaN(e) || 0 > e || 255 < e)
			throw Error('"(' + a + "," + c + "," + f + "," + d
					+ '") is not a valid RGBA color');
		d = CPWAFc(e.toString(16));
		a = CPWACc(a, c, f) + d;
		b.Ha = a;
		b.type = "rgba";
		return b
	}
	t : {
		if (e = a.match(CPWAWc))
			if (c = Number(e[1]), f = Number(e[2]), d = Number(e[3]), e = Number(e[4]), 0 <= c
					&& 360 >= c
					&& 0 <= f
					&& 100 >= f
					&& 0 <= d
					&& 100 >= d
					&& 0 <= e && 1 >= e) {
				e = [c, f, d, e];
				break t
			}
		e = []
	}
	if (e.length) {
		c = e[0];
		f = e[1];
		d = e[2];
		a = e[3];
		e = Math.floor(255 * a);
		if (isNaN(e) || 0 > e || 255 < e)
			throw Error('"(' + c + "," + f + "," + d + "," + a
					+ '") is not a valid HSLA color');
		a = CPWAFc(e.toString(16));
		f /= 100;
		d /= 100;
		var g = e = 0, h = 0, c = c / 360;
		if (0 == f)
			e = g = h = 255 * d;
		else
			var k = h = 0, k = 0.5 > d ? d * (1 + f) : d + f - f * d, h = 2 * d
					- k, e = 255 * CPWAGc(h, k, c + 1 / 3), g = 255
					* CPWAGc(h, k, c), h = 255 * CPWAGc(h, k, c - 1 / 3);
		c = [Math.round(e), Math.round(g), Math.round(h)];
		c = CPWACc(c[0], c[1], c[2]);
		b.Ha = c + a;
		b.type = "hsla";
		return b
	}
	throw Error(a + " is not a valid color string");
}, CPWAYc = /#(.)(.)(.)(.)/, CPWAUc = function(a) {
	if (!CPWATc.test(a))
		throw Error("'" + a + "' is not a valid alpha hex color");
	5 == a.length && (a = a.replace(CPWAYc, "#$1$1$2$2$3$3$4$4"));
	return a.toLowerCase()
}, CPWATc = /^#(?:[0-9a-f]{4}){1,2}$/i, CPWAVc = /^(?:rgba)?\((0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2}),\s?(0|1|0\.\d{0,10})\)$/i, CPWAWc = /^(?:hsla)\((0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2})\%,\s?(0|[1-9]\d{0,2})\%,\s?(0|1|0\.\d{0,10})\)$/i;
var CPWAZc = RegExp("^(?:([^:/?#.]+):)?(?://(?:([^/?#]*)@)?([^/#?]*?)(?::([0-9]+))?(?=[/#?]|$))?([^?#]+)?(?:\\?([^#]*))?(?:#(.*))?$"), CPWA0c = function(
		a) {
	if (CPWA_c) {
		CPWA_c = !1;
		var b = CPWAa.location;
		if (b) {
			var c = b.href;
			if (c && (c = (c = CPWA0c(c)[3] || null) && decodeURIComponent(c))
					&& c != b.hostname)
				throw CPWA_c = !0, Error();
		}
	}
	return a.match(CPWAZc)
}, CPWA_c = CPWAz;
var CPWAV = function(a, b) {
	var c;
	if (a instanceof CPWAV)
		this.O = CPWAb(b) ? b : a.O, CPWA1c(this, a.Na), c = a.Rb, CPWAW(this), this.Rb = c, CPWA2c(
				this, a.ta), CPWA3c(this, a.Qb), CPWA4c(this, a.ib), CPWA5c(
				this, a.fa.U()), c = a.Pb, CPWAW(this), this.Pb = c;
	else if (a && (c = CPWA0c(String(a)))) {
		this.O = !!b;
		CPWA1c(this, c[1] || "", !0);
		var d = c[2] || "";
		CPWAW(this);
		this.Rb = d ? decodeURIComponent(d) : "";
		CPWA2c(this, c[3] || "", !0);
		CPWA3c(this, c[4]);
		CPWA4c(this, c[5] || "", !0);
		CPWA5c(this, c[6] || "", !0);
		c = c[7] || "";
		CPWAW(this);
		this.Pb = c ? decodeURIComponent(c) : ""
	} else
		this.O = !!b, this.fa = new CPWAX(null, 0, this.O)
};
CPWA = CPWAV.prototype;
CPWA.Na = "";
CPWA.Rb = "";
CPWA.ta = "";
CPWA.Qb = null;
CPWA.ib = "";
CPWA.Pb = "";
CPWA.le = !1;
CPWA.O = !1;
CPWA.toString = function() {
	var a = [], b = this.Na;
	b && a.push(CPWA6c(b, CPWA7c), ":");
	if (b = this.ta) {
		a.push("//");
		var c = this.Rb;
		c && a.push(CPWA6c(c, CPWA7c), "@");
		a.push(encodeURIComponent(String(b)));
		b = this.Qb;
		null != b && a.push(":", String(b))
	}
	if (b = this.ib)
		this.ta && "/" != b.charAt(0) && a.push("/"), a.push(CPWA6c(b, "/" == b
						.charAt(0) ? CPWA8c : CPWA9c));
	(b = this.fa.toString()) && a.push("?", b);
	(b = this.Pb) && a.push("#", CPWA6c(b, CPWA$c));
	return a.join("")
};
CPWA.U = function() {
	return new CPWAV(this)
};
var CPWA1c = function(a, b, c) {
	CPWAW(a);
	a.Na = c ? b ? decodeURIComponent(b) : "" : b;
	a.Na && (a.Na = a.Na.replace(/:$/, ""))
}, CPWA2c = function(a, b, c) {
	CPWAW(a);
	a.ta = c ? b ? decodeURIComponent(b) : "" : b
}, CPWA3c = function(a, b) {
	CPWAW(a);
	if (b) {
		b = Number(b);
		if (isNaN(b) || 0 > b)
			throw Error("Bad port number " + b);
		a.Qb = b
	} else
		a.Qb = null
}, CPWA4c = function(a, b, c) {
	CPWAW(a);
	a.ib = c ? b ? decodeURIComponent(b) : "" : b
}, CPWA5c = function(a, b, c) {
	CPWAW(a);
	b instanceof CPWAX ? (a.fa = b, a.fa.Lc(a.O)) : (c
			|| (b = CPWA6c(b, CPWAad)), a.fa = new CPWAX(b, 0, a.O))
}, CPWAcd = function(a, b, c) {
	CPWAW(a);
	CPWAc(c) || (c = [String(c)]);
	CPWAbd(a.fa, b, c)
}, CPWAW = function(a) {
	if (a.le)
		throw Error("Tried to modify a read-only Uri");
};
CPWAV.prototype.Lc = function(a) {
	this.O = a;
	this.fa && this.fa.Lc(a);
	return this
};
var CPWA6c = function(a, b) {
	return CPWAe(a) ? encodeURI(a).replace(b, CPWAdd) : null
}, CPWAdd = function(a) {
	a = a.charCodeAt(0);
	return "%" + (a >> 4 & 15).toString(16) + (a & 15).toString(16)
}, CPWA7c = /[#\/\?@]/g, CPWA9c = /[\#\?:]/g, CPWA8c = /[\#\?]/g, CPWAad = /[\#\?@]/g, CPWA$c = /#/g, CPWAX = function(
		a, b, c) {
	this.M = a || null;
	this.O = !!c
}, CPWAY = function(a) {
	if (!a.o && (a.o = new CPWAv, a.q = 0, a.M))
		for (var b = a.M.split("&"), c = 0; c < b.length; c++) {
			var d = b[c].indexOf("="), e = null, f = null;
			0 <= d
					? (e = b[c].substring(0, d), f = b[c].substring(d + 1))
					: e = b[c];
			e = decodeURIComponent(e.replace(/\+/g, " "));
			e = CPWAed(a, e);
			a.add(e, f ? decodeURIComponent(f.replace(/\+/g, " ")) : "")
		}
};
CPWA = CPWAX.prototype;
CPWA.o = null;
CPWA.q = null;
CPWA.$ = function() {
	CPWAY(this);
	return this.q
};
CPWA.add = function(a, b) {
	CPWAY(this);
	this.M = null;
	a = CPWAed(this, a);
	var c = this.o.get(a);
	c || this.o.set(a, c = []);
	c.push(b);
	this.q++;
	return this
};
CPWA.remove = function(a) {
	CPWAY(this);
	a = CPWAed(this, a);
	return this.o.D(a) ? (this.M = null, this.q -= this.o.get(a).length, this.o
			.remove(a)) : !1
};
CPWA.clear = function() {
	this.o = this.M = null;
	this.q = 0
};
CPWA.D = function(a) {
	CPWAY(this);
	a = CPWAed(this, a);
	return this.o.D(a)
};
CPWA.N = function() {
	CPWAY(this);
	for (var a = this.o.P(), b = this.o.N(), c = [], d = 0; d < b.length; d++)
		for (var e = a[d], f = 0; f < e.length; f++)
			c.push(b[d]);
	return c
};
CPWA.P = function(a) {
	CPWAY(this);
	var b = [];
	if (CPWAe(a))
		this.D(a) && (b = CPWAAa(b, this.o.get(CPWAed(this, a))));
	else {
		a = this.o.P();
		for (var c = 0; c < a.length; c++)
			b = CPWAAa(b, a[c])
	}
	return b
};
CPWA.set = function(a, b) {
	CPWAY(this);
	this.M = null;
	a = CPWAed(this, a);
	this.D(a) && (this.q -= this.o.get(a).length);
	this.o.set(a, [b]);
	this.q++;
	return this
};
CPWA.get = function(a, b) {
	var c = a ? this.P(a) : [];
	return 0 < c.length ? String(c[0]) : b
};
var CPWAbd = function(a, b, c) {
	a.remove(b);
	0 < c.length
			&& (a.M = null, a.o.set(CPWAed(a, b), CPWAt(c)), a.q += c.length)
};
CPWAX.prototype.toString = function() {
	if (this.M)
		return this.M;
	if (!this.o)
		return "";
	for (var a = [], b = this.o.N(), c = 0; c < b.length; c++)
		for (var d = b[c], e = encodeURIComponent(String(d)), d = this.P(d), f = 0; f < d.length; f++) {
			var g = e;
			"" !== d[f] && (g += "=" + encodeURIComponent(String(d[f])));
			a.push(g)
		}
	return this.M = a.join("&")
};
CPWAX.prototype.U = function() {
	var a = new CPWAX;
	a.M = this.M;
	this.o && (a.o = this.o.U(), a.q = this.q);
	return a
};
var CPWAed = function(a, b) {
	var c = String(b);
	a.O && (c = c.toLowerCase());
	return c
};
CPWAX.prototype.Lc = function(a) {
	a && !this.O && (CPWAY(this), this.M = null, CPWA$b(this.o, function(a, c) {
				var d = c.toLowerCase();
				c != d && (this.remove(c), CPWAbd(this, d, a))
			}, this));
	this.O = a
};
var CPWAfd = {
	"wapi.css" : ".panoramio\u002Dwapi\u002Dphoto {\u000A  width: 500px\u003B\u000A  height: 375px\u003B\u000A  position: relative\u003B\u000A  font\u002Dfamily: arial, sans\u002Dserif\u003B\u000A  text\u002Dalign: center\u003B\u000A  overflow: hidden\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dcrop\u002Ddiv {\u000A  position: relative\u003B\u000A  overflow: hidden\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dloaded\u002Dimg {\u000A  position: absolute\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto a img {\u000A  border: 0\u003B\u000A}\u000A.panoramio\u002Dwapi\u002Dphoto a.panoramio\u002Dwapi img {\u000A  margin\u002Dtop: 180px\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dimages {\u000A  overflow: hidden\u003B\u000A  background\u002Dcolor: black\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Doverlay {\u000A  z\u002Dindex: 1\u003B\u000A  position: absolute\u003B\u000A  background: transparent\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Doverlay\u002Dloading {\u000A  background: transparent url(http://www.panoramio.com/img/wapi/loading.png) no\u002Drepeat scroll center center\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Doverlay\u002Derror {\u000A  background: gray url(http://www.panoramio.com/img/wapi/error.png) no\u002Drepeat scroll center center\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrow,\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrowbox {\u000A  text\u002Dalign: left\u003B\u000A  position: absolute\u003B\u000A  padding: 0 10px\u003B\u000A  top: 0\u003B\u000A  height: 100%\u003B\u000A  text\u002Ddecoration: none\u003B\u000A  color: #fff\u003B\u000A  outline: none\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrow {\u000A  z\u002Dindex: 10\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrowbox {\u000A  z\u002Dindex: 5\u003B\u000A  \u000A  background\u002Dcolor: #fff\u003B\u000A  opacity: 0\u003B\u000A  filter: alpha(opacity\u003D0)\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dprev,\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrowbox\u002Dprev {\u000A  left: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dnext,\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Darrowbox\u002Dnext {\u000A  right: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dtitle {\u000A  background\u002Dcolor: #000\u003B\u000A  position: absolute\u003B\u000A  bottom: 0\u003B\u000A  left: 0\u003B\u000A  margin: 0\u003B\u000A  padding: 1%\u003B\u000A  width: 98%\u003B\u000A  color: #fff\u003B\u000A  font\u002Dsize: 80%\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dphoto\u002Dtitle {\u000A  font\u002Dweight: bold\u003B\u000A  color: #fff\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphoto .panoramio\u002Dwapi\u002Dphoto\u002Dauthor {\u000A  color: #fff\u003B\u000A}\u000A\u000A\u000A\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist {\u000A  width: 500px\u003B\u000A  height: 375px\u003B\u000A  position: relative\u003B\u000A  font\u002Dfamily: arial, sans\u002Dserif\u003B\u000A  text\u002Dalign: center\u003B\u000A  overflow: hidden\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dimages {\u000A  overflow: hidden\u003B\u000A  background\u002Dcolor: white\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Doverlay {\u000A  z\u002Dindex: 1\u003B\u000A  position: absolute\u003B\u000A  background: transparent\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Doverlay\u002Dloading {\u000A  background: transparent url(http://www.panoramio.com/img/wapi/loading.png) no\u002Drepeat scroll center center\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Doverlay\u002Derror {\u000A  background: gray url(http://www.panoramio.com/img/wapi/error.png) no\u002Drepeat scroll center center\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dimages\u000A  .panoramio\u002Dwapi\u002Dcrop\u002Ddiv {\u000A  position: relative\u003B\u000A  overflow: hidden\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dimages\u000A  .panoramio\u002Dwapi\u002Dwrapper\u002Ddiv {\u000A  border\u002Dstyle: solid\u003B\u000A  border\u002Dcolor: transparent\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dloaded\u002Dimg\u002Ddiv,\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dempty\u002Dimg\u002Ddiv {\u000A  position: relative\u003B\u000A  margin: 0\u003B\u000A  border: 0\u003B\u000A  padding: 0\u003B\u000A  float: left\u003B\u000A  overflow: hidden\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dextra\u002Dhtml\u002Ddiv {\u000A  position: absolute\u003B\u000A  z\u002Dindex: 1\u003B\u000A  left: 0\u003B\u000A  top: 0\u003B\u000A  margin: 0\u003B\u000A  border: 0\u003B\u000A  padding: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dimages\u000A  .panoramio\u002Dwapi\u002Dloaded\u002Dimg\u002Ddiv .panoramio\u002Dwapi\u002Dwrapper\u002Ddiv:hover {\u000A  border\u002Dcolor: #ff3333\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Dloaded\u002Dimg {\u000A  position: absolute\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist a img {\u000A  border: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Darrow,\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Darrowbox {\u000A  text\u002Dalign: left\u003B\u000A  position: absolute\u003B\u000A  text\u002Ddecoration: none\u003B\u000A  color: #fff\u003B\u000A  outline: none\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Darrow {\u000A  z\u002Dindex: 10\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist .panoramio\u002Dwapi\u002Darrowbox {\u000A  z\u002Dindex: 5\u003B\u000A  \u000A  background\u002Dcolor: #fff\u003B\u000A  opacity: 0\u003B\u000A  filter: alpha(opacity\u003D0)\u003B\u000A}\u000A\u000A\u000A\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh a.panoramio\u002Dwapi\u002Darrow img {\u000A  margin\u002Dtop: 180px\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Dimages {\u000A  padding: 0 15px\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Doverlay {\u000A  padding: 0 15px\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Darrow,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Darrowbox {\u000A  top: 0\u003B\u000A  height: 100%\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Dprev,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Darrowbox\u002Dprev {\u000A  left: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Dnext,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dh .panoramio\u002Dwapi\u002Darrowbox\u002Dnext {\u000A  right: 0\u003B\u000A}\u000A\u000A\u000A\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv a.panoramio\u002Dwapi\u002Darrow img {\u000A  margin\u002Dleft: 180px\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Dimages {\u000A  padding: 15px 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Doverlay {\u000A  padding: 15px 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Darrow,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Darrowbox {\u000A  left: 0\u003B\u000A  width: 100%\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Dprev,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Darrowbox\u002Dprev {\u000A  top: 0\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Dnext,\u000A.panoramio\u002Dwapi\u002Dphotolist\u002Dv .panoramio\u002Dwapi\u002Darrowbox\u002Dnext {\u000A  bottom: 0\u003B\u000A}\u000A\u000A\u000A\u000A\u000A.panoramio\u002Dwapi\u002Dtos {\u000A  background\u002Dcolor: transparent !important\u003B\u000A  color: #666 !important\u003B\u000A  border: none !important\u003B\u000A  margin: 0 !important\u003B\u000A  padding: 0 2px !important\u003B\u000A}\u000A\u000A.panoramio\u002Dwapi\u002Dtos * {\u000A  border: none !important\u003B\u000A  text\u002Ddecoration: none !important\u003B\u000A  text\u002Dtransform: none !important\u003B\u000A  font\u002Dfamily: verdana, sans\u002Dserif !important\u003B\u000A  font\u002Dsize: 11px !important\u003B\u000A  font\u002Dweight: normal !important\u003B\u000A  font\u002Dstyle: normal !important\u003B\u000A  margin: 0 !important\u003B\u000A  vertical\u002Dalign: middle !important\u003B\u000A}\u000A",
	"photo_widget.html" : "\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dphoto\u0022\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dprev\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_widget/left_arrow.png\u0022\u000A         alt\u003D\u0022Previous\u0022\u000A         width\u003D\u002239\u0022 height\u003D\u002238\u0022\u000A         title\u003D\u0022Previous\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dprev\u0022 style\u003D\u0022width: 39px\u0022\u003E\u003C/span\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Doverlay\u0022\u003E\u003C/div\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dimages\u0022\u003E\u003C/div\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dnext\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_widget/right_arrow.png\u0022\u000A         alt\u003D\u0022Next\u0022\u000A         width\u003D\u002239\u0022 height\u003D\u002238\u0022\u000A         title\u003D\u0022Next\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dnext\u0022 style\u003D\u0022width: 39px\u0022\u003E\u003C/span\u003E\u000A  \u003Cp class\u003D\u0022panoramio\u002Dwapi\u002Dtitle\u0022\u003E\u000A    \u000A      \u000A        \u000A          \u000A            \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Dphoto\u002Dtitle\u0022 target\u003D\u0022_top\u0022 href\u003D\u0022#\u0022\u003E\u003C/a\u003E\u0026nbsp\u003Bby \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Dphoto\u002Dauthor\u0022 target\u003D\u0022_top\u0022 href\u003D\u0022#\u0022\u003E\u003C/a\u003E\u000A          \u000A        \u000A      \u000A    \u000A  \u003C/p\u003E\u000A\u003C/div\u003E\u000A\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dtos\u0022\u003E\u000A\u003C/div\u003E\u000A",
	"photo_list_widget_h.html" : "\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dphotolist\u002Dh panoramio\u002Dwapi\u002Dphotolist\u0022\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dprev\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_list_widget/left_arrow.png\u0022\u000A         alt\u003D\u0022Previous\u0022\u000A         width\u003D\u002239\u0022 height\u003D\u002238\u0022\u000A         title\u003D\u0022Previous\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dprev\u0022 style\u003D\u0022width: 39px\u0022\u003E\u003C/span\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Doverlay\u0022\u003E\u003C/div\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dimages\u0022\u003E\u003C/div\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dnext\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_list_widget/right_arrow.png\u0022\u000A         alt\u003D\u0022Next\u0022\u000A         width\u003D\u002239\u0022 height\u003D\u002238\u0022\u000A         title\u003D\u0022Next\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dnext\u0022 style\u003D\u0022width: 39px\u0022\u003E\u003C/span\u003E\u000A\u003C/div\u003E\u000A\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dtos\u0022\u003E\u000A\u003C/div\u003E\u000A",
	"photo_list_widget_v.html" : "\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dphotolist\u002Dv panoramio\u002Dwapi\u002Dphotolist\u0022\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dprev\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_list_widget/up_arrow.png\u0022\u000A         alt\u003D\u0022Previous\u0022\u000A         width\u003D\u002238\u0022 height\u003D\u002239\u0022\u000A         title\u003D\u0022Previous\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dprev\u0022 style\u003D\u0022height: 39px\u0022\u003E\u003C/span\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Doverlay\u0022\u003E\u003C/div\u003E\u000A  \u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dimages\u0022\u003E\u003C/div\u003E\u000A  \u003Ca class\u003D\u0022panoramio\u002Dwapi\u002Darrow panoramio\u002Dwapi\u002Dnext\u0022 href\u003D\u0022#\u0022\u003E\u000A    \u003Cimg src\u003D\u0022http://www.panoramio.com/img/wapi/photo_list_widget/down_arrow.png\u0022\u000A         alt\u003D\u0022Next\u0022\u000A         width\u003D\u002238\u0022 height\u003D\u002239\u0022\u000A         title\u003D\u0022Next\u0022 /\u003E\u000A  \u003C/a\u003E\u000A  \u003Cspan class\u003D\u0022panoramio\u002Dwapi\u002Darrowbox panoramio\u002Dwapi\u002Darrowbox\u002Dnext\u0022 style\u003D\u0022height: 39px\u0022\u003E\u003C/span\u003E\u000A\u003C/div\u003E\u000A\u003Cdiv class\u003D\u0022panoramio\u002Dwapi\u002Dtos\u0022\u003E\u000A\u003C/div\u003E\u000A",
	"attribution_block.html" : "\u003Ca href\u003D\u0022http://www.panoramio.com\u0022 target\u003D\u0022_top\u0022\u000A   \u003E\u003Cimg src\u003D\u0022http://www.panoramio.com/img/logo\u002Dtos.png\u0022\u000A         width\u003D\u002267\u0022 height\u003D\u002214\u0022 /\u003E\u003C/a\u003E\u000A\u003Cspan\u003E\u000A  Photos are copyrighted by their owners\u000A\u003C/span\u003E\u000A"
};
CPWAi("panoramio.tos.Style", {
			Ee : "hidden",
			Fe : "hidden_no_name",
			Ce : "default",
			De : "default_add"
		});
CPWAi("panoramio.tos.Style.HIDDEN", "hidden");
CPWAi("panoramio.tos.Style.HIDDEN_NO_NAME", "hidden_no_name");
CPWAi("panoramio.tos.Style.DEFAULT", "default");
CPWAi("panoramio.tos.Style.DEFAULT_ADD", "default_add");
var CPWAgd = function() {
};
CPWAi("panoramio.TermsOfServiceWidgetOptions", CPWAgd);
CPWAgd.prototype.width = void 0;
CPWAgd.prototype.width = CPWAgd.prototype.width;
var CPWAZ = function(a, b) {
	var c = b || {};
	this.n = CPWAe(a) ? document.getElementById(a) : a;
	c = c.width || 400;
	100 > c && (c = 100);
	CPWAhd(this, this.n, "default", c);
	this.Ta();
	CPWAid(this);
	var d = CPWAjd(this);
	this.h = new CPWAF(c, d)
};
CPWAj(CPWAZ, CPWAx);
CPWAi("panoramio.TermsOfServiceWidget", CPWAZ);
CPWAZ.prototype.a = function() {
	this.n && CPWAJ(this.n);
	delete this.n;
	CPWAZ.c.a.call(this)
};
CPWAZ.prototype.Ta = function() {
	var a = CPWAI("div", {
				"class" : CPWAK() + "-tos"
			});
	CPWAJ(this.n);
	this.n.appendChild(a)
};
CPWAZ.prototype.Tc = function() {
	return this.h.height
};
CPWAZ.prototype.getHeight = CPWAZ.prototype.Tc;
var CPWAkd = function(a) {
	this.pe = a
};
CPWAi("panoramio.TermsOfServiceViolationError", CPWAkd);
CPWAkd.prototype.toString = function() {
	return "Terms of Service violation: " + this.pe
};
CPWAkd.prototype.toString = CPWAkd.prototype.toString;
function CPWAld(a, b, c, d) {
	this.sc = a;
	this.hd = b;
	this.style = c;
	this.width = d;
	this.xc = null
}
CPWAj(CPWAld, CPWAx);
CPWAld.prototype.a = function() {
	delete this.sc;
	delete this.hd;
	delete this.style;
	delete this.width;
	delete this.xc;
	CPWAld.c.a.call(this)
};
var CPWAmd = function(a) {
	this.fb = null;
	a && (this.fb = new CPWAV(a));
	var b;
	t : {
		if (this.fb) {
			a = ["^www\\.panoramio\\.com$", "\\.corp\\.google\\.com$",
					"\\.panoramio-prod-live-4\\.appspot\\.com$",
					"\\.panoramio-fe\\.appspot\\.com$",
					"^pano-prod-test-gyuri-3\\.appspot\\.com$"];
			"True" == CPWAdc.is_devappserver && a.push("^localhost$");
			var c = this.fb.ta, d = this.fb.ib;
			for (b in a)
				if (RegExp(a[b]).test(c)
						&& ("/wapi/template/" != d.substr(0, 15) || "/wapi/template/internal-test.html" == d
								.substr(0, 33))) {
					b = !0;
					break t
				}
		}
		b = !1
	}
	this.Pa = b;
	this.Ba = [];
	this.Kb = null
};
CPWAj(CPWAmd, CPWAx);
CPWAmd.prototype.a = function() {
	CPWAq(this.Ba, function(a) {
				a.e()
			});
	delete this.fb;
	delete this.Pa;
	delete this.Ba;
	delete this.Kb;
	CPWAmd.c.a.call(this)
};
var CPWAnd = function(a) {
	return CPWAya(function(b) {
				return b.sc === a
			})
}, CPWAid = function(a) {
	var b = CPWAnd(a);
	if ("hidden" != b.style && "hidden_no_name" != b.style) {
		a = CPWATb(document, null, CPWAK() + "-tos", b.hd)[0];
		var c;
		c = CPWAYb(CPWAfd["attribution_block.html"]);
		CPWAQ(a, "width", b.width - 4 + "px");
		CPWAJ(a);
		a.appendChild(c);
		b.xc = a;
		var d;
		t : {
			c = CPWAIc(a, "backgroundColor");
			var e = [0, 0, 0], b = 1;
			if ("transparent" == c)
				b = 0;
			else
				try {
					d = CPWAXc(c).Ha, d = CPWAUc(d), e = [
							parseInt(d.substr(1, 2), 16),
							parseInt(d.substr(3, 2), 16),
							parseInt(d.substr(5, 2), 16),
							parseInt(d.substr(7, 2), 16) / 255], b = e[3]
				} catch (f) {
					try {
						var g = CPWADc(c).Ha, g = CPWAAc(g), e = [
								parseInt(g.substr(1, 2), 16),
								parseInt(g.substr(3, 2), 16),
								parseInt(g.substr(5, 2), 16)]
					} catch (h) {
						d = {
							yb : "white",
							Yb : "black"
						};
						break t
					}
				}
			d = (e[0] + e[1] + e[2]) / 3;
			d = 0.3 > b ? {
				yb : "white",
				Yb : "black"
			} : 127 < d ? {
				yb : null,
				Yb : "black"
			} : {
				yb : null,
				Yb : "white"
			}
		}
		g = "width: " + (CPWALc(a).width - 4) + "px; color: " + d.Yb
				+ " !important";
		d.yb && (g += "; background-color: " + d.yb + " !important");
		CPWAH(a, {
					style : g
				})
	}
}, CPWAjd = function(a) {
	a = CPWAnd(a);
	return "default" == a.style ? CPWALc(a.xc).height : 0
};
CPWAmd.prototype.ae = function() {
	if (this.g())
		throw new CPWAkd("ToS controller unexpectedly disposed of");
	var a = !1, a = 0 == this.Ba.length || this.Pa ? !0 : CPWAva(this.Ba,
			function(a) {
				return "hidden" != a.style && "hidden_no_name" != a.style
			});
	if (!a)
		throw CPWAq(this.Ba, function(a) {
					CPWAJ(a.sc.element)
				}), new CPWAkd("All widgets have their attribution block hidden");
};
var CPWAhd = function(a, b, c, d) {
	var e = CPWAwa;
	a = new CPWAld(a, b, c, d);
	e.Ba.push(a);
	null !== e.Kb && CPWAa.clearTimeout(e.Kb);
	e.Kb = CPWAmc(e.ae, 3E3, e)
}, CPWAwa = new CPWAmd(window.location.href);
var CPWA_ = function() {
};
CPWAi("panoramio.PhotoListWidgetOptions", CPWA_);
CPWA_.Orientation = {
	Ge : "horizontal",
	We : "vertical"
};
CPWAi("panoramio.PhotoListWidgetOptions.Orientation.HORIZONTAL", "horizontal");
CPWAi("panoramio.PhotoListWidgetOptions.Orientation.VERTICAL", "vertical");
CPWA_.prototype.width = void 0;
CPWA_.prototype.width = CPWA_.prototype.width;
CPWA_.prototype.height = void 0;
CPWA_.prototype.height = CPWA_.prototype.height;
CPWA_.prototype.H = void 0;
CPWA_.prototype.croppedPhotos = CPWA_.prototype.H;
CPWA_.prototype.Oc = void 0;
CPWA_.prototype.disableDefaultEvents = CPWA_.prototype.Oc;
CPWA_.prototype.ce = void 0;
CPWA_.prototype.columns = CPWA_.prototype.ce;
CPWA_.prototype.rows = void 0;
CPWA_.prototype.rows = CPWA_.prototype.rows;
CPWA_.prototype.orientation = void 0;
CPWA_.prototype.orientation = CPWA_.prototype.orientation;
CPWA_.prototype.Nc = void 0;
CPWA_.prototype.attributionStyle = CPWA_.prototype.Nc;
CPWA_.prototype.b = void 0;
CPWA_.prototype.photoMargins = CPWA_.prototype.b;
CPWA_.prototype.Wc = void 0;
CPWA_.prototype.openLinksInNewWindow = CPWA_.prototype.Wc;
CPWA_.prototype.Y = void 0;
CPWA_.prototype.photoUrlGetter = CPWA_.prototype.Y;
CPWA_.prototype.zb = void 0;
CPWA_.prototype.buffering = CPWA_.prototype.zb;/*
 * Portions
 * of
 * this
 * code
 * are
 * from
 * MochiKit,
 * received
 * by
 * The
 * Closure
 * Authors
 * under
 * the
 * MIT
 * license.
 * All
 * other
 * code
 * is
 * Copyright
 * 2005-2009
 * The
 * Closure
 * Authors.
 * All
 * Rights
 * Reserved.
 */
var CPWAod = function(a, b) {
	this.lb = [];
	this.Kd = b || null;
	this.pb = this.jb = !1;
	this.Vb = void 0;
	this.rd = this.Id = this.Gc = !1;
	this.Wb = 0;
	this.Da = null;
	this.Jd = 0
};
CPWAod.prototype.sd = function(a, b) {
	this.Gc = !1;
	CPWApd(this, a, b)
};
var CPWApd = function(a, b, c) {
	a.jb = !0;
	a.Vb = c;
	a.pb = !b;
	CPWAqd(a)
}, CPWAsd = function(a) {
	if (a.jb) {
		if (!a.rd)
			throw new CPWArd;
		a.rd = !1
	}
}, CPWAtd = function(a) {
	return CPWAva(a.lb, function(a) {
				return CPWAg(a[1])
			})
}, CPWAqd = function(a) {
	a.Wb && a.jb && CPWAtd(a) && (CPWAa.clearTimeout(a.Wb), delete a.Wb);
	a.Da && (a.Da.Jd--, delete a.Da);
	for (var b = a.Vb, c = !1, d = !1; a.lb.length && !a.Gc;) {
		var e = a.lb.shift(), f = e[0], g = e[1], e = e[2];
		if (f = a.pb ? g : f)
			try {
				var h = f.call(e || a.Kd, b);
				CPWAb(h)
						&& (a.pb = a.pb && (h == b || h instanceof Error), a.Vb = b = h);
				b instanceof CPWAod && (d = !0, a.Gc = !0)
			} catch (k) {
				b = k, a.pb = !0, CPWAtd(a) || (c = !0)
			}
	}
	a.Vb = b;
	d
			&& (d = b, h = CPWAh(a.sd, a, !0), f = CPWAh(a.sd, a, !1), d.lb
					.push([h, f, void 0]), d.jb && CPWAqd(d), b.Id = !0);
	c && (a.Wb = CPWAa.setTimeout(CPWARb(b), 0))
}, CPWArd = function() {
	CPWAo.call(this)
};
CPWAj(CPWArd, CPWAo);
CPWArd.prototype.message = "Deferred has already fired";
CPWArd.prototype.name = "AlreadyCalledError";
var CPWAxd = function(a, b) {
	var c = b || {}, d = c.document || document, e = document
			.createElement("SCRIPT"), f = {
		ye : e,
		qb : void 0
	}, g = new CPWAod(0, f), h = null, k = null != c.timeout ? c.timeout : 5E3;
	0 < k && (h = window.setTimeout(function() {
				CPWAud(e, !0);
				var b = new CPWAvd(1, "Timeout reached for loading script " + a);
				CPWAsd(g);
				CPWApd(g, !1, b)
			}, k), f.qb = h);
	e.onload = e.onreadystatechange = function() {
		e.readyState && "loaded" != e.readyState && "complete" != e.readyState
				|| (CPWAud(e, c.Md || !1, h), CPWAsd(g), CPWApd(g, !0, null))
	};
	e.onerror = function() {
		CPWAud(e, !0, h);
		var b = new CPWAvd(0, "Error while loading script " + a);
		CPWAsd(g);
		CPWApd(g, !1, b)
	};
	CPWAH(e, {
				type : "text/javascript",
				charset : "UTF-8",
				src : a
			});
	CPWAwd(d).appendChild(e);
	return g
}, CPWAwd = function(a) {
	var b = a.getElementsByTagName("HEAD");
	return b && 0 != b.length ? b[0] : a.documentElement
}, CPWAud = function(a, b, c) {
	null != c && CPWAa.clearTimeout(c);
	a.onload = CPWAaa;
	a.onerror = CPWAaa;
	a.onreadystatechange = CPWAaa;
	b && window.setTimeout(function() {
				CPWAZb(a)
			}, 0)
}, CPWAvd = function(a, b) {
	var c = "Jsloader error (code #" + a + ")";
	b && (c += ": " + b);
	CPWAo.call(this, c);
	this.code = a
};
CPWAj(CPWAvd, CPWAo);
var CPWAyd = function(a, b) {
	this.Od = new CPWAV(a);
	this.Nd = b ? b : "callback";
	this.qb = 5E3
}, CPWAzd = 0;
CPWAyd.prototype.send = function(a, b, c, d) {
	a = a || null;
	d = d || "_" + (CPWAzd++).toString(36) + CPWAia().toString(36);
	CPWAa._callbacks_ || (CPWAa._callbacks_ = {});
	var e = this.Od.U();
	if (a)
		for (var f in a)
			a.hasOwnProperty && !a.hasOwnProperty(f) || CPWAcd(e, f, a[f]);
	b
			&& (CPWAa._callbacks_[d] = CPWAAd(d, b), CPWAcd(e, this.Nd,
					"_callbacks_." + d));
	b = CPWAxd(e.toString(), {
				timeout : this.qb,
				Md : !0
			});
	b.lb.push([null, CPWABd(d, a, c), void 0]);
	b.jb && CPWAqd(b);
	return {
		Fa : d,
		we : b
	}
};
var CPWABd = function(a, b, c) {
	return function() {
		CPWACd(a, !1);
		c && c(b)
	}
}, CPWAAd = function(a, b) {
	return function(c) {
		CPWACd(a, !0);
		b.apply(void 0, arguments)
	}
}, CPWACd = function(a, b) {
	CPWAa._callbacks_[a]
			&& (b ? delete CPWAa._callbacks_[a] : CPWAa._callbacks_[a] = CPWAaa)
};
var CPWADd = {
	ze : "all",
	Qe : "public",
	Re : "recent"
};
CPWAi("panoramio.PhotoSet", CPWADd);
CPWAi("panoramio.PhotoSet.ALL", "all");
CPWAi("panoramio.PhotoSet.PUBLIC", "public");
CPWAi("panoramio.PhotoSet.RECENT", "recent");
var CPWAEd = {
	Ae : "date",
	Be : "date_desc"
};
CPWAi("panoramio.PhotoOrder", CPWAEd);
CPWAi("panoramio.PhotoOrder.DATE", "date");
CPWAi("panoramio.PhotoOrder.DATE_DESC", "date_desc");
var CPWA0 = function() {
};
CPWAi("panoramio.PhotoRequestOptions", CPWA0);
CPWA0.prototype.ke = void 0;
CPWA0.prototype.ids = CPWA0.prototype.ke;
CPWA0.prototype.ue = void 0;
CPWA0.prototype.user = CPWA0.prototype.ue;
CPWA0.prototype.group = void 0;
CPWA0.prototype.group = CPWA0.prototype.group;
CPWA0.prototype.te = void 0;
CPWA0.prototype.tag = CPWA0.prototype.te;
CPWA0.prototype.set = void 0;
CPWA0.prototype.set = CPWA0.prototype.set;
CPWA0.prototype.rect = void 0;
CPWA0.prototype.rect = CPWA0.prototype.rect;
CPWA0.prototype.re = void 0;
CPWA0.prototype.order = CPWA0.prototype.re;
CPWA0.prototype.ve = void 0;
CPWA0.prototype.userAvatars = CPWA0.prototype.ve;
CPWA0.prototype.de = void 0;
CPWA0.prototype.extendedInfo = CPWA0.prototype.de;
var CPWAFd = function() {
};
CPWAj(CPWAFd, CPWAx);
CPWAFd.prototype.a = function() {
	CPWAFd.c.a.call(this)
};
CPWAFd.prototype.Sb = function(a, b) {
	b(new CPWAv, null, 1)
};
CPWAFd.prototype.Mc = function(a, b, c, d) {
	d(null, new CPWAv, null, 1)
};
var CPWAGd = function(a, b) {
	this.Rd = a;
	this.Xd = b
}, CPWA1 = function(a, b) {
	this.kd = a;
	this.ld = CPWANa(b)
};
CPWAj(CPWA1, CPWAFd);
CPWA1.prototype.a = function() {
	delete this.kd;
	delete this.ld;
	CPWA1.c.a.call(this)
};
CPWA1.prototype.Cd = function(a) {
	if (0 == a.length)
		return null;
	var b = CPWAua(a, function(a, b) {
				return Math.min(a, b)
			}, a[0]);
	a = CPWAua(a, function(a, b) {
				return Math.max(a, b)
			}, a[0]) + 1;
	b = Math.max(0, b);
	a = Math.max(0, a);
	return b >= a ? null : new CPWAGd(null, {
				offset : b,
				length : a - b
			})
};
CPWA1.prototype.Yd = function(a, b, c) {
	function d(a, b) {
		if (!(b >= g.length)) {
			var c = g[b], d = new CPWAM;
			CPWAlc(d, a);
			e.set(c, d)
		}
	}
	var e = new CPWAv;
	if (this.g())
		b(e, null, 1);
	else {
		var f = c.photos, g = a.Rd || c.positions;
		a = null;
		c.has_more || 0 == f.length
				|| (c = f.length - 1, c < g.length && (a = g[c] + 1));
		CPWAq(f, d, this);
		e.N();
		b(e, a, 0)
	}
};
var CPWAHd = function(a, b, c) {
	var d = b.Xd, e = new CPWAyd(a.kd, "callback");
	b = CPWAh(a.Yd, a, b, c);
	a = CPWANa(a.ld);
	CPWAPa(a, d);
	e.qb = 11E3;
	e.send(a, b, function() {
				c(new CPWAv, null, 2)
			})
};
CPWA1.prototype.Sb = function(a, b) {
	var c = this.Cd(a);
	c ? CPWAHd(this, c, b) : b(new CPWAv, null, 0)
};
var CPWAId = function(a, b) {
	var c = a.ja(!0), c = CPWAHa(c, function(c) {
				return a.get(c).ga() == b
			}), d;
	try {
		d = CPWAFa(c).next()
	} catch (e) {
		if (e != CPWADa)
			throw e;
		d = null
	}
	return d
};
CPWA1.prototype.Mc = function(a, b, c, d) {
	(b = new CPWAGd(null, {
				base_photo_id : a,
				offset : -b,
				length : b + 1 + c
			})) ? CPWAHd(this, b, CPWAh(function(b, c, g) {
						var h = CPWAId(b, a);
						d(h, b, c, g)
					}, this)) : d(null, new CPWAv, null, 0)
};
var CPWA2 = function(a, b, c, d) {
	CPWA1.call(this, a, b);
	this.J = CPWAt(c);
	this.jd = CPWAt(d);
	this.Aa = this.J.length
};
CPWAj(CPWA2, CPWA1);
CPWA2.prototype.a = function() {
	delete this.J;
	delete this.jd;
	delete this.Aa;
	CPWA2.c.a.call(this)
};
CPWA2.prototype.Cd = function(a) {
	if (0 == a.length)
		return null;
	var b = [], c = [];
	CPWAq(a, function(a) {
				if (!(0 > a || a >= this.Aa)) {
					var d = this.J[a];
					b.push(a);
					c.push(d)
				}
			}, this);
	if (0 == c.length)
		return null;
	var d = this.jd;
	a = {
		ids : CPWAta(c, function(a) {
					return CPWAta(d, function(b) {
								return a[b]
							}).join(",")
				}).join(";"),
		offset : 0,
		length : c.length
	};
	return new CPWAGd(b, a)
};
CPWA2.prototype.Sb = function(a, b) {
	CPWA2.c.Sb.call(this, a, CPWAh(function(a, d, e) {
						b(a, this.Aa, e)
					}, this))
};
CPWA2.prototype.Mc = function(a, b, c, d) {
	b = CPWAxa(this.J, function(b) {
				return b.xe == a
			});
	-1 == b && (b = null);
	d(b, new CPWAv, null, 0)
};
var CPWAJd = function(a, b) {
	this.A = new CPWAw(a, !0);
	this.Ob = b;
	this.Aa = null
};
CPWAj(CPWAJd, CPWAx);
CPWA = CPWAJd.prototype;
CPWA.a = function() {
	this.A.e();
	this.Ob.e();
	delete this.A;
	delete this.Ob;
	delete this.Aa
};
CPWA.Dc = function() {
	return this.Aa
};
CPWA.get = function(a, b) {
	this.V([a], function(c, d) {
				var e = c.get(a, null);
				b(e, d)
			})
};
CPWA.td = function(a, b, c, d, e) {
	this.g() ? a(b, 1) : (c.N(), null !== d && (this.Aa = d), CPWAGa(c.ja(!0),
			function(a) {
				var d = c.get(a), e = this.A.get(a);
				b.D(a) ? d.e() : CPWAb(e)
						? (d.e(), b.set(a, e))
						: (b.set(a, d), this.A.set(a, d), this.A.$())
			}, this), a(b, e))
};
CPWA.V = function(a, b) {
	var c = new CPWAv, d = [];
	CPWAq(a, function(a) {
				var b = this.A.get(String(a));
				CPWAb(b) ? c.set(a, b) : d.push(a)
			}, this);
	0 == d.length ? b(c, 0) : this.Ob.Sb(d, CPWAh(this.td, this, b, c))
};
CPWA.sb = function(a) {
	return this.Ca([a]).get(a, null)
};
CPWA.Ca = function(a) {
	var b = new CPWAv;
	CPWAq(a, function(a) {
				var d = this.A.get(String(a));
				CPWAb(d) && b.set(a, d)
			}, this);
	return b
};
CPWA.ma = function(a, b, c, d) {
	var e = this.wb(a);
	null !== e ? d(e, 0) : this.Ob.Mc(a, b, c, CPWAh(function(a, b, c, e) {
						this.td(function() {
								}, new CPWAv, b, c, e);
						d(a, e)
					}, this))
};
CPWA.wb = function(a) {
	var b = null;
	this.A.map(function(c, d) {
				c.ga() == a && (b = d)
			});
	return b
};
var CPWAKd = function() {
};
CPWAi("panoramio.InvalidPhotoRequestError", CPWAKd);
CPWAKd.prototype.toString = function() {
	return "Invalid request type"
};
CPWAKd.prototype.toString = CPWAKd.prototype.toString;
var CPWALd = function(a) {
	var b;
	b = 0;
	"ids" in a && b++;
	"user" in a && b++;
	"group" in a && b++;
	"tag" in a && b++;
	"set" in a && b++;
	2 < b || 2 == b
			&& !("user" in a && "tag" in a || "user" in a && "group" in a)
			? b = !1
			: (0 == b && (a.set = "public"), b = "userAvatars" in a
					&& (!("user" in a || "group" in a || "ids" in a)
							|| "rect" in a || "order" in a || "set" in a)
					|| "rect" in a
					&& ("ids" in a || "user" in a && "tag" in a || "user" in a
							&& "group" in a || !("sw" in a.rect
							&& "ne" in a.rect && "lat" in a.rect.sw
							&& "lng" in a.rect.sw && "lat" in a.rect.ne && "lng" in a.rect.ne))
					|| "order" in a
					&& (!CPWALa(CPWAEd, a.order) || "ids" in a || "set" in a
							|| "tag" in a && !("user" in a) || "rect" in a)
					? !1
					: "set" in a && !CPWALa(CPWADd, a.set) ? !1 : !0);
	if (!b)
		throw new CPWAKd;
	this.Hb = CPWANa(a);
	a = this.Hb;
	b = CPWAwa.Pa;
	var c = CPWAec() + "/wapi/data", d = [];
	a.userAvatars && b
			? (c += "/get_users", d = ["userId"])
			: (c += "/get_photos", d = ["userId", "photoId"]);
	var e = {
		v : "1",
		key : "dummykey"
	};
	"rect" in a
			&& (e.minx = a.rect.sw.lng, e.miny = a.rect.sw.lat, e.maxx = a.rect.ne.lng, e.maxy = a.rect.ne.lat);
	"order" in a && (e.order = a.order);
	"extendedInfo" in a && b && (e.extinfo = a.extendedInfo);
	"ids" in a
			? a = new CPWA2(c, e, a.ids, d)
			: ("user" in a && (e.user = a.user), "group" in a
					&& (e.group = a.group), "tag" in a && (e.tag = a.tag), "set" in a
					&& (e.set = a.set), a = new CPWA1(c, e));
	this.uc = new CPWAJd(2E3, a)
};
CPWAj(CPWALd, CPWAx);
CPWAi("panoramio.PhotoRequest", CPWALd);
CPWALd.prototype.a = function() {
	this.uc.e();
	delete this.Hb;
	delete this.uc;
	CPWALd.c.a.call(this)
};
var CPWAOd = function(a, b) {
	var c = new CPWAMd(a.uc);
	return new CPWANd(c, CPWAkc, b)
};
CPWAi("panoramio.PhotoRequest.prototype.dispose", CPWALd.prototype.e);
var CPWA3 = function(a, b, c, d, e) {
	this.n = a;
	this.Fd = e || "<unnamed>";
	this.qc = CPWANa(b);
	this.fd = c;
	this.ed = d;
	this.Ka = !1;
	this.oa = null;
	CPWANc(this.n, !1);
	this.Hc(CPWAPd(this), !1)
};
CPWAj(CPWA3, CPWAx);
CPWA3.prototype.a = function() {
	this.n && CPWAJ(this.n);
	this.oa && this.oa.a();
	delete this.n;
	delete this.Fd;
	delete this.qc;
	delete this.fd;
	delete this.ed;
	delete this.Ka;
	delete this.oa;
	CPWA3.c.a.call(this)
};
var CPWAPd = function(a) {
	return CPWAva(a.fd, function(a) {
				a = CPWAta(a, function(a) {
							return this.qc[a]
						}, this);
				return !CPWAs(a, !1)
			}, a)
};
CPWA3.prototype.Hc = function(a, b) {
	this.oa
			&& (this.oa.e(), this.oa = null, CPWAMc(this.n, ""), CPWANc(this.n,
					a), this.Ka = a);
	if (a != this.Ka)
		if (b) {
			var c = new CPWAxc;
			c.add(new (a ? CPWARc : CPWAQc)(this.n, 200));
			this.oa = c;
			c.play();
			CPWAC(c, "finish", CPWAh(function(a) {
								this.g()
										|| (this.Ka = a, this.oa = null, CPWAMc(
												this.n, ""))
							}, this, a))
		} else
			CPWANc(this.n, a), CPWAMc(this.n, ""), this.Ka = a
};
CPWA3.prototype.set = function(a, b) {
	this.qc[a] = b;
	var c = CPWAs(this.ed, a) && !CPWAy, d = CPWAPd(this);
	this.Hc(d, c)
};
var CPWA4 = function(a) {
	CPWAD.call(this);
	this.element = CPWAe(a) ? document.getElementById(a) : a;
	this.Va = this.Wa = this.T = this.C = null;
	this.m = {};
	this.t = new CPWAF(0, 0);
	this.w = this.B = this.b = null;
	this.H = !1;
	this.Ua = [];
	this.Y = CPWAh(this.Ed, this)
};
CPWAj(CPWA4, CPWAD);
CPWA4.prototype.a = function() {
	this.element && CPWAJ(this.element);
	CPWAIa(this.m, function(a) {
				a.e()
			});
	this.C && this.C.e();
	this.T && this.T.e();
	CPWAQd(this);
	delete this.C;
	delete this.T;
	delete this.element;
	delete this.Wa;
	delete this.Va;
	delete this.m;
	delete this.t;
	delete this.b;
	delete this.H;
	delete this.Y;
	delete this.Ua;
	CPWA4.c.a.call(this)
};
var CPWAQd = function(a) {
	CPWAq(a.Ua, CPWALb);
	a.Ua = []
};
CPWA4.prototype.k = function(a, b) {
	b && (b = CPWAK() + "-" + b);
	return CPWATb(document, a, b, this.element)
};
var CPWARd = function(a, b) {
	a.Wa = b;
	a.m.prev.set("notAtListEdge", !b)
}, CPWASd = function(a, b) {
	a.Va = b;
	a.m.next.set("notAtListEdge", !b)
};
CPWA4.prototype.Sc = function() {
	return this.Wa
};
CPWAi("panoramio.Widget.prototype.getAtStart", CPWA4.prototype.Sc);
CPWA4.prototype.Rc = function() {
	return this.Va
};
CPWAi("panoramio.Widget.prototype.getAtEnd", CPWA4.prototype.Rc);
var CPWATd = function(a, b, c) {
	var d = a.k("a", "prev")[0], e = a.k("a", "next")[0], f = CPWAh(
			function(a) {
				this.g()
						|| (a.preventDefault(), a.stopPropagation(), this
								.dispatchEvent("previous_clicked"))
			}, a), g = CPWAh(function(a) {
				this.g()
						|| (a.preventDefault(), a.stopPropagation(), this
								.dispatchEvent("next_clicked"))
			}, a);
	CPWAC(d, "click", f);
	CPWAC(e, "click", g);
	CPWAs(b, "previous_clicked") || (d = CPWAh(function() {
				this.g() || this.ob(this.W() - c)
			}, a), CPWAC(a, "previous_clicked", d));
	CPWAs(b, "next_clicked") || (b = CPWAh(function() {
				this.g() || this.ob(this.W() + c)
			}, a), CPWAC(a, "next_clicked", b))
};
CPWA4.prototype.Qc = function(a) {
	this.m.prev.set("userEnabled", a);
	this.m.prevBox.set("userEnabled", a)
};
CPWAi("panoramio.Widget.prototype.enablePreviousArrow", CPWA4.prototype.Qc);
CPWA4.prototype.Pc = function(a) {
	this.m.next.set("userEnabled", a);
	this.m.nextBox.set("userEnabled", a)
};
CPWAi("panoramio.Widget.prototype.enableNextArrow", CPWA4.prototype.Pc);
var CPWAUd = function(a) {
	return a instanceof CPWALd ? {
		request : a,
		"new" : !1
	} : CPWAca(a) ? (a = new CPWALd(a), {
		request : a,
		"new" : !0
	}) : {
		request : null,
		"new" : !1
	}
};
CPWA4.prototype.Ed = function(a) {
	a = new CPWAV(a.rc());
	CPWAW(a);
	a.fa.set("source", "wapi");
	var b = CPWAVd;
	CPWAW(a);
	a.fa.set("referrer", b);
	return a.toString()
};
CPWA4.prototype.Uc = function() {
	return this.t
};
CPWAi("panoramio.Widget.prototype.getPhotoSize", CPWA4.prototype.Uc);
var CPWAWd = function(a, b, c) {
	var d = CPWAI("div", {
				"class" : CPWAK() + "-wrapper-div"
			}), e = CPWAI("div", {
				"class" : CPWAK() + "-crop-div"
			}), f = b.ad.cloneNode(!0);
	CPWA3a(f, CPWAK() + "-loaded-img");
	b = new CPWAF(b.description.width, b.description.height);
	if (a.H)
		b.scale(CPWAG(b) < CPWAG(a.t) ? a.t.width / b.width : a.t.height
				/ b.height);
	else {
		var g = a.t;
		b.scale(CPWAG(b) > CPWAG(g) ? g.width / b.width : g.height / b.height)
	}
	b.floor();
	CPWAR(f, b);
	CPWAQ(d, "padding-left", a.w.left + "px");
	CPWAQ(d, "padding-right", a.w.right + "px");
	CPWAQ(d, "padding-top", a.w.top + "px");
	CPWAQ(d, "padding-bottom", a.w.bottom + "px");
	CPWAQ(d, "border-left-width", a.B.left + "px");
	CPWAQ(d, "border-right-width", a.B.right + "px");
	CPWAQ(d, "border-top-width", a.B.top + "px");
	CPWAQ(d, "border-bottom-width", a.B.bottom + "px");
	g = new CPWAP(a.t.height - b.height, a.t.width - b.width, 0, 0);
	g.bottom = Math.floor(g.top / 2);
	g.top -= g.bottom;
	g.left = Math.floor(g.right / 2);
	g.right -= g.left;
	a.H
			? (CPWAR(e, a.t), CPWAQ(f, "left", g.left + "px"), CPWAQ(f, "top",
					g.top + "px"), CPWAQ(d, "margin-left", a.b.left + "px"), CPWAQ(
					d, "margin-right", a.b.right + "px"), CPWAQ(d,
					"margin-top", a.b.top + "px"), CPWAQ(d, "margin-bottom",
					a.b.bottom + "px"), b = a.t.U(), g = new CPWAE(a.b.left
							+ a.B.left + a.w.left, a.b.top + a.B.top + a.w.top))
			: (CPWAR(e, b), CPWAQ(f, "left", "0"), CPWAQ(f, "top", "0"), CPWAQ(
					d, "margin-left", a.b.left + g.left + "px"), CPWAQ(d,
					"margin-right", a.b.right + g.right + "px"), CPWAQ(d,
					"margin-top", a.b.top + g.top + "px"), CPWAQ(d,
					"margin-bottom", a.b.bottom + g.bottom + "px"), b = b.U(), g = new CPWAE(
					a.b.left + a.B.left + a.w.left + g.left, a.b.top + a.B.top
							+ a.w.top + g.top));
	c = a.Y(c);
	var h = {};
	c && (h.href = c);
	c = CPWAI("a", h);
	a.Ia && CPWAH(c, {
				target : "_blank"
			});
	c.appendChild(f);
	e.appendChild(c);
	d.appendChild(e);
	return {
		imageVisibleSize : b,
		imageVisibleOffset : g,
		newImage : f,
		wrapped : d
	}
}, CPWAXd = {
	LOADING : "loading",
	be : "error"
};
CPWAi("panoramio.Widget.OverlayStatus", CPWAXd);
var CPWAYd = function(a, b) {
	var c = a.k("div", "overlay")[0];
	CPWAIa(CPWAXd, function(a) {
				CPWA5a(c, CPWAK() + "-overlay-" + a)
			}, a);
	b ? (CPWA3a(c, CPWAK() + "-overlay-" + b), CPWANc(c, !0)) : CPWANc(c, !1)
};
CPWA4.prototype.ma = function(a, b) {
	this.C ? this.C.ma(a, function(a, d) {
				0 != d ? b(null) : b(a)
			}) : b(null)
};
CPWAi("panoramio.Widget.prototype.getPositionById", CPWA4.prototype.ma);
var CPWAZd = function(a, b) {
	CPWA3.call(this, a, {
				userEnabled : !0
			}, [["userEnabled"]], [], b)
};
CPWAj(CPWAZd, CPWA3);
CPWAZd.prototype.Hc = function(a) {
	CPWANc(this.n, a);
	this.Ka = a
};
var CPWA_d = null, CPWA2d = function(a, b) {
	if ("True" == CPWAdc.enable_gen204) {
		var c = "" + b.width + "," + b.height + "," + b.da, d;
		CPWA_d || (CPWA_d = new CPWAac);
		d = CPWA_d;
		var e = "" + a + "-" + ("" + b.width + "," + b.height + "," + b.da);
		d.contains(e) || 5E4 <= d.$() ? d = !0 : (d.add(e), d = !1);
		d
				|| (c in CPWA0d || (d = new CPWA1d(b), CPWA0d[c] = d), CPWAu(
						CPWA0d, c).log(a))
	}
}, CPWA3d = null, CPWAVd = null, CPWA4d = function(a) {
	var b = ["^www.panoramio.com$", ".corp.google.com$",
			".panoramio-prod-live-4.appspot.com$", ".panoramio-fe.appspot.com$"];
	"True" == CPWAdc.is_devappserver && b.push("^localhost$");
	for (var c in b)
		if (RegExp(b[c]).test(a))
			return !0;
	return !1
}, CPWA1d = function(a) {
	this.dc = "photo_view";
	this.h = [a.width, a.height, a.da ? "c" : "n"];
	this.J = [];
	this.cd = 15E3;
	this.Ib = null;
	this.dd = CPWAC(window, "unload", CPWAh(this.Ac, this))
};
CPWAj(CPWA1d, CPWAx);
CPWA1d.prototype.a = function() {
	CPWALb(this.dd);
	delete this.dc;
	delete this.h;
	delete this.J;
	delete this.cd;
	delete this.Ib;
	delete this.dd;
	CPWA1d.c.a.call(this)
};
var CPWA0d = {};
CPWA1d.prototype.log = function(a) {
	this.J.push(a);
	1024 < CPWA5d(this.dc, {
				usage : CPWA3d,
				size : this.h,
				ids : this.J
			}).toString().length && (this.J.pop(), this.Ac(), this.J.push(a));
	this.Ib || (this.Ib = CPWAmc(this.Ld, this.cd, this))
};
CPWA1d.prototype.Ac = function() {
	!this.g() && this.J.length && (CPWA6d(this.dc, {
				usage : CPWA3d,
				size : this.h,
				ids : this.J
			}), this.J = [])
};
CPWA1d.prototype.Ld = function() {
	this.g() || (this.J.length && this.Ac(), this.Ib = null)
};
var CPWA5d = function(a, b) {
	var c = {
		sa : "T",
		ct : "panoramio:wapi." + a,
		cad : CPWA7d(b)
	}, d = CPWA9b(c);
	if ("undefined" == typeof d)
		throw Error("Keys are undefined");
	for (var e = new CPWAX(null, 0, void 0), c = CPWA8b(c), f = 0; f < d.length; f++) {
		var g = d[f], h = c[f];
		CPWAc(h) ? CPWAbd(e, g, h) : e.add(g, h)
	}
	d = new CPWAV(null, void 0);
	CPWA2c(d, "www.google.com");
	CPWA4c(d, "/maps/gen_204");
	e && CPWA5c(d, e);
	return d
}, CPWA6d = function(a, b) {
	var c = CPWA5d(a, b), c = new CPWAyd(c);
	c.qb = 2E3;
	c.send({})
}, CPWA8d = function(a) {
	return CPWAe(a) || "boolean" == typeof a || CPWAf(a)
}, CPWA9d = function(a) {
	var b = function(a) {
		return CPWA8d(a) || (CPWAd(a) ? CPWAr(a, CPWA8d) : !1)
	};
	return CPWAca(a) ? CPWAr(CPWAKa(a), CPWA8d) && CPWAr(CPWAJa(a), b) : !1
}, CPWA$d = function(a) {
	return a.toString()
}, CPWAae = function(a, b) {
	var c = CPWA$d(b), d;
	d = CPWAd(a) && CPWAr(a, CPWA8d) ? CPWAta(a, CPWA$d).join(",") : CPWA$d(a);
	return c + ":" + d
}, CPWAbe = function(a) {
	var b = {}, c;
	for (c in a)
		b[c] = CPWAae.call(void 0, a[c], c);
	return CPWAJa(b).join(";")
}, CPWAce = function(a) {
	this.data = a
};
CPWAce.prototype.toString = function() {
	return "Invalid CSI payload: " + CPWAcc(this.data)
};
var CPWA7d = function(a) {
	if (CPWAd(a) && CPWAr(a, CPWA9d))
		return CPWAta(a, CPWAbe).join("|");
	if (CPWA9d(a))
		return CPWAbe(a);
	if (CPWAd(a) && CPWAr(a, CPWA8d))
		return CPWAta(a, CPWA$d).join(",");
	if (CPWA8d(a))
		return CPWA$d(a);
	throw new CPWAce(a);
};
CPWAi("panoramio.WidgetTransitionType", {
			NONE : "none",
			Te : "slide_previous",
			Se : "slide_next"
		});
CPWAi("panoramio.WidgetTransitionType.NONE", "none");
CPWAi("panoramio.WidgetTransitionType.SLIDE_PREVIOUS", "slide_previous");
CPWAi("panoramio.WidgetTransitionType.SLIDE_NEXT", "slide_next");
var CPWA6 = function(a, b, c) {
	CPWA4.call(this, a);
	a = c || {};
	this.S = null;
	this.Qa = [];
	this.Db = [];
	this.H = CPWAs([!0, "to_square", "to_fill"], a.croppedPhotos);
	this.Ra = CPWAs([!0, "to_square"], a.croppedPhotos);
	this.ha = a.columns || 5;
	this.ia = a.rows || 1;
	this.$c = a.orientation || "horizontal";
	this.K = this.ia * this.ha;
	this.xa = !0;
	this.Ia = a.openLinksInNewWindow || !1;
	this.b = null;
	c = CPWAwa.Pa;
	this.b = a.photoMargins && c
			? new CPWAP(a.photoMargins.top, a.photoMargins.right,
					a.photoMargins.bottom, a.photoMargins.left)
			: CPWA5(this) ? new CPWAP(3, 2, 3, 2) : new CPWAP(2, 3, 2, 3);
	this.B = a.photoBorders && c
			? new CPWAP(a.photoBorders.top, a.photoBorders.right,
					a.photoBorders.bottom, a.photoBorders.left)
			: new CPWAP(1, 1, 1, 1);
	this.w = null;
	this.w = a.photoPaddings && c
			? new CPWAP(a.photoPaddings.top, a.photoPaddings.right,
					a.photoPaddings.bottom, a.photoPaddings.left)
			: new CPWAP(0, 0, 0, 0);
	this.Zc = CPWA5(this) ? this.ha : this.ia;
	this.$b = CPWA5(this) ? this.ia : this.ha;
	c = a.width || 750;
	var d = a.height || 150;
	CPWAhd(this, this.element, a.attributionStyle || "default", c);
	this.Ta();
	CPWAid(this);
	d -= CPWAjd(this);
	this.h = new CPWAF(c, d);
	this.Sa = new CPWAF(this.h.width - (CPWA5(this) ? 30 : 0), this.h.height
					- (CPWA5(this) ? 0 : 30));
	this.Fb = 200;
	this.va = CPWAu(a, "buffering", {
				metadata : {
					prefetchBefore : 2 * this.K,
					prefetchAfter : 3 * this.K + 6,
					hysteresis : this.K
				},
				images : {
					prefetchBefore : this.K,
					prefetchAfter : this.K
				}
			});
	c = Math.floor(this.Sa.width / this.ha);
	d = Math.floor(this.Sa.height / this.ia);
	c -= this.b.left + this.b.right + this.B.left + this.B.right + this.w.left
			+ this.w.right;
	d -= this.b.top + this.b.bottom + this.B.top + this.B.bottom + this.w.top
			+ this.w.bottom;
	if (c > this.Fb) {
		var e = c - this.Fb;
		0 != e % 2 && e++;
		c -= e;
		a.photoMargins
				|| (this.b.left += Math.round(e / 2), e -= Math.round(e / 2));
		this.b.right += e
	}
	d > this.Fb
			&& (e = d - this.Fb, 0 != e % 2 && e++, d -= e, a.photoMargins
					|| (this.b.top += Math.round(e / 2), e -= Math.round(e / 2)), this.b.bottom += e);
	this.Ra
			&& (c > d
					? (e = c - d, c -= e, a.photoMargins
							|| (this.b.left += Math.round(e / 2), e -= Math
									.round(e / 2)), this.b.right += e)
					: (e = d - c, d -= e, a.photoMargins
							|| (this.b.top += Math.round(e / 2), e -= Math
									.round(e / 2)), this.b.bottom += e));
	this.t = new CPWAF(c, d);
	this.t.floor();
	this.wa = new CPWAjc(this.t.width, this.t.height, this.H);
	this.ya = new CPWAF(this.t.width + this.b.left + this.b.right + this.B.left
					+ this.B.right + this.w.left + this.w.right, this.t.height
					+ this.b.top + this.b.bottom + this.B.top + this.B.bottom
					+ this.w.top + this.w.bottom);
	for (c = 0; c < this.K; c++)
		this.Qa.push(null), this.Db.push(null);
	this.cc();
	c = this.k("a", "prev")[0];
	var d = this.k("span", "arrowbox-prev")[0], e = this.k("a", "next")[0], f = this
			.k("span", "arrowbox-next")[0];
	CPWAPa(this.m, {
				prev : new CPWAde(c, "list prev"),
				prevBox : new CPWAZd(d, "list prevbox"),
				next : new CPWAde(e, "list next"),
				nextBox : new CPWAZd(f, "list nextbox")
			});
	this.Oa = this.bc(a.disableDefaultEvents);
	this.Y = a.photoUrlGetter || this.Y;
	b && this.na(b)
};
CPWAj(CPWA6, CPWA4);
CPWAi("panoramio.PhotoListWidget", CPWA6);
CPWA6.prototype.a = function() {
	delete this.S;
	delete this.Qa;
	delete this.Db;
	delete this.H;
	delete this.Ra;
	delete this.ha;
	delete this.ia;
	delete this.$c;
	delete this.K;
	delete this.xa;
	delete this.Ia;
	delete this.Zc;
	delete this.$b;
	delete this.h;
	delete this.Sa;
	delete this.va;
	delete this.wa;
	delete this.ya;
	delete this.Oa;
	CPWA6.c.a.call(this)
};
var CPWAde = function(a, b) {
	CPWA3.call(this, a, {
				userEnabled : !0,
				notAtListEdge : !1
			}, [["userEnabled", "notAtListEdge"]], [], b)
};
CPWAj(CPWAde, CPWA3);
var CPWA5 = function(a) {
	return "horizontal" == a.$c
};
CPWA6.prototype.na = function(a) {
	this.C && this.C.e();
	this.T && this.T.e();
	this.Va = this.Wa = this.T = this.C = null;
	if (a) {
		var b = CPWAUd(a);
		a = b.request;
		b = b["new"];
		this.C = CPWAOd(a, this.wa);
		this.C.eb(this.va);
		this.T = b ? a : null;
		this.xa = !("userAvatars" in a.Hb)
	}
};
CPWA6.prototype.setRequest = CPWA6.prototype.na;
CPWA6.prototype.Vc = function() {
	return this.h
};
CPWA6.prototype.getSize = CPWA6.prototype.Vc;
CPWA6.prototype.bc = function(a) {
	var b = [], b = !0 === a ? ["previous_clicked", "next_clicked",
			"photo_clicked"] : "array" == CPWAba(a) ? a : [];
	CPWATd(this, b, this.K);
	return b
};
CPWA6.prototype.cc = function() {
	CPWAR(this.element.childNodes[0], this.h);
	var a = this.k("div", "images")[0];
	CPWAR(a, this.Sa);
	a = this.k("div", "overlay")[0];
	CPWAR(a, this.Sa);
	var b = 0, c = "";
	CPWA5(this)
			? (b = Math.round((this.h.height - 38) / 2) + "px", c = "margin-top")
			: (b = Math.round((this.h.width - 39) / 2) + "px", c = "margin-left");
	CPWAq(this.k("a", "arrow"), function(a) {
				CPWAQ(a.getElementsByTagName("img")[0], c, b)
			})
};
CPWA6.prototype.W = function() {
	return this.S
};
CPWA6.prototype.getPosition = CPWA6.prototype.W;
CPWA6.prototype.he = function() {
	return this.Qa
};
CPWA6.prototype.getPhotos = CPWA6.prototype.he;
CPWA6.prototype.Ta = function() {
	var a = void 0, a = CPWA5(this)
			? CPWAfd["photo_list_widget_h.html"]
			: CPWAfd["photo_list_widget_v.html"], a = CPWAYb(a);
	CPWAJ(this.element);
	this.element.appendChild(a)
};
CPWA6.prototype.Bc = function() {
	var a = CPWAK() + "-empty-img", b = CPWAI("div", {
				"class" : CPWAK() + "-empty-img-div"
			});
	CPWAR(b, this.ya);
	a = CPWAI("img", {
				"class" : a,
				src : CPWAec() + "/img/transparent.gif",
				alt : ""
			});
	CPWAR(a, this.t);
	CPWAQ(a, "margin-left", this.b.left + "px");
	CPWAQ(a, "margin-right", this.b.right + "px");
	CPWAQ(a, "margin-top", this.b.top + "px");
	CPWAQ(a, "margin-bottom", this.b.bottom + "px");
	b.appendChild(a);
	return b
};
CPWA6.prototype.pc = function(a) {
	if (!a)
		return this.Bc();
	var b = CPWAI("div", {
				"class" : CPWAK() + "-loaded-img-div"
			});
	CPWAR(b, this.ya);
	var c = CPWAWd(this, a.Za, a.F), d = c.newImage, e = c.wrapped, f = c.imageVisibleSize, c = c.imageVisibleOffset, g = a.F
			.nd();
	null === g ? CPWAH(d, {
				alt : "",
				title : a.F.oc() + " - " + a.F.nc()
			}) : "" == g ? CPWAH(d, {
				alt : ""
			}) : CPWAH(d, {
				alt : "",
				title : g
			});
	CPWAq(a.F.md(), function(a) {
				var c = CPWAI("div", {
							"class" : CPWAK() + "-extra-html-div"
						});
				CPWAR(c, this.ya);
				a = CPWAYb(a);
				c.appendChild(a);
				b.appendChild(c)
			}, this);
	b.appendChild(e);
	this.Ua.push(CPWAC(d, "click", CPWAh(function(a, b, c) {
						if (!this.g()) {
							var d = CPWAee(this, a);
							CPWAs(this.Oa, "photo_clicked")
									&& c.preventDefault();
							c.stopPropagation();
							this.dispatchEvent(new CPWAT(this, a, d, b))
						}
					}, this, a.F, b)));
	CPWAOb(this, new CPWAU(this, a.F, b, f, c));
	return b
};
var CPWAfe = function(a, b, c) {
	return 0 > b || 0 > c || b >= a.ha || c >= a.ia ? null : CPWA5(a) ? c + b
			* a.ia : b + c * a.ha
}, CPWAge = function(a, b) {
	var c = a.k("div", "images")[0];
	CPWAJ(c);
	for (var d = 0; d < a.K; ++d)
		a.Db[d] = b[d];
	for (d = 0; d < a.ia; d++)
		for (var e = 0; e < a.ha; e++)
			c.appendChild(b[CPWAfe(a, e, d)])
}, CPWAee = function(a, b) {
	var c = b.ga(), d = CPWAxa(a.Qa, function(a) {
				return a.ga() == c
			});
	return -1 == d ? null : a.W() + d
}, CPWAje = function(a, b, c, d, e, f) {
	var g = CPWAh(function(a) {
				if (!this.g()) {
					this.S = c;
					for (var b = 0; b < this.K; ++b) {
						var d = a[b];
						this.Qa[b] = d ? d.F : null
					}
					f()
				}
			}, a, b), h = [], k = [];
	CPWAq(b, function(a) {
				k.push(this.pc(a));
				h.push(a ? a.F : null)
			}, a);
	"slide_previous" == d ? CPWAhe(a, k, h, 1, e, g) : "slide_next" == d
			? CPWAhe(a, k, h, -1, e, g)
			: (CPWAge(a, k), CPWAie(a, h), g())
}, CPWAie = function(a, b) {
	a.xa && CPWAq(b, function(a) {
				a && a.ga() && CPWA2d(a.ga(), this.wa)
			}, a)
}, CPWAhe = function(a, b, c, d, e, f) {
	var g = a.k("div", "images")[0];
	CPWAJ(g);
	var h = a.ia, k = a.ha;
	CPWA5(a) ? k += e : h += e;
	for (var n = CPWAI("div", {
				style : "position: relative; width: " + k * a.ya.width + "px"
			}), l = 0; l < h; ++l)
		for (var m = 0; m < k; ++m) {
			var p = m, r = l, q = m, s = l;
			1 == d ? CPWA5(a) ? p -= e : r -= e : CPWA5(a) ? q -= e : s -= e;
			p = CPWAfe(a, p, r);
			q = CPWAfe(a, q, s);
			null != q ? n.appendChild(b[q]) : n.appendChild(a.Db[p])
		}
	g.appendChild(n);
	g = new CPWAxc;
	m = l = k = h = 0;
	CPWA5(a)
			? (e *= a.ya.width, 1 == d ? h = -e : l = -e)
			: (e *= a.ya.height, 1 == d ? k = -e : m = -e);
	g.add(new CPWAOc(n, [h, k], [l, m], 250, CPWASc));
	g.play();
	CPWAC(g, "end", CPWAh(function(a, c) {
						this.g() || (CPWAge(this, b), CPWAie(this, a), c())
					}, a, c, f))
};
CPWA6.prototype.ob = function(a) {
	function b(a, b) {
		this.g()
				|| (CPWARd(this, a), CPWASd(this, b), this
						.dispatchEvent("photo_changed"))
	}
	CPWAQd(this);
	for (var c = [], d = a - 1; d < a + this.K + 1; ++d)
		c.push(d);
	var e = null, e = CPWAmc(CPWAh(function() {
						this.g() || (CPWAYd(this, "loading"), e = null)
					}, this), 500);
	this.C.V(c, CPWAh(function(c, d) {
		if (!this.g())
			if (e && (CPWAa.clearTimeout(e), e = null), 0 != d)
				CPWAYd(this, "error"), b.call(this, !1, !1);
			else {
				CPWAYd(this, null);
				for (var h = !c.D(a - 1), k = !c.D(a + this.K), n = [], l = a; l < a
						+ this.K; ++l)
					n.push(c.get(l, null));
				var l = a - this.S, m = "none", p = 0;
				null !== this.S
						&& 0 == l % this.$b
						&& (p = l / this.$b, Math.abs(p) <= this.Zc
								&& (0 > p ? m = "slide_previous" : 0 < p
										&& (m = "slide_next")));
				CPWAje(this, n, a, m, Math.abs(p), CPWAh(b, this, h, k))
			}
	}, this))
};
CPWA6.prototype.setPosition = CPWA6.prototype.ob;
CPWAi("panoramio.PhotoListWidget.prototype.dispose", CPWA6.prototype.e);
CPWAi("panoramio.PhotoListWidget.prototype.getAtStart", CPWA6.prototype.Sc);
CPWAi("panoramio.PhotoListWidget.prototype.getAtEnd", CPWA6.prototype.Rc);
CPWAi("panoramio.PhotoListWidget.prototype.getPositionById", CPWA6.prototype.ma);
CPWAi("panoramio.PhotoListWidget.prototype.enablePreviousArrow",
		CPWA6.prototype.Qc);
CPWAi("panoramio.PhotoListWidget.prototype.enableNextArrow", CPWA6.prototype.Pc);
CPWAi("panoramio.PhotoListWidget.prototype.getPhotoSize", CPWA6.prototype.Uc);
var CPWAke = function(a, b, c) {
	if (0 == a.length)
		return [];
	var d = [], e = CPWAua(a, function(a, b) {
				return Math.min(a, b)
			}, a[0]);
	a = CPWAua(a, function(a, b) {
				return Math.max(a, b)
			}, a[0]) + 1;
	c = a + c;
	for (b = e - b; b < e; ++b)
		d.push(b);
	for (b = a; b < c; ++b)
		d.push(b);
	return d
};
var CPWAMd = function(a) {
	this.ea = a;
	this.kb = this.ba = this.ca = 0
};
CPWAj(CPWAMd, CPWAx);
CPWA = CPWAMd.prototype;
CPWA.a = function() {
	delete this.ea;
	delete this.ca;
	delete this.ba;
	delete this.kb
};
CPWA.eb = function(a) {
	var b = CPWAu(a, "prefetchAfter", 0), c = CPWAu(a, "hysteresis", 0);
	this.ca = Math.max(this.ca, CPWAu(a, "prefetchBefore", 0));
	this.ba = Math.max(this.ba, b);
	this.kb = Math.max(this.kb, c)
};
CPWA.Dc = function() {
	return this.ea.Dc()
};
CPWA.get = function(a, b) {
	this.V([a], function(c, d) {
				var e = c.get(a, null);
				b(e, d)
			})
};
CPWA.V = function(a, b, c) {
	function d(a) {
		return 0 <= a && (null === e || a < e)
	}
	c = c || function() {
	};
	var e = this.ea.Dc(), f = CPWAsa(a, d);
	if (0 == f.length)
		b(new CPWAv, 0), c(new CPWAv, 0);
	else {
		var g = CPWAt(a);
		CPWABa(g, CPWAke(a, this.ca, this.ba));
		var h = CPWAsa(g, d), k = this.ea.Ca(h);
		a = !1;
		CPWAr(f, function(a) {
					return k.D(a)
				}) && (k.N(), b(k, 0), a = !0);
		CPWAr(h, function(a) {
					return k.D(a)
				}) ? c(new CPWAv, 0) : (f = CPWAt(g), CPWABa(f, CPWAke(g,
						this.kb, this.kb)), g = CPWAsa(f, d), g = CPWAsa(g,
				function(a) {
					return !k.D(a)
				}), a ? this.ea.V(g, c) : (this.ea.V(g, function(a, c) {
					k.Jb(a);
					b(k, c)
				}), c(new CPWAv, 0)))
	}
};
CPWA.sb = function(a) {
	return this.ea.sb(a)
};
CPWA.Ca = function(a) {
	return this.ea.Ca(a)
};
CPWA.ma = function(a, b) {
	this.ea.ma(a, this.ca, this.ba, b)
};
CPWA.wb = function(a) {
	return this.ea.wb(a)
};
var CPWA7 = function() {
};
CPWAi("panoramio.PhotoPrefetcherOptions", CPWA7);
CPWA7.prototype.width = 0;
CPWA7.prototype.width = CPWA7.prototype.width;
CPWA7.prototype.height = 0;
CPWA7.prototype.height = CPWA7.prototype.height;
CPWA7.prototype.H = !1;
CPWA7.prototype.croppedPhotos = CPWA7.prototype.H;
CPWA7.prototype.zb = void 0;
CPWA7.prototype.buffering = CPWA7.prototype.zb;
var CPWAle = function(a, b) {
	this.F = a;
	this.Za = b
};
CPWAj(CPWAle, CPWAx);
CPWAle.prototype.a = function() {
	this.Za.e();
	delete this.F;
	delete this.Za;
	CPWAle.c.a.call(this)
};
CPWAle.prototype.toString = function() {
	return CPWAZa("<%s %s>", this.F.toString(), this.Za.toString())
};
var CPWANd = function(a, b, c) {
	this.La = a;
	this.Lb = b;
	this.tc = c;
	this.ba = this.ca = 0
};
CPWAj(CPWANd, CPWAx);
CPWA = CPWANd.prototype;
CPWA.a = function() {
	delete this.La;
	delete this.Lb;
	delete this.tc;
	delete this.ca;
	delete this.ba
};
CPWA.eb = function(a) {
	var b = CPWAu(a, "images", {}), c = CPWAu(b, "prefetchAfter", 0);
	this.ca = Math.max(this.ca, CPWAu(b, "prefetchBefore", 0));
	this.ba = Math.max(this.ba, c);
	this.La.eb(CPWAu(a, "metadata", {}))
};
CPWA.get = function(a, b) {
	this.V([a], function(c, d) {
				var e = c.get(a, null);
				b(e, d)
			})
};
CPWA.sb = function(a) {
	return this.Ca([a]).get(a, null)
};
CPWA.V = function(a, b) {
	function c(b, c) {
		var d = c.vc(this.tc);
		if (d)
			if (CPWAs(a, b)) {
				var n = f.get(b, c, d);
				this.Lb.get(d.url, n)
			} else
				CPWAs(e, b) && this.Lb.get(d.url, function() {
						})
	}
	function d(a, d, e) {
		d.N();
		0 != e || this.g() ? b(new CPWAv, e) : (CPWAGa(d.ja(!0), function(a) {
					c.call(this, a, d.get(a))
				}, this), a && CPWAme(f), d.N())
	}
	0 == a.length && b(new CPWAv, 0);
	var e = CPWAke(a, this.ca, this.ba);
	CPWABa(e, a);
	var f = new CPWAne(b);
	this.La.V(a, CPWAh(d, this, !0), CPWAh(d, this, !1))
};
CPWA.Ca = function(a) {
	if (0 == a.length)
		return new CPWAv;
	var b = this.La.Ca(a), c = new CPWAv;
	CPWAGa(b.ja(!0), function(a) {
				var e = b.get(a), f = e.vc(this.tc);
				if (f) {
					var g = this.Lb.sb(f.url);
					g && c.set(a, new CPWAle(e, new CPWA0a(f, g)))
				}
			}, this);
	return c
};
CPWA.ma = function(a, b) {
	this.La.ma(a, b)
};
CPWA.wb = function(a) {
	return this.La.wb(a)
};
var CPWAne = function(a) {
	this.Vd = a;
	this.Kc = new CPWAv;
	this.xb = 0;
	this.wd = this.xd = !1
}, CPWAme = function(a, b) {
	a.xd = !0;
	CPWAoe(a, b || 0)
}, CPWAoe = function(a, b) {
	!a.xd || 0 < a.xb || a.wd || (a.wd = !0, a.Vd(a.Kc, b))
};
CPWAne.prototype.get = function(a, b, c) {
	this.xb++;
	return CPWAh(function(a, b, c, g, h) {
		0 != h
				? (this.xb = 0, CPWAme(this, h))
				: (this.Kc.set(a, new CPWAle(b, new CPWA0a(c, g))), this.xb--, this.Kc
						.$(), 0 == this.xb && CPWAoe(this, 0))
	}, this, a, b, c)
};
var CPWA8 = function(a, b) {
	this.Ya = b.width;
	this.Xa = b.height;
	this.Gd = b.croppedPhotos;
	this.va = CPWAu(b, "buffering", {
				metadata : {
					prefetchBefore : 0,
					prefetchAfter : 0,
					hysteresis : 0
				},
				images : {
					prefetchBefore : 0,
					prefetchAfter : 0
				}
			});
	this.pa = null;
	this.na(a)
};
CPWAj(CPWA8, CPWAx);
CPWAi("panoramio.PhotoPrefetcher", CPWA8);
CPWA8.prototype.a = function() {
	this.pa && this.pa.e();
	delete this.pa;
	CPWA8.c.a.call(this)
};
CPWA8.prototype.na = function(a) {
	var b = new CPWAjc(this.Ya, this.Xa, this.Gd);
	this.pa && this.pa.e();
	this.pa = CPWAOd(a, b);
	this.pa.eb(this.va)
};
CPWA8.prototype.setRequest = CPWA8.prototype.na;
CPWA8.prototype.se = function(a) {
	this.pa.V(a, function() {
			})
};
CPWA8.prototype.prefetchPhotos = CPWA8.prototype.se;
CPWAi("panoramio.PhotoPrefetcher.prototype.dispose", CPWA8.prototype.e);
var CPWA9 = function() {
};
CPWAi("panoramio.PhotoWidgetOptions", CPWA9);
CPWA9.prototype.width = void 0;
CPWA9.prototype.width = CPWA9.prototype.width;
CPWA9.prototype.height = void 0;
CPWA9.prototype.height = CPWA9.prototype.height;
CPWA9.prototype.H = void 0;
CPWA9.prototype.croppedPhotos = CPWA9.prototype.H;
CPWA9.prototype.Oc = void 0;
CPWA9.prototype.disableDefaultEvents = CPWA9.prototype.Oc;
CPWA9.prototype.Nc = void 0;
CPWA9.prototype.attributionStyle = CPWA9.prototype.Nc;
CPWA9.prototype.Wc = void 0;
CPWA9.prototype.openLinksInNewWindow = CPWA9.prototype.Wc;
CPWA9.prototype.Y = void 0;
CPWA9.prototype.photoUrlGetter = CPWA9.prototype.Y;
CPWA9.prototype.zb = void 0;
CPWA9.prototype.buffering = CPWA9.prototype.zb;
var CPWA$ = function(a, b, c) {
	CPWA4.call(this, a);
	a = c || {};
	this.Eb = this.ac = this.S = this.za = null;
	this.H = CPWAs([!0, "to_square", "to_fill"], a.croppedPhotos);
	this.Ra = CPWAs([!0, "to_square"], a.croppedPhotos);
	this.xa = this.Cb = !0;
	this.Ia = a.openLinksInNewWindow || !1;
	this.Gb = a.attributionStyle || "default";
	c = CPWAwa.Pa;
	"hidden_no_name" != this.Gb || c || (this.Gb = "hidden");
	c = a.width || 400;
	var d = a.height || 300;
	CPWAhd(this, this.element, this.Gb, c);
	this.Ta();
	CPWAid(this);
	d -= CPWAjd(this);
	this.h = new CPWAF(c, d);
	this.b = new CPWAP(0, 0, 0, 0);
	this.B = new CPWAP(0, 0, 0, 0);
	this.w = new CPWAP(0, 0, 0, 0);
	if (this.Ra)
		if (c > d) {
			var e = c - d;
			c = d;
			this.b.left += Math.round(e / 2);
			e -= Math.round(e / 2);
			this.b.right += e
		} else
			e = d - c, d = c, this.b.top += Math.round(e / 2), e -= Math
					.round(e / 2), this.b.bottom += e;
	this.t = new CPWAF(c, d);
	this.t.floor();
	this.wa = new CPWAjc(c, d, this.H);
	this.va = CPWAu(a, "buffering", {
				metadata : {
					prefetchBefore : 5,
					prefetchAfter : 10,
					hysteresis : 5
				},
				images : {
					prefetchBefore : 1,
					prefetchAfter : 1
				}
			});
	this.cc();
	c = this.k("a", "prev")[0];
	var d = this.k("span", "arrowbox-prev")[0], e = this.k("a", "next")[0], f = this
			.k("span", "arrowbox-next")[0], g = this.k("p", "title")[0];
	CPWAPa(this.m, {
				prev : new CPWApe(c, "photo prev"),
				prevBox : new CPWAZd(d, "photo prevbox"),
				next : new CPWApe(e, "photo next"),
				nextBox : new CPWAZd(f, "photo nextbox"),
				title : new CPWAqe(g, "photo title")
			});
	"hidden_no_name" == this.Gb && this.m.title.set("enabled", !1);
	this.Oa = this.bc(a.disableDefaultEvents);
	this.Y = a.photoUrlGetter || this.Y;
	b && this.na(b)
};
CPWAj(CPWA$, CPWA4);
CPWAi("panoramio.PhotoWidget", CPWA$);
CPWA$.prototype.a = function() {
	this.za && CPWAJ(this.za);
	delete this.za;
	delete this.S;
	delete this.ac;
	delete this.Eb;
	delete this.H;
	delete this.Ra;
	delete this.Cb;
	delete this.xa;
	delete this.Ia;
	delete this.h;
	delete this.wa;
	delete this.va;
	delete this.Oa;
	CPWA$.c.a.call(this)
};
var CPWApe = function(a, b) {
	CPWA3.call(this, a, {
				userEnabled : !0,
				mouseOver : !1,
				notAtListEdge : !1,
				start : !1
			}, [["userEnabled", "mouseOver", "notAtListEdge"],
					["userEnabled", "start"]], ["mouseOver", "start"], b)
};
CPWAj(CPWApe, CPWA3);
var CPWAqe = function(a, b) {
	CPWA3
			.call(this, a, {
						enabled : !0,
						hasData : !1,
						mouseOver : !1,
						start : !1
					}, [["enabled", "hasData", "mouseOver"],
							["enabled", "hasData", "start"]], ["mouseOver",
							"start"], b)
};
CPWAj(CPWAqe, CPWA3);
CPWA$.prototype.na = function(a) {
	this.C && this.C.e();
	this.T && this.T.e();
	this.Va = this.Wa = this.T = this.C = null;
	this.m.title.set("hasData", !1);
	if (a) {
		var b = CPWAUd(a);
		a = b.request;
		b = b["new"];
		this.C = CPWAOd(a, this.wa);
		this.C.eb(this.va);
		this.T = b ? a : null;
		this.xa = !("userAvatars" in a.Hb);
		this.Cb
				&& (this.m.prev.set("start", !0), this.m.next.set("start", !0), this.m.title
						.set("start", !0))
	}
};
CPWA$.prototype.setRequest = CPWA$.prototype.na;
CPWA$.prototype.Vc = function() {
	return this.h
};
CPWA$.prototype.getSize = CPWA$.prototype.Vc;
CPWA$.prototype.bc = function(a) {
	CPWAC(this.za, ["mouseover", "mouseout"], CPWAh(function(a) {
						if (!this.g()) {
							var b = new CPWAsb(a);
							b.relatedTarget
									&& CPWA_b(this.za, b.relatedTarget)
									|| (a = "mouseover" == a.type, this.m.prev
											.set("mouseOver", a), this.m.next
											.set("mouseOver", a), this.m.title
											.set("mouseOver", a))
						}
					}, this));
	var b = [], b = !0 === a ? ["previous_clicked", "next_clicked",
			"photo_clicked"] : "array" == CPWAba(a) ? a : [];
	CPWATd(this, b, 1);
	return b
};
CPWA$.prototype.cc = function() {
	CPWAR(this.za, this.h);
	var a = this.k("div", "images")[0];
	CPWAR(a, this.h);
	a = this.k("div", "overlay")[0];
	CPWAR(a, this.h);
	var b = Math.round((this.h.height - 38) / 2) + "px";
	CPWAq(this.k("a", "arrow"), function(a) {
				CPWAQ(a.getElementsByTagName("img")[0], "margin-top", b)
			})
};
CPWA$.prototype.W = function() {
	return this.S
};
CPWA$.prototype.getPosition = CPWA$.prototype.W;
CPWA$.prototype.Bb = function() {
	return this.ac
};
CPWA$.prototype.getPhoto = CPWA$.prototype.Bb;
CPWA$.prototype.Ta = function() {
	var a = CPWAYb(CPWAfd["photo_widget.html"]);
	CPWAJ(this.element);
	this.element.appendChild(a);
	this.za = this.element.childNodes[0]
};
CPWA$.prototype.Bc = function() {
	var a = CPWAI("img", {
				"class" : CPWAK() + "-empty-img",
				src : CPWAec() + "/img/transparent.gif",
				alt : ""
			});
	CPWAR(a, this.h);
	return a
};
CPWA$.prototype.pc = function(a) {
	if (!a)
		return this.Bc();
	var b = CPWAWd(this, a.Za, a.F), c = b.newImage, d = b.wrapped, b = b.imageVisibleSize, e = new CPWAE(
			0, 0);
	this.Ua.push(CPWAC(c, "click", CPWAh(function(a, b, c) {
						if (!this.g()) {
							var d = this.W();
							CPWAs(this.Oa, "photo_clicked")
									&& c.preventDefault();
							c.stopPropagation();
							this.dispatchEvent(new CPWAT(this, a, d, b))
						}
					}, this, a.F, c)));
	CPWAOb(this, new CPWAU(this, a.F, d, b, e));
	return d
};
var CPWAre = function(a, b) {
	var c = a.k("div", "images")[0];
	CPWAJ(c);
	c.appendChild(b);
	a.Eb = b
}, CPWAue = function(a, b, c, d, e) {
	var f = a.pc(b), g = b ? b.F : null;
	b = CPWAh(function(a) {
				if (!this.g()) {
					this.S = c;
					if (this.ac = a = a ? a.F : null) {
						var b = a.oc(), d = a.nc();
						60 < b.length && (b = b.substr(0, 57) + "...");
						60 < d.length && (d = d.substr(0, 57) + "...");
						var f = this.k("a", "photo-title")[0];
						CPWAH(f, {
									href : a.rc()
								});
						this.Ia && CPWAH(f, {
									target : "_blank"
								});
						CPWA1b(f, b);
						b = this.k("a", "photo-author")[0];
						CPWAH(b, {
									href : a.gd()
								});
						this.Ia && CPWAH(b, {
									target : "_blank"
								});
						CPWA1b(b, d)
					}
					this.m.title.set("hasData", null !== a);
					e()
				}
			}, a, b);
	"slide_previous" == d ? CPWAse(a, f, g, 1, b) : "slide_next" == d ? CPWAse(
			a, f, g, -1, b) : (CPWAre(a, f), CPWAte(a, g), b())
}, CPWAte = function(a, b) {
	a.xa && b && b.ga() && CPWA2d(b.ga(), a.wa)
}, CPWAse = function(a, b, c, d, e) {
	var f = a.k("div", "images")[0];
	CPWAJ(f);
	var g = CPWAI("div", {
				style : "position: relative; width: " + 2 * a.h.width + "px"
			}), h = CPWAI("span", {
				style : "position: absolute; left: 0; top: 0"
			});
	CPWAR(h, a.h);
	var k = CPWAI("span", {
				style : "position: absolute; left: " + a.h.width + "px; top: 0"
			});
	CPWAR(k, a.h);
	1 == d ? (h.appendChild(b), k.appendChild(a.Eb)) : (h.appendChild(a.Eb), k
			.appendChild(b));
	g.appendChild(h);
	g.appendChild(k);
	f.appendChild(g);
	f = new CPWAxc;
	f.add(new CPWAOc(g, [(-1 - d) * a.h.width / 2, 0], [
					(-1 + d) * a.h.width / 2, 0], 250, CPWASc));
	f.play();
	CPWAC(f, "end", CPWAh(function(a, c) {
						this.g() || (CPWAre(this, b), CPWAte(this, a), c())
					}, a, c, e))
};
CPWA$.prototype.ob = function(a) {
	function b(a, b, c) {
		this.g()
				|| (CPWARd(this, a), CPWASd(this, b), this.Cb && c
						&& (this.Cb = !1, CPWAmc(function() {
							this.g()
									|| (this.m.prev.set("start", !1), this.m.next
											.set("start", !1), this.m.title
											.set("start", !1))
						}, 300, this)), this.dispatchEvent("photo_changed"))
	}
	CPWAQd(this);
	var c = null, c = CPWAmc(CPWAh(function() {
						this.g() || (CPWAYd(this, "loading"), c = null)
					}, this), 500);
	this.C.V([a - 1, a, a + 1], CPWAh(function(d, e) {
		if (!this.g())
			if (c && (CPWAa.clearTimeout(c), c = null), 0 != e)
				CPWAYd(this, "error"), b.call(this, !1, !1, null);
			else {
				CPWAYd(this, null);
				var f = !d.D(a - 1), g = !d.D(a + 1), h = d.get(a, null), k = "none";
				if (null !== this.S && !CPWAz) {
					var n = a - this.S;
					1 == n ? k = "slide_next" : -1 == n
							&& (k = "slide_previous")
				}
				CPWAue(this, h, a, k, CPWAh(b, this, f, g, h))
			}
	}, this))
};
CPWA$.prototype.setPosition = CPWA$.prototype.ob;
CPWAi("panoramio.PhotoWidget.prototype.dispose", CPWA$.prototype.e);
CPWAi("panoramio.PhotoWidget.prototype.getAtStart", CPWA$.prototype.Sc);
CPWAi("panoramio.PhotoWidget.prototype.getAtEnd", CPWA$.prototype.Rc);
CPWAi("panoramio.PhotoWidget.prototype.getPositionById", CPWA$.prototype.ma);
CPWAi("panoramio.PhotoWidget.prototype.enablePreviousArrow", CPWA$.prototype.Qc);
CPWAi("panoramio.PhotoWidget.prototype.enableNextArrow", CPWA$.prototype.Pc);
CPWAi("panoramio.PhotoWidget.prototype.getPhotoSize", CPWA$.prototype.Uc);/*
 * 
 * 
 * Copyright
 * 2011
 * Google
 * Inc.
 * All
 * Rights
 * Reserved. --
 * google
 */
var CPWAve = CPWAfd["wapi.css"], CPWAwe = document, CPWAxe = CPWAwe
		.createElement("style");
CPWAxe.type = "text/css";
CPWAwe.getElementsByTagName("head")[0].appendChild(CPWAxe);
CPWAxe.styleSheet ? CPWAxe.styleSheet.cssText = CPWAve : CPWAxe
		.appendChild(CPWAwe.createTextNode(CPWAve));
if ("True" == CPWAdc.enable_gen204) {
	var CPWAye = new CPWAV(window.location.href), CPWAze;
	CPWAze = CPWA4d(CPWAye.ta) ? "/wapi/template/" == CPWAye.ib.substr(0, 15)
			? "iframe"
			: "pjs" : "js";
	var CPWAAe = CPWAye.ta;
	if ("iframe" == CPWAze) {
		var CPWABe = CPWAdc.frame_referrer, CPWAAe = "" == CPWABe
				? ""
				: (new CPWAV(CPWABe)).ta;
		CPWA4d(CPWAAe) && (CPWAze = "piframe")
	}
	CPWA3d = CPWAze;
	CPWAVd = CPWAAe;
	CPWA6d("activation", {
				usage : CPWAze,
				page : CPWAAe
			})
};