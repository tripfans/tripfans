{
    //"appDir": "../www",
    "baseUrl": "../",
    //"dir": "../www-build",
    //Comment out the optimize line if you want
    //the code minified by UglifyJS
    //"optimize": "none",

    // point to the shim config we set up before
    //"mainConfigFile": "../www/js/app.js",

    /*"modules": [
        //Optimize the application files. jQuery and the plugins will be included
        //automatically, as they are dependencies of app.js.
        {
            "name": "app"
        }
    ]*/
    
    paths: {
        app: 'app',
        home: 'app/home'
    },
    //dir: '../scripts-built',
    skipDirOptimize: true,
    optimizeCss: 'none',
    preserveLicenseComments: false,
    /*modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: 'app/home',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: ['jquery-1.7.1']
        },

        {
            //module names are relative to baseUrl
            name: 'app/home2',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: ['jquery-ui-1.8.16.custom.min']
        },

    ]*/
}