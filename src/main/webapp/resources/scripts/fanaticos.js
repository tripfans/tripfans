/**
 * Essa função habilita os fields recebidos como arguments
 */
function enableFields() {
	 jQuery.each(arguments, function(index, arg){
		 enableDisableField(arg);
	 });
}

/**
 * Essa função desabilita os fields recebidos como arguments
 */
function disableFields() {
	jQuery.each(arguments, function(index, arg){
		$(arg).attr('disabled', 'disabled');
	 });
}

/**
 * Esta função habilita ou desabilita o elemento, conforme o estado atual do mesmo.
 * @param id
 */
function enableDisableField(id) {
	var disabled = $('#' + id).attr('disabled');
	if(disabled) {
		$('#' + id).prop('disabled', false);
	} else {
		$('#' + id).prop('disabled', true);
	}
}

/**
 * Abre uma janela popup centralizada na tela.
 * 
 * @param url a url que a janela irá carregar
 * @param idWindow a id da janela; pode ser nulo
 * @param width a largura da janela
 * @param height a altura da janela
 * @returns
 */
function openPopupWindow(url, idWindow, width, height) {
	if(width == null) width = 800;
	if(height == null) height = 600;
	var left = (screen.width/2) - width/2;
	var top = (screen.height/2) - height/2;
	var formWindow = window.open(url,idWindow,'titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width='+width+',height='+height+',top='+top+',left='+left);
	return formWindow;
}


/**
 * Essa função configura os tooltips de um elemento pai.
 * O elemento tem que conter o atributo title definido para ter o tooltip configurado.
 */
(function($)
{
    var element = {};
    
    jQuery.fn.initToolTips = function()
    {
        return this.each(function()
        {
            element = $(this);
            setupToolTips();
        });
        
    };
    
    function setupToolTips()
    {
    	var elements = jQuery(element).find('*[title]');
        jQuery.each(elements, function(index, element){
        	if($(element).attr("title") !== undefined && $(element).attr("title") !== '' && $(element).data('tooltip') !== false) {
        		$(element).tooltip();
        	}
        });
    };
})(jQuery);


function loadGMap(idElemento, latitude, longitude, icon, mapType, zoom) {
	var latlng = new google.maps.LatLng(latitude, longitude);
	
	if (!mapType) {
		mapType = google.maps.MapTypeId.ROADMAP;
	}
	if (!zoom) {
		zoom = 14;
	}
	options = {
	  zoom: zoom,
	  center: latlng,
	  mapTypeId: mapType,
      scrollwheel : false
	};
	var map = new google.maps.Map(document.getElementById(idElemento), options);
	var marker = new google.maps.Marker({
	      map: map,
	      position: map.getCenter(),
	      icon: icon,
	      clickable: true
	});
	/*var infoWindow = new google.maps.InfoWindow();
	google.maps.event.addListener(marker, "mouseover", function() {
		infoWindow.setContent('Some info on yourOverlay');
		infoWindow.setOptions({disableAutoPan: true, zIndex: 3000});
		infoWindow.open(map, marker);
	});*/
}

/**
 * Função a ser chamada para abrir a janela de login.
 * 
 */
function openLoginWindow(idElem) {
	$('#'+idElem).fancybox({
        'scrolling'     : 'no',
        'titleShow'     : false,
        'width'         : 400,
        'height'        : 500,
        'hideOnOverlayClick' : false,
        'onClosed'      : function() {
            $("#login_error").hide();
        },
        'onComplete'	: function() {
        	$("#fancybox-wrap").css({'margin': '-70px 0 0 0px', 'z-index': '110000'});
        }
    }); 
}

/**
 * Atualiza uma notificação para que ela não fique mais pendente.
 * @param url
 */
function atualizarNotificacao(url) {
	$.post(url,{},function(response){});	
}

/**
 * Essa função trata o placeholder em browsers que não possuem o suporte do mesmo.
 */
(function($) {
  $.fn.placeholder = function() {
    if(typeof document.createElement("input").placeholder == 'undefined') {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
        })
      });
    }
  }
})(jQuery);

jQuery('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});

function isDate(txtDate)
{
  var currVal = txtDate;
  if(currVal == '')
    return false;
  
  //Declare Regex  
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
 
  //Checks for mm/dd/yyyy format.
  dtDay= dtArray[1];
  dtMonth = dtArray[3];
  dtYear = dtArray[5];

  if (dtMonth < 1 || dtMonth > 12)
      return false;
  else if (dtDay < 1 || dtDay> 31)
      return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }
  return true;
}

function enviarConvitesEmails(enderecosEmail, urlEnvio, callback) {
    jQuery.ajax({
        type: 'POST',
        url: urlEnvio,
        data: {
            enderecosEmail : enderecosEmail
        },
        success: function(data) {
        	if (data.success) {
        	    callback();
            }
        }
    });
}

function mostraDadosAvaliacao(link) {
	var target = $('#'+$(link).data('target'));
	$('#escondeAvaliacao-' + $(link).data('target')).show();
	$(link).hide();
	target.show();
}

function escondeDadosAvaliacao(link) {
	var target = $('#'+$(link).data('target'));
	var linkMostra = $('#mostraAvaliacao-'+$(link).data('target')); 
	target.hide();
	$('#escondeAvaliacao-' + $(link).data('target')).hide();
	linkMostra.show();
}

/**
 * Mostra as fotos de uma avaliação
 * 
 * @param link
 */
function verFotosAvaliacao(link) {
	var url = $(link).data('href');
	var items = [];
	$.getJSON(url, {
		jsonFields: $.toJSON([
		                      {name: 'urlOriginal', type: 'string'},
		                      {name: 'descricao', type: 'string'}
		                      ])
	}, function(response){
		$.each(response.records, function(key, item) {
			var urlImg = item.urlOriginal;
			var title = item.descricao !== null ? item.descricao : ''; 
			items.push({'href' : urlImg, 'title' : title});
		});
		$.fancybox(items, {
			transitionIn      : 'elastic',
			transitionOut     : 'elastic',
			type              : 'image',
			speedIn: 400, 
			speedOut: 200, 
			overlayShow: true,
			overlayOpacity: 0.1,
			hideOnContentClick: true,
			titlePosition: 'inside'
		});
	});
}

function mostraRespostas(url, idRender) {
	$.get(url, {}, function(response){
		$(idRender).html(response);
	});
}

function resetForm(id) {
	$('#'+id).each(function(){
		this.reset();
	});
}

function mostraMapaLocal(link, idDivMapa, latitude, longitude, urlIcone, mapType, zoom) {
    var divMapa = $('#'+idDivMapa); 
	if(!divMapa.is(':visible')) {
		divMapa.show();
		google.maps.event.addDomListener(window, 'load', loadGMap(idDivMapa, latitude, longitude, urlIcone, mapType, zoom));
		if (link) {
			$(link).text('Ocultar Mapa');
		}
	} else {
		divMapa.hide();
		if (link) {
			$(link).text('Ver Mapa');
		}
	}
}