requirejs.config({
    'baseUrl': './',
    paths: {
        requireLib: '../scripts/require',
        jquery: '../scripts/jquery-1.7.1',
        jqueryui: '../scripts/jquery-ui-1.8.16.custom.min',
        bootstrap3: '../components/bootstrap/bootstrap3/js/bootstrap',
        autocomplete: '../scripts/jquery.ui.fanaticos.autocomplete',
        common: 'app/common',
        home: 'app/home'
    },
    /*paths: {
        requireLib: '../scripts/require',
    },*/
    //include: 'requireLib',
    skipDirOptimize: true,
    optimizeCss: 'standard.keepLines.keepWhitespace',
    optimize: 'uglify',
    preserveLicenseComments: false
});