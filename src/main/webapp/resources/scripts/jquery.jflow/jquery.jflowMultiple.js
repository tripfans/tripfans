 /**
  * @author Alexandre Magno (http://blog.alexandremagno.net)
  * @version 1.0
  * @description Plugin de jQuery para Carousel que suporta orientacao vertical e horizontal
  * @requires ScrollTo
  * @param {String} [item=".item"] O seletor usado como referencia para o carousel
  * @param {Number} [itens=5] A quantidade de itens que ira deslizar no carousel
  * @param {String} [inativeClass="inativo"] Classe inserida na paginacao quando chegar nos limites de paginacao
  * @param {String} [prev=".widget-resumo-viagem-botao-topo a"] Seletor usado como botao de voltar
  * @param {String} [next=".widget-resumo-viagem-botao-rodape a"] Seletor usado como botao de avancar
  * @param {String} [mode="vertical"] Modo de orientacao dos slides, aceita os parametros horizontal e vertical
  * @param {Number} [speed=800] Velocidade em milisegundos do slide
  * @example
  * <div id="container">
  *     <ul>
  *         <li>item 1</li>
  *         <li>item 2</li>
  *         <li>item 3</li>
  *         <li>item 4</li>
  *     </ul>
  * </div>
  * $('#container').jflow({
  *     itens: 3,
  *     mode: 'horizontal'
  * });
  * @returns {Object} jQuery
  *
  *
 */

(function($) {
    $.fn.jflowMultiple = function(params){

      var options = {
          item: '.item',
          itens: 5,
          inativeClass : 'inativo',
          prev: '.widget-resumo-viagem-botao-topo a',
          next: '.widget-resumo-viagem-botao-rodape a',
          mode: 'vertical',
          speed: 800,
          heightDefault: 92
      };

      var op = $.extend(options, params);
      var $self = this;
      var count = 0;
  		var $flowbox = $(op.item);
      var li_count = $(op.item).length;
      
      if( li_count <= op.itens ) {
          $(op.next).addClass(op.inativeClass);
      }
      
      var li_amount = 0;
      
      if( li_count < op.itens ) {

           li_amount = li_count;
      
      } else {
      
          li_amount = op.itens;
      
      }
	    //console.log(op.item); 
      
      var max_itens = li_count - op.itens;
      var flowbox_dimensions = op.mode == 'vertical' ? op.heightDefault : $flowbox.outerWidth(true);
		
  		var li_size = flowbox_dimensions;
      var overall_size = li_count * flowbox_dimensions;
      var overflow_size = li_size * li_amount;
	   
    //  console.log(op.item, li_count, li_size, li_amount, max_itens);


  		if(op.mode === 'vertical') {
        
        $self.css({
                  'height': overflow_size,
  				        'overflow' : 'hidden'});
      } else {
      
        $self.css({
           'width': overflow_size,
           'overflow':'hidden'
        });
      
        $self.children(':first').css({
           'width':overall_size
        });
      }

     // console.log(op, ":", overall_size, overflow_size, li_amount);
     //console.log(op.prev);

     $(op.prev).addClass(op.inativeClass).unbind().bind('click.jflowMultiple',function(){
        
        if(count>0) {
        
            count-=op.itens;
        
            $self.scrollTo( $(op.item).eq(count), op.speed, {onAfter: function (){
                if(count===0) {
                    $(op.prev).addClass(op.inativeClass);
                }
            }});

            $(op.next).removeClass(op.inativeClass);
        }

        //IE 7 Bug
        $(this).blur();
        return false;
      });

      $(op.next).unbind().bind('click.jflowMultiple',function(){
            
            if(count < max_itens) {
                
                count += op.itens;

                $self.scrollTo( $(op.item).eq(count), op.speed, {onAfter: function (){
                    
                    if( count >= max_itens){
                        
                      $(op.next).addClass(op.inativeClass);
  
                    }

                }});
                
               // console.log(op.prev, $(op.prev).length);

                $(op.prev).removeClass(op.inativeClass);
            }

            //IE7 Bug
            $(this).blur();
            return false;
      });
    };
})(jQuery);