{
    baseUrl: './',
    // point to the shim config we set up before
    'mainConfigFile': '../scripts/main.js',
    skipDirOptimize: true,
    optimizeCss: 'standard.keepLines.keepWhitespace',
    optimize: 'uglify',
    preserveLicenseComments: false
    /*paths: {
        requireLib: '../scripts/require',
    },
    include: 'requireLib'*/
}