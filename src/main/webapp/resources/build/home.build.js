{
    'baseUrl': '../scripts',
    // point to the shim config we set up before
    //'mainConfigFile': '../www/js/app.js',
    paths: {
        app: 'app',
        home: 'app/home.config'
    },
    optimize: 'uglify',
    skipDirOptimize: true,
    out: '../scripts/app/home.js',
    optimizeCss: 'standard.keepLines.keepWhitespace',
    preserveLicenseComments: false
}