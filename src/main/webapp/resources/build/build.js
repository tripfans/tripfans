{
    appDir: '../scripts',
    baseUrl: 'lib',
    paths: {
        app: '../app'
    },
    dir: '../scripts-built',
    modules: [
        //First set up the common build layer.
        {
            //module names are relative to baseUrl
            name: '../app/home',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: ['jquery-1.7.1']
        },

        {
            //module names are relative to baseUrl
            name: '../app/home2',
            //List common dependencies here. Only need to list
            //top level dependencies, "include" will find
            //nested dependencies.
            include: ['jquery-ui-1.8.16.custom.min']
        },

    ]
}
