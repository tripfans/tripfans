/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean;

import com.geeoz.ean.dsl.GetHotelListBuilder;
import com.geeoz.ean.dsl.GetLocationInfoBuilder;
import com.geeoz.ean.dsl.GetPingBuilder;

/**
 * Use the EAN API to build and grow a profitable travel business selling hotel
 * reservations.
 *
 * @author Alex Voloshyn
 * @author Eugene Shevchenko
 * @version 1.1 7/13/2013
 */
public final class Expedia {
    /**
     * Private, no-op constructor to prevent instantiation.
     */
    private Expedia() {
        super();
    }

    /**
     * Send a ping request to our API servers to determine if service is
     * available in the event of a suspected outage or ISP issue, or to obtain
     * EAN's Unix server time when troubleshooting issues with signature
     * authentication.
     * <p/>
     * Every search for available hotels requires a minimum of a date range,
     * room count and adult guest count, and a location or hotelId list.
     * <p/>
     * To obtain a "dateless list," or a list of all active properties in
     * a location without specific availability information, simply omit the
     * arrivalDate, departureDate, and RoomGroup parameters.
     *
     * @return a ping request builder
     */
    public static GetHotelListBuilder getHotelList() {
        return new GetHotelListBuilder();
    }

    /**
     * Send a ping request to our API servers to determine if service is
     * available in the event of a suspected outage or ISP issue, or to obtain
     * EAN's Unix server time when troubleshooting issues with signature
     * authentication.
     *
     * @return a ping request builder
     */
    public static GetPingBuilder getPing() {
        return new GetPingBuilder();
    }

    /**
     * Send a location info request to our API servers obtain location data
     * such as a specific destinationId, latitude/longitude coordinates,
     * and the number of active properties available within the location.
     *
     * @return a location info request builder
     */
    public static GetLocationInfoBuilder getLocationInfo() {
        return new GetLocationInfoBuilder();
    }
}
