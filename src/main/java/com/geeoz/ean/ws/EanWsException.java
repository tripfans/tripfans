/*
 * Copyright 2013 Geeoz Software
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.geeoz.ean.ws;

import com.ean.wsapi.hotel.v3.EanWsError;

/**
 * EAN service exception wrapper.
 *
 * @author Alex Voloshyn
 * @version 1.0 7/13/2013
 */
public final class EanWsException extends Exception {
    /**
     * Use serialVersionUID from JDK 1.0.2 for interoperability.
     */
    private static final long serialVersionUID = 3232892583744502194L;
    /**
     * If your request generates an error, information will be contained within
     * this object. See our exceptions overview for details on this object.
     */
    private final transient EanWsError error;

    /**
     * Default construction.
     *
     * @param value an EAN web service error instance
     */
    public EanWsException(final EanWsError value) {
        super(value.getVerboseMessage());
        error = value;
    }

    /**
     * If your request generates an error, information will be contained within
     * this object. See our exceptions overview for details on this object.
     *
     * @return an EAN web service error instance
     */
    public EanWsError getError() {
        return error;
    }
}
