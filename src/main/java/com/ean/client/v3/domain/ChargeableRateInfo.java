package com.ean.client.v3.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Contains the details on the chargeable rate.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChargeableRateInfo implements Serializable {

    private static final long serialVersionUID = -3557042242820190093L;

    @JsonProperty("@averageBaseRate")
    private final BigDecimal averageBaseRate = BigDecimal.ZERO;

    @JsonProperty("@averageRate")
    private final BigDecimal averageRate = BigDecimal.ZERO;

    @JsonProperty("@commissionableUsdTotal")
    private final BigDecimal commissionableUsdTotal = BigDecimal.ZERO;

    @JsonProperty("@currencyCode")
    private String currencyCode;

    @JsonProperty("@maxNightlyRate")
    private final BigDecimal maxNightlyRate = BigDecimal.ZERO;

    @JsonProperty("NightlyRatesPerRoom")
    private NightlyRatesPerRoom nightlyRatesPerRoom;

    @JsonProperty("@nightlyRateTotal")
    private final BigDecimal nightlyRateTotal = BigDecimal.ZERO;

    @JsonProperty("Surcharges")
    private Surcharges surcharges;

    @JsonProperty("@surchargeTotal")
    private final BigDecimal surchargeTotal = BigDecimal.ZERO;

    @JsonProperty("@total")
    private final BigDecimal total = BigDecimal.ZERO;

    /**
     * Returns the averageBaseRate for the ChargeableRateInfo
     *
     * @return BigDecimal for the averageBaseRate
     */
    public BigDecimal getAverageBaseRate() {
        return this.averageBaseRate;
    }

    /**
     * Returns the averageRate for the ChargeableRateInfo
     *
     * @return BigDecimal for the averageRate
     */
    public BigDecimal getAverageRate() {
        return this.averageRate;
    }

    /**
     * Returns the commissionableUsdTotal for the ChargeableRateInfo
     *
     * @return BigDecimal for the commissionableUsdTotal
     */
    public BigDecimal getCommissionableUsdTotal() {
        return this.commissionableUsdTotal;
    }

    /**
     * Returns the currencyCode for the ChargeableRateInfo
     *
     * @return String for the currencyCode
     */
    public String getCurrencyCode() {
        return this.currencyCode;
    }

    /**
     * Returns the maxNightlyRate for the ChargeableRateInfo
     *
     * @return BigDecimal for the maxNightlyRate
     */
    public BigDecimal getMaxNightlyRate() {
        return this.maxNightlyRate;
    }

    /**
     * Returns the nightlyRatesPerRoom for the ChargeableRateInfo
     *
     * @return NightlyRatesPerRoom for the nightlyRatesPerRoom
     */
    public NightlyRatesPerRoom getNightlyRatesPerRoom() {
        return this.nightlyRatesPerRoom;
    }

    /**
     * Returns the nightlyRateTotal for the ChargeableRateInfo
     *
     * @return BigDecimal for the nightlyRateTotal
     */
    public BigDecimal getNightlyRateTotal() {
        return this.nightlyRateTotal;
    }

    /**
     * Returns the surcharges for the ChargeableRateInfo
     *
     * @return Surcharges for the surcharges
     */
    public Surcharges getSurcharges() {
        return this.surcharges;
    }

    /**
     * Returns the surchargeTotal for the ChargeableRateInfo
     *
     * @return BigDecimal for the surchargeTotal
     */
    public BigDecimal getSurchargeTotal() {
        return this.surchargeTotal;
    }

    /**
     * Returns the total for the ChargeableRateInfo
     *
     * @return BigDecimal for the total
     */
    public BigDecimal getTotal() {
        return this.total;
    }

}