import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.codec.EncoderException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;

public class UrlShortenerTest {
    /*
     * private static final String CALLBACK_URL = "http://localhost/oauth2callback";
     * 
     * // FILL THESE IN WITH YOUR VALUES FROM THE API CONSOLE private static final String CLIENT_ID = "202244445170.apps.googleusercontent.com";
     * 
     * private static final String CLIENT_SECRET = "EVbLWKLTl5j2p0CyGp_7JZlG";
     * 
     * private static final JsonFactory JSON_FACTORY = new JacksonFactory();
     * 
     * // private static final String SCOPE = "https://www.googleapis.com/auth/urlshortener"; private static final String SCOPE =
     * "https://www.google.com/m8/feeds/contacts/default/full";
     * 
     * private static final HttpTransport TRANSPORT = new NetHttpTransport();
     * 
     * public static void main(final String[] args) throws IOException { // Generate the URL to which we will direct users final String authorizeUrl =
     * new GoogleAuthorizationRequestUrl(CLIENT_ID, CALLBACK_URL, SCOPE).build(); System.out.println("Paste this url in your browser: " +
     * authorizeUrl);
     * 
     * // Wait for the authorization code System.out.println("Type the code you received here: "); final BufferedReader in = new BufferedReader(new
     * InputStreamReader(System.in)); final String authorizationCode = in.readLine();
     * 
     * // Exchange for an access and refresh token final GoogleAuthorizationCodeGrant authRequest = new GoogleAuthorizationCodeGrant(TRANSPORT,
     * JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, authorizationCode, CALLBACK_URL); authRequest.useBasicAuthorization = false; final AccessTokenResponse
     * authResponse = authRequest.execute(); final String accessToken = authResponse.accessToken; final GoogleAccessProtectedResource access = new
     * GoogleAccessProtectedResource(accessToken, TRANSPORT, JSON_FACTORY, CLIENT_ID, CLIENT_SECRET, authResponse.refreshToken); final
     * HttpRequestFactory rf = TRANSPORT.createRequestFactory(access); System.out.println("Access token: " + authResponse.accessToken);
     * 
     * // Make an authenticated request // final GenericUrl shortenEndpoint = new GenericUrl("https://www.googleapis.com/urlshortener/v1/url"); final
     * GenericUrl shortenEndpoint = new GenericUrl("https://www.google.com/m8/feeds/contacts/default/full?access_token=" + accessToken); // final
     * String requestBody = "{\"longUrl\":\"http://farm6.static.flickr.com/5281/5686001474_e06f1587ff_o.jpg\"}"; // final HttpRequest request =
     * rf.buildPostRequest(shortenEndpoint, new ByteArrayContent(requestBody)); final HttpRequest request = rf.buildGetRequest(shortenEndpoint);
     * request.headers.contentType = "application/json"; final HttpResponse shortUrl = request.execute(); final BufferedReader output = new
     * BufferedReader(new InputStreamReader(shortUrl.getContent())); System.out.println("Shorten Response: "); for (String line = output.readLine();
     * line != null; line = output.readLine()) { System.out.println(line); }
     * 
     * // Refresh a token (SHOULD ONLY BE DONE WHEN ACCESS TOKEN EXPIRES) // access.refreshToken(); System.out.println("Original Token: " +
     * accessToken + " New Token: " + access.getAccessToken()); }
     */

    // Client ID: 58493c70783f52e581efb3774b6ad71aea8e82be
    // Client Secret: 2bced21943b78e6338a463878eaaae2480e0c513

    public static void main(final String[] args) throws HttpException, IOException, EncoderException, URISyntaxException {
        final HttpClient httpclient = new HttpClient();
        final org.apache.commons.httpclient.HttpMethod method = new GetMethod("https://api-ssl.bitly.com/v3/shorten");
        final String longUrl = "http://127.0.0.1:8080/tripfans/locais/praia-do-futuro?tab=dicas&dica=Dica-kldwk454-545";
        final String urlEncoded = new URI(longUrl).toASCIIString();
        System.out.println("url encoded : " + urlEncoded);
        method.setQueryString(new NameValuePair[] { new NameValuePair("longUrl", urlEncoded),
                new NameValuePair("access_token", "4f1750aa924580782cc7146b3b55db1c7f7e8baf") });
        httpclient.executeMethod(method);
        final String response = method.getResponseBodyAsString();
        System.out.println("resposta: " + response);
    }
}