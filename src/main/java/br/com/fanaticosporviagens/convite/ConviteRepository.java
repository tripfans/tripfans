package br.com.fanaticosporviagens.convite;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Viagem;

@Repository
class ConviteRepository extends GenericCRUDRepository<Convite, Long> {

    public void excluirConvites(final Viagem viagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("viagem", viagem);
        this.alterarPorNamedQuery("Convite.excluirConvitesViagem", parametros);
    }

}
