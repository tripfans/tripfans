package br.com.fanaticosporviagens.convite;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author André Thiago
 * 
 */
@Controller
public class ConviteController extends BaseController {

    @Autowired
    private ConviteService service;

    @RequestMapping(value = "/aceitarConviteTripFansViaFacebook", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public StatusMessage aceitarConviteTripFansViaFacebook(@RequestParam(value = "requestIds[]") final String[] facebookAppRequestIds,
            @RequestParam(value = "userId") final String facebookUserId) {
        this.service.aceitarConviteTripFansViaFacebook(Arrays.asList(facebookAppRequestIds), facebookUserId);
        return this.success("Convite aceito com sucesso.");
    }

    @RequestMapping(value = "/enviarConviteTripFansViaPostMuralFacebook", method = RequestMethod.POST, produces = "application/json")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage enviarConviteTripFansViaPostMuralFacebook(@RequestParam(value = "requestId") final String facebookAppRequestId,
            @RequestParam final String idFacebookConvidado) {
        this.service.convidarParaTripFansViaFacebook(facebookAppRequestId, Arrays.asList(idFacebookConvidado));
        return this.success("Os convites foram enviados com sucesso.");
    }

    @RequestMapping(value = "/enviarConviteTripFansViaRequisicaoAplicativoFacebook", method = RequestMethod.POST, produces = "application/json")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage enviarConviteTripFansViaRequisicaoAplicativoFacebook(@RequestParam(value = "requestId") final String facebookAppRequestId,
            @RequestParam(value = "idsFacebookConvidados[]") final String[] idsFacebookConvidados) {
        this.service.convidarParaTripFansViaFacebook(facebookAppRequestId, Arrays.asList(idsFacebookConvidados));
        return this.success("Os convites foram enviados com sucesso.");
    }

    @RequestMapping(value = "/ignorarConvite/{convite}", method = RequestMethod.POST, produces = "application/json")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage ignorarConvite(@PathVariable("convite") final Convite convite) {
        this.service.ignorar(convite);
        return this.success(String.format("O convite enviado por %s foi ignorado.", convite.getRemetente().getDisplayName()));
    }

    @RequestMapping(value = "/ignorarConviteAmizade/{usuario}", method = RequestMethod.POST, produces = "application/json")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage ignorarConviteAmizade(@PathVariable("usuario") final Usuario usuarioQueConvidou) {
        final Convite conviteAmizade = this.service.consultarConviteAmizade(usuarioQueConvidou.getId(), this.getUsuarioAutenticado().getId());
        this.service.ignorar(conviteAmizade);
        return this.success(String.format("O convite enviado por %s foi ignorado.", usuarioQueConvidou.getDisplayName()));
    }

}
