package br.com.fanaticosporviagens.convite;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.EmailTemplate;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Notificacao;
import br.com.fanaticosporviagens.model.entity.ParticipanteViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.TipoConvite;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;
import br.com.fanaticosporviagens.publicacao.PublicacaoService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * @author André Thiago
 *
 */
@Service
public class ConviteService extends GenericCRUDService<Convite, Long> {

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private NotificacaoService notificacaoService;

    @Autowired
    private PublicacaoService publicacaoService;

    @Autowired
    private ConviteRepository repository;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private UsuarioService usuarioService;

    @Transactional
    public void aceitar(final Convite convite) {
        convite.aceitar();
        this.notificacaoService.retirarPendencia(convite);
        // this.publicacaoService.publicar(convite);
        super.alterar(convite);
    }

    @Transactional
    public void aceitarConviteTripFansViaFacebook(final List<String> facebookAppRequestIds, final String idFacebookConvidado) {
        final List<Convite> convites = consultarConvitesTripFansPendentes(facebookAppRequestIds, idFacebookConvidado);
        if (!convites.isEmpty()) {
            Convite convite = null;
            for (int i = 0; i < convites.size(); i++) {
                convite = convites.get(0);
                // Pegar o primeiro e ignonar os demais
                if (i == 0) {
                    if (!convite.isAceito()) {
                        convite.aceitar();
                    }
                } else {
                    if (!convite.isAceito()) {
                        convite.ignorar();
                    }
                }
                this.alterar(convite);
            }
        }
    }

    public Convite consultarConviteAmizade(final Long idRemetente, final Long idConvidado) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("remetente", idRemetente);
        parametros.put("convidado", idConvidado);
        final List<Convite> convites = this.searchRepository.consultarPorNamedQuery("Convite.conviteAmizadePendente", parametros, Convite.class);
        return !convites.isEmpty() ? convites.get(0) : null;
    }

    public Convite consultarConviteAmizade(final Usuario remetente, final Usuario convidado) {
        return this.consultarConviteAmizade(remetente.getId(), convidado.getId());
    }

    public List<Convite> consultarConvitesAmigos(final Long idUsuario, final TipoConvite tipoConvite, final Map<String, Object> parametros) {
        parametros.put("idUsuario", idUsuario);
        parametros.put("tipoConvite", tipoConvite);
        return this.searchRepository.consultarPorNamedQuery("Convite.consultarConvitesAmigos", parametros, Convite.class);
    }

    public List<Convite> consultarConvitesAmigosParaRecomendacao(final Long idViagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", idViagem);
        return this.consultarConvitesAmigos(null, TipoConvite.RECOMENDAR_SOBRE_VIAGEM, parametros);
    }

    public List<Convite> consultarConvitesAmigosParaViagem(final Long idViagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", idViagem);
        return this.consultarConvitesAmigos(null, TipoConvite.PARTICIPACAO_VIAGEM, parametros);
    }

    public List<Convite> consultarConvitesPendentesFacebook(final String idFacebookConvidado, final String... idsAppRequestFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFacebookConvidado", idFacebookConvidado);
        if (idsAppRequestFacebook != null && idsAppRequestFacebook.length > 0) {
            parametros.put("idsAppRequestFacebook", idFacebookConvidado);
        }
        return this.searchRepository.consultarPorNamedQuery("Convite.consultarConvitesPendentesFacebook", parametros, Convite.class);
    }

    public List<Convite> consultarConvitesTripFansPendentes(final List<String> idsAppRequestFacebook, final String idUsuarioFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFacebookConvidado", idUsuarioFacebook);
        parametros.put("idsAppRequestFacebook", idsAppRequestFacebook);
        return this.searchRepository.consultarPorNamedQuery("Convite.convitesTripfansRecebidosPendentes", parametros, Convite.class);
    }

    public Convite consultarConviteTripFansPendenteViaFacebook(final Long idUsuarioConvidou, final String idUsuarioFacebookConvidado) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuarioConvidou", idUsuarioConvidou);
        parametros.put("idFacebookConvidado", idUsuarioFacebookConvidado);
        final List<Convite> convites = this.searchRepository.consultarPorNamedQuery("Convite.consultarConviteTripFansPendenteViaFacebook",
                parametros, Convite.class);
        if (!convites.isEmpty()) {
            return convites.get(0);
        }
        return null;
    }

    /*
     * public void convidarParaTripFansPorPorMensagemPrivada(final String userIds) { if (userIds != null && !userIds.isEmpty()) { final String
     * remetente = this.getUsuarioAutenticado().getUsername(); final List<String> usernames =
     * this.amizadeService.consultarUsernamesFacebookAmigosPorIdsFacebook(Arrays.asList(userIds.split("\\,")), this.getUsuarioAutenticado().getId());
     * for (String email : usernames) { // Para enviar uma mensagem privada, deve-se utilizar o username + '@facebook.com' email += "@facebook.com";
     * // final Convite convite = new Convite(getUsuarioAutenticado(), email).deAmizade(); // incluir(convite);
     *
     * final String assunto = "Venha fazer parte da minha rede no TripFans"; final String mensagem = "Venha fazer parte da minha rede no TripFans";
     * this.emailService.enviarEmail(remetente, "carlinhoswake@gmail.com", assunto, mensagem, new HashMap()); } } }
     */

    @Transactional
    public Convite convidarParaAmizade(final Usuario remetente, final Usuario convidado) {
        final Convite convite = new Convite(remetente, convidado).paraAmizade();
        incluirConvite(convite, false);
        return convite;
        // this.publicacaoService.publicar(convite);
    }

    @Transactional
    public void convidarParaTripFansPorEmails(final String enderecosEmailPorVirgula) {
        if (enderecosEmailPorVirgula != null && !enderecosEmailPorVirgula.isEmpty()) {
            final String[] emails = enderecosEmailPorVirgula.split("[\\s,;\\n\\t]+");
            for (final String email : emails) {
                final Convite convite = new Convite(getUsuarioAutenticado(), email).paraEntrarTripFans();
                if (incluirConvite(convite)) {
                    final String assunto = getUsuarioAutenticado().getDisplayName() + " te convidou para fazer parte rede de amigos dele no TripFans";

                    final Map<String, Object> parametros = new HashMap<String, Object>();
                    try {
                        parametros.put("nomeUsuarioConvidante", StringEscapeUtils.escapeHtml(getUsuarioAutenticado().getDisplayName()));
                        parametros.put("idUsuarioConvidante",
                                URLEncoder.encode(CriptografiaUtil.encrypt(getUsuarioAutenticado().getId() + ""), "UTF-8"));
                        parametros.put("idConvite", convite.getId().toString());
                        parametros.put("emailConvidadoCriptografado", URLEncoder.encode(CriptografiaUtil.encrypt(email), "UTF-8"));
                    } catch (final Exception e) {
                    }
                    if (convite.isAmizade()) {
                        final Notificacao notificacao = this.notificacaoService.notificar(convite);
                        this.notificacaoService.enviarEmail(notificacao, TipoAcao.CONVIDAR_PARA_AMIZADE, convite.getDestinatario());
                    } else {
                        this.emailService.enviarEmail(email, assunto, EmailTemplate.EMAIL_CONVITE, parametros);
                    }
                }
            }
        }
    }

    @Transactional
    public void convidarParaTripFansViaFacebook(final String facebookAppRequestId, final List<String> idsFacebookConvidados) {
        // TODO mudar para um batch insert
        for (final String idFacebookConvidado : idsFacebookConvidados) {
            final Convite convite = new Convite();
            convite.setRemetente(this.getUsuarioAutenticado());
            convite.setIdAppRequestFacebook(facebookAppRequestId);
            convite.setIdFacebookConvidado(idFacebookConvidado);
            convite.paraEntrarTripFans();
            this.salvar(convite);
        }
    }

    @Transactional
    public Convite convidarParaViagem(final Viagem viagem, final ParticipanteViagem participante, final String mensagem) {
        final Usuario remetente = getUsuarioAutenticado();
        Convite convite = null;
        if (participante.getUsuario() != null) {
            convite = new Convite(remetente, participante.getUsuario()).paraParticipacaoViagem(viagem);
        } else {
            convite = new Convite(remetente, participante.getEmail()).paraParticipacaoViagem(viagem);
        }
        if (participante.getUsuario() != null && Boolean.TRUE.equals(participante.getUsuario().getAtivo())) {
            incluirConvite(convite, true);
        } else {
            incluirConvite(convite, false);

            final String email = participante.getUsuario() != null ? participante.getUsuario().getUsername() : participante.getEmail();
            final String assunto = " te convidou para embarcar com ele na Viagem \"" + viagem.getTitulo() + "\" dele.";

            final Map<String, Object> parametros = new HashMap<String, Object>();
            try {
                parametros.put("nomeUsuarioConvidante", StringEscapeUtils.escapeHtml(getUsuarioAutenticado().getDisplayName()));
                parametros.put("idUsuarioConvidante", URLEncoder.encode(CriptografiaUtil.encrypt(getUsuarioAutenticado().getId() + ""), "UTF-8"));
                parametros.put("idConvite", convite.getId().toString());
                parametros.put("emailConvidadoCriptografado", URLEncoder.encode(CriptografiaUtil.encrypt(email), "UTF-8"));

                parametros.put("textoAcao", assunto);
                parametros.put("hideFooter", true);
                parametros.put("textoLink", "Clique aqui para aceitar o convite");

                parametros.put("autor", getUsuarioAutenticado());
                // parametros.put("destinatario", acaoUsuario.getDestinatario());

                parametros.put("url", "/viagem/" + viagem.getId() + "/viajantes/" + participante.getId() + "/aceitarConvite/" + convite.getId());

            } catch (final Exception e) {
            }
            this.emailService.enviarEmail(email, getUsuarioAutenticado().getDisplayName() + assunto, EmailTemplate.EMAIL_NOTIFICACAO, parametros);
        }
        return convite;
    }

    @Transactional
    public Convite convidarParaViagem(final Viagem viagem, final Usuario remetente, final List<Amizade> amizades, final List<String> emails,
            final String mensagem) {
        Convite convite = null;
        for (final Amizade amizade : amizades) {
            if (amizade != null) {
                convite = new Convite(remetente, amizade).paraParticipacaoViagem(viagem);
                incluirConvite(convite);
                if (amizade.isFacebook()) {
                    // TODO ver uma forma melhor de pegar/guardar a URL
                    final String url = "23.23.255.139:8080/tripfans";
                    // TODO ver uma forma melhor de guardar estas mensagens
                    final String titulo = remetente.getDisplayName() + " convidou " + amizade.getNomeAmigo() + " para o TripFans!";
                    final String subTitulo = "Para aceitar o convite, clique no link acima";
                    // postar convite no mural
                    // TODO remover ID do facebook fixo (tripfans1@gmail)
                    this.facebookService.postarLinkNoMuralDeAmigo("100003941183003", url, titulo, subTitulo, mensagem);
                    // this.facebookService.postarLinkNoMuralDeAmigo(amizade.getIdAmigoFacebook(), url, titulo, subTitulo, mensagem);
                }
                // this.publicacaoService.publicar(convite);
            }
        }
        if (emails != null) {
            for (final String email : emails) {
                convite = new Convite(remetente, email).paraParticipacaoViagem(viagem);
                incluirConvite(convite);
            }
        }
        return convite;
    }

    public List<Convite> convitesAmizadeEnviadosPendentes(final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idRemetente", idUsuario);
        return this.searchRepository.consultarPorNamedQuery("Convite.convitesAmizadeEnviadosPendentes", parametros, Convite.class);
    }

    public List<Convite> convitesAmizadeRecebidosPendentes(final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idConvidado", idUsuario);
        return this.searchRepository.consultarPorNamedQuery("Convite.convitesAmizadeRecebidosPendentes", parametros, Convite.class);
    }

    @Transactional
    public void excluirConvites(final Viagem viagem) {
        this.repository.excluirConvites(viagem);
    }

    public boolean existeConviteRecomendacaoSobreViagem(final Viagem viagem, final Usuario destinatario) {
        if (destinatario != null) {
            final List<Convite> convitesRecomendacao = consultarConvitesAmigosParaRecomendacao(viagem.getId());
            for (final Convite convite : convitesRecomendacao) {
                if (StringUtils.isNotEmpty(convite.getEmailConvidado()) && convite.getEmailConvidado().equals(destinatario.getEmail())) {
                    return true;
                }
            }
        }
        return false;
    }

    @Transactional
    public void ignorar(final Convite convite) {
        convite.ignorar();
        super.alterar(convite);
        this.notificacaoService.retirarPendencia(convite);
    }

    @Transactional
    public boolean incluirConvite(final Convite convite) {
        return this.incluirConvite(convite, false);
    }

    @Transactional
    public boolean incluirConvite(final Convite convite, final boolean notificar) {
        try {
            // se o convite para for para Tripfans
            if (convite.isTripfans()) {
                // verificar se o convidado já existe
                final Usuario usuario = verificarUsuarioJaExiste(convite);
                if (usuario != null) {
                    convite.setConvidado(usuario);
                    // se o usuário já existe, convidar para amizade
                    convite.setTipoConvite(TipoConvite.AMIZADE);
                }
            } else if (convite.isAmizade()) {
                // verificar se já são amigos
                if (convite.getConvidado() != null) {
                    if (this.amizadeService.saoAmigos(convite.getAutor().getId(), convite.getConvidado().getId())) {
                        return false;
                    }
                } else if (convite.getIdFacebookConvidado() != null) {
                    if (this.amizadeService.saoAmigosNoFacebook(convite.getAutor().getId(), convite.getIdFacebookConvidado())) {
                        return false;
                    }
                } else if (convite.getEmailConvidado() != null) {
                    if (this.amizadeService.saoAmigos(convite.getAutor().getId(), convite.getEmailConvidado())) {
                        return false;
                    }
                }
            }
            // verificar se convite já existe no tripfans
            if (verificarConviteJaExiste(convite)) {
                // se existir, não inclui outro
                // return false;
            }
            super.incluir(convite);
            if (notificar) {
                this.notificacaoService.notificar(convite);
            }
            return true;
        } catch (final ConstraintViolationException e) {
            e.printStackTrace();
            return false;
            // tentativa de incluir um convite q já existe - não faz nada
        }
    }

    @Transactional
    public void solicitarRecomendacoesParaViagem(final Viagem viagem, final Usuario remetente, final List<Amizade> amizades,
            final List<String> emails, final String mensagem) {
        Convite convite = null;
        for (final Amizade amizade : amizades) {
            if (amizade != null) {
                convite = new Convite(remetente, amizade).paraRecomendacoesViagem(viagem);
                if (amizade.isFacebook()) {
                    // TODO ver uma forma melhor de pegar/guardar a URL
                    final String url = "23.23.255.139:8080/tripfans";
                    // TODO ver uma forma melhor de guardar estas mensagens
                    final String titulo = remetente.getDisplayName() + " está pedindo dicas para " + amizade.getNomeAmigo() + " no TripFans!";
                    final String subTitulo = "Para ajudá-lo, clique no link acima";
                    // postar convite no mural
                    // TODO remover ID do facebook fixo (tripfans1@gmail)
                    this.facebookService.postarLinkNoMuralDeAmigo("100003941183003", url, titulo, subTitulo, mensagem);
                    // this.facebookService.postarLinkNoMuralDeAmigo(amizade.getIdAmigoFacebook(), url, titulo, subTitulo, mensagem);
                }
                // this.publicacaoService.publicar(convite);
            }
        }
        if (emails != null) {
            for (final String email : emails) {
                convite = new Convite(remetente, email).paraRecomendacoesViagem(viagem);
                incluirConvite(convite);
            }
        }
    }

    public boolean temConviteAmizadePendente(final Long idRemetente, final Long idConvidado) {
        return this.consultarConviteAmizade(idRemetente, idConvidado) != null;
    }

    private boolean verificarConviteJaExiste(final Convite convite) {
        final Map<String, Object> parametros = new HashMap<String, Object>();

        parametros.put("idRemetente", convite.getAutor().getId());
        parametros.put("tipoConvite", convite.getTipoConvite());
        if (convite.getConvidado() != null) {
            parametros.put("idConvidado", convite.getConvidado().getId());
        }
        if (convite.getEmailConvidado() != null) {
            parametros.put("emailConvidado", convite.getEmailConvidado());
        } else if (convite.getIdFacebookConvidado() != null) {
            parametros.put("idFacebookConvidado", convite.getIdFacebookConvidado());
        }
        final List<Convite> convites = this.searchRepository.consultarPorNamedQuery("Convite.convites", parametros, Convite.class);
        return (convites != null && !convites.isEmpty());
    }

    private Usuario verificarUsuarioJaExiste(final Convite convite) {
        if (convite.isTripfans()) {
            if (convite.getEmailConvidado() != null) {
                final Usuario usuario = this.usuarioService.consultarUsuarioPorUsername(convite.getEmailConvidado());
                return usuario;
            } else if (convite.getIdFacebookConvidado() != null) {
                final Usuario usuario = this.usuarioService.consultarUsuarioPorIdFacebook(convite.getIdFacebookConvidado());
                return usuario;
            }
        }
        return null;
    }

}
