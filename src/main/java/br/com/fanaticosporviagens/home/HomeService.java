package br.com.fanaticosporviagens.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.MarcacoesUsuarioLocal;
import br.com.fanaticosporviagens.util.SessaoWebUtils;
import br.com.fanaticosporviagens.util.SorteadorItens;
import br.com.fanaticosporviagens.viagem.RoteiroViagemView;

@Service
public class HomeService extends BaseService {

    @Autowired
    private HomeRepository homeRepository;

    @Autowired
    private MarcacoesUsuarioLocal marcacoesUsuarioLocal;

    @Autowired
    private SessaoWebUtils sessaoWebUtils;

    @Autowired
    private SorteadorItens sorteador;

    public List<RoteiroViagemView> consultarRoteirosDestaques() {
        return this.homeRepository.consultarRoteirosDestaque();
    }

}
