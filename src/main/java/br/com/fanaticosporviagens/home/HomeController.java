package br.com.fanaticosporviagens.home;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.viagem.ViagemService;

@Controller
public class HomeController extends BaseController {

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private ViagemService viagemService;

    @RequestMapping(value = "/booking")
    public String booking(final Model model) {
        model.addAttribute("nomeParceiro", "Booking");
        model.addAttribute("nomeLinkParceiro", "Booking.com");
        model.addAttribute("percentualCashbackParceiro", "1,6%");
        model.addAttribute("urlParceiro", "http://www.booking.com?aid=382185");
        model.addAttribute("imgParceiro", "booking.png");

        model.addAttribute("textoParceiro", "Faça sua reserva no Booking.com utilizando o link: "
                + "<a href=\"http://www.booking.com?aid=382185\">http://www.booking.com?aid=382185</a> <br/> ou clique no botão ao lado "
                + "e receba de volta parte do valor da sua compra.");

        return "/redirecionar";
    }

    @RequestMapping(value = "/carros")
    public String carros(final Model model) {
        return "/rentcars";
    }

    @RequestMapping(value = "/destinosDestaques")
    public String destinosDestaques(final Model model) {
        return "redirect:/destinos/explorar";
    }

    @RequestMapping(value = "/home")
    public String home(final Principal currentUser, @RequestParam(value = "fb_source", required = false) final String fbSource, final Model model) {
        model.addAttribute("roteirosDestaques", this.homeService.consultarRoteirosDestaques());
        return "/home";
    }

    @RequestMapping(value = "/fb/home")
    public String homeFacebook(final Principal currentUser, @RequestParam(value = "fb_source", required = false) final String fbSource,
            @RequestParam(value = "request_ids", required = false) final String[] requestIds, final Model model) {
        if (fbSource != null) {
            // currentUser.
            // model.addAttribute("convites", this.conviteService.consultarConvitesPendentesFacebook("100003941183003"));

            // model.addAttribute("convites", this.pedidoDicaService.consultarPedidosPendentesFacebook(usuarioDestinatario));

            if (requestIds != null) {
                model.addAttribute("pedidosDicas", this.pedidoDicaService.consultarPedidosPorFacebookRequestIds(requestIds));
            }

            return "facebookHome";
        }
        model.addAttribute("source", "fb");
        return home(currentUser, fbSource, model);
    }

    @RequestMapping(value = "")
    public String index(final Principal currentUser, @RequestParam(value = "fb_source", required = false) final String fbSource, final Model model) {
        return home(currentUser, fbSource, model);
    }

    @RequestMapping(value = "/parceiros/{parceiro}")
    public String parceiros(@PathVariable final String parceiro, final Model model) {
        model.addAttribute("nomeParceiro", "Booking");
        model.addAttribute("urlParceiro", "http://www.booking.com?aid=382185");
        model.addAttribute("imgParceiro", "booking.png");
        return "/redirecionar";
    }

}
