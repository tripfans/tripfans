package br.com.fanaticosporviagens.home;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class LocalEmDestaque implements Serializable {

    private static final long serialVersionUID = -5358125974924560022L;

    private List<Usuario> amigosJaForam;

    private Set<InteresseUsuarioEmLocal> amostraInteresseUsuarioEmLocal = new HashSet<InteresseUsuarioEmLocal>();

    private Local local;

    private Long quantidadeAmigosJaForam = 0L;

    public LocalEmDestaque() {
        super();
    }

    public LocalEmDestaque(final Local local) {
        super();
        this.local = local;
    }

    public List<Usuario> getAmigosJaForam() {
        return this.amigosJaForam;
    }

    public Set<InteresseUsuarioEmLocal> getAmostraInteresseUsuarioEmLocal() {
        return this.amostraInteresseUsuarioEmLocal;
    }

    public Integer getClassificacaoNota() {
        if ((this.local == null) || (this.local.getAvaliacaoConsolidadaGeral() == null)
                || (this.local.getAvaliacaoConsolidadaGeral().getClassificacaoGeral() == null)) {
            return 0;
        }
        return this.local.getAvaliacaoConsolidadaGeral().getClassificacaoGeral().getNota();
    }

    public BigDecimal getClassificacaoNotaGeral() {
        if ((this.local == null) || (this.local.getAvaliacaoConsolidadaGeral() == null)) {
            return new BigDecimal(0);
        }
        return this.local.getAvaliacaoConsolidadaGeral().getNotaGeral();
    }

    public String getDescricaoQuantidadeJaForam() {
        if (this.getQuantidadeAmigosJaForam() > 0L) {
            return this.getQuantidadeAmigosJaForam() + ((this.getQuantidadeAmigosJaForam() == 1) ? " amigo já foi" : " amigos já foram");
        }
        return this.getQuantidadeJaForam() + ((this.getQuantidadeJaForam() == 1L) ? " viajante já foi" : " viajantes já foram");
    }

    public Long getIdLocal() {
        return getLocal().getId();
    }

    public Local getLocal() {
        return this.local;
    }

    public Boolean getPossuiInteresseJaFoi() {
        return (this.getQuantidadeJaForam() > 0);
    }

    public Long getQuantidadeAmigosJaForam() {
        return this.quantidadeAmigosJaForam;
    }

    public Long getQuantidadeAvaliacoes() {
        return this.local.getQuantidadeAvaliacoes();
    }

    public Long getQuantidadeJaForam() {
        return this.local.getQuantidadeInteressesJaFoi();
    }

    public void setAmigosJaForam(final List<Usuario> amigosJaForam) {
        this.amigosJaForam = amigosJaForam;
    }

    public void setAmostraInteresseUsuarioEmLocal(final Set<InteresseUsuarioEmLocal> amostraInteresseUsuarioEmLocal) {
        this.amostraInteresseUsuarioEmLocal = amostraInteresseUsuarioEmLocal;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setQuantidadeAmigosJaForam(final Long quantidadeAmigosJaForam) {
        this.quantidadeAmigosJaForam = quantidadeAmigosJaForam;
    }
}
