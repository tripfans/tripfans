package br.com.fanaticosporviagens.home.pojo;

public class QuantidadeAmigosJaForamLocalHomePojo {

    private Long idLocal;

    private Long quantidade;

    public Long getIdLocal() {
        return this.idLocal;
    }

    public Long getQuantidade() {
        return this.quantidade;
    }

    public void setIdLocal(final Long idLocal) {
        this.idLocal = idLocal;
    }

    public void setQuantidade(final Long quantidade) {
        this.quantidade = quantidade;
    }

}
