package br.com.fanaticosporviagens.home.pojo;

import br.com.fanaticosporviagens.model.entity.Usuario;

public class DicaHomePojo {

    private Usuario autorDica;

    private Long idAutorDica;

    private Long idDica;

    private Long idLocalDica;

    private String nomeLocal;

    private String textoDica;

    private String urlPathDica;

    private String urlPathLocal;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final DicaHomePojo other = (DicaHomePojo) obj;
        if (this.urlPathDica == null) {
            if (other.urlPathDica != null)
                return false;
        } else if (!this.urlPathDica.equals(other.urlPathDica))
            return false;
        return true;
    }

    public Usuario getAutorDica() {
        return this.autorDica;
    }

    public Long getIdAutorDica() {
        return this.idAutorDica;
    }

    public Long getIdDica() {
        return this.idDica;
    }

    public Long getIdLocalDica() {
        return this.idLocalDica;
    }

    public String getNomeLocal() {
        return this.nomeLocal;
    }

    public String getTextoDica() {
        return this.textoDica;
    }

    public String getUrlPathDica() {
        return this.urlPathDica;
    }

    public String getUrlPathLocal() {
        return this.urlPathLocal;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.urlPathDica == null) ? 0 : this.urlPathDica.hashCode());
        return result;
    }

    public void setAutorDica(final Usuario autorDica) {
        this.autorDica = autorDica;
    }

    public void setIdAutorDica(final Long idAutorDica) {
        this.idAutorDica = idAutorDica;
    }

    public void setIdDica(final Long idDica) {
        this.idDica = idDica;
    }

    public void setIdLocalDica(final Long idLocalDica) {
        this.idLocalDica = idLocalDica;
    }

    public void setNomeLocal(final String nomeLocal) {
        this.nomeLocal = nomeLocal;
    }

    public void setTextoDica(final String textoDica) {
        this.textoDica = textoDica;
    }

    public void setUrlPathDica(final String urlPathDica) {
        this.urlPathDica = urlPathDica;
    }

    public void setUrlPathLocal(final String urlPathLocal) {
        this.urlPathLocal = urlPathLocal;
    }

}
