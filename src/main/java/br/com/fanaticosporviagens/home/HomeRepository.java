package br.com.fanaticosporviagens.home;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.BaseRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.viagem.RoteiroViagemView;

import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.KeyGenerator;

@Repository
public class HomeRepository extends BaseRepository {

    @Autowired
    private GenericSearchRepository searchRepository;

    @Cacheable(cacheName = "roteirosDestaqueHomeCache", keyGenerator = @KeyGenerator(name = "HashCodeCacheKeyGenerator"))
    public List<RoteiroViagemView> consultarRoteirosDestaque() {
        return this.searchRepository.consultarPorNamedNativeQuery("Home.roteirosDestaque", RoteiroViagemView.class);
    }

}
