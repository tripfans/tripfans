package br.com.fanaticosporviagens.noticia;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Estado;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.NoticiaSobreLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Repository
public class NoticiaSobreLocalRepository extends GenericCRUDRepository<NoticiaSobreLocal, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<NoticiaSobreLocal> consultarNoticias(final Local local) {
        final StringBuilder hqlNoticias = new StringBuilder();
        hqlNoticias.append("select noticia ");
        hqlNoticias.append("  from NoticiaSobreLocalParaLocal noticia ");
        hqlNoticias.append(" where noticia.localInteressadoNaNoticia = :localInteressadoNaNoticia ");
        hqlNoticias.append(" order by noticia.dataPublicacao desc ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("localInteressadoNaNoticia", local);

        return this.searchRepository.consultaHQL(hqlNoticias.toString(), parametros);
    }

    public List<NoticiaSobreLocal> consultarNoticias(final Usuario usuarioInteressado) {
        final StringBuilder hqlNoticias = new StringBuilder();
        hqlNoticias.append("select noticia ");
        hqlNoticias.append("  from NoticiaSobreLocalParaUsuario noticia ");
        hqlNoticias.append(" where noticia.usuarioInteressadoNaNoticia = :usuarioInteressadoNaNoticia ");
        hqlNoticias.append(" order by noticia.dataPublicacao desc ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuarioInteressadoNaNoticia", usuarioInteressado);

        return this.searchRepository.consultaHQL(hqlNoticias.toString(), parametros);
    }

    public Long consultarQuantidadeNoticiasPorUsuario(final Long idUsuario) {
        final StringBuilder sqlInsert = new StringBuilder();
        sqlInsert.append(" select count(id_noticia_sobre_local) ");
        sqlInsert.append("   from noticia_sobre_local_para_usuario ");
        sqlInsert.append("  where id_usuario_interessado = :id_usuario_interessado ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("id_usuario_interessado", idUsuario);

        return ((BigInteger) this.searchRepository.consultaSQLResultadoUnico(sqlInsert.toString(), parametros)).longValue();
    }

    public void publicarNoticiaParaUsuariosInteressados(final Avaliacao avaliacao) {
        publicarNoticiaParaUsuariosInteressados("id_avaliacao_publicada", avaliacao.getLocalAvaliado(), avaliacao.getId());

        // TODO Gustavo publicar para os seguidores da cidade, estado, país
    }

    public void publicarNoticiaParaUsuariosInteressados(final Dica dica) {
        publicarNoticiaParaUsuariosInteressados("id_dica_publicada", dica.getLocalDica(), dica.getId());

        // TODO Gustavo publicar para os seguidores da cidade, estado, país
    }

    private void publicarNoticiaParaUsuariosInteressados(final String atributoEntidadePublicada, final Local local, final Long idEntidadePublicada) {
        final StringBuilder sqlInsert = new StringBuilder();
        sqlInsert.append(" insert into noticia_sobre_local_para_usuario ");
        sqlInsert.append("      ( id_noticia_sobre_local ");
        sqlInsert.append("      , id_usuario_interessado ");
        sqlInsert.append("      , data_publicacao ");
        sqlInsert.append("      , id_local_noticia ");
        sqlInsert.append("      , local_type ");
        sqlInsert.append("      , " + atributoEntidadePublicada);
        sqlInsert.append("      ) ");
        sqlInsert.append(" select nextval('seq_noticia_sobre_local') ");
        sqlInsert.append("     , liu.id_usuario_interessado ");
        sqlInsert.append("     , current_timestamp ");
        sqlInsert.append("     , :id_local_de_interesse ");
        sqlInsert.append("     , :local_type ");
        sqlInsert.append("     , :id_entidade_publicada ");
        sqlInsert.append("  from interesse_usuario_em_local liu ");
        sqlInsert.append("  where liu.id_local_de_interesse = :id_local_de_interesse ");
        sqlInsert.append("    and liu.interesse_seguir = true ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("id_local_de_interesse", local.getId());
        parametros.put("id_entidade_publicada", idEntidadePublicada);
        parametros.put("local_type", local.getTipoLocal().getCodigo());

        this.searchRepository.executarSQL(sqlInsert.toString(), parametros);

        if (local.isTipoLocalEnderecavel()) {
            final LocalEnderecavel localEnderecavel = (LocalEnderecavel) local;
            if (localEnderecavel.getCidade() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, localEnderecavel.getCidade(), idEntidadePublicada);
            }
            if (localEnderecavel.getEstado() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, localEnderecavel.getEstado(), idEntidadePublicada);
            }
            if (localEnderecavel.getPais() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, localEnderecavel.getPais(), idEntidadePublicada);
            }
        } else if (local.isTipoCidade()) {
            final Cidade cidade = (Cidade) local;
            if (cidade.getEstado() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, cidade.getEstado(), idEntidadePublicada);
            }
            if (cidade.getPais() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, cidade.getPais(), idEntidadePublicada);
            }
        } else if (local.isTipoEstado()) {
            final Estado estado = (Estado) local;
            if (estado.getPais() != null) {
                publicarNoticiaParaUsuariosInteressados(atributoEntidadePublicada, estado.getPais(), idEntidadePublicada);
            }
        }
    }
}
