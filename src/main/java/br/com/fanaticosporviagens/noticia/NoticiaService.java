package br.com.fanaticosporviagens.noticia;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.NoticiaSobreLocal;
import br.com.fanaticosporviagens.model.entity.NoticiaSobreLocalParaLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Service
public class NoticiaService extends BaseService {

    @Autowired
    private NoticiaSobreLocalRepository noticiaSobreLocalRepository;

    public List<NoticiaSobreLocal> consultarNoticias(final Local local) {
        return this.noticiaSobreLocalRepository.consultarNoticias(local);
    }

    public List<NoticiaSobreLocal> consultarNoticias(final Usuario usuarioInteressado) {
        return this.noticiaSobreLocalRepository.consultarNoticias(usuarioInteressado);
    }

    public Long consultarQuantidadeNoticiasPorUsuario(final Long idUsuario) {
        return this.noticiaSobreLocalRepository.consultarQuantidadeNoticiasPorUsuario(idUsuario);
    }

    @Transactional
    public void publicarNoticia(final Avaliacao avaliacao) {

        for (final Local localInteressado : avaliacao.getLocalAvaliado().getLocaisInteressados()) {
            final NoticiaSobreLocalParaLocal noticia = new NoticiaSobreLocalParaLocal(avaliacao, localInteressado);
            this.noticiaSobreLocalRepository.incluir(noticia);
        }

        this.noticiaSobreLocalRepository.publicarNoticiaParaUsuariosInteressados(avaliacao);
    }

    @Transactional
    public void publicarNoticia(final Dica dica) {

        for (final Local localInteressado : dica.getLocalDica().getLocaisInteressados()) {
            final NoticiaSobreLocalParaLocal noticia = new NoticiaSobreLocalParaLocal(dica, localInteressado);
            this.noticiaSobreLocalRepository.incluir(noticia);
        }

        this.noticiaSobreLocalRepository.publicarNoticiaParaUsuariosInteressados(dica);
    }
}
