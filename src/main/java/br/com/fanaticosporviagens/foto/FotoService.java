package br.com.fanaticosporviagens.foto;

import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Photo;
import org.springframework.social.foursquare.api.Photos;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.foursquare.FoursquareService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.Album;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.ImageFormat;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalAlbum;
import br.com.fanaticosporviagens.model.entity.LocalidadeEmbeddable;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.model.entity.TipoVisibilidade;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * @author André Thiago
 * 
 */
@Service
public class FotoService extends GenericCRUDService<Foto, Long> {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private FotoRepository fotoRepository;

    @Autowired
    private FoursquareService foursquareService;

    @Autowired
    private FotoStorageService storageService;

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    @Autowired
    private UsuarioService usuarioService;

    @Transactional
    public void cancelarPublicacaoFotos(final Long... idsFotos) throws IOException {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idsFotos", idsFotos);
        this.genericCRUDRepository.alterarPorNamedQuery("Foto.cancelarPublicacaoFotos", parametros);

    }

    @Transactional
    public void configuarFotoPadraoExterna(final Local local) {
        final Foto foto = local.getFotoPadrao();
        foto.setTipoArmazenamento(TipoArmazenamentoFoto.EXTERNO);
        if (foto != null) {
            foto.setLocal(local);
            // Usuario TripFans
            foto.setAutor(this.usuarioService.consultarPorId(1l));
            local.setFotoPadraoAnonima(false);
            local.setUsarFotoPanoramio(false);
            foto.setAnonima(false);
            foto.setVisibilidade(TipoVisibilidade.PUBLICO);
            local.setUrlFotoAlbum(foto.getUrlAlbum());
            local.setUrlFotoBig(foto.getUrlBig());
            local.setUrlFotoSmall(foto.getUrlSmall());
            if (foto.getId() == null) {
                this.incluir(foto);
            } else {
                this.alterar(foto);
            }
        }
    }

    public List<Foto> consultarFotosAvaliacao(final Avaliacao avaliacao) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("avaliacao", avaliacao);
        return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosAvaliacao", parametros, Foto.class);
    }

    public List<org.springframework.social.foursquare.api.Photo> consultarFotosFoursquare(final Local local) {

        final Photos photos = this.foursquareService.consultarPhotosLocal(local);
        if (photos != null) {
            return photos.getItems();
        }
        return new ArrayList<org.springframework.social.foursquare.api.Photo>();
    }

    public List<Foto> consultarFotosLocal(final Local local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local);
        if (local.isTipoLocalGeografico()) {
            return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosLocalGeografico", parametros, Foto.class);
        } else {
            return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosLocalEnderecavel", parametros, Foto.class);
        }
    }

    public LocalAlbum consultarFotosParaAlbum(final Local local) {
        final List<Foto> fotos = this.fotoRepository.consultarFotosParaAlbum(local);

        final HashMap<Long, List<Foto>> mapFotos = new HashMap<Long, List<Foto>>();
        for (final Foto foto : fotos) {
            final Long idLocal = foto.getLocal().getId();
            if (!mapFotos.containsKey(idLocal)) {
                mapFotos.put(idLocal, new ArrayList<Foto>());
            }
            mapFotos.get(idLocal).add(foto);
        }

        if (local.isTipoLocalEnderecavel()) {
            return gerarLocalAlbumParaLocalEnderecavel(local, fotos, mapFotos);
        }

        if (local.isTipoLocalInteresseTuristico()) {
            return gerarLocalAlbumParaLocalInteresseTuristico(local, fotos, mapFotos);
        } else if (local.isTipoCidade()) {
            return gerarLocalAlbumParaCidade(local, fotos, mapFotos);
        } else if (local.isTipoEstado()) {
            return gerarLocalAlbumParaEstado(local, fotos, mapFotos);
        } else if (local.isTipoPais()) {
            return gerarLocalAlbumParaPais(local, fotos, mapFotos);
        } else if (local.isTipoContinente()) {
            return gerarLocalAlbumParaContinente(local, fotos, mapFotos);
        }

        return null;
    }

    /**
     * Consulta as fotos postadas por determinado usuário.
     * 
     * @param idUsuario
     * @return
     */
    public List<Foto> consultarFotosPorAutor(final Long idAutor) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAutor", idAutor);
        return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosPorAutor", parametros, Foto.class);
    }

    /**
     * Consulta as fotos sem álbum postadas por determinado usuário.
     * 
     * @param idUsuario
     * @return
     */
    public List<Foto> consultarFotosSemAlbumPorAutor(final Long idAutor) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAutor", idAutor);
        return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosSemAlbumPorAutor", parametros, Foto.class);
    }

    public List<Foto> consultarFotosViagem(final Long idViagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", idViagem);
        return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosViagem", parametros, Foto.class);
    }

    /**
     * Consulta as fotos sem álbum postadas por determinado usuário.
     * 
     * @param idUsuario
     * @return
     */
    public Long consultarQuantidadeFotosSemAlbumPorAutor(final Long idAutor) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAutor", idAutor);
        return this.searchRepository.queryByNamedQueryUniqueResult("Foto.consultarQuantidadeFotosSemAlbumPorAutor", parametros);
    }

    public List<Foto> consultarUltimasFotosLocal(final Local local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local);
        if (local.isTipoLocalGeografico()) {
            return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosLocalGeografico", parametros, 20, Foto.class);
        } else {
            return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosLocalEnderecavel", parametros, 20, Foto.class);
        }
    }

    /**
     * Consulta as últimas fotos postadas por determinado usuário.
     * 
     * @param idUsuario
     * @return
     */
    public List<Foto> consultarUltimasFotosPorAutor(final Long idAutor) {
        return this.consultarUltimasFotosPorAutor(idAutor, null);
    }

    /**
     * Consulta as últimas fotos postadas por determinado usuário.
     * 
     * @param idUsuario
     * @param tipoVisibilidade
     * @return
     */
    public List<Foto> consultarUltimasFotosPorAutor(final Long idAutor, final TipoVisibilidade tipoVisibilidade) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAutor", idAutor);
        if (tipoVisibilidade != null) {
            parametros.put("tipoVisibilidade", tipoVisibilidade);
        }
        return this.searchRepository.consultarPorNamedQuery("Foto.consultarFotosPorAutor", parametros, 10, Foto.class);
    }

    public void cortarFotoUsuario(final Foto foto, final Rectangle coords) {
        this.storageService.crop(foto, ImageFormat.CROP, coords);
    }

    public boolean existeFotoUsuario(final Foto foto) {
        return this.storageService.exists(foto, ImageFormat.ALBUM);
    }

    @Transactional
    public void importarFotosAlbumFacebook(Album albumDestino, final List<Photo> fotosFacebook, final String idFacebookFotoCapa) {
        if (albumDestino == null) {
            albumDestino = this.albumService.criarNovoAlbum();
        }
        this.importarFotosAlbumFacebook(fotosFacebook, idFacebookFotoCapa, albumDestino, null);
    }

    @Transactional
    public void importarFotosAlbumFacebook(final AtividadePlano atividadeDestino, final List<Photo> fotosFacebook) {
        this.importarFotosAlbumFacebook(fotosFacebook, null, null, atividadeDestino);
    }

    @Transactional
    public void incluirFoto(final Foto foto) {
        this.incluirFoto(foto, true, true);
    }

    @Transactional
    public void incluirFoto(final Foto foto, final boolean publicar) {
        this.incluirFoto(foto, true, publicar);
    }

    @Transactional
    public void incluirFoto(final Foto foto, final boolean writeInBackground, final boolean publicar) {
        if (publicar) {
            foto.setDataPublicacao(new LocalDateTime());
        }
        super.incluir(foto);
        if (writeInBackground) {
            this.storageService.writeInBackground(foto);
        } else {
            this.storageService.write(foto);
        }
    }

    @Transactional
    public Foto incluirFotoCapaLocal(final InputStream arquivo, final String nomeArquivo, final Local local) throws IOException {
        final Foto foto = new Foto();
        foto.setLocal(local);
        this.urlGeneratorService.generate(foto);
        foto.setArquivoFoto(IOUtils.toByteArray(arquivo), nomeArquivo);
        foto.setDataCriacao(LocalDateTime.now());
        foto.setDataPublicacao(foto.getDataCriacao());
        foto.setAnonima(false);
        foto.setVisibilidade(TipoVisibilidade.PUBLICO);
        foto.setAutor(this.usuarioService.consultarPorId(1L));

        local.setFotoPadrao(foto);
        local.setFotoPadraoAnonima(false);
        local.setUsarFotoPanoramio(false);
        foto.definirNovaFotoCapaLocal();

        incluir(foto);
        this.storageService.write(foto);

        return foto;
    }

    @Transactional
    public void incluirFotoPadrao(final Local local) {
        final Foto foto = local.getFotoPadrao();
        if (foto != null && foto.temArquivo()) {
            foto.setLocal(local);
            foto.setAutor(this.getUsuarioAutenticado());
            local.setFotoPadraoAnonima(false);
            local.setUsarFotoPanoramio(false);
            foto.setAnonima(false);
            foto.setVisibilidade(TipoVisibilidade.PUBLICO);
            foto.definirNovaFotoCapaLocal();
            local.setUrlFotoAlbum(foto.getUrlAlbum());
            local.setUrlFotoBig(foto.getUrlBig());
            local.setUrlFotoSmall(foto.getUrlSmall());
            this.incluirFoto(foto);
            salvarFotoPadraoLocal(foto);
        } else {
            if (foto.getId() == null) {
                local.setFotoPadrao(null);
            }
        }
    }

    /**
     * Inclui as fotos de uma AtividadePlano de PlanoViagem. A inclusão das fotos é feita de forma assíncrona
     * 
     * @param AtividadePlano
     *            A AtividadePlano da PlanoViagem à qual as fotos estão associadas
     */
    @Transactional
    public void incluirFotos(final AtividadePlano atividadePlano) {
        for (final Foto foto : atividadePlano.getFotos()) {
            if (foto.temArquivo()) {
                foto.setAtividade(atividadePlano);
                this.urlGeneratorService.generate(foto);
                this.incluirFoto(foto, false);
            }
        }
    }

    /**
     * Inclui as fotos de uma avaliação. A inclusão das fotos é feita de forma assíncrona
     * 
     * @param avaliacao
     */
    // TODO configurar para o @Transactional funcionar em métodos privados
    @Transactional
    public void incluirFotos(final Avaliacao avaliacao) {
        for (final Foto foto : avaliacao.getFotos()) {
            if (foto.temArquivo()) {
                foto.setAvaliacao(avaliacao);
                foto.setVisibilidade(TipoVisibilidade.PUBLICO);
                this.urlGeneratorService.generate(foto);
                this.incluirFoto(foto, avaliacao.isPublicado());
            }
        }
    }

    /**
     * Inclui as fotos de um local. A inclusão das fotos é feita de forma assíncrona
     * 
     * @param local
     *            o local ao qual as fotos estão associadas
     */
    @Transactional
    public void incluirFotos(final Local local) {
        // TODO criar álbum
        for (final Foto foto : local.getFotos()) {
            if (foto.temArquivo()) {
                foto.setLocal(local);
                if (foto.getVisibilidade() == null) {
                    foto.setVisibilidade(TipoVisibilidade.PUBLICO);
                }
                foto.setAutor(this.getUsuarioAutenticado());
                this.urlGeneratorService.generate(foto);
                this.incluirFoto(foto);
            }
        }
    }

    /**
     * Inclui as fotos de uma PlanoViagem. A inclusão das fotos é feita de forma assíncrona
     * 
     * @param PlanoViagem
     *            a PlanoViagem à qual as fotos estão associadas
     */
    @Transactional
    public void incluirFotos(final PlanoViagem planoViagem) {
        for (final Foto foto : planoViagem.getFotos()) {
            if (foto.temArquivo()) {
                foto.setViagem(planoViagem);
                this.urlGeneratorService.generate(foto);
                this.incluirFoto(foto, false);
            }
        }
    }

    /**
     * Verifica se o arquivo da foto está bloqueado (por algum processo gravando no arquivo) ou não existe
     * 
     * @param foto
     * @return
     */
    public boolean isArquivoFotoUsuarioBloqueada(final Foto foto) {
        if (this.existeFotoUsuario(foto)) {
            return this.storageService.blocked(foto, ImageFormat.ALBUM);
        }
        return true;
    }

    @Transactional
    public void publicar(final Foto foto) {
        foto.setDataPublicacao(new LocalDateTime());
        this.alterar(foto);
    }

    @Transactional
    public void publicar(final List<Foto> fotos) {
        if (fotos != null) {
            for (final Foto foto : fotos) {
                this.publicar(foto);
            }
        }
    }

    @Transactional
    public void removerFotoPerfil(final Usuario usuario) {
        this.storageService.delete(usuario.getFoto(), ImageFormat.values());
        usuario.setFotoExterna(false);
        usuario.getFoto().setAnonima(true);
        usuario.getFoto().definirFotoPadraoAnonima();
    }

    public void salvarFotoCapaDiarioViagem(final Foto foto) {
        this.urlGeneratorService.generate(foto);
        this.storageService.writeInFormatsInBackground(foto, ImageFormat.BIG);
    }

    public void salvarFotoIlustrativaDeViagem(final Foto foto) {
        this.storageService.writeInBackground(foto);
        if (foto.getId() == null) {
            this.urlGeneratorService.generate(foto);
            this.incluirFoto(foto, false);
        } else {
            this.alterar(foto);
        }
    }

    public void salvarFotoPadraoLocal(final Foto foto) {
        this.storageService.delete(foto);
        this.storageService.writeInBackground(foto);
    }

    @Transactional
    public void salvarFotoPerfilUsuario(final Foto foto) {
        if (foto.getId() != null) {
            foto.setDataUltimaAlteracao(new LocalDateTime());
        }
        foto.definirNovaFotoPerfilUsuario();
        this.salvar(foto);
        this.storageService.delete(foto);
        this.storageService.writeInFormatsInBackground(foto, ImageFormat.ALBUM, ImageFormat.BIG, ImageFormat.SMALL, ImageFormat.CROP);
    }

    @Transactional
    public void selecionarComoCapaDoAlbum(final Foto foto) {
        foto.getAlbum().setFotoPrincipal(foto);
        this.albumService.alterar(foto.getAlbum());
    }

    private LocalAlbum gerarLocalAlbumParaCidade(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoCidade()) {
            throw new IllegalArgumentException("Esperado local do tipo Cidade e o local passado é do tipo " + local.getTipoLocal().getDescricao());
        }

        final HashSet<Long> idsLocais = new HashSet<Long>();
        idsLocais.add(local.getId());

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            final LocalidadeEmbeddable localizacao = foto.getLocal().getLocalizacao();
            if (local.getId().equals(localizacao.getLocalMaisEspecificoId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            } else if (local.getId().equals(localizacao.getCidadeId())) {
                if (localizacao.getLocalInteresseTuristico() != null) {
                    if (!idsLocais.contains(localizacao.getLocalInteresseTuristicoId())) {
                        idsLocais.add(localizacao.getLocalInteresseTuristicoId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalInteresseTuristico(localizacao.getLocalInteresseTuristico().getLocal(), fotos,
                                mapFotos));
                    }
                } else {
                    if (!idsLocais.contains(localizacao.getLocalEnderecavelId())) {
                        idsLocais.add(localizacao.getLocalEnderecavelId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalEnderecavel(localizacao.getLocalEnderecavel().getLocal(), fotos, mapFotos));
                    }
                }
            }
        }

        return album;
    }

    private LocalAlbum gerarLocalAlbumParaContinente(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoContinente()) {
            throw new IllegalArgumentException("Esperado local do tipo Continente e o local passado é do tipo " + local.getTipoLocal().getDescricao());
        }

        final HashSet<Long> idsLocais = new HashSet<Long>();
        idsLocais.add(local.getId());

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            final LocalidadeEmbeddable localizacao = foto.getLocal().getLocalizacao();
            if (local.getId().equals(localizacao.getLocalMaisEspecificoId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            } else if (local.getId().equals(localizacao.getContinenteId())) {
                if (localizacao.getPais() != null) {
                    if (!idsLocais.contains(localizacao.getPaisId())) {
                        idsLocais.add(localizacao.getPaisId());
                        album.addAlbumFilho(gerarLocalAlbumParaPais(localizacao.getPais().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getEstado() != null) {
                    if (!idsLocais.contains(localizacao.getEstadoId())) {
                        idsLocais.add(localizacao.getEstadoId());
                        album.addAlbumFilho(gerarLocalAlbumParaEstado(localizacao.getEstado().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getCidade() != null) {
                    if (!idsLocais.contains(localizacao.getCidadeId())) {
                        idsLocais.add(localizacao.getCidadeId());
                        album.addAlbumFilho(gerarLocalAlbumParaCidade(localizacao.getCidade().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getLocalInteresseTuristico() != null) {
                    if (!idsLocais.contains(localizacao.getLocalInteresseTuristicoId())) {
                        idsLocais.add(localizacao.getLocalInteresseTuristicoId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalInteresseTuristico(localizacao.getLocalInteresseTuristico().getLocal(), fotos,
                                mapFotos));
                    }
                } else {
                    if (!idsLocais.contains(localizacao.getLocalEnderecavelId())) {
                        idsLocais.add(localizacao.getLocalEnderecavelId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalEnderecavel(localizacao.getLocalEnderecavel().getLocal(), fotos, mapFotos));
                    }
                }
            }
        }

        return album;
    }

    private LocalAlbum gerarLocalAlbumParaEstado(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoEstado()) {
            throw new IllegalArgumentException("Esperado local do tipo Estado e o local passado é do tipo " + local.getTipoLocal().getDescricao());
        }

        final HashSet<Long> idsLocais = new HashSet<Long>();
        idsLocais.add(local.getId());

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            final LocalidadeEmbeddable localizacao = foto.getLocal().getLocalizacao();
            if (local.getId().equals(localizacao.getLocalMaisEspecificoId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            } else if (local.getId().equals(localizacao.getEstadoId())) {
                if (localizacao.getCidade() != null) {
                    if (!idsLocais.contains(localizacao.getCidadeId())) {
                        idsLocais.add(localizacao.getCidadeId());
                        album.addAlbumFilho(gerarLocalAlbumParaCidade(localizacao.getCidade().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getLocalInteresseTuristico() != null) {
                    if (!idsLocais.contains(localizacao.getLocalInteresseTuristicoId())) {
                        idsLocais.add(localizacao.getLocalInteresseTuristicoId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalInteresseTuristico(localizacao.getLocalInteresseTuristico().getLocal(), fotos,
                                mapFotos));
                    }
                } else {
                    if (!idsLocais.contains(localizacao.getLocalEnderecavelId())) {
                        idsLocais.add(localizacao.getLocalEnderecavelId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalEnderecavel(localizacao.getLocalEnderecavel().getLocal(), fotos, mapFotos));
                    }
                }
            }
        }

        return album;
    }

    private LocalAlbum gerarLocalAlbumParaLocalEnderecavel(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoLocalEnderecavel()) {
            throw new IllegalArgumentException("Esperado local do tipo Local Endereçavel e o local passado é do tipo "
                    + local.getTipoLocal().getDescricao());
        }

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            if (foto.getLocal().getLocalizacao().getLocalMaisEspecificoId().equals(local.getId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            }
        }

        return album;
    }

    private LocalAlbum gerarLocalAlbumParaLocalInteresseTuristico(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoLocalInteresseTuristico()) {
            throw new IllegalArgumentException("Esperado local do tipo Local de Interesse Turistico e o local passado é do tipo "
                    + local.getTipoLocal().getDescricao());
        }

        final HashSet<Long> idsLocais = new HashSet<Long>();
        idsLocais.add(local.getId());

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            final LocalidadeEmbeddable localizacao = foto.getLocal().getLocalizacao();
            if (local.getId().equals(localizacao.getLocalMaisEspecificoId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            } else {
                if (local.getId().equals(localizacao.getLocalInteresseTuristicoId()) && !idsLocais.contains(localizacao.getLocalMaisEspecificoId())) {
                    idsLocais.add(localizacao.getLocalMaisEspecificoId());
                    album.addAlbumFilho(gerarLocalAlbumParaLocalEnderecavel(localizacao.getLocalMaisEspecifico().getLocal(), fotos, mapFotos));
                }
            }
        }

        return album;
    }

    private LocalAlbum gerarLocalAlbumParaPais(final Local local, final List<Foto> fotos, final HashMap<Long, List<Foto>> mapFotos) {
        if (!local.isTipoPais()) {
            throw new IllegalArgumentException("Esperado local do tipo Pais e o local passado é do tipo " + local.getTipoLocal().getDescricao());
        }

        final HashSet<Long> idsLocais = new HashSet<Long>();
        idsLocais.add(local.getId());

        final LocalAlbum album = new LocalAlbum(local);

        for (final Foto foto : fotos) {
            final LocalidadeEmbeddable localizacao = foto.getLocal().getLocalizacao();
            if (local.getId().equals(localizacao.getLocalMaisEspecificoId())) {
                album.addAllFotos(mapFotos.get(local.getId()));
            } else if (local.getId().equals(localizacao.getPaisId())) {
                if (localizacao.getEstado() != null) {
                    if (!idsLocais.contains(localizacao.getEstadoId())) {
                        idsLocais.add(localizacao.getEstadoId());
                        album.addAlbumFilho(gerarLocalAlbumParaEstado(localizacao.getEstado().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getCidade() != null) {
                    if (!idsLocais.contains(localizacao.getCidadeId())) {
                        idsLocais.add(localizacao.getCidadeId());
                        album.addAlbumFilho(gerarLocalAlbumParaCidade(localizacao.getCidade().getLocal(), fotos, mapFotos));
                    }
                } else if (localizacao.getLocalInteresseTuristico() != null) {
                    if (!idsLocais.contains(localizacao.getLocalInteresseTuristicoId())) {
                        idsLocais.add(localizacao.getLocalInteresseTuristicoId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalInteresseTuristico(localizacao.getLocalInteresseTuristico().getLocal(), fotos,
                                mapFotos));
                    }
                } else {
                    if (!idsLocais.contains(localizacao.getLocalEnderecavelId())) {
                        idsLocais.add(localizacao.getLocalEnderecavelId());
                        album.addAlbumFilho(gerarLocalAlbumParaLocalEnderecavel(localizacao.getLocalEnderecavel().getLocal(), fotos, mapFotos));
                    }
                }
            }
        }

        return album;
    }

    @Transactional
    private void importarFotosAlbumFacebook(final List<Photo> fotosFacebook, final String idFacebookFotoCapa, final Album albumDestino,
            final AtividadePlano atividadeDestino) {
        Foto foto = null;
        for (final Photo photo : fotosFacebook) {
            foto = new Foto();
            foto.setAutor(this.getUsuarioAutenticado());
            foto.setImportadaFacebook(true);
            foto.setIdFacebook(photo.getId());
            foto.setDescricao(photo.getName());
            // TODO ver como pegar o local da foto
            // foto.setLocal(photo.getPlace())
            foto.setDataPostagem(new Date());
            // TODO ver como pegar a data da foto
            // foto.setData(data)
            // TODO ver como definir a ordem da foto nesse caso
            // foto.setOrdem(photo.getPosition());

            if (albumDestino != null) {
                foto.setAlbum(albumDestino);
            }
            if (atividadeDestino != null) {
                atividadeDestino.addFoto(foto);
            } else {
                foto.setDataPublicacao(new LocalDateTime());
            }

            if (photo.getId().equals(idFacebookFotoCapa)) {
                // definir a foto de capa do album
                albumDestino.setFotoPrincipal(foto);
            }

            foto.setTipoArmazenamento(TipoArmazenamentoFoto.EXTERNO);
            foto.setUrlExternaAlbum(photo.getAlbumImage().getSource());
            foto.setUrlExternaBig(photo.getSourceImage().getSource());
            // TODO verificar se tem outra forma de pegar a imagem small
            foto.setUrlExternaSmall(photo.getTinyImage().getSource().replace("_n.", "_a."));
            this.urlGeneratorService.generate(foto);
            super.incluir(foto);
        }
    }

    @Override
    @Transactional
    protected void preExclusao(final Foto foto) {
        final Album album = foto.getAlbum();
        if (album != null && album.getFotoPrincipal() != null && album.getFotoPrincipal().getId() == foto.getId()) {
            album.setFotoPrincipal(null);
        }
    }

}
