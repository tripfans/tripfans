package br.com.fanaticosporviagens.foto;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;
import org.im4java.core.Stream2BufferedImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.exception.ServiceException;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.ImageFormat;

@Service
public class FotoStorageService {

    public static final Dimension MAX_SIZE = new Dimension(1024, 1024);

    private static final String IM4JAVA_TOOLPATH = "IM4JAVA_TOOLPATH";

    @Autowired(required = true)
    private Environment environment;

    private String im4JavaPath;

    private final Log log = LogFactory.getLog(FotoStorageService.class);

    private final String ROOT_FOTOS_DIR = System.getProperty("user.home") + File.separator + "fanaticos" + File.separator;

    public FotoStorageService() {
    }

    public FotoStorageService(final Environment environment) {
        this.environment = environment;
        this.im4JavaPath = this.environment.getProperty(IM4JAVA_TOOLPATH);
    }

    @PostConstruct
    public void afterInit() {
        this.im4JavaPath = this.environment.getProperty(IM4JAVA_TOOLPATH);
    }

    /**
     * Verifica se a foto está bloqueada (sendo gravada por outro processo) no sistema de arquivos.
     * 
     * @param foto
     * @return
     */
    public boolean blocked(final Foto foto, final ImageFormat imageFormat) {
        System.out.println("can read = " + new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).canRead());
        System.out.println("can write = " + new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).canWrite());
        System.out.println("space = " + new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).getTotalSpace());
        System.out.println("length = " + new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).length());
        return !(new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).canRead());
    }

    public void crop(final Foto foto, final ImageFormat format, final Rectangle coords) {
        try {
            final File originalCropFile = new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(ImageFormat.CROP));
            final File tempFile = new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(ImageFormat.CROP) + "_temp");
            FileUtils.copyFile(originalCropFile, tempFile);
            final BufferedImage bufferedImage = ImageIO.read(originalCropFile);
            final IMOperation crop = createOperationCrop(coords.x, coords.y, coords.width, coords.height, true);
            executeCommand(crop, bufferedImage, foto.getNomeCompletoSemExtensao(format));
            // resize(foto, bufferedImage, ImageFormat.ALBUM, ImageFormat.SMALL, ImageFormat.BIG);

            final ImageFormat[] formats = { ImageFormat.ALBUM, ImageFormat.SMALL, ImageFormat.BIG };

            for (final ImageFormat imageFormat : formats) {
                final String imageName = getFullRootPathDir() + File.separator + foto.getNomeCompleto(format);
                if (this.log.isDebugEnabled()) {
                    this.log.debug("Nome do arquivo : " + imageName);
                }
                if (new File(imageName).exists()) {
                    resize(foto, ImageIO.read(new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(format))), imageFormat);
                }
            }

            FileUtils.copyFile(tempFile, originalCropFile);
            tempFile.delete();
        } catch (final Exception e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Erro ao fazer crop.");
                e.printStackTrace();
            }
            throw new ServiceException(e.getMessage());
        }
    }

    public void delete(final Foto foto) {
        delete(foto, ImageFormat.values());
    }

    public void delete(final Foto foto, final ImageFormat... formats) {
        File imageFile = new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto());
        if (imageFile.exists()) {
            imageFile.delete();
        }
        if (formats != null) {
            for (final ImageFormat format : formats) {
                imageFile = new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(format));
                if (imageFile.exists()) {
                    imageFile.delete();
                }
            }
        }
    }

    /**
     * Verifica se a foto existe no sistema de arquivos.
     * 
     * @param foto
     * @return
     */
    public boolean exists(final Foto foto, final ImageFormat imageFormat) {
        return new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto(imageFormat)).exists();
    }

    // TODO ver se vale a pena manter este e o Async
    public void write(final Foto foto) {
        // escreve a foto
        final BufferedImage image = writeToFileSystem(foto);
        // redimensiona
        resize(foto, image, ImageFormat.ALBUM, ImageFormat.SMALL, ImageFormat.BIG);
    }

    @Async
    public void writeInBackground(final Foto foto) {
        // escreve a foto
        final BufferedImage image = writeToFileSystem(foto);
        // redimensiona
        resize(foto, image, ImageFormat.ALBUM, ImageFormat.SMALL, ImageFormat.BIG);
    }

    @Async
    public void writeInFormatsInBackground(final Foto foto, final ImageFormat... formats) {
        // escreve a foto
        final BufferedImage image = writeToFileSystem(foto);
        // redimensiona
        resize(foto, image, formats);
    }

    private BufferedImage convertToJPG(final Foto foto) {
        final IMOperation operation = new IMOperation();
        operation.addImage();
        operation.addImage("jpg:-");
        final ConvertCmd command = new ConvertCmd();
        if (!StringUtils.isEmpty(this.im4JavaPath)) { // HACK para funcionar no Windows
            command.setSearchPath(this.im4JavaPath);
        }
        try {
            final BufferedImage originalImage = ImageIO.read(new ByteArrayInputStream(foto.getBytes()));
            final Stream2BufferedImage outputImage = new Stream2BufferedImage();
            command.setOutputConsumer(outputImage);
            command.run(operation, originalImage);
            return outputImage.getImage();
        } catch (final Exception e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Erro ao dimensionar imagem.");
                e.printStackTrace();
            }
            throw new SystemException("Erro ao converter imagem.", e);
        }
    }

    private IMOperation createOperationCrop(final int x, final int y, final int width, final int height, final boolean ignoreRatio) {
        final IMOperation operation = new IMOperation();
        operation.addImage();
        if (ignoreRatio) {
            operation.crop(width, height, x, y, '!'); // a exclamação faz com que o ratio seja ignorado
        } else {
            operation.crop(width, height, x, y);
        }
        operation.addImage();
        return operation;
    }

    private IMOperation createOperationResize(final int width) {
        final IMOperation operation = new IMOperation();
        operation.addImage();
        operation.resize(width);
        operation.addImage();
        return operation;
    }

    private void executeCommand(final IMOperation operation, final BufferedImage originalImage, final String imageName) throws Exception {
        final ConvertCmd command = new ConvertCmd();
        if (!StringUtils.isEmpty(this.im4JavaPath)) { // HACK para funcionar no Windows
            command.setSearchPath(this.im4JavaPath);
        }
        command.run(operation, originalImage, getFullRootPathDir() + File.separator + imageName + Foto.JPEG_EXTENSION);
    }

    private String getFullRootPathDir() {
        if (Boolean.valueOf(this.environment.getProperty(AppEnviromentConstants.APPLICATION_IS_DEVEL))) {
            return this.ROOT_FOTOS_DIR + this.environment.getProperty(AppEnviromentConstants.APPLICATION_IMAGES_PATH) + File.separator;
        } else {
            return this.environment.getProperty(AppEnviromentConstants.IMAGES_FULL_DIRECTORY);
        }
    }

    private int getMaxWidth(final BufferedImage originalImage) {
        final int newWidth = originalImage.getWidth() > MAX_SIZE.width ? MAX_SIZE.width : originalImage.getWidth();
        return newWidth;
    }

    private boolean needsToResizeToMax(final BufferedImage originalImage) {
        return (originalImage.getWidth() > MAX_SIZE.width);
    }

    /**
     * @see resize(Foto, BufferedImage, ImageFormat)
     */
    private void resize(final Foto foto, final BufferedImage originalImage, final ImageFormat... formats) {
        if (formats != null) {
            for (final ImageFormat format : formats) {
                this.resize(foto, originalImage, format);
            }
        }
    }

    /**
     * @see resize(Foto, BufferedImage, int, int, suffix)
     */
    private void resize(final Foto foto, final BufferedImage originalImage, final ImageFormat format) {
        try {
            int newWidth = format.getWidth();
            if (format.equals(ImageFormat.BIG)) {
                newWidth = originalImage.getWidth();
            }
            final IMOperation operation = createOperationResize(newWidth);
            executeCommand(operation, originalImage, foto.getNomeCompletoSemExtensao(format));
        } catch (final Exception e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Erro ao dimensionar imagem.");
                e.printStackTrace();
            }
            throw new SystemException("Erro ao dimensionar imagem", e);
        }
    }

    /**
     * Redimensiona a imagem nos tamanhos especificados e renomeia com o sufixo informado
     * 
     * @param foto
     * @param originalImage
     * @param newWidth
     * @param newHeight
     * @param suffix
     * @throws Exception
     */
    private void resize(final Foto foto, final BufferedImage originalImage, final int newWidth, final String suffix) {
        try {
            final IMOperation operation = createOperationResize(newWidth);
            executeCommand(operation, originalImage, foto.getNomeCompletoSemExtensao());
        } catch (final Exception e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Erro ao dimensionar imagem.");
                e.printStackTrace();
            }
            throw new SystemException("Erro ao dimensionar imagem", e);
        }
    }

    /**
     * Escreve uma foto no sistema de arquivos em todos os formatos disponíveis.
     * 
     * @param foto
     */
    private BufferedImage writeToFileSystem(final Foto foto) {
        try {
            BufferedImage originalImage = ImageIO.read(new ByteArrayInputStream(foto.getBytes()));
            if (!foto.isJPG()) {
                // precisa converter a imagem para .jpg
                originalImage = convertToJPG(foto);
                final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ImageIO.write(originalImage, Foto.JPEG_EXTENSION, outputStream);
                foto.setBytes(outputStream.toByteArray());
                outputStream.close();
            }
            // checa se o diretório existe
            final File dir = new File(getFullRootPathDir() + File.separator + foto.getDiretorio() + File.separator);
            if (!dir.exists()) {
                dir.mkdirs();
            }

            if (needsToResizeToMax(originalImage)) {
                // redimensiona para o tamanho máximo permitido
                resize(foto, originalImage, getMaxWidth(originalImage), null);
                // recarrega o arquivo, pois, os metadados do mesmo não são atualizados no resize
                final FileInputStream fileInputStream = new FileInputStream(new File(getFullRootPathDir() + File.separator + foto.getNomeCompleto()));
                originalImage = ImageIO.read(fileInputStream);
                fileInputStream.close();
            } else {
                // grava o arquivo original no caminho definido
                final FileOutputStream fileOutputStream = new FileOutputStream(new File(getFullRootPathDir() + File.separator
                        + foto.getNomeCompleto()));
                fileOutputStream.write(foto.getBytes());
                fileOutputStream.flush();
                fileOutputStream.close();
            }

            return originalImage;

        } catch (final Exception e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Erro ao dimensionar imagem.");
                e.printStackTrace();
            }
            throw new SystemException("Erro ao escrever imagem no disco.", e);
        }
    }
}
