package br.com.fanaticosporviagens.foto;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.Local;

/**
 * @author André Thiago
 * 
 */
@Controller
@RequestMapping("/fotos")
public class FotoController extends BaseController {

    @Autowired
    private FotoService fotoService;

    @RequestMapping(value = "/salvarFotoCapaLocal/{local}", method = { RequestMethod.POST })
    public @ResponseBody Map<String, Object> adicionar(@PathVariable final Local local, final MultipartHttpServletRequest request) throws IOException {

        try {
            final MultipartFile mpf = request.getFile(request.getFileNames().next());
            final Foto foto = this.fotoService.incluirFotoCapaLocal(mpf.getInputStream(), mpf.getOriginalFilename(), local);
            final Map<String, Object> files = new HashMap<String, Object>();
            files.put("fileName", foto.getUrlAlbum());
            files.put("fileSyze", mpf.getSize() / 1024 + " Kb");
            files.put("fileType", mpf.getContentType());

            success("Imagem alterada com sucesso");

            return files;

        } catch (final Exception e) {
            error("Erro interno no servidor!");
        }

        return null;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public @ResponseBody String handleFileUpload(@RequestParam("name") final String name, @RequestParam("file") final MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                final byte[] bytes = file.getBytes();
                final BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
                stream.write(bytes);
                stream.close();
                return "You successfully uploaded " + name + " into " + name + "-uploaded !";
            } catch (final Exception e) {
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }

    @RequestMapping(value = "/xxx", method = RequestMethod.POST)
    public void salvarFotoCapaLocal(@RequestParam final Long idLocal, final MultipartHttpServletRequest request) {
        System.out.println("Recebi!");
    }

}