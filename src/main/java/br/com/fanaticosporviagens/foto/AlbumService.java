package br.com.fanaticosporviagens.foto;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.Album;
import br.com.fanaticosporviagens.model.entity.Foto;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Service
public class AlbumService extends GenericCRUDService<Album, Long> {

    @Autowired
    private FotoService fotoService;

    @Autowired
    private FotoStorageService storageService;

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    /**
     * Inclui uma foto no album.
     * 
     * @param usuario
     */
    @Transactional
    public Foto adicionarNovaFoto(final Album album, final byte[] arquivo, final String nomeArquivo) throws IOException {
        final Foto foto = new Foto();
        foto.setAutor(getUsuarioAutenticado());
        foto.setAnonima(false);
        foto.setArquivoFoto(arquivo, nomeArquivo);
        foto.setAlbum(album);
        foto.setDataPublicacao(new LocalDateTime());
        this.urlGeneratorService.generate(foto);

        this.storageService.write(foto);
        this.fotoService.incluir(foto);

        if (album != null && album.getFotoPrincipal() == null) {
            album.setFotoPrincipal(foto);
            this.alterar(album);
        }

        return foto;
    }

    @Transactional
    public void atualizarQuantidadeFotos(final Album album) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAlbum", album.getId());
        getRepository().alterarPorNamedNativeQuery("Album.atualizarQuantidadeFotos", parametros);
    }

    /**
     * Consulta os albuns postados por determinado usuário.
     * 
     * @param idUsuario
     * @return
     */
    public List<Album> consultarAlbunsPorAutor(final Long idAutor) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAutor", idAutor);
        return this.searchRepository.consultarPorNamedQuery("Album.consultarAlbunsPorAutor", parametros, Album.class);
    }

    @Transactional
    public Album criarNovoAlbum() {
        return criarNovoAlbum("Album sem título");
    }

    public Album criarNovoAlbum(final String titulo) {
        final Album album = new Album();
        album.setTitulo(titulo);
        album.setAutor(getUsuarioAutenticado());
        this.urlGeneratorService.generate(album);

        return album;
    }

    @Transactional
    public Album importarAlbumFacebook(final org.springframework.social.facebook.api.Album albumFacebook) {
        final Album album = criarNovoAlbum();
        album.setTitulo(albumFacebook.getName());
        album.setDescricao(albumFacebook.getDescription());
        album.setImportadoFacebook(true);
        album.setIdFacebook(albumFacebook.getId());
        album.setUrlExterna(albumFacebook.getLink());
        album.setQuantidadeFotos(albumFacebook.getCount());
        // TODO definir visibilidade
        // albumDestino.setVisibilidade()

        incluir(album);
        return album;
    }

}
