package br.com.fanaticosporviagens.foto;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.model.entity.TipoVisibilidade;

@Repository
public class FotoRepository extends GenericCRUDRepository<Foto, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<Foto> consultarFotosParaAlbum(final Local local) {

        final StringBuilder hql = new StringBuilder();
        hql.append("select foto ");
        hql.append("  from br.com.fanaticosporviagens.model.entity.Foto foto ");
        hql.append(" where foto.aprovada = true ");
        hql.append("   and foto.tipoArmazenamento = :tipoArmazenamentoFoto ");
        hql.append("   and foto.visibilidade = :visibilidade ");
        if (local.isTipoContinente()) {
            hql.append("   and foto.local.localizacao.continente.local = :local ");
        } else if (local.isTipoPais()) {
            hql.append("   and foto.local.localizacao.pais.local = :local ");
        } else if (local.isTipoEstado()) {
            hql.append("   and foto.local.localizacao.estado.local = :local ");
        } else if (local.isTipoCidade()) {
            hql.append("   and foto.local.localizacao.cidade.local = :local ");
        } else if (local.isTipoLocalInteresseTuristico()) {
            hql.append("   and foto.local.localizacao.localInteresseTuristico.local = :local ");
        } else if (local.isTipoLocalEnderecavel()) {
            hql.append("   and foto.local.localizacao.localEnderecavel.local = :local ");
        }
        hql.append(" order by coalesce(foto.local.localizacao.continente.nome,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.pais.nome,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.estado.nome,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.cidade.nome,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.localInteresseTuristico.nome,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.localEnderecavel.tipo,'0') ");
        hql.append("        , coalesce(foto.local.localizacao.localEnderecavel.nome,'0') ");
        hql.append("        , foto.destaque ");
        hql.append("        , foto.destaqueLocal ");
        hql.append("        , foto.data desc ");

        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("local", local);
        params.put("tipoArmazenamentoFoto", TipoArmazenamentoFoto.LOCAL);
        params.put("visibilidade", TipoVisibilidade.PUBLICO);

        return this.searchRepository.consultaHQL(hql.toString(), params, 2000);
    }

}
