package br.com.fanaticosporviagens.gmail.service;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.sprockets.google.Place;
import net.sf.sprockets.google.Place.Review;
import net.sf.sprockets.google.Places;
import net.sf.sprockets.google.Places.Params;
import net.sf.sprockets.google.Places.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.facebook.service.BaseProviderService;
import br.com.fanaticosporviagens.infra.exception.ServiceException;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;
import br.com.fanaticosporviagens.usuario.view.ContatoView;

import com.google.gdata.client.AuthTokenFactory;
import com.google.gdata.client.Service.GDataRequestFactory;
import com.google.gdata.client.contacts.ContactsService;
import com.google.gdata.client.http.GoogleGDataRequest;
import com.google.gdata.data.contacts.ContactEntry;
import com.google.gdata.data.contacts.ContactFeed;
import com.google.gdata.data.extensions.Email;
import com.google.gdata.data.extensions.Name;

@Service
public class GoogleService extends BaseProviderService {

    final GDataRequestFactory gDataRequestFactory = new GoogleGDataRequest.Factory();

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private UsuarioService usuarioService;

    public List<Review> consultarReviewsLocal(final Local local) {
        if (local.getIdGooglePlaces() != null) {
            final Place place = recuperarLocalNoGooglePlaces(local.getIdGooglePlaces(), local.getReferenceGooglePlaces(), local);
            if (place != null) {
                return place.getReviews();
            }
        }
        return null;
    }

    public String getGoogleAccessToken() throws IOException {
        final Connection<Google> connection = getConnection(Google.class);
        if (connection.hasExpired()) {
            connection.refresh();
        }
        return connection.createData().getAccessToken();
    }

    public List<ContatoView> recuperarContatos(final GDataRequestFactory requestFactory, final AuthTokenFactory authTokenFactory,
            final String accessToken) throws ServiceException, IOException, com.google.gdata.util.ServiceException {

        final ContactsService contactsService = new ContactsService("tripfans", requestFactory, authTokenFactory);

        final ArrayList<ContactEntry> profiles = new ArrayList<ContactEntry>();

        URL feedUrl = new URL("https://www.google.com/m8/feeds/contacts/default/full?v=3.0&access_type=offline&access_token="
                + getGoogleAccessToken());
        while (feedUrl != null) {
            final ContactFeed resultFeed = contactsService.getFeed(feedUrl, ContactFeed.class);

            profiles.addAll(resultFeed.getEntries());

            if (resultFeed.getNextLink() != null && resultFeed.getNextLink().getHref() != null && resultFeed.getNextLink().getHref().length() > 0) {
                feedUrl = new URL(resultFeed.getNextLink().getHref() + "&access_token=" + accessToken);
            } else {
                feedUrl = null;
            }
        }

        final List<String> listaEmails = new ArrayList<String>();

        /*
         * Varre todos os contatos, verifica quais possuem emails e armazenda na lista
         */
        for (final ContactEntry entry : profiles) {
            if (entry.hasEmailAddresses()) {
                for (final Email email : entry.getEmailAddresses()) {
                    listaEmails.add(email.getAddress());
                }
            }
        }

        this.usuarioService = SpringBeansProvider.getBean(UsuarioService.class);
        this.amizadeService = SpringBeansProvider.getBean(AmizadeService.class);

        final String emailProprio = getUsuarioAutenticado().getUsername();

        // Removendo o próprio email da lista (caso exista)
        listaEmails.remove(emailProprio);

        // Realiza uma consulta para saber quais dos emails já possuem cadastro
        final List<String> listaEmailsJaCadastrados = this.usuarioService.consultarEmailsJaCadastrados(listaEmails);

        List<String> listaEmailsDeAmigos = new ArrayList<String>();

        // Realiza uma consulta para saber quais dos emails já cadastrados são amigos do usuario
        if (listaEmailsJaCadastrados != null && !listaEmailsJaCadastrados.isEmpty()) {
            listaEmailsDeAmigos = this.amizadeService.consultarEmailsQuePossuemAmizade(listaEmailsJaCadastrados, getUsuarioAutenticado().getId());
        }

        ContatoView contato = null;
        String enderecoEmail = null;
        final Set<ContatoView> contatos = new HashSet<ContatoView>();

        // Varre todos os contatos novamente para pegar o nome e marcar quais já estão cadastrados
        for (final ContactEntry entry : profiles) {

            if (entry.hasEmailAddresses()) {
                for (final Email email : entry.getEmailAddresses()) {
                    enderecoEmail = email.getAddress();
                    // descosiderando o proprio email
                    if (enderecoEmail.equals(emailProprio)) {
                        continue;
                    }
                    contato = new ContatoView();
                    if (entry.hasName()) {
                        final Name name = entry.getName();
                        if (name.hasFullName()) {
                            contato.setNome(name.getFullName().getValue());
                        } else if (name.hasGivenName()) {
                            contato.setNome(name.getGivenName().getValue());
                        }
                    } else {
                        // usar o email retirando '@' e o que vem depois
                        contato.setNome(enderecoEmail.substring(0, enderecoEmail.indexOf("@")));
                    }
                    contato.setEmail(enderecoEmail);
                    // Ver se o email esta na lista dos cadastrados
                    if (listaEmailsJaCadastrados.contains(enderecoEmail)) {
                        contato.setCadastradoTripfans(true);
                    }
                    // Ver se o email esta na lista dos amigos
                    if (listaEmailsDeAmigos.contains(enderecoEmail)) {
                        contato.setAmigo(true);
                    }
                    contatos.add(contato);
                }
            }
        }
        return new ArrayList<ContatoView>(contatos);

    }

    public List<Place> recuperarLocaisSemelhantesNoGoogle(final Local local) {

        final List<Place> resultPlaces = new ArrayList<Place>();
        try {
            Response<List<Place>> responsePlaces;
            List<Place> places = null;
            if (local.getLatitude() != null && local.getLongitude() != null) {
                responsePlaces = Places.radarSearch(new Params().location(local.getLatitude(), local.getLongitude()).keyword(local.getNome())
                        .radius(200).maxResults(5).language("pt-BR"));
            } else {
                Double latitudeCidade = null, longitudeCidade = null;
                if (local instanceof LocalEnderecavel) {
                    latitudeCidade = ((LocalEnderecavel) local).getCidade().getLatitude();
                    longitudeCidade = ((LocalEnderecavel) local).getCidade().getLongitude();
                }
                responsePlaces = Places.textSearch(new Params().query(local.getNome()).location(latitudeCidade, longitudeCidade).radius(10000)
                        .maxResults(5).language("pt-BR"));
            }

            if (responsePlaces.getStatus() == Places.Response.Status.OK) {
                places = responsePlaces.getResult();
                if (places != null) {

                    for (final Place place : places) {
                        if (place.getReference().length() > 0) {
                            resultPlaces.add(recuperarLocalNoGooglePlaces(place.getReference()));
                        }
                    }

                }

            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return resultPlaces;
    }

    public Place recuperarLocalNoGooglePlaces(final String placeReference) throws IOException {
        Place place = null;
        // Tenta buscar diretamente pela referencia
        final Response<Place> details = Places.details(new Params().reference(placeReference).language("pt-BR"));
        if (details.getStatus() == Places.Response.Status.OK) {
            place = details.getResult();
        }
        return place;
    }

    public Place recuperarLocalNoGooglePlaces(final String placeId, final String placeReference, final Local local) {
        Place place = null;
        try {
            if (placeReference != null) {
                // Tenta buscar diretamente pela referencia
                place = recuperarLocalNoGooglePlaces(placeReference);
            }
            if (place == null) {
                // Se nao encontrar, executa uma nova busca pela Latitude/Longitude ou nome para pegar uma nova referencia
                Response<List<Place>> search = null;
                // Se tiver latitude/longitude, fazer a busca por 'radarSearch'
                if (local.getLatitude() != null) {
                    search = Places.radarSearch(new Params().location(local.getLatitude(), local.getLongitude()));
                } else {
                    // Se tiver latitude/longitude tentar buscar pelo nome do local
                    search = Places.textSearch(new Params().query(local.getNome()));
                }
                // Varre a lista com o resultado e busca o 'place' que tenha o placeId informado como parametro
                if (search.getStatus() == Places.Response.Status.OK) {
                    final List<Place> places = search.getResult();
                    if (places != null && !places.isEmpty()) {
                        for (final Place result : places) {
                            if (result.getId().equals(placeId) && result.getReference().length() > 0) {
                                place = recuperarLocalNoGooglePlaces(result.getReference());
                                break;
                            }
                        }
                    }
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return place;
    }

    protected Google getGoogle() {
        return getGoogle(null);
    }

    protected Google getGoogle(final Usuario usuario) {
        final Connection<Google> google = getConnection(usuario, Google.class);
        if (google != null && google.hasExpired()) {
            google.refresh();
        }
        return google != null ? google.getApi() : new GoogleTemplate();
    }

    protected boolean isConnected(final Usuario usuario) {
        return this.getConnection(usuario, Google.class) != null;
    }

}
