package br.com.fanaticosporviagens.dica.controller;

import java.util.Collections;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.OperationNotPermittedException;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.UncategorizedApiException;
import org.springframework.social.foursquare.api.Tip;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.twitter.service.TwitterService;

/**
 * @author André Thiago
 * 
 */
// @Controller
// @RequestMapping("/dicas")
public class DicaController extends BaseController {

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private LocalService localService;

    @Autowired
    private DicaService service;

    @Autowired
    private TwitterService twitterService;

    @RequestMapping(value = "/compartilharFacebook/{dica}", method = { RequestMethod.GET })
    public @ResponseBody StatusMessage compartilharFacebook(@PathVariable final Dica dica, final Model model) {
        try {
            this.facebookService.postarNoMural(dica);
        } catch (final OperationNotPermittedException e) {
            return error("Não foi possível compartilhar a dica.");
        } catch (final RevokedAuthorizationException e) {
            return error("Não é possível compartilhar a dica no Facebook, pois o TripFans teve a autorização de compartilhamento revogado"
                    + " (isso aconteceu OU porque você OU o Facebook removeu esta autorização).");
        } catch (final UncategorizedApiException e) {
            return error("Houve um erro ao compartilhar a dica no Facebook. Tente novamente mais tarde.");
        } catch (final Exception e) {
            return error("Houve um erro ao compartilhar a dica no Facebook. Tente novamente mais tarde.");
        }
        return success("Dica compartilhada no Facebook com sucesso.");
    }

    @RequestMapping(value = "/compartilharTwitter/{dica}", method = { RequestMethod.GET })
    public @ResponseBody StatusMessage compartilharTwitter(@PathVariable final Dica dica, final Model model) {
        try {
            this.twitterService.postarTweet(dica);
        } catch (final OperationNotPermittedException e) {
            return error("Não foi possível compartilhar a dica.");
        }
        return success("Dica compartilhada no Twitter com sucesso.");
    }

    @RequestMapping(value = "/dicaComSucesso")
    @PreAuthorize("isAuthenticated()")
    public String dicaComSucesso(final Model model) {
        return "/dicas/dicaComSucesso";
    }

    @RequestMapping(value = "/excluir/{dica}")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public JSONResponse excluirDica(@PathVariable final Dica dica, final Model model) {
        if (dica != null) {
            this.service.excluir(dica);
        }
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/exibirCard/{dica}", method = { RequestMethod.GET })
    public String exibirCard(@PathVariable final Dica dica, final Model model) {
        model.addAttribute("dica", dica);
        return "/dicas/exibirCardDica";
    }

    @RequestMapping(value = "/escrever/{urlLocal}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String formDica(@PathVariable final String urlLocal, final Model model) {
        if (!model.containsAttribute("dica")) {
            model.addAttribute("dica", new Dica());
        }
        Local local = this.localService.consultarLocalPorUrlPath(urlLocal);
        if (local == null && StringUtils.isNumeric(urlLocal)) {
            local = this.localService.consultarPorId(Long.parseLong(urlLocal));
        }
        model.addAttribute("local", local);
        model.addAttribute("itensClassificacao", this.service.consultarItensClassificacaoDica());
        if (local.getValidado()) {
            model.addAttribute("amostraDicas", this.service.consultarDicasMaisRecentes(local, 4));
        }

        return "/dicas/formDica";
    }

    @RequestMapping(value = "/escrever/incluir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String incluirDica(@Valid @ModelAttribute("dica") final Dica dica, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/dicas/escrever";
        }
        incluir(dica);
        final Local localDica = dica.getLocalDica();
        if (localDica.getValidado()) {
            redirectAttributes.addFlashAttribute("dica", dica);
            redirectAttributes.addFlashAttribute("local", localDica);
            redirectAttributes.addFlashAttribute("promocaoTabletValida", promocaoTabletValida());
            return "redirect:/dicas/dicaComSucesso";
        } else {
            success("Dica incluída com sucesso. Ela será exibida assim que este local for validado pela Equipe do TripFans.");
            return "redirect:/perfil/" + getUsuarioAutenticado().getUrlPath();
        }
    }

    @RequestMapping(value = "/atividade/{atividade}/incluir", method = { RequestMethod.POST })
    public @ResponseBody JSONResponse incluirDicaAtividade(@Valid @ModelAttribute("dica") final Dica dica,
            @PathVariable final AtividadePlano atividadePlano, final Model model) {
        dica.setAtividade(atividadePlano);
        this.service.incluir(dica, false);
        dica.getAlvo();
        this.jsonResponse.addParam("id", dica.getId());
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/escrever/incluirEscreverOutra", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String incluirDicaEscreverOutra(@Valid @ModelAttribute("dica") final Dica dica, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/dicas/escrever";
        }
        incluir(dica);
        success("Dica incluída com sucesso.");
        return "redirect:/dicas/escrever/" + dica.getLocalDica().getUrlPath();
    }

    @RequestMapping(value = "/responderPedido/incluirOutraRespostaPedidoDica", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String incluirOutraRespostaPedidoDica(@Valid @ModelAttribute("dica") final Dica dica, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/dicas/formRespostaPedidoDica";
        }
        incluir(dica);
        success("Resposta de pedido dica incluída com sucesso.");
        return "redirect:/dicas/formRespostaPedidoDica/" + dica.getIdPedidoDica();
    }

    @RequestMapping(value = "/listar/{local}", method = { RequestMethod.GET })
    public String listarDicas(@PathVariable final Local local, @RequestParam(value = "item", required = false) final Dica dicaDestaque,
            final Model model) {
        List<Dica> dicas = Collections.emptyList();
        if (local.getQuantidadeDicas() >= 1) {
            dicas = this.service.consultarDicas(local);
        }
        model.addAttribute("comentario", new Comentario());
        model.addAttribute("urlLocal", local.getUrlPath());
        model.addAttribute("dicaDestaque", dicaDestaque);
        model.addAttribute("dicas", dicas);
        if (this.getSearchData() != null && this.getSearchData().getStart() == 0 && !this.getSearchData().isOrderDataValid()) {
            return "/dicas/dicasLocal";
        }
        return "/dicas/listaDicas";
    }

    @RequestMapping(value = "/4sq/listar/{local}", method = { RequestMethod.GET })
    public String listarDicasFoursquare(@PathVariable final Local local, @RequestParam(value = "item", required = false) final Dica dicaDestaque,
            final Model model) {
        final List<Tip> dicasFoursquare = this.service.consultarDicasFoursquare(local);
        model.addAttribute("dicasFoursquare", dicasFoursquare);
        return "/dicas/listaDicasFoursquare";
    }

    @RequestMapping(value = "/sobreOQueEscreverDica")
    @PreAuthorize("isAuthenticated()")
    public String sobreOQueEscreverDica() {
        return "/dicas/sobreOQueEscreverDica";
    }

    private void incluir(final Dica dica) {
        final Local localDica = dica.getLocalDica();
        if (localDica.getValidado()) {
            this.service.incluir(dica);
            try {
                this.interesseUsuarioEmLocalService.publicarInteresseJaFoi(dica.getAutor(), dica.getLocalDica());
                final List<Local> locais = dica.getLocalDica().getTodosPais();
                for (final Local local : locais) {
                    try {
                        this.interesseUsuarioEmLocalService.publicarInteresseJaFoi(dica.getAutor(), local);
                    } catch (final PersistenceException e) {
                        // geralmente é erro de violação de UK; o local já foi marcado anteriomente
                        continue;
                    }
                }
            } catch (final PersistenceException e) {
                // geralmente é erro de violação de UK; o local já foi marcado anteriomente
            }
        } else {
            this.service.incluir(dica, false);
        }
    }

}
