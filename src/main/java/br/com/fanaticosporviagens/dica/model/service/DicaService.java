package br.com.fanaticosporviagens.dica.model.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.foursquare.api.FoursquareUser;
import org.springframework.social.foursquare.api.Tip;
import org.springframework.social.foursquare.api.Tips;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.dica.model.repository.DicaRepository;
import br.com.fanaticosporviagens.foursquare.FoursquareService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.ItemClassificacaoDica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.publicacao.PublicacaoService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Service
public class DicaService extends GenericCRUDService<Dica, Long> {

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private DicaRepository dicaRepository;

    @Autowired
    private FoursquareService foursquareService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PublicacaoService publicacaoService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    @Autowired
    private UsuarioService usuarioService;

    public void associarTags(final Dica dica, final List<Long> tags) {
        this.transactionTemplate.execute(new TransactionCallback<Object>() {

            @Override
            public Object doInTransaction(final TransactionStatus ts) {
                try {
                    DicaService.this.dicaRepository.associarTags(dica.getId(), tags);
                } catch (final Exception e) {
                    ts.setRollbackOnly();
                }
                return null;
            }

        });
    }

    public List<Dica> consultarDicas(final Local local) {
        return this.dicaRepository.consultarDicasPorUrlLocal(local);
    }

    public List<Tip> consultarDicasFoursquare(final Local local) {
        final Tips tips = this.foursquareService.consultarTipsLocal(local);
        if (tips != null) {
            return tips.getItems();
        }
        return new ArrayList<Tip>();
    }

    public List<Dica> consultarDicasMaisRecentes(final int tamanhoAmostra) {
        return consultarDicasMaisRecentes(null, tamanhoAmostra);
    }

    public List<Dica> consultarDicasMaisRecentes(final Local local, final int tamanhoAmostra) {
        return this.dicaRepository.consultarDicasMaisRecentes(local, tamanhoAmostra);
    }

    public List<Dica> consultarDicasPorUsuario(final Usuario usuario) {
        return this.dicaRepository.consultarDicasPorUsuario(usuario);
    }

    public List<ItemClassificacaoDica> consultarItensClassificacaoDica() {
        return this.searchRepository.consultarPorNamedQuery("Dica.consultarItensClassificacaoDica", new HashMap<String, Object>(),
                ItemClassificacaoDica.class);
    }

    @Override
    @Transactional
    @PreAuthorize("#dica.autor.username == authentication.name")
    public void excluir(final Dica dica) {
        super.excluir(dica);
    }

    @Override
    @Transactional
    public void incluir(final Dica dica) {
        this.incluir(dica, true);
    }

    @Transactional
    public void incluir(final Dica dica, final Amizade amizade) {
        if (amizade.getAmigo() != null) {
            // ver se pubica ou nao
            this.incluir(dica, amizade.getAmigo(), true);
        } else if (amizade.getIdAmigoFacebook() != null) {
            this.incluir(dica, amizade.getIdAmigoFacebook());
        }
    }

    @Transactional
    public void incluir(final Dica dica, final boolean publicar) {
        this.incluir(dica, getUsuarioAutenticado(), publicar);
    }

    @Transactional
    public void incluir(final Dica dica, final String idUsuarioFacebook) {
        try {
            this.urlGeneratorService.generate(dica);
            dica.setIdAutorFacebook(idUsuarioFacebook);
            final Usuario usuario = this.usuarioService.consultarUsuarioPorIdFacebook(idUsuarioFacebook);
            if (usuario == null) {
                final List<String> listaIdsFacebook = new ArrayList<String>();
                listaIdsFacebook.add(idUsuarioFacebook);
                final List<String> usernames = this.amizadeService.consultarNomesAmigosPorIdsFacebook(listaIdsFacebook, getUsuarioAutenticado()
                        .getId());
                if (!usernames.isEmpty()) {
                    dica.setNomeAutor(usernames.get(0));
                }
            } else {
                dica.setAutor(usuario);
            }
            dica.setDataCadastro(new Date());
            dica.setQuantidadeVotoUtil(0L);

            this.dicaRepository.incluir(dica);
            // TODO ver se iremos "publicar" dicas vindas de outras redes
            this.publicacaoService.publicar(dica);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Transactional
    public void incluir(final Dica dica, final Usuario usuario, final boolean publicar) {
        this.urlGeneratorService.generate(dica);
        dica.setAutor(usuario);
        dica.setDataCadastro(new Date());
        dica.setQuantidadeVotoUtil(0L);
        dica.ajustarItensClassificacao();

        if (publicar) {
            dica.setDataPublicacao(new LocalDateTime());
        } else {
            dica.setDataPublicacao(null);
        }
        this.dicaRepository.incluir(dica);
    }

    @Transactional
    public void publicar(final Dica dica) {
        dica.setDataPublicacao(new LocalDateTime());
        this.alterar(dica);
    }

    @Transactional
    public void publicar(final List<Dica> dicas) {
        for (final Dica dica : dicas) {
            this.publicar(dica);
        }
    }

    public void registrarDicasAmigosAPartirDoFoursquare(final Usuario usuario, final FoursquareUser friend) {
        // consultar Amizade
        if (friend.getContactInfo().getFacebook() != null && !friend.getContactInfo().getFacebook().isEmpty()) {
            final Amizade amizade = this.amizadeService.consultarAmigo(usuario.getId(), friend.getContactInfo().getFacebook());
            // se a amizade existir, importar dicas
            if (amizade != null) {
                final Tips tips = this.foursquareService.consultarTipsAmigo(usuario, friend.getId());
                for (final Tip tip : tips.getItems()) {
                    // Verificar se a dica não existe
                    if (consultarDicaPorIdFoursquare(tip.getId()) == null) {
                        final Dica dica = criarDicaAPartirFoursquareTip(tip);
                        if (dica != null) {
                            incluir(dica, amizade);
                        }
                    }
                }
            }
        }
    }

    public void registrarDicasAPartirDoFoursquare(final Usuario usuario) {
        final Tips tips = this.foursquareService.consultarTipsUsuario(usuario);
        for (final Tip tip : tips.getItems()) {
            // Verificar se a dica não existe
            if (consultarDicaPorIdFoursquare(tip.getId()) == null) {
                final Dica dica = criarDicaAPartirFoursquareTip(tip);
                if (dica != null) {
                    // TODO Publicar dica ou nao nesse caso?
                    incluir(dica, usuario, true);
                }
            }
        }
    }

    private Dica consultarDicaPorIdFoursquare(final String idFoursquare) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFoursquare", idFoursquare);
        final List<Dica> dicas = this.searchRepository.consultarPorNamedQuery("Dica.consultarPorIdFoursquare", parametros, Dica.class);
        if (dicas != null && !dicas.isEmpty()) {
            return dicas.get(0);
        }
        return null;
    }

    private Dica criarDicaAPartirFoursquareTip(final Tip tip) {
        Dica dica = null;
        // Verificar se o local existe na nossa base
        final Local local = this.localService.consultarLocalEquivalente(tip.getVenue());
        if (local != null) {
            dica = new Dica();
            dica.setLocalDica(local);
            dica.setDataCadastro(new Date());
            dica.setDescricao(tip.getText());
            dica.setIdFoursquare(tip.getId());
        }
        return dica;
    }

}
