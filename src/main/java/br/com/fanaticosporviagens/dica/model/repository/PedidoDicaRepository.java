package br.com.fanaticosporviagens.dica.model.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.Viagem;

@Repository
public class PedidoDicaRepository extends GenericCRUDRepository<PedidoDica, Long> {

    public void excluirPedidos(final Viagem viagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("viagem", viagem);
        this.alterarPorNamedQuery("PedidoDica.excluirPedidosViagem", parametros);
    }

}
