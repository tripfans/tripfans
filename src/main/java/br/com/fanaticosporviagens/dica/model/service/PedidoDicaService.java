package br.com.fanaticosporviagens.dica.model.service;

import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.repository.PedidoDicaRepository;
import br.com.fanaticosporviagens.dica.pedidodica.ItemPedidoDicaView;
import br.com.fanaticosporviagens.dica.pedidodica.PedidosDicaView;
import br.com.fanaticosporviagens.dica.pedidodica.RespostaPedidoDicaView;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.EmailTemplate;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.RecomendacaoLocalPedidoDica;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Service
public class PedidoDicaService extends GenericCRUDService<PedidoDica, Long> {

    class PedidosDicaViewPredicado implements Predicate {

        private final Object expected;

        private final String propriedade;

        public PedidosDicaViewPredicado(final String propriedade, final Object expected) {
            super();
            this.propriedade = propriedade;
            this.expected = expected;
        }

        @Override
        public boolean evaluate(final Object object) {
            try {
                return this.expected.equals(PropertyUtils.getProperty(object, this.propriedade));
            } catch (final Exception e) {
                return false;
            }
        }

    }

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private DicaService dicaService;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private PedidoDicaRepository repository;

    @Autowired
    private UsuarioService usuarioService;

    public List<PedidosDicaView> consultarPedidosPendentes(final Usuario usuarioDestinatario) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("destinatario", usuarioDestinatario.getId());
        final List<ItemPedidoDicaView> itens = this.searchRepository.consultarPorNamedNativeQuery("PedidoDica.consultarPedidosPendentes", params,
                ItemPedidoDicaView.class);
        final List<PedidosDicaView> pedidos = new ArrayList<PedidosDicaView>();
        for (final ItemPedidoDicaView item : itens) {
            PedidosDicaView pedidosDicaView = (PedidosDicaView) CollectionUtils.find(pedidos,
                    new PedidosDicaViewPredicado("idUsuarioPediu", item.getIdUsuarioPediu()));
            if (pedidosDicaView == null) {
                pedidosDicaView = new PedidosDicaView(item.getIdUsuarioPediu(), item.getNomeUsuarioPediu(), item.getFotoUsuarioPediu());
                pedidos.add(pedidosDicaView);
            }
            pedidosDicaView.adicionaItem(item);
        }
        return pedidos;
    }

    public List<PedidoDica> consultarPedidosPendentesFacebook(final String idFacebookUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFacebookUsuario", idFacebookUsuario);
        final List<PedidoDica> pedidos = this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarPedidosPendentesFacebook", parametros,
                PedidoDica.class);
        return pedidos;
    }

    public List<PedidoDica> consultarPedidosPorFacebookRequestIds(final String... requestIds) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("requestIds", requestIds);
        final List<PedidoDica> pedidos = this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarPedidosPendentesFacebook", parametros,
                PedidoDica.class);
        return pedidos;
    }

    public List<PedidoDica> consultarPedidosRespondidos(final Usuario autorPedido) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("autorPedido", autorPedido);
        return this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarPedidosRespondidos", params, PedidoDica.class);
    }

    public Long consultarPorUsuarioPediuUrlPathLocal(final Long idUsuarioPediu, final Long idUsuarioDestinatario, final String urlPathLocal) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("idUsuarioPediu", idUsuarioPediu);
        params.put("idUsuarioDestinatario", idUsuarioDestinatario);
        params.put("urlPathLocal", urlPathLocal);
        return ((BigInteger) this.searchRepository.queryByNamedNativeQueryUniqueResult("PedidoDica.consultarPorUsuarioPediuUrlPathLocal", params))
                .longValue();
    }

    public List<PedidoDica> consultarTodosPedidosDica(final Usuario praQuemPediu) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("praQuemPediu", praQuemPediu);
        final List<PedidoDica> pedidos = this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarTodosPedidosDica", parametros,
                PedidoDica.class);
        return pedidos;
    }

    public List<PedidoDica> consultarTodosPedidosPorViagem(final Viagem viagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("viagem", viagem);
        final List<PedidoDica> pedidos = this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarTodosPedidosPorViagem", parametros,
                PedidoDica.class);
        return pedidos;
    }

    @Transactional
    public void excluirPedidos(final Viagem viagem) {
        this.repository.excluirPedidos(viagem);
    }

    public boolean existePedidoDica(final Viagem viagem, final Usuario destinatario) {
        final List<PedidoDica> pedidosPorViagem = consultarTodosPedidosPorViagem(viagem);
        for (final PedidoDica pedido : pedidosPorViagem) {
            if (pedido.getDestinatario().equals(destinatario)) {
                return true;
            }
        }
        return false;
    }

    @Transactional
    public void finalizar(final Long pedido) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("pedido", pedido);
        params.put("dataResposta", new Date());
        this.genericCRUDRepository.alterarPorNamedNativeQuery("PedidoDica.finalizar", params);
    }

    @Transactional
    public PedidoDica pedirDica(final Usuario quePediu, final Usuario praQuemPedir, final Local localDica, final String mensagem) {
        final PedidoDica pedido = new PedidoDica(quePediu, praQuemPedir, localDica, mensagem);
        incluir(pedido);
        return pedido;
    }

    /*
     * Rodar na produção
    ALTER TABLE convite DROP CONSTRAINT fk38b82ce6e088f3fd;

    ALTER TABLE convite
      ADD CONSTRAINT fk38b82ce6e088f3fd FOREIGN KEY (id_viagem)
          REFERENCES viagem (id_viagem) MATCH SIMPLE
          ON UPDATE NO ACTION ON DELETE NO ACTION;
     */
    @Transactional
    public void pedirDicasDeViagemParaAmigosPorEmails(final Viagem viagem, final String enderecosEmailPorVirgula) {
        if (enderecosEmailPorVirgula != null && !enderecosEmailPorVirgula.isEmpty()) {
            final String[] emails = enderecosEmailPorVirgula.split("[\\s,;\\n\\t]+");
            for (final String email : emails) {

                PedidoDica pedido = null;
                Usuario usuario = null;

                final String assunto = " gostaria de suas dicas e sugestões para a montar o Roteiro de Viagem \"" + viagem.getTitulo() + "\" dele.";

                // Verificar se o email informado já é usuario do TripFans
                usuario = this.usuarioService.consultarUsuarioPorUsername(email);
                if (usuario != null) {
                    pedido = new PedidoDica(getUsuarioAutenticado(), usuario, viagem, null, assunto);
                    this.incluir(pedido);
                } else {
                    final Convite convite = new Convite(getUsuarioAutenticado(), email).paraRecomendacoesViagem(viagem);
                    if (this.conviteService.incluirConvite(convite)) {

                        final Map<String, Object> parametros = new HashMap<String, Object>();
                        try {
                            parametros.put("nomeUsuarioConvidante", StringEscapeUtils.escapeHtml(getUsuarioAutenticado().getDisplayName()));
                            parametros.put("idUsuarioConvidante",
                                    URLEncoder.encode(CriptografiaUtil.encrypt(getUsuarioAutenticado().getId() + ""), "UTF-8"));
                            parametros.put("idConvite", convite.getId().toString());
                            parametros.put("emailConvidadoCriptografado", URLEncoder.encode(CriptografiaUtil.encrypt(email), "UTF-8"));

                            parametros.put("textoAcao", assunto);
                            parametros.put("hideFooter", true);
                            parametros.put("textoLink", "Clique aqui para ajudá-lo");

                            parametros.put("autor", getUsuarioAutenticado());
                            // parametros.put("destinatario", acaoUsuario.getDestinatario());

                            parametros.put("url", "/viagem/" + viagem.getId() + "/ajudar/sugerirLocais");

                        } catch (final Exception e) {
                        }
                        this.emailService.enviarEmail(email, getUsuarioAutenticado().getDisplayName() + assunto, EmailTemplate.EMAIL_NOTIFICACAO,
                                parametros);
                    }
                }
            }
        }
    }

    @Transactional
    public void recomendarLocal(final Long idPedido, final Long idLocalRecomendado, final Long idUsuarioRecomendou) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("idPedidoDica", idPedido);
        params.put("idLocalRecomendado", idLocalRecomendado);
        params.put("idUsuarioRecomendou", idUsuarioRecomendou);
        this.genericCRUDRepository.alterarPorNamedNativeQuery("PedidoDica.recomendarLocal", params);
    }

    @Transactional
    public Dica responderPedido(final PedidoDica pedido, final String textoDica, final Usuario usuarioRespondeu) {
        final Dica dica = new Dica();
        dica.setDescricao(textoDica);
        dica.setLocalDica(pedido.getLocalDica());
        dica.setPedidoDica(pedido);
        this.dicaService.incluir(dica);
        // pedido.setRespondido(true); // já responder?
        // alterar(pedido);
        return dica;
    }

    public RespostaPedidoDicaView respostaPedidoDica(final PedidoDica pedido) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("pedido", pedido);
        final List<Dica> dicas = this.searchRepository.consultarPorNamedQuery("PedidoDica.consultarDicasPorPedidoDica", params, Dica.class);
        final List<RecomendacaoLocalPedidoDica> recomendacoes = this.searchRepository.consultarPorNamedQuery(
                "PedidoDica.consultarRecomendacoesPorPedidoDica", params, RecomendacaoLocalPedidoDica.class);
        return new RespostaPedidoDicaView(dicas, recomendacoes);
    }
}
