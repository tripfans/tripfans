package br.com.fanaticosporviagens.dica.model.repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Repository
public class DicaRepository extends GenericCRUDRepository<Dica, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public void associarTags(final Long idDica, final List<Long> tags) {
        final String insert = "INSERT INTO classificacao_dica (id_dica, id_item_classificacao_dica) VALUES (?, ?)";
        this.jdbcTemplate.batchUpdate(insert, new BatchPreparedStatementSetter() {
            @Override
            public int getBatchSize() {
                return tags.size();
            }

            @Override
            public void setValues(final PreparedStatement ps, final int index) throws SQLException {
                final Long tag = tags.get(index);
                ps.setLong(1, idDica);
                ps.setLong(2, tag);
            }
        });
    }

    public List<Dica> consultarDicasMaisRecentes(final Local local, final int tamanhoAmostra) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select dica ");
        hql.append("  from Dica dica ");
        if (local != null) {
            if (local.isTipoLocalGeografico()) {
                hql.append(" where (   dica.localDica.id = :local ");
                hql.append("        or dica.localDica.localizacao.continente.id = :local ");
                hql.append("        or dica.localDica.localizacao.pais.id = :local ");
                hql.append("        or dica.localDica.localizacao.estado.id = :local ");
                hql.append("        or dica.localDica.localizacao.cidade.id = :local ");
                hql.append("        or dica.localDica.localizacao.localInteresseTuristico.id = :local ");
                hql.append("       ) ");
            } else {
                hql.append(" where dica.localDica.id = :local");
            }
        }
        hql.append(" and dica.dataExclusao is null ");
        hql.append(" and dica.dataPublicacao is not null ");
        hql.append(" order by dica.dataCriacao desc, dica.quantidadeVotoUtil desc  ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local.getId());

        if (tamanhoAmostra >= 1) {
            return this.searchRepository.consultaHQL(hql.toString(), parametros, 0, tamanhoAmostra);
        }
        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Dica> consultarDicasPorUrlLocal(final Local local) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select dica from Dica dica ");

        if (local.isTipoLocalGeografico()) {
            hql.append(" where (   dica.localDica.id = :local ");
            hql.append("        or dica.localDica.localizacao.continente.id = :local ");
            hql.append("        or dica.localDica.localizacao.pais.id = :local ");
            hql.append("        or dica.localDica.localizacao.estado.id = :local ");
            hql.append("        or dica.localDica.localizacao.cidade.id = :local ");
            hql.append("        or dica.localDica.localizacao.localInteresseTuristico.id = :local ");
            hql.append("       ) ");
        } else {
            hql.append(" where dica.localDica.id = :local");
        }

        hql.append(" and dica.dataExclusao is null ");
        hql.append(" and dica.dataPublicacao is not null ");
        hql.append(" order by dica.quantidadeVotoUtil desc, dica.dataCriacao desc ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local.getId());

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Dica> consultarDicasPorUsuario(final Usuario usuario) {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT dica FROM Dica dica ");
        hql.append(" WHERE dica.autor = :usuario");
        hql.append("   AND dica.dataExclusao is null ");
        hql.append(" AND dica.dataPublicacao is not null ");
        hql.append(" ORDER BY dica.dataCriacao DESC, dica.quantidadeVotoUtil DESC ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

}
