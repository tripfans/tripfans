package br.com.fanaticosporviagens.dica.pedidodica.controller;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.model.repository.SortDirection;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.ParametrosPesquisaTextual;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;

@Controller
@RequestMapping("/dicas/pedidoDica")
public class PedidoDicaController extends BaseController {

    @Autowired
    private DicaService dicaService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private PesquisaTextualService pesquisaTextualService;

    @RequestMapping(value = "/enviarRequisicaoAplicativo", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage enviarRequisicaoAplicativo() {
        this.facebookService.enviarRequisicaoAplicativo("100003941183003", "Message");
        return success("Solicitacao de APP enviada com sucesso");
    }

    @RequestMapping(value = "/finalizar/{pedido}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String finalizar(@PathVariable("pedido") final Long pedido, final Model model) {
        this.pedidoDicaService.finalizar(pedido);
        success("Pedido respondido com sucesso");
        return "redirect:/dicas/pedidoDica/listagemPedidosDicaAmigos";
    }

    private List<Long> formatTagsList(final String[] tags) {
        final String[] preList = (String[]) ArrayUtils.subarray(tags, 0, tags.length);
        final List<Long> tagsList = new ArrayList<Long>();
        for (final String tag : preList) {
            try {
                tagsList.add(Long.parseLong(tag));
            } catch (final NumberFormatException e) {
                continue;
            }
        }
        return tagsList;
    }

    @RequestMapping(value = "/formPedirDica/{local}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String formPedirDica(@PathVariable final Local local, @RequestParam(required = true) final Usuario usuario, final Model model) {
        model.addAttribute("local", local);
        model.addAttribute("usuario", usuario);
        return "/dicas/pedidoDica/formPedirDica";
    }

    @RequestMapping(value = "/formRespostaPedidoDica/{pedido}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String formRespostaPedidoDica(@PathVariable final PedidoDica pedido, final Model model) {
        model.addAttribute("pedido", pedido);
        model.addAttribute("itensClassificacao", this.dicaService.consultarItensClassificacaoDica());

        final ParametrosPesquisaTextual parametrosPesquisaTextual = new ParametrosPesquisaTextual();
        final Local local = pedido.getLocalDica();
        Local localPai = null;
        if (local.isTipoLocalEnderecavel()) {
            localPai = local.getPai();
        } else {
            localPai = local;
        }
        if (localPai.isTipoCidade()) {
            parametrosPesquisaTextual.addParametro(EntidadeIndexada.FIELD_ID_CIDADE, localPai.getId());
        } else if (local.isTipoLocalInteresseTuristico()) {
            parametrosPesquisaTextual.addParametro("idLocalTuristico", localPai.getId());
        }
        parametrosPesquisaTextual.addParametro("-id", EntidadeIndexada.INDEX_LOCAL_PREFIX + local.getId());
        parametrosPesquisaTextual.addParametro("usuariosJaForam", getUsuarioAutenticado().getId());

        getSearchData().setStart(0);
        getSearchData().setLimit(30);

        parametrosPesquisaTextual.addOrdenacao("tripfansRanking", SortDirection.DESC);

        SolrResponseTripFans respostaSolr = this.pesquisaTextualService.consultar(parametrosPesquisaTextual);

        final Set<EntidadeIndexada> entidades = new LinkedHashSet<EntidadeIndexada>(respostaSolr.getResultados());

        // consulta os lugares que o usuário não foi
        if (CollectionUtils.isEmpty(respostaSolr.getResultados()) || respostaSolr.getResultados().size() < 30) {
            parametrosPesquisaTextual.getParametros().remove("usuariosJaForam");
            getSearchData().setStart(0);
            getSearchData().setLimit(30 - respostaSolr.getResultados().size());
            respostaSolr = this.pesquisaTextualService.consultar(parametrosPesquisaTextual);

            entidades.addAll(respostaSolr.getResultados());
        }

        model.addAttribute("locaisRecomendacao", new ArrayList<EntidadeIndexada>(entidades));

        return "/dicas/pedidoDica/formRespostaPedidoDica";
    }

    @RequestMapping(value = "/listagemPedidosDicaAmigos", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String listarPedidosDicaAmigos(final Model model) {
        model.addAttribute("pedidos", this.pedidoDicaService.consultarPedidosPendentes(getUsuarioAutenticado()));
        return "/dicas/pedidoDica/listagemPedidosDicaAmigos";

    }

    @RequestMapping(value = "/listarRespostasMeusPedidosDica", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String listarRespostasMeusPedidosDica(final Model model) {
        getSearchData().setEnableCount(false);
        model.addAttribute("pedidosRespondidos", this.pedidoDicaService.consultarPedidosRespondidos(getUsuarioAutenticado()));
        return "/dicas/pedidoDica/listarRespostaPedidosDica";

    }

    @RequestMapping(value = "/pedirDica", method = { RequestMethod.POST })
    public @ResponseBody @PreAuthorize("isAuthenticated()") StatusMessage pedirDica(@RequestParam(required = true) final Usuario quemPedir,
            @RequestParam(required = true) final Local localDica, @RequestParam(required = false) final String mensagem,
            @RequestParam(required = false) final String facebookPostId, final Model model) {
        this.pedidoDicaService.pedirDica(getUsuarioAutenticado(), quemPedir, localDica, mensagem);
        return success("Seu pedido de dica foi enviado com sucesso.");
    }

    @RequestMapping(value = "/recomendarLocal", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage recomendarLocal(@RequestParam(required = true) final Long idPedido,
            @RequestParam(required = true) final Long idLocal, final Model model) {
        this.pedidoDicaService.recomendarLocal(idPedido, idLocal, getUsuarioAutenticado().getId());
        return success("Recomendação enviada com sucesso");
    }

    @RequestMapping(value = "/responderPedidoDica", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage responderPedidoDica(@RequestParam(value = "idPedido", required = true) final PedidoDica pedido,
            @RequestParam(required = true) final String textoDica, @RequestParam(value = "as_values_tags", required = false) final String[] tags,
            final Model model) {
        final Dica dica = this.pedidoDicaService.responderPedido(pedido, textoDica, getUsuarioAutenticado());
        if (!ArrayUtils.isEmpty(tags) && tags.length != 1) {
            this.dicaService.associarTags(dica, formatTagsList(tags));
        }
        return success("Dica enviada com sucesso");
    }

    @RequestMapping(value = "/verResposta/{pedido}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String verResposta(@PathVariable final PedidoDica pedido, final Model model) {
        model.addAttribute("nomeUsuarioRespondeu", pedido.getPraQuemPedir().getDisplayName());
        model.addAttribute("nomeLocal", pedido.getLocalDica().getNome());
        model.addAttribute("resposta", this.pedidoDicaService.respostaPedidoDica(pedido));
        return "/dicas/pedidoDica/verRespostaPedidoDica";
    }

}
