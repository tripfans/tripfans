package br.com.fanaticosporviagens.dica.pedidodica;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

public class PedidosDicaView {

    private final Long idUsuarioPediu;

    private List<ItemPedidoDicaView> itens;

    private final String nomeUsuarioPediu;

    private final String urlFotoUsuarioPediu;

    public PedidosDicaView(final Long idUsuarioPediu, final String nomeUsuarioPediu, final String urlFotoUsuarioPediu) {
        super();
        this.idUsuarioPediu = idUsuarioPediu;
        this.nomeUsuarioPediu = nomeUsuarioPediu;
        this.urlFotoUsuarioPediu = urlFotoUsuarioPediu;
    }

    public void adicionaItem(final ItemPedidoDicaView item) {
        if (CollectionUtils.isEmpty(this.itens)) {
            this.itens = new ArrayList<ItemPedidoDicaView>();
        }
        this.itens.add(item);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final PedidosDicaView other = (PedidosDicaView) obj;
        if (this.idUsuarioPediu == null) {
            if (other.idUsuarioPediu != null)
                return false;
        } else if (!this.idUsuarioPediu.equals(other.idUsuarioPediu))
            return false;
        return true;
    }

    public Long getIdUsuarioPediu() {
        return this.idUsuarioPediu;
    }

    public List<ItemPedidoDicaView> getItens() {
        return this.itens;
    }

    public String getNomeUsuarioPediu() {
        return this.nomeUsuarioPediu;
    }

    public String getUrlFotoUsuarioPediu() {
        return this.urlFotoUsuarioPediu;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.idUsuarioPediu == null) ? 0 : this.idUsuarioPediu.hashCode());
        return result;
    }
}
