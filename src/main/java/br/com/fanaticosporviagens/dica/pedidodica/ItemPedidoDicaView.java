package br.com.fanaticosporviagens.dica.pedidodica;

import java.util.Date;

public class ItemPedidoDicaView {

    private Date dataPedido;

    private String fotoUsuarioPediu;

    private Long idPedido;

    private Long idUsuarioDestinatario;

    private Long idUsuarioPediu;

    private String mensagemPedido;

    private String nomeLocalPedido;

    private String nomeUsuarioPediu;

    private boolean respondido;

    private String urlPathLocalPedido;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ItemPedidoDicaView other = (ItemPedidoDicaView) obj;
        if (this.idPedido == null) {
            if (other.idPedido != null)
                return false;
        } else if (!this.idPedido.equals(other.idPedido))
            return false;
        return true;
    }

    public Date getDataPedido() {
        return this.dataPedido;
    }

    public String getFotoUsuarioPediu() {
        return this.fotoUsuarioPediu;
    }

    public Long getIdPedido() {
        return this.idPedido;
    }

    public Long getIdUsuarioDestinatario() {
        return this.idUsuarioDestinatario;
    }

    public Long getIdUsuarioPediu() {
        return this.idUsuarioPediu;
    }

    public String getMensagemPedido() {
        return this.mensagemPedido;
    }

    public String getNomeLocalPedido() {
        return this.nomeLocalPedido;
    }

    public String getNomeUsuarioPediu() {
        return this.nomeUsuarioPediu;
    }

    public String getUrlPathLocalPedido() {
        return this.urlPathLocalPedido;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.idPedido == null) ? 0 : this.idPedido.hashCode());
        return result;
    }

    public boolean isRespondido() {
        return this.respondido;
    }

    public void setDataPedido(final Date dataPedido) {
        this.dataPedido = dataPedido;
    }

    public void setFotoUsuarioPediu(final String fotoUsuarioPediu) {
        this.fotoUsuarioPediu = fotoUsuarioPediu;
    }

    public void setIdPedido(final Long idPedido) {
        this.idPedido = idPedido;
    }

    public void setIdUsuarioDestinatario(final Long idUsuarioDestinatario) {
        this.idUsuarioDestinatario = idUsuarioDestinatario;
    }

    public void setIdUsuarioPediu(final Long idUsuarioPediu) {
        this.idUsuarioPediu = idUsuarioPediu;
    }

    public void setMensagemPedido(final String mensagemPedido) {
        this.mensagemPedido = mensagemPedido;
    }

    public void setNomeLocalPedido(final String nomeLocalPedido) {
        this.nomeLocalPedido = nomeLocalPedido;
    }

    public void setNomeUsuarioPediu(final String nomeUsuarioPediu) {
        this.nomeUsuarioPediu = nomeUsuarioPediu;
    }

    public void setRespondido(final boolean respondido) {
        this.respondido = respondido;
    }

    public void setUrlPathLocalPedido(final String urlPathLocalPedido) {
        this.urlPathLocalPedido = urlPathLocalPedido;
    }

}
