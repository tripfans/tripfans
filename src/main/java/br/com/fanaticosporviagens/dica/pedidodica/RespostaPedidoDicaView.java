package br.com.fanaticosporviagens.dica.pedidodica;

import java.util.List;

import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.RecomendacaoLocalPedidoDica;

public class RespostaPedidoDicaView {

    private final List<Dica> dicas;

    private final List<RecomendacaoLocalPedidoDica> recomendacoesLocal;

    public RespostaPedidoDicaView(final List<Dica> dicas, final List<RecomendacaoLocalPedidoDica> recomendacoesLocal) {
        this.dicas = dicas;
        this.recomendacoesLocal = recomendacoesLocal;
    }

    public List<Dica> getDicas() {
        return this.dicas;
    }

    public List<RecomendacaoLocalPedidoDica> getRecomendacoesLocal() {
        return this.recomendacoesLocal;
    }

}
