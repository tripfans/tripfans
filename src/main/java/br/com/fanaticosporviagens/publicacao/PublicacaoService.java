package br.com.fanaticosporviagens.publicacao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.noticia.NoticiaService;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;

@Service
public class PublicacaoService extends BaseService {

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private NoticiaService noticiaService;

    @Autowired
    private NotificacaoService notificacaoService;

    /*@Transactional
    public void publicar(final Amizade amizadeAceita) {
        this.notificacaoService.notificar(amizadeAceita);
    }*/

    @Transactional
    public void publicar(final Avaliacao avaliacao) {
        this.noticiaService.publicarNoticia(avaliacao);
        this.interesseUsuarioEmLocalService.publicarInteresse(avaliacao);
        // TODO Gustavo Implementar log de ações do usuário para mostrar as ações que o usuário executou e pontuar
    }

    /*@Transactional
    public void publicar(final Convite convite) {
        // TODO criar um interceptor do Hibernate faça as publicações quando necessário
        if (convite.isAceito()) {
            this.notificacaoService.retirarPendencia(convite);
        } else {
            this.notificacaoService.notificar(convite);
        }
    }*/

    @Transactional
    public void publicar(final Dica dica) {
        this.noticiaService.publicarNoticia(dica);
        this.interesseUsuarioEmLocalService.publicarInteresse(dica);
        // TODO Gustavo Implementar log de ações do usuário para mostrar as ações que o usuário executou e pontuar
    }
}
