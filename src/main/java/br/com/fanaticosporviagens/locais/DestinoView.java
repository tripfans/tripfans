package br.com.fanaticosporviagens.locais;

import java.util.List;

import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.viagem.RoteiroViagemView;

public class DestinoView {

    public static final String SECAO_AM_LATINA = "América Latina";

    public static final String SECAO_BRASIL = "Brasil";

    public static final String SECAO_EUA_CANADA = "Estados Unidos da América";

    public static final String SECAO_EUROPA = "Europa";

    private List<LocalEnderecavelView> atracoesDestaques;

    private String descricao;

    private List<LocalEnderecavelView> hoteisDestaques;

    private Long id;

    private String nome;

    private String nomeContinente;

    private String nomePais;

    private String nomeSecao;

    private List<LocalEnderecavelView> restaurantesDestaques;

    private List<RoteiroViagemView> roteirosDestaques;

    private String siglaIsoPais;

    private String urlFotoAlbum;

    private String urlFotoBig;

    private String urlFotoSmall;

    private String urlPath;

    public void defineNomeSecao() {
        if (this.nomePais.equalsIgnoreCase("brasil")) {
            this.nomeSecao = SECAO_BRASIL;
        } else if (this.nomeContinente.equalsIgnoreCase("américa do sul")) {
            this.nomeSecao = SECAO_AM_LATINA;
        } else if (this.nomePais.equalsIgnoreCase("estados unidos da américa")) {
            this.nomeSecao = SECAO_EUA_CANADA;
        } else if (this.nomeContinente.equalsIgnoreCase("europa")) {
            this.nomeSecao = SECAO_EUROPA;
        }
    }

    public List<LocalEnderecavelView> getAtracoesDestaques() {
        return this.atracoesDestaques;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public List<LocalEnderecavelView> getHoteisDestaques() {
        return this.hoteisDestaques;
    }

    public Long getId() {
        return this.id;
    }

    public Local getLocal() {
        final Local cidade = new Cidade();
        cidade.setId(this.id);
        cidade.setNome(this.nome);
        cidade.setUrlFotoAlbum(this.urlFotoAlbum);
        cidade.setUrlFotoBig(this.urlFotoBig);
        cidade.setUrlFotoSmall(this.urlFotoSmall);
        cidade.setUrlPath(this.urlPath);
        return cidade;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeContinente() {
        return this.nomeContinente;
    }

    public String getNomePais() {
        return this.nomePais;
    }

    public String getNomeSecao() {
        return this.nomeSecao;
    }

    public List<LocalEnderecavelView> getRestaurantesDestaques() {
        return this.restaurantesDestaques;
    }

    public List<RoteiroViagemView> getRoteirosDestaques() {
        return this.roteirosDestaques;
    }

    public String getSiglaIsoPais() {
        return this.siglaIsoPais;
    }

    public String getUrlFotoAlbum() {
        return this.urlFotoAlbum;
    }

    public String getUrlFotoBig() {
        return this.urlFotoBig;
    }

    public String getUrlFotoSmall() {
        return this.urlFotoSmall;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setAtracoesDestaques(final List<LocalEnderecavelView> atracoesDestaques) {
        this.atracoesDestaques = atracoesDestaques;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setHoteisDestaques(final List<LocalEnderecavelView> hoteisDestaques) {
        this.hoteisDestaques = hoteisDestaques;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeContinente(final String nomeContinente) {
        this.nomeContinente = nomeContinente;
    }

    public void setNomePais(final String nomePais) {
        this.nomePais = nomePais;
    }

    public void setRestaurantesDestaques(final List<LocalEnderecavelView> restaurantesDestaques) {
        this.restaurantesDestaques = restaurantesDestaques;
    }

    public void setRoteirosDestaques(final List<RoteiroViagemView> roteirosDestaques) {
        this.roteirosDestaques = roteirosDestaques;
    }

    public void setSiglaIsoPais(final String siglaIsoPais) {
        this.siglaIsoPais = siglaIsoPais;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}
