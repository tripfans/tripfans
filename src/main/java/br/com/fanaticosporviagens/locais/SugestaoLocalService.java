package br.com.fanaticosporviagens.locais;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.SugestaoLocal;
import br.com.fanaticosporviagens.model.entity.Viagem;

/**
 *
 * @author carlosn
 *
 */
@Service
public class SugestaoLocalService extends GenericCRUDService<SugestaoLocal, Long> {

    @Autowired
    protected InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    protected LocalService localService;

    @Autowired
    protected PedidoDicaService pedidoDicaService;

    @Transactional
    public List<SugestaoLocal> consultarSugestoesAbertas(final Viagem viagem) {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT sugestaoLocal");
        hql.append("  FROM SugestaoLocal sugestaoLocal");
        hql.append(" WHERE sugestaoLocal.viagem.id = :idViagem ");
        hql.append("   AND sugestaoLocal.aceito = false");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", viagem.getId());

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    @Transactional
    public void enviarSugestoes(final PedidoDica pedido, final List<Long> idsLocaisSugeridos, final List<Long> idsLocaisMarcadosComoImperdiveis) {
        this.enviarSugestoes(pedido.getViagem(), pedido, idsLocaisSugeridos, idsLocaisMarcadosComoImperdiveis);
    }

    @Transactional
    public void enviarSugestoes(final Viagem viagem, final List<Long> idsLocaisSugeridos, final List<Long> idsLocaisMarcadosComoImperdiveis) {
        this.enviarSugestoes(viagem, null, idsLocaisSugeridos, idsLocaisMarcadosComoImperdiveis);
    }

    @Transactional
    private void enviarSugestoes(final Viagem viagem, final PedidoDica pedido, final List<Long> idsLocaisSugeridos,
            final List<Long> idsLocaisMarcadosComoImperdiveis) {
        SugestaoLocal sugestaoLocal = null;
        Local local = null;
        boolean imperdivel = false;
        for (final Long idLocal : idsLocaisSugeridos) {
            local = this.localService.consultarPorId(idLocal);
            sugestaoLocal = new SugestaoLocal();
            sugestaoLocal.setAutor(getUsuarioAutenticado());
            sugestaoLocal.setDataCriacao(new LocalDateTime());
            sugestaoLocal.setLocal(local);
            sugestaoLocal.setPedidoDica(pedido);
            sugestaoLocal.setAceito(false);
            sugestaoLocal.setViagem(viagem);
            if (!idsLocaisMarcadosComoImperdiveis.isEmpty() && idsLocaisMarcadosComoImperdiveis.contains(idLocal)) {
                imperdivel = true;
            } else {
                imperdivel = false;
            }
            sugestaoLocal.setImperdivel(imperdivel);
            this.incluir(sugestaoLocal);
            if (imperdivel) {
                this.interesseUsuarioEmLocalService.registrarInteresseImperdivel(local, getUsuarioAutenticado());
            } else {
                this.interesseUsuarioEmLocalService.registrarInteresseIndica(local, getUsuarioAutenticado());
            }
        }
        if (pedido != null) {
            this.pedidoDicaService.finalizar(pedido.getId());
        }
    }
}
