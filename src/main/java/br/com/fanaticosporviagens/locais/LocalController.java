package br.com.fanaticosporviagens.locais;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.foursquare.api.Photo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.acaousuario.AcaoUsuarioService;
import br.com.fanaticosporviagens.avaliacao.AvaliacaoService;
import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.model.repository.FacetField;
import br.com.fanaticosporviagens.infra.model.repository.SortDirection;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.infra.util.HibernateUtil;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;
import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalAlbum;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.Restaurante;
import br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.TipoServicoHotel;
import br.com.fanaticosporviagens.noticia.NoticiaService;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.ParametrosPesquisaTextual;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;
import br.com.fanaticosporviagens.viagem.ViagemService;

/**
 * @author André Thiago (andrethiagos@gmail.com)
 *
 */
@Controller
@RequestMapping(value = { UrlLocalResolver.LOCAL_URL_MAPPING, UrlLocalResolver.LOCAL_URL_MAPPING_OLD })
public class LocalController extends BaseController {

    private static final int TAMANHO_AMOSTRA_INTERESSE_AMIGOS = 7;

    private static final int TAMANHO_AMOSTRA_PONTUACAO_USUARIOS = 5;

    private static final int TAMANHO_AMOSTRA_ULTIMAS_AVALIACOES = 3;

    private static final int TAMANHO_AMOSTRA_ULTIMAS_DICAS = 3;

    private static final int TAMANHO_AMOSTRA_ULTIMAS_PERGUNTAS = 3;

    @Autowired
    private AcaoUsuarioService acaoUsuarioService;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private DicaService dicaService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private LocalService localService;

    @Autowired
    private NoticiaService noticiaService;

    @Autowired
    private PesquisaTextualService pesquisaTextualService;

    @Autowired
    private ViagemService viagemService;

    @RequestMapping(value = "/atividadesAmigos/{local}", method = { RequestMethod.GET })
    public String atividadesAmigos(@PathVariable final Local local, final Model model) {
        model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesAmigosEmLocal(getUsuarioAutenticado(), local, null));
        return "/locais/atividadesAmigos";
    }

    @RequestMapping(value = "/consultarAeroportos", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarAeroportos(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarAeroportosPorNome(nome), this.jsonFields);
    }

    @RequestMapping(value = "/consultarAtracoes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarAtracoes(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarAtracoesPorNome(nome), this.jsonFields);
    }

    @RequestMapping(value = "/cidade/consultarCidades", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarCidades(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarCidadesPorNome(nome, null, null), this.jsonFields);
    }

    @RequestMapping(value = "/consultarLocadorasVeiculos", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarLocadorasVeiculos(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarLocadorasVeiculosPorNome(nome), this.jsonFields);
    }

    @RequestMapping(value = "/consultarRegioes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarRegioes(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarRegioesPorNome(nome), this.jsonFields);
    }

    @RequestMapping(value = "/consultarRegioesPorIds", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarRegioesPorIds(@RequestParam(required = false) final Long[] ids) {
        return new JSONResponse(this.localService.consultarRegioesPorIds(ids), this.jsonFields);
    }

    @RequestMapping(value = "/consultarRestaurantes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarRestaurantes(@RequestParam("term") final String nome) {
        return new JSONResponse(this.localService.consultarRestaurantesPorNome(nome), this.jsonFields);
    }

    @RequestMapping(value = "/desmarcarComoFavorito/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage desmarcarComoFavorito(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.desmarcarInteresseLocalFavorito(local, this.getUsuarioAutenticado());
        return success(String.format("Você desmarcou  %s como um local favorito.", local.getNome()));
    }

    @RequestMapping(value = "/desmarcarDesejaIr/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage desmarcarDesejaIr(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.desmarcarInteresseDesejaIr(local, this.getUsuarioAutenticado());
        return success(String.format("Você desmarcou que deseja ir a %s.", local.getNome()));
    }

    @RequestMapping(value = "/desmarcarJaFoi/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage desmarcarJaFoi(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.desmarcarInteresseJaFoi(local, this.getUsuarioAutenticado());
        return success(String.format("Você desmarcou que foi a %s.", local.getNome()));
    }

    @RequestMapping(value = "/desmarcarSeguir/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage desmarcarSeguir(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.desmarcarInteresseSeguir(local, this.getUsuarioAutenticado());
        return success(String.format("Você deixou de seguir %s.", local.getNome()));
    }

    @RequestMapping(value = "/{local}/{pagina}")
    public String exibirPagina(@PathVariable final Local local, @PathVariable final String pagina, final Model model) {
        if (pagina != null) {
            if (AjaxUtils.isAjaxRequest(this.request)) {
                return "/locais/" + pagina;
            }
            model.addAttribute("pagina", pagina);
        }
        return local(local, model);
    }

    @RequestMapping(value = "/fotos/{local}", method = { RequestMethod.GET })
    public String fotos(@PathVariable final Local local, final Model model) {
        if (local.getQuantidadeFotos() >= 1) {
            final LocalAlbum album = this.fotoService.consultarFotosParaAlbum(local);
            model.addAttribute("albumFotoLocal", album);
        } else {
            model.addAttribute("albumFotoLocal", null);
        }
        return "/locais/fotos";
    }

    public String fotosAntigo(@PathVariable final Local local, final Model model) {
        List<Foto> fotos = Collections.emptyList();
        if (local.getQuantidadeFotos() >= 1) {
            fotos = this.fotoService.consultarFotosLocal(local);
        }
        model.addAttribute("fotos", fotos);
        model.addAttribute("quantidadeFotos", fotos.size());
        return "/locais/fotos";
    }

    @RequestMapping(value = "/4sq/fotos/{local}", method = { RequestMethod.GET })
    public String fotosFoursquare(@PathVariable final Local local, final Model model) {
        final List<Photo> fotosFoursquare = this.fotoService.consultarFotosFoursquare(local);
        model.addAttribute("fotosFoursquare", fotosFoursquare);
        return "/locais/listaFotosFoursquare";
    }

    @RequestMapping(value = "/fotos/panoramio/{local}", method = { RequestMethod.GET })
    public String fotosPanoramio(@PathVariable final Local local, @RequestParam(required = false) final Integer position, final Model model) {
        model.addAttribute("position", position);
        return "/locais/modalFotosPanoramio";
    }

    @RequestMapping(value = "/interessesAmigos/{local}", method = { RequestMethod.GET })
    public String interessesAmigos(@PathVariable final Local local, final Model model) {

        final List<InteresseAmigosEmLocal> interessesAmigos = this.interesseUsuarioEmLocalService.consultarTodosInteressesAmigosEmLocal(
                this.getUsuarioAutenticado(), local);
        model.addAttribute("interessesAmigos", interessesAmigos);

        return "/locais/interessesAmigos";
    }

    @RequestMapping(value = "/interesseTodosAmigos/{urlPathTipoInteresse}/{local}", method = { RequestMethod.GET })
    public String interresseTodosAmigos(@PathVariable final String urlPathTipoInteresse, @PathVariable final Local local, final Model model) {
        model.addAttribute("local", local);

        final TipoInteresseUsuarioEmLocal tipoInteresse = TipoInteresseUsuarioEmLocal.fromUrlPath(urlPathTipoInteresse);
        final InteresseAmigosEmLocal interesseTodosAmigos = this.interesseUsuarioEmLocalService.consultarInteresseTodosAmigosEmLocal(
                this.getUsuarioAutenticado(), local, tipoInteresse);
        model.addAttribute("interesseAmigo", interesseTodosAmigos);
        return "/locais/localInteresseTodosAmigos";
    }

    @RequestMapping(value = "/{local}", method = { RequestMethod.GET })
    public String local(@PathVariable final Local local, final Model model) {
        if (isUsuarioAutenticado()) {
            model.addAttribute("planosViagem", this.viagemService.viagemConsultarProximasViagens(getUsuarioAutenticado()));
        }
        if (local.isTipoLocalGeografico()) {
            return this.localGeograficoIndex((LocalGeografico) local, model);
        } else {
            return this.localEnderecavelIndex((LocalEnderecavel) local, model);
        }
        // return "redirect:" + this.urlLocalResolver.getUrl(local);
    }

    @RequestMapping(value = { "/aeroporto/{local}", "/agencia/{local}", "/atracao/{local}", "/hotel/{local}", "/imovelAluguelTemporada/{local}",
            "/restaurante/{local}" }, method = { RequestMethod.GET })
    public String localEnderecavelIndex(@PathVariable final LocalEnderecavel local, final Model model) {
        this.localEnderecavelPopularInformacoes(local, model);
        return "/locais/localEnderecavelIndex";
    }

    @RequestMapping(value = "/localEnderecavel/informacoes/{local}", method = { RequestMethod.GET })
    public String localEnderecavelInformacoes(@PathVariable final LocalEnderecavel local, final Model model) {
        this.localEnderecavelPopularInformacoes(local, model);
        return "/locais/localEnderecavelInformacoes";
    }

    private void localEnderecavelPopularInformacoes(final Local local, final Model model) {
        model.addAttribute("local", local);

        final boolean deveMostrarSobre = local.getQuantidadeAvaliacoes() > 0 || local.getQuantidadeDicas() > 0 || local.getQuantidadeFotos() > 0
                || local.getQuantidadePerguntas() > 0 || local.isPossuiAlgumInteresse() || local.getEndereco() != null
                || !((LocalEnderecavel) local).getClassificacoes().isEmpty();

        model.addAttribute("deveMostrarSobre", deveMostrarSobre);

        if (local.getQuantidadeAvaliacoes() >= 1) {
            model.addAttribute("amostraAvaliacoes", this.avaliacaoService.consultarAvaliacoesMaisRecentes(local, TAMANHO_AMOSTRA_ULTIMAS_AVALIACOES));
        }

        if (local.getQuantidadeDicas() >= 1) {
            model.addAttribute("amostraDicas", this.dicaService.consultarDicasMaisRecentes(local, TAMANHO_AMOSTRA_ULTIMAS_DICAS));
        }

        if (local.getQuantidadeFotos() > 0) {
            final List<Foto> fotos = this.fotoService.consultarUltimasFotosLocal(local);
            model.addAttribute("fotos", fotos);
            if (fotos != null) {
                model.addAttribute("quantidadeFotos", fotos.size());
            }
        }

        model.addAttribute("amostraPontuacaoUsuario",
                this.acaoUsuarioService.consultarPontuacaoUsuarioLocal(local, TAMANHO_AMOSTRA_PONTUACAO_USUARIOS));

        this.localPopularInteresseUsuario(local, model);
    }

    @RequestMapping(value = { "/continente/{local}", "/pais/{local}", "/estado/{local}", "/cidade/{local}" }, method = { RequestMethod.GET })
    public String localGeograficoIndex(@PathVariable final LocalGeografico local, final Model model) {
        this.localGeograficoPopularInformacoes(local, model);
        return "/locais/localGeograficoIndex";
    }

    @RequestMapping(value = "/localGeografico/informacoes/{local}", method = { RequestMethod.GET })
    public String localGeograficoInformacoes(@PathVariable final LocalGeografico local, final Model model) {
        this.localGeograficoPopularInformacoes(local, model);
        return "/locais/localGeograficoInformacoes";
    }

    private void localGeograficoPopularInformacoes(final LocalGeografico local, final Model model) {
        model.addAttribute("local", local);

        final boolean deveMostrarSobre = local.getQuantidadeAvaliacoes() > 0 || local.getQuantidadeDicas() > 0 || local.getQuantidadeFotos() > 0
                || local.getQuantidadePerguntas() > 0 || local.isPossuiAlgumInteresse();

        model.addAttribute("deveMostrarSobre", deveMostrarSobre);

        if (local.getQuantidadeAvaliacoes() >= 1) {
            model.addAttribute("avaliacoesConsolidadas", this.avaliacaoService.consultarAvaliacoesConsolidadasPorCriterio(local));
            model.addAttribute("amostraAvaliacoes", this.avaliacaoService.consultarAvaliacoesMaisRecentes(local, TAMANHO_AMOSTRA_ULTIMAS_AVALIACOES));
        }

        if (local.getQuantidadeDicas() >= 1) {
            model.addAttribute("amostraDicas", this.dicaService.consultarDicasMaisRecentes(local, TAMANHO_AMOSTRA_ULTIMAS_DICAS));
        }

        if (local.getQuantidadeFotos() > 0) {
            final List<Foto> fotos = this.fotoService.consultarUltimasFotosLocal(local);
            model.addAttribute("fotos", fotos);
            if (fotos != null) {
                model.addAttribute("quantidadeFotos", local.getQuantidadeFotos());
            }
        }

        this.localPopularInteresseUsuario(local, model);

        model.addAttribute("filhosMaisPopulares",
                HibernateUtil.getInstance().derreferenciarLista(this.localService.consultarFilhosMaisPopulares(local, 6)));
        model.addAttribute("melhoresAtracoes", HibernateUtil.getInstance().derreferenciarLista(this.localService.consultarMelhoresAtracoes(local, 6)));
        model.addAttribute("melhoresHoteis", HibernateUtil.getInstance().derreferenciarLista(this.localService.consultarMelhoresHoteis(local, 6)));
        model.addAttribute("melhoresRestaurantes",
                HibernateUtil.getInstance().derreferenciarLista(this.localService.consultarMelhoresRestaurantes(local, 6)));

        model.addAttribute("amostraPontuacaoUsuario",
                this.acaoUsuarioService.consultarPontuacaoUsuarioLocal(local, TAMANHO_AMOSTRA_PONTUACAO_USUARIOS));
    }

    @RequestMapping(value = "/todos/{urlPathTipoLocal}/{local}", method = { RequestMethod.GET, RequestMethod.POST })
    public String localGeograficoTodosLocaisEnderecaveis(@PathVariable final String urlPathTipoLocal, @PathVariable final LocalGeografico local,
            @RequestParam(required = false) final boolean filtro, @RequestParam(required = false) final String nome,
            @RequestParam(required = false) final Integer[] idsCategorias, @RequestParam(required = false) final Integer[] idsClasses,
            @RequestParam(required = false) final Integer[] estrelas, @RequestParam(required = false) final String bairro,
            @RequestParam(value = "sort", required = false) String ordem, @RequestParam(value = "dir", required = false) final String direcaoOrdem,
            final Model model) {
        model.addAttribute("local", local);
        final LocalType tipoLocal = LocalType.getLocalTypePorUrlPath(urlPathTipoLocal);
        model.addAttribute("tipoLocalListado", tipoLocal);

        //@formatter:off

        final List<Integer> tipos = new ArrayList<Integer>();
        tipos.add(tipoLocal.getCodigo());
        if (tipoLocal.equals(LocalType.ATRACAO)) {
            tipos.add(LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo());
        }

        final ParametrosPesquisaTextual parametrosPesquisaTextual =
                new ParametrosPesquisaTextual(nome)
        //.addParametro(EntidadeIndexada.FIELD_ID_CIDADE, local.getId())
        .addParametro(EntidadeIndexada.FIELD_TIPO_ENTIDADE, tipos);


        if(local.isTipoCidade()) {
            parametrosPesquisaTextual.addParametro(EntidadeIndexada.FIELD_ID_CIDADE, local.getId());
        } else if(local.isTipoLocalInteresseTuristico()) {
            parametrosPesquisaTextual.addParametro("idLocalTuristico", local.getId());
        }

        if (filtro) {
            parametrosPesquisaTextual
            //.addParametro(EntidadeIndexada.FIELD_IDS_CATEGORIAS, idsCategorias)
            //.addParametro(EntidadeIndexada.FIELD_IDS_CLASSES, idsClasses)
            .addParametro(EntidadeIndexada.FIELD_ESTRELAS, estrelas)
            .addParametro(EntidadeIndexada.FIELD_BAIRRO, bairro);
        } else {
            parametrosPesquisaTextual.addAgrupamento(
                    new FacetField(EntidadeIndexada.FIELD_ESTRELAS, 1, FacetField.SORT_TYPE_INDEX))
                    //.addAgrupamento(new FacetField(EntidadeIndexada.FIELD_IDS_CATEGORIAS, 1, FacetField.SORT_TYPE_INDEX))
                    //    .addAgrupamento(new FacetField(EntidadeIndexada.FIELD_IDS_CLASSES, 1, FacetField.SORT_TYPE_INDEX))
                    .addAgrupamento(new FacetField(EntidadeIndexada.FIELD_BAIRRO, 1, FacetField.SORT_TYPE_INDEX));
        }
        if (ordem == null) {
            ordem = "classificacaoGeral";
        }
        SortDirection direction = SortDirection.DESC;
        if (direcaoOrdem != null) {
            try {
                direction = SortDirection.valueOf(direcaoOrdem.toUpperCase());
            } catch (final Exception e) {
                // continua
            }
        }
        parametrosPesquisaTextual.addOrdenacao(ordem, direction);

        //@formatter:on
        if (!this.getSearchData().isPaginationDataValid()) {
            this.getSearchData().setStart(0);
            this.getSearchData().setLimit(20);
        }
        parametrosPesquisaTextual.setPaginacao(this.searchData.getStart(), this.searchData.getLimit());
        final SolrResponseTripFans respostaSolr = this.pesquisaTextualService.consultar(parametrosPesquisaTextual);
        final List<EntidadeIndexada> resultados = respostaSolr.getResultados();
        final List<org.apache.solr.client.solrj.response.FacetField> camposAgrupados = respostaSolr.getCamposAgrupados();

        if (tipoLocal.equals(LocalType.ATRACAO)) {
            parametrosPesquisaTextual.addParametro(EntidadeIndexada.FIELD_TIPO_ENTIDADE, LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo());
            /*respostaSolr = this.pesquisaTextualService.consultar(parametrosPesquisaTextual);
            if (CollectionUtils.isEmpty(resultados)) {
                resultados = respostaSolr.getResultados();
            } else {
                resultados.addAll(respostaSolr.getResultados());
            }
            if (CollectionUtils.isEmpty(camposAgrupados)) {
                camposAgrupados = respostaSolr.getCamposAgrupados();
            } else {
                camposAgrupados.addAll(respostaSolr.getCamposAgrupados());
            }*/
        }

        model.addAttribute("listaLocais", resultados);
        model.addAttribute("camposAgrupados", camposAgrupados);

        if (filtro || getSearchData().getStart() != 0) {
            return "/locais/localEnderecavelTodosPagina";
        } else {
            return "/locais/localEnderecavelTodos";
        }
    }

    @RequestMapping(value = "/noticias/{local}", method = { RequestMethod.GET })
    public String localNoticias(@PathVariable final Local local, final Model model) {
        model.addAttribute("noticias", this.noticiaService.consultarNoticias(local));
        return "/locais/noticias";
    }

    private void localPopularInteresseUsuario(final Local local, final Model model) {
        if (this.getUsuarioAutenticado() != null) {
            /*InteresseUsuarioEmLocal interesse = this.interesseUsuarioEmLocalService.consultarInteressesUsuarioEmLocal(this.getUsuarioAutenticado(),
                    local);
            if (interesse == null) {
                interesse = new InteresseUsuarioEmLocal(local, this.getUsuarioAutenticado(), null, null);
            }
            model.addAttribute("interesseUsuario", interesse);*/
            final TipoInteresseUsuarioEmLocal[] tipos = { TipoInteresseUsuarioEmLocal.JA_FOI, TipoInteresseUsuarioEmLocal.DESEJA_IR,
                    TipoInteresseUsuarioEmLocal.FAVORITO, TipoInteresseUsuarioEmLocal.SEGUIR };

            final List<InteresseAmigosEmLocal> interessesAmigos = this.interesseUsuarioEmLocalService.consultarInteressesAmigosEmLocal(
                    this.getUsuarioAutenticado(), local, tipos, TAMANHO_AMOSTRA_INTERESSE_AMIGOS);
            model.addAttribute("interessesAmigos", interessesAmigos);

            if (getUsuarioAutenticado() != null) { // TODO pedido dicas; mudar para fazer um SQL nativo
                final List<InteresseAmigosEmLocal> interesses = this.interesseUsuarioEmLocalService.consultarInteressesAmigosEmLocal(
                        getUsuarioAutenticado(), local, new TipoInteresseUsuarioEmLocal[] { TipoInteresseUsuarioEmLocal.JA_FOI }, 20);
                if (CollectionUtils.isNotEmpty(interesses)) {
                    final InteresseAmigosEmLocal amigosJaForam = interesses.get(0);
                    model.addAttribute("quantidadeAmigosJaForam", amigosJaForam.getQuantidadeAmigos());
                    model.addAttribute("interessesAmigosJaForam", amigosJaForam.sorteia(5));
                }
            }

            model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesAmigosEmLocal(getUsuarioAutenticado(), local, 5));

        }
    }

    @RequestMapping(value = "/marcarComoFavorito/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage marcarComoFavorito(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.registrarInteresseLocalFavorito(local, this.getUsuarioAutenticado());
        return success(String.format("Você marcou  %s como um local favorito.", local.getNome()));
    }

    @RequestMapping(value = "/marcarDesejaIr/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage marcarDesejaIr(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.registrarInteresseDesejaIr(local, this.getUsuarioAutenticado());
        return success(String.format("Você marcou que deseja ir a %s.", local.getNome()));
    }

    @RequestMapping(value = "/marcarJaFoi/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage marcarJaFoi(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.registrarInteresseJaFoi(local, this.getUsuarioAutenticado());
        return success(String.format("Você marcou que foi a %s.", local.getNome()));
    }

    @RequestMapping(value = "/marcarSeguir/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage marcarSeguir(@PathVariable final Local local, final Model model) {
        this.interesseUsuarioEmLocalService.registrarInteresseSeguir(local, this.getUsuarioAutenticado());
        return success(String.format("Você passou a seguir %s.", local.getNome()));
    }

    private void print(final LocalAlbum album) {
        System.out.println(album.getLocal().getNome());
        System.out.println("\t- " + album.getFotos().size() + " fotos");
        for (final LocalAlbum albumFilho : album.getAlbunsFilhos()) {
            print(albumFilho);
        }

    }

    @RequestMapping(value = "/recuperarServicosHotel/{idLocal}", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public Map<String, Object> recuperarServicosHotel(@PathVariable final Long idLocal) {
        final Hotel hotel = new Hotel();
        hotel.setId(idLocal);
        final List<TipoServicoHotel> servicosDisponiveis = this.localService.consultarServicosDisponiveis(hotel);
        final Map<String, Object> resposta = new HashMap<String, Object>();
        // TODO vamos ter que encontrar uma formalbum.getLocal().getNome()a de a enum ser serialziada corretamente de forma a evitar o loop a seguir
        final List<String> descricaoServicos = new ArrayList<String>();
        for (final TipoServicoHotel tipoServico : servicosDisponiveis) {
            descricaoServicos.add(tipoServico.getDescricao());
        }
        resposta.put("servicos", descricaoServicos);
        return resposta;
    }

    @RequestMapping(value = "/sugerirLocal", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String sugerirLocal(@RequestParam(required = false) final Cidade cidade,
            @RequestParam(value = "tipoLocal", required = false) final String urlPathTipoLocal,
            @RequestParam(value = "term", required = false) final String nomeLocal, @RequestParam(required = false) final String acao,
            final Model model) {
        model.addAttribute("cidade", cidade);
        if (urlPathTipoLocal != null) {
            model.addAttribute("tipoLocal", LocalType.getLocalTypePorUrlPath(urlPathTipoLocal));
        }
        model.addAttribute("nomeLocal", nomeLocal);
        model.addAttribute("acao", acao);
        return "/locais/sugerirLocal";
    }

    @RequestMapping(value = "/sugerirLocal/consultarLocaisPossiveis/{destino}/{urlPathTipoLocal}/{nomeLocal}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String sugerirLocalConsultarLocaisPossiveis(@PathVariable final LocalGeografico destino, @PathVariable final String urlPathTipoLocal,
            @PathVariable final String nomeLocal, @RequestParam(required = false) final String acao, final Model model) {
        final LocalType tipoLocal = LocalType.getLocalTypePorUrlPath(urlPathTipoLocal);
        final List<LocalEnderecavel> locais = this.localService.consultarLocaisEnderecaveis(tipoLocal, destino, nomeLocal, null);
        model.addAttribute("locais", locais);
        model.addAttribute("acao", acao);
        return "/locais/listaLocaisPossiveis";
    }

    @RequestMapping(value = "/sugerirLocal/escolher/{local}", method = { RequestMethod.GET })
    public String sugerirLocalEscolherDaLista(@PathVariable final LocalEnderecavel local, @RequestParam(required = false) final String acao,
            final Model model) {
        if (acao != null) {
            if (acao.equalsIgnoreCase("avaliar")) {
                return "redirect:/avaliacao/avaliar/" + local.getUrlPath();
            } else if (acao.equalsIgnoreCase("escreverDica")) {
                return "redirect:/dicas/escrever/" + local.getUrlPath();
            } else if (acao.equalsIgnoreCase("perguntar")) {
                return "redirect:/perguntas/perguntar?local=" + local.getUrlPath();
            }
        }
        return "redirect:" + local.getUrl();
    }

    @RequestMapping(value = "/sugerirLocal/incluir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String sugerirLocalIncluir(@RequestParam final LocalGeografico localPai, @RequestParam final String tipoLocal,
            @RequestParam("nome") final String nomeLocal, @RequestParam(required = false) final String endereco,
            @RequestParam(required = false) final String bairro, @RequestParam(required = false) final String informacoesAdicionais,
            @RequestParam(required = false) final String acao, final Model model) {
        final LocalType localType = LocalType.getLocalTypePorUrlPath(tipoLocal);
        LocalEnderecavel novoLocal = null;
        if (LocalType.ATRACAO.equals(localType)) {
            novoLocal = new Atracao();
        } else if (LocalType.HOTEL.equals(localType)) {
            novoLocal = new Hotel();
        } else {
            novoLocal = new Restaurante();
        }
        novoLocal.setLocalizacao(localPai.getLocalizacao());
        novoLocal.setNome(nomeLocal);
        novoLocal.setIdUsuarioSolcitante(getUsuarioAutenticado().getId());
        novoLocal.getEndereco(true).setDescricao(endereco);
        novoLocal.getEndereco(true).setBairro(bairro);
        novoLocal.setInformacoesAdicionais(informacoesAdicionais);

        this.localService.incluirNovoLocal(novoLocal);
        model.addAttribute("novoLocal", novoLocal);
        model.addAttribute("exibirAgradecimento", true);

        if (acao != null) {
            if (acao.equalsIgnoreCase("avaliar")) {
                return "redirect:/avaliacao/avaliar/" + novoLocal.getId();
            } else if (acao.equalsIgnoreCase("escreverDica")) {
                return "redirect:/dicas/escrever/" + novoLocal.getId();
            } else if (acao.equalsIgnoreCase("perguntar")) {
                return "redirect:/perguntas/perguntar?local=" + novoLocal.getId();
            }
        }
        return "/locais/acoesNovoLocal";
    }

    @RequestMapping(value = "/ultimasAtividadesAmigos/{local}", method = { RequestMethod.GET })
    public String ultimasAtividadesAmigos(@PathVariable final Local local, final Model model) {
        model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesAmigosEmLocal(getUsuarioAutenticado(), local, 5));
        return "/locais/atividadesAmigos";
    }
}
