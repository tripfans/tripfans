package br.com.fanaticosporviagens.locais;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryProcessor;

@Component
public class TagsLocal {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final Map<Long, TagLocal> tags = new HashMap<Long, TagLocal>();

    @Autowired
    protected GenericSearchRepository searchRepository;

    public String getPorCodigo(final Long codigo) {
        final TagLocal tagLocal = this.tags.get(codigo);
        if (tagLocal != null) {
            return tagLocal.getNome();
        }
        return "";
    }

    public TagLocal getTagPorCodigo(final Long codigo) {
        final TagLocal tagLocal = this.tags.get(codigo);
        return tagLocal;
    }

    public String getTagsBomPara(final List<Long> idsTags) {
        final StringBuffer tags = new StringBuffer();
        for (final Long id : idsTags) {
            final TagLocal tag = getTagPorCodigo(id);
            if (tag.getTipoTag() == 2) {
                if (tags.length() != 0) {
                    tags.append(" - ");
                }
                tags.append(tag.getNome());
            }
        }
        return tags.toString();
    }

    public String getTagsCategoria(final List<Long> idsTags) {
        final StringBuffer tags = new StringBuffer();
        for (final Long id : idsTags) {
            final TagLocal tag = getTagPorCodigo(id);
            if (tag.getTipoTag() == 1) {
                if (tags.length() != 0) {
                    tags.append(" - ");
                }
                tags.append(tag.getNome());
            }
        }
        return tags.toString();
    }

    @PostConstruct
    public void popular() {
        final String sqlQuery = XMLQueryProcessor.createQuery("Local.todasTagsLocal");
        final List<TagLocal> tags = this.namedParameterJdbcTemplate.query(sqlQuery, new HashMap<String, Object>(), getTagLocalRowMapper());

        for (final TagLocal tag : tags) {
            this.tags.put(tag.getId(), tag);
        }
    }

    private RowMapper<TagLocal> getTagLocalRowMapper() {
        return new RowMapper<TagLocal>() {

            @Override
            public TagLocal mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final TagLocal tag = new TagLocal();
                tag.setId(rs.getLong("id"));
                tag.setNome(rs.getString("nome"));
                tag.setTipoTag(rs.getInt("tipoTag"));
                return tag;
            }
        };
    }
}
