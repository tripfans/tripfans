package br.com.fanaticosporviagens.locais;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.foursquare.FoursquareService;
import br.com.fanaticosporviagens.gmail.service.GoogleService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.AutoCompleteJSONResult;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Estado;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.Pais;
import br.com.fanaticosporviagens.model.entity.Restaurante;

/**
 * @author André Thiago (andrethiagos@gmail.com)
 * 
 */
@Controller
@RequestMapping("/locais/cruds")
@PreAuthorize("@securityService.isAdmin()")
public class LocalCrudController extends BaseController {

    @Autowired
    private FotoService fotoService;

    @Autowired
    private FoursquareService foursquareService;

    @Autowired
    private GoogleService googleService;

    @Autowired
    private LocalService service;

    @RequestMapping(value = "/adicionarFotos", method = { RequestMethod.POST })
    public String adicionarFotos(@Valid final Local local, final BindingResult result, final Model model, final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("local", local);
            return "/locais/cruds/formAdicaoFotos";
        }
        if (local.possuiFotos()) {
            this.fotoService.incluirFotos(local);
        }
        return "redirect:/locais/cruds/sucesso";
    }

    @RequestMapping(value = "/alterarAtracao", method = { RequestMethod.POST })
    public String alterarAtracao(@Valid @ModelAttribute("atracao") final Atracao atracao, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("atracao", atracao);
            return "/locais/cruds/formAlterarAtracao";
        }
        if (atracao.getFotoPadrao() != null) {
            if (atracao.getFotoPadrao().isTipoArmazenamentoLocal()) {
                this.fotoService.incluirFotoPadrao(atracao);
            } else {
                this.fotoService.configuarFotoPadraoExterna(atracao);
            }
        }
        this.service.alterar(atracao);
        if (atracao.possuiFotos()) {
            this.fotoService.incluirFotos(atracao);
        }
        this.success("Atração alterada com sucesso");
        return "redirect:/locais/cruds/exibirAlteracaoAtracao/" + atracao.getId();
    }

    @RequestMapping(value = "/alterarCidade", method = { RequestMethod.POST })
    public String alterarCidade(@Valid @ModelAttribute("cidade") final LocalGeografico cidade, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("cidade", cidade);
            return "/locais/cruds/formAlterarCidade";
        }
        if (cidade.getFotoPadrao() != null) {
            if (cidade.getFotoPadrao().isTipoArmazenamentoLocal()) {
                this.fotoService.incluirFotoPadrao(cidade);
            } else {
                this.fotoService.configuarFotoPadraoExterna(cidade);
            }
        }
        this.service.alterar(cidade);
        if (cidade.possuiFotos()) {
            this.fotoService.incluirFotos(cidade);
        }
        this.success("Cidade alterada com sucesso");
        return "redirect:/locais/cruds/exibirAlteracaoCidade/" + cidade.getId();
    }

    @RequestMapping(value = "/alterarHotel", method = { RequestMethod.POST })
    public String alterarHotel(@Valid @ModelAttribute("hotel") final Hotel hotel, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("hotel", hotel);
            return "/locais/cruds/formAlterarHotel";
        }
        if (hotel.getFotoPadrao() != null) {
            if (hotel.getFotoPadrao().isTipoArmazenamentoLocal()) {
                this.fotoService.incluirFotoPadrao(hotel);
            } else {
                this.fotoService.configuarFotoPadraoExterna(hotel);
            }
        }
        this.service.alterar(hotel);
        if (hotel.possuiFotos()) {
            this.fotoService.incluirFotos(hotel);

        }
        this.success("Hotel alterado com sucesso");
        return "redirect:/locais/cruds/exibirAlteracaoHotel/" + hotel.getId();
        // return "redirect:/locais/cruds/sucesso";
    }

    @RequestMapping(value = "/alterarLocal", method = { RequestMethod.POST })
    public @ResponseBody
    JSONResponse alterarLocal(@RequestParam final Long idLocal, @RequestParam final String nome,
            @RequestParam(required = false) final String endereco, @RequestParam(required = false) final String bairro,
            @RequestParam(required = false) final String cep, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude, @RequestParam(required = false) final String telefone,
            @RequestParam(required = false) final String email, @RequestParam(required = false) final String site,
            @RequestParam(required = false) final Boolean mostrarOutrasFotosPanoramio, @RequestParam(required = false) final String outrasCategorias,
            final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        local.setNome(nome);
        local.getEndereco(true).setDescricao(endereco);
        local.getEndereco(true).setCep(cep);
        if (bairro != null && !bairro.trim().equals("")) {
            local.getEndereco(true).setBairro(bairro);
        }
        local.getCoordenadaGeografica().setLatitude(latitude);
        local.getCoordenadaGeografica().setLongitude(longitude);
        local.setTelefone(telefone);
        local.setEmail(email);
        local.setMostrarOutrasFotosPanoramio(mostrarOutrasFotosPanoramio);
        local.setSite(site);
        local.setOutrasCategorias(outrasCategorias);

        this.service.salvar(local);

        final JSONResponse jsonResponse = new JSONResponse();
        if (local.getPerimetroParaConsultaNoMapa() != null) {
            jsonResponse.addParam("latSW", local.getPerimetroParaConsultaNoMapa().getLatitudeSudoeste());
            jsonResponse.addParam("lngSW", local.getPerimetroParaConsultaNoMapa().getLongitudeSudoeste());
            jsonResponse.addParam("latNE", local.getPerimetroParaConsultaNoMapa().getLatitudeNordeste());
            jsonResponse.addParam("lngNE", local.getPerimetroParaConsultaNoMapa().getLongitudeNordeste());
        }
        this.statusMessage.success("Local alterado com sucesso!");
        return jsonResponse.success();
    }

    @RequestMapping(value = "/alterarRestaurante", method = { RequestMethod.POST })
    public String alterarRestaurante(@Valid @ModelAttribute("restaurante") final Restaurante restaurante, final BindingResult result,
            final Model model, final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("restaurante", restaurante);
            return "/locais/cruds/formAlterarRestaurante";
        }
        if (restaurante.getFotoPadrao() != null) {
            if (restaurante.getFotoPadrao().isTipoArmazenamentoLocal()) {
                this.fotoService.incluirFotoPadrao(restaurante);
            } else {
                this.fotoService.configuarFotoPadraoExterna(restaurante);
            }
        }
        this.service.alterar(restaurante);
        if (restaurante.possuiFotos()) {
            this.fotoService.incluirFotos(restaurante);
        }
        // return "redirect:/locais/cruds/sucesso";
        this.success("Restaurante alterado com sucesso");
        return "redirect:/locais/cruds/exibirAlteracaoRestaurante/" + restaurante.getId();
    }

    @RequestMapping(value = "/associarComFoursquare/{idLocal}/{idFoursquare}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage associarComFoursquare(@PathVariable final Long idLocal, @PathVariable final String idFoursquare, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        this.service.associarLocalComFoursquare(local, idFoursquare);
        return success("Local " + local.getNome() + " associado com Foursquare com sucesso!");
    }

    @RequestMapping(value = "/associarComGoogle/{idLocal}/{idGoogle}/{reference}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage associarComGoogle(@PathVariable final Long idLocal, @PathVariable final String idGoogle, @PathVariable final String reference,
            final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        this.service.associarLocalComGoogle(local, idGoogle, reference);
        return success("Local " + local.getNome() + " associado com Google com sucesso!");
    }

    @RequestMapping(value = "/concluirLocal/{idLocal}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage concluirLocal(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        local.setCadastroConcluido(true);
        this.service.salvar(local);
        return success("Cadastro concluido para " + local.getNome() + " com sucesso!");
    }

    @RequestMapping(value = "/consultarCidadesPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<Cidade> consultarCidadesPorNome(@RequestParam("term") final String nome,
            @RequestParam(value = "estado", required = false) final Estado estado) {
        final List<Cidade> cidades = this.service.consultarCidadesPorNome(nome, null, estado);
        return cidades;
    }

    @RequestMapping(value = "/consultarDisponibilidadeNome")
    public @ResponseBody
    JSONResponse consultarDisponibilidadeNome(@RequestParam final String nomeLocal) {
        final boolean disponivel = this.service.consultarPorUrlPath(nomeLocal) == null;
        String resposta = "Nome de local não disponível.";
        if (disponivel) {
            resposta = "Este nome de local está disponível";
        }
        this.jsonResponse.addParam("disponivel", disponivel);
        this.jsonResponse.addParam("resposta", resposta);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/consultarEstadosPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarEstadosPorNome(@RequestParam("term") final String nome, @RequestParam final Local pais) {
        System.out.println(pais.getNome());
        final List<Estado> estados = this.service.consultarEstadosPorNome(nome);
        return new JSONResponse(estados, this.jsonFields);
    }

    @RequestMapping(value = "/consultarLocaisPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<AutoCompleteJSONResult> consultarLocaisPorNome(@RequestParam("term") final String nome) {
        final List<Local> locais = this.service.consultarLocaisPorNome(nome);
        return AutoCompleteJSONResult.createList(locais, "id", "nome", "nome");
    }

    @RequestMapping(value = "/consultarPaisesPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse consultarPaisesPorNome(@RequestParam("term") final String nome) {
        final List<Pais> paises = this.service.consultarPaisesPorNome(nome);
        return new JSONResponse(paises, this.jsonFields);
    }

    @RequestMapping(value = "/dadosLocalFoursquare/{idLocal}", method = { RequestMethod.GET })
    public String dadosLocalFoursquare(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        model.addAttribute("local", local);

        if (local.getIdFoursquare() != null) {
            model.addAttribute("dadosFoursquare", local.getLocalFoursquare());
        }
        return "/locais/cruds/dadosLocalFoursquare";
    }

    @RequestMapping(value = "/dadosLocalGoogle/{idLocal}", method = { RequestMethod.GET })
    public String dadosLocalGoogle(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        model.addAttribute("local", local);

        if (local.getIdGooglePlaces() != null) {
            model.addAttribute("localGoogle", local.getLocalGoogle());
        }
        return "/locais/cruds/dadosLocalGoogle";
    }

    @RequestMapping(value = "/exibirAdicaoFotosLocal", method = { RequestMethod.GET })
    public String exibirAdicaoFotosLocal(final Local local, final Model model) {
        model.addAttribute("local", local);
        return "/locais/cruds/formAdicaoFotos";
    }

    @RequestMapping(value = "/exibirAlteracaoAtracao/{atracao}", method = { RequestMethod.GET })
    public String exibirAlteracaoAtracao(@PathVariable final Atracao atracao, final Model model) {
        model.addAttribute("atracao", atracao);
        return "/locais/cruds/formAlterarAtracao";
    }

    @RequestMapping(value = "/exibirAlteracaoCidade/{cidade}", method = { RequestMethod.GET })
    public String exibirAlteracaoCidade(@PathVariable final LocalGeografico cidade, final Model model) {
        model.addAttribute("cidade", cidade);
        return "/locais/cruds/formAlterarCidade";
    }

    @RequestMapping(value = "/exibirAlteracaoHotel/{hotel}", method = { RequestMethod.GET })
    public String exibirAlteracaoHotel(@PathVariable final Hotel hotel, final Model model) {
        model.addAttribute("hotel", hotel);
        return "/locais/cruds/formAlterarHotel";
    }

    @RequestMapping(value = "/exibirAlteracaoRestaurante/{restaurante}", method = { RequestMethod.GET })
    public String exibirAlteracaoRestaurante(@PathVariable final Restaurante restaurante, final Model model) {
        model.addAttribute("restaurante", restaurante);
        return "/locais/cruds/formAlterarRestaurante";
    }

    @RequestMapping(value = "/exibirInclusaoAtracao", method = { RequestMethod.GET })
    public String exibirInclusaoAtracao(final Model model) {
        model.addAttribute("atracao", new Atracao());
        model.addAttribute("inclusao", true);
        return "/locais/cruds/formAlterarAtracao";
    }

    @RequestMapping(value = "/inicio", method = { RequestMethod.GET })
    public String inicio() {
        return "/locais/cruds/inicioCruds";
    }

    @RequestMapping(value = "/listaAtracoes", method = { RequestMethod.GET })
    public String listaAtracoes(@RequestParam(required = false) final LocalGeografico local, final Model model) {
        if (local != null) {
            model.addAttribute("atracoes", this.service.consultarAtracoes(local));
            model.addAttribute("limit", this.getSearchData().getLimit());
            final int quantidadePaginas = (int) ((this.getSearchData().getTotal() % this.getSearchData().getLimit() == 0 ? 0 : 1) + this
                    .getSearchData().getTotal() / this.getSearchData().getLimit());
            final int startUltimaPagina = (int) ((this.getSearchData().getTotal() - this.getSearchData().getLimit()));
            final int paginaAtual = this.getSearchData().getStart() / this.getSearchData().getLimit() + 1;
            model.addAttribute("paginaAtual", paginaAtual);
            model.addAttribute("startUltimaPagina", startUltimaPagina);
            model.addAttribute("quantidadePaginas", quantidadePaginas);
            model.addAttribute("temMaisPaginas", paginaAtual < quantidadePaginas);
            model.addAttribute("cidade", local);
            return "/locais/cruds/listaAtracoesPaginada";
        } else {
            return "/locais/cruds/listaAtracoes";
        }
    }

    @RequestMapping(value = "/listaCidades", method = { RequestMethod.GET })
    public String listaCidades(@RequestParam(required = false) final Pais pais, final Model model) {
        if (pais != null) {
            // model.addAttribute("start", getSearchData().getStart());
            model.addAttribute("cidades", this.service.consultarCidades(pais));
            model.addAttribute("limit", this.getSearchData().getLimit());
            final int quantidadePaginas = (int) ((this.getSearchData().getTotal() % this.getSearchData().getLimit() == 0 ? 0 : 1) + this
                    .getSearchData().getTotal() / this.getSearchData().getLimit());
            final int startUltimaPagina = (int) ((this.getSearchData().getTotal() / this.getSearchData().getLimit()) * this.getSearchData()
                    .getLimit());
            final int paginaAtual = this.getSearchData().getStart() / this.getSearchData().getLimit() + 1;
            model.addAttribute("paginaAtual", paginaAtual);
            model.addAttribute("startUltimaPagina", startUltimaPagina);
            model.addAttribute("quantidadePaginas", quantidadePaginas);
            model.addAttribute("temMaisPaginas", paginaAtual < quantidadePaginas);
            model.addAttribute("pais", pais);
            return "/locais/cruds/listaCidadesPaginada";
        } else {
            return "/locais/cruds/listaCidades";
        }
    }

    @RequestMapping(value = "/listaCidadesPorEstado/{idEstado}", method = { RequestMethod.GET })
    public String listaCidadesPorEstado(@RequestParam(required = false) Integer page, @PathVariable final Long idEstado, final Model model) {
        if (page == null) {
            page = 1;
        }
        model.addAttribute("estado", this.service.consultarEstado(idEstado));
        model.addAttribute("cidades", this.service.consultarCidadesPorEstado(idEstado, 30 * (page - 1), 30));
        final Long quantidadeCidades = this.service.consultarQuantidadeCidadesPorEstado(idEstado);
        model.addAttribute("paginaAtual", page);
        final long quantidadePaginas = (quantidadeCidades / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaCidades";
    }

    @RequestMapping(value = "/listaCidadesPorPais/{idPais}", method = { RequestMethod.GET })
    public String listaCidadesPorPais(@RequestParam(required = false) Integer page, @PathVariable final Long idPais, final Model model) {
        if (page == null) {
            page = 1;
        }
        model.addAttribute("pais", this.service.consultarPais(idPais));
        model.addAttribute("cidades", this.service.consultarCidadesPorPais(idPais, 30 * (page - 1), 30));
        final Long quantidadeCidades = this.service.consultarQuantidadeCidadesPorPais(idPais);
        model.addAttribute("paginaAtual", page);
        final long quantidadePaginas = (quantidadeCidades / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaCidades";
    }

    @RequestMapping(value = "/listaEstados/{idPais}", method = { RequestMethod.GET })
    public String listaEstados(@RequestParam(required = false) Integer page, @PathVariable final Long idPais, final Model model) {
        if (page == null) {
            page = 1;
        }
        model.addAttribute("pais", this.service.consultarPais(idPais));
        model.addAttribute("estados", this.service.consultarEstados(idPais, 30 * (page - 1), 30));
        final Long quantidadeEstados = this.service.consultarQuantidadeEstados(idPais);
        model.addAttribute("paginaAtual", page);
        final long quantidadePaginas = (quantidadeEstados / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaEstados";
    }

    @RequestMapping(value = "/listaHoteis", method = { RequestMethod.GET })
    public String listaHoteis(@RequestParam(required = false) final LocalGeografico cidade, final Model model) {

        if (cidade != null) {
            model.addAttribute("hoteis", this.service.consultarHoteis(cidade));
            model.addAttribute("limit", this.getSearchData().getLimit());
            final int quantidadePaginas = (int) ((this.getSearchData().getTotal() % this.getSearchData().getLimit() == 0 ? 0 : 1) + this
                    .getSearchData().getTotal() / this.getSearchData().getLimit());
            final int startUltimaPagina = (int) ((this.getSearchData().getTotal() - this.getSearchData().getLimit()));
            final int paginaAtual = this.getSearchData().getStart() / this.getSearchData().getLimit() + 1;
            model.addAttribute("paginaAtual", paginaAtual);
            model.addAttribute("startUltimaPagina", startUltimaPagina);
            model.addAttribute("quantidadePaginas", quantidadePaginas);
            model.addAttribute("temMaisPaginas", paginaAtual < quantidadePaginas);
            model.addAttribute("cidade", cidade);
            return "/locais/cruds/listaHoteisPaginada";
        } else {
            return "/locais/cruds/listaHoteis";
        }
        /*if (page == null) {
            page = 1;
        }
        model.addAttribute("hoteis", this.service.consultarHoteis(30 * (page - 1), 30));
        model.addAttribute("paginaAtual", page);
        final Long quantidadeHoteis = this.service.consultarQuantidadeHoteis();
        final long quantidadePaginas = (quantidadeHoteis / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaHoteis";*/
    }

    @RequestMapping(value = "/listaPaises", method = { RequestMethod.GET })
    public String listaPaises(@RequestParam(required = false) Integer page, final Model model) {
        if (page == null) {
            page = 1;
        }
        model.addAttribute("paises", this.service.consultarPaises(30 * (page - 1), 30));
        final Long quantidadePaises = this.service.consultarQuantidadePaises();
        model.addAttribute("paginaAtual", page);
        final long quantidadePaginas = (quantidadePaises / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaPaises";
    }

    @RequestMapping(value = "/listarAtracoesCidade/{cidade}", method = { RequestMethod.GET })
    public String listarAtracoesCidade(@PathVariable final LocalGeografico cidade, final Model model) {
        boolean paginar = false;
        if (!this.searchData.isPaginationDataValid()) {
            this.searchData.setStart(0);
            this.searchData.setLimit(50);
        } else {
            paginar = true;
        }
        final List<Atracao> locais = this.service.consultarAtracoesCadastroNaoConcluido(cidade);
        model.addAttribute("locais", locais);
        model.addAttribute("quantidadeLocais", locais.size());
        model.addAttribute("cidade", cidade);
        if (paginar) {
            return "/locais/cruds/listaTodosLocaisCidadePagina";
        } else {
            return "/locais/cruds/listaTodosLocaisCidade";
        }
    }

    @RequestMapping(value = "/listaRestaurantes", method = { RequestMethod.GET })
    public String listaRestaurantes(@RequestParam(required = false) final LocalGeografico cidade, final Model model) {
        if (cidade != null) {
            model.addAttribute("restaurantes", this.service.consultarRestaurantes(cidade));
            model.addAttribute("limit", this.getSearchData().getLimit());
            final int quantidadePaginas = (int) ((this.getSearchData().getTotal() % this.getSearchData().getLimit() == 0 ? 0 : 1) + this
                    .getSearchData().getTotal() / this.getSearchData().getLimit());
            final int startUltimaPagina = (int) ((this.getSearchData().getTotal() - this.getSearchData().getLimit()));
            final int paginaAtual = this.getSearchData().getStart() / this.getSearchData().getLimit() + 1;
            model.addAttribute("paginaAtual", paginaAtual);
            model.addAttribute("startUltimaPagina", startUltimaPagina);
            model.addAttribute("quantidadePaginas", quantidadePaginas);
            model.addAttribute("temMaisPaginas", paginaAtual < quantidadePaginas);
            model.addAttribute("cidade", cidade);
            return "/locais/cruds/listaRestaurantesPaginada";
        } else {
            return "/locais/cruds/listaRestaurantes";
        }
        /*if (page == null) {
            page = 1;
        }
        model.addAttribute("restaurantes", this.service.consultarRestaurantes(30 * (page - 1), 30));
        model.addAttribute("paginaAtual", page);
        final Long quantidadeRestaurantes = this.service.consultarQuantidadeRestaurantes();
        final long quantidadePaginas = (quantidadeRestaurantes / 30) + 1;
        model.addAttribute("quantidadePaginas", quantidadePaginas);
        model.addAttribute("temMaisPaginas", page < quantidadePaginas);
        return "/locais/cruds/listaRestaurantes";*/
    }

    @RequestMapping(value = "/listarHoteisCidade/{cidade}", method = { RequestMethod.GET })
    public String listarHoteisCidade(@PathVariable final LocalGeografico cidade, final Model model) {
        boolean paginar = false;
        if (!this.searchData.isPaginationDataValid()) {
            this.searchData.setStart(0);
            this.searchData.setLimit(50);
        } else {
            paginar = true;
        }
        final List<Hotel> locais = this.service.consultarHoteisCadastroNaoConcluido(cidade);
        model.addAttribute("locais", locais);
        model.addAttribute("quantidadeLocais", locais.size());
        model.addAttribute("cidade", cidade);
        if (paginar) {
            return "/locais/cruds/listaTodosLocaisCidadePagina";
        } else {
            return "/locais/cruds/listaTodosLocaisCidade";
        }
    }

    @RequestMapping(value = "/listarLocaisFoursquare/{idLocal}", method = { RequestMethod.GET })
    public String listarLocaisFoursquare(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        model.addAttribute("local", local);

        if (local.getLocalFoursquare() != null) {
            model.addAttribute("dadosFoursquare", local.getLocalFoursquare());
            return "/locais/cruds/dadosLocalFoursquare";
        }

        model.addAttribute("venues", this.foursquareService.recuperarLocaisSemelhantesNoFoursquare(local));

        /*final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);

        final Venue venue = new Venue("1", local.getNome(), new ContactInfo("", "", "", "", ""), new Location(local.getEndereco().getDescricao(), "",
                "", "", "", "", local.getLatitude(), local.getLongitude()), null, true, null);

        final Venue venue2 = new Venue("2", local.getNome(), new ContactInfo("", "", "", "", ""), new Location(local.getEndereco().getDescricao(),
                "", "", "", "", "", local.getLatitude(), local.getLongitude()), null, true, null);

        final List<Venue> venues = new ArrayList<Venue>();
        venues.add(venue);
        venues.add(venue2);

        model.addAttribute("venues", venues);*/

        return "/locais/cruds/listaLocaisFoursquare";
    }

    @RequestMapping(value = "/listarLocaisGoogle/{idLocal}", method = { RequestMethod.GET })
    public String listarLocaisGoogle(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        model.addAttribute("local", local);

        if (local.getIdGooglePlaces() != null) {
            model.addAttribute("localGoogle", local.getLocalGoogle());
            return "/locais/cruds/dadosLocalGoogle";
        }

        model.addAttribute("places", this.googleService.recuperarLocaisSemelhantesNoGoogle(local));
        return "/locais/cruds/listaLocaisGoogle";
    }

    @RequestMapping(value = "/listarRestaurantesCidade/{cidade}", method = { RequestMethod.GET })
    public String listarRestaurantesCidade(@PathVariable final LocalGeografico cidade, final Model model) {
        boolean paginar = false;
        if (!this.searchData.isPaginationDataValid()) {
            this.searchData.setStart(0);
            this.searchData.setLimit(50);
        } else {
            paginar = true;
        }

        final List<Restaurante> locais = this.service.consultarRestaurantesCadastroNaoConcluido(cidade);
        model.addAttribute("locais", locais);
        model.addAttribute("quantidadeLocais", locais.size());
        model.addAttribute("cidade", cidade);
        if (paginar) {
            return "/locais/cruds/listaTodosLocaisCidadePagina";
        } else {
            return "/locais/cruds/listaTodosLocaisCidade";
        }
    }

    @RequestMapping(value = "/listaTodosLocais", method = { RequestMethod.GET })
    public String listaTodosLocais(@RequestParam(required = false) final LocalGeografico cidade, final Model model) {
        if (cidade != null) {
            model.addAttribute("exibirLocais", true);
            model.addAttribute("atracoes", this.service.consultarAtracoes(cidade));
            model.addAttribute("hoteis", this.service.consultarHoteis(cidade));
            model.addAttribute("restaurantes", this.service.consultarRestaurantes(cidade));
            model.addAttribute("cidade", cidade);
            return "/locais/cruds/listaTodosLocaisCidade";
        } else {
            return "/locais/cruds/gerenciarLocaisCidade";
        }
    }

    @RequestMapping(value = "/marcarLocalCandidatoExclusao/{idLocal}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage marcarLocalCandidatoExclusao(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        local.setCandidatoExclusao(true);
        this.service.salvar(local);
        return success(local.getNome() + " marcado como candidato à exclusão com sucesso!");
    }

    @RequestMapping(value = "/marcarLocalExcluido/{idLocal}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage marcarLocalExcluido(@PathVariable final Long idLocal, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        local.setExcluido(true);
        // this.service.excluir(local);
        this.service.alterar(local);
        return success(local.getNome() + " excluído com sucesso!");
    }

    @RequestMapping(value = "/sucesso", method = { RequestMethod.GET })
    public String sucesso(final Model model) {
        return "/locais/cruds/sucesso";
    }

    @RequestMapping(value = "/usarFotoPanoramio/{idLocal}/{idFotoPanoramio}/{idUsuarioFotoPanoramio}", method = { RequestMethod.POST })
    public @ResponseBody
    StatusMessage usarFotoPanoramio(@PathVariable final Long idLocal, @PathVariable final String idFotoPanoramio,
            @PathVariable final String idUsuarioFotoPanoramio, final Model model) {
        final LocalEnderecavel local = (LocalEnderecavel) this.service.consultarPorId(idLocal);
        this.service.associarFotoLocalComFotoPanoramio(local, idFotoPanoramio, idUsuarioFotoPanoramio);
        return success("Foto do Panoramio associada para " + local.getNome() + " com sucesso!");
    }

    public String validaUrlPath(final String urlPath) {
        if (this.service.consultarLocalPorUrlPath(urlPath).getId() == null) {
            return "problema";
        }
        return "sucesso";
    }

}
