package br.com.fanaticosporviagens.locais;

import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.GrupoEmpresarial;

@Service
public class GrupoEmpresarialService extends GenericCRUDService<GrupoEmpresarial, Long> {

}
