package br.com.fanaticosporviagens.locais;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.ClasseLocal;

@Component
public class TodasClassesLocal {

    private Map<Integer, String> classes = new HashMap<Integer, String>();

    public String getPorCodigo(final Integer codigo) {
        return this.classes.get(codigo);
    }

    public void popular(final List<ClasseLocal> todas) {
        this.classes = new HashMap<Integer, String>();
        for (final ClasseLocal categoria : todas) {
            this.classes.put(categoria.getCodigo(), categoria.getDescricao());
        }
    }
}
