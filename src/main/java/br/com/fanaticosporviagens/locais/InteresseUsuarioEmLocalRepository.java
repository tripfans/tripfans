package br.com.fanaticosporviagens.locais;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 *
 * @author Gustavo Henrique
 *
 */
@Repository
public class InteresseUsuarioEmLocalRepository extends GenericCRUDRepository<InteresseUsuarioEmLocal, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public InteresseUsuarioEmLocal consultarInteresse(final String idUsuarioFacebook, final Local local) {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT interesse ");
        hql.append("  FROM InteresseUsuarioEmLocal interesse ");
        hql.append(" WHERE interesse.idUsuarioFacebook = :idUsuarioFacebook ");
        hql.append("   AND interesse.local.id = :idLocal ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuarioFacebook", idUsuarioFacebook);
        parametros.put("idLocal", local.getId());

        return this.searchRepository.queryByHQLUniqueResult(hql.toString(), parametros);
    }

    /**
     *
     * @param usuarioAmigo
     * @param local
     * @param tipoInteresse
     * @param tamanhoAmostra
     *            Caso queira que o tamanho da amostra seja todos os interesses informe um número menor que 1.
     * @return
     */
    private InteresseAmigosEmLocal consultarInteresseAmigos(final Usuario usuarioAmigo, final Local local,
            final TipoInteresseUsuarioEmLocal tipoInteresse, final int tamanhoAmostra) {
        final StringBuilder corpoHql = new StringBuilder();

        final HashMap<String, Object> parametros = new HashMap<String, Object>();

        corpoHql.append("  FROM InteresseUsuarioEmLocal interesse ");
        corpoHql.append(" WHERE interesse.local = :local ");
        corpoHql.append("   AND EXISTS (SELECT 1 ");
        corpoHql.append("                 FROM Amizade amizade ");
        corpoHql.append("                WHERE amizade.usuario = :usuarioAmigo ");
        corpoHql.append("                  AND amizade.amigo = interesse.usuario ");
        corpoHql.append("              ) ");
        corpoHql.append("   AND interesse." + tipoInteresse.getAtributoInteresse() + " = true ");

        parametros.put("usuarioAmigo", usuarioAmigo);

        parametros.put("local", local);

        final String hqlCount = "SELECT COUNT(*) " + corpoHql;
        final String hqlAmostra = "SELECT interesse " + corpoHql + " ORDER BY interesse.nomeUsuario ";
        final Long quantidade = Long.valueOf(this.searchRepository.queryByHQLUniqueResult(hqlCount, parametros).toString());
        if (quantidade > 0) {
            final InteresseAmigosEmLocal interesseAmigos = new InteresseAmigosEmLocal(local, usuarioAmigo, tipoInteresse);
            interesseAmigos.setQuantidadeAmigos(quantidade);

            final List<InteresseUsuarioEmLocal> interesses;
            if (tamanhoAmostra >= 1) {
                interesses = this.searchRepository.consultaHQL(hqlAmostra, parametros, 0, tamanhoAmostra);
                interesseAmigos.setAmostraInteresses(interesses);
            } else {
                interesses = this.searchRepository.consultaHQL(hqlAmostra, parametros);
                interesseAmigos.setAmostraInteresses(interesses);
            }

            return interesseAmigos;
        }
        return null;
    }

    public List<InteresseUsuarioEmLocal> consultarInteresses(final String idUsuarioFacebook, final List<Local> locais) {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT interesse ");
        hql.append("  FROM InteresseUsuarioEmLocal interesse ");
        hql.append(" WHERE interesse.idUsuarioFacebook = :idUsuarioFacebook ");
        hql.append("   AND interesse.local in (:locais) ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuarioFacebook", idUsuarioFacebook);
        parametros.put("locais", locais);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<InteresseUsuarioEmLocal> consultarInteresses(final Usuario usuario, final List<Local> locais) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select interesse ");
        hql.append("  from InteresseUsuarioEmLocal interesse ");
        hql.append(" where interesse.usuario = :usuario ");
        hql.append("   and interesse.local in (:locais) ");

        "c.id_local AS \'local.id\' ".charAt(0);

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        parametros.put("locais", locais);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public InteresseUsuarioEmLocal consultarInteresses(final Usuario usuario, final Local local) {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT interesse ");
        hql.append("  FROM InteresseUsuarioEmLocal interesse ");
        hql.append(" WHERE interesse.usuario = :usuario ");
        hql.append("   AND interesse.local = :local ");

        final HashMap<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        parametros.put("local", local);

        return this.searchRepository.queryByHQLUniqueResult(hql.toString(), parametros);
    }

    public List<InteresseAmigosEmLocal> consultarInteressesAmigos(final Usuario usuarioAmigo, final Local local) {

        final List<InteresseAmigosEmLocal> interessesAmigos = new ArrayList<InteresseAmigosEmLocal>();
        for (final TipoInteresseUsuarioEmLocal tipoInteresse : TipoInteresseUsuarioEmLocal.values()) {
            final InteresseAmigosEmLocal interesseAmigos = consultarInteresseTodosAmigos(usuarioAmigo, local, tipoInteresse);
            if (interesseAmigos != null) {
                interessesAmigos.add(interesseAmigos);
            }
        }

        return interessesAmigos;
    }

    public List<InteresseAmigosEmLocal> consultarInteressesAmigos(final Usuario usuarioAmigo, final Local local,
            final TipoInteresseUsuarioEmLocal[] tipos, final int tamanhoAmostra) {

        final List<InteresseAmigosEmLocal> interessesAmigos = new ArrayList<InteresseAmigosEmLocal>();
        for (final TipoInteresseUsuarioEmLocal tipoInteresse : tipos) {
            final InteresseAmigosEmLocal interesseAmigos = consultarInteresseAmigos(usuarioAmigo, local, tipoInteresse, tamanhoAmostra);
            if (interesseAmigos != null) {
                interessesAmigos.add(interesseAmigos);
            }
        }

        return interessesAmigos;
    }

    public InteresseAmigosEmLocal consultarInteresseTodosAmigos(final Usuario usuarioAmigo, final Local local,
            final TipoInteresseUsuarioEmLocal tipoInteresse) {
        return consultarInteresseAmigos(usuarioAmigo, local, tipoInteresse, 0);
    }

}
