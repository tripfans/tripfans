package br.com.fanaticosporviagens.locais;

import java.util.List;

public class LocalEnderecavelView {

    private String endereco;

    private Integer estrelas;

    private Long id;

    private String nome;

    private Double notaBooking;

    private Double notaTripAdvisor;

    private Integer numAvaliacoesTripAdvisor;

    private List<String> tags;

    private Double tripfansRanking;

    private String urlBooking;

    private String urlFotoAlbum;

    private String urlFotoBig;

    private String urlFotoSmall;

    private String urlPath;

    public String getEndereco() {
        return this.endereco;
    }

    public Integer getEstrelas() {
        return this.estrelas;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public Double getNotaBooking() {
        return this.notaBooking;
    }

    public Double getNotaTripAdvisor() {
        return this.notaTripAdvisor;
    }

    public Integer getNumAvaliacoesTripAdvisor() {
        return this.numAvaliacoesTripAdvisor;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public Double getTripfansRanking() {
        return this.tripfansRanking;
    }

    public String getUrlBooking() {
        return this.urlBooking;
    }

    public String getUrlFotoAlbum() {
        return this.urlFotoAlbum;
    }

    public String getUrlFotoBig() {
        return this.urlFotoBig;
    }

    public String getUrlFotoSmall() {
        return this.urlFotoSmall;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public void setEstrelas(final Integer estrelas) {
        this.estrelas = estrelas;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNotaBooking(final Double notaBooking) {
        this.notaBooking = notaBooking;
    }

    public void setNotaTripAdvisor(final Double notaTripAdvisor) {
        this.notaTripAdvisor = notaTripAdvisor;
    }

    public void setNumAvaliacoesTripAdvisor(final Integer numAvaliacoesTripAdvisor) {
        this.numAvaliacoesTripAdvisor = numAvaliacoesTripAdvisor;
    }

    public void setTags(final List<String> tags) {
        this.tags = tags;
    }

    public void setTripfansRanking(final Double tripfansRanking) {
        this.tripfansRanking = tripfansRanking;
    }

    public void setUrlBooking(final String urlBooking) {
        this.urlBooking = urlBooking;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}
