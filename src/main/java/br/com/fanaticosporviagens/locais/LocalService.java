package br.com.fanaticosporviagens.locais;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.sprockets.google.Place;
import net.sf.sprockets.google.Place.OpeningHours;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Checkin;
import org.springframework.social.facebook.api.CheckinPost;
import org.springframework.social.facebook.api.Location;
import org.springframework.social.facebook.api.Page;
import org.springframework.social.facebook.api.Post;
import org.springframework.social.foursquare.api.Category;
import org.springframework.social.foursquare.api.Venue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.foursquare.FoursquareService;
import br.com.fanaticosporviagens.gmail.service.GoogleService;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.Stopwords;
import br.com.fanaticosporviagens.model.entity.Agencia;
import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.Estado;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalFoursquare;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.LocalGoogle;
import br.com.fanaticosporviagens.model.entity.LocalInteresseTuristico;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.LocalidadeUtils;
import br.com.fanaticosporviagens.model.entity.Pais;
import br.com.fanaticosporviagens.model.entity.Restaurante;
import br.com.fanaticosporviagens.model.entity.TipoServicoHotel;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;
import br.com.fanaticosporviagens.util.SorteadorItens;
import br.com.fanaticosporviagens.viagem.ViagemService;
import br.com.fanaticosporviagens.viagem.view.FiltroLocaisViagem;
import br.com.tripfans.importador.RegiaoTripFans;

/**
 *
 * @author André Thiago
 *
 */
@Service
public class LocalService extends GenericCRUDService<Local, Long> {

    class VenueComparable implements Comparable<VenueComparable> {

        private int quantidadePalavras;

        private Venue venue;

        VenueComparable(final Venue venue, final int quantidadePalavras) {
            this.venue = venue;
            this.quantidadePalavras = quantidadePalavras;
        }

        @Override
        public int compareTo(final VenueComparable other) {
            Integer pontuacaoEste = 0;
            Integer pontuacaoOutro = 0;
            if (this.quantidadePalavras == other.quantidadePalavras) {
                pontuacaoEste += obterPontuacaoLocalizacao(this.venue);
                pontuacaoOutro += obterPontuacaoLocalizacao(other.venue);
                if (this.venue.getStats().getCheckinsCount() > other.venue.getStats().getCheckinsCount()) {
                    pontuacaoEste += 1;
                } else if (this.venue.getStats().getCheckinsCount() < other.venue.getStats().getCheckinsCount()) {
                    pontuacaoOutro += 1;
                }
            } else if (this.quantidadePalavras > other.quantidadePalavras) {
                pontuacaoEste += 1;
            } else {
                pontuacaoOutro += 1;
            }
            return pontuacaoEste.compareTo(pontuacaoOutro);
        }

        public int getQuantidadePalavras() {
            return this.quantidadePalavras;
        }

        public Venue getVenue() {
            return this.venue;
        }

        private int obterPontuacaoLocalizacao(final Venue venue) {
            int pontuacao = 0;
            if (venue.getLocation().getAddress() != null) {
                pontuacao += 1;
            }
            if (venue.getLocation().getCity() != null) {
                pontuacao += 1;
            }
            return pontuacao;
        }

        public void setQuantidadePalavras(final int quantidadePalavras) {
            this.quantidadePalavras = quantidadePalavras;
        }

        public void setVenue(final Venue venue) {
            this.venue = venue;
        }
    }

    private static int contarPalavrasIguais(final String texto1, String texto2) {
        int quantidade = 0;
        String palavra = null;
        // Adiciona um espaço no início e ao final para ajudar na comparação
        texto2 = " " + texto2.toLowerCase() + " ";
        // remover palavras muito comuns (hotel, pousada, restaurante)
        texto2 = texto2.replace(" hotel ", " ");
        texto2 = texto2.replace(" pousada ", " ");
        texto2 = texto2.replace(" restaurante ", " ");
        texto2 = texto2.replace("'", "");
        final String[] palavras = texto1.split(" ");
        for (int i = 0; i < palavras.length; i++) {
            palavra = palavras[i];
            palavra = palavra.replace("'", "");
            // Adiciona um espaço no início e ao final para ajudar na comparação
            palavra = " " + palavra + " ";
            if (texto2.contains(palavra.toLowerCase())) {
                quantidade += 1;
            }
        }
        return quantidade;
    }

    public static double distanciaEntreCoordenadasGeograficas(final CoordenadaGeografica coordenadaGeografica1,
            final CoordenadaGeografica coordenadaGeografica2) {
        return distanciaEntreDoisPontos(coordenadaGeografica1.getLatitude(), coordenadaGeografica2.getLatitude(),
                coordenadaGeografica1.getLongitude(), coordenadaGeografica2.getLongitude());
    }

    public static double distanciaEntreDoisPontos(final Double lat1, final Double lat2, final Double lon1, final Double lon2) {
        return LocalidadeUtils.distance(lat1, lat2, lon1, lon2, 0, 0);
    }

    public static double distanciaEntreLocais(final Local local1, final Local local2) {
        return distanciaEntreCoordenadasGeograficas(local1.getCoordenadaGeografica(), local2.getCoordenadaGeografica());
    }

    public static void main(final String[] s) {
        System.out.println(contarPalavrasIguais("Tucunaré Casa Do Peixe", "Restaurante tucunaré casa do peixe"));
    }

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    protected FotoService fotoService;

    @Autowired
    private FoursquareService foursquareService;

    @Autowired
    private GoogleService googleService;

    @Autowired
    private InteresseUsuarioEmLocalRepository interesseUsuarioEmLocalRepository;

    @Autowired
    private LocalRepository localRepository;

    @Autowired
    private PesquisaTextualService pesquisaTextualService;

    @Autowired
    protected GenericSearchRepository searchRepository;

    @Autowired
    private SorteadorItens sorteador;

    @Autowired
    private TagsLocal todasCategoriasLocal;

    @Autowired
    private TodasClassesLocal todasClassesLocal;

    @Autowired
    private ViagemService viagemService;

    private void adequarCidadeEstadoPaisContinentePeloMaisEspecifico(final LocalEnderecavel local) {
        if ((local.getCidade() != null) && (local.getCidade().getEstado() != null) && (!local.getCidade().getEstado().equals(local.getEstado()))) {
            local.setEstado(local.getCidade().getEstado());
        }
        if ((local.getEstado() != null) && (local.getEstado().getPais() != null) && (!local.getEstado().getPais().equals(local.getPais()))) {
            local.setPais(local.getEstado().getPais());
        }
        if ((local.getPais() != null) && (local.getPais().getContinente() != null)
                && (!local.getPais().getContinente().equals(local.getContinente()))) {
            local.setContinente(local.getPais().getContinente());
        }
    }

    @Transactional
    public void alterar(final Atracao atracao) {
        System.out.println("1");
        if (atracao.getFotoPadrao() != null && atracao.getFotoPadrao().temArquivo()) {
            atracao.setFotoPadraoAnonima(false);
            atracao.setUsarFotoPanoramio(false);
            atracao.getFotoPadrao().setAnonima(false);
            atracao.getFotoPadrao().setAutor(getUsuarioAutenticado());
            atracao.getFotoPadrao().setLocal(atracao);
            atracao.getFotoPadrao().definirNovaFotoCapaLocal();
            this.fotoService.salvarFotoPadraoLocal(atracao.getFotoPadrao());
        }
        this.localRepository.alterar(atracao);
    }

    @Transactional
    public void alterar(final Cidade cidade) {
        this.localRepository.alterar(cidade);
    }

    @Transactional
    public void alterar(final Hotel hotel) {
        adequarCidadeEstadoPaisContinentePeloMaisEspecifico(hotel);

        this.localRepository.alterar(hotel);
    }

    @Transactional
    public void alterar(final Restaurante restaurante) {
        adequarCidadeEstadoPaisContinentePeloMaisEspecifico(restaurante);

        this.localRepository.alterar(restaurante);
    }

    @Transactional
    public void associarFotoLocalComFotoPanoramio(final Local local, final String idFotoPanoramio, final String idUsuarioFotoPanoramio) {
        local.setIdFotoPanoramio(idFotoPanoramio);
        local.setIdUsuarioFotoPanoramio(idUsuarioFotoPanoramio);
        local.setUsarFotoPanoramio(true);
        this.localRepository.alterar(local);
    }

    @Transactional
    public void associarLocalComFoursquare(final Local local, final String idFoursquare) {
        final Venue venue = this.foursquareService.recuperarLocalNoFoursquare(idFoursquare);

        if (venue != null) {
            local.setIdFoursquare(venue.getId());
            copiarDadosVenueFoursquareParaLocal(venue, local);
            this.localRepository.alterar(local);
        } else {
            throw new BusinessException("Local com ID: " + idFoursquare + " não encontrado no Foursquare!");
        }
    }

    @Transactional
    public void associarLocalComGoogle(final Local local, final String idGoogle, final String reference) {

        final Place place = this.googleService.recuperarLocalNoGooglePlaces(idGoogle, reference, local);

        if (place != null) {
            local.setIdGooglePlaces(idGoogle);
            local.setReferenceGooglePlaces(reference);
            copiarDadosGooglePlaceParaLocal(place, local);
            this.localRepository.alterar(local);
        } else {
            throw new BusinessException("Local com ID: " + idGoogle + " não encontrado no Google!");
        }
    }

    public <T extends Local> List<T> consultarAeroportosPorNome(final String nome) {
        return this.localRepository.consultarLocaisPorNome(nome, LocalType.AEROPORTO);
    }

    public List<Agencia> consultarAgenciasPorNome(final String nome) {
        return this.localRepository.consultarAgenciasPorNome(nome);
    }

    public List<Atracao> consultarAtracoes(final int first, final int last) {
        return this.localRepository.consultarAtracoes(first, last);
    }

    public List<Atracao> consultarAtracoes(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        return this.searchRepository.consultarPorNamedQuery("Local.consultarAtracoesPorCidade", parametros, Atracao.class);
    }

    public List<Atracao> consultarAtracoesCadastroNaoConcluido(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        parametros.put("cadastroConcluido", false);
        return this.searchRepository.consultarPorNamedQuery("Local.consultarAtracoesPorCidade", parametros, Atracao.class);
    }

    public <T extends Local> List<T> consultarAtracoesPorNome(final String nome) {
        return this.localRepository.consultarLocaisPorNome(nome, LocalType.ATRACAO);
    }

    public Cidade consultarCidadePorId(final Long id) {
        return this.localRepository.consultarCidadePorId(id);
    }

    public Cidade consultarCidadePorNome(final String nome, final Pais pais) {
        return this.localRepository.consultarCidadePorNome(nome, pais);
    }

    public Cidade consultarCidadePorReferenciaFacebook(final Page page) {
        if (page.getLocation() != null) {
            final Location location = page.getLocation();
            String nomeLocal = page.getName();
            if (nomeLocal.contains(",")) {
                nomeLocal = nomeLocal.split(",")[0];
            }
            // TODO mudar para tratar o país
            final List<EntidadeIndexada> locais = this.pesquisaTextualService.consultarLocais(nomeLocal, location.getLatitude(),
                    location.getLongitude(), new LocalType[] { LocalType.CIDADE });
            if (!locais.isEmpty() && locais.size() == 1) {
                return (Cidade) consultarPorId(locais.get(0).getId());
            }
        }
        return null;
    }

    public List<Cidade> consultarCidades(final Pais pais) {
        return this.localRepository.consultarCidades(pais);
    }

    public List<Cidade> consultarCidadesPopularesComAmigosJaForam(final int tamanhoAmostraCidades, final Usuario usuario,
            final int tamanhoAmostraAmigos) {
        return this.localRepository.consultarCidadesPopularesComAmigosJaForam(tamanhoAmostraCidades, usuario, tamanhoAmostraAmigos);
    }

    public List<Cidade> consultarCidadesPorEstado(final Long idEstado, final int first, final int last) {
        return this.localRepository.consultarCidadesPorEstado(idEstado, first, last);
    }

    public List<Cidade> consultarCidadesPorNome(final String nome) {
        return this.localRepository.consultarCidadesPorNome(nome, null, null);
    }

    public List<Cidade> consultarCidadesPorNome(final String nome, final Pais pais, final Estado estado) {
        return this.localRepository.consultarCidadesPorNome(nome, pais, estado);
    }

    public List<Cidade> consultarCidadesPorPais(final Long idPais, final int first, final int last) {
        return this.localRepository.consultarCidadesPorPais(idPais, first, last);
    }

    public Estado consultarEstado(final Long idEstado) {
        return this.localRepository.consultarEstado(idEstado);
    }

    public List<Estado> consultarEstados(final Long idPais, final int first, final int last) {
        return this.localRepository.consultarEstados(idPais, first, last);
    }

    public List<Estado> consultarEstadosPorNome(final String nome) {
        return this.localRepository.consultarEstadosPorNome(nome);
    }

    public List<Local> consultarFilhosMaisPopulares(final LocalGeografico localPai, final int tamanhoAmostra) {
        return this.localRepository.consultarFilhosMaisPopulares(localPai, tamanhoAmostra);
    }

    public List<Hotel> consultarHoteis(final int first, final int last) {
        return this.localRepository.consultarHoteis(first, last);
    }

    public List<Hotel> consultarHoteis(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        return this.searchRepository.consultarPorNamedQuery("Local.consultarHoteisPorCidade", parametros, Hotel.class);
    }

    public List<Hotel> consultarHoteisCadastroNaoConcluido(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        parametros.put("cadastroConcluido", false);
        return this.searchRepository.consultarPorNamedQuery("Local.consultarHoteisPorCidade", parametros, Hotel.class);
    }

    public List<Hotel> consultarHoteisPorNome(final String nome) {
        return this.localRepository.consultarHoteisPorNome(nome, (Long) null);
    }

    public List<Hotel> consultarHoteisPorNome(final String nome, final List<LocalGeografico> cidades) {
        Long[] idsCidades = null;
        if (cidades != null && !cidades.isEmpty()) {
            idsCidades = new Long[cidades.size()];
            for (int i = 0; i < idsCidades.length; i++) {
                idsCidades[i] = cidades.get(i).getId();
            }
        }
        return consultarHoteisPorNome(nome, idsCidades);
    }

    public List<Hotel> consultarHoteisPorNome(final String nome, final Long... idCidades) {
        return this.localRepository.consultarHoteisPorNome(nome, idCidades);
    }

    public <T extends Local> List<T> consultarLocadorasVeiculosPorNome(final String nome) {
        return this.localRepository.consultarLocaisPorNome(nome, LocalType.LOCADORA_VEICULOS);
    }

    public List<LocalView> consultarLocais(final FiltroLocaisViagem filtroLocais, final LocalType tipoLocal, final Long idCidade,
            final String nomeLocal, final Integer[] idsCategorias, final Integer[] idsTiposCozinha, final Integer[] idsTiposHospedagem,
            final Integer estrelas, final Integer precoMinino, final Integer precoMaximo) {

        final Map<String, Object> parametros = new HashMap<String, Object>();

        parametros.put("codigoTipoLocal", tipoLocal.getCodigo());
        parametros.put("idCidade", idCidade);

        parametros.put("nomeLocal", nomeLocal);
        parametros.put("categorias", idsCategorias);
        parametros.put("tiposCozinha", idsTiposCozinha);
        parametros.put("tiposHospedagem", idsTiposHospedagem);
        parametros.put("estrelas", estrelas);
        parametros.put("precoMinino", precoMinino);
        parametros.put("precoMaximo", precoMaximo);

        if (filtroLocais.equals(FiltroLocaisViagem.LOCAIS_RECOMENDADOS_POR_AMIGOS)) {
            parametros.put("idUsuario", getUsuarioAutenticado().getId());
        }

        return this.searchRepository.consultarPorNamedNativeQuery("Local." + filtroLocais.name(), parametros, LocalView.class);
    }

    public List<LocalEnderecavel> consultarLocaisEnderecaveis(final LocalGeografico local) {
        return this.consultarLocaisEnderecaveis(null, local);
    }

    public List<LocalEnderecavel> consultarLocaisEnderecaveis(final LocalType tipo, final LocalGeografico local) {
        return this.localRepository.consultarLocaisEnderecaveis(tipo, local);
    }

    public List<LocalEnderecavel> consultarLocaisEnderecaveis(final LocalType tipo, final LocalGeografico local, final String nome,
            final Integer[] estrelas) {
        return this.localRepository.consultarLocaisEnderecaveis(tipo, local, nome, estrelas);
    }

    public List<LocalEnderecavel> consultarLocaisPendentesAprovacao(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        return this.searchRepository.consultarPorNamedQuery("Local.consultarLocaisPendentesAprovacao", parametros, LocalEnderecavel.class);
    }

    public List<Local> consultarLocaisPorNome(final String nome) {
        return this.localRepository.consultarLocaisPorNome(nome);
    }

    public Local consultarLocalEquivalente(final Checkin checkin) {
        // Pega a pagina associada ao local do checkin, se tiver
        return consultarLocalEquivalente(checkin.getPlace());
    }

    public Local consultarLocalEquivalente(final org.springframework.social.foursquare.api.Checkin checkin) {
        // System.out.println(checkin.getVenue());

        System.out.println("Name :" + checkin.getVenue().getName());
        System.out.println("Description :" + checkin.getVenue().getDescription());
        System.out.println("Address :" + checkin.getVenue().getLocation().getAddress());
        System.out.println("City :" + checkin.getVenue().getLocation().getCity());
        System.out.println("State :" + checkin.getVenue().getLocation().getState());
        System.out.println("Country :" + checkin.getVenue().getLocation().getCountry());
        System.out.println("Lat :" + checkin.getVenue().getLocation().getLatitude());
        System.out.println("Long :" + checkin.getVenue().getLocation().getLongitude());

        return null;
    }

    public Local consultarLocalEquivalente(final Page place) {
        Local local = null;

        if (place != null) {
            // Recupera o local da foto
            final Location location = place.getLocation();
            final String placeName = place.getName();
            String cityName = null;
            boolean isCity = true;
            // Há casos que o Local só vem com latitude e longitude e o nome do local fica no Place (Page.getName())
            // e o nome do local fica no Place (Page.getName())
            if (location != null) {
                // Pegar o nome da cidade
                cityName = location.getCity();
                location.getCountry();
            }
            if (cityName == null) {
                isCity = false;
                // Pega o nome da pagina que geralmente é o nome do local juntamente com o nome do pais (as vezes em Ingles)
                cityName = place.getName();
                // Se tiver ',' no nome, separar o nome da cidade do nome do país
                if (cityName.contains(",")) {
                    final String[] cityAndCountryName = cityName.split(",");
                    cityName = cityAndCountryName[0].trim();
                    cityAndCountryName[1].trim();
                }
            }
            // Se o local for uma Cidade
            if (isCity) {
                // Recuperar Cidade na base do tripfans
                // TODO incluir o país na consulta e verificar como tratar o nome do país que está em ingles
                local = this.consultarCidadePorNome(cityName, null);
                if (local == null) {
                    // TODO tratar
                    System.out.println("cidade com o nome de '" + place.getName() + "' não encontrada na base");
                }
            } else {
                // Recuperar Local na base do tripfans
                // TODO ver a questão de quanto o resultado trouxer mais de um registro
                final List<Local> locais = this.consultarLocaisPorNome(cityName);
                if (locais != null && !locais.isEmpty()) {
                    if (locais.size() > 1) {
                        System.out.println("Mais de um registro na base com o nome: " + placeName);
                    }
                    local = locais.iterator().next();
                } else {
                    System.out.println("local com o nome de '" + place.getName() + "' não encontrada na base");
                }
            }
        }
        return local;

    }

    public Local consultarLocalEquivalente(final Post post) {
        if (post instanceof CheckinPost) {
            return consultarLocalEquivalente(((CheckinPost) post).getPlace());
        }
        return null;
    }

    public Local consultarLocalEquivalente(final Venue venue) {
        Local local = null;
        final List<Local> locais = this.consultarLocaisPorNome(venue.getName());
        if (locais != null && !locais.isEmpty()) {
            if (locais.size() > 1) {
                System.out.println("Mais de um registro na base com o nome: " + venue.getName());
            }
            local = locais.iterator().next();
        } else {
            System.out.println("local com o nome de '" + venue.getName() + "' não encontrada na base");
        }
        return local;
    }

    public Local consultarLocalPorIdFacebook(final String idFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFacebook", idFacebook);
        return this.searchRepository.queryByNamedQueryUniqueResult("Local.consultarLocalPorIdFacebook", parametros);
    }

    public Local consultarLocalPorUrlPath(final String urlLocal) {
        return this.localRepository.consultarLocalPorUrlPath(urlLocal);
    }

    public List<Atracao> consultarMelhoresAtracoes(final LocalGeografico local, final int tamanhoAmostra) {
        return this.localRepository.consultarMelhoresAtracoes(local, tamanhoAmostra);
    }

    public List<Hotel> consultarMelhoresHoteis(final LocalGeografico local, final int tamanhoAmostra) {
        return this.localRepository.consultarMelhoresHoteis(local, tamanhoAmostra);
    }

    public List<LocalEnderecavel> consultarMelhoresLocaisEnderecaveis(final LocalType tipo, final LocalGeografico localGeografico,
            final int tamanhoAmostra) {
        return this.localRepository.consultarMelhoresLocaisEnderecaveis(tipo, localGeografico, tamanhoAmostra);
    }

    public List<Restaurante> consultarMelhoresRestaurantes(final LocalGeografico local, final int tamanhoAmostra) {
        return this.localRepository.consultarMelhoresRestaurantes(local, tamanhoAmostra);
    }

    public Pais consultarPais(final Long idPais) {
        return this.localRepository.consultarPais(idPais);
    }

    public List<Pais> consultarPaises(final int first, final int last) {
        return this.localRepository.consultarPaises(first, last);
    }

    public List<Pais> consultarPaisesPorNome(final String nome) {
        return this.localRepository.consultarPaisesPorNome(nome);
    }

    @Override
    public Local consultarPorId(final Long id) {
        return this.getRepository().consultarPorId(Local.class, id);
    }

    /**
     * Retorna os principais hotéis de um local baseado em avaliações. Os parceiros são inseridos nesta lista.
     *
     * @param idCidade
     *            o identificador do local
     * @return lista de {@link Hotel}
     */
    public List<Hotel> consultarPrincipaisHoteisCidade(final Long idCidade) {
        return this.localRepository.consultarPrincipaisHoteisCidade(idCidade);
    }

    /**
     * Retorna os principais restaurantes de um local baseado em avaliações. Os parceiros são inseridos nesta lista.
     *
     * @param idCidade
     *            o identificador do local
     * @return lista de {@link Restaurante}
     */
    public List<Restaurante> consultarPrincipaisRestaurantesCidade(final Long idCidade) {
        return this.localRepository.consultarPrincipaisRestaurantesCidade(idCidade);
    }

    public Long consultarQuantidadeCidadesPorEstado(final Long idEstado) {
        return this.localRepository.consultarQuantidadeCidadesPorEstado(idEstado);
    }

    public Long consultarQuantidadeCidadesPorPais(final Long idPais) {
        return this.localRepository.consultarQuantidadeCidadesPorPais(idPais);
    }

    public Long consultarQuantidadeEstados(final Long idPais) {
        return this.localRepository.consultarQuantidadeEstados(idPais);
    }

    public Long consultarQuantidadePaises() {
        return this.localRepository.consultarQuantidadePaises();
    }

    public RegiaoTripFans consultarRegiaoPorId(final Long id) {
        final List<RegiaoTripFans> lista = this.localRepository.consultarRegioesPorIds(id);
        if (!lista.isEmpty()) {
            return lista.get(0);
        }
        return null;
    }

    public List<RegiaoTripFans> consultarRegioesPorIds(final Long[] ids) {
        return this.localRepository.consultarRegioesPorIds(ids);
    }

    public <T extends RegiaoTripFans> List<T> consultarRegioesPorNome(final String nome) {
        return this.localRepository.consultarRegioesPorNome(nome);
    }

    public List<Restaurante> consultarRestaurantes(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        return this.searchRepository.consultarPorNamedQuery("Local.consultarRestaurantesPorCidade", parametros, Restaurante.class);
    }

    public List<Restaurante> consultarRestaurantesCadastroNaoConcluido(final LocalGeografico local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (local instanceof Cidade) {
            parametros.put("localCidade", local);
        } else if (local instanceof LocalInteresseTuristico) {
            parametros.put("localInteresseTuristico", local);
        }
        parametros.put("cadastroConcluido", false);
        return this.searchRepository.consultarPorNamedQuery("Local.consultarRestaurantesPorCidade", parametros, Restaurante.class);
    }

    public <T extends Local> List<T> consultarRestaurantesPorNome(final String nome) {
        return this.localRepository.consultarLocaisPorNome(nome, LocalType.RESTAURANTE);
    }

    public List<TipoServicoHotel> consultarServicosDisponiveis(final Hotel hotel) {
        return this.localRepository.consultarServicosDisponiveis(hotel);
    }

    private void copiarDadosGooglePlaceParaLocal(final Place place, final Local local) {
        local.setIdGooglePlaces(place.getId());
        LocalGoogle localGoogle = local.getLocalGoogle();

        if (localGoogle == null) {
            localGoogle = new LocalGoogle();
        }

        localGoogle.setAdministrativeAreaLevel1(place.getAddress().getAdminAreaL1());
        localGoogle.setAdministrativeAreaLevel1Abbr(place.getAddress().getAdminAreaL1Abbr());
        localGoogle.setAdministrativeAreaLevel2(place.getAddress().getAdminAreaL2());
        localGoogle.setAdministrativeAreaLevel2Abbr(place.getAddress().getAdminAreaL2Abbr());
        localGoogle.setCountry(place.getAddress().getCountry());
        localGoogle.setCountryAbbr(place.getAddress().getCountryAbbr());
        localGoogle.setFormattedAddress(place.getFormattedAddress());
        localGoogle.setFormattedPhoneNumber(place.getFormattedPhoneNumber());
        localGoogle.setIcon(place.getIcon());
        localGoogle.setId(place.getId());
        localGoogle.setInternationalPhoneNumber(place.getIntlPhoneNumber());
        localGoogle.setLatitude(place.getLatitude());
        localGoogle.setLocality(place.getAddress().getLocality());
        localGoogle.setLocalityAbbr(place.getAddress().getLocalityAbbr());
        localGoogle.setLongitude(place.getLongitude());
        localGoogle.setName(place.getName());
        if (place.getOpeningHours() != null && !place.getOpeningHours().isEmpty()) {
            String openingHoursConcat = "";
            for (final OpeningHours openingHours : place.getOpeningHours()) {
                openingHoursConcat += "{ ";
                openingHoursConcat += "'open': { 'day': '" + openingHours.getOpenDay() + "', 'time': '" + openingHours.getOpenHour() + ":"
                        + openingHours.getOpenMinute() + "' }";
                openingHoursConcat += "'close': { 'day': '" + openingHours.getCloseDay() + "', 'time': '" + openingHours.getCloseHour() + ":"
                        + openingHours.getCloseMinute() + "' }";
                openingHoursConcat += "}, ";
            }
            localGoogle.setOpeningHours(openingHoursConcat);
        }
        localGoogle.setPostalCode(place.getAddress().getPostalCode());
        localGoogle.setPostalCodeAbbr(place.getAddress().getPostalCodeAbbr());
        localGoogle.setPostalTown(place.getAddress().getPostalTown());
        localGoogle.setPostalTownAbbr(place.getAddress().getPostalTownAbbr());
        localGoogle.setPriceLevel(place.getPriceLevel() > -1 ? place.getPriceLevel() : null);
        localGoogle.setRating(place.getRating() > -1.0 ? new Double(place.getRating()) : null);
        localGoogle.setReference(place.getReference());
        localGoogle.setRoute(place.getAddress().getRoute());
        localGoogle.setRouteAbbr(place.getAddress().getRouteAbbr());
        localGoogle.setStreetNumber(place.getAddress().getStreetNumber());
        localGoogle.setStreetNumberAbbr(place.getAddress().getStreetNumberAbbr());
        localGoogle.setSublocality(place.getAddress().getSublocality());
        localGoogle.setSublocalityAbbr(place.getAddress().getSublocalityAbbr());

        if (place.getTypes() != null && !place.getTypes().isEmpty()) {
            String types = "";
            for (final String type : place.getTypes()) {
                if (!types.equals("")) {
                    types += ",";
                }
                types += type;
            }
            localGoogle.setTypes(types);
        }

        localGoogle.setUrl(place.getUrl());
        localGoogle.setVicinity(place.getVicinity());
        localGoogle.setWebsite(place.getWebsite());

        local.setLocalGoogle(localGoogle);
    }

    private void copiarDadosVenueFoursquareParaLocal(final Venue venue, final Local local) {
        local.setIdFoursquare(venue.getId());
        LocalFoursquare localFoursquare = local.getLocalFoursquare();

        if (localFoursquare == null) {
            localFoursquare = new LocalFoursquare();
        }

        // Dados basicos
        localFoursquare.setId(venue.getId());
        localFoursquare.setNome(venue.getName());
        localFoursquare.setDescricao(venue.getDescription());
        localFoursquare.setUrl(venue.getUrl());
        localFoursquare.setShortUrl(venue.getShortUrl());

        // Dados de localizacao
        localFoursquare.setEndereco(venue.getLocation().getAddress());
        localFoursquare.setComplementoEndereco(venue.getLocation().getCrossStreet());
        localFoursquare.setDistanciaDoLocalOriginal(venue.getLocation().getDistance());
        localFoursquare.setLatitude(venue.getLocation().getLatitude());
        localFoursquare.setLongitude(venue.getLocation().getLongitude());
        localFoursquare.setCep(venue.getLocation().getPostalCode());
        // Dados de contato
        localFoursquare.setEmail(venue.getContactInfo().getEmail());
        localFoursquare.setFacebook(venue.getContactInfo().getFacebook());
        localFoursquare.setTelefone(venue.getContactInfo().getPhone());
        localFoursquare.setTelefoneFormatado(venue.getContactInfo().getFormattedPhone());
        localFoursquare.setTwitter(venue.getContactInfo().getTwitter());
        // Dados de estatisticas
        // localFoursquare.set

        localFoursquare.setCheckinsCount(venue.getStats().getCheckinsCount());
        localFoursquare.setUsersCount(venue.getStats().getUsersCount());
        // Alterar o .jar do foursquare-social para contemplar esse atributo
        // localFoursquare.setTipCount(venue.getStats().getUsersCount());

        // Dados de categoria
        if (!venue.getCategories().isEmpty()) {
            final Category mainCategory = venue.getCategories().get(0);
            localFoursquare.setIdCategoria(mainCategory.getId());
            localFoursquare.setNomeCategoria(mainCategory.getName());

            String idsCategoria = "";
            String nomesCategoria = "";
            for (final Category category : venue.getCategories()) {
                if (!idsCategoria.equals("")) {
                    idsCategoria += ",";
                    nomesCategoria += ",";
                }
                idsCategoria += category.getId();
                nomesCategoria += category.getName();
            }

            localFoursquare.setIdsTodasCategorias(idsCategoria);
            localFoursquare.setNomesTodasCategorias(nomesCategoria);

            // localFoursquare.setSiglaCategoria(category.get);
        }
        local.setLocalFoursquare(localFoursquare);
    }

    @Transactional
    public void corresponderLocaisComFoursquare() {
        // final Local local = this.consultarPorId(10174L);
        // final Local cidade = this.consultarPorId(1197L);

        final Local cidade = this.consultarPorId(1072L);

        final List<LocalEnderecavel> locais = this.consultarLocaisEnderecaveis((Cidade) cidade);

        for (final Local local : locais) {
            try {
                this.corresponderLocalComFoursquare(local);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    public void corresponderLocalComFoursquare(final Local local) {
        if (local.getLocalFoursquare() == null) {
            local.setLocalFoursquare(new LocalFoursquare());
        }
        final List<Venue> venues = this.foursquareService.recuperarLocaisSemelhantesNoFoursquare(local);
        if (!venues.isEmpty()) {

            final List<VenueComparable> lista = new ArrayList<VenueComparable>();

            Venue venueComMaiorSemelhancaNoNome = null;
            String nomeVenue = null;
            final String nomeLocal = Stopwords.removeStopWords(local.getNome());
            final int maiorQuantidade = 0;

            // Pegar o primeiro
            final Venue primeiroVenue = venues.get(0);

            // Varrer os locais e tentar encontrar o que tem o nome mais parecido
            for (final Venue venue : venues) {
                System.out.println("\n###\n Nome do local antes: " + venue.getName());
                nomeVenue = Stopwords.removeStopWords(venue.getName());
                System.out.println("Nome do local depois: " + nomeVenue);
                final int quantidade = contarPalavrasIguais(nomeLocal, nomeVenue);
                if (quantidade > 0) {
                    lista.add(new VenueComparable(venue, quantidade));
                }
            }

            if (!lista.isEmpty()) {

                Collections.sort(lista);

                // Pegar o ultimo
                venueComMaiorSemelhancaNoNome = lista.get(lista.size() - 1).getVenue();
                // Setar a quantidade de locais que o foursquare retornou
                local.getLocalFoursquare().setQuantidadeLocaisSemelhantes(lista.size());

                local.getLocalFoursquare().setLocalNaoEncontrado(false);

                if (venueComMaiorSemelhancaNoNome != null) {
                    if (venueComMaiorSemelhancaNoNome.getId().equals(primeiroVenue.getId())) {
                        local.getLocalFoursquare().setLocalCombinouDePrimeira(true);
                    } else {
                        local.getLocalFoursquare().setLocalCombinouDePrimeira(false);
                    }
                    copiarDadosVenueFoursquareParaLocal(venueComMaiorSemelhancaNoNome, local);
                } else {
                    copiarDadosVenueFoursquareParaLocal(primeiroVenue, local);
                }
            }

        } else {
            System.out.println("Local não encontrado no no Foursquare: " + local.getNomeCompleto());
            local.setLocalFoursquare(new LocalFoursquare());
            local.getLocalFoursquare().setLocalNaoEncontrado(true);
        }
        this.salvar(local);
    }

    @Transactional
    public void incluirNovoLocal(final LocalEnderecavel local) {
        local.setValidado(false);
        local.setParceiro(false);
        local.setDataCadastro(new LocalDateTime());
        this.incluir(local);
        this.emailService.enviarEmail(
                "suporte@tripfans.com.br",
                "suporte@tripfans.com.br",
                "Novo Local cadastrado na base",
                "<p>Foi cadastrado um novo local na base do TripFans.</p><br/>ID do local: <strong>" + local.getId()
                        + "</strong><br/>Cidade: <strong>" + local.getLocalizacao().getCidadeNome() + "</strong><br/>Nome do Local: <strong>"
                        + local.getNome() + "</strong><br/>ID do Usuario que cadastrou: <strong>" + local.getIdUsuarioSolcitante() + "</strong>",
                new HashMap());
    }

}
