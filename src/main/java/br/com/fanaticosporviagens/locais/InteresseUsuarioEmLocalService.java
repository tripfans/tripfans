package br.com.fanaticosporviagens.locais;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.CheckinPost;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.foursquare.FoursquareService;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.MarcacoesUsuarioLocal;
import br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Service
public class InteresseUsuarioEmLocalService extends GenericCRUDService<InteresseUsuarioEmLocal, Long> {

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private FoursquareService foursquareService;

    @Autowired
    private InteresseUsuarioEmLocalRepository interesseUsuarioEmLocalRepository;

    @Autowired
    private LocalService localService;

    private final Log log = LogFactory.getLog(InteresseUsuarioEmLocalService.class);

    @Autowired
    private MarcacoesUsuarioLocal marcacoesUsuarioLocal;

    @Autowired
    protected GenericSearchRepository searchRepository;

    public List<InteresseAmigosEmLocal> consultarInteressesAmigosEmLocais(final Usuario usuarioAmigo, final List<Local> locais,
            final TipoInteresseUsuarioEmLocal tipoInteresse, final int tamanhoAmostra) {
        final List<InteresseAmigosEmLocal> listaInteresses = new ArrayList<InteresseAmigosEmLocal>();
        for (final Local local : locais) {
            listaInteresses.addAll(this.consultarInteressesAmigosEmLocal(usuarioAmigo, local, new TipoInteresseUsuarioEmLocal[] { tipoInteresse },
                    tamanhoAmostra));
        }
        return listaInteresses;
    }

    public List<InteresseAmigosEmLocal> consultarInteressesAmigosEmLocal(final Usuario usuarioAmigo, final Local local,
            final TipoInteresseUsuarioEmLocal[] tipos, final int tamanhoAmostra) {
        return this.interesseUsuarioEmLocalRepository.consultarInteressesAmigos(usuarioAmigo, local, tipos, tamanhoAmostra);
    }

    /**
     * Consultar interesses que um usuário tem em Cidades
     *
     * @param idUsuario
     * @return
     */
    public List<InteresseUsuarioEmLocal> consultarInteressesUsuarioEmCidades(final Long idUsuario) {
        return this.consultarInteressesUsuarioEmCidades(idUsuario, null);
    }

    /**
     * Consultar interesses que um usuário tem na Cidade
     *
     * @param idUsuario
     * @param idCidade
     * @return
     */
    public List<InteresseUsuarioEmLocal> consultarInteressesUsuarioEmCidades(final Long idUsuario, final Long idCidade) {
        return this.consultarInteressesUsuarioEmDestinos(idUsuario, idCidade, true);
    }

    public List<InteresseUsuarioEmLocal> consultarInteressesUsuarioEmDestinos(final Long idUsuario, final Long idCidade,
            final boolean somenteComInteresses) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", idUsuario);
        parametros.put("idLocal", idCidade);
        parametros.put("tipoLocais", Arrays.asList(LocalType.CIDADE.getCodigo(), LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo()));
        parametros.put("somenteComInteresses", somenteComInteresses);

        return this.searchRepository.consultarPorNamedNativeQuery("InteresseUsuarioEmLocal.consultarInteressesUsuarioEmDestinos", parametros,
                InteresseUsuarioEmLocal.class);
    }

    /**
     * Realiza uma consulta de Cidades de interesse dentro de um perimetro delimitado pela Latitude / Longitude de duas coordenadas no mapa: NE -
     * Nordeste e SW - Sudoeste
     *
     * @return
     */
    public List<InteresseUsuarioEmLocal> consultarInteressesUsuarioEmDestinosPorPerimetro(final Long idUsuario,
            final CoordenadaGeografica coordenadaNE, final CoordenadaGeografica coordenadaSW) {

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", idUsuario);
        parametros.put("tipoLocais", Arrays.asList(LocalType.CIDADE.getCodigo(), LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo()));

        parametros.put("latitudeNE", coordenadaNE.getLatitude());
        parametros.put("longitudeNE", coordenadaNE.getLongitude());
        parametros.put("latitudeSW", coordenadaSW.getLatitude());
        parametros.put("longitudeSW", coordenadaSW.getLongitude());

        return this.searchRepository.consultarPorNamedNativeQuery("InteresseUsuarioEmLocal.consultarInteressesUsuarioEmDestinosPorPerimetro",
                parametros, InteresseUsuarioEmLocal.class);
    }

    public InteresseUsuarioEmLocal consultarInteressesUsuarioEmLocal(final Usuario usuario, final Local local) {
        return this.interesseUsuarioEmLocalRepository.consultarInteresses(usuario, local);
    }

    public InteresseAmigosEmLocal consultarInteresseTodosAmigosEmLocal(final Usuario usuarioAmigo, final Local local,
            final TipoInteresseUsuarioEmLocal tipoInteresse) {
        return this.interesseUsuarioEmLocalRepository.consultarInteresseTodosAmigos(usuarioAmigo, local, tipoInteresse);
    }

    public List<InteresseAmigosEmLocal> consultarTodosInteressesAmigosEmLocal(final Usuario usuarioAmigo, final Local local) {
        return this.interesseUsuarioEmLocalRepository.consultarInteressesAmigos(usuarioAmigo, local);
    }

    public List<InteresseUsuarioEmLocal> consultarTodosInteressesUsuario(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);

        return this.searchRepository.consultarPorNamedQuery("InteresseUsuarioEmLocal.consultarTodosInteressesUsuario", parametros,
                InteresseUsuarioEmLocal.class);
    }

    @Transactional
    public void desmarcarInteresse(final Local local, final Usuario usuario, final TipoInteresseUsuarioEmLocal tipoInteresse) {
        if (this.log.isDebugEnabled()) {
            this.log.debug(String.format("Desmarcando interesse em %s para usuário %s", local.getNome(), usuario.getNome()));
        }
        final InteresseUsuarioEmLocal interesse = this.interesseUsuarioEmLocalRepository.consultarInteresses(usuario, local);

        // TODO marcar toda a hierarquia
        interesse.setInteresse(tipoInteresse, Boolean.FALSE);

        salvar(interesse);

    }

    @Transactional
    public void desmarcarInteresseDesejaIr(final Local local, final Usuario usuario) {
        desmarcarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.DESEJA_IR);
    }

    @Transactional
    public void desmarcarInteresseJaFoi(final Local local, final Usuario usuario) {
        desmarcarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.JA_FOI);
    }

    @Transactional
    public void desmarcarInteresseLocalFavorito(final Local local, final Usuario usuario) {
        this.desmarcarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.FAVORITO);
    }

    @Transactional
    public void desmarcarInteresseSeguir(final Local local, final Usuario usuario) {
        desmarcarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.SEGUIR);
    }

    @Transactional
    public void popularMarcacoesUsuarioLocal(final Usuario usuario) {
        final List<InteresseUsuarioEmLocal> interesses = this.consultarTodosInteressesUsuario(usuario);
        this.marcacoesUsuarioLocal.inicializar(interesses);
    }

    @Override
    protected void posAlteracao(final InteresseUsuarioEmLocal interesse) {
        if (interesse.getUsuario() != null && interesse.getUsuario().getId().equals(getUsuarioAutenticado().getId())) {
            this.marcacoesUsuarioLocal.atualizaMarcacao(interesse);
        }
        // guardarInteresseAnterior(interesse);
    }

    @Override
    protected void posInclusao(final InteresseUsuarioEmLocal interesse) {
        if (interesse.getUsuario() != null && interesse.getUsuario().getId().equals(getUsuarioAutenticado().getId())) {
            this.marcacoesUsuarioLocal.atualizaMarcacao(interesse);
        }
        // guardarInteresseAnterior(interesse);
    }

    @Transactional
    public void publicarInteresse(final Avaliacao avaliacao) {
        InteresseUsuarioEmLocal interesse = consultarInteressesUsuarioEmLocal(avaliacao.getAutor(), avaliacao.getLocalAvaliado());
        if (interesse == null) {
            interesse = new InteresseUsuarioEmLocal(avaliacao.getLocalAvaliado(), avaliacao.getAutor(), avaliacao.getAutor().getIdFacebook(),
                    avaliacao.getAutor().getDisplayName());
            interesse.setEscreveuAvaliacao(Boolean.TRUE);
            interesse.setJaFoi(Boolean.TRUE);

            incluir(interesse);
        } else if (!interesse.getJaFoi() || !interesse.getEscreveuAvaliacao()) {
            interesse.setEscreveuAvaliacao(Boolean.TRUE);
            interesse.setJaFoi(Boolean.TRUE);

            alterar(interesse);
        }

        publicarInteresseJaFoi(avaliacao.getAutor(), avaliacao.getLocalAvaliado());
    }

    @Transactional
    public void publicarInteresse(final Dica dica) {
        if (dica.getAutor() != null) {
            // autor pode ser nulo (dica vinda do foursquare de usuário que não está no TF); nesse caso, não registra
            this.registrarInteresse(dica.getLocalDica(), dica.getAutor(), TipoInteresseUsuarioEmLocal.ESCREVEU_DICA);
        }
    }

    @Transactional
    public void publicarInteresseJaFoi(final Usuario usuario, final Local local) {

        if (local != null) {
            final List<Local> locais = Arrays.asList(local);

            final List<InteresseUsuarioEmLocal> interesses = this.interesseUsuarioEmLocalRepository.consultarInteresses(usuario, locais);

            final HashMap<Local, InteresseUsuarioEmLocal> mapInteresses = new HashMap<Local, InteresseUsuarioEmLocal>();
            for (final InteresseUsuarioEmLocal interesse : interesses) {
                mapInteresses.put(interesse.getLocal(), interesse);
            }

            for (final Local localInteresse : locais) {
                if (mapInteresses.containsKey(localInteresse)) {
                    final InteresseUsuarioEmLocal interesseJaExistente = mapInteresses.get(localInteresse);
                    if (!interesseJaExistente.getJaFoi()) {
                        interesseJaExistente.setJaFoi(Boolean.TRUE);
                        alterar(interesseJaExistente);
                    }
                } else {
                    final InteresseUsuarioEmLocal novoInteresse = new InteresseUsuarioEmLocal(localInteresse, usuario, usuario.getIdFacebook(),
                            usuario.getDisplayName());
                    novoInteresse.setJaFoi(Boolean.TRUE);
                    incluir(novoInteresse);
                }
            }
        }
    }

    @Transactional
    // TODO unificar esse método com o método publicarInteresseJaFoi(final Usuario usuario, final Local local)
    // está duplicado
    public void publicarInteresseJaFoiUsuarioFacebook(final String idUsuarioFacebook, final String nomeUsuarioFacebook, final Local local) {
        if (this.log.isDebugEnabled()) {
            this.log.debug(String.format("Registrando interesse em %s para amigo Facebook %s", local.getNome(), nomeUsuarioFacebook));
        }

        final List<Local> locais = local.getTodosPais();
        locais.add(local);

        if (this.log.isDebugEnabled()) {
            this.log.debug("Antes consulta...");
        }

        final List<InteresseUsuarioEmLocal> interesses = this.interesseUsuarioEmLocalRepository.consultarInteresses(idUsuarioFacebook, locais);

        if (this.log.isDebugEnabled()) {
            this.log.debug("Depois da consulta...");
        }

        final HashMap<Local, InteresseUsuarioEmLocal> mapInteresses = new HashMap<Local, InteresseUsuarioEmLocal>();
        for (final InteresseUsuarioEmLocal interesse : interesses) {
            mapInteresses.put(interesse.getLocal(), interesse);
        }

        for (final Local localInteresse : locais) {
            if (mapInteresses.containsKey(localInteresse)) {
                final InteresseUsuarioEmLocal interesseJaExistente = mapInteresses.get(localInteresse);
                if (!interesseJaExistente.getJaFoi()) {
                    interesseJaExistente.setJaFoi(Boolean.TRUE);
                    alterar(interesseJaExistente);
                }
            } else {
                final InteresseUsuarioEmLocal novoInteresse = new InteresseUsuarioEmLocal(localInteresse, null, idUsuarioFacebook,
                        nomeUsuarioFacebook);
                novoInteresse.setJaFoi(Boolean.TRUE);
                incluir(novoInteresse);
            }
        }
        if (this.log.isDebugEnabled()) {
            this.log.debug("Saindo de publicarInteresseJaFoiUsuarioFacebook");
        }
    }

    @Transactional
    public void registrarInteresse(final Local local, final Usuario usuario, final TipoInteresseUsuarioEmLocal tipoInteresse) {
        if (this.log.isDebugEnabled()) {
            this.log.debug(String.format("Registrando interesse em %s para usuário %s", local.getNome(), usuario.getNome()));
        }
        InteresseUsuarioEmLocal interesse = this.interesseUsuarioEmLocalRepository.consultarInteresses(usuario, local);

        if (interesse == null) {
            interesse = new InteresseUsuarioEmLocal(local, usuario, usuario.getIdFacebook(), usuario.getDisplayName());
        }

        // TODO marcar toda a hierarquia
        interesse.setInteresse(tipoInteresse, Boolean.TRUE);

        salvar(interesse);

    }

    private void registrarInteresseAmigoEmLocalAPartirDoFacebook(final String idAmigoFacebook, final String nomeAmigoFacebook, final Post post) {
        final Local local = this.localService.consultarLocalEquivalente(post);
        if (local != null) {
            this.publicarInteresseJaFoiUsuarioFacebook(idAmigoFacebook, nomeAmigoFacebook, local);
        } else if (this.log.isDebugEnabled()) {
            this.log.debug("local com o nome de '" + ((CheckinPost) post).getPlace().getName() + "' não encontrada na base");
        }
    }

    private void registrarInteresseAmigoEmLocalAPartirDoFoursquare(final String idAmigoFoursquare,
            final org.springframework.social.foursquare.api.Checkin checkin) {
        final Local local = this.localService.consultarLocalEquivalente(checkin);
        /*if (local != null) {
            this.registrarInteresseJaFoi(local, usuario);
        } else {
            System.out.println("local com o nome de '" + (checkin).getVenue().getName() + "' não encontrada na base");
        }*/
    }

    @Transactional
    public void registrarInteresseDesejaIr(final Local local, final Usuario usuario) {
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.DESEJA_IR);
    }

    private void registrarInteresseEmLocalAPartirDoFacebook(final Usuario usuario, final CheckinPost checkin) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Registrando interesse em local " + checkin.checkinId());
        }
        final Local local = this.localService.consultarLocalEquivalente(checkin);
        if (local != null) {
            this.publicarInteresseJaFoi(usuario, local);
        } else if (this.log.isDebugEnabled()) {
            this.log.debug("local com o nome de '" + checkin.getPlace().getName() + "' não encontrada na base");
        }

    }

    private void registrarInteresseEmLocalAPartirDoFoursquare(final Usuario usuario, final org.springframework.social.foursquare.api.Checkin checkin) {
        final Local local = this.localService.consultarLocalEquivalente(checkin);
        if (local != null) {
            this.publicarInteresseJaFoi(usuario, local);
        } else {
            if (this.log.isDebugEnabled()) {
                this.log.debug("local com o nome de '" + checkin.getVenue().getName() + "' não encontrada na base");
            }
        }

    }

    @Transactional
    public void registrarInteresseImperdivel(final Local local, final Usuario usuario) {
        registrarInteresseIndica(local, usuario);
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.IMPERDIVEL);
    }

    @Transactional
    public void registrarInteresseIndica(final Local local, final Usuario usuario) {
        publicarInteresseJaFoi(usuario, local);
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.INDICA);
    }

    @Transactional
    public void registrarInteresseJaFoi(final Local local, final Usuario usuario) {
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.JA_FOI);
    }

    @Transactional
    public void registrarInteresseLocalFavorito(final Local local, final Usuario usuario) {
        publicarInteresseJaFoi(usuario, local);
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.FAVORITO);
    }

    private void registrarInteressesAmigoEmLocaisAPartirDoFacebook(final String idAmigoFacebook, final String nomeAmigoFacebook,
            final List<CheckinPost> locationPosts) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Registrando interesse em local do amigo " + idAmigoFacebook);
        }
        for (final Post post : locationPosts) {
            registrarInteresseAmigoEmLocalAPartirDoFacebook(idAmigoFacebook, nomeAmigoFacebook, post);
        }
    }

    public void registrarInteressesAmigoEmLocaisAPartirDoFacebook(final Usuario usuario, final String idAmigoFacebook,
            final String nomeUsuarioFacebook) {
        final List<CheckinPost> locationPosts = this.facebookService.consultarCheckinsAmigo(usuario, idAmigoFacebook);
        if (!locationPosts.isEmpty()) {
            this.registrarInteressesAmigoEmLocaisAPartirDoFacebook(idAmigoFacebook, nomeUsuarioFacebook, locationPosts);
        }
    }

    private void registrarInteressesAmigoEmLocaisAPartirDoFoursquare(final String idAmigoFoursquare,
            final List<org.springframework.social.foursquare.api.Checkin> checkins) {
        for (final org.springframework.social.foursquare.api.Checkin checkin : checkins) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("checkin amigo : " + checkin.getVenue().getName());
            }
            registrarInteresseAmigoEmLocalAPartirDoFoursquare(idAmigoFoursquare, checkin);
        }
    }

    public void registrarInteressesAmigoEmLocaisAPartirDoFoursquare(final Usuario usuario, final String idAmigoFoursquare) {
        final List<org.springframework.social.foursquare.api.Checkin> checkins = this.foursquareService.consultarCheckinsAmigo(usuario,
                idAmigoFoursquare);
        this.registrarInteressesAmigoEmLocaisAPartirDoFoursquare(idAmigoFoursquare, checkins);
    }

    /*private void guardarInteresseAnterior(final InteresseUsuarioEmLocal interesseUsuarioEmLocal) {
        interesseUsuarioEmLocal.setDesejaIrAnterior(interesseUsuarioEmLocal.getDesejaIr());
        interesseUsuarioEmLocal.setFavoritoAnterior(interesseUsuarioEmLocal.getFavorito());
        interesseUsuarioEmLocal.setImperdivelAnterior(interesseUsuarioEmLocal.getImperdivel());
        interesseUsuarioEmLocal.setIndicaAnterior(interesseUsuarioEmLocal.getIndica());
        interesseUsuarioEmLocal.setJaFoiAnterior(interesseUsuarioEmLocal.getJaFoi());
    }*/

    @Transactional
    public void registrarInteresseSeguir(final Local local, final Usuario usuario) {
        registrarInteresse(local, usuario, TipoInteresseUsuarioEmLocal.SEGUIR);
    }

    private void registrarInteressesEmLocaisAPartirDoFacebook(final Usuario usuario, final List<CheckinPost> checkins) {
        for (final CheckinPost checkin : checkins) {
            registrarInteresseEmLocalAPartirDoFacebook(usuario, checkin);
        }
    }

    private void registrarInteressesEmLocaisAPartirDoFoursquare(final Usuario usuario,
            final List<org.springframework.social.foursquare.api.Checkin> checkins) {
        for (final org.springframework.social.foursquare.api.Checkin checkin : checkins) {
            registrarInteresseEmLocalAPartirDoFoursquare(usuario, checkin);
        }
    }

    @Transactional
    public void registrarInteressesUsuarioEmLocaisAPartirDeRedesSociaisExternas(final Usuario usuario) {
        if (Boolean.TRUE.equals(usuario.getConectadoFacebook())) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Registar interesses em locais a partir do Face " + usuario.getUsername());
            }
            registrarInteressesUsuarioEmLocaisAPartirDoFacebook(usuario);
        }
        if (Boolean.TRUE.equals(usuario.getConectadoFoursquare())) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Registar interesses em locais a partir do Foursquare " + usuario.getUsername());
            }
            registrarInteressesUsuarioEmLocaisAPartirDoFoursquare(usuario);
        }

        // Registra o momento que foi feita a ultima verificacao dos checkins
        // TODO ver a questao do fuso-horario
        usuario.setDataUltimaVerificacaoCheckins(new LocalDate());
    }

    public void registrarInteressesUsuarioEmLocaisAPartirDoFacebook(final Usuario usuario) {
        final List<CheckinPost> checkins = this.facebookService.consultarCheckinsUsuario(usuario);
        if (this.log.isDebugEnabled()) {
            this.log.debug("Checkins do usuario ");
        }
        if (checkins != null && !checkins.isEmpty()) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Vai registrar interesses em locais a partir do FB " + usuario.getUsername());
            }
            registrarInteressesEmLocaisAPartirDoFacebook(usuario, checkins);
            if (this.log.isDebugEnabled()) {
                this.log.debug(String.format("Interesses do usuario %s registrados ", usuario.getUsername()));
            }
        }
    }

    public void registrarInteressesUsuarioEmLocaisAPartirDoFoursquare(final Usuario usuario) {
        final List<org.springframework.social.foursquare.api.Checkin> checkins = this.foursquareService.consultarCheckinsUsuario(usuario);
        if (checkins != null && !checkins.isEmpty()) {
            registrarInteressesEmLocaisAPartirDoFoursquare(usuario, checkins);
        }
    }

    @Transactional
    public void salvarInteressesUsuarioEmLocais(final List<InteresseUsuarioEmLocal> interessesUsuarioEmLocais) {
        for (final InteresseUsuarioEmLocal interesseEmLocal : interessesUsuarioEmLocais) {
            // salvarLocalUsuario(interesseEmLocal);
        }
    }

    @Transactional
    // TODO verificar a necessidade desse metodo
    public InteresseUsuarioEmLocal salvarInteresseUsuarioEmLocal(final InteresseUsuarioEmLocal interesseEmLocal) {
        // Se o ID for null
        if (interesseEmLocal.getId() == null) {
            // Tenta recuperar o InteresseUsuarioEmLocal pela cidade/usuario
            final List<InteresseUsuarioEmLocal> interessesUsuario = this.consultarInteressesUsuarioEmDestinos(interesseEmLocal.getUsuario().getId(),
                    interesseEmLocal.getLocal().getId(), false);
            if (interessesUsuario != null && !interessesUsuario.isEmpty()) {
                final InteresseUsuarioEmLocal interesseConsultado = this.consultarPorId(interessesUsuario.iterator().next().getId());

                interesseConsultado.setDesejaIr(interesseEmLocal.getDesejaIr());
                interesseConsultado.setFavorito(interesseEmLocal.getFavorito());
                interesseConsultado.setImperdivel(interesseEmLocal.getImperdivel());
                interesseConsultado.setIndica(interesseEmLocal.getIndica());
                interesseConsultado.setJaFoi(interesseEmLocal.getJaFoi());

                // interesseEmLocal.setId(interesseConsultado.getId());
                alterar(interesseConsultado);
            } else {
                try {
                    incluir(interesseEmLocal);
                } catch (final ConstraintViolationException e) {

                }
            }
        }
        // - Verificar se o usuario nao possui interesses
        // if (interesseEmLocal.getId() != null && interesseEmLocal.semNenhumInteresse()) {
        // Se não tiver, excluir o objeto InteresseUsuarioEmLocal
        // interesseEmLocal = consultarPorId(interesseEmLocal.getId());
        // excluir(interesseEmLocal);
        // return null;
        // } else {
        // Se não, apenas /incluir/alterar o local
        else if (interesseEmLocal.getId() != null) {
            alterar(interesseEmLocal);
        }
        // }
        return interesseEmLocal;
    }
}
