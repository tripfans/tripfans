package br.com.fanaticosporviagens.locais;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Agencia;
import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Estado;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.Pais;
import br.com.fanaticosporviagens.model.entity.Restaurante;
import br.com.fanaticosporviagens.model.entity.TipoServicoHotel;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.tripfans.importador.RegiaoTripFans;

/**
 * @author André Thiago
 *
 *
 */
@Repository
public class LocalRepository extends GenericCRUDRepository<Local, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<Agencia> consultarAgenciasPorNome(final String nome) {
        final String select = "SELECT e.id_local as id, e.nome as nome, e.descricao as descricao, e.telefone as telefone, e.site as site FROM local e "
                + " WHERE upper(e.nome) like '%' || upper(:nome) || '%' AND e.local_type = :local_type_hotel";

        final Map<String, Object> parametros = new HashMap<String, Object>();
        this.searchData.setStart(0);
        this.searchData.setLimit(20);
        parametros.put("nome", nome);
        parametros.put("local_type_hotel", LocalType.AGENCIA.getCodigo());

        return this.searchRepository.consultaSQL(select, parametros, Agencia.class);
    }

    @SuppressWarnings("unchecked")
    public List<Atracao> consultarAtracoes(final int first, final int last) {
        final String select = "select * from local where local_type = :local_type order by local.nome";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Atracao.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.ATRACAO.getCodigo());
        return query.list();
    }

    public List<LocalEnderecavelView> consultarAtracoesDestaques(final Long idDestino) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idDestino", idDestino);
        parametros.put("tipoLocal", LocalType.ATRACAO.getCodigo());
        return this.searchRepository.consultarPorNamedNativeQuery("Local.consultarLocaisEnderecaveisViewDestaques", parametros,
                LocalEnderecavelView.class);
    }

    public Cidade consultarCidadePorId(final Long id) {
        final String select = "select * from local where local_type = :local_type and id_local = :id ";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Cidade.class);
        query.setInteger("local_type", LocalType.CIDADE.getCodigo());
        query.setLong("id", id);
        return (Cidade) query.uniqueResult();
    }

    public Cidade consultarCidadePorNome(final String nome, final Pais pais) {
        final List<Cidade> cidades = consultarCidadesPorNome(nome, pais, null);
        if (!cidades.isEmpty()) {
            return cidades.iterator().next();
        }
        return null;
    }

    public List<Cidade> consultarCidades(final Pais pais) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select cidade ");
        hql.append("  from Cidade cidade ");
        hql.append(" where cidade.pais = :pais ");
        hql.append(" order by cidade.notaRanking DESC, cidade.nome ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("pais", pais);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Cidade> consultarCidadesPopularesComAmigosJaForam(final int tamanhoAmostra, final Usuario usuario, final int tamanhoAmostraAmigos) {
        final StringBuilder hql = new StringBuilder();

        hql.append("SELECT cidade ");
        hql.append("  FROM Cidade cidade ");
        hql.append(" ORDER BY cidade.notaRanking DESC, cidade.nome ");

        final Map<String, Object> parametros = new HashMap<String, Object>();

        final List<Cidade> cidades = this.searchRepository.consultaHQL(hql.toString(), parametros, 1, tamanhoAmostra);

        // TODO Código para incluir informações de amigos

        return cidades;
    }

    @SuppressWarnings("unchecked")
    public List<Cidade> consultarCidadesPorEstado(final Long idEstado, final int first, final int last) {
        final String select = "select * from local where local_type = :local_type and id_local_estado = :idEstado order by local.notaRanking DESC, local.nome";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Cidade.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.CIDADE.getCodigo());
        query.setParameter("idEstado", idEstado);
        return query.list();
    }

    public List<Cidade> consultarCidadesPorNome(final String nome, final Pais pais, final Estado estado) {
        String hql = "SELECT cidade FROM Local cidade WHERE cidade.tipoLocal = :local_type_cidade AND upper(cidade.nome) like '%' || :nome || '%'";
        if (pais != null) {
            hql += " AND cidade.pais = :pais ";
        }
        if (estado != null) {
            hql += " AND cidade.estado = :estado ";
        }
        hql += " ORDER BY cidade.notaRanking DESC, cidade.nome ";

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("nome", nome.toUpperCase().trim());
        parametros.put("local_type_cidade", LocalType.CIDADE);

        if (pais != null) {
            parametros.put("pais", pais);
        }
        if (estado != null) {
            parametros.put("estado", estado);
        }
        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    @SuppressWarnings("unchecked")
    public List<Cidade> consultarCidadesPorPais(final Long idPais, final int first, final int last) {
        final String select = "select * from local where local_type = :local_type and id_local_pais = :idPais order by local.notaRanking DESC, local.nome ";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Cidade.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.CIDADE.getCodigo());
        query.setParameter("idPais", idPais);
        return query.list();
    }

    public DestinoView consultarDestinoPorUrlPath(final String urlPath) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("urlPath", urlPath);
        final List<DestinoView> destinosDestaques = this.searchRepository.consultarPorNamedNativeQuery("Local.consultarDestinoPorUrlPath",
                parametros, DestinoView.class);
        if (CollectionUtils.isNotEmpty(destinosDestaques)) {
            return destinosDestaques.get(0);
        }
        return null;
    }

    public List<DestinoView> consultarDestinosDestaques() {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        return this.searchRepository.consultarPorNamedNativeQuery("Local.consultarDestinosDestaques", parametros, DestinoView.class);
    }

    public Estado consultarEstado(final Long idEstado) {
        final String select = "select id_local as id, nome as nome from local where local_type = :local_type and id_local = :idEstado";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idEstado", idEstado);
        parametros.put("local_type", LocalType.ESTADO.getCodigo());
        return this.searchRepository.consultaSQLResultadoUnico(select, parametros, Estado.class);
    }

    @SuppressWarnings("unchecked")
    public List<Estado> consultarEstados(final Long idPais, final int first, final int last) {
        final String select = "select * from local where local_type = :local_type and id_local_pais = :idPais order by local.nome";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Estado.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.ESTADO.getCodigo());
        query.setParameter("idPais", idPais);

        return query.list();
    }

    public List<Estado> consultarEstadosPorNome(final String nome) {
        final String select = "SELECT e.id_local as id, e.nome as nome, e.url_path as \"urlPath\", p.nome as \"pais.nome\""
                + " FROM local e join local p on e.local_type = :local_type_estado and p.local_type = :local_type_pais and e.id_local_pais = p.id_local"
                + " WHERE upper(e.nome) like '%' || upper(:nome) || '%'";

        final Map<String, Object> parametros = new HashMap<String, Object>();
        this.searchData.setStart(0);
        this.searchData.setLimit(20);
        parametros.put("nome", nome);
        parametros.put("local_type_estado", LocalType.ESTADO.getCodigo());
        parametros.put("local_type_pais", LocalType.PAIS.getCodigo());

        return this.searchRepository.consultaSQL(select, parametros, Estado.class);
    }

    public List<Local> consultarFilhosMaisPopulares(final LocalGeografico localPai, final int tamanhoAmostra) {

        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (localPai.isTipoCidade()) {
            parametros.put("localCidade", localPai);
        } else if (localPai.isTipoLocalInteresseTuristico()) {
            parametros.put("localInteresseTuristico", localPai);
        }

        return this.searchRepository.consultarPorNamedQuery("Local.consultarFilhosMaisPopularesCidade", parametros, tamanhoAmostra, Local.class);
    }

    @SuppressWarnings("unchecked")
    public List<Hotel> consultarHoteis(final int first, final int last) {
        final String select = "select * from local where local_type = :local_type order by local.nome";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Hotel.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.HOTEL.getCodigo());
        return query.list();
    }

    public List<LocalEnderecavelView> consultarHoteisDestaques(final Long idDestino) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idDestino", idDestino);
        parametros.put("tipoLocal", LocalType.HOTEL.getCodigo());
        return this.searchRepository.consultarPorNamedNativeQuery("Local.consultarLocaisEnderecaveisViewDestaques", parametros,
                LocalEnderecavelView.class);
    }

    public List<Hotel> consultarHoteisPorNome(final String nome, final Long... idCidades) {
        final StringBuilder select = new StringBuilder();

        final Map<String, Object> parametros = new HashMap<String, Object>();

        select.append("  SELECT l.id_local as id,   ");
        select.append("        l.nome as nome, ");
        select.append("        l.local_type as \"tipoLocal\", ");
        select.append("        l.descricao as descricao, ");
        select.append("        l.url_foto_album, ");
        select.append("        l.url_foto_big, ");
        select.append("        l.url_foto_small, ");
        select.append("        l.endereco as \"endereco.descricao\" ");

        select.append("    FROM local l ");
        select.append("   WHERE UPPER(l.nome) LIKE '%' || UPPER(:nome) || '%' ");
        if (idCidades != null) {
            select.append(" AND l.id_local_cidade in (:idCidades) ");
            parametros.put("idCidades", idCidades);
        }
        select.append("     AND l.local_type = :local_type_hotel");

        this.searchData.setStart(0);
        this.searchData.setLimit(20);
        parametros.put("nome", nome);
        parametros.put("local_type_hotel", LocalType.HOTEL.getCodigo());

        return this.searchRepository.consultaSQL(select.toString(), parametros, Hotel.class);
    }

    public List<LocalEnderecavel> consultarLocaisEnderecaveis(final LocalType tipo, final LocalGeografico localGeografico) {
        return this.consultarLocaisEnderecaveis(tipo, localGeografico, null, null);
    }

    public List<LocalEnderecavel> consultarLocaisEnderecaveis(final LocalType tipo, final LocalGeografico localGeografico, final String nome,
            final Integer[] estrelas) {
        if (tipo != null && !tipo.isTipoLocalEnderecavel()) {
            throw new IllegalArgumentException("O tipo informado deve ser de um Local Endereçavel (" + tipo + ")");
        }

        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (tipo != null) {
            parametros.put("tipoLocal", tipo);
        }
        if (nome != null) {
            parametros.put("nome", nome);
        }
        if (estrelas != null && estrelas.length > 0) {
            parametros.put("estrelas", estrelas);
        }
        if (tipo.isTipoHotel()) {
            parametros.put("tipoHotel", true);
        } else if (tipo.isTipoRestaurante()) {
            parametros.put("tipoRestaurante", true);
        } else if (tipo.isTipoAtracao()) {

        }
        if (localGeografico.isTipoCidade()) {
            parametros.put("localCidade", localGeografico);
        } else if (localGeografico.isTipoLocalInteresseTuristico()) {
            parametros.put("localInteresseTuristico", localGeografico);
        }

        return this.searchRepository.consultarPorNamedQuery("Local.consultarLocaisEnderecaveis", parametros, LocalEnderecavel.class);
    }

    public List<Local> consultarLocaisPorNome(final String nome) {
        final StringBuilder select = new StringBuilder();

        select.append("SELECT l ");
        select.append("  FROM Local l ");
        select.append(" WHERE UPPER(l.nome) = UPPER(:nome)");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        // TODO retirar?
        // this.searchData.setStart(0);
        // this.searchData.setLimit(20);
        parametros.put("nome", nome);

        return this.searchRepository.consultaHQL(select.toString(), parametros);
    }

    @SuppressWarnings("unchecked")
    public <T extends Local> List<T> consultarLocaisPorNome(final String nome, final LocalType tipo, final Long... idCidades) {
        final StringBuilder select = new StringBuilder();
        final Map<String, Object> parametros = new HashMap<String, Object>();

        select.append("   SELECT l.id_local AS \"id\", ");
        select.append("          l.nome, ");
        select.append("          l.descricao, ");
        select.append("          l.url_foto_album, ");
        select.append("          l.url_foto_big, ");
        select.append("          l.url_foto_small, ");
        select.append("          l.descricao, ");
        // select.append("          l.numero_avaliacoes, ");
        select.append("          l.url_path ");
        select.append("     FROM local l ");
        select.append("    WHERE l.local_type in (:local_type) ");
        select.append("      AND UPPER(l.nome) LIKE '%' || UPPER(:nome) || '%' ");

        if (idCidades != null && idCidades.length > 0) {
            select.append(" AND l.id_local_cidade in (:idCidades) ");
            parametros.put("idCidades", idCidades);
        }

        select.append(" ORDER BY l.nome ");

        parametros.put("local_type", tipo.getCodigo());
        parametros.put("nome", nome);

        return (List<T>) this.searchRepository.consultaSQL(select.toString(), parametros, Local.class);
    }

    public Local consultarLocalPorUrlPath(final String urlLocal) {
        final String hql = "select local from Local local where local.urlPath = :urlPath ";

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("urlPath", urlLocal);

        return this.searchRepository.queryByHQLUniqueResult(hql, parametros);
    }

    public List<Atracao> consultarMelhoresAtracoes(final LocalGeografico localGeografico, final int tamanhoAmostra) {
        return this.searchRepository.consultarPorNamedQuery("Local.consultarMelhoresAtracoes",
                getParametrosMelhoresLocaisEnderecaveis(localGeografico, LocalType.ATRACAO), tamanhoAmostra, Atracao.class);
    }

    public List<Hotel> consultarMelhoresHoteis(final LocalGeografico localGeografico, final int tamanhoAmostra) {
        return this.searchRepository.consultarPorNamedQuery("Local.consultarMelhoresHoteis",
                getParametrosMelhoresLocaisEnderecaveis(localGeografico, LocalType.HOTEL), tamanhoAmostra, Hotel.class);
    }

    public List<LocalEnderecavel> consultarMelhoresLocaisEnderecaveis(final LocalType tipo, final LocalGeografico localGeografico,
            final int tamanhoAmostra) {
        if (!tipo.isTipoLocalEnderecavel()) {
            throw new IllegalArgumentException("O tipo informado deve ser de um Local Endereçavel (" + tipo + ")");
        }

        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (tipo.isTipoHotel()) {
            parametros.put("tipoHotel", true);
        } else if (tipo.isTipoRestaurante()) {
            parametros.put("tipoRestaurante", true);
        } else if (tipo.isTipoAtracao()) {

        }

        parametros.put("tipoLocal", tipo);

        if (localGeografico.isTipoCidade()) {
            parametros.put("localCidade", localGeografico);
        } else if (localGeografico.isTipoLocalInteresseTuristico()) {
            parametros.put("localInteresseTuristico", localGeografico);
        }

        return this.searchRepository.consultarPorNamedQuery("Local.consultarMelhoresLocaisEnderecaveis", parametros, tamanhoAmostra,
                LocalEnderecavel.class);
    }

    public List<Restaurante> consultarMelhoresRestaurantes(final LocalGeografico localGeografico, final int tamanhoAmostra) {
        return this.searchRepository.consultarPorNamedQuery("Local.consultarMelhoresRestaurantes",
                getParametrosMelhoresLocaisEnderecaveis(localGeografico, LocalType.RESTAURANTE), tamanhoAmostra, Restaurante.class);
    }

    public Pais consultarPais(final Long idPais) {
        final String select = "select id_local as id, nome as nome from local where local_type = :local_type and id_local = :idPais";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idPais", idPais);
        parametros.put("local_type", LocalType.PAIS.getCodigo());
        return this.searchRepository.consultaSQLResultadoUnico(select, parametros, Pais.class);
    }

    @SuppressWarnings("unchecked")
    public List<Pais> consultarPaises(final int first, final int last) {
        final String select = "select * from local where local_type = :local_type order by local.nome";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Pais.class);
        query.setFirstResult(first);
        query.setMaxResults(last);
        query.setInteger("local_type", LocalType.PAIS.getCodigo());

        return query.list();
    }

    public List<Pais> consultarPaisesPorNome(final String nome) {
        final String select = "SELECT p.id_local as id, p.nome as nome, p.url_path as \"urlPath\""
                + " FROM local p where p.local_type = :local_type_pais and upper(p.nome) like '%' || upper(:nome) || '%'";

        final Map<String, Object> parametros = new HashMap<String, Object>();
        this.searchData.setStart(0);
        this.searchData.setLimit(20);
        parametros.put("nome", nome);
        parametros.put("local_type_pais", LocalType.PAIS.getCodigo());

        return this.searchRepository.consultaSQL(select, parametros, Pais.class);
    }

    @SuppressWarnings("unchecked")
    public List<Hotel> consultarPrincipaisHoteisCidade(final Long idCidade) {
        final String select = "select * from local where local_type = :local_type and id_local_cidade = :idCidade "; // order by numero_avaliacoes
        // desc";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Hotel.class);
        query.setMaxResults(5);
        query.setLong("idCidade", idCidade);
        query.setInteger("local_type", LocalType.HOTEL.getCodigo());

        return query.list();
    }

    @SuppressWarnings("unchecked")
    public List<Restaurante> consultarPrincipaisRestaurantesCidade(final Long idCidade) {
        final String select = "select * from local where local_type = :local_type and id_local_cidade = :idCidade";
        final SQLQuery query = getSession().createSQLQuery(select);
        query.addEntity(Restaurante.class);
        query.setMaxResults(5);
        query.setLong("idCidade", idCidade);
        query.setInteger("local_type", LocalType.RESTAURANTE.getCodigo());

        return query.list();
    }

    public Long consultarQuantidadeCidadesPorEstado(final Long idEstado) {
        final String select = "select count(*) from local where local_type = :local_type and id_local_estado = :idEstado";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idEstado", idEstado);
        parametros.put("local_type", LocalType.CIDADE.getCodigo());
        return ((Number) (this.searchRepository.consultaSQLResultadoUnico(select, parametros))).longValue();
    }

    public Long consultarQuantidadeCidadesPorPais(final Long idPais) {
        final String select = "select count(*) from local where local_type = :local_type and id_local_pais = :idPais";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idPais", idPais);
        parametros.put("local_type", LocalType.CIDADE.getCodigo());
        return ((Number) (this.searchRepository.consultaSQLResultadoUnico(select, parametros))).longValue();
    }

    public Long consultarQuantidadeEstados(final Long idPais) {
        final String select = "select count(*) from local where local_type = :local_type and id_local_pais = :idPais";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idPais", idPais);
        parametros.put("local_type", LocalType.ESTADO.getCodigo());
        return ((Number) (this.searchRepository.consultaSQLResultadoUnico(select, parametros))).longValue();
    }

    public Long consultarQuantidadePaises() {
        final String select = "select count(*) from local where local_type = " + LocalType.PAIS.getCodigo();
        return ((Number) (this.searchRepository.consultaSQLResultadoUnico(select, null))).longValue();
    }

    public List<RegiaoTripFans> consultarRegioesPorIds(final Long... idsRegioes) {
        final StringBuffer sql = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        sql.append(" SELECT  ");
        sql.append("        id_regiao_tripfans AS id, ");
        sql.append("        nome_regiao AS nome, ");
        sql.append("        nome_regiao_en AS \"nomeIngles\", ");
        sql.append("        nome_regiao_completo AS \"nomeCompleto\", ");
        sql.append("        nome_regiao_completo_en AS \"nomeCompletoIngles\", ");
        sql.append("        id_expedia, ");
        sql.append("        id_booking, ");
        sql.append("        tipo_expedia, ");
        sql.append("        latitude, ");
        sql.append("        longitude, ");
        sql.append("        id_pai_expedia, ");
        sql.append("        nome_pai_regiao AS \"nomePai\", ");
        sql.append("        nome_pai_regiao_completo AS \"nomePaiCompleto\", ");
        sql.append("        tipo_pai_expedia, ");
        sql.append("        sigla_iso_pais AS \"siglaISOPais\" ");

        sql.append("   FROM tripfans_regiao ");
        sql.append("  WHERE id_regiao_tripfans in (:idsRegioes) ");

        params.put("idsRegioes", idsRegioes);

        return this.searchRepository.consultaSQL(sql.toString(), params, RegiaoTripFans.class);

    }

    @SuppressWarnings("unchecked")
    public <T extends RegiaoTripFans> List<T> consultarRegioesPorNome(final String nome) {

        final StringBuffer sql = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        sql.append(" SELECT  ");
        sql.append("        id_regiao_tripfans AS id, ");
        sql.append("        nome_regiao AS nome, ");
        sql.append("        nome_regiao_en AS \"nomeIngles\", ");
        sql.append("        nome_regiao_completo AS \"nomeCompleto\", ");
        sql.append("        nome_regiao_completo_en AS \"nomeCompletoIngles\", ");
        sql.append("        id_expedia, ");
        sql.append("        id_booking, ");
        sql.append("        tipo_expedia, ");
        sql.append("        latitude, ");
        sql.append("        longitude, ");
        sql.append("        id_pai_expedia, ");
        sql.append("        nome_pai_regiao AS \"nomePai\", ");
        sql.append("        nome_pai_regiao_completo AS \"nomePaiCompleto\", ");
        sql.append("        tipo_pai_expedia, ");
        sql.append("        sigla_iso_pais AS \"siglaISOPais\" ");

        sql.append("   FROM tripfans_regiao ");
        sql.append("  WHERE tipo_expedia = 4 ");
        sql.append("    AND UPPER(nome_regiao_en) LIKE '%' || UPPER(:nome) || '%' ");
        sql.append(" ORDER BY numero_hoteis_booking DESC NULLS LAST, nome_regiao_en LIMIT 20");

        params.put("nome", nome);

        return (List<T>) this.searchRepository.consultaSQL(sql.toString(), params, RegiaoTripFans.class);
    }

    public List<LocalEnderecavelView> consultarRestaurantesDestaques(final Long idDestino) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idDestino", idDestino);
        parametros.put("tipoLocal", LocalType.RESTAURANTE.getCodigo());
        return this.searchRepository.consultarPorNamedNativeQuery("Local.consultarLocaisEnderecaveisViewDestaques", parametros,
                LocalEnderecavelView.class);
    }

    public List<TipoServicoHotel> consultarServicosDisponiveis(final Hotel hotel) {
        final String select = "select sh.id_tipo_servico_hotel as id from servico_hotel sh where sh.id_hotel = :id";
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("id", hotel.getId());
        return this.searchRepository.consultaSQL(select, parametros, TipoServicoHotel.class);
    }

    private Map<String, Object> getParametrosMelhoresLocaisEnderecaveis(final LocalGeografico localGeografico, final LocalType localType) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        if (localGeografico.isTipoCidade()) {
            parametros.put("localCidade", localGeografico);
        } else if (localGeografico.isTipoLocalInteresseTuristico()) {
            parametros.put("localInteresseTuristico", localGeografico);
        }
        if (localType.equals(LocalType.ATRACAO)) {
            parametros.put("tipos", Arrays.asList(new LocalType[] { LocalType.ATRACAO, LocalType.LOCAL_INTERESSE_TURISTICO }));
        } else {
            parametros.put("tipoLocal", localType);
        }
        return parametros;
    }
}
