package br.com.fanaticosporviagens.locais;

public class TagLocal {

    private Long id;

    private String nome;

    private int tipoTag;

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public int getTipoTag() {
        return this.tipoTag;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setTipoTag(final int tipoTag) {
        this.tipoTag = tipoTag;
    }

}
