package br.com.fanaticosporviagens.locais;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

/**
 * @author André Thiago (andrethiagos@gmail.com)
 *
 */
@Controller
@RequestMapping(value = { UrlLocalResolver.LOCAL_URL_MAPPING })
public class DestinosController extends BaseController {

    @Autowired
    private DestinosService destinosService;

    @RequestMapping(value = "/{urlPath}", method = { RequestMethod.GET })
    public String destino(@PathVariable final String urlPath, final Model model) {
        final DestinoView destino = this.destinosService.destinoView(urlPath);
        if (destino != null) {
            model.addAttribute("destino", destino);
            return "/destinos/destino";
        }
        return "/erros/404";
    }

    @RequestMapping(value = "/explorar")
    public String explorar(final Model model) {
        final List<DestinoView> destinos = this.destinosService.consultarDestinosDestaques();
        model.addAttribute("destinos", destinos);
        return "/destinos/explorar";
    }

}
