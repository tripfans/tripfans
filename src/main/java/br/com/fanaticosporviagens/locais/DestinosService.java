package br.com.fanaticosporviagens.locais;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;
import br.com.fanaticosporviagens.util.SorteadorItens;
import br.com.fanaticosporviagens.viagem.ViagemService;

@Service
public class DestinosService extends BaseService {

    @Autowired
    private LocalRepository localRepository;

    @Autowired
    private PesquisaTextualService pesquisaTextualService;

    @Autowired
    private SorteadorItens sorteador;

    @Autowired
    private ViagemService viagemService;

    public List<DestinoView> consultarDestinosDestaques() {
        final List<DestinoView> destinosDestaques = this.localRepository.consultarDestinosDestaques();
        for (final DestinoView destino : destinosDestaques) {
            destino.defineNomeSecao();
        }
        return destinosDestaques;
    }

    public DestinoView destinoView(final String urlPath) {
        final DestinoView destino = this.localRepository.consultarDestinoPorUrlPath(urlPath);

        if (destino != null) {

            final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery().withType(LocalType.ATRACAO.getCodigo()).withLimit(30)
                    .withExtraParam("idCidade", destino.getId()).sortByRanking();
            destino.setAtracoesDestaques(this.sorteador.sortear(locaisEnderecaveisView(this.pesquisaTextualService.consultarAtracoes(params)
                    .getResultados()), 10));

            params.withType(LocalType.HOTEL.getCodigo());
            destino.setHoteisDestaques(this.sorteador.sortear(locaisEnderecaveisView(this.pesquisaTextualService.consultarHoteis(params)
                    .getResultados()), 10));

            params.withType(LocalType.RESTAURANTE.getCodigo());
            destino.setRestaurantesDestaques(this.sorteador.sortear(locaisEnderecaveisView(this.pesquisaTextualService.consultarRestaurantes(params)
                    .getResultados()), 10));

            destino.setRoteirosDestaques(this.viagemService.consultarRoteirosDestaques(getUsuarioAutenticado(), destino.getId()));
        }

        return destino;
    }

    private List<LocalEnderecavelView> locaisEnderecaveisView(final List<EntidadeIndexada> entidadesIndexadas) {
        final List<LocalEnderecavelView> locais = new ArrayList<LocalEnderecavelView>();

        for (final EntidadeIndexada entidade : entidadesIndexadas) {
            final LocalEnderecavelView view = new LocalEnderecavelView();
            view.setEndereco(entidade.getEndereco());
            view.setEstrelas(entidade.getEstrelas());
            view.setId(entidade.getId());
            view.setNome(entidade.getNome());
            view.setNotaTripAdvisor(entidade.getNotaTripAdvisor());
            view.setNumAvaliacoesTripAdvisor(entidade.getQuantidadeReviewsTripAdvisor());
            view.setTags(entidade.getTags());
            view.setTripfansRanking(entidade.getTripfansRanking());
            view.setUrlBooking(entidade.getUrlBooking());
            view.setUrlFotoAlbum(entidade.getUrlFotoAlbum());
            view.setUrlFotoBig(entidade.getUrlFotoBig());
            view.setUrlFotoSmall(entidade.getUrlFotoSmall());
            view.setUrlPath(entidade.getUrlPath());

            locais.add(view);
        }

        return locais;
    }
}
