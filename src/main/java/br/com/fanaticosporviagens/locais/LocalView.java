package br.com.fanaticosporviagens.locais;

import java.math.BigDecimal;

public class LocalView {

    private Integer classificacaoGeral;

    private String descricao;

    private Long id;

    private String nome;

    private BigDecimal notaGeral;

    private Integer quantidadeAmigosAchamImperdivel;

    private Integer quantidadeAmigosIndicam;

    private Integer quantidadeAvaliacoes;

    private Integer quantidadeRecomendacoes;

    private Integer quantidadeVisitas;

    private Integer ranking;

    private String urlFotoAlbum;

    private String urlFotoBig;

    private String urlFotoSmall;

    private String urlPath;

    public Integer getClassificacaoGeral() {
        return this.classificacaoGeral;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getDescricaoResumida() {
        if (this.descricao != null && this.descricao.length() > 100) {
            return this.getDescricao().substring(0, 99) + "...";
        }
        return this.descricao;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public BigDecimal getNotaGeral() {
        return this.notaGeral;
    }

    public Integer getQuantidadeAmigosAchamImperdivel() {
        return this.quantidadeAmigosAchamImperdivel;
    }

    public Integer getQuantidadeAmigosIndicam() {
        return this.quantidadeAmigosIndicam;
    }

    public Integer getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public Integer getQuantidadeRecomendacoes() {
        return this.quantidadeRecomendacoes;
    }

    public Integer getQuantidadeVisitas() {
        return this.quantidadeVisitas;
    }

    public Integer getRanking() {
        return this.ranking;
    }

    public String getUrlFotoAlbum() {
        return this.urlFotoAlbum;
    }

    public String getUrlFotoBig() {
        return this.urlFotoBig;
    }

    public String getUrlFotoSmall() {
        return this.urlFotoSmall;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setClassificacaoGeral(final Integer classificacaoGeral) {
        this.classificacaoGeral = classificacaoGeral;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNotaGeral(final BigDecimal notaGeral) {
        this.notaGeral = notaGeral;
    }

    public void setQuantidadeAmigosAchamImperdivel(final Integer quantidadeAmigosAchamImperdivel) {
        this.quantidadeAmigosAchamImperdivel = quantidadeAmigosAchamImperdivel;
    }

    public void setQuantidadeAmigosIndicam(final Integer quantidadeAmigosIndicam) {
        this.quantidadeAmigosIndicam = quantidadeAmigosIndicam;
    }

    public void setQuantidadeAvaliacoes(final Integer quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

    public void setQuantidadeRecomendacoes(final Integer quantidadeRecomendacoes) {
        this.quantidadeRecomendacoes = quantidadeRecomendacoes;
    }

    public void setQuantidadeVisitas(final Integer quantidadeVisitas) {
        this.quantidadeVisitas = quantidadeVisitas;
    }

    public void setRanking(final Integer ranking) {
        this.ranking = ranking;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}
