package br.com.fanaticosporviagens.locais;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.Hotel;

@Repository
public class HotelRepository extends GenericCRUDRepository<Hotel, Long> {
}
