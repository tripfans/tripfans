package br.com.fanaticosporviagens.job;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RotinaRowMapper implements RowMapper<Rotina> {

    @Override
    public Rotina mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        return new Rotina(rs.getLong("id_job_rotina"), PeriodicidadeExecucao.getPeriodicidade(rs.getLong("id_job_periodicidade_execucao")),
                rs.getString("nm_rotina"), rs.getString("tx_objetivo"), rs.getBoolean("habilitada"), rs.getInt("nr_ordem"));
    }

}
