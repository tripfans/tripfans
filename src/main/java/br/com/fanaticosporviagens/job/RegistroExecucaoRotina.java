package br.com.fanaticosporviagens.job;

import java.util.Date;

public class RegistroExecucaoRotina {

    private Date dataFimExecucao;

    private Date dataInicioExecucao;

    private RegistroExecucao execucao;

    private Long id;

    private InformacaoExcecao informacaoExcecao;

    private Rotina rotina;

    public RegistroExecucaoRotina(final RegistroExecucao execucao, final Rotina rotina, final Date dataInicioExecucao, final Date dataFimExecucao) {
        super();
        this.dataFimExecucao = dataFimExecucao;
        this.dataInicioExecucao = dataInicioExecucao;
        this.execucao = execucao;
        this.rotina = rotina;
    }

    public RegistroExecucaoRotina(final RegistroExecucao execucao, final Rotina rotina, final Date dataInicioExecucao, final Date dataFimExecucao,
            final Throwable excecao) {
        super();
        this.dataFimExecucao = dataFimExecucao;
        this.dataInicioExecucao = dataInicioExecucao;
        this.execucao = execucao;
        this.informacaoExcecao = new InformacaoExcecao(excecao);
        this.rotina = rotina;
    }

    public Date getDataFimExecucao() {
        return this.dataFimExecucao;
    }

    public Date getDataInicioExecucao() {
        return this.dataInicioExecucao;
    }

    public RegistroExecucao getExecucao() {
        return this.execucao;
    }

    public Long getId() {
        return this.id;
    }

    public InformacaoExcecao getInformacaoExcecao() {
        return this.informacaoExcecao;
    }

    public Rotina getRotina() {
        return this.rotina;
    }

    public void setDataFimExecucao(final Date dataFimExecucao) {
        this.dataFimExecucao = dataFimExecucao;
    }

    public void setDataInicioExecucao(final Date dataInicioExecucao) {
        this.dataInicioExecucao = dataInicioExecucao;
    }

    public void setExecucao(final RegistroExecucao execucao) {
        this.execucao = execucao;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setInformacaoExcecao(final InformacaoExcecao informacaoExcecao) {
        this.informacaoExcecao = informacaoExcecao;
    }

    public void setRotina(final Rotina rotina) {
        this.rotina = rotina;
    }

}
