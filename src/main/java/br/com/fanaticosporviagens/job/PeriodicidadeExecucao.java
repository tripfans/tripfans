package br.com.fanaticosporviagens.job;

public enum PeriodicidadeExecucao {

    TEMPO_HORA_01(6L, 1 * 60 * 60 * 1000L),
    TEMPO_HORA_02(7L, 2 * 60 * 60 * 1000L),
    TEMPO_HORA_06(8L, 6 * 60 * 60 * 1000L),
    TEMPO_HORA_12(9L, 12 * 60 * 60 * 1000L),
    TEMPO_HORA_24(10L, 24 * 60 * 60 * 1000L),
    TEMPO_MINUTO_01(1L, 1 * 60 * 1000L),
    TEMPO_MINUTO_02(2L, 2 * 60 * 1000L),
    TEMPO_MINUTO_05(3L, 5 * 60 * 1000L),
    TEMPO_MINUTO_10(4L, 10 * 60 * 1000L),
    TEMPO_MINUTO_30(5L, 30 * 60 * 1000L),
    TEMPO_SEGUNDO_15(11L, 15 * 1000L),
    TEMPO_SEGUNDO_30(12L, 30 * 1000L),
    TEMPO_SEGUNDO_45(13L, 45 * 1000L);

    public static PeriodicidadeExecucao getPeriodicidade(final Long id) {
        for (final PeriodicidadeExecucao periodicidade : PeriodicidadeExecucao.values()) {
            if (periodicidade.getId().equals(id)) {
                return periodicidade;
            }
        }

        return null;
    }

    private final Long id;

    private final Long tempoEmMilesegundos;

    PeriodicidadeExecucao(final Long id, final Long tempoEmMilesegundos) {
        this.id = id;
        this.tempoEmMilesegundos = tempoEmMilesegundos;
    }

    public Long getId() {
        return this.id;
    }

    public Long getTempoEmMilesegundos() {
        return this.tempoEmMilesegundos;
    }
}
