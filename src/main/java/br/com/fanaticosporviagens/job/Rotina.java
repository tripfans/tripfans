package br.com.fanaticosporviagens.job;

public class Rotina {
    private String comando;

    private String descricacao;

    private boolean habilitada;

    private Long id;

    private Integer ordem;

    private PeriodicidadeExecucao periodicidade;

    public Rotina() {
        super();
    }

    public Rotina(final Long id, final PeriodicidadeExecucao periodicidade, final String comando, final String descricacao, final boolean habilitada,
            final Integer ordem) {
        super();
        this.comando = comando;
        this.descricacao = descricacao;
        this.habilitada = habilitada;
        this.id = id;
        this.periodicidade = periodicidade;
        this.ordem = ordem;
    }

    public String getComando() {
        return this.comando;
    }

    public String getDescricacao() {
        return this.descricacao;
    }

    public Long getId() {
        return this.id;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    public PeriodicidadeExecucao getPeriodicidade() {
        return this.periodicidade;
    }

    public boolean isHabilitada() {
        return this.habilitada;
    }

    public void setComando(final String comando) {
        this.comando = comando;
    }

    public void setDescricacao(final String descricacao) {
        this.descricacao = descricacao;
    }

    public void setHabilitada(final boolean habilitada) {
        this.habilitada = habilitada;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setOrdem(final Integer ordem) {
        this.ordem = ordem;
    }

    public void setPeriodicidade(final PeriodicidadeExecucao periodicidade) {
        this.periodicidade = periodicidade;
    }

}
