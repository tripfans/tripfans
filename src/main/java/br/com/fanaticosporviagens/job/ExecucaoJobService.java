package br.com.fanaticosporviagens.job;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

public class ExecucaoJobService {

    private static final long TEMPO_HORA_01 = 1 * 60 * 60 * 1000L;

    private static final long TEMPO_HORA_02 = 2 * 60 * 60 * 1000L;

    private static final long TEMPO_HORA_06 = 6 * 60 * 60 * 1000L;

    private static final long TEMPO_HORA_12 = 12 * 60 * 60 * 1000L;

    private static final long TEMPO_HORA_24 = 24 * 60 * 60 * 1000L;

    private static final long TEMPO_MINUTO_01 = 1 * 60 * 1000L;

    private static final long TEMPO_MINUTO_02 = 2 * 60 * 1000L;

    private static final long TEMPO_MINUTO_05 = 5 * 60 * 1000L;

    private static final long TEMPO_MINUTO_10 = 10 * 60 * 1000L;

    private static final long TEMPO_MINUTO_30 = 30 * 60 * 1000L;

    private static final long TEMPO_SEGUNDO_15 = 15 * 1000L;

    private static final long TEMPO_SEGUNDO_30 = 30 * 1000L;

    private static final long TEMPO_SEGUNDO_45 = 45 * 1000L;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplateConsultaRotinas;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplateExecucaoRotina;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplateRegistroExecucao;

    // private final Log log = LogFactory.getLog(ContadorLocalJob.class);

    private final String sqlInsertRegistroExecucao;

    private final String sqlInsertRegistroRotina;

    @Autowired
    @Qualifier("jobTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    private UsuarioService usuarioService;

    public ExecucaoJobService() {
        final StringBuilder sqlInsertRegistroExecucao = new StringBuilder();
        sqlInsertRegistroExecucao.append("insert into job_execucao (");
        sqlInsertRegistroExecucao.append("       id_job_execucao ");
        sqlInsertRegistroExecucao.append("     , id_job_periodicidade_execucao ");
        sqlInsertRegistroExecucao.append("     , dt_inicio ");
        sqlInsertRegistroExecucao.append("     , dt_fim ");
        sqlInsertRegistroExecucao.append("     , dt_exception ");
        sqlInsertRegistroExecucao.append("     , nm_exception ");
        sqlInsertRegistroExecucao.append("     , tx_exception_message ");
        sqlInsertRegistroExecucao.append("     , nm_exception_cause ");
        sqlInsertRegistroExecucao.append("     , tx_exception_cause_message ");
        sqlInsertRegistroExecucao.append("     , tx_exception_stack_trace ");
        sqlInsertRegistroExecucao.append(") values ( ");
        sqlInsertRegistroExecucao.append("       :id_job_execucao ");
        sqlInsertRegistroExecucao.append("     , :id_job_periodicidade_execucao ");
        sqlInsertRegistroExecucao.append("     , :dt_inicio ");
        sqlInsertRegistroExecucao.append("     , :dt_fim ");
        sqlInsertRegistroExecucao.append("     , :dt_exception ");
        sqlInsertRegistroExecucao.append("     , :nm_exception ");
        sqlInsertRegistroExecucao.append("     , :tx_exception_message ");
        sqlInsertRegistroExecucao.append("     , :nm_exception_cause ");
        sqlInsertRegistroExecucao.append("     , :tx_exception_cause_message ");
        sqlInsertRegistroExecucao.append("     , :tx_exception_stack_trace ");
        sqlInsertRegistroExecucao.append("     ) ");

        this.sqlInsertRegistroExecucao = sqlInsertRegistroExecucao.toString();

        final StringBuilder sqlInsertRegistroRotina = new StringBuilder();
        sqlInsertRegistroRotina.append("insert into job_rotina_execucao (");
        sqlInsertRegistroRotina.append("       id_job_rotina_execucao ");
        sqlInsertRegistroRotina.append("     , id_job_rotina ");
        sqlInsertRegistroRotina.append("     , id_job_execucao ");
        sqlInsertRegistroRotina.append("     , dt_inicio ");
        sqlInsertRegistroRotina.append("     , dt_fim ");
        sqlInsertRegistroRotina.append("     , dt_exception ");
        sqlInsertRegistroRotina.append("     , nm_exception ");
        sqlInsertRegistroRotina.append("     , tx_exception_message ");
        sqlInsertRegistroRotina.append("     , nm_exception_cause ");
        sqlInsertRegistroRotina.append("     , tx_exception_cause_message ");
        sqlInsertRegistroRotina.append("     , tx_exception_stack_trace ");
        sqlInsertRegistroRotina.append(") values ( ");
        sqlInsertRegistroRotina.append("       (select nextval('seq_job_rotina_execucao')) ");
        sqlInsertRegistroRotina.append("     , :id_job_rotina ");
        sqlInsertRegistroRotina.append("     , :id_job_execucao ");
        sqlInsertRegistroRotina.append("     , :dt_inicio ");
        sqlInsertRegistroRotina.append("     , :dt_fim ");
        sqlInsertRegistroRotina.append("     , :dt_exception ");
        sqlInsertRegistroRotina.append("     , :nm_exception ");
        sqlInsertRegistroRotina.append("     , :tx_exception_message ");
        sqlInsertRegistroRotina.append("     , :nm_exception_cause ");
        sqlInsertRegistroRotina.append("     , :tx_exception_cause_message ");
        sqlInsertRegistroRotina.append("     , :tx_exception_stack_trace ");
        sqlInsertRegistroRotina.append("     ) ");

        this.sqlInsertRegistroRotina = sqlInsertRegistroRotina.toString();
    }

    public void executar(final String rotina) {
        final TransactionTemplate template = new TransactionTemplate(this.transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus status) {
                ExecucaoJobService.this.jdbcTemplateExecucaoRotina.queryForLong(rotina, new HashMap<String, Object>());
            }
        });
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_HORA_01)
    public void executarEmHora_01() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_HORA_01);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_HORA_02)
    public void executarEmHora_02() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_HORA_02);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_HORA_06)
    public void executarEmHora_06() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_HORA_06);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_HORA_12)
    public void executarEmHora_12() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_HORA_12);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_HORA_24)
    public void executarEmHora_24() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_HORA_24);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_MINUTO_01)
    public void executarEmMinuto_01() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_MINUTO_01);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_MINUTO_02)
    public void executarEmMinuto_02() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_MINUTO_02);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_MINUTO_05)
    public void executarEmMinuto_05() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_MINUTO_05);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_MINUTO_10)
    public void executarEmMinuto_10() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_MINUTO_10);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_MINUTO_30)
    public void executarEmMinuto_30() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_MINUTO_30);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_SEGUNDO_15)
    public void executarEmSegundo_15() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_SEGUNDO_15);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_SEGUNDO_30)
    public void executarEmSegundo_30() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_SEGUNDO_30);
    }

    @Scheduled(initialDelay = 30000, fixedDelay = TEMPO_SEGUNDO_45)
    public void executarEmSegundo_45() {
        executarJobParaOPeriodo(PeriodicidadeExecucao.TEMPO_SEGUNDO_45);
    }

    public void executarJobParaOPeriodo(final PeriodicidadeExecucao periodicidade) {
        final RegistroExecucao registroExecucao = new RegistroExecucao(periodicidade, new Date());
        try {
            final HashMap<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("periodicidade", periodicidade.getId());

            final StringBuilder sql = new StringBuilder();
            sql.append("select jr.id_job_rotina ");
            sql.append("     , jr.id_job_periodicidade_execucao ");
            sql.append("     , jr.nm_rotina ");
            sql.append("     , jr.tx_objetivo ");
            sql.append("     , jr.nr_ordem ");
            sql.append("     , jr.habilitada ");
            sql.append("  from job_rotina jr ");
            sql.append(" where jr.habilitada = true ");
            sql.append("   and jr.id_job_periodicidade_execucao = :periodicidade ");
            sql.append(" order by jr.nr_ordem ");

            final List<Rotina> rotinas = this.jdbcTemplateConsultaRotinas.query(sql.toString(), paramMap, new RotinaRowMapper());

            Date dataInicioExecucaoRotina;
            for (final Rotina rotina : rotinas) {
                dataInicioExecucaoRotina = new Date();
                try {
                    executar(rotina.getComando());
                    registroExecucao.addExecucaoRotina(rotina, dataInicioExecucaoRotina, new Date());
                } catch (final Throwable ex) {
                    registroExecucao.addExecucaoRotina(rotina, dataInicioExecucaoRotina, new Date(), ex);
                }
            }

            registroExecucao.setDataFimExecucao(new Date());
        } catch (final Throwable ex) {
            registroExecucao.setDataFimExecucao(new Date());
            registroExecucao.setInformacaoExcecao(ex);
        }

        registrarExcecucao(registroExecucao);
    }

    private Map<String, Object> getParamMap(final RegistroExecucao registroExecucao) {
        final Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id_job_execucao", registroExecucao.getId());
        paramMap.put("id_job_periodicidade_execucao", registroExecucao.getPeriodicidade().getId());
        paramMap.put("dt_inicio", registroExecucao.getDataInicioExecucao());
        paramMap.put("dt_fim", registroExecucao.getDataFimExecucao());
        if (registroExecucao.getInformacaoExcecao() != null) {
            paramMap.put("dt_exception", registroExecucao.getInformacaoExcecao().getDataOcorrencia());
            paramMap.put("nm_exception", registroExecucao.getInformacaoExcecao().getExcecaoNome());
            paramMap.put("tx_exception_message", registroExecucao.getInformacaoExcecao().getExcecaoMensagem());
            paramMap.put("nm_exception_cause", registroExecucao.getInformacaoExcecao().getExcecaoCausaNome());
            paramMap.put("tx_exception_cause_message", registroExecucao.getInformacaoExcecao().getExcecaoCausaMensagem());
            paramMap.put("tx_exception_stack_trace", registroExecucao.getInformacaoExcecao().getStackTrace());
        } else {
            paramMap.put("dt_exception", null);
            paramMap.put("nm_exception", null);
            paramMap.put("tx_exception_message", null);
            paramMap.put("nm_exception_cause", null);
            paramMap.put("tx_exception_cause_message", null);
            paramMap.put("tx_exception_stack_trace", null);
        }

        return paramMap;
    }

    private Map<String, Object> getParamMap(final RegistroExecucaoRotina registroRotina) {
        final Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("id_job_rotina", registroRotina.getRotina().getId());
        paramMap.put("id_job_execucao", registroRotina.getExecucao().getId());
        paramMap.put("dt_inicio", registroRotina.getDataInicioExecucao());
        paramMap.put("dt_fim", registroRotina.getDataFimExecucao());
        if (registroRotina.getInformacaoExcecao() != null) {
            paramMap.put("dt_exception", registroRotina.getInformacaoExcecao().getDataOcorrencia());
            paramMap.put("nm_exception", registroRotina.getInformacaoExcecao().getExcecaoNome());
            paramMap.put("tx_exception_message", registroRotina.getInformacaoExcecao().getExcecaoMensagem());
            paramMap.put("nm_exception_cause", registroRotina.getInformacaoExcecao().getExcecaoCausaNome());
            paramMap.put("tx_exception_cause_message", registroRotina.getInformacaoExcecao().getExcecaoCausaMensagem());
            paramMap.put("tx_exception_stack_trace", registroRotina.getInformacaoExcecao().getStackTrace());
        } else {
            paramMap.put("dt_exception", null);
            paramMap.put("nm_exception", null);
            paramMap.put("tx_exception_message", null);
            paramMap.put("nm_exception_cause", null);
            paramMap.put("tx_exception_cause_message", null);
            paramMap.put("tx_exception_stack_trace", null);
        }

        return paramMap;
    }

    // private void printDebug(final String mensagem) {
    // if (this.log.isDebugEnabled()) {
    // this.log.debug(mensagem);
    // }
    // }

    private void registrarExcecucao(final RegistroExecucao registroExecucao) {
        final TransactionTemplate template = new TransactionTemplate(this.transactionManager);
        template.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(final TransactionStatus status) {
                final long idRegistroExecucao = ExecucaoJobService.this.jdbcTemplateRegistroExecucao.queryForLong(
                        "select nextval('seq_job_execucao')", new HashMap<String, Object>());

                registroExecucao.setId(idRegistroExecucao);

                ExecucaoJobService.this.jdbcTemplateRegistroExecucao.update(ExecucaoJobService.this.sqlInsertRegistroExecucao,
                        getParamMap(registroExecucao));

                for (final RegistroExecucaoRotina registroRotina : registroExecucao.getRotinasExecutadas()) {
                    ExecucaoJobService.this.jdbcTemplateRegistroExecucao.update(ExecucaoJobService.this.sqlInsertRegistroRotina,
                            getParamMap(registroRotina));
                }
            }
        });
    }
}
