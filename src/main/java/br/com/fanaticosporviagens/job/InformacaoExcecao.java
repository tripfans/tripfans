package br.com.fanaticosporviagens.job;

import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;

public class InformacaoExcecao {

    private Date dataOcorrencia;

    private String excecaoCausaMensagem;

    private String excecaoCausaNome;

    private String excecaoMensagem;

    private String excecaoNome;

    private String stackTrace;

    public InformacaoExcecao(final Date dataOcorrencia, final String excecaoNome, final String excecaoMensagem, final String excecaoCausaNome,
            final String excecaoCausaMensagem, final String stackTrace) {
        super();
        this.dataOcorrencia = dataOcorrencia;
        this.excecaoNome = excecaoNome;
        this.excecaoMensagem = excecaoMensagem;
        this.excecaoCausaNome = excecaoCausaNome;
        this.excecaoCausaMensagem = excecaoCausaMensagem;
        this.stackTrace = stackTrace;
    }

    public InformacaoExcecao(final Throwable excecao) {
        super();
        this.dataOcorrencia = new Date();

        this.excecaoNome = excecao.getClass().getName();
        this.excecaoMensagem = excecao.getMessage();

        final Throwable causa = ExceptionUtils.getRootCause(excecao);
        this.excecaoCausaNome = causa.getClass().getName();
        this.excecaoCausaMensagem = causa.getMessage();

        this.stackTrace = ExceptionUtils.getFullStackTrace(excecao);
    }

    public Date getDataOcorrencia() {
        return this.dataOcorrencia;
    }

    public String getExcecaoCausaMensagem() {
        return this.excecaoCausaMensagem;
    }

    public String getExcecaoCausaNome() {
        return this.excecaoCausaNome;
    }

    public String getExcecaoMensagem() {
        return this.excecaoMensagem;
    }

    public String getExcecaoNome() {
        return this.excecaoNome;
    }

    public String getStackTrace() {
        return this.stackTrace;
    }

    public void setDataOcorrencia(final Date dataOcorrencia) {
        this.dataOcorrencia = dataOcorrencia;
    }

    public void setExcecaoCausaMensagem(final String excecaoCausaMensagem) {
        this.excecaoCausaMensagem = excecaoCausaMensagem;
    }

    public void setExcecaoCausaNome(final String excecaoCausaNome) {
        this.excecaoCausaNome = excecaoCausaNome;
    }

    public void setExcecaoMensagem(final String excecaoMensagem) {
        this.excecaoMensagem = excecaoMensagem;
    }

    public void setExcecaoNome(final String excecaoNome) {
        this.excecaoNome = excecaoNome;
    }

    public void setStackTrace(final String stackTrace) {
        this.stackTrace = stackTrace;
    }
}
