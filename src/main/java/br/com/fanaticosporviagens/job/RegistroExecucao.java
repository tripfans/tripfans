package br.com.fanaticosporviagens.job;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RegistroExecucao {

    private Date dataFimExecucao;

    private Date dataInicioExecucao;

    private Long id;

    private InformacaoExcecao informacaoExcecao;

    private PeriodicidadeExecucao periodicidade;

    private List<RegistroExecucaoRotina> rotinasExecutadas = new ArrayList<RegistroExecucaoRotina>();

    public RegistroExecucao(final PeriodicidadeExecucao periodicidade, final Date dataInicioExecucao) {
        super();
        this.dataInicioExecucao = dataInicioExecucao;
        this.periodicidade = periodicidade;
    }

    public void addExecucaoRotina(final Rotina rotina, final Date dataInicioExecucao, final Date dataFimExecucao) {
        this.rotinasExecutadas.add(new RegistroExecucaoRotina(this, rotina, dataInicioExecucao, dataFimExecucao));
    }

    public void addExecucaoRotina(final Rotina rotina, final Date dataInicioExecucao, final Date dataFimExecucao, final Throwable excecao) {
        this.rotinasExecutadas.add(new RegistroExecucaoRotina(this, rotina, dataInicioExecucao, dataFimExecucao, excecao));
    }

    public Date getDataFimExecucao() {
        return this.dataFimExecucao;
    }

    public Date getDataInicioExecucao() {
        return this.dataInicioExecucao;
    }

    public Long getId() {
        return this.id;
    }

    public InformacaoExcecao getInformacaoExcecao() {
        return this.informacaoExcecao;
    }

    public PeriodicidadeExecucao getPeriodicidade() {
        return this.periodicidade;
    }

    public List<RegistroExecucaoRotina> getRotinasExecutadas() {
        return this.rotinasExecutadas;
    }

    public void setDataFimExecucao(final Date dataFimExecucao) {
        this.dataFimExecucao = dataFimExecucao;
    }

    public void setDataInicioExecucao(final Date dataInicioExecucao) {
        this.dataInicioExecucao = dataInicioExecucao;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setInformacaoExcecao(final InformacaoExcecao informacaoExcecao) {
        this.informacaoExcecao = informacaoExcecao;
    }

    public void setInformacaoExcecao(final Throwable excecao) {
        this.informacaoExcecao = new InformacaoExcecao(excecao);
    }

    public void setPeriodicidade(final PeriodicidadeExecucao periodicidade) {
        this.periodicidade = periodicidade;
    }

    public void setRotinasExecutadas(final List<RegistroExecucaoRotina> rotinasExecutadas) {
        this.rotinasExecutadas = rotinasExecutadas;
    }

}
