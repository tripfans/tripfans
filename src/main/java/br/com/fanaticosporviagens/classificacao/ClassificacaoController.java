package br.com.fanaticosporviagens.classificacao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fanaticosporviagens.model.entity.CategoriaLocal;
import br.com.fanaticosporviagens.model.entity.LocalType;

@Controller
@RequestMapping("/classificacao")
public class ClassificacaoController {

    @Autowired
    private ClassificacaoService classificacaoService;

    @RequestMapping(value = "/{urlPathlocalType}", method = { RequestMethod.GET })
    public String classificacaoPorLocalType(@PathVariable final String urlPathlocalType, final Model model) {
        final LocalType localType = LocalType.getLocalTypePorUrlPath(urlPathlocalType);

        final List<CategoriaLocal> categorias = this.classificacaoService.consultarCategoriaLocalPorLocalType(localType);

        model.addAttribute("localType", localType);
        model.addAttribute("categorias", categorias);

        return "/classificacao/classificacao";
    }

}
