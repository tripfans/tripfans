package br.com.fanaticosporviagens.classificacao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.CategoriaLocal;
import br.com.fanaticosporviagens.model.entity.LocalType;

@Service
public class ClassificacaoService {

    @Autowired
    protected GenericSearchRepository searchRepository;

    public List<CategoriaLocal> consultarCategoriaLocalPorLocalType(final LocalType localType) {

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("localType", localType);

        final StringBuilder hql = new StringBuilder();
        hql.append("select categoria ");
        hql.append("  from br.com.fanaticosporviagens.model.entity.CategoriaLocal categoria ");
        hql.append(" where categoria.localType = :localType ");
        hql.append(" order by  categoria.descricao ");

        final List<CategoriaLocal> categorias = this.searchRepository.consultaHQL(hql.toString(), params);

        return categorias;
    }

}
