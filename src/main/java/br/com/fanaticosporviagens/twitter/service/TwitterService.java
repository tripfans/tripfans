package br.com.fanaticosporviagens.twitter.service;

import javax.inject.Inject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.social.connect.Connection;
import org.springframework.social.twitter.api.StatusDetails;
import org.springframework.social.twitter.api.TimelineOperations;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.facebook.service.BaseProviderService;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.util.TripFansShareableLinkBuilder;
import br.com.fanaticosporviagens.util.UrlShortenerService;

/**
 * @author André Thiago
 *
 */
@Service
public class TwitterService extends BaseProviderService {

    @Inject
    private Environment environment;

    private final Log log = LogFactory.getLog(TwitterService.class);

    @Autowired
    private TripFansShareableLinkBuilder shareableLinkBuilder;

    @Autowired
    private UrlShortenerService urlShortenerService;

    private StatusDetails getStatusDetails() {
        final StatusDetails statusDetails = new StatusDetails();
        statusDetails.setWrapLinks(true);
        return statusDetails;
    }

    private TimelineOperations getTimelineOperations() {
        return getTwitter().timelineOperations();
    }

    private Twitter getTwitter() {
        final Connection<Twitter> connection = getConnection(Twitter.class);
        if (connection != null) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Encontrou conexão com Twitter para o usuário " + connection.getDisplayName());
            }
            return connection.getApi();
        }

        final String consumerKey = this.environment.getProperty("twitter.consumerKey");
        final String consumerSecret = this.environment.getProperty("twitter.consumerSecret");

        return new TwitterTemplate(consumerKey, consumerSecret);
    }

    private void logSuccessfullTweetPost(final Tweet updateStatus) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Tweet publicado com id " + updateStatus.getId());
        }
    }

    public void postarTweet(final SocialShareableItem shareable) {
        final String link = this.shareableLinkBuilder.getLink(shareable);
        final String text = shareable.getTwitterPostText() + this.urlShortenerService.shorten(link);
        final Tweet updateStatus = getTimelineOperations().updateStatus(text, getStatusDetails());
        logSuccessfullTweetPost(updateStatus);
    }

}
