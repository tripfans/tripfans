package br.com.fanaticosporviagens.infra.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

public class AutoCompleteJSONResult implements Serializable {

    private static final long serialVersionUID = 3989503990658759019L;

    public static AutoCompleteJSONResult create(final Object source, final String idAttribute, final String valueAttribute,
            final String labelAttribute) {
        return new AutoCompleteJSONResult(getValue(source, idAttribute), getValue(source, valueAttribute), getValue(source, labelAttribute));
    }

    public static List<AutoCompleteJSONResult> createList(final Collection<? extends Object> list, final String idAttribute,
            final String valueAttribute, final String labelAttribute) {
        final List<AutoCompleteJSONResult> result = new ArrayList<AutoCompleteJSONResult>();
        if (list != null && !list.isEmpty()) {
            for (final Object item : list) {
                result.add(AutoCompleteJSONResult.create(item, idAttribute, valueAttribute, labelAttribute));
            }
        }
        return result;
    }

    private static Object getValue(final Object source, final String attributeName) {
        try {
            return PropertyUtils.getNestedProperty(source, attributeName);
        } catch (final Exception e) {
            System.out.println("erro : " + e.getMessage());
            return null;
        }
    }

    private Object id;

    private Object label;

    private Object value;

    private AutoCompleteJSONResult(final Object id, final Object value, final Object label) {
        this.id = id;
        this.value = value;
        this.label = label;
    }

    public Object getId() {
        return this.id;
    }

    public Object getLabel() {
        return this.label;
    }

    public Object getValue() {
        return this.value;
    }

    public void setId(final Object id) {
        this.id = id;
    }

    public void setLabel(final Object label) {
        this.label = label;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

}
