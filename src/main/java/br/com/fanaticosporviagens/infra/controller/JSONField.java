package br.com.fanaticosporviagens.infra.controller;

/**
 * Classe que representa um field enviado como parametro pela tela.
 * 
 * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
 */
public class JSONField {

    private String mapping;

    private String name;

    private String type;

    public JSONField() {
        super();
    }

    public String getMapping() {
        return this.mapping;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public void setMapping(final String mapping) {
        this.mapping = mapping;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
