package br.com.fanaticosporviagens.infra.controller;

public enum MessageType {
    ERROR("error", "Erro"),
    SUCCESS("info", "Informação"),
    WARN("warn", "Aviso");

    private final String defaultTitle;

    private final String type;

    MessageType(final String type, final String defaultTitle) {
        this.type = type;
        this.defaultTitle = defaultTitle;
    }

    public String getDefaultTitle() {
        return this.defaultTitle;
    }

    public String getType() {
        return this.type;
    }

}
