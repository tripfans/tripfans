package br.com.fanaticosporviagens.infra.controller;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.CookieTheftException;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.social.ExpiredAuthorizationException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import br.com.fanaticosporviagens.infra.component.SearchData;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.exception.ExceptionLogService;
import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.infra.security.SecurityService;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class BaseController {

    protected static final DateTime DATA_FIM_PROMOCAO = new DateTime(2014, 01, 31, 23, 59, 59);

    protected static final DateTime DATA_LANCAMENTO_PROMOCAO = new DateTime(2013, 12, 22, 0, 0, 0);

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @Inject
    protected HttpServletRequest httpRequest;

    @Inject
    protected HttpSession httpSession;

    protected String jsonFields;

    /**
     * Bean que indica o resultado da operação
     */
    protected final JSONResponse jsonResponse = new JSONResponse();

    @Inject
    protected WebRequest request;

    @Autowired
    protected SearchData searchData;

    @Autowired
    protected SecurityService securityService;

    @Autowired
    protected StatusMessage statusMessage;

    @Autowired
    private ExceptionLogService exceptionLogService;

    private final Log log = LogFactory.getLog(BaseController.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(AccessDeniedException.class)
    public String handleAccessDeniedException(final AccessDeniedException exception, final HttpServletRequest request,
            final HttpServletResponse response) {
        new HttpSessionRequestCache().saveRequest(request, response);
        return "redirect:/entrar";
    }

    @ExceptionHandler(BusinessException.class)
    public StatusMessage handleBusinessException(final BusinessException exception, final HttpServletRequest request,
            final HttpServletResponse response) {
        return error(exception.getMessage());
    }

    @ExceptionHandler(CookieTheftException.class)
    public String handleCookieTheftException(final CookieTheftException exception, final HttpServletRequest request,
            final HttpServletResponse response) {
        new HttpSessionRequestCache().saveRequest(request, response);
        return "redirect:/entrar";
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(final Exception exception, final HttpServletRequest request, final HttpServletResponse response) {
        this.exceptionLogService.registerException(exception, request, getUsuarioAutenticado());
        request.setAttribute("exception", exception);
        throw new SystemException("Oppss... foi encontrado um erro. Tente novamente depois.");
    }

    @ExceptionHandler(ExpiredAuthorizationException.class)
    public String handleExpiredToken() {
        return "redirect:/signout";
    }

    @ExceptionHandler(NotFoundException.class)
    public ModelAndView handleNotFoundException(final NotFoundException exception, final HttpServletRequest request,
            final HttpServletResponse response) {
        this.exceptionLogService.registerException(exception, request, getUsuarioAutenticado());
        request.setAttribute("exception", exception);
        return new ModelAndView("/errors/404.jsp");
    }

    @InitBinder
    public void initBinder(final WebDataBinder binder) {
        if (LocaleContextHolder.getLocale().getLanguage().equals("en")) {
            // binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true));
            binder.registerCustomEditor(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, NumberFormat.getNumberInstance(new Locale("en")),
                    true));
        } else {
            binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("dd/MM/yyyy"), true));
            binder.registerCustomEditor(BigDecimal.class,
                    new CustomNumberEditor(BigDecimal.class, NumberFormat.getNumberInstance(new Locale("pt", "BR")), true));
        }
        checkUserAgentMobile();
        registerUseOfInternetExplorer();
    }

    public boolean isUsuarioAutenticado() {
        return getUsuarioAutenticado() != null;
    }

    public Boolean promocaoTabletValida() {
        final DateTime agora = DateTime.now();
        final Interval interval = new Interval(DATA_LANCAMENTO_PROMOCAO, DATA_FIM_PROMOCAO);
        return interval.contains(agora);
    }

    @ModelAttribute
    public void setupJSONFields(@RequestParam(required = false) final String jsonFields) {
        this.jsonFields = jsonFields;
    }

    @ModelAttribute
    public void setupSearchData() {
        if (this.searchData == null) {
            this.searchData = SpringBeansProvider.getBean(SearchData.class);
        }
        final Set<String> keySet = this.request.getParameterMap().keySet();
        if (CollectionUtils.containsAny(keySet, getSearchDataAttributes())) {
            for (final String key : keySet) {
                final String[] values = this.request.getParameterMap().get(key);
                if (values != null) {
                    try {
                        BeanUtils.setProperty(this.searchData, key, values[0]);
                    } catch (final IllegalAccessException e) {
                        if (this.log.isInfoEnabled()) {
                            this.log.info("A propriedade " + key + " não existe em SearchData.");
                        }
                    } catch (final InvocationTargetException e) {
                        throw new SystemException("Erro ao popular SearchData.", e);
                    }
                }
            }
        }
    }

    protected StatusMessage error(final String message, final Object... args) {
        if (isJSONResponse()) {
            final StatusMessage statusMessage = new StatusMessage();
            statusMessage.error(message, args);
            return statusMessage;
        } else {
            this.statusMessage.error(message, args);
            return getTargetStatusMessage();
        }
    }

    protected Integer[] getAnosVisita() {
        final Integer[] anos = new Integer[10];
        anos[0] = DateTime.now().getYear();
        for (int i = 1; i < anos.length; i++) {
            anos[i] = anos[i - 1] - 1;
        }
        return anos;
    }

    protected String getMessage(final String code) {
        return this.messageSource.getMessage(code, null, Locale.getDefault());
    }

    protected String getMessage(final String code, final Object[] args) {
        return this.messageSource.getMessage(code, args, Locale.getDefault());
    }

    protected SearchData getSearchData() {
        return this.searchData;
    }

    protected Usuario getUsuarioAutenticado() {
        return this.securityService.getUsuarioAutenticado();
    }

    protected StatusMessage success(final String message, final Object... args) {
        if (isJSONResponse()) {
            final StatusMessage statusMessage = new StatusMessage();
            statusMessage.success(message, args);
            return statusMessage;
        } else {
            this.statusMessage.success(message, args);
            return getTargetStatusMessage();
        }
    }

    protected StatusMessage warning(final String message, final Object... args) {
        if (isJSONResponse()) {
            final StatusMessage statusMessage = new StatusMessage();
            statusMessage.warning(message, args);
            return statusMessage;
        } else {
            this.statusMessage.warning(message, args);
            return getTargetStatusMessage();
        }
    }

    private void checkUserAgentMobile() {
        final String userAgent = this.httpRequest.getHeader("User-Agent");
        if (StringUtils.isNotEmpty(userAgent) && ((userAgent.indexOf("Mobile") != -1))) {
            this.httpRequest.setAttribute("isMobile", true);
        } else {
            this.httpRequest.setAttribute("isMobile", false);
        }
    }

    private Set<String> getSearchDataAttributes() {
        final Field[] declaredFields = SearchData.class.getDeclaredFields();
        final Set<String> fields = new HashSet<String>();
        for (final Field field : declaredFields) {
            fields.add(field.getName());
        }
        return fields;
    }

    private StatusMessage getTargetStatusMessage() {
        if (AopUtils.isAopProxy(this.statusMessage)) {
            try {
                return (StatusMessage) ((Advised) this.statusMessage).getTargetSource().getTarget();
            } catch (final Exception e) {
                if (this.log.isDebugEnabled()) {
                    this.log.debug("Erro ao pegar target StatusMessage.", e);
                }
                return null;
            }
        } else {
            return this.statusMessage;
        }
    }

    private boolean isJSONResponse() {
        final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        if (stackTraceElements.length >= 4) {
            final String calleeMethodName = stackTraceElements[3].getMethodName();
            try {
                final Method[] declaredMethods = getClass().getDeclaredMethods();
                for (final Method method : declaredMethods) {
                    if (method.getName().equals(calleeMethodName) && method.isAnnotationPresent(ResponseBody.class)) {
                        return true;
                    }
                }
            } catch (final Exception e) {
                throw new SystemException("Erro ao verificar se o método responde com JSON.", e);
            }
        }
        return false;
    }

    private void registerUseOfInternetExplorer() {
        final Object usingIE = this.httpSession.getAttribute("usingIE");
        if (usingIE == null) {
            final String userAgent = this.httpRequest.getHeader("User-Agent");
            if (StringUtils.isNotEmpty(userAgent) && ((userAgent.indexOf("MSIE") != -1) || (userAgent.indexOf("msie") != -1))) {
                this.httpSession.setAttribute("usingIE", true);
                final String tempStr = userAgent.substring(userAgent.indexOf("MSIE"), userAgent.length());
                try {
                    final Double version = Double.valueOf(tempStr.substring(4, tempStr.indexOf(";")).trim());
                    if (version < 10) {
                        this.httpSession.setAttribute("unsuportedIEVersion", true);
                    }
                } catch (final Exception e) {
                }
            } else {
                this.httpSession.setAttribute("usingIE", false);
            }
        }
    }
}
