package br.com.fanaticosporviagens.infra.controller;

import java.lang.reflect.ParameterizedType;

import org.springframework.util.AutoPopulatingList;

/**
 * Esta classe deve ser utilizada quando for necessario popular um parametro do tipo <tt>List</tt> a partir de um
 * <tt>request<tt> dentro de um <tt>Controller</tt>.
 * 
 * Exemplo:
 * 
 * Parametros do request:
 * 
 * list[0].id = 1
 * list[0].nome = Nome1
 * list[1].id = 2
 * list[1].nome = Nome2
 * 
 * A classe que será populada deverá conter os atributos <tt>id</tt> e <tt>nome</tt>
 * 
 * public class Classe {
 * private Long id;
 * private String nome.
 * // getters and setters
 * }
 * 
 * Você então deverá criar uma classe extendendo <tt>ListWrapper</tt> para fazer o Spring popular
 * automaticamente os seus objetos:
 * 
 * public class ClasseListWrapper extends ListWrapper<Classe> {
 * }
 * 
 * Depois basta incluí-la como parametro no método do controller:
 * 
 * <code>
 * @ RequestMapping(value = "/executarAcao")
 * public executarAcao(final ClasseListWrapper wrapper) {
 *     // Verificar se a lista não está vazia
 *     if (wrapper.isNotEmpty()) {
 *         // recuperar a lista
 *         wrapper.getList();
 *     }
 * } 
 * </code>
 * 
 * @author Carlos Nascimento
 * 
 */
public abstract class ListWrapper<T extends Object> {

    private AutoPopulatingList<T> list = new AutoPopulatingList<T>(getWrapperClass());

    public AutoPopulatingList<T> getList() {
        return this.list;
    }

    public boolean isEmpty() {
        return this.list == null || this.list.isEmpty();
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public void setList(final AutoPopulatingList<T> list) {
        this.list = list;
    }

    @SuppressWarnings("unchecked")
    protected Class<T> getWrapperClass() {
        try {
            return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } catch (final Exception e) {
            throw new RuntimeException("Erro ao recuperar a classe: " + e.getMessage());
        }
    }
}
