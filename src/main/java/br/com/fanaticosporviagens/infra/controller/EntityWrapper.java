package br.com.fanaticosporviagens.infra.controller;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * Esta classe deve ser utilizada quando for necessario popular um parametro do tipo <tt>List</tt> a partir de um
 * <tt>request<tt> dentro de um <tt>Controller</tt>.
 * 
 * @author Carlos Nascimento
 */
public class EntityWrapper<T extends BaseEntity<?>> {

    private T entity;

    public T getEntity() {
        return this.entity;
    }

    public void setEntity(final T entity) {
        this.entity = entity;
    }
}
