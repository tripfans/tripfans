package br.com.fanaticosporviagens.infra.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import br.com.fanaticosporviagens.infra.component.SearchData;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

import com.google.gson.Gson;

/**
 * Representa a resposta que será enviada em formato JSON.
 *
 * @author André Thiago (andrethiago@pgr.mpf.gov.br)
 *
 */
public class JSONResponse {
    private JSONField[] jsonFields;

    private final Log log = LogFactory.getLog(JSONResponse.class);

    // Parametos adicionais para serem enviados na resposta JSON
    private final Map<String, Object> params = new HashMap<String, Object>();

    private Object records;

    private boolean success;

    private Long total;

    public JSONResponse() {
    }

    public JSONResponse(final Collection<?> records) {
        this(records, (SpringBeansProvider.getBean(SearchData.class)).getTotal());
    }

    public JSONResponse(final Collection<?> records, final Long total) {
        this.records = records;
        this.total = total;
    }

    public JSONResponse(final Collection<?> records, final Long total, final String jsonFields) {
        this.records = records;
        this.total = total;
        this.jsonFields = getJSONExtFields(jsonFields);
        try {
            if (jsonFields != null) {
                this.records = createPropertiesMap(records, this.jsonFields);
            }
        } catch (final Exception e) {
            throw new RuntimeException("Erro ao gerar o objeto " + this.getClass() + ". Exception: " + e.getClass().getName() + " - "
                    + e.getMessage(), e);
        }
    }

    public JSONResponse(final Collection<?> registros, final String camposJSON) {
        this(registros, (SpringBeansProvider.getBean(SearchData.class)).getTotal(), camposJSON);
    }

    public JSONResponse(final Long total) {
        this.total = total;
    }

    public JSONResponse(final Map map) {
        this.records = map;
    }

    public JSONResponse(final Object object) {
        this(Collections.singleton(object));
    }

    public JSONResponse(final Object object, final String jsonFieds) {
        this(Collections.singleton(object), jsonFieds);
    }

    public JSONResponse addParam(final String key, final Object value) {
        this.params.put(key, value);
        return this;
    }

    /**
     * Responsável por criar os mapas aninhados caso a propriedade informada contenha mais de um nivel de relacionamento
     *
     * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
     * @param object
     *            o objeto que terá os valores extraídos para o mapa
     * @param property
     *            o nome da propriedade/relacionamento
     * @param rootMap
     *            mapa raiz para o qual as informações serão incluídas
     */
    @SuppressWarnings("unchecked")
    private void createNestedMap(final Object object, String property, final Map<String, Object> rootMap) {
        Map<String, Object> map = new HashMap<String, Object>();
        // Recupera a propriedade raiz
        final String rootProperty = property.split("\\.")[0];
        // Recupera o valor da propriedade raiz
        Object rootValue = getNestedPropertyValue(object, rootProperty);
        // Verifica se é uma propriedade com mais de um nivel
        if (property.indexOf(".") != -1) {
            if (rootValue == null) {
                rootValue = new HashMap<String, Object>();
            }
            // Verifica se o mapa pai já contém a propriedade raiz
            if (rootMap.containsKey(rootProperty)) {
                // recupera o mapa referente a propriedade raiz
                map = (Map<String, Object>) rootMap.get(rootProperty);
            }
            // retira a primeira propriedade da string
            property = property.substring(property.indexOf(".") + 1);
            // Verifica se a propriedade continua com mais de um nivel
            if (property.indexOf(".") != -1) {
                // Chamada recursiva para este método
                createNestedMap(rootValue, property, map);
            } else {
                final Object nestedValue = getNestedPropertyValue(rootValue, property);
                // Coloca o valor da ultima propriedade no Mapa
                map.put(property, nestedValue);
            }
        }
        // Coloca o valor da propriedade raiz no Mapa
        rootMap.put(rootProperty, map);
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object>[] createPropertiesMap(final Collection<?> sourceList, final JSONField[] jsonFields) {
        final Map<String, Object>[] map = new Map[sourceList.size()];
        for (int i = 0; i < sourceList.toArray().length; i++) {
            map[i] = extractProperties(sourceList.toArray()[i], jsonFields);
        }
        return map;
    }

    /**
     * Extrai para um <code>Map<code> todas as propriedades do objeto que estejam no declaradas no array de <code>JSONExtField</code>.
     *
     * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
     * @param object
     *            o objeto que terá os valores extraídos para o mapa
     * @param jsonFields
     *            o array que contem as propriedades que devem ser extraídas
     * @return um <code>Map<code> contendo todas as propriedades que foram informadas no array
     */
    private Map<String, Object> extractProperties(final Object object, final JSONField[] jsonFields) {
        JSONField jsonExtField = null;
        String property;
        final Map<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < jsonFields.length; i++) {
            jsonExtField = jsonFields[i];
            // Comentado pois no caso do Fanaticos não utilizamos o 'mapping'
            /*if (jsonExtField.getMapping() != null) {
                property = jsonExtField.getMapping();
                if (property.indexOf(".") != -1) {
                    createNestedMap(object, property, map);
                } else if (property.indexOf("_") != -1) {
                    map.put(property, getNestedPropertyValue(object, property.replace("_", ".")));
                } else {
                    map.put(property, getNestedPropertyValue(object, property));
                }
            } else {*/
            property = jsonExtField.getName();
            if (property.indexOf(".") != -1) {
                createNestedMap(object, property, map);
            } else {
                map.put(property, getNestedPropertyValue(object, property));
            }
            // }
        }
        return map;
    }

    public JSONResponse fail() {
        this.success = false;
        return this;
    }

    private JSONField[] getJSONExtFields(final String camposJSON) {
        final Gson gson = new Gson();
        return gson.fromJson(camposJSON, JSONField[].class);
    }

    /**
     * Extrai o valor da propriedade do objeto
     *
     * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
     * @param source
     *            o objeto que terá o valor extraído
     * @param property
     *            o nome da propriedade/relacionamento
     */
    private Object getNestedPropertyValue(final Object source, final String property) {
        try {
            return PropertyUtils.getNestedProperty(source, property);
        } catch (final NestedNullException e) {
            return null;
        } catch (final NoSuchMethodException e) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Propriedade " + property + " não existe no objeto " + source.getClass().getSimpleName()
                        + ". Verifique se o nome da propriedade foi digitado corretamente ou se existe o método 'get"
                        + (property.charAt(0) + "").toUpperCase() + property.substring(1) + "'");
            }
            return null;
        } catch (final Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Map<String, Object> getParams() {
        return this.params;
    }

    public Object getRecords() {
        return this.records;
    }

    public Long getTotal() {
        return this.total;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setRecords(final Object records) {
        this.records = records;
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }

    public JSONResponse success() {
        this.success = true;
        return this;
    }

}
