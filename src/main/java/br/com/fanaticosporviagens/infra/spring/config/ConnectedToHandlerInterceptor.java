package br.com.fanaticosporviagens.infra.spring.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ConnectedToHandlerInterceptor extends HandlerInterceptorAdapter {

    private final UsersConnectionRepository usersConnectionRepository;

    public ConnectedToHandlerInterceptor(final UsersConnectionRepository usersConnectionRepository) {
        this.usersConnectionRepository = usersConnectionRepository;
    }

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        if (request.getUserPrincipal() != null) {
            final ConnectionRepository connectionRepository = this.usersConnectionRepository.createConnectionRepository(request.getUserPrincipal()
                    .getName() + "");
            request.setAttribute("connectedToTwitter", connectionRepository.findConnections("twitter").size() > 0);
            request.setAttribute("connectedToFacebook", connectionRepository.findConnections("facebook").size() > 0);
        }
        return true;
    }

}
