package br.com.fanaticosporviagens.infra.spring.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;

@Configuration
@ComponentScan("br.com.tripfans.mail.amqp.publisher")
public class AmqpConfiguration {

    @Autowired
    private Environment environment;

    @Bean
    public AmqpAdmin amqpAdmin() {
        return new RabbitAdmin(connectionFactory());
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        final String host = this.environment.getProperty(AppEnviromentConstants.AMQP_HOST);
        final Integer port = new Integer(this.environment.getProperty(AppEnviromentConstants.AMQP_PORT));

        final CachingConnectionFactory connectionFactory = new CachingConnectionFactory(host, port);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        return connectionFactory;
    }

    @Bean
    public Queue queue() {
        final String queue = this.environment.getProperty(AppEnviromentConstants.AMQP_MAIL_QUEUE);
        return new Queue(queue);
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        final String queue = this.environment.getProperty(AppEnviromentConstants.AMQP_MAIL_QUEUE);
        final RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setQueue(queue);
        template.setRoutingKey(queue);
        return template;
    }
}
