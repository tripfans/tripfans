package br.com.fanaticosporviagens.infra.spring.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Essa classe permite que objetos não gerenciados pelo Spring acessem beans do Spring. <br/>
 * Para isso, basta utilizar o m�todo {@link #getBean(Class)}.
 * 
 * @author André Thiago
 * 
 */
@Scope(value = "singleton")
@Component
public class SpringBeansProvider implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    /**
     * Retorna um bean gerenciado pelo Spring.
     * 
     * @param beanClass
     *            a classe do bean que se deseja obter
     * 
     * @return o bean gerenciado pelo Spring
     */
    public static <T> T getBean(final Class<T> beanClass) {
        return applicationContext.getBean(beanClass);
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        SpringBeansProvider.applicationContext = applicationContext;
    }

}
