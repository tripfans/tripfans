package br.com.fanaticosporviagens.infra.spring.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Before a request is handled: 1. sets the current User in the {@link SecurityContext} from a cookie, if present and the user is still connected to
 * Facebook. 2. requires that the user sign-in if he or she hasn't already.
 * 
 * @author Keith Donald
 */
public final class UserInterceptor extends HandlerInterceptorAdapter {

    private final UsersConnectionRepository connectionRepository;

    // private final UserCookieGenerator userCookieGenerator = new UserCookieGenerator();

    public UserInterceptor(final UsersConnectionRepository connectionRepository) {
        this.connectionRepository = connectionRepository;
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex)
            throws Exception {
        // SecurityContext.remove();
    }

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        // rememberUser(request, response);
        handleSignOut(request, response);
        // if (SecurityContext.userSignedIn() || requestForSignIn(request)) {
        return true;
        // } else {
        // return requireSignIn(request, response);
        // }
    }

    // internal helpers

    private void handleSignOut(final HttpServletRequest request, final HttpServletResponse response) {
        // if (SecurityContext.userSignedIn() && request.getServletPath().startsWith("/signout")) {
        // connectionRepository.createConnectionRepository(SecurityContext.getCurrentUser().getId()).removeConnections("facebook");
        // userCookieGenerator.removeCookie(response);
        // SecurityContext.remove();
        // }
    }

    private void remember(final HttpServletRequest request, final HttpServletResponse response) {
        final String userId = ""; // userCookieGenerator.readCookieValue(request);
        if (userId == null) {
            return;
        }
        if (!userNotFound(userId)) {
            // userCookieGenerator.removeCookie(response);
            return;
        }
        // SecurityContext.setCurrentUser(new User(userId));
    }

    private boolean requestForSignIn(final HttpServletRequest request) {
        return request.getServletPath().startsWith("/signin");
    }

    private boolean requireSignIn(final HttpServletRequest request, final HttpServletResponse response) throws Exception {
        new RedirectView("/signin", true).render(null, request, response);
        return false;
    }

    private boolean userNotFound(final String userId) {
        // doesn't bother checking a local user database: simply checks if the userId is connected to Facebook
        return this.connectionRepository.createConnectionRepository(userId).findPrimaryConnection(Facebook.class) != null;
    }

}
