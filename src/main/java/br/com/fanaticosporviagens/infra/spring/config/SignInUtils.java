package br.com.fanaticosporviagens.infra.spring.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.web.context.request.NativeWebRequest;

import br.com.fanaticosporviagens.infra.util.UserCookieGenerator;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.PosLoginService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

public class SignInUtils {

    private static final UserCookieGenerator userCookieGenerator = new UserCookieGenerator();

    public static TokenBasedRememberMeServices getRememberMeServices() {
        return SpringBeansProvider.getBean(TokenBasedRememberMeServices.class);
    }

    public static UsuarioService getUsuarioService() {
        return SpringBeansProvider.getBean(UsuarioService.class);
    }

    public static void signin(final Authentication authentication, final HttpServletRequest request) {
        if (authentication.getDetails() instanceof Usuario) {
            signin((Usuario) authentication.getDetails(), request);
        } else {
            signin((Usuario) authentication.getPrincipal(), request);
        }
    }

    public static void signin(final Usuario usuario, final HttpServletRequest request) {
        SpringBeansProvider.getBean(PosLoginService.class).popularAtributosSessao(usuario);
        request.getSession().setAttribute("usuario", usuario);
    }

    /**
     * Programmatically signs in the user
     */
    public static void signin(final Usuario usuario, final NativeWebRequest webRequest) {
        signin(usuario, webRequest, false);
    }

    public static void signin(final Usuario usuario, final NativeWebRequest webRequest, final boolean rememberUser) {
        if (rememberUser) {
            userCookieGenerator.addCookie(usuario.getUsername(), webRequest.getNativeResponse(HttpServletResponse.class));

            // add remember me
            getRememberMeServices().loginSuccess(webRequest.getNativeRequest(HttpServletRequest.class),
                    webRequest.getNativeResponse(HttpServletResponse.class), SecurityContextHolder.getContext().getAuthentication());
        }
        signin(usuario, webRequest.getNativeRequest(HttpServletRequest.class));
    }

}
