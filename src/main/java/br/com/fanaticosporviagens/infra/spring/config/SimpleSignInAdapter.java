/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.fanaticosporviagens.infra.spring.config;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

public class SimpleSignInAdapter implements SignInAdapter {

    private final RequestCache requestCache;

    @Inject
    public SimpleSignInAdapter(final RequestCache requestCache) {
        this.requestCache = requestCache;
    }

    @Override
    public String signIn(final String localUserId, final Connection<?> connection, final NativeWebRequest request) {
        // SignInUtils.signin(localUserId);
        return extractOriginalUrl(request);
    }

    private String extractOriginalUrl(final NativeWebRequest request) {
        final HttpServletRequest nativeReq = request.getNativeRequest(HttpServletRequest.class);
        final HttpServletResponse nativeRes = request.getNativeResponse(HttpServletResponse.class);
        final SavedRequest saved = this.requestCache.getRequest(nativeReq, nativeRes);
        if (saved == null) {
            return null;
        }
        this.requestCache.removeRequest(nativeReq, nativeRes);
        removeAutheticationAttributes(nativeReq.getSession(false));
        return saved.getRedirectUrl();
    }

    private void removeAutheticationAttributes(final HttpSession session) {
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

}
