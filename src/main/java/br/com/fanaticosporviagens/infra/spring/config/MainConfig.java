package br.com.fanaticosporviagens.infra.spring.config;

import java.net.MalformedURLException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CommonsHttpSolrServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

import br.com.fanaticosporviagens.infra.component.SearchData;
import br.com.fanaticosporviagens.infra.security.SecurityService;
import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;
import br.com.fanaticosporviagens.util.TripFansEnviroment;

/**
 * Main configuration class for the application. Turns on @Component scanning, loads externalized application.properties, and sets up the database.
 */
@Configuration
@ComponentScan(basePackages = { "br.com.fanaticosporviagens", "br.com.tripfans.externalservices" }, excludeFilters = { @Filter(Configuration.class), })
@ImportResource("classpath:applicationContext.xml")
public class MainConfig {

    @Autowired
    private Environment environment;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UsuarioService usuarioService;

    @PostConstruct
    public void afterInit() {
        final Properties properties = System.getProperties();
        final String context = this.environment.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT);
        final String protocol = this.environment.getProperty(AppEnviromentConstants.APPLICATION_PROTOCOL);
        final String domain = this.environment.getProperty(AppEnviromentConstants.APPLICATION_DOMAIN);
        final String imagesPath = this.environment.getProperty(AppEnviromentConstants.APPLICATION_IMAGES_PATH);
        final String imagesRootDirectory = this.environment.getProperty(AppEnviromentConstants.APPLICATION_IMAGES_ROOT_DIRECTORY);
        final String isDevel = this.environment.getProperty(AppEnviromentConstants.APPLICATION_IS_DEVEL);
        final String isDevelAtWork = this.environment.getProperty(AppEnviromentConstants.APPLICATION_IS_DEVEL_AT_WORK);
        final String isEnabledSocialNetworkActions = this.environment
                .getProperty(AppEnviromentConstants.APPLICATION_IS_ENABLED_SOCIAL_NETWORK_ACTIONS);

        final String serverUrl = protocol + "://" + domain + context;
        final String imagesServerUrl = protocol + "://" + domain + imagesPath;

        properties.setProperty(AppEnviromentConstants.APPLICATION_CONTEXT, context);
        properties.setProperty(AppEnviromentConstants.SERVER_URL, serverUrl);
        properties.setProperty(AppEnviromentConstants.IMAGES_SERVER_URL, imagesServerUrl);
        properties.setProperty(AppEnviromentConstants.APPLICATION_IS_DEVEL, isDevel);

        final String imagesFullDirectory = imagesRootDirectory + imagesPath;
        properties.setProperty(AppEnviromentConstants.IMAGES_FULL_DIRECTORY, imagesFullDirectory);

        final TripFansEnviroment tripFansEnviroment = this.tripFansEnviroment();
        tripFansEnviroment.setDevel(Boolean.valueOf(isDevel));
        if (isDevelAtWork != null) {
            properties.setProperty(AppEnviromentConstants.APPLICATION_IS_DEVEL_AT_WORK, isDevelAtWork);
            tripFansEnviroment.setDevelAtWork(Boolean.valueOf(isDevelAtWork));
        }
        if (isEnabledSocialNetworkActions != null) {
            properties.setProperty(AppEnviromentConstants.APPLICATION_IS_ENABLED_SOCIAL_NETWORK_ACTIONS, isEnabledSocialNetworkActions);
            tripFansEnviroment.setEnableSocialNetworkActions(Boolean.valueOf(isEnabledSocialNetworkActions));
        }
        tripFansEnviroment.setImagesServerUrl(imagesServerUrl);
        tripFansEnviroment.setServerUrl(serverUrl);
        tripFansEnviroment.setFacebookClientId(this.environment.getProperty(AppEnviromentConstants.FACEBOOK_CLIENT_ID));
        tripFansEnviroment.setFourSquareClientId(this.environment.getProperty(AppEnviromentConstants.FOURSQUARE_CLIENT_ID));
        tripFansEnviroment.setGoogleClientId(this.environment.getProperty(AppEnviromentConstants.GOOGLE_CLIENT_ID));
        tripFansEnviroment.setTwitterClientId(this.environment.getProperty(AppEnviromentConstants.TWITTER_CONSUMER_KEY));
        tripFansEnviroment.setBitlyAccessToken(this.environment.getProperty(AppEnviromentConstants.BITLY_ACCESS_TOKEN));
        tripFansEnviroment.setBitlyShortenUrl(this.environment.getProperty(AppEnviromentConstants.BITLY_SHORTEN_URL));
        tripFansEnviroment.setEnabledPedidoDica(Boolean.valueOf(this.environment
                .getProperty(AppEnviromentConstants.APPLICATION_IS_ENABLED_PEDIDO_DICA)));
    }

    @Bean
    public MessageSource messageSource() {
        final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("/WEB-INF/messages/messages");
        return messageSource;
    }

    @Bean
    public TokenBasedRememberMeServices rememberMeServices() {
        final TokenBasedRememberMeServices tokenBasedRememberMeServices = new TokenBasedRememberMeServices("rememberMeKey", this.usuarioService);
        return tokenBasedRememberMeServices;
    }

    @Bean
    @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "request")
    public SearchData searchData() {
        return new SearchData();
    }

    @Bean
    public SolrServer solrServer() throws MalformedURLException {
        final String url = this.environment.getProperty(AppEnviromentConstants.SOLR_SERVER_URL);
        final boolean allowCompression = Boolean.valueOf(this.environment.getProperty(AppEnviromentConstants.SOLR_ALLOW_COMPRESSION));
        final CommonsHttpSolrServer solrServer = new CommonsHttpSolrServer(url);
        solrServer.setAllowCompression(allowCompression);
        return solrServer;
    }

    @Bean
    public TripFansEnviroment tripFansEnviroment() {
        final TripFansEnviroment tripFansEnviroment = new TripFansEnviroment(this.securityService);
        return tripFansEnviroment;
    }

}
