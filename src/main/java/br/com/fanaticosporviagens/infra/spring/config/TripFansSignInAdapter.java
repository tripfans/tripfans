package br.com.fanaticosporviagens.infra.spring.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import br.com.fanaticosporviagens.infra.interceptor.TripFansAuthenticationSuccessHandler;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Service
public class TripFansSignInAdapter implements SignInAdapter {

    @Autowired
    private TripFansAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private RequestCache requestCache;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public String signIn(final String localUserId, final Connection<?> connection, final NativeWebRequest request) {

        this.usuarioService.autenticarExternalProfile(localUserId);

        final HttpServletRequest nativeReq = request.getNativeRequest(HttpServletRequest.class);
        final HttpServletResponse nativeRes = request.getNativeResponse(HttpServletResponse.class);

        return this.authenticationSuccessHandler.onProviderAuthenticationSuccess(nativeReq, nativeRes, SecurityContextHolder.getContext()
                .getAuthentication());
    }

}
