package br.com.fanaticosporviagens.infra.spring.config;

import javax.inject.Inject;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.NotConnectedException;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.foursquare.api.Foursquare;
import org.springframework.social.foursquare.api.impl.FoursquareTemplate;
import org.springframework.social.foursquare.connect.FoursquareConnectionFactory;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

/**
 * Spring Social Configuration.
 */
@Configuration
// @ImportResource("classpath:applicationContext-social.xml")
public class SocialConfig {

    @Inject
    private DataSource dataSource;

    @Inject
    private Environment environment;

    @Bean
    // @Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
    public ConnectionFactoryLocator connectionFactoryLocator() {
        final ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();

        registry.addConnectionFactory(new GoogleConnectionFactory(this.environment.getProperty("google.clientId"), this.environment
                .getProperty("google.clientSecret")));
        registry.addConnectionFactory(new FacebookConnectionFactory(this.environment.getProperty("facebook.clientId"), this.environment
                .getProperty("facebook.clientSecret")));
        registry.addConnectionFactory(new TwitterConnectionFactory(this.environment.getProperty("twitter.consumerKey"), this.environment
                .getProperty("twitter.consumerSecret")));
        registry.addConnectionFactory(new FoursquareConnectionFactory(this.environment.getProperty("foursquare.clientId"), this.environment
                .getProperty("foursquare.clientSecret")));

        return registry;
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public ConnectionRepository connectionRepository() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) {
            throw new IllegalStateException("Unable to get a ConnectionRepository: no user signed in");
        }
        // User user = SecurityContext.getCurrentUser();

        return usersConnectionRepository().createConnectionRepository(authentication.getName());
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public Facebook facebook() {
        final Connection<Facebook> facebook = this.connectionRepository().findPrimaryConnection(Facebook.class);
        return facebook != null ? facebook.getApi() : new FacebookTemplate();
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public Foursquare foursquare() {
        final Connection<Foursquare> foursquare = this.connectionRepository().findPrimaryConnection(Foursquare.class);

        final String clientId = this.environment.getProperty("foursquare.clientId");
        final String clientSecret = this.environment.getProperty("foursquare.clientSecret");
        return foursquare != null ? foursquare.getApi() : new FoursquareTemplate(clientId, clientSecret);
    }

    /**
     * A proxy to a request-scoped object representing the current user's primary Google account.
     *
     * @throws NotConnectedException
     *             if the user is not connected to google.
     */
    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public Google google() {
        return this.connectionRepository().getPrimaryConnection(Google.class).getApi();
    }

    @Bean
    public ProviderSignInController providerSignInController(final RequestCache requestCache) {
        // return new ProviderSignInController(this.connectionFactoryLocator, this.usersConnectionRepository, signInAdapter());
        final ProviderSignInController controller = new ProviderSignInController(connectionFactoryLocator(), usersConnectionRepository(),
                signInAdapter());

        // controller.setSignUpUrl("/signin/cadastro");

        return controller;
    }

    @Bean
    public SignInAdapter signInAdapter() {
        return new TripFansSignInAdapter();
    }

    @Bean
    @Scope(value = "request", proxyMode = ScopedProxyMode.INTERFACES)
    public Twitter twitter() {
        final Connection<Twitter> twitter = this.connectionRepository().findPrimaryConnection(Twitter.class);

        final String consumerKey = this.environment.getProperty("twitter.consumerKey");
        final String consumerSecret = this.environment.getProperty("twitter.consumerSecret");

        return twitter != null ? twitter.getApi() : new TwitterTemplate(consumerKey, consumerSecret);
    }

    @Bean
    // @Scope(value = "singleton", proxyMode = ScopedProxyMode.INTERFACES)
    public UsersConnectionRepository usersConnectionRepository() {
        return new JdbcUsersConnectionRepository(this.dataSource, connectionFactoryLocator(), Encryptors.noOpText());
        // return new UsuarioRepository();
    }

}