package br.com.fanaticosporviagens.infra.spring.config;

import java.util.Locale;
import java.util.Properties;

import javax.inject.Inject;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfig;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.interceptor.ContaUsuarioInterceptor;
import br.com.fanaticosporviagens.infra.interceptor.StatusMessageInterceptor;
import br.com.fanaticosporviagens.util.SessaoWebUtils;

/**
 * Spring MVC Configuration.
 */
@Configuration
@EnableWebMvc
@Import(PersistenceConfig.class)
@ImportResource("classpath:applicationContext-servlet.xml")
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Inject
    private PersistenceConfig persistenceConfig;

    @Inject
    private UsersConnectionRepository usersConnectionRepository;

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        try {
            registry.addWebRequestInterceptor(this.persistenceConfig.openEntityManagerInViewInterceptor());
            final ContaUsuarioInterceptor contaUsuarioInterceptor = this.contaUsuarioInterceptor();
            registry.addInterceptor(contaUsuarioInterceptor).addPathPatterns("/**").excludePathPatterns("/geradorAcoes/**", "/pesquisaTextual/**");
            final LocaleChangeInterceptor localeInterceptor = new LocaleChangeInterceptor();
            localeInterceptor.setParamName("language"); // o nome do parâmetro que troca o idioma
            registry.addInterceptor(localeInterceptor);

            registry.addWebRequestInterceptor(new StatusMessageInterceptor(this.statusMessage()));
            registry.addInterceptor(new ConnectedToHandlerInterceptor(this.usersConnectionRepository));

            super.addInterceptors(registry);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
        super.addResourceHandlers(registry);
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/criarConta/confirmar").setViewName("/conta/confirmar");
        registry.addViewController("/criarConta/completar").setViewName("/conta/completar");
        super.addViewControllers(registry);
    }

    @Bean
    public ContaUsuarioInterceptor contaUsuarioInterceptor() {
        final ContaUsuarioInterceptor interceptor = new ContaUsuarioInterceptor();
        return interceptor;
    }

    @Bean
    public FreeMarkerConfig freeMarkerConfig() {
        final FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPaths(new String[] { "/WEB-INF/freemarker",
        "classpath:/org/springframework/web/servlet/view/freemarker/" });
        return freeMarkerConfigurer;
    }

    @Bean
    public FreeMarkerConfigurationFactoryBean freemarkerMailConfiguration() {
        final FreeMarkerConfigurationFactoryBean freemarkerMailConfiguration = new FreeMarkerConfigurationFactoryBean();
        freemarkerMailConfiguration.setTemplateLoaderPaths(new String[] { "/WEB-INF/freemarker",
        "classpath:/org/springframework/web/servlet/view/freemarker/" });
        freemarkerMailConfiguration.setDefaultEncoding("UTF-8");
        return freemarkerMailConfiguration;
    }

    @Bean
    public FreeMarkerViewResolver freeMarkerViewResolver() {
        final FreeMarkerViewResolver freeMarkerViewResolver = new FreeMarkerViewResolver();
        freeMarkerViewResolver.setCache(true);
        freeMarkerViewResolver.setSuffix(".ftl");
        // freeMarkerViewResolver.setExposeRequestAttributes(true);
        freeMarkerViewResolver.setExposeSpringMacroHelpers(true);
        freeMarkerViewResolver.setContentType("text/html;charset=UTF-8");
        return freeMarkerViewResolver;
    }

    @Bean
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("pt", "BR"));
        return localeResolver;
    }

    @Bean
    public MultipartResolver multipartResolver() {
        final CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        /*
         * multipartResolver.setMaxUploadSize(4194304); multipartResolver.setResolveLazily(true);
         */
        return multipartResolver;
    }

    @Bean
    @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
    public SessaoWebUtils sessaoWebUtils() {
        return new SessaoWebUtils();
    }

    @Bean
    @Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = "session")
    public StatusMessage statusMessage() {
        return new StatusMessage();
    }

    /*@Bean
    public TilesConfigurer tilesConfigurer() {
        final TilesConfigurer configurer = new TilesConfigurer();
        configurer.setDefinitions(new String[] { "/WEB-INF/layouts/tiles.xml" });
        configurer.setCheckRefresh(true);
        return configurer;
    }*/

    @Bean
    public VelocityEngineFactoryBean velocityEngine() {
        final VelocityEngineFactoryBean velocityEngine = new VelocityEngineFactoryBean();
        final Properties properties = new Properties();
        properties.put("resource.loader", "class");
        properties.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        velocityEngine.setVelocityProperties(properties);
        return velocityEngine;
    }

    /*@Bean
    public ViewResolver viewResolver() {
        final InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".html");
        viewResolver.setExposedContextBeanNames(new String[] { "tripFansEnviroment", "searchData", "marcacoesUsuarioLocal", "tagsLocal",
                "todasClassesLocal" });
        return viewResolver;
    }*/
}
