package br.com.fanaticosporviagens.infra.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Security Configuration.
 */
@Configuration
@ImportResource("classpath:applicationContext-security.xml")
public class SecurityConfig {

}
