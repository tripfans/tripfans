package br.com.fanaticosporviagens.infra.model.entity;

/**
 * Esta interface deve ser implementada pelas entidades que devem ter um nome único. Esse
 * nome será utilizado como caminho nas urls, para esconder o identificador.
 * 
 * @author André Thiago
 * 
 */
public interface UrlNameable {

    public String getPrefix();

    public String getUrlPath();

    public void setUrlPath(String urlPath);

}
