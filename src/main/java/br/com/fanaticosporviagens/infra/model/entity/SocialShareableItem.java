package br.com.fanaticosporviagens.infra.model.entity;

/**
 * Esta interface deve ser implementada pelas entidades que podem ser compartilhadas em redes
 * sociais.
 * 
 * @author André Thiago
 * 
 */
public interface SocialShareableItem {

    public String getFacebookPostText();

    public String getFacebookPostTitle();

    public String getShareableLink();

    public String getTwitterPostText();

}
