package br.com.fanaticosporviagens.infra.model.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.MappingException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.acaousuario.AcaoUsuarioService;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryProcessor;

/**
 * Classe de repositorio generica que implementa as operacoes CRUD de uma entidade
 * 
 * @author Carlos Nascimento
 */
@Repository
public class GenericCRUDRepository<E extends BaseEntity<ID>, ID extends Serializable> extends BaseRepository {

    @Autowired
    protected AcaoUsuarioService acaoUsuarioService;

    @Autowired
    protected GenericSearchRepository searchRepository;

    /**
     * Altera uma entidade e gera ações do usuário se for necessário
     * 
     * @param entidade
     *            A entidade a alterar
     */
    public void alterar(final E entidade) {
        this.entityManager.merge(entidade);
        this.entityManager.flush();

        if (entidade instanceof Acao) {
            this.acaoUsuarioService.registrarAcaoAlteracao((Acao) entidade);
        }
    }

    public void alterarPorNamedNativeQuery(final String namedQuery, final Map<String, Object> parameters) {
        SQLQuery query = null;
        try {
            query = (SQLQuery) getSession().getNamedQuery(namedQuery);
        } catch (final MappingException me) {
            final String sql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            query = getSession().createSQLQuery(sql);
        }
        setParameters(query, parameters);
        query.executeUpdate();
    }

    public void alterarPorNamedQuery(final String namedQuery, final Map<String, Object> parameters) {
        Query query = null;
        try {
            query = getSession().getNamedQuery(namedQuery);
        } catch (final MappingException me) {
            final String hql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            query = getSession().createQuery(hql);
        }
        setParameters(query, parameters);
        query.executeUpdate();
    }

    /**
     * Consulta uma entidade na base de dados pelo <code>id</code>
     * 
     * @param id
     *            o identificador do registro a ser consultado na base
     * @return a instancia da entidade caso exista na base
     */
    public E consultarPorId(final Class<E> entityClass, final ID id) {
        return this.entityManager.find(entityClass, id);
    }

    /**
     * Consulta os registros da entidade pelos ids na base de dados
     * 
     * @return uma colecao contendo todos os registros
     */
    @SuppressWarnings("unchecked")
    public List<E> consultarPorIds(final Class<E> entityClass, final Collection<ID> ids) {
        return consultarPorIds(entityClass, (ID[]) ids.toArray());
    }

    /**
     * Consulta os registros da entidade pelos ids na base de dados
     * 
     * @return uma colecao contendo todos os registros
     */
    public List<E> consultarPorIds(final Class<E> entityClass, final ID... ids) {
        final List<E> resultado = new ArrayList<E>();
        for (final ID id : ids) {
            resultado.add(consultarPorId(entityClass, id));
        }
        return resultado;
    }

    /**
     * Consulta uma entidade na base de dados pelo <code>urlPath</code>
     * 
     * @param urlPath
     * @return a instancia da entidade caso exista na base
     */
    public E consultarPorUrlPath(final Class<E> entityClass, final String urlPath) {
        final CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<E> criteria = criteriaBuilder.createQuery(entityClass);
        final Root<E> root = criteria.from(entityClass);
        final TypedQuery<E> query = this.entityManager.createQuery(criteria.select(root).where(criteriaBuilder.equal(root.get("urlPath"), urlPath)));
        try {
            return query.getSingleResult();
        } catch (final NoResultException e) {
            return null;
        }
    }

    /**
     * Consulta todos os registros da entidade na base de dados
     * 
     * @return uma colecao contendo todos os registros
     */
    @SuppressWarnings("unchecked")
    public List<E> consultarTodos(final Class<E> entityClass) {
        return getSession().createCriteria(entityClass).list();
    }

    /**
     * Exclui uma entidade
     * 
     * @param entidade
     *            A entidade a excluir
     */
    public void excluir(final E entidade) {
        this.entityManager.remove(entidade);
        this.entityManager.flush();
    }

    /**
     * Inclui uma entidade e gera ações do usuário se for necessário
     * 
     * @param entidade
     *            A entidade a incluir
     */
    public void incluir(final E entidade) {
        this.entityManager.persist(entidade);
        this.entityManager.flush();

        if (entidade instanceof Acao) {
            this.acaoUsuarioService.registrarAcaoInclusao((Acao) entidade);
        }
    }

    private void setParameters(final Query query, final Map<String, Object> params) {
        if (!CollectionUtils.isEmpty(params)) {
            final String[] paramNames = params.keySet().toArray(new String[params.size()]);
            final Object[] values = params.values().toArray();
            // final String[] paramNames, final Object[] values
            for (int i = 0; i < paramNames.length; i++) {
                if (values[i].getClass().isArray()) {
                    query.setParameterList(paramNames[i], (Object[]) values[i]);
                } else if (values[i] instanceof Collection) {
                    query.setParameterList(paramNames[i], (Collection<?>) values[i]);
                } else {
                    query.setParameter(paramNames[i], values[i]);
                }
            }
        }
    }

}
