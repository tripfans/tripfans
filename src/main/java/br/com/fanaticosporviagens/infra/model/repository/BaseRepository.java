package br.com.fanaticosporviagens.infra.model.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import br.com.fanaticosporviagens.infra.component.SearchData;

/**
 * Classe base para os repositorios
 * 
 * @author Carlos Nascimento
 */
public abstract class BaseRepository implements Repository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected EntityManagerFactory entityManagerFactory;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    @Autowired
    protected SearchData searchData;

    // @Autowired
    // protected JpaTemplate jpaTemplate;

    protected Session getSession() {
        return getSession(false);
    }

    protected Session getSession(final boolean create) {
        if (this.entityManager.isOpen()) {
            Session session = (Session) this.entityManager.getDelegate();
            if (!session.isOpen() && create) {
                session = createSession();
            }
            session.enableFilter("dataExclusao");
            return session;
        } else if (create) {
            createSession();
        }
        return null;
    }

    private Session createSession() {
        // by Carlos
        // Nao sei por qual motivo, mas as vezes o EntityManager está aberto e a Session fechada.
        // Quando isso acontece, tem q fechar o EntityManager e criar um novo para depois criar uma Session
        // Isso foi feito para atender chamadas a partir dos objetos GerenciadorAcoesUsuario (possuem uma chamada para GenericSearchRepository)
        // O que estava acontecendo, é que estava abrindo novas Sessions aqui e estava estourando o pool de conexões, pois as sessões nunca eram
        // fechadas
        if (this.entityManager.isOpen()) {
            this.entityManager.close();
        }
        this.entityManager = this.entityManagerFactory.createEntityManager();
        return (Session) this.entityManager.getDelegate();
    }
}
