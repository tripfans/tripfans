package br.com.fanaticosporviagens.infra.model.service;

import java.util.Map;

import javax.activation.DataSource;
import javax.mail.util.ByteArrayDataSource;

public interface BaseEmailService {

    class AnexoEmail {

        private DataSource dataSource;

        private String extensao;

        private String mimeType;

        private String nome;

        public AnexoEmail(final byte[] arquivo, final String nome, final String extensao, final String mimeType) {
            this.dataSource = new ByteArrayDataSource(arquivo, mimeType);
            this.nome = nome;
            this.extensao = extensao;
            this.mimeType = mimeType;
        }

        public AnexoEmail(final DataSource dataSource, final String nome, final String extensao, final String mimeType) {
            this.dataSource = dataSource;
            this.nome = nome;
            this.extensao = extensao;
            this.mimeType = mimeType;
        }

        public DataSource getDataSource() {
            return dataSource;
        }

        public String getExtensao() {
            return extensao;
        }

        public String getMimeType() {
            return mimeType;
        }

        public String getNome() {
            return nome;
        }

        public void setDataSource(final DataSource dataSource) {
            this.dataSource = dataSource;
        }

        public void setExtensao(final String extensao) {
            this.extensao = extensao;
        }

        public void setMimeType(final String mimeType) {
            this.mimeType = mimeType;
        }

        public void setNome(final String nome) {
            this.nome = nome;
        }
    }

    public abstract void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template,
            final Map<String, Object> parametros, final AnexoEmail anexo);

    public abstract void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template,
            final Map<String, Object> parametros);

    public abstract void enviarEmail(final String remetente, final String destinatario, final String assunto, final EmailTemplate template,
            final Map<String, Object> parametros, final AnexoEmail anexo);

    public abstract void enviarEmail(final String remetente, final String destinatario, final String assunto, final String mensagem,
            final Map<String, Object> parametros);

    public abstract void enviarEmail(final String destinatario, final String assunto, final String mensagem, final Map<String, Object> parametros,
            final AnexoEmail anexo);

    public abstract Map<String, Object> getDefaultTemplateVariables();

}