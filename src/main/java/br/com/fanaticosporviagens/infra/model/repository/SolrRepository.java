package br.com.fanaticosporviagens.infra.model.repository;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.fanaticosporviagens.infra.component.SearchData;
import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.Indexavel;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.ParametrosPesquisaTextual;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.SolrQueryFactory;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.TripFansSolrQueryParams;

public abstract class SolrRepository<T extends Indexavel> implements Repository {

    @Autowired
    private SearchData searchData;

    @Autowired
    private SolrQueryFactory solrQueryFactory;;

    @Autowired
    private SolrServer solrServer;

    public T findById(final T indexavel) {
        final SolrQuery query = new SolrQuery();
        query.setQuery("id:" + indexavel.getId());
        List<T> beans;
        try {
            beans = this.solrServer.query(query).getBeans(getGenericClass());
            if (beans != null) {
                return beans.get(0);
            }
        } catch (final SolrServerException e) {
            e.printStackTrace();
            throw new SystemException("Erro ao pesquisar entidade.", e);
        }
        return null;
    }

    /**
     * Faz a indexação de uma entidade indexável ({@link Indexavel}
     *
     * @param indexavel
     *            a entidade a ser indexada
     * @throws SolrServerException
     * @throws IOException
     */
    public void index(final T indexavel) {
        try {
            this.solrServer.deleteById(indexavel.getId().toString());
            this.solrServer.commit();
            this.solrServer.addBean(indexavel);
            this.solrServer.commit();
        } catch (final Exception e) {
            throw new SystemException("Erro ao indexar entidade.", e);
        }
    }

    @SuppressWarnings("unchecked")
    public SolrResponseTripFans query(final LocalParamsTextualSearchQuery queryParams) {
        // HACK: enquanto não refaço o mapeamento de tags, os filtros abaixo servem para remover dos resultados os passeios/tours
        queryParams.addExtraParam("-tags", "Passeios").addExtraParam("-edgyTextField", "*tour*");

        // pode haver "múltiplas" queries, no caso de pesquisa de destinos em roteiros com múltiplos destinos
        final List<SolrQuery> solrQueries = this.solrQueryFactory.buildQuery(queryParams);
        final SolrResponseTripFans response = new SolrResponseTripFans();
        final Set beans = new LinkedHashSet();
        long total = 0;

        for (final SolrQuery solrQuery : solrQueries) {
            if (solrQueries.size() == 1) {
                // o facet só faz sentido quando a query é única: não há como agrupar em um único resultado o resultado de múltipas queries
                setFacetFields(solrQuery, queryParams.getGroupFields());
            }
            setPaginationData(solrQuery);
            try {
                System.out.println(solrQuery);
                final QueryResponse queryResponse = this.solrServer.query(solrQuery);
                beans.addAll(queryResponse.getBeans(getGenericClass()));
                if (solrQueries.size() == 1) {
                    response.setCamposAgrupados(queryResponse.getFacetFields());
                }
                total += queryResponse.getResults().getNumFound();
            } catch (final SolrServerException e) {
                throw new SystemException("Erro ao executar consulta no Solr", e);
            }
        }
        final List<EntidadeIndexada> entidades = new ArrayList<EntidadeIndexada>();
        entidades.addAll(beans);
        response.setResultados(entidades);
        response.setTotal(total);
        return response;// new ArrayList<T>(beans);
    }

    public SolrResponseTripFans query(final ParametrosPesquisaTextual parametrosPesquisa) {
        //@formatter:off
        return this.query(parametrosPesquisa.getTermoPrincipal(),
                parametrosPesquisa.getParametros(),
                parametrosPesquisa.getAgrupamentos(),
                parametrosPesquisa.getOrdenacoes());
        //@formatter:on
    }

    public List<T> query(final String termoPesquisa, final Double latitude, final Double longitude, final boolean geoSort,
            final Map<String, Object> filtros, final SortField[] defaultSortFields) {
        try {

            final TripFansSolrQueryParams params = new TripFansSolrQueryParams().withTerm(termoPesquisa)
                    .addGeoCoord(new CoordenadaGeografica(latitude, longitude)).sortBy("geodist()");

            final Iterator<Entry<String, Object>> iterator = filtros.entrySet().iterator();
            while (iterator.hasNext()) {
                final Entry<String, Object> pair = iterator.next();
                params.addExtraParam(pair.getKey(), pair.getValue());
                iterator.remove(); // avoids a ConcurrentModificationException
            }

            final SolrQuery query = this.solrQueryFactory.buildQuery(params);
            setPaginationData(query);
            // setSortFields(query, orderBy);
            if (this.searchData.isPaginationDataValid() && (this.searchData.getTotal() == null || this.searchData.getTotal() == 0L)) {
                return Collections.emptyList();
            }
            return this.solrServer.query(query).getBeans(getGenericClass());
        } catch (final SolrServerException e) {
            throw new SystemException("Erro ao executar consulta no Solr");
        }
    }

    public List<T> query(final String termoPesquisa, final Double latitude, final Double longitude, final Map<String, Object> filtrosAdicionais,
            final SortField... orderBy) {
        try {
            final SolrQuery query = this.solrQueryFactory.buildQuery(termoPesquisa, latitude, longitude, filtrosAdicionais);
            setPaginationData(query);
            setSortFields(query, orderBy);
            if (this.searchData.isPaginationDataValid() && (this.searchData.getTotal() == null || this.searchData.getTotal() == 0L)) {
                return Collections.emptyList();
            }
            return this.solrServer.query(query).getBeans(getGenericClass());
        } catch (final SolrServerException e) {
            throw new SystemException("Erro ao executar consulta no Solr");
        }
    }

    public List<T> query(final String termoPesquisa, final Double latitude, final Double longitude, final SortField... orderBy) {
        try {
            final SolrQuery query = this.solrQueryFactory.buildQuery(termoPesquisa, latitude, longitude);
            setPaginationData(query);
            setSortFields(query, orderBy);
            if (this.searchData.isPaginationDataValid() && (this.searchData.getTotal() == null || this.searchData.getTotal() == 0L)) {
                return Collections.emptyList();
            }
            return this.solrServer.query(query).getBeans(getGenericClass());
        } catch (final SolrServerException e) {
            e.printStackTrace();
            throw new SystemException("Erro ao executar consulta no Solr");
        }
    }

    public SolrResponseTripFans query(final String termoPesquisa, final Map<String, Object> filtrosAdicionais, final List<FacetField> facets,
            final List<SortField> orderBy) {
        try {

            final SolrResponseTripFans solrResponse = new SolrResponseTripFans();
            SolrQuery query = null;
            if (termoPesquisa != null && !termoPesquisa.trim().equals("")) {
                query = this.solrQueryFactory.buildQuery(termoPesquisa, filtrosAdicionais);
            } else {
                query = this.solrQueryFactory.buildQuery(filtrosAdicionais);
            }
            setPaginationData(query);
            setFacetFields(query, facets);
            setSortFields(query, orderBy);
            if (this.searchData.isPaginationDataValid() && (this.searchData.getTotal() == null || this.searchData.getTotal() == 0L)) {
                return solrResponse;
            }

            final QueryResponse queryResponse = this.solrServer.query(query);

            solrResponse.setResultados((List<EntidadeIndexada>) queryResponse.getBeans(getGenericClass()));
            solrResponse.setCamposAgrupados(queryResponse.getFacetFields());

            return solrResponse;
        } catch (final SolrServerException e) {
            e.printStackTrace();
            throw new SystemException("Erro ao executar consulta no Solr");
        }
    }

    public SolrResponseTripFans query(final String termoPesquisa, final Map<String, Object> filtrosAdicionais, final List<FacetField> facets,
            final SortField... orderBy) {
        return this.query(termoPesquisa, filtrosAdicionais, facets, Arrays.asList(orderBy));
    }

    public List<T> query(final String termoPesquisa, final Map<String, Object> filtrosAdicionais, final SortField... orderBy) {
        final SolrResponseTripFans solrResponse = this.query(termoPesquisa, filtrosAdicionais, null, orderBy);
        return (List<T>) solrResponse.getResultados();
    }

    public List<T> query(final String termoPesquisa, final SortField... orderBy) {
        try {
            final SolrQuery query = this.solrQueryFactory.buildQuery(termoPesquisa);
            setPaginationData(query);
            setSortFields(query, orderBy);
            if (this.searchData.isPaginationDataValid() && (this.searchData.getTotal() == null || this.searchData.getTotal() == 0L)) {
                return Collections.emptyList();
            }
            return this.solrServer.query(query).getBeans(getGenericClass());
        } catch (final SolrServerException e) {
            throw new SystemException("Erro ao executar consulta no Solr");
        }
    }

    @SuppressWarnings("unchecked")
    private Class<T> getGenericClass() {
        return (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private void setFacetFields(final SolrQuery query, final FacetField[] facets) {
        this.setFacetFields(query, Arrays.asList(facets));
    }

    private void setFacetFields(final SolrQuery query, final List<FacetField> facets) {
        if (facets != null) {
            for (final FacetField facetField : facets) {
                query.addFacetField(facetField.getName());
                if (facetField.getMinCount() != null) {
                    query.setFacetMinCount(facetField.getMinCount());
                }
                if (facetField.getSortType() != null) {
                    query.setFacetSort(facetField.getSortType());
                }
            }
        }
    }

    private void setPaginationData(final SolrQuery query) {
        try {
            if ((this.searchData != null) && this.searchData.isPaginationDataValid()) {
                query.setRows(0);
                this.searchData.setTotal(new Long(this.solrServer.query(query).getResults().getNumFound()));
                query.setStart(this.searchData.getStart());
                query.setRows(this.searchData.getLimit());
            }
        } catch (final SolrServerException e) {
            e.printStackTrace();
            throw new SystemException("Erro ao executar consulta no Solr");
        }

    }

    private void setSortFields(final SolrQuery query, final List<SortField> orderBy) {
        if (orderBy != null) {
            for (final SortField sortField : orderBy) {
                if (sortField.getFieldName() != null) {
                    query.addSortField(sortField.getFieldName(), toSolrOrder(sortField.getDirection()));
                }
            }
        }
    }

    private void setSortFields(final SolrQuery query, final SortField[] orderBy) {
        this.setSortFields(query, Arrays.asList(orderBy));
    }

    private ORDER toSolrOrder(final SortDirection sortDirection) {
        return sortDirection == SortDirection.ASC ? ORDER.asc : ORDER.desc;
    }

}
