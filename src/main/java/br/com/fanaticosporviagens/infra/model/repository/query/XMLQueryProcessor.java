package br.com.fanaticosporviagens.infra.model.repository.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.fanaticosporviagens.infra.exception.SystemException;

/**
 * Gerencia comandos de manipulacao de bancos de dados. Utiliza arquivos de
 * de consultas "*-queries.xml" para armazenar as consultas.
 * 
 * @author Carlos Nascimento
 */
public class XMLQueryProcessor {

    /**
     * Singleton
     */
    private static final XMLQueryProcessor DEFAULT_INSTANCE = new XMLQueryProcessor();

    /**
     * Cria a consulta no formato SQL/HQL para posterior execucao no Repository
     * 
     * @param queryName
     *            nome da consulta
     * @param parameters
     *            parametros para preencher os parametros dinamicos da consulta
     * @return A consulta pronta para ser executada
     */
    public static String createQuery(final String queryName) {
        return createQuery(queryName, new HashMap<String, Object>());
    }

    /**
     * Cria a consulta no formato SQL/HQL para posterior execucao no Repository
     * 
     * @param queryName
     *            nome da consulta
     * @param parameters
     *            parametros para preencher os parametros dinamicos da consulta
     * @return A consulta pronta para ser executada
     */
    public static String createQuery(final String queryName, final Map<String, Object> parameters) {
        // Recupera a consulta
        final XMLQuery queryXml = getXMLQuery(queryName);
        // TODO - Carlos melhorar depois
        // Codigo incluido pois este metodo estava acumulando consultas toda vez que chamava o metodo
        // processarQuery()
        final XMLQuery clone = new XMLQuery();
        clone.setName(queryXml.getName());
        clone.setQuery(queryXml.getQuery());
        // Gera a consulta final
        return processQuery(clone).generateQuery(parameters);
    }

    /**
     * Cria a consulta no formato SQL/HQL para posterior execucao no Repository
     * 
     * @param queryName
     *            nome da consulta
     * @param parameters
     *            parametros para preencher os parametros dinamicos da consulta
     * @return A consulta pronta para ser executada
     */
    public static String createQuery(final XMLQuery queryXML, final Map<String, Object> parameters) {
        queryXML.clearTokens();
        return processQuery(queryXML).generateQuery(parameters);
    }

    public static XMLQueryProcessor getInstance() {
        return DEFAULT_INSTANCE;
    }

    /**
     * Retorna um objeto <tt>XMLQuery</tt> que esteja armazenado no mapa de consultas
     * 
     * @param queryName
     *            nome da consulta a ser localizada no mapa
     * @see XMLQuery.loadQuery(XMLQueryRoot, File)
     */
    public static XMLQuery getXMLQuery(final String queryName) {

        Object object = null;
        if (getInstance().queriesXmlMap != null && !getInstance().queriesXmlMap.isEmpty()) {
            object = getInstance().queriesXmlMap.get(queryName);
        } else {
            object = XMLQueryLoader.createXMLQueriesMap().get(queryName);
        }
        if (object instanceof XMLQuery) {
            return (XMLQuery) object;
        } else if (object instanceof String) {
            // Se o objeto armazenado no map for String significa que este representa
            // o nome do arquivo XML no qual a consulta foi declarada.
            // Estes casos ocorrem somente no ambiente de desenvolvimento e existem para que quando um Repository solicitar
            // uma consulta para o XMLQueryProcessor, o arquivo XML na qual esta consulta esteja declarada seja carregado no momento da solicitacao.
            // Isto e necessario para permitir que, caso tenha tido alguma alteracao e/ou inclusao de consultas, estas consultas sejam
            // carregadas/recarregadas sem a necessidade de reinicializar o Servidor de aplicacao (Caso que so tem sentido no ambiente DESENV)
            return null;
        } else {
            throw new SystemException("A query '" + queryName + "' nao foi encontrada. Por favor verifique se "
                    + "a query esta definida nos arquivos '*-queries.xml'");
        }

    }

    /**
     * Segmenta as consultas oriundas do XML em tokens condicionais ou
     * parametrizados. Esses tokens ficam disponiveis no bean
     * ConsultaEstruturada.
     * 
     * @param config
     *            Bean-espelho de uma tag de consulta vinda do XML
     * @return um bean com os tokens condicionais ou parametrizados prontos para
     *         gerarem consultas SQL/HQL
     */
    public static XMLQuery processQuery(final XMLQuery queryXml) { // Instancia a consulta estruturada
        // Padrao que captura espacos em branco [ \t\n\x0B\f\r]
        final Pattern patternBranco = Pattern.compile("(\\s+).*", Pattern.MULTILINE | Pattern.DOTALL);
        // Padrao que captura qualquer string antes de abre-colchete
        final Pattern patternPreColchete = Pattern.compile("([^\\[]+).*", Pattern.MULTILINE | Pattern.DOTALL);
        // Padrao que captura o miolo entre colchetes e o fecha-colchete
        final Pattern patternColchete = Pattern.compile("\\[([^]]+)(\\]).*", Pattern.MULTILINE | Pattern.DOTALL);
        // Padrao que captura a expressao condicional entre barras
        // Pattern patternBarra = Pattern.compile("\\/\\s*(\\S+)\\s*\\/");
        final Pattern patternBarra = Pattern.compile("\\/(\\S+)\\/");
        // Padrao que captura o parametro de consulta apos dois pontos
        final Pattern patternDoisPontos = Pattern.compile(":([\\w_.]+)", Pattern.MULTILINE);
        // Obtem o comando
        String consulta = queryXml.getQuery();
        final Pattern patternChave = Pattern.compile("[{](\\S+)[}]", Pattern.MULTILINE);
        Matcher matcherChave = patternChave.matcher(consulta);
        while (matcherChave.find()) {
            queryXml.addReplacementToken(matcherChave.group(), matcherChave.group(1));
            consulta = matcherChave.replaceFirst("");
            matcherChave = patternChave.matcher(consulta);
        }
        consulta = queryXml.getQuery();
        for (int posicao = 0, tamanhoString = consulta.length(); posicao < tamanhoString;) {
            final String substring = consulta.substring(posicao);
            final Matcher matcherBranco = patternBranco.matcher(substring);
            final Matcher matcherPreColchete = patternPreColchete.matcher(substring);
            final Matcher matcherColchete = patternColchete.matcher(substring);
            if (matcherBranco.matches()) {
                // ignora espacos em branco
                posicao += matcherBranco.end(1);
            } else if (matcherPreColchete.matches()) {
                // coloca a string antes do colchete como
                // comando de consulta incondicional
                queryXml.addToken(matcherPreColchete.group(1) + " ");
                posicao += matcherPreColchete.end(1);
            } else if (matcherColchete.matches()) {
                // testa se ha expressao entre barras
                final String entreColchete = matcherColchete.group(1);
                final Matcher matchBarra = patternBarra.matcher(entreColchete);
                Matcher matchDoisPontos = patternDoisPontos.matcher(entreColchete);
                if (matchBarra.find()) {
                    final String condicao = matchBarra.group(1);
                    // o comando sao as strings antes e depois do entre-barras
                    final String resto = entreColchete.substring(0, matchBarra.start()) + entreColchete.substring(matchBarra.end());

                    // Se houver uma mistura de barras e parametros
                    // dois-pontos, as duas condicoes devem ser atendidas
                    matchDoisPontos = patternDoisPontos.matcher(resto);
                    if (matchDoisPontos.find()) {
                        final List<String> parametros = new ArrayList<String>();
                        do {
                            parametros.add(matchDoisPontos.group(1));
                        } while (matchDoisPontos.find(matchDoisPontos.end()));
                        queryXml.addConditionalAndParameterizedToken(condicao, resto + " ", parametros);
                    } else {
                        queryXml.addConditionalToken(condicao, resto + " ");
                    }
                } else if (matchDoisPontos.find()) {
                    // procura por todos os parametros nomeados apos dois pontos
                    final List<String> parametros = new ArrayList<String>();
                    do {
                        parametros.add(matchDoisPontos.group(1));
                    } while (matchDoisPontos.find(matchDoisPontos.end()));
                    queryXml.addParameterizedToken(entreColchete + " ", parametros);
                }
                posicao += matcherColchete.end(2);
            }
        }

        return queryXml;
    }

    /**
     * Mapa que guarda todas as consultas que estao aramazenadas dentros dos arquivos XML.
     * As consultas serao armazenadas utilizando o nome (definido no arquivo) como sendo a chave do mapa
     * e
     * Este mapa sera utilizado pela camada de persistencia (<tt>Repository<tt>)  
     * para localizar as consultas
     */
    private Map<String, Object> queriesXmlMap;

    private XMLQueryProcessor() {
    }

    public void setQueriesXmlMap(final Map<String, Object> queriesXmlMap) {
        this.queriesXmlMap = queriesXmlMap;
    }

}
