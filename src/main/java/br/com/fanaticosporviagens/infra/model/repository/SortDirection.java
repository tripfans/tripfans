package br.com.fanaticosporviagens.infra.model.repository;

public enum SortDirection {
    ASC,
    DESC
}