package br.com.fanaticosporviagens.infra.model.repository.query;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Carlos Nascimento
 */
public class XMLQueryCollection {

    private Set<XMLQuery> queries = new HashSet<XMLQuery>();

    public void addQuery(final XMLQuery query) {
        if (this.queries == null) {
            this.queries = new HashSet<XMLQuery>();
        }
        this.queries.add(query);
    }

    public Set<XMLQuery> getQueries() {
        return this.queries;
    }

    public void setQueries(final Set<XMLQuery> queries) {
        this.queries = queries;
    }

}
