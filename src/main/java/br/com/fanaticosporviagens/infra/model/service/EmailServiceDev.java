package br.com.fanaticosporviagens.infra.model.service;

import static br.com.fanaticosporviagens.infra.util.AppEnviromentConstants.*;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.support.RequestContext;
import org.springframework.web.util.HtmlUtils;

import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.util.TripFansEnviroment;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Configurable
@Profile("dev")
public class EmailServiceDev implements BaseEmailService {

    private static final String REMETENTE_DEFAULT = "Tripfans <tripfansfpv@gmail.com>";

    @Inject
    private Environment environment;

    @Autowired
    private FreeMarkerConfigurationFactoryBean freemarkerMailConfiguration;

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private SendEmailService sendEmailService;

    @Autowired
    private ServletContext servletContext;

    @Inject
    private TripFansEnviroment tripFansEnviroment;

    @Autowired
    private VelocityEngine velocityEngine;

    /* (non-Javadoc)
     * @see br.com.fanaticosporviagens.infra.model.service.BaseEmailService#enviarEmail(java.lang.String, java.lang.String, br.com.fanaticosporviagens.infra.model.service.EmailServiceDev.Template, java.util.Map)
     */
    @Override
    public void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template, final Map<String, Object> parametros) {
        this.enviarEmail(null, destinatario, assunto, template, parametros, null);
    }

    @Override
    public void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template, final Map<String, Object> parametros,
            final AnexoEmail anexo) {
        this.enviarEmail(null, destinatario, assunto, template, parametros, anexo);
    }

    /* (non-Javadoc)
     * @see br.com.fanaticosporviagens.infra.model.service.BaseEmailService#enviarEmail(java.lang.String, java.lang.String, java.lang.String, br.com.fanaticosporviagens.infra.model.service.EmailServiceDev.Template, java.util.Map)
     */
    @Override
    public void enviarEmail(String remetente, final String destinatario, final String assunto, final EmailTemplate template,
            final Map<String, Object> parametros, final AnexoEmail anexo) {

        final Map<String, Object> model = getDefaultTemplateVariables();
        if (parametros != null) {
            model.putAll(parametros);
        }
        model.put("tripFansEnviroment", this.tripFansEnviroment);

        try {
            if (remetente == null) {
                remetente = EmailServiceDev.this.mailSender.getSession().getProperty("username");
                // remetente = EmailServiceDev.this.mailSender.getUsername();
            }
            model.put("springMacroRequestContext", new RequestContext(EmailServiceDev.this.request, null, EmailServiceDev.this.servletContext, model));
            final String body = FreeMarkerTemplateUtils.processTemplateIntoString(EmailServiceDev.this.freemarkerMailConfiguration
                    .createConfiguration().getTemplate(template.getNomeArquivo()), model);

            send(createMessagePreparator(remetente, destinatario, assunto, body, parametros, anexo));
        } catch (final Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void enviarEmail(final String destinatario, final String assunto, final String mensagem, final Map<String, Object> parametros,
            final AnexoEmail anexo) {
        send(createMessagePreparator(null, destinatario, assunto, mensagem, parametros, anexo));
    }

    /* (non-Javadoc)
     * @see br.com.fanaticosporviagens.infra.model.service.BaseEmailService#enviarEmail(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map)
     */
    @Override
    public void enviarEmail(final String remetente, final String destinatario, final String assunto, final String mensagem,
            final Map<String, Object> parametros) {
        send(createMessagePreparator(remetente, destinatario, assunto, mensagem, parametros, null));
    }

    /* (non-Javadoc)
     * @see br.com.fanaticosporviagens.infra.model.service.BaseEmailService#getDefaultTemplateVariables()
     */
    @Override
    public Map<String, Object> getDefaultTemplateVariables() {
        final Map<String, Object> templateVars = new HashMap<String, Object>();
        templateVars.put("protocolo", this.environment.getProperty(APPLICATION_PROTOCOL));
        templateVars.put("dominio", this.environment.getProperty(APPLICATION_DOMAIN));
        templateVars.put("contexto", this.environment.getProperty(APPLICATION_CONTEXT));

        templateVars.put("messageSource", this.messageSource);

        return templateVars;
    }

    private MimeMessagePreparator createMessagePreparator(final String from, final String to, final String subject, final String message,
            final Map<String, Object> parametros, final AnexoEmail anexo) {
        final Map<String, Object> model = getDefaultTemplateVariables();
        if (parametros != null) {
            model.putAll(parametros);
        }

        return new MimeMessagePreparator() {
            @Override
            public void prepare(final MimeMessage mimeMessage) throws Exception {
                // use the true flag to indicate you need a multipart message
                mimeMessage.setSubject(subject, "UTF-8");
                final MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

                String fromParam = "TripFans";
                if (parametros.get("nomeUsuarioConvidante") != null) {
                    fromParam = parametros.get("nomeUsuarioConvidante") + ", via TripFans";
                } else if (parametros.get("autor") != null) {
                    final Usuario autor = (Usuario) parametros.get("autor");
                    fromParam = autor.getDisplayName() + ", via TripFans";
                }

                messageHelper.setFrom(from, HtmlUtils.htmlUnescape(fromParam));
                messageHelper.setTo(to);
                messageHelper.setSubject(subject);

                /*final String body = VelocityEngineUtils.mergeTemplateIntoString(EmailService.this.velocityEngine,
                        getTemplateLocation(EMAIL_CONFIRMACAO), templateVars);*/

                messageHelper.setText(message, true);
                /*final FileSystemResource resource = new FileSystemResource(SpringBeansProvider.getApplicationContext()
                        .getResource("/resources/images/logo_150.png").getFile());
                messageHelper.addInline("logo", resource);*/

                if (anexo != null) {
                    messageHelper.addAttachment(anexo.getNome() + "." + anexo.getExtensao(), anexo.getDataSource());
                } /*else {

                    // Construindo o texto do corpo do email
                    final MimeBodyPart textBodyPart = new MimeBodyPart();
                    textBodyPart.setText(message);

                    final ByteArrayOutputStream outputStream = null;

                    // Construindo a parte do anexo
                    final DataSource dataSource = new ByteArrayDataSource(anexo.getArquivo(), anexo.getMimeType());
                    final MimeBodyPart pdfBodyPart = new MimeBodyPart();
                    pdfBodyPart.setDataHandler(new DataHandler(dataSource));
                    pdfBodyPart.setFileName(anexo.getNome() + "." + anexo.getExtensao());

                    // Construindo mime multi part
                    final MimeMultipart mimeMultipart = new MimeMultipart();
                    mimeMultipart.addBodyPart(textBodyPart);
                    mimeMultipart.addBodyPart(pdfBodyPart);

                    // messageHelper.
                  }*/
            }
        };
    }

    private void send(final MimeMessagePreparator preparator) {
        this.sendEmailService.sendEmail(preparator);
    }

}
