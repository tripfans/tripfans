package br.com.fanaticosporviagens.infra.model.service;

import static br.com.fanaticosporviagens.infra.util.AppEnviromentConstants.APPLICATION_CONTEXT;
import static br.com.fanaticosporviagens.infra.util.AppEnviromentConstants.APPLICATION_DOMAIN;
import static br.com.fanaticosporviagens.infra.util.AppEnviromentConstants.APPLICATION_PROTOCOL;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.support.RequestContext;

import br.com.fanaticosporviagens.util.TripFansEnviroment;
import br.com.tripfans.mail.amqp.domain.Mensagem;
import br.com.tripfans.mail.amqp.publisher.PublisherService;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Configurable
@Profile("prod")
public class EmailService implements BaseEmailService {

    private static final String REMETENTE_DEFAULT = "Tripfans <membros@tripfans.com.br>";

    @Autowired
    private PublisherService amqpMailService;

    @Inject
    private Environment environment;

    @Autowired
    private FreeMarkerConfigurationFactoryBean freemarkerMailConfiguration;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private ServletContext servletContext;

    @Inject
    private TripFansEnviroment tripFansEnviroment;

    @Autowired
    private VelocityEngine velocityEngine;

    private Mensagem createMessagePreparator(final String from, final String to, final String subject, final String message,
            final Map<String, Object> parametros, final BaseEmailService.AnexoEmail anexo) {
        final Map<String, Object> model = getDefaultTemplateVariables();
        if (parametros != null) {
            model.putAll(parametros);
        }

        final Mensagem msg = new Mensagem();

        msg.setFrom(from);
        msg.setTo(to);
        msg.setSubject(subject);
        msg.setBody(message);

        return msg;
    }

    @Override
    public void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template, final Map<String, Object> parametros) {
        this.enviarEmail(null, destinatario, assunto, template, parametros, null);
    }

    @Override
    public void enviarEmail(final String destinatario, final String assunto, final EmailTemplate template, final Map<String, Object> parametros,
            final AnexoEmail anexo) {
        this.enviarEmail(REMETENTE_DEFAULT, destinatario, assunto, template, parametros, anexo);
    }

    @Override
    public void enviarEmail(String remetente, final String destinatario, final String assunto, final EmailTemplate template,
            final Map<String, Object> parametros, final AnexoEmail anexo) {

        final Map<String, Object> model = getDefaultTemplateVariables();
        if (parametros != null) {
            model.putAll(parametros);
        }
        model.put("tripFansEnviroment", this.tripFansEnviroment);

        try {
            if (remetente == null) {
                remetente = REMETENTE_DEFAULT;
            }
            model.put("springMacroRequestContext", new RequestContext(EmailService.this.request, null, EmailService.this.servletContext, model));
            final String body = FreeMarkerTemplateUtils.processTemplateIntoString(EmailService.this.freemarkerMailConfiguration.createConfiguration()
                    .getTemplate(template.getNomeArquivo()), model);

            // enviarEmail(remetente, destinatario, assunto, body, parametros);
            send(createMessagePreparator(remetente, destinatario, assunto, body, parametros, null));
        } catch (final Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void enviarEmail(final String destinatario, final String assunto, final String mensagem, final Map<String, Object> parametros,
            final AnexoEmail anexo) {
        send(createMessagePreparator(REMETENTE_DEFAULT, destinatario, assunto, mensagem, parametros, anexo));
    }

    @Override
    public void enviarEmail(final String remetente, final String destinatario, final String assunto, final String mensagem,
            final Map<String, Object> parametros) {
        send(createMessagePreparator(remetente, destinatario, assunto, mensagem, parametros, null));
    }

    @Override
    public Map<String, Object> getDefaultTemplateVariables() {
        final Map<String, Object> templateVars = new HashMap<String, Object>();
        templateVars.put("protocolo", this.environment.getProperty(APPLICATION_PROTOCOL));
        templateVars.put("dominio", this.environment.getProperty(APPLICATION_DOMAIN));
        templateVars.put("contexto", this.environment.getProperty(APPLICATION_CONTEXT));

        templateVars.put("messageSource", this.messageSource);

        return templateVars;
    }

    private void send(final Mensagem preparator) {
        this.amqpMailService.publish(preparator);
    }

}
