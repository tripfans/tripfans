package br.com.fanaticosporviagens.infra.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * Classe base para os serviços
 * 
 * @author Carlos Nascimento
 */
public abstract class BaseService implements Service {

    @Autowired
    protected MessageSource messageSource;

    protected Usuario getUsuarioAutenticado() {
        if (SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof Usuario) {
            return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } else if (SecurityContextHolder.getContext().getAuthentication().getDetails() instanceof Usuario) {
            // usuários de redes externas
            return (Usuario) SecurityContextHolder.getContext().getAuthentication().getDetails();
        }
        return null;
    }

}
