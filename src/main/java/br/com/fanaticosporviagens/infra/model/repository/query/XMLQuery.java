package br.com.fanaticosporviagens.infra.model.repository.query;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Classe que armazena uma consulta HQL/SQL.
 * 
 * @author Carlos Nascimento
 * 
 */
public class XMLQuery {

    private String name;

    /**
     * Conjunto de parâmetros e condições
     */
    private final Set<String> paramsAndConditions = new HashSet<String>();

    private String query;

    private final Set<ReplacementToken> replacementTokens = new HashSet<ReplacementToken>();

    /**
     * Os tokens da consulta
     */
    private final List<SimpleToken> tokens = new ArrayList<SimpleToken>();

    public void addConditionalAndParameterizedToken(final String condition, final String token, final List<String> params) {
        this.paramsAndConditions.addAll(params);
        this.paramsAndConditions.add(condition);
        this.tokens.add(new ConditionalAndParameterizedToken(condition, params, token));
    }

    /**
     * Adiciona token na consulta, dependente de condição
     * 
     * @param condition
     *            A condição de inclusão
     * @param token
     *            O token
     */
    public void addConditionalToken(final String condition, final String token) {
        this.paramsAndConditions.add(condition);
        this.tokens.add(new ConditionalToken(condition, token));
    }

    /**
     * Adiciona token parametrizado
     * 
     * @param token
     *            O token
     * @param params
     *            Os parâmetros
     */
    public void addParameterizedToken(final String token, final List<String> params) {
        this.paramsAndConditions.addAll(params);
        this.tokens.add(new ParameterizedToken(params, token));
    }

    public void addReplacementToken(final String token, final String param) {
        getParamsAndConditions().add(param);
        this.replacementTokens.add(new ReplacementToken(token, param));
    }

    /**
     * Adiciona token na consulta, incondicionalmente
     * 
     * @param token
     *            O token
     */
    public void addToken(final String token) {
        this.tokens.add(new SimpleToken(token));
    }

    /**
     * Limpa os tokens
     */
    public void clearTokens() {
        this.tokens.clear();
    }

    /**
     * Gera a consulta
     * 
     * @param bean
     *            O bean com parâmetros básicos
     * @param parametrosAdicionais
     *            Parâmetros adicionais
     * @return A query
     */
    public String generateQuery(final Map<String, Object> params) {

        // String que contem o comando da consulta que sera retornada
        final StringBuffer buffer = new StringBuffer();

        // Itera pelos tokens e os popula com os valores pesquisados no mapa e
        // nos beans
        for (final SimpleToken token : this.tokens) {

            if (token instanceof ConditionalAndParameterizedToken) {
                evaluateConditionalAndParameterizedToken((ConditionalAndParameterizedToken) token, params, buffer);

            } else if (token instanceof ConditionalToken) {
                evaluateConditionalToken((ConditionalToken) token, params, buffer);

            } else if (token instanceof ParameterizedToken) {
                evaluateParameterizedToken((ParameterizedToken) token, params, buffer);

            } else {
                buffer.append(token.getToken());
            }
        }
        String result = buffer.toString();
        for (final ReplacementToken token : this.replacementTokens) {
            final Object value = params.get(token.getParam());
            if (value != null) {
                while (result.indexOf(token.getToken()) != -1) {
                    result = result.replace(token.getToken(), value.toString());
                }
            } else {
                throw new RuntimeException("parametro de substuicao '" + token.getToken() + "' nao informado");
            }
        }
        // Retorna a consulta
        return result;
    }

    public String getName() {
        return this.name;
    }

    /**
     * Recupera os parâmetros e condições da consulta
     * 
     * @return Os parâmetros e condições
     */
    public Set<String> getParamsAndConditions() {
        return this.paramsAndConditions;
    }

    public String getQuery() {
        return this.query;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setQuery(final String query) {
        this.query = query;
    }

    /**
     * Verifica a validade de um valor (se foi iniciado). Retorna <code>false</code> só, e somente só, se:
     * <ul>
     * <li>o objeto for <code>null</code> ou</li>
     * <li>número zero ou</li>
     * <li>coleção vazia</li>
     * <li>string de tamanho zero</li>
     * </ul>
     * 
     * @param value
     *            O valor a validar
     */
    protected boolean validateValue(final Object value) {
        if (value == null) {
            return false;
        } else if (value instanceof Boolean && ((Boolean) value).booleanValue() == false) {
            return false;
        } else if (value instanceof Number && ((Number) value).floatValue() == 0.0) {
            return false;
        } else if (value instanceof String && ((String) value).length() == 0) {
            return false;
        } else if (value instanceof List && ((List<?>) value).size() == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void evaluateConditionalAndParameterizedToken(final ConditionalAndParameterizedToken token, final Map<String, Object> values,
            final StringBuffer buffer) {

        // Verifica se a condição deste token está satisfeita
        final StringBuffer bufferCond = new StringBuffer();
        final ConditionalToken condToken = new ConditionalToken(token.getCondition(), token.getToken());
        final boolean continueCheck = evaluateConditionalToken(condToken, values, bufferCond);

        if (continueCheck) {
            // Verifica se os parametros deste token estão preenchidos
            final StringBuffer bufferParametros = new StringBuffer();
            final ParameterizedToken paramToken = new ParameterizedToken(token.getParams(), token.getToken());
            evaluateParameterizedToken(paramToken, values, bufferParametros);

            // Se a condicao e os parametros foram passados
            // o token é adicionado ao comando final
            if (bufferCond.length() > 0 && bufferParametros.length() > 0) {
                buffer.append(token.getToken());
            }
        } else {
            // Remover os parametros
            values.remove(token.getCondition());
            for (final Object name : token.getParams()) {
                values.remove(name);
            }
        }
    }

    private boolean evaluateConditionalToken(final ConditionalToken tokenCond, final Map<String, Object> values, final StringBuffer buffer) {

        // Testa por valores de verdade
        final Object value = values.get(tokenCond.getCondition());
        // Se for verdade, adiciona ao comando
        final boolean result = validateValue(value);
        if (validateValue(value)) {
            buffer.append(tokenCond.getToken());
            values.remove(tokenCond.getCondition());
        }
        return result;
    }

    private void evaluateParameterizedToken(final ParameterizedToken tokenParam, final Map<String, Object> values, final StringBuffer buffer) {

        // Verifica se todos os parametros estao presentes (not-null)
        boolean paramsOk = true;
        for (final Iterator<?> it = tokenParam.getParams().iterator(); it.hasNext();) {
            final String nomeValor = it.next().toString();
            final Object value = values.get(nomeValor);
            if (value == null) {
                paramsOk = false;
                break;
            }
        }
        if (paramsOk) {
            buffer.append(tokenParam.getToken());
        }
    }

    /**
     * Trecho de OQL/SQL que é adicionado à consulta se a condição for
     * satisfeita e se os parametros estiverem presentes.
     */
    protected class ConditionalAndParameterizedToken extends ParameterizedToken {

        private final String condition;

        public ConditionalAndParameterizedToken(final String condition, final List<String> params, final String token) {

            super(params, token);
            this.condition = condition;
        }

        public String getCondition() {
            return this.condition;
        }
    }

    /**
     * Trecho de HQL/SQL que é adicionado à consulta se a condição for
     * satisfeita.
     */
    protected class ConditionalToken extends SimpleToken {

        /**
         * A condição associada
         */
        private final String condition;

        /**
         * Instancia um token condicional
         * 
         * @param condition
         *            A condição
         * @param token
         *            O conteúdo do token
         */
        public ConditionalToken(final String condition, final String token) {
            super(token);
            this.condition = condition;
        }

        /**
         * Recupera a condição
         * 
         * @return A condição
         */
        public String getCondition() {
            return this.condition;
        }
    }

    /**
     * Trecho de HQL/SQL que é adicionado à consulta se os todos parâmetros
     * forem passados.
     */
    protected class ParameterizedToken extends SimpleToken {

        /**
         * Coleção de parâmetros
         */
        private final List<?> params;

        /**
         * Cria um token parametrizado
         * 
         * @param params
         *            A coleção de parâmetros
         * @param token
         *            o conteúdo do token
         */
        public ParameterizedToken(final List<?> params, final String token) {
            super(token);
            this.params = params;
        }

        /**
         * Recupera os parâmetros do token
         * 
         * @return Os parâmetros do token
         */
        public List<?> getParams() {
            return this.params;
        }
    }

    protected class ReplacementToken extends SimpleToken {

        private String param;

        public ReplacementToken(final String token, final String param) {
            super(token);
            setParam(param);
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof ReplacementToken) {
                final ReplacementToken token = (ReplacementToken) obj;
                boolean parametroIgual = false;
                boolean tokenIgual = false;
                if (getParam() != null) {
                    parametroIgual = getParam().equals(token.getParam());
                } else {
                    parametroIgual = token.getParam() == null;
                }
                if (getToken() != null) {
                    tokenIgual = getToken().equals(token.getToken());
                } else {
                    tokenIgual = token.getToken() == null;
                }
                return parametroIgual && tokenIgual;
            }

            return false;
        }

        public String getParam() {
            return this.param;
        }

        public void setParam(final String param) {
            this.param = param;
        }

    }

    /**
     * Trecho de HQL/SQL que é adicionado à consulta incondicionalmente.
     */
    protected class SimpleToken {

        /**
         * O conteúdo do token
         */
        private final String token;

        /**
         * Cria um token simples
         * 
         * @param token
         *            O conteúdo do token
         */
        public SimpleToken(final String token) {
            this.token = token;
        }

        /**
         * Retorna o contéudo do token
         * 
         * @return O conteúdo do token
         */
        public String getToken() {
            return this.token;
        }
    }

}
