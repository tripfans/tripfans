package br.com.fanaticosporviagens.infra.model.repository;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import br.com.fanaticosporviagens.infra.converter.AliasToEntityConverter;
import br.com.fanaticosporviagens.infra.model.repository.query.XMLQuery;
import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryProcessor;

/**
 * Repositório genérico para realizar consultas em geral
 *
 * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
 */
@Repository
public class GenericSearchRepository extends BaseRepository {

    /**
     * Classe criada para auxiliar na conversão do resultado de uma consulta SQL para objeto Java.
     *
     * @author Carlos Nascimento, André Thiago
     */
    class CustomResultTransformer implements ResultTransformer {

        private static final long serialVersionUID = 2613065904580922692L;

        private final Class<?> resultClass;

        public CustomResultTransformer(final Class<?> resultClass) {
            if (resultClass == null)
                throw new IllegalArgumentException("resultClass cannot be null");
            this.resultClass = resultClass;
        }

        @SuppressWarnings("rawtypes")
        @Override
        public List<?> transformList(final List collection) {
            return collection;
        }

        @Override
        public Object transformTuple(final Object[] tuple, final String[] aliases) {
            Object result = null;
            try {
                result = GenericSearchRepository.this.entityConverter.convert(this.resultClass, aliases, tuple);
            } catch (final Exception e) {
                e.printStackTrace();
                throw new HibernateException("Erro ao tranformar entidade do tipo " + this.resultClass, e);
            }
            return result;
        }
    }

    private static final String ORDER_BY = " ORDER BY %s %s";

    private static final String TAMANHO_AMOSTRA_KEY = "TAMANHO_AMOSTRA_KEY";

    @Autowired
    private AliasToEntityConverter entityConverter;

    private final Log logger = LogFactory.getLog(GenericSearchRepository.class);

    /**
     * Consulta os registros baseado em uma HQL. <br/>
     * <br/>
     * Se necessário for, leva em conta parâmetros de paginação.
     *
     * @param hql
     *            o HQL da consulta
     * @param params
     *            os parâmetros (chave,valor) para a consulta
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Object> List<E> consultaHQL(final String hql, final Map<String, Object> params) {
        final Query query = this.getSession().createQuery(this.getQueryStringWithOrderByClause(hql));
        if (!params.isEmpty() && params.containsKey(TAMANHO_AMOSTRA_KEY)) {
            query.setMaxResults((Integer) params.remove(TAMANHO_AMOSTRA_KEY));
        }
        Long count = 0L;
        try {
            paginarQuery(query);
            if (this.searchData.isEnableCount()) {
                count = this.totalizarConsultaHQL(hql, params);
                if (count == 0L) {
                    return Collections.emptyList();
                }
                this.searchData.setTotal(count);
            }
        } catch (final org.springframework.beans.factory.BeanCreationException e) {
            // ignora o erro e continua a execucao
        }
        this.setParameters(query, params);
        return query.list();
    }

    /**
     * Consulta os registros baseado em uma HQL. Leva em conta parâmetros de paginação passados como parâmetros.
     *
     * @param hql
     *            o HQL da consulta
     * @param params
     *            os parâmetros (chave,valor) para a consulta
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Object> List<E> consultaHQL(final String hql, final Map<String, Object> params, final Integer limit) {
        final Query query = this.getSession().createQuery(this.getQueryStringWithOrderByClause(hql));
        query.setMaxResults(limit);
        this.setParameters(query, params);
        return query.list();
    }

    /**
     * Consulta os registros baseado em uma HQL. Leva em conta parâmetros de paginação passados como parâmetros.
     *
     * @param hql
     *            o HQL da consulta
     * @param params
     *            os parâmetros (chave,valor) para a consulta
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Object> List<E> consultaHQL(final String hql, final Map<String, Object> params, final Integer start, final Integer limit) {
        final Query query = this.getSession().createQuery(this.getQueryStringWithOrderByClause(hql));
        query.setFirstResult(start);
        query.setMaxResults(limit);
        this.setParameters(query, params);
        return query.list();
    }

    /**
     * @see consultarPorNamedNativeQuery(Class<T>, String, Map<String, Object>, Map<String, Type>)
     */
    public <T extends Object> List<T> consultarPorNamedNativeQuery(final String namedQuery, final Class<T> returnType) {
        return this.consultarPorNamedNativeQuery(namedQuery, new HashMap<String, Object>(), null, returnType);
    }

    /**
     * @see consultarPorNamedNativeQuery(Class<T>, String, Map<String, Object>, Map<String, Type>)
     */
    public <T extends Object> List<T> consultarPorNamedNativeQuery(final String namedQuery, final Map<String, Object> parameters,
            final Class<T> returnType) {
        return this.consultarPorNamedNativeQuery(namedQuery, parameters, null, returnType);
    }

    /**
     * Realiza uma consulta SQL na base de dados
     *
     * @param returnType
     *            tipo de objeto que conterá na lista que será retornada
     * @param namedQuery
     *            nome da consulta que porderá ser uma anotação do tipo <tt>@NamedNativeQuery</tt> ou
     * @param parameters
     *            parametros repassados para filtros na consultas
     * @param scalars
     *            map contendo o mapeamento a ser utilizado para conversão de tipos (<tt>org.hibernate.type.Type</tt>)
     */
    @SuppressWarnings("unchecked")
    public <T extends Object> List<T> consultarPorNamedNativeQuery(final String namedQuery, final Map<String, Object> parameters,
            final Map<String, Type> scalars, final Class<T> returnType) {
        try {
            final Query query = this.getSession().getNamedQuery(namedQuery);
            paginarQuery(query);
            return query.list();
        } catch (final MappingException me) {
            final String sql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            return this.consultaSQL(sql, parameters, returnType);
        }
    }

    /**
     * Realiza uma consulta HQL na base de dados
     *
     * @param returnType
     *            tipo de objeto que conterá na lista que será retornada
     * @param namedQuery
     *            nome da consulta que porderá ser uma anotação do tipo <tt>@NamedQuery</tt> ou uma consulta mapeada em um arquivo de consultas
     *            (*-queries.xml)
     * @param parameters
     *            parametros repassados para filtros na consultas
     */
    @SuppressWarnings("unchecked")
    public <T extends Object> List<T> consultarPorNamedQuery(final String namedQuery, final Class<T> returnType) {
        try {
            final Query query = this.getSession().getNamedQuery(namedQuery);
            paginarQuery(query);
            return query.list();
        } catch (final MappingException me) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug(me.getMessage());
            }

            final HashMap<String, Object> parameters = new HashMap<String, Object>();
            final String hql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            return this.consultaHQL(hql, parameters);
        }
    }

    /**
     * Realiza uma consulta HQL na base de dados
     *
     * @param returnType
     *            tipo de objeto que conterá na lista que será retornada
     * @param namedQuery
     *            nome da consulta que porderá ser uma anotação do tipo <tt>@NamedQuery</tt> ou uma consulta mapeada em um arquivo de consultas
     *            (*-queries.xml)
     * @param parameters
     *            parametros repassados para filtros na consultas
     */
    @SuppressWarnings("unchecked")
    public <T extends Object> List<T> consultarPorNamedQuery(final String namedQuery, final Map<String, Object> parameters, final Class<T> returnType) {
        try {
            final Query query = this.getSession().getNamedQuery(namedQuery);
            paginarQuery(query);
            return query.list();
        } catch (final MappingException me) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug(me.getMessage());
            }
            final String hql = XMLQueryProcessor.createQuery(namedQuery, parameters);

            return this.consultaHQL(hql, parameters);
        }
    }

    /**
     * Realiza uma consulta HQL na base de dados
     *
     * @param returnType
     *            tipo de objeto que conterá na lista que será retornada
     * @param namedQuery
     *            nome da consulta que porderá ser uma anotação do tipo <tt>@NamedQuery</tt> ou uma consulta mapeada em um arquivo de consultas
     *            (*-queries.xml)
     * @param parameters
     *            parametros repassados para filtros na consultas
     * @param limit
     *            quantidade máxima de resultados para serem retornados na consulta
     */
    @SuppressWarnings("unchecked")
    public <T extends Object> List<T> consultarPorNamedQuery(final String namedQuery, final Map<String, Object> parameters, final Integer limit,
            final Class<T> returnType) {
        try {
            final Query query = this.getSession().getNamedQuery(namedQuery);
            if (limit != null) {
                query.setMaxResults(limit);
            }
            return query.list();
        } catch (final MappingException me) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug(me.getMessage());
            }
            final String hql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            parameters.put(TAMANHO_AMOSTRA_KEY, limit);
            return this.consultaHQL(hql, parameters);
        }
    }

    /**
     * @see consultaSQL(String, Map<String, Object>, Class<E>, Map<String, Type>)
     */
    public <E extends Object> List<E> consultaSQL(final String sql, final Class<E> returnType) {
        return this.consultaSQL(sql, null, returnType, null);
    }

    public List<?> consultaSQL(final String sql, final Map<String, Object> namedParameters) {
        return this.consultaSQL(sql, namedParameters, null);
    }

    /**
     * @see consultaSQL(String, Map<String, Object>, Class<E>, Map<String, Type>)
     */
    public <E extends Object> List<E> consultaSQL(final String sql, final Map<String, Object> namedParameters, final Class<E> returnType) {
        return this.consultaSQL(sql, namedParameters, returnType, null);
    }

    /**
     * Consulta os registros baseado em um SQL nativo. <br/>
     * <br/>
     * Realiza uma consulta baseada em SQL. Se necessário, fará o sql count para a paginação.
     *
     * @param sql
     *            o SQL da consulta
     * @param namedParameters
     * @param scalars
     *            map contendo o mapeamento a ser utilizado para conversão de tipos (<tt>org.hibernate.type.NullableType</tt>)
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Object> List<E> consultaSQL(final String sql, final Map<String, Object> namedParameters, final Class<E> returnType,
            final Map<String, Type> scalars) {
        final SQLQuery query = this.getSession(true).createSQLQuery(this.getQueryStringWithOrderByClause(sql));
        // Adiciona à query os 'scalars' para conversão correta dos tipos caso o mapa de scalars venha preenchido
        if (scalars != null && !scalars.isEmpty()) {
            for (final String key : scalars.keySet()) {
                query.addScalar(key, scalars.get(key));
            }
        }

        Long count = 0L;
        try {
            if (this.searchData.isPaginationDataValid()) {
                if (this.logger.isDebugEnabled()) {
                    this.logger.debug(String.format("Query paginada com start %d, limit %d", this.searchData.getStart(), this.searchData.getLimit()));
                }
                if (this.searchData.isEnableCount()) {
                    count = this.totalizarConsultaSQL(sql, namedParameters);
                    if (count == 0L) {
                        return Collections.emptyList();
                    }
                    this.searchData.setTotal(count);
                }
                query.setFirstResult(this.searchData.getStart());
                query.setMaxResults(this.searchData.getLimit());
            }
        } catch (final Exception e) {
            // ignora o erro e continua a execucao
        }

        // Define os parametros na query
        this.setSQLQueryParameters(query, namedParameters);
        if (returnType != null) {
            query.setResultTransformer(new CustomResultTransformer(returnType));
        }
        return query.list();
    }

    public Object consultaSQLResultadoUnico(final String sql, final Map<String, Object> namedParameters) {
        return this.consultaSQLResultadoUnico(sql, namedParameters, null);
    }

    @SuppressWarnings("unchecked")
    public <E extends Object> E consultaSQLResultadoUnico(final String sql, final Map<String, Object> namedParameters, final Class<E> returnType) {
        final SQLQuery sqlQuery = this.getSession(true).createSQLQuery(sql);
        // Define os parametros na query
        this.setSQLQueryParameters(sqlQuery, namedParameters);
        if (returnType != null) {
            sqlQuery.setResultTransformer(new CustomResultTransformer(returnType));
        }
        final List<E> resultadoConsulta = sqlQuery.list();
        if (resultadoConsulta.size() > 0) {
            return resultadoConsulta.get(0);
        }
        return null;
    }

    public Integer executarHQL(final String hql, final Map<String, Object> params) {
        final Query query = this.getSession().createQuery(hql);
        this.setParameters(query, params);
        return query.executeUpdate();
    }

    public Integer executarSQL(final String sql, final Map<String, Object> namedParameters) {
        final SQLQuery sqlQuery = this.getSession().createSQLQuery(sql);
        this.setSQLQueryParameters(sqlQuery, namedParameters);
        return sqlQuery.executeUpdate();
    }

    /**
     * Retorna uma consulta mapeada em um arquivo de consultas (*-queries.xml)
     *
     * @param namedQuery
     * @return
     */
    public XMLQuery getNamedQueryFromFile(final String namedQuery) {
        return XMLQueryProcessor.getXMLQuery(namedQuery);
    }

    /**
     * Retorna uma consulta mapeada em um arquivo de consultas (*-queries.xml) A passagem do parameters acontece por causa do método
     * XMLQueryProcessor.creataQuery Mas aqui os parâmetros não são setados
     *
     * @param namedQuery
     * @return
     */
    public SQLQuery getNamedQueryFromFile(final String namedQuery, final Map<String, Object> parameters) {
        final XMLQuery XMLQuery = XMLQueryProcessor.getXMLQuery(namedQuery);
        final SQLQuery q = this.getSession().createSQLQuery(XMLQueryProcessor.createQuery(XMLQuery, parameters));
        return q;
    }

    /**
     * Retorna uma consulta mapeada em um arquivo de consultas (*-queries.xml) E já retorna a consulta parametrizada.
     *
     * @param namedQuery
     * @return
     */
    public SQLQuery getNamedQueryParametrizada(final String namedQuery, final Map<String, Object> parameters) {
        final SQLQuery query = this.getNamedQueryFromFile(namedQuery, parameters);
        this.setSQLQueryParameters(query, parameters);
        return query;
    }

    /**
     * Retorna um SQLQuery do hibernate
     *
     * @param query
     * @param parameters
     * @return
     */
    public SQLQuery getQuery(final XMLQuery query, final Map<String, Object> parameters) {
        return this.getSession().createSQLQuery(XMLQueryProcessor.createQuery(query, parameters));
    }

    /**
     * Consulta os registros baseado em uma HQL. <br/>
     *
     * @param hql
     *            o HQL da consulta
     * @param params
     *            os parâmetros (chave,valor) para a consulta
     *
     * @return
     */
    @SuppressWarnings("unchecked")
    public <E extends Object> E queryByHQLUniqueResult(final String hql, final Map<String, Object> params) {
        final Query query = this.getSession().createQuery(hql);
        this.setParameters(query, params);
        return (E) query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public <T extends Object> T queryByNamedNativeQueryUniqueResult(final String namedQuery, final Map<String, Object> parameters) {
        try {
            final SQLQuery query = (SQLQuery) this.getSession().getNamedQuery(namedQuery);
            return (T) query.uniqueResult();
        } catch (final MappingException me) {
            final String sql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            return (T) this.consultaSQLResultadoUnico(sql, parameters);
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends Object> T queryByNamedQueryUniqueResult(final String namedQuery, final Map<String, Object> parameters) {
        try {
            final Query query = this.getSession().getNamedQuery(namedQuery);
            return (T) query.uniqueResult();
        } catch (final MappingException me) {
            final String hql = XMLQueryProcessor.createQuery(namedQuery, parameters);
            return this.queryByHQLUniqueResult(hql, parameters);
        }
    }

    public Long totalizarConsultaSQL(final String sql, final Map<String, Object> parameters) {
        Long count = 0L;
        // fazer o count
        final String sqlCount = "SELECT COUNT(*) FROM ( " + sql + " ) as count";
        final SQLQuery sqlQuery = this.getSession().createSQLQuery(sqlCount);
        this.setSQLQueryParameters(sqlQuery, parameters);
        count = ((BigInteger) sqlQuery.uniqueResult()).longValue();
        this.searchData.setTotal(count);
        return count;
    }

    private String getQueryStringWithOrderByClause(final String queryString) {
        String queryStringWithOrder = queryString;
        try {
            if (this.searchData.isOrderDataValid()) {
                if (StringUtils.containsIgnoreCase(queryString, "order by")) {
                    queryStringWithOrder = queryStringWithOrder.substring(0, queryStringWithOrder.toLowerCase().lastIndexOf("order by"));
                    queryStringWithOrder = queryStringWithOrder.concat(String.format(ORDER_BY, new Object[] { this.searchData.getSort(),
                            this.searchData.getDir() }));
                }
            }
        } catch (final Exception e) {
            // ignora o erro e continua a execucao
        }
        if (this.logger.isDebugEnabled()) {
            // this.logger.debug("HQL/SQL: " + queryStringWithOrder);
        }
        return queryStringWithOrder;
    }

    private void paginarQuery(final Query query) {
        if ((this.searchData != null) && this.searchData.isPaginationDataValid()) {
            if (this.logger.isDebugEnabled()) {
                this.logger.debug(String.format("Query paginada com start %d, limit %d", this.searchData.getStart(), this.searchData.getLimit()));
            }
            query.setFirstResult(this.searchData.getStart());
            query.setMaxResults(this.searchData.getLimit());

        }
    }

    private void setParameters(final Query query, final Map<String, Object> params) {
        if (!CollectionUtils.isEmpty(params)) {
            final String[] paramNames = params.keySet().toArray(new String[params.size()]);
            final Object[] values = params.values().toArray();
            // final String[] paramNames, final Object[] values
            for (int i = 0; i < paramNames.length; i++) {
                if (values[i] != null) {
                    if (values[i].getClass().isArray()) {
                        query.setParameterList(paramNames[i], (Object[]) values[i]);
                    } else if (values[i] instanceof Collection) {
                        query.setParameterList(paramNames[i], (Collection<?>) values[i]);
                    } else {
                        query.setParameter(paramNames[i], values[i]);
                    }
                }
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void setSQLQueryParameters(final Query query, final Map<String, Object> params) {
        if (!CollectionUtils.isEmpty(params)) {
            // itera os parametros nomeados da query, nao do mapa de parametros
            // o Set evita repeticao de parametros
            final Set parametrosQuery = new HashSet(Arrays.asList(query.getNamedParameters()));
            for (final Iterator<String> it = params.keySet().iterator(); it.hasNext();) {
                final String nome = it.next();
                // injeta na query os parametros contidos no Map
                if (parametrosQuery.contains(nome)) {
                    final Object valor = params.get(nome);
                    if (valor instanceof Collection) {
                        // Caso o parametro passado seja uma colecao
                        query.setParameterList(nome, (Collection) valor);
                    } else if (valor.getClass().isArray()) {
                        query.setParameterList(nome, (Object[]) valor);
                    } else {
                        // Caso o parametro seja um tipo trivial
                        query.setParameter(nome, valor);
                    }
                }
            }
        }
    }

    private Long totalizarConsultaHQL(final String hql, final Map<String, Object> params) {
        Long count;
        String hqlCount = "";
        if (StringUtils.containsIgnoreCase(hql, "from")) {
            hqlCount = hql.substring(hql.toLowerCase().indexOf("from"));
        }
        if (StringUtils.containsIgnoreCase(hqlCount, "order by")) {
            hqlCount = hqlCount.substring(0, hqlCount.toLowerCase().indexOf("order by"));
        }
        if (StringUtils.containsIgnoreCase(hqlCount, "fetch")) {
            hqlCount = hqlCount.replaceAll("fetch", "");
            hqlCount = hqlCount.replaceAll("FETCH", "");
        }
        hqlCount = "select count(*) " + hqlCount;
        final Query countQuery = this.getSession().createQuery(hqlCount);
        this.setParameters(countQuery, params);
        // countQuery.
        count = (Long) countQuery.uniqueResult();
        return count;
    }

}
