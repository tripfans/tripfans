package br.com.fanaticosporviagens.infra.model.service;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.EntidadeGeradoraAcao;

public class GenericCRUDService<E extends BaseEntity<I>, I extends Serializable> extends BaseService {

    @Autowired
    protected GenericCRUDRepository<E, I> genericCRUDRepository;

    @Autowired
    protected GenericSearchRepository searchRepository;

    /**
     * Altera uma entidade
     * 
     * @param entidade
     *            A entidade a alterar
     */
    @Transactional
    public void alterar(final E entidade) {
        preAlteracao(entidade);

        // Se a entidade for do tipo EntidadeGeradoraAcao, atualizar a Data Ultima Alteracao
        if (entidade instanceof EntidadeGeradoraAcao<?>) {
            ((EntidadeGeradoraAcao<?>) entidade).setDataUltimaAlteracao(new LocalDateTime());
        }
        getRepository().alterar(entidade);
        posAlteracao(entidade);
    }

    /**
     * Consulta uma entidade na base de dados pelo <code>id</code>
     * 
     * @param id
     *            o identificador do registro a ser consultado na base
     * @return a instancia da entidade caso exista na base
     */
    public E consultarPorId(final I id) {
        return getRepository().consultarPorId(getPersistentClass(), id);
    }

    public E consultarPorUrlPath(final String urlPath) {
        return getRepository().consultarPorUrlPath(getPersistentClass(), urlPath);
    }

    /**
     * Consulta todos os registros da entidade na base de dados
     * 
     * @return uma colecao contendo todos os registros
     */
    public List<E> consultarTodos() {
        return getRepository().consultarTodos(getPersistentClass());
    }

    /**
     * Exclui uma entidade
     * 
     * @param entidade
     *            A entidade a excluir
     */
    @Transactional
    public void excluir(final E entidade) {
        preExclusao(entidade);
        // Se a entidade for do tipo EntidadeGeradoraAcao, realizar a exclusão lógica
        if (entidade instanceof EntidadeGeradoraAcao<?>) {
            ((EntidadeGeradoraAcao<?>) entidade).setDataExclusao(new Date());
            getRepository().alterar(entidade);
        } else {
            getRepository().excluir(entidade);
        }
        posExclusao(entidade);
    }

    /**
     * Inclui uma entidade
     * 
     * @param entidade
     *            A entidade a incluir
     */
    @Transactional
    public void incluir(final E entidade) {
        getRepository().incluir(entidade);
        posInclusao(entidade);
    }

    /**
     * Inclui ou Altera uma entidade
     * 
     * @param entidade
     *            A entidade a incluir/alterar
     */
    @Transactional
    public void salvar(final E entidade) {
        if (entidade.getId() == null) {
            this.incluir(entidade);
        } else {
            this.alterar(entidade);
        }
    }

    @SuppressWarnings("unchecked")
    protected Class<E> getPersistentClass() {
        try {
            final Class<E> persistentClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
            return persistentClass;
        } catch (final Exception e) {
            throw new RuntimeException("Erro ao recuperar a classe peristente: " + e.getMessage());
        }
    }

    protected GenericCRUDRepository<E, I> getRepository() {
        return this.genericCRUDRepository;
    }

    protected void posAlteracao(final E entidade) {
    }

    protected void posExclusao(final E entidade) {
    }

    protected void posInclusao(final E entidade) {
    }

    protected void preAlteracao(final E entidade) {
    }

    protected void preExclusao(final E entidade) {
    }

    protected void preInclusao(final E entidade) {
    }

    /*@SuppressWarnings({ "unchecked" })
    protected <R extends GenericCRUDRepository<E, I>> R getRepository() {
        final Class<R> repositoryClass = getRepositoryClass();
        if (repositoryClass == null) {
            return (R) new GenericCRUDRepository<E, I>();
        }
        return SpringBeansProvider.getBean(repositoryClass);
    }

    @SuppressWarnings({ "unchecked" })
    private <R extends GenericCRUDRepository<E, I>> Class<R> getRepositoryClass() {

        final Class<E> entityClass = getPersistentClass();
        final String repositoryClassName = entityClass.getName() + "Repository";
        Class<R> repositoryClass = null;
        try {
            repositoryClass = (Class<R>) Class.forName(repositoryClassName);
        } catch (final ClassNotFoundException e) {
            // Nao faz nada
        }
        return repositoryClass;
    }*/

}