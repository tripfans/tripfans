package br.com.fanaticosporviagens.infra.model.entity;

import java.io.Serializable;

public interface IEntity<ID extends Serializable> extends Serializable {

    public ID getId();

    public void setId(final ID id);
}
