package br.com.fanaticosporviagens.infra.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.util.TripFansEnviroment;

/**
 * Classe criada para solucionar o problema do envio do email de form assincrona(@Async).
 * Esta classe realiza apenas o envio do email
 *
 * @author Carlos Nascimento
 *
 */
@Service
@Profile("dev")
public class SendEmailService {

    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private TripFansEnviroment tripFansEnviroment;

    @Async
    public void sendEmail(final MimeMessagePreparator preparator) {
        // if (!this.tripFansEnviroment.isDevelAtWork()) {
        this.mailSender.send(preparator);
        // }
    }

}
