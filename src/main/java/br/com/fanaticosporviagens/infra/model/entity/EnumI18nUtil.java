package br.com.fanaticosporviagens.infra.model.entity;

import java.util.HashMap;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Classe para facilitar a utilizacao de I18N (Internationalization) dentro dos tipos Enum
 */
public class EnumI18nUtil implements EnumTypeInteger {

    private static final String DEFAULT_LOCALE = "pt_BR";

    private final Integer codigo;

    private final HashMap<String, String> descricoes = new HashMap<String, String>();

    public EnumI18nUtil(final Integer codigo, final String[] listOfLocales, final String[] descricoes) {
        if (listOfLocales.length != descricoes.length) {
            throw new IllegalArgumentException("Número de Locales diferente do número de descrições.");
        }

        this.codigo = codigo;

        for (int i = 0; i < descricoes.length; i++) {
            this.descricoes.put(listOfLocales[i], descricoes[i]);
        }
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        final String descricao = this.descricoes.get(LocaleContextHolder.getLocale().toString());

        if (descricao != null) {
            return descricao;
        }

        return this.descricoes.get(EnumI18nUtil.DEFAULT_LOCALE);
    }

}
