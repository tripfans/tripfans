package br.com.fanaticosporviagens.infra.model.service;

public enum EmailTemplate {
    EMAIL_CONFIRMACAO("EmailConfirmacao"),
    EMAIL_CONVITE("EmailConvite"),
    EMAIL_NOTIFICACAO("EmailNotificacao"),
    EMAIL_RECUPERACAO_SENHA("EmailRecuperacaoSenha");

    private final static String EXTENSAO = ".ftl";

    private final String arquivo;

    EmailTemplate(final String arquivo) {
        this.arquivo = arquivo;
    }

    public String getNomeArquivo() {
        return this.arquivo + EXTENSAO;
    }

    @Override
    public String toString() {
        return getNomeArquivo();
    }
}
