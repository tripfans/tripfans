package br.com.fanaticosporviagens.infra.model.service;

import java.util.List;

import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import br.com.fanaticosporviagens.model.entity.Usuario;

public class OpenIdUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(final String openIdIdentifier) {
        final List<Usuario> employeeList = null;
        // Employee.findEmployeesByOpenIdIdentifier(openIdIdentifier).getResultList();
        final Usuario user = new Usuario(); // employeeList.size() == 0 ? null : employeeList.get(0);

        if (user == null) {
            throw new UsernameNotFoundException("User not found for OpenID: " + openIdIdentifier);
        } else {
            if (!user.isEnabled()) {
                throw new DisabledException("User is disabled");
            }
            return user;
        }
    }

}
