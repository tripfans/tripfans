package br.com.fanaticosporviagens.infra.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Classe base para todas as entidades. Possui por padrão o atributo <tt>id</tt> e seus métodos acessores e tamb�m j� mapeia este <tt>id</tt> com os
 * valores padr�es para que n�o seja necess�rio repetir o mapeamento em cada classe filha.
 * 
 * Deve-se sobrescrever a coluna ID nas entidades em que a Primary Key n�o tenha o nome de ID na tabela anotando a entidade com a seguinte anota��o:
 * 
 * Ex:
 * 
 * @AttributeOverrides({@AttributeOverride(name = "id", column = @Column(name = "ID_PROCESSO_JUDICIAL", nullable = false, unique = true))})
 * 
 *                                              Sobrescrever tamb�m o <tt>sequenceGenerator</tt> com o nome da sequence correto
 * 
 *                                              Ex:
 * 
 * @SequenceGenerator(name = "sequenceGenerator", sequenceName = "SEQ_PROCESSO_JUDICIAL")
 * 
 * @author Carlos Nascimento
 * 
 * @param <ID>
 */
@Embeddable
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements IEntity<ID> {

    private static final long serialVersionUID = -7155322713287544274L;

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sequenceGenerator")
    protected ID id;

    // @Transient
    // private MessageSource messageSource;

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final BaseEntity other = (BaseEntity) obj;
        if (this.id == null) {
            if (other.id != null)
                return false;
        } else if (!this.id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public ID getId() {
        return this.id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public void setId(final ID id) {
        this.id = id;
    }

    /*protected String getMessage(final String code) {
        if (this.messageSource == null) {
            this.messageSource = SpringBeansProvider.getBean(MessageSource.class);
        }
        return this.messageSource.getMessage(code, null, Locale.getDefault());
    }*/

}
