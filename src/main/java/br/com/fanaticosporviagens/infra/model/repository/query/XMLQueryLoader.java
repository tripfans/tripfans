package br.com.fanaticosporviagens.infra.model.repository.query;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.MappingException;
import org.springframework.util.ResourceUtils;

import br.com.fanaticosporviagens.infra.exception.SystemException;

import com.thoughtworks.xstream.XStream;

/**
 * Classe que realiza a leitura dos arquivos XML de consultas e converte o conteudo desses arquivos em objetos <tt>XMLQuery</tt> que contem
 * SQL's/HQL's que serao utilizados para realizar consultas ao banco de dados dentro dos Repositorios (<tt>Repository</tt>)
 * 
 * @author Carlos Nascimento
 */
public class XMLQueryLoader {

    private static final String CLASS_PATH = "/WEB-INF/classes";

    private static XMLQueryLoader INSTANCE = new XMLQueryLoader();

    private static final Log LOG = LogFactory.getLog(XMLQueryLoader.class);

    private static final Map<String, Object> queriesXmlMap = new HashMap<String, Object>();

    /**
     * Metodo que faz a varredura dos diretorios dentro do 'classpath' em busca dos arquivos XML de consultas
     */
    public static Map<String, Object> createXMLQueriesMap() {
        if (LOG.isInfoEnabled()) {
            LOG.info("Carregando arquivos de consulta (*-queries.xml)");
        }
        try {
            final File dir = ResourceUtils.getFile("classpath:");
            if (LOG.isInfoEnabled()) {
                LOG.info("Varrendo diretorios em busca de arquivos (*-queries.xml): " + dir);
            }
            loadQueriesFromDir(dir);
            return queriesXmlMap;
        } catch (final Exception e) {
            e.printStackTrace();
            throw new SystemException("Erro na criacao do mapa de queries ");
        }
    }

    /**
     * Popula os objetos do tipo <tt>XMLQuery</tt> com as informacoes lidas a partir do conteudo dos XML's.
     * Adiciona estes objetos ao mapa de consultas (<tt>queriesXmlMap<tt>)
     * Caso o ambiente for de DESENV, guarda somente o nome do arquivo no mapa.
     * 
     * Se o ambiente for de desenvolvimento, guarda somente o nome do arquivo no mapa.
     * O nome do arquivo sera utilizado mais tarde para carregar a consulta em tempo de execucao 
     * permitindo a alteracao de consultas em arquivos XML sem necessidade de reiniciar o servidor para
     * a alteracao surtir efeito.
     * 
     * Se o ambiente for outro, guarda o objeto <tt>XMLQuery<tt>. 
     * Neste caso, as alteracoes nos arquivos de consulta XML
     * nao surtirao efeito ate a reinicializacao do servidor.
     * 
     * Le o XML e faz o 'parse' (conversao) do conteudo em objetos do tipo <tt>XMLQuery</tt>
     * 
     * @param arquivo
     *            arquivo XML que sera lido
     * @return uma lista com <tt>XMLQuery</tt>
     */
    private static List<XMLQuery> loadQueries(final XMLQueryCollection xmlQueryCollection, final File arquivo) {
        if (xmlQueryCollection.getQueries() != null && !xmlQueryCollection.getQueries().isEmpty()) {
            for (final XMLQuery query : xmlQueryCollection.getQueries()) {
                if (LOG.isInfoEnabled()) {
                    // LOG.info("Query '" + query.getName() + "' carregada ");
                }
                // Se o ambiente for de desenvolvimento, guarda somente o nome do arquivo no mapa.
                /*if (isAmbienteDesenvolvimento()) {
                    queriesXmlMap.put(query.getName(), arquivo.getName());
                } else {*/
                // Se o ambiente for outro, guarda o objeto XMLQuery.
                queriesXmlMap.put(query.getName(), query);
                // }
            }
            return new ArrayList<XMLQuery>(xmlQueryCollection.getQueries());
        } else {
            if (LOG.isWarnEnabled()) {
                LOG.warn("Arquivo de consultas vazio: " + arquivo);
            }
        }
        return new ArrayList<XMLQuery>();
    }

    /**
     * Le todos os arquivos (*-queries.xml) a partir de um diretorio e carrega as consultas.
     * 
     * @param dir
     *            o diretorio
     * @see loadQueriesFromDir(File, String)
     */
    private static List<XMLQuery> loadQueriesFromDir(final File dir) throws MappingException {
        return loadQueriesFromDir(dir, null);
    }

    /**
     * Le todos os arquivos (*-queries.xml) a partir de um diretorio e carrega as consultas.
     * 
     * @param dir
     *            o diretorio
     * @param fileName
     *            nome do arquivo
     * @return uma lista contendo todas as consultas lidas nos arquivos XML contidos dentro do diretorio informado
     * @throws MappingException
     */
    private static List<XMLQuery> loadQueriesFromDir(final File dir, final String fileName) throws MappingException {
        // Recupera os arquivos contidos diretorio
        final File[] files = dir.listFiles();
        final List<XMLQuery> queries = new ArrayList<XMLQuery>();
        // Varre os arquivos
        for (int i = 0; i < files.length; i++) {
            // Se o arquivo for um subdiretorio, chamar recursivamente este mesmo metodo para
            // varrer este subdiretorio em busca do arquivo XML
            if (files[i].isDirectory()) {
                queries.addAll((loadQueriesFromDir(files[i], fileName)));
            }
            // Se o nome do arquivo foi informado, carregar as consultas a partir do aquivo especifico
            if (fileName != null) {
                // Se encontrou o arquivo, carregar as consultas (XMLQuery) contidas nele
                if (fileName.equals(files[i].getName())) {
                    return loadQueriesFromFile(files[i]);
                }
            }
            // Se o nome do arquivo nao foi informado, carregar as consultas de qualquer arquivo
            // que contenha o sufixo (*-queries)
            else if (files[i].getName().endsWith("queries.xml")) {
                /*if (LOG.isInfoEnabled()) {
                	LOG.info("Arquivo de consulta encontrado: " + files[i].getName());
                }*/
                queries.addAll(loadQueriesFromFile(files[i]));
            }
        }
        // Retorna a lita de queries (consultas)
        return queries;
    }

    /**
     * Le o XML e faz o 'parse' (conversao) do conteudo em objetos do tipo <tt>XMLQuery</tt>
     * 
     * @param arquivo
     *            arquivo XML que sera lido
     * @return uma lista com <tt>XMLQuery</tt>
     * @see loadQuery(XMLQueryCollection, File)
     */
    private static List<XMLQuery> loadQueriesFromFile(final File xmlFile) {
        final XStream xstream = new XStream();
        xstream.alias("named-queries", XMLQueryCollection.class);
        xstream.alias("named-query", XMLQuery.class);

        xstream.addImplicitCollection(XMLQueryCollection.class, "queries", XMLQuery.class);

        final List<XMLQuery> queries = new ArrayList<XMLQuery>();

        // Finalmente processa o XML e converte em JavaBean
        final XMLQueryCollection queryCollection = (XMLQueryCollection) xstream.fromXML(xmlFile);

        queries.addAll(loadQueries(queryCollection, xmlFile));
        return queries;
    }

    private ServletContextEvent servletContextEvent;

    private XMLQueryLoader() {
    }
}
