package br.com.fanaticosporviagens.infra.model.repository;

public class SortField {
    private final SortDirection direction;

    private final String fieldName;

    public SortField(final String fieldName, final SortDirection direction) {
        this.fieldName = fieldName;
        this.direction = direction;
    }

    public SortDirection getDirection() {
        return this.direction;
    }

    public String getFieldName() {
        return this.fieldName;
    }

}