package br.com.fanaticosporviagens.infra.model.repository;

import org.apache.solr.common.params.FacetParams;

public class FacetField {

    public static final String SORT_TYPE_COUNT = FacetParams.FACET_SORT_COUNT;

    public static final String SORT_TYPE_INDEX = FacetParams.FACET_SORT_INDEX;

    private Integer minCount;

    private String name;

    private String sortType;

    public FacetField(final String name) {
        this.name = name;
    }

    public FacetField(final String name, final Integer minCount, final String sortType) {
        this.name = name;
        this.minCount = minCount;
        this.sortType = sortType;
    }

    public Integer getMinCount() {
        return this.minCount;
    }

    public String getName() {
        return this.name;
    }

    public String getSortType() {
        return this.sortType;
    }

    public void setMinCount(final Integer minCount) {
        this.minCount = minCount;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setSortType(final String sortType) {
        this.sortType = sortType;
    }

}
