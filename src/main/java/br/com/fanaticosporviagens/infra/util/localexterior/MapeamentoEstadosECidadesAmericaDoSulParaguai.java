package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulParaguai implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar paraguai = MapeamentoPais.AMERICA_DO_SUL_PARAGUAI.getPais();
        paraguai.comCapital("Assunção");
        paraguai.comCidades("Abaí", "Acahay", "Aguaray", "Alberdi", "Alto Verá", "Altos", "Antequera", "Areguá", "Arroyos y Esteros", "Assunção",
                "Atyrá", "Ayolas", "Bahia Negra", "Belén", "Bella Vista", "Bella Vista", "Benjamín Aceval", "Borja", "Buena Vista", "Caacupé",
                "Caaguazú", "Caapucú", "Caazapá", "Cambyretá", "Capiatá", "Capiibary", "Capitán Bado", "Capitán Mauricio José Troche",
                "Capitán Meza", "Capitán Miranda", "Capitan Pablo Lageranza", "Caraguatay", "Carapeguá", "Carayaó", "Carlos Antonio López",
                "Carmen del Paraná", "Cerrito", "Choré", "Ciudad del Este", "Colonia Fernheim", "Concepción", "Coronel Bogado", "Coronel Martínez",
                "Coronel Oviedo", "Corpus Christi", "Curuguaty", "Desmochados", "Doctor Botrell", "Doctor Cecilio Báez",
                "Doctor J. Eulogio Estigarribia", "Doctor Juan León Mallorquín", "Doctor Juan Manuel Frutos", "Doctor Moisés S. Bertoni",
                "Doctor Pedro P. Peña", "Domingo Martínez de Irala", "Edelira", "Emboscada", "Encarnación", "Escobar", "Eusebio Ayala",
                "Félix Pérez Cardozo", "Fernando de la Mora", "Filadelfia", "Fram", "Fuerte Olimpo", "General Artigas",
                "General Bernardino Caballero", "General Delgado", "General Elizardo Aquino", "General Eugenio A. Garay", "General Eugenio A. Garay",
                "General Francisco Caballero Álvarez", "General Higinio Morínigo", "General Isidoro Resquín", "General José Eduvigis Díaz",
                "Guarambaré", "Guayaibí", "Guazú Cuá", "Hernandarias", "Hohenau", "Horqueta", "Humaitá", "Independencia", "Iruña", "Isla Margarita",
                "Isla Pucú", "Isla Umbú", "Itá", "Itacurubí de la Cordillera", "Itacurubí del Rosario", "Itakyry", "Itanará", "Itapé", "Itapúa Poty",
                "Itauguá", "Iturbe", "Jesús", "Jose A. Fassardi", "José Domingo Ocampos", "José Leandro Oviedo", "Juan Augusto Saldívar",
                "Juan de Mena", "Juan Emilio O'Leary", "Katueté", "La Colmena", "La Paloma", "La Pastora", "La Paz", "La Victoria", "Lambaré",
                "Laureles", "Lima", "Limpio", "Loma Grande", "Loma Plata", "Loreto", "Los Cedrales", "Luque", "Maciel", "Mariano Roque Alonso",
                "Mariscal Francisco Solano López", "Mariscal José Félix Estigarribia", "Mayor José Martinez", "Mayor Julio D. Otaño", "Mbaracayú",
                "Mbocayaty del Guairá", "Mbocayaty del Yhaguy", "Mbuyapey", "Minga Guazú", "Minga Porá", "Ñacunday", "Nanawa", "Naranjal",
                "Natalicio Talavera", "Natalio", "Ñemby", "Neuland", "Nueva Alborada", "Nueva Colombia", "Nueva Esperanza", "Nueva Germania",
                "Nueva Italia", "Nueva Londres", "Ñumí", "Obligado", "Paraguarí", "Paso de Patria", "Paso Yovai", "Pedro Juan Caballero", "Pilar",
                "Pirapó", "Pirayú", "Piribebuy", "Pozo Colorado", "Presidente Franco", "Primero de Marzo", "Puerto Casado", "Puerto Esperanza",
                "Puerto Falcon", "Puerto Guarani", "Puerto Pinasco", "Quiindy", "Quyquyhó", "R. I. Tres Corrales", "Raúl Arsenio Oviedo",
                "Repatriación", "Salto del Guairá", "San Alberto", "San Antonio", "San Bernardino", "San Carlos", "San Cosme y Damián",
                "San Cristóbal", "San Estanislao", "San Ignacio", "San Joaquín", "San José de los Arroyos", "San José Obrero", "San Juan Bautista",
                "San Juan Bautista del Ñeembucú", "San Juan del Paraná", "San Juan Nepomuceno", "San Lázaro", "San Lorenzo", "San Miguel",
                "San Pablo", "San Patricio", "San Pedro", "San Pedro del Paraná", "San Rafael del Paraná", "San Roque González de Santa Cruz",
                "San Salvador", "Santa Elena", "Santa María", "Santa Rita", "Santa Rosa", "Santa Rosa del Mbutuy", "Santa Rosa del Monday",
                "Santiago", "Sapucai", "Simón Bolívar", "Tacuaras", "Tacuatí", "Tavaí", "Tebicuarymí", "Teniente Irala Fernandez", "Tobatí",
                "Tomás Romero Pereira", "Tres de Febrero", "Trinidad", "Unión", "Valenzuela", "Vaquería", "Veinticinco de Diciembre",
                "Villa del Rosario", "Villa Elisa", "Villa Florida", "Villa Franca", "Villa Hayes", "Villa Oliva", "Villa Ygatimí", "Villalbín",
                "Villarrica", "Villeta", "Yabebyry", "Yaguarón", "Yataity del Guairá", "Yataity del Norte", "Yatytay", "Yby Yaú", "Ybycuí",
                "Ybytymí", "Fulgencio Yegros", "Yguazú", "Yhú", "Ypacaraí", "Ypané", "Ypejhú", "Yrybucua", "Yuty");
    }

}
