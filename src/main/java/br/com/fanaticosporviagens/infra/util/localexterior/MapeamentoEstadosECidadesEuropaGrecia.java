package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaGrecia implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar grecia = MapeamentoPais.EUROPA_GRECIA.getPais();
        grecia.comCapital("Atenas");
        grecia.comCidades("Atenas", "Tessalônica", "Pireu", "Patras", "Peristeri", "Heraclião", "Lárissa", "Kallithea", "Nicaia", "Calamária",
                "Glyfáda", "Vólos", "Acharnes", "Ilio", "Keratsini", "Iliópolis", "Nea Smirni", "Zografou", "Chalandri", "Aigaleo", "Marousi",
                "Korydallos", "Agios Dimitrios", "Nea Ionia", "Palaio Faliro", "Janina", "Evosmos", "Vironas", "Agia Paraskevi", "Kavala", "Galatsi",
                "Rhodes", "Serres", "Chania", "Cálcis", "Katerini", "Alexandrópolis", "Petrópolis", "Kalamata", "Trikala", "Komotini", "Lamia",
                "Chaidari", "Neo Irakleio", "Xanti", "Agrinio", "Kifisia", "Véria", "Drama", "Sykies", "Estavrópolis", "Ampelokipoi", "Alimos",
                "Polichni", "Kozani", "Corfu", "Argirópolis", "Agioi Anargyroi", "Karditsa", "Colargo", "Nea Ionia", "Agia Varvara", "Nápoles");

    }

}
