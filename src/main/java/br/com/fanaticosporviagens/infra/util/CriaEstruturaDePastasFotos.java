package br.com.fanaticosporviagens.infra.util;

import java.io.File;

import br.com.fanaticosporviagens.model.entity.Foto;

public class CriaEstruturaDePastasFotos {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        System.out.println("Vai criar a estrutura de diretórios ...");
        final String base = System.getProperty("user.home") + File.separator + "fanaticos" + File.separator + "fotos" + File.separator;
        final File mainDir = new File(base);
        if (!mainDir.exists()) {
            System.out.println("Criando o diretório " + mainDir);
            mainDir.mkdirs();
        }
        // cria o diretório avaliações
        final File avaliacaoDir = new File(base + File.separator + Foto.AVALIACAO_PHOTO_DIR);
        if (!avaliacaoDir.exists()) {
            System.out.println("Criando o diretório " + avaliacaoDir);
            avaliacaoDir.mkdirs();
        }
        // cria o diretório de locais
        final File localDir = new File(base + File.separator + Foto.LOCAL_PHOTO_DIR);
        if (!localDir.exists()) {
            System.out.println("Criando o diretório " + localDir);
            localDir.mkdirs();
        }
        // cria o diretório de usuários
        final File usuarioDir = new File(base + File.separator + Foto.USUARIO_PHOTO_DIR);
        if (!usuarioDir.exists()) {
            System.out.println("Criando o diretório " + usuarioDir);
            usuarioDir.mkdirs();
        }
        // cria o diretório de álbuns
        final File albunsDir = new File(base + File.separator + Foto.ALBUM_PHOTO_DIR);
        if (!albunsDir.exists()) {
            System.out.println("Criando o diretório " + albunsDir);
            albunsDir.mkdirs();
        }
        // cria o diretório de viagens
        final File viagemDir = new File(base + File.separator + Foto.VIAGEM_PHOTO_DIR);
        if (!viagemDir.exists()) {
            System.out.println("Criando o diretório " + viagemDir);
            viagemDir.mkdirs();
        }
        System.out.println("Pronto.");
    }

}
