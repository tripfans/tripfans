package br.com.fanaticosporviagens.infra.util.localexterior;

import java.sql.SQLException;

public class GeradorScriptParaInsercaoLocaisExteriorPlsql {
    public static void main(final String[] args) throws SQLException, ClassNotFoundException {
        final GeradorScriptParaInsercaoLocaisExteriorPlsql gerador = new GeradorScriptParaInsercaoLocaisExteriorPlsql();
        gerador.popularEstadosECidades();
    }

    private void gerarPlsqlImportacaoPais(final Lugar pais) {
        System.out.println("\t -- " + pais.getId() + " - " + pais.getNome());
        final String functionName = "carga_" + pais.getUrlPath().replaceAll("-", "_") + "()";
        System.out.println("CREATE OR REPLACE FUNCTION " + functionName);
        System.out.println("  RETURNS bigint AS ");
        System.out.println("$BODY$ ");
        System.out.println("DECLARE ");
        System.out.println("    v_id_estado local.id_local%type; ");
        System.out.println("    v_id_cidade local.id_local%type; ");
        System.out.println("    v_qtd_locais bigint := 0; ");
        System.out.println("BEGIN ");

        for (final Lugar estado : pais.getSublocais()) {

            if (estado.getLocalType().isTipoEstado()) {
                System.out.println("    v_id_estado := cad_local_estado(" + estado.getPai().getId() + ", '" + estado.getNome().replaceAll("'", "''")
                        + "', '" + estado.getSigla() + "');");
            } else if (estado.getLocalType().isTipoCidade()) {
                System.out.println("    v_id_cidade := cad_local_cidade(" + estado.getPai().getId() + ", '" + estado.getNome().replaceAll("'", "''")
                        + "', " + estado.isCapitalPais() + ", " + estado.isCapitalEstado() + ");");
            }

            for (final Lugar cidade : estado.getSublocais()) {
                System.out.println("    \tv_id_cidade := cad_local_cidade(v_id_estado, '" + cidade.getNome().replaceAll("'", "''") + "', "
                        + cidade.isCapitalPais() + ", " + cidade.isCapitalEstado() + ");");
            }
        }

        System.out.println("    RETURN v_qtd_locais;");
        System.out.println("END; ");
        System.out.println("$BODY$ ");
        System.out.println("  LANGUAGE plpgsql VOLATILE ");
        System.out.println("  COST 100; ");
        System.out.println("ALTER FUNCTION " + functionName + " OWNER TO fpv;");
        System.out.println("SELECT " + functionName + ";");
    }

    public void popularEstadosECidades() throws SQLException, ClassNotFoundException {
        final MapeamentoEstadosClassesExterior mapeamento = new MapeamentoEstadosClassesExterior();

        for (final Lugar continente : mapeamento.createContinentes()) {
            System.out.println(continente.getId() + " - " + continente.getNome());
            for (final Lugar pais : continente.getSublocais()) {
                if (pais.isCriarScript()) {
                    gerarPlsqlImportacaoPais(pais);
                }
            }
        }
    }

}
