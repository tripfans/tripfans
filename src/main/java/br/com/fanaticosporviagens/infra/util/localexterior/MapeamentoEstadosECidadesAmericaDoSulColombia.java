package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulColombia implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar colombia = MapeamentoPais.AMERICA_DO_SUL_COLOMBIA.getPais();
        colombia.comCapital("Bogotá");
        colombia.comCidades("Bogotá", "Medellín", "Cáli", "Barranquilha", "Cartagena das Índias", "Cúcuta", "Bucaramanga", "Soledad", "Pereira",
                "Ibagué", "Santa Marta", "Soacha", "Pasto", "Montería", "Villavicencio", "Manizales", "Bello", "Neiva", "Valledupar", "Armenia",
                "Buenaventura", "Palmira", "Popayán", "Sincelejo", "Floridablanca", "Itagüí", "Ocaña", "Quibdó", "Barrancabermeja", "Tuluá",
                "Dosquebradas", "Envigado", "Villa del Rosario", "Los Patios", "Tumaco", "Florencia", "Girardot", "Girón", "Apartadó", "Sabanalarga",
                "Cartago", "Maicao", "Turbo", "Magangué", "Tunja", "Uribia", "Piedecuesta", "Sogamoso", "Buga", "Lorica", "Ipiales", "Fusagasugá",
                "Facatativa", "Duitama", "Pitalito", "Pamplona", "Ciénaga", "Zipaquirá", "Malambo", "Rionegro");
    }

}
