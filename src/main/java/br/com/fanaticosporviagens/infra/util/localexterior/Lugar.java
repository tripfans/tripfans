package br.com.fanaticosporviagens.infra.util.localexterior;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import br.com.fanaticosporviagens.model.entity.LocalType;

public class Lugar implements Comparable<Lugar> {

    public static Lugar criarContinente(final Long id, final String nome, final String sigla) {
        final Lugar continente = new Lugar();
        continente.setLocalType(LocalType.CONTINENTE);
        continente.setNome(nome);
        continente.setSigla(sigla);
        continente.setId(id);

        return continente;
    }

    public static Lugar criarEstado(final Lugar pais, final String nome, final String sigla) {
        final Lugar estado = new Lugar();
        estado.setLocalType(LocalType.ESTADO);
        estado.setNome(nome);
        estado.setSigla(sigla);
        pais.addSublocal(estado);

        return estado;
    }

    public static Lugar criarPais(final Lugar continente, final Long id, final String nome, final String sigla, final boolean criarScript) {
        final Lugar pais = new Lugar();
        pais.setLocalType(LocalType.PAIS);
        pais.setId(id);
        pais.setNome(nome);
        pais.setSigla(sigla);
        pais.setCriarScript(criarScript);
        continente.addSublocal(pais);

        return pais;
    }

    private boolean capitalEstado;

    private boolean capitalPais;

    private boolean criarScript;

    private Long id;

    private LocalType localType;

    private String nome;

    private String nomeCapital;

    private Lugar pai;

    private String sigla;

    private final Set<Lugar> sublocais = new TreeSet<Lugar>();

    public Lugar addSublocal(final Lugar local) {
        local.setPai(this);
        this.sublocais.add(local);
        return this;
    }

    public Lugar comCapital(final String nome) {
        setNomeCapital(nome);
        definirCapital();
        return this;
    }

    public Lugar comCidades(final String... nomes) {
        for (final String nome : nomes) {
            final Lugar cidade = new Lugar();
            cidade.setLocalType(LocalType.CIDADE);
            cidade.setNome(nome);
            addSublocal(cidade);
        }
        definirCapital();
        return this;
    }

    @Override
    public int compareTo(final Lugar other) {
        return this.nome.compareTo(other.nome);
    }

    private void definirCapital() {
        if (this.nomeCapital != null) {
            if (this.localType.isTipoPais()) {
                for (final Lugar lugar : this.getLugares(LocalType.CIDADE)) {
                    lugar.setCapitalPais(lugar.getNome().equals(this.nomeCapital));
                }
            } else if (this.localType.isTipoEstado()) {
                for (final Lugar lugar : this.getLugares(LocalType.CIDADE)) {
                    lugar.setCapitalEstado(lugar.getNome().equals(this.nomeCapital));
                }
            }
        }
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Lugar other = (Lugar) obj;
        if (this.localType != other.localType)
            return false;
        if (this.nome == null) {
            if (other.nome != null)
                return false;
        } else if (!this.nome.equals(other.nome))
            return false;
        if (this.pai == null) {
            if (other.pai != null)
                return false;
        } else if (!this.pai.equals(other.pai))
            return false;
        return true;
    }

    public String gerarDMLEstado(final Lugar continente, final Lugar pais) {
        final String script = "INSERT LOCAL (local_type, id_local,nome,sigla,id_continete, id_pais) VALUES (" + getNome() + ");";

        return script;
    }

    public Long getId() {
        return this.id;
    }

    public LocalType getLocalType() {
        return this.localType;
    }

    public Lugar getLugar(final LocalType tipo) {
        if (this.localType.equals(tipo)) {
            return this;
        }
        if (this.pai == null) {
            return null;
        }
        if (this.pai.getLocalType().equals(tipo)) {
            return this.pai;
        }
        return this.pai.getLugar(tipo);
    }

    private Set<Lugar> getLugares(final LocalType tipo) {
        final HashSet<Lugar> lugares = new HashSet<Lugar>();

        if (this.getLocalType().equals(tipo)) {
            lugares.add(this);
        }

        for (final Lugar lugar : this.sublocais) {
            if (lugar.getLocalType().equals(tipo)) {
                lugares.addAll(lugar.getLugares(tipo));
            }
        }

        return lugares;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeLimpo() {
        final String nomeLimpo = Normalizer.normalize(this.nome, Normalizer.Form.NFD).toLowerCase();

        return nomeLimpo.replaceAll("[^\\p{ASCII}]", "").replaceAll("[^0-9|^a-z|\\s]", "").replaceAll("\\s+", " ").trim().replaceAll("\\s", "-");
    }

    public Lugar getPai() {
        return this.pai;
    }

    public String getSigla() {
        return this.sigla;
    }

    public Set<Lugar> getSublocais() {
        return this.sublocais;
    }

    private List<Lugar> getTodosPais() {
        final List<Lugar> todosPais = new ArrayList<Lugar>();

        Lugar lugar = this;
        while (lugar.getPai() != null) {
            lugar = lugar.getPai();
            todosPais.add(lugar);
        }
        Collections.reverse(todosPais);

        return todosPais;
    }

    public String getUrlPath() {
        final StringBuilder url = new StringBuilder();

        url.append(getUrlPathPrefixo());

        final String urlPathSufixoPai = getUrlPathSufixoPai();
        if ((urlPathSufixoPai != null) && (!urlPathSufixoPai.trim().isEmpty())) {
            url.append(urlPathSufixoPai);
        }

        url.append("_" + this.getNomeLimpo());

        return url.toString();
    }

    private String getUrlPathPrefixo() {
        return this.localType.getUrlPath();
    }

    private String getUrlPathSufixoPai() {
        final StringBuilder sufixo = new StringBuilder();

        for (final Lugar lugar : getTodosPais()) {
            if ((lugar.getSigla() != null) && (!lugar.getSigla().trim().isEmpty())) {
                sufixo.append("_" + lugar.getSigla().toLowerCase());
            } else {
                sufixo.append("_" + lugar.getNomeLimpo().toLowerCase());
            }
        }

        return sufixo.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.localType == null) ? 0 : this.localType.hashCode());
        result = prime * result + ((this.nome == null) ? 0 : this.nome.hashCode());
        result = prime * result + ((this.pai == null) ? 0 : this.pai.hashCode());
        return result;
    }

    public boolean isCapitalEstado() {
        return this.capitalEstado;
    }

    public boolean isCapitalPais() {
        return this.capitalPais;
    }

    public boolean isCriarScript() {
        return this.criarScript;
    }

    private void setCapitalEstado(final boolean capital) {
        this.capitalEstado = capital;
    }

    private void setCapitalPais(final boolean capitalPais) {
        this.capitalPais = capitalPais;
    }

    private void setCriarScript(final boolean criarScript) {
        this.criarScript = criarScript;

    }

    public void setId(final Long id) {
        this.id = id;
    }

    private void setLocalType(final LocalType localType) {
        this.localType = localType;
    }

    private void setNome(final String nome) {
        this.nome = nome;
    }

    private void setNomeCapital(final String nomeCapital) {
        this.nomeCapital = nomeCapital;
    }

    private void setPai(final Lugar pai) {
        this.pai = pai;
    }

    private void setSigla(final String sigla) {
        this.sigla = sigla;
    }

}