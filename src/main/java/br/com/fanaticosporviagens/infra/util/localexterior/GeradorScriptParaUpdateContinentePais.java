package br.com.fanaticosporviagens.infra.util.localexterior;

import java.sql.SQLException;

public class GeradorScriptParaUpdateContinentePais {
    public static void main(final String[] args) throws SQLException, ClassNotFoundException {
        for (final MapeamentoPais pais : MapeamentoPais.values()) {
            System.out.println("update local set d_continente_id = " + pais.getPais().getPai().getId() + " where d_pais_id = "
                    + pais.getPais().getId() + ";");
        }
    }

}
