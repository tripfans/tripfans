package br.com.fanaticosporviagens.infra.util;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * Responsável por gerar URLs únicas para as entidades.
 * 
 * @author André Thiago
 * 
 */
@Service
public class UrlPathGeneratorService {

    /**
     * Gera um nome único para o objeto informado.
     * 
     * @param object
     *            para o qual o nome único será gerado
     */
    public void generate(final UrlNameable object) {
        final String prefix = object.getPrefix();
        object.setUrlPath(prefix + RandomStringUtils.random(24, false, true));
    }

}
