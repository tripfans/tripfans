package br.com.fanaticosporviagens.infra.util;

/**
 * Contém constantes ligadas à variáveis de ambientes usadas na aplicação.
 * 
 */
public interface AppEnviromentConstants {

    public static final String AMQP_HOST = "rabbit.host";

    public static final String AMQP_MAIL_QUEUE = "rabbit.queue";

    public static final String AMQP_PORT = "rabbit.port";

    public static final String APPLICATION_CONTEXT = "application.context";

    public static final String APPLICATION_DOMAIN = "application.domain";

    public static final String APPLICATION_IMAGES_PATH = "application.imagesPath";

    public static final String APPLICATION_IMAGES_ROOT_DIRECTORY = "application.imagesRootDirectory";

    public static final String APPLICATION_IS_DEVEL = "application.isDevel";

    public static final String APPLICATION_IS_DEVEL_AT_WORK = "application.isDevelAtWork";

    public static final String APPLICATION_IS_ENABLED_PEDIDO_DICA = "application.enabledPedidoDica";

    public static final String APPLICATION_IS_ENABLED_SOCIAL_NETWORK_ACTIONS = "application.enableSocialNetworkActions";

    public static final String APPLICATION_PROTOCOL = "application.protocol";

    public static final String BITLY_ACCESS_TOKEN = "bitly.accessToken";

    public static final String BITLY_SHORTEN_URL = "bitly.shortenUrl";

    public static final String FACEBOOK_CLIENT_ID = "facebook.clientId";

    public static final String FACEBOOK_CLIENT_SECRET = "facebook.clientSecret";

    public static final String FOURSQUARE_CLIENT_ID = "foursquare.clientId";

    public static final String FOURSQUARE_CLIENT_SECRET = "foursquare.clientSecret";

    public static final String GOOGLE_CLIENT_ID = "google.clientId";

    public static final String GOOGLE_CLIENT_SECRET = "google.clientSecret";

    public static final String IMAGES_FULL_DIRECTORY = "imagesFullDirectory";

    public static final String IMAGES_SERVER_URL = "imagesServerUrl";

    public static final String MAIL_HOST = "mail.host";

    public static final String SERVER_URL = "app.serverUrl";

    public static final String SOLR_ALLOW_COMPRESSION = "solr.allowCompression";

    public static final String SOLR_SERVER_URL = "solr.serverUrl";

    public static final String TWITTER_CONSUMER_KEY = "twitter.consumerKey";

    public static final String TWITTER_CONSUMER_SECRET = "twitter.clientSecret";

}
