package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaHolanda implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar holanda = MapeamentoPais.EUROPA_HOLANDA.getPais();
        holanda.comCapital("Amsterdã");

        Lugar.criarEstado(holanda, "Drente", null).comCidades("Aa en Hunze", "Assen", "Borger-Odoorn", "Coevorden", "De Wolden", "Emmen",
                "Hoogeveen", "Meppel", "Midden-Drenthe", "Noordenveld", "Tynaarlo", "Westerveld");

        Lugar.criarEstado(holanda, "Flevolândia", null).comCidades("Almere", "Dronten", "Lelystad", "Noordoostpolder", "Urk", "Zeewolde");

        Lugar.criarEstado(holanda, "Frísia", null).comCidades("Drachten", "Harlingen", "Heerenveen", "Leeuwarden", "Sneek");

        Lugar.criarEstado(holanda, "Guéldria", null).comCidades("Apeldoorn", "Arnhem", "Buren", "Bredevoort", "Culemborg", "Doetinchem", "Ede",
                "Groenlo", "Harderwijk", "Hattem", "Huissen", "Nijkerk", "Nimegue", "Tiel", "Wageningen", "Winterswijk", "Zutphen");

        Lugar.criarEstado(holanda, "Groninga", null).comCidades("Appingedam", "Bedum", "Bellingwedde", "De Marne", "Delfzijl", "Eemsmond",
                "Groninga", "Grootegast", "Haren", "Hoogezand-Sappemeer", "Leek", "Loppersum", "Marum", "Menterwolde", "Pekela", "Reiderland",
                "Scheemda", "Slochteren", "Stadskanaal", "Ten Boer", "Veendam", "Vlagtwedde", "Winschoten", "Winsum", "Zuidhorn");

        Lugar.criarEstado(holanda, "Limburgo", null).comCidades("Geleen", "Heerlen", "Kerkrade", "Maastricht", "Roermond", "Sittard",
                "Valkenburg aan de Geul", "Venlo", "Weert");

        Lugar.criarEstado(holanda, "Brabante do Norte", null).comCidades("Aalburg", "Alphen-Chaam", "Asten", "Baarle-Nassau", "Bergeijk",
                "Bergen op Zoom", "Bernheze", "Best (Brabante do Norte)", "Bladel", "Boekel", "Boxmeer", "Boxtel", "Breda", "Cranendonck", "Cuijk",
                "Deurne", "Dongen", "Drimmelen", "Eersel", "Eindhoven", "Etten-Leur", "Geertruidenberg", "Geldrop-Mierlo", "Gemert-Bakel",
                "Gilze en Rijen", "Goirle", "Grave", "Haaren", "Halderberge", "Heeze-Leende", "Helmond", "'s-Hertogenbosch", "Heusden",
                "Hilvarenbeek", "Laarbeek", "Landerd", "Lith", "Loon op Zand", "Maasdonk", "Mill en Sint Hubert", "Moerdijk",
                "Nuenen, Gerwen en Nederwetten", "Oirschot", "Oisterwijk", "Oosterhout", "Oss", "Reusel-De Mierden", "Roosendaal", "Rucphen",
                "Schijndel", "Sint Anthonis", "Sint-Michielsgestel", "Sint-Oedenrode", "Someren", "Son en Breugel", "Steenbergen", "Tilburg", "Uden",
                "Valkenswaard", "Veghel", "Veldhoven", "Vught", "Waalre", "Waalwijk", "Werkendam", "Woensdrecht", "Woudrichem", "Zundert");

        Lugar.criarEstado(holanda, "Holanda do Norte", null).comCidades("Alkmaar", "Amstelveen", "Amsterdã", "Den Helder", "Edam", "Enkhuizen",
                "Haarlem", "Hilversum", "Hoofddorp", "Hoorn");

        Lugar.criarEstado(holanda, "Transisalania", null).comCidades("Almelo", "Deventer", "Enschede", "Hengelo", "Oldenzaal", "Zwolle");

        Lugar.criarEstado(holanda, "Holanda do Sul", null).comCidades("Zoetermeer", "Delft", "Dordrecht", "Gouda", "Leida", "Roterdã", "Haia",
                "Alphen aan den Rijn");

        Lugar.criarEstado(holanda, "Utrecht", null).comCidades("Abcoude", "Amerongen", "Amersfoort", "Baarn", "Breukelen", "Bunnik", "Bunschoten",
                "De Bilt", "De Ronde Venen", "Doorn", "Driebergen-Rijsenburg", "Eemnes", "Houten", "IJsselstein", "Leersum", "Leusden", "Loenen",
                "Lopik", "Maarn", "Maarssen", "Montfoort", "Nieuwegein", "Oudewater", "Renswoude", "Rhenen", "Soest", "Utrecht", "Veenendaal",
                "Vianen", "Wijk bij Duurstede", "Woerden", "Woudenberg", "Zeist");

        Lugar.criarEstado(holanda, "Zelândia", null).comCidades("Borsele", "Goes", "Hulst", "Kapelle", "Middelburg", "Noord-Beveland", "Reimerswaal",
                "Schouwen-Duiveland", "Sluis", "Terneuzen", "Tholen", "Veere", "Vlissingen");

    }

}
