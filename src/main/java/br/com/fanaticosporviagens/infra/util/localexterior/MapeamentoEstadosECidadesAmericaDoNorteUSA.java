package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoNorteUSA implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {
        final Lugar usa = MapeamentoPais.AMERICA_DO_NORTE_ESTADOS_UNIDOS_DA_AMERICA.getPais();

        usa.comCapital("Washington, DC");

        Lugar.criarEstado(usa, "Alabama", "AL")
                .comCapital("Montgomery")
                .comCidades("Anniston", "Auburn", "Bessemer", "Birmingham", "Decatur", "Demopolis", "Dothan", "Eufaula", "Florence", "Gadsden",
                        "Gulf Shores", "Hoover", "Huntsville", "Mobile", "Montgomery", "Phenix City", "Selma", "Troy", "Tuscaloosa", "Tuskegee");

        Lugar.criarEstado(usa, "Alaska (Alasca)", "AK").comCapital("Juneau")
                .comCidades("Anchorage", "Barrow", "Fairbanks", "Homer", "Juneau", "Ketchikan", "Nome", "Seward", "Sitka", "Valdez");

        Lugar.criarEstado(usa, "Arizona", "AZ")
                .comCapital("Phoenix")
                .comCidades("Chandler", "Flagstaff", "Gilbert", "Glendale", "Marana", "Mesa", "Nogales", "Oro Valley", "Peoria", "Phoenix",
                        "Prescott", "Sahuarita", "Scottsdale", "Sierra Vista", "Tempe", "Tucson", "Yuma");

        Lugar.criarEstado(usa, "Arkansas", "AR")
                .comCapital("Little Rock")
                .comCidades("Bentonville", "Fayetteville", "Fort Smith", "Hope", "Hot Springs", "Jonesboro", "Little Rock", "North Little Rock",
                        "Pine Bluff", "Springdale", "Texarkana", "West Memphis");

        Lugar.criarEstado(usa, "Califórnia", "CA")
                .comCapital("Sacramento")
                .comCidades("Alameda", "Alhambra", "Anaheim", "Apple Valley", "Bakersfield", "Berkeley", "Beverly Hills", "Cerritos", "Chico",
                        "Chino", "Chino Hills", "Citrus Heights", "Corona", "Cypress", "Daly City", "Diamond Bar", "Downey", "Escondido", "Eureka",
                        "Fairfield", "Fremont", "Fresno", "Fullerton", "Garden Grove", "Glendale", "Half Moon Bay", "Huntington Beach",
                        "Huntington Park", "Irvine", "Lakewood", "Lancaster", "Lodi", "Long Beach", "Los Angeles", "Malibu", "Merced", "Modesto",
                        "Monterey", "Moreno Valley", "Moraga", "Newport Beach", "Norwalk", "Oakland", "Ontário", "Orange", "Oxnard", "Palmdale",
                        "Palm Springs", "Palo Alto", "Paramount", "Pasadena", "Pomona", "Rancho Cordova", "Rancho Cucamonga", "Redding", "Redlands",
                        "Rialto", "Riverside", "Sacramento", "Salinas", "San Bernardino", "San Diego", "San Francisco", "San José",
                        "San Juan Capistrano", "San Luis Obispo", "San Mateo", "San Rafael", "Santa Ana", "Santa Bárbara", "Santa Cruz",
                        "Santa Mônica", "Simi Valley", "Stockton", "Thousand Oaks", "Tustin", "Sonoma", "Vallejo", "Visalia", "West Covina",
                        "Whittier");

        Lugar.criarEstado(usa, "Colorado", "CO")
                .comCapital("Denver")
                .comCidades("Acres Green", "Aspen", "Aurora", "Boulder", "Centennial", "Colorado Springs", "Denver", "Durango", "Fort Collins",
                        "Grand Junction", "Lakewood", "Littleton", "Pueblo", "Vail");

        Lugar.criarEstado(usa, "Connecticut", "CT")
                .comCapital("Hartford")
                .comCidades("Bridgeport", "Greenwich", "Hartford", "Middletown", "New Britain", "New Haven", "New London", "Norwich", "Stamford",
                        "Storrs");

        Lugar.criarEstado(usa, "Delaware", "DE").comCapital("Dover")
                .comCidades("Dover", "Elsmere", "Georgetown", "Milford", "Newark", "New Castle", "Rehoboth Beach", "Seaford", "Wilmington");

        Lugar.criarEstado(usa, "Flórida", "FL")
                .comCapital("Tallahassee")
                .comCidades("Boca Raton", "Bradenton", "Brandon", "Cape Coral", "Clearwater", "Coral Gables", "Coral Springs", "Daytona Beach",
                        "Delray Beach", "Deltona", "Fort Lauderdale", "Fort Myers", "Gainesville", "Hialeah", "Hollywood", "Jacksonville", "Kendall",
                        "Key West", "Kissimmee", "Lakeland", "Melbourne", "Miami", "Miami Beach", "Miami Gardens", "North Miami", "Ocala", "Orlando",
                        "Palm Beach", "Panama City", "Pembroke Pines", "Pensacola", "Plantation", "Pompano Beach", "Port St. Lucie",
                        "Port Charlotte", "Saint Augustine", "São Petersburgo", "Sarasota", "Sunrise", "Tallahassee", "Tampa", "West Palm Beach");

        Lugar.criarEstado(usa, "Geórgia", "GA")
                .comCapital("Atlanta")
                .comCidades("Alpharetta", "Athens", "Atlanta", "Augusta", "Brunswick", "Columbus", "Dalton", "Gainesville", "Kennesaw", "LaGrange",
                        "Macon", "Marietta", "Rome", "Roswell", "Sandy Springs", "Savannah", "Statesboro", "Thomasville", "Valdosta",
                        "Warner Robins", "Waycross");

        Lugar.criarEstado(usa, "Hawaii (Havaí)", "HI")
                .comCapital("Honolulu")
                .comCidades("Hilo", "Honolulu", "Kailua", "Kailua-Kona", "Lahaina", "Lihue", "Pearl City", "Schofield Barracks", "Wahiawa", "Waipahu");

        Lugar.criarEstado(usa, "Idaho", "ID")
                .comCapital("Boise")
                .comCidades("Boise", "Coeur d'Alene", "Idaho Falls", "Lewiston", "Moscow", "Nampa", "Pocatello", "Sandpoint", "Sun Valley",
                        "Twin Falls");

        Lugar.criarEstado(usa, "Illinois", "IL")
                .comCapital("Springfield")
                .comCidades("Aurora", "Belleville", "Bloomington", "Cairo", "Calumet City", "Carbondale", "Champaign", "Charleston", "Chicago",
                        "Chicago Heights", "Cicero", "Danville", "Decatur", "DeKalb", "East St. Louis", "Evanston", "Farmer City", "Galesburg",
                        "Highland Park", "Joliet", "Kankakee", "Macomb", "Moline", "Naperville", "Normal", "North Chicago", "Peoria", "Quincy",
                        "Rock Island", "Rockford", "Springfield", "Urbana", "Waukegan", "Wheaton");

        Lugar.criarEstado(usa, "Indiana", "IN")
                .comCapital("Indianápolis")
                .comCidades("Bloomington", "Elkhart", "Evansville", "Fort Wayne", "Gary", "Hammond", "Indianápolis", "Kokomo", "Lafayette", "Muncie",
                        "South Bend", "Terre Haute", "Valparaiso", "Vincennes", "West Lafayette");

        Lugar.criarEstado(usa, "Iowa", "IA")
                .comCapital("Des Moines")
                .comCidades("Ames", "Cedar Falls", "Cedar Rapids", "Council Bluffs", "Davenport", "Des Moines", "Dubuque", "Fort Dodge", "Iowa City",
                        "Mason City", "Sioux City", "Waterloo");

        Lugar.criarEstado(usa, "Kansas", "KS")
                .comCapital("Topeka")
                .comCidades("Abilene", "Dodge City", "Hutchinson", "Kansas City", "Lawrence", "Liberal", "Manhattan", "Overland Park", "Topeka",
                        "Wichita");

        Lugar.criarEstado(usa, "Kentucky", "KY")
                .comCapital("Frankfort")
                .comCidades("Bowling Green", "Covington", "Danville", "Florence", "Frankfort", "Hopkinsville", "Jeffersontown", "Lexington",
                        "Louisville", "Murray", "Owensboro", "Paducah", "Richmond");

        Lugar.criarEstado(usa, "Louisiana", "LA")
                .comCapital("Baton Rouge")
                .comCidades("Alexandria", "Baton Rouge", "Bossier City", "Grambling", "Hammond", "Houma", "Kenner", "Lafayette", "Lake Charles",
                        "Metairie", "Monroe", "Morgan City", "Natchitoches", "New Iberia", "New Orleans", "Opelousas", "Ruston", "Shreveport",
                        "Slidell", "Thibodaux");

        Lugar.criarEstado(usa, "Maine", "ME").comCapital("Augusta")
                .comCidades("Augusta", "Bangor", "Bar Harbor", "Caribou", "Freeport", "Kennebunk", "Lewiston", "Orono", "Portland", "Presque Isle");

        Lugar.criarEstado(usa, "Maryland", "MD")
                .comCapital("Annapolis")
                .comCidades("Annapolis", "Baltimore", "Bethesda", "Bowie", "College Park", "Dundalk", "Cumberland", "Frederick", "Gaithersburg",
                        "Germantown", "Hagerstown", "Laurel", "Ocean City", "Rockville (Maryland)", "Salisbury", "Silver Spring", "Towson",
                        "Waldorf", "Washington, DC");

        Lugar.criarEstado(usa, "Massachusetts", "MA")
                .comCapital("Boston")
                .comCidades("Agawam", "Amesbur", "Attleboro", "Beverly", "Boston", "Brockton", "Cambridge", "Chelsea", "Chicopee", "Easthampton",
                        "Everett", "Fall River", "Fitchburg", "Franklin", "Gardner", "Gloucester", "Greenfield", "Haverhill", "Holyoke", "Lawrence",
                        "Leominster", "Lowell", "Lynn", "Malden", "Marlborough", "Medford", "Melrose", "Methuen", "Needham", "New Bedford",
                        "Newburyport", "Newton", "North Adams", "Northampton", "Peabody", "Pittsfield", "Quincy", "Revere", "Salem", "Somerville",
                        "Springfield", "Taunton", "Waltham", "Watertown", "West Springfield", "Westfield", "Weymouth", "Woburn", "Worcester");

        Lugar.criarEstado(usa, "Michigan", "MI")
                .comCapital("Lansing")
                .comCidades("Ann Arbor", "Auburn Hills", "Battle Creek", "Bay City", "Benton Harbor", "Dearborn", "Detroit", "East Lansing", "Flint",
                        "Grand Rapids", "Kalamazoo", "Lansing", "Marquette", "Midland", "Mount Pleasant", "Muskegon", "Port Huron", "Saginaw",
                        "Sault Ste. Marie", "Warren", "Ypsilanti");

        Lugar.criarEstado(usa, "Minnesota", "MN")
                .comCapital("Saint Paul")
                .comCidades("Bloomington", "Brooklyn Park", "Duluth", "Forest Lake", "International Falls", "Mankato", "Minneapolis", "Minnetonka",
                        "Moorhead", "Rochester", "Saint Cloud", "Saint Paul");

        Lugar.criarEstado(usa, "Mississippi", "MS")
                .comCapital("Jackson")
                .comCidades("Bay St. Louis", "Biloxi", "Columbus", "Corinth", "Greenville", "Gulfport", "Hattiesburg", "Jackson", "Laurel",
                        "Meridian", "Natchez", "Oxford", "Pascagoula", "Pass Christian", "Starkville", "Tupelo", "Vicksburg", "Yazoo");

        Lugar.criarEstado(usa, "Missouri", "MO")
                .comCapital("Jefferson City")
                .comCidades("Branson", "Cape Girardeau", "Columbia", "Fulton", "Hannibal", "Independence", "Jefferson City", "Joplin", "Kansas City",
                        "New Madrid", "Rolla", "Sedalia", "Springfield", "Saint Joseph", "Saint Louis", "Webster Groves");

        Lugar.criarEstado(usa, "Montana", "MT").comCapital("Helena")
                .comCidades("Anaconda", "Billings", "Bozeman", "Butte", "Great Falls", "Helena", "Kalispell", "Missoula");

        Lugar.criarEstado(usa, "Nebraska", "NE").comCapital("Lincoln")
                .comCidades("Grand Island", "Hastings", "Kearney", "Lincoln", "North Platte", "Omaha", "Scottsbluff", "Wahoo");

        Lugar.criarEstado(usa, "Nevada", "NV")
                .comCapital("Carson City")
                .comCidades("Boulder City", "Carson City", "Elko", "Fallon", "Fernley", "Henderson", "Las Vegas", "Laughlin", "North Las Vegas",
                        "Pahrump", "Reno", "Winnemucca");

        Lugar.criarEstado(usa, "New Hampshire (Nova Hampshire)", "NH")
                .comCapital("Concord")
                .comCidades("Nova Hampshire", "Berlin", "Claremont", "Concord", "Dover", "Franklin", "Keene", "Laconia", "Lebanon", "Loudon",
                        "Manchester", "Nashua", "Portsmouth", "Rochester", "Somersworth");

        Lugar.criarEstado(usa, "New Jersey (Nova Jérsei)", "NJ")
                .comCapital("Trenton")
                .comCidades("Asbury Park", "Atlantic City", "Camden", "Cape May", "East Rutherford", "Eatontown", "Hackensack", "Hoboken",
                        "Jersey City", "Newark", "New Brunswick", "Passaic", "Paterson", "Perth Amboy", "Princeton", "Trenton", "Vineland");

        Lugar.criarEstado(usa, "New Mexico (Novo México)", "NM")
                .comCapital("Santa Fé")
                .comCidades("Alamogordo", "Albuquerque", "Clovis", "Farmington", "Gallup", "Las Cruces", "Los Alamos", "Roswell", "Santa Fé",
                        "Truth or Consequences");

        Lugar.criarEstado(usa, "New York (Nova Iorque)", "NY")
                .comCapital("Albany")
                .comCidades("Albany", "Binghamton", "Buffalo", "Cooperstown", "Elmira", "Glens Falls", "Ithaca", "Lockport", "Long Island",
                        "Manhattan", "Mount Vernon", "New York (Nova Iorque)", "Newburgh", "New Rochelle", "Niagara Falls", "Oneonta", "Peekskill",
                        "Poughkeepsie", "Rochester", "Schenectady", "Syracuse", "Utica", "Watertown", "West Point", "White Plains", "Yonkers");

        Lugar.criarEstado(usa, "North Carolina (Carolina do Norte)", "NC")
                .comCapital("Raleigh")
                .comCidades("Asheville", "Burlington", "Cary", "Chapel Hill", "Charlotte", "Concord", "Durham", "Elizabeth City", "Fayetteville",
                        "Gastonia", "Greensboro", "Greenville", "Hickory", "High Point", "Jacksonville", "New Bern", "Raleigh", "Rocky Mount",
                        "Wilmington", "Winston-Salem");

        Lugar.criarEstado(usa, "North Dakota (Dakota do Norte)", "ND").comCapital("Bismarck")
                .comCidades("Bismarck", "Dickinson", "Fargo", "Grand Forks", "Jamestown", "Mandan", "Minot", "Williston");

        Lugar.criarEstado(usa, "Ohio", "OH")
                .comCapital("Columbus")
                .comCidades("Akron", "Atenas", "Bowling Green", "Canton", "Cincinnati", "Cleveland", "Columbus", "Dayton", "Hamilton", "Kent",
                        "Kettering", "Lakewood", "Lima", "Middletown", "Oxford", "Parma", "Springfield", "Steubenville", "Toledo", "Youngstown");

        Lugar.criarEstado(usa, "Oklahoma", "OK")
                .comCapital("Oklahoma City")
                .comCidades("Bartlesville", "Bethany", "Broken Arrow", "Edmond", "Enid", "Lawton", "Midwest City", "Moore", "Muskogee",
                        "Nichols Hills", "Norman (Oklahoma)", "Oklahoma City", "Shawnee", "Stillwater", "Tulsa");

        Lugar.criarEstado(usa, "Oregon", "OR")
                .comCapital("Salem")
                .comCidades("Astoria", "Bend", "Corvallis", "City of The Dalles", "Eugene", "Jordan Valley", "Klamath Falls", "Medford", "Ontário",
                        "Pendleton", "Portland", "Salem");

        Lugar.criarEstado(usa, "Pennsylvania (Pensilvânia)", "PA")
                .comCapital("Harrisburg")
                .comCidades("Allentown", "Altoona", "Erie", "Gettysburg", "Harrisburg", "Hershey", "Johnstown", "Lock Haven", "Filadélfia",
                        "Pittsburgh", "Punxsutawney", "Reading", "Scranton", "State College", "Titusville", "Wilkes-Barre", "Williamsport", "York");

        Lugar.criarEstado(usa, "Rhode Island", "RI").comCapital("Providence")
                .comCidades("Cranston", "Narragansett", "Newport", "Pawtucket", "Providence", "Warwick", "West Warwick", "Woonsocket");

        Lugar.criarEstado(usa, "South Carolina (Carolina do Sul)", "SC")
                .comCapital("Columbia")
                .comCidades("Anderson", "Charleston", "Clemson", "Columbia", "Darlington", "Florence", "Greenville", "Myrtle Beach", "Orangeburg",
                        "Spartanburg");

        Lugar.criarEstado(usa, "South Dakota (Dakota do Sul)", "SD").comCapital("Pierre")
                .comCidades("Aberdeen", "Deadwood", "Pierre", "Rapid City", "Sioux Falls", "Sturgis", "Vermillion", "Watertown");

        Lugar.criarEstado(usa, "Tennessee", "TN")
                .comCapital("Nashville")
                .comCidades("Bristol", "Chattanooga", "Clarksville", "Franklin", "Gatlinburg", "Jackson", "Johnson City", "Kingsport", "Knoxville",
                        "Memphis", "Murfreesboro", "Nashville", "Oak Ridge", "Pulaski");

        Lugar.criarEstado(usa, "Texas", "TX")
                .comCapital("Austin")
                .comCidades("Abilene", "Amarillo", "Arlington", "Austin", "Baytown", "Beaumont", "Brownsville", "College Station", "Corpus Christi",
                        "Dallas", "Del Rio", "Denton", "El Paso", "Fort Worth", "Galveston", "Houston", "Laredo", "Longview", "Lubbock", "Lufkin",
                        "McAllen", "Marshall", "Midland", "Nacogdoches", "Odessa", "Plano", "Port Arthur", "San Antonio", "Sugar Land", "Tyler",
                        "Waco", "Wichita Falls");

        Lugar.criarEstado(usa, "Utah", "UT")
                .comCapital("Salt Lake City")
                .comCidades("Cedar City", "Kearns", "Layton", "Logan", "Ogden", "Orem", "Provo", "St. George", "Salt Lake City", "Sandy",
                        "Taylorsville", "West Jordan", "West Valley City");

        Lugar.criarEstado(usa, "Vermont", "VT")
                .comCapital("Montpelier")
                .comCidades("Burlington", "Barre", "Montpelier", "Newport", "Rutland", "St. Albans", "St. Johnsbury", "South Burlington",
                        "Vergennes", "Winooski");

        Lugar.criarEstado(usa, "Virginia", "VA")
                .comCapital("Richmond")
                .comCidades("Alexandria", "Blacksburg", "Bristol", "Charlottesville", "Chesapeake", "Fairfax", "Fredericksburg", "Hampton",
                        "Lexington", "Lynchburg", "Manassas", "Newport News", "Norfolk", "Radford", "Richmond", "Roanoke", "Staunton", "Suffolk",
                        "Tysons Corner", "Virginia Beach", "Williamsburg");

        Lugar.criarEstado(usa, "Washington", "WA")
                .comCapital("Olympia")
                .comCidades("Aberdeen", "Bellingham", "Bremerton", "Kennewick", "Olympia", "Pasco", "Pullman", "Richland", "Seattle", "Spokane",
                        "Spokane Valley", "Tacoma", "Vancouver", "Walla Walla", "Yakima");

        Lugar.criarEstado(usa, "West Virginia (Virgínia Ocidental)", "WV")
                .comCapital("Charleston")
                .comCidades("Beckley", "Charleston", "Clarksburg", "Fairmont", "Harpers Ferry", "Huntington", "Martinsburg", "Morgantown",
                        "Parkersburg", "Salem", "Weirton", "Wheeling");

        Lugar.criarEstado(usa, "Wisconsin", "WI")
                .comCapital("Madison")
                .comCidades("Appleton", "Beloit", "Eau Claire", "Fond du Lac", "Green Bay", "Janesville", "Kenosha", "La Crosse", "Madison",
                        "Manitowoc", "Marshfield", "Milwaukee", "Oshkosh", "Racine", "Sheboygan", "Stevens Point", "Superior", "Wausau", "West Bend",
                        "Wisconsin Rapids");

        Lugar.criarEstado(usa, "Wyoming", "WY")
                .comCapital("Cheyenne")
                .comCidades("Basin", "Buffalo", "Casper", "Cheyenne", "Cody", "Douglas", "Evanston", "Gillette", "Green River", "Kemmerer", "Lander",
                        "Laramie", "Newcastle", "Powell", "Rawlins", "Riverton", "Rock Springs", "Sheridan", "Torrington", "Worland");
    }

}
