package br.com.fanaticosporviagens.infra.util.localexterior;

public enum MapeamentoPais {

    AMERICA_DO_NORTE_CANADA(MapeamentoContinente.AMERICA_DO_NORTE.getContinente(), 42L, "Canadá", "CAN", false),
    AMERICA_DO_NORTE_ESTADOS_UNIDOS_DA_AMERICA(MapeamentoContinente.AMERICA_DO_NORTE.getContinente(), 76L, "Estados Unidos da América", "USA", false),
    AMERICA_DO_SUL_ARGENTINA(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 14L, "Argentina", "ARG", false),
    AMERICA_DO_SUL_BOLIVIA(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 29L, "Bolívia", "BOL", false),
    AMERICA_DO_SUL_CHILE(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 48L, "Chile", "CHL", false),
    AMERICA_DO_SUL_COLOMBIA(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 53L, "Colômbia", "COL", false),
    AMERICA_DO_SUL_EQUADOR(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 71L, "Equador", "ECU", false),
    AMERICA_DO_SUL_PARAGUAI(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 176L, "Paraguai", "PRY", false),
    AMERICA_DO_SUL_PERU(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 177L, "Peru", "PER", false),
    AMERICA_DO_SUL_URUGUAI(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 234L, "Uruguai", "URY", false),
    AMERICA_DO_SUL_VENEZUELA(MapeamentoContinente.AMERICA_DO_SUL.getContinente(), 238L, "Venezuela", "VEN", false),
    EUROPA_ALEMANHA(MapeamentoContinente.EUROPA.getContinente(), 5L, "Alemanha", "DEU", false),
    EUROPA_BELGICA(MapeamentoContinente.EUROPA.getContinente(), 24L, "Bélgica", "BEL", false),
    EUROPA_ESPANHA(MapeamentoContinente.EUROPA.getContinente(), 75L, "Espanha", "ESP", false),
    EUROPA_FRANCA(MapeamentoContinente.EUROPA.getContinente(), 83L, "França", "FRA", false),
    EUROPA_GRECIA(MapeamentoContinente.EUROPA.getContinente(), 90L, "Grécia", "GRC", false),
    EUROPA_HOLANDA(MapeamentoContinente.EUROPA.getContinente(), 170L, "Países Baixos (Holanda)", "NLD", false),
    EUROPA_INGLATERRA(MapeamentoContinente.EUROPA.getContinente(), 186L, "Reino Unido Da Grã-Bretanha E Irlanda Do Norte", "GBR", false),
    EUROPA_ITALIA(MapeamentoContinente.EUROPA.getContinente(), 115L, "Itália", "ITA", false),
    EUROPA_PORTUGAL(MapeamentoContinente.EUROPA.getContinente(), 182L, "Portugal", "PRT", false),
    EUROPA_SUICA(MapeamentoContinente.EUROPA.getContinente(), 213L, "Suíça", "CHE", false);

    private final Lugar pais;

    private MapeamentoPais(final Lugar continente, final Long id, final String nome, final String sigla, final boolean criarScript) {
        this.pais = Lugar.criarPais(continente, id, nome, sigla, criarScript);
    }

    public Lugar getPais() {
        return this.pais;
    }

    public void registrarEstadoOuCidade(final Lugar estadoOuCidade) {
        this.pais.addSublocal(estadoOuCidade);
    }

}
