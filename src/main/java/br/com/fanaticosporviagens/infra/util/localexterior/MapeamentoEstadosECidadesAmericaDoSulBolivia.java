package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulBolivia implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar bolivia = MapeamentoPais.AMERICA_DO_SUL_BOLIVIA.getPais();
        bolivia.comCapital("Sucre");
        bolivia.comCidades("Santa Cruz", "La Paz", "El Alto", "Cochabamba", "Sucre", "Oruro", "Tarija", "Potosí", "Sacaba", "Quillacollo", "Yacuiba",
                "Montero", "Trinidad", "Riberalta", "Colcapirhua", "Tiquipaya", "Viacha", "Guayaramerín", "Cobija", "Villazón", "Bermejo", "Camiri",
                "San Ignacio de Velasco", "Tupiza", "Warnes", "Villamontes", "Cotoca", "San Borja", "Llallagua", "Yapacaní", "Vinto", "El Torno",
                "Huanuni", "Punata", "Caranavi", "El Carmen", "Ascensión", "Mineros", "Puerto Suarez", "Achocalla", "Portachuelo", "Santa Ana",
                "San Ignacio", "Rurrenabaque", "La Guardia", "Uyuni", "Roboré", "Patacamaya");
    }

}
