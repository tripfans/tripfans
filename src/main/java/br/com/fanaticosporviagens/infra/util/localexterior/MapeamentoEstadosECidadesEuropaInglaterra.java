package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaInglaterra implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar inglaterra = MapeamentoPais.EUROPA_INGLATERRA.getPais();
        inglaterra.comCapital("Londres");
        inglaterra.comCidades("Londres", "Liverpool", "Manchester", "Leeds", "Sheffield", "York", "Southampton", "Birmingham", "Oxford",
                "Canterbury ou Cantuária", "Carlisle", "Cambridge", "Gloucester", "Dover", "Coventry", "Reading", "Bristol", "Salisbury",
                "Newcastle", "Exeter", "Nottingham", "Livingstone", "Brighton", "Hastings", "Warwick", "Melrose", "Norwich", "Richmond", "Leicester",
                "Ipswich", "Plymouth", "Portsmouth", "Lincoln", "Bedford", "Winchester", "Stratford", "Worcester", "Blackpool", "Bath", "Derby",
                "Shrewsbury", "San Ives", "Middlesbrough", "Luton");

    }

}
