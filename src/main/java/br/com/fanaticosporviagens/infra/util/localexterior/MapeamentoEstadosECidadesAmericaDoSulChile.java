package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulChile implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar chile = MapeamentoPais.AMERICA_DO_SUL_CHILE.getPais();
        chile.comCapital("Santiago");
        chile.comCidades("Santiago", "Puente Alto", "Concepción (Conceição)", "Viña del Mar (Vila del Mar)", "Antofagasta", "Valparaíso",
                "Talcahuano", "San Bernardo", "Temuco", "Iquique", "Rancagua", "Punta Arenas", "Puerto Montt", "San Vicente de Tagua Tagua",
                "San Antonio", "Coquimbo", "Calama", "Chillán", "Coihaique", "Copiapó", "La Serena", "Arica", "Mejillones", "Pucón", "Puerto Varas",
                "Talca");
    }

}
