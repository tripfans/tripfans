package br.com.fanaticosporviagens.infra.util;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.codec.Base64;

import br.com.fanaticosporviagens.infra.exception.SystemException;

public final class CriptografiaUtil {

    private static Key key;

    static {
        final byte[] raw = { (byte) 0x74, (byte) 0x72, (byte) 0x69, (byte) 0x70, (byte) 0x66, (byte) 0x61, (byte) 0x6e, (byte) 0x73, (byte) 0x66,
                (byte) 0x61, (byte) 0x6e, (byte) 0x61, (byte) 0x74, (byte) 0x69, (byte) 0x63, (byte) 0x6f };

        key = new SecretKeySpec(raw, "AES");
    }

    public static final String decrypt(final String ciphertext) {

        try {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            final byte[] decodedData = Base64.decode(ciphertext.getBytes());
            final byte[] utf8 = cipher.doFinal(decodedData);
            return new String(utf8, "UTF-8");
        } catch (final Exception e) {
            throw new SystemException("Erro ao decifrar texto.", e);
        }
    }

    public static final String encrypt(final String text) {
        try {
            final Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            final byte[] utf8 = text.getBytes("UTF-8");
            final byte[] encryptedData = cipher.doFinal(utf8);
            return new String(Base64.encode(encryptedData));
        } catch (final Exception e) {
            throw new SystemException("Erro ao encriptar texto", e);
        }
    }

}