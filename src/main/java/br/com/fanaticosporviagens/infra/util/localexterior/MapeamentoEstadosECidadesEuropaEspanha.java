package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaEspanha implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar espanha = MapeamentoPais.EUROPA_ESPANHA.getPais();
        espanha.comCapital("Madrid");
        espanha.comCidades("Álava", "Albacete", "Alicante", "Almería", "Astúrias", "Ávila", "Badajoz", "Ilhas Baleares", "Barcelona", "Biscaia",
                "Burgos", "Cáceres", "Cádis", "Cantábria", "Castelló", "Ceuta", "Ciudad Real", "Córdoba", "Corunha", "Cuenca", "Gerunda", "Granada",
                "Guadalajara", "Guipúscoa", "Huelva", "Huesca", "Jaén", "La Rioja", "León", "Lérida", "Lugo", "Madrid", "Málaga", "Melilla",
                "Múrcia", "Navarra", "Ourense", "Palência", "Las Palmas", "Pontevedra", "Salamanca", "Santa Cruz de Tenerife", "Saragoça", "Segóvia",
                "Sevilha", "Sória", "Tarragona", "Teruel", "Toledo", "Valência", "Valladolid", "Zamora");

    }

}
