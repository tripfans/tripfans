package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoNorteCanada implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {
        final Lugar canada = MapeamentoPais.AMERICA_DO_NORTE_CANADA.getPais();

        canada.comCapital("Ottawa");

        Lugar.criarEstado(canada, "Colúmbia Britânica", "BC")
                .comCapital("Victoria")
                .comCidades("Abbotsford", "Armstrong", "Burnaby", "Castlegar", "Chilliwack", "Colwood", "Coquitlam", "Courtenay", "Cranbrook",
                        "Dawson Creek", "Duncan", "Enderby", "Fernie", "Fort St. John", "Grand Forks", "Greenwood", "Kamloops", "Kimberley",
                        "Kelowna", "Kitimat", "Langley", "Merritt", "Nanaimo", "Nelson", "New Westminster", "North Vancouver, cidade",
                        "North Vancouver, distrito", "Parksville", "Penticton", "Port Alberni", "Port Coquitlam", "Port Moody", "Prince George",
                        "Prince Rupert", "Quesnel", "Revelstoke", "Richmond", "Rossland", "Saanich", "Surrey", "Terrace", "Trail", "Vancouver",
                        "Vernon", "Victoria", "White Rock", "Williams Lake");

        Lugar.criarEstado(canada, "Manitoba", "MB")
                .comCapital("Winnipeg")
                .comCidades("Brandon (Manitoba)", "Churchill (Manitoba)", "Dauphin (Manitoba)", "Flin Flon", "Morden (Manitoba)",
                        "Portage la Prairie", "Selkirk (Manitoba)", "Steinbach (Manitoba)", "The Pas", "Thompson (Manitoba)", "Winkler (Manitoba)",
                        "Winnipeg", "York Factory (Povoado)");

        Lugar.criarEstado(canada, "Nova Brunswick", "NB").comCapital("Fredericton")
                .comCidades("Bathurst", "Campbellton", "Edmundston", "Fredericton", "Miramichi", "Moncton", "Saint John");

        Lugar.criarEstado(canada, "Nova Escócia", "NS").comCapital("Halifax").comCidades("Halifax", "Sydney");

        Lugar.criarEstado(canada, "Ontário", "ON")
                .comCapital("Toronto")
                .comCidades("Barrie", "Belleville", "Brampton", "Brant", "Brantford", "Brockville", "Burlington", "Cambridge", "Chatham-Kent",
                        "Clarence-Rockland", "Cornwall", "Dryden", "Elliot Lake", "Fort Erie", "Sudbury", "Guelph", "Haldimand", "Hamilton",
                        "Kawartha Lakes", "Kenora", "Kingston", "Kitchener", "Lambton Shores", "Londres", "Mississauga", "Niagara Falls",
                        "Condado de Norfolk", "North Bay", "Orillia", "Oshawa", "Ottawa", "Owen Sound", "Pembroke", "Peterborough", "Pickering",
                        "Condado de Prince Edward", "Port Colborne", "Quinte West", "Sarnia", "Sault Ste. Marie", "St. Catharines", "St. Thomas",
                        "Stratford", "Temiskaming Shores", "Thorold", "Thunder Bay", "Timmins", "Toronto", "Vaughan", "Waterloo", "Welland",
                        "Windsor", "Woodstock");

        Lugar.criarEstado(canada, "Ilha do Príncipe Eduardo", "PE").comCapital("Charlottetown").comCidades("Charlottetown", "Summerside");

        Lugar.criarEstado(canada, "Quebec", "QC")
                .comCapital("Quebec")
                .comCidades("Acton Vale", "Alma", "Amos", "Amqui", "Asbestos", "Baie-Comeau", "Baie-Saint-Paul", "Barkmere", "Beauceville",
                        "Beauharnois", "Beaupré", "Bécancour", "Bedford", "Belleterre", "Beloeil", "Berthierville", "Blainville", "Boisbriand",
                        "Bois-de-Filion", "Bonaventure", "Bromont", "Brownsburg-Chatham", "Cabano", "Candiac", "Cap-Chat", "Cap-Santé", "Carignan",
                        "Carleton-Saint-Omer", "Causapscal", "Chambly", "Chandler", "Chapais", "Charlemagne", "Châteauguay", "Château-Richer",
                        "Chibougamau", "Clermont", "Coaticook", "Contrecoeur", "Cookshire-Eaton", "Cowansville", "Danville", "Daveluyvilleille",
                        "Dégelis", "Delson", "Desbiens", "Deux-Montagnes", "Disraeli", "Dolbeau-Mistassini", "Donnacona", "Drummondville", "Dunham",
                        "Duparquet", "East Angus", "Farnham", "Fermont", "Forestville", "Fossambault-sur-le-Lac", "Gaspé", "Gatineau", "Gracefield",
                        "Granby", "Grande-Rivière", "Hudson", "Huntingdon", "Joliette", "Kingsey Falls", "Lac-Brome", "Lac-Delage", "Lachute",
                        "Lac-Mégantic", "Lac-Saint-Joseph", "Lac-Sergent", "La Malbaie", "La Pocatière", "La Prairie", "La Sarre", "L'Assomption",
                        "La Tuque", "Laval", "Lavaltrie", "Lebel-sur-Quévillon", "L'Épiphanie", "Léry", "Lévis", "L'Île-Cadieux", "L'Île-Perrot",
                        "Longueuil", "Lorraine", "Louiseville", "Magog", "Malartic", "Maniwaki", "Marieville", "Mascouche", "Matagami", "Matane",
                        "Mercier", "Métabetchouan e Lac-à-la-Croix", "Métis-sur-Mer", "Mirabel", "Mont-Joli", "Mont-Laurier", "Montmagny",
                        "Montreal - maior cidade da província", "Mont-Saint-Hilaire", "Mont-Tremblant", "Murdochville", "Neuville", "New Richmond",
                        "Nicolet", "Normandin", "Notre-Dame-de-l'Île-Perrot", "Notre-Dame-du-Lac", "Otterburn Park", "Paspébiac", "Percé",
                        "Pincourt", "Plessisville", "Pohénégamook", "Port-Cartier", "Pont-Rouge", "Portneuf", "Prévost", "Princeville", "Quebec",
                        "Repentigny", "Richelieu", "Richmond", "Rimouski", "Rivière-du-Loup", "Rivière-Rouge", "Roberval", "Rosemère",
                        "Rouyn-Noranda", "Saguenay", "Saint-Basile", "Saint-Basile-le-Grand", "Saint-Césaire", "Saint-Constant", "Sainte-Adèle",
                        "Sainte-Agathe-des-Monts", "Sainte-Anne-de-Beaupré", "Sainte-Anne-des-Monts", "Sainte-Anne-des-Plaines", "Sainte-Catherine",
                        "Sainte-Catherine-de-la-Jacques-Cartier", "Sainte-Julie", "Sainte-Marguerite e Estérel", "Sainte-Marie",
                        "Sainte-Marthe-sur-le-Lac", "Sainte-Thérèse", "Saint-Eustache", "Saint-Félicien", "Saint-Gabriel", "Saint-Georges",
                        "Saint-Hyacinthe", "Saint-Jean-sur-Richelieu", "Saint-Jérôme", "Saint-Joseph-de-Beauce", "Saint-Joseph-de-Sorel",
                        "Saint-Lazare", "Saint-Lin-Laurentides", "Saint-Marc-des-Carrières", "Saint-Ours", "Saint-Pamphile", "Saint-Pascal",
                        "Saint-Pie", "Saint-Raymond", "Saint-Rémi", "Saint-Sauveur", "Saint-Tite", "Salaberry-de-Valleyfield", "Schefferville",
                        "Scotstown", "Senneterre", "Sept-Îles", "Shawinigan", "Sherbrooke", "Sorel-Tracy", "Stanstead", "Sutton", "Témiscaming",
                        "Terrebonne", "Thetford Mines", "Thurso", "Trois-Pistoles", "Trois-Rivières", "Valcourt", "Val-d'Or", "Varennes",
                        "Vaudreuil-Dorion", "Victoriaville", "Ville-Marie", "Warwick", "Waterloo", "Waterville", "Windsor");

        Lugar.criarEstado(canada, "Saskatchewan", "SK")
                .comCapital("Regina")
                .comCidades("Estevan", "Flin Flon", "Humboldt", "Lloydminster", "Melfort", "Melville", "Moose Jaw", "North Battleford",
                        "Prince Albert", "Regina", "Saskatoon", "Swift Current", "Weyburn", "Yorkton");

        Lugar.criarEstado(canada, "Terra Nova e Labrador", "NL").comCapital("St. John's").comCidades("Corner Brook", "Mount Pearl", "St. John's");

        Lugar.criarEstado(canada, "Territórios do Noroeste", "NT").comCapital("Yellowknife").comCidades("Yellowknife");

        Lugar.criarEstado(canada, "Nunavut", "NU").comCapital("Iqaluit").comCidades("Iqaluit");

        Lugar.criarEstado(canada, "Yukon", "YT").comCapital("Whitehorse").comCidades("Whitehorse");
    }

}
