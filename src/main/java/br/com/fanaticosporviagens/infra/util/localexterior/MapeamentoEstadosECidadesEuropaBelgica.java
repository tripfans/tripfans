package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaBelgica implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar belgica = MapeamentoPais.EUROPA_BELGICA.getPais();
        belgica.comCapital("Bruxelas (Brussel)");
        belgica.comCidades("Aalst", "Aarschot", "Andenne", "Antoing", "Antuérpia (Antwerpen)", "Arlon", "Ath", "Bastogne", "Beaumont", "Beauraing",
                "Beringen", "Bilzen", "Binche", "Blankenberge", "Borgloon", "Bouillon", "Braine-le-Comte", "Bree", "Bruges (Brugge)",
                "Bruxelas (Brussel)", "Charleroi", "Châtelet", "Chièvres", "Chimay", "Chiny", "Ciney", "Comines-Warneton", "Couvin", "Damme",
                "Deinze", "Dendermonde", "Diest", "Diksmuide", "Dilsen-Stokkem", "Dinant", "Durbuy", "Eeklo", "Enghien", "Eupen", "Fleurus",
                "Florenville", "Fontaine-l'Evêque", "Fosses-la-Ville", "Gante", "Geel", "Gembloux", "Genappe", "Genk", "Geraardsbergen", "Gistel",
                "Halen", "Halle", "Hamont-Achel", "Hannut", "Harelbeke", "Hasselt", "Herentals", "Herk-de-Stad", "Herve", "Hoogstraten",
                "Houffalize", "Huy", "Izegem", "Jodoigne", "Kortrijk", "La Louvière", "La Roche-en-Ardenne", "Landen", "Le Rulx", "Lessines",
                "Leuze-en-Hainaut", "Liège", "Leuven", "Lier", "Limbourg", "Lokeren", "Lommel", "Lo-Reninge", "Maaseik", "Malmedy",
                "Marche-en-Famenne", "Mechelen", "Menen", "Mesen", "Mons", "Mortsel", "Mouscron", "Namur", "Neufchâteau", "Nieuwpoort", "Ninove",
                "Nivelles", "Oostende", "Ottignies-Louvain-la-Neuve", "Oudenaarde", "Oudenburg", "Peer", "Péruwelz", "Philippeville", "Poperinge",
                "Rochefort", "Ronse", "Roeselare", "Saint-Ghislain", "Saint-Hubert", "Sankt Vith", "Scherpenheuvel-Zichem", "Seraing",
                "Sint-Niklaas", "Sint-Truiden", "Soignies", "Spa", "Stavelot", "Thuin", "Tielt", "Tienen", "Tongeren", "Torhout", "Tournai",
                "Turnhout", "Verviers", "Veurne", "Vilvoorde", "Virton", "Visé", "Walcourt", "Waregem", "Waremme", "Wavre", "Wervik", "Ypres",
                "Zottegem", "Zoutleeuw");

    }

}
