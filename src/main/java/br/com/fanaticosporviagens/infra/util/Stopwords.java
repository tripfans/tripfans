package br.com.fanaticosporviagens.infra.util;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public class Stopwords {

    private static final String[] PORTUGUESE_LIST = new String[] { "a", "ainda", "alem", "ambas", "ambos", "antes", "ao", "aonde", "aos", "apos",
            "aquele", "aqueles", "as", "assim", "com", "como", "contra", "contudo", "cuja", "cujas", "cujo", "cujos", "da", "das", "de", "dela",
            "dele", "deles", "demais", "depois", "desde", "desta", "deste", "dispoe", "dispoem", "diversa", "diversas", "diversos", "do", "dos",
            "durante", "e", "ela", "elas", "ele", "eles", "em", "entao", "entre", "essa", "essas", "esse", "esses", "esta", "estas", "este", "estes",
            "ha", "isso", "isto", "logo", "mais", "mas", "mediante", "menos", "mesma", "mesmas", "mesmo", "mesmos", "na", "nas", "nao", "nas", "nem",
            "nesse", "neste", "nos", "o", "os", "ou", "outra", "outras", "outro", "outros", "pelas", "pelas", "pelo", "pelos", "perante", "pois",
            "por", "porque", "portanto", "proprio", "propios", "quais", "qual", "qualquer", "quando", "quanto", "que", "quem", "quer", "se", "seja",
            "sem", "sendo", "seu", "seus", "sob", "sobre", "sua", "suas", "tal", "tambem", "teu", "teus", "toda", "todas", "todo", "todos", "tua",
            "tuas", "tudo", "um", "uma", "umas", "uns" };

    public static List<String> getPortugueseList() {
        return Arrays.asList(PORTUGUESE_LIST);
    }

    public static String removeStopWords(String text) {
        // Adiciona espaços no início e ao final para ajudar a localizar as palavras inteiras
        text = " " + text.toLowerCase() + " ";
        for (String word : getPortugueseList()) {
            // Adicionar espaços
            word = " " + word + " ";
            text = text.replace(word, " ");
        }
        return text.trim();
    }

}
