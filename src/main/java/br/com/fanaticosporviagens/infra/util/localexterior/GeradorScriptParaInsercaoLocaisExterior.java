package br.com.fanaticosporviagens.infra.util.localexterior;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import br.com.fanaticosporviagens.model.entity.LocalType;

public class GeradorScriptParaInsercaoLocaisExterior {
    public static void main(final String[] args) throws SQLException, ClassNotFoundException {
        final GeradorScriptParaInsercaoLocaisExterior gerador = new GeradorScriptParaInsercaoLocaisExterior();
        gerador.popularEstadosECidades();
    }

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        return DriverManager.getConnection("jdbc:postgresql://localhost:5432/fanaticos", "postgres", "admin");
    }

    public void popularEstadosECidades() throws SQLException, ClassNotFoundException {
        final Connection connection = getConnection();
        connection.setAutoCommit(false);
        try {
            final MapeamentoEstadosClassesExterior mapeamento = new MapeamentoEstadosClassesExterior();

            for (final Lugar continente : mapeamento.createContinentes()) {
                for (final Lugar pais : continente.getSublocais()) {
                    System.out.println("/************************************************");
                    System.out.println(" * " + continente.getNome() + " - " + pais.getNome());
                    System.out.println(" ************************************************/");
                    for (final Lugar estado : pais.getSublocais()) {
                        incluirLugarESublugares(connection, estado);
                    }
                }
            }
            connection.commit();
        } finally {
            connection.close();
        }
    }

    private void incluirLugarESublugares(final Connection connection, final Lugar lugar) throws SQLException {
        if (lugar.getId() == null) {
            popularIdPorUrlPath(connection, lugar);
        }

        if (lugar.getId() == null) {
            System.out.println("NAO EXISTE - " + lugar.getId() + " - " + lugar.getLocalType() + " - " + lugar.getUrlPath() + " - " + lugar.getNome());
        } else {
            System.out.println("EXISTE - " + lugar.getId() + " - " + lugar.getLocalType() + " - " + lugar.getUrlPath() + " - " + lugar.getNome());
        }

        final Lugar continente = lugar.getLugar(LocalType.CONTINENTE);
        final Lugar pais = lugar.getLugar(LocalType.PAIS);
        final Lugar estado = lugar.getLugar(LocalType.ESTADO);
        final Lugar cidade = lugar.getLugar(LocalType.CIDADE);

        if (lugar.getId() == null) {
            final Long novoId = recupararNovoId(connection);
            lugar.setId(novoId);

            final StringBuilder sql = new StringBuilder();
            sql.append("insert into local ");
            sql.append("     ( local_type ");
            sql.append("     , id_local ");
            sql.append("     , nome ");
            sql.append("     , sigla ");
            sql.append("     , url_path ");
            sql.append("     , d_continente_id ");
            sql.append("     , id_local_continente ");
            sql.append("     , d_pais_id ");
            sql.append("     , id_local_pais ");
            sql.append("     , id_local_estado ");
            sql.append("     , d_estado_id ");
            sql.append("     , id_local_cidade ");
            sql.append("     , d_cidade_id ");
            sql.append("     , capital_pais ");
            sql.append("     , capital_estado ");
            sql.append("     ) values ");
            sql.append("     ( ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     , ? ");
            sql.append("     ) ");

            final PreparedStatement prepareStatement = connection.prepareStatement(sql.toString());

            prepareStatement.setLong(1, lugar.getLocalType().getCodigo());
            prepareStatement.setLong(2, lugar.getId());
            prepareStatement.setString(3, lugar.getNome());
            prepareStatement.setString(4, lugar.getSigla());
            prepareStatement.setString(5, lugar.getUrlPath());
            prepareStatement.setLong(6, continente.getId());
            prepareStatement.setLong(7, continente.getId());
            prepareStatement.setLong(8, pais.getId());
            prepareStatement.setLong(9, pais.getId());
            if (estado != null) {
                prepareStatement.setLong(10, estado.getId());
                prepareStatement.setLong(11, estado.getId());
            } else {
                prepareStatement.setNull(10, Types.INTEGER);
                prepareStatement.setNull(11, Types.INTEGER);
            }
            if (cidade != null) {
                prepareStatement.setLong(12, cidade.getId());
                prepareStatement.setLong(13, cidade.getId());
                prepareStatement.setBoolean(14, cidade.isCapitalPais());
                prepareStatement.setBoolean(15, cidade.isCapitalEstado());
            } else {
                prepareStatement.setNull(12, Types.INTEGER);
                prepareStatement.setNull(13, Types.INTEGER);
                prepareStatement.setBoolean(14, Boolean.FALSE);
                prepareStatement.setBoolean(15, Boolean.FALSE);
            }
            prepareStatement.executeUpdate();
        }

        for (final Lugar sublugar : lugar.getSublocais()) {
            incluirLugarESublugares(connection, sublugar);
        }
    }

    private void popularIdPorUrlPath(final Connection connection, final Lugar lugar) throws SQLException {
        if ((lugar.getUrlPath() != null) || (!lugar.getUrlPath().trim().isEmpty())) {
            final StringBuilder sql = new StringBuilder();
            sql.append("select l.id_local ");
            sql.append("  from local l ");
            sql.append(" where l.url_path = ? ");

            final PreparedStatement prepareStatement = connection.prepareStatement(sql.toString());
            prepareStatement.setString(1, lugar.getUrlPath());

            final ResultSet resultSet = prepareStatement.executeQuery();

            if (resultSet.next()) {
                lugar.setId(resultSet.getLong("id_local"));
            }
            resultSet.close();
            prepareStatement.close();
        }
    }

    private Long recupararNovoId(final Connection connection) throws SQLException {
        final StringBuilder sql = new StringBuilder();
        sql.append("select nextval('seq_local') as id_local");

        final PreparedStatement prepareStatement = connection.prepareStatement(sql.toString());
        final ResultSet resultSet = prepareStatement.executeQuery();

        Long novoId = null;
        if (resultSet.next()) {
            novoId = resultSet.getLong("id_local");
        }
        resultSet.close();
        prepareStatement.close();

        return novoId;
    }

}
