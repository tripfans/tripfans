package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulEquador implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar equador = MapeamentoPais.AMERICA_DO_SUL_EQUADOR.getPais();
        equador.comCapital("Quito");
        equador.comCidades("Quito", "Ambato", "Babahoyo", "Cuenca", "Esmeraldas", "Guaranda", "Guayaquil", "Ilbana", "Ibarra", "Latacunga", "Macas",
                "Mantas", "Milargro", "Nueva Loja", "Portoviejo", "Riobamba", "Rose Zarate", "Tulcan");
    }

}
