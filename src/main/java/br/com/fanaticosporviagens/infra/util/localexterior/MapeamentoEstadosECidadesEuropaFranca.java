package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaFranca implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar belgica = MapeamentoPais.EUROPA_FRANCA.getPais();
        belgica.comCapital("Paris");
        belgica.comCidades("Aix en Provence", "Arles", "Avignon", "Bordéus", "Cahors", "Chantilly (Oise)", "Clamart", "Cannes", "Cahors",
                "Chantilly (Oise)", "Clamart", "Cannes", "Douai", "Drancy", "Dunquerque", "Falaise", "Ferrette", "Grenoble", "Hyères",
                "Le Chambon-sur-Lignon", "Lille", "Lisieux", "Lyon", "Marselha", "Montpellier", "Mâcon", "Nantes", "Nice", "Noyon", "Nancy",
                "Orleães", "Paris", "Pau", "Provença", "Plomeur", "Reims", "Rennes", "Ruão", "Strasbourg", "Toulon", "Toulouse", "Trévoux",
                "Versalhes", "Vichy", "Villeneuve d'Ascq");

    }

}
