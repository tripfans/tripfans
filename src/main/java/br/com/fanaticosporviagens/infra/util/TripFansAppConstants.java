package br.com.fanaticosporviagens.infra.util;

/**
 * Contém constantes usadas na aplicação.
 * 
 */
public interface TripFansAppConstants {

    public static final String FACEBOOK_PROFILE_LARGE_PHOTO = "http://graph.facebook.com/%s/picture?type=large";

    public static final String FACEBOOK_PROFILE_SMALL_PHOTO = "http://graph.facebook.com/%s/picture?type=small";

    public static final String FOTO_ANONIMA_FEMININA_ALBUM = "/resources/images/perfil/blank_female_album.jpg";

    public static final String FOTO_ANONIMA_FEMININA_BIG = "/resources/images/perfil/blank_female_big.jpg";

    public static final String FOTO_ANONIMA_FEMININA_SMALL = "/resources/images/perfil/blank_female_small.jpg";

    public static final String FOTO_ANONIMA_MASCULINA_ALBUM = "/resources/images/perfil/blank_male_album.jpg";

    public static final String FOTO_ANONIMA_MASCULINA_BIG = "/resources/images/perfil/blank_male_big.jpg";

    public static final String FOTO_ANONIMA_MASCULINA_SMALL = "/resources/images/perfil/blank_male_small.jpg";

    public static final String ID_CONVITE = "ID_CONVITE";

    public static final String ID_FACEBOOK_CONVIDADO = "ID_FACEBOOK_CONVIDADO";

    public static final String ID_USUARIO_CONVIDANTE = "ID_USUARIO_CONVIDANTE";

}
