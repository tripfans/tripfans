package br.com.fanaticosporviagens.infra.util.localexterior;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// http://pt.wikipedia.org/wiki/Anexo:Lista_de_Estados_soberanos
public class MapeamentoEstadosClassesExterior {

    public static void main(final String[] args) {
        final MapeamentoEstadosClassesExterior mapeamento = new MapeamentoEstadosClassesExterior();

        final List<Lugar> lugares = mapeamento.createContinentes();

        print("", lugares);
    }

    public static void print(final String prefixo, final Collection<Lugar> lugares) {
        for (final Lugar lugar : lugares) {
            System.out.println(prefixo + lugar.getNome() + " => " + lugar.getNomeLimpo() + " => " + lugar.getUrlPath());
            if (lugar.getSublocais() != null) {
                print(prefixo + "\t", lugar.getSublocais());
            }
        }
    }

    public List<Lugar> createContinentes() {

        configurarEstadosECidades();

        final List<Lugar> continetes = new ArrayList<Lugar>();

        for (final MapeamentoContinente continente : MapeamentoContinente.values()) {
            continetes.add(continente.getContinente());
        }

        return continetes;
    }

    private void configurarEstadosECidades() {

        final String packageName = "br.com.fanaticosporviagens.infra.util.localexterior";

        final URL root = Thread.currentThread().getContextClassLoader().getResource(packageName.replace(".", "/"));

        // Filter .class files.
        final File[] files = new File(root.getFile()).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                return name.endsWith(".class");
            }
        });

        for (final File file : files) {

            final String className = packageName + "." + file.getName().replaceAll(".class$", "");

            try {

                final Class<?> classeMapeamento = Class.forName(className);

                if (MapeamentoEstadosECidades.class.isAssignableFrom(classeMapeamento) && !classeMapeamento.isInterface()) {
                    try {
                        final MapeamentoEstadosECidades mapeamento = (MapeamentoEstadosECidades) classeMapeamento.newInstance();
                        mapeamento.configurarEstadosECidades();
                    } catch (final InstantiationException e) {
                        throw new RuntimeException(e);
                    } catch (final IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }

            } catch (final ClassNotFoundException e) {
                throw new RuntimeException("Classe nao encontrada (" + className + ")", e);
            }
        }
    }
}
