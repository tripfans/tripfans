package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulArgentina implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {
        final Lugar argentina = MapeamentoPais.AMERICA_DO_SUL_ARGENTINA.getPais();

        Lugar.criarEstado(argentina, "Província de Buenos Aires", "BUE").comCidades("Buenos Aires", "La Plata", "Quilmes", "Bahía Blanca",
                "Ituzaingó", "Lanús", "Tandil", "Lobos", "Trujui", "Lomas de Zamora", "Junin", "Mar del Plata", "Tigre", "Bragado",
                "San Nicolás de los Arroyos", "Moron", "Pergamino", "Moreno", "San Martín", "Carmen de Patagones", "Chivilcoy", "Chascomús",
                "Quequén", "San Clemente del Tuyu", "Mar de Ajó", "Mar del Tuyú");

        Lugar.criarEstado(argentina, "Província de Chaco", "CHA").comCidades("Resistência", "Presidencia Roque Sáenz Peña", "Villa Ángela",
                "Barranqueras", "General José de San Martín");

        Lugar.criarEstado(argentina, "Província de Chubut", "CHU").comCidades("Rawson", "Comodoro Rivadavia", "Esquel", "Puerto Madryn", "Trelew");

        Lugar.criarEstado(argentina, "Província de Córdoba", "CBA").comCidades("Córdoba", "Cosquín", "Jesús María", "La Carlota", "Río Cuarto",
                "San Francisco", "Villa María");

        Lugar.criarEstado(argentina, "Província de Corrientes", "CTE").comCidades("Corrientes", "Goya", "Santo Tomé", "Paso de los Libres",
                "Curuzú Cuatiá", "Gobernador Virasoro");

        Lugar.criarEstado(argentina, "Província de Entre Ríos", "ERI").comCidades("Paraná", "Concordia", "Basavilbaso", "Gualeguay", "Gualeguaychú",
                "La Paz", "Victoria", "Crespo", "Libertador San Martín", "Concepción del Uruguay", "Villa Elisa", "Villaguay", "Urdinarrain");

        Lugar.criarEstado(argentina, "Província de Formosa", "FOR").comCidades("Formosa", "Clorinda");

        Lugar.criarEstado(argentina, "Província de Jujuy", "JUY").comCidades("San Salvador de Jujuy", "Tilcara");

        Lugar.criarEstado(argentina, "Província de La Pampa", "LPA").comCidades("Santa Rosa", "Eduardo Castex");

        Lugar.criarEstado(argentina, "Província de La Rioja", "LRJ").comCidades("La Rioja");

        Lugar.criarEstado(argentina, "Província de Mendoza", "MZA").comCidades("Mendonza");

        Lugar.criarEstado(argentina, "Província de Misiones", "MIS").comCidades("25 de Mayo", "9 de Julio", "Alba Posse", "Almafuerte", "Apóstoles",
                "Aristóbulo del Valle", "Arroyo del Medio", "Azara", "Bernardo de Irigoyen", "Bonpland", "Caá Yarí", "Campo Grande", "Campo Ramón",
                "Campo Viera", "Candelaria", "Capioví", "Caraguatay", "Cerro Azul", "Cerro Corá", "Colonia Alberdi", "Colonia Aurora",
                "Colonia Delicia", "Colonia Polana", "Colonia Victoria", "Comandante Andrés Guacurarí", "Concepción de la Sierra", "Corpus",
                "Dos Arroyos", "Dos de Mayo", "El Alcázar", "Eldorado", "El Soberbio", "Fachinal", "Florentino Ameghino", "Garuhapé", "Garupá",
                "General Alvear", "General Urquiza", "Gobernador López", "Gobernador Roca", "Guaraní", "Hipólito Yrigoyen", "Itacaruaré",
                "Jardín América", "Leandro N. Alem", "Loreto", "Los Helechos", "Mártires", "Mojón Grande", "Montecarlo", "Oberá",
                "Olegario Víctor Andrade", "Panambí", "Posadas", "Profundidad", "Puerto Esperanza", "Puerto Iguazú", "Puerto Leoni",
                "Puerto Libertad", "Puerto Piray", "Puerto Rico", "Ruiz de Montoya", "San Antonio", "San Ignacio", "San Javier", "San José",
                "San Martín", "San Pedro", "Santa Ana", "Santa María", "Santiago de Liniers", "Santo Pipó", "San Vicente", "Tres Capones");

        Lugar.criarEstado(argentina, "Província de Neuquén", "NEU").comCidades("Neuquén", "Junín de los Andes", "San Martín de los Andes", "Zapala",
                "Chos Malal", "Centenário", "Plottier");

        Lugar.criarEstado(argentina, "Província de Río Negro", "RNG").comCidades("Cipolletti", "General Roca", "Viedma", "San Carlos de Bariloche");

        Lugar.criarEstado(argentina, "Província de Salta", "STA").comCidades("Salta");

        Lugar.criarEstado(argentina, "Província de San Juan", "SJU").comCidades("San Juan");

        Lugar.criarEstado(argentina, "Província de San Luis", "SLU").comCidades("San Luis");

        Lugar.criarEstado(argentina, "Província de Santa Cruz", "SCR").comCidades("Río Gallegos");

        Lugar.criarEstado(argentina, "Província de Santa Fé", "SFE").comCidades("Santa Fé", "Rosário");

        Lugar.criarEstado(argentina, "Província de Santiago del Estero", "SGO").comCidades("Santiago del Estero", "Isca Yacu");

        Lugar.criarEstado(argentina, "Província de Tierra del Fuego", "TDF").comCidades("Ushuaia", "Rio Grande");

        Lugar.criarEstado(argentina, "Província de Tucumán", "TUC").comCidades("San Miguel de Tucumán");

    }

}
