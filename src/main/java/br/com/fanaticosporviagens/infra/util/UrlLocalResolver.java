package br.com.fanaticosporviagens.infra.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.reflections.Reflections;

import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalType;

/**
 * Resolve URLs de determinados tipos de locais.
 * 
 * <br/>
 * 
 * Por exemplo, para um {@link Hotel}, ele retorna a url /destino/{urlPathDoLocal}.
 * 
 * @author André Thiago
 * 
 */
public class UrlLocalResolver {

    public static final String LOCAL_URL_MAPPING = "/destinos";

    public static final String LOCAL_URL_MAPPING_OLD = "/locais";

    private static final Map<String, String> localUrlsMap;

    static {
        localUrlsMap = new HashMap<String, String>();
        final Reflections reflections = new Reflections("br.com.fanaticosporviagens.model.entity");
        final Set<Class<? extends Local>> subClasses = reflections.getSubTypesOf(Local.class);
        for (final Class<?> clazz : subClasses) {
            localUrlsMap.put(clazz.getName(), "/" + clazz.getSimpleName().toLowerCase() + "/");
        }
    }

    public static String getUrl(final Local local) {
        if (local == null) {
            throw new IllegalArgumentException("O local deve ser informado para a obtenção da URL");
        }
        if (StringUtils.isEmpty(local.getUrlPath())) {
            throw new IllegalArgumentException("O local deve ter o campo 'urlPath' preenchido [id=" + local.getId() + ";nome=" + local.getNome()
                    + ";tipo=" + local.getTipoLocal() + "]");
        }
        return getUrl(local.getTipoLocal(), local.getUrlPath());
    }

    public static String getUrl(final LocalType localType, final String urlPath) {
        /*String nome = localType.getDescricao().toLowerCase();
        nome = Normalizer.normalize(nome, Normalizer.Form.NFD);
        nome = nome.replaceAll("[^\\p{ASCII}]", "");*/
        return LOCAL_URL_MAPPING + "/" + urlPath;
    }

}
