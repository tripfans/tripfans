package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaSuica implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar suica = MapeamentoPais.EUROPA_SUICA.getPais();
        suica.comCapital("Berna");
        suica.comCidades("Aarau", "L'Abbaye", "Aigle", "Alpnach", "Appenzell", "Apples", "Arni", "Aubonne", "Bad Ragaz", "Baden", "Ballens",
                "Basileia", "Beckenried", "Bellinzona", "Belmont-sur-Lausanne", "Berna", "Berolle", "Bex", "Biel Bienne", "Bière", "Bougy-Villars",
                "Bühler", "Buochs", "Cham", "Cheseaux-sur-Lausanne", "Chessel", "Chexbres", "Coira", "Corbeyrier", "Crissier", "Conters",
                "Dallenwil", "Davos", "Düdingen", "Egg", "Emmetten", "Engelberg", "Ennetbürgen", "Ennetmoos", "Epalinges", "Erlenbach", "Esslingen",
                "Etoy", "Féchy", "Feldbach", "Friburgo", "Gais", "Genebra", "Gimel", "Giswil", "Gonten", "Grub", "Gryon", "Habsburgo", "Heiden",
                "Hergiswil", "Herisau", "Hundwil", "Interlaken", "Jouxtens-Mézery", "Kerns", "Lausana", "Lauterbrunnen", "Lavey-Morcles",
                "Le Chenit", "Leysin", "Le Chenit", "Locarno", "Longirod", "Lucerna", "Lugano", "Lungern", "Lutzenberg", "Männedorf", "Meilen",
                "Le Mont-sur-Lausanne", "Montreux", "Noville", "Oberdorf", "Oberegg", "Ollon", "Ormont-Dessous", "Ormont-Dessus", "Paudex", "Prilly",
                "Pully", "Rapperswil", "Rehetobel", "Reichenau", "Renens", "Rennaz", "Reute", "Roche", "Rolle", "Romanel-sur-Lausanne", "Rüte",
                "Sachseln", "São Galo", "Sarnen", "Schaffhausen", "Schlatt-Haslen", "Schleinikon", "Schönengrund", "Schwellbrunn", "Schwyz",
                "Schwende", "Sion", "Soleura", "Speicher", "Sankt-Moritz", "Stans", "Stansstad", "Stein", "Teufen", "Thal", "Thalwil", "Tuen",
                "Trogen", "Urnäsch", "Uzwil", "Vevey", "Villeneuve", "Wald", "Waldstatt", "Walzenhausen", "Weiach", "Wildhaus", "Winterthur",
                "Wolfenschiessen", "Wolfhalden", "Yvorne", "Zugo", "Zurique");

    }
}
