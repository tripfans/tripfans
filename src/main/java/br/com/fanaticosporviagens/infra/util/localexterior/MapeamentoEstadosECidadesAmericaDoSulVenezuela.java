package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulVenezuela implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar venezuela = MapeamentoPais.AMERICA_DO_SUL_VENEZUELA.getPais();
        venezuela.comCapital("Caracas");
        venezuela.comCidades("Caracas", "Acarigua", "Altagracia de Orituco", "Anaco", "Araure", "Bachaquero", "Barcelona", "Barinas", "Barquisimeto",
                "Baruta", "Cabimas", "Cabudare", "Cagua", "Calabozo", "Cantaura", "Caraballeda", "Carora", "Carrizal", "Carúpano", "Catia La Mar",
                "Caucagüita", "Chacao", "Charallave", "Chivacoa", "Ciudad Bolívar", "Ciudad Guayana", "Ciudad Ojeda", "Cúa", "Cumaná", "Ejido",
                "El Cafetal", "El Hatillo", "El Limón", "El Tigre", "El Tocuyo", "El Vigía", "Guacara", "Guanare", "Guarenas", "Guasdualito",
                "Guatire", "Güigüe", "La Asunción", "La Dolorita", "La Guaira", "La Victoria", "Las Tejerías", "Los Dos Caminos", "Los Rastrojos",
                "Los Teques", "Machiques", "Maiquetía", "Maracaibo", "Maracay", "Mariara", "Maturín", "Mérida", "Moron", "Ocumare del Tuy",
                "Palo Negro", "Petare", "Porlamar", "Puerto Ayacucho", "Puerto Cabello", "Puerto la Cruz", "Punta Cardón", "Punto Fijo", "Quíbor",
                "Rubio", "San Antonio de los Altos", "San Antonio del Tachira", "San Carlos", "San Carlos del Zulia", "San Cristóbal", "San Felipe",
                "San Fernando de Apure", "San Joaquín", "San José de Guanipa", "San Juan de Colón", "San Juan de los Morros", "San Mateo",
                "Santa Ana de Coro", "Santa Rita", "Santa Teresa del Tuy", "Tacarigua", "Táriba", "Tinaquillo", "Trujillo", "Tucupita", "Turmero",
                "Upata", "Valencia", "Valera", "Valle de la Pascua", "Villa Bruzual", "Villa de Cura", "Villa del Rosario", "Villa Rosa",
                "Yaritagua", "Zaraza");
    }
}
