package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesEuropaItalia implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar italia = MapeamentoPais.EUROPA_ITALIA.getPais();
        italia.comCapital("Roma");
        italia.comCidades("Roma", "Milão", "Nápoles", "Turim", "Palermo", "Génova", "Bolonha", "Florença", "Bari", "Catânia", "Veneza", "Verona",
                "Messina", "Pádua", "Trieste", "Taranto", "Bréscia", "Prato", "Reggio Calábria", "Parma", "Modena", "Reggio nell'Emilia", "Peruggia",
                "Livorno", "Ravena", "Cagliari", "Foggia", "Rimini", "Salerno", "Ferrara", "Sássari", "Siracusa", "Pescara", "Monza", "Latina",
                "Bérgamo", "Forlì", "Giugliano in Campania", "Trento", "Vicenza", "Terni", "Novara", "Bolzano", "Piacenza", "Ancona", "Arezzo",
                "Ándria", "Údine", "Cesena", "La Spezia", "Lecce", "Pésaro", "Alexandria", "Barletta", "Catanzaro", "Pistoia", "Brindisi", "Pisa",
                "Torre del Greco", "Como", "Lucca", "Pozzuoli", "Guidonia Montecelio", "Marsala", "Treviso", "Busto Arsizio", "Varese",
                "Sesto San Giovanni", "Grosseto", "Casoria", "Caserta", "Gela", "Asti", "Cinisello Balsamo", "Ragusa", "L'Aquila", "Cremona",
                "Quartu Sant'Elena", "Pavia", "Lamezia Terme", "Massa", "Trapani", "Aprilia", "Cosenza", "Fiumicino", "Altamura", "Imola", "Potenza",
                "Carpi", "Carrara", "Castellammare di Stabia", "Viareggio", "Fano", "Afragola", "Vigevano", "Viterbo", "Vittoria", "Savona",
                "Benevento", "Crotone");

    }

}
