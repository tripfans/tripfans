package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulPeru implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar peru = MapeamentoPais.AMERICA_DO_SUL_PERU.getPais();
        peru.comCapital("Lima");
        peru.comCidades("Abancay", "Acobamba", "Acomayo", "Aguaytía", "Aija", "Ambo", "Andahuaylas", "Anta", "Antabamba", "Aplao", "Arequipa",
                "Atalaya (Ucayali)", "Ayacucho", "Bagua Grande", "Barranca", "Belén (Ayacucho)", "Caballococha", "Cabana", "Cajamarca", "Cajatambo",
                "Calca", "Camaná", "Candarave", "Canta", "Caravelí", "Caraz", "Carhuaz", "Casma", "Cerro de Pasco", "Chacas", "Chachapoyas",
                "Chalhuanca", "Chavinillo", "Chimbote", "Chincheros", "Chiquián", "Chivay", "Chuquibamba", "Chuquibambilla", "Contamana", "Corongo",
                "Cotahuasi", "Cusco", "Desaguadero", "El Callao", "Huacaybamba", "Huacho", "Huacrachuco", "Huancavelica", "Huancayo", "Huánuco",
                "Huaral", "Huaraz", "Huari", "Huarmey", "Ica", "Ilo", "Iñapari", "Iquitos", "Jesús", "Juliaca", "La Rinconada", "La Unión (Huánuco)",
                "Lambayeque", "Lamud", "Leymebamba", "Llamellín", "Llata", "Locumba", "Loreto", "Machu Picchu pueblo", "Matucana", "Mendoza",
                "Mollendo", "Moquegua", "Moyobamba", "Nauta", "Nazca", "Ocros", "Ollantaytambo", "Omate", "Oyón", "Paita", "Pampas (Tayacaja)",
                "Panao", "Paruro", "Paucartambo", "Pisco", "Piscobamba", "Piura", "Pomabamba", "Pucallpa", "Puerto Inca", "Puerto Maldonado",
                "Puerto Ocopa", "Puno", "Quillabamba", "Recuay", "Requena", "Salaverry", "San Lorenzo", "San Luis", "San Vicente de Cañete",
                "Santa María de Nieva", "São Tomás", "Satipo", "Sicuani", "Sihuas", "Sullana", "Tacna", "Tambobamba", "Tarapoto", "Tarata", "Tarma",
                "Tingo María", "Trujillo", "Tumbes", "Ucayali", "Urcos", "Urubamba", "Villa de Jumbilla", "Yanaoacá", "Yauri", "Yauyos", "Yungay",
                "Yurimaguas", "Zarumilla", "Zorritos", "Chiclayo", "Lima");
    }
}
