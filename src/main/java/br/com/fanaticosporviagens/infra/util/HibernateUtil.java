package br.com.fanaticosporviagens.infra.util;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.proxy.HibernateProxy;

public class HibernateUtil {

    private static final HibernateUtil hibernateUtil = new HibernateUtil();

    public static HibernateUtil getInstance() {
        return hibernateUtil;
    }

    private HibernateUtil() {

    }

    public <T> List<T> derreferenciarLista(final List<T> lista) {
        final List<T> derreferenciada = new ArrayList<T>();
        for (final T elemento : lista) {
            derreferenciada.add(derreferenciarProxy(elemento));
        }
        return derreferenciada;
    }

    @SuppressWarnings("unchecked")
    public <T> T derreferenciarProxy(final T entidadeOuProxy) {
        if (entidadeOuProxy instanceof HibernateProxy) {
            final HibernateProxy proxy = (HibernateProxy) entidadeOuProxy;
            return ((T) proxy.getHibernateLazyInitializer().getImplementation());
        } else {
            return entidadeOuProxy;
        }
    }

    public <T> String getClassNameSemProxy(final T entidadeOuProxy) {
        String className = entidadeOuProxy.getClass().getSimpleName();
        if (className.contains("_")) {
            className = className.substring(0, className.indexOf("_"));
        }
        return className;
    }

}
