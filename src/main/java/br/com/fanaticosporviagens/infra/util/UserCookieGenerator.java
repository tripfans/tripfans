/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.fanaticosporviagens.infra.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.CookieGenerator;

/**
 * Classe utilitaria para gerenciar o cookie que lembra o nome do usuario logado.
 * 
 */
public final class UserCookieGenerator {

    private final CookieGenerator userCookieGenerator = new CookieGenerator();

    public UserCookieGenerator() {
        this.userCookieGenerator.setCookieName("usuario_fanaticos");
        this.userCookieGenerator.setCookieMaxAge(Integer.MAX_VALUE);
    }

    public void addCookie(final String userId, final HttpServletResponse response) {
        this.userCookieGenerator.addCookie(response, userId);
    }

    public String readCookieValue(final HttpServletRequest request) {
        final Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            return null;
        }
        for (final Cookie cookie : cookies) {
            if (cookie.getName().equals(this.userCookieGenerator.getCookieName())) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public void removeCookie(final HttpServletResponse response) {
        this.userCookieGenerator.removeCookie(response);
    }

}