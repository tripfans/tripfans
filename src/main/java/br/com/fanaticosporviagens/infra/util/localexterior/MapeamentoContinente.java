package br.com.fanaticosporviagens.infra.util.localexterior;

public enum MapeamentoContinente {
    AFRICA(19192L, "Africa", "AFR"),
    AMERICA_CENTRAL(19185L, "América Central", "AMC"),
    AMERICA_DO_NORTE(19187L, "América do Norte", "AMN"),
    AMERICA_DO_SUL(19188L, "América do Sul", "AMS"),
    ANTARTIDA(19189L, "Antártida", "ANT"),
    ASIA(19193L, "Ásia", "ASI"),
    EUROPA(19190L, "Europa", "EUR"),
    OCEANIA(19191L, "Oceania", "OCE");

    private final Lugar continente;

    private MapeamentoContinente(final Long id, final String nome, final String sigla) {
        this.continente = Lugar.criarContinente(id, nome, sigla);
    }

    public Lugar getContinente() {
        return this.continente;
    }

    public void registrarPais(final Lugar pais) {
        this.continente.addSublocal(pais);
    }

}
