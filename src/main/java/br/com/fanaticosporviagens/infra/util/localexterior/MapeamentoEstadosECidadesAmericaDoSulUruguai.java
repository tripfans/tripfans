package br.com.fanaticosporviagens.infra.util.localexterior;

public class MapeamentoEstadosECidadesAmericaDoSulUruguai implements MapeamentoEstadosECidades {

    @Override
    public void configurarEstadosECidades() {

        final Lugar uruguai = MapeamentoPais.AMERICA_DO_SUL_URUGUAI.getPais();
        uruguai.comCapital("Montevidéu");
        uruguai.comCidades("Montevidéu", "Artigas", "Canelones", "Ciudad de la Costa", "Colônia do Sacramento", "Chuy", "Durazno", "Florida",
                "Fray Bentos", "Las Piedras", "Maldonado", "Melo", "Aceguá", "Mercedes", "Minas", "Nueva Palmira", "Paysandú", "Punta del Este",
                "Rivera", "Rocha", "Salto", "San José de Mayo", "Treinta y Tres", "Trinidad");
    }

}
