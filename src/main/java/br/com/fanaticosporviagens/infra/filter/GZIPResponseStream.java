package br.com.fanaticosporviagens.infra.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

/**
 * Classe acessória para realizar a compactação da resposta web
 * 
 * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
 */
public class GZIPResponseStream extends ServletOutputStream {
    public ByteArrayOutputStream baos = null;

    public boolean closed = false;

    public GZIPOutputStream gzipstream = null;

    public ServletOutputStream output = null;

    public HttpServletResponse response = null;

    public GZIPResponseStream(final HttpServletResponse response) throws IOException {
        super();
        this.closed = false;
        this.response = response;
        this.output = response.getOutputStream();
        this.baos = new ByteArrayOutputStream();
        this.gzipstream = new GZIPOutputStream(this.baos);
    }

    @Override
    public void close() throws IOException {
        if (this.closed) {
            throw new IOException("This output stream has already been closed");
        }
        this.gzipstream.finish();

        final byte[] bytes = this.baos.toByteArray();

        this.response.addHeader("Content-Length", Integer.toString(bytes.length));
        this.response.addHeader("Content-Encoding", "gzip");
        this.output.write(bytes);
        this.output.flush();
        this.output.close();
        this.closed = true;
    }

    public boolean closed() {
        return (this.closed);
    }

    @Override
    public void flush() throws IOException {
        if (this.closed) {
            throw new IOException("Cannot flush a closed output stream");
        }
        this.gzipstream.flush();
    }

    public void reset() {
        // noop
    }

    @Override
    public void write(final byte b[]) throws IOException {
        write(b, 0, b.length);
    }

    @Override
    public void write(final byte b[], final int off, final int len) throws IOException {
        if (this.closed) {
            throw new IOException("Cannot write to a closed output stream");
        }
        this.gzipstream.write(b, off, len);
    }

    @Override
    public void write(final int b) throws IOException {
        if (this.closed) {
            throw new IOException("Cannot write to a closed output stream");
        }
        this.gzipstream.write((byte) b);
    }
}
