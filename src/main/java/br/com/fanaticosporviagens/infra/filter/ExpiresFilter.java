package br.com.fanaticosporviagens.infra.filter;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Filtro utilizado para forçar os browsers a utilizarem o cache local para recursos web padrões
 * como imagens, estilos, scritps
 * Os recursos requisitados tem seus cabeçalhos (HTTP HEADER) alterados
 * para que suas datas de expiração de cache - tempo em que o browser armazena os recursos localmente -
 * durem um período longo, evitando que sejam feitas requisições desnecessárias ao servidor e consequentemente
 * diminuindo o trafego de dados na rede.
 * 
 * @author Carlos Nascimento (carlosn@pgr.mpf.gov.br)
 */
public class ExpiresFilter implements Filter {

    private static final String HEADER_CACHE_CONTROL = "Cache-Control";

    private static final String HEADER_EXPIRES = "Expires";

    // private static final String HEADER_LAST_MODIFIED = "Last-Modified";

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {

        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            final HttpServletRequest httpRequest = (HttpServletRequest) request;
            final HttpServletResponse httpResponse = (HttpServletResponse) response;

            if (response.isCommitted()) {
                chain.doFilter(request, response);
            } else {

                // Recupera a versão do sistema
                // String version = SpringUtils.getBean(UtilConfiguracaoSistema.class).getVersaoSistema();

                // Recupera a URL da requisição
                final String requestURL = httpRequest.getRequestURL().toString().toLowerCase();

                final boolean isImageRequest = requestURL.endsWith(".gif") || requestURL.endsWith(".jpg") || requestURL.endsWith(".png")
                        || requestURL.endsWith(".ico");
                final boolean isCssOrJSRequest = requestURL.endsWith(".js") || requestURL.endsWith(".css");

                // Verifica se é uma requisição por imagem ou por JS ou CSS que sejam gerados para versão especifica.
                if (isImageRequest || (isCssOrJSRequest)) { // && requestURL.indexOf(version) != 1)) {
                    Date expirationDate = null;

                    final Calendar calendar = Calendar.getInstance();
                    // Validade de 1 mes a partir da data atual
                    calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1);
                    expirationDate = calendar.getTime();

                    final String maxAgeDirective = "max-age=" + ((expirationDate.getTime() - System.currentTimeMillis()) / 1000);

                    // Forçar a utilização do cache por um período de 1 mes
                    httpResponse.setHeader(HEADER_CACHE_CONTROL, maxAgeDirective);
                    httpResponse.setDateHeader(HEADER_EXPIRES, expirationDate.getTime());
                }

                httpResponse.setHeader("Vary", "Accept-Encoding");

                // Executar GZIP apenas no ambiente de desenvolvimento.
                // No ambiente de produção, o web server é responsável por realizar a compactação
                // if (SpringUtils.getBean(UtilConfiguracaoSistema.class).isAmbienteDesenvolvimento()) {
                // Se for requisição por JS ou CSS, compactar os arquivos
                if (!isImageRequest) {
                    // Check for the HTTP header that signifies GZIP support
                    final String acceptEncoding = httpRequest.getHeader("accept-encoding");
                    if (acceptEncoding != null && acceptEncoding.indexOf("gzip") != -1) {
                        final GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(httpResponse);
                        chain.doFilter(request, wrappedResponse);
                        wrappedResponse.finishResponse();
                        return;
                    }
                }
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }
}
