package br.com.fanaticosporviagens.infra.view;

import java.io.File;
import java.io.IOException;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.util.TripFansEnviroment;

/**
 * Classe que realiza operações ligadas à unificação e à compactação/compressão do conteúdo de arquivos web.
 * 
 * @author carlosn
 */
public class JavaScriptCssOptmizer {

    public static void main(final String[] args) throws IOException {
        /*
            node r.js -o app.build.js name=home out=../scripts/app/home.js
            node r.js -o app.build.js name=home out=../scripts/app/home.js
            
            node r.js -o cssIn=./app/home.css out=../styles/app/home.css
         */

        /*
            java -classpath "C:\Projetos\.metadata\.plugins\org.eclipse.wst.server.core\tmp9\wtpwebapps\fpv\resources\build\js.jar" org.mozilla.javascript.tools.shell.Main r.js -o app.build.js name=home out=../scripts/app/home.js
            
            java -classpath "C:\Projetos\.metadata\.plugins\org.eclipse.wst.server.core\tmp9\wtpwebapps\fpv\resources\build\js.jar" org.mozilla.javascript.tools.shell.Main r.js -o app.build.js name=home cssIn=./app/home.css out=../styles/app/home.css

         */
    }

    public static void optimizeCss(final String path, final String fileName) throws IOException, InterruptedException {
        String parameters = "cssIn=./app/" + fileName + " out=../styles/app/" + fileName;
        if (!isDevel()) {
            parameters += " optimizeCss=standard.keepLines";
        }
        executeCommand(path, parameters);
    }

    public static void optimizeJavascripts(final String path, final String fileName) throws IOException, InterruptedException {
        String parameters = "app.build.js name=" + fileName.substring(0, fileName.indexOf(".")) + " out=../scripts/app/" + fileName;
        if (!isDevel()) {
            parameters += " optimize=uglify";
        }
        executeCommand(path, parameters);
    }

    public static void optimizeJavascriptsAndCss(final String path) throws IOException, InterruptedException {
        // Caminho para encontrar os scritps que serão otimizados
        final String classPath = path + "resources" + File.separator + "build" + File.separator + "app";

        // Mapeia o diretorio
        final File dir = new File(classPath);
        for (final File file : dir.listFiles()) {
            if (file.getName().endsWith(".js")) {
                optimizeJavascripts(path, file.getName());
            } else if (file.getName().endsWith(".css")) {
                optimizeCss(path, file.getName());
            }
        }
    }

    private static void executeCommand(final String path, final String parameters) throws IOException, InterruptedException {

        // Caminho para encontrar a biblioteca que fará a otimização
        final String classPath = path + "resources" + File.separator + "build";

        final Runtime rt = Runtime.getRuntime();

        // Mapeia o diretorio
        final File dir = new File(classPath);

        final String aspas = (System.getProperty("os.name").toUpperCase()).contains("WINDOWS") ? "\"" : "";
        // Montagem da linha de comando para executar a otimização

        final String command = "java -classpath " + aspas + "js.jar" + aspas + " org.mozilla.javascript.tools.shell.Main " + "r.js -o " + parameters;

        // Executação do comando
        final Process p = rt.exec(command, new String[] {}, dir);
        // p.waitFor();

        // Realizar a leitura do console em busca de erros ocorridos durante a execução
        /*final BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line = reader.readLine();
        while (line != null) {
            line = reader.readLine();
            System.out.println(line);
        }*/
    }

    private static boolean isDevel() {
        final TripFansEnviroment tripfansEnv = SpringBeansProvider.getBean(TripFansEnviroment.class);
        return tripfansEnv.isDevel();
    }

}
