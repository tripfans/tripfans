package br.com.fanaticosporviagens.infra.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StaticGoogleMapProxyServlet extends HttpServlet {

    protected static final int bufferSize = 4 * 1024;

    @Override
    public void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {

        final ServletContext cntx = getServletContext();

        HttpURLConnection conexaoHttp = null;
        InputStream in = null;
        OutputStream out = null;

        try {
            final URL urlStaticMap = new URL("http://maps.googleapis.com/maps/api/staticmap?" + req.getQueryString());
            conexaoHttp = (HttpURLConnection) urlStaticMap.openConnection();
            conexaoHttp.setRequestMethod("GET");
            conexaoHttp.connect();
            conexaoHttp.getContent();

            resp.setContentType(conexaoHttp.getContentType());
            resp.setContentLength(conexaoHttp.getContentLength());

            in = urlStaticMap.openStream();
            out = resp.getOutputStream();

            final byte[] buf = new byte[bufferSize];
            int bytesRead;
            while ((bytesRead = in.read(buf)) != -1) {
                out.write(buf, 0, bytesRead);
            }

        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            conexaoHttp.disconnect();
            conexaoHttp = null;
            if (in != null)
                in.close();
            if (out != null)
                out.close();
        }
    }

    @Override
    public final void init() throws ServletException {

    }

}
