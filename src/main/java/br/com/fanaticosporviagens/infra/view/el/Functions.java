package br.com.fanaticosporviagens.infra.view.el;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.apache.commons.beanutils.PropertyUtils;

import br.com.fanaticosporviagens.model.entity.ConfiguracaoNotificacoesPorEmail;
import br.com.fanaticosporviagens.model.entity.TipoNotificacaoPorEmail;

public final class Functions {

    public static Object getPropertyValue(final Object object, final String propertyName) {
        try {
            return PropertyUtils.getProperty(object, propertyName);
        } catch (final IllegalAccessException e) {
            e.printStackTrace();
        } catch (final InvocationTargetException e) {
            e.printStackTrace();
        } catch (final NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    // TODO ver se nao dá pra usar o metodo getProperty para substituir esse
    public static boolean getStatusTipoNotificacao(final ConfiguracaoNotificacoesPorEmail configuracao, final String tipo) {
        return configuracao.getStatusConfiguracao(tipo);
    }

    public static boolean getStatusTipoNotificacao(final ConfiguracaoNotificacoesPorEmail configuracao, final TipoNotificacaoPorEmail tipo) {
        return configuracao.getStatusConfiguracao(tipo);
    }

    public static boolean localMarcado(final Collection<Long> locaisMarcados, final Long idLocal) {
        return locaisMarcados.contains(idLocal);
    }

    private Functions() {
    }

}
