package br.com.fanaticosporviagens.infra.converter;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javassist.util.proxy.ProxyFactory;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * Converte um conjunto de aliases retornados de uma consulta SQL em uma entidade.
 * 
 * @author André Thiago
 * 
 */
@Component
public class AliasToEntityConverter {

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Object convert(final Class<?> clazz, final String[] aliases, final Object[] values) throws InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException, NoSuchFieldException {
        if (aliases.length != values.length) {
            throw new IllegalArgumentException("A quantidade de aliases deve ser igual a quantidade de valores.");
        }
        if (clazz.isEnum() && ArrayUtils.contains(clazz.getInterfaces(), EnumTypeInteger.class)) {
            final Class<Enum> enumeration = (Class<Enum>) clazz;
            final Enum[] enumConstants = enumeration.getEnumConstants();

            final Integer codigo = values[0] instanceof BigInteger ? ((BigInteger) values[0]).intValue() : (Integer) values[0];
            for (final Enum enumConstant : enumConstants) {
                final EnumTypeInteger object = (EnumTypeInteger) enumConstant;
                if (object.getCodigo().equals(codigo)) {
                    return object;
                }
            }
            return null;
        } else {
            Object object = null;
            object = createObject(clazz);

            final Field[] fields = new Field[aliases.length];

            for (int i = 0; i < fields.length; i++) {
                if (aliases[i].contains(".")) {
                    initNestedProperty(clazz, aliases[i], object, values[i]);
                    continue;
                } else if (aliases[i].contains("_")) {
                    aliases[i] = removeUnderscores(aliases[i]);
                }
                /*if (value != null)({*/
                try {
                    final Class<?> propertyType = PropertyUtils.getPropertyType(object, aliases[i]);
                    final Object value = convertToCorrectType(values[i], propertyType);
                    PropertyUtils.setProperty(object, aliases[i], value);
                    // PropertyUtils.setNestedProperty(object, aliases[i], value);
                } catch (final NoSuchMethodException e) {
                    final DiscriminatorColumn annotation = AnnotationUtils.findAnnotation(clazz, DiscriminatorColumn.class);
                    if (annotation != null) {
                        if (AnnotationUtils.getValue(annotation, "name").toString().contains(aliases[i])) {
                            // ignora as colunas do tipo discriminator
                            continue;
                        }
                    }
                    throw e;
                }
                // }
            }

            return object;
        }
    }

    /**
     * Realiza a conversão (cast) de um objeto para o tipo desejado
     * Exemplo: Converter <tt>BigDecimal</tt> para <tt>Integer</tt>
     * 
     * @param value
     *            valor a ser convertido
     * @param type
     *            tipo para o qual será convertido
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Object convertToCorrectType(final Object value, final Class<?> type) throws InstantiationException, IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        if (value != null && type != null) {
            if (value instanceof String && type.equals(Boolean.class)) {
                if (((String) value).equalsIgnoreCase("true") || ((String) value).equalsIgnoreCase("false")) {
                    return Boolean.valueOf(value.toString());
                }
            } else if (value instanceof BigDecimal && !type.equals(BigDecimal.class)) {
                final BigDecimal bd = (BigDecimal) value;
                if (type.equals(Long.class)) {
                    return bd.longValue();
                } else if (type.equals(Integer.class)) {
                    return bd.intValue();
                } else if (type.equals(Double.class)) {
                    return bd.doubleValue();
                } else if (type.equals(Boolean.class)) {
                    return bd.equals(1);
                }
            } else if (value instanceof BigInteger) {
                final BigInteger bi = (BigInteger) value;
                if (type.equals(Long.class)) {
                    return bi.longValue();
                } else if (type.equals(Serializable.class)) {
                    return bi.longValue(); // para o caso da PK
                } else if (type.equals(Integer.class)) {
                    return bi.intValue();
                } else if (type.isAnnotationPresent(Entity.class)) {
                    final Object object = createObject(type);
                    PropertyUtils.setProperty(object, "id", value);
                    return object;
                } else if (type.isEnum() && ArrayUtils.contains(type.getInterfaces(), EnumTypeInteger.class)) {
                    final Integer codigo = bi.intValue();
                    final Class<Enum> enumeration = (Class<Enum>) type;
                    final Enum[] enumConstants = enumeration.getEnumConstants();
                    for (final Enum enumConstant : enumConstants) {
                        final EnumTypeInteger object = (EnumTypeInteger) enumConstant;
                        if (object.getCodigo().equals(codigo)) {
                            return object;
                        }
                    }
                }
            } else if (value instanceof Character && !type.equals(Character.class)) {
                final Character character = (Character) value;
                if (type.equals(String.class)) {
                    return character.toString();
                }
            } else if (value instanceof Date && !Date.class.isAssignableFrom(type)) {
                final Date date = (Date) value;
                if (LocalDate.class.isAssignableFrom(type)) {
                    // DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
                    return new LocalDate(date);
                }
                return new SimpleDateFormat("dd/MM/yyyy HH:mm", new Locale("pt", "BR")).format(date);
            } else if (value instanceof Integer && type.isEnum()) {
                final Integer codigo = (Integer) value;
                final Class<Enum> enumeration = (Class<Enum>) type;
                final Enum[] enumConstants = enumeration.getEnumConstants();
                for (final Enum enumConstant : enumConstants) {
                    final EnumTypeInteger object = (EnumTypeInteger) enumConstant;
                    if (object.getCodigo().equals(codigo)) {
                        return object;
                    }
                }
            }
        }
        return value;
    }

    private Object createObject(final Class<?> clazz) throws NoSuchMethodException, InstantiationException, IllegalAccessException,
            InvocationTargetException {
        Object object;
        if (Modifier.isAbstract(clazz.getModifiers())) {
            final ProxyFactory factory = new ProxyFactory();
            factory.setSuperclass(clazz);
            object = factory.create(null, null);
        } else {
            object = clazz.newInstance();
        }
        return object;
    }

    private Field getField(final Class<?> clazz, final String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (final NoSuchFieldException e) {
            final Class<?> superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw e;
            } else {
                return getField(superClass, fieldName);
            }
        }
    }

    private Method getMethod(final Class<?> clazz, final String methodName, final Class<?> parameter) {
        final Method method = MethodUtils.getAccessibleMethod(clazz, methodName, parameter);
        if (method == null) {
            final Class<?> superClass = clazz.getSuperclass();
            if (superClass == null) {
                throw new RuntimeException("Método " + methodName + " não encontrado.");
            }
            return getMethod(superClass, methodName, parameter);
        }
        return method;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void initNestedProperty(final Class<?> clazz, final String nestedProperty, final Object object, final Object value)
            throws IllegalAccessException, InvocationTargetException, NoSuchMethodException, NoSuchFieldException, InstantiationException {
        final String[] subAliases = nestedProperty.split("\\.");
        final Class<?> propertyClass = getField(clazz, subAliases[0]).getType();
        Object property = MethodUtils.getAccessibleMethod(clazz, "get" + StringUtils.capitalize(subAliases[0]), new Class[] {}).invoke(object);
        if (property == null) {
            if (propertyClass.isEnum() && ArrayUtils.contains(propertyClass.getInterfaces(), EnumTypeInteger.class)) {
                final Class<Enum> enumeration = (Class<Enum>) propertyClass;
                final Enum[] enumConstants = enumeration.getEnumConstants();

                final Integer codigo = value instanceof BigInteger ? ((BigInteger) value).intValue() : (Integer) value;
                EnumTypeInteger enumTypeInteger = null;
                for (final Enum enumConstant : enumConstants) {
                    enumTypeInteger = (EnumTypeInteger) enumConstant;
                    if (enumTypeInteger.getCodigo().equals(codigo)) {
                        break;
                    }
                }
                property = enumTypeInteger;
            } else {
                property = createObject(propertyClass);
            }
            final Method setterProperty = getMethod(clazz, "set" + StringUtils.capitalize(subAliases[0]), propertyClass);
            setterProperty.invoke(object, property);
        }
        if (subAliases.length == 2) {
            final Class<?> propertyType = PropertyUtils.getPropertyType(property, subAliases[1]);
            final Object correctValue = convertToCorrectType(value, propertyType);
            PropertyUtils.setProperty(property, subAliases[1], correctValue);
        } else {
            final String[] tokens = nestedProperty.split("\\.", 2);
            initNestedProperty(propertyClass, tokens[1], property, value);
        }
    }

    private String removeUnderscores(final String alias) {
        final String[] tokens = alias.split("\\_");
        String result = tokens[0];
        for (int i = 1; i < tokens.length; i++) {
            result += tokens[i].substring(0, 1).toUpperCase() + tokens[i].substring(1);
        }
        return result;
    }
}
