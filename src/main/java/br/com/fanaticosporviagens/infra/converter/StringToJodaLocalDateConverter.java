package br.com.fanaticosporviagens.infra.converter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Carlos Nascimento
 */
final class StringToJodaLocalDateConverter<S, T> implements Converter<String, LocalDate> {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");

    @Override
    public LocalDate convert(final String value) {
        try {
            if (value == null || value.isEmpty()) {
                return null;
            }
            return fmt.parseLocalDate(value);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Invalid date");
        }
    }
}
