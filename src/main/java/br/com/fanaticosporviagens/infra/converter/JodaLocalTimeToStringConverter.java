package br.com.fanaticosporviagens.infra.converter;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Carlos Nascimento
 */
final class JodaLocalTimeToStringConverter<S, T> implements Converter<LocalTime, String> {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

    @Override
    public String convert(final LocalTime value) {
        try {
            return value.toString(fmt);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Invalid date");
        }
    }
}
