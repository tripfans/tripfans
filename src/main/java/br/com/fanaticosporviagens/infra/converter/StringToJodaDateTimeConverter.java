package br.com.fanaticosporviagens.infra.converter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Carlos Nascimento
 */
final class StringToJodaDateTimeConverter<S, T> implements Converter<String, DateTime> {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");

    @Override
    public DateTime convert(final String value) {
        try {
            if (value == null || value.isEmpty()) {
                return null;
            }
            return fmt.parseDateTime(value);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Invalid date");
        }
    }
}
