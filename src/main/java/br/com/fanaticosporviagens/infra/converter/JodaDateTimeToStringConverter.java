package br.com.fanaticosporviagens.infra.converter;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Carlos Nascimento
 */
final class JodaDateTimeToStringConverter<S, T> implements Converter<LocalDate, String> {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");

    @Override
    public String convert(final LocalDate value) {
        try {
            return value.toString(fmt);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Invalid date");
        }
    }
}
