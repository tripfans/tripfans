package br.com.fanaticosporviagens.infra.converter;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;

/**
 * @author Carlos Nascimento
 */
final class StringToJodaLocalTimeConverter<S, T> implements Converter<String, LocalTime> {

    private static final DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm");

    @Override
    public LocalTime convert(final String value) {
        try {
            if (value == null || value.isEmpty()) {
                return null;
            }
            return fmt.parseLocalTime(value);
        } catch (final Exception e) {
            throw new IllegalArgumentException("Invalid date");
        }
    }
}
