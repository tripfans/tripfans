package br.com.fanaticosporviagens.infra.converter;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ognl.OgnlOps;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.orm.jpa.EntityManagerFactoryUtils;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.Local;

/**
 * Classe responsável por fazer a conversão de <tt>String</tt> para <tt>Entity</tt> ou vice-versa
 * 
 * @author Carlos Nascimento
 * 
 */
@Component
public class EntityConverter implements Converter<Object, Object>, GenericConverter {

    public EntityConverter() {
    }

    @Override
    public Object convert(final Object source) {
        return null;
    }

    /**
     * Realiza a conversão
     * 
     * @param source
     *            valor que será convertido
     * @param sourceType
     *            tipo do valor
     * @param targetType
     *            classe para qual o valor será convertido
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Object convert(final Object source, final TypeDescriptor sourceType, final TypeDescriptor targetType) {

        Object result = null;
        // Converter e String para Entity
        if (sourceType.getObjectType().equals(String.class)) {

            final String value = (String) source;

            if (value == null || value.length() == 0)
                return null;

            // Fazendo o cast para BaseEntity
            final Class<BaseEntity> entityClass = (Class<BaseEntity>) targetType.getType();

            // Recuperar EM
            final EntityManager entityManager = EntityManagerFactoryUtils.getTransactionalEntityManager(getEntityManagerFactory());

            if (ClassUtils.getAllInterfaces(entityClass).contains(UrlNameable.class) && !StringUtils.isNumeric(value)) {
                // a recuperação baseada na url única
                final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
                final CriteriaQuery<BaseEntity> criteria = criteriaBuilder.createQuery(entityClass);
                final Root<BaseEntity> root = criteria.from(entityClass);
                final TypedQuery<BaseEntity> query = entityManager.createQuery(criteria.select(root).where(
                        criteriaBuilder.equal(root.get("urlPath"), value)));

                final List<BaseEntity> resultList = query.getResultList();
                for (final BaseEntity baseEntity : resultList) {
                    return baseEntity;
                }
                if (entityClass.equals(Local.class)) {
                    final StringBuilder hql = new StringBuilder();
                    hql.append("select localUrlPathAntiga.local ");
                    hql.append("  from br.com.fanaticosporviagens.model.entity.LocalUrlPathAntiga localUrlPathAntiga ");
                    hql.append(" where localUrlPathAntiga.urlPath = :urlPath ");
                    hql.append(" order by localUrlPathAntiga.dataAlteracao desc ");

                    final Query queryLocalUrlPathAntiga = entityManager.createQuery(hql.toString());
                    queryLocalUrlPathAntiga.setParameter("urlPath", value);

                    final List<Local> listLocal = queryLocalUrlPathAntiga.getResultList();
                    for (final Local local : listLocal) {
                        return local;
                    }
                }
                return null;
            } else {
                // Recuperar os meta-dados
                final ClassMetadata metadata = getSessionFactory().getClassMetadata(entityClass);
                Class<?> idType = null;
                if (metadata != null) {
                    // Recupera o tipo do ID da entidade
                    idType = metadata.getIdentifierType().getReturnedClass();
                }

                try {
                    // Converter o valor submetido para o tipo do ID da entidade
                    final Serializable id = (Serializable) OgnlOps.convertValue(value, idType);
                    // Recuperar o objeto da base e retorná-lo como resultado da conversão
                    return entityManager.find(entityClass, id);
                    // Em caso de erro
                } catch (final Exception ex) {
                    // LOG.error("Erro ao converter String para tipo persistente", ex);
                    throw new ConversionFailedException(sourceType, targetType, value, ex);
                }
            }
        } else
        // converter de Entity para String
        if (targetType.getObjectType().equals(String.class)) {

            if (source == null)
                return "";

            final BaseEntity item = (BaseEntity) source;
            // Retorna o ID do objeto
            result = item.getId();
        }
        return result;
    }

    /**
     * Declara uma lista com os tipos de conversão suportados
     */
    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        final Set<ConvertiblePair> pairs = new HashSet<ConvertiblePair>();

        pairs.add(new ConvertiblePair(String.class, BaseEntity.class));
        pairs.add(new ConvertiblePair(BaseEntity.class, String.class));

        return pairs;
    }

    private EntityManagerFactory getEntityManagerFactory() {
        return SpringBeansProvider.getBean(EntityManagerFactory.class);
    }

    private SessionFactory getSessionFactory() {
        return SpringBeansProvider.getBean(SessionFactory.class);
    }
}
