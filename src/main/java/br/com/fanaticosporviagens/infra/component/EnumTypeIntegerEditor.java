package br.com.fanaticosporviagens.infra.component;

import java.beans.PropertyEditorSupport;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * Classe que deve ser utilizada para a conversão customizada de parâmetros de uma requisição em propriedades do tipo {@link EnumTypeInteger}.
 * 
 * @author André Thiago
 * 
 */
public class EnumTypeIntegerEditor extends PropertyEditorSupport {

    private final Class<?> clazz;

    public EnumTypeIntegerEditor(final Class<?> clazz) {
        if (!ArrayUtils.contains(clazz.getInterfaces(), EnumTypeInteger.class)) {
            throw new IllegalArgumentException("A Enumeration deve ser do tipo EnumTypeInteger");
        }
        this.clazz = clazz;
    }

    @Override
    public String getAsText() {
        return super.getAsText();
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        final Class<EnumTypeInteger> enumeration = (Class<EnumTypeInteger>) this.clazz;
        final Enum[] enumConstants = (Enum[]) enumeration.getEnumConstants();

        if (StringUtils.isNumeric(text)) {
            final Integer code = Integer.parseInt(text);
            for (final Enum enumConstant : enumConstants) {
                final EnumTypeInteger object = (EnumTypeInteger) enumConstant;
                if (object.getCodigo().equals(code)) {
                    setValue(object);
                    break;
                }
            }
        } else {
            for (final Enum enumConstant : enumConstants) {
                final EnumTypeInteger object = (EnumTypeInteger) enumConstant;
                if (object.getDescricao().equals(text)) {
                    setValue(object);
                    break;
                }
            }
        }
    }

}
