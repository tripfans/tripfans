package br.com.fanaticosporviagens.infra.component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalidadeEmbeddable;

public class BreadCrumb {

    private final List<BreadCrumbItem> items = new ArrayList<BreadCrumbItem>();

    public BreadCrumb(final Local local) {
        this(local.getLocalizacao());
    }

    public BreadCrumb(final LocalidadeEmbeddable localizacao) {
        if (localizacao != null) {
            addItem(localizacao.getPais()).addItem(localizacao.getEstado()).addItem(localizacao.getCidade())
                    .addItem(localizacao.getLocalInteresseTuristico()).addItem(localizacao.getLocalEnderecavel());
        }
    }

    private BreadCrumb addItem(final BreadCrumbItem item) {
        if (item != null) {
            this.items.add(item);
        }
        return this;
    }

    public List<BreadCrumbItem> getItems() {
        return Collections.unmodifiableList(this.items);
    }
}
