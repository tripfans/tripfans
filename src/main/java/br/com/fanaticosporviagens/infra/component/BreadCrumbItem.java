package br.com.fanaticosporviagens.infra.component;

public interface BreadCrumbItem {

    public String getNome();

    public String getUrl();
}
