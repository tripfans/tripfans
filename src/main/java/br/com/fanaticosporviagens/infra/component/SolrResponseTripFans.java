package br.com.fanaticosporviagens.infra.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;

import br.com.fanaticosporviagens.model.entity.CampoAgrupadorSolr;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SolrResponseTripFans {

    private final List<CampoAgrupadorSolr> agrupadores = new ArrayList<CampoAgrupadorSolr>();

    @JsonIgnore
    private List<FacetField> camposAgrupados = new ArrayList<FacetField>();

    @JsonIgnore
    private List<FacetField> datasAgrugadas = new ArrayList<FacetField>();

    @JsonIgnore
    private List<FacetField> intervalosAgrupados = new ArrayList<FacetField>();

    private List<EntidadeIndexada> resultados = new ArrayList<EntidadeIndexada>();

    private long total;

    public List<CampoAgrupadorSolr> getAgrupadores() {
        return this.agrupadores;
    }

    public List<FacetField> getCamposAgrupados() {
        return this.camposAgrupados;
    }

    public List<FacetField> getDatasAgrugadas() {
        return this.datasAgrugadas;
    }

    public List<FacetField> getIntervalosAgrupados() {
        return this.intervalosAgrupados;
    }

    public List<EntidadeIndexada> getResultados() {
        return this.resultados;
    }

    public long getTotal() {
        return this.total;
    }

    public void setCamposAgrupados(final List<FacetField> camposAgrupados) {
        this.camposAgrupados = camposAgrupados;
        if (CollectionUtils.isNotEmpty(camposAgrupados)) {
            for (final FacetField facet : camposAgrupados) {
                if (CollectionUtils.isNotEmpty(facet.getValues())) {
                    final CampoAgrupadorSolr agrupador = new CampoAgrupadorSolr(facet.getName());
                    for (final Count value : facet.getValues()) {
                        agrupador.adicionaValores(value.getName(), value.getCount());
                    }
                    this.agrupadores.add(agrupador);
                }
            }
        }
    }

    public void setDatasAgrugadas(final List<FacetField> datasAgrugadas) {
        this.datasAgrugadas = datasAgrugadas;
    }

    public void setIntervalosAgrupados(final List<FacetField> intervalosAgrupados) {
        this.intervalosAgrupados = intervalosAgrupados;
    }

    public void setResultados(final List<EntidadeIndexada> resultados) {
        this.resultados = resultados;
    }

    public void setTotal(final long total) {
        this.total = total;
    }

}