package br.com.fanaticosporviagens.infra.component;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable("searchData")
public class SearchData {
    public static final Integer DEFAULT_PAGE_SIZE = 16;

    private String defaultSort;

    private String dir;

    private boolean enableCount = true;

    private boolean enableIgnoreCase = true;

    private boolean enableLike = true;

    private Integer limit;

    private String sort;

    private Integer start;

    private Long total;

    public String getDefaultSort() {
        return this.defaultSort;
    }

    public String getDir() {
        return this.dir;
    }

    public boolean getMoreItens() {
        if (this.getTotal() != null) {
            return this.isPaginationDataValid() && (this.getTotal() > (this.getStart() + this.getLimit()));
        }
        return false;
    }

    public Integer getLimit() {
        if (this.limit == null) {
            this.limit = DEFAULT_PAGE_SIZE;
        }
        return this.limit;
    }

    public String getSort() {
        return this.sort;
    }

    public Integer getStart() {
        return this.start;
    }

    public Long getTotal() {
        return this.total;
    }

    public boolean isDefaultOrderDataValid() {
        return (this.getDefaultSort() != null && !this.getDefaultSort().equals(this.getSort()));
    }

    public boolean isEnableCount() {
        return this.enableCount;
    }

    public boolean isEnableIgnoreCase() {
        return this.enableIgnoreCase;
    }

    public boolean isEnableLike() {
        return this.enableLike;
    }

    public boolean isHasMoreItens() {
        if (this.getTotal() != null) {
            return this.isPaginationDataValid() && (this.getTotal() > (this.getStart() + this.getLimit()));
        }
        return false;
    }

    public boolean isOrderDataValid() {
        return ((this.getSort() != null || this.getDefaultSort() != null) && this.getDir() != null);
    }

    public boolean isPaginationDataValid() {
        return (this.getStart() != null && this.getLimit() != null);
    }

    /**
     * Checa se os parâmetros de paginação são válidos.
     * 
     * @return
     */
    public boolean isValid() {
        return (this.getStart() != null && this.getLimit() != null && this.getSort() != null && this.getDir() != null);
    }

    public void setDefaultSort(final String defaultSort) {
        this.defaultSort = defaultSort;
    }

    public void setDir(final String dir) {
        this.dir = dir;
    }

    public void setEnableCount(final boolean enableCount) {
        this.enableCount = enableCount;
    }

    public void setEnableIgnoreCase(final boolean enableIgnoreCase) {
        this.enableIgnoreCase = enableIgnoreCase;
    }

    public void setEnableLike(final boolean enableLike) {
        this.enableLike = enableLike;
    }

    public void setLimit(final Integer limit) {
        this.limit = limit;
    }

    public void setSort(final String sort) {
        this.sort = sort;
    }

    public void setStart(final Integer start) {
        this.start = start;
    }

    public void setTotal(final Long total) {
        this.total = total;
    }
}
