package br.com.fanaticosporviagens.infra.component;

public interface BreadCrumbGenerator {
    public BreadCrumb getBreadCrumb();
}
