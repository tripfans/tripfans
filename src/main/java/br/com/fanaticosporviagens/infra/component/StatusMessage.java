package br.com.fanaticosporviagens.infra.component;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Configurable;

import br.com.fanaticosporviagens.infra.controller.MessageType;

/**
 * @author André Thiago
 * 
 */
@Configurable("statusMessage")
public class StatusMessage implements Serializable {

    private static final long serialVersionUID = 7289736082667966462L;

    private String message;

    // Parametos adicionais para serem enviados na resposta JSON
    private final Map<String, Object> params = new HashMap<String, Object>();

    private boolean success = false;

    private String title;

    private MessageType type;

    public StatusMessage addParam(final String key, final Object value) {
        this.params.put(key, value);
        return this;
    }

    public StatusMessage error(final String message, final Object... args) {
        this.message = String.format(message, args);
        this.success = false;
        this.type = MessageType.ERROR;
        return this;
    }

    public String getImageName() {
        return this.type.getType();
    }

    public String getMessage() {
        return this.message;
    }

    public Map<String, Object> getParams() {
        return this.params;
    }

    public String getTitle() {
        return this.title != null ? this.title : this.type.getDefaultTitle();
    }

    public MessageType getType() {
        return this.type;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void reset() {
        this.message = null;
        this.title = null;
        this.success = false;
        this.type = null;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public StatusMessage success(final String message, final Object... args) {
        this.message = String.format(message, args);
        this.success = true;
        this.type = MessageType.SUCCESS;
        return this;
    }

    public StatusMessage warning(final String message, final Object... args) {
        this.message = String.format(message, args);
        this.success = true;
        this.type = MessageType.WARN;
        return this;
    }

}