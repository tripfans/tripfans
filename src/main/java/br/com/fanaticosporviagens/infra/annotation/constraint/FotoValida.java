package br.com.fanaticosporviagens.infra.annotation.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import br.com.fanaticosporviagens.infra.validator.FotoValidator;

@Documented
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FotoValidator.class)
public @interface FotoValida {

    Class<?>[] groups() default {};

    /**
     * O tamanho máximo, em MB (MegaBytes).
     * 
     * @return
     */
    int max();

    /**
     * A mensagem a ser exibida em caso de erro.
     * 
     * @return
     */
    String message() default "{validation.foto.invalid}";

    Class<? extends Payload>[] payload() default {};

}
