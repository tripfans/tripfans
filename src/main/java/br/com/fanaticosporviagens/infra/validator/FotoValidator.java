package br.com.fanaticosporviagens.infra.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.web.multipart.MultipartFile;

import br.com.fanaticosporviagens.infra.annotation.constraint.FotoValida;

/**
 * @author André Thiago
 * 
 */
public class FotoValidator implements ConstraintValidator<FotoValida, MultipartFile> {

    private int maxSize;

    @Override
    public void initialize(final FotoValida constraintAnnotation) {
        this.maxSize = constraintAnnotation.max();
    }

    @Override
    public boolean isValid(final MultipartFile value, final ConstraintValidatorContext context) {
        if (value != null && !value.isEmpty()) {
            final long sizeMB = value.getSize() / (1024 * 1024);
            if (sizeMB > this.maxSize) {
                return false;
            }
            if (!value.getContentType().contains("image")) {
                return false;
            }
        }
        return true;
    }
}
