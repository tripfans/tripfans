package br.com.fanaticosporviagens.infra.security;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequest;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Component
public class SecurityService {

    @Inject
    protected HttpSession httpSession;

    @Inject
    protected WebRequest request;

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public List<Long> getFriendsIds() {
        List<Long> listaIdsAmigos = (List<Long>) this.httpSession.getAttribute("IDS_AMIGOS");
        if (listaIdsAmigos == null) {
            listaIdsAmigos = new ArrayList<Long>();
        }
        return listaIdsAmigos;
    }

    public Usuario getUsuarioAutenticado() {
        final Usuario usuario = getUsuarioSession();
        if (usuario == null) {
            if (getAuthentication().getPrincipal() instanceof Usuario) {
                return (Usuario) getAuthentication().getPrincipal();
            } else if (getAuthentication().getDetails() instanceof Usuario) {
                return (Usuario) getAuthentication().getDetails();
            }
        }
        return usuario;
    }

    public boolean isAdmin() {
        if (isAuthenticated()) {
            final String username = getUsuarioAutenticado().getUsername();
            if (username.equals("tripfans@tripfans.com.br") || username.equals("carlinhoswake@gmail.com")
                    || username.equals("andrethiagos@gmail.com") || username.equals("gustavohopaiva@gmail.com")) {
                return true;
            }
        }
        return false;
    }

    public boolean isFriend(final Long idUsuario) {
        if (isAuthenticated()) {
            if (getUsuarioAutenticado().getId().equals(idUsuario)) {
                return true;
            }
            final List<Long> listaIdsAmigos = (List<Long>) this.httpSession.getAttribute("IDS_AMIGOS");
            if (listaIdsAmigos != null) {
                return listaIdsAmigos.contains(idUsuario);
            }
            return true;
        }
        return false;
    }

    public boolean isOwner(final Acao acao) {
        if (isAuthenticated() && acao.getAutor() != null) {
            return acao.getAutor().getId().equals(getUsuarioAutenticado().getId());
        }
        return false;
    }

    private Usuario getUsuarioSession() {
        return (Usuario) this.request.getAttribute("usuario", WebRequest.SCOPE_SESSION);
    }

    private boolean isAuthenticated() {
        return getUsuarioAutenticado() != null;
    }

}
