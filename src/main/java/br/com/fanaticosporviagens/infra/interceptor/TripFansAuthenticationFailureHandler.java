package br.com.fanaticosporviagens.infra.interceptor;

import java.io.IOException;
import java.io.Writer;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

@Component
public class TripFansAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private final Log logger = LogFactory.getLog(TripFansAuthenticationFailureHandler.class);;

    @Override
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException exception)
            throws IOException, ServletException {
        final HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);

        final Writer out = responseWrapper.getWriter();
        responseWrapper.setContentType("application/json");
        responseWrapper.setCharacterEncoding("UTF-8");

        if (this.logger.isDebugEnabled()) {
            exception.printStackTrace();
        }

        // out.write("{ \"success\": false, \"error\": \"" + getMessage("exception.badCredentials") + "\"}");

        out.write("{ \"success\": false, \"error\": \"" + getMessage("exception.badCredentials") + "\"}");
        out.close();
    }

    protected String getMessage(final String code) {
        final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);
        return messageSource.getMessage(code, null, Locale.getDefault());
    }

    protected void onUnsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
            final AuthenticationException failed) throws IOException {

        final HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);

        final Writer out = responseWrapper.getWriter();
        responseWrapper.setContentType("application/json");
        responseWrapper.setCharacterEncoding("UTF-8");

        if (failed instanceof BadCredentialsException) {
            out.write("{ \"success\": false, \"error\": \"" + getMessage("exception.badCredentials") + "\"}");
            out.close();
        }

    }
}
