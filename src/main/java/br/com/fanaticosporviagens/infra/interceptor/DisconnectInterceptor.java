package br.com.fanaticosporviagens.infra.interceptor;

import org.springframework.social.connect.ConnectionFactory;
import org.springframework.web.context.request.WebRequest;

/**
 * Listens for service provider disconnection events.
 * Allows for custom logic to be executed before and after connections are deleted with a specific service provider.
 * 
 * @author Craig Walls
 * @param <S>
 *            The service API hosted by the intercepted service provider.
 */
public interface DisconnectInterceptor<S> {

    /**
     * Called immediately after a connection is removed.
     * 
     * @param providerId
     *            the providerId for the connection(s) to be removed.
     * @param request
     *            the web request
     */
    void postDisconnect(ConnectionFactory<S> connectionFactory, WebRequest request);

    /**
     * Called immediately before a connection is removed.
     * 
     * @param providerId
     *            the providerId for the connection(s) to be removed.
     * @param request
     *            the web request
     */
    void preDisconnect(ConnectionFactory<S> connectionFactory, WebRequest request);

}
