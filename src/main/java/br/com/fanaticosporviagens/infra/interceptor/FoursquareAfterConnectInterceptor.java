package br.com.fanaticosporviagens.infra.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ConnectInterceptor;
import org.springframework.social.foursquare.api.Foursquare;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public class FoursquareAfterConnectInterceptor extends AbstractAfterConnectInterceptor implements ConnectInterceptor<Foursquare>,
        DisconnectInterceptor<Foursquare> {

    @Override
    public void postConnect(final Connection<Foursquare> connection, final WebRequest request) {
        final UsuarioService usuarioService = getUsuarioService();
        final Usuario usuario = usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        usuarioService.conectarProvedorExterno(usuario, connection);
        WebUtils.setSessionAttribute(((NativeWebRequest) request).getNativeRequest(HttpServletRequest.class), "usuario", usuario);
    }

    @Override
    public void postDisconnect(final ConnectionFactory<Foursquare> connectionFactory, final WebRequest request) {
        final UsuarioService usuarioService = getUsuarioService();
        final Usuario usuario = usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        usuarioService.desconectarProvedorExterno(usuario, connectionFactory.getProviderId());
        WebUtils.setSessionAttribute(((NativeWebRequest) request).getNativeRequest(HttpServletRequest.class), "usuario", usuario);
    }

    @Override
    public void preConnect(final ConnectionFactory<Foursquare> provider, final MultiValueMap<String, String> parameters, final WebRequest request) {
    }

    @Override
    public void preDisconnect(final ConnectionFactory<Foursquare> connectionFactory, final WebRequest request) {
    }
}
