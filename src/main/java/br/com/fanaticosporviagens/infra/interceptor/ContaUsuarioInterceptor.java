package br.com.fanaticosporviagens.infra.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.fanaticosporviagens.infra.spring.config.SignInUtils;
import br.com.fanaticosporviagens.infra.util.UserCookieGenerator;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;
import br.com.fanaticosporviagens.usuario.PosLoginService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

public class ContaUsuarioInterceptor extends HandlerInterceptorAdapter {

    private static Log logger = LogFactory.getLog(ContaUsuarioInterceptor.class);

    @Autowired
    private NotificacaoService notificacaoService;

    @Autowired
    private PosLoginService posLoginService;

    @Autowired
    private PersistentTokenBasedRememberMeServices rememberMeServices;

    private final UserCookieGenerator userCookieGenerator = new UserCookieGenerator();

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView)
            throws Exception {
        final Usuario usuario = getUsuario(request);

        if (requerComplemento(usuario, request)) {
            modelAndView.addObject("requererComplemento", "true");
            return;
        }

        if (usuario != null && modelAndView != null) {
            final Long notificacoesNaoVisualizadas = this.notificacaoService.consultarQuantidadeNotificacoesNaoVisualizadas(usuario);
            if (notificacoesNaoVisualizadas != 0L) {
                modelAndView.addObject("notificacoesNaoVisualizadas", notificacoesNaoVisualizadas);
            }
        }
    }

    /**
     * Intercepta a chamada antes de atingir o controller
     */
    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {
        try {
            final String requestURI = request.getRequestURI();
            logger.debug("Interceptando " + requestURI + "...");

            final Usuario usuario = getUsuario(request);

            rememberUser(request, response);
            handleSignOut(request, response);

            /*if (usuario != null && !requestURI.startsWith(request.getContextPath() + "/confirmacao") && !usuario.isCadastroCompleto()) {
                final Map<String, Object> model = new HashMap<String, Object>();
                new RedirectView("/confirmacaoBemVindo/" + usuario.getUrlPath(), true).render(model, request, response);
                return false;
            }*/
            return true;
        } catch (final Exception e) {
            e.printStackTrace();
            logger.info("request update failed");
            return false;
        }
    }

    private Usuario getUsuario(final HttpServletRequest request) {
        return (Usuario) request.getSession().getAttribute("usuario");
    }

    private void handleSignOut(final HttpServletRequest request, final HttpServletResponse response) {
        final Usuario usuario = getUsuario(request);
        if (usuario != null && request.getServletPath().startsWith("/signout")) {
            this.userCookieGenerator.removeCookie(response);
            request.getSession().removeAttribute("usuario");
            this.rememberMeServices.logout(request, response, SecurityContextHolder.getContext().getAuthentication());
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    private void rememberUser(final HttpServletRequest request, final HttpServletResponse response) {
        if (request.getSession().getAttribute("usuario") == null && !request.getServletPath().startsWith("/signout")) {
            final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null && authentication.getPrincipal() instanceof Usuario) {
                final Usuario usuario = (Usuario) authentication.getPrincipal();
                this.posLoginService.popularAtributosSessao(usuario);
                request.getSession().setAttribute("usuario", usuario);
            }
        }
    }

    /**
     * Checa se o usuário está confirmado o seu endereco de email e se é necessária a complementação do cadastro
     *
     * @param usuario
     * @param request
     * @return true se for necessária a complementação do cadastro
     */
    private boolean requerComplemento(final Usuario usuario, final HttpServletRequest request) {
        final String codigoConfirmacao = request.getParameter("codigoConfirmacao");
        if (codigoConfirmacao != null) {
            final String idUsuario = request.getParameter("usr");
            // TODO tratar as excecoes (id usuario nao existe, parseLong, etc)
            if (idUsuario != null) {
                final Long id = Long.parseLong(idUsuario);
                // Recuperar usuario da base
                final Usuario contaUsuario = this.usuarioService.consultarPorId(id);
                // Se o usuario nao completou o cadastro, requerer complemento
                if (contaUsuario.isConfirmado()) {
                    // TODO Enviar mensagem informando que o usuario ja foi confirmado
                } else {
                    this.usuarioService.autenticar(usuario.getUsername(), usuario.getPassword());
                    SignInUtils.signin(usuario, request);
                    return true;
                }
            } else {
                // TODO codigo de confirmacao invalido (tratar mensagem)
            }
        }
        return false;
    }

}
