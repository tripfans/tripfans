package br.com.fanaticosporviagens.infra.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ConnectInterceptor;
import org.springframework.social.google.api.Google;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.WebUtils;

import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public class GoogleAfterConnectInterceptor extends AbstractAfterConnectInterceptor implements ConnectInterceptor<Google>,
        DisconnectInterceptor<Google> {

    @Override
    public void postConnect(final Connection<Google> connection, final WebRequest request) {
        final UsuarioService usuarioService = getUsuarioService();
        final Usuario usuario = usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        usuarioService.conectarProvedorExterno(usuario, connection);
        WebUtils.setSessionAttribute(((NativeWebRequest) request).getNativeRequest(HttpServletRequest.class), "usuario", usuario);
    }

    @Override
    public void postDisconnect(final ConnectionFactory<Google> connectionFactory, final WebRequest request) {
        final UsuarioService usuarioService = getUsuarioService();
        final Usuario usuario = usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        usuarioService.desconectarProvedorExterno(usuario, connectionFactory.getProviderId());
        WebUtils.setSessionAttribute(((NativeWebRequest) request).getNativeRequest(HttpServletRequest.class), "usuario", usuario);
    }

    @Override
    public void preConnect(final ConnectionFactory<Google> provider, final MultiValueMap<String, String> parameters, final WebRequest request) {
        // Parametro que solicita ao Google o refreshtoken para acessar sem o usuario estar 'logado'
        parameters.set("access_type", "offline");
    }

    @Override
    public void preDisconnect(final ConnectionFactory<Google> connectionFactory, final WebRequest request) {
    }
}
