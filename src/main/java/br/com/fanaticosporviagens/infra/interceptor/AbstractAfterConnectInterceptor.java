package br.com.fanaticosporviagens.infra.interceptor;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public abstract class AbstractAfterConnectInterceptor {

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    protected Usuario getUsuarioAutenticado() {
        if (getAuthentication().getPrincipal() instanceof Usuario) {
            return (Usuario) getAuthentication().getPrincipal();
        } else if (getAuthentication().getDetails() instanceof Usuario) {
            // usuários de redes externas
            return (Usuario) getAuthentication().getDetails();
        }
        return null;
    }

    protected UsuarioService getUsuarioService() {
        return SpringBeansProvider.getBean(UsuarioService.class);
    }

}
