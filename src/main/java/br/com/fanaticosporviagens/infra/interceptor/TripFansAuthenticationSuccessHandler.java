package br.com.fanaticosporviagens.infra.interceptor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.infra.spring.config.SignInUtils;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;
import br.com.fanaticosporviagens.util.TripFansEnviroment;

@Component
public class TripFansAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private LocalService localService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private PersistentTokenBasedRememberMeServices rememberMeServices;

    @Autowired
    private TripFansEnviroment tripFansEnviroment;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) {
        SignInUtils.signin(authentication, request);// loginForm.isRememberMe());
        final HttpServletResponseWrapper responseWrapper = new HttpServletResponseWrapper(response);
        try {
            final Writer out = responseWrapper.getWriter();
            response.setContentType("application/json");
            responseWrapper.setCharacterEncoding("UTF-8");
            final Usuario usuario = (Usuario) authentication.getPrincipal();
            final String targetUrl = getTargetUrl(usuario, request, response);
            out.write("{\"success\" : true, \"targetUrl\" : \"" + targetUrl + "\", \"referer\" : \"" + request.getHeader("Referer") + "\"}");
            out.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    public String onProviderAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response,
            final Authentication authentication) {
        SignInUtils.signin(authentication, request);
        return getTargetUrl((Usuario) authentication.getDetails(), request, response);
    }

    private Map<String, String> getQueryMap(final String query) {
        final String[] params = query.split("&");
        final Map<String, String> map = new HashMap<String, String>();
        for (final String param : params) {
            try {
                final String name = param.split("=")[0];
                final String value = URLDecoder.decode(param.split("=")[1], "UTF-8");
                map.put(name, value);
            } catch (final UnsupportedEncodingException e) {
                break;
            }
        }
        return map;
    }

    private String getTargetUrl(final Usuario usuario, final HttpServletRequest request, final HttpServletResponse response) {
        String referer = request.getHeader("Referer");
        if (referer != null && referer.endsWith("/")) {
            referer = referer.substring(0, referer.length() - 1);
        }
        final boolean responderPedidoDica = referer != null && referer.contains("responderPedidoDica");
        String targetUrl = this.tripFansEnviroment.getServerUrl() + "/viagem/planejamento/inicio";
        if (responderPedidoDica) {
            final Map<String, String> queryMap = getQueryMap(referer.substring(referer.indexOf("?") + 1));
            final Long idUsuarioPediu = Long.parseLong(CriptografiaUtil.decrypt(queryMap.get("ucv")));
            final String urlPathLocal = queryMap.get("local");
            final Long idPedido = this.pedidoDicaService.consultarPorUsuarioPediuUrlPathLocal(idUsuarioPediu, usuario.getId(), urlPathLocal);
            targetUrl = "/dicas/pedidoDica/formRespostaPedidoDica/" + idPedido;
        } /*else if (usuario.naoCompletouCadastro()) {
            targetUrl = this.tripFansEnviroment.getServerUrl() + "/confirmacaoBemVindo/" + usuario.getUrlPath();
          }*/else {
            final SavedRequest savedRequest = new HttpSessionRequestCache().getRequest(request, response);
            if (savedRequest != null) {
                targetUrl = savedRequest.getRedirectUrl();
            } else {
                if (referer != null) {
                    if (!referer.contains("home") && !referer.equals(this.tripFansEnviroment.getServerUrl())) {
                        targetUrl = referer;
                    } else if (referer.contains("fb_source")) {
                        targetUrl = referer;
                    }
                }
            }
        }
        return targetUrl;
    }
}
