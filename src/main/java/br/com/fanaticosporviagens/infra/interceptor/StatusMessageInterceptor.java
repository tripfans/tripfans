package br.com.fanaticosporviagens.infra.interceptor;

import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;

import br.com.fanaticosporviagens.infra.component.StatusMessage;

/**
 * @author André Thiago
 * 
 */
public class StatusMessageInterceptor implements WebRequestInterceptor {

    private final StatusMessage statusMessage;

    public StatusMessageInterceptor(final StatusMessage statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Override
    public void afterCompletion(final WebRequest request, final Exception exception) throws Exception {
    }

    @Override
    public void postHandle(final WebRequest request, final ModelMap modelMap) throws Exception {
    }

    @Override
    public void preHandle(final WebRequest request) throws Exception {
        final String message = this.statusMessage.getMessage();
        if (message != null) {
            request.setAttribute("statusMessage", message, RequestAttributes.SCOPE_REQUEST);
            request.setAttribute("title", this.statusMessage.getTitle(), RequestAttributes.SCOPE_REQUEST);
            request.setAttribute("image", this.statusMessage.getImageName(), RequestAttributes.SCOPE_REQUEST);
            this.statusMessage.reset();
        }
    }

}
