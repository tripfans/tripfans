package br.com.fanaticosporviagens.infra.support;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class LocalLatLongAdjuster {

    private static final String GEO_REQUEST = "http://maps.google.com/maps/api/geocode/json?address=%s&sensor=false";

    private static final String SELECT_ATRACOES = "SELECT a.id_local, a.nome as \"nomeAtracao\", a.endereco, a.cep, c.nome as cidade, e.sigla"
            + "  FROM local a, local c, local e "
            + "  where a.local_type = 7 and a.id_local_cidade = c.id_local and c.id_local_estado = e.id_local and a.latitude is null and a.endereco is not null"
            + " order by id_local offset %d limit 50";

    private static final String SELECT_CIDADES = "select c.id_local, c.nome, e.sigla, p.nome as pais " + "from local c, local e, local p "
            + "where c.local_type = 4 and c.id_local_estado = e.id_local and e.id_local_pais = p.id_local and e.local_type = 3 "
            + "and p.local_type = 2 and c.latitude is null order by c.id_local offset %d limit 20";

    private static final String SELECT_HOTEIS = "SELECT h.id_local, h.nome as \"nomeHotel\", h.endereco, h.cep, c.nome as cidade, e.sigla"
            + "  FROM local h, local c, local e "
            + "  where h.local_type = 8 and h.id_local_cidade = c.id_local and c.id_local_estado = e.id_local and h.latitude is null and h.endereco is not null"
            + " order by id_local offset %d limit 20";

    private static final String UPDATE = "UPDATE local set latitude = %s, longitude = %s where id_local = %d";

    public static void main(final String[] args) throws Exception {
        final ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-indexer.xml");
        final JdbcTemplate jdbcTemplate = ctx.getBean(JdbcTemplate.class);

        List<Map<String, Object>> atracoes = jdbcTemplate.queryForList(String.format(SELECT_ATRACOES, new Object[] { 0 }));
        final int page = 1;
        final LocalLatLongAdjuster adjuster = new LocalLatLongAdjuster();
        do {
            final List<String> updates = new ArrayList<String>();
            int i = 0;
            for (final Map<String, Object> resultado : atracoes) {
                final Long id = Long.parseLong(resultado.get("id_local").toString());
                final String nomeAtracao = resultado.get("nomeAtracao").toString().trim();
                String endereco = resultado.get("endereco").toString().trim();
                String cep = "";
                if (resultado.get("cep") != null) {
                    cep = resultado.get("cep").toString().trim();
                }
                final String cidade = resultado.get("cidade").toString().trim();
                final String siglaEstado = resultado.get("sigla").toString().trim();
                // String pais = resultado.get("pais").toString().trim();

                // http://maps.google.com/maps/api/geocode/json?address=Templo+Budista+Jodoshu+Nippakuji+de+MaringáAv.+Londrina,+477Maringá,PR&sensor=false

                String address = nomeAtracao.replace(" ", "+");
                endereco = endereco.replace(" - ", ",").replace(", ", "+");
                address += "," + endereco.replace(" ", "+");
                if (StringUtils.isNotEmpty(cep)) {
                    address += "," + cep;
                }
                address += "," + cidade.replace(" ", "+");
                address += "," + siglaEstado.replace(" ", "+");
                // address += "," + pais.replace(" ", ",");

                final String urlSolicitacao = String.format(GEO_REQUEST, new Object[] { address });
                System.out.println("Url Solicitação = " + urlSolicitacao);

                final String[] coords = adjuster.getConteudo(urlSolicitacao);
                if (coords != null) {
                    updates.add(String.format(UPDATE, new Object[] { coords[0], coords[1], id }));
                    System.out.println(updates.get(i) + ";");
                    i++;
                }
            }
            if (!updates.isEmpty()) {
                final String[] sqlUpdates = updates.toArray(new String[0]);
                jdbcTemplate.batchUpdate(sqlUpdates);
            }
            atracoes = jdbcTemplate.queryForList(String.format(SELECT_ATRACOES, new Object[] { page * 50 }));

        } while (!atracoes.isEmpty());
    }

    private String[] getConteudo(final String urlGeo) throws MalformedURLException, IOException {
        final URL url = new URL(urlGeo);
        final HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        final BufferedReader content = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        final StringBuffer buffer = new StringBuffer();
        String line;
        while ((line = content.readLine()) != null) {
            buffer.append(line);
        }
        content.close();

        final int posLocation = buffer.indexOf("location");
        if (posLocation != -1) {
            final int posLat = buffer.indexOf("\"lat\" : ", posLocation);
            final int posLong = buffer.indexOf("\"lng\" : ", posLat);
            final int posFim = buffer.indexOf("}", posLong);
            final String latitude = buffer.substring(posLat, (posLong)).replace(',', ' ').trim();
            final String longitude = buffer.substring(posLong, posFim).trim();
            final String[] coords = new String[2];
            coords[0] = latitude.split(" : ")[1];
            coords[1] = longitude.split(" : ")[1];
            return coords;
        }

        return null;
    }

}