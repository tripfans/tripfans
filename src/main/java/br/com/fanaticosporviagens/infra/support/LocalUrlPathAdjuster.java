package br.com.fanaticosporviagens.infra.support;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import br.com.fanaticosporviagens.model.entity.LocalType;

/**
 * 
 * Classe utilitária para configurar a coluna url_path dos Locais.
 * 
 * @author André Thiago
 * 
 */
public class LocalUrlPathAdjuster {

    private static final String SELECT_COUNT_URL_PATH = "SELECT count(*) FROM local where local_type = :localType and url_path like (:urlPath || '%')";

    private static final String SELECT_LOCAL = "select l.id_local, l.nome, l.local_type, url_path_local_cidade as cidade, nm_local_estado "
            + "from local l where url_path is null order by l.id_local asc offset %d limit 100";

    private static final String UPDATE = "UPDATE local set url_path = '%s' where id_local = %d";

    public static void main(final String... args) {
        final long antes = System.currentTimeMillis();
        final ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-indexer.xml");
        final JdbcTemplate jdbcTemplate = ctx.getBean(JdbcTemplate.class);
        final NamedParameterJdbcTemplate namedParameterJdbcTemplate = ctx.getBean(NamedParameterJdbcTemplate.class);

        List<Map<String, Object>> locais = jdbcTemplate.queryForList(String.format(SELECT_LOCAL, new Object[] { 0 }));

        int page = 0;
        final LocalUrlPathAdjuster adjuster = new LocalUrlPathAdjuster();
        do {
            final List<String> updates = new ArrayList<String>();
            final int i = 0;
            for (final Map<String, Object> resultado : locais) {
                final Long id = Long.parseLong(resultado.get("id_local").toString());
                final String nome = resultado.get("nome").toString().trim();
                final Integer localType = Integer.parseInt(resultado.get("local_type").toString());
                String cidade = null;
                if (resultado.get("cidade") != null) {
                    cidade = resultado.get("cidade").toString().trim();
                }
                if (resultado.get("sigla") != null) {
                    final String siglaEstado = resultado.get("sigla").toString().trim();
                }
                boolean mustIncrement = false;
                String urlPath = adjuster.getUrlPath(nome);
                String update = String.format(UPDATE, new Object[] { urlPath, id });
                try {
                    jdbcTemplate.update(update);
                } catch (final DuplicateKeyException e) {
                    System.err.println("Já tem urlPath " + urlPath);
                    urlPath = adjuster.getUrlPathConcatenadoComTipo(urlPath, localType);
                    update = String.format(UPDATE, new Object[] { urlPath, id });
                    try {
                        jdbcTemplate.update(update);
                    } catch (final DuplicateKeyException e1) {
                        System.err.println("Já tem urlPath " + urlPath);
                        if (cidade != null) {
                            urlPath = adjuster.getUrlPathConcatenadoComCidade(urlPath, cidade);
                            try {
                                update = String.format(UPDATE, new Object[] { urlPath, id });
                                jdbcTemplate.update(update);
                            } catch (final DuplicateKeyException e2) {
                                System.err.println("Já tem urlPath " + urlPath);
                                mustIncrement = true;
                            }
                        } else {
                            mustIncrement = true;
                        }
                    }
                }
                if (mustIncrement) {
                    mustIncrement = false;
                    System.out.println("UrlPath para incrementar " + urlPath);
                    final Map<String, Object> params = new HashMap<String, Object>();
                    params.put("urlPath", urlPath);
                    params.put("localType", localType);
                    final long quantidadeUrlPathSemelhante = namedParameterJdbcTemplate.queryForLong(SELECT_COUNT_URL_PATH, params);
                    // jdbcTemplate.queryForLong(MessageFormat.format(SELECT_COUNT_URL_PATH, new Object[] { localType, urlPath }));
                    urlPath = adjuster.getUrlPathIncrementado(urlPath, quantidadeUrlPathSemelhante);
                    update = String.format(UPDATE, new Object[] { urlPath, id });
                    jdbcTemplate.update(update);
                }
            }
            /*if (!updates.isEmpty()) {
                final String[] sqlUpdates = updates.toArray(new String[0]);
                jdbcTemplate.batchUpdate(sqlUpdates);
            }*/
            page += 100;
            locais = jdbcTemplate.queryForList(String.format(SELECT_LOCAL, new Object[] { page }));
        } while (!locais.isEmpty());
        final long depois = System.currentTimeMillis();
        System.out.println("Finalizado em " + (depois - antes) / 1000 + "s");
    }

    public String removeAccents(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^\\p{ASCII}]", "");
        return str;
    }

    private String getUrlPath(final String nome) {
        String urlPath = removeAccents(nome).toLowerCase();
        urlPath = urlPath.replace("/", "").replace("(", "").replace(")", "").replace("&", "").replace("\'", "").replace(" - ", " ")
                .replace(" / ", " ").replace("...", " ").replace(",", " ").replace(".", " ");
        urlPath = urlPath.replaceAll(" ", "-").replace("--", "-");
        System.out.println(urlPath);
        return urlPath;
    }

    private String getUrlPathConcatenadoComCidade(final String urlPathAtual, final String cidade) {
        final String urlPath = urlPathAtual + "-" + cidade.toLowerCase();
        System.out.println(urlPath);
        return urlPath;
    }

    private String getUrlPathConcatenadoComTipo(final String urlPathAtual, final int localType) {
        final String urlPath = LocalType.getEnumPorCodigo(localType).getUrlPath() + "-" + urlPathAtual;
        System.out.println(urlPath);
        return urlPath;
    }

    private String getUrlPathIncrementado(final String urlPathAtual, Long quantidade) {
        quantidade++;
        final String urlPath = urlPathAtual + "-" + quantidade;
        System.out.println(urlPath);
        return urlPath;
    }

}
