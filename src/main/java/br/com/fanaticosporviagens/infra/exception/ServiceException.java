package br.com.fanaticosporviagens.infra.exception;

public class ServiceException extends BaseRuntimeException {

    private static final long serialVersionUID = -4600227667581426521L;

    public ServiceException(final String message) {
        super(message);
    }

}
