package br.com.fanaticosporviagens.infra.exception;

public class NotFoundException extends BaseRuntimeException {

    private static final long serialVersionUID = -4600227667581426521L;

    public NotFoundException(final String message) {
        super(message);
    }

}
