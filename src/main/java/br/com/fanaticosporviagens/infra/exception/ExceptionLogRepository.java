package br.com.fanaticosporviagens.infra.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;

@Repository
public class ExceptionLogRepository extends GenericCRUDRepository<ExceptionLog, Long> {

    @Autowired
    protected GenericSearchRepository genericSearchRepository;

}
