package br.com.fanaticosporviagens.infra.exception;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

public class BaseCheckedException extends Exception {

    protected final static MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    private static final long serialVersionUID = -3571981977546875514L;

    public BaseCheckedException(final String message) {
        super(message);
    }

}
