package br.com.fanaticosporviagens.infra.exception;

import java.util.Locale;

public class UsernameAlreadyInUseException extends ServiceException {

    private static final long serialVersionUID = 7184649312407880937L;

    public UsernameAlreadyInUseException(final String username) {
        // TODO mudar para default Locale
        super(messageSource.getMessage("exception.usernameAlreadyInUse", new Object[] { username }, new Locale("pt", "BR")));
    }
}