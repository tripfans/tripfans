package br.com.fanaticosporviagens.infra.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.model.entity.Usuario;

@Service
public class ExceptionLogService {

    private static final Log logger = LogFactory.getLog(ExceptionLogService.class);

    @Autowired
    private ExceptionLogRepository exceptionLogRepository;

    @Transactional
    public ExceptionLog registerException(final Throwable exception, final HttpServletRequest request, final Usuario usuario) {
        final ExceptionLog log = new ExceptionLog(request, exception, usuario);

        registerExceptionInConsole(log);

        registerExceptionLogInDatabase(log);

        return log;
    }

    public ExceptionLog registerExceptionInConsole(final ExceptionLog log) {

        if (logger.isDebugEnabled()) {
            logger.debug("-------------------------------------------------------------------------------------------------------------"
                    + "\nBEGIN LOG DEBUG\n" + log + "\nEND LOG DEBUG"
                    + "\n-------------------------------------------------------------------------------------------------------------");
        }

        return log;
    }

    @Transactional
    public void registerExceptionLogInDatabase(final ExceptionLog log) {
        this.exceptionLogRepository.incluir(log);
    }

}
