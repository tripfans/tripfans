package br.com.fanaticosporviagens.infra.exception;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_log_exception")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_log_exception", allocationSize = 1)
@Table(name = "log_exception")
public class ExceptionLog extends BaseEntity<Long> {

    private static final long serialVersionUID = -6432678754653371115L;

    @Column(name = "dt_ocorrencia", nullable = false, updatable = false)
    private Date dataOcorrencia;

    @Column(name = "dd_ocorrencia", nullable = false, updatable = false)
    private Date diaOcorrencia;

    @Column(name = "tx_exception_cause_message", nullable = true, updatable = false, length = 2000)
    private String exceptionCauseMessage;

    @Column(name = "nm_exception_cause", nullable = true, updatable = false, length = 2000)
    private String exceptionCauseName;

    @Column(name = "tx_exception_message", nullable = true, updatable = false, length = 4000)
    private String exceptionMessage;

    @Column(name = "nm_exception", nullable = false, updatable = false, length = 2000)
    private String exceptionName;

    @Column(name = "tx_exception_stack_trace", nullable = false, updatable = false, columnDefinition = "text not null")
    private String exceptionStackTrace;

    @Column(name = "ed_request_action_url", nullable = true, updatable = false, length = 2000)
    private String requestActionUrl;

    @Column(name = "tx_request_parameters", nullable = true, updatable = false, columnDefinition = "text")
    private String requestParameters;

    @Column(name = "nm_request_remote_user", nullable = true, updatable = false, length = 2000)
    private String requestRemoteUser;

    @Column(name = "ed_request_url", nullable = true, updatable = false, length = 2000)
    private String requestUrl;

    @Column(name = "nm_request_user_principal", nullable = true, updatable = false, length = 2000)
    private String requestUserPrincipalName;

    @Column(name = "id_usuario", nullable = true, updatable = false, length = 2000)
    private Long usuarioId;

    public ExceptionLog() {
    }

    public ExceptionLog(final HttpServletRequest request, final Throwable exception, final Usuario usuario) {
        popularDados(request, exception, usuario);
    }

    public Date getDataOcorrencia() {
        return this.dataOcorrencia;
    }

    public Date getDiaOcorrencia() {
        return this.diaOcorrencia;
    }

    public String getExceptionCauseMessage() {
        return this.exceptionCauseMessage;
    }

    public String getExceptionCauseName() {
        return this.exceptionCauseName;
    }

    public String getExceptionMessage() {
        return this.exceptionMessage;
    }

    public String getExceptionName() {
        return this.exceptionName;
    }

    public String getExceptionStackTrace() {
        return this.exceptionStackTrace;
    }

    public String getRequestActionUrl() {
        return this.requestActionUrl;
    }

    public String getRequestParameters() {
        return this.requestParameters;
    }

    public String getRequestRemoteUser() {
        return this.requestRemoteUser;
    }

    public String getRequestUrl() {
        return this.requestUrl;
    }

    public String getRequestUserPrincipalName() {
        return this.requestUserPrincipalName;
    }

    public Long getUsuarioId() {
        return this.usuarioId;
    }

    public void popularDados(final HttpServletRequest request, final Throwable exception, final Usuario usuario) {

        setDataOcorrencia(new Date());

        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            setDiaOcorrencia(sdf.parse(sdf.format(new Date())));
        } catch (final ParseException e) {
            throw new RuntimeException("Erro ao popular o dia do ExceptionLog", e);
        }

        setExceptionName(exception.getClass().getName());
        setExceptionMessage(exception.getMessage());
        setExceptionStackTrace(ExceptionUtils.getFullStackTrace(exception));

        final Throwable cause = ExceptionUtils.getRootCause(exception);
        if (cause != null) {
            setExceptionCauseName(cause.getClass().getName());
            setExceptionCauseMessage(cause.getMessage());
        }

        final String servletPath = request.getServletPath();
        setRequestUrl(servletPath);
        if (servletPath.lastIndexOf("/") > 0) {
            setRequestActionUrl(servletPath.substring(0, servletPath.lastIndexOf("/")));
        }

        @SuppressWarnings("unchecked")
        final Enumeration<String> parameterNames = request.getParameterNames();
        final StringBuilder parameters = new StringBuilder();
        while (parameterNames.hasMoreElements()) {
            final String parameterName = parameterNames.nextElement();
            parameters.append("\n[" + parameterName + "][");
            String separador = "";
            for (final String parameter : request.getParameterValues(parameterName)) {
                parameters.append(separador + parameter);
                separador = ",";
            }
            parameters.append("]");
        }
        setRequestParameters(parameters.toString().trim());

        if (request.getUserPrincipal() != null) {
            setRequestUserPrincipalName(request.getUserPrincipal().getName());
        }

        setRequestRemoteUser(request.getRemoteUser());

        if (usuario != null) {
            setUsuarioId(usuario.getId());
        }

    }

    public void setDataOcorrencia(final Date dataOcorrencia) {
        this.dataOcorrencia = dataOcorrencia;
    }

    public void setDiaOcorrencia(final Date diaOcorrencia) {
        this.diaOcorrencia = diaOcorrencia;
    }

    public void setExceptionCauseMessage(final String exceptionCauseMessage) {
        this.exceptionCauseMessage = exceptionCauseMessage;
    }

    public void setExceptionCauseName(final String exceptionCauseName) {
        this.exceptionCauseName = exceptionCauseName;
    }

    public void setExceptionMessage(final String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public void setExceptionName(final String exceptionName) {
        this.exceptionName = exceptionName;
    }

    public void setExceptionStackTrace(final String exceptionStackTrace) {
        this.exceptionStackTrace = exceptionStackTrace;
    }

    public void setRequestActionUrl(final String requestActionUrl) {
        this.requestActionUrl = requestActionUrl;
    }

    public void setRequestParameters(final String requestParameters) {
        this.requestParameters = requestParameters;
    }

    public void setRequestRemoteUser(final String requestRemoteUser) {
        this.requestRemoteUser = requestRemoteUser;
    }

    public void setRequestUrl(final String url) {
        this.requestUrl = url;
    }

    public void setRequestUserPrincipalName(final String requestUserPrincipalName) {
        this.requestUserPrincipalName = requestUserPrincipalName;
    }

    public void setUsuarioId(final Long usuarioId) {
        this.usuarioId = usuarioId;
    }

    @Override
    public String toString() {
        final StringBuilder toStringFormatado = new StringBuilder();

        toStringFormatado.append("\n\t - dataOcorrencia = [" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.dataOcorrencia) + "]");
        toStringFormatado.append("\n\t - diaOcorrencia = [" + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(this.diaOcorrencia) + "]");

        toStringFormatado.append("\n\t - requestUrl = [" + this.requestUrl + "]");
        toStringFormatado.append("\n\t - requestActionUrl = [" + this.requestActionUrl + "]");
        toStringFormatado.append("\n\t - requestParameters = [\n" + this.requestParameters + "\n]");
        toStringFormatado.append("\n\t - requestRemoteUser = [\n" + this.requestRemoteUser + "\n]");
        toStringFormatado.append("\n\t - requestUserPrincipalName = [\n" + this.requestUserPrincipalName + "\n]");

        toStringFormatado.append("\n\t - exceptionName = [" + this.exceptionName + "]");
        toStringFormatado.append("\n\t - exceptionMessage = [" + this.exceptionMessage + "]");

        toStringFormatado.append("\n\t - exceptionCauseName = [" + this.exceptionCauseName + "]");
        toStringFormatado.append("\n\t - exceptionCauseMessage = [" + this.exceptionCauseMessage + "]");
        toStringFormatado.append("\n\t - exceptionStackTrace = [\n" + this.exceptionStackTrace + "\n]");

        toStringFormatado.append("\n\t - usuarioId = [\n" + this.usuarioId + "\n]");

        return toStringFormatado.toString();
    }

}
