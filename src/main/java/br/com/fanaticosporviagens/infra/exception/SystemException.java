package br.com.fanaticosporviagens.infra.exception;

/**
 * Exceção para erros de sistema, que não devem ser apresentados para o usuário.
 * 
 * @author André Thiago
 * 
 */
public class SystemException extends BaseRuntimeException {

    private static final long serialVersionUID = -2184088292300927249L;

    public SystemException(final String message) {
        super(message);
    }

    public SystemException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
