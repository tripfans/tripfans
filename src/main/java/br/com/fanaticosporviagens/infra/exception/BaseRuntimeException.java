package br.com.fanaticosporviagens.infra.exception;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

public class BaseRuntimeException extends RuntimeException {

    protected static final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    private static final long serialVersionUID = -646665802115335551L;

    public BaseRuntimeException(final String message) {
        super(message);
    }

    public BaseRuntimeException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
