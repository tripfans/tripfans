package br.com.fanaticosporviagens.infra.exception;

public class BusinessException extends BaseRuntimeException {

    private static final long serialVersionUID = -3727435149535219279L;

    public BusinessException(final String message) {
        super(message);
    }

    public BusinessException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

}
