package br.com.fanaticosporviagens.infra.listener;

import java.util.Arrays;
import java.util.List;

import org.hibernate.event.spi.PostInsertEvent;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.persister.entity.EntityPersister;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;
import br.com.fanaticosporviagens.model.entity.CategoriasPesquisa;
import br.com.fanaticosporviagens.model.entity.Indexavel;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;

/**
 * @author André Thiago
 *
 */
public class IndexableListener implements org.hibernate.event.spi.PostInsertEventListener, org.hibernate.event.spi.PostUpdateEventListener {

    private static final long serialVersionUID = -2653720451944950191L;

    private static List<String> urlsFotosAnonimas = Arrays.asList(new String[] {
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_ALBUM,
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_BIG,
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_SMALL,
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_ALBUM,
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_BIG,
            System.getProperty(AppEnviromentConstants.APPLICATION_CONTEXT) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_SMALL });

    private boolean isUpdateOnFoto(final String dirty) {
        return dirty.contains("Foto") || urlsFotosAnonimas.contains(dirty); // dirty.contains("perfil/blank_");
    }

    /**
     * Determina se a indexação deve ser feita no update. Para isso, checa se o nome ou o urlPath foram modificados.
     *
     * @param event
     * @return
     */
    private boolean mustIndexOnUpdate(final PostUpdateEvent event) {
        boolean mustIndex = false;
        final String nome = ((Indexavel) event.getEntity()).getNome();
        final String urlPath = ((Indexavel) event.getEntity()).getUrlPath();
        final int[] dirtyProperties = event.getDirtyProperties();
        for (int i = 0; i < dirtyProperties.length; i++) {
            if (event.getState()[dirtyProperties[i]] == null) {
                continue;
            }
            final String dirty = event.getState()[dirtyProperties[i]].toString();
            if (dirty.equals(nome) || dirty.equals(urlPath) || isUpdateOnFoto(dirty)) {
                mustIndex = true;
                break;
            }
        }
        return mustIndex;
    }

    @Override
    public void onPostInsert(final PostInsertEvent event) {
        if (event.getEntity() instanceof Indexavel
                && !((Indexavel) event.getEntity()).getTipoEntidade().equals(CategoriasPesquisa.PESSOAS.getCodigo())
                && ((Indexavel) event.getEntity()).getNome() != null) {
            SpringBeansProvider.getBean(PesquisaTextualService.class).indexar((Indexavel) event.getEntity());
        }
    }

    @Override
    public void onPostUpdate(final PostUpdateEvent event) {
        if (event.getEntity() instanceof Indexavel && mustIndexOnUpdate(event)) {
            SpringBeansProvider.getBean(PesquisaTextualService.class).indexar((Indexavel) event.getEntity());
        }
    }

    @Override
    public boolean requiresPostCommitHanding(final EntityPersister persister) {
        return false;
    }
}
