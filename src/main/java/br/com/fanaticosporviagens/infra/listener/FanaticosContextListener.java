package br.com.fanaticosporviagens.infra.listener;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryLoader;
import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryProcessor;

/**
 * Classe para inicializacao/configuracao de alguns recursos importantes de infraestrutura
 * durante a inicializacao do Servidor WEB
 *
 * @author Carlos Nascimento
 */
public class FanaticosContextListener implements ServletContextListener {

    private ServletContextEvent servletContextEvent;

    private void configureXMLQueriesMap() {
        // Cria o mapa que contem todas as consultas armazenadas em arquivos XML
        // Essas consultas podem ser localizadas/utilizadas a partir
        // do metodo getQueryXml(Strign) da classe QueryXmlManager
        XMLQueryProcessor.getInstance().setQueriesXmlMap(XMLQueryLoader.createXMLQueriesMap());
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextInitialized(final ServletContextEvent servletContextEvent) {
        this.servletContextEvent = servletContextEvent;
        configureXMLQueriesMap();
    }

    /**
     * Recupera o caminho real de um arquivo
     *
     * @param file
     *            nome do arquivo
     */
    private String getRealPath(final String file) {
        if (this.servletContextEvent.getServletContext() != null) {
            return this.servletContextEvent.getServletContext().getRealPath(file);
        } else {
            return new File("").getAbsolutePath() + File.separator + "src" + File.separator + "main" + File.separator + "webapp" + File.separator
                    + file;
        }
    }

}
