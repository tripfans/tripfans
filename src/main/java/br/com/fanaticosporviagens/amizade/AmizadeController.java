package br.com.fanaticosporviagens.amizade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * @author André Thiago
 *
 */
@Controller
@PreAuthorize("isAuthenticated()")
public class AmizadeController extends BaseController {

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private AmizadeService service;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/aceitarConviteAmizade/{usuario}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public StatusMessage aceitarConviteAmizade(@PathVariable("usuario") final Usuario usuarioQueConvidou) {
        this.service.aceitar(usuarioQueConvidou);
        return this.success(String.format("Agora você e %s são amigos.", usuarioQueConvidou.getDisplayName()));
    }

    @RequestMapping(value = "/amigos/consultarAmigos", method = RequestMethod.GET)
    @ResponseBody
    public JSONResponse consultarAmigos(@RequestParam("term") final String nomeAmigo) {
        return new JSONResponse(this.service.consultarAmizades(this.getUsuarioAutenticado(), nomeAmigo), this.searchData.getTotal(), this.jsonFields);
    }

    @RequestMapping(value = "/amigos/consultarInteressesLocal/{local}", method = RequestMethod.GET)
    public String consultarInteressesLocal(@PathVariable final Local local, @RequestParam final TipoInteresseUsuarioEmLocal tipoInteresse,
            final Model model) {
        final InteresseAmigosEmLocal interesseAmigosEmLocal = this.interesseUsuarioEmLocalService.consultarInteresseTodosAmigosEmLocal(
                this.getUsuarioAutenticado(), local, tipoInteresse);

        final PopoverCard popoverCard = new PopoverCard(interesseAmigosEmLocal);
        model.addAttribute("popoverCard", popoverCard);

        return "popoverCard";
    }

    @RequestMapping(value = "/convidarParaAmizade/{usuario}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public StatusMessage convidarParaAmizade(@PathVariable("usuario") final Usuario usuario) {
        // TODO melhor forma de recuperar o usuário autenticado?
        this.conviteService.convidarParaAmizade(this.usuarioService.consultarPorId(this.getUsuarioAutenticado().getId()), usuario);

        // Exemplo de mensagem (retirado do facebook): Uma solicitação de amizade foi enviada ao seu contato. Quando seu contato confirmar sua
        // amizade, vocês poderão ver o perfil um do outro.
        // O usuário irá receber sua solicitação de amizade em breve.

        return this.success("Seu convite de amizade foi enviado com sucesso.");
    }

    @RequestMapping(value = "/desfazerAmizadeSomenteFacebook/{idFacebook}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public JSONResponse desfazerAmizadeSomenteFacebook(@PathVariable("idFacebook") final String idFacebook) {
        this.service.desfazerAmizadeSomenteFacebook(this.getUsuarioAutenticado(), idFacebook);
        this.jsonResponse.setSuccess(true);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/desfazerAmizadeTripFans/{amigo}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public JSONResponse desfazerAmizadeTripFans(@PathVariable("amigo") final Usuario amigo) {
        this.service.desfazerAmizadeTripFans(this.getUsuarioAutenticado(), amigo);
        this.jsonResponse.setSuccess(true);
        return this.jsonResponse;
    }

}
