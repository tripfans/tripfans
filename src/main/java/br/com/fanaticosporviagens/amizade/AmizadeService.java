package br.com.fanaticosporviagens.amizade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.publicacao.PublicacaoService;

/**
 * @author André Thiago
 *
 */
@Service
public class AmizadeService extends GenericCRUDService<Amizade, Long> {

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private PublicacaoService publicacaoService;

    @Transactional
    public void aceitar(final Usuario usuarioQueConvidou) {
        final Convite convite = this.conviteService.consultarConviteAmizade(usuarioQueConvidou.getId(), this.getUsuarioAutenticado().getId());
        this.conviteService.aceitar(convite);
        this.incluirAmizade(convite.getRemetente(), convite.getConvidado());
    }

    public Amizade consultarAmigo(final Long idUsuario, final String idAmigoFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", idUsuario);
        parametros.put("idAmigoFacebook", idAmigoFacebook);
        final List<Amizade> amizades = this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmigoPorIdFacebook", parametros, Amizade.class);
        if (amizades != null && !amizades.isEmpty()) {
            return amizades.get(0);
        }
        return null;
    }

    public List<Usuario> consultarAmigos(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmigos", parametros, Usuario.class);
    }

    public Amizade consultarAmizade(final Long idUsuario, final Long idOutro) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", idUsuario);
        parametros.put("outro", idOutro);
        final List<Amizade> amizades = this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizade", parametros, Amizade.class);
        if (amizades != null && !amizades.isEmpty()) {
            return amizades.get(0);
        }
        return null;
    }

    public Amizade consultarAmizadePorEmailAmigo(final Long idUsuario, final String emailAmigo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", idUsuario);
        parametros.put("emailAmigo", emailAmigo);
        final List<Amizade> amizades = this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizade", parametros, Amizade.class);
        if (amizades != null && !amizades.isEmpty()) {
            return amizades.get(0);
        }
        return null;
    }

    public Amizade consultarAmizadePorIdFacebookAmigo(final Long idUsuario, final String idAmigoFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", idUsuario);
        parametros.put("idAmigoFacebook", idAmigoFacebook);
        final List<Amizade> amizades = this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizade", parametros, Amizade.class);
        if (amizades != null && !amizades.isEmpty()) {
            return amizades.get(0);
        }
        return null;
    }

    public List<Amizade> consultarAmizades(final Usuario usuario) {
        return this.consultarAmizades(usuario, null, null);
    }

    public List<Amizade> consultarAmizades(final Usuario usuario, final Boolean importadaFacebook, final String nomeAmigo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        if (importadaFacebook != null) {
            parametros.put("importadaFacebook", importadaFacebook);
        }
        if (nomeAmigo != null) {
            parametros.put("nome", nomeAmigo);
        }
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizades", parametros, Amizade.class);
    }

    public List<Amizade> consultarAmizades(final Usuario usuario, final String nomeAmigo) {
        return this.consultarAmizades(usuario, null, nomeAmigo);
    }

    public List<Amizade> consultarAmizadesAtivasRecentemente(final Usuario usuario, final Integer tamanhoAmostra) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizadesAtivasRecentemente", parametros, tamanhoAmostra, Amizade.class);
    }

    public List<Amizade> consultarAmizadesFacebook(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        parametros.put("somenteFacebook", true);
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizades", parametros, Amizade.class);
    }

    public List<Amizade> consultarAmizadesTripfans(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        parametros.put("somenteTripfans", true);
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarAmizades", parametros, Amizade.class);
    }

    public List<AmizadeView> consultarAmizadesVerificandoConvitesTripfans(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        return this.searchRepository.consultarPorNamedNativeQuery("Amizade.consultarAmizadesVerificandoConvitesTripfans", parametros,
                AmizadeView.class);
    }

    /**
     * Realiza uma consulta para verificar quais dos emails informados como parametros, já possuem amizade com o usuario informado.
     */
    public List<String> consultarEmailsQuePossuemAmizade(final List<String> listaEmails, final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("listaEmails", listaEmails);
        parametros.put("idUsuario", idUsuario);
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarEmailsQuePossuemAmizade", parametros, String.class);
    }

    @Transactional
    public List<Long> consultarIdsAmigos(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarIdsAmigos", parametros, Long.class);
    }

    public List<String> consultarNomesAmigosPorIdsFacebook(final List<String> listaIdsFacebookAmigos, final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("listaIdsFacebook", listaIdsFacebookAmigos);
        parametros.put("idUsuario", idUsuario);
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarNomesAmigosPorIdsFacebook", parametros, String.class);
    }

    public List<Amizade> consultarTodasAmizades(final Usuario usuario) {
        return null;
    }

    /**
     * Realiza uma consulta com ids informados como parametros e retorna os usernames que estao na base.
     */
    public List<String> consultarUsernamesFacebookAmigosPorIdsFacebook(final List<String> listaIdsFacebook, final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("listaIdsFacebook", listaIdsFacebook);
        parametros.put("idUsuario", idUsuario);
        return this.searchRepository.consultarPorNamedQuery("Amizade.consultarUsernamesFacebookAmigosPorIdsFacebook", parametros, String.class);
    }

    @Transactional
    public void desfazerAmizadeSomenteFacebook(final Usuario usuarioAutenticado, final String idAmigoFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuarioAutenticado);
        parametros.put("idAmigoFacebook", idAmigoFacebook);
        this.genericCRUDRepository.alterarPorNamedQuery("Amizade.desfazerAmizadeSomenteFacebook", parametros);
    }

    @Transactional
    public void desfazerAmizadeTripFans(final Usuario usuario, final Usuario amigo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        parametros.put("amigo", amigo);
        this.genericCRUDRepository.alterarPorNamedQuery("Amizade.desfazerAmizadeTripFans", parametros);
    }

    @Transactional
    public void incluirAmizade(final Usuario usuario, final Usuario amigo) {
        final Amizade amizadeUsuarioAmigo = new Amizade(usuario, amigo);
        final Amizade amizadeAmigoUsuario = new Amizade(amigo, usuario);
        super.incluir(amizadeUsuarioAmigo);
        super.incluir(amizadeAmigoUsuario);
        // TODO criar um interceptor do Hibernate que crie as notificações quando necessário
        // this.publicacaoService.publicar(amizadeUsuarioAmigo);
    }

    public boolean saoAmigos(final Long idUsuario, final Long idOutro) {
        return this.consultarAmizade(idUsuario, idOutro) != null;
    }

    public boolean saoAmigos(final Long idUsuario, final String emailOutroUsuario) {
        return this.consultarAmizadePorEmailAmigo(idUsuario, emailOutroUsuario) != null;
    }

    public boolean saoAmigos(final Usuario usuario, final Usuario outro) {
        return this.saoAmigos(usuario.getId(), outro.getId());
    }

    public boolean saoAmigosNoFacebook(final Long idUsuario, final String idFacebookOutro) {
        return this.consultarAmizadePorIdFacebookAmigo(idUsuario, idFacebookOutro) != null;
    }

}
