package br.com.fanaticosporviagens.amizade;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.Amizade;

public class AmizadeView {

    public static List<AmizadeView> filtrarAmizadesSomenteFacebook(final List<AmizadeView> listaAmizades) {
        final List<AmizadeView> listaFiltrada = new ArrayList<AmizadeView>();
        if (listaAmizades != null) {
            for (final AmizadeView amizadeView : listaAmizades) {
                if (amizadeView.getAmizade() != null && amizadeView.getAmizade().isSomenteFacebook()) {
                    listaFiltrada.add(amizadeView);
                }
            }
        }
        return listaFiltrada;
    }

    public static List<AmizadeView> filtrarAmizadesSomenteTripFans(final List<AmizadeView> listaAmizades) {
        final List<AmizadeView> listaFiltrada = new ArrayList<AmizadeView>();
        if (listaAmizades != null) {
            for (final AmizadeView amizadeView : listaAmizades) {
                if (amizadeView.getAmizade() != null && amizadeView.getAmizade().isTripfans()) {
                    listaFiltrada.add(amizadeView);
                }
            }
        }
        return listaFiltrada;
    }

    private Amizade amizade;

    /**
     * Indica se o amigo ja foi convidado para o tripfans
     */
    private boolean convidadoParaTripfans;

    public Amizade getAmizade() {
        return this.amizade;
    }

    public boolean isConvidadoParaTripfans() {
        return this.convidadoParaTripfans;
    }

    public void setAmizade(final Amizade amizade) {
        this.amizade = amizade;
    }

    public void setConvidadoParaTripfans(final boolean convidadoParaTripfans) {
        this.convidadoParaTripfans = convidadoParaTripfans;
    }

}
