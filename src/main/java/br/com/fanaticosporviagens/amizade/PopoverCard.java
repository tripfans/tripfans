package br.com.fanaticosporviagens.amizade;

import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;

public class PopoverCard {

    private Long quantidadeAmigos;

    private Long quantidadeTotal;

    private String subTitulo;

    private String titulo;

    public PopoverCard() {

    }

    public PopoverCard(final InteresseAmigosEmLocal interesseAmigosEmLocal) {
        if (interesseAmigosEmLocal != null) {
            this.quantidadeAmigos = interesseAmigosEmLocal.getQuantidadeAmigos();
            this.titulo = interesseAmigosEmLocal.getLocalDeInteresse().getNome();
            // subTitulo = interesseAmigosEmLocal.getTitulo()
        }
    }

    public Long getQuantidadeAmigos() {
        return this.quantidadeAmigos;
    }

    public Long getQuantidadeTotal() {
        return this.quantidadeTotal;
    }

    public String getSubTitulo() {
        return this.subTitulo;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setQuantidadeAmigos(final Long quantidadeAmigos) {
        this.quantidadeAmigos = quantidadeAmigos;
    }

    public void setQuantidadeTotal(final Long quantidadeTotal) {
        this.quantidadeTotal = quantidadeTotal;
    }

    public void setSubTitulo(final String subTitulo) {
        this.subTitulo = subTitulo;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }
}
