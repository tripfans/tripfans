package br.com.fanaticosporviagens.facebook.service;

import javax.inject.Inject;
import javax.inject.Provider;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author Carlos Nascimento
 */
@Service
public abstract class BaseProviderService extends BaseService {

    @Inject
    private Provider<ConnectionRepository> connectionRepositoryProvider;

    public ConnectionRepository getConnectionRepository(final Usuario usuario) {
        return getConnectionRepository(usuario.getUsername());
    }

    protected <T> Connection<T> getConnection(final Class<T> provider) {
        ConnectionRepository connectionRepository = null;
        connectionRepository = getConnectionRepository();
        return connectionRepository.findPrimaryConnection(provider);
    }

    protected <T> Connection<T> getConnection(final Usuario usuario, final Class<T> provider) {
        ConnectionRepository connectionRepository = null;
        if (usuario != null) {
            connectionRepository = getConnectionRepository(usuario);
        } else {
            connectionRepository = getConnectionRepository();
        }
        return connectionRepository.findPrimaryConnection(provider);
    }

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private ConnectionRepository getConnectionRepository() {
        return getConnectionRepository(getAuthentication().getName());
    }

    private ConnectionRepository getConnectionRepository(final String username) {
        final UsersConnectionRepository usersConnectionRepository = SpringBeansProvider.getBean(UsersConnectionRepository.class);
        if (usersConnectionRepository != null && username != null) {
            return usersConnectionRepository.createConnectionRepository(username);
        }
        return null;
    }

}
