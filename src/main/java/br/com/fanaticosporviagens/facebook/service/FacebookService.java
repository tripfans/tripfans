package br.com.fanaticosporviagens.facebook.service;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Album;
import org.springframework.social.facebook.api.Checkin;
import org.springframework.social.facebook.api.CheckinPost;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookLink;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.FeedOperations;
import org.springframework.social.facebook.api.FriendOperations;
import org.springframework.social.facebook.api.GraphApi;
import org.springframework.social.facebook.api.MediaOperations;
import org.springframework.social.facebook.api.Photo;
import org.springframework.social.facebook.api.Post.PostType;
import org.springframework.social.facebook.api.Reference;
import org.springframework.social.facebook.api.UserOperations;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.api.impl.json.FacebookModule;
import org.springframework.social.support.URIBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.com.fanaticosporviagens.foto.AlbumService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.util.TripFansShareableLinkBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Carlos Nascimento
 */
@Service
public class FacebookService extends BaseProviderService {

    public class LocationOperations {

        private ObjectMapper objectMapper;

        private Usuario usuario;

        public LocationOperations() {
        }

        public LocationOperations(final Usuario usuario) {
            this.usuario = usuario;
        }

        private <T> List<T> deserializeList(final JsonNode dataNode, final String postType, final Class<T> type) {
            final List<T> posts = new ArrayList<T>();
            for (final Iterator<JsonNode> iterator = dataNode.iterator(); iterator.hasNext();) {
                posts.add(deserializePost(postType, type, (ObjectNode) iterator.next()));
            }
            return posts;
        }

        private <T> T deserializePost(String postType, final Class<T> type, final ObjectNode node) {
            try {
                if (postType == null) {
                    postType = determinePostType(node);
                }
                // Must have separate postType field for polymorphic deserialization. If we key off of the "type" field, then it will
                // be null when trying to deserialize the type property.
                node.put("postType", postType); // used for polymorphic deserialization
                node.put("type", postType); // used to set Post's type property
                this.objectMapper = new ObjectMapper();
                this.objectMapper.registerModule(new FacebookModule());

                return this.objectMapper.readValue(node.binaryValue(), type);
            } catch (final IOException shouldntHappen) {
                shouldntHappen.printStackTrace();
                throw new RuntimeException("Error deserializing " + postType + " post", shouldntHappen);
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        private String determinePostType(final ObjectNode node) {
            if (node.has("type")) {
                try {
                    final String type = node.get("type").textValue();
                    PostType.valueOf(type.toUpperCase());
                    return type;
                } catch (final IllegalArgumentException e) {
                    return "post";
                }
            }
            return "post";
        }

        private JsonNode fetchConnectionList(final String baseUri, final int offset, final int limit) {
            final URI uri = URIBuilder.fromUri(baseUri).queryParam("offset", String.valueOf(offset)).queryParam("limit", String.valueOf(limit))
                    .build();

            final Facebook facebook = getFacebook(this.usuario);

            final JsonNode responseNode = facebook.restOperations().getForObject(uri, JsonNode.class);
            return responseNode;
        }

        public <T> List<T> fetchConnections(final String objectId, final String connectionType, final Class<T> type,
                MultiValueMap<String, String> queryParameters) {

            if (queryParameters == null) {
                queryParameters = new LinkedMultiValueMap<String, String>();
            }

            // Colocando uma data limite para trazer somente registros que tenham sido criados apos esta data
            // Formato da data : yyyy-MM-dd
            if (this.usuario.getDataUltimaVerificacaoCheckins() != null) {
                final String dataFormatada = this.usuario.getDataUltimaVerificacaoCheckins().toString("yyyy-MM-dd");
                queryParameters.add("since", dataFormatada);
            }

            final String connectionPath = connectionType != null && connectionType.length() > 0 ? "/" + connectionType : "";
            final URIBuilder uriBuilder = URIBuilder.fromUri(GraphApi.GRAPH_API_URL + objectId + connectionPath).queryParams(queryParameters);

            JsonNode jsonNode = null;
            JsonNode dataNode = null;
            JsonNode pagingNode = null;
            String nextURI = uriBuilder.build().toString();
            // HACK: o Set abaixo é um workaround de um bug da api do Facebook que, em determinados casos,
            // retorna sempre a mesma uri, ficando em um loop infinito
            final Set<String> uris = new HashSet<String>();
            uris.add(nextURI);
            final List<T> listResult = new ArrayList<T>();
            List<T> listTemp = null;
            final int offset = 1;
            final int limit = 25;
            try {
                while (true) {
                    if (FacebookService.this.log.isDebugEnabled()) {
                        FacebookService.this.log.debug(nextURI);
                    }
                    jsonNode = fetchConnectionList(nextURI, offset, limit);

                    dataNode = jsonNode.get("data");

                    System.out.println("objectId" + objectId);
                    System.out.println("SIZE" + dataNode.size());

                    pagingNode = jsonNode.get("paging");
                    if (pagingNode != null) {
                        nextURI = pagingNode.get("next").asText();
                        // HACK: o if abaixo é um outro workaround de um bug da api do Facebook que não
                        // inclui na Url "next" da paginacao o parametro "since"
                        if (queryParameters.get("since") != null && !nextURI.contains("since")) {
                            // forçando a inclusao do parametro "since"
                            nextURI += "&since=" + queryParameters.get("since").get(0);
                        }
                        if (uris.contains(nextURI)) {
                            if (FacebookService.this.log.isDebugEnabled()) {
                                FacebookService.this.log.debug(String.format("Url %s já retornada anteriormente. Vai parar.", nextURI));
                            }
                            break;
                        } else {
                            uris.add(nextURI);
                        }
                    }
                    listTemp = deserializeList(dataNode, "checkin", type);
                    if (listTemp == null || listTemp.isEmpty()) {
                        break;
                    }
                    listResult.addAll(listTemp);
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }

            // JsonNode dataNode = getFacebook().restOperations().getForObject(uriBuilder.build(), JsonNode.class);
            return listResult;
        }

        public <T> List<T> fetchConnections(final Usuario usuario, final String connectionType, final Class<T> type,
                MultiValueMap<String, String> queryParameters) {
            if (queryParameters == null) {
                queryParameters = new LinkedMultiValueMap<String, String>();
            }
            return this.fetchConnections(getUserProfileId(usuario), connectionType, type, queryParameters);
        }

        protected MappingJackson2HttpMessageConverter getJsonMessageConverter() {
            final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
            this.objectMapper = new ObjectMapper();
            this.objectMapper.registerModule(new FacebookModule());
            converter.setObjectMapper(this.objectMapper);
            return converter;
        }
    }

    @Autowired
    private AlbumService albumService;

    @Autowired
    private FotoService fotoService;

    private final Log log = LogFactory.getLog(FacebookService.class);

    @Autowired
    private TripFansShareableLinkBuilder shareableLinkBuilder;

    public Album consultarAlbumUsuario(final Usuario usuario, final String idAlbum) {
        return getMediaOperations(usuario).getAlbum(idAlbum);
    }

    public List<Album> consultarAlbunsUsuario(final Usuario usuario) {
        return getMediaOperations(usuario).getAlbums();
    }

    public List<Reference> consultarAmigosUsuario(final Usuario usuario) {
        return getFriendOperations(usuario).getFriends();
    }

    public List<CheckinPost> consultarCheckinsAmigo(final String idAmigoFacebook) {
        return consultarCheckinsAmigo(null, idAmigoFacebook);
    }

    public List<CheckinPost> consultarCheckinsAmigo(final Usuario usuario, final String idAmigoFacebook) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Consultando checkins amigo " + idAmigoFacebook);
        }
        final List<CheckinPost> checkins = getLocationOperations(usuario).fetchConnections(idAmigoFacebook, "locations", CheckinPost.class, null);
        return removerCheckinsPlacesDuplicados(checkins);
    }

    public List<CheckinPost> consultarCheckinsUsuario() {
        return consultarCheckinsUsuario(null);
    }

    public List<CheckinPost> consultarCheckinsUsuario(final Usuario usuario) {
        /*final List<Checkin> checkins = new ArrayList<Checkin>();
        checkins.addAll(this.getFacebook().placesOperations().getCheckins(1, 1000));
        checkins.addAll(consultarCheckinsUsuarioEmFotos(getUserProfileId()));
        return checkins;*/
        final List<CheckinPost> checkins = getLocationOperations(usuario).fetchConnections(usuario, "locations", CheckinPost.class, null);
        if (this.log.isDebugEnabled()) {
            this.log.debug("Retornou com os checkins do usuário " + usuario.getUsername());
        }
        return removerCheckinsPlacesDuplicados(checkins);
    }

    private List<Checkin> consultarCheckinsUsuarioEmFotos(final String userProfileId) {

        final List<Checkin> checkins = new ArrayList<Checkin>();

        // Varres as fotos do usuario
        // TODO ver a questao da paginacao, pois se não informar a paginacao o FB só retorna as 25 primeiras fotos
        // tentando pegar as fotos com o maximo de 1000
        for (final Photo photo : this.getFacebook().mediaOperations().getPhotos(userProfileId, 1, 1000)) {

            // Pegar o local (Checkin) da foto a partir o ID da foto
            final Checkin checkin = this.getFacebook().placesOperations().getCheckin(photo.getId());

            if (checkin != null) {
                checkins.add(checkin);
            }

        }
        return checkins;
    }

    public List<Photo> consultarFotosAlbumUsuario(final Usuario usuario, final String idAlbum) {
        return getMediaOperations(usuario).getPhotos(idAlbum);
    }

    public FacebookProfile consultarPerfilUsuario(final String idUsuarioFabebook) {
        return getUserOperations().getUserProfile(idUsuarioFabebook);
    }

    public List<FacebookProfile> consultarProfileAmigosUsuario(final Usuario usuario) {
        final List<FacebookProfile> profiles = new ArrayList<FacebookProfile>();
        List<FacebookProfile> profilesTemp = new ArrayList<FacebookProfile>();
        int offset = 0;
        // Recuperar profiles de 100 em 100 (maximo permitido pela API)
        while (!(profilesTemp = getFriendOperations(usuario).getFriendProfiles(offset, 100)).isEmpty()) {
            profiles.addAll(profilesTemp);
            offset += 100;
        }
        return profiles;
    }

    public void enviarRequisicaoAplicativo(final String userId, final String message) {
        final MultiValueMap<String, Object> requestData = new LinkedMultiValueMap<String, Object>();
        requestData.set("message", "Sending a request through to you...");
        try {
            getFacebook().publish(userId, "apprequests", requestData);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private List<Photo> filtrarFotosFacebookSelecionadas(final List<Photo> fotos, final List<String> idsFotosFacebook) {

        List<Photo> fotosSelecionadas = new ArrayList<Photo>();
        // se a lista de idsFotos não for vazia, filtrar as fotos
        if (idsFotosFacebook != null && !idsFotosFacebook.isEmpty()) {
            for (final Photo foto : fotos) {
                if (idsFotosFacebook.contains(foto.getId())) {
                    fotosSelecionadas.add(foto);
                }
            }
        } else {
            // se a lista de idsFotos for vazia, importar o album todo
            fotosSelecionadas = fotos;
        }
        return fotosSelecionadas;
    }

    protected Facebook getFacebook() {
        final Connection<Facebook> facebook = getConnection(Facebook.class);
        // TODO ver como resolver quando expirar o token
        if (facebook != null && facebook.hasExpired()) {
            // facebook.refresh();
        }
        return facebook != null ? facebook.getApi() : new FacebookTemplate();
    }

    protected Facebook getFacebook(final Usuario usuario) {
        final Connection<Facebook> facebook = getConnection(usuario, Facebook.class);
        // TODO ver como resolver quando expirar o token
        if (facebook != null && facebook.hasExpired()) {
            // facebook.refresh();
        }
        return facebook != null ? facebook.getApi() : new FacebookTemplate();
    }

    private FeedOperations getFeedOperations() {
        return this.getFacebook().feedOperations();
    }

    private FriendOperations getFriendOperations() {
        return this.getFacebook().friendOperations();
    }

    private FriendOperations getFriendOperations(final Usuario usuario) {
        return this.getFacebook(usuario).friendOperations();
    }

    private LocationOperations getLocationOperations() {
        return new LocationOperations();
    }

    private LocationOperations getLocationOperations(final Usuario usuario) {
        return new LocationOperations(usuario);
    }

    private MediaOperations getMediaOperations(final Usuario usuario) {
        return this.getFacebook(usuario).mediaOperations();
    }

    private UserOperations getUserOperations() {
        return this.getFacebook().userOperations();
    }

    private String getUserProfileId(final Usuario usuario) {
        return this.getFacebook(usuario).userOperations().getUserProfile().getId();
    }

    public void importarAlbumUsuario(final Usuario usuario, final String idAlbumFacebook) {
        final Album albumFacebook = this.consultarAlbumUsuario(usuario, idAlbumFacebook);
        final String idFacebookFotoCapa = albumFacebook.getCoverPhotoId();
        final br.com.fanaticosporviagens.model.entity.Album album = this.albumService.importarAlbumFacebook(albumFacebook);
        importarFotosAlbumUsuario(usuario, album, idAlbumFacebook, null, idFacebookFotoCapa);
    }

    public void importarFotosAlbumUsuario(final Usuario usuario, final AtividadePlano atividadePlano, final String idAlbumFacebook,
            final List<String> idsFotosFacebook) {
        final List<Photo> fotos = consultarFotosAlbumUsuario(usuario, idAlbumFacebook);
        this.fotoService.importarFotosAlbumFacebook(atividadePlano, filtrarFotosFacebookSelecionadas(fotos, idsFotosFacebook));
    }

    public void importarFotosAlbumUsuario(final Usuario usuario, final br.com.fanaticosporviagens.model.entity.Album album,
            final String idAlbumFacebook, final List<String> idsFotosFacebook) {
        this.importarFotosAlbumUsuario(usuario, album, idAlbumFacebook, idsFotosFacebook, null);
    }

    private void importarFotosAlbumUsuario(final Usuario usuario, final br.com.fanaticosporviagens.model.entity.Album album,
            final String idAlbumFacebook, final List<String> idsFotosFacebook, final String idFacebookFotoCapa) {
        final List<Photo> fotos = consultarFotosAlbumUsuario(usuario, idAlbumFacebook);

        this.fotoService.importarFotosAlbumFacebook(album, filtrarFotosFacebookSelecionadas(fotos, idsFotosFacebook), idFacebookFotoCapa);
    }

    protected boolean isConnected(final Usuario usuario) {
        return this.getConnection(usuario, Facebook.class) != null;
    }

    public void postarLinkNoMuralDeAmigo(final String idFacebookAmigo, final String urlLink, final String titulo, final String subTitulo,
            final String mensagem) {
        getFeedOperations().postLink(idFacebookAmigo, "", new FacebookLink(urlLink, titulo, subTitulo, mensagem));
    }

    public void postarNoMural(final SocialShareableItem shareable) {
        final String postLink = this.shareableLinkBuilder.getLink(shareable);
        final String postTitle = shareable.getFacebookPostTitle();
        final String postText = shareable.getFacebookPostText();
        final FacebookLink link = new FacebookLink(postLink, postTitle, postLink, postText);
        final String feedId = getFacebook().feedOperations().postLink(postTitle, link);
        if (this.log.isDebugEnabled()) {
            this.log.debug("Objeto compartilhado com sucesso. Id do feed: " + feedId);
        }

    }

    public void postarNoMural(final String message) {
        getFeedOperations().updateStatus(message);
    }

    public void postarNoMural(final String facebookUserId, final String message) {
        // getFeedOperations().post(facebookUserId, message);
    }

    private List<CheckinPost> removerCheckinsPlacesDuplicados(final List<CheckinPost> checkins) {
        // Removendo os 'places' repetidos
        final Map<String, CheckinPost> checkinsMap = new HashMap<String, CheckinPost>();
        for (final CheckinPost checkinPost : checkins) {
            if (!checkinsMap.containsKey(checkinPost.getPlace().getId())) {
                checkinsMap.put(checkinPost.getPlace().getId(), checkinPost);
            }
        }
        return new ArrayList<CheckinPost>(checkinsMap.values());
    }

}
