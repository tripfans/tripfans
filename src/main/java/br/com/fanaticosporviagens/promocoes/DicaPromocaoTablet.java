package br.com.fanaticosporviagens.promocoes;

public class DicaPromocaoTablet extends ItemPromocaoTablet {

    private Integer quantidadeDicas;

    public void atualizar(final DicaPromocaoTablet dicaPromocao) {
        super.soma(dicaPromocao.getPontos());
        this.quantidadeDicas += dicaPromocao.getQuantidadeDicas();
    }

    public Integer getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public void setQuantidadeDicas(final Integer quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

}
