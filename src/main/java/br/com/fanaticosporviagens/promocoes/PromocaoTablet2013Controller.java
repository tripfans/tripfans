package br.com.fanaticosporviagens.promocoes;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fanaticosporviagens.infra.controller.BaseController;

@Controller
public class PromocaoTablet2013Controller extends BaseController {

    @Autowired
    private PromocaoTablet2013Service service;

    @RequestMapping(value = "/promocaoTablet")
    public String promocaoTablet(final Model model) {
        final List<UsuarioPromocaoTabletPojo> usuarios = this.service.consultarUsuariosPromocao();
        if (getUsuarioAutenticado() != null
                && !CollectionUtils.exists(usuarios, new ItemPromocaoTabletPredicado("idUsuario", getUsuarioAutenticado().getId()))) {
            // adiciona o usuário atual ao final da lista
            final UsuarioPromocaoTabletPojo usuarioAtual = this.service.getUsuarioPojo(getUsuarioAutenticado());
            model.addAttribute("usuarioAtual", usuarioAtual);
        }
        model.addAttribute("usuarios", usuarios);
        return "/promocaoTablet/promocaoTablet";
    }

}
