package br.com.fanaticosporviagens.promocoes;

public class ConvitePromocaoTablet extends ItemPromocaoTablet {

    private Integer quantidadeConvites;

    public void atualizar(final ConvitePromocaoTablet convitePromocao) {
        super.soma(convitePromocao.getPontos());
        this.quantidadeConvites += convitePromocao.getQuantidadeConvites();
    }

    public Integer getQuantidadeConvites() {
        return this.quantidadeConvites;
    }

    public void setQuantidadeConvites(final Integer quantidadeConvites) {
        this.quantidadeConvites = quantidadeConvites;
    }

}
