package br.com.fanaticosporviagens.promocoes;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.Predicate;

public class ItemPromocaoTabletPredicado implements Predicate {

    private final Object expected;

    private final String propriedade;

    public ItemPromocaoTabletPredicado(final String propriedade, final Object expected) {
        super();
        this.propriedade = propriedade;
        this.expected = expected;
    }

    @Override
    public boolean evaluate(final Object object) {
        try {
            return this.expected.equals(PropertyUtils.getProperty(object, this.propriedade));
        } catch (final Exception e) {
            return false;
        }
    }

}
