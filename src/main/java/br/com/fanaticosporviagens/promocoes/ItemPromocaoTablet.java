package br.com.fanaticosporviagens.promocoes;

public abstract class ItemPromocaoTablet implements Comparable<ItemPromocaoTablet> {

    private Long idUsuario;

    private String nomeUsuario;

    private Integer pontos = 0;

    private String urlPathUsuario;

    public ItemPromocaoTablet() {
        super();
    }

    @Override
    public int compareTo(final ItemPromocaoTablet outra) {
        if (this.getPontos() < outra.getPontos()) {
            return 1;
        } else if (this.getPontos() > outra.getPontos()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(final Object outro) {
        if (this == outro)
            return true;
        if (outro == null)
            return false;
        if (getClass() != outro.getClass())
            return false;
        final ItemPromocaoTablet other = (ItemPromocaoTablet) outro;
        if (this.idUsuario == null) {
            if (other.idUsuario != null)
                return false;
        } else if (!this.idUsuario.equals(other.idUsuario))
            return false;
        return true;
    }

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    public Integer getPontos() {
        return this.pontos;
    }

    public String getUrlPathUsuario() {
        return this.urlPathUsuario;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.idUsuario == null) ? 0 : this.idUsuario.hashCode());
        return result;
    }

    public void setIdUsuario(final Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setNomeUsuario(final String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public void setPontos(final Integer pontos) {
        this.pontos = pontos;
    }

    public void setUrlPathUsuario(final String urlPathUsuario) {
        this.urlPathUsuario = urlPathUsuario;
    }

    public void soma(final Integer pontos) {
        this.pontos += pontos;
    }

}