package br.com.fanaticosporviagens.promocoes;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

public class UsuarioPromocaoTabletPojo implements Comparable<UsuarioPromocaoTabletPojo> {

    private Long idUsuario;

    private String nome;

    private Integer pontuacao = 0;

    private Integer quantidadeAvaliacoes = 0;

    private Integer quantidadeConvitesAceitos = 0;

    private Integer quantidadeDicas = 0;

    private String urlPath;

    public UsuarioPromocaoTabletPojo() {
    }

    public UsuarioPromocaoTabletPojo(final AvaliacaoPromocaoTablet avaliacao) {
        this.idUsuario = avaliacao.getIdUsuario();
        this.nome = avaliacao.getNomeUsuario();
        this.urlPath = avaliacao.getUrlPathUsuario();
        this.quantidadeAvaliacoes = avaliacao.getQuantidadeAvaliacoes();
        soma(avaliacao.getPontos());

    }

    public UsuarioPromocaoTabletPojo(final ConvitePromocaoTablet convite) {
        this.idUsuario = convite.getIdUsuario();
        this.nome = convite.getNomeUsuario();
        this.urlPath = convite.getUrlPathUsuario();
        this.quantidadeConvitesAceitos = convite.getQuantidadeConvites();
        soma(convite.getPontos());
    }

    public UsuarioPromocaoTabletPojo(final DicaPromocaoTablet dica) {
        this.idUsuario = dica.getIdUsuario();
        this.nome = dica.getNomeUsuario();
        this.urlPath = dica.getUrlPathUsuario();
        this.quantidadeDicas = dica.getQuantidadeDicas();
        soma(dica.getPontos());
    }

    public void adicionaAvaliacoes(final List<AvaliacaoPromocaoTablet> avaliacoes) {
        if (CollectionUtils.isNotEmpty(avaliacoes)) {
            for (final AvaliacaoPromocaoTablet avaliacao : avaliacoes) {
                if (avaliacao != null) {
                    this.quantidadeAvaliacoes += avaliacao.getQuantidadeAvaliacoes();
                    soma(avaliacao.getPontos());
                }
            }
        }
    }

    public void adicionaConvites(final List<ConvitePromocaoTablet> convites) {
        if (CollectionUtils.isNotEmpty(convites)) {
            for (final ConvitePromocaoTablet convite : convites) {
                if (convite != null) {
                    this.quantidadeConvitesAceitos += convite.getQuantidadeConvites();
                    soma(convite.getPontos());
                }
            }
        }
    }

    public void adicionaDicas(final List<DicaPromocaoTablet> dicas) {
        if (CollectionUtils.isNotEmpty(dicas)) {
            for (final DicaPromocaoTablet dica : dicas) {
                if (dica != null) {
                    this.quantidadeDicas += dica.getQuantidadeDicas();
                    soma(dica.getPontos());
                }
            }
        }
    }

    @Override
    public int compareTo(final UsuarioPromocaoTabletPojo outro) {
        if (this.getPontuacao() < outro.getPontuacao()) {
            return 1;
        } else if (this.getPontuacao() > outro.getPontuacao()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final UsuarioPromocaoTabletPojo other = (UsuarioPromocaoTabletPojo) obj;
        if (this.idUsuario == null) {
            if (other.idUsuario != null)
                return false;
        } else if (!this.idUsuario.equals(other.idUsuario))
            return false;
        return true;
    }

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public String getNome() {
        return this.nome;
    }

    public Integer getPontuacao() {
        return this.pontuacao;
    }

    public Integer getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public Integer getQuantidadeConvitesAceitos() {
        return this.quantidadeConvitesAceitos;
    }

    public Integer getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.idUsuario == null) ? 0 : this.idUsuario.hashCode());
        return result;
    }

    public void setIdUsuario(final Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setPontuacao(final Integer pontuacao) {
        this.pontuacao = pontuacao;
    }

    public void setQuantidadeAvaliacoes(final Integer quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

    public void setQuantidadeConvitesAceitos(final Integer quantidadeConvitesAceitos) {
        this.quantidadeConvitesAceitos = quantidadeConvitesAceitos;
    }

    public void setQuantidadeDicas(final Integer quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void soma(final Integer pontos) {
        this.pontuacao += pontos;
    }

    @Override
    public String toString() {
        return "UsuarioPromocaoTabletPojo [idUsuario=" + this.idUsuario + ", nome=" + this.nome + ", pontuacao=" + this.pontuacao
                + ", quantidadeAvaliacoes=" + this.quantidadeAvaliacoes + ", quantidadeConvitesAceitos=" + this.quantidadeConvitesAceitos
                + ", quantidadeDicas=" + this.quantidadeDicas + "]";
    }

}
