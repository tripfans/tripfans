package br.com.fanaticosporviagens.promocoes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Service
public class PromocaoTablet2013Service extends BaseService {

    private static final int TAMANHO_LISTA = 20;

    private final PromocaoTablet2013Repository repository;

    @Autowired
    public PromocaoTablet2013Service(final PromocaoTablet2013Repository repository) {
        this.repository = repository;
    }

    public List<UsuarioPromocaoTabletPojo> consultarUsuariosPromocao() {
        //
        final List<AvaliacaoPromocaoTablet> avaliacoesPrePromocao = this.repository.consultarAvaliacoesPrePromocao();
        final List<AvaliacaoPromocaoTablet> avaliacoesPromocao = this.repository.consultarAvaliacoesPromocao();

        final List<DicaPromocaoTablet> dicasPrePromocao = this.repository.consultarDicasPrePromocao();
        final List<DicaPromocaoTablet> dicasPromocao = this.repository.consultarDicasPromocao();

        final List<ConvitePromocaoTablet> convitesPrePromocao = this.repository.consultarConvitesPrePromocao();
        final List<ConvitePromocaoTablet> convitesPromocao = this.repository.consultarConvitesPromocao();

        final List<AvaliacaoPromocaoTablet> avaliacoes = mergeAvaliacoes(avaliacoesPrePromocao, avaliacoesPromocao);
        final List<DicaPromocaoTablet> dicas = mergeDicas(dicasPrePromocao, dicasPromocao);
        final List<ConvitePromocaoTablet> convites = mergeConvites(convitesPrePromocao, convitesPromocao);

        final List<UsuarioPromocaoTabletPojo> usuarios = usuarios(avaliacoes, dicas, convites);

        if (usuarios.size() > TAMANHO_LISTA) {
            return new ArrayList<UsuarioPromocaoTabletPojo>(usuarios.subList(0, TAMANHO_LISTA));
        }

        return usuarios;
    }

    public UsuarioPromocaoTabletPojo getUsuarioPojo(final Usuario usuario) {
        final AvaliacaoPromocaoTablet avaliacoesPrePromocao = this.repository.consultarAvaliacoesPrePromocao(usuario.getId());
        final AvaliacaoPromocaoTablet avaliacoesPromocao = this.repository.consultarAvaliacoesPromocao(usuario.getId());
        final ConvitePromocaoTablet convitesPrePromocao = this.repository.consultarConvitesPrePromocao(usuario.getId());
        final ConvitePromocaoTablet convitesPromocao = this.repository.consultarConvitesPromocao(usuario.getId());
        final DicaPromocaoTablet dicasPrePromocao = this.repository.consultarDicasPrePromocao(usuario.getId());
        final DicaPromocaoTablet dicasPromocao = this.repository.consultarDicasPromocao(usuario.getId());

        final UsuarioPromocaoTabletPojo usuarioPojo = new UsuarioPromocaoTabletPojo();
        usuarioPojo.setIdUsuario(usuario.getId());
        usuarioPojo.setNome(usuario.getNome());
        usuarioPojo.setUrlPath(usuario.getUrlPath());
        usuarioPojo.adicionaAvaliacoes(Arrays.asList(new AvaliacaoPromocaoTablet[] { avaliacoesPrePromocao, avaliacoesPromocao }));
        usuarioPojo.adicionaConvites(Arrays.asList(new ConvitePromocaoTablet[] { convitesPrePromocao, convitesPromocao }));
        usuarioPojo.adicionaDicas(Arrays.asList(new DicaPromocaoTablet[] { dicasPrePromocao, dicasPromocao }));

        return usuarioPojo;
    }

    private List<AvaliacaoPromocaoTablet> mergeAvaliacoes(final List<AvaliacaoPromocaoTablet> avaliacoesPrePromocao,
            final List<AvaliacaoPromocaoTablet> avaliacoesPromocao) {

        final List<AvaliacaoPromocaoTablet> todas = new ArrayList<AvaliacaoPromocaoTablet>(avaliacoesPrePromocao);

        for (final AvaliacaoPromocaoTablet avaliacaoPromocao : avaliacoesPromocao) {
            final int index = todas.indexOf(avaliacaoPromocao);
            if (index != -1) {
                final AvaliacaoPromocaoTablet avaliacao = todas.get(index);
                avaliacao.atualizar(avaliacaoPromocao);
            } else {
                todas.add(avaliacaoPromocao);
            }
        }

        Collections.sort(todas);
        return todas;
    }

    private List<ConvitePromocaoTablet> mergeConvites(final List<ConvitePromocaoTablet> convitesPrePromocao,
            final List<ConvitePromocaoTablet> convitesPromocao) {
        final List<ConvitePromocaoTablet> todos = new ArrayList<ConvitePromocaoTablet>(convitesPrePromocao);

        for (final ConvitePromocaoTablet convitePromocao : convitesPromocao) {
            final int index = todos.indexOf(convitePromocao);
            if (index != -1) {
                final ConvitePromocaoTablet convite = todos.get(index);
                convite.atualizar(convitePromocao);
            } else {
                todos.add(convitePromocao);
            }
        }

        Collections.sort(todos);
        return todos;
    }

    private List<DicaPromocaoTablet> mergeDicas(final List<DicaPromocaoTablet> dicasPrePromocao, final List<DicaPromocaoTablet> dicasPromocao) {
        final List<DicaPromocaoTablet> todas = new ArrayList<DicaPromocaoTablet>(dicasPrePromocao);

        for (final DicaPromocaoTablet dicaPromocao : dicasPromocao) {
            final int index = todas.indexOf(dicaPromocao);
            if (index != -1) {
                final DicaPromocaoTablet dicas = todas.get(index);
                dicas.atualizar(dicaPromocao);
            } else {
                todas.add(dicaPromocao);

            }

        }

        Collections.sort(todas);
        return todas;
    }

    private List<UsuarioPromocaoTabletPojo> usuarios(final List<AvaliacaoPromocaoTablet> avaliacoes, final List<DicaPromocaoTablet> dicas,
            final List<ConvitePromocaoTablet> convites) {

        final List<UsuarioPromocaoTabletPojo> usuarios = new ArrayList<UsuarioPromocaoTabletPojo>();

        for (final AvaliacaoPromocaoTablet avaliacao : avaliacoes) {
            usuarios.add(new UsuarioPromocaoTabletPojo(avaliacao));
        }

        for (final DicaPromocaoTablet dica : dicas) {
            final UsuarioPromocaoTabletPojo usuario = (UsuarioPromocaoTabletPojo) CollectionUtils.find(usuarios, new ItemPromocaoTabletPredicado(
                    "idUsuario", dica.getIdUsuario()));
            if (usuario != null) {
                usuario.setQuantidadeDicas(dica.getQuantidadeDicas());
                usuario.soma(dica.getPontos());
            } else {
                usuarios.add(new UsuarioPromocaoTabletPojo(dica));
            }

        }

        for (final ConvitePromocaoTablet convite : convites) {
            final UsuarioPromocaoTabletPojo usuario = (UsuarioPromocaoTabletPojo) CollectionUtils.find(usuarios, new ItemPromocaoTabletPredicado(
                    "idUsuario", convite.getIdUsuario()));
            if (usuario != null) {
                usuario.setQuantidadeConvitesAceitos(convite.getQuantidadeConvites());
                usuario.soma(convite.getPontos());
            } else {
                usuarios.add(new UsuarioPromocaoTabletPojo(convite));
            }

        }

        Collections.sort(usuarios);

        return usuarios;
    }

}
