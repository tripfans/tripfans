package br.com.fanaticosporviagens.promocoes;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;

@Repository
public class PromocaoTablet2013Repository {

    private static final Date DATA_FIM_PROMOCAO = new LocalDate(2014, 02, 01).toDate();

    private static final Date DATA_LANCAMENTO_PROMOCAO = new LocalDate(2013, 12, 22).toDate();

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<AvaliacaoPromocaoTablet> consultarAvaliacoesPrePromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(a.id_avaliacao) as \"quantidadeAvaliacoes\", (count(id_avaliacao)*3) as pontos ");
        sql.append("FROM usuario u, avaliacao a ");
        sql.append("WHERE u.ativo = true and a.data_publicacao IS NOT NULL ");
        sql.append("AND a.id_autor = u.id_usuario ");
        sql.append("AND a.data_publicacao < :data ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_avaliacao pta WHERE pta.id_avaliacao = a.id_avaliacao and pta.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, AvaliacaoPromocaoTablet.class);
    }

    public AvaliacaoPromocaoTablet consultarAvaliacoesPrePromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(a.id_avaliacao) as \"quantidadeAvaliacoes\", (count(id_avaliacao)*3) as pontos ");
        sql.append("FROM usuario u, avaliacao a ");
        sql.append("WHERE u.ativo = true and a.data_publicacao IS NOT NULL ");
        sql.append("AND a.id_autor = u.id_usuario ");
        sql.append("AND a.data_publicacao  < :data ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_avaliacao pta WHERE pta.id_avaliacao = a.id_avaliacao and pta.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, AvaliacaoPromocaoTablet.class);
    }

    public List<AvaliacaoPromocaoTablet> consultarAvaliacoesPromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(a.id_avaliacao) as \"quantidadeAvaliacoes\", (count(id_avaliacao)*6) as pontos ");
        sql.append("FROM usuario u, avaliacao a ");
        sql.append("WHERE u.ativo = true and a.data_publicacao IS NOT NULL ");
        sql.append("AND a.id_autor = u.id_usuario ");
        sql.append("AND a.data_publicacao >= :dataInicio AND a.data_publicacao <= :dataFim ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_avaliacao pta WHERE pta.id_avaliacao = a.id_avaliacao and pta.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, AvaliacaoPromocaoTablet.class);
    }

    public AvaliacaoPromocaoTablet consultarAvaliacoesPromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(a.id_avaliacao) as \"quantidadeAvaliacoes\", (count(id_avaliacao)*3) as pontos ");
        sql.append("FROM usuario u, avaliacao a ");
        sql.append("WHERE u.ativo = true and a.data_publicacao IS NOT NULL ");
        sql.append("AND a.id_autor = u.id_usuario ");
        sql.append("AND a.data_publicacao >= :dataInicio AND a.data_publicacao <= :dataFim ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_avaliacao pta WHERE pta.id_avaliacao = a.id_avaliacao and pta.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, AvaliacaoPromocaoTablet.class);
    }

    public List<ConvitePromocaoTablet> consultarConvitesPrePromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(c.id_convite) as \"quantidadeConvites\", (count(c.id_convite)*1) as pontos ");
        sql.append("FROM usuario u, convite c ");
        sql.append("WHERE u.ativo = true and c.tipo_convite = 6 ");
        sql.append("AND c.id_remetente = u.id_usuario ");
        sql.append("AND c.aceito = true ");
        sql.append("AND c.data_aceite  < :data ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, ConvitePromocaoTablet.class);
    }

    public ConvitePromocaoTablet consultarConvitesPrePromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(c.id_convite) as \"quantidadeConvites\", (count(c.id_convite)*1) as pontos ");
        sql.append("FROM usuario u, convite c ");
        sql.append("WHERE u.ativo = true and c.tipo_convite = 6 ");
        sql.append("AND c.id_remetente = u.id_usuario ");
        sql.append("AND c.aceito = true ");
        sql.append("AND c.data_aceite  < :data ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, ConvitePromocaoTablet.class);
    }

    public List<ConvitePromocaoTablet> consultarConvitesPromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(c.id_convite) as \"quantidadeConvites\", (count(c.id_convite)*2) as pontos ");
        sql.append("FROM usuario u, convite c ");
        sql.append("WHERE u.ativo = true and c.tipo_convite = 6 ");
        sql.append("AND c.id_remetente = u.id_usuario ");
        sql.append("AND c.aceito = true ");
        sql.append("AND c.data_aceite >= :dataInicio AND c.data_aceite <= :dataFim ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, ConvitePromocaoTablet.class);
    }

    public ConvitePromocaoTablet consultarConvitesPromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(c.id_convite) as \"quantidadeConvites\", (count(c.id_convite)*2) as pontos ");
        sql.append("FROM usuario u, convite c ");
        sql.append("WHERE u.ativo = true and c.tipo_convite = 6 ");
        sql.append("AND c.id_remetente = u.id_usuario ");
        sql.append("AND c.aceito = true ");
        sql.append("AND c.data_aceite >= :dataInicio AND c.data_aceite <= :dataFim ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, ConvitePromocaoTablet.class);
    }

    public List<DicaPromocaoTablet> consultarDicasPrePromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(d.id_dica) as \"quantidadeDicas\", (count(id_dica)*2) as pontos ");
        sql.append("FROM usuario u, dica d ");
        sql.append("WHERE u.ativo = true and d.data_publicacao IS NOT NULL ");
        sql.append("AND d.autor = u.id_usuario ");
        sql.append("AND d.data_publicacao < :data ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_dica ptd WHERE ptd.id_dica = d.id_dica and ptd.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, DicaPromocaoTablet.class);
    }

    public DicaPromocaoTablet consultarDicasPrePromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(d.id_dica) as \"quantidadeDicas\", (count(id_dica)*2) as pontos ");
        sql.append("FROM usuario u, dica d ");
        sql.append("WHERE u.ativo = true and d.data_publicacao IS NOT NULL ");
        sql.append("AND d.autor = u.id_usuario ");
        sql.append("AND d.data_publicacao < :data ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_dica ptd WHERE ptd.id_dica = d.id_dica and ptd.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("data", DATA_LANCAMENTO_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, DicaPromocaoTablet.class);
    }

    public List<DicaPromocaoTablet> consultarDicasPromocao() {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(d.id_dica) as \"quantidadeDicas\", (count(id_dica)*4) as pontos ");
        sql.append("FROM usuario u, dica d ");
        sql.append("WHERE u.ativo = true and d.data_publicacao IS NOT NULL ");
        sql.append("AND d.autor = u.id_usuario ");
        sql.append("AND d.data_publicacao >= :dataInicio AND d.data_publicacao <= :dataFim ");
        sql.append("AND u.id_usuario not in  (2,25,60,70,91,93,112,2790,4980,6909) ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_dica ptd WHERE ptd.id_dica = d.id_dica and ptd.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");
        sql.append("ORDER BY pontos DESC ");
        sql.append("FETCH FIRST 50 ROWS ONLY");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);

        return this.searchRepository.consultaSQL(sql.toString(), params, DicaPromocaoTablet.class);
    }

    public DicaPromocaoTablet consultarDicasPromocao(final Long idUsuario) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT u.id_usuario as \"idUsuario\", u.url_path as \"urlPathUsuario\", u.display_name as \"nomeUsuario\", count(d.id_dica) as \"quantidadeDicas\", (count(id_dica)*4) as pontos ");
        sql.append("FROM usuario u, dica d ");
        sql.append("WHERE u.ativo = true and d.data_publicacao IS NOT NULL ");
        sql.append("AND d.autor = u.id_usuario ");
        sql.append("AND d.data_publicacao >= :dataInicio AND d.data_publicacao <= :dataFim ");
        sql.append("AND u.id_usuario =  :idUsuario ");
        sql.append("AND NOT EXISTS  (SELECT 1 FROM promocao_tablet_dica ptd WHERE ptd.id_dica = d.id_dica and ptd.aprovada = false) ");
        sql.append("GROUP BY u.id_usuario ");

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("dataInicio", DATA_LANCAMENTO_PROMOCAO);
        params.put("dataFim", DATA_FIM_PROMOCAO);
        params.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQLResultadoUnico(sql.toString(), params, DicaPromocaoTablet.class);
    }

}
