package br.com.fanaticosporviagens.promocoes;

public class AvaliacaoPromocaoTablet extends ItemPromocaoTablet {

    private Integer quantidadeAvaliacoes = 0;

    public void atualizar(final AvaliacaoPromocaoTablet avaliacaoPromocao) {
        super.soma(avaliacaoPromocao.getPontos());
        this.quantidadeAvaliacoes += avaliacaoPromocao.getQuantidadeAvaliacoes();
    }

    public Integer getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public void setQuantidadeAvaliacoes(final Integer quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

}
