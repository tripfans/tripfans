package br.com.fanaticosporviagens.seo;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Sitemap;

@Repository
public class SitemapRepository {

    @Autowired
    private GenericSearchRepository searchRepository;

    public Sitemap consultarSitemapPorUrlPath(final String urlPath) {

        final StringBuilder hql = new StringBuilder();
        hql.append("select sitemap ");
        hql.append("  from br.com.fanaticosporviagens.model.entity.Sitemap sitemap ");
        hql.append(" where sitemap.urlPath = :urlPath ");

        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("urlPath", urlPath);

        return (Sitemap) this.searchRepository.consultaHQL(hql.toString(), params, 1).iterator().next();
    }

    public List<Sitemap> consultarTodos() {
        final StringBuilder hql = new StringBuilder();
        hql.append("select sitemap ");
        hql.append("  from br.com.fanaticosporviagens.model.entity.Sitemap sitemap ");
        hql.append(" order by sitemap.urlPath ");

        final HashMap<String, Object> params = new HashMap<String, Object>();

        return this.searchRepository.consultaHQL(hql.toString(), params, 50000);
    }
}
