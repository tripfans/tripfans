package br.com.fanaticosporviagens.seo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.model.entity.Sitemap;

@Service
public class SitemapService {

    @Autowired
    private SitemapRepository sitemapRepository;

    public Sitemap consultarSitemapPorUrlPath(final String urlPath) {
        return this.sitemapRepository.consultarSitemapPorUrlPath(urlPath);
    }

    public List<Sitemap> consultarTodos() {
        return this.sitemapRepository.consultarTodos();
    }

}
