package br.com.fanaticosporviagens.seo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Sitemap;

@Controller
public class SitemapController extends BaseController {

    @Autowired
    private SitemapService sitemapService;

    @RequestMapping(value = "/sitemap-{urlPath}.xml", method = { RequestMethod.GET })
    public String sitemapDestino(@PathVariable final String urlPath, final Model model) {

        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        System.out.println("Inicio busca sitemap:\t\t" + sdf.format(new Date()));
        final Sitemap sitemap = this.sitemapService.consultarSitemapPorUrlPath(urlPath);
        System.out.println("Fim busca sitemap:\t\t" + sdf.format(new Date()));
        System.out.println("Inicio busca locais:\t\t" + sdf.format(new Date()));
        final List<Local> locais = sitemap.getLocais();
        System.out.println("Fim busca locais:\t\t" + sdf.format(new Date()));

        model.addAttribute("locais", locais);

        return "/sitemap/sitemapLocal";
    }

    @RequestMapping(value = "/sitemap.xml", method = { RequestMethod.GET })
    public String sitemapIndex(final Model model) {

        final List<Sitemap> sitemaps = this.sitemapService.consultarTodos();

        model.addAttribute("sitemaps", sitemaps);

        return "/sitemap/sitemapIndex";
    }
}
