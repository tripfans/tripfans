package br.com.fanaticosporviagens.tipo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.model.entity.TipoViagem;

@Service
public class TipoService {

    @Autowired
    private TipoViagemRepository tipoViagemRepository;

    public List<TipoViagem> tipoViagemConsultarAtivos() {
        return this.tipoViagemRepository.consultarTiposViagemAtivos();
    }

}
