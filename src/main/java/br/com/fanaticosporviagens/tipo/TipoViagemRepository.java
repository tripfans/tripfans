package br.com.fanaticosporviagens.tipo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.TipoViagem;

@Repository
public class TipoViagemRepository extends GenericCRUDRepository<TipoViagem, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<TipoViagem> consultarTiposViagemAtivos() {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT tipoViagem ");
        hql.append("  FROM TipoViagem tipoViagem ");
        hql.append(" WHERE tipoViagem.ativo = :ativo");
        hql.append(" ORDER BY tipoViagem.descricao ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("ativo", true);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

}
