package br.com.fanaticosporviagens.util;

import org.apache.commons.configuration.DatabaseConfiguration;
import org.springframework.core.env.PropertySource;

public class DatabasePropertySource extends PropertySource<DatabaseConfiguration> {
    public static final String DATABASE_PROPERTY_SOURCE_NAME = "database";

    private final DatabaseConfiguration repository;

    public DatabasePropertySource(final DatabaseConfiguration source) {
        this(DATABASE_PROPERTY_SOURCE_NAME, source);
    }

    public DatabasePropertySource(final String name, final DatabaseConfiguration source) {
        super(name, source);
        this.repository = source;
    }

    @Override
    public Object getProperty(final String name) {
        try {
            return this.repository.getProperty(name);
        } catch (final Exception e) {
            this.logger.error("Error accessing properties from Database: " + e.getMessage());
            return null;
        }
    }
}
