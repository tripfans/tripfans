package br.com.fanaticosporviagens.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;

@Component
public class TripFansShareableLinkBuilder {

    private final String serverUrl;

    @Autowired
    public TripFansShareableLinkBuilder(final TripFansEnviroment tripFansEnviroment) {
        this.serverUrl = tripFansEnviroment.getServerUrl();
    }

    public String getLink(final SocialShareableItem shareable) {
        return this.serverUrl + shareable.getShareableLink();
    }

}
