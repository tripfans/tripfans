package br.com.fanaticosporviagens.util.local;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.LocalidadeEmbeddable;

public class GeradorPlSqlAtualizacaoDesnormalizacao {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        final GeradorPlSqlAtualizacaoDesnormalizacao gerador = new GeradorPlSqlAtualizacaoDesnormalizacao();

        gerador.print();
    }

    private final String ID_LOCAL_MAIS_ESPECIFICO = nomeCampo(LocalDesnormalizado.LOCAL_MAIS_ESPECIFICO, CampoDesnormalizado.ID);

    public List<String> buildFunctionAtualizarLocalidade() {

        final ArrayList<String> function = new ArrayList<String>();

        function.add("CREATE OR REPLACE FUNCTION localidade_atualizar() ");
        function.add("    RETURNS int4");
        function.add(" AS ");
        function.add(" $BODY$ ");
        function.add(" DECLARE ");
        function.add("    v_qtd_locais_desatualizados  INTEGER; ");
        function.add("    cursor_tabela                RECORD; ");
        function.add(" BEGIN ");
        function.add("    v_qtd_locais_desatualizados := 0; ");

        function.add("");

        function.add(tab(1) + "v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_local();");

        function.add("");

        function.add(tab(1) + "v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_local_mais_especifico();");

        function.add("");

        final String packageName = "br.com.fanaticosporviagens.model.entity";
        final String tabelaDeOrigem = "local";
        for (final String tabela : getTabelasParaAtualizacao(packageName, LocalidadeEmbeddable.class)) {
            if (!tabelaDeOrigem.equalsIgnoreCase(tabela)) {
                function.add(tab(1) + "v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_outras_tabelas('" + tabela
                        + "');");
            }
        }
        function.add("");
        function.add("    RETURN v_qtd_locais_desatualizados; ");
        function.add(" END; ");
        function.add(" $BODY$ ");
        function.add(" LANGUAGE plpgsql VOLATILE COST 100; ");

        return function;
    }

    public List<String> buildFunctionAtualizarLocalidadeEmLocalLocalizacao() {
        final ArrayList<String> function = new ArrayList<String>();

        function.add("CREATE OR REPLACE FUNCTION localidade_atualizar_local() ");
        function.add("    RETURNS int4");
        function.add(" AS ");
        function.add(" $BODY$ ");
        function.add(" DECLARE ");
        function.add("    v_qtd_locais_desatualizados   INTEGER; ");
        function.add("    cursor_local                 RECORD; ");
        function.add(" BEGIN ");
        function.add("    v_qtd_locais_desatualizados := 0; ");

        function.add("");

        for (final LocalDesnormalizado local : LocalDesnormalizado.values()) {
            if ((local.getTypes() != null) && (local.getTypes().length > 0)) {

                function.add(tab(2) + "FOR cursor_local IN (SELECT");
                String separador = "  ";
                for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
                    function.add(tab(4) + separador + campo.getNomeCampoOriginaEmLocal());
                    separador = ", ";
                }
                function.add(tab(3) + " FROM Local l");

                function.add(tab(3) + "WHERE l.local_type IN (");
                separador = "  ";
                for (final LocalType type : local.getTypes()) {
                    function.add(tab(5) + separador + type.getCodigo());
                    separador = ", ";
                }
                function.add(tab(4) + "   )");
                function.add(tab(4) + "AND EXISTS (SELECT 1");
                function.add(tab(6) + " FROM Local l2");
                function.add(tab(6) + " WHERE l2." + nomeCampo(local, CampoDesnormalizado.ID) + " = l."
                        + CampoDesnormalizado.ID.getNomeCampoOriginaEmLocal());

                function.add(tab(6) + "   AND (");

                String comparacaoDiferente;
                String comparacaoNullNotNull;
                String comparacaoNotNullNull;
                separador = "   ";
                for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
                    if (!campo.equals(CampoDesnormalizado.ID)) {
                        comparacaoDiferente = "(l2." + nomeCampo(local, campo) + " <> l." + campo.getNomeCampoOriginaEmLocal() + ")";
                        comparacaoNullNotNull = "(l2." + nomeCampo(local, campo) + " IS NULL AND l." + campo.getNomeCampoOriginaEmLocal()
                                + " IS NOT NULL)";
                        comparacaoNotNullNull = "(l2." + nomeCampo(local, campo) + " IS NOT NULL AND l." + campo.getNomeCampoOriginaEmLocal()
                                + " IS NULL)";

                        function.add(tab(7) + separador + "(" + comparacaoDiferente + " OR " + comparacaoNullNotNull + " OR " + comparacaoNotNullNull
                                + ")");

                        separador = "OR ";
                    }
                }

                function.add(tab(7) + ")");
                function.add(tab(6) + ")");
                function.add(tab(3) + ")");
                function.add(tab(2) + "LOOP");

                function.add("");

                function.add(tab(3) + "UPDATE Local");

                separador = "SET ";
                for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
                    if (!campo.equals(CampoDesnormalizado.ID)) {
                        function.add(tab(4) + separador + nomeCampo(local, campo) + " = cursor_local." + campo.getNomeCampoOriginaEmLocal());

                        separador = "  , ";
                    }
                }
                function.add(tab(3) + "WHERE " + nomeCampo(local, CampoDesnormalizado.ID) + " = cursor_local."
                        + CampoDesnormalizado.ID.getNomeCampoOriginaEmLocal());
                function.add(tab(3) + "   AND (");

                separador = "   ";
                for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
                    if (!campo.equals(CampoDesnormalizado.ID)) {
                        comparacaoDiferente = "(" + nomeCampo(local, campo) + " <> cursor_local." + campo.getNomeCampoOriginaEmLocal() + ")";
                        comparacaoNullNotNull = "(" + nomeCampo(local, campo) + " IS NULL AND cursor_local." + campo.getNomeCampoOriginaEmLocal()
                                + " IS NOT NULL)";
                        comparacaoNotNullNull = "(" + nomeCampo(local, campo) + " IS NOT NULL AND cursor_local." + campo.getNomeCampoOriginaEmLocal()
                                + " IS NULL)";

                        function.add(tab(4) + separador + "(" + comparacaoDiferente + " OR " + comparacaoNullNotNull + " OR " + comparacaoNotNullNull
                                + ")");

                        separador = "OR ";
                    }
                }

                function.add(tab(3) + ")");
                function.add(tab(3) + ";");

                function.add("");

                function.add(tab(3) + "v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; ");
                function.add(tab(2) + "END LOOP;");
            }
            function.add("");
        }

        function.add("");

        function.add("    RETURN v_qtd_locais_desatualizados; ");
        function.add(" END; ");
        function.add(" $BODY$ ");
        function.add(" LANGUAGE plpgsql VOLATILE COST 100; ");

        return function;
    }

    public List<String> buildFunctionAtualizarLocalidadeEmLocalLocalLocalMaisEspecifico() {
        final ArrayList<String> function = new ArrayList<String>();

        function.add("CREATE OR REPLACE FUNCTION localidade_atualizar_local_mais_especifico() ");
        function.add("    RETURNS int4");
        function.add(" AS ");
        function.add(" $BODY$ ");
        function.add(" DECLARE ");
        function.add("    v_qtd_locais_desatualizados   INTEGER; ");
        function.add("    cursor_local                 RECORD; ");
        function.add("    v_sql                     TEXT; ");
        function.add("    v_update                  TEXT; ");
        function.add(" BEGIN ");
        function.add("    v_qtd_locais_desatualizados := 0; ");

        function.add("");

        function.add(tab(1) + "UPDATE Local ");

        String separador = "SET ";
        for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
            function.add(tab(2) + separador + nomeCampo(LocalDesnormalizado.LOCAL_MAIS_ESPECIFICO, campo) + " = "
                    + campo.getNomeCampoOriginaEmLocal());

            separador = "  , ";
        }
        String comparacaoDiferente;
        String comparacaoNullNotNull;
        String comparacaoNotNullNull;
        separador = "WHERE ";
        for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
            comparacaoDiferente = "(" + nomeCampo(LocalDesnormalizado.LOCAL_MAIS_ESPECIFICO, campo) + " <> " + campo.getNomeCampoOriginaEmLocal()
                    + ")";
            comparacaoNullNotNull = "(" + nomeCampo(LocalDesnormalizado.LOCAL_MAIS_ESPECIFICO, campo) + " IS NULL AND "
                    + campo.getNomeCampoOriginaEmLocal() + " IS NOT NULL)";
            comparacaoNotNullNull = "(" + nomeCampo(LocalDesnormalizado.LOCAL_MAIS_ESPECIFICO, campo) + " IS NOT NULL AND "
                    + campo.getNomeCampoOriginaEmLocal() + " IS NULL)";

            function.add(tab(2) + separador + "(" + comparacaoDiferente + " OR " + comparacaoNullNotNull + " OR " + comparacaoNotNullNull + ")");

            separador = "   OR ";
        }

        function.add(tab(1) + ";");

        function.add("");

        function.add("    RETURN v_qtd_locais_desatualizados; ");
        function.add(" END; ");
        function.add(" $BODY$ ");
        function.add(" LANGUAGE plpgsql VOLATILE COST 100; ");

        return function;
    }

    public List<String> buildFunctionAtualizarLocalidadeEmOutrasTabelas() {

        final ArrayList<String> function = new ArrayList<String>();

        function.add("CREATE OR REPLACE FUNCTION localidade_atualizar_outras_tabelas(p_table_name CHARACTER VARYING) ");
        function.add("    RETURNS int4");
        function.add(" AS ");
        function.add(" $BODY$ ");
        function.add(" DECLARE ");
        function.add("    v_qtd_locais_desatualizados   INTEGER; ");
        function.add("    cursor_local                 RECORD; ");
        function.add("    v_sql                     TEXT; ");
        function.add("    v_update                  TEXT; ");
        function.add(" BEGIN ");
        function.add("    v_qtd_locais_desatualizados := 0; ");

        function.add("");

        function.add(tab(1) + "v_sql := 'SELECT ");

        String separador = "  ";
        for (final String campo : campos()) {
            function.add(tab(3) + separador + campo);

            separador = ", ";
        }
        function.add(tab(1) + "  FROM Local l ");
        function.add(tab(1) + " WHERE EXISTS (SELECT 1 FROM '|| p_table_name ||' d ");
        function.add(tab(1) + "                       WHERE l." + this.ID_LOCAL_MAIS_ESPECIFICO + " = d." + this.ID_LOCAL_MAIS_ESPECIFICO);
        function.add(tab(1) + "                         AND (");

        String comparacaoDiferente;
        String comparacaoNullNotNull;
        String comparacaoNotNullNull;
        separador = "   ";
        for (final String campo : campos()) {
            comparacaoDiferente = "(l." + campo + " <> d." + campo + ")";
            comparacaoNullNotNull = "(l." + campo + " IS NULL AND d." + campo + " IS NOT NULL)";
            comparacaoNotNullNull = "(l." + campo + " IS NOT NULL AND d." + campo + " IS NULL)";

            function.add(tab(5) + separador + "(" + comparacaoDiferente + " or " + comparacaoNullNotNull + " or " + comparacaoNotNullNull + ")");

            separador = "OR ";
        }
        function.add("                             )");
        function.add("                         )");
        function.add(tab(1) + " ORDER BY l.id_local';");

        function.add("");

        function.add(tab(2) + " v_update := 'UPDATE ' || p_table_name || ' ");

        int count = 1;
        separador = "SET ";
        for (final String campo : campos()) {
            function.add(tab(4) + separador + campo + " =  $" + count);

            separador = "  , ";
            count++;
        }
        function.add(tab(3) + " WHERE " + this.ID_LOCAL_MAIS_ESPECIFICO + " = $" + count + "';");

        function.add("");

        function.add(tab(1) + " FOR cursor_local IN EXECUTE v_sql ");
        function.add(tab(1) + " LOOP ");

        function.add(tab(2) + " EXECUTE v_update  ");
        separador = "USING ";
        for (final String campo : campos()) {
            function.add(tab(4) + separador + "cursor_local." + campo);

            separador = "    , ";
        }
        function.add(tab(4) + separador + "cursor_local." + this.ID_LOCAL_MAIS_ESPECIFICO + ";");

        function.add(tab(2) + " v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; ");
        function.add(tab(1) + " END LOOP; ");

        function.add("");

        function.add("    RETURN v_qtd_locais_desatualizados; ");
        function.add(" END; ");
        function.add(" $BODY$ ");
        function.add(" LANGUAGE plpgsql VOLATILE COST 100; ");

        return function;
    }

    public String nomeCampo(final LocalDesnormalizado prefixo, final CampoDesnormalizado sufixo) {
        return prefixo.getPrefixo() + "_" + sufixo.getSufixo();
    }

    public void print() {
        for (final String print : buildFunctionAtualizarLocalidadeEmLocalLocalizacao()) {
            System.out.println(print);
        }
        System.out.println("");
        for (final String print : buildFunctionAtualizarLocalidadeEmLocalLocalLocalMaisEspecifico()) {
            System.out.println(print);
        }
        System.out.println("");
        for (final String print : buildFunctionAtualizarLocalidadeEmOutrasTabelas()) {
            System.out.println(print);
        }
        System.out.println("");
        for (final String print : buildFunctionAtualizarLocalidade()) {
            System.out.println(print);
        }
    }

    private List<String> campos() {
        final ArrayList<String> campos = new ArrayList<String>();

        for (final LocalDesnormalizado local : LocalDesnormalizado.values()) {
            for (final CampoDesnormalizado campo : CampoDesnormalizado.values()) {
                campos.add(nomeCampo(local, campo));
            }
        }
        Collections.sort(campos);

        return campos;
    }

    private List<Class<?>> getReflectionAllClasses(final String packageName) {

        final URL root = Thread.currentThread().getContextClassLoader().getResource(packageName.replace(".", "/"));

        // Filter .class files.
        final File[] files = new File(root.getFile()).listFiles(new FilenameFilter() {
            @Override
            public boolean accept(final File dir, final String name) {
                return name.endsWith(".class");
            }
        });

        final List<Class<?>> allClass = new ArrayList<Class<?>>();
        // Find classes implementing ICommand.
        for (final File file : files) {

            final String className = packageName + "." + file.getName().replaceAll(".class$", "");

            if (!className.equals("br.com.fanaticosporviagens.model.entity.TipoCompra")
                    && !className.equals("br.com.fanaticosporviagens.model.entity.TipoHospedagem")
                    && !className.equals("br.com.fanaticosporviagens.model.entity.TipoQualificacaoAtracao")
                    && !className.equals("br.com.fanaticosporviagens.model.entity.TipoVidaNoturna")) {
                try {
                    allClass.add(Class.forName(className));
                } catch (final ClassNotFoundException e) {
                    throw new RuntimeException("Classe nao encontrada (" + className + ")", e);
                }
            }

        }
        return allClass;
    }

    private List<Class<?>> getReflectionClassesComAtributoDoTipo(final String packageName, final Class<?> tipoAtributo) {

        final List<Class<?>> allClasses = getReflectionAllClasses(packageName);

        final List<Class<?>> classesComAtributo = new ArrayList<Class<?>>();

        for (final Class<?> clazz : allClasses) {
            for (final Field atributo : clazz.getDeclaredFields()) {
                if (atributo.getType().equals(tipoAtributo)) {
                    classesComAtributo.add(clazz);
                    break;
                }
            }
        }

        return classesComAtributo;

    }

    private List<String> getTabelasParaAtualizacao(final String entidadesDoPackage, final Class<?> classesComAtributoDoTipo) {

        final List<Class<?>> classesComLocalidadeEmbeddable = getReflectionClassesComAtributoDoTipo(entidadesDoPackage, classesComAtributoDoTipo);

        final List<String> tabelas = new ArrayList<String>();
        for (final Class<?> clazz : classesComLocalidadeEmbeddable) {
            tabelas.add(clazz.getAnnotation(javax.persistence.Table.class).name());
        }

        return tabelas;
    }

    private String tab(final int tabs) {
        final String tabPadrao = "    ";

        String t = "";
        for (int i = 0; i < tabs; i++) {
            t += tabPadrao;
        }
        return t;
    }

    private enum CampoDesnormalizado {
        ID("id", "id_local"),
        ID_FACEBOOK("id_facebook", "id_facebook"),
        ID_FOTO_PADRAO("id_foto_padrao", "id_foto"),
        ID_FOURSQUARE("id_foursquare", "id_foursquare"),
        NOME("nome", "nome"),
        SIGLA("sigla", "sigla"),
        TYPE("type", "local_type"),
        URL_PATH("url_path", "url_path");

        private final String nomeCampoOriginaEmLocal;

        private final String sufixo;

        CampoDesnormalizado(final String campo, final String nomeCampoOriginaEmLocal) {
            this.sufixo = campo;
            this.nomeCampoOriginaEmLocal = nomeCampoOriginaEmLocal;
        }

        public String getNomeCampoOriginaEmLocal() {
            return this.nomeCampoOriginaEmLocal;
        }

        public String getSufixo() {
            return this.sufixo;
        }

    }

    private enum LocalDesnormalizado {
        CIDADE("d_cidade", LocalType.CIDADE),
        CONTINENTE("d_continente", LocalType.CONTINENTE),
        ESTADO("d_estado", LocalType.ESTADO),
        LOCAL_ENDERECAVEL("d_local_enderecavel", LocalType.AEROPORTO, LocalType.AGENCIA, LocalType.ATRACAO, LocalType.HOTEL,
                          LocalType.IMOVEL_ALUGUEL_TEMPORADA, LocalType.LOCADORA_VEICULOS, LocalType.RESTAURANTE),
        LOCAL_INTERESSE_TURISTICO("d_local_interesse_turistico", LocalType.LOCAL_INTERESSE_TURISTICO),
        LOCAL_MAIS_ESPECIFICO("d_local_mais_especifico"),
        PAIS("d_pais", LocalType.PAIS);

        private final String prefixo;

        private final LocalType[] types;

        LocalDesnormalizado(final String local, final LocalType... types) {
            this.prefixo = local;
            this.types = types;
        }

        public String getPrefixo() {
            return this.prefixo;
        }

        public LocalType[] getTypes() {
            return this.types;
        }
    }
}
