package br.com.fanaticosporviagens.util;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.fanaticosporviagens.infra.security.SecurityService;

/**
 * Essa classe mantém variáveis de ambiente específicas da aplicação.
 * 
 * @author André Thiago
 * 
 */
public class TripFansEnviroment implements Serializable {

    private static final long serialVersionUID = 6594481360561552864L;

    private String bitlyAccessToken;

    private String bitlyShortenUrl;

    private boolean devel;

    private boolean develAtWork = false;

    private boolean enabledPedidoDica;

    private boolean enableSocialNetworkActions = true;

    private String facebookClientId;

    private String fourSquareClientId;

    private String googleClientId;

    private String imagesServerUrl;

    @Autowired
    private SecurityService securityService;

    private String serverUrl;

    private String twitterClientId;

    public TripFansEnviroment() {
    }

    public TripFansEnviroment(final SecurityService securityService) {
        this.securityService = securityService;
    }

    public String getBitlyAccessToken() {
        return this.bitlyAccessToken;
    }

    public String getBitlyShortenUrl() {
        return this.bitlyShortenUrl;
    }

    public String getFacebookClientId() {
        return this.facebookClientId;
    }

    public String getFourSquareClientId() {
        return this.fourSquareClientId;
    }

    public String getGoogleClientId() {
        return this.googleClientId;
    }

    public String getImagesServerUrl() {
        return this.imagesServerUrl;
    }

    public String getServerUrl() {
        return this.serverUrl;
    }

    public String getTwitterClientId() {
        return this.twitterClientId;
    }

    public boolean isDevel() {
        return this.devel;
    }

    public boolean isDevelAtWork() {
        return this.develAtWork;
    }

    public boolean isEnabledPedidoDica() {
        return this.enabledPedidoDica;
    }

    public boolean isEnableSocialNetworkActions() {
        return this.enableSocialNetworkActions || (this.securityService != null && this.securityService.isAdmin());
    }

    public void setBitlyAccessToken(final String bitlyAccessToken) {
        this.bitlyAccessToken = bitlyAccessToken;
    }

    public void setBitlyShortenUrl(final String bitlyShortenUrl) {
        this.bitlyShortenUrl = bitlyShortenUrl;
    }

    public void setDevel(final boolean devel) {
        this.devel = devel;
    }

    public void setDevelAtWork(final boolean develAtWork) {
        this.develAtWork = develAtWork;
    }

    public void setEnabledPedidoDica(final boolean enabledPedidoDica) {
        this.enabledPedidoDica = enabledPedidoDica;
    }

    public void setEnableSocialNetworkActions(final boolean enableSocialNetworkActions) {
        this.enableSocialNetworkActions = enableSocialNetworkActions;
    }

    public void setFacebookClientId(final String facebookClientId) {
        this.facebookClientId = facebookClientId;
    }

    public void setFourSquareClientId(final String fourSquareClientId) {
        this.fourSquareClientId = fourSquareClientId;
    }

    public void setGoogleClientId(final String googleClientId) {
        this.googleClientId = googleClientId;
    }

    public void setImagesServerUrl(final String imagesServerUrl) {
        this.imagesServerUrl = imagesServerUrl;
    }

    public void setServerUrl(final String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public void setTwitterClientId(final String twitterClientId) {
        this.twitterClientId = twitterClientId;
    }

}
