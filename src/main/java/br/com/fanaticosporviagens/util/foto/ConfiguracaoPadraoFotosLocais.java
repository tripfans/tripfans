package br.com.fanaticosporviagens.util.foto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import br.com.fanaticosporviagens.foto.FotoStorageService;
import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.ImageFormat;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;

public class ConfiguracaoPadraoFotosLocais {

    private static final String INSERT_FOTO = "INSERT INTO foto(id_foto, data_criacao, data_publicacao, tipo_armazenamento, url_local_album, url_local_big, url_local_small, "
            + " id_autor, id_local, anonima) VALUES(nextval('seq_foto'), current_timestamp,current_date,%d,'%s','%s', '%s',%d,%d,%b)";

    private static final String SELECT_FOTOS = "SELECT id_foto, id_local FROM foto WHERE id_local IS NOT NULL AND anonima = true order by id_foto offset %d limit 100";

    private static final String SELECT_LOCAIS = "SELECT l.id_local, l.nome, l.url_path, l.local_type FROM local l WHERE l.id_foto is null and l.local_type != 3 order by l.id_local offset %d limit 100";

    private static final String UPDATE_LOCAL = "UPDATE local set id_foto = %d WHERE id_local = %d";

    /**
     * @param args
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws IOException
     */
    public static void main(final String[] args) throws InstantiationException, IllegalAccessException, IOException {
        final ConfiguracaoPadraoFotosLocais configuracao = new ConfiguracaoPadraoFotosLocais();
        configuracao.populaFotosLocais();
    }

    private final FotoStorageService fotoStorageService;

    private final Long idUsuarioTripFans;

    private final String imagesFullDirectory;

    private final JdbcTemplate jdbcTemplate;

    public ConfiguracaoPadraoFotosLocais() throws IOException {
        final ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-indexer.xml");
        this.jdbcTemplate = ctx.getBean(JdbcTemplate.class);
        this.fotoStorageService = ctx.getBean(FotoStorageService.class);

        final List<Map<String, Object>> usuarios = this.jdbcTemplate
                .queryForList("SELECT id_usuario FROM usuario where email = 'tripfans@tripfans.com.br'");
        if (usuarios.isEmpty()) {
            throw new RuntimeException("O usuário TripFans não existe na base!!! Você rodou o script config/db/criaUsuarioTripFans.sql?");
        }
        this.idUsuarioTripFans = Long.valueOf(usuarios.get(0).get("id_usuario").toString());

        final Properties properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("application.properties"));

        final String protocol = properties.getProperty(AppEnviromentConstants.APPLICATION_PROTOCOL);
        final String domain = properties.getProperty(AppEnviromentConstants.APPLICATION_DOMAIN);
        final String imagesPath = properties.getProperty(AppEnviromentConstants.APPLICATION_IMAGES_PATH);
        this.imagesFullDirectory = properties.getProperty(AppEnviromentConstants.APPLICATION_IMAGES_ROOT_DIRECTORY) + imagesPath;

        final String imagesServerUrl = protocol + "://" + domain + imagesPath;

        System.setProperty(AppEnviromentConstants.IMAGES_SERVER_URL, imagesServerUrl);
        System.setProperty(AppEnviromentConstants.IMAGES_FULL_DIRECTORY, this.imagesFullDirectory);

    }

    private void populaFotosLocais() throws InstantiationException, IllegalAccessException {
        System.out.println("Iniciando....");
        int offset = 0;
        List<Map<String, Object>> lista = this.jdbcTemplate.queryForList(String.format(SELECT_LOCAIS, new Object[] { offset }));
        do {
            final List<String> inserts = new ArrayList<String>();
            for (final Map<String, Object> resultado : lista) {
                final Long idLocal = Long.parseLong(resultado.get("id_local").toString());
                final int codigo = (Integer) resultado.get("local_type");
                System.out.println("Configurando fotos para local " + idLocal);
                final LocalType localType = LocalType.getEnumPorCodigo(codigo);
                final Local local = (Local) localType.getLocalClass().newInstance();
                local.setId(idLocal);
                local.setTipoLocal(localType);
                final Foto foto = new Foto();
                foto.setLocal(local);
                foto.setAnonima(true);
                salvarFotoSistemaArquivos(foto);
                final String insert = String.format(INSERT_FOTO, new Object[] { TipoArmazenamentoFoto.LOCAL.getCodigo(), foto.getUrlLocalAlbum(),
                        foto.getUrlLocalBig(), foto.getUrlLocalSmall(), this.idUsuarioTripFans, idLocal, true });
                inserts.add(insert);
            }
            if (!inserts.isEmpty()) {
                final String[] sqlInserts = inserts.toArray(new String[0]);
                this.jdbcTemplate.batchUpdate(sqlInserts);
            }
            offset += 100;
            lista = this.jdbcTemplate.queryForList(String.format(SELECT_LOCAIS, new Object[] { offset }));
        } while (!lista.isEmpty());

        System.out.println("Inserção em fotos pronta...");
        System.out.println("Vai atualizar as fotos dos locais...");
        // Pega o id da foto da operação anterior, e atualiza o campo na tabela logal
        offset = 0;
        lista = this.jdbcTemplate.queryForList(String.format(SELECT_FOTOS, new Object[] { offset }));
        do {
            final List<String> updates = new ArrayList<String>();
            for (final Map<String, Object> resultado : lista) {
                final Long idLocal = Long.parseLong(resultado.get("id_local").toString());
                final Long idFoto = Long.parseLong(resultado.get("id_foto").toString());
                final String update = String.format(UPDATE_LOCAL, new Object[] { idFoto, idLocal });
                updates.add(update);
            }
            if (!updates.isEmpty()) {
                final String[] sqlInserts = updates.toArray(new String[0]);
                this.jdbcTemplate.batchUpdate(sqlInserts);
            }
            offset += 100;
            lista = this.jdbcTemplate.queryForList(String.format(SELECT_FOTOS, new Object[] { offset }));
        } while (!lista.isEmpty());
        System.out.println("Fim.");
    }

    private void salvarFotoSistemaArquivos(final Foto foto) {
        this.fotoStorageService.writeInFormatsInBackground(foto, ImageFormat.ALBUM, ImageFormat.SMALL);
    }
}
