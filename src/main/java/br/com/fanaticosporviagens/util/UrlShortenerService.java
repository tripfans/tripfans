package br.com.fanaticosporviagens.util;

import java.io.IOException;
import java.net.URI;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.exception.SystemException;

/**
 * Serviço utilizado para fazer encurtamento de urls.
 * 
 * @author André Thiago (andrethiago@tripfans.com.br)
 * 
 */
@Service
public class UrlShortenerService {

    private static final String URL = "\"url\":";

    private final String accessToken;

    private final String shortenUrl;

    @Autowired
    public UrlShortenerService(final TripFansEnviroment tripFansEnviroment) {
        this.accessToken = tripFansEnviroment.getBitlyAccessToken();
        this.shortenUrl = tripFansEnviroment.getBitlyShortenUrl();
    }

    public String shorten(final String url) {
        final HttpClient httpclient = new HttpClient();
        final HttpMethod method = new GetMethod(this.shortenUrl);
        try {
            final String urlEncoded = new URI(url).toASCIIString();
            method.setQueryString(new NameValuePair[] { new NameValuePair("longUrl", urlEncoded), new NameValuePair("access_token", this.accessToken) });
            httpclient.executeMethod(method);
            return getShortenedUrl(method.getResponseBodyAsString());
        } catch (final Exception e) {
            throw new SystemException("Erro ao encurtar url " + url);
        }
    }

    private String getShortenedUrl(final String response) throws IOException {
        if (StringUtils.isNotEmpty(response)) {
            if (response.contains(URL)) {
                final int begin = response.indexOf(URL) + URL.length();
                final int end = response.indexOf(",", begin);
                return response.substring(begin, end).replaceAll("\"", "").replaceAll("\\\\", "");
            }
        }
        return null;
    }

}
