package br.com.fanaticosporviagens.util;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;
import org.springframework.web.context.ConfigurableWebApplicationContext;

public class XmlConfigPropertySourceInitializer implements ApplicationContextInitializer<ConfigurableWebApplicationContext> {

    private PropertySource getPropertySource() {
        final ClassPathXmlApplicationContext propertySourceContext = new ClassPathXmlApplicationContext("classpath:applicationContext-properties.xml");

        return propertySourceContext.getBean(DatabasePropertySource.class);
    }

    @Override
    public void initialize(final ConfigurableWebApplicationContext applicationContext) {
        final MutablePropertySources propertySources = applicationContext.getEnvironment().getPropertySources();
        propertySources.addFirst(getPropertySource());
    }
}
