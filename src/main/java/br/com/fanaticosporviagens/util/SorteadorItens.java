package br.com.fanaticosporviagens.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

@Component
public class SorteadorItens {

    private final Random random = new Random();

    public <T> List<T> sortear(final List<T> listaParaSorteio, final int tamanhoAmostra) {
        if (CollectionUtils.isEmpty(listaParaSorteio)) {
            return Collections.emptyList();
        }

        final List<T> sorteados = new ArrayList<T>(tamanhoAmostra);
        final List<Integer> indicesSorteados = new ArrayList<Integer>();
        Integer indiceSorteado = null;

        for (int i = 0; i < tamanhoAmostra; i++) {
            do {
                indiceSorteado = this.random.nextInt(listaParaSorteio.size());
            } while (indicesSorteados.contains(indiceSorteado));
            indicesSorteados.add(indiceSorteado);
            sorteados.add(listaParaSorteio.get(indiceSorteado));
        }
        return sorteados;
    }
}
