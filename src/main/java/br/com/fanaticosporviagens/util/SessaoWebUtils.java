package br.com.fanaticosporviagens.util;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

@Configurable("sessaoWebUtils")
public class SessaoWebUtils implements Serializable, DisposableBean {

    private static final long serialVersionUID = -8472190309484351782L;

    @Autowired
    private HttpSession httpSession;

    @Override
    public void destroy() throws Exception {
        System.out.println("DESTROY sessaoWebUtils");
        this.httpSession.removeAttribute("IDS_AMIGOS");
    }

    @SuppressWarnings("unchecked")
    public List<Long> getAmigosUsuario() {
        final List<Long> idsAmigos = (List<Long>) this.httpSession.getAttribute("IDS_AMIGOS");
        return idsAmigos;
    }
}
