package br.com.fanaticosporviagens.pesquisaTextual.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.CategoriasPesquisa;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;

/**
 * @author André Thiago
 *
 */
@Controller
public class PesquisaController extends BaseController {

    @Autowired
    private PesquisaTextualService service;

    @RequestMapping(value = "/pesquisaTextual/consultar", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public SolrResponseTripFans consultar(final LocalParamsTextualSearchQuery queryParams) {
        return this.service.consultarEntidades(queryParams);
    }

    // @RequestMapping(value = "/pesquisaTextual/consultar", method = { RequestMethod.GET }, produces = "application/json")
    // @ResponseBody
    public List<EntidadeIndexada> consultar(@RequestParam("term") final String termo, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude);
        }
        return this.service.consultarEntidades(termo);
    }

    @RequestMapping(value = "/pesquisaTextual/consultarAtracoes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarAtracoes(@RequestParam("term") final String termo, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude,
                    new LocalType[] { LocalType.ATRACAO, LocalType.LOCAL_INTERESSE_TURISTICO });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.ATRACAO, LocalType.LOCAL_INTERESSE_TURISTICO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarAtracoesGeo", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarAtracoes(@RequestParam("term") final String termo, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude, @RequestParam(required = false) final boolean geoSort) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude, geoSort, new LocalType[] { LocalType.ATRACAO,
                    LocalType.LOCAL_INTERESSE_TURISTICO });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.ATRACAO, LocalType.LOCAL_INTERESSE_TURISTICO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarCidades", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarCidades(@RequestParam("term") final String termo, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service
                    .consultarLocais(termo, latitude, longitude, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarCidadesEAeroportos", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarCidadesEAeroportos(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.CIDADE, LocalType.AEROPORTO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarCidadesEAtracoes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarCidadesEAtracoes(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.ATRACAO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarDestinos", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarDestinos(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.ATRACAO,
                LocalType.HOTEL, LocalType.RESTAURANTE });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarEstados", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarEstados(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.ESTADO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarHoteis", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarHoteis(@RequestParam("term") final String termo, @RequestParam(required = false) final Double latitude,
            @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude, new LocalType[] { LocalType.HOTEL });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.HOTEL });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarLocaisAtividades", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarLocaisAtividades(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.ATRACAO, LocalType.HOTEL, LocalType.RESTAURANTE });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarPaises", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarPaises(@RequestParam("term") final String termo) {
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.PAIS });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarRegioesTripfans", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarRegioesTripfans(@RequestParam("term") final String termo,
            @RequestParam(required = false) final Double latitude, @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service
                    .consultarLocais(termo, latitude, longitude, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarRestaurantes", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarRestaurantes(@RequestParam("term") final String termo,
            @RequestParam(required = false) final Double latitude, @RequestParam(required = false) final Double longitude) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude, new LocalType[] { LocalType.RESTAURANTE });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.RESTAURANTE });
    }

    @RequestMapping(value = "/pesquisaTextual/consultarRestaurantesGeo", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<EntidadeIndexada> consultarRestaurantes(@RequestParam("term") final String termo,
            @RequestParam(required = false) final Double latitude, @RequestParam(required = false) final Double longitude,
            @RequestParam(required = false) final boolean geoSort) {
        if (latitude != null && longitude != null) {
            return this.service.consultarLocais(termo, latitude, longitude, geoSort, new LocalType[] { LocalType.RESTAURANTE });
        }
        return this.service.consultarLocais(termo, new LocalType[] { LocalType.RESTAURANTE });
    }

    @RequestMapping(value = "/pesquisa/pesquisaGeralResultados", method = { RequestMethod.GET })
    public String lista(@RequestParam(value = "term") final String termo,
            @RequestParam(value = "codigoCategoriasPesquisa") final Integer codigoCategoriasPesquisa, final Model model) {

        if (codigoCategoriasPesquisa != null && !codigoCategoriasPesquisa.equals(0)) {
            if (codigoCategoriasPesquisa.equals(CategoriasPesquisa.PESSOAS)) {
                model.addAttribute("lista", this.service.consultarUsuarios(termo));
            } else {
                model.addAttribute("lista",
                        this.service.consultarLocais(termo, new LocalType[] { LocalType.getEnumPorCodigo(codigoCategoriasPesquisa) }));
            }
        } else {
            model.addAttribute("lista", this.service.consultarEntidades(termo));
        }
        return "/pesquisa/pesquisaGeralResultados";
    }

    public List<CategoriasPesquisa> recuperarCategoriasPesquisa() {
        return Arrays.asList(CategoriasPesquisa.values());
    }

    @RequestMapping(value = "/pesquisa/resultados", method = { RequestMethod.GET })
    public String resultados(@RequestParam(value = "term") final String termo, final Model model) {
        model.addAttribute("categorias", this.recuperarCategoriasPesquisa());
        return "/pesquisa/pesquisaGeralMenu";
    }
}
