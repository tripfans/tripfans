package br.com.fanaticosporviagens.pesquisaTextual.indexacao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.fanaticosporviagens.infra.model.repository.query.XMLQueryProcessor;
import br.com.fanaticosporviagens.model.entity.CategoriasPesquisa;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.LocalType;

/**
 * Realiza a indexação de entidades para serem usadas na pesquisa textual.
 *
 * @author André Thiago
 *
 */
public class IndexadorEntidades {

    private static final int OFFSET = 500;

    private int contador;

    private final Map<Long, String> mapTagsTripFans = new HashMap<Long, String>();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final SolrServer solrServer;

    private final String sqlQueryUsuarios;

    private final String sqlQueryUsuariosForamLocal;

    private final TransactionTemplate transactionTemplate;

    public static void main(final String[] args) throws SolrServerException, IOException {
        try {
            final long inicio = System.currentTimeMillis();
            if (args.length < 1) {
                printArgsError();
                System.exit(-1);
            }
            final IndexadorEntidades indexador = new IndexadorEntidades();

            final String operacao = args[0];
            if (operacao.equals("indexar")) {
                if (args.length < 2) {
                    printArgsError();
                }

                final String tipo = args[1];

                if (tipo.equals("local")) {
                    boolean completa = false;
                    if (args.length == 3 && args[2].equals("completa")) {
                        completa = true;
                    }
                    indexador.indexaLocais(completa);
                } else if (tipo.equals("usuario")) {
                    indexador.indexaUsuarios();
                } else if (tipo.equals("tudo")) {
                    indexador.indexaLocais(true);
                    indexador.indexaUsuarios();
                }
            } else if (operacao.equals("remover")) {
                if (args.length < 3) {
                    printArgsError();
                }
                final String tipo = args[1];
                final List<String> ids = listaIdsRemocao(tipo, args);
                indexador.removeEntidade(ids);
            } else {
                printArgsError();
            }

            final long fim = System.currentTimeMillis();
            System.out.println(String.format("TERMINOU (duração = %d) .", (fim - inicio) / 1000));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private static List<String> listaIdsRemocao(final String tipo, final String[] args) {
        final List<String> ids = new ArrayList<String>();
        String prefix = "";
        if (tipo.equals("local")) {
            prefix = EntidadeIndexada.INDEX_LOCAL_PREFIX;
        } else if (tipo.equals("usuario")) {
            prefix = EntidadeIndexada.INDEX_USUARIO_PREFIX;
        }

        for (int index = 2; index < args.length; index++) {
            ids.add(prefix + args[index]);
        }
        return ids;
    }

    private static void printArgsError() {
        System.out.println("Formato:");
        System.out.println("\t indexar <local|usuario|tudo  [completa]> (completa se a indexação de locais for completa; por padrão, é incremental)");
        System.out.println("\t remover <local|usuario> lista_ids_remocao (lista separada por espaços)");
        System.out.println("\n\nExemplos:");
        System.out.println("\t Para indexar incremental: sh indexador.sh indexar local");
        System.out.println("\t Para indexar completo: sh indexador.sh indexar local completa");
        System.out.println("\t Para remover os locais com id 10, 20 e 30: sh indexador.sh remover 10 20 30");
    }

    public IndexadorEntidades() throws SolrServerException, IOException {
        final ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext-indexer.xml");
        this.namedParameterJdbcTemplate = ctx.getBean(NamedParameterJdbcTemplate.class);
        this.transactionTemplate = new TransactionTemplate(ctx.getBean(PlatformTransactionManager.class));
        this.sqlQueryUsuarios = XMLQueryProcessor.createQuery("IndexadorEntidades.consultarUsuarios");
        this.sqlQueryUsuariosForamLocal = XMLQueryProcessor.createQuery("IndexadorEntidades.consultarUsuariosJaForamLocal");
        this.solrServer = ctx.getBean(SolrServer.class);
    }

    public void indexaLocais(final boolean completa) throws SolrServerException, IOException, NumberFormatException, InstantiationException,
            IllegalAccessException {
        if (completa) {
            this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.CIDADES.getCodigo()));
            this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.ATRACOES.getCodigo()));
            this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.HOTEIS.getCodigo()));
            this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.RESTAURANTES.getCodigo()));
            this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.AEROPORTOS.getCodigo()));
            this.solrServer.commit();
        }
        int offset = 0;
        final int limit = OFFSET;
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("offset", offset);
        params.put("limit", limit);
        params.put("incremental", !completa);
        params.put("notIncremental", completa);

        final Map<Long, EntidadeIndexada> mapEntidades = new HashMap<Long, EntidadeIndexada>();

        final String sqlTodasTags = XMLQueryProcessor.createQuery("IndexadorEntidades.consultarTodasTags", new HashMap<String, Object>());

        final List<Map<String, Object>> listaTodasTags = this.namedParameterJdbcTemplate.getJdbcOperations().queryForList(sqlTodasTags);

        for (final Map<String, Object> tag : listaTodasTags) {
            this.mapTagsTripFans.put((Long) tag.get("id"), (String) tag.get("nome"));
        }

        final String sqlEntidades = XMLQueryProcessor.createQuery("IndexadorEntidades.consultarLocais", params);
        final RowMapper<EntidadeIndexada> localRowMapper = getLocalRowMapper(mapEntidades);

        List<EntidadeIndexada> entidades = this.namedParameterJdbcTemplate.query(sqlEntidades, params, localRowMapper);
        adicionaTags(mapEntidades);

        this.contador = 0;
        while (!entidades.isEmpty()) {
            try {
                this.solrServer.addBeans(entidades);
                this.solrServer.commit();
            } catch (final Exception e) {
                e.printStackTrace();
                for (final EntidadeIndexada entidade : entidades) {
                    try {
                        System.out.println("REPROCESSANDO: id = " + entidade.getId() + " - " + entidade.getNomePais() + " - "
                                + entidade.getNomeEstado() + " - " + entidade.getNomeCidade() + " - "
                                + ((entidade.getNomeLocalTuristico() != null) ? entidade.getNomeLocalTuristico() + " - " : "") + entidade.getNome());
                        this.solrServer.addBean(entidade);
                        this.solrServer.commit();
                    } catch (final Exception exceptionEntidade) {
                        System.err.println("ERRO: id = " + entidade.getId() + " - " + entidade.getNomePais() + " - " + entidade.getNomeEstado()
                                + " - " + entidade.getNomeCidade() + " - "
                                + ((entidade.getNomeLocalTuristico() != null) ? entidade.getNomeLocalTuristico() + " - " : "") + entidade.getNome());
                        exceptionEntidade.printStackTrace();
                    }
                }
            }
            if (completa) {
                offset += OFFSET;
                params.put("offset", offset);
            } else {
                updateStatusIndexacaoLocal(entidades);
            }
            entidades = this.namedParameterJdbcTemplate.query(sqlEntidades, params, localRowMapper);
            if (CollectionUtils.isNotEmpty(entidades)) {
                adicionaTags(mapEntidades);
            }
        }
    }

    public void removeEntidade(final List<String> ids) throws SolrServerException, IOException {
        if (!ids.isEmpty()) {
            this.solrServer.deleteById(ids);
            this.solrServer.commit();
        }
    }

    public void removeLocalAutomatico() throws SolrServerException, IOException, NumberFormatException, InstantiationException,
            IllegalAccessException {
        final Map<String, Object> params = new HashMap<String, Object>();
        final List<String> ids = this.namedParameterJdbcTemplate.query(
                XMLQueryProcessor.createQuery("IndexadorEntidades.consultarIdsLocaisExcluidos"), params, getIdRowMapper());

        removeEntidade(ids);

        for (final String id : ids) {
            final Map<String, Object> paramsDelete = new HashMap<String, Object>();
            paramsDelete.put("idLocal", Long.parseLong(id));
            this.namedParameterJdbcTemplate.update(XMLQueryProcessor.createQuery("IndexadorEntidades.deleteRegistroLocalExcluido"), paramsDelete);
        }
    }

    private void adicionaTags(final Map<Long, EntidadeIndexada> mapEntidades) {

        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("ids", mapEntidades.keySet());

        final String sqlTags = XMLQueryProcessor.createQuery("IndexadorEntidades.consultarTagsLocais", params);

        this.namedParameterJdbcTemplate.query(sqlTags, params, getTagLocalRowMapper(mapEntidades));

        mapEntidades.clear();
    }

    private RowMapper<String> getIdRowMapper() {
        return new RowMapper<String>() {
            @Override
            public String mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                return rs.getString("id_local");
            }
        };
    }

    private RowMapper<EntidadeIndexada> getLocalRowMapper(final Map<Long, EntidadeIndexada> mapEntidades) {
        return new RowMapper<EntidadeIndexada>() {

            @Override
            public EntidadeIndexada mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final EntidadeIndexada entidade = new EntidadeIndexada(rs.getString("nome"));
                IndexadorEntidades.this.contador++;
                System.out.println(IndexadorEntidades.this.contador + " - Nome: " + rs.getString("nomePais") + " - " + rs.getString("nomeEstado")
                        + " - " + rs.getString("nomeCidade") + " - "
                        + ((rs.getString("nomeLocalTuristico") != null) ? rs.getString("nomeLocalTuristico") + " - " : "") + rs.getString("nome"));
                final String id = rs.getString("id");
                entidade.setId(EntidadeIndexada.INDEX_LOCAL_PREFIX + id);
                entidade.setIdCidade(rs.getString("idCidade"));
                entidade.setIdEstado(rs.getString("idEstado"));
                entidade.setIdPais(rs.getString("idPais"));
                entidade.setNome(rs.getString("nome"));
                entidade.setNomeCidade(rs.getString("nomeCidade"));
                entidade.setNomeEstado(rs.getString("nomeEstado"));
                entidade.setNomeLocalTuristico(rs.getString("nomeLocalTuristico"));
                entidade.setIdLocalTuristico(rs.getString("idLocalTuristico"));
                entidade.setNomePais(rs.getString("nomePais"));
                entidade.setIndexar(rs.getBoolean("st_indexar"));
                entidade.setTripfansRanking(Double.valueOf(rs.getString("tripfansRanking")));

                final Integer tipoEntidade = Integer.valueOf(rs.getString("tipoEntidade"));
                if (tipoEntidade.equals(LocalType.ATRACAO.getCodigo()) || tipoEntidade.equals(LocalType.HOTEL.getCodigo())
                        || tipoEntidade.equals(LocalType.RESTAURANTE.getCodigo())) {

                }
                entidade.setTipoEntidade(tipoEntidade);
                entidade.setUrlFotoAlbum(rs.getString("urlFotoAlbum"));
                entidade.setUrlFotoBig(rs.getString("urlFotoBig"));
                entidade.setUrlFotoSmall(rs.getString("urlFotoSmall"));
                entidade.setUrlPath(rs.getString("urlPath"));
                entidade.setLatitude(rs.getDouble("latitude"));
                entidade.setLongitude(rs.getDouble("longitude"));
                entidade.setEstrelas(rs.getInt("estrelas"));
                entidade.setEndereco(rs.getString("endereco"));
                entidade.setBairro(rs.getString("bairro"));
                entidade.setCep(rs.getString("cep"));
                entidade.setQuantidadeInteressesJaFoi(rs.getLong("quantidadeInteressesJaFoi"));

                if (entidade.getQuantidadeInteressesJaFoi() > 0L) {
                    int offset = 0;
                    final int limit = OFFSET;
                    final Map<String, Object> params = new HashMap<String, Object>();
                    params.put("offset", offset);
                    params.put("limit", limit);
                    params.put("idLocal", Long.valueOf(id));
                    List<Integer> idsUsuarios = IndexadorEntidades.this.namedParameterJdbcTemplate.queryForList(
                            IndexadorEntidades.this.sqlQueryUsuariosForamLocal, params, Integer.class);
                    do {
                        entidade.adicionaUsuariosJaForamLocal(idsUsuarios);
                        offset += OFFSET;
                        params.put("offset", offset);
                        idsUsuarios = IndexadorEntidades.this.namedParameterJdbcTemplate.queryForList(
                                IndexadorEntidades.this.sqlQueryUsuariosForamLocal, params, Integer.class);
                    } while (!idsUsuarios.isEmpty());
                }

                entidade.setFotoPadraoAnonima(rs.getBoolean("fotoPadraoAnonima"));
                entidade.setUsarFotoPanoramio(rs.getBoolean("usarFotoPanoramio"));
                entidade.setIdFotoPanoramio(rs.getString("idFotoPanoramio"));
                entidade.setIdUsuarioFotoPanoramio(rs.getString("idUsuarioFotoPanoramio"));
                entidade.setCidadeDestaque(rs.getBoolean("cidadeDestaque"));
                entidade.setSiglaEstado(rs.getString("siglaEstado"));
                entidade.setSiglaPais(rs.getString("siglaPais"));
                entidade.setNotaTripAdvisor(rs.getDouble("notaTripAdvisor"));
                entidade.setQuantidadeReviewsTripAdvisor(rs.getInt("quantidadeReviewsTripAdvisor"));
                entidade.setUrlBooking(rs.getString("urlBooking"));
                entidade.configuraLocalizacao();
                entidade.montaNomeExibicao();
                entidade.ajustaAgrupador();

                mapEntidades.put(entidade.getId(), entidade);

                return entidade;
            }
        };
    }

    private RowMapper<Long> getTagLocalRowMapper(final Map<Long, EntidadeIndexada> mapEntidades) {
        return new RowMapper<Long>() {

            @Override
            public Long mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final Long idTag = rs.getLong("id");
                final Long idLocal = rs.getLong("idLocal");

                final EntidadeIndexada entidade = mapEntidades.get(idLocal);

                entidade.adicionaTag(idTag, IndexadorEntidades.this.mapTagsTripFans.get(idTag));

                return idTag;
            }
        };
    }

    private RowMapper<EntidadeIndexada> getUsuarioRowMapper() {
        return new RowMapper<EntidadeIndexada>() {

            @Override
            public EntidadeIndexada mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final EntidadeIndexada entidade = new EntidadeIndexada(rs.getString("nome"));
                System.out.println("Usuário: " + rs.getString("nome"));
                entidade.setId(EntidadeIndexada.INDEX_USUARIO_PREFIX + rs.getString("id"));
                entidade.setNome(rs.getString("nome"));
                entidade.setTipoEntidade(CategoriasPesquisa.PESSOAS.getCodigo());

                final String urlFotoAlbum = rs.getString("urlFotoLocalAlbum") != null ? rs.getString("urlFotoLocalAlbum") : rs
                        .getString("urlFotoExternaAlbum");
                final String urlFotoBig = rs.getString("urlFotoLocalBig") != null ? rs.getString("urlFotoLocalBig") : rs
                        .getString("urlFotoExternaBig");
                final String urlFotoSmall = rs.getString("urlFotoLocalSmall") != null ? rs.getString("urlFotoLocalSmall") : rs
                        .getString("urlFotoExternaSmall");

                entidade.setUrlFotoAlbum(urlFotoAlbum);
                entidade.setUrlFotoBig(urlFotoBig);
                entidade.setUrlFotoSmall(urlFotoSmall);
                entidade.setUrlPath(rs.getString("urlPath"));
                entidade.montaNomeExibicao();
                entidade.ajustaAgrupador();
                return entidade;
            }
        };
    }

    private void indexaUsuarios() throws SolrServerException, IOException {
        final UpdateResponse response = this.solrServer.deleteByQuery(String.format("tipoEntidade:%s", CategoriasPesquisa.PESSOAS.getCodigo()));
        this.solrServer.commit();
        System.out.println("Resposta do delete de usuários : " + response.getResponse());

        int offset = 0;
        final int limit = OFFSET;
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("offset", offset);
        params.put("limit", limit);
        List<EntidadeIndexada> lista = this.namedParameterJdbcTemplate.query(this.sqlQueryUsuarios, params, getUsuarioRowMapper());
        do {
            this.solrServer.addBeans(lista);
            this.solrServer.commit();
            offset += OFFSET;
            params.put("offset", offset);
            lista = this.namedParameterJdbcTemplate.query(this.sqlQueryUsuarios, params, getUsuarioRowMapper());
        } while (!lista.isEmpty());
    }

    private void updateStatusIndexacaoLocal(final List<EntidadeIndexada> entidades) {
        final StringBuilder sqlUpdate = new StringBuilder();
        sqlUpdate.append("update local ");
        sqlUpdate.append("   set st_indexar = false ");
        sqlUpdate.append(" where st_indexar = true ");
        sqlUpdate.append("   and id_local = :id");
        this.transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(final TransactionStatus status) {
                IndexadorEntidades.this.namedParameterJdbcTemplate.batchUpdate(sqlUpdate.toString(),
                        SqlParameterSourceUtils.createBatch(entidades.toArray()));
                return null;
            }
        });

    }
}
