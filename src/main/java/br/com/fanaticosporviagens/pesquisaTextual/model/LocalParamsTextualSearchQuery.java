package br.com.fanaticosporviagens.pesquisaTextual.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import br.com.fanaticosporviagens.infra.model.repository.FacetField;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.LocalType;

public class LocalParamsTextualSearchQuery {

    private static Pattern LL_PATTERN = Pattern.compile("(\\(?([+-]?\\d+\\.?\\d+)\\s*,\\s*([+-]?\\d+\\.?\\d+)\\)?\\;?)+?");

    private final List<CoordenadaGeografica> coordinates = new ArrayList<CoordenadaGeografica>();

    private Map<String, Object> extraParams = new HashMap<String, Object>();

    private boolean group = false;

    private final List<FacetField> groupFields = new ArrayList<FacetField>();

    private Integer limit;

    private String near;

    private Integer offset;

    private Integer radius;

    private List<String> returnedFields;

    private boolean sortByDistance = false;

    private boolean sortByRanking = false;

    private boolean sortByType = false;

    private String term;

    private List<Integer> types = new ArrayList<Integer>();

    public LocalParamsTextualSearchQuery addExtraParam(final String name, final Object value) {
        this.extraParams.put(name, value);
        return this;
    }

    public List<CoordenadaGeografica> getCoordinates() {
        return this.coordinates;
    }

    public Map<String, Object> getExtraParams() {
        return this.extraParams;
    }

    public List<FacetField> getGroupFields() {
        if (isGroup()) {
            if (CollectionUtils.isNotEmpty(this.types))
                if (this.types.contains(LocalType.ATRACAO.getCodigo()) || this.types.contains(LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo())
                        || this.types.contains(LocalType.RESTAURANTE.getCodigo())) {
                    // inicialmente, as tags só estão associadas às atrações e restaurantes
                    this.groupFields.add(new FacetField(EntidadeIndexada.FIELD_IDS_TAGS, 1, FacetField.SORT_TYPE_COUNT));
                } else if (this.types.contains(LocalType.HOTEL.getCodigo())) {
                    this.groupFields.add(new FacetField(EntidadeIndexada.FIELD_ESTRELAS, 1, FacetField.SORT_TYPE_INDEX));
                } else if (this.types.contains(LocalType.CIDADE.getCodigo())) {
                    this.groupFields.add(new FacetField(EntidadeIndexada.FIELD_TIPO_ENTIDADE, 1, FacetField.SORT_TYPE_COUNT));
                }
            this.groupFields.add(new FacetField(EntidadeIndexada.FIELD_BAIRRO, 1, FacetField.SORT_TYPE_INDEX));
        }
        return this.groupFields;
    }

    public Integer getLimit() {
        return this.limit;
    }

    public String getNear() {
        return this.near;
    }

    public Integer getOffset() {
        return this.offset;
    }

    public Integer getRadius() {
        return this.radius;
    }

    public List<String> getReturnedFields() {
        return this.returnedFields;
    }

    public String getTerm() {
        if (StringUtils.isEmpty(this.term)) {
            return "*";
        }
        return this.term;
    }

    public List<Integer> getTypes() {
        return this.types;
    }

    public boolean hasExtraParams() {
        return !this.extraParams.isEmpty();
    }

    public boolean isGroup() {
        return this.group;
    }

    public boolean isMultiplePoint() {
        return isSpatial() && this.coordinates.size() > 1;
    }

    public boolean isSortByDistance() {
        return this.sortByDistance;
    }

    public boolean isSortByRanking() {
        return this.sortByRanking;
    }

    public boolean isSortByType() {
        return this.sortByType;
    }

    public boolean isSpatial() {
        return CollectionUtils.isNotEmpty(this.coordinates);
    }

    public void setExtraParams(final Map<String, Object> extraParams) {
        this.extraParams = extraParams;
    }

    public void setGroup(final boolean group) {
        this.group = group;
    }

    public void setLimit(final Integer limit) {
        this.limit = limit;
    }

    public void setLl(final String ll) {
        final Matcher matcher = LL_PATTERN.matcher(ll);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Error: ll parameter invalid!");
        }

        fillCoordinates(ll);
    }

    public void setNear(final String near) {
        this.near = near;
    }

    public void setOffset(final Integer offset) {
        this.offset = offset;
    }

    public void setRadius(final Integer radius) {
        this.radius = radius;
    }

    public void setReturnedFields(final List<String> returnedFields) {
        this.returnedFields = returnedFields;
    }

    public void setSortByDistance(final boolean sortByDistance) {
        this.sortByDistance = sortByDistance;
    }

    public void setSortByRanking(final boolean sortByRanking) {
        this.sortByRanking = sortByRanking;
    }

    public void setSortByType(final boolean sortByType) {
        this.sortByType = sortByType;
    }

    public void setTerm(final String term) {
        this.term = term;
    }

    public void setTypes(final List<Integer> types) {
        this.types = types;
    }

    public LocalParamsTextualSearchQuery sortByDistance() {
        setSortByDistance(true);
        return this;
    }

    public LocalParamsTextualSearchQuery sortByRanking() {
        setSortByRanking(true);
        return this;
    }

    public LocalParamsTextualSearchQuery sortByType() {
        setSortByType(true);
        return this;
    }

    public LocalParamsTextualSearchQuery withExtraParam(final String name, final Object value) {
        this.extraParams.put(name, value);
        return this;
    }

    public LocalParamsTextualSearchQuery withLimit(final Integer limit) {
        setLimit(limit);
        return this;
    }

    public LocalParamsTextualSearchQuery withType(final Integer type) {
        this.types.add(type);
        return this;
    }

    public LocalParamsTextualSearchQuery withTypes(final List<Integer> types) {
        setTypes(types);
        return this;
    }

    private void fillCoordinates(final String ll) {
        final String ll2Convert = ((ll.trim()).replaceAll("\\(", "")).replaceAll("\\)", "");
        final String[] coordsPair = ll2Convert.split(";");

        for (final String pair : coordsPair) {
            final String[] c = pair.split(",");
            final CoordenadaGeografica coordenada = new CoordenadaGeografica(Double.valueOf(c[0]), Double.valueOf(c[1]));
            this.coordinates.add(coordenada);
        }
    }

}
