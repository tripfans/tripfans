package br.com.fanaticosporviagens.pesquisaTextual.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;

public class TripFansSolrQueryParams {

    private final List<CoordenadaGeografica> coords = new ArrayList<CoordenadaGeografica>();

    private final Map<String, Object> extraParams = new HashMap<String, Object>();

    private final List<String> returnedFields = new ArrayList<String>();

    private String sort;

    private String term;

    public TripFansSolrQueryParams addExtraParam(final String name, final Object value) {
        this.extraParams.put(name, value);
        return this;
    }

    public TripFansSolrQueryParams addGeoCoord(final CoordenadaGeografica coordenadaGeografica) {
        this.coords.add(coordenadaGeografica);
        return this;
    }

    public TripFansSolrQueryParams addReturnedField(final String fieldName, final String... otherFields) {
        this.returnedFields.add(fieldName);
        if (otherFields != null) {
            for (final String field : otherFields) {
                this.returnedFields.add(field);
            }
        }
        return this;
    }

    public List<CoordenadaGeografica> getCoords() {
        return this.coords;
    }

    public Map<String, Object> getExtraParams() {
        return this.extraParams;
    }

    public List<String> getReturnedFields() {
        return this.returnedFields;
    }

    public String getSort() {
        return this.sort;
    }

    public String getTerm() {
        return this.term;
    }

    public TripFansSolrQueryParams sortBy(final String sort) {
        this.sort = sort;
        return this;
    }

    public TripFansSolrQueryParams withTerm(final String term) {
        this.term = term;
        return this;
    }

}
