package br.com.fanaticosporviagens.pesquisaTextual.model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.Indexavel;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.repository.PesquisaTextualRepository;

/**
 * @author André Thiago
 *
 */
@Service
public class PesquisaTextualService extends BaseService {

    @Autowired
    private PesquisaTextualRepository pesquisaTextualRepository;

    public SolrResponseTripFans consultar(final ParametrosPesquisaTextual parametrosPesquisaTextual) {
        return this.pesquisaTextualRepository.consultar(parametrosPesquisaTextual);
    }

    public SolrResponseTripFans consultarAtracoes(final LocalParamsTextualSearchQuery queryParams) {
        return this.pesquisaTextualRepository.consultarEntidades(queryParams);
    }

    public SolrResponseTripFans consultarEntidades(final LocalParamsTextualSearchQuery queryParams) {
        return this.pesquisaTextualRepository.consultarEntidades(queryParams);
    }

    /**
     * Realiza a pesquisa textual por entidades, independentemente do seu tipo.
     *
     * @param termoPesquisa
     *            o termo que vai ser utilizado como filtro da pesquisa
     * @return
     */
    public List<EntidadeIndexada> consultarEntidades(final String termoPesquisa) {
        final List<EntidadeIndexada> locais = this.consultarLocais(termoPesquisa, new LocalType[] { LocalType.CIDADE,
                LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.ATRACAO, LocalType.HOTEL, LocalType.RESTAURANTE });
        final List<EntidadeIndexada> usuarios = this.consultarUsuarios(termoPesquisa);

        final int quantidadeLocais = locais.size();

        final List<EntidadeIndexada> entidades = new ArrayList<EntidadeIndexada>();
        if (!usuarios.isEmpty()) {
            final int size = quantidadeLocais - 4;
            for (int i = 0; i < size; i++) {
                entidades.add(locais.get(i));
            }
            entidades.addAll(usuarios);
        } else {
            entidades.addAll(locais);
        }

        return entidades;
    }

    public SolrResponseTripFans consultarHoteis(final LocalParamsTextualSearchQuery queryParams) {
        return this.pesquisaTextualRepository.consultarEntidades(queryParams);
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude) {
        return this.pesquisaTextualRepository.consultarLocais(termoPesquisa, latitude, longitude);
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude, final boolean geoSort,
            final LocalType[] localTypes) {
        return this.pesquisaTextualRepository.consultarLocais(termoPesquisa, latitude, longitude, geoSort, localTypes);
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude,
            final LocalType[] localTypes) {
        return this.pesquisaTextualRepository.consultarLocais(termoPesquisa, latitude, longitude, localTypes);
    }

    /**
     * Realiza a pesquisa textual por locais filtrando por tipo de local, por exemplo, cidade, estado etc.
     *
     * @param termoPesquisa
     * @param localTypes
     *            os tipos de locais que deseja ser retornado na consulta
     * @return
     */
    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final LocalType[] localTypes) {
        return this.pesquisaTextualRepository.consultarLocais(termoPesquisa, localTypes);
    }

    public SolrResponseTripFans consultarRestaurantes(final LocalParamsTextualSearchQuery queryParams) {
        return this.pesquisaTextualRepository.consultarEntidades(queryParams);
    }

    /**
     * Realiza a pesquisa textual por usuários.
     *
     * @param termoPesquisa
     *
     * @return
     */
    public List<EntidadeIndexada> consultarUsuarios(final String termoPesquisa) {
        return this.pesquisaTextualRepository.consultarUsuarios(termoPesquisa);
    }

    public void indexar(final Indexavel indexavel) {
        final EntidadeIndexada entidadeIndexada = new EntidadeIndexada(indexavel);
        this.pesquisaTextualRepository.index(entidadeIndexada);

    }

}
