package br.com.fanaticosporviagens.pesquisaTextual.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.infra.util.Stopwords;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;

/**
 * @author André Thiago
 *
 */
@Component
public class SolrQueryFactory {

    private static final char[] SPECIAL_CHARS = { '+', '-', '&', '!', '(', ')', '{', '}', '[', ']', '^', '\"', '~', '*', '?', ':', '\\', '/' };

    private static final String[] SPECIAL_WORDS = { "AND", "OR" };

    public List<SolrQuery> buildQuery(final LocalParamsTextualSearchQuery query) {

        if (query.isSpatial()) {
            final List<SolrQuery> queries = new ArrayList<SolrQuery>();
            for (final CoordenadaGeografica coordenada : query.getCoordinates()) {
                queries.add(buildSpatialQuery(query, coordenada));
            }

            return queries;
        } else {
            return Collections.singletonList(buildNonSpatialQuery(query));
        }

    }

    public SolrQuery buildQuery(final Map<String, Object> filtrosAdicionais) {
        final SolrQuery query = new SolrQuery();
        final String queryFiltros = "(" + getQueryByFilters(filtrosAdicionais) + ")";
        query.setQuery(queryFiltros);
        return query;
    }

    /**
     * Constrói uma {@link SolrQuery} baseada somente no termo de pesquisa.
     *
     * @param termoPesquisa
     *            o termo de pesquisa que será utilizado para montar a query.
     * @return
     */
    public SolrQuery buildQuery(final String termoPesquisa) {
        final SolrQuery query = new SolrQuery();
        final String termo = getTermWithoutSpecialCharsAndSpecialWords(termoPesquisa);
        query.setQuery(getEdgedQuery(termo) + " OR (" + getTokenizedQuery(termo) + ")");
        return query;
    }

    public SolrQuery buildQuery(final String termoPesquisa, final Double latitude, final Double longitude) {
        final SolrQuery query = new SolrQuery();
        final String termo = getTermWithoutSpecialCharsAndSpecialWords(termoPesquisa);
        query.setQuery(getEdgedQuery(termo) + " OR (" + getTokenizedQuery(termo) + ")");
        addParamsLocalizacao(query, latitude, longitude);
        return query;
    }

    public SolrQuery buildQuery(final String termoPesquisa, final Double latitude, final Double longitude, final Map<String, Object> filtrosAdicionais) {
        final SolrQuery query = buildQuery(termoPesquisa, filtrosAdicionais);
        addParamsLocalizacao(query, latitude, longitude);
        return query;
    }

    /**
     * Constrói uma {@link SolrQuery} baseada no termo de pesquisa e nos filtros adicionais. Esses filtros serão
     * adicionados como uma cláusula adicional na query (AND) sendo que, se houver mais de um eles serão montados
     * conforme uma conjunção (OR). <br/>
     * Por exemplo, suponha que seja passado o termo de pesquisa Rio e os filtros adicionais tipoLocal = 2 e tipoLocal = 4.
     * A query será montada da seguinte maneira: <br/>
     * <br/>
     * ((campoPesquisaNGram:"Rio" or (campoPesquisaTokens:Rio*)) and (tipoLocal:2 or tipoLocal:4)
     *
     * @param termoPesquisa
     *            o termo de pesquisa que será utilizad para montar a query.
     * @param filtrosAdicionais
     *            filtros adicionais do tipo chave/valor
     * @return
     */
    public SolrQuery buildQuery(final String termoPesquisa, final Map<String, Object> filtrosAdicionais) {
        final SolrQuery query = new SolrQuery();
        final String termo = getTermWithoutSpecialCharsAndSpecialWords(termoPesquisa);
        final String queryBase = "(" + getEdgedQuery(termo) + " OR (" + getTokenizedQuery(termo) + "))";
        final String queryFiltros = "(" + getQueryByFilters(filtrosAdicionais) + ")";
        query.setQuery(queryBase + " AND " + queryFiltros);
        return query;
    }

    public SolrQuery buildQuery(final TripFansSolrQueryParams params) {

        final SolrQuery query = new SolrQuery();

        final String termo = getTermWithoutSpecialCharsAndSpecialWords(params.getTerm());
        final String queryBase = "(" + getEdgedQuery(termo) + " OR (" + getTokenizedQuery(termo) + "))";
        query.setQuery(queryBase);

        query.setFilterQueries(getQueryByFilters(params.getExtraParams()));

        final List<CoordenadaGeografica> coords = params.getCoords();

        if (CollectionUtils.isNotEmpty(coords)) {
            query.set("spatial", true);
        }

        for (final CoordenadaGeografica coord : coords) {
            query.set("sfield", "localizacao");
            query.set("pt", "" + coord.getLatitude() + "," + coord.getLongitude());
        }

        query.setSortField(params.getSort(), ORDER.asc);

        return query;
    }

    private void addExtraParams(final LocalParamsTextualSearchQuery query, final SolrQuery solrQuery) {
        if (query.hasExtraParams()) {
            solrQuery.addFilterQuery(getQueryByFilters(query.getExtraParams()));
        }
    }

    private void addParamsLocalizacao(final SolrQuery query, final Double latitude, final Double longitude) {
        if (latitude != null & longitude != null) {
            query.setFilterQueries("{!bbox}");
            query.set("sfield", "localizacao");
            query.set("pt", "" + latitude + "," + longitude);
            query.set("d", "0.05");
        }
    }

    private void addSortParams(final LocalParamsTextualSearchQuery query, final SolrQuery solrQuery) {
        if (query.isSortByType()) {
            /*solrQuery.add("group", new String[] { "true" });
            solrQuery.add("group.field", new String[] { "tipoAgrupador" });
            solrQuery.add("group.main", new String[] { "true" });
            solrQuery.add("group.limit", new String[] { "3" });*/
            // solrQuery.addSortField("tipoEntidade", ORDER.asc);
        }
        if (query.isSortByDistance()) {
            solrQuery.addSortField("geodist()", ORDER.asc);
        }
        if (query.isSortByRanking()) {
            solrQuery.addSortField("tripfansRanking", ORDER.desc);
        }

    }

    private void addSpatialParams(final LocalParamsTextualSearchQuery query, final CoordenadaGeografica coordenada, final SolrQuery solrQuery) {
        if (query.isSpatial()) {
            solrQuery.set("spatial", true);
            solrQuery.set("sfield", "localizacao");

            solrQuery.set("pt", coordenada.getLatitude() + "," + coordenada.getLongitude());

            if (query.getRadius() != null) {
                solrQuery.set("d", query.getRadius() / 1000);
                solrQuery.addFilterQuery("{!bbox}");
            }
        }
    }

    private void addTypeToFilterQueryParam(final LocalParamsTextualSearchQuery query, final SolrQuery solrQuery) {
        if (CollectionUtils.isNotEmpty(query.getTypes())) {
            final Map<String, Object> typeParams = new HashMap<String, Object>();
            typeParams.put("tipoEntidade", query.getTypes());
            solrQuery.addFilterQuery(getQueryByFilters(typeParams));
        }
    }

    private SolrQuery buildNonSpatialQuery(final LocalParamsTextualSearchQuery query) {
        final SolrQuery solrQuery = buildQuery(query.getTerm());
        addTypeToFilterQueryParam(query, solrQuery);
        addExtraParams(query, solrQuery);
        if (CollectionUtils.isNotEmpty(query.getReturnedFields())) {
            for (final String field : query.getReturnedFields()) {
                solrQuery.addField(field);
            }
        }
        addSortParams(query, solrQuery);
        return solrQuery;
    }

    private SolrQuery buildSpatialQuery(final LocalParamsTextualSearchQuery query, final CoordenadaGeografica coordenada) {
        final SolrQuery solrQuery = buildNonSpatialQuery(query);
        addSpatialParams(query, coordenada, solrQuery);
        addSortParams(query, solrQuery);
        return solrQuery;
    }

    private String getEdgedQuery(final String termoPesquisa) {
        return "edgyTextField:\"" + termoPesquisa + "\"";
    }

    private String getQueryByFilters(final Map<String, Object> filtrosAdicionais) {
        final StringBuffer buffer = new StringBuffer();
        for (final String key : filtrosAdicionais.keySet()) {
            final Object value = filtrosAdicionais.get(key);
            if (value != null) {
                if (isMultiValue(value)) {
                    final StringBuffer filterBuffer = new StringBuffer();
                    for (final Object obj : valueAsList(value)) {
                        if (filterBuffer.length() > 0) {
                            filterBuffer.append(" OR ");
                        }
                        filterBuffer.append(key + ":" + (obj instanceof String ? ("'" + obj + "'") : obj));
                    }
                    if (buffer.length() > 0 && filterBuffer.length() > 0) {
                        buffer.append(" AND ");
                    }
                    buffer.append("(" + filterBuffer.toString() + ")");
                } else {
                    if (buffer.length() > 0) {
                        buffer.append(" AND ");
                    }
                    buffer.append(key + ":" + value);
                }
            }
        }
        return buffer.toString();
    }

    private String getTermWithoutSpecialCharsAndSpecialWords(final String term) {
        final StringBuilder sb = new StringBuilder();
        final char[] charArray = term.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            if (!ArrayUtils.contains(SPECIAL_CHARS, charArray[i])) {
                sb.append(charArray[i]);
            }
        }
        final String termWithoutSpecialChars = sb.toString().trim().replaceAll(" +", " ");
        final String[] tokens = termWithoutSpecialChars.split(" ");
        final StringBuffer buffer = new StringBuffer();
        if (tokens.length > 1) {
            for (final String token : tokens) {
                if (!ArrayUtils.contains(SPECIAL_WORDS, token)) {
                    buffer.append(token);
                    buffer.append(" ");
                }
            }
            return Stopwords.removeStopWords(buffer.toString()).trim();
        }
        return termWithoutSpecialChars;
    }

    private String getTokenizedQuery(final String termoPesquisa) {
        final List<String> tokens = new ArrayList<String>(Arrays.asList(termoPesquisa.split(" ")));
        final StringBuffer buffer = new StringBuffer();
        for (int index = 0; index < tokens.size(); index++) {
            if (index > 0) {
                buffer.append(" AND ");
            }
            final String token = tokens.get(index);
            buffer.append("tokenField:" + token);
            if (index == tokens.size() - 1) {
                buffer.append("*");
            }
        }
        return buffer.toString();
    }

    private boolean isMultiValue(final Object value) {
        if (value == null) {
            return false;
        }
        return (value.getClass().isArray() || Collection.class.isAssignableFrom(value.getClass()));
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private Collection<?> valueAsList(final Object value) {
        if (value.getClass().isArray()) {
            return Arrays.asList((Object[]) value);
        } else if (Collection.class.isAssignableFrom(value.getClass())) {
            return new ArrayList((Collection) value);
        }
        return null;
    }

}
