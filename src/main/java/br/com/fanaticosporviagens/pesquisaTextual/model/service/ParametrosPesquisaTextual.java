package br.com.fanaticosporviagens.pesquisaTextual.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.fanaticosporviagens.infra.model.repository.FacetField;
import br.com.fanaticosporviagens.infra.model.repository.SortDirection;
import br.com.fanaticosporviagens.infra.model.repository.SortField;

public class ParametrosPesquisaTextual {

    private List<FacetField> agrupamentos = new ArrayList<FacetField>();

    private Integer inicioPaginacao;

    private Integer limitePaginacao;

    private List<SortField> ordenacoes = new ArrayList<SortField>();

    private final Map<String, Object> parametros = new HashMap<String, Object>();

    private String termoPrincipal;

    public ParametrosPesquisaTextual() {
    }

    public ParametrosPesquisaTextual(final String termoPrincipal) {
        this.termoPrincipal = termoPrincipal;
    }

    public ParametrosPesquisaTextual addAgrupamento(final FacetField agrupamento) {
        this.agrupamentos.add(agrupamento);
        return this;
    }

    public ParametrosPesquisaTextual addOrdenacao(final SortField ordenacao) {
        this.ordenacoes.add(ordenacao);
        return this;
    }

    public ParametrosPesquisaTextual addOrdenacao(final String campo, final SortDirection direcao) {
        this.ordenacoes.add(new SortField(campo, direcao));
        return this;
    }

    public ParametrosPesquisaTextual addParametro(final String nome, final Object valor) {
        this.parametros.put(nome, valor);
        return this;
    }

    public List<FacetField> getAgrupamentos() {
        return this.agrupamentos;
    }

    public Integer getInicioPaginacao() {
        return this.inicioPaginacao;
    }

    public Integer getLimitePaginacao() {
        return this.limitePaginacao;
    }

    public List<SortField> getOrdenacoes() {
        return this.ordenacoes;
    }

    public Map<String, Object> getParametros() {
        return this.parametros;
    }

    public String getTermoPrincipal() {
        return this.termoPrincipal;
    }

    public ParametrosPesquisaTextual setAgrupamentos(final FacetField... agrupamentos) {
        this.agrupamentos = Arrays.asList(agrupamentos);
        return this;
    }

    public ParametrosPesquisaTextual setAgrupamentos(final List<FacetField> agrupamentos) {
        this.agrupamentos = agrupamentos;
        return this;
    }

    public ParametrosPesquisaTextual setOrdenacoes(final List<SortField> ordenacoes) {
        this.ordenacoes = ordenacoes;
        return this;
    }

    public ParametrosPesquisaTextual setOrdenacoes(final SortField... ordenacoes) {
        this.ordenacoes = Arrays.asList(ordenacoes);
        return this;
    }

    public ParametrosPesquisaTextual setPaginacao(final Integer inicio, final Integer limite) {
        this.inicioPaginacao = inicio;
        this.limitePaginacao = limite;
        return this;
    }

    public void setTermoPrincipal(final String termoPrincipal) {
        this.termoPrincipal = termoPrincipal;
    }

}
