package br.com.fanaticosporviagens.pesquisaTextual.model.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.model.repository.SolrRepository;
import br.com.fanaticosporviagens.infra.model.repository.SortDirection;
import br.com.fanaticosporviagens.infra.model.repository.SortField;
import br.com.fanaticosporviagens.model.entity.CategoriasPesquisa;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.ParametrosPesquisaTextual;

@Repository
public class PesquisaTextualRepository extends SolrRepository<EntidadeIndexada> {

    public SolrResponseTripFans consultar(final ParametrosPesquisaTextual parametrosPesquisaTextual) {
        return query(parametrosPesquisaTextual);
    }

    public SolrResponseTripFans consultarEntidades(final LocalParamsTextualSearchQuery queryParams) {
        return query(queryParams);
    }

    /**
     * Realiza a pesquisa textual por entidades, independentemente do seu tipo.
     *
     * @param termoPesquisa
     *            o termo que vai ser utilizado como filtro da pesquisa
     * @return
     */
    public List<EntidadeIndexada> consultarEntidades(final String termoPesquisa) {
        return query(termoPesquisa, getDefaultSortFields());
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude) {
        return query(termoPesquisa, latitude, longitude, getDefaultSortFields());
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude, final boolean geoSort,
            final LocalType[] localTypes) {
        final Integer[] valores = new Integer[localTypes.length];
        for (int i = 0; i < valores.length; i++) {
            valores[i] = localTypes[i].getCodigo();
        }
        final Map<String, Object> filtros = new HashMap<String, Object>();
        filtros.put("tipoEntidade", valores);
        return query(termoPesquisa, latitude, longitude, geoSort, filtros, getDefaultSortFields());
    }

    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final Double latitude, final Double longitude,
            final LocalType[] localTypes) {
        final Integer[] valores = new Integer[localTypes.length];
        for (int i = 0; i < valores.length; i++) {
            valores[i] = localTypes[i].getCodigo();
        }
        final Map<String, Object> filtros = new HashMap<String, Object>();
        filtros.put("tipoEntidade", valores);
        return query(termoPesquisa, latitude, longitude, filtros, getDefaultSortFields());
    }

    /**
     * Realiza a pesquisa textual por locais filtrando por tipo de local, por exemplo, cidade, estado etc.
     *
     * @param termoPesquisa
     * @param localTypes
     *            os tipos de locais que deseja ser retornado na consulta
     * @return
     */
    public List<EntidadeIndexada> consultarLocais(final String termoPesquisa, final LocalType[] localTypes) {
        final Integer[] valores = new Integer[localTypes.length];
        for (int i = 0; i < valores.length; i++) {
            valores[i] = localTypes[i].getCodigo();
        }
        final Map<String, Object> filtros = new HashMap<String, Object>();
        filtros.put("tipoEntidade", valores);
        return query(termoPesquisa, filtros, getDefaultSortFields());
    }

    public List<EntidadeIndexada> consultarLocaisSemelhantes(final String nome, final String cidade, final String estado, final String pais,
            final CoordenadaGeografica coordenadaGeografica, final LocalType[] tiposLocal) {
        final Map<String, Object> filtros = new HashMap<String, Object>();
        if (tiposLocal != null && tiposLocal.length > 0) {
            final Integer[] valores = new Integer[tiposLocal.length];
            for (int i = 0; i < valores.length; i++) {
                valores[i] = tiposLocal[i].getCodigo();
            }
            filtros.put("tipoEntidade", valores);
        }
        return query(nome, filtros, getDefaultSortFields());
    }

    /**
     * Realiza a pesquisa textual por usuários.
     *
     * @param termoPesquisa
     *
     * @return
     */
    public List<EntidadeIndexada> consultarUsuarios(final String termoPesquisa) {
        final Map<String, Object> filtros = new HashMap<String, Object>();
        filtros.put("tipoEntidade", CategoriasPesquisa.PESSOAS.getCodigo());
        return query(termoPesquisa, filtros, getDefaultSortFields());
    }

    private SortField[] getDefaultSortFields() {
        return new SortField[] { new SortField("tipoAgrupador", SortDirection.ASC), new SortField("tripfansRanking", SortDirection.DESC),
                new SortField("score", SortDirection.DESC) };
    }

}
