package br.com.fanaticosporviagens.model.entity;

import java.util.Locale;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

/*
 * Comentários 
 * LGBTT - (Lésbicas, Gays, Bissexuais, Travestis e Transexuais) Esse aqui não sei se nos referimos dessa forma.
 * Gastronomia (lembrar que tem o item restaurante)
 */
public enum TipoQualificacaoAtracao {
    ARQUITETURA(1),
    ARTE_CULTURA(2),
    AVENTURA(3),
    BUNGEE_JUMPING(4),
    CASTELOS_PALACIOS(5),
    CICLISMO(6),
    CINEMA(7),
    ECO_TURISMO(8),
    ESTUDO_EXTERIOR(9),
    FAMILIA(10),
    FERIAS_ESCOLARES(11),
    FESTIVAIS(12),
    FOTOGRAFIA(13),
    GASTRONOMIA(14),
    HISTORICO(15),
    LGBTT(16),
    LUXO_TOTAL(17),
    MELHOR_IDADE(18),
    MERGULHO_SNORKELING(19),
    MOCHILEIRO(20),
    MUSEUS(21),
    MUSICA(22),
    NEGOCIOS(23),
    PARA_MULHERES(24),
    PARAQUEDISMO(25),
    PARQUES(26),
    PARQUES_TEMATICOS(27),
    PESCARIA(28),
    PRAIA(29),
    RELIGIOSO(30),
    ROMANTICO(31),
    SELVA_FLORESTA(32),
    SKI_SNOWBOARD(33),
    SOLTEIROS(34),
    SURFE(35),
    TEATRO(36),
    TRILHAS_CAMINHADAS(37),
    VIAGEM_CARRO(38),
    VOO_LIVRE(39),
    WINDSURFE(40),
    ZOOLOGICO(41);

    private int codigo;

    private final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    TipoQualificacaoAtracao(final int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.messageSource.getMessage(this.getClass().getSimpleName() + "." + name(), null, Locale.getDefault());
    }
}
