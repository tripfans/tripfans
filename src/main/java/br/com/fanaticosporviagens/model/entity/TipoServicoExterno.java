package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoServicoExterno implements EnumTypeInteger {

    EMAIL(1, new String[] { "Email" }),
    SOCIAL_NETWORK(3, new String[] { "Rede Social" }),
    WEBSITE_BLOG(2, new String[] { "Website/Blog" });

    private final EnumI18nUtil util;

    TipoServicoExterno(final int codigo, final String[] descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}