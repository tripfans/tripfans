package br.com.fanaticosporviagens.model.entity;

import static br.com.fanaticosporviagens.model.entity.CategoriaNotificacaoPorEmail.AMIGOS;
import static br.com.fanaticosporviagens.model.entity.CategoriaNotificacaoPorEmail.COMENTARIOS;
import static br.com.fanaticosporviagens.model.entity.CategoriaNotificacaoPorEmail.NOTICIAS_OFERTAS_ATUALIZACOES;
import static br.com.fanaticosporviagens.model.entity.CategoriaNotificacaoPorEmail.VIAGENS;
import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoNotificacaoPorEmail implements EnumTypeInteger {

    AMIGOS_ACEITAR_CONVITE_TRIPFANS(1, AMIGOS, "aceitarConviteTripfans", "Quando alguém entrar para o TripFans após seu convite"),
    AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_AMIZADE(2, AMIGOS, "receberConfirmacoesOuSolicitacoesAmizade",
                                                        "Quando eu receber solicitações ou confirmações de amizade"),
    // AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_PARTICIPACAO_EM_GRUPO(3, AMIGOS, "receberConfirmacoesOuSolicitacoesGrupo",
    // "Quando eu receber solicitações ou confirmações para participar de um grupo"),

    COMENTARIOS_COMENTAR_EM_DIARIO_VIAGEM(4, COMENTARIOS, "receberComentariosDiarioViagem", "Quando alguém comentar no meu diário de viagem"),
    COMENTARIOS_COMENTAR_EM_FOTOS(5, COMENTARIOS, "receberComentariosFotos", "Quando alguém comentar minhas fotos"),
    // COMENTARIOS_COMENTAR_EM_PLANO_VIAGEM(6, COMENTARIOS, "receberComentariosPlanoViagem", "Quando alguém comentar no meu plano de viagem"),
    COMENTARIOS_COMENTAR_EM_RELATOS_DICAS_AVALIACOES(7, COMENTARIOS, "receberComentariosDicasRelatosAvaliacoes",
                                                     "Quando alguém comentar meus relatos, dicas ou avaliações"),
    COMENTARIOS_COMENTAR_NO_PERFIL(8, COMENTARIOS, "receberComentariosPerfil", "Quando alguém comentar no meu perfil"),
    COMENTARIOS_RECEBER_INDICAO_UTIL(9, COMENTARIOS, "receberIndicacoesDeUtil",
                                     "Quando meus relatos, dicas, respostas ou avaliações forem indicados como úteis"),
    COMENTARIOS_RECEBER_RESPOSTAS_PERGUNTAS_OU_COMENTARIOS(10, COMENTARIOS, "receberRespostasPerguntasComentarios",
                                                           "Quando alguém responder minhas perguntas ou meus comentários"),

    DICA_RECEBER_RESPOSTA_PEDIDO_DICA(22, VIAGENS, "receberRespostaPedidoDica", "Quando meus amigos responderem aos meus pedidos de dicas"),
    DICA_RECEBER_SOLICITACAO_PEDIDO_DICA(21, VIAGENS, "receberPedidoDica", "Quando meus amigos me pedirem dicas"),
    NOTICIAS_RECEBER_NOTICIAS_SOBRE_LUGARES_QUE_PLANEJA(11, NOTICIAS_OFERTAS_ATUALIZACOES, "receberNoticiasSobreLugaresQuePlaneja",
                                                        "Desejo receber notícias semanais sobre lugares que estou planejando ir"),
    NOTICIAS_RECEBER_NOTICIAS_SOBRE_LUGARES_QUE_SEGUE(12, NOTICIAS_OFERTAS_ATUALIZACOES, "receberNoticiasSobreLugaresQueSegue",
                                                      "Desejo receber notícias semanais sobre lugares que estou seguindo"),
    NOTICIAS_RECEBER_NOVIDADES(13, NOTICIAS_OFERTAS_ATUALIZACOES, "receberNovidades", "Desejo receber novidades do Tripfans"),

    NOTICIAS_RECEBER_OFERTAS(14, NOTICIAS_OFERTAS_ATUALIZACOES, "receberOfertas", "Desejo receber ofertas/promoções do Tripfans"),
    VIAGENS_ACEITAR_CONVITE_PARA_PARTICIPAR_DE_VIAGEM(15, VIAGENS, "aceitarConviteViagem",
            "Quando alguém aceitar meu convite para participar de uma viagem"),
    VIAGENS_ACEITAR_RECOMENDACAO_SOBRE_LUGARES(16, VIAGENS, "aceitarRecomendacaoLugares", "Quando alguém aceitar minhas recomendações sobre lugares"),
    VIAGENS_RECEBER_CONVITE_PARA_PARTICIPAR_DE_VIAGEM(17, VIAGENS, "receberConviteViagem",
            "Quando eu for convidado para participar de uma viagem"),
    VIAGENS_RECEBER_RECOMENDACAO_SOBRE_LUGARES(18, VIAGENS, "receberRecomendacaoLugares", "Quando alguém me recomendar algum lugar"),
    VIAGENS_RECEBER_SOLICITACAO_PARA_RECOMENDAR_SOBRE_LUGARES(19, VIAGENS, "receberSolicitacaoParaRecomendarLugares",
                                                              "Quando eu for solicitado para recomendar sobre lugares"),
    VIAGENS_RECEBER_SOLICITACAO_PARA_RECOMENDAR_SOBRE_VIAGEM(20, VIAGENS, "receberSolicitacaoParaRecomendarViagem",
                                                             "Quando eu for solicitado para recomendar sobre viagens");

    private final String atributoDaClasse;

    private final CategoriaNotificacaoPorEmail categoria;

    private final EnumI18nUtil util;

    private TipoNotificacaoPorEmail(final Integer codigo, final CategoriaNotificacaoPorEmail categoria, final String atributoDaClasse,
            final String... descricoes) {
        this.categoria = categoria;
        this.atributoDaClasse = atributoDaClasse;
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    public String getAtributoDaClasse() {
        return this.atributoDaClasse;
    }

    public CategoriaNotificacaoPorEmail getCategoria() {
        return this.categoria;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }
}