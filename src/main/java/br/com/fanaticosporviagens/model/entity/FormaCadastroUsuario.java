package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * 
 * @author @author Carlos Nascimento
 * 
 */
public enum FormaCadastroUsuario implements EnumTypeInteger {

    FACEBOOK(2, "Facebook"),
    FOURSQUARE(5, "Foursquare"),
    GOOGLE(3, "Google"),
    TRIPFANS(1, "Tripfans"),
    TWITTER(4, "Twitter");

    private final EnumI18nUtil util;

    FormaCadastroUsuario(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}
