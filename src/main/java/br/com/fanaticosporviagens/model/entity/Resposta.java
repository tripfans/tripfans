package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * @author André Thiago
 * 
 */
@Entity
@Table(name = "resposta")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_resposta", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_resposta", nullable = false, unique = true)) })
public class Resposta extends EntidadeGeradoraAcao<Long> implements AlvoAcao, UrlNameable, Comentavel, Votavel {

    private static final long serialVersionUID = 3473415481139005127L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pergunta", nullable = false)
    private Pergunta pergunta;

    @Column(name = "qtd_voto_util", nullable = false)
    private Long quantidadeVotoUtil = 0L;

    @Column(name = "texto_resposta", length = 1000, nullable = false)
    @NotNull
    private String textoResposta;

    @Column(name = "url_path", nullable = false, unique = true)
    private String urlPath;

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.pergunta;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public Date getData() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    @Override
    public String getDescricaoAlvo() {
        return getPergunta().getDescricaoAlvo();
    }

    public Pergunta getPergunta() {
        return this.pergunta;
    }

    @Override
    public String getPrefix() {
        return "r";
    }

    @Override
    public Long getQuantidadeVotoUtil() {
        return this.quantidadeVotoUtil;
    }

    public String getTextoResposta() {
        return this.textoResposta;
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    @Override
    public void incrementaQuantidadeVotoUtil() {
        this.quantidadeVotoUtil += 1;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setData(final Date data) {
        if (data != null) {
            this.setDataCriacao(new LocalDateTime(data));
        }
    }

    public void setPergunta(final Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    public void setQuantidadeVotoUtil(final Long quantidadeVotoUtil) {
        this.quantidadeVotoUtil = quantidadeVotoUtil;
    }

    public void setTextoResposta(final String textoResposta) {
        this.textoResposta = textoResposta;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}
