package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "conta_servico_externo")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_conta_servico_externo", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_conta_servico_externo", nullable = false, unique = true)) })
@TypeDefs(value = { @TypeDef(name = "TipoServicoExterno", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoServicoExterno") }) })
public class ContaServicoExterno extends BaseEntity<Long> {

    private static final long serialVersionUID = -1108299546219500686L;

    @Column(name = "access_token")
    private String accessToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro")
    private Date dataCadastro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_confirmacao")
    private Date dataConfirmacao;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "expire_time")
    private Long expireTime;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "login_automatico")
    private Boolean loginAutomatico;

    @Column(name = "profile_url")
    private String profileUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_provedor_servico_externo")
    private ProvedorServicoExterno provedorServicoExterno;

    // @Column(name = "providerId")
    // private String providerId;

    // @Column(name = "providerUserId")
    // private String providerUserId;

    @Column(name = "rank")
    private Integer rank;

    @Column(name = "refresh_token")
    private String refreshToken;

    @Column(name = "secret")
    private String secret;

    @Type(type = "TipoServicoExterno")
    @Column(name = "id_tipo_servico_externo", nullable = false)
    private TipoServicoExterno tipoServicoExterno;

    @Column(name = "url")
    private String url;

    // @Column(name = "userId")
    // private String userId;

    @Column(name = "username")
    private String username;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    public String getAccessToken() {
        return this.accessToken;
    }

    public Date getDataCadastro() {
        return this.dataCadastro;
    }

    public Date getDataConfirmacao() {
        return this.dataConfirmacao;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public Long getExpireTime() {
        return this.expireTime;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public Boolean getLoginAutomatico() {
        return this.loginAutomatico;
    }

    /*public String getProviderId() {
        return this.providerId;
    }

    public String getProviderUserId() {
        return this.providerUserId;
    }*/

    public String getProfileUrl() {
        return this.profileUrl;
    }

    public ProvedorServicoExterno getProvedorServicoExterno() {
        return this.provedorServicoExterno;
    }

    public Integer getRank() {
        return this.rank;
    }

    public String getRefreshToken() {
        return this.refreshToken;
    }

    public String getSecret() {
        return this.secret;
    }

    public TipoServicoExterno getTipoServicoExterno() {
        return this.tipoServicoExterno;
    }

    /*public String getUserId() {
        return this.userId;
    }*/

    public String getUrl() {
        return this.url;
    }

    public String getUsername() {
        return this.username;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public boolean isConfirmado() {
        return getDataConfirmacao() != null;
    }

    public boolean isEmail() {
        return getTipoServicoExterno().equals(TipoServicoExterno.EMAIL);
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public void setDataCadastro(final Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void setDataConfirmacao(final Date dataConfirmacao) {
        this.dataConfirmacao = dataConfirmacao;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    /*public void setProviderId(final String providerId) {
        this.providerId = providerId;
    }

    public void setProviderUserId(final String providerUserId) {
        this.providerUserId = providerUserId;
    }*/

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public void setExpireTime(final Long expireTime) {
        this.expireTime = expireTime;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setLoginAutomatico(final Boolean loginAutomatico) {
        this.loginAutomatico = loginAutomatico;
    }

    public void setProfileUrl(final String profileUrl) {
        this.profileUrl = profileUrl;
    }

    /*public void setUserId(final String userId) {
        this.userId = userId;
    }*/

    public void setProvedorServicoExterno(final ProvedorServicoExterno provedorServicoExterno) {
        this.provedorServicoExterno = provedorServicoExterno;
    }

    public void setRank(final Integer rank) {
        this.rank = rank;
    }

    public void setRefreshToken(final String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public void setSecret(final String secret) {
        this.secret = secret;
    }

    public void setTipoServicoExterno(final TipoServicoExterno tipoServicoExterno) {
        this.tipoServicoExterno = tipoServicoExterno;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
