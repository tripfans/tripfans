package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Period;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "atividade_type", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_atividade", allocationSize = 1)
@Table(name = "atividade")
@TypeDefs(value = { @TypeDef(name = "TipoAtividade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoAtividade") }) })
public abstract class AtividadePlano extends BaseEntity<Long> implements UrlNameable {

    private static final long serialVersionUID = -6743172075035181047L;

    /*
     * Para o caso de agencias parceiras. Também tem o nome da agencia e endereço Caso não esteja cadastrado.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_agencia", nullable = true)
    private Agencia agencia;

    @Column(name = "anotacao", nullable = true)
    private String anotacao;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @Column(name = "comentario", nullable = true)
    private String comentario;

    @Column(name = "confirmacao", nullable = true)
    private String confirmacao;

    @Column(name = "data_fim")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataFim;

    @Column(name = "\"dataFimTZ\"")
    private String dataFimTZ;

    @Column(name = "data_inicio")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataInicio;

    @Column(name = "\"dataInicioTZ\"")
    private String dataInicioTZ;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_destino_viagem", nullable = true)
    private DestinoViagem destinoViagem;

    @Embedded
    private DetalheReservaAtividade detalheReserva = new DetalheReservaAtividade();

    @Column(name = "dia_inicio")
    private Integer diaInicio;

    @OneToMany(mappedBy = "atividadePlano", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Dica> dicas;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "foto_atividade", joinColumns = { @JoinColumn(name = "id_atividade", nullable = false, updatable = false) }, inverseJoinColumns = { @JoinColumn(name = "id_foto", nullable = false, updatable = false) })
    private List<Foto> fotos;

    @Column(name = "hora_fim")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsTimestamp")
    private LocalTime horaFim;

    // @OneToMany(mappedBy = "atividade", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    /*@OrderBy("ordem")
    @IndexColumn(base = 1, name = "ORDEM")*/
    // private List<FotoAtividade> fotosAtividade = new LinkedList<FotoAtividade>();

    @Column(name = "hora_inicio")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsTimestamp")
    private LocalTime horaInicio;

    @Column(name = "id_avaliacao", nullable = true, insertable = false, updatable = false)
    private Long idAvaliacao;

    // @Columns(columns = { @Column(name = "\"dataFim\""), @Column(name = "\"dataFimTZ\"") })
    // @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTimeTZ")
    // private DateTime dataFim;
    //
    // @Columns(columns = { @Column(name = "\"dataInicio\""), @Column(name = "\"dataInicioTZ\"") })
    // @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTimeTZ")
    // private DateTime dataInicio;
    /*
     * Atentar que para o caso de data da reserva provavelmente a hora n�o faz sentido.
     */

    @Column(name = "id_relato_viagem", nullable = true, insertable = false, updatable = false)
    private Long idRelatoViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = true)
    private Local local;

    // @Columns(columns = { @Column(name = "\"dataReserva\""), @Column(name = "\"dataReservaTZ\"") })
    // @Type(type = "org.joda.time.contrib.hibernate.PersistentDateTimeTZ")
    // private DateTime dataReserva;

    @Embedded
    private LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    /** Exibir ou não no diário **/
    @Column(name = "oculta", nullable = true)
    private Boolean oculta = false;

    @Column
    private Integer ordem;

    @OneToMany(mappedBy = "atividadePlano", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ParticipacaoAtividade> participantes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private PlanoViagem planoViagem;

    @Column(name = "qtd_dicas", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer quantidadeDicas = 0;

    @Column(name = "qtd_fotos", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer quantidadeFotos = 0;

    @Column(name = "qtd_participantes", nullable = false, columnDefinition = "INT DEFAULT 0")
    private Integer quantidadeParticipantes = 0;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_relato_viagem", nullable = true)
    private RelatoViagem relato;

    @Type(type = "TipoAtividade")
    @Column(name = "atividade_type", nullable = false, insertable = false, updatable = false)
    private TipoAtividade tipo;

    @Column(name = "titulo", nullable = true)
    private String titulo;

    @Column(name = "url_path", nullable = false, unique = true, updatable = false)
    private String urlPath;

    public boolean addFoto(final Foto foto) {
        return this.fotos.add(foto);
    }

    /*public boolean addFoto(final Foto foto) {
        final FotoAtividade fotoAtividade = new FotoAtividade();
        fotoAtividade.setAtividade(this);
        fotoAtividade.setFoto(foto);
        return this.fotosAtividade.add(fotoAtividade);
    }*/

    public Agencia getAgencia() {
        return this.agencia;
    }

    public Integer getAnoInicio() {
        if (this.dataInicio != null) {
            return this.dataInicio.getYear();
        }
        return null;
    }

    public String getAnotacao() {
        return this.anotacao;
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public String getComentario() {
        return this.comentario;
    }

    public String getConfirmacao() {
        return this.confirmacao;
    }

    public LocalDate getDataFim() {
        return this.dataFim;
    }

    public String getDataFimTZ() {
        return this.dataFimTZ;
    }

    public LocalDate getDataInicio() {
        return this.dataInicio;
    }

    public String getDataInicioTZ() {
        return this.dataInicioTZ;
    }

    public String getDecricaoTipo() {
        return getTipo().getDescricao();
    }

    public abstract String getDescricaoAtividade();

    public String getDescricaoLocal() {
        if (getLocal() != null) {
            return this.local.getDescricao();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getDescricao();
        }
        return null;
    }

    public DestinoViagem getDestinoViagem() {
        return this.destinoViagem;
    }

    public DetalheReservaAtividade getDetalheReserva() {
        return this.detalheReserva;
    }

    public Integer getDiaInicio() {
        return this.diaInicio;
    }

    public List<Dica> getDicas() {
        return this.dicas;
    }

    public String getEnderecoLocal() {
        // final Local local = initializeAndUnproxy(getLocal());
        if (this.local != null && this.local.getEndereco() != null) {
            return this.local.getEndereco().getDescricao();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getEndereco().getDescricao();
        }
        return null;
    }

    public List<Foto> getFotos() {
        return this.fotos;
    }

    public LocalTime getHoraFim() {
        return this.horaFim;
    }

    public LocalTime getHoraInicio() {
        return this.horaInicio;
    }

    public Local getLocal() {
        return this.local;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalNaoCadastrado() {
        if (this.localNaoCadastrado == null) {
            this.localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();
        }
        return this.localNaoCadastrado;
    }

    public Mes getMesInicio() {
        if (this.dataInicio != null) {
            return Mes.getMes(this.dataInicio.getMonthOfYear());
        }
        return null;
    }

    /*public List<FotoAtividade> getFotosAtividade() {
        return this.fotosAtividade;
    }*/

    public String getNomeLocal() {
        if (getLocal() != null) {
            return this.local.getNome();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getNome();
        }
        return null;
    }

    public Boolean getOculta() {
        return this.oculta;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    public List<ParticipacaoAtividade> getParticipantes() {
        return this.participantes;
    }

    public Period getPeriodo() {
        if (getDataInicio() != null && getDataFim() != null) {
            return new Period(this.dataInicio, this.dataFim);
        }
        return null;
    }

    /**
     * Verifica se o usuario ainda não preencheu algo que impeça a exclusão direta (antes de ter feito alguma alteração relevante na atividade)
     * 
     * @see getPreencheuDadosDiario()
     */
    public boolean getPermiteExclusaoDireta() {
        return !getPreencheuDadosDiario();
    }

    public boolean getPossuiAvaliacao() {
        return this.idAvaliacao != null;
    }

    public boolean getPossuiDicas() {
        return this.quantidadeDicas > 0;
    }

    public boolean getPossuiFotos() {
        return this.quantidadeFotos > 0;
    }

    public boolean getPossuiParticipantes() {
        return this.quantidadeParticipantes > 0;
    }

    public boolean getPossuiRelato() {
        return this.idRelatoViagem != null;
    }

    public boolean getPossuiTitulo() {
        return this.titulo != null;
    }

    /**
     * Verifica se o usuario ainda preencheu algum item referente ao diário de planoViagem (ex: Avaliacao, Dicas, Relato)
     * 
     * @return
     */
    public boolean getPreencheuDadosDiario() {
        return getPossuiAvaliacao() || getPossuiDicas() || getPossuiFotos() || getPossuiRelato();
    }

    @Override
    public String getPrefix() {
        return "atv";
    }

    public Integer getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public Integer getQuantidadeFotos() {
        return this.quantidadeFotos;
    }

    public Integer getQuantidadeParticipantes() {
        return this.quantidadeParticipantes;
    }

    public RelatoViagem getRelato() {
        return this.relato;
    }

    public TipoAtividade getTipo() {
        return this.tipo;
    }

    public String getTitulo() {
        return this.titulo;
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public PlanoViagem getViagem() {
        return this.planoViagem;
    }

    public boolean isAlimentacao() {
        return this instanceof Alimentacao;
    }

    public boolean isAluguelVeiculo() {
        return this instanceof AluguelVeiculo;
    }

    public boolean isCurtaDuracao() {
        return this instanceof AtividadeCurtaDuracao;
    }

    public boolean isHospedagem() {
        return this instanceof Hospedagem;
    }

    public boolean isLongaDuracao() {
        return this instanceof AtividadeLongaDuracao;
    }

    public boolean isTranporte() {
        return this instanceof Transporte;
    }

    public boolean isVisitaAtracao() {
        return this instanceof VisitaAtracao;
    }

    public Boolean isVisivel() {
        return this.oculta == null || !this.oculta;
    }

    public boolean isVoo() {
        return this instanceof Voo;
    }

    public boolean removeFoto(final Foto foto) {
        return this.fotos.remove(foto);
    }

    public void setAgencia(final Agencia agencia) {
        this.agencia = agencia;
    }

    public void setAnotacao(final String anotacao) {
        this.anotacao = anotacao;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
        if (avaliacao.getAtividade() == null) {
            avaliacao.setAtividade(this);
        }
    }

    public void setComentario(final String comentario) {
        this.comentario = comentario;
    }

    public void setConfirmacao(final String confirmacao) {
        this.confirmacao = confirmacao;
    }

    public void setDataFim(final LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public void setDataFimTZ(final String dataFimTZ) {
        this.dataFimTZ = dataFimTZ;
    }

    public void setDataInicio(final LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public void setDataInicioTZ(final String dataInicioTZ) {
        this.dataInicioTZ = dataInicioTZ;
    }

    public void setDescricaoLocal(final String descricao) {
        getLocalNaoCadastrado().setDescricao(descricao);
    }

    public void setDestinoViagem(final DestinoViagem destinoViagem) {
        this.destinoViagem = destinoViagem;
    }

    public void setDetalheReserva(final DetalheReservaAtividade detalheReserva) {
        this.detalheReserva = detalheReserva;
    }

    public void setDiaInicio(final Integer dia) {
        this.diaInicio = dia;
    }

    public void setDicas(final List<Dica> dicas) {
        this.dicas = dicas;
    }

    public void setEnderecoLocal(final String endereco) {
        getLocalNaoCadastrado().getEndereco().setDescricao(endereco);
    }

    public void setFotos(final List<Foto> fotos) {
        this.fotos = fotos;
    }

    /*public void setFotosAtividade(final List<FotoAtividade> fotosAtividade) {
        this.fotosAtividade = fotosAtividade;
    }*/

    public void setHoraFim(final LocalTime horaFim) {
        this.horaFim = horaFim;
    }

    public void setHoraInicio(final LocalTime horaInicio) {
        this.horaInicio = horaInicio;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setLocalNaoCadastrado(final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado) {
        this.localNaoCadastrado = localNaoCadastrado;
    }

    public void setNomeLocal(final String nome) {
        getLocalNaoCadastrado().setNome(nome);
    }

    public void setOculta(final Boolean oculta) {
        this.oculta = oculta;
    }

    public void setOrdem(final Integer ordem) {
        this.ordem = ordem;
    }

    public void setParticipantes(final List<ParticipacaoAtividade> participantes) {
        this.participantes = participantes;
    }

    public void setQuantidadeDicas(final Integer quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

    public void setQuantidadeFotos(final Integer quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setQuantidadeParticipantes(final Integer quantidadeParticipantes) {
        this.quantidadeParticipantes = quantidadeParticipantes;
    }

    public void setRelato(final RelatoViagem relato) {
        this.relato = relato;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setViagem(final PlanoViagem planoViagem) {
        this.planoViagem = planoViagem;
    }

}
