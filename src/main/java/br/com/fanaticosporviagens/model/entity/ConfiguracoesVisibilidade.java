package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Embeddable
@TypeDefs(value = { @TypeDef(name = "Visibilidade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoVisibilidade") }) })
public class ConfiguracoesVisibilidade implements Serializable {

    private static final long serialVersionUID = -7714123902116874729L;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_albuns_foto", nullable = false, columnDefinition = "integer not null default 2")
    private TipoVisibilidade mostrarAlbunsFoto = TipoVisibilidade.AMIGOS;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_diarios_viagens", nullable = false, columnDefinition = "integer not null default 2")
    private TipoVisibilidade mostrarDiariosViagens = TipoVisibilidade.AMIGOS;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_dicas", nullable = false, columnDefinition = "integer not null default 1")
    private TipoVisibilidade mostrarDicas = TipoVisibilidade.PUBLICO;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_fotos", nullable = false, columnDefinition = "integer not null default 2")
    private TipoVisibilidade mostrarFotos = TipoVisibilidade.AMIGOS;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_informacoes_perfil", nullable = false, columnDefinition = "integer not null default 1")
    private TipoVisibilidade mostrarInformacoesPerfil = TipoVisibilidade.PUBLICO;

    @Type(type = "Visibilidade")
    @Column(name = "mostrar_planos_viagens", nullable = false, columnDefinition = "integer not null default 2")
    private TipoVisibilidade mostrarPlanosViagens = TipoVisibilidade.AMIGOS;

    public TipoVisibilidade getMostrarAlbunsFoto() {
        return this.mostrarAlbunsFoto;
    }

    public TipoVisibilidade getMostrarDiariosViagens() {
        return this.mostrarDiariosViagens;
    }

    public TipoVisibilidade getMostrarDicas() {
        return this.mostrarDicas;
    }

    public TipoVisibilidade getMostrarFotos() {
        return this.mostrarFotos;
    }

    public TipoVisibilidade getMostrarInformacoesPerfil() {
        return this.mostrarInformacoesPerfil;
    }

    public TipoVisibilidade getMostrarPlanosViagens() {
        return this.mostrarPlanosViagens;
    }

    public void setMostrarAlbunsFoto(final TipoVisibilidade mostrarAlbunsFoto) {
        this.mostrarAlbunsFoto = mostrarAlbunsFoto;
    }

    public void setMostrarDiariosViagens(final TipoVisibilidade mostrarDiariosViagens) {
        this.mostrarDiariosViagens = mostrarDiariosViagens;
    }

    public void setMostrarDicas(final TipoVisibilidade mostrarDicas) {
        this.mostrarDicas = mostrarDicas;
    }

    public void setMostrarFotos(final TipoVisibilidade mostrarFotos) {
        this.mostrarFotos = mostrarFotos;
    }

    public void setMostrarInformacoesPerfil(final TipoVisibilidade mostrarInformacoesPerfil) {
        this.mostrarInformacoesPerfil = mostrarInformacoesPerfil;
    }

    public void setMostrarPlanosViagens(final TipoVisibilidade mostrarPlanosViagens) {
        this.mostrarPlanosViagens = mostrarPlanosViagens;
    }

}
