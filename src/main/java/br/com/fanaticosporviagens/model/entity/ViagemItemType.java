package br.com.fanaticosporviagens.model.entity;

public enum ViagemItemType {
    AVULSO,
    LOCAL,
    TRANSPORTE;
}
