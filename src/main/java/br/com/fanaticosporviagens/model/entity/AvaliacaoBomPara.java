package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_avaliacao_bom_para")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_avaliacao_bom_para", allocationSize = 1)
@Table(name = "avaliacao_bom_para")
@TypeDefs(value = { @TypeDef(name = "AvaliacaoItemBomParaTypeDef", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.AvaliacaoItemBomPara") }) })
public class AvaliacaoBomPara extends BaseEntity<Long> {

    private static final long serialVersionUID = 2655136919297489232L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @Type(type = "AvaliacaoItemBomParaTypeDef")
    @Column(name = "id_avaliacao_item_bom_para", nullable = false)
    private AvaliacaoItemBomPara item;

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public AvaliacaoItemBomPara getItem() {
        return this.item;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setItem(final AvaliacaoItemBomPara item) {
        this.item = item;
    }

}
