package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/*
 * Acredito que essa lista pode ser usada para fazer, al�m da lista de encomendal, uma lista de gastos da viagem.
 */
public class ItemDeCompra extends BaseEntity<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = -3542678505229132659L;

    private String descricao;

    private LocalEnderecavel localCompra;

    /*
     * No momento de incluir um participante faremos um cadastro simplificado. Se por poss�vel pegamos o e-mail para mandar confirma��es e convidar
     * para conhecer melhor nosso servi�o.
     */
    private Usuario usuarioSolicitante;

    private BigDecimal valorPago;

    private BigDecimal valorPrevisto;

}
