package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author André Thiago
 * 
 */
@Embeddable
public class IdAmizade implements Serializable {

    private static final long serialVersionUID = -7321966646316735823L;

    @Column(name = "id_usuario_amigo")
    private Long idAmigo;

    @Column(name = "id_usuario")
    private Long idUsuario;

    public IdAmizade() {

    }

    public IdAmizade(final Long idUsuario, final Long idAmigo) {
        super();
        this.idUsuario = idUsuario;
        this.idAmigo = idAmigo;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final IdAmizade other = (IdAmizade) obj;
        return (this.idUsuario.equals(other.idUsuario) && this.idAmigo.equals(other.idAmigo));

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.idAmigo == null) ? 0 : this.idAmigo.hashCode());
        result = prime * result + ((this.idUsuario == null) ? 0 : this.idUsuario.hashCode());
        return result;
    }

}