package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;

import br.com.fanaticosporviagens.infra.component.BreadCrumb;
import br.com.fanaticosporviagens.infra.component.BreadCrumbGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 
 * 
 * 
 * @author Casa
 * 
 */
@Embeddable
public class LocalidadeEmbeddable implements Serializable, BreadCrumbGenerator {

    private static final long serialVersionUID = -4612596715427798616L;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_cidade_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_cidade_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_cidade_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_cidade_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_cidade_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_cidade_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_cidade_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_cidade_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_cidade_type")) })
    private LocalEmbeddable<Cidade> cidade;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_continente_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_continente_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_continente_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_continente_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_continente_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_continente_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_continente_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_continente_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_continente_type")) })
    private LocalEmbeddable<Continente> continente;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_estado_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_estado_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_estado_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_estado_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_estado_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_estado_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_estado_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_estado_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_estado_type")) })
    private LocalEmbeddable<Estado> estado;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_local_enderecavel_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_local_enderecavel_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_local_enderecavel_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_local_enderecavel_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_local_enderecavel_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_local_enderecavel_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_local_enderecavel_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_local_enderecavel_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_local_enderecavel_type")) })
    private LocalEmbeddable<LocalEnderecavel> localEnderecavel;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_local_interesse_turistico_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_local_interesse_turistico_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_local_interesse_turistico_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_local_interesse_turistico_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_local_interesse_turistico_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_local_interesse_turistico_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_local_interesse_turistico_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_local_interesse_turistico_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_local_interesse_turistico_type")) })
    private LocalEmbeddable<LocalInteresseTuristico> localInteresseTuristico;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_local_mais_especifico_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_local_mais_especifico_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_local_mais_especifico_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_local_mais_especifico_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_local_mais_especifico_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_local_mais_especifico_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_local_mais_especifico_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_local_mais_especifico_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_local_mais_especifico_type")) })
    private LocalEmbeddable<Local> localMaisEspecifico;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "local", joinColumns = @JoinColumn(name = "d_pais_id", insertable = false, updatable = false)),
            @AssociationOverride(name = "fotoPadrao", joinColumns = @JoinColumn(name = "d_pais_id_foto_padrao")) })
    @AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "d_pais_id")),
            @AttributeOverride(name = "idFacebook", column = @Column(name = "d_pais_id_facebook")),
            @AttributeOverride(name = "idFoursquare", column = @Column(name = "d_pais_id_foursquare")),
            @AttributeOverride(name = "nome", column = @Column(name = "d_pais_nome")),
            @AttributeOverride(name = "sigla", column = @Column(name = "d_pais_sigla")),
            @AttributeOverride(name = "urlPath", column = @Column(name = "d_pais_url_path")),
            @AttributeOverride(name = "tipo", column = @Column(name = "d_pais_type")) })
    private LocalEmbeddable<Pais> pais;

    @Override
    @JsonIgnore
    public BreadCrumb getBreadCrumb() {
        return new BreadCrumb(this);
    }

    public LocalEmbeddable<Cidade> getCidade() {
        return this.cidade;
    }

    public Foto getCidadeFotoPadrao() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getFotoPadrao();
    }

    public Long getCidadeId() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getId();
    }

    public String getCidadeIdFacebook() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getIdFacebook();
    }

    public String getCidadeIdFoursquare() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getIdFoursquare();
    }

    public Cidade getCidadeLocal() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getLocal();
    }

    public String getCidadeNome() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getNome();
    }

    public LocalType getCidadeTipo() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getTipo();
    }

    public String getCidadeUrl() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getUrl();
    }

    public String getCidadeUrlPath() {
        if (this.cidade == null) {
            return null;
        }
        return this.cidade.getUrlPath();
    }

    public LocalEmbeddable<Continente> getContinente() {
        return this.continente;
    }

    public Foto getContinenteFotoPadrao() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getFotoPadrao();
    }

    public Long getContinenteId() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getId();
    }

    public String getContinenteIdFacebook() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getIdFacebook();
    }

    public String getContinenteIdFoursquare() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getIdFoursquare();
    }

    public Continente getContinenteLocal() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getLocal();
    }

    public String getContinenteNome() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getNome();
    }

    public String getContinenteSigla() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getSigla();
    }

    public LocalType getContinenteTipo() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getTipo();
    }

    public String getContinenteUrl() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getUrl();
    }

    public String getContinenteUrlPath() {
        if (this.continente == null) {
            return null;
        }
        return this.continente.getUrlPath();
    }

    public LocalEmbeddable<Estado> getEstado() {
        return this.estado;
    }

    public Foto getEstadoFotoPadrao() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getFotoPadrao();
    }

    public Long getEstadoId() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getId();
    }

    public String getEstadoIdFacebook() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getIdFacebook();
    }

    public String getEstadoIdFoursquare() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getIdFoursquare();
    }

    public Estado getEstadoLocal() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getLocal();
    }

    public String getEstadoNome() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getNome();
    }

    public String getEstadoSigla() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getSigla();
    }

    public LocalType getEstadoTipo() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getTipo();
    }

    public String getEstadoUrl() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getUrl();
    }

    public String getEstadoUrlPath() {
        if (this.estado == null) {
            return null;
        }
        return this.estado.getUrlPath();
    }

    public List<LocalEmbeddable<? extends Local>> getLocaisEmOrdemHierarquica() {

        final List<LocalEmbeddable<? extends Local>> localizacaoHierarquica = new ArrayList<LocalEmbeddable<? extends Local>>();

        if (getLocalEnderecavel() != null) {
            localizacaoHierarquica.add(getLocalEnderecavel());
        }
        if (getLocalInteresseTuristico() != null) {
            localizacaoHierarquica.add(getLocalInteresseTuristico());
        }
        if (getCidade() != null) {
            localizacaoHierarquica.add(getCidade());
        }
        if (getEstado() != null) {
            localizacaoHierarquica.add(getEstado());
        }
        if (getPais() != null) {
            localizacaoHierarquica.add(getPais());
        }
        if (getContinente() != null) {
            localizacaoHierarquica.add(getContinente());
        }

        return localizacaoHierarquica;
    }

    public List<Local> getLocaisInteressados() {
        final ArrayList<Local> interessados = new ArrayList<Local>();

        if (this.cidade != null) {
            interessados.add(this.cidade.getLocal());
        }

        if (this.continente != null) {
            interessados.add(this.continente.getLocal());
        }

        if (this.estado != null) {
            interessados.add(this.estado.getLocal());
        }

        if (this.localEnderecavel != null) {
            interessados.add(this.localEnderecavel.getLocal());
        }

        if (this.localInteresseTuristico != null) {
            interessados.add(this.localInteresseTuristico.getLocal());
        }

        if (this.pais != null) {
            interessados.add(this.pais.getLocal());
        }

        return interessados;
    }

    public LocalEmbeddable<LocalEnderecavel> getLocalEnderecavel() {
        return this.localEnderecavel;
    }

    public Foto getLocalEnderecavelFotoPadrao() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getFotoPadrao();
    }

    public Long getLocalEnderecavelId() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getId();
    }

    public String getLocalEnderecavelIdFacebook() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getIdFacebook();
    }

    public String getLocalEnderecavelIdFoursquare() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getIdFoursquare();
    }

    public LocalEnderecavel getLocalEnderecavelLocal() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getLocal();
    }

    public String getLocalEnderecavelNome() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getNome();
    }

    public String getLocalEnderecavelSigla() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getSigla();
    }

    public LocalType getLocalEnderecavelTipo() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getTipo();
    }

    public String getLocalEnderecavelUrl() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getUrl();
    }

    public String getLocalEnderecavelUrlPath() {
        if (this.localEnderecavel == null) {
            return null;
        }
        return this.localEnderecavel.getUrlPath();
    }

    public LocalEmbeddable<LocalInteresseTuristico> getLocalInteresseTuristico() {
        return this.localInteresseTuristico;
    }

    public Foto getLocalInteresseTuristicoFotoPadrao() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getFotoPadrao();
    }

    public Long getLocalInteresseTuristicoId() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getId();
    }

    public String getLocalInteresseTuristicoIdFacebook() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getIdFacebook();
    }

    public String getLocalInteresseTuristicoIdFoursquare() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getIdFoursquare();
    }

    public LocalInteresseTuristico getLocalInteresseTuristicoLocal() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getLocal();
    }

    public String getLocalInteresseTuristicoNome() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getNome();
    }

    public String getLocalInteresseTuristicoSigla() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getSigla();
    }

    public LocalType getLocalInteresseTuristicoTipo() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getTipo();
    }

    public String getLocalInteresseTuristicoUrl() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getUrl();
    }

    public String getLocalInteresseTuristicoUrlPath() {
        if (this.localInteresseTuristico == null) {
            return null;
        }
        return this.localInteresseTuristico.getUrlPath();
    }

    public LocalEmbeddable<Local> getLocalMaisEspecifico() {
        return this.localMaisEspecifico;
    }

    public Foto getLocalMaisEspecificoFotoPadrao() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getFotoPadrao();
    }

    public Long getLocalMaisEspecificoId() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getId();
    }

    public String getLocalMaisEspecificoIdFacebook() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getIdFacebook();
    }

    public String getLocalMaisEspecificoIdFoursquare() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getIdFoursquare();
    }

    public Local getLocalMaisEspecificoLocal() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getLocal();
    }

    public String getLocalMaisEspecificoNome() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getNome();
    }

    public String getLocalMaisEspecificoSigla() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getSigla();
    }

    public LocalType getLocalMaisEspecificoTipo() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getTipo();
    }

    public String getLocalMaisEspecificoUrl() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getUrl();
    }

    public String getLocalMaisEspecificoUrlPath() {
        if (this.localMaisEspecifico == null) {
            return null;
        }
        return this.localMaisEspecifico.getUrlPath();
    }

    public LocalEmbeddable<Pais> getPais() {
        return this.pais;
    }

    public Foto getPaisFotoPadrao() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getFotoPadrao();
    }

    public Long getPaisId() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getId();
    }

    public String getPaisIdFacebook() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getIdFacebook();
    }

    public String getPaisIdFoursquare() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getIdFoursquare();
    }

    public Pais getPaisLocal() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getLocal();
    }

    public String getPaisNome() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getNome();
    }

    public String getPaisSigla() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getSigla();
    }

    public LocalType getPaisTipo() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getTipo();
    }

    public String getPaisUrl() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getUrl();
    }

    public String getPaisUrlPath() {
        if (this.pais == null) {
            return null;
        }
        return this.pais.getUrlPath();
    }

    public List<LocalEmbeddable<?>> getReferenciaLocaisInteressados() {
        final ArrayList<LocalEmbeddable<?>> interessados = new ArrayList<LocalEmbeddable<?>>();

        if ((this.localEnderecavel != null) && (this.localEnderecavel.getId() != null)) {
            interessados.add(this.localEnderecavel);
        }

        if ((this.localInteresseTuristico != null) && (this.localInteresseTuristico.getId() != null)) {
            interessados.add(this.localInteresseTuristico);
        }

        if ((this.cidade != null) && (this.cidade.getId() != null)) {
            interessados.add(this.cidade);
        }

        if ((this.continente != null) && (this.continente.getId() != null)) {
            interessados.add(this.continente);
        }

        if ((this.estado != null) && (this.estado.getId() != null)) {
            interessados.add(this.estado);
        }

        if ((this.pais != null) && (this.pais.getId() != null)) {
            interessados.add(this.pais);
        }

        return interessados;
    }

    public void setCidade(final LocalEmbeddable<Cidade> cidade) {
        this.cidade = cidade;
    }

    public void setContinente(final LocalEmbeddable<Continente> continente) {
        this.continente = continente;
    }

    public void setEstado(final LocalEmbeddable<Estado> estado) {
        this.estado = estado;
    }

    public void setLocalEnderecavel(final LocalEmbeddable<LocalEnderecavel> localEnderecavel) {
        this.localEnderecavel = localEnderecavel;
    }

    public void setLocalInteresseTuristico(final LocalEmbeddable<LocalInteresseTuristico> localInteresseTuristico) {
        this.localInteresseTuristico = localInteresseTuristico;
    }

    public void setLocalMaisEspecifico(final LocalEmbeddable<Local> localMaisEspecifico) {
        this.localMaisEspecifico = localMaisEspecifico;
    }

    public void setPais(final LocalEmbeddable<Pais> pais) {
        this.pais = pais;
    }

}
