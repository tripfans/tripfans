package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class PreferenciasViagemCompanhia implements Serializable {

    private static final long serialVersionUID = -8699462583401441427L;

    @Column(name = "amigos")
    private Boolean amigos = false;

    @Column(name = "casal")
    private Boolean casal = false;

    @Column(name = "familia")
    private Boolean familia = false;

    @Column(name = "sozinho")
    private Boolean sozinho = false;

    public Boolean getAmigos() {
        return this.amigos;
    }

    public Boolean getCasal() {
        return this.casal;
    }

    public Boolean getFamilia() {
        return this.familia;
    }

    public Boolean getSozinho() {
        return this.sozinho;
    }

    public void setAmigos(final Boolean amigos) {
        this.amigos = amigos;
    }

    public void setCasal(final Boolean casal) {
        this.casal = casal;
    }

    public void setFamilia(final Boolean familia) {
        this.familia = familia;
    }

    public void setSozinho(final Boolean sozinho) {
        this.sozinho = sozinho;
    }
}