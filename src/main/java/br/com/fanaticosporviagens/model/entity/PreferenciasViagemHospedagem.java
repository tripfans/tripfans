package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class PreferenciasViagemHospedagem implements Serializable {

    private static final long serialVersionUID = -5984940931004396881L;

    @Column(name = "albergue")
    private Boolean albergue = false;

    @Column(name = "bed_breakfast")
    private Boolean bedBreakfast = false;

    @Column(name = "camping")
    private Boolean camping = false;

    @Column(name = "casa_apartamento")
    private Boolean casaApartamento = false;

    @Column(name = "hotel")
    private Boolean hotel = false;

    @Column(name = "pousada")
    private Boolean pousada = false;

    @Column(name = "resort")
    private Boolean resort = false;

    public Boolean getAlbergue() {
        return this.albergue;
    }

    public Boolean getBedBreakfast() {
        return this.bedBreakfast;
    }

    public Boolean getCamping() {
        return this.camping;
    }

    public Boolean getCasaApartamento() {
        return this.casaApartamento;
    }

    public Boolean getHotel() {
        return this.hotel;
    }

    public Boolean getPousada() {
        return this.pousada;
    }

    public Boolean getResort() {
        return this.resort;
    }

    public void setAlbergue(final Boolean albergue) {
        this.albergue = albergue;
    }

    public void setBedBreakfast(final Boolean bedBreakfast) {
        this.bedBreakfast = bedBreakfast;
    }

    public void setCamping(final Boolean camping) {
        this.camping = camping;
    }

    public void setCasaApartamento(final Boolean casaApartamento) {
        this.casaApartamento = casaApartamento;
    }

    public void setHotel(final Boolean hotel) {
        this.hotel = hotel;
    }

    public void setPousada(final Boolean pousada) {
        this.pousada = pousada;
    }

    public void setResort(final Boolean resort) {
        this.resort = resort;
    }
}