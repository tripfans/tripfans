package br.com.fanaticosporviagens.model.entity;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
public class DetalheTrechoCruzeiro extends DetalheTrecho {

    private static final long serialVersionUID = -5719184836062615539L;

    private String classeCabine;

    private String codigoCabine;

    private String nomeNavio;

    private String pensao;

}
