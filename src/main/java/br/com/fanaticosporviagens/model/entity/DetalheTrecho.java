package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "detalhe_trecho_type", discriminatorType = DiscriminatorType.INTEGER)
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_detalhe_trecho", allocationSize = 1)
@Table(name = "detalhe_trecho")
public abstract class DetalheTrecho extends BaseEntity<Long> {

    private static final long serialVersionUID = -4688673059470494945L;

}
