package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum CategoriaNotificacaoPorEmail implements EnumTypeInteger {

    AMIGOS(1, "Amigos"),
    COMENTARIOS(2, "Comentários"),
    NOTICIAS_OFERTAS_ATUALIZACOES(4, "Notícias, ofertas e atualizações"),
    VIAGENS(3, "Viagens");

    private final Integer codigo;

    private final String descricao;

    private CategoriaNotificacaoPorEmail(final Integer codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    public List<TipoNotificacaoPorEmail> getTiposNotificoesPorEmail() {
        final List<TipoNotificacaoPorEmail> lista = new ArrayList<TipoNotificacaoPorEmail>();
        for (final TipoNotificacaoPorEmail tipo : TipoNotificacaoPorEmail.values()) {
            if (tipo.getCategoria().equals(this)) {
                lista.add(tipo);
            }
        }
        return lista;
    }
}