package br.com.fanaticosporviagens.model.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "recomendacao_local_pedido_dica")
public class RecomendacaoLocalPedidoDica {

    @EmbeddedId
    private RecomendacaoLocalPedidoDicaId id;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final RecomendacaoLocalPedidoDica other = (RecomendacaoLocalPedidoDica) obj;
        if (this.id.localRecomendado == null) {
            if (other.id.localRecomendado != null)
                return false;
        } else if (!this.id.localRecomendado.equals(other.id.localRecomendado))
            return false;
        if (this.id.pedidoDica == null) {
            if (other.id.pedidoDica != null)
                return false;
        } else if (!this.id.pedidoDica.equals(other.id.pedidoDica))
            return false;
        return true;
    }

    public RecomendacaoLocalPedidoDicaId getId() {
        return this.id;
    }

    public Local getLocalRecomendado() {
        return this.id.localRecomendado;
    }

    public PedidoDica getPedidoDica() {
        return this.id.pedidoDica;
    }

    public Usuario getUsuarioRecomendou() {
        return this.id.usuarioRecomendou;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id.localRecomendado == null) ? 0 : this.id.localRecomendado.hashCode());
        result = prime * result + ((this.id.pedidoDica == null) ? 0 : this.id.pedidoDica.hashCode());
        return result;
    }

    public void setId(final RecomendacaoLocalPedidoDicaId id) {
        this.id = id;
    }

    public void setLocalRecomendado(final Local localRecomendado) {
        this.id.localRecomendado = localRecomendado;
    }

    public void setPedidoDica(final PedidoDica pedidoDica) {
        this.id.pedidoDica = pedidoDica;
    }

    public void setUsuarioRecomendou(final Usuario usuarioRecomendou) {
        this.id.usuarioRecomendou = usuarioRecomendou;
    }

}
