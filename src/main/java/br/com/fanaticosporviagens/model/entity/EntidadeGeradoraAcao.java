package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Embeddable
@MappedSuperclass
@Filters(@Filter(name = "dataExclusao", condition = "data_exclusao is null"))
public abstract class EntidadeGeradoraAcao<ID extends Serializable> extends BaseEntity<ID> implements Acao {

    private static final long serialVersionUID = -2292069363464796910L;

    @Column(name = "data_criacao", nullable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataCriacao;

    @Column(name = "data_exclusao", nullable = true, insertable = false)
    private Date dataExclusao;

    @Column(name = "data_geracao_acao_alteracao", nullable = true, insertable = false)
    private Date dataGeracaoAcaoAlteracao;

    @Column(name = "data_geracao_acao_inclusao", nullable = true, insertable = false)
    private Date dataGeracaoAcaoInclusao;

    @Column(name = "data_publicacao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataPublicacao;

    // TODO criar interceptor/listener que popule isso automaticamente
    @Column(name = "data_ultima_alteracao", nullable = true)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataUltimaAlteracao;

    public EntidadeGeradoraAcao() {
        this.setDataCriacao(new LocalDateTime());
    }

    @Override
    public LocalDateTime getDataCriacao() {
        return this.dataCriacao;
    }

    @Override
    public Date getDataExclusao() {
        return this.dataExclusao;
    }

    @Override
    public Date getDataGeracaoAcaoAlteracao() {
        return this.dataGeracaoAcaoAlteracao;
    }

    @Override
    public Date getDataGeracaoAcaoInclusao() {
        return this.dataGeracaoAcaoInclusao;
    }

    @Override
    public LocalDateTime getDataPublicacao() {
        return this.dataPublicacao;
    }

    @Override
    public LocalDateTime getDataUltimaAlteracao() {
        return this.dataUltimaAlteracao;
    }

    @Override
    public Usuario getDestinatario() {
        if (getAlvo() != null) {
            if (getAlvo() instanceof Acao) {
                return ((Acao) getAlvo()).getAutor();
            } else
                return getAlvo().getDestinatario();
        }
        return null;
    }

    @Override
    public ID getId() {
        return super.getId();
    }

    public boolean isPublicado() {
        return this.dataPublicacao != null;
    }

    @Override
    public void setDataCriacao(final LocalDateTime dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    @Override
    public void setDataExclusao(final Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    @Override
    public void setDataGeracaoAcaoAlteracao(final Date dataGeracaoAcaoAlteracao) {
        this.dataGeracaoAcaoAlteracao = dataGeracaoAcaoAlteracao;
    }

    @Override
    public void setDataGeracaoAcaoInclusao(final Date dataGeracaoAcaoInclusao) {
        this.dataGeracaoAcaoInclusao = dataGeracaoAcaoInclusao;
    }

    @Override
    public void setDataPublicacao(final LocalDateTime dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    @Override
    public void setDataUltimaAlteracao(final LocalDateTime dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public void setPublicada(final boolean publicar) {
        if (publicar) {
            this.dataPublicacao = LocalDateTime.now();
        }
    }

}
