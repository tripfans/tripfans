package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe que representa uma história que o usuario queira contar em seu diário de viagem
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@DiscriminatorValue("10")
public class Historia extends AtividadeCurtaDuracao {

    private static final long serialVersionUID = -54495746608087400L;

    @Override
    public TipoAtividade getTipo() {
        // return TipoAtividade.HISTORIA;
        return null;
    }

}