package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "album")
@DynamicUpdate
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_album", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false, unique = true)) })
public class Album extends EntidadeGeradoraAcao<Long> implements UrlNameable, Comentavel {

    private static final long serialVersionUID = -2946791878726846595L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    @OneToMany(mappedBy = "album", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Comentario> comentarios;

    @Column(nullable = true, length = 500)
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_foto_principal")
    private Foto fotoPrincipal;

    @OneToMany(mappedBy = "album", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "data_exclusao IS NULL AND data_publicacao IS NOT NULL")
    @OrderBy("dataCriacao")
    private List<Foto> fotos;

    /**
     * Id do Album no Facebook, caso tenha sido importado do face
     */
    @Column(name = "id_facebook")
    private String idFacebook;

    @Column(name = "id_foto_principal", insertable = false, updatable = false)
    private Long idFotoPrincipal;

    @Column(name = "importado_facebook", nullable = true)
    private Boolean importadoFacebook = false;

    @Column(name = "quantidade_fotos", nullable = true, columnDefinition = "BIGINT DEFAULT 0")
    private Integer quantidadeFotos = 0;

    @Column(nullable = true, length = 500)
    private String titulo;

    @Column(name = "url_externa", nullable = true, length = 1000)
    private String urlExterna;

    @Column(name = "url_path", unique = true, nullable = false)
    private String urlPath;

    @Column
    @Type(type = "Visibilidade")
    private TipoVisibilidade visibilidade;

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return null;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public String getDescricaoAlvo() {
        return "Album: \"" + getTitulo() + "\"";
    }

    @Override
    public Usuario getDestinatario() {
        return getAutor();
    }

    public Foto getFotoPrincipal() {
        return this.fotoPrincipal;
    }

    public List<Foto> getFotos() {
        return this.fotos;
    }

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public Long getIdFotoPrincipal() {
        return this.idFotoPrincipal;
    }

    public Boolean getImportadoFacebook() {
        return this.importadoFacebook;
    }

    @Override
    public String getPrefix() {
        return "alb";
    }

    public Integer getQuantidadeFotos() {
        return this.quantidadeFotos;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public String getUrlExterna() {
        return this.urlExterna;
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setComentarios(final List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setFotoPrincipal(final Foto fotoPrincipal) {
        this.fotoPrincipal = fotoPrincipal;
    }

    public void setFotos(final List<Foto> fotos) {
        this.fotos = fotos;
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setIdFotoPrincipal(final Long idFotoPrincipal) {
        this.idFotoPrincipal = idFotoPrincipal;
    }

    public void setImportadoFacebook(final Boolean importadoFacebook) {
        this.importadoFacebook = importadoFacebook;
    }

    public void setQuantidadeFotos(final Integer quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    public void setUrlExterna(final String urlExterna) {
        this.urlExterna = urlExterna;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

}
