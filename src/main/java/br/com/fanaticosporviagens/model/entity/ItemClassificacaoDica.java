package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_item_classificacao_dica")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_item_classificacao_dica", allocationSize = 1)
@Table(name = "item_classificacao_dica")
public class ItemClassificacaoDica extends BaseEntity<Long> {

    private static final long serialVersionUID = 6455883408073027145L;

    @Column(name = "descricao")
    private String descricao;

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

}
