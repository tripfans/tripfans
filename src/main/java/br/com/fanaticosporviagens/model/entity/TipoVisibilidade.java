package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * 
 * @author @author Carlos Nascimento
 * 
 */

public enum TipoVisibilidade implements EnumTypeInteger {

    AMIGOS(2, "icon-cog", "Amigos"),
    PUBLICO(1, "icon-globe", "Público"),
    SOMENTE_EU(3, "icon-user", "Somente Eu");

    public static TipoVisibilidade[] valuesOrdenadosPorCodigo() {
        return new TipoVisibilidade[] { PUBLICO, AMIGOS, SOMENTE_EU };
    }

    private String icone;

    private final EnumI18nUtil util;

    TipoVisibilidade(final int codigo, final String icone, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        this.icone = icone;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public TipoVisibilidade getEnumPorCodigo(final Integer codigo) {
        for (final TipoVisibilidade visibilidade : values()) {
            if (visibilidade.getCodigo().equals(codigo)) {
                return visibilidade;
            }
        }
        throw new IllegalArgumentException("Código do Tipo de Visibilidade Inválido!");
    }

    public String getIcone() {
        return this.icone;
    }

}
