package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

/**
 * @author CarlosN
 */
@Embeddable
@MappedSuperclass
public class LocalNaoCadastradoAtividade {

    @Column(name = "descricao_local", nullable = true)
    private String descricao;

    @Column(name = "nome_local", nullable = true)
    private String nome;

    public LocalNaoCadastradoAtividade() {
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getNome() {
        return this.nome;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

}
