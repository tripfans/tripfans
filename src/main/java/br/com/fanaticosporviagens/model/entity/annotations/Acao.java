package br.com.fanaticosporviagens.model.entity.annotations;

import br.com.fanaticosporviagens.model.entity.TipoAcao;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public @interface Acao {

    /**
     * (Opcional) Objeto alvo da ação
     */
    String alvo() default "";

    /**
     * (Opcional) Usuario autor da ação.
     */
    String autor() default "";

    /**
     * (Opcional) nome do método que indica que a acao deve ser executada.
     */
    String condicao() default "";

    /**
     * (Opcional) Usuario destinatário da ação.
     */
    String destinatario() default "";

    /**
     * (Opcional) nome do atributo que guardará a referencia para a AcaoUsuario gerada.
     */
    String referenciaAcaoGerada() default "";

    /**
     * (Requerido) O Tipo da Ação a ser gerada.
     */
    TipoAcao tipo();

}
