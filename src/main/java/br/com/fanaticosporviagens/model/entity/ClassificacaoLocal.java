package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_classificacao_local")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_classificacao_local", allocationSize = 1)
@Table(name = "classificacao_local", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_local", "id_categoria_local", "id_classe_local" }) })
public class ClassificacaoLocal extends BaseEntity<Long> {

    private static final long serialVersionUID = -850540933180100561L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_categoria_local", nullable = false)
    private CategoriaLocal categoria;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_classe_local", nullable = true)
    private ClasseLocal classe;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = false)
    private LocalEnderecavel local;

    public CategoriaLocal getCategoria() {
        return this.categoria;
    }

    public ClasseLocal getClasse() {
        return this.classe;
    }

    public LocalEnderecavel getLocal() {
        return this.local;
    }

    public void setCategoria(final CategoriaLocal categoria) {
        this.categoria = categoria;
    }

    public void setClasse(final ClasseLocal classe) {
        this.classe = classe;
    }

    public void setLocal(final LocalEnderecavel local) {
        this.local = local;
    }

}
