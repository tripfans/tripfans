package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

/**
 * 
 * @author André Thiago
 * 
 */
@Entity
@DiscriminatorValue("2")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Pais extends LocalGeografico {

    private static final long serialVersionUID = -2926805644318845426L;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente", nullable = false)
    private Continente continente;

    @Deprecated
    @Column(name = "nm_local_continente", nullable = true)
    private String continenteNome;

    @Deprecated
    @Column(name = "url_path_local_continente", nullable = true)
    private String continenteUrlPath;

    @Column(name = "numcode")
    private String numCode;

    @Column(name = "qtd_cidade", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeCidades = 0L;

    @Column(name = "qtd_estado", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeEstados = 0L;

    @Column(name = "sigla_iso")
    private String siglaIso;

    @Column(name = "sigla_iso3")
    private String siglaIso3;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Pais other = (Pais) obj;
        if (this.continente == null) {
            if (other.continente != null)
                return false;
        } else if (!this.continente.equals(other.continente))
            return false;
        if (this.numCode == null) {
            if (other.numCode != null)
                return false;
        } else if (!this.numCode.equals(other.numCode))
            return false;
        if (this.siglaIso == null) {
            if (other.siglaIso != null)
                return false;
        } else if (!this.siglaIso.equals(other.siglaIso))
            return false;
        if (this.siglaIso3 == null) {
            if (other.siglaIso3 != null)
                return false;
        } else if (!this.siglaIso3.equals(other.siglaIso3))
            return false;
        return true;
    }

    public Continente getContinente() {
        return this.continente;
    }

    public String getContinenteNome() {
        return this.continenteNome;
    }

    public String getContinenteUrl() {
        return UrlLocalResolver.getUrl(LocalType.CONTINENTE, getContinenteUrlPath());
    }

    public String getContinenteUrlPath() {
        return this.continenteUrlPath;
    }

    @Override
    public List<Local> getLocaisInteressados() {
        final List<Local> interessados = new ArrayList<Local>();

        interessados.add(this);
        if (this.continente != null) {
            interessados.add(this.continente);
        }

        return interessados;
    }

    public String getNumCode() {
        return this.numCode;
    }

    @Override
    public Local getPai() {
        return getContinente();
    }

    public Long getQuantidadeCidades() {
        return this.quantidadeCidades;
    }

    public Long getQuantidadeEstados() {
        return this.quantidadeEstados;
    }

    public String getSiglaIso() {
        return this.siglaIso;
    }

    public String getSiglaIso3() {
        return this.siglaIso3;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.continente == null) ? 0 : this.continente.hashCode());
        result = prime * result + ((this.numCode == null) ? 0 : this.numCode.hashCode());
        result = prime * result + ((this.siglaIso == null) ? 0 : this.siglaIso.hashCode());
        result = prime * result + ((this.siglaIso3 == null) ? 0 : this.siglaIso3.hashCode());
        return result;
    }

    public void setContinente(final Continente continente) {
        this.continente = continente;
    }

    public void setContinenteNome(final String continenteNome) {
        this.continenteNome = continenteNome;
    }

    public void setContinenteUrlPath(final String continenteUrlPath) {
        this.continenteUrlPath = continenteUrlPath;
    }

    public void setNumCode(final String numCode) {
        this.numCode = numCode;
    }

    public void setQuantidadeCidades(final Long quantidadeCidades) {
        this.quantidadeCidades = quantidadeCidades;
    }

    public void setQuantidadeEstados(final Long quantidadeEstados) {
        this.quantidadeEstados = quantidadeEstados;
    }

    public void setSiglaIso(final String siglaIso) {
        this.siglaIso = siglaIso;
    }

    public void setSiglaIso3(final String siglaIso3) {
        this.siglaIso3 = siglaIso3;
    }
}
