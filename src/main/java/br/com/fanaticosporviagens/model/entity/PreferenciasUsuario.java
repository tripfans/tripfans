package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "preferencias_usuario")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_preferencias_usuario", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_preferencias_usuario", nullable = false, unique = true)) })
public class PreferenciasUsuario extends BaseEntity<Long> {

    private static final long serialVersionUID = 3626390832434098403L;

    @Column(name = "adicionar_mapas_direcoes")
    private Boolean adicionarAutomaticamenteMapasDirecoes;

    @Column(name = "formato_data")
    @Enumerated(EnumType.ORDINAL)
    private FormatoData formatoData;

    @Column(name = "formato_distancia")
    @Enumerated(EnumType.ORDINAL)
    private FormatoDistancia formatoDistancia;

    @Column(name = "formato_hora")
    @Enumerated(EnumType.ORDINAL)
    private FormatoHora formatoHora;

    @Column(name = "formato_temperatura")
    @Enumerated(EnumType.ORDINAL)
    private FormatoTemperatura formatoTemperatura;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = true)
    private Usuario usuario;

    public Boolean getAdicionarAutomaticamenteMapasDirecoes() {
        return this.adicionarAutomaticamenteMapasDirecoes;
    }

    public FormatoData getFormatoData() {
        return this.formatoData;
    }

    public FormatoDistancia getFormatoDistancia() {
        return this.formatoDistancia;
    }

    public FormatoHora getFormatoHora() {
        return this.formatoHora;
    }

    public FormatoTemperatura getFormatoTemperatura() {
        return this.formatoTemperatura;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setAdicionarAutomaticamenteMapasDirecoes(final Boolean adicionarAutomaticamenteMapasDirecoes) {
        this.adicionarAutomaticamenteMapasDirecoes = adicionarAutomaticamenteMapasDirecoes;
    }

    public void setFormatoData(final FormatoData formatoData) {
        this.formatoData = formatoData;
    }

    public void setFormatoDistancia(final FormatoDistancia formatoDistancia) {
        this.formatoDistancia = formatoDistancia;
    }

    public void setFormatoHora(final FormatoHora formatoHora) {
        this.formatoHora = formatoHora;
    }

    public void setFormatoTemperatura(final FormatoTemperatura formatoTemperatura) {
        this.formatoTemperatura = formatoTemperatura;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
