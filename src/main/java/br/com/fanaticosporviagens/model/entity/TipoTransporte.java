package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.context.i18n.LocaleContextHolder;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 *
 * @author Pedro Sebba
 * @author Carlos Nascimento
 *
 */
public enum TipoTransporte implements EnumTypeInteger {

    ALUGUEL_VEICULO(6, "aluguel", "Aluguel de Veículo"),
    AVIAO(1, "aviao", "Voo"),
    EMBARCACAO(4, "embarcacao", "Navio/Barco"),
    ONIBUS(2, "onibus", "Ônibus"),
    TREM(3, "trem", "Trem"),
    VEICULO_PARTICULAR(5, "carro", "Veículo");

    public static TipoTransporte[] listaOrdenada() {
        return new TipoTransporte[] { AVIAO, ONIBUS, TREM, EMBARCACAO, VEICULO_PARTICULAR, ALUGUEL_VEICULO };
    }

    private int codigo;

    private final String defaultLocale = "pt_BR";

    private final HashMap<String, String> descricoes = new HashMap<String, String>();

    private String icone;

    TipoTransporte(final int codigo, final String icone, final String... descricoes) {
        this.codigo = codigo;
        this.icone = icone;
        final ArrayList<String> locales = new ArrayList<String>();
        locales.add("pt_BR");
        for (int i = 0; i < descricoes.length; i++) {
            this.descricoes.put(locales.get(i), descricoes[i]);
        }
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        final String descricao = this.descricoes.get(LocaleContextHolder.getLocale().toString());
        if (descricao == null) {
            this.descricoes.get(this.defaultLocale);
        }
        return descricao;
    }

    public String getIcone() {
        return this.icone;
    }

    public boolean isAluguelVeiculo() {
        return this.equals(ALUGUEL_VEICULO);
    }

    public boolean isNavegacao() {
        return this.equals(EMBARCACAO);
    }

    public boolean isOnibus() {
        return this.equals(ONIBUS);

    }

    public boolean isTrem() {
        return this.equals(TREM);
    }

    public boolean isVeiculoParticular() {
        return this.equals(VEICULO_PARTICULAR);
    }

    public boolean isVoo() {
        return this.equals(AVIAO);

    }

}
