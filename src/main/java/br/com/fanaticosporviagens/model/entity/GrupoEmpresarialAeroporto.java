package br.com.fanaticosporviagens.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "grupo_empresarial_aeroporto")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_grupo_empresarial_aeroporto", allocationSize = 1)
public class GrupoEmpresarialAeroporto extends BaseEntity<Long> {

    private static final long serialVersionUID = -6060949399056234851L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_local")
    private Aeroporto aeroporto;

    @Column(nullable = false)
    private Boolean ativo;

    /**
     * Codeshare é um acordo de cooperação pelo qual uma companhia aérea transporta passageiros
     * cujos bilhetes tenham sido emitidos por outra companhia.
     * O objetivo é oferecer aos passageiros mais destinos do que uma companhia aérea poderia
     * oferecer isoladamente.
     */
    @Column(nullable = false)
    private Boolean codeshare;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_grupo_empresarial")
    private GrupoEmpresarial grupoEmpresarial;

    public Aeroporto getAeroporto() {
        return this.aeroporto;
    }

    public Boolean getAtivo() {
        return this.ativo;
    }

    public Boolean getCodeshare() {
        return this.codeshare;
    }

    public GrupoEmpresarial getGrupoEmpresarial() {
        return this.grupoEmpresarial;
    }

    public void setAeroporto(final Aeroporto aeroporto) {
        this.aeroporto = aeroporto;
    }

    public void setAtivo(final Boolean ativo) {
        this.ativo = ativo;
    }

    public void setCodeshare(final Boolean codeshare) {
        this.codeshare = codeshare;
    }

    public void setGrupoEmpresarial(final GrupoEmpresarial grupoEmpresarial) {
        this.grupoEmpresarial = grupoEmpresarial;
    }

}
