package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seo_sitemap_type")
public class SitemapType {

    @Id
    @Column(name = "id_seo_sitemap_type")
    private Long id;

    @Column(name = "nm_seo_sitemap_type")
    private String nome;

    @Column(name = "seo_sitemap_type_prefixo")
    private String prefixo;

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getPrefixo() {
        return this.prefixo;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setPrefixo(final String prefixo) {
        this.prefixo = prefixo;
    }

}
