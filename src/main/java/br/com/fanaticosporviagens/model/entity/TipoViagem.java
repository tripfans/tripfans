package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_tipo_viagem")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_tipo_viagem", allocationSize = 1)
@Table(name = "tipo_viagem")
public class TipoViagem extends BaseEntity<Long> {

    private static final long serialVersionUID = -7063511866160479588L;

    @Column(name = "st_ativo", nullable = false)
    private boolean ativo;

    @Column(name = "ds_tipo_viagem", nullable = false)
    private String descricao;

    public String getDescricao() {
        return this.descricao;
    }

    public boolean isAtivo() {
        return this.ativo;
    }

    public void setAtivo(final boolean ativo) {
        this.ativo = ativo;
    }

    public void setDescricao(final String nome) {
        this.descricao = nome;
    }

}
