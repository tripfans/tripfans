package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class LocalAlbum {
    private final List<LocalAlbum> albunsFilhos = new ArrayList<LocalAlbum>();

    private final Set<Foto> fotos = new TreeSet<Foto>(new Comparator<Foto>() {

        @Override
        public int compare(final Foto o1, final Foto o2) {
            return o1.getId().compareTo(o2.getId());
        }
    });

    private final Local local;

    public LocalAlbum(final Local local) {
        this.local = local;
    }

    public void addAlbumFilho(final LocalAlbum albumFilho) {
        this.albunsFilhos.add(albumFilho);
    }

    public void addAllFotos(final Collection<Foto> fotos) {
        if (fotos != null) {
            this.fotos.addAll(fotos);
        }
    }

    public List<LocalAlbum> getAlbunsFilhos() {
        return this.albunsFilhos;
    }

    public Set<Foto> getFotos() {
        return this.fotos;
    }

    public Local getLocal() {
        return this.local;
    }

    public Integer getQuantidadeTotalFotos() {
        int quantidade = this.fotos.size();

        for (final LocalAlbum album : this.albunsFilhos) {
            quantidade += album.getQuantidadeTotalFotos();
        }

        return quantidade;
    }

}
