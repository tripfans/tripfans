package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe criada para armazenar temporariamente os dados importados da base do Foursquare
 * 
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "local_foursquare")
public class LocalFoursquare implements Serializable {

    @Column(name = "cep_4sq")
    private String cep;

    @Column(name = "checkins_count_4sq")
    private Integer checkinsCount;

    @Column(name = "complemento_endereco_4sq")
    private String complementoEndereco;

    /**
     * Indica se nós confirmamos as informações e se o local está correto
     */
    @Column(name = "dados_confirmado_4sq")
    private boolean dadosConfirmados;

    @Column(name = "descricao_4sq", length = 2000)
    private String descricao;

    /**
     * Registra qual a distancia (em metros) do local no 4SQ do local da nossa base (informacao fornecida pelo 4SQ através da Lat/Long)
     */
    @Column(name = "distancia_local_original_4sq")
    private Integer distanciaDoLocalOriginal;

    @Column(name = "email_4sq")
    private String email;

    @Column(name = "endereco_4sq")
    private String endereco;

    @Column(name = "facebook_4sq")
    private String facebook;

    @Id
    @Column
    private String id;

    @Column(name = "id_categoria_4sq")
    private String idCategoria;

    @Column(name = "ids_todas_categoria_4sq", length = 500)
    private String idsTodasCategorias;

    @Column(name = "latitude_4sq")
    private Double latitude;

    /**
     * Indica se o primeiro registro retornado pelo Foursquare bateu com o nosso criterio de verificação do nome
     */
    @Column(name = "local_combinou_4sq")
    private Boolean localCombinouDePrimeira;

    /**
     * Indica se o local nao foi encontrado na base do Foursquare
     */
    @Column(name = "local_nao_encontrado_4sq")
    private Boolean localNaoEncontrado;

    @Column(name = "longitude_4sq")
    private Double longitude;

    @Column(name = "nome_4sq")
    private String nome;

    @Column(name = "nome_categoria_4sq")
    private String nomeCategoria;

    @Column(name = "nomes_todas_categoria_4sq", length = 500)
    private String nomesTodasCategorias;

    @Column(name = "quantidade_locais_semelhantes_4sq")
    private Integer quantidadeLocaisSemelhantes;

    @Column(name = "short_url_4sq")
    private String shortUrl;

    @Column(name = "sigla_categoria_4sq")
    private String siglaCategoria;

    @Column(name = "telefone_4sq")
    private String telefone;

    @Column(name = "telefone_formatado_4sq")
    private String telefoneFormatado;

    @Column(name = "tip_count_4sq")
    private Integer tipCount;

    @Column(name = "twitter_4sq")
    private String twitter;

    @Column(name = "url_4sq")
    private String url;

    @Column(name = "users_count_4sq")
    private Integer usersCount;

    public String getCep() {
        return this.cep;
    }

    public Integer getCheckinsCount() {
        return this.checkinsCount;
    }

    public String getComplementoEndereco() {
        return this.complementoEndereco;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Integer getDistanciaDoLocalOriginal() {
        return this.distanciaDoLocalOriginal;
    }

    public String getEmail() {
        return this.email;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public String getFacebook() {
        return this.facebook;
    }

    public String getId() {
        return this.id;
    }

    public String getIdCategoria() {
        return this.idCategoria;
    }

    public String getIdsTodasCategorias() {
        return this.idsTodasCategorias;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public String[] getListaNomesCategorias() {
        if (this.nomesTodasCategorias != null) {
            return this.nomesTodasCategorias.split(",");
        }
        return null;
    }

    public Boolean getLocalCombinouDePrimeira() {
        return this.localCombinouDePrimeira;
    }

    public Boolean getLocalNaoEncontrado() {
        return this.localNaoEncontrado;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeCategoria() {
        return this.nomeCategoria;
    }

    public String getNomesTodasCategorias() {
        return this.nomesTodasCategorias;
    }

    public Integer getQuantidadeLocaisSemelhantes() {
        return this.quantidadeLocaisSemelhantes;
    }

    public String getShortUrl() {
        return this.shortUrl;
    }

    public String getSiglaCategoria() {
        return this.siglaCategoria;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public String getTelefoneFormatado() {
        return this.telefoneFormatado;
    }

    public Integer getTipCount() {
        return this.tipCount;
    }

    public String getTwitter() {
        return this.twitter;
    }

    public String getUrl() {
        return this.url;
    }

    public Integer getUsersCount() {
        return this.usersCount;
    }

    public boolean isDadosConfirmados() {
        return this.dadosConfirmados;
    }

    public void setCep(final String cep) {
        this.cep = cep;
    }

    public void setCheckinsCount(final Integer checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public void setComplementoEndereco(final String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }

    public void setDadosConfirmados(final boolean dadosConfirmados) {
        this.dadosConfirmados = dadosConfirmados;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDistanciaDoLocalOriginal(final Integer distanciaDoLocalOriginal) {
        this.distanciaDoLocalOriginal = distanciaDoLocalOriginal;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public void setFacebook(final String facebook) {
        this.facebook = facebook;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setIdCategoria(final String idCategoria) {
        this.idCategoria = idCategoria;
    }

    public void setIdsTodasCategorias(final String idsTodasCategorias) {
        this.idsTodasCategorias = idsTodasCategorias;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLocalCombinouDePrimeira(final Boolean localCombinouDePrimeira) {
        this.localCombinouDePrimeira = localCombinouDePrimeira;
    }

    public void setLocalNaoEncontrado(final Boolean localNaoEncontrado) {
        this.localNaoEncontrado = localNaoEncontrado;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeCategoria(final String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    public void setNomesTodasCategorias(final String nomesTodasCategorias) {
        this.nomesTodasCategorias = nomesTodasCategorias;
    }

    public void setQuantidadeLocaisSemelhantes(final Integer quantidadeLocaisSemelhantes) {
        this.quantidadeLocaisSemelhantes = quantidadeLocaisSemelhantes;
    }

    public void setShortUrl(final String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public void setSiglaCategoria(final String siglaCategoria) {
        this.siglaCategoria = siglaCategoria;
    }

    public void setTelefone(final String telefone) {
        this.telefone = telefone;
    }

    public void setTelefoneFormatado(final String telefoneFormatado) {
        this.telefoneFormatado = telefoneFormatado;
    }

    public void setTipCount(final Integer tipCount) {
        this.tipCount = tipCount;
    }

    public void setTwitter(final String twitter) {
        this.twitter = twitter;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setUsersCount(final Integer usersCount) {
        this.usersCount = usersCount;
    }
}
