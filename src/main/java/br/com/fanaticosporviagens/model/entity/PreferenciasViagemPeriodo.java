package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class PreferenciasViagemPeriodo implements Serializable {

    private static final long serialVersionUID = 8391775903760768417L;

    @Column(name = "alta_temporada")
    private Boolean altaTemporada = false;

    @Column(name = "baixa_temporada")
    private Boolean baixaTemporada = false;

    @Column(name = "feriados")
    private Boolean feriados = false;

    @Column(name = "ferias")
    private Boolean ferias = false;

    @Column(name = "finais_semana")
    private Boolean finaisSemana = false;

    public Boolean getAltaTemporada() {
        return this.altaTemporada;
    }

    public Boolean getBaixaTemporada() {
        return this.baixaTemporada;
    }

    public Boolean getFeriados() {
        return this.feriados;
    }

    public Boolean getFerias() {
        return this.ferias;
    }

    public Boolean getFinaisSemana() {
        return this.finaisSemana;
    }

    public void setAltaTemporada(final Boolean altaTemporada) {
        this.altaTemporada = altaTemporada;
    }

    public void setBaixaTemporada(final Boolean baixaTemporada) {
        this.baixaTemporada = baixaTemporada;
    }

    public void setFeriados(final Boolean feriados) {
        this.feriados = feriados;
    }

    public void setFerias(final Boolean ferias) {
        this.ferias = ferias;
    }

    public void setFinaisSemana(final Boolean finaisSemana) {
        this.finaisSemana = finaisSemana;
    }
}
