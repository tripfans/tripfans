package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum AvaliacaoCriterio implements EnumTypeInteger {

    ATENDIMENTO(2, "classificacaoAtendimento", new LocalType[] { LocalType.AGENCIA, LocalType.HOTEL, LocalType.RESTAURANTE }, "Atendimento"),
    BELEZA(12, "classificacaoBeleza", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE, LocalType.ESTADO, LocalType.PAIS,
            LocalType.CONTINENTE, LocalType.ATRACAO }, "Beleza"),
    COISAS_PARA_FAZER(20, "classificacaoCoisasParaFazer", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE },
                      "Coisas Para Fazer"),
    COMIDA(3, "classificacaoComida", new LocalType[] { LocalType.RESTAURANTE }, "Comida"),
    CUSTO(14, "classificacaoCusto", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE, LocalType.ESTADO, LocalType.PAIS,
            LocalType.CONTINENTE }, "Custo"),
    LIMPEZA(4, "classificacaoLimpeza", new LocalType[] { LocalType.AEROPORTO, LocalType.AGENCIA, LocalType.ATRACAO,
            LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE, LocalType.HOTEL, LocalType.IMOVEL_ALUGUEL_TEMPORADA, LocalType.RESTAURANTE },
            "Limpeza"),
    LOCALIZACAO(5, "classificacaoLocalizacao", new LocalType[] { LocalType.AEROPORTO, LocalType.AGENCIA, LocalType.ATRACAO, LocalType.HOTEL,
            LocalType.IMOVEL_ALUGUEL_TEMPORADA, LocalType.RESTAURANTE }, "Localização"),
    LOCOMOCAO_TRANSITO(15, "classificacaoLocomocaoTransito", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE,
            LocalType.ESTADO, LocalType.PAIS, LocalType.CONTINENTE }, "Locomoção/Transito"),
    LOJAS(11, "classificacaoLojas", new LocalType[] { LocalType.AEROPORTO }, "Lojas"),
    ORGANICACAO(16, "classificacaoOrganicacao", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE, LocalType.ESTADO,
            LocalType.PAIS, LocalType.CONTINENTE }, "Organização"),
    PRACA_DE_ALIMENTACAO(10, "classificacaoPracaDeAlimentacao", new LocalType[] { LocalType.AEROPORTO }, "Praça de alimentação"),
    PRECO_QUALIDADE(6, "classificacaoPreco", new LocalType[] { LocalType.AGENCIA, LocalType.ATRACAO, LocalType.HOTEL,
            LocalType.IMOVEL_ALUGUEL_TEMPORADA, LocalType.RESTAURANTE }, "Preço/Qualidade"),
    QUARTOS(18, "classificacaoQuartos", new LocalType[] { LocalType.HOTEL, LocalType.IMOVEL_ALUGUEL_TEMPORADA }, "Quartos"),
    SEGURANCA(17, "classificacaoSeguranca", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE, LocalType.ESTADO,
            LocalType.PAIS, LocalType.CONTINENTE, LocalType.ATRACAO }, "Segurança"),
    SERVICOS_OFERECIDOS(7, "classificacaoServicosOferecidos", new LocalType[] { LocalType.AGENCIA, LocalType.HOTEL,
            LocalType.IMOVEL_ALUGUEL_TEMPORADA, LocalType.ATRACAO, LocalType.RESTAURANTE }, "Serviços oferecidos"),
    VIDA_NOTURNA(19, "classificacaoVidaNoturna", new LocalType[] { LocalType.LOCAL_INTERESSE_TURISTICO, LocalType.CIDADE }, "Vida Noturna");

    public static List<AvaliacaoCriterio> getCriterios(final LocalType tipoLocal) {
        final List<AvaliacaoCriterio> classificacoesPossiveis = new ArrayList<AvaliacaoCriterio>();

        for (final AvaliacaoCriterio classificacao : AvaliacaoCriterio.values()) {
            for (final LocalType tipo : classificacao.getTiposLocais()) {
                if (tipo.equals(tipoLocal)) {
                    classificacoesPossiveis.add(classificacao);
                }
            }
        }

        return classificacoesPossiveis;
    }

    private final String nomeAtributo;

    private final LocalType[] tiposLocais;

    private final EnumI18nUtil util;

    AvaliacaoCriterio(final int codigo, final String nomeAtributo, final LocalType[] tiposLocais, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        this.tiposLocais = tiposLocais;
        this.nomeAtributo = nomeAtributo;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public String getNomeAtributo() {
        return this.nomeAtributo;
    }

    public LocalType[] getTiposLocais() {
        return this.tiposLocais;
    }

}
