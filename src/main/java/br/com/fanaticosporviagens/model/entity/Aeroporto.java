package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("5")
public class Aeroporto extends LocalEnderecavel {

    private static final long serialVersionUID = -2268558192716169667L;

    /**
     * Código IATA (International Air Transport Association) do aeroporto. Por exemplo:
     * Congonhas - CGH
     * Aeroporto Internacional Juscelino Kubitschek - Brasília - BSB
     */
    @Column(name = "sigla_iso")
    private String codigoIATA;

    /**
     * Código ICAO (International Civil Aviation Organization) do aeroporto. Por exemplo:
     * Congonhas - SBSP
     * Aeroporto Internacional Juscelino Kubitschek - Brasília - SBBR
     */
    @Column(name = "sigla_iso3")
    private String codigoICAO;

    public String getCodigoIATA() {
        return this.codigoIATA;
    }

    public String getCodigoICAO() {
        return this.codigoICAO;
    }

    public String getNomeCidadeComSiglaAeroporoto() {
        String nome = "";
        // TODO ver pq está dando LAZY EXCEPTION
        try {
            if (this.getCidade() != null) {
                nome += getCidade().getNome();
            }
        } catch (final Exception e) {
            nome += getNome();
        }
        return nome += " (" + getCodigoIATA() + ")";
    }

    public void setCodigoIATA(final String codigoIATA) {
        this.codigoIATA = codigoIATA;
    }

    public void setCodigoICAO(final String codigoICAO) {
        this.codigoICAO = codigoICAO;
    }

    // TODO Arrumar uma forma de mostrar no mapa esse aeroporto.
}
