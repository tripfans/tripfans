package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

public enum TipoInteresseUsuarioEmLocal implements Serializable {

    DESEJA_IR("Deseja ir", "Desejam Ir", "desejaIr", true),
    ESCREVEU_AVALIACAO("Escreveu Avaliação", "Escreveram Avaliações", "escreveuAvaliacao", false),
    ESCREVEU_DICA("Escreveu Dica", "Escreveram Dicas", "escreveuDica", false),
    ESCREVEU_PERGUNTA("Escreveu Pergunta", "Escreveram Perguntas", "escreveuPergunta", false),
    FAVORITO("Um dos seus favoritos", "Marcaram como Local Favorito", "favorito", true),
    IMPERDIVEL("Imperdível", "Consideram Imperdível", "imperdivel", true),
    INDICA("Recomendável", "Indicam", "indica", true),
    JA_FOI("Já foi", "Já Foram", "jaFoi", true),
    RESPONDEU_PERGUNTA("Respondeu Pergunta", "Responderam Pergunta", "respondeuPergunta", false),
    SEGUIR("Seguir", "Seguem", "seguir", true);

    public static TipoInteresseUsuarioEmLocal fromString(final String tipo) {
        for (final TipoInteresseUsuarioEmLocal tipoInteresse : TipoInteresseUsuarioEmLocal.values()) {
            if (tipoInteresse.atributoInteresse.equals(tipo)) {
                return tipoInteresse;
            }
        }
        throw new IllegalArgumentException("Tipo de Interesse inválido.");
    }

    public static TipoInteresseUsuarioEmLocal fromUrlPath(final String urlPath) {
        for (final TipoInteresseUsuarioEmLocal tipo : TipoInteresseUsuarioEmLocal.values()) {
            if (tipo.getUrlPath().equals(urlPath)) {
                return tipo;
            }
        }
        throw new IllegalArgumentException("UrlPath inválida. (" + urlPath + ")");
    }

    private final String atributoInteresse;

    private final String tituloInteresse;

    private final String tituloInteresseAmigos;

    private final boolean usuarioInforma;

    private TipoInteresseUsuarioEmLocal(final String tituloInteresse, final String tituloInteresseAmigos, final String atributoInteresse,
            final boolean usuarioInforma) {
        this.tituloInteresse = tituloInteresse;
        this.tituloInteresseAmigos = tituloInteresseAmigos;
        this.atributoInteresse = atributoInteresse;
        this.usuarioInforma = usuarioInforma;
    }

    public String getAtributoInteresse() {
        return this.atributoInteresse;
    }

    public String getTituloInteresse() {
        return this.tituloInteresse;
    }

    public String getTituloInteresseAmigos() {
        return this.tituloInteresseAmigos;
    }

    public String getUrlPath() {
        return this.toString().toLowerCase();
    }

    public boolean isInteresseDesejaIr() {
        return DESEJA_IR.equals(this);
    }

    public boolean isInteresseFavorito() {
        return FAVORITO.equals(this);
    }

    public boolean isInteresseImperdivel() {
        return IMPERDIVEL.equals(this);
    }

    public boolean isInteresseIndica() {
        return INDICA.equals(this);
    }

    public boolean isInteresseJaFoi() {
        return JA_FOI.equals(this);
    }

    public boolean isUsuarioInforma() {
        return this.usuarioInforma;
    }
}
