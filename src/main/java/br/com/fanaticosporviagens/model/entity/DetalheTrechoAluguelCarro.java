package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
public class DetalheTrechoAluguelCarro extends DetalheTrecho {

    private static final long serialVersionUID = -7620093083405179756L;

    private String acessorios;

    private String detalhesCarro;

    private String modeloCarro;

    /*
     * Coloquei distanciaExtra e n�o quilometroExtra pensando nas configura��es do usu�rio que podem ser persolanizadas para milhas ou quilometros
     */
    private BigDecimal valorPorDistanciaExtra;

}
