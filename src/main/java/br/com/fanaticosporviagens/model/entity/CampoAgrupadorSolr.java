package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

public class CampoAgrupadorSolr {

    class ValorAgrupador {
        public long count;

        public String nome;

        public ValorAgrupador(final String nome, final long count) {
            super();
            this.nome = nome;
            this.count = count;
        }

        public long getCount() {
            return this.count;
        }

        public String getNome() {
            return this.nome;
        }

    }

    private String nome;

    private List<ValorAgrupador> valores = new ArrayList<ValorAgrupador>();

    public CampoAgrupadorSolr(final String nome) {
        this.nome = nome;
    }

    public void adicionaValores(final String nome, final long count) {
        this.valores.add(new ValorAgrupador(nome, count));
    }

    public String getNome() {
        return this.nome;
    }

    public List<ValorAgrupador> getValores() {
        return this.valores;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setValores(final List<ValorAgrupador> valores) {
        this.valores = valores;
    }

}
