package br.com.fanaticosporviagens.model.entity.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 
 * Usada para especificar quais açoes devem ser geradas apos uma entidade ser persisistida na base.
 * 
 * @author Carlos Nascimento
 * 
 */
@Target({ TYPE })
@Retention(RUNTIME)
public @interface GeradorAcoes {

    Acao[] acoes();

    /**
     * (Opcional) Objeto alvo da ação
     */
    String alvo() default "";

    /**
     * (Opcional) Usuario autor da ação.
     */
    String autor() default "";

    /**
     * (Opcional) Usuario destinatário da ação.
     */
    String destinatario() default "";

    // GerarVariasAcoes[] gerarVarias() default {};
}
