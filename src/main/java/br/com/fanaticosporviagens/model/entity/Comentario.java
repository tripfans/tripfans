package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.MetaValue;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.Where;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "comentario")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_comentario", allocationSize = 1)
@TypeDefs(value = { @TypeDef(name = "TipoComentario", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.Comentario$TipoComentario") }) })
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_comentario", nullable = false, unique = true)) })
@Filters(@Filter(name = "dataExclusao", condition = "data_exclusao is null"))
public class Comentario extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel, SocialShareableItem {

    private static final long serialVersionUID = 7533360135369077623L;

    private static final String TEXTO_TWITTER = "Escrevi um comentário sobre %s no TripFans: ";

    @Any(metaColumn = @Column(name = "tipo_objeto_comentado"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "Album", targetEntity = Album.class),
            @MetaValue(value = "Avaliacao", targetEntity = Avaliacao.class), @MetaValue(value = "Comentario", targetEntity = Comentario.class),
            @MetaValue(value = "DiarioViagem", targetEntity = DiarioViagem.class), @MetaValue(value = "Dica", targetEntity = Dica.class),
            @MetaValue(value = "Foto", targetEntity = Foto.class), @MetaValue(value = "Usuario", targetEntity = Usuario.class),
            @MetaValue(value = "RelatoViagem", targetEntity = RelatoViagem.class), @MetaValue(value = "PlanoViagem", targetEntity = PlanoViagem.class) })
    @JoinColumn(name = "id_objeto_comentado")
    public Comentavel objetoComentado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_album", nullable = true)
    private Album album;

    @Column(name = "aprovado")
    private Boolean aprovado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @Column(name = "bloqueado", columnDefinition = "boolean DEFAULT false")
    private Boolean bloqueado = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pai", nullable = true)
    private Comentario comentarioPai;

    @OneToMany(mappedBy = "comentarioPai", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "data_exclusao IS NULL")
    @OrderBy("dataCriacao")
    private List<Comentario> comentarios = new ArrayList<Comentario>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_diario_viagem", nullable = true)
    private DiarioViagem diarioViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = true)
    private Dica dica;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_foto", nullable = true)
    private Foto foto;

    @Column(name = "id_pai", insertable = false, updatable = false)
    private Long idComentarioPai;

    @Column(name = "id_objeto_comentado", insertable = false, updatable = false)
    private Long idObjetoComentado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local", nullable = true)
    private Local local;

    @Column(name = "motivoBloqueio")
    private String motivoBloqueio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_item_diario_viagem", nullable = true)
    private RelatoViagem relatoViagem;

    @Column(name = "texto", length = 2000)
    private String texto;

    @Type(type = "TipoComentario")
    @Column(name = "id_tipo_comentario", nullable = false)
    private TipoComentario tipo;

    @Column(name = "tipo_objeto_comentado", insertable = false, updatable = false)
    private String tipoObjetoComentado;

    @Column(name = "url_path", nullable = false, unique = true, updatable = false)
    private String urlPath;

    /**
     * Perfil do usuario que esta sendo comentado
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = true)
    private Usuario usuario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private PlanoViagem planoViagem;

    @Column(name = "visibilidade")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade visibilidade;

    public Album getAlbum() {
        return this.album;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) getObjetoAlvo();
    }

    public Boolean getAprovado() {
        return this.aprovado;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public Boolean getBloqueado() {
        return this.bloqueado;
    }

    public boolean getComentarioAlbum() {
        return TipoComentario.ALBUM.equals(this.tipo);
    }

    public Comentario getComentarioPai() {
        return this.comentarioPai;
    }

    public boolean getComentarioPerfil() {
        return TipoComentario.PERFIL.equals(this.tipo);
    }

    public List<Comentario> getComentarios() {
        return this.comentarios;
    }

    public Date getData() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    @Override
    public String getDescricaoAlvo() {
        return null;
    }

    public DiarioViagem getDiarioViagem() {
        return this.diarioViagem;
    }

    public Dica getDica() {
        return this.dica;
    }

    @Override
    public String getFacebookPostText() {
        return getTexto();
    }

    @Override
    public String getFacebookPostTitle() {
        return "Comentário";
    }

    public Foto getFoto() {
        return this.foto;
    }

    public Long getIdObjetoComentado() {
        return this.idObjetoComentado;
    }

    public RelatoViagem getItemDiarioViagem() {
        return this.relatoViagem;
    }

    public Local getLocal() {
        return this.local;
    }

    public String getMotivoBloqueio() {
        return this.motivoBloqueio;
    }

    @Transient
    public Comentavel getObjetoAlvo() {
        if (getObjetoComentado() != null) {
            return getObjetoComentado();
        } else if (this.tipo.equals(TipoComentario.COMENTARIO)) {
            return getComentarioPai();
        } else if (this.tipo.equals(TipoComentario.ALBUM)) {
            return this.getAlbum();
        } else if (this.tipo.equals(TipoComentario.AVALIACAO)) {
            return this.getAvaliacao();
        } else if (this.tipo.equals(TipoComentario.DIARIO)) {
            return this.getDiarioViagem();
        } else if (this.tipo.equals(TipoComentario.DICA)) {
            return this.getDica();
        } else if (this.tipo.equals(TipoComentario.FOTO)) {
            return this.getFoto();
        } else if (this.tipo.equals(TipoComentario.RELATO_VIAGEM)) {
            return this.getItemDiarioViagem();
        } else if (this.tipo.equals(TipoComentario.PERFIL)) {
            return this.getUsuario();
        } else if (this.tipo.equals(TipoComentario.VIAGEM)) {
            return this.getViagem();
        }
        return null;
    }

    public Comentavel getObjetoComentado() {
        return this.objetoComentado;
    }

    @Override
    public String getPrefix() {
        return "c";
    }

    @Override
    public String getShareableLink() {
        final StringBuffer linkBuffer = new StringBuffer();// = new StringBuffer(UrlLocalResolver.getUrl(this.getLocalAvaliado()));
        linkBuffer.append("?tab=avaliacoes&avaliacao=");
        if (getUrlPath() != null) {
            linkBuffer.append(this.getUrlPath());
        } else {
            linkBuffer.append(this.getId());
        }
        return linkBuffer.toString();
    }

    public String getTexto() {
        return this.texto;
    }

    public TipoComentario getTipo() {
        return this.tipo;
    }

    @Override
    public String getTwitterPostText() {
        return String.format(TEXTO_TWITTER, getTexto());
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public PlanoViagem getViagem() {
        return this.planoViagem;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    public boolean isComentario(final Class<? extends Comentavel> classe) {
        return classe.getSimpleName().equals(this.tipoObjetoComentado);
    }

    public boolean isComentarioAlbum() {
        return TipoComentario.ALBUM.equals(this.tipo) || isComentario(Album.class);
    }

    public boolean isComentarioAvaliacao() {
        return TipoComentario.AVALIACAO.equals(this.tipo) || isComentario(Avaliacao.class);
    }

    public boolean isComentarioDiarioViagem() {
        return TipoComentario.DIARIO.equals(this.tipo) || isComentario(DiarioViagem.class);
    }

    public boolean isComentarioDica() {
        return TipoComentario.DICA.equals(this.tipo) || isComentario(Dica.class);
    }

    public boolean isComentarioFoto() {
        return TipoComentario.FOTO.equals(this.tipo) || isComentario(Foto.class);
    }

    public boolean isComentarioPerfil() {
        return TipoComentario.PERFIL.equals(this.tipo) || isComentario(Usuario.class);
    }

    public boolean isComentarioRelatoViagem() {
        return TipoComentario.RELATO_VIAGEM.equals(this.tipo) || isComentario(RelatoViagem.class);
    }

    public boolean isComentarioViagem() {
        return TipoComentario.VIAGEM.equals(this.tipo) || isComentario(PlanoViagem.class);
    }

    public boolean isRespostaComentario() {
        return this.idComentarioPai != null;
    }

    public void setAlbum(final Album album) {
        this.album = album;
    }

    public void setAprovado(final Boolean aprovado) {
        this.aprovado = aprovado;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setBloqueado(final Boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public void setComentarioPai(final Comentario comentarioPai) {
        this.comentarioPai = comentarioPai;
    }

    public void setComentarios(final List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    public void setData(final Date data) {
        if (data != null) {
            this.setDataCriacao(new LocalDateTime(data));
        }
    }

    public void setDiarioViagem(final DiarioViagem diarioViagem) {
        this.diarioViagem = diarioViagem;
    }

    public void setDica(final Dica dica) {
        this.dica = dica;
    }

    public void setFoto(final Foto foto) {
        this.foto = foto;
    }

    public void setItemDiarioViagem(final RelatoViagem relatoViagem) {
        this.relatoViagem = relatoViagem;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setMotivoBloqueio(final String motivoBloqueio) {
        this.motivoBloqueio = motivoBloqueio;
    }

    @Transient
    public void setObjetoAlvo(final TipoComentario tipo, final Comentavel objetoAlvo) {
        this.setObjetoComentado(objetoAlvo);
        if (tipo.equals(TipoComentario.AVALIACAO)) {
            this.setAvaliacao((Avaliacao) objetoAlvo);
        } else if (tipo.equals(TipoComentario.DIARIO)) {
            this.setDiarioViagem((DiarioViagem) objetoAlvo);
        } else if (tipo.equals(TipoComentario.DICA)) {
            this.setDica((Dica) objetoAlvo);
        } else if (tipo.equals(TipoComentario.RELATO_VIAGEM)) {
            this.setItemDiarioViagem((RelatoViagem) objetoAlvo);
        } else if (tipo.equals(TipoComentario.PERFIL)) {
            this.setUsuario((Usuario) objetoAlvo);
        } else if (tipo.equals(TipoComentario.VIAGEM)) {
            this.setViagem((PlanoViagem) objetoAlvo);
        } else if (tipo.equals(TipoComentario.ALBUM)) {
            this.setAlbum((Album) objetoAlvo);
        } else if (tipo.equals(TipoComentario.FOTO)) {
            this.setFoto((Foto) objetoAlvo);
        }
    }

    public void setObjetoComentado(final Comentavel objetoComentado) {
        this.objetoComentado = objetoComentado;
    }

    public void setTexto(final String texto) {
        this.texto = texto;
    }

    public void setTipo(final TipoComentario tipo) {
        this.tipo = tipo;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

    public void setViagem(final PlanoViagem planoViagem) {
        this.planoViagem = planoViagem;
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

    public enum TipoComentario implements EnumTypeInteger {

        /**
         * TODO ver uma forma de melhorar isso:
         * 
         * Ao acrecestar um tipo de comentario, lembra de:
         * 
         * - acrescentar e mapear na classe Comentario, um atributo representando este novo tipo e gerar seus respectivos metodos getter e setter
         * - acrescentar na classe Comentario, um bloco if no método setObjetoAlvo() representando este novo tipo
         * - incluir o parametro na query "Comentario.consultarComentarios" no arquivo Comentario-queries.xml
         * - acrescentar um bloco if no método consultarComentarios da classe ComentarioService
         */
        ALBUM(6, Album.class, "Album"),
        AVALIACAO(5, Avaliacao.class, "Avaliação"),
        COMENTARIO(9, Comentario.class, "Comentario"),
        DIARIO(3, DiarioViagem.class, "Diário de PlanoViagem"),
        DICA(8, Dica.class, "Dica"),
        FOTO(7, Foto.class, "Foto"),
        PERFIL(1, Usuario.class, "Perfil"),
        RELATO_VIAGEM(4, RelatoViagem.class, "AtividadePlano do diário"),
        VIAGEM(2, PlanoViagem.class, "PlanoViagem");

        private final Class<? extends Comentavel> classeAlvo;

        private final EnumI18nUtil util;

        TipoComentario(final int codigo, final Class<? extends Comentavel> classeAlvo, final String... descricoes) {
            this.classeAlvo = classeAlvo;
            this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        }

        public Class<? extends Comentavel> getClasseAlvo() {
            return this.classeAlvo;
        }

        @Override
        public Integer getCodigo() {
            return this.util.getCodigo();
        }

        @Override
        public String getDescricao() {
            return this.util.getDescricao();
        }
    }

}
