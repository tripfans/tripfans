package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

public class ContadoresLocal implements Serializable {

    private static final long serialVersionUID = -3026357928015527663L;

    private Long quantidadeAtracoes;

    private Long quantidadeAvaliacoes;

    private Long quantidadeCidades;

    private Long quantidadeDesejamIr;

    private Long quantidadeDicas;

    private Long quantidadeFavorito;

    private Long quantidadeFotos;

    private Long quantidadeHoteis;

    private Long quantidadeImperdivel;

    private Long quantidadeIndicam;

    private Long quantidadeJaForam;

    private Long quantidadePerguntas;

    private Long quantidadeRestaurantes;

    private Long quantidadeSeguem;

    public Long getQuantidadeAtracoes() {
        return this.quantidadeAtracoes;
    }

    public Long getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public Long getQuantidadeCidades() {
        return this.quantidadeCidades;
    }

    public Long getQuantidadeDesejamIr() {
        return this.quantidadeDesejamIr;
    }

    public Long getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public Long getQuantidadeFavorito() {
        return this.quantidadeFavorito;
    }

    public Long getQuantidadeFoto() {
        return this.quantidadeFotos;
    }

    public Long getQuantidadeHotel() {
        return this.quantidadeHoteis;
    }

    public Long getQuantidadeImperdivel() {
        return this.quantidadeImperdivel;
    }

    public Long getQuantidadeIndicam() {
        return this.quantidadeIndicam;
    }

    public Long getQuantidadeJaForam() {
        return this.quantidadeJaForam;
    }

    public Long getQuantidadePergunta() {
        return this.quantidadePerguntas;
    }

    public Long getQuantidadeRestaurante() {
        return this.quantidadeRestaurantes;
    }

    public Long getQuantidadeSeguem() {
        return this.quantidadeSeguem;
    }

    public void setQuantidadeAtracoes(final Long quantidadeAtracoes) {
        this.quantidadeAtracoes = quantidadeAtracoes;
    }

    public void setQuantidadeAvaliacoes(final Long quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

    public void setQuantidadeCidades(final Long quantidadeCidades) {
        this.quantidadeCidades = quantidadeCidades;
    }

    public void setQuantidadeDesejamIr(final Long quantidadeDesejamIr) {
        this.quantidadeDesejamIr = quantidadeDesejamIr;
    }

    public void setQuantidadeDicas(final Long quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

    public void setQuantidadeFavorito(final Long quantidadeFavorito) {
        this.quantidadeFavorito = quantidadeFavorito;
    }

    public void setQuantidadeFoto(final Long quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setQuantidadeHotel(final Long quantidadeHoteis) {
        this.quantidadeHoteis = quantidadeHoteis;
    }

    public void setQuantidadeImperdivel(final Long quantidadeImperdivel) {
        this.quantidadeImperdivel = quantidadeImperdivel;
    }

    public void setQuantidadeIndicam(final Long quantidadeIndicam) {
        this.quantidadeIndicam = quantidadeIndicam;
    }

    public void setQuantidadeJaForam(final Long quantidadeJaForam) {
        this.quantidadeJaForam = quantidadeJaForam;
    }

    public void setQuantidadePergunta(final Long quantidadePerguntas) {
        this.quantidadePerguntas = quantidadePerguntas;
    }

    public void setQuantidadeRestaurante(final Long quantidadeRestaurantes) {
        this.quantidadeRestaurantes = quantidadeRestaurantes;
    }

    public void setQuantidadeSeguem(final Long quantidadeSeguem) {
        this.quantidadeSeguem = quantidadeSeguem;
    }

}
