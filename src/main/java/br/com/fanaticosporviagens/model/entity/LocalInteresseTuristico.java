package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author André Thiago
 * 
 */
@Entity
@DiscriminatorValue("12")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class LocalInteresseTuristico extends LocalGeografico {

    private static final long serialVersionUID = -1286883823103012060L;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_cidade")
    private Cidade cidade;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente")
    private Continente continente;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_estado")
    @JsonIgnore
    private Estado estado;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_pais")
    @JsonIgnore
    private Pais pais;

    public Cidade getCidade() {
        return this.cidade;
    }

    public Continente getContinente() {
        return this.continente;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public String getNomeComEstadoPais() {
        String nomeComEstadoPais = getNome();
        if (this.getLocalizacao().getEstadoNome() != null) {
            nomeComEstadoPais += ", " + this.getLocalizacao().getEstadoNome();
        }
        if (this.getLocalizacao().getPaisNome() != null) {
            nomeComEstadoPais += ", " + this.getLocalizacao().getPaisNome();
        }
        return nomeComEstadoPais;
    }

    public String getNomeComPais() {
        String nomeComPais = getNome();
        if (this.getLocalizacao().getPaisNome() != null) {
            nomeComPais += ", " + this.getLocalizacao().getPaisNome();
        }
        return nomeComPais;
    }

    @Override
    public String getNomeComSiglaEstado() {
        String nomeComSiglaEstado = getNome();
        if (this.getLocalizacao().getEstadoSigla() != null) {
            nomeComSiglaEstado += ", " + this.getLocalizacao().getEstadoSigla();
        }
        return nomeComSiglaEstado;
    }

    public String getNomeComSiglaEstadoNomePais() {
        String nomeComSiglaEstadoNomePais = getNome();
        if (getLocalizacao() != null) {
            if (this.getLocalizacao().getEstadoSigla() != null) {
                nomeComSiglaEstadoNomePais += ", " + this.getLocalizacao().getEstadoSigla();
            }
            if (this.getLocalizacao().getPaisNome() != null) {
                nomeComSiglaEstadoNomePais += ", " + this.getLocalizacao().getPaisNome();
            }
        }
        return nomeComSiglaEstadoNomePais;
    }

    @Override
    public Local getPai() {
        if (getLocalizacao().getCidade() != null) {
            return getLocalizacao().getCidadeLocal();
        }
        if (getLocalizacao().getEstado() != null) {
            return getLocalizacao().getEstadoLocal();
        }
        return getLocalizacao().getPaisLocal();
    }

    public Pais getPais() {
        return this.pais;
    }

    public void setCidade(final Cidade cidade) {
        this.cidade = cidade;
    }

    public void setContinente(final Continente continente) {
        this.continente = continente;
    }

    public void setEstado(final Estado estado) {
        this.estado = estado;
    }

    public void setPais(final Pais pais) {
        this.pais = pais;
    }

}
