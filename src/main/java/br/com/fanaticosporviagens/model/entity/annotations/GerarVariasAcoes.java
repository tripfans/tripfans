package br.com.fanaticosporviagens.model.entity.annotations;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public @interface GerarVariasAcoes {

    /**
     * (Requerido) Ações a serem geradas.
     * Somente uma será executada
     */
    Acao[] acoes();

    /**
     * (Opcional) Objeto alvo da ação
     */
    String alvo() default "";

    /**
     * (Opcional) Usuario autor da ação.
     */
    String autor() default "";

    /**
     * (Opcional) Usuario destinatário da ação.
     */
    String destinatario() default "";
}
