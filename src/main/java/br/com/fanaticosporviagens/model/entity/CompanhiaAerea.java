package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@DiscriminatorValue("19")
public class CompanhiaAerea extends Empresa {

    private static final long serialVersionUID = 9106427472433881022L;

}