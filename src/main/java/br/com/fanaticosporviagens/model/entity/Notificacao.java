package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
import org.hibernate.annotations.Type;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.util.HibernateUtil;

/**
 * Representa as notificações que um usuário pode receber de outros usuários
 * do sistema. Essas notificações podem ser oriundas das seguintes ações:
 * <ul>
 * <li>Convite de amizade;</li>
 * <li>Convite para participação em grupos;</li>
 * <li>Envio de mensagens;</li>
 * <li>Comentários em avaliações;</li>
 * <li>Comentários em dicas;</li>
 * <li>Comentários em respostas;</li>
 * <li>Respostas a perguntas;</li>
 * <li>Recomendações;</li>
 * <li>Etc.</li>
 * </ul>
 * 
 * @author André Thiago
 * 
 */
// TODO criar um método getUrl que resolver a url que deve aparecer...
@Entity
@Table(name = "notificacao")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_notificacao", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_notificacao", nullable = false, unique = true)) })
public class Notificacao extends BaseEntity<Long> {

    private static final long serialVersionUID = -5787547470453188606L;

    @Any(metaColumn = @Column(name = "tipo_acao_principal"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "Amizade", targetEntity = Amizade.class),
            @MetaValue(value = "Comentario", targetEntity = Comentario.class), @MetaValue(value = "Convite", targetEntity = Convite.class),
            @MetaValue(value = "PedidoDica", targetEntity = PedidoDica.class), @MetaValue(value = "Resposta", targetEntity = Resposta.class),
            @MetaValue(value = "VotoUtil", targetEntity = VotoUtil.class), })
    @JoinColumn(name = "id_acao_principal")
    public Acao acaoPrincipal;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_acao_usuario")
    private AcaoUsuario acaoUsuario;

    @Column(name = "data_notificacao", nullable = false)
    private Date dataNotificacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_destinatario", nullable = false)
    private Usuario destinatario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_originador", nullable = false)
    private Usuario originador; // desnormalização

    @Column(name = "pendente", nullable = false, columnDefinition = "boolean DEFAULT true")
    private boolean pendente = true; // indicador de que alguma ação foi tomada (não sei se é preciso)

    @Type(type = "TipoAcao")
    @Column(name = "id_tipo_acao", nullable = true)
    private TipoAcao tipoAcao;

    @Column(name = "tipo_acao_principal", insertable = false, updatable = false)
    private String tipoAcaoPrincipal;

    public Notificacao() {
    }

    public Notificacao(final AcaoUsuario acaoUsuario) {
        this(acaoUsuario.getAutor(), acaoUsuario.getDestinatario());
        this.setAcaoUsuario(acaoUsuario);
        setAcaoPrincipal(acaoUsuario.getAcao());
    }

    public Notificacao(final Convite convite, final TipoAcao tipoAcao) {
        this(convite.getRemetente(), convite.getDestinatario(), tipoAcao);
        setAcaoPrincipal(convite);
    }

    private Notificacao(final Usuario originador, final Usuario destinatario) {
        this(originador, destinatario, null);
    }

    private Notificacao(final Usuario originador, final Usuario destinatario, final TipoAcao tipoAcao) {
        this.originador = originador;
        this.destinatario = destinatario;
        if (tipoAcao != null) {
            this.tipoAcao = tipoAcao;
        }
        this.dataNotificacao = new Date();
    }

    public Acao getAcaoPrincipal() {
        return this.acaoPrincipal;
    }

    public AcaoUsuario getAcaoUsuario() {
        return this.acaoUsuario;
    }

    public Date getDataNotificacao() {
        return this.dataNotificacao;
    }

    public Usuario getDestinatario() {
        return this.destinatario;
    }

    public Usuario getDestino() {
        return this.destinatario;
    }

    public void getObjetoAcao() {

    }

    @Transient
    public AlvoAcao getObjetoAcao(final AlvoAcao alvo) {
        return getAcaoUsuario().getAlvo();
    }

    public Usuario getOriginador() {
        return this.originador;
    }

    public TipoAcao getTipoAcao() {
        if (this.tipoAcao == null && getAcaoUsuario() != null) {
            this.acaoUsuario.getTipoAcao();
        }
        return this.tipoAcao;
    }

    public boolean isNotificacao(final Class<? extends BaseEntity<?>> classe) {
        return classe.getSimpleName().equals(this.tipoAcaoPrincipal);
    }

    public boolean isNotificacaoAmizade() {
        return isNotificacao(Amizade.class);
    }

    public boolean isNotificacaoComentario() {
        return isNotificacao(Comentario.class);
    }

    public boolean isNotificacaoConvite() {
        return isNotificacao(Convite.class);
    }

    public boolean isNotificacaoPedidoDica() {
        return isNotificacao(PedidoDica.class);
    }

    public boolean isNotificacaoResposta() {
        return isNotificacao(Resposta.class);
    }

    public boolean isNotificacaoVotoUtil() {
        return isNotificacao(VotoUtil.class);
    }

    public boolean isPendente() {
        return this.pendente;
    }

    public void setAcaoPrincipal(final Acao acaoPrincipal) {
        this.acaoPrincipal = acaoPrincipal;
        this.tipoAcaoPrincipal = HibernateUtil.getInstance().getClassNameSemProxy(acaoPrincipal);
    }

    public void setAcaoUsuario(final AcaoUsuario acaoUsuario) {
        this.acaoUsuario = acaoUsuario;
        if (acaoUsuario != null) {
            this.tipoAcao = acaoUsuario.getTipoAcao();
        }
    }

    public void setDataNotificacao(final Date dataNotificacao) {
        this.dataNotificacao = dataNotificacao;
    }

    public void setDestinatario(final Usuario destinatario) {
        this.destinatario = destinatario;
    }

    public void setDestino(final Usuario destinatario) {
        this.destinatario = destinatario;
    }

    public void setOriginador(final Usuario originador) {
        this.originador = originador;
    }

    public void setPendente(final boolean pendente) {
        this.pendente = pendente;
    }

    public void setTipoAcao(final TipoAcao tipoAcao) {
        this.tipoAcao = tipoAcao;
    }

}
