package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author André Thiago
 * 
 */
@Embeddable
public class Endereco {

    @Column(name = "bairro")
    private String bairro;

    @Column(name = "cep")
    private String cep;

    @Column(name = "endereco")
    private String descricao;

    public Endereco() {
    }

    public Endereco(final String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Endereco other = (Endereco) obj;
        if (this.cep == null) {
            if (other.cep != null)
                return false;
        } else if (!this.cep.equals(other.cep))
            return false;
        if (this.descricao == null) {
            if (other.descricao != null)
                return false;
        } else if (!this.descricao.equals(other.descricao))
            return false;
        return true;
    }

    public String getBairro() {
        return this.bairro;
    }

    public String getCep() {
        return this.cep;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.cep == null) ? 0 : this.cep.hashCode());
        result = prime * result + ((this.descricao == null) ? 0 : this.descricao.hashCode());
        return result;
    }

    public void setBairro(final String bairro) {
        this.bairro = bairro;
    }

    public void setCep(final String cep) {
        this.cep = cep;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

}
