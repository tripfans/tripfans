package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoPreferenciasViagem implements EnumTypeInteger {

    COMPANHIA_AMIGOS(1, CategoriaPreferenciasViagem.COMPANHIA, "amigos", "Amigos"),
    COMPANHIA_CASAL(2, CategoriaPreferenciasViagem.COMPANHIA, "casal", "Casal"),
    COMPANHIA_FAMILIA(3, CategoriaPreferenciasViagem.COMPANHIA, "familia", "Família"),
    COMPANHIA_SOZINHO(4, CategoriaPreferenciasViagem.COMPANHIA, "sozinho", "Sozinho"),

    ESTILO_CONFORTO(1, CategoriaPreferenciasViagem.ESTILO, "conforto", "Conforto"),
    ESTILO_CUSTO_BENEFICIO(4, CategoriaPreferenciasViagem.ESTILO, "custoBeneficio", "Custo/Benefício"),
    ESTILO_LUXO(3, CategoriaPreferenciasViagem.ESTILO, "luxo", "Luxo"),
    ESTILO_MOCHILEIRO(4, CategoriaPreferenciasViagem.ESTILO, "mochileiro", "Mochileiro"),

    HOSPEDAGEM_ALBERGUE(1, CategoriaPreferenciasViagem.HOSPEDAGEM, "albergue", "Albergue"),
    HOSPEDAGEM_BB(2, CategoriaPreferenciasViagem.HOSPEDAGEM, "bedBreakfast", "Bed and Breakfast (B & B)"),
    HOSPEDAGEM_CAMPING(3, CategoriaPreferenciasViagem.HOSPEDAGEM, "camping", "Camping"),
    HOSPEDAGEM_CASA_APARTAMENTO(4, CategoriaPreferenciasViagem.HOSPEDAGEM, "casaApartamento", "Casa / Apartamento"),
    HOSPEDAGEM_HOTEL(5, CategoriaPreferenciasViagem.HOSPEDAGEM, "hotel", "Hotel"),
    HOSPEDAGEM_POUSADA(6, CategoriaPreferenciasViagem.HOSPEDAGEM, "pousada", "Pousada"),
    HOSPEDAGEM_RESORT(7, CategoriaPreferenciasViagem.HOSPEDAGEM, "resort", "Resort"),

    MEIO_TRANPORTE_AVIAO(1, CategoriaPreferenciasViagem.MEIO_TRANPORTE, "aviao", "Avião"),
    MEIO_TRANPORTE_CARRO(2, CategoriaPreferenciasViagem.MEIO_TRANPORTE, "carro", "Carro"),
    MEIO_TRANPORTE_NAVIO(3, CategoriaPreferenciasViagem.MEIO_TRANPORTE, "navio", "Navio"),
    MEIO_TRANPORTE_ONIBUS(4, CategoriaPreferenciasViagem.MEIO_TRANPORTE, "onibus", "Ônibus"),
    MEIO_TRANPORTE_TRME(5, CategoriaPreferenciasViagem.MEIO_TRANPORTE, "trem", "Trem"),

    PERIODO_ALTA_TEMPORADA(1, CategoriaPreferenciasViagem.PERIODO, "altaTemporada", "Alta temporada"),
    PERIODO_BAIXA_TEMPORADA(2, CategoriaPreferenciasViagem.PERIODO, "baixaTemporada", "Baixa temporada"),
    PERIODO_FERIADOS(3, CategoriaPreferenciasViagem.PERIODO, "feriados", "Feriados"),
    PERIODO_FINAIS_SEMANA(5, CategoriaPreferenciasViagem.PERIODO, "finaisSemana", "Finais de Semana");

    public static List<TipoPreferenciasViagem> getListaTipoPreferencias(final int tipo) {
        final List<TipoPreferenciasViagem> lista = new ArrayList<TipoPreferenciasViagem>();
        for (final TipoPreferenciasViagem tipoPreferenciasViagem : values()) {
            if ((tipoPreferenciasViagem.getCodigo() + "").startsWith(tipo + "")) {
                lista.add(tipoPreferenciasViagem);
            }
        }
        return lista;
    }

    private final CategoriaPreferenciasViagem categoria;

    private final String nome;

    private final EnumI18nUtil util;

    TipoPreferenciasViagem(final int codigo, final CategoriaPreferenciasViagem categoria, final String nome, final String... descricoes) {
        this.nome = nome;
        this.categoria = categoria;
        this.util = new EnumI18nUtil(codigo + (100 * categoria.getCodigo()), new String[] { "pt_BR" }, descricoes);
    }

    public CategoriaPreferenciasViagem getCategoria() {
        return this.categoria;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public String getNome() {
        return this.nome;
    }

}