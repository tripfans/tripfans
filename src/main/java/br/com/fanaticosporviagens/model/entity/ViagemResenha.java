package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;

public class ViagemResenha {

    private final String descricao;

    private final String icone;

    private Integer quantidade = 0;

    private BigDecimal valor = new BigDecimal(0);

    public ViagemResenha() {
        super();
        this.icone = Viagem.ICONE_AVULSO;
        this.descricao = "Item pessoal";
    }

    public ViagemResenha(final LocalType localType) {
        super();
        this.icone = localType.getIconeMapa();
        this.descricao = localType.getDescricao();
    }

    public ViagemResenha(final TipoTransporte transporte) {
        super();
        this.icone = transporte.getIcone();
        this.descricao = transporte.getDescricao();
    }

    public void addItem(final Atividade item) {
        this.quantidade++;
        if (item.getValor() != null) {
            this.valor = this.valor.add(item.getValor());
        }
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getIcone() {
        return this.icone;
    }

    public Integer getQuantidade() {
        return this.quantidade;
    }

    public BigDecimal getValor() {
        return this.valor;
    }
}
