package br.com.fanaticosporviagens.model.entity;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;

@Entity
@Table(name = "interesses_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_interesses_viagem", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_interesses_viagem", nullable = false, unique = true)) })
public class InteressesViagem extends EntidadeGeradoraAcao<Long> implements br.com.fanaticosporviagens.acaousuario.Acao {

    private static final long serialVersionUID = 2358048596163281946L;

    @Column(name = "artes_cultura")
    private Boolean artesCultura = false;

    @Column(name = "aventura")
    private Boolean aventura = false;

    @Column(name = "caca_pesca")
    private Boolean cacaPesca = false;

    @Column(name = "carnaval")
    private Boolean carnaval = false;

    @Column(name = "compras")
    private Boolean compras = false;

    @Column(name = "cruzeiros")
    private Boolean cruzeiros = false;

    @Column(name = "descanso_relaxamento")
    private Boolean descansoRelaxamento = false;

    @Column(name = "ecoturismo")
    private Boolean ecoturismo = false;

    @Column(name = "escolar")
    private Boolean escolar = false;

    @Column(name = "esportes")
    private Boolean esportes = false;

    @Column(name = "estrada")
    private Boolean estrada = false;

    @Column(name = "eventos")
    private Boolean eventos = false;

    @Column(name = "eventos_esportivos")
    private Boolean eventosEsportivos = false;

    @Column(name = "festas_tematicas")
    private Boolean festasTematicas = false;

    @Column(name = "gastronomia")
    private Boolean gastronomia = false;

    @Column(name = "historia")
    private Boolean historia = false;

    @Column(name = "jogos_apostas")
    private Boolean jogosApostas = false;

    @Column(name = "mergulho")
    private Boolean mergulho = false;

    @Column(name = "micaretas")
    private Boolean micaretas = false;

    @Column(name = "negocios")
    private Boolean negocios = false;

    @Column(name = "neve")
    private Boolean neve = false;

    @Column(name = "parques_tematicos")
    private Boolean parquesTematicos = false;

    @Column(name = "praia")
    private Boolean praia = false;

    @Column(name = "religiao")
    private Boolean religiao = false;

    @Column(name = "reveillon")
    private Boolean reveillon = false;

    @Column(name = "romantismo")
    private Boolean romantismo = false;

    @Column(name = "safari")
    private Boolean safari = false;

    @Column(name = "shows")
    private Boolean shows = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) getUsuario();
    }

    public Boolean getArtesCultura() {
        return this.artesCultura;
    }

    @Override
    public Usuario getAutor() {
        return getUsuario();
    }

    public Boolean getAventura() {
        return this.aventura;
    }

    public Boolean getCacaPesca() {
        return this.cacaPesca;
    }

    public Boolean getCarnaval() {
        return this.carnaval;
    }

    public Boolean getCompras() {
        return this.compras;
    }

    public Boolean getCruzeiros() {
        return this.cruzeiros;
    }

    public Boolean getDescansoRelaxamento() {
        return this.descansoRelaxamento;
    }

    public Boolean getEcoturismo() {
        return this.ecoturismo;
    }

    public Boolean getEscolar() {
        return this.escolar;
    }

    public Boolean getEsportes() {
        return this.esportes;
    }

    public Boolean getEstrada() {
        return this.estrada;
    }

    public Boolean getEventos() {
        return this.eventos;
    }

    public Boolean getEventosEsportivos() {
        return this.eventosEsportivos;
    }

    public Boolean getFestasTematicas() {
        return this.festasTematicas;
    }

    public Boolean getGastronomia() {
        return this.gastronomia;
    }

    public Boolean getHistoria() {
        return this.historia;
    }

    public Boolean getJogosApostas() {
        return this.jogosApostas;
    }

    public List<TipoInteressesViagem> getListaInteressesViagem() {
        final List<TipoInteressesViagem> lista = new ArrayList<TipoInteressesViagem>();

        for (final Field field : this.getClass().getDeclaredFields()) {
            try {
                // Verifica se o valor do atributo é TRUE
                if (Boolean.TRUE.equals(field.get(this))) {
                    // Adiciona na lista
                    lista.add(TipoInteressesViagem.getTipoPorNome(field.getName()));
                }
            } catch (final IllegalArgumentException e) {
                e.printStackTrace();
            } catch (final IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return lista;
    }

    public Boolean getMergulho() {
        return this.mergulho;
    }

    public Boolean getMicaretas() {
        return this.micaretas;
    }

    public Boolean getNegocios() {
        return this.negocios;
    }

    public Boolean getNeve() {
        return this.neve;
    }

    public Boolean getParquesTematicos() {
        return this.parquesTematicos;
    }

    public Boolean getPraia() {
        return this.praia;
    }

    public Boolean getReligiao() {
        return this.religiao;
    }

    public Boolean getReveillon() {
        return this.reveillon;
    }

    public Boolean getRomantismo() {
        return this.romantismo;
    }

    public Boolean getSafari() {
        return this.safari;
    }

    public Boolean getShows() {
        return this.shows;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setArtesCultura(final Boolean artesCultura) {
        this.artesCultura = artesCultura;
    }

    public void setAventura(final Boolean aventura) {
        this.aventura = aventura;
    }

    public void setCacaPesca(final Boolean cacaPesca) {
        this.cacaPesca = cacaPesca;
    }

    public void setCarnaval(final Boolean carnaval) {
        this.carnaval = carnaval;
    }

    public void setCompras(final Boolean compras) {
        this.compras = compras;
    }

    public void setCruzeiros(final Boolean cruzeiros) {
        this.cruzeiros = cruzeiros;
    }

    public void setDescansoRelaxamento(final Boolean descansoRelaxamento) {
        this.descansoRelaxamento = descansoRelaxamento;
    }

    public void setEcoturismo(final Boolean ecoturismo) {
        this.ecoturismo = ecoturismo;
    }

    public void setEscolar(final Boolean escolar) {
        this.escolar = escolar;
    }

    public void setEsportes(final Boolean esportes) {
        this.esportes = esportes;
    }

    public void setEstrada(final Boolean estrada) {
        this.estrada = estrada;
    }

    public void setEventos(final Boolean eventos) {
        this.eventos = eventos;
    }

    public void setEventosEsportivos(final Boolean eventosEsportivos) {
        this.eventosEsportivos = eventosEsportivos;
    }

    public void setFestasTematicas(final Boolean festasTematicas) {
        this.festasTematicas = festasTematicas;
    }

    public void setGastronomia(final Boolean gastronomia) {
        this.gastronomia = gastronomia;
    }

    public void setHistoria(final Boolean historia) {
        this.historia = historia;
    }

    public void setJogosApostas(final Boolean jogosApostas) {
        this.jogosApostas = jogosApostas;
    }

    public void setMergulho(final Boolean mergulho) {
        this.mergulho = mergulho;
    }

    public void setMicaretas(final Boolean micaretas) {
        this.micaretas = micaretas;
    }

    public void setNegocios(final Boolean negocios) {
        this.negocios = negocios;
    }

    public void setNeve(final Boolean neve) {
        this.neve = neve;
    }

    public void setParquesTematicos(final Boolean parquesTematicos) {
        this.parquesTematicos = parquesTematicos;
    }

    public void setPraia(final Boolean praia) {
        this.praia = praia;
    }

    public void setReligiao(final Boolean religiao) {
        this.religiao = religiao;
    }

    public void setReveillon(final Boolean reveillon) {
        this.reveillon = reveillon;
    }

    public void setRomantismo(final Boolean romantismo) {
        this.romantismo = romantismo;
    }

    public void setSafari(final Boolean safari) {
        this.safari = safari;
    }

    public void setShows(final Boolean shows) {
        this.shows = shows;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }
}
