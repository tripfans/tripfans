package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class DetalheReservaAtividade {

    @Column(name = "codigo_referencia_reserva", nullable = true)
    private String codigoReferencia;

    @Column(name = "data_reserva")
    private Date data;

    @Column(name = "email_contato", nullable = true)
    private String emailContato;

    @Column(name = "nome_agencia", nullable = true)
    private String nomeAgencia;

    @Column(name = "nome_contato", nullable = true)
    private String nomeContato;

    @Column(name = "restricoes", nullable = true)
    private String restricoes;

    @Column(name = "site_agencia")
    private String siteAgencia;

    @Column(name = "telefone_agencia")
    private String telefone_agencia;

    @Column(name = "telefone_contato", nullable = true)
    private String telefone_contato;

    @Column(name = "valor_pago", nullable = true)
    private BigDecimal valorPago;

    public String getCodigoReferencia() {
        return this.codigoReferencia;
    }

    public Date getData() {
        return this.data;
    }

    public String getEmailContato() {
        return this.emailContato;
    }

    public String getNomeAgencia() {
        return this.nomeAgencia;
    }

    public String getNomeContato() {
        return this.nomeContato;
    }

    public String getRestricoes() {
        return this.restricoes;
    }

    public String getSiteAgencia() {
        return this.siteAgencia;
    }

    public String getTelefone_agencia() {
        return this.telefone_agencia;
    }

    public String getTelefone_contato() {
        return this.telefone_contato;
    }

    public BigDecimal getValorPago() {
        return this.valorPago;
    }

    public void setCodigoReferencia(final String codigoReferencia) {
        this.codigoReferencia = codigoReferencia;
    }

    public void setData(final Date data) {
        this.data = data;
    }

    public void setEmailContato(final String emailContato) {
        this.emailContato = emailContato;
    }

    public void setNomeAgencia(final String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public void setNomeContato(final String nomeContato) {
        this.nomeContato = nomeContato;
    }

    public void setRestricoes(final String restricoes) {
        this.restricoes = restricoes;
    }

    public void setSiteAgencia(final String siteAgencia) {
        this.siteAgencia = siteAgencia;
    }

    public void setTelefone_agencia(final String telefone_agencia) {
        this.telefone_agencia = telefone_agencia;
    }

    public void setTelefone_contato(final String telefone_contato) {
        this.telefone_contato = telefone_contato;
    }

    public void setValorPago(final BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

}