package br.com.fanaticosporviagens.model.entity;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * Bean que representa o arquivo de foto em um upload.
 * 
 * @author André Thiago
 * 
 */
public class FotoUploadItem {

    private String descricaoFoto;

    private CommonsMultipartFile foto;

    public String getDescricaoFoto() {
        return this.descricaoFoto;
    }

    public CommonsMultipartFile getFoto() {
        return this.foto;
    }

    public void setDescricaoFoto(final String descricaoFoto) {
        this.descricaoFoto = descricaoFoto;
    }

    public void setFoto(final CommonsMultipartFile foto) {
        this.foto = foto;
    }

    public boolean temFoto() {
        return !(this.foto == null || this.foto.isEmpty());
    }

}
