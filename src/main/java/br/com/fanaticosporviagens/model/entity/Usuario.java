package br.com.fanaticosporviagens.model.entity;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.Years;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.social.connect.UserProfile;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@DynamicUpdate
@Table(name = "usuario")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_usuario", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_usuario", nullable = false, unique = true)) })
@TypeDefs(value = {
        @TypeDef(name = "FormaCadastroUsuario", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.FormaCadastroUsuario") }),
        @TypeDef(name = "TipoExibicaoDataNascimento", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoExibicaoDataNascimento") }) })
public class Usuario extends EntidadeGeradoraAcao<Long> implements UserDetails, UrlNameable, Indexavel, AlvoAcao, Comentavel {

    private static final long serialVersionUID = 2604245589399658346L;

    public static Usuario criarDeUmaContaExterna(final UserProfile providerUser) {
        final Usuario usuario = new Usuario();
        usuario.setPrimeiroNome(providerUser.getFirstName());
        usuario.setUltimoNome(providerUser.getLastName());
        usuario.setUsername(providerUser.getUsername());
        return usuario;
    }

    @Transient
    private final Set<Long> amigos = new HashSet<Long>();

    @Column(name = "ativo")
    private Boolean ativo;

    @Column(name = "bloqueado", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean bloqueado = false;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cidade_natal")
    private Cidade cidadeNatal;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_cidade_residencia")
    private Cidade cidadeResidencia;

    @Column(name = "codigo_recuperacao_senha")
    private String codigoRecuperacaoSenha;

    @Column(name = "conectado_facebook", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean conectadoFacebook = false;

    @Column(name = "conectado_foursquare", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean conectadoFoursquare = false;

    @Column(name = "conectado_google", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean conectadoGoogle = false;

    @Column(name = "conectado_twitter", columnDefinition = "BOOLEAN DEFAULT false")
    private Boolean conectadoTwitter = false;

    @Embedded
    private ConfiguracoesVisibilidade configuracoesVisibilidade = new ConfiguracoesVisibilidade();

    @Embedded
    private final ContadoresUsuario contadoresUsuario = new ContadoresUsuario();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "usuario", orphanRemoval = true)
    private Set<ContaServicoExterno> contasEmServicosExternos = new HashSet<ContaServicoExterno>();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro")
    private Date dataCadastro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_complemento_cadastro")
    private Date dataComplementoCadastro;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_confirmacao")
    private Date dataConfirmacao;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_nascimento")
    private Date dataNascimento;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_residencia")
    private Date dataResidencia;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_ultima_acao")
    private Date dataUltimaAcao = new Date();

    /**
     * Guarda quando foi feita a ultima alteracao nos dados básicos do perfil - para motivos de log de ações do usuário
     */
    @Column(name = "data_ultima_alteracao_dados_perfil")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataUltimaAlteracaoDadosPerfil;

    /**
     * Guarda quando foi feita o ultimo 'download' de checkins de locais
     */
    @Column(name = "data_ultima_verificacao_checkins")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataUltimaVerificacaoCheckins;

    /**
     * Guarda a ultima vez que o usuario viu suas notificações
     */
    @Column(name = "data_ultima_verificacao_notificacoes")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataUltimaVerificacaoNotificacoes = new LocalDateTime();

    @Column(name = "data_ultimo_login")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataUltimoLogin;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "email")
    private String email;

    @Column(name = "exibir_genero")
    private Boolean exibirGenero = true;

    @Column(name = "forma_cadastro")
    @Type(type = "FormaCadastroUsuario")
    private FormaCadastroUsuario formaCadastro;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_foto")
    private Foto foto;

    @Column(name = "foto_externa")
    private Boolean fotoExterna = false;

    @Column(name = "frase_perfil")
    private String frasePerfil;

    @Column(name = "genero")
    private String genero;

    @Column(name = "id_facebook")
    private String idFacebook;

    @Column(name = "id_foursquare")
    private String idFoursquare;

    @Column(name = "id_google")
    private String idGoogle;

    @Column(name = "id_twitter")
    private Long idTwitter;

    @Column(name = "id_usuario_convidante")
    private Long idUsuarioConvidante;

    @Column(name = "importado_facebook")
    private Boolean importadoFacebook = false;

    @Column(name = "importado_google")
    private Boolean importadoGoogle = false;

    @Column(name = "informacoes_adicionais")
    private String informacoesAdicionais;

    @Column(name = "locale")
    private String locale;

    @Column(name = "nome_meio")
    private String nomeMeio;

    @Column(name = "password")
    private String password;

    @Column(name = "primeiro_nome")
    private String primeiroNome;

    @Column(name = "tipo_exibicao_data_nascimento")
    @Type(type = "TipoExibicaoDataNascimento")
    private TipoExibicaoDataNascimento tipoExibicaoDataNascimento;

    @Column(name = "ultimo_nome")
    private String ultimoNome;

    @Column(name = "url_foto_album")
    private String urlFotoAlbum;

    @Column(name = "url_foto_big")
    private String urlFotoBig;

    @Column(name = "url_foto_small")
    private String urlFotoSmall;

    @Column(name = "url_path", nullable = false, unique = true)
    private String urlPath;

    @Column(name = "username", unique = true)
    private String username;

    public boolean addContaServicoExterno(final ContaServicoExterno contaServicoExterno) {
        contaServicoExterno.setUsuario(this);
        return this.contasEmServicosExternos.add(contaServicoExterno);
    }

    public boolean estaConectadoFacebook() {
        return Boolean.TRUE.equals(getConectadoFacebook());
    }

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return null;
    }

    public Boolean getAtivo() {
        return this.ativo;
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        final Collection<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        grantedAuthorities.add(new GrantedAuthorityImpl("ROLE_ADMIN"));
        return grantedAuthorities;
    }

    @Override
    public Usuario getAutor() {
        return this;
    }

    public Boolean getBloqueado() {
        return this.bloqueado;
    }

    public Cidade getCidadeNatal() {
        return this.cidadeNatal;
    }

    public Cidade getCidadeResidencia() {
        return this.cidadeResidencia;
    }

    public String getCodigoRecuperacaoSenha() {
        return this.codigoRecuperacaoSenha;
    }

    public Boolean getConectadoFacebook() {
        return this.conectadoFacebook;
    }

    public Boolean getConectadoFoursquare() {
        return this.conectadoFoursquare;
    }

    public Boolean getConectadoGoogle() {
        return this.conectadoGoogle;
    }

    public Boolean getConectadoTwitter() {
        return this.conectadoTwitter;
    }

    public ConfiguracoesVisibilidade getConfiguracoesVisibilidade() {
        return this.configuracoesVisibilidade;
    }

    public ContadoresUsuario getContadoresUsuario() {
        return this.contadoresUsuario;
    }

    public Set<ContaServicoExterno> getContasEmServicosExternos() {
        return this.contasEmServicosExternos;
    }

    public Date getDataCadastro() {
        return this.dataCadastro;
    }

    public Date getDataComplementoCadastro() {
        return this.dataComplementoCadastro;
    }

    public Date getDataConfirmacao() {
        return this.dataConfirmacao;
    }

    public Date getDataNascimento() {
        return this.dataNascimento;
    }

    public Date getDataResidencia() {
        return this.dataResidencia;
    }

    public Date getDataUltimaAcao() {
        return this.dataUltimaAcao;
    }

    public LocalDateTime getDataUltimaAlteracaoDadosPerfil() {
        return this.dataUltimaAlteracaoDadosPerfil;
    }

    public LocalDate getDataUltimaVerificacaoCheckins() {
        return this.dataUltimaVerificacaoCheckins;
    }

    public LocalDateTime getDataUltimaVerificacaoNotificacoes() {
        return this.dataUltimaVerificacaoNotificacoes;
    }

    public LocalDate getDataUltimoLogin() {
        return this.dataUltimoLogin;
    }

    @Override
    public String getDescricaoAlvo() {
        return this.getDisplayName();
    }

    @Override
    public Usuario getDestinatario() {
        return this;
    }

    public String getDisplayName() {
        if (this.displayName == null) {
            return this.getUsername();
        }
        return this.displayName;
    }

    @Override
    public String getEdgyTextField() {
        return this.displayName;
    }

    public String getEmail() {
        return this.email;
    }

    public boolean getExibirDataNascimento() {
        return this.getTipoExibicaoDataNascimento() != null && getTipoExibicaoDataNascimento() != TipoExibicaoDataNascimento.OCULTAR;
    }

    public boolean getExibirDataNascimentoCompacta() {
        return this.getExibirDataNascimento() && getTipoExibicaoDataNascimento() != TipoExibicaoDataNascimento.APENAS_DIA_MES;
    }

    public boolean getExibirDataNascimentoCompleta() {
        return getExibirDataNascimento() && getTipoExibicaoDataNascimento() == TipoExibicaoDataNascimento.COMPLETA;
    }

    public Boolean getExibirGenero() {
        return this.exibirGenero != null ? this.exibirGenero : Boolean.TRUE;
    }

    public FormaCadastroUsuario getFormaCadastro() {
        return this.formaCadastro;
    }

    public Foto getFoto() {
        return this.foto;
    }

    public Boolean getFotoExterna() {
        return this.fotoExterna;
    }

    public String getFrasePerfil() {
        return this.frasePerfil;
    }

    public String getGenero() {
        return this.genero;
    }

    @Transient
    public Integer getIdade() {
        if (this.dataNascimento != null) {
            final LocalDate birthdate = new LocalDate(this.dataNascimento);
            final LocalDate now = new LocalDate();
            final Years age = Years.yearsBetween(birthdate, now);
            return age.getYears();
        }
        return null;
    }

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public String getIdFoursquare() {
        return this.idFoursquare;
    }

    public String getIdGoogle() {
        return this.idGoogle;
    }

    public Long getIdTwitter() {
        return this.idTwitter;
    }

    public Long getIdUsuarioConvidante() {
        return this.idUsuarioConvidante;
    }

    public Boolean getImportadoFacebook() {
        return this.importadoFacebook != null ? this.importadoFacebook : false;
    }

    public Boolean getImportadoGoogle() {
        return this.importadoGoogle != null ? this.importadoGoogle : false;
    }

    public String getInformacoesAdicionais() {
        return this.informacoesAdicionais;
    }

    public String getLocale() {
        return this.locale;
    }

    @Override
    public String getNome() {
        return this.displayName;
    }

    public String getNomeCompleto() {
        final StringBuffer nome = new StringBuffer(this.primeiroNome);
        if (this.nomeMeio != null) {
            nome.append(" " + this.nomeMeio);
        }
        if (this.ultimoNome != null) {
            nome.append(" " + this.ultimoNome);
        }
        return nome.toString();
    }

    @Override
    public String getNomeExibicao() {
        return this.displayName;
    }

    public String getNomeMeio() {
        return this.nomeMeio;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    public String getPasswordSalt() {
        return this.username + this.dataCadastro.getTime();
    }

    public boolean getPossuiInteressesEmCidades() {
        return this.contadoresUsuario.getPossuiInteressesEmCidades();
    }

    @Override
    public String getPrefix() {
        return "u";
    }

    public String getPrimeiroNome() {
        return this.primeiroNome;
    }

    public Long getQuantidadeAmigos() {
        return this.contadoresUsuario.getQuantidadeAmigos();
    }

    public Long getQuantidadeAtividades() {
        return this.contadoresUsuario.getQuantidadeAtividades();
    }

    public Long getQuantidadeAtracoesDesejaIr() {
        return this.contadoresUsuario.getQuantidadeAtracoesDesejaIr();
    }

    public Long getQuantidadeAtracoesFavorito() {
        return this.contadoresUsuario.getQuantidadeAtracoesFavorito();
    }

    public Long getQuantidadeAtracoesIndica() {
        return this.contadoresUsuario.getQuantidadeAtracoesIndica();
    }

    public Long getQuantidadeAtracoesJaFoi() {
        return this.contadoresUsuario.getQuantidadeAtracoesJaFoi();
    }

    public Long getQuantidadeAvaliacoes() {
        return this.contadoresUsuario.getQuantidadeAvaliacoes();
    }

    public Long getQuantidadeCidadesDesejaIr() {
        return this.contadoresUsuario.getQuantidadeCidadesDesejaIr();
    }

    public Long getQuantidadeCidadesFavorito() {
        return this.contadoresUsuario.getQuantidadeCidadesFavorito();
    }

    public Long getQuantidadeCidadesIndica() {
        return this.contadoresUsuario.getQuantidadeCidadesIndica();
    }

    public Long getQuantidadeCidadesJaFoi() {
        return this.contadoresUsuario.getQuantidadeCidadesJaFoi();
    }

    public Long getQuantidadeComentarios() {
        return this.contadoresUsuario.getQuantidadeComentarios();
    }

    public Long getQuantidadeDicas() {
        return this.contadoresUsuario.getQuantidadeDicas();
    }

    public Long getQuantidadeFotos() {
        return this.contadoresUsuario.getQuantidadeFotos();
    }

    public Long getQuantidadeHoteisDesejaIr() {
        return this.contadoresUsuario.getQuantidadeHoteisDesejaIr();
    }

    public Long getQuantidadeHoteisFavorito() {
        return this.contadoresUsuario.getQuantidadeHoteisFavorito();
    }

    public Long getQuantidadeHoteisIndica() {
        return this.contadoresUsuario.getQuantidadeHoteisIndica();
    }

    public Long getQuantidadeHoteisJaFoi() {
        return this.contadoresUsuario.getQuantidadeHoteisJaFoi();
    }

    public Long getQuantidadePaisesDesejaIr() {
        return this.contadoresUsuario.getQuantidadePaisesDesejaIr();
    }

    public Long getQuantidadePaisesFavorito() {
        return this.contadoresUsuario.getQuantidadePaisesFavorito();
    }

    public Long getQuantidadePaisesIndica() {
        return this.contadoresUsuario.getQuantidadePaisesIndica();
    }

    public Long getQuantidadePaisesJaFoi() {
        return this.contadoresUsuario.getQuantidadePaisesJaFoi();
    }

    public Long getQuantidadePerguntas() {
        return this.contadoresUsuario.getQuantidadePerguntas();
    }

    public Long getQuantidadeRespostas() {
        return this.contadoresUsuario.getQuantidadeRespostas();
    }

    public Long getQuantidadeRestaurantesDesejaIr() {
        return this.contadoresUsuario.getQuantidadeRestaurantesDesejaIr();
    }

    public Long getQuantidadeRestaurantesFavorito() {
        return this.contadoresUsuario.getQuantidadeRestaurantesFavorito();
    }

    public Long getQuantidadeRestaurantesIndica() {
        return this.contadoresUsuario.getQuantidadeRestaurantesIndica();
    }

    public Long getQuantidadeRestaurantesJaFoi() {
        return this.contadoresUsuario.getQuantidadeRestaurantesJaFoi();
    }

    public Long getQuantidadeViagens() {
        return this.contadoresUsuario.getQuantidadeViagens();
    }

    @Override
    public Integer getTipoEntidade() {
        return CategoriasPesquisa.PESSOAS.getCodigo();
    }

    public TipoExibicaoDataNascimento getTipoExibicaoDataNascimento() {
        return this.tipoExibicaoDataNascimento;
    }

    @Override
    public String getTokenField() {
        return this.displayName;
    }

    public String getUltimoNome() {
        return this.ultimoNome;
    }

    @Override
    public String getUrlFotoAlbum() {
        if (this.urlFotoAlbum != null) {
            return this.urlFotoAlbum;
        } else if (this.foto == null || Boolean.TRUE.equals(this.foto.getAnonima())) {
            return "/resources/images/perfil/blank_" + (isFeminino() ? "fe" : "") + "male_album.jpg";
        } else {
            return this.foto.getUrlAlbum();
        }
    }

    public String getUrlFotoAvatar() {
        return getUrlFotoSmall();
    }

    @Override
    public String getUrlFotoBig() {
        if (this.urlFotoBig != null) {
            return this.urlFotoBig;
        } else if (this.foto == null || Boolean.TRUE.equals(this.foto.getAnonima())) {
            return "/resources/images/perfil/blank_" + (isFeminino() ? "fe" : "") + "male_big.jpg";
        } else {
            return this.foto.getUrlBig();
        }
    }

    public String getUrlFotoCrop() {
        if (this.foto == null || Boolean.TRUE.equals(this.foto.getAnonima())) {
            return "/resources/images/perfil/blank_" + (isFeminino() ? "fe" : "") + "male_crop.jpg";
        } else {
            return this.foto.getUrlCrop();
        }
    }

    public String getUrlFotoPerfil() {
        return getUrlFotoAlbum();
    }

    public String getUrlFotoPerfilOriginal() {
        if (this.foto == null || Boolean.TRUE.equals(this.foto.getAnonima())) {
            return "/resources/images/perfil/blank_" + (isFeminino() ? "fe" : "") + "male_album.jpg";
        } else {
            return this.getFoto().getUrlOriginal();
        }
    }

    @Override
    public String getUrlFotoSmall() {
        if (this.urlFotoSmall != null) {
            return this.urlFotoSmall;
        } else if (this.foto == null || Boolean.TRUE.equals(this.foto.getAnonima())) {
            return "/resources/images/perfil/blank_" + (isFeminino() ? "fe" : "") + "male_small.jpg";
        } else {
            return this.foto.getUrlSmall();
        }
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return Boolean.FALSE.equals(this.getBloqueado());
    }

    public boolean isCadastroCompleto() {
        return this.getDataComplementoCadastro() != null;
    }

    public boolean isConfirmado() {
        return this.getDataConfirmacao() != null;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return Boolean.TRUE.equals(this.getAtivo());
    }

    public boolean isFeminino() {
        return this.genero != null && this.genero.equals("F");
    }

    public boolean isImportado() {
        return this.getImportadoFacebook() || this.getImportadoGoogle();
    }

    public boolean isMasculino() {
        return this.genero != null && this.genero.equals("M");
    }

    public boolean isPerfilPublico() {
        return this.configuracoesVisibilidade != null ? this.configuracoesVisibilidade.getMostrarInformacoesPerfil().equals(TipoVisibilidade.PUBLICO)
                : false;
    }

    public boolean jaCompletouCadastro() {
        return this.dataComplementoCadastro != null;
    }

    public boolean naoCompletouCadastro() {
        return !jaCompletouCadastro();
    }

    public boolean removeContaServicoExterno(final ContaServicoExterno contaServicoExterno) {
        contaServicoExterno.setUsuario(null);
        return this.contasEmServicosExternos.remove(contaServicoExterno);
    }

    public void setAtivo(final Boolean ativo) {
        this.ativo = ativo;
    }

    public void setBloqueado(final Boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public void setCidadeNatal(final Cidade cidadeNatal) {
        this.cidadeNatal = cidadeNatal;
    }

    public void setCidadeResidencia(final Cidade cidadeResidencia) {
        this.cidadeResidencia = cidadeResidencia;
    }

    public void setCodigoRecuperacaoSenha(final String codigoRecuperacaoSenha) {
        this.codigoRecuperacaoSenha = codigoRecuperacaoSenha;
    }

    public void setConectadoFacebook(final Boolean conectadoFacebook) {
        this.conectadoFacebook = conectadoFacebook;
    }

    public void setConectadoFoursquare(final Boolean conectadoFoursquare) {
        this.conectadoFoursquare = conectadoFoursquare;
    }

    public void setConectadoGoogle(final Boolean conectadoGoogle) {
        this.conectadoGoogle = conectadoGoogle;
    }

    public void setConectadoTwitter(final Boolean conectadoTwitter) {
        this.conectadoTwitter = conectadoTwitter;
    }

    public void setConfiguracoesVisibilidade(final ConfiguracoesVisibilidade configuracoesVisibilidade) {
        this.configuracoesVisibilidade = configuracoesVisibilidade;
    }

    public void setContasEmServicosExternos(final Set<ContaServicoExterno> contasEmServicosExternos) {
        this.contasEmServicosExternos = contasEmServicosExternos;
    }

    public void setDataCadastro(final Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void setDataComplementoCadastro(final Date dataComplementoCadastro) {
        this.dataComplementoCadastro = dataComplementoCadastro;
    }

    public void setDataConfirmacao(final Date dataConfirmacao) {
        this.dataConfirmacao = dataConfirmacao;
    }

    public void setDataNascimento(final Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public void setDataResidencia(final Date dataResidencia) {
        this.dataResidencia = dataResidencia;
    }

    public void setDataUltimaAcao(final Date dataUltimaAcao) {
        this.dataUltimaAcao = dataUltimaAcao;
    }

    public void setDataUltimaAlteracaoDadosPerfil(final LocalDateTime dataUltimaAlteracaoDadosPerfil) {
        this.dataUltimaAlteracaoDadosPerfil = dataUltimaAlteracaoDadosPerfil;
    }

    public void setDataUltimaVerificacaoCheckins(final LocalDate dataUltimaVerificacaoCheckins) {
        this.dataUltimaVerificacaoCheckins = dataUltimaVerificacaoCheckins;
    }

    public void setDataUltimaVerificacaoNotificacoes(final LocalDateTime dataUltimaVerificacaoNotificacoes) {
        this.dataUltimaVerificacaoNotificacoes = dataUltimaVerificacaoNotificacoes;
    }

    public void setDataUltimoLogin(final LocalDate dataUltimoLogin) {
        this.dataUltimoLogin = dataUltimoLogin;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setExibirGenero(final Boolean exibirGenero) {
        this.exibirGenero = exibirGenero;
    }

    public void setFormaCadastro(final FormaCadastroUsuario formaCadastro) {
        this.formaCadastro = formaCadastro;
    }

    public void setFoto(final Foto foto) {
        this.foto = foto;
    }

    public void setFotoExterna(final Boolean fotoExterna) {
        this.fotoExterna = fotoExterna;
    }

    public void setFrasePerfil(final String frasePerfil) {
        this.frasePerfil = frasePerfil;
    }

    public void setGenero(final String genero) {
        this.genero = genero;
        if (this.foto != null && this.foto.getAnonima()) {
            this.foto.definirFotoPadraoAnonima();
        }
    }

    @Override
    public void setId(final Long id) {
        super.setId(id);
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setIdFoursquare(final String idFoursquare) {
        this.idFoursquare = idFoursquare;
    }

    public void setIdGoogle(final String idGoogle) {
        this.idGoogle = idGoogle;
    }

    public void setIdTwitter(final Long idTwitter) {
        this.idTwitter = idTwitter;
    }

    public void setIdUsuarioConvidante(final Long idUsuarioConvidante) {
        this.idUsuarioConvidante = idUsuarioConvidante;
    }

    public void setImportadoFacebook(final Boolean importadoFacebook) {
        this.importadoFacebook = importadoFacebook;
    }

    public void setImportadoGoogle(final Boolean importadoGoogle) {
        this.importadoGoogle = importadoGoogle;
    }

    public void setInformacoesAdicionais(final String informacoesAdicionais) {
        this.informacoesAdicionais = informacoesAdicionais;
    }

    public void setLocale(final String locale) {
        this.locale = locale;
    }

    public void setNomeMeio(final String nomeMeio) {
        this.nomeMeio = nomeMeio;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setPrimeiroNome(final String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public void setTipoExibicaoDataNascimento(final TipoExibicaoDataNascimento tipoExibicaoDataNascimento) {
        this.tipoExibicaoDataNascimento = tipoExibicaoDataNascimento;
    }

    public void setUltimoNome(final String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

}