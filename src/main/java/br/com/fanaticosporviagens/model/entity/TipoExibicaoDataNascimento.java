package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * 
 * @author @author Carlos Nascimento
 * 
 */
public enum TipoExibicaoDataNascimento implements EnumTypeInteger {

    APENAS_DIA_MES(2, "Exibir apenas o dia e o mês no meu perfil"),
    COMPLETA(1, "Exibir a data completa no meu perfil"),
    OCULTAR(3, "Não exibir no meu perfil");

    private final EnumI18nUtil util;

    TipoExibicaoDataNascimento(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}
