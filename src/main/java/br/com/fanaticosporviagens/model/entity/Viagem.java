package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.Where;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@Table(name = "viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_viagem", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_viagem", nullable = false, unique = true)) })
@TypeDefs(value = { @TypeDef(name = "Visibilidade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoVisibilidade") }) })
public class Viagem extends EntidadeGeradoraAcao<Long> implements Comentavel {

    public static final String ICONE_AVULSO = "plan.png";

    private static final long serialVersionUID = 8775940238047936836L;

    @OneToMany(mappedBy = "viagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = false)
    @OrderBy("ordem")
    @IndexColumn(base = 0, name = "ORDEM")
    private List<Atividade> atividades;

    @Column(name = "controlar_gastos", nullable = true)
    private Boolean controlarGastos = false;

    @Column(name = "controlar_tempo", nullable = true)
    private Boolean controlarTempo = false;

    @Column(name = "data_inicio", nullable = true)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataInicio;

    @Column(name = "descricao_viagem", nullable = true)
    private String descricao;

    @OneToMany(mappedBy = "viagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("ordem")
    @IndexColumn(base = 1, name = "ORDEM")
    private List<DestinoViagem> destinosViagem = new ArrayList<DestinoViagem>();

    @OneToMany(mappedBy = "viagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("numero")
    private List<DiaViagem> dias;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_origem", nullable = true)
    private Local origem;

    @OneToMany(mappedBy = "viagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "viaja = 'false'")
    private List<ParticipanteViagem> participantesCompartilhamento;

    @OneToMany(mappedBy = "viagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Where(clause = "confirmado = 'true' and viaja = 'true'")
    private List<ParticipanteViagem> participantesViajantes;

    @Column(name = "titulo_viagem", nullable = false)
    private String titulo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_criador", nullable = false)
    private Usuario usuarioCriador;

    @Type(type = "Visibilidade")
    @Column(name = "codigo_visibilidade", nullable = false)
    private TipoVisibilidade visibilidade;

    public Viagem() {
        super();
    }

    public Viagem(final String titulo, final String descricao, final Integer quantidadeDias, final Date dataInicio, final Usuario usuarioCriador,
            final TipoVisibilidade visibilidade) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.dataInicio = new LocalDate(dataInicio);
        this.usuarioCriador = usuarioCriador;
        this.visibilidade = visibilidade;

        this.dias = new ArrayList<DiaViagem>();
        if (quantidadeDias != null) {
            for (int i = 0; i < quantidadeDias; i++) {
                this.dias.add(new DiaViagem(this, i + 1));
            }
        }
    }

    public void addDestino(final DestinoViagem destinoViagem) {
        if (this.destinosViagem == null) {
            this.destinosViagem = new ArrayList<DestinoViagem>();
        }
        for (final DestinoViagem destino : this.destinosViagem) {
            if (destino.getDestino().getId().equals(destinoViagem.getDestino().getId())) {
                return;
            }
        }
        destinoViagem.setViagem(this);
        this.destinosViagem.add(destinoViagem);
    }

    public void addDestino(final LocalGeografico destino) {
        final DestinoViagem destinoViagem = new DestinoViagem();
        destinoViagem.setDestino(destino);
        this.addDestino(destinoViagem);
    }

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return null;
    }

    public List<Atividade> getAtividades() {
        return this.atividades;
    }

    public List<Atividade> getAtividadesAlimentacaoOrdenadas() {
        return this.getAtividadesPorTipoOrdenadas(TipoAtividade.ALIMENTACAO);
    }

    public List<Atividade> getAtividadesAOrganizarPorTipo(final TipoAtividade tipoAtividade) {
        final ArrayList<Atividade> atividadesPorTipo = new ArrayList<Atividade>();
        if ((this.atividades != null) && !this.atividades.isEmpty()) {
            for (final Atividade atividade : this.atividades) {
                if (atividade.getTipo().equals(tipoAtividade)) {
                    atividadesPorTipo.add(atividade);
                }
            }
        }
        return atividadesPorTipo;
    }

    public List<Atividade> getAtividadesAtracoesOrdenadas() {
        return this.getAtividadesPorTipoOrdenadas(TipoAtividade.VISITA_ATRACAO);
    }

    public List<Atividade> getAtividadesHospedagemOrdenadas() {
        return this.getAtividadesPorTipoOrdenadas(TipoAtividade.HOSPEDAGEM);
    }

    public List<Atividade> getAtividadesPersonalizadasOrdenadas() {
        return this.getAtividadesPorTipoOrdenadas(TipoAtividade.PERSONALIZADO);
    }

    @Deprecated
    public List<Atividade> getAtividadesPorTipoOrdenadas(final TipoAtividade tipoAtividade) {
        final ArrayList<Atividade> atividadesPorTipoOrdenadas = new ArrayList<Atividade>();

        if ((this.atividades != null) && !this.atividades.isEmpty()) {
            Atividade atividade = this.atividades.get(0).getItemPrimeiro();
            if (atividade.getTipo().equals(tipoAtividade)) {
                atividadesPorTipoOrdenadas.add(atividade);
            } else {
                while (atividade.getItemProximo() != null) {
                    atividade = atividade.getItemProximo();
                    if (atividade.getTipo().equals(tipoAtividade)) {
                        atividadesPorTipoOrdenadas.add(atividade);
                    }
                }
            }
        }

        if (this.dias != null) {
            for (final DiaViagem dia : this.dias) {
                if ((dia.getAtividadesQueComecam() != null) && !dia.getAtividadesQueComecam().isEmpty()) {
                    Atividade atividade = dia.getAtividadesQueComecam().get(0).getItemPrimeiro();
                    if (atividade.getTipo().equals(tipoAtividade)) {
                        atividadesPorTipoOrdenadas.add(atividade);
                    } else {
                        while (atividade.getItemProximo() != null) {
                            atividade = atividade.getItemProximo();
                            if (atividade.getTipo().equals(tipoAtividade)) {
                                atividadesPorTipoOrdenadas.add(atividade);
                            }
                        }
                    }
                }
            }
        }

        return atividadesPorTipoOrdenadas;
    }

    public List<Atividade> getAtividadesTransporteOrdenadas() {
        return this.getAtividadesPorTipoOrdenadas(TipoAtividade.TRANSPORTE);
    }

    @Override
    public Usuario getAutor() {
        return getUsuarioCriador();
    }

    public ViagemCalendario getCalendario() {
        return new ViagemCalendario(this);
    }

    /*public List<ViagemItem> getAtividadesPorTipo(final TipoAtividade tipoAtividade) {
        final List<ViagemItem> atividadesOrdenadas = getItensOrdenados();
        final List<ViagemItem> atividadesPorTipo = new ArrayList<ViagemItem>();

        if ((atividadesOrdenadas != null) && !atividadesOrdenadas.isEmpty()) {
            for (final ViagemItem atividade : atividadesOrdenadas) {
                if (atividade.getTipo().equals(tipoAtividade)) {
                    atividadesPorTipo.add(atividade);
                }
            }
        }

        return atividadesPorTipo;
    }*/

    public List<LocalGeografico> getCidades() {
        final List<LocalGeografico> listaCidades = new ArrayList<LocalGeografico>();
        if (getDestinosViagem() != null) {
            for (final DestinoViagem destinoViagem : getDestinosViagem()) {
                listaCidades.add(destinoViagem.getDestino());
            }
        }
        return listaCidades;
    }

    public Boolean getControlarGastos() {
        return this.controlarGastos;
    }

    public Boolean getControlarTempo() {
        return this.controlarTempo;
    }

    public Viagem getCopia(final Usuario usuarioAutenticado) {
        final Viagem copia = new Viagem();

        copia.setDataInicio(getDataInicio());
        copia.setDescricao(getDescricao());
        copia.setTitulo(getTitulo() + " - Cópia");

        copia.setUsuarioCriador(usuarioAutenticado);
        copia.setVisibilidade(TipoVisibilidade.SOMENTE_EU);

        if (this.atividades != null && !this.atividades.isEmpty()) {
            copia.setAtividades(new ArrayList<Atividade>());
            Atividade atividadeCopiaAnterior = null;
            for (final Atividade atividade : this.getItensOrdenados()) {
                final Atividade atividadeCopia = atividade.getCopia(copia);

                copia.getItens().add(atividadeCopia);

                if (atividadeCopiaAnterior != null) {
                    atividadeCopia.setItemAnterior(atividadeCopiaAnterior);
                    atividadeCopiaAnterior.setItemProximo(atividadeCopia);
                }

                atividadeCopiaAnterior = atividadeCopia;
            }
        }

        if (this.dias != null && !this.dias.isEmpty()) {
            copia.setDias(new ArrayList<DiaViagem>());
            for (final DiaViagem dia : this.dias) {
                copia.getDias().add(dia.getCopia(copia));
            }
        }

        for (final DestinoViagem original : getDestinosViagem()) {
            final DestinoViagem destinoCopia = new DestinoViagem();
            try {
                PropertyUtils.copyProperties(destinoCopia, original);
            } catch (final Exception e) {
                throw new SystemException("Erro ao copiar viagem.");
            }
            destinoCopia.setId(null);
            copia.addDestino(destinoCopia);
        }

        return copia;
    }

    public LocalDate getDataFim() {
        if (this.getDataInicio() != null && this.getQuantidadeDias() != null) {
            return this.dataInicio.plusDays(this.getQuantidadeDias() - 1);
        }
        return null;
    }

    public Date getDataInclusao() {
        if (this.getDataCriacao() != null) {
            return getDataCriacao().toDate();
        }
        return null;
    }

    public LocalDate getDataInicio() {
        return this.dataInicio;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public String getDescricaoAlvo() {
        return this.getTitulo();
    }

    public List<DestinoViagem> getDestinosViagem() {
        return this.destinosViagem;
    }

    public DiaViagem getDia(final Integer numero) {
        for (final DiaViagem dia : getDias()) {
            if (dia.getNumero().equals(numero)) {
                return dia;
            }
        }
        return null;
    }

    public List<DiaViagem> getDias() {
        return this.dias;
    }

    /**
     * Recupera o principal hotel do dia
     *
     * @param numeroDia
     * @return
     */
    public Hotel getHotelDoDia(final Integer numeroDia) {
        final List<Atividade> hospedagens = this.getAtividadesHospedagemOrdenadas();

        final DiaViagem dia = this.getDia(numeroDia);

        for (int i = 0; i < hospedagens.size(); i++) {
            final Atividade hospedagem = hospedagens.get(i);
            final DiaViagem diaInicioHospedagem = hospedagem.getDia();
            final DiaViagem diaFimHospedagem = hospedagem.getDiaFim();
            if (hospedagem.isComecaNoDia(dia.getId()) || numeroDia >= diaInicioHospedagem.getNumero() && numeroDia < diaFimHospedagem.getNumero()) {
                return (Hotel) hospedagem.getLocal();
            }
        }
        return null;
    }

    public Atividade getItemPrimeiro() {
        if ((this.atividades != null) && !this.atividades.isEmpty()) {
            return this.atividades.get(0).getItemPrimeiro();
        }
        return null;
    }

    public Atividade getItemUltimo() {
        if ((this.atividades != null) && !this.atividades.isEmpty()) {
            return this.atividades.get(0).getItemUltimo();
        }
        return null;
    }

    public List<Atividade> getItens() {
        return this.atividades;
    }

    public List<Atividade> getItensOrdenados() {
        return this.atividades;
    }

    private Set<Local> getLocaisEnderecaveisVisitados() {
        final Set<Local> locais = new TreeSet<Local>(new Comparator<Local>() {

            @Override
            public int compare(final Local arg0, final Local arg1) {
                return arg0.getNome().compareTo(arg1.getNome());
            }
        });

        if (getItens() != null) {
            for (final Atividade atividade : getItens()) {
                if ((atividade.getLocal() != null) && atividade.getLocal().getTipoLocal().isTipoLocalEnderecavel()) {
                    locais.add(atividade.getLocal());
                }
            }
        }

        if (getDias() != null) {
            for (final DiaViagem dia : getDias()) {
                if (dia.getItens() != null) {
                    for (final Atividade atividade : dia.getItens()) {
                        if ((atividade.getLocal() != null) && atividade.getLocal().getTipoLocal().isTipoLocalEnderecavel()) {
                            locais.add(atividade.getLocal());
                        }
                    }
                }
            }
        }

        return locais;
    }

    public Set<ViagemLocalVisitado> getLocaisVisitados() {
        final Set<ViagemLocalVisitado> todos = new HashSet<ViagemLocalVisitado>();
        final Map<Long, ViagemLocalVisitado> mapLocalVisitadoKeyId = new HashMap<Long, ViagemLocalVisitado>();

        final Set<Local> locaisVisitadosTodos = getLocaisVisitadosTodos();

        for (Local local : locaisVisitadosTodos) {
            ViagemLocalVisitado localViagem = mapLocalVisitadoKeyId.get(local.getId());
            if (localViagem == null) {
                localViagem = new ViagemLocalVisitado(local);
                todos.add(localViagem);
                mapLocalVisitadoKeyId.put(local.getId(), localViagem);
            }

            while (local.getPai() != null) {
                local = local.getPai();

                localViagem = mapLocalVisitadoKeyId.get(local.getId());
                if (localViagem == null) {
                    localViagem = new ViagemLocalVisitado(local);
                    todos.add(localViagem);
                    mapLocalVisitadoKeyId.put(local.getId(), localViagem);
                }
            }
        }

        final Set<ViagemLocalVisitado> locaisSemPai = new HashSet<ViagemLocalVisitado>();
        for (final ViagemLocalVisitado local : todos) {
            if (local.getIdPai() != null) {
                mapLocalVisitadoKeyId.get(local.getIdPai()).addSubLocal(local);
            } else {
                locaisSemPai.add(local);
            }
        }

        return locaisSemPai;
    }

    public List<ViagemLocalVisitado> getLocaisVisitados2() {
        final List<ViagemLocalVisitado> locais = new ArrayList<ViagemLocalVisitado>();

        final Map<Long, ViagemLocalVisitado> mapLocalVisitado = new HashMap<Long, ViagemLocalVisitado>();

        final Set<Local> continentes = getLocaisVisitadosPorTipo(LocalType.CONTINENTE);

        for (final Local continente : continentes) {
            final ViagemLocalVisitado relatorioContinente = new ViagemLocalVisitado(continente);
            mapLocalVisitado.put(continente.getId(), relatorioContinente);

            locais.add(relatorioContinente);

            final Set<Local> paises = getLocaisVisitadosPorTipoEPai(LocalType.PAIS, continente);

            for (final Local pais : paises) {
                final ViagemLocalVisitado relatorioPais = new ViagemLocalVisitado(pais);
                mapLocalVisitado.put(pais.getId(), relatorioPais);

                relatorioContinente.addSubLocal(relatorioPais);

                final Set<Local> estados = getLocaisVisitadosPorTipoEPai(LocalType.ESTADO, pais);

                for (final Local estado : estados) {
                    final ViagemLocalVisitado relatorioEstado = new ViagemLocalVisitado(estado);
                    mapLocalVisitado.put(estado.getId(), relatorioEstado);

                    relatorioPais.addSubLocal(relatorioEstado);

                    final Set<Local> cidades = getLocaisVisitadosPorTipoEPai(LocalType.CIDADE, estado);

                    for (final Local cidade : cidades) {
                        final ViagemLocalVisitado relatorioCidade = new ViagemLocalVisitado(cidade);
                        mapLocalVisitado.put(cidade.getId(), relatorioCidade);

                        relatorioEstado.addSubLocal(relatorioCidade);

                        final Set<Local> turisticos = getLocaisVisitadosPorTipoEPai(LocalType.LOCAL_INTERESSE_TURISTICO, estado);
                        for (final Local turistico : turisticos) {
                            final ViagemLocalVisitado relatorioTuristico = new ViagemLocalVisitado(turistico);
                            mapLocalVisitado.put(turistico.getId(), relatorioTuristico);

                            relatorioCidade.addSubLocal(relatorioTuristico);

                        }
                    }
                }
            }
        }

        final Set<Local> locaisEnderecaveisVisitados = getLocaisEnderecaveisVisitados();

        for (final Local local : locaisEnderecaveisVisitados) {
            mapLocalVisitado.get(local.getPai().getId()).addSubLocal(local);
        }

        return locais;
    }

    private Set<Local> getLocaisVisitadosPorTipo(final LocalType localType) {
        final Set<Local> locais = new TreeSet<Local>(new Comparator<Local>() {

            @Override
            public int compare(final Local arg0, final Local arg1) {
                return arg0.getNome().compareTo(arg1.getNome());
            }
        });

        if (getItens() != null) {
            for (final Atividade atividade : getItens()) {
                if (atividade.getLocal() != null) {
                    locais.addAll(getLocaisVisitadosPorTipo(localType, atividade.getLocal()));
                }
            }
        }

        if (getDias() != null) {
            for (final DiaViagem dia : getDias()) {
                if (dia.getItens() != null) {
                    for (final Atividade atividade : dia.getItens()) {
                        if (atividade.getLocal() != null) {
                            locais.addAll(getLocaisVisitadosPorTipo(localType, atividade.getLocal()));
                        }
                    }
                }
            }
        }

        return locais;
    }

    private Set<Local> getLocaisVisitadosPorTipo(final LocalType localType, final Local local) {
        final Set<Local> locais = new HashSet<Local>();
        Local localPesquisa = local;
        while (localPesquisa != null) {
            if (localPesquisa.getTipoLocal().equals(localType)) {
                locais.add(localPesquisa);
            }

            localPesquisa = localPesquisa.getPai();
        }

        return locais;
    }

    private Set<Local> getLocaisVisitadosPorTipoEPai(final LocalType localType, final Local pai) {
        final Set<Local> locais = new TreeSet<Local>(new Comparator<Local>() {

            @Override
            public int compare(final Local arg0, final Local arg1) {
                return arg0.getNome().compareTo(arg1.getNome());
            }
        });

        if (getItens() != null) {
            for (final Atividade atividade : getItens()) {
                if (atividade.getLocal() != null && isPai(atividade.getLocal(), pai)) {
                    locais.addAll(getLocaisVisitadosPorTipo(localType, atividade.getLocal()));
                }
            }
        }

        if (getDias() != null) {
            for (final DiaViagem dia : getDias()) {
                if (dia.getItens() != null) {
                    for (final Atividade atividade : dia.getItens()) {
                        if (atividade.getLocal() != null && isPai(atividade.getLocal(), pai)) {
                            locais.addAll(getLocaisVisitadosPorTipo(localType, atividade.getLocal()));
                        }
                    }
                }
            }
        }

        return locais;
    }

    private Set<Local> getLocaisVisitadosTodos() {
        final Set<Local> locais = new HashSet<Local>();
        if (this.atividades != null) {
            for (final Atividade atividade : this.atividades) {
                locais.addAll(atividade.getLocaisVisitados());
            }
        }
        if (this.dias != null) {
            for (final DiaViagem dia : this.dias) {
                locais.addAll(dia.getLocaisVisitados());
            }
        }
        return locais;
    }

    /*public List<Atividade> getItensOrdenados() {
        final ArrayList<Atividade> atividadesOrdenadas = new ArrayList<Atividade>();

        if ((this.atividades != null) && !this.atividades.isEmpty()) {
            Atividade atividade = this.atividades.get(0).getItemPrimeiro();
            atividadesOrdenadas.add(atividade);
            while (atividade.getItemProximo() != null) {
                atividade = atividade.getItemProximo();
                atividadesOrdenadas.add(atividade);
            }
        }

        return atividadesOrdenadas;
    }*/

    public String getNome() {
        return this.titulo;
    }

    public Local getOrigem() {
        return this.origem;
    }

    public List<ParticipanteViagem> getParticipantesCompartilhamento() {
        return this.participantesCompartilhamento;
    }

    public List<ParticipanteViagem> getParticipantesViajantes() {
        return this.participantesViajantes;
    }

    public String getPeriodoFormatado() {
        final LocalDate dataFim = getDataFim();

        String periodo = "";
        if (this.dataInicio != null && dataFim != null) {
            periodo = this.dataInicio.toString("dd");
            if (this.dataInicio.getMonthOfYear() != dataFim.getMonthOfYear()) {
                periodo += " de " + this.dataInicio.toString("MMMM", new Locale("pt", "BR"));
                if (this.dataInicio.getYear() != dataFim.getYear()) {
                    periodo += " de " + this.dataInicio.toString("yyyy");
                } else {
                    periodo += " a " + dataFim.toString("dd");
                }
            } else {
                periodo += " a " + dataFim.toString("dd");
            }
            periodo += " de " + dataFim.toString("MMMM", new Locale("pt", "BR"));
            periodo += " de " + dataFim.toString("yyyy");
        }
        return periodo;
    }

    public boolean getPossuiAtividades() {
        final Integer quantidade = this.atividades.size();
        if (quantidade > 0) {
            return true;
        }
        for (final DiaViagem dia : this.dias) {
            if (dia.getAtividades() != null) {
                if (dia.getAtividades().size() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean getPossuiAtividadesOrganizar() {
        if (this.atividades.size() > 1) {
            for (final Atividade atividade : this.atividades) {
                if (atividade.getLocal() != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public DestinoViagem getPrimeiroDestino() {
        return getDestinosViagem().get(0);
    }

    public Integer getQuantidadeDias() {
        if (CollectionUtils.isNotEmpty(this.dias)) {
            return this.dias.size();
        }
        return null;
    }

    public Integer getQuantidadeTotalItens() {
        Integer quantidade = this.atividades.size();

        for (final DiaViagem dia : this.dias) {
            if (dia.getItens() != null) {
                quantidade += dia.getItens().size();
            }
        }

        return quantidade;
    }

    public List<ViagemResenha> getResenha() {
        final Map<LocalType, ViagemResenha> mapLocal = new TreeMap<LocalType, ViagemResenha>();
        final Map<TipoTransporte, ViagemResenha> mapTransporte = new TreeMap<TipoTransporte, ViagemResenha>();
        final ViagemResenha resenhaAvulso = new ViagemResenha();

        getResenha(mapLocal, mapTransporte, resenhaAvulso, getItens());
        for (final DiaViagem dia : getDias()) {
            getResenha(mapLocal, mapTransporte, resenhaAvulso, dia.getItens());
        }

        final List<ViagemResenha> resenhas = new ArrayList<ViagemResenha>();
        for (final LocalType localType : mapLocal.keySet()) {
            resenhas.add(mapLocal.get(localType));
        }
        for (final TipoTransporte transporte : mapTransporte.keySet()) {
            resenhas.add(mapTransporte.get(transporte));
        }
        if (resenhaAvulso.getQuantidade() > 0) {
            resenhas.add(resenhaAvulso);
        }

        return resenhas;
    }

    private void getResenha(final Map<LocalType, ViagemResenha> mapLocal, final Map<TipoTransporte, ViagemResenha> mapTransporte,
            final ViagemResenha resenhaAvulso, final Collection<Atividade> atividades) {
        ViagemResenha resenhaAdd = null;
        if (atividades != null) {
            for (final Atividade atividade : atividades) {
                if (atividade.getLocal() != null) {
                    if (!mapLocal.containsKey(atividade.getLocal().getTipoLocal())) {
                        mapLocal.put(atividade.getLocal().getTipoLocal(), new ViagemResenha(atividade.getLocal().getTipoLocal()));
                    }
                    resenhaAdd = mapLocal.get(atividade.getLocal().getTipoLocal());
                } else if (atividade.getTransporte() != null) {
                    if (!mapTransporte.containsKey(atividade.getTransporte())) {
                        mapTransporte.put(atividade.getTransporte(), new ViagemResenha(atividade.getTransporte()));
                    }
                    resenhaAdd = mapTransporte.get(atividade.getTransporte());
                } else {
                    resenhaAdd = resenhaAvulso;
                }

                resenhaAdd.addItem(atividade);
            }
        }
    }

    public String getTitulo() {
        return this.titulo;
    }

    public List<Atividade> getTodasAtividadesViagem() {
        final List<Atividade> atividades = new ArrayList();
        atividades.addAll(this.atividades);
        if (this.dias != null) {
            for (final DiaViagem dia : this.dias) {
                atividades.addAll(dia.getAtividadesQueComecam());
            }
        }
        return atividades;
    }

    public String getUrlFotoPrincipal() {
        final Local destino = getDestinosViagem().get(0).getDestino();
        return destino.getUrlFotoAlbum();
    }

    public Usuario getUsuarioCriador() {
        return this.usuarioCriador;
    }

    public BigDecimal getValorAtividadesAlimentacao() {
        return getValorPorTipoAtividade(TipoAtividade.ALIMENTACAO);
    }

    public BigDecimal getValorAtividadesAtracoes() {
        return getValorPorTipoAtividade(TipoAtividade.VISITA_ATRACAO);

    }

    public BigDecimal getValorAtividadesHospedagem() {
        return getValorPorTipoAtividade(TipoAtividade.HOSPEDAGEM);

    }

    public BigDecimal getValorAtividadesPersonalizadas() {
        return getValorPorTipoAtividade(TipoAtividade.PERSONALIZADO);

    }

    public BigDecimal getValorAtividadesTransporte() {
        return getValorPorTipoAtividade(TipoAtividade.TRANSPORTE);
    }

    public BigDecimal getValorItens() {
        BigDecimal valor = new BigDecimal(0);

        if (this.atividades != null) {
            for (final Atividade atividade : this.atividades) {
                if (atividade == null) {
                    System.out.println(atividade.getId());
                }
                if (atividade.getValor() != null) {
                    valor = valor.add(atividade.getValor());
                }
            }
        }

        return valor;
    }

    public BigDecimal getValorPorTipoAtividade(final TipoAtividade tipoAtividade) {
        BigDecimal valor = new BigDecimal(0);
        final List<Atividade> atividades = getAtividadesPorTipoOrdenadas(tipoAtividade);
        if (atividades != null) {
            for (final Atividade atividade : atividades) {
                if (atividade.getValor() != null) {
                    valor = valor.add(atividade.getValor());
                }
            }
        }
        return valor;
    }

    public BigDecimal getValorTotal() {
        BigDecimal valor = getValorItens();

        if (this.dias != null) {
            for (final DiaViagem dia : this.dias) {
                valor = valor.add(dia.getValor());
            }
        }

        return valor;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    private boolean isPai(final Local local, final Local pai) {
        for (final Local paiPesquisa : local.getTodosPais()) {
            if (paiPesquisa.getId().equals(pai.getId())) {
                return true;
            }
        }

        return false;
    }

    public void setAtividades(final List<Atividade> atividades) {
        this.atividades = atividades;
    }

    public void setControlarGastos(final Boolean controlarGastos) {
        this.controlarGastos = controlarGastos;
    }

    public void setControlarTempo(final Boolean controlarTempo) {
        this.controlarTempo = controlarTempo;
    }

    public void setDataInclusao(final Date dataInclusao) {
        if (dataInclusao != null) {
            setDataCriacao(new LocalDateTime(dataInclusao));
        }
    }

    public void setDataInicio(final LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDestinosViagem(final List<DestinoViagem> destinosViagem) {
        this.destinosViagem = destinosViagem;
    }

    public void setDias(final List<DiaViagem> dias) {
        this.dias = dias;
    }

    public void setNome(final String nome) {
        this.titulo = nome;
    }

    public void setOrigem(final Local origem) {
        this.origem = origem;
    }

    public void setParticipantesCompartilhamento(final List<ParticipanteViagem> participantesCompartilhamento) {
        this.participantesCompartilhamento = participantesCompartilhamento;
    }

    public void setParticipantesViajantes(final List<ParticipanteViagem> participantesViajantes) {
        this.participantesViajantes = participantesViajantes;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    public void setUsuarioCriador(final Usuario usuarioCriador) {
        this.usuarioCriador = usuarioCriador;
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

    public boolean usuarioPodeEditar(final Usuario usuario) {
        if (this.usuarioCriador.equals(usuario)) {
            return true;
        }
        return false;
    }

    public boolean usuarioPodeExcluir(final Usuario usuario) {
        if (this.usuarioCriador.equals(usuario)) {
            return true;
        }
        return false;
    }

    public boolean usuarioPodeVer(final Usuario usuario, final boolean usuarioAmigoCriador) {
        if (this.visibilidade == null) {
            return false;
        }
        if (this.visibilidade.equals(TipoVisibilidade.PUBLICO)) {
            return true;
        }
        if (usuario == null) {
            return false;
        }
        if (this.usuarioCriador.equals(usuario)) {
            return true;
        }
        if (this.visibilidade.equals(TipoVisibilidade.AMIGOS) && usuarioAmigoCriador) {
            return true;
        }
        return false;
    }

}
