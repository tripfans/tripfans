package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.springframework.util.AutoPopulatingList;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 *
 * @author Pedro Sebba
 * @author Carlos Nascimento
 *
 */
@Entity
@Table(name = "drop_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_viagem", allocationSize = 1)
@TypeDefs(value = {
        @TypeDef(name = "SituacaoViagem", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.PlanoViagem$SituacaoViagem") }),
        @TypeDef(name = "TipoPeriodoViagem", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.PlanoViagem$TipoPeriodoViagem") }),
        @TypeDef(name = "Visibilidade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoVisibilidade") }) })
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false, unique = true)) })
public class PlanoViagem extends EntidadeGeradoraAcao<Long> implements Comentavel {

    /**
     * Classe que encapsula o numero dos dias e seus respectivos dias do mes
     *
     * @author carlosn
     *
     */
    public static class DiaViagem {

        public static List<DiaViagem> criarDiasViagem(final PlanoViagem planoViagem) {
            final List<DiaViagem> lista = new ArrayList<DiaViagem>();

            if (planoViagem.isPeriodoNaoDefinido()) {
                lista.add(new DiaViagem(1, null, planoViagem.getAtividades()));
            } else {

                LocalDate data = planoViagem.getDataInicio();
                if (planoViagem.getQuantidadeDias() != null) {
                    for (int i = 1; i <= planoViagem.getQuantidadeDias(); i++) {
                        lista.add(new DiaViagem(i, data, planoViagem.getAtividadesDoDia(i)));
                        if (data != null) {
                            data = data.plusDays(1);
                        }
                    }
                }
            }
            return lista;
        }

        public static DiaViagem criarDiaViagem(final PlanoViagem planoViagem, final Integer numeroDia) {
            LocalDate data = planoViagem.getDataInicio();
            if (data != null) {
                data = data.plusDays(numeroDia - 1);
            }
            return new DiaViagem(numeroDia, data, planoViagem.getAtividadesDoDia(numeroDia));
        }

        private final List<AtividadePlano> atividadePlanos;

        private LocalDate data;

        private Integer numeroDia;

        private DiaViagem(final Integer numeroDia, final LocalDate data, final List<AtividadePlano> atividadePlanos) {
            this.numeroDia = numeroDia;
            this.data = data;
            this.atividadePlanos = atividadePlanos;
        }

        public List<AtividadePlano> getAtividades() {
            return this.atividadePlanos;
        }

        public LocalDate getData() {
            return this.data;
        }

        public String getDiaMes() {
            if (this.data != null) {
                return this.data.toString("dd");
            }
            return null;
        }

        public String getDiaMesComMesCurto() {
            if (this.data != null) {
                return this.data.toString("dd 'de' MMM");
            }
            return null;
        }

        public String getDiaMesComMesLongo() {
            if (this.data != null) {
                return this.data.toString("dd 'de' MMMM");
            }
            return null;
        }

        public String getDiaSemanaCurto() {
            if (this.data != null) {
                return this.data.toString("EEE");
            }
            return null;
        }

        public String getDiaSemanaCurtoDiaMesComMesCurto() {
            if (this.data != null) {
                return this.data.toString("EEE, dd 'de' MMM");
            }
            return null;
        }

        public String getDiaSemanaCurtoDiaMesComMesLongo() {
            if (this.data != null) {
                return this.data.toString("EEE, dd 'de' MMMM");
            }
            return null;
        }

        public String getDiaSemanaLongo() {
            if (this.data != null) {
                return this.data.toString("EEEE");
            }
            return null;
        }

        public String getDiaSemanaLongoDiaMesComMesLongo() {
            if (this.data != null) {
                return this.data.toString("EEEE, dd 'de' MMMM");
            }
            return null;
        }

        public String getMes() {
            if (this.data != null) {
                return this.data.toString("MM");
            }
            return null;
        }

        public String getMesCurto() {
            if (this.data != null) {
                return this.data.toString("MMM");
            }
            return null;
        }

        public String getMesLongo() {
            if (this.data != null) {
                return this.data.toString("MMMM");
            }
            return null;
        }

        public Integer getNumero() {
            return this.numeroDia;
        }

        public Integer getNumeroDiaMes() {
            if (this.data != null) {
                return this.data.getDayOfMonth();
            }
            return null;
        }

        public Integer getNumeroMes() {
            if (this.data != null) {
                return this.data.getMonthOfYear();
            }
            return null;
        }

        public boolean getPossuiAtividadesAExibir() {
            if (this.atividadePlanos == null || this.atividadePlanos.isEmpty()) {
                return false;
            } else {
                for (final AtividadePlano atividadePlano : this.atividadePlanos) {
                    if (atividadePlano.isVisivel()) {
                        return true;
                    }
                }
                return false;
            }
        }

        public Integer getQuantidadeAtividades() {
            return this.atividadePlanos != null ? this.atividadePlanos.size() : null;
        }

        public void setData(final LocalDate data) {
            this.data = data;
        }

        public void setNumeroDia(final Integer numeroDia) {
            this.numeroDia = numeroDia;
        }
    }

    public enum SituacaoViagem implements EnumTypeInteger {
        EXECUTADA(1, "Executada"),
        PLANEJADA(2, "Planejada");

        private final EnumI18nUtil util;

        SituacaoViagem(final int codigo, final String... descricoes) {
            this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        }

        @Override
        public Integer getCodigo() {
            return this.util.getCodigo();
        }

        @Override
        public String getDescricao() {
            return this.util.getDescricao();
        }
    }

    public enum TipoPeriodoViagem implements EnumTypeInteger {

        NAO_DEFINIDO(3, "Não definido"),
        POR_DATAS(1, "Informar datas"),
        POR_QUANTIDADE_DIAS(2, "Quantidade de dias");

        private final EnumI18nUtil util;

        TipoPeriodoViagem(final int codigo, final String... descricoes) {
            this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        }

        @Override
        public Integer getCodigo() {
            return this.util.getCodigo();
        }

        @Override
        public String getDescricao() {
            return this.util.getDescricao();
        }
    }

    private static final long serialVersionUID = -2499849897982827294L;

    @OneToMany(mappedBy = "planoViagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("ordem")
    @IndexColumn(base = 1, name = "ORDEM")
    private List<AtividadePlano> atividadePlanos = new ArrayList<AtividadePlano>();

    @Column(name = "compartilhada_facebook", nullable = true)
    private Boolean compartilhadaFacebook = false;

    @Column(name = "compartilhada_twitter", nullable = true)
    private Boolean compartilhadaTwitter = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "criador", nullable = false)
    private Usuario criador;

    @Column(name = "criador_viaja", nullable = true)
    private Boolean criadorViaja = true;

    @Column(name = "data_fim")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataFim;

    @Column(name = "\"dataFimTZ\"")
    private String dataFimTZ;

    @Column(name = "data_inicio")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataInicio;

    @Column(name = "\"dataInicioTZ\"")
    private String dataInicioTZ;

    @Column(name = "descricao", nullable = false, length = 500)
    private String descricao;

    // @OneToMany(mappedBy = "planoViagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    // @OrderBy("ordem")
    // @IndexColumn(base = 1, name = "ORDEM")
    // private List<DestinoViagem> destinosViagem = new ArrayList<DestinoViagem>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "planoViagem")
    @JoinColumn(name = "id_diario")
    private DiarioViagem diario;

    @Column(name = "finalizada", nullable = true)
    private Boolean finalizada = false;

    /*
     * Cidade, Estado ou País. Atentar que esse é o destino inicial. Essa informação também é uma espécie de desnormalização. Deve ser atualizada caso
     * seja feito um plano de transporte que leve a outro lugar. Podemos até mostrar outros destinos que forem adicionados ao planejamento.
     */

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_foto_ilustrativa")
    private Foto fotoIlustrativa;

    @Transient
    @Valid
    private List<Foto> fotos = new AutoPopulatingList<Foto>(Foto.class);

    @Column(name = "mostrar_detalhes")
    @Type(type = "Visibilidade")
    private TipoVisibilidade mostrarDetalhes;

    @Column(nullable = false)
    private String nome;

    /*@OneToMany(mappedBy = "planoViagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ParticipanteViagem> participantes;*/

    @Column(name = "possui_diario", nullable = true)
    private Boolean possuiDiario = false;

    @Column(name = "quantidade_dias")
    private Integer quantidadeDias;

    @Type(type = "SituacaoViagem")
    @Column(name = "situacao")
    private SituacaoViagem situacao = SituacaoViagem.PLANEJADA;

    @Type(type = "TipoPeriodoViagem")
    @Column(name = "tipo_periodo")
    private TipoPeriodoViagem tipoPeriodo = TipoPeriodoViagem.POR_DATAS;

    @Type(type = "Visibilidade")
    @Column(name = "visibilidade", nullable = true)
    private TipoVisibilidade visibilidade;

    public void addAtividade(final AtividadePlano atividadePlano) {
        atividadePlano.setViagem(this);
        this.atividadePlanos.add(atividadePlano);
    }

    public AtividadePlano addAtividade(final LocalEnderecavel local, final Integer dia) {
        final AtividadePlano atividadePlano = criarAtividade(local);
        atividadePlano.setLocal(local);
        atividadePlano.setDiaInicio(dia);
        atividadePlano.setOrdem(getUltimaPosicaoOrdem());
        this.addAtividade(atividadePlano);
        return atividadePlano;
    }

    public void addDestino(final DestinoViagem destinoViagem) {
        // COMENTEI PARA NAO QUEBRA O NOVO PLANEJAMENTO
        // if (this.destinosViagem == null) {
        // this.destinosViagem = new ArrayList<DestinoViagem>();
        // }
        // // destinoViagem.setViagem(this);
        // this.destinosViagem.add(destinoViagem);
    }

    private AtividadePlano criarAtividade(final LocalEnderecavel local) {
        AtividadePlano atividadePlano = null;
        if (local == null) {
            atividadePlano = new Historia();
        } else if (local.getTipoLocal().equals(LocalType.ATRACAO)) {
            atividadePlano = new VisitaAtracao();
        } else if (local.getTipoLocal().equals(LocalType.RESTAURANTE)) {
            atividadePlano = new Alimentacao();
        } else if (local.getTipoLocal().equals(LocalType.HOTEL)) {
            atividadePlano = new Hospedagem();
        }
        return atividadePlano;
    }

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return null;
    }

    public List<AtividadePlano> getAtividades() {
        return this.atividadePlanos;
    }

    public List<AtividadePlano> getAtividadesDoDia(final Integer numeroDia) {
        final List<AtividadePlano> atividadesDoDia = new ArrayList<AtividadePlano>();
        for (final AtividadePlano atividadePlano : getAtividades()) {
            // TODO ver pq algumas vezes a atividade está 'null'. Acho q é por conta do campo ordem
            if (atividadePlano != null) {
                if (atividadePlano.getDiaInicio().equals(numeroDia)) {
                    atividadesDoDia.add(atividadePlano);
                }
            }
        }
        return atividadesDoDia;
    }

    @Override
    public Usuario getAutor() {
        return getCriador();
    }

    public List<LocalGeografico> getCidades() {
        final List<LocalGeografico> listaCidades = new ArrayList<LocalGeografico>();
        if (getDestinosViagem() != null) {
            for (final DestinoViagem destinoViagem : getDestinosViagem()) {
                listaCidades.add(destinoViagem.getDestino());
            }
        }
        return listaCidades;
    }

    public Boolean getCompartilhadaFacebook() {
        return this.compartilhadaFacebook;
    }

    public Boolean getCompartilhadaTwitter() {
        return this.compartilhadaTwitter;
    }

    public Usuario getCriador() {
        return this.criador;
    }

    public Boolean getCriadorViaja() {
        return this.criadorViaja;
    }

    public LocalDate getDataFim() {
        return this.dataFim;
    }

    public String getDataFimTZ() {
        return this.dataFimTZ;
    }

    public LocalDate getDataInicio() {
        return this.dataInicio;
    }

    public String getDataInicioTZ() {
        return this.dataInicioTZ;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public String getDescricaoAlvo() {
        return "PlanoViagem: \"" + this.getNome() + "\"";
    }

    @Override
    public Usuario getDestinatario() {
        return getCriador();
    }

    public List<DestinoViagem> getDestinosViagem() {
        // COMENTEI PARA NAO QUEBRA O NOVO PLANEJAMENTO
        // return this.destinosViagem;
        return null;
    }

    public DiarioViagem getDiario() {
        return this.diario;
    }

    public List<DiaViagem> getDiasViagem() {
        return DiaViagem.criarDiasViagem(this);
    }

    /*public enum PropositoViagem {
        LAZER,
        NEGOCIOS;
    }*/

    public DiaViagem getDiaViagem(final Integer numeroDia) {
        return DiaViagem.criarDiaViagem(this, numeroDia);
    }

    public Boolean getFinalizada() {
        return this.finalizada;
    }

    public Foto getFotoIlustrativa() {
        return this.fotoIlustrativa;
    }

    public List<Foto> getFotos() {
        return this.fotos;
    }

    public TipoVisibilidade getMostrarDetalhes() {
        return this.mostrarDetalhes;
    }

    public String getNome() {
        return this.nome;
    }

    /*public List<ParticipanteViagem> getParticipantes() {
        return this.participantes;
    }*/

    public Period getPeriodo() {
        if (getDataInicio() != null && getDataFim() != null) {
            return new Period(this.dataInicio, this.dataFim);
        }
        return null;
    }

    public String getPeriodoFormatado() {
        /*final PeriodFormatter formatter = new PeriodFormatterBuilder().appendYears().appendSeparator(" and ").printZeroRarelyLast().appendMonths()
                .toFormatter();
        return formatter.withLocale(LocaleContextHolder.getLocale()).print(getPeriodo());*/
        String periodo = "";
        if (this.dataInicio != null && this.dataFim != null) {
            periodo = this.dataInicio.toString("dd");
            if (this.dataInicio.getMonthOfYear() != this.dataFim.getMonthOfYear()) {
                periodo += " de " + this.dataInicio.toString("MMMM");
                if (this.dataInicio.getYear() != this.dataFim.getYear()) {
                    periodo += " de " + this.dataInicio.toString("yyyy");
                }
            } else {
                periodo += " a " + this.dataFim.toString("dd");
            }
            periodo += " de " + this.dataFim.toString("MMMM");
            periodo += " de " + this.dataFim.toString("yyyy");
        }
        return periodo;
    }

    public Boolean getPossuiDiario() {
        return this.possuiDiario;
    }

    public Integer getQuantidadeDias() {
        if (isPeriodoPorDatas() && (getDataInicio() != null && getDataFim() != null)) {

            final Interval intervalo = new Interval(this.dataInicio.toDate().getTime(), this.dataFim.toDate().getTime());
            this.quantidadeDias = ((Long) intervalo.toDuration().getStandardDays()).intValue();
        }
        return this.quantidadeDias;
    }

    public SituacaoViagem getSituacao() {
        return this.situacao;
    }

    public TipoPeriodoViagem getTipoPeriodo() {
        return this.tipoPeriodo;
    }

    private Integer getUltimaPosicaoOrdem() {
        Integer ordem = null;
        if (getAtividades() != null && !this.atividadePlanos.isEmpty()) {
            // encontra a ultima da lista
            ordem = getAtividades().get(this.atividadePlanos.size() - 1).getOrdem();
        }
        if (ordem != null) {
            return ordem + 1;
        }
        return 1;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    public boolean isPeriodoNaoDefinido() {
        return TipoPeriodoViagem.NAO_DEFINIDO.equals(getTipoPeriodo());
    }

    public boolean isPeriodoPorDatas() {
        return TipoPeriodoViagem.POR_DATAS.equals(getTipoPeriodo());
    }

    public boolean isPeriodoPorQuantidadeDias() {
        return TipoPeriodoViagem.POR_QUANTIDADE_DIAS.equals(getTipoPeriodo());
    }

    public boolean isPlanejamento() {
        return getSituacao() != null && this.situacao.equals(SituacaoViagem.PLANEJADA);
    }

    public void removeAtividade(final AtividadePlano atividadePlano) {
        atividadePlano.setViagem(null);
        this.atividadePlanos.remove(atividadePlano);
    }

    public void setAtividades(final List<AtividadePlano> atividadePlanos) {
        this.atividadePlanos = atividadePlanos;
    }

    public void setCompartilhadaFacebook(final Boolean compartilhadaFacebook) {
        this.compartilhadaFacebook = compartilhadaFacebook;
    }

    public void setCompartilhadaTwitter(final Boolean compartilhadaTwitter) {
        this.compartilhadaTwitter = compartilhadaTwitter;
    }

    public void setCriador(final Usuario criador) {
        this.criador = criador;
    }

    public void setCriadorViaja(final Boolean criadorViaja) {
        this.criadorViaja = criadorViaja;
    }

    public void setDataFim(final LocalDate dataFim) {
        this.dataFim = dataFim;
    }

    public void setDataFimTZ(final String dataFimTZ) {
        this.dataFimTZ = dataFimTZ;
    }

    public void setDataInicio(final LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    public void setDataInicioTZ(final String dataInicioTZ) {
        this.dataInicioTZ = dataInicioTZ;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDestinosViagem(final List<DestinoViagem> destinosViagem) {
        // COMENTEI PARA NAO QUEBRA O NOVO PLANEJAMENTO
        // this.destinosViagem = destinosViagem;
    }

    public void setDiario(final DiarioViagem diario) {
        this.diario = diario;
    }

    public void setFinalizada(final Boolean finalizada) {
        this.finalizada = finalizada;
    }

    public void setFotoIlustrativa(final Foto fotoIlustrativa) {
        this.fotoIlustrativa = fotoIlustrativa;
    }

    public void setFotos(final List<Foto> fotos) {
        this.fotos = fotos;
    }

    public void setMostrarDetalhes(final TipoVisibilidade mostrarDetalhesViagem) {
        this.mostrarDetalhes = mostrarDetalhesViagem;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    /*public void setParticipantes(final List<ParticipanteViagem> participantes) {
        this.participantes = participantes;
    }*/

    public void setPossuiDiario(final Boolean possuiDiario) {
        this.possuiDiario = possuiDiario;
    }

    public void setQuantidadeDias(final Integer quantidadeDias) {
        this.quantidadeDias = quantidadeDias;
    }

    public void setSituacao(final SituacaoViagem situacao) {
        this.situacao = situacao;
    }

    public void setTipoPeriodo(final TipoPeriodoViagem tipoPeriodo) {
        // Zerar os valore de acordo com o tipo
        if (TipoPeriodoViagem.POR_DATAS.equals(tipoPeriodo) || TipoPeriodoViagem.NAO_DEFINIDO.equals(tipoPeriodo)) {
            setQuantidadeDias(null);
        }
        if (TipoPeriodoViagem.POR_QUANTIDADE_DIAS.equals(tipoPeriodo) || TipoPeriodoViagem.NAO_DEFINIDO.equals(tipoPeriodo)) {
            setDataInicio(null);
            setDataFim(null);
        }
        this.tipoPeriodo = tipoPeriodo;
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

}
