package br.com.fanaticosporviagens.model.entity;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
/*@Entity
@Table(name = "dica_atividade")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_dica_atividade", allocationSize = 1)*/
public class DicaAtividade extends BaseEntity<Long> {

    private static final long serialVersionUID = 3592294674422967324L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_atividade", nullable = false)
    private AtividadePlano atividadePlano;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = false)
    private Dica dica;

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    public Dica getDica() {
        return this.dica;
    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
    }

    public void setDica(final Dica dica) {
        this.dica = dica;
    }

}
