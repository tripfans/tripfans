package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoAcao implements EnumTypeInteger {

    ACEITAR_CONVITE_AMIZADE(1, "Adicionar", "adicionou", "", "você", "como amigo", "", Usuario.class,
    // executarPosInclusao
                            false,
                            // executarPosAlteracao
                            true,
                            // visivel
                            true,
                            // notificavel
                            true,
                            // notificavelEmail
                            true,
                            // TipoNotificacaoEmail
                            TipoNotificacaoPorEmail.AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_AMIZADE,
                            // publicavel
                            false,
                            // publicavelNaHome
                            false,
                            // pontuavel
                            true,
                            // quantidadePontos
                            new BigDecimal("2"),
                            // icone
                            "Users-13.png"),
    // TODO Mudar ao criar a classe Grupo
    /*ACEITAR_CONVITE_GRUPO(2, "Aceitar", "aceitou", "", "seu convite para", "fazer parte do grupo", "", null,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          true,
                          // notificavel
                          true,
                          // notificavelEmail
                          true,
                          // TipoNotificacaoEmail
                          TipoNotificacaoPorEmail.AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_PARTICIPACAO_EM_GRUPO,
                          // publicavel
                          false,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          true,
                          // quantidadePontos
                          new BigDecimal("2"),
                          // icone
                          ""),*/
    ACEITAR_CONVITE_TRIPFANS(3, "Aceitar", "aceitou", "o", "seu", "convite para entrar no TripFans", "", null,
    // executarPosInclusao
                             true,
                             // executarPosAlteracao
                             false,
                             // visivel
                             false,
                             // notificavel
                             true,
                             // notificavelEmail
                             true,
                             // TipoNotificacaoEmail
                             TipoNotificacaoPorEmail.AMIGOS_ACEITAR_CONVITE_TRIPFANS,
                             // publicavel
                             false,
                             // publicavelNaHome
                             false,
                             // pontuavel
                             true,
                             // quantidadePontos
                             new BigDecimal("2"),
                             // icone
                             ""),
    ACEITAR_CONVITE_VIAGEM(4, "Aceitar", "aceitou", "o", "seu", "convite para viajar", "com", Viagem.class,
    // executarPosInclusao
                           true,
                           // executarPosAlteracao
                           false,
                           // visivel
                           true,
                           // notificavel
                           true,
                           // notificavelEmail
                           true,
                           // TipoNotificacaoEmail
                           TipoNotificacaoPorEmail.VIAGENS_ACEITAR_CONVITE_PARA_PARTICIPAR_DE_VIAGEM,
                           // publicavel
                           false,
                           // publicavelNaHome
                           false,
                           // pontuavel
                           true,
                           // quantidadePontos
                           new BigDecimal("2"),
                           // icone
                           "Leisure-2.png"),
    ACEITAR_RECOMENDACAO_LOCAL_AMIGO(5, "Aceitar", "aceitou", "uma", "sua", "recomendação sobre", "", null,
    // executarPosInclusao
                                     true,
                                     // executarPosAlteracao
                                     false,
                                     // visivel
                                     true,
                                     // notificavel
                                     true,
                                     // notificavelEmail
                                     true,
                                     // TipoNotificacaoEmail
                                     TipoNotificacaoPorEmail.VIAGENS_ACEITAR_RECOMENDACAO_SOBRE_LUGARES,
                                     // publicavel
                                     false,
                                     // publicavelNaHome
                                     false,
                                     // pontuavel
                                     true,
                                     // quantidadePontos
                                     new BigDecimal("0.1"),
                                     // icone
                                     ""),
    ADICIONAR_ALBUM(6, "Adicionar", "adicionou", "um", "", "album de fotos", "", null,
    // executarPosInclusao
                    true,
                    // executarPosAlteracao
                    false,
                    // visivel
                    true,
                    // notificavel
                    false,
                    // notificavelEmail
                    false,
                    // TipoNotificacaoEmail
                    null,
                    // publicavel
                    false,
                    // publicavelNaHome
                    false,
                    // pontuavel
                    true,
                    // quantidadePontos
                    new BigDecimal("2"),
                    // icone
                    "Multimedia-13.png"),
    ADICIONAR_FOTO(7, "Adicionar", "adicionou", "", "", "nova(s) foto(s)", "", Foto.class,
    // executarPosInclusao
                   true,
                   // executarPosAlteracao
                   false,
                   // visivel
                   true,
                   // notificavel
                   false,
                   // notificavelEmail
                   false,
                   // TipoNotificacaoEmail
                   null,
                   // publicavel
                   false,
                   // publicavelNaHome
                   false,
                   // pontuavel
                   true,
                   // quantidadePontos
                   new BigDecimal("2"),
                   // icone
                   "Multimedia-14.png"),

    ADICIONAR_FOTO_ALBUM(57, "Adicionar", "adicionou", "", "", "nova(s) foto(s)", "em seu", Foto.class,
    // executarPosInclusao
                         true,
                         // executarPosAlteracao
                         false,
                         // visivel
                         true,
                         // notificavel
                         false,
                         // notificavelEmail
                         false,
                         // TipoNotificacaoEmail
                         null,
                         // publicavel
                         false,
                         // publicavelNaHome
                         false,
                         // pontuavel
                         true,
                         // quantidadePontos
                         new BigDecimal("2"),
                         // icone
                         "Multimedia-14.png"),
    ADICIONAR_FOTO_VIAGEM(58, "Adicionar", "adicionou", "", "", "nova(s) foto(s)", "em sua", Foto.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          true,
                          // notificavel
                          false,
                          // notificavelEmail
                          false,
                          // TipoNotificacaoEmail
                          null,
                          // publicavel
                          false,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          true,
                          // quantidadePontos
                          new BigDecimal("2"),
                          // icone
                          "Multimedia-14.png"),
    ALTERAR_DIARIO_VIAGEM(8, "Alterar", "alterou", "o", "", "diário de viagem", "", PlanoViagem.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          true,
                          // notificavel
                          false,
                          // notificavelEmail
                          false,
                          // TipoNotificacaoEmail
                          null,
                          // publicavel
                          false,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          false,
                          // quantidadePontos
                          new BigDecimal(0),
                          // icone
                          ""),
    ALTERAR_FOTO_PERFIL(9, "Alterar", "alterou", "a", "", "foto do perfil", "", Usuario.class,
    // executarPosInclusao
                        true,
                        // executarPosAlteracao
                        false,
                        // visivel
                        true,
                        // notificavel
                        false,
                        // notificavelEmail
                        false,
                        // TipoNotificacaoEmail
                        null,
                        // publicavel
                        false,
                        // publicavelNaHome
                        false,
                        // pontuavel
                        false,
                        // quantidadePontos
                        new BigDecimal(0),
                        // icone
                        "Multimedia-4.png"),
    ALTERAR_INFORMACOES_BASICAS_PERFIL(10, "Alterar", "alterou", "as", "", "informações básicas do perfil", "", null,
    // executarPosInclusao
                                       true,
                                       // executarPosAlteracao
                                       false,
                                       // visivel
                                       true,
                                       // notificavel
                                       false,
                                       // notificavelEmail
                                       false,
                                       // TipoNotificacaoEmail
                                       null,
                                       // publicavel
                                       false,
                                       // publicavelNaHome
                                       false,
                                       // pontuavel
                                       false,
                                       // quantidadePontos
                                       new BigDecimal(0),
                                       // icone
                                       "Users-30.png"),
    ALTERAR_INTERESSES_VIAGEM_PERFIL(11, "Alterar", "alterou", "os", "", "interesses de viagem", "", Usuario.class,
    // executarPosInclusao
                                     true,
                                     // executarPosAlteracao
                                     false,
                                     // visivel
                                     true,
                                     // notificavel
                                     false,
                                     // notificavelEmail
                                     false,
                                     // TipoNotificacaoEmail
                                     null,
                                     // publicavel
                                     false,
                                     // publicavelNaHome
                                     false,
                                     // pontuavel
                                     false,
                                     // quantidadePontos
                                     new BigDecimal(0),
                                     // icone
                                     "Users-41.png"),
    ALTERAR_PLANO_VIAGEM(12, "Alterar", "alterou", "o", "", "plano de viagem", "", PlanoViagem.class,
    // executarPosInclusao
                         true,
                         // executarPosAlteracao
                         false,
                         // visivel
                         true,
                         // notificavel
                         false,
                         // notificavelEmail
                         false,
                         // TipoNotificacaoEmail
                         null,
                         // publicavel
                         false,
                         // publicavelNaHome
                         false,
                         // pontuavel
                         false,
                         // quantidadePontos
                         new BigDecimal(0),
                         // icone
                         ""),
    ALTERAR_PREFERENCIAS_VIAGEM_PERFIL(59, "Alterar", "alterou", "as", "", "preferências de viagem", "", Usuario.class,
    // executarPosInclusao
                                       true,
                                       // executarPosAlteracao
                                       false,
                                       // visivel
                                       true,
                                       // notificavel
                                       false,
                                       // notificavelEmail
                                       false,
                                       // TipoNotificacaoEmail
                                       null,
                                       // publicavel
                                       false,
                                       // publicavelNaHome
                                       false,
                                       // pontuavel
                                       false,
                                       // quantidadePontos
                                       new BigDecimal(0),
                                       // icone
                                       "Setting-2.png"),
    COMENTAR_ALBUM(13, "Comentar", "comentou", "no", "em seu", "album de fotos", "", Album.class,
    // executarPosInclusao
                   true,
                   // executarPosAlteracao
                   false,
                   // visivel
                   true,
                   // notificavel
                   true,
                   // notificavelEmail
                   true,
                   // TipoNotificacaoEmail
                   TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_FOTOS,
                   // publicavel
                   false,
                   // publicavelNaHome
                   false,
                   // pontuavel
                   false,
                   // quantidadePontos
                   new BigDecimal(0),
                   // icone
                   "Communication-95.png"),
    COMENTAR_AVALIACAO(14, "Comentar", "comentou", "uma", "sua", "avaliação", "sobre", Avaliacao.class,
    // executarPosInclusao
                       true,
                       // executarPosAlteracao
                       false,
                       // visivel
                       true,
                       // notificavel
                       true,
                       // notificavelEmail
                       true,
                       // TipoNotificacaoEmail
                       TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_RELATOS_DICAS_AVALIACOES,
                       // publicavel
                       false,
                       // publicavelNaHome
                       false,
                       // pontuavel
                       false,
                       // quantidadePontos
                       new BigDecimal(0),
                       // icone
                       "Communication-95.png"),
    COMENTAR_DIARIO_VIAGEM(15, "Comentar", "comentou", "no", "em seu", "diário de viagem", "", DiarioViagem.class,
    // executarPosInclusao
                           true,
                           // executarPosAlteracao
                           false,
                           // visivel
                           true,
                           // notificavel
                           true,
                           // notificavelEmail
                           true,
                           // TipoNotificacaoEmail
                           TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_DIARIO_VIAGEM,
                           // publicavel
                           false,
                           // publicavelNaHome
                           false,
                           // pontuavel
                           false,
                           // quantidadePontos
                           new BigDecimal(0),
                           // icone
                           "Communication-95.png"),
    COMENTAR_DICA(16, "Comentar ", "comentou", "uma", "sua", "dica", "sobre", Dica.class,
    // executarPosInclusao
                  true,
                  // executarPosAlteracao
                  false,
                  // visivel
                  true,
                  // notificavel
                  true,
                  // notificavelEmail
                  true,
                  // TipoNotificacaoEmail
                  TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_RELATOS_DICAS_AVALIACOES,
                  // publicavel
                  false,
                  // publicavelNaHome
                  false,
                  // pontuavel
                  false,
                  // quantidadePontos
                  new BigDecimal(0),
                  // icone
                  "Communication-95.png"),
    COMENTAR_FOTO(17, "Comentar ", "comentou", "uma", "sua", "foto", "", Foto.class,
    // executarPosInclusao
                  true,
                  // executarPosAlteracao
                  false,
                  // visivel
                  true,
                  // notificavel
                  true,
                  // notificavelEmail
                  true,
                  // TipoNotificacaoEmail
                  TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_FOTOS,
                  // publicavel
                  false,
                  // publicavelNaHome
                  false,
                  // pontuavel
                  false,
                  // quantidadePontos
                  new BigDecimal(0),
                  // icone
                  "Communication-95.png"),
    COMENTAR_LOCAL(18, "Fazer um comentário", "fez um comentário", "sobre", "", "", "", Local.class,
    // executarPosInclusao
                   true,
                   // executarPosAlteracao
                   false,
                   // visivel
                   true,
                   // notificavel
                   false,
                   // notificavelEmail
                   false,
                   // TipoNotificacaoEmail
                   null,
                   // publicavel
                   true,
                   // publicavelNaHome
                   true,
                   // pontuavel
                   false,
                   // quantidadePontos
                   new BigDecimal(0),
                   // icone
                   "Communication-95.png"),
    COMENTAR_PERFIL(19, "Comentar", "comentou", "no", "em seu", "perfil", "", Usuario.class,
    // executarPosInclusao
                    true,
                    // executarPosAlteracao,
                    false,
                    // visivel
                    true,
                    // notificavel
                    true,
                    // notificavelEmail
                    true,
                    // TipoNotificacaoEmail
                    TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_NO_PERFIL,
                    // publicavel
                    false,
                    // publicavelNaHome
                    false,
                    // pontuavel
                    false,
                    // quantidadePontos
                    new BigDecimal(0),
                    // icone
                    "Communication-95.png"),
    COMENTAR_RELATO_LOCAL(20, "Comentar", "comentou", "um", "seu", "relato", "sobre", RelatoViagem.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          true,
                          // notificavel
                          true,
                          // notificavelEmail
                          true,
                          // TipoNotificacaoEmail
                          TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_RELATOS_DICAS_AVALIACOES,
                          // publicavel
                          false,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          false,
                          // quantidadePontos
                          new BigDecimal(0),
                          // icone
                          "Communication-95.png"),
    /*COMENTAR_VIAGEM(21, "Comentar", "comentou", "na", "sua", "viagem", "de", PlanoViagem.class,
    // executarPosInclusao
                    true,
                    // executarPosAlteracao
                    false,
                    // visivel
                    true,
                    // notificavel
                    true,
                    // notificavelEmail
                    true,
                    // TipoNotificacaoEmail
                    TipoNotificacaoPorEmail.COMENTARIOS_COMENTAR_EM_PLANO_VIAGEM,
                    // publicavel
                    false,
                    // publicavelNaHome
                    false,
                    // pontuavel
                    false,
                    // quantidadePontos
                    new BigDecimal(0),
                    // icone
                    "Communication-95.png"),*/
    COMPARTILHAR_DIARIO_VIAGEM(22, "Compartilhar", "compartilhou", "um", "", "diário de viagem", "", PlanoViagem.class,
    // executarPosInclusao
                               false,
                               // executarPosAlteracao
                               true,
                               // visivel
                               true,
                               // notificavel
                               false,
                               // notificavelEmail
                               false,
                               // TipoNotificacaoEmail
                               null,
                               // publicavel
                               false,
                               // publicavelNaHome
                               false,
                               // pontuavel
                               true,
                               // quantidadePontos
                               new BigDecimal("2"),
                               // icone
                               ""),
    COMPARTILHAR_PLANO_VIAGEM(23, "Compartilhar", "compartilhou", "um", "", "plano de viagem", "", PlanoViagem.class,
    // executarPosInclusao
                              false,
                              // executarPosAlteracao
                              true,
                              // visivel
                              true,
                              // notificavel
                              false,
                              // notificavelEmail
                              false,
                              // TipoNotificacaoEmail
                              null,
                              // publicavel
                              false,
                              // publicavelNaHome
                              false,
                              // pontuavel
                              true,
                              // quantidadePontos
                              new BigDecimal("2"),
                              // icone
                              ""),
    CONECTAR_FACEBOOK(24, "Conectar", "conectou-se", "ao", "", "Facebook", "", null,
    // executarPosInclusao
                      false,
                      // executarPosAlteracao
                      true,
                      // visivel
                      true,
                      // notificavel
                      false,
                      // notificavelEmail
                      false,
                      // TipoNotificacaoEmail
                      null,
                      // publicavel
                      false,
                      // publicavelNaHome
                      false,
                      // pontuavel
                      true,
                      // quantidadePontos
                      new BigDecimal("2"),
                      // icone
                      ""),
    CONECTAR_FOURSQUARE(25, "Conectar", "conectou-se", "ao", "", "Foursquare", "", null,
    // executarPosInclusao
                        false,
                        // executarPosAlteracao
                        true,
                        // visivel
                        true,
                        // notificavel
                        false,
                        // notificavelEmail
                        false,
                        // TipoNotificacaoEmail
                        null,
                        // publicavel
                        false,
                        // publicavelNaHome
                        false,
                        // pontuavel
                        true,
                        // quantidadePontos
                        new BigDecimal("2"),
                        // icone
                        ""),
    CONECTAR_GOOGLE(26, "Conectar-se", "conectou-se", "ao", "", "Google", "", null,
    // executarPosInclusao
                    false,
                    // executarPosAlteracao
                    true,
                    // visivel
                    true,
                    // notificavel
                    false,
                    // notificavelEmail
                    false,
                    // TipoNotificacaoEmail
                    null,
                    // publicavel
                    false,
                    // publicavelNaHome
                    false,
                    // pontuavel
                    true,
                    // quantidadePontos
                    new BigDecimal("2"),
                    // icone
                    ""),
    CONECTAR_TWITTER(27, "Conectar-se", "conectou-se", "ao", "", "Twitter", "", null,
    // executarPosInclusao
                     false,
                     // executarPosAlteracao
                     true,
                     // visivel
                     true,
                     // notificavel
                     false,
                     // notificavelEmail
                     false,
                     // TipoNotificacaoEmail
                     null,
                     // publicavel
                     false,
                     // publicavelNaHome
                     false,
                     // pontuavel
                     true,
                     // quantidadePontos
                     new BigDecimal("2"),
                     // icone
                     ""),
    CONVIDAR_PARA_AMIZADE(28, "Quer", "quer", "", "ser", "seu amigo no TripFans", "", Usuario.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          false,
                          // notificavel
                          true,
                          // notificavelEmail
                          true,
                          // TipoNotificacaoEmail
                          TipoNotificacaoPorEmail.AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_AMIZADE,
                          // publicavel
                          false,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          true,
                          // quantidadePontos
                          new BigDecimal("2"),
                          // icone
                          "Users-13.png"),
    /*CONVIDAR_PARA_GRUPO(29, "Convidar", "convidou", "um amigo para", "você para", "participar do grupo", "", Usuario.class,
    // executarPosInclusao
                        true,
                        // executarPosAlteracao
                        false,
                        // visivel
                        true,
                        // notificavel
                        true,
                        // notificavelEmail
                        true,
                        // TipoNotificacaoEmail
                        TipoNotificacaoPorEmail.AMIGOS_RECEBER_SOLICITACOES_OU_CONFIRMACOES_PARTICIPACAO_EM_GRUPO,
                        // publicavel
                        false,
                        // publicavelNaHome
                        false,
                        // pontuavel
                        true,
                        // quantidadePontos
                        new BigDecimal("2"),
                        // icone
                        ""),*/
    CONVIDAR_PARA_PARTICIPAR_DE_VIAGEM(30, "Convidar", "convidou", "um amigo para", "você para", "participar de uma viagem", "", Usuario.class,
    // executarPosInclusao
                                       true,
                                       // executarPosAlteracao
                                       false,
                                       // visivel
                                       true,
                                       // notificavel
                                       true,
                                       // notificavelEmail
                                       true,
                                       // TipoNotificacaoEmail
                                       TipoNotificacaoPorEmail.VIAGENS_RECEBER_CONVITE_PARA_PARTICIPAR_DE_VIAGEM,
                                       // publicavel
                                       false,
                                       // publicavelNaHome
                                       false,
                                       // pontuavel
                                       true,
                                       // quantidadePontos
                                       new BigDecimal("2"),
                                       // icone
                                       "Users-8.png"),
    CONVIDAR_PARA_RECOMENDAR_SOBRE_LOCAL(31, "Convidar", "convidou", "um amigo para", "você para", "recomendar", "sobre", Usuario.class,
    // executarPosInclusao
                                         true,
                                         // executarPosAlteracao
                                         false,
                                         // visivel
                                         true,
                                         // notificavel
                                         true,
                                         // notificavelEmail
                                         true,
                                         // TipoNotificacaoEmail
                                         TipoNotificacaoPorEmail.VIAGENS_RECEBER_SOLICITACAO_PARA_RECOMENDAR_SOBRE_LUGARES,
                                         // publicavel
                                         false,
                                         // publicavelNaHome
                                         false,
                                         // pontuavel
                                         true,
                                         // quantidadePontos
                                         new BigDecimal("2"),
                                         // icone
                                         ""),
    CONVIDAR_PARA_RECOMENDAR_SOBRE_VIAGEM(32, "Convidar", "convidou", "um amigo para", "você para", "ajudá-lo a montar um Roteiro de Viagem", "", Usuario.class,
    // executarPosInclusao
                                          true,
                                          // executarPosAlteracao
                                          false,
                                          // visivel
                                          true,
                                          // notificavel
                                          true,
                                          // notificavelEmail
                                          true,
                                          // TipoNotificacaoEmail
                                          TipoNotificacaoPorEmail.VIAGENS_RECEBER_SOLICITACAO_PARA_RECOMENDAR_SOBRE_VIAGEM,
                                          // publicavel
                                          false,
                                          // publicavelNaHome
                                          false,
                                          // pontuavel
                                          true,
                                          // quantidadePontos
                                          new BigDecimal("2"),
                                          // icone
                                          "Places-35.png"),
    CONVIDAR_PARA_TRIPFANS(33, "Convidar", "convidou", "", "", "amigos para o TripFans", "", Usuario.class,
    // executarPosInclusao
                           true,
                           // executarPosAlteracao
                           false,
                           // visivel
                           true,
                           // notificavel
                           false,
                           // notificavelEmail
                           false,
                           // TipoNotificacaoEmail
                           null,
                           // publicavel
                           false,
                           // publicavelNaHome
                           false,
                           // pontuavel
                           true,
                           // quantidadePontos
                           new BigDecimal("2"),
                           // icone
                           "Users-13.png"),
    /*
     * Ordem dos parametros:
     * ID, descricao, descricaoNoPassado, preposicaoPosAcao, pronome, descricaoAlvo, preposicaoPosAlvo, classe do objetoAlvo, executarPosInclusao, executarPosAlteracao
     * visivel, notificavel, notificavelEmail, TipoNotificacaoEmail, publicavel, publicavelNaHome, pontuavel, quantidadePontos, icone
     */
    ENTRAR_NO_TRIPFANS(60, "Entrar", "entrou", "no", "", "TripFans", "", null,
    // executarPosInclusao
                       false,
                       // executarPosAlteracao
                       false,
                       // visivel
                       true,
                       // notificavel
                       false,
                       // notificavelEmail
                       false,
                       // TipoNotificacaoEmail
                       null,
                       // publicavel
                       false,
                       // publicavelNaHome
                       false,
                       // pontuavel
                       true,
                       // quantidadePontos
                       new BigDecimal("2"),
                       // icone
                       "tripfans.png"),
    ESCREVER_AVALIACAO(34, "Escrever", "escreveu", "uma", "", "avaliação", "sobre", Local.class,
    // executarPosInclusao
                       true,
                       // executarPosAlteracao
                       false,
                       // visivel
                       true,
                       // notificavel
                       false,
                       // notificavelEmail
                       false,
                       // TipoNotificacaoEmail
                       null,
                       // publicavel
                       true,
                       // publicavelNaHome
                       true,
                       // pontuavel
                       true,
                       // quantidadePontos
                       new BigDecimal("2"),
                       // icone
                       "Edition-35.png"),
    ESCREVER_DICA(35, "Escrever ", "escreveu", "uma", "", "dica", "sobre", Local.class,
    // executarPosInclusao
                  true,
                  // executarPosAlteracao
                  false,
                  // visivel
                  true,
                  // notificavel
                  false,
                  // notificavelEmail
                  false,
                  // TipoNotificacaoEmail
                  null,
                  // publicavel
                  true,
                  // publicavelNaHome,
                  true,
                  // pontuavel
                  true,
                  // quantidadePontos
                  new BigDecimal("2"),
                  // icone
                  "Edition-35.png"),
    ESCREVER_PERGUNTA(36, "Perguntar", "fez", "uma", "", "pergunta", "sobre", Local.class,
    // executarPosInclusao
                      true,
                      // executarPosAlteracao
                      false,
                      // visivel
                      true,
                      // notificavel
                      false,
                      // notificavelEmail
                      false,
                      // TipoNotificacaoEmail
                      null,
                      // publicavel
                      true,
                      // publicavelNaHome
                      true,
                      // pontuavel
                      true,
                      // quantidadePontos
                      new BigDecimal("2"),
                      // icone
                      "Communication-97.png"),
    INDICAR_AVALIACAO_COMO_UTIL(37, "Indicar como útil", "indicou como útil", "a", "sua", "avaliação", "sobre", Avaliacao.class,
    // executarPosInclusao
                                true,
                                // executarPosAlteracao
                                false,
                                // visivel
                                true,
                                // notificavel
                                true,
                                // notificavelEmail
                                true,
                                // TipoNotificacaoEmail
                                TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_INDICAO_UTIL,
                                // publicavel
                                true,
                                // publicavelNaHome
                                true,
                                // pontuavel
                                false,
                                // quantidadePontos
                                new BigDecimal(0),
                                // icone
                                "Vote-27.png"),
    INDICAR_DICA_COMO_UTIL(38, "Indicar como útil", "indicou como útil", "a", "sua", "dica", "sobre", Dica.class,
    // executarPosInclusao
                           true,
                           // executarPosAlteracao
                           false,
                           // visivel
                           true,
                           // notificavel
                           true,
                           // notificavelEmail
                           true,
                           // TipoNotificacaoEmail
                           TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_INDICAO_UTIL,
                           // publicavel
                           true,
                           // publicavelNaHome
                           true,
                           // pontuavel
                           false,
                           // quantidadePontos
                           new BigDecimal(0),
                           // icone
                           "Vote-27.png"),

    INDICAR_RELATO_COMO_UTIL(39, "Indicar como útil", "indicou como útil", "o", "seu", "relato", "sobre", RelatoViagem.class,
    // executarPosInclusao
                             true,
                             // executarPosAlteracao
                             false,
                             // visivel
                             true,
                             // notificavel
                             true,
                             // notificavelEmail
                             true,
                             // TipoNotificacaoEmail
                             TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_INDICAO_UTIL,
                             // publicavel
                             true,
                             // publicavelNaHome
                             true,
                             // pontuavel
                             false,
                             // quantidadePontos
                             new BigDecimal(0),
                             // icone
                             "Vote-27.png"),
    INDICAR_RESPOSTA_COMO_UTIL(40, "Indicar como útil", "indicou como útil", "a", "sua", "resposta", "sobre", Resposta.class,
    // executarPosInclusao
                               true,
                               // executarPosAlteracao
                               false,
                               // visivel
                               true,
                               // notificavel
                               true,
                               // notificavelEmail
                               true,
                               // TipoNotificacaoEmail
                               TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_INDICAO_UTIL,
                               // publicavel
                               true,
                               // publicavelNaHome
                               true,
                               // pontuavel
                               false,
                               // quantidadePontos
                               new BigDecimal(0),
                               // icone
                               "Vote-27.png"),
    MARCAR_LOCAL_DESEJA_IR(41, "Marcar", "marcou", "", "", "novo(s) local(is) que deseja visitar", "", Local.class,
    // Marcou/Adicionou em seu mapa...
    // executarPosInclusao
                           true,
                           // executarPosAlteracao
                           true,
                           // visivel
                           true,
                           // notificavel
                           false,
                           // notificavelEmail
                           false,
                           // TipoNotificacaoEmail
                           null,
                           // publicavel
                           true,
                           // publicavelNaHome
                           false,
                           // pontuavel
                           true,
                           // quantidadePontos
                           new BigDecimal("2"),
                           // icone
                           "Places-32.png"),

    MARCAR_LOCAL_FAVORITO(42, "Marcar", "marcou", "", "", "local(is) como favorito", "", Local.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          true,
                          // visivel
                          true,
                          // notificavel
                          false,
                          // notificavelEmail
                          false,
                          // TipoNotificacaoEmail
                          null,
                          // publicavel
                          true,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          true,
                          // quantidadePontos
                          new BigDecimal("0.1"),
                          // icone
                          "Places-32.png"),
    MARCAR_LOCAL_IMPERDIVEL(43, "Marcar", "marcou", "", "", "local(is) como imperdível", "", Local.class,
    // executarPosInclusao
                            true,
                            // executarPosAlteracao
                            true,
                            // visivel
                            true,
                            // notificavel
                            false,
                            // notificavelEmail
                            false,
                            // TipoNotificacaoEmail
                            null,
                            // publicavel
                            true,
                            // publicavelNaHome
                            false,
                            // pontuavel
                            true,
                            // quantidadePontos
                            new BigDecimal("2"),
                            // icone
                            "Places-32.png"),
    MARCAR_LOCAL_INDICA(44, "Indicar", "indicou", "", "", "novo(s) local(is)", "", Local.class,
    // executarPosInclusao
                        true,
                        // executarPosAlteracao
                        true,
                        // visivel
                        true,
                        // notificavel
                        false,
                        // notificavelEmail
                        false,
                        // TipoNotificacaoEmail
                        null,
                        // publicavel
                        true,
                        // publicavelNaHome
                        false,
                        // pontuavel
                        true,
                        // quantidadePontos
                        new BigDecimal("2"),
                        // icone
                        "Places-32.png"),
    MARCAR_LOCAL_JA_FOI(45, "Marcar", "marcou", "", "", "novo(s) local(is) que já visitou", "", Local.class,
    // executarPosInclusao
                        true,
                        // executarPosAlteracao
                        true,
                        // visivel
                        true,
                        // notificavel
                        false,
                        // notificavelEmail
                        false,
                        // TipoNotificacaoEmail
                        null,
                        // publicavel
                        true,
                        // publicavelNaHome
                        false,
                        // pontuavel
                        true,
                        // quantidadePontos
                        new BigDecimal("2"),
                        // icone
                        "Places-32.png"),
    MARCAR_LOCAL_MOROU(46, "Informar que já morou", "informou que já morou", "em", "", "", "", Local.class,
    // executarPosInclusao
                       true,
                       // executarPosAlteracao
                       true,
                       // visivel
                       true,
                       // notificavel
                       false,
                       // notificavelEmail
                       false,
                       // TipoNotificacaoEmail
                       null,
                       // publicavel
                       true,
                       // publicavelNaHome
                       false,
                       // pontuavel
                       true,
                       // quantidadePontos
                       new BigDecimal("2"),
                       // icone
                       ""),
    PEDIR_DICA(61, "Pedir dica", "te pediu", "uma", "", "dicas de viagem", "sobre", Local.class,
    // executarPosInclusao
               true,
               // executarPosAlteracao
               false,
               // visivel
               true,
               // notificavel
               true,
               // notificavelEmail
               true,
               // TipoNotificacaoEmail
               TipoNotificacaoPorEmail.DICA_RECEBER_SOLICITACAO_PEDIDO_DICA,
               // publicavel
               true,
               // publicavelNaHome
               false,
               // pontuavel
               false,
               // quantidadePontos
               new BigDecimal(0),
               // icone
               "Communication-97.png"),
    PUBLICAR_DIARIO_VIAGEM(47, "Publicar", "publicou", "um", "", "diário de viagem", "", PlanoViagem.class,
    // executarPosInclusao
                           false,
                           // executarPosAlteracao
                           true,
                           // visivel
                           true,
                           // notificavel
                           false,
                           // notificavelEmail
                           false,
                           // TipoNotificacaoEmail
                           null,
                           // publicavel
                           true,
                           // publicavelNaHome
                           true,
                           // pontuavel
                           true,
                           // quantidadePontos
                           new BigDecimal("0.1"),
                           // icone
                           "Content-13.png"),
    PUBLICAR_PLANO_VIAGEM(48, "Publicar", "publicou", "um", "", "plano de viagem", "", Viagem.class,
    // executarPosInclusao
                          true,
                          // executarPosAlteracao
                          false,
                          // visivel
                          true,
                          // notificavel
                          false,
                          // notificavelEmail
                          false,
                          // TipoNotificacaoEmail
                          null,
                          // publicavel
                          true,
                          // publicavelNaHome
                          true,
                          // pontuavel
                          true,
                          // quantidadePontos
                          new BigDecimal("0.1"),
                          // icone
                          "Leisure-1.png"),
    RECEBER_INDICACAO_AVALIACAO_UTIL(49, "Receber uma indicação de útil", "recebeu uma indicação de útil", "em sua", "em sua", "avaliação", "sobre",
                                     Avaliacao.class,
                                     // executarPosInclusao
                                     true,
                                     // executarPosAlteracao
                                     false,
                                     // visivel
                                     true,
                                     // notificavel
                                     false,
                                     // notificavelEmail
                                     false,
                                     // TipoNotificacaoEmail
                                     null, false, false, true, new BigDecimal("0.1"),
                                     // icone
                                     "Vote-27.png"),
    RECEBER_INDICACAO_DICA_UTIL(50, "Receber uma indicação de útil", "recebeu uma indicação de útil", "em sua", "em sua", "dica", "sobre",
                                Dica.class,
                                // executarPosInclusao
                                true,
                                // executarPosAlteracao
                                false,
                                // visivel
                                true,
                                // notificavel
                                false,
                                // notificavelEmail
                                false,
                                // TipoNotificacaoEmail
                                null,
                                // publicavel
                                false,
                                // publicavelNaHome
                                false,
                                // pontuavel
                                true,
                                // quantidadePontos
                                new BigDecimal("0.1"),
                                // icone
                                "Vote-27.png"),
    RECEBER_INDICACAO_RELATO_UTIL(51, "Receber uma indicação de útil", "recebeu uma indicação de útil", "em seu", "em seu", "relato", "sobre",
                                  RelatoViagem.class,
                                  // executarPosInclusao
                                  true,
                                  // executarPosAlteracao
                                  false,
                                  // visivel
                                  true,
                                  // notificavel
                                  false,
                                  // notificavelEmail
                                  false,
                                  // TipoNotificacaoEmail
                                  null,
                                  // publicavel
                                  false,
                                  // publicavelNaHome
                                  false,
                                  // pontuavel
                                  true,
                                  // quantidadePontos
                                  new BigDecimal("0.1"),
                                  // icone
                                  "Vote-27.png"),
    RECEBER_INDICACAO_RESPOSTA_UTIL(52, "Receber uma indicação de útil", "recebeu uma indicação de útil", "em sua", "em sua", "resposta", "sobre",
                                    Resposta.class,
                                    // executarPosInclusao
                                    true,
                                    // executarPosAlteracao
                                    false,
                                    // visivel
                                    true,
                                    // notificavel
                                    false,
                                    // notificavelEmail
                                    false,
                                    // TipoNotificacaoEmail
                                    null,
                                    // publicavel
                                    false,
                                    // publicavelNaHome
                                    false,
                                    // pontuavel
                                    true,
                                    // quantidadePontos
                                    new BigDecimal("0.1"),
                                    // icone
                                    "Vote-27.png"),
    RECOMENDAR_LOCAL_PARA_AMIGO(53, "Recomendar", "recomendou", "um", "um", "local", "para", null,
    // executarPosInclusao
                                true,
                                // executarPosAlteracao
                                false,
                                // visivel
                                true,
                                // notificavel
                                true,
                                // notificavelEmail
                                true,
                                // TipoNotificacaoEmail
                                TipoNotificacaoPorEmail.VIAGENS_RECEBER_RECOMENDACAO_SOBRE_LUGARES,
                                // publicavel
                                false,
                                // publicavelNaHome
                                false,
                                // pontuavel
                                true,
                                // quantidadePontos
                                new BigDecimal("0.1"),
                                // icone
                                ""),
    RESPONDER_COMENTARIO(54, "Responder", "respondeu", "o", "seu", "comentário", "", Comentario.class,
    // executarPosInclusao
                         true,
                         // executarPosAlteracao
                         false,
                         // visivel
                         true,
                         // notificavel
                         true,
                         // notificavelEmail
                         true,
                         // TipoNotificacaoEmail
                         TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_RESPOSTAS_PERGUNTAS_OU_COMENTARIOS,
                         // publicavel
                         false,
                         // publicavelNaHome
                         false,
                         // pontuavel
                         true,
                         // quantidadePontos
                         new BigDecimal("0.1"),
                         // icone
                         "Communication-96.png"),
    RESPONDER_PEDIDO_DICA(62, "Responder pedido dica", "respondeu", "o seu", "", "seu pedido de dica", "sobre", Local.class,
    // executarPosInclusao
                          false,
                          // executarPosAlteracao
                          true,
                          // visivel
                          true,
                          // notificavel
                          true,
                          // notificavelEmail
                          true,
                          // TipoNotificacaoEmail
                          TipoNotificacaoPorEmail.DICA_RECEBER_RESPOSTA_PEDIDO_DICA,
                          // publicavel
                          true,
                          // publicavelNaHome
                          false,
                          // pontuavel
                          false,
                          // quantidadePontos
                          new BigDecimal(0),
                          // icone
                          "Communication-97.png"),
    RESPONDER_PERGUNTA(55, "Responder", "respondeu", "uma", "sua", "pergunta", "sobre", Pergunta.class,
    // executarPosInclusao
                       true,
                       // executarPosAlteracao
                       false,
                       // visivel
                       true,
                       // notificavel
                       true,
                       // notificavelEmail
                       true,
                       // TipoNotificacaoEmail
                       TipoNotificacaoPorEmail.COMENTARIOS_RECEBER_RESPOSTAS_PERGUNTAS_OU_COMENTARIOS,
                       // publicavel
                       true,
                       // publicavelNaHome
                       true,
                       // pontuavel
                       true,
                       // quantidadePontos
                       new BigDecimal("2"),
                       // icone
                       "Communication-98.png"),
    SEGUIR_LOCAL(56, "Seguir", "seguiu", "", "", "", "", null,
    // executarPosInclusao
                 true,
                 // executarPosAlteracao
                 false,
                 // visivel
                 true,
                 // notificavel
                 false,
                 // notificavelEmail
                 false,
                 // TipoNotificacaoEmail
                 null,
                 // publicavel
                 true,
                 // publicavelNaHome
                 true,
                 // pontuavel
                 true,
                 // quantidadePontos
                 new BigDecimal("2"),
                 // icone
                 "");

    public static List<TipoAcao> getTiposNotificaveisEmail() {
        final List<TipoAcao> lista = new ArrayList<TipoAcao>();
        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isNotificavelEmail()) {
                lista.add(tipoAcao);
            }
        }
        return lista;
    }

    public static void main(final String[] s) {

        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            System.out.println(tipoAcao.name() + ",-,-," + tipoAcao.notificavel + "," + tipoAcao.notificavelEmail + ",-,-," + tipoAcao.publicavel
                    + "," + tipoAcao.publicavelNaHome + ",-," + tipoAcao.pontuavel);
        }

        System.out.println("\n###### ATIVIDADES PRIVADAS (Vistas no perfil do usuario) ######\n");
        /*for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isVisivel() && !tipoAcao.isPublicavel()) {
                System.out.println(tipoAcao.name() + " : " + "Fulano " + tipoAcao.descricaoAcaoNoPassado + " " + tipoAcao.preposicaoPosAcao + " "
                        + tipoAcao.descricaoAlvo + " " + tipoAcao.preposicaoPosAlvo + " <nome-do-objeto> ");
            }
        }

        System.out.println("\n###### ATIVIDADES PUBLICAVEIS EM NOTICIAS ######\n");
        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isVisivel() && tipoAcao.isPublicavel()) {
                System.out.println(tipoAcao.name() + " : " + "Fulano " + tipoAcao.descricaoAcaoNoPassado + " " + tipoAcao.preposicaoPosAcao + " "
                        + tipoAcao.descricaoAlvo + " " + tipoAcao.preposicaoPosAlvo + " <nome-do-objeto> ");
            }
        }

        System.out.println("\n###### ATIVIDADES PUBLICAVEIS NA HOME ######\n");
        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isVisivel() && tipoAcao.isPublicavelNaHome()) {
                System.out.println(tipoAcao.name() + " : " + "Fulano " + tipoAcao.descricaoAcaoNoPassado + " " + tipoAcao.preposicaoPosAcao + " "
                        + tipoAcao.descricaoAlvo + " " + tipoAcao.preposicaoPosAlvo + " <nome-do-objeto> ");
            }
        }

        System.out.println("\n###### ATIVIDADES NOTIFICAVEIS ######\n");
        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isNotificavel()) {
                System.out.println(tipoAcao.name() + " : " + "Fulano " + tipoAcao.descricaoAcaoNoPassado + " " + tipoAcao.pronome + " "
                        + tipoAcao.descricaoAlvo);
            }
        }

        System.out.println("\n###### ATIVIDADES PONTUAVEIS ######\n");
        for (final TipoAcao tipoAcao : TipoAcao.values()) {
            if (tipoAcao.isPontuavel()) {
                System.out.println(tipoAcao.name() + " : " + tipoAcao.descricaoAcao + " " + tipoAcao.descricaoAlvo);
            }
        }*/
    }

    private final Class<? extends AlvoAcao> classeDoAlvo;

    private final Integer codigo;

    private final String descricaoAcao;

    private final String descricaoAcaoNoPassado;

    private final String descricaoAlvo;

    private final boolean executarPosAlteracao;

    private final boolean executarPosInclusao;

    private final String icone;

    private final boolean notificavel;

    private final boolean notificavelEmail;

    private final BigDecimal pontos;

    private final boolean pontuavel;

    private final String preposicaoPosAcao;

    private final String preposicaoPosAlvo;

    private final String pronome;

    private final boolean publicavel;

    private final boolean publicavelNaHome;

    private final TipoNotificacaoPorEmail tipoNotificacaoEmail;

    private final boolean visivel;

    private TipoAcao(final Integer codigo, final String descricaoAcao, final String descricaoAcaoNoPassado, final String preposicaoPosAcao,
            final String pronome, final String descricaoAlvo, final String preposicaoPosAlvo, final Class<? extends AlvoAcao> classeDoAlvo,
            final boolean executarPosInclusao, final boolean executarPosAlteracao, final boolean visivel, final boolean notificavel,
            final boolean notificavelEmail, final TipoNotificacaoPorEmail tipoNotificacaoEmail, final boolean publicavel,
            final boolean publicavelNaHome, final boolean pontuavel, final BigDecimal pontos, final String icone) {
        this.codigo = codigo;
        this.classeDoAlvo = classeDoAlvo;
        this.descricaoAcao = descricaoAcao;
        this.pontos = pontos;
        this.descricaoAcaoNoPassado = descricaoAcaoNoPassado;
        this.preposicaoPosAcao = preposicaoPosAcao;
        this.pronome = pronome;
        this.descricaoAlvo = descricaoAlvo;
        this.preposicaoPosAlvo = preposicaoPosAlvo;
        this.executarPosAlteracao = executarPosAlteracao;
        this.executarPosInclusao = executarPosInclusao;
        this.notificavel = notificavel;
        this.notificavelEmail = notificavelEmail;
        this.tipoNotificacaoEmail = tipoNotificacaoEmail;
        this.publicavel = publicavel;
        this.publicavelNaHome = publicavelNaHome;
        this.pontuavel = pontuavel;
        this.visivel = visivel;
        this.icone = icone;
    }

    public Class<? extends AlvoAcao> getClasseDoAlvo() {
        return this.classeDoAlvo;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricaoAcao;
    }

    public String getDescricaoAcao() {
        return this.descricaoAcao;
    }

    public String getDescricaoAcaoNoPassado() {
        return this.descricaoAcaoNoPassado;
    }

    public String getDescricaoAlvo() {
        return this.descricaoAlvo;
    }

    public String getIcone() {
        return this.icone;
    }

    public BigDecimal getPontos() {
        return this.pontos;
    }

    public String getPreposicaoPosAcao() {
        return this.preposicaoPosAcao;
    }

    public String getPreposicaoPosAlvo() {
        return this.preposicaoPosAlvo;
    }

    public String getPronome() {
        return this.pronome;
    }

    public TipoNotificacaoPorEmail getTipoNotificacaoEmail() {
        return this.tipoNotificacaoEmail;
    }

    public boolean isExecutarPosAlteracao() {
        return this.executarPosAlteracao;
    }

    public boolean isExecutarPosInclusao() {
        return this.executarPosInclusao;
    }

    public boolean isNotificavel() {
        return this.notificavel;
    }

    public boolean isNotificavelEmail() {
        return this.notificavelEmail;
    }

    public boolean isPontuavel() {
        return this.pontuavel;
    }

    public boolean isPublicavel() {
        return this.publicavel;
    }

    public boolean isPublicavelNaHome() {
        return this.publicavelNaHome;
    }

    public boolean isVisivel() {
        return this.visivel;
    }
}