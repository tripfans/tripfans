package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FetchType;
import javax.persistence.FieldResult;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_interesse_usuario_em_local")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_interesse_usuario_em_local", allocationSize = 1)
@Table(name = "interesse_usuario_em_local", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_local_de_interesse", "id_usuario_interessado" }) })
@SqlResultSetMapping(name = "InteresseUsuarioLocalView", entities = { @EntityResult(entityClass = InteresseUsuarioLocalView.class, fields = {
        @FieldResult(name = "idUsuario", column = "id_usuario"), @FieldResult(name = "quantidadeDesejaIr", column = "quantidade_deseja_ir"),
        @FieldResult(name = "quantidadeFavorito", column = "quantidade_favorito"),
        @FieldResult(name = "quantidadeImperdivel", column = "quantidade_imperdivel"),
        @FieldResult(name = "quantidadeIndica", column = "quantidade_indica"), @FieldResult(name = "quantidadeJaFoi", column = "quantidade_ja_foi") }) })
public class InteresseUsuarioEmLocal extends EntidadeGeradoraAcao<Long> {

    private static final long serialVersionUID = 4799826288269604578L;

    @Transient
    // TODO Gato para funcionar a consulta "InteresseUsuarioEmLocal.consultarInteressesUsuarioEmCidadesPorPerimetro"
    // Ver outra forma de resolver isso - retirar os metodos get e set;
    private Cidade cidade;

    @Column(name = "interesse_desejar_ir", nullable = false)
    private Boolean desejaIr = Boolean.FALSE;

    @Column(name = "anterior_desejar_ir", nullable = true)
    private Boolean desejaIrAnterior = Boolean.FALSE;

    @Column(name = "interesse_escreveu_avaliacao", nullable = false)
    private Boolean escreveuAvaliacao = Boolean.FALSE;

    @Column(name = "interesse_escreveu_dica", nullable = false)
    private Boolean escreveuDica = Boolean.FALSE;

    @Column(name = "interesse_escreveu_pergunta", nullable = false)
    private Boolean escreveuPergunta = Boolean.FALSE;

    @Column(name = "interesse_local_favorito", nullable = false)
    private Boolean favorito = Boolean.FALSE;

    @Column(name = "anterior_local_favorito", nullable = true)
    private Boolean favoritoAnterior = Boolean.FALSE;

    @Column(name = "id_usuario_facebook", nullable = true)
    private String idUsuarioFacebook;

    @Column(name = "interesse_imperdivel", nullable = false)
    private Boolean imperdivel = Boolean.FALSE;

    @Column(name = "anterior_imperdivel", nullable = true)
    private Boolean imperdivelAnterior = Boolean.FALSE;

    @Column(name = "importado_facebook", insertable = false, updatable = false)
    private Boolean importadoFacebook;

    @Column(name = "interesse_indica", nullable = false)
    private Boolean indica = Boolean.FALSE;

    @Column(name = "anterior_indica", nullable = true)
    private Boolean indicaAnterior = Boolean.FALSE;

    @Column(name = "interesse_ja_foi", nullable = false)
    private Boolean jaFoi = Boolean.FALSE;

    @Column(name = "anterior_ja_foi", nullable = true)
    private Boolean jaFoiAnterior = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_de_interesse", nullable = true)
    private Local local;

    @Column(name = "nome_usuario")
    private String nomeUsuario;

    @Column(name = "interesse_respondeu_pergunta", nullable = false)
    private Boolean respondeuPergunta = Boolean.FALSE;

    @Column(name = "interesse_seguir", nullable = false)
    private Boolean seguir = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_interessado", nullable = true)
    private Usuario usuario;

    public InteresseUsuarioEmLocal() {
        super();
    }

    public InteresseUsuarioEmLocal(final Local localDeInteresse, final Usuario usuarioInteressado, final String idUsuarioFacebook,
            final String nomeUsuarioFacebook) {
        this.local = localDeInteresse;
        if (idUsuarioFacebook != null) {
            this.idUsuarioFacebook = idUsuarioFacebook;
            this.nomeUsuario = nomeUsuarioFacebook;
        }
        if (usuarioInteressado != null) {
            this.usuario = usuarioInteressado;
            if (usuarioInteressado.getIdFacebook() != null) {
                this.idUsuarioFacebook = usuarioInteressado.getIdFacebook();
                this.nomeUsuario = usuarioInteressado.getDisplayName();
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) getLocal();
    }

    @Override
    public Usuario getAutor() {
        return getUsuario();
    }

    public Cidade getCidade() {
        if (getLocal() != null && getLocal() instanceof Cidade) {
            this.cidade = (Cidade) getLocal();
            return this.cidade;
        }
        return this.cidade;
    }

    public Boolean getDesejaIr() {
        return this.desejaIr;
    }

    public Boolean getDesejaIrAnterior() {
        return this.desejaIrAnterior;
    }

    public Boolean getEscreveuAvaliacao() {
        return this.escreveuAvaliacao;
    }

    public Boolean getEscreveuDica() {
        return this.escreveuDica;
    }

    public Boolean getEscreveuPergunta() {
        return this.escreveuPergunta;
    }

    public Boolean getFavorito() {
        return this.favorito;
    }

    public Boolean getFavoritoAnterior() {
        return this.favoritoAnterior;
    }

    public String getIdUsuarioFacebook() {
        return this.idUsuarioFacebook;
    }

    public Boolean getImperdivel() {
        return this.imperdivel;
    }

    public Boolean getImperdivelAnterior() {
        return this.imperdivelAnterior;
    }

    public Boolean getImportadoFacebook() {
        return this.importadoFacebook;
    }

    public Boolean getIndica() {
        return this.indica;
    }

    public Boolean getIndicaAnterior() {
        return this.indicaAnterior;
    }

    public Boolean getJaFoi() {
        return this.jaFoi;
    }

    public Boolean getJaFoiAnterior() {
        return this.jaFoiAnterior;
    }

    public Local getLocal() {
        return this.local;
    }

    public boolean getMarcouInteresseDesejaIr() {
        return this.getDesejaIr() && !this.getDesejaIrAnterior().equals(this.getDesejaIr());
    }

    public boolean getMarcouInteresseFavorito() {
        return this.getFavorito() && !this.getFavoritoAnterior().equals(this.getFavorito());
    }

    public boolean getMarcouInteresseImperdivel() {
        return this.getImperdivel() && !this.getImperdivelAnterior().equals(this.getImperdivel());
    }

    public boolean getMarcouInteresseIndica() {
        return this.getIndica() && !this.getIndicaAnterior().equals(this.getIndica());
    }

    public boolean getMarcouInteresseJaFoi() {
        return this.getJaFoi() && !this.getJaFoiAnterior().equals(this.getJaFoi());
    }

    public String getNomeUsuario() {
        return this.nomeUsuario;
    }

    public Boolean getRespondeuPergunta() {
        return this.respondeuPergunta;
    }

    public Boolean getSeguir() {
        return this.seguir;
    }

    public List<TipoInteresseUsuarioEmLocal> getTiposInteresseNaoSelecionadosIrFoiLocal() {
        final ArrayList<TipoInteresseUsuarioEmLocal> tipos = new ArrayList<TipoInteresseUsuarioEmLocal>();

        if (!this.desejaIr) {
            tipos.add(TipoInteresseUsuarioEmLocal.DESEJA_IR);
        }
        if (!this.jaFoi) {
            tipos.add(TipoInteresseUsuarioEmLocal.JA_FOI);
        }

        return tipos;
    }

    public List<TipoInteresseUsuarioEmLocal> getTiposInteresseNaoSelecionadosOpiniao() {
        final ArrayList<TipoInteresseUsuarioEmLocal> tipos = new ArrayList<TipoInteresseUsuarioEmLocal>();

        if (!this.favorito) {
            tipos.add(TipoInteresseUsuarioEmLocal.FAVORITO);
        }
        if (!this.imperdivel) {
            tipos.add(TipoInteresseUsuarioEmLocal.IMPERDIVEL);
        }
        if (!this.indica) {
            tipos.add(TipoInteresseUsuarioEmLocal.INDICA);
        }

        return tipos;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public boolean semNenhumInteresse() {
        return !(verificarInteresse(this.desejaIr) || verificarInteresse(this.jaFoi) || verificarInteresse(this.favorito)
                || verificarInteresse(this.indica) || verificarInteresse(this.escreveuAvaliacao) || verificarInteresse(this.escreveuDica)
                || verificarInteresse(this.escreveuPergunta) || verificarInteresse(this.respondeuPergunta));
    }

    public void setCidade(final Cidade cidade) {
        this.cidade = cidade;
        setLocal(cidade);
    }

    public void setDesejaIr(final Boolean desejaIr) {
        this.desejaIr = desejaIr;
    }

    public void setDesejaIrAnterior(final Boolean desejaIrAnterior) {
        this.desejaIrAnterior = desejaIrAnterior;
    }

    public void setEscreveuAvaliacao(final Boolean escreveuAvaliacao) {
        this.escreveuAvaliacao = escreveuAvaliacao;
    }

    public void setEscreveuDica(final Boolean escreveuDica) {
        this.escreveuDica = escreveuDica;
    }

    public void setEscreveuPergunta(final Boolean escreveuPergunta) {
        this.escreveuPergunta = escreveuPergunta;
    }

    public void setFavorito(final Boolean favorito) {
        this.favorito = favorito;
    }

    public void setFavoritoAnterior(final Boolean favoritoAnterior) {
        this.favoritoAnterior = favoritoAnterior;
    }

    public void setIdUsuarioFacebook(final String idUsuarioFacebook) {
        this.idUsuarioFacebook = idUsuarioFacebook;
    }

    public void setImperdivel(final Boolean imperdivel) {
        this.imperdivel = imperdivel;
    }

    public void setImperdivelAnterior(final Boolean imperdivelAnterior) {
        this.imperdivelAnterior = imperdivelAnterior;
    }

    public void setImportadoFacebook(final Boolean importadoFacebook) {
        this.importadoFacebook = importadoFacebook;
    }

    public void setIndica(final Boolean indica) {
        this.indica = indica;
    }

    public void setIndicaAnterior(final Boolean indicaAnterior) {
        this.indicaAnterior = indicaAnterior;
    }

    public void setInteresse(final TipoInteresseUsuarioEmLocal tipo, final Boolean valor) {
        if (tipo == TipoInteresseUsuarioEmLocal.DESEJA_IR) {
            setDesejaIr(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.ESCREVEU_AVALIACAO) {
            setEscreveuAvaliacao(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.ESCREVEU_DICA) {
            setEscreveuDica(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.ESCREVEU_PERGUNTA) {
            setEscreveuPergunta(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.FAVORITO) {
            setFavorito(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.IMPERDIVEL) {
            setImperdivel(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.INDICA) {
            setIndica(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.JA_FOI) {
            setJaFoi(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.RESPONDEU_PERGUNTA) {
            setRespondeuPergunta(valor);
        }
        if (tipo == TipoInteresseUsuarioEmLocal.SEGUIR) {
            setSeguir(valor);
        }
    }

    public void setJaFoi(final Boolean jaFoi) {
        this.jaFoi = jaFoi;
    }

    public void setJaFoiAnterior(final Boolean jaFoiAnterior) {
        this.jaFoiAnterior = jaFoiAnterior;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setNomeUsuario(final String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public void setRespondeuPergunta(final Boolean respondeuPergunta) {
        this.respondeuPergunta = respondeuPergunta;
    }

    public void setSeguir(final Boolean seguir) {
        this.seguir = seguir;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

    private boolean verificarInteresse(final Boolean interesse) {
        return (interesse != null && interesse);
    }
}