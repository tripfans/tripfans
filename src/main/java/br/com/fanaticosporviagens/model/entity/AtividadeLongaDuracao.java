package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.joda.time.Period;

/**
 * Classe que representa Atividades que durem mais de um dia
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
public abstract class AtividadeLongaDuracao extends AtividadePlano {

    private static final long serialVersionUID = 5503045005233116509L;

    @Column(name = "dia_fim")
    private Integer diaFim;

    /**
     * Indica se o fim da atividade será no mesmo local do início
     */
    @Column(name = "fim_mesmo_local")
    private Boolean fimMesmoLocal = Boolean.TRUE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_fim", nullable = true)
    private Local localFim;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "nome", column = @Column(name = "nome_local_fim")),
            @AttributeOverride(name = "descricao", column = @Column(name = "descricao_local_fim")),
            @AttributeOverride(name = "endereco.descricao", column = @Column(name = "endereco_local_fim")),
            @AttributeOverride(name = "endereco.bairro", column = @Column(name = "bairro_local_fim")),
            @AttributeOverride(name = "endereco.cep", column = @Column(name = "cep_local_fim")) })
    private final LocalEnderecavelNaoCadastradoAtividade localFimNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    @Column(name = "quantidade_dias")
    private Integer quantidadeDias;

    public abstract String getDescricaoFim();

    public abstract String getDescricaoInicio();

    public Integer getDiaFim() {
        return this.diaFim;
    }

    public String getEnderecoLocalFim() {
        // final Local local = initializeAndUnproxy(getLocal());
        if (this.localFim != null && this.localFim.getEndereco() != null) {
            return this.localFim.getEndereco().getDescricao();
        } else if (getLocalFimNaoCadastrado() != null) {
            return this.localFimNaoCadastrado.getEndereco().getDescricao();
        }
        return null;
    }

    public Boolean getFimMesmoLocal() {
        return this.fimMesmoLocal;
    }

    public Local getLocalFim() {
        return this.localFim;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalFimNaoCadastrado() {
        return this.localFimNaoCadastrado;
    }

    public Integer getQuantidadeDias() {
        if (getDiaInicio() != null && getDiaFim() != null) {
            final Period periodo = new Period(getDiaInicio(), getDiaFim());
            return periodo.getDays() + 1;
        }
        return this.quantidadeDias;
    }

    public void setDiaFim(final Integer diaFim) {
        this.diaFim = diaFim;
    }

    public void setFimMesmoLocal(final Boolean fimMesmoLocal) {
        this.fimMesmoLocal = fimMesmoLocal;
    }

    public void setLocalFim(final Local localFim) {
        this.localFim = localFim;
    }

    public void setQuantidadeDias(final Integer quantidadeDias) {
        this.quantidadeDias = quantidadeDias;
    }

}
