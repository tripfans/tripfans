package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Guarda todas as marcações (Já Foi, Deseja Ir, Favorito, Seguir) do usuário em locais.
 * 
 * @author André Thiago
 * 
 */
public class MarcacoesUsuarioLocal implements Serializable {

    private static final long serialVersionUID = 2333199534601721010L;

    private final Set<Long> desejaIr = new HashSet<Long>();

    private final Set<Long> favorito = new HashSet<Long>();

    private final boolean inicializado = false;

    private final Set<Long> jaFoi = new HashSet<Long>();

    private final Set<Long> seguir = new HashSet<Long>();

    public void atualizaMarcacao(final InteresseUsuarioEmLocal interesse) {
        final Long idLocal = interesse.getLocal().getId();
        if (interesse.getDesejaIr()) {
            this.desejaIr.add(idLocal);
        } else {
            this.desejaIr.remove(idLocal);
        }
        if (interesse.getFavorito()) {
            this.favorito.add(idLocal);
        } else {
            this.favorito.remove(idLocal);
        }
        if (interesse.getJaFoi()) {
            this.jaFoi.add(idLocal);
        } else {
            this.jaFoi.remove(idLocal);
        }
        if (interesse.getSeguir()) {
            this.seguir.add(idLocal);
        } else {
            this.seguir.remove(idLocal);
        }
    }

    public Set<Long> getDesejaIr() {
        return this.desejaIr;
    }

    public Set<Long> getFavorito() {
        return this.favorito;
    }

    public Set<Long> getJaFoi() {
        return this.jaFoi;
    }

    public Set<Long> getSeguir() {
        return this.seguir;
    }

    // @Async
    public void inicializar(final List<InteresseUsuarioEmLocal> interesses) {
        for (final InteresseUsuarioEmLocal interesse : interesses) {
            atualizaMarcacao(interesse);
        }
    }

    public boolean isInicializado() {
        return this.inicializado;
    }

}
