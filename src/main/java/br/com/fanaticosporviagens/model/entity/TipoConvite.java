package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * @author André Thiago
 *
 */
public enum TipoConvite implements EnumTypeInteger {

    AMIZADE(1, "Amizade"),
    ENTRAR_TRIPFANS(6, "Entrar no TripFans"),
    PARTICIPACAO_GRUPO(2, "Participação Grupo"),
    PARTICIPACAO_VIAGEM(3, "Participação em Viagem"),
    RECOMENDAR_SOBRE_LOCAL(5, "Recomendar sobre Local"),
    RECOMENDAR_SOBRE_VIAGEM(4, "Recomendar sobre Viagem");

    private final EnumI18nUtil util;

    TipoConvite(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}
