package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;

public class InteresseAmigosEmLocal implements Serializable {

    private static final long serialVersionUID = 5147811547457542673L;

    private List<InteresseUsuarioEmLocal> amostraInteresses;

    private String idUsuarioAmigoFacebook;

    private Local localDeInteresse;

    private Long quantidadeAmigos;

    private TipoInteresseUsuarioEmLocal tipo;

    private Usuario usuarioAmigo;

    public InteresseAmigosEmLocal() {
        super();
    }

    public InteresseAmigosEmLocal(final Local localDeInteresse, final String idUsuarioAmigoFacebook, final TipoInteresseUsuarioEmLocal tipo) {
        super();
        this.localDeInteresse = localDeInteresse;
        this.tipo = tipo;
        this.idUsuarioAmigoFacebook = idUsuarioAmigoFacebook;
    }

    public InteresseAmigosEmLocal(final Local localDeInteresse, final Usuario usuarioAmigo, final TipoInteresseUsuarioEmLocal tipo) {
        super();
        this.localDeInteresse = localDeInteresse;
        this.tipo = tipo;
        this.usuarioAmigo = usuarioAmigo;
        this.amostraInteresses = new ArrayList<InteresseUsuarioEmLocal>();
    }

    public List<InteresseUsuarioEmLocal> getAmostraInteresses() {
        return this.amostraInteresses;
    }

    public String getIdUsuarioAmigoFacebook() {
        return this.idUsuarioAmigoFacebook;
    }

    public Local getLocalDeInteresse() {
        return this.localDeInteresse;
    }

    public Long getQuantidadeAmigos() {
        return this.quantidadeAmigos;
    }

    public TipoInteresseUsuarioEmLocal getTipo() {
        return this.tipo;
    }

    public String getTitulo() {
        return this.tipo.getTituloInteresseAmigos();
    }

    public Usuario getUsuarioAmigo() {
        return this.usuarioAmigo;
    }

    public boolean isAmostraMenorQueQuantidade() {
        return this.amostraInteresses.size() < this.quantidadeAmigos;
    }

    public void setAmostraInteresses(final List<InteresseUsuarioEmLocal> amostraInteresses) {
        this.amostraInteresses = amostraInteresses;
    }

    public void setIdUsuarioAmigoFacebook(final String idUsuarioAmigoFacebook) {
        this.idUsuarioAmigoFacebook = idUsuarioAmigoFacebook;
    }

    public void setLocalDeInteresse(final Local localDeInteresse) {
        this.localDeInteresse = localDeInteresse;
    }

    public void setQuantidadeAmigos(final Long quantidadeAmigos) {
        this.quantidadeAmigos = quantidadeAmigos;
    }

    public void setTipo(final TipoInteresseUsuarioEmLocal tipo) {
        this.tipo = tipo;
    }

    public void setUsuarioAmigo(final Usuario usuarioAmigo) {
        this.usuarioAmigo = usuarioAmigo;
    }

    public List<InteresseUsuarioEmLocal> sorteia(final int quantidade) {
        if (CollectionUtils.isEmpty(this.amostraInteresses) || this.amostraInteresses.size() < quantidade) {
            return this.amostraInteresses;
        }
        final Random random = new Random();
        final int max = this.amostraInteresses.size();
        final List<InteresseUsuarioEmLocal> sorteados = new ArrayList<InteresseUsuarioEmLocal>(quantidade);
        for (int i = 0; i < quantidade; i++) {
            int indice = -1;
            do {
                indice = random.nextInt(max);
            } while (sorteados.contains(this.amostraInteresses.get(indice)));
            sorteados.add(this.amostraInteresses.get(indice));
        }
        return sorteados;
    }

}
