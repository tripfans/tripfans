package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Classe criada para armazenar temporariamente os dados importados da base do Google Places
 * 
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "local_google")
public class LocalGoogle implements Serializable {

    private static final long serialVersionUID = 7121728443656163191L;

    /**
     * State or province.
     */
    @Column(name = "administrative_area_level_1")
    private String administrativeAreaLevel1;

    @Column(name = "administrative_area_level_1_abbr")
    private String administrativeAreaLevel1Abbr;

    /**
     * County or region.
     */
    @Column(name = "administrative_area_level_2")
    private String administrativeAreaLevel2;

    @Column(name = "administrative_area_level_2_abbr")
    private String administrativeAreaLevel2Abbr;

    @Column
    private String country;

    @Column(name = "country_abbr")
    private String countryAbbr;

    /**
     * All address components formatted together.
     */
    @Column(name = "formatted_address")
    private String formattedAddress;

    /**
     * In local format.
     */
    @Column(name = "formatted_phone_number")
    private String formattedPhoneNumber;

    /**
     * URL for an icon representing this type of place.
     */
    @Column
    private String icon;

    /**
     * Unique identifier that can be used to consolidate information about this place.
     */
    @Id
    @Column
    private String id;

    /**
     * Includes prefixed country code.
     */
    @Column(name = "international_phone_number")
    private String internationalPhoneNumber;

    @Column
    private Double latitude;

    /**
     * City.
     */
    @Column
    private String locality;

    @Column(name = "locality_abbr")
    private String localityAbbr;

    @Column
    private Double longitude;

    /**
     * Name of this place, for example a business or landmark name.
     */
    @Column
    private String name;

    /**
     * Opening and closing times for each day that this place is open.
     */
    @Column(name = "opening_hours", length = 1000)
    private String openingHours;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "postal_code_abbr")
    private String postalCodeAbbr;

    @Column(name = "postal_town")
    private String postalTown;

    @Column(name = "postal_town_abbr")
    private String postalTownAbbr;

    /**
     * Relative level of average expenses at this place. From 0 (least expensive) to 4 (most
     * expensive). Default value: -1.
     */
    @Column(name = "price_level")
    private Integer priceLevel;

    /**
     * From 0.0 to 5.0, based on user reviews. Default value: -1.0.
     */
    @Column
    private Double rating;

    /**
     * Token that can be used to retrieve details about this place. This may be one of multiple
     * references that can be used to access this place.
     */
    @Column(length = 2000)
    private String reference;

    /**
     * Street.
     */
    @Column
    private String route;

    @Column(name = "route_abbr")
    private String routeAbbr;

    @Column(name = "street_number")
    private String streetNumber;

    @Column(name = "street_number_abbr")
    private String streetNumberAbbr;

    /**
     * City district.
     */
    @Column
    private String sublocality;

    @Column(name = "sublocality_abbr")
    private String sublocalityAbbr;

    /**
     * Features describing this place.
     * 
     * @see <a href="https://developers.google.com/places/documentation/supported_types" target="_blank">Supported Place Types</a>
     */
    @Column(length = 500)
    private String types;

    /**
     * Google Place page.
     */
    @Column
    private String url;

    /**
     * Simplified address that stops after the city level.
     */
    @Column
    private String vicinity;

    /**
     * URL of the website for this place.
     */
    @Column
    private String website;

    public String getAdministrativeAreaLevel1() {
        return this.administrativeAreaLevel1;
    }

    public String getAdministrativeAreaLevel1Abbr() {
        return this.administrativeAreaLevel1Abbr;
    }

    public String getAdministrativeAreaLevel2() {
        return this.administrativeAreaLevel2;
    }

    public String getAdministrativeAreaLevel2Abbr() {
        return this.administrativeAreaLevel2Abbr;
    }

    public String getCountry() {
        return this.country;
    }

    public String getCountryAbbr() {
        return this.countryAbbr;
    }

    public String getFormattedAddress() {
        return this.formattedAddress;
    }

    public String getFormattedPhoneNumber() {
        return this.formattedPhoneNumber;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getId() {
        return this.id;
    }

    public String getInternationalPhoneNumber() {
        return this.internationalPhoneNumber;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public String[] getListOfTypes() {
        if (this.types != null) {
            return this.types.split(",");
        }
        return null;
    }

    public String getLocality() {
        return this.locality;
    }

    public String getLocalityAbbr() {
        return this.localityAbbr;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getName() {
        return this.name;
    }

    public String getOpeningHours() {
        return this.openingHours;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public String getPostalCodeAbbr() {
        return this.postalCodeAbbr;
    }

    public String getPostalTown() {
        return this.postalTown;
    }

    public String getPostalTownAbbr() {
        return this.postalTownAbbr;
    }

    public Integer getPriceLevel() {
        return this.priceLevel;
    }

    public Double getRating() {
        return this.rating;
    }

    public String getReference() {
        return this.reference;
    }

    public String getRoute() {
        return this.route;
    }

    public String getRouteAbbr() {
        return this.routeAbbr;
    }

    public String getStreetNumber() {
        return this.streetNumber;
    }

    public String getStreetNumberAbbr() {
        return this.streetNumberAbbr;
    }

    public String getSublocality() {
        return this.sublocality;
    }

    public String getSublocalityAbbr() {
        return this.sublocalityAbbr;
    }

    public String getTypes() {
        return this.types;
    }

    public String getUrl() {
        return this.url;
    }

    public String getVicinity() {
        return this.vicinity;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setAdministrativeAreaLevel1(final String administrativeAreaLevel1) {
        this.administrativeAreaLevel1 = administrativeAreaLevel1;
    }

    public void setAdministrativeAreaLevel1Abbr(final String administrativeAreaLevel1Abbr) {
        this.administrativeAreaLevel1Abbr = administrativeAreaLevel1Abbr;
    }

    public void setAdministrativeAreaLevel2(final String administrativeAreaLevel2) {
        this.administrativeAreaLevel2 = administrativeAreaLevel2;
    }

    public void setAdministrativeAreaLevel2Abbr(final String administrativeAreaLevel2Abbr) {
        this.administrativeAreaLevel2Abbr = administrativeAreaLevel2Abbr;
    }

    public void setCountry(final String country) {
        this.country = country;
    }

    public void setCountryAbbr(final String countryAbbr) {
        this.countryAbbr = countryAbbr;
    }

    public void setFormattedAddress(final String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    public void setFormattedPhoneNumber(final String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }

    public void setIcon(final String icon) {
        this.icon = icon;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setInternationalPhoneNumber(final String internationalPhoneNumber) {
        this.internationalPhoneNumber = internationalPhoneNumber;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLocality(final String locality) {
        this.locality = locality;
    }

    public void setLocalityAbbr(final String localityAbbr) {
        this.localityAbbr = localityAbbr;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setOpeningHours(final String openingHours) {
        this.openingHours = openingHours;
    }

    public void setPostalCode(final String postalCode) {
        this.postalCode = postalCode;
    }

    public void setPostalCodeAbbr(final String postalCodeAbbr) {
        this.postalCodeAbbr = postalCodeAbbr;
    }

    public void setPostalTown(final String postalTown) {
        this.postalTown = postalTown;
    }

    public void setPostalTownAbbr(final String postalTownAbbr) {
        this.postalTownAbbr = postalTownAbbr;
    }

    public void setPriceLevel(final Integer priceLevel) {
        this.priceLevel = priceLevel;
    }

    public void setRating(final Double rating) {
        this.rating = rating;
    }

    public void setReference(final String reference) {
        this.reference = reference;
    }

    public void setRoute(final String route) {
        this.route = route;
    }

    public void setRouteAbbr(final String routeAbbr) {
        this.routeAbbr = routeAbbr;
    }

    public void setStreetNumber(final String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public void setStreetNumberAbbr(final String streetNumberAbbr) {
        this.streetNumberAbbr = streetNumberAbbr;
    }

    public void setSublocality(final String sublocality) {
        this.sublocality = sublocality;
    }

    public void setSublocalityAbbr(final String sublocalityAbbr) {
        this.sublocalityAbbr = sublocalityAbbr;
    }

    public void setTypes(final String types) {
        this.types = types;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public void setVicinity(final String vicinity) {
        this.vicinity = vicinity;
    }

    public void setWebsite(final String website) {
        this.website = website;
    }
}
