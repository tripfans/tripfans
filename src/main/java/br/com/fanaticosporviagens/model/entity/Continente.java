package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Andr� Thiago
 * 
 */
@Entity
@DiscriminatorValue("1")
public class Continente extends LocalGeografico {

    private static final long serialVersionUID = 4365223318722157987L;

    @Column(name = "qtd_cidade", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeCidades = 0L;

    @Column(name = "qtd_estado", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeEstados = 0L;

    @Column(name = "qtd_pais", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadePaises = 0L;

    @Override
    public Local getPai() {
        // TODO Auto-generated method stub
        return null;
    }

    public Long getQuantidadeCidades() {
        return this.quantidadeCidades;
    }

    public Long getQuantidadeEstados() {
        return this.quantidadeEstados;
    }

    public Long getQuantidadePaises() {
        return this.quantidadePaises;
    }

    public void setQuantidadeCidades(final Long quantidadeCidades) {
        this.quantidadeCidades = quantidadeCidades;
    }

    public void setQuantidadeEstados(final Long quantidadeEstados) {
        this.quantidadeEstados = quantidadeEstados;
    }

    public void setQuantidadePaises(final Long quantidadePaises) {
        this.quantidadePaises = quantidadePaises;
    }
}
