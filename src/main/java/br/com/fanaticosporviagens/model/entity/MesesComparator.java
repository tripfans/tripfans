package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.util.Comparator;

class MesesComparator implements Comparator<Mes>, Serializable {

    private static final long serialVersionUID = 5481463412645129604L;

    @Override
    public int compare(final Mes mes1, final Mes mes2) {
        if (mes1.getCodigo() < mes2.getCodigo()) {
            return -1;
        } else if (mes1.getCodigo() > mes2.getCodigo()) {
            return 1;
        }
        return 0;
    }

}
