package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * Classe que representa um local sugerido por alguém para um Viagem
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_sugestao_local")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_sugestao_local", allocationSize = 1)
@Table(name = "sugestao_local")
public class SugestaoLocal extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel, Votavel, SocialShareableItem {

    private static final long serialVersionUID = 4349940951126941122L;

    @Column(name = "aceito", nullable = true)
    private Boolean aceito;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "autor", nullable = true)
    private Usuario autor;

    @Column(name = "imperdivel", nullable = false)
    private Boolean imperdivel = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = false)
    private Local local;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_dica", nullable = true)
    private PedidoDica pedidoDica;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private Viagem viagem;

    @Column(name = "visto", nullable = false, columnDefinition = "boolean DEFAULT false")
    private final Boolean visto = false;

    public Boolean getAceito() {
        return this.aceito;
    }

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    @Override
    public String getDescricaoAlvo() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Usuario getDestinatario() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getFacebookPostText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getFacebookPostTitle() {
        // TODO Auto-generated method stub
        return null;
    }

    public Boolean getImperdivel() {
        return this.imperdivel;
    }

    public Local getLocal() {
        return this.local;
    }

    public PedidoDica getPedidoDica() {
        return this.pedidoDica;
    }

    @Override
    public String getPrefix() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Long getQuantidadeVotoUtil() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getShareableLink() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getTwitterPostText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getUrlPath() {
        // TODO Auto-generated method stub
        return null;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    public Boolean getVisto() {
        return this.visto;
    }

    @Override
    public void incrementaQuantidadeVotoUtil() {
        // TODO Auto-generated method stub

    }

    public void setAceito(final Boolean aceito) {
        this.aceito = aceito;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setImperdivel(final Boolean imperdivel) {
        this.imperdivel = imperdivel;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setPedidoDica(final PedidoDica pedidoDica) {
        this.pedidoDica = pedidoDica;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        // TODO Auto-generated method stub

    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }

}
