package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "trecho_transporte")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_trecho_tranporte", allocationSize = 1)
public class TrechoTransporte<T extends DetalheTrecho> extends BaseEntity<Long> implements Comparable<TrechoTransporte<T>> {

    private static final long serialVersionUID = -3987376420579856481L;

    @Column
    private String assentos;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "data", column = @Column(name = "data_chegada")),
            @AttributeOverride(name = "hora", column = @Column(name = "hora_chegada")),
            @AttributeOverride(name = "dia", column = @Column(name = "dia_chegada")),
            @AttributeOverride(name = "descricao", column = @Column(name = "descricao_chegada")),
            @AttributeOverride(name = "local", column = @Column(name = "local_chegada")),
            @AttributeOverride(name = "terminal", column = @Column(name = "terminal_chegada")),
            @AttributeOverride(name = "portao", column = @Column(name = "portao_chegada")),
            @AttributeOverride(name = "observacao", column = @Column(name = "observacao_chegada")),
            @AttributeOverride(name = "localNaoCadastrado.nome", column = @Column(name = "nome_local_chegada")),
            @AttributeOverride(name = "localNaoCadastrado.descricao", column = @Column(name = "descricao_local_chegada")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.descricao", column = @Column(name = "endereco_local_chegada")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.bairro", column = @Column(name = "bairro_local_chegada")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.cep", column = @Column(name = "cep_local_chegada")) })
    private final DadosBasicosTrecho chegada = new DadosBasicosTrecho();

    /*
     * Para voos seria n�mero do voo. Para trens seria o n�mero do Trem. Para Cruzeiros a companhia do cruzeiro.
     */
    @Column(name = "codigo")
    private String codigo;

    /*
     * As datas de partida e chegada do trecho, ser�o iguais �s datas do plano de viagem, se for apenas um trecho. Caso seja mais de um trecho, a
     * menor data de partida ser� a data inicial do plano de viagem. E a maior data de chegada ser� a data final do plano de viagem. IMPORTANTE:
     * Quando for aluguel de carro, tem de colocar dois avisos. Um para o dia da retirada do carro e um para o dia da devolu��o. Para Cruzeiros
     * CadaTrecho pode representar as paradas do Navio em alguns portos.
     */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_grupo_empresarial", nullable = true)
    private GrupoEmpresarial companhia;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = DetalheTrecho.class)
    @JoinColumn(name = "id_detalhe_trecho", nullable = true)
    private T detalheTrecho;

    /*
     * Para o caso de aluguel de carro a companhia é a empresa que aluga os carros.
     */
    @Column(name = "nome_companhia")
    private String nomeCompanhia;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "data", column = @Column(name = "data_partida")),
            @AttributeOverride(name = "hora", column = @Column(name = "hora_partida")),
            @AttributeOverride(name = "dia", column = @Column(name = "dia_partida")),
            @AttributeOverride(name = "descricao", column = @Column(name = "descricao_partida")),
            @AttributeOverride(name = "local", column = @Column(name = "local_partida")),
            @AttributeOverride(name = "terminal", column = @Column(name = "terminal_partida")),
            @AttributeOverride(name = "portao", column = @Column(name = "portao_partida")),
            @AttributeOverride(name = "observacao", column = @Column(name = "observacao_partida")),
            @AttributeOverride(name = "localNaoCadastrado.nome", column = @Column(name = "nome_local_partida")),
            @AttributeOverride(name = "localNaoCadastrado.descricao", column = @Column(name = "descricao_local_partida")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.descricao", column = @Column(name = "endereco_local_partida")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.bairro", column = @Column(name = "bairro_local_partida")),
            @AttributeOverride(name = "localNaoCadastrado.endereco.cep", column = @Column(name = "cep_local_partida")) })
    private final DadosBasicosTrecho partida = new DadosBasicosTrecho();

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "id_transporte", nullable = false)
    private Transporte<?> transporte;

    @Override
    public int compareTo(final TrechoTransporte<T> trecho) {
        if (this.getPartida().getData() != null && trecho.getPartida().getData() != null) {
            return this.getPartida().getData().compareTo(trecho.getPartida().getData());
        } else if (this.getPartida().getDia() != null && trecho.getPartida().getDia() != null) {
            this.getPartida().getDia().compareTo(trecho.getPartida().getDia());
        }
        return 0;
    }

    public String getAssentos() {
        return this.assentos;
    }

    public DadosBasicosTrecho getChegada() {
        return this.chegada;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public GrupoEmpresarial getCompanhia() {
        return this.companhia;
    }

    public T getDetalheTrecho() {
        return this.detalheTrecho;
    }

    public String getNomeCompanhia() {
        if (getCompanhia() != null) {
            return this.companhia.getNome();
        }
        return this.nomeCompanhia;
    }

    public DadosBasicosTrecho getPartida() {
        return this.partida;
    }

    public Transporte<?> getTransporte() {
        return this.transporte;
    }

    public void setAssentos(final String assentos) {
        this.assentos = assentos;
    }

    public void setCodigo(final String codigo) {
        this.codigo = codigo;
    }

    public void setCompanhia(final GrupoEmpresarial companhia) {
        this.companhia = companhia;
    }

    public void setDetalheTrecho(final T detalheTrecho) {
        this.detalheTrecho = detalheTrecho;
    }

    public void setNomeCompanhia(final String nomeCompanhia) {
        this.nomeCompanhia = nomeCompanhia;
    }

    public void setTransporte(final Transporte<?> transporte) {
        this.transporte = transporte;
    }

}
