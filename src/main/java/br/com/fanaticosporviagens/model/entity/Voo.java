package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Carlos Nascimento
 */
@Entity
@DiscriminatorValue("51")
public class Voo extends Transporte<DetalheTrechoVoo> {

    private static final long serialVersionUID = 4356631838903708573L;

    /**
     * Localizador
     */
    @Column(name = "codigo_reserva_voo")
    private String codigoReserva;

    public String getCodigoReserva() {
        return this.codigoReserva;
    }

    @Override
    // TODO I18N
    public String getDescricaoAtividade() {
        String descricao = "Voo ";
        final TrechoTransporte<?> trechoInicial = getTrechoInicial();
        final TrechoTransporte<?> trechoFinal = getTrechoFinal();
        if (trechoInicial != null) {
            String nomeLocal = "";
            if (trechoInicial.getPartida().getLocal() instanceof Aeroporto) {
                nomeLocal += ((Aeroporto) trechoInicial.getPartida().getLocal()).getNomeCidadeComSiglaAeroporoto();
            } else {
                nomeLocal = trechoInicial.getPartida().getNomeLocal();
            }
            if (nomeLocal != null) {
                descricao += "de " + nomeLocal;
            }
        }
        if (trechoFinal != null) {
            String nomeLocal = "";
            if (trechoInicial.getPartida().getLocal() instanceof Aeroporto) {
                nomeLocal += ((Aeroporto) trechoInicial.getChegada().getLocal()).getNomeCidadeComSiglaAeroporoto();
            } else {
                nomeLocal = trechoInicial.getChegada().getNomeLocal();
            }
            if (nomeLocal != null) {
                descricao += " para " + nomeLocal;
            }
        }
        return descricao;
    }

    @Override
    public String getDescricaoFim() {
        return null;
    }

    @Override
    public String getDescricaoInicio() {
        return null;
    }

    @Override
    public TipoAtividade getTipo() {
        // return TipoAtividade.VOO;
        return null;
    }

    public void setCodigoReserva(final String codigoReserva) {
        this.codigoReserva = codigoReserva;
    }
}
