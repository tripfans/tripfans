package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_avaliacao_consolidada")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_avaliacao_consolidada", allocationSize = 1)
@Table(name = "avaliacao_consolidada", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_local_avaliado", "id_avaliacao_criterio" }) })
@TypeDefs(value = { @TypeDef(name = "TipoAvaliacaoCriterio", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.AvaliacaoCriterio") }) })
public class AvaliacaoConsolidadaPorCriterio extends BaseEntity<Long> implements Comparable<AvaliacaoConsolidadaPorCriterio> {

    private static final long serialVersionUID = 697086441989002475L;

    @Embedded
    private AvaliacaoConsolidada avaliacaoConsolidada;

    @Type(type = "TipoAvaliacaoCriterio")
    @Column(name = "id_avaliacao_criterio", nullable = false)
    private AvaliacaoCriterio criterio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_avaliado", nullable = false)
    private Local localAvaliado;

    @Override
    public int compareTo(final AvaliacaoConsolidadaPorCriterio other) {
        return this.criterio.getDescricao().compareTo(other.getCriterio().getDescricao());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final AvaliacaoConsolidadaPorCriterio other = (AvaliacaoConsolidadaPorCriterio) obj;
        if (this.criterio != other.criterio)
            return false;
        if (this.localAvaliado == null) {
            if (other.localAvaliado != null)
                return false;
        } else if (!this.localAvaliado.equals(other.localAvaliado))
            return false;
        return true;
    }

    public AvaliacaoConsolidada getAvaliacaoConsolidada() {
        return this.avaliacaoConsolidada;
    }

    public AvaliacaoQualidade getClassificacaoGeral() {
        return this.avaliacaoConsolidada.getClassificacaoGeral();
    }

    public AvaliacaoCriterio getCriterio() {
        return this.criterio;
    }

    public Date getDataConsolidacao() {
        return this.avaliacaoConsolidada.getDataConsolidacao();
    }

    public Local getLocalAvaliado() {
        return this.localAvaliado;
    }

    public BigDecimal getNotaGeral() {
        return this.avaliacaoConsolidada.getNotaGeral();
    }

    public Long getPorcentagemAvaliacaoBom() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoBom();
    }

    public Long getPorcentagemAvaliacaoExcelente() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoExcelente();
    }

    public Long getPorcentagemAvaliacaoPessimo() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoPessimo();
    }

    public Long getPorcentagemAvaliacaoPositivas() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoPositivas();
    }

    public Long getPorcentagemAvaliacaoRegular() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoRegular();
    }

    public Long getPorcentagemAvaliacaoRuim() {
        return this.avaliacaoConsolidada.getPorcentagemAvaliacaoRuim();
    }

    public Long getQuantidadeAvaliacaoBom() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacaoBom();
    }

    public Long getQuantidadeAvaliacaoExcelente() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacaoExcelente();
    }

    public Long getQuantidadeAvaliacaoPessimo() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacaoPessimo();
    }

    public Long getQuantidadeAvaliacaoRegular() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacaoRegular();
    }

    public Long getQuantidadeAvaliacaoRuim() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacaoRuim();
    }

    public Long getQuantidadeAvaliacoes() {
        return this.avaliacaoConsolidada.getQuantidadeAvaliacoes();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.criterio == null) ? 0 : this.criterio.hashCode());
        result = prime * result + ((this.localAvaliado == null) ? 0 : this.localAvaliado.hashCode());
        return result;
    }

    public void setAvaliacaoConsolidada(final AvaliacaoConsolidada avaliacaoConsolidada) {
        this.avaliacaoConsolidada = avaliacaoConsolidada;
    }

    public void setCriterio(final AvaliacaoCriterio criterio) {
        this.criterio = criterio;
    }

    public void setLocalAvaliado(final Local localAvaliado) {
        this.localAvaliado = localAvaliado;
    }

}
