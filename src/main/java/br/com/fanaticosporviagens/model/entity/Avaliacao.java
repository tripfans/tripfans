package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDateTime;
import org.springframework.util.AutoPopulatingList;
import org.springframework.util.CollectionUtils;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Classe que representa uma avaliação.
 * 
 * @author AndreThiago
 * 
 */

@Entity
@DynamicUpdate
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_avaliacao")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_avaliacao", allocationSize = 1)
@Table(name = "avaliacao")
@TypeDefs(value = {
        @TypeDef(name = "TipoMes", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.Mes") }),
        @TypeDef(name = "TipoAvaliacaoQualidade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade") }) })
public class Avaliacao extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel, Votavel, SocialShareableItem {

    private static final long serialVersionUID = -396905254523655314L;

    private static final String TEXTO_TWITTER = "Escrevi uma avaliação sobre %s com TripFans: ";

    private static final String TITULO_FACEBOOK = "Escreveu uma avaliação sobre %s.";

    @Column(name = "ano_visita", nullable = false)
    @NotNull(message = "{validation.avaliacao.anoVisita.required}")
    private Integer anoVisita;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_atividade", nullable = true)
    private AtividadePlano atividadePlano;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    @OneToMany(mappedBy = "avaliacao", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AvaliacaoClassificacao> avaliacoesClassificacao;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_atendimento", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoAtendimento = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_beleza", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoBeleza = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_coisas_fazer", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoCoisasParaFazer = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_comida", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoComida = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_custo", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoCusto = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_geral", columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoGeral;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_limpeza", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoLimpeza = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_localizacao", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoLocalizacao = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_locomocao_transito", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoLocomocaoTransito = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_lojas", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoLojas = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_organicacao", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoOrganicacao = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_praca_alimentacao", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoPracaDeAlimentacao = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_preco", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoPreco = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_quartos", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoQuartos = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_seguranca", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoSeguranca = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_servicos_oferecidos", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoServicosOferecidos = AvaliacaoQualidade.NAO_SE_APLICA;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_vida_noturna", nullable = false, columnDefinition = "int4 not null default 0")
    private AvaliacaoQualidade classificacaoVidaNoturna = AvaliacaoQualidade.NAO_SE_APLICA;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    @Transient
    @Valid
    private List<Foto> fotos = new AutoPopulatingList<Foto>(Foto.class);

    @OneToMany(mappedBy = "avaliacao", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AvaliacaoBomPara> itensBomPara;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_avaliado", nullable = false)
    private Local localAvaliado;

    @Type(type = "TipoMes")
    @Column(name = "mes_visita", nullable = false)
    @NotNull(message = "{validation.avaliacao.mesVisita.required}")
    private Mes mesVisita;

    @Column(name = "nota_geral", nullable = true)
    private Integer notaGeral; // TODO mudar para poder ser quebrada (2.00, 2.50 etc)

    @Column(name = "participa_consolidacao", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean participaConsolidacao;

    @Column(name = "qtd_foto", nullable = true)
    private Long quantidadeFotos = 0L;

    @Column(name = "qtd_voto_util", nullable = false)
    private Long quantidadeVotoUtil = 0L;

    @OneToMany(mappedBy = "avaliacao", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AvaliacaoTipoViagem> tiposViagemRealizada;

    @Column(name = "titulo", nullable = false)
    @NotNull(message = "${validation.avaliacao.titulo.required}")
    private String titulo;

    @Column(name = "url_path", nullable = false, unique = true)
    private String urlPath;

    public void ajustarItensBomPara() {
        if (this.itensBomPara != null) {
            final Iterator<AvaliacaoBomPara> iterator = this.itensBomPara.iterator();
            while (iterator.hasNext()) {
                final AvaliacaoBomPara bomPara = iterator.next();
                if (bomPara.getItem() == null) {
                    iterator.remove();
                } else {
                    bomPara.setAvaliacao(this);
                }
            }
        }
    }

    public void ajustarQuantidadeFotos() {
        if (possuiFotos()) {
            this.quantidadeFotos = Long.valueOf(this.fotos.size());
        }
    }

    public void ajustarTiposViagem() {
        if (this.tiposViagemRealizada != null) {
            final Iterator<AvaliacaoTipoViagem> iterator = this.tiposViagemRealizada.iterator();
            while (iterator.hasNext()) {
                final AvaliacaoTipoViagem avaliacaoTipoViagem = iterator.next();
                if (avaliacaoTipoViagem.getTipo() == null) {
                    iterator.remove();
                } else {
                    avaliacaoTipoViagem.setAvaliacao(this);
                }
            }
        }
    }

    /*public void calcularAvaliacaoQualidadeGeralENotaGeral() {
        Integer notasSomadas = 0;
        final List<AvaliacaoClassificacao> avaliacoesClassificacaoAplicaveis = getAvaliacoesClassificacaoAplicaveis();
        for (final AvaliacaoClassificacao classificacao : avaliacoesClassificacaoAplicaveis) {
            notasSomadas += classificacao.getQualidade().getNota();
        }
        this.notaGeral = (new BigDecimal(notasSomadas)).divide(new BigDecimal(avaliacoesClassificacaoAplicaveis.size()), 2, RoundingMode.HALF_UP);

        final int notaGeralInteira = (new BigDecimal(notasSomadas)).divide(new BigDecimal(avaliacoesClassificacaoAplicaveis.size()), 0,
                RoundingMode.HALF_UP).intValue();

        this.classificacaoGeral = AvaliacaoQualidade.getAvaliacaoQualidadePorNota(notaGeralInteira);
    }*/

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.localAvaliado;
    }

    public Integer getAnoVisita() {
        return this.anoVisita;
    }

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public List<AvaliacaoClassificacao> getAvaliacoesClassificacaoAplicaveis() {
        final List<AvaliacaoClassificacao> classificacoes = new ArrayList<AvaliacaoClassificacao>();
        if (this.classificacaoAtendimento != null && this.classificacaoAtendimento.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.ATENDIMENTO, this.classificacaoAtendimento));
        }
        if (this.classificacaoComida != null && this.classificacaoComida.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.COMIDA, this.classificacaoComida));
        }
        if (this.classificacaoLimpeza != null && this.classificacaoLimpeza.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.LIMPEZA, this.classificacaoLimpeza));
        }
        if (this.classificacaoLocalizacao != null && this.classificacaoLocalizacao.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.LOCALIZACAO, this.classificacaoLocalizacao));
        }
        if (this.classificacaoLojas != null && this.classificacaoLojas.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.LOJAS, this.classificacaoLojas));
        }
        if (this.classificacaoPracaDeAlimentacao != null && this.classificacaoPracaDeAlimentacao.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.PRACA_DE_ALIMENTACAO, this.classificacaoPracaDeAlimentacao));
        }
        if (this.classificacaoPreco != null && this.classificacaoPreco.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.PRECO_QUALIDADE, this.classificacaoPreco));
        }
        if (this.classificacaoServicosOferecidos != null && this.classificacaoServicosOferecidos.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.SERVICOS_OFERECIDOS, this.classificacaoServicosOferecidos));
        }
        if (this.classificacaoBeleza != null && this.classificacaoBeleza.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.BELEZA, this.classificacaoBeleza));
        }
        if (this.classificacaoCusto != null && this.classificacaoCusto.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.CUSTO, this.classificacaoCusto));
        }
        if (this.classificacaoLocomocaoTransito != null && this.classificacaoLocomocaoTransito.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.LOCOMOCAO_TRANSITO, this.classificacaoLocomocaoTransito));
        }
        if (this.classificacaoOrganicacao != null && this.classificacaoOrganicacao.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.ORGANICACAO, this.classificacaoOrganicacao));
        }
        if (this.classificacaoSeguranca != null && this.classificacaoSeguranca.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.SEGURANCA, this.classificacaoSeguranca));
        }
        if (this.classificacaoCoisasParaFazer != null && this.classificacaoCoisasParaFazer.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.COISAS_PARA_FAZER, this.classificacaoCoisasParaFazer));
        }
        if (this.classificacaoVidaNoturna != null && this.classificacaoVidaNoturna.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.VIDA_NOTURNA, this.classificacaoVidaNoturna));
        }
        if (this.classificacaoQuartos != null && this.classificacaoQuartos.isAplicavel()) {
            classificacoes.add(new AvaliacaoClassificacao(this, AvaliacaoCriterio.QUARTOS, this.classificacaoQuartos));
        }

        return classificacoes;
    }

    public AvaliacaoQualidade getClassificacaoAtendimento() {
        return this.classificacaoAtendimento;
    }

    public AvaliacaoQualidade getClassificacaoBeleza() {
        return this.classificacaoBeleza;
    }

    public AvaliacaoQualidade getClassificacaoCoisasParaFazer() {
        return this.classificacaoCoisasParaFazer;
    }

    public AvaliacaoQualidade getClassificacaoComida() {
        return this.classificacaoComida;
    }

    public AvaliacaoQualidade getClassificacaoCusto() {
        return this.classificacaoCusto;
    }

    public AvaliacaoQualidade getClassificacaoGeral() {
        return this.classificacaoGeral;
    }

    public AvaliacaoQualidade getClassificacaoLimpeza() {
        return this.classificacaoLimpeza;
    }

    public AvaliacaoQualidade getClassificacaoLocalizacao() {
        return this.classificacaoLocalizacao;
    }

    public AvaliacaoQualidade getClassificacaoLocomocaoTransito() {
        return this.classificacaoLocomocaoTransito;
    }

    public AvaliacaoQualidade getClassificacaoLojas() {
        return this.classificacaoLojas;
    }

    public AvaliacaoQualidade getClassificacaoOrganicacao() {
        return this.classificacaoOrganicacao;
    }

    public AvaliacaoQualidade getClassificacaoPracaDeAlimentacao() {
        return this.classificacaoPracaDeAlimentacao;
    }

    public AvaliacaoQualidade getClassificacaoPreco() {
        return this.classificacaoPreco;
    }

    public AvaliacaoQualidade getClassificacaoQuartos() {
        return this.classificacaoQuartos;
    }

    public AvaliacaoQualidade getClassificacaoSeguranca() {
        return this.classificacaoSeguranca;
    }

    public AvaliacaoQualidade getClassificacaoServicosOferecidos() {
        return this.classificacaoServicosOferecidos;
    }

    public AvaliacaoQualidade getClassificacaoVidaNoturna() {
        return this.classificacaoVidaNoturna;
    }

    public Date getDataAvaliacao() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    public String getDescricao() {
        if (this.descricao != null) {
            return this.descricao.replaceAll("\r\n", "<br/>");
        }
        return null;
    }

    @Override
    public String getDescricaoAlvo() {
        return getAlvo().getDescricaoAlvo();
    }

    @Override
    public String getFacebookPostText() {
        return getDescricao();
    }

    @Override
    public String getFacebookPostTitle() {
        return String.format(TITULO_FACEBOOK, getLocalAvaliado().getNome());
    }

    @JsonIgnore
    public List<Foto> getFotos() {
        return this.fotos;
    }

    public List<AvaliacaoBomPara> getItensBomPara() {
        return this.itensBomPara;
    }

    public Local getLocalAvaliado() {
        return this.localAvaliado;
    }

    public Mes getMesVisita() {
        return this.mesVisita;
    }

    public Integer getNotaGeral() {
        return this.notaGeral;
    }

    public Boolean getParticipaConsolidacao() {
        return this.participaConsolidacao;
    }

    @Override
    public String getPrefix() {
        return "avl";
    }

    public Long getQuantidadeFotos() {
        return this.quantidadeFotos;
    }

    @Override
    public Long getQuantidadeVotoUtil() {
        return this.quantidadeVotoUtil;
    }

    @Override
    public String getShareableLink() {
        final StringBuffer linkBuffer = new StringBuffer(UrlLocalResolver.getUrl(this.getLocalAvaliado()));
        linkBuffer.append("/avaliacoes?item=");
        if (getUrlPath() != null) {
            linkBuffer.append(this.getUrlPath());
        } else {
            linkBuffer.append(this.getId());
        }
        return linkBuffer.toString();
    }

    public List<AvaliacaoTipoViagem> getTiposViagemRealizada() {
        return this.tiposViagemRealizada;
    }

    public String getTitulo() {
        return this.titulo;
    }

    @Override
    public String getTwitterPostText() {
        return String.format(TEXTO_TWITTER, getLocalAvaliado().getNome());
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    @Override
    public void incrementaQuantidadeVotoUtil() {
        this.quantidadeVotoUtil += 1;
    }

    public boolean possuiFotos() {
        return (!CollectionUtils.isEmpty(this.fotos) && this.fotos.get(0).temArquivo()) || this.quantidadeFotos != 0L;
    }

    public void setAnoVisita(final Integer anoVisita) {
        this.anoVisita = anoVisita;
    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
        if (atividadePlano.getAvaliacao() == null) {
            atividadePlano.setAvaliacao(this);
        }
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setClassificacaoAtendimento(final AvaliacaoQualidade classificacaoAtendimento) {
        this.classificacaoAtendimento = classificacaoAtendimento;
    }

    public void setClassificacaoBeleza(final AvaliacaoQualidade classificacaoBeleza) {
        this.classificacaoBeleza = classificacaoBeleza;
    }

    public void setClassificacaoCoisasParaFazer(final AvaliacaoQualidade classificacaoCoisasParaFazer) {
        this.classificacaoCoisasParaFazer = classificacaoCoisasParaFazer;
    }

    public void setClassificacaoComida(final AvaliacaoQualidade classificacaoComida) {
        this.classificacaoComida = classificacaoComida;
    }

    public void setClassificacaoCusto(final AvaliacaoQualidade classificacaoCusto) {
        this.classificacaoCusto = classificacaoCusto;
    }

    public void setClassificacaoGeral(final AvaliacaoQualidade classificacaoGeral) {
        this.classificacaoGeral = classificacaoGeral;
    }

    public void setClassificacaoLimpeza(final AvaliacaoQualidade classificacaoLimpeza) {
        this.classificacaoLimpeza = classificacaoLimpeza;
    }

    public void setClassificacaoLocalizacao(final AvaliacaoQualidade classificacaoLocalizacao) {
        this.classificacaoLocalizacao = classificacaoLocalizacao;
    }

    public void setClassificacaoLocomocaoTransito(final AvaliacaoQualidade classificacaoLocomocaoTransito) {
        this.classificacaoLocomocaoTransito = classificacaoLocomocaoTransito;
    }

    public void setClassificacaoLojas(final AvaliacaoQualidade classificacaoLojas) {
        this.classificacaoLojas = classificacaoLojas;
    }

    public void setClassificacaoOrganicacao(final AvaliacaoQualidade classificacaoOrganicacao) {
        this.classificacaoOrganicacao = classificacaoOrganicacao;
    }

    public void setClassificacaoPracaDeAlimentacao(final AvaliacaoQualidade classificacaoPracaDeAlimentacao) {
        this.classificacaoPracaDeAlimentacao = classificacaoPracaDeAlimentacao;
    }

    public void setClassificacaoPreco(final AvaliacaoQualidade classificacaoPreco) {
        this.classificacaoPreco = classificacaoPreco;
    }

    public void setClassificacaoQuartos(final AvaliacaoQualidade classificacaoQuartos) {
        this.classificacaoQuartos = classificacaoQuartos;
    }

    public void setClassificacaoSeguranca(final AvaliacaoQualidade classificacaoSeguranca) {
        this.classificacaoSeguranca = classificacaoSeguranca;
    }

    public void setClassificacaoServicosOferecidos(final AvaliacaoQualidade classificacaoServicosOferecidos) {
        this.classificacaoServicosOferecidos = classificacaoServicosOferecidos;
    }

    public void setClassificacaoVidaNoturna(final AvaliacaoQualidade classificacaoVidaNoturna) {
        this.classificacaoVidaNoturna = classificacaoVidaNoturna;
    }

    public void setDataAvaliacao(final Date dataAvaliacao) {
        if (dataAvaliacao != null) {
            this.setDataCriacao(new LocalDateTime(dataAvaliacao));
        }
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setFotos(final List<Foto> fotos) {
        this.fotos = fotos;
    }

    public void setItensBomPara(final List<AvaliacaoBomPara> itensBomPara) {
        this.itensBomPara = itensBomPara;
    }

    public void setLocalAvaliado(final Local localAvaliado) {
        this.localAvaliado = localAvaliado;
    }

    public void setMesVisita(final Mes mesVisita) {
        this.mesVisita = mesVisita;
    }

    public void setNotaGeral(final Integer notaGeral) {
        this.notaGeral = notaGeral;
    }

    public void setParticipaConsolidacao(final Boolean participaConsolidacao) {
        this.participaConsolidacao = participaConsolidacao;
    }

    public void setQuantidadeFotos(final Long quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setQuantidadeVotoUtil(final Long quantidadeVotoUtil) {
        this.quantidadeVotoUtil = quantidadeVotoUtil;
    }

    public void setTiposViagemRealizada(final List<AvaliacaoTipoViagem> tiposViagemRealizada) {
        this.tiposViagemRealizada = tiposViagemRealizada;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }
}
