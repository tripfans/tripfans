package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Oportunidade de negócio. Idéia para o futuro.
 * 
 * http://www.aluguetemporada.com.br/imovel/p507954829?cid=E_habrownerinquiry_DB_O_20111120_propertyurl_text_LPROP_
 * 
 * @author GustavoHenrique
 * 
 */
@Entity
@DiscriminatorValue("9")
public class ImovelAluguelTemporada extends LocalEnderecavel {

}
