package br.com.fanaticosporviagens.model.entity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public enum FormatoDistancia {

    KM, MILHAS

}
