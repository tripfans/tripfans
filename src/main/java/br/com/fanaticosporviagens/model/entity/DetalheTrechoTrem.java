package br.com.fanaticosporviagens.model.entity;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
public class DetalheTrechoTrem extends DetalheTrecho {

    private static final long serialVersionUID = -8986586881796398358L;

    private String assentos;

    private String classe;

    private String modeloTrem;

    private String pensao;

    private String vagao;

}
