package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author André Thiago
 * 
 */
@Entity
@DiscriminatorValue("3")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Estado extends LocalGeografico {

    private static final long serialVersionUID = -7579183100396637340L;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente")
    private Continente continente;

    @Deprecated
    @Column(name = "nm_local_continente", nullable = true)
    private String continenteNome;

    @Deprecated
    @Column(name = "url_path_local_continente", nullable = true)
    private String continenteUrlPath;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_pais", nullable = false)
    @JsonIgnore
    private Pais pais;

    @Deprecated
    @Column(name = "nm_local_pais", nullable = true)
    private String paisNome;

    @Deprecated
    @Column(name = "url_path_local_pais", nullable = true)
    private String paisUrlPath;

    @Column(name = "qtd_cidade", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeCidades = 0L;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Estado other = (Estado) obj;
        if (this.pais == null) {
            if (other.pais != null)
                return false;
        } else if (!this.pais.equals(other.pais))
            return false;
        if (this.getSigla() == null) {
            if (other.getSigla() != null)
                return false;
        } else if (!this.getSigla().equals(other.getSigla()))
            return false;
        return true;
    }

    public Continente getContinente() {
        return this.continente;
    }

    public String getContinenteNome() {
        return this.continenteNome;
    }

    public String getContinenteUrl() {
        return UrlLocalResolver.getUrl(LocalType.CONTINENTE, getContinenteUrlPath());
    }

    public String getContinenteUrlPath() {
        return this.continenteUrlPath;
    }

    @Override
    public List<Local> getLocaisInteressados() {
        final List<Local> interessados = new ArrayList<Local>();

        interessados.add(this);
        if (this.pais != null) {
            interessados.add(this.pais);
        }
        if (this.continente != null) {
            interessados.add(this.continente);
        }

        return interessados;
    }

    @Override
    public Local getPai() {
        return getPais();
    }

    public Pais getPais() {
        return this.pais;
    }

    public String getPaisNome() {
        return this.paisNome;
    }

    public String getPaisUrl() {
        return UrlLocalResolver.getUrl(LocalType.PAIS, getPaisUrlPath());
    }

    public String getPaisUrlPath() {
        return this.paisUrlPath;
    }

    public Long getQuantidadeCidades() {
        return this.quantidadeCidades;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.pais == null) ? 0 : this.pais.hashCode());
        result = prime * result + ((this.getSigla() == null) ? 0 : this.getSigla().hashCode());
        return result;
    }

    public void setContinente(final Continente continente) {
        this.continente = continente;
    }

    public void setContinenteNome(final String continenteNome) {
        this.continenteNome = continenteNome;
    }

    public void setContinenteUrlPath(final String continenteUrlPath) {
        this.continenteUrlPath = continenteUrlPath;
    }

    public void setPais(final Pais pais) {
        this.pais = pais;
    }

    public void setPaisNome(final String paisNome) {
        this.paisNome = paisNome;
    }

    public void setPaisUrlPath(final String paisUrlPath) {
        this.paisUrlPath = paisUrlPath;
    }

    public void setQuantidadeCidades(final Long quantidadeCidades) {
        this.quantidadeCidades = quantidadeCidades;
    }
}
