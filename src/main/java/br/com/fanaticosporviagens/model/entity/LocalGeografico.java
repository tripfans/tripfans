package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.BatchSize;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@BatchSize(size = 10)
public abstract class LocalGeografico extends Local {
    private static final long serialVersionUID = -5453069941281478697L;

    @Column(name = "qtd_aeroporto", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeAeroportos = 0L;

    @Column(name = "qtd_agencia", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeAgencias = 0L;

    @Column(name = "qtd_atracao", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeAtracoes = 0L;

    @Column(name = "qtd_hotel", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeHoteis = 0L;

    @Column(name = "qtd_restaurante", nullable = false, columnDefinition = "BIGINT DEFAULT 0")
    private Long quantidadeRestaurantes = 0L;

    @Column(name = "sigla")
    private String sigla;

    public LocalGeografico() {
        setTipoLocal(LocalType.getLocalTypePorClasse(getClass()));
    }

    public String getNomeComSiglaEstado() {
        return "";
    }

    public Long getQuantidadeAeroportos() {
        return this.quantidadeAeroportos;
    }

    public Long getQuantidadeAgencias() {
        return this.quantidadeAgencias;
    }

    public Long getQuantidadeAtracoes() {
        return this.quantidadeAtracoes;
    }

    public Long getQuantidadeHoteis() {
        return this.quantidadeHoteis;
    }

    public Long getQuantidadeRestaurantes() {
        return this.quantidadeRestaurantes;
    }

    public String getSigla() {
        return this.sigla;
    }

    public void setQuantidadeAeroportos(final Long quantidadeAeroportos) {
        this.quantidadeAeroportos = quantidadeAeroportos;
    }

    public void setQuantidadeAgencias(final Long quantidadeAgencias) {
        this.quantidadeAgencias = quantidadeAgencias;
    }

    public void setQuantidadeAtracoes(final Long quantidadeAtracoes) {
        this.quantidadeAtracoes = quantidadeAtracoes;
    }

    public void setQuantidadeHoteis(final Long quantidadeHoteis) {
        this.quantidadeHoteis = quantidadeHoteis;
    }

    public void setQuantidadeRestaurantes(final Long quantidadeRestaurantes) {
        this.quantidadeRestaurantes = quantidadeRestaurantes;
    }

    public void setSigla(final String sigla) {
        this.sigla = sigla;
    }

}
