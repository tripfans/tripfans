package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * Convite para amizade, para participação em grupos ou para participar de viagens.
 *
 * @author André Thiago
 *
 */
@Entity
@Table(name = "convite", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_remetente", "id_convidado", "tipo_convite", "data_aceite" }),
        @UniqueConstraint(columnNames = { "id_remetente", "id_facebook_convidado", "tipo_convite", "data_aceite" }),
        @UniqueConstraint(columnNames = { "id_remetente", "email_convidado", "tipo_convite", "data_aceite" }) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_convite", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_convite", nullable = false, unique = true)) })
@TypeDefs(value = { @TypeDef(name = "TipoConvite", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoConvite") }) })
public class Convite extends EntidadeGeradoraAcao<Long> {

    private static final long serialVersionUID = 4336785966226066446L;

    @Column(name = "aceito", nullable = false, columnDefinition = "boolean DEFAULT false")
    private boolean aceito = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_convidado", nullable = true)
    private Usuario convidado;

    @Column(name = "data_aceite")
    private Date dataAceite;

    @Column(name = "email_convidado")
    private String emailConvidado;

    /**
     * ID do App Request no Facebook
     * Utilizado para fazer o controle de quando o convite foi aceito
     */
    @Column(name = "id_request_facebook")
    private String idAppRequestFacebook;

    @Column(name = "id_facebook_convidado")
    private String idFacebookConvidado;

    @Column(name = "ignorado", nullable = false, columnDefinition = "boolean DEFAULT false")
    private boolean ignorado = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_remetente", nullable = false, updatable = false)
    private Usuario remetente;

    @Column(name = "tipo_convite", nullable = false, updatable = false)
    @Type(type = "TipoConvite")
    private TipoConvite tipoConvite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private Viagem viagem;

    public Convite() {
        setDataCriacao(new LocalDateTime());
    }

    public Convite(final Usuario remetente, final Amizade amizade) {
        this.remetente = remetente;
        if (amizade.isTripfans()) {
            this.convidado = amizade.getAmigo();
        } else {
            this.idFacebookConvidado = amizade.getIdAmigoFacebook();
        }
        setDataCriacao(new LocalDateTime());
    }

    public Convite(final Usuario remetente, final String emailconvidado) {
        this.remetente = remetente;
        this.emailConvidado = emailconvidado;
        setDataCriacao(new LocalDateTime());
    }

    public Convite(final Usuario remetente, final Usuario convidado) {
        this.remetente = remetente;
        this.convidado = convidado;
        setDataCriacao(new LocalDateTime());
    }

    public void aceitar() {
        this.aceito = true;
        this.dataAceite = new Date();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.getConvidado();
    }

    @Override
    public Usuario getAutor() {
        return this.getRemetente();
    }

    public Usuario getConvidado() {
        return this.convidado;
    }

    public Date getDataAceite() {
        return this.dataAceite;
    }

    public Date getDataConvite() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    @Override
    public Usuario getDestinatario() {
        return this.getConvidado();
    }

    public String getEmailConvidado() {
        return this.emailConvidado;
    }

    public String getIdAppRequestFacebook() {
        return this.idAppRequestFacebook;
    }

    public String getIdFacebookConvidado() {
        return this.idFacebookConvidado;
    }

    public Usuario getRemetente() {
        return this.remetente;
    }

    public TipoConvite getTipoConvite() {
        return this.tipoConvite;
    }

    public void ignorar() {
        this.ignorado = true;
    }

    public boolean isAceito() {
        return this.aceito;
    }

    public boolean isAmizade() {
        return this.tipoConvite.equals(TipoConvite.AMIZADE);
    }

    public boolean isIgnorado() {
        return this.ignorado;
    }

    public boolean isParticipacaoGrupo() {
        return this.tipoConvite.equals(TipoConvite.PARTICIPACAO_GRUPO);
    }

    public boolean isParticipacaoViagem() {
        return this.tipoConvite.equals(TipoConvite.PARTICIPACAO_VIAGEM);
    }

    public boolean isRecomendacaoLocal() {
        return this.tipoConvite.equals(TipoConvite.RECOMENDAR_SOBRE_LOCAL);
    }

    public boolean isRecomendacaoViagem() {
        return this.tipoConvite.equals(TipoConvite.RECOMENDAR_SOBRE_VIAGEM);
    }

    public boolean isTripfans() {
        return this.tipoConvite.equals(TipoConvite.ENTRAR_TRIPFANS);
    }

    public Convite paraAmizade() {
        this.setTipoConvite(TipoConvite.AMIZADE);
        return this;
    }

    public Convite paraEntrarTripFans() {
        this.setTipoConvite(TipoConvite.ENTRAR_TRIPFANS);
        return this;
    }

    public Convite paraParticipacaoGrupo() {
        this.setTipoConvite(TipoConvite.PARTICIPACAO_GRUPO);
        return this;
    }

    public Convite paraParticipacaoViagem(final Viagem viagem) {
        this.setViagem(viagem);
        this.setTipoConvite(TipoConvite.PARTICIPACAO_VIAGEM);
        return this;
    }

    public Convite paraRecomendacoesViagem(final Viagem viagem) {
        this.setViagem(viagem);
        this.setTipoConvite(TipoConvite.RECOMENDAR_SOBRE_VIAGEM);
        return this;
    }

    public void setConvidado(final Usuario usuario) {
        this.convidado = usuario;
    }

    public void setDataAceite(final Date dataAceite) {
        this.dataAceite = dataAceite;
    }

    public void setEmailConvidado(final String emailConvidado) {
        this.emailConvidado = emailConvidado;
    }

    public void setIdAppRequestFacebook(final String idAppRequestFacebook) {
        this.idAppRequestFacebook = idAppRequestFacebook;
    }

    public void setIdFacebookConvidado(final String idFacebookConvidado) {
        this.idFacebookConvidado = idFacebookConvidado;
    }

    public void setIgnorado(final boolean ignorado) {
        this.ignorado = ignorado;
    }

    public void setRemetente(final Usuario remetente) {
        this.remetente = remetente;
    }

    public void setTipoConvite(final TipoConvite tipoConvite) {
        this.tipoConvite = tipoConvite;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }

}
