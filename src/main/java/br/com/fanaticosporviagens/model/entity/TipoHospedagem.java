package br.com.fanaticosporviagens.model.entity;

import java.util.Locale;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

public enum TipoHospedagem {
    ACAMPAMENTO(1),
    ALBERGUE(2),
    FLAT(3),
    HOTEL(4),
    POUSADA(5);

    private int codigo;

    private final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    TipoHospedagem(final int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.messageSource.getMessage(this.getClass().getSimpleName() + "." + name(), null, Locale.getDefault());
    }
}
