package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Carlos Nascimento
 */
@Entity
@DiscriminatorValue("3")
public class Alimentacao extends AtividadePlano {

    private static final long serialVersionUID = 6981153640461547703L;

    @Override
    public String getDescricaoAtividade() {
        return getNomeLocal();
    }

    @Override
    public TipoAtividade getTipo() {
        return TipoAtividade.ALIMENTACAO;
    }

}
