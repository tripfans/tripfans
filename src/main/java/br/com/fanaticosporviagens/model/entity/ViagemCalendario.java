package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViagemCalendario {

    private final Viagem viagem;

    public ViagemCalendario(final Viagem viagem) {
        this.viagem = viagem;
    }

    public Integer getPercentualWidthColuna() {
        final Map<Integer, Integer> mapPercentualWidthColuna = new HashMap<Integer, Integer>();
        mapPercentualWidthColuna.put(1, 100);
        mapPercentualWidthColuna.put(2, 50);
        mapPercentualWidthColuna.put(3, 33);
        mapPercentualWidthColuna.put(4, 25);
        mapPercentualWidthColuna.put(5, 20);
        mapPercentualWidthColuna.put(6, 17);
        mapPercentualWidthColuna.put(7, 14);

        return mapPercentualWidthColuna.get(getQuantidadeDiasPorLinha());
    }

    public Integer getPercentualWidthColunaEdicao() {
        final Map<Integer, Integer> mapPercentualWidthColuna = new HashMap<Integer, Integer>();
        mapPercentualWidthColuna.put(1, 100);
        mapPercentualWidthColuna.put(2, 50);
        mapPercentualWidthColuna.put(3, 33);
        mapPercentualWidthColuna.put(4, 25);
        mapPercentualWidthColuna.put(5, 20);
        mapPercentualWidthColuna.put(6, 17);
        mapPercentualWidthColuna.put(7, 14);

        return mapPercentualWidthColuna.get(getQuantidadeDiasPorLinhaEdicao());
    }

    public Integer getQuantidadeDias() {
        return this.viagem.getDias().size();
    }

    public Integer getQuantidadeDiasCompletarSemana() {
        if (getQuantidadeDias() < 7) {
            return 0;
        }
        return 7 - getQuantidadeDiasUltimaLinha();
    }

    public Integer getQuantidadeDiasCompletarSemanaEdicao() {
        if (getQuantidadeDias() < 7) {
            return 1;
        }
        return 7 - getQuantidadeDiasUltimaLinhaEdicao();
    }

    public Integer getQuantidadeDiasPorLinha() {
        if (getQuantidadeDias() >= 7) {
            return 7;
        }
        return getQuantidadeDias();
    }

    public Integer getQuantidadeDiasPorLinhaEdicao() {
        if (getQuantidadeDias() >= 7) {
            return 7;
        }
        return getQuantidadeDias() + 1;
    }

    public Integer getQuantidadeDiasUltimaLinha() {
        if (getQuantidadeDias() < 7) {
            return getQuantidadeDias();
        }
        return (getQuantidadeDias() % 7 == 0) ? 7 : getQuantidadeDias() % 7;
    }

    public Integer getQuantidadeDiasUltimaLinhaEdicao() {
        return (getQuantidadeDias() % 7 == 0) ? 0 : getQuantidadeDias() % 7;
    }

    public List<List<DiaViagem>> getSemanas() {
        final List<List<DiaViagem>> semanas = new ArrayList<List<DiaViagem>>();

        List<DiaViagem> itensSemana = new ArrayList<DiaViagem>();
        int countItem = 0;
        for (final DiaViagem dia : this.viagem.getDias()) {
            countItem++;

            itensSemana.add(dia);

            if ((countItem % 7 == 0) || (countItem == getQuantidadeDias())) {
                semanas.add(itensSemana);
                itensSemana = new ArrayList<DiaViagem>();
            }
        }

        return semanas;
    }

    public List<List<DiaViagem>> getSemanasEdicao() {
        final List<List<DiaViagem>> semanas = new ArrayList<List<DiaViagem>>();

        List<DiaViagem> itensSemana = new ArrayList<DiaViagem>();
        int countItem = 0;
        for (final DiaViagem dia : this.viagem.getDias()) {
            countItem++;

            itensSemana.add(dia);

            if ((countItem % 7 == 0) || (countItem == getQuantidadeDias())) {
                semanas.add(itensSemana);
                itensSemana = new ArrayList<DiaViagem>();
            }
        }

        if (countItem % 7 == 0) {
            semanas.add(new ArrayList<DiaViagem>());
        }

        return semanas;
    }
}
