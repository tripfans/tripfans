package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoOrganizacaoViagem implements EnumTypeInteger {

    EXCURSAO(3, "Excursões"),
    PACOTE_TURISTICO(2, "Pacotes Turísticos"),
    POR_CONTA_PROPRIA(1, "Eu mesmo");

    private final EnumI18nUtil util;

    TipoOrganizacaoViagem(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}
