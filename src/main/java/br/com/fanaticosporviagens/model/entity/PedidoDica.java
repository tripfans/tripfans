package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;

@SuppressWarnings("unchecked")
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_pedido_dica")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_pedido_dica", allocationSize = 1)
@Table(name = "pedido_dica")
@DynamicUpdate
public class PedidoDica extends EntidadeGeradoraAcao<Long> {

    private static final long serialVersionUID = 914027485793434104L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_que_pediu", nullable = false)
    private Usuario autor;

    @Column(name = "data_resposta")
    private Date dataResposta;

    /**
     * ID do App Request no Facebook
     * Utilizado para fazer o controle de quando o convite foi aceito
     */
    @Column(name = "id_request_facebook")
    private String idAppRequestFacebook;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_dica", nullable = true)
    private Local localDica;

    @Column(name = "mensagem", nullable = true)
    private String mensagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_quem_pedir", nullable = false)
    private Usuario praQuemPedir;

    @Column(name = "respondido", nullable = false, columnDefinition = "SET DEFAULT false")
    private Boolean respondido = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private Viagem viagem;

    public PedidoDica() {

    }

    public PedidoDica(final Usuario autor, final Usuario praQuemPedir, final Local localDica, final String mensagem) {
        super();
        this.autor = autor;
        this.praQuemPedir = praQuemPedir;
        this.localDica = localDica;
        this.mensagem = mensagem;
        this.respondido = false;
        setDataPublicacao(LocalDateTime.now());
    }

    public PedidoDica(final Usuario autor, final Usuario praQuemPedir, final Viagem viagem, final String facebookAppRequestId, final String mensagem) {
        super();
        this.autor = autor;
        this.praQuemPedir = praQuemPedir;
        this.viagem = viagem;
        this.idAppRequestFacebook = facebookAppRequestId;
        this.mensagem = mensagem;
        this.respondido = false;
        setDataPublicacao(LocalDateTime.now());
    }

    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.localDica;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public Date getDataResposta() {
        return this.dataResposta;
    }

    @Override
    public Usuario getDestinatario() {
        return this.praQuemPedir;
    }

    public String getIdAppRequestFacebook() {
        return this.idAppRequestFacebook;
    }

    public Local getLocalDica() {
        return this.localDica;
    }

    public String getMensagem() {
        return this.mensagem;
    }

    public Usuario getPraQuemPedir() {
        return this.praQuemPedir;
    }

    /**
     * @deprecated Use {@link #getAutor()} instead
     */
    @Deprecated
    public Usuario getQuePediu() {
        return getAutor();
    }

    public Boolean getRespondido() {
        return this.respondido;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setDataResposta(final Date dataResposta) {
        this.dataResposta = dataResposta;
    }

    public void setIdAppRequestFacebook(final String idAppRequestFacebook) {
        this.idAppRequestFacebook = idAppRequestFacebook;
    }

    public void setLocalDica(final Local localDica) {
        this.localDica = localDica;
    }

    public void setPraQuemPedir(final Usuario praQuemPedir) {
        this.praQuemPedir = praQuemPedir;
    }

    public void setRespondido(final Boolean respondido) {
        this.respondido = respondido;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }

}
