package br.com.fanaticosporviagens.model.entity;

import org.springframework.context.i18n.LocaleContextHolder;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum AvaliacaoQualidade implements EnumTypeInteger {

    BOM(4, 4, new String[] { "Bom" }, new String[] { "Gostei da experiência. Valeu a pena." }),
    EXCELENTE(5, 5, new String[] { "Excelente" }, new String[] { "Realmente foi muito bom! Vou voltar com certeza!" }),
    NAO_SE_APLICA(0, 0, new String[] { "Não se Aplica" }, new String[] { "Não tenho como avaliar este aspecto." }),
    PESSIMO(1, 1, new String[] { "Péssimo" }, new String[] { "Foi muito ruim. Não recomendo a ninguém." }),
    REGULAR(3, 3, new String[] { "Regular" }, new String[] { "Foi satisfatório." }),
    RUIM(2, 2, new String[] { "Ruim" }, new String[] { "Poderia ser melhor. Não volto aqui." });

    public static AvaliacaoQualidade getAvaliacaoQualidadePorNota(final Integer nota) {
        for (final AvaliacaoQualidade qualidade : AvaliacaoQualidade.values()) {
            if (qualidade.getNota().equals(nota)) {
                return qualidade;
            }
        }
        throw new IllegalArgumentException("Nota de avaliação de qualidade inválida! (" + nota + ").");
    }

    public static AvaliacaoQualidade getEnumPorCodigo(final Integer codigo) {
        for (final AvaliacaoQualidade qualidade : values()) {
            if (qualidade.codigo == codigo) {
                return qualidade;
            }
        }
        throw new IllegalArgumentException("Código de avaliação de qualidade inválido! (" + codigo + ")");
    }

    private final Integer codigo;

    private final String[] descricoes;

    private final Integer nota;

    private final String[] textosExplicativos;

    AvaliacaoQualidade(final Integer codigo, final Integer nota, final String[] descricoes, final String[] textosExplicativos) {
        this.codigo = codigo;
        this.descricoes = descricoes;
        this.textosExplicativos = textosExplicativos;
        this.nota = nota;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        if (LocaleContextHolder.getLocale().toString().equals("pt_BR")) {
            return this.descricoes[0];
        }
        return null;
    }

    public Integer getNota() {
        return this.nota;
    }

    public String getTextoExplicativo() {
        if (LocaleContextHolder.getLocale().toString().equals("pt_BR")) {
            return this.textosExplicativos[0];
        }
        return null;
    }

    public boolean isAplicavel() {
        return !this.equals(AvaliacaoQualidade.NAO_SE_APLICA);
    }

}
