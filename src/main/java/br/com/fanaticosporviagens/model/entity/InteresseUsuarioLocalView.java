package br.com.fanaticosporviagens.model.entity;

public class InteresseUsuarioLocalView {

    private Long idUsuario;

    private Integer quantidadeDesejaIr;

    private Integer quantidadeFavorito;

    private Integer quantidadeImperdivel;

    private Integer quantidadeIndica;

    private Integer quantidadeJaFoi;

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public Integer getQuantidadeDesejaIr() {
        return this.quantidadeDesejaIr;
    }

    public Integer getQuantidadeFavorito() {
        return this.quantidadeFavorito;
    }

    public Integer getQuantidadeImperdivel() {
        return this.quantidadeImperdivel;
    }

    public Integer getQuantidadeIndica() {
        return this.quantidadeIndica;
    }

    public Integer getQuantidadeJaFoi() {
        return this.quantidadeJaFoi;
    }

    public void setIdUsuario(final Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setQuantidadeDesejaIr(final Integer quantidadeDesejaIr) {
        this.quantidadeDesejaIr = quantidadeDesejaIr;
    }

    public void setQuantidadeFavorito(final Integer quantidadeFavorito) {
        this.quantidadeFavorito = quantidadeFavorito;
    }

    public void setQuantidadeImperdivel(final Integer quantidadeImperdivel) {
        this.quantidadeImperdivel = quantidadeImperdivel;
    }

    public void setQuantidadeIndica(final Integer quantidadeIndica) {
        this.quantidadeIndica = quantidadeIndica;
    }

    public void setQuantidadeJaFoi(final Integer quantidadeJaFoi) {
        this.quantidadeJaFoi = quantidadeJaFoi;
    }

}
