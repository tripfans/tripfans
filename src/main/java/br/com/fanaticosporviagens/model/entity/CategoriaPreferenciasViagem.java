package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;


public enum CategoriaPreferenciasViagem {
    COMPANHIA(1, "companhia"),
    ESTILO(2, "estilo"),
    HOSPEDAGEM(3, "hospedagem"),
    MEIO_TRANPORTE(4, "meioTransporte"),
    PERIODO(5, "periodo");

    public int codigo;

    public String nome;

    CategoriaPreferenciasViagem(final int codigo, final String nome) {
        this.codigo = codigo;
        this.nome = nome;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.nome;
    }

    public List<TipoPreferenciasViagem> getTiposPreferencias() {
        final List<TipoPreferenciasViagem> lista = new ArrayList<TipoPreferenciasViagem>();
        for (final TipoPreferenciasViagem tiposPrefencias : TipoPreferenciasViagem.values()) {
            if (tiposPrefencias.getCategoria().equals(this)) {
                lista.add(tiposPrefencias);
            }
        }
        return lista;
    }
}