package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Embeddable
public class RecomendacaoLocalPedidoDicaId implements Serializable {
    private static final long serialVersionUID = 6854699580756870658L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_recomendado")
    Local localRecomendado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_dica")
    PedidoDica pedidoDica;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_recomendou")
    Usuario usuarioRecomendou;

    public Local getLocalRecomendado() {
        return this.localRecomendado;
    }

    public PedidoDica getPedidoDica() {
        return this.pedidoDica;
    }

    public Usuario getUsuarioRecomendou() {
        return this.usuarioRecomendou;
    }

    public void setLocalRecomendado(final Local localRecomendado) {
        this.localRecomendado = localRecomendado;
    }

    public void setPedidoDica(final PedidoDica pedidoDica) {
        this.pedidoDica = pedidoDica;
    }

    public void setUsuarioRecomendou(final Usuario usuarioRecomendou) {
        this.usuarioRecomendou = usuarioRecomendou;
    }

}