package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class CoordenadaGeografica implements Serializable {

    public class Perimetro {

        private final Double latitudeNordeste;

        private final Double latitudeSudoeste;

        private final Double longitudeNordeste;

        private final Double longitudeSudoeste;

        public Perimetro(final Double latitude, final Double longitude, final Double espaco) {
            this.latitudeNordeste = moverCoordenadaNordeste(latitude, espaco);
            this.longitudeNordeste = moverCoordenadaNordeste(longitude, espaco);
            this.latitudeSudoeste = moverCoordenadaSudoeste(latitude, espaco);
            this.longitudeSudoeste = moverCoordenadaSudoeste(longitude, espaco);
        }

        public Double getLatitudeNordeste() {
            return this.latitudeNordeste;
        }

        public Double getLatitudeSudoeste() {
            return this.latitudeSudoeste;
        }

        public Double getLongitudeNordeste() {
            return this.longitudeNordeste;
        }

        public Double getLongitudeSudoeste() {
            return this.longitudeSudoeste;
        }

        private Double moverCoordenadaNordeste(final Double coordenada, final Double espaco) {
            Double retorno = 0D;
            if (coordenada != null) {
                if (coordenada < 0) {
                    retorno = coordenada + espaco;
                } else {
                    retorno = coordenada - espaco;
                }
            }
            return retorno;
        }

        private Double moverCoordenadaSudoeste(final Double coordenada, final Double espaco) {
            Double retorno = 0D;
            if (coordenada != null) {
                if (coordenada < 0) {
                    retorno = coordenada - espaco;
                } else {
                    retorno = coordenada + espaco;
                }
            }
            return retorno;
        }

    }

    private static final long serialVersionUID = -7653476731145445633L;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    public CoordenadaGeografica() {
    }

    public CoordenadaGeografica(final Double latitude, final Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public Perimetro getPerimetro(final Double espaco) {
        return new Perimetro(this.latitude, this.longitude, espaco);
    }

    public Perimetro getPerimetroPadrao() {
        return new Perimetro(this.latitude, this.longitude, 0.03);
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

}
