package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "noticia_sobre_local_para_usuario", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_avaliacao_publicada",
        "id_dica_publicada", "id_usuario_interessado" }) })
public class NoticiaSobreLocalParaUsuario extends NoticiaSobreLocal {

    private static final long serialVersionUID = 7314545179031826281L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_interessado", nullable = false)
    private Usuario usuarioInteressadoNaNoticia;

    public NoticiaSobreLocalParaUsuario() {
        super();
    }

    public NoticiaSobreLocalParaUsuario(final Avaliacao avaliacaoPublicada, final Usuario usuarioInteressado) {
        super(avaliacaoPublicada);
        this.usuarioInteressadoNaNoticia = usuarioInteressado;
    }

    public NoticiaSobreLocalParaUsuario(final Dica dicaPublicada, final Usuario usuarioInteressado) {
        super(dicaPublicada);
        this.usuarioInteressadoNaNoticia = usuarioInteressado;
    }

    public Usuario getUsuarioInteressadoNaNoticia() {
        return this.usuarioInteressadoNaNoticia;
    }

    public void setUsuarioInteressadoNaNoticia(final Usuario usuarioInteressadoNaNoticia) {
        this.usuarioInteressadoNaNoticia = usuarioInteressadoNaNoticia;
    }
}
