package br.com.fanaticosporviagens.model.entity;

import java.io.IOException;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.annotation.constraint.FotoValida;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;
import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;

/**
 * Representa os metadados de uma foto.
 *
 * @author André Thiago
 *
 */
@Entity
@org.hibernate.annotations.Entity(dynamicUpdate = true)
@Table(name = "foto")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_foto")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_foto", allocationSize = 1)
@TypeDefs(value = { @TypeDef(name = "TipoArmazenamento", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto") }) })
public class Foto extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel {

    public static final String ALBUM_PHOTO_DIR = "usuarios/albuns";

    public static final String ATIVIDADE_PHOTO_DIR = "viagens/%d/atividades";

    public static final String ATTRACTION_ANONYMOUS_PHOTO = "atracao";

    public static final String AVALIACAO_PHOTO_DIR = "avaliacoes";

    public static final String FEMALE_ANONYMOUS_PHOTO = "blank-female";

    private static final String FOTO_PERFIL = "FotoPerfil";

    public static final String GEOLOCAL_ANONYMOUS_PHOTO = "localGeografico";

    public static final String HOTEL_ANONYMOUS_PHOTO = "hotel";

    public static final String JPEG_EXTENSION = ".jpg";

    public static final String LOCAL_PHOTO_DIR = "locais";

    public static final String MALE_ANONYMOUS_PHOTO = "blank-male";

    public static final int MAX_FOTO_SIZE_MB = 4;

    public static final String RESTAURANT_ANONYMOUS_PHOTO = "restaurante";

    private static final long serialVersionUID = 6698634482480838949L;

    public static final String USUARIO_PHOTO_DIR = "usuarios";

    public static final String VIAGEM_PHOTO_DIR = "viagens";

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_album", nullable = true)
    private Album album;

    @Column(name = "altura_album")
    private Integer alturaAlbum;

    @Column(name = "altura_big")
    private Integer alturaBig;

    @Column(name = "altura_small")
    private Integer alturaSmall;

    /**
     * Para identificar se o usuario está utilizando a foto de anonimo
     */
    @Column(name = "anonima")
    private Boolean anonima = false;

    /**
     * Indica se a foto foi aprovada com base nos termos de uso
     * A foto é aprovada automaticamente. Caso haja problema, esta deve ser alterada para 'não aprovada'
     */
    @Column(name = "aprovada")
    private Boolean aprovada = true;

    @Transient
    @FotoValida(max = MAX_FOTO_SIZE_MB)
    private MultipartFile arquivoFoto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_atividade", nullable = true)
    private AtividadePlano atividadePlano;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    /**
     * Nome do usuario/autor da foto no site ou serviço da qual foi retirada
     */
    @Column(name = "autor_fonte_imagem")
    private String autorFonteImagem;

    /*@Column(name = "nome_usuario_foto_500px")
    private String nomeUsuarioFoto500px;*/

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @Transient
    private byte[] bytes;

    /**
     * Data em que a foto foi retirada
     */
    @Column(name = "data")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate data;

    @Column(name = "descricao")
    private String descricao;

    /**
     * Indica se a foto será exibida na home
     */
    @Column(name = "destaque")
    private Boolean destaque;

    /**
     * Indica se a foto será exibida na página do local
     */
    @Column(name = "destaque_local")
    private Boolean destaqueLocal;

    /**
     * Nome do site ou serviço do qual a imagem foi retirada
     */
    @Column(name = "fonte_imagem")
    private String fonteImagem;

    @Column(name = "foto_500px", columnDefinition = "BOOLEAN DEFAULT FALSE")
    private boolean foto500px = false;

    @Column(name = "id_album", insertable = false, updatable = false)
    private Long idAlbum;

    @Column(name = "id_atividade", insertable = false, updatable = false)
    private Long idAtividade;

    @Column(name = "id_avaliacao", insertable = false, updatable = false)
    private Long idAvaliacao;

    /**
     * Id da Foto no Facebook, caso tenha sido importada do face
     */
    @Column(name = "id_facebook")
    private String idFacebook;

    @Column(name = "id_foto_500px")
    private String idFoto500px;

    @Column(name = "id_usuario", insertable = false, updatable = false)
    private Long idUsuario;

    @Column(name = "id_usuario_foto_500px")
    private String idUsuarioFoto500px;

    @Column(name = "id_viagem", insertable = false, updatable = false)
    private Long idViagem;

    @Column(name = "importada_facebook")
    private Boolean importadaFacebook = false;

    @Column(name = "largura_album")
    private Integer larguraAlbum;

    @Column(name = "largura_big")
    private Integer larguraBig;

    @Column(name = "largura_small")
    private Integer larguraSmall;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = true)
    private Local local;

    @Column(name = "motivo_nao_aprovacao")
    private String motivoNaoAprovacao;

    /**
     * Posição (ordem) da foto no album
     */
    @Column
    private Integer ordem;

    @Transient
    private String originalFileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private PlanoViagem planoViagem;

    @Column(name = "tipo_armazenamento")
    @Type(type = "TipoArmazenamento")
    private TipoArmazenamentoFoto tipoArmazenamento = TipoArmazenamentoFoto.LOCAL;

    /**
     * URL ou link para o perfil do usuario no site ou serviço da qual foi retirada
     */
    @Column(name = "url_autor_imagem")
    private String urlAutorImagem;

    @Column(name = "url_crop")
    private String urlCrop;

    @Column(name = "url_externa_album")
    private String urlExternaAlbum;

    @Column(name = "url_externa_big")
    private String urlExternaBig;

    @Column(name = "url_externa_small")
    private String urlExternaSmall;

    /**
     * URL ou link para a foto no site ou serviço da qual foi retirada
     */
    @Column(name = "url_fonte_imagem")
    private String urlFonteImagem;

    @Column(name = "url_local_album")
    private String urlLocalAlbum;

    @Column(name = "url_local_big")
    private String urlLocalBig;

    @Column(name = "url_local_small")
    private String urlLocalSmall;

    @Column(name = "url_path", unique = true)
    private String urlPath;

    /**
     * Indica se é foto de perfil
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    @Column(name = "tipo_visibilidade")
    private TipoVisibilidade visibilidade = TipoVisibilidade.PUBLICO;

    public void definirFotoPadraoAnonima() {
        this.anonima = true;
        String urlAlbum = null, urlBig = null, urlSmall = null;
        if (this.isFotoDeUsuario()) {
            urlAlbum = getUrlAnonimaAlbumUsuario();
            urlBig = getUrlAnonimaBigAlbumUsuario();
            urlSmall = getUrlAnonimaSmallUsuario();
            this.usuario.setUrlFotoAlbum(urlAlbum);
            this.usuario.setUrlFotoBig(urlBig);
            this.usuario.setUrlFotoSmall(urlSmall);
        } else if (this.isFotoDeLocal()) {
            String tipoLocal = null;
            if (this.local.isTipoAtracao()) {
                tipoLocal = "atracao";
            } else if (this.local.isTipoCidade()) {
                tipoLocal = "cidade";
            } else if (this.local.isTipoHotel()) {
                tipoLocal = "hotel";
            } else if (this.local.isTipoRestaurante()) {
                tipoLocal = "restaurante";
            }
            urlAlbum = "/resources/images/locais/" + tipoLocal + "_album.png";
            urlBig = "/resources/images/locais/" + tipoLocal + "_big.png";
            urlSmall = "/resources/images/locais/" + tipoLocal + "_small.png";
            this.local.setUrlFotoAlbum(urlAlbum);
            this.local.setUrlFotoBig(urlBig);
            this.local.setUrlFotoSmall(urlSmall);
        }
        this.setUrlLocalAlbum(urlAlbum);
        this.setUrlLocalBig(urlBig);
        this.setUrlLocalSmall(urlSmall);
    }

    public void definirNovaFotoCapaLocal() {
        final String end = this.getDiretorio() + (this.urlPath != null ? this.urlPath : FOTO_PERFIL);
        this.setBaseLocalUrl(System.getProperty(AppEnviromentConstants.IMAGES_SERVER_URL) + "/" + end);
    }

    public void definirNovaFotoPerfilUsuario() {
        this.setBaseLocalUrl(System.getProperty(AppEnviromentConstants.IMAGES_SERVER_URL) + "/" + this.getDiretorio() + FOTO_PERFIL);
    }

    public Album getAlbum() {
        return this.album;
    }

    public String getAlt() {
        final StringBuilder alt = new StringBuilder();

        if (this.local != null) {
            alt.append("Foto de ");
            alt.append(this.local.getNomeCompleto());
        }
        if (this.autor != null) {
            alt.append(" por ");
            alt.append(this.autor.getDisplayName());
        }
        if (this.data != null) {
            alt.append(" em ");
            alt.append(DateTimeFormat.forPattern("dd/MM/yyyy").print(this.data));
        }
        return alt.toString().trim();
    }

    public Integer getAlturaAlbum() {
        return this.alturaAlbum;
    }

    public Integer getAlturaBig() {
        return this.alturaBig;
    }

    public Integer getAlturaSmall() {
        return this.alturaSmall;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.getObjetoAlvo();
    }

    public Boolean getAnonima() {
        return this.anonima;
    }

    public Boolean getAprovada() {
        return this.aprovada;
    }

    public MultipartFile getArquivoFoto() {
        return this.arquivoFoto;
    }

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public String getAutorFonteImagem() {
        return this.autorFonteImagem;
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public LocalDate getData() {
        return this.data;
    }

    public Date getDataPostagem() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public String getDescricaoAlvo() {
        // TODO ver o nome que será retornado
        return null;
    }

    public Boolean getDestaque() {
        return this.destaque;
    }

    public Boolean getDestaqueLocal() {
        return this.destaqueLocal;
    }

    /**
     * Retorna o diretório no qual a foto está armazenada.
     *
     * @return
     */
    public String getDiretorio() {
        String caminhoFoto = "";
        if (this.isFotoDeAvaliacao()) {
            caminhoFoto += AVALIACAO_PHOTO_DIR + "/" + this.avaliacao.getId();
        } else if (this.isFotoDeLocal()) {
            caminhoFoto += LOCAL_PHOTO_DIR + "/" + this.local.getId();
        } else if (this.isFotoDeUsuario()) {
            caminhoFoto += USUARIO_PHOTO_DIR + "/" + this.getUsuario().getId();
        } else if (this.isFotoDeViagem()) {
            caminhoFoto += VIAGEM_PHOTO_DIR + "/" + this.getViagem().getId();
        } else if (this.isFotoDeAtividade()) {
            caminhoFoto += String.format(ATIVIDADE_PHOTO_DIR, this.getViagem().getId()) + "/" + this.getAtividade().getId();
        } else if (this.isFotoDeAlbum()) {
            caminhoFoto += ALBUM_PHOTO_DIR + "/" + this.album.getId();
        }
        return caminhoFoto + "/";
    }

    private String getFileExtension() {
        return this.originalFileName.substring(this.originalFileName.lastIndexOf(".") + 1);
    }

    public String getFonteImagem() {
        return this.fonteImagem;
    }

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public String getIdFoto500px() {
        return this.idFoto500px;
    }

    public String getIdUsuarioFoto500px() {
        return this.idUsuarioFoto500px;
    }

    public Boolean getImportadaFacebook() {
        return this.importadaFacebook;
    }

    public Integer getLarguraAlbum() {
        return this.larguraAlbum;
    }

    public Integer getLarguraBig() {
        return this.larguraBig;
    }

    public Integer getLarguraSmall() {
        return this.larguraSmall;
    }

    public Local getLocal() {
        return this.local;
    }

    public String getMotivoNaoAprovacao() {
        return this.motivoNaoAprovacao;
    }

    public String getNome() {
        return this.urlPath;
    }

    /**
     * Retorna o caminho completo do arquivo da foto incluindo seu nome.
     *
     * @return
     */
    public String getNomeCompleto() {
        if (this.anonima || this.isFotoDePerfilUsuario()) {
            return this.getDiretorio() + FOTO_PERFIL + JPEG_EXTENSION;
        }
        return this.getNomeCompletoSemExtensao() + JPEG_EXTENSION;
    }

    public String getNomeCompleto(final ImageFormat formatoImagem) {
        return this.getNomeCompletoSemExtensao(formatoImagem) + JPEG_EXTENSION;
    }

    public String getNomeCompletoSemExtensao() {
        if (this.anonima || this.isFotoDePerfilUsuario()) {
            return this.getDiretorio() + FOTO_PERFIL;
        }
        return this.getDiretorio() + this.getNome();
    }

    public String getNomeCompletoSemExtensao(final ImageFormat formatoImagem) {
        return this.getNomeCompletoSemExtensao() + formatoImagem.getSuffix();
    }

    @SuppressWarnings("unchecked")
    @Transient
    protected <T extends AlvoAcao> T getObjetoAlvo() {
        if (this.isFotoDePerfil()) {
            return (T) this.usuario;
        } else if (this.isFotoDeLocal()) {
            return (T) this.local;
        }
        return null;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    @Override
    public String getPrefix() {
        return "f";
    }

    public String getPrefixoFoto() {
        String prefixo = "";
        if (this.isFotoDeAtividade()) {
            prefixo = this.getAtividade().getUrlPath();
        } else if (this.isFotoDeAvaliacao()) {
            prefixo = this.getAvaliacao().getUrlPath();
        } else if (this.isFotoDeLocal()) {
            prefixo = this.getLocal().getUrlPath();
        } else if (this.isFotoDeViagem()) {
            prefixo = "capa-planoViagem-" + (this.getViagem().getId());
        } else if (this.isFotoDeUsuario()) {
            prefixo = this.getUsuario().getUsername();
        }
        return prefixo;
    }

    public TipoArmazenamentoFoto getTipoArmazenamento() {
        return this.tipoArmazenamento;
    }

    public String getUrlAlbum() {
        if (TipoArmazenamentoFoto.LOCAL.equals(this.getTipoArmazenamento())) {
            return this.getUrlLocalAlbum();
        }
        return this.getUrlExternaAlbum();
    }

    private String getUrlAnonimaAlbumUsuario() {
        if (this.usuario.isFeminino()) {
            return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_ALBUM;
        }
        return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_ALBUM;
    }

    private String getUrlAnonimaBigAlbumUsuario() {
        if (this.usuario.isFeminino()) {
            return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_BIG;
        }
        return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_BIG;
    }

    private String getUrlAnonimaSmallUsuario() {
        if (this.usuario.isFeminino()) {
            return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_FEMININA_SMALL;
        }
        return System.getProperty(AppEnviromentConstants.SERVER_URL) + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_SMALL;
    }

    public String getUrlAutorImagem() {
        return this.urlAutorImagem;
    }

    public String getUrlAvatar() {
        if (TipoArmazenamentoFoto.LOCAL.equals(this.getTipoArmazenamento())) {
            return this.getUrlLocalSmall();
        }
        return this.getUrlExternaSmall();
    }

    public String getUrlBig() {
        if (TipoArmazenamentoFoto.LOCAL.equals(this.getTipoArmazenamento())) {
            return this.getUrlLocalBig();
        }
        return this.getUrlExternaBig();
    }

    public String getUrlCarrosel() {
        return this.getUrlAlbum();
    }

    public String getUrlCrop() {
        return this.urlCrop;
    }

    public String getUrlExternaAlbum() {
        return this.urlExternaAlbum;
    }

    public String getUrlExternaBig() {
        return this.urlExternaBig;
    }

    public String getUrlExternaSmall() {
        return this.urlExternaSmall;
    }

    public String getUrlFonteImagem() {
        return this.urlFonteImagem;
    }

    @Transient
    public String getUrlFoto500px() {
        if (isTipoArmazenamentoExterno() && isFoto500px()) {
            return getUrlExternaAlbum();
        }
        return null;
    }

    public String getUrlLocalAlbum() {
        return this.urlLocalAlbum;
    }

    public String getUrlLocalBig() {
        return this.urlLocalBig;
    }

    public String getUrlLocalSmall() {
        return this.urlLocalSmall;
    }

    public String getUrlOriginal() {
        return this.getUrlBig();
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public String getUrlPerfil() {
        return this.getUrlAlbum();
    }

    public String getUrlSmall() {
        if (TipoArmazenamentoFoto.LOCAL.equals(this.getTipoArmazenamento())) {
            return this.getUrlLocalSmall();
        }
        return this.getUrlExternaSmall();
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public PlanoViagem getViagem() {
        return this.planoViagem;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    public boolean isFoto500px() {
        return this.foto500px;
    }

    private boolean isFotoDeAlbum() {
        return this.idAlbum != null && !isFotoDeViagem() && !isFotoDeAtividade();
    }

    private boolean isFotoDeAtividade() {
        return this.idViagem != null && this.idAtividade != null;
    }

    /**
     * Verifica se a foto é de uma {@link Avaliacao}.
     *
     * @return
     */
    public boolean isFotoDeAvaliacao() {
        return this.idAvaliacao != null;
    }

    private boolean isFotoDeCapaLocal() {
        return isFotoDeLocal() && isFotoDePerfil();
    }

    /**
     * Verifica se a foto é de um Local.
     *
     * @return
     */

    public boolean isFotoDeLocal() {
        return (this.getLocal() != null && getLocal().getId() != null) && !isFotoDeAtividade() && !isFotoDeViagem();
    }

    private boolean isFotoDePerfil() {
        if (this.isFotoDeUsuario()) {
            return this.usuario.getFoto() != null && this.usuario.getFoto().equals(this);
        } else if (this.local != null && this.local.getId() != null && this.local.getFotoPadrao() != null) {
            return this.local.getFotoPadrao().equals(this);
        }
        return false;
    }

    private boolean isFotoDePerfilUsuario() {
        return isFotoDeUsuario() && isFotoDePerfil();
    }

    public boolean isFotoDeUsuario() {
        return this.idUsuario != null || this.usuario != null;
    }

    private boolean isFotoDeViagem() {
        return this.idViagem != null && this.idAtividade == null;
    }

    public boolean isJPG() {
        if (this.anonima) {
            return true;
        }
        final String extension = this.getFileExtension();
        return ArrayUtils.contains(new String[] { "jpg", "jpeg" }, extension.toLowerCase());
    }

    public boolean isTipoArmazenamentoExterno() {
        return TipoArmazenamentoFoto.EXTERNO.equals(this.getTipoArmazenamento());
    }

    public boolean isTipoArmazenamentoLocal() {
        return TipoArmazenamentoFoto.LOCAL.equals(this.getTipoArmazenamento());
    }

    public void setAlbum(final Album album) {
        this.album = album;
        this.idAlbum = album.getId();
    }

    public void setAlturaAlbum(final Integer alturaAlbum) {
        this.alturaAlbum = alturaAlbum;
    }

    public void setAlturaBig(final Integer alturaBig) {
        this.alturaBig = alturaBig;
    }

    public void setAlturaSmall(final Integer alturaSmall) {
        this.alturaSmall = alturaSmall;
    }

    public void setAnonima(final Boolean anonima) {
        this.anonima = anonima;
        if (this.tipoArmazenamento == null) {
            this.tipoArmazenamento = TipoArmazenamentoFoto.LOCAL;
        }
        if (this.anonima && this.getId() == null) {
            this.definirFotoPadraoAnonima();
        }
    }

    public void setAprovada(final Boolean aprovada) {
        this.aprovada = aprovada;
    }

    public void setArquivoFoto(final byte[] arquivoFoto, final String nomeArquivo) throws IOException {
        this.bytes = arquivoFoto;
        this.originalFileName = nomeArquivo;
    }

    public void setArquivoFoto(final MultipartFile arquivoFoto) throws IOException {
        this.arquivoFoto = arquivoFoto;
        this.bytes = arquivoFoto.getBytes();
        this.originalFileName = arquivoFoto.getOriginalFilename();
    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
        this.idAtividade = atividadePlano.getId();
        if (atividadePlano != null && atividadePlano.getViagem() != null) {
            this.setViagem(atividadePlano.getViagem());
            this.setAutor(atividadePlano.getViagem().getCriador());
            if (atividadePlano.getLocal() != null) {
                this.setLocal(atividadePlano.getLocal());
            } else if (atividadePlano.getDestinoViagem() != null) {
                this.setLocal(atividadePlano.getDestinoViagem().getDestino());
            }
            if (atividadePlano.getDataInicio() != null) {
                this.setData(atividadePlano.getDataInicio());
            }
        }
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setAutorFonteImagem(final String autorFonteImagem) {
        this.autorFonteImagem = autorFonteImagem;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
        this.idAvaliacao = avaliacao.getId();
        if (avaliacao != null) {
            this.setAutor(avaliacao.getAutor());
            this.setLocal(avaliacao.getLocalAvaliado());
        }
    }

    private void setBaseLocalUrl(final String baseUrl) {
        this.setUrlLocalAlbum(baseUrl + ImageFormat.ALBUM.getSuffix() + JPEG_EXTENSION);
        this.setUrlLocalBig(baseUrl + ImageFormat.BIG.getSuffix() + JPEG_EXTENSION);
        this.setUrlLocalSmall(baseUrl + ImageFormat.SMALL.getSuffix() + JPEG_EXTENSION);
        if (this.isFotoDeUsuario()) {
            this.setUrlCrop(baseUrl + ImageFormat.CROP.getSuffix() + JPEG_EXTENSION);
        }
    }

    public void setBytes(final byte[] bytes) {
        this.bytes = bytes;
    }

    public void setData(final LocalDate data) {
        this.data = data;
    }

    public void setDataPostagem(final Date dataPostagem) {
        if (dataPostagem != null) {
            this.setDataCriacao(new LocalDateTime(dataPostagem));
        }
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDestaque(final Boolean destaque) {
        this.destaque = destaque;
    }

    public void setDestaqueLocal(final Boolean destaqueLocal) {
        this.destaqueLocal = destaqueLocal;
    }

    public void setFonteImagem(final String fonteImagem) {
        this.fonteImagem = fonteImagem;
    }

    public void setFoto500px(final boolean foto500px) {
        this.foto500px = foto500px;
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setIdFoto500px(final String idFoto500px) {
        // configurando a url para a foto do 500px
        if (idFoto500px != null) {
            setUrlFonteImagem("http://500px.com/photo/" + idFoto500px);
        }
        this.idFoto500px = idFoto500px;
    }

    public void setIdUsuarioFoto500px(final String idUsuarioFoto500px) {
        // configurando a url para o perfil do usuario autor da foto do 500px
        if (idUsuarioFoto500px != null) {
            setUrlAutorImagem("http://500px.com/" + idUsuarioFoto500px);
        }
        this.idUsuarioFoto500px = idUsuarioFoto500px;
    }

    public void setImportadaFacebook(final Boolean importadaFacebook) {
        this.importadaFacebook = importadaFacebook;
    }

    public void setLarguraAlbum(final Integer larguraAlbum) {
        this.larguraAlbum = larguraAlbum;
    }

    public void setLarguraBig(final Integer larguraBig) {
        this.larguraBig = larguraBig;
    }

    public void setLarguraSmall(final Integer larguraSmall) {
        this.larguraSmall = larguraSmall;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setMotivoNaoAprovacao(final String motivoNaoAprovacao) {
        this.motivoNaoAprovacao = motivoNaoAprovacao;
    }

    public void setOrdem(final Integer ordem) {
        this.ordem = ordem;
    }

    public void setTipoArmazenamento(final TipoArmazenamentoFoto tipoArmazenamento) {
        this.tipoArmazenamento = tipoArmazenamento;
        if (tipoArmazenamento.equals(TipoArmazenamentoFoto.LOCAL)) {
            if (this.anonima) {
                this.definirFotoPadraoAnonima();
            }
            this.setUrlExternaAlbum(null);
            this.setUrlExternaBig(null);
            this.setUrlExternaSmall(null);
        }
    }

    public void setUrlAutorImagem(final String urlAutorImagem) {
        this.urlAutorImagem = urlAutorImagem;
    }

    public void setUrlCrop(final String urlCrop) {
        this.urlCrop = urlCrop;
    }

    public void setUrlExternaAlbum(final String urlExternaAlbum) {
        this.urlExternaAlbum = urlExternaAlbum;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoAlbum(urlExternaAlbum);
        }
    }

    public void setUrlExternaBig(final String urlExternaBig) {
        this.urlExternaBig = urlExternaBig;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoBig(urlExternaBig);
        }
    }

    public void setUrlExternaSmall(final String urlExternaSmall) {
        this.urlExternaSmall = urlExternaSmall;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoSmall(urlExternaSmall);
        }
    }

    public void setUrlFonteImagem(final String urlFonteImagem) {
        this.urlFonteImagem = urlFonteImagem;
    }

    @Transient
    public void setUrlFoto500px(final String urlFoto500px) {
        if (urlFoto500px != null && urlFoto500px.indexOf("/") != -1) {
            final String urlBase = urlFoto500px.substring(0, urlFoto500px.lastIndexOf("/"));
            // Configurando as URLs para as fotos do 500px
            setUrlExternaSmall(urlBase + "/" + "1.jpg");
            setUrlExternaAlbum(urlBase + "/" + "3.jpg");
            setUrlExternaBig(urlBase + "/" + "4.jpg");
        }
    }

    public void setUrlLocalAlbum(final String urlLocalAlbum) {
        this.urlLocalAlbum = urlLocalAlbum;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoAlbum(urlLocalAlbum);
        } else if (isFotoDeCapaLocal()) {
            getLocal().setUrlFotoAlbum(urlLocalAlbum);
        }
    }

    public void setUrlLocalBig(final String urlLocalBig) {
        this.urlLocalBig = urlLocalBig;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoBig(urlLocalBig);
        } else if (isFotoDeCapaLocal()) {
            getLocal().setUrlFotoBig(urlLocalBig);
        }
    }

    public void setUrlLocalSmall(final String urlLocalSmall) {
        this.urlLocalSmall = urlLocalSmall;
        if (isFotoDePerfilUsuario()) {
            getUsuario().setUrlFotoSmall(urlLocalSmall);
        } else if (isFotoDeCapaLocal()) {
            getLocal().setUrlFotoSmall(urlLocalSmall);
        }
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
        if (!this.anonima && !this.isFotoDePerfil()) {
            this.setBaseLocalUrl(System.getProperty(AppEnviromentConstants.IMAGES_SERVER_URL) + "/" + this.getDiretorio() + "/" + urlPath);
        }
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
        this.idUsuario = usuario.getId();
        this.usuario.setFoto(this);
    }

    public void setViagem(final PlanoViagem planoViagem) {
        this.planoViagem = planoViagem;
        this.idViagem = planoViagem.getId();
        if (planoViagem != null) {
            this.setAutor(planoViagem.getCriador());
            // Antes fazia sentido pegar o destino da planoViagem. Agora são "destinosViagem"
            // Deixei comentado para o caso de dar algum problema em algum local
            // setLocal(planoViagem.getDestino());
        }
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

    public boolean temArquivo() {
        return !(this.bytes == null || this.bytes.length == 0);
    }

}
