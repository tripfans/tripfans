package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author André Thiago
 * 
 */
@Entity
@DiscriminatorValue("4")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Cidade extends LocalGeografico {

    private static final long serialVersionUID = -3017946644390571123L;

    @Column(name = "capital_estado", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean capitalEstado;

    @Column(name = "capital_pais", nullable = false, columnDefinition = "BOOLEAN DEFAULT FALSE")
    private Boolean capitalPais;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente")
    private Continente continente;

    @Deprecated
    @Column(name = "nm_local_continente", nullable = true)
    private String continenteNome;

    @Deprecated
    @Column(name = "url_path_local_continente", nullable = true)
    private String continenteUrlPath;

    @Embedded
    private final CriteriosRankingCidade criteriosRanking = new CriteriosRankingCidade();

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_estado")
    @JsonIgnore
    private Estado estado;

    @Deprecated
    @Column(name = "nm_local_estado", nullable = true)
    private String estadoNome;

    @Deprecated
    @Column(name = "sg_local_estado", nullable = true)
    private String estadoSigla;

    @Deprecated
    @Column(name = "url_path_local_estado", nullable = true)
    private String estadoUrlPath;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_pais")
    @JsonIgnore
    private Pais pais;

    @Deprecated
    @Column(name = "nm_local_pais", nullable = true)
    private String paisNome;

    @Deprecated
    @Column(name = "url_path_local_pais", nullable = true)
    private String paisUrlPath;

    @Column(name = "ranking_nacional", nullable = true)
    private Integer rankingNacional;

    @Deprecated
    @Column(name = "sigla_iso_pais", nullable = true)
    private String siglaPais;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Cidade other = (Cidade) obj;
        if (this.estado == null) {
            if (other.estado != null)
                return false;
        } else if (!this.estado.equals(other.estado))
            return false;
        if (this.pais == null) {
            if (other.pais != null)
                return false;
        } else if (!this.pais.equals(other.pais))
            return false;
        return true;
    }

    public Boolean getCapitalEstado() {
        return this.capitalEstado;
    }

    public Boolean getCapitalPais() {
        return this.capitalPais;
    }

    public Continente getContinente() {
        return this.continente;
    }

    public String getContinenteNome() {
        return this.continenteNome;
    }

    public String getContinenteUrl() {
        return UrlLocalResolver.getUrl(LocalType.CONTINENTE, getContinenteUrlPath());
    }

    public String getContinenteUrlPath() {
        return this.continenteUrlPath;
    }

    public Estado getEstado() {
        return this.estado;
    }

    public String getEstadoNome() {
        return this.estadoNome;
    }

    public String getEstadoSigla() {
        return this.estadoSigla;
    }

    public String getEstadoUrl() {
        return UrlLocalResolver.getUrl(LocalType.ESTADO, getEstadoUrlPath());
    }

    public String getEstadoUrlPath() {
        return this.estadoUrlPath;
    }

    @Override
    public List<Local> getLocaisInteressados() {
        final List<Local> interessados = new ArrayList<Local>();

        interessados.add(this);
        if (this.estado != null) {
            interessados.add(this.estado);
        }
        if (this.pais != null) {
            interessados.add(this.pais);
        }
        if (this.continente != null) {
            interessados.add(this.continente);
        }

        return interessados;
    }

    public String getNomeComEstadoPais() {
        String nomeComEstadoPais = getNome();
        if (this.getEstadoNome() != null) {
            nomeComEstadoPais += ", " + this.getEstadoNome();
            if (this.getPaisNome() != null) {
                nomeComEstadoPais += ", " + this.getPaisNome();
            }
        }
        return nomeComEstadoPais;
    }

    public String getNomeComPais() {
        String nomeComPais = getNome();
        if (this.getPaisNome() != null) {
            nomeComPais += ", " + this.getPaisNome();
        }
        return nomeComPais;
    }

    @Override
    public String getNomeComSiglaEstado() {
        String nomeComSiglaEstado = getNome();
        if (this.getEstadoSigla() != null) {
            nomeComSiglaEstado += ", " + this.getEstadoSigla();
        }
        return nomeComSiglaEstado;
    }

    public String getNomeComSiglaEstadoNomePais() {
        String nomeComSiglaEstadoNomePais = getNome();
        if (this.getEstadoSigla() != null) {
            nomeComSiglaEstadoNomePais += ", " + this.getEstadoSigla();
        }
        if (this.getPaisNome() != null) {
            nomeComSiglaEstadoNomePais += ", " + this.getPaisNome();
        }
        return nomeComSiglaEstadoNomePais;
    }

    @Override
    public Local getPai() {
        if (getEstado() != null) {
            return getEstado();
        }
        return getPais();
    }

    public Pais getPais() {
        return this.pais;
    }

    public String getPaisNome() {
        return this.paisNome;
    }

    public String getPaisUrl() {
        return UrlLocalResolver.getUrl(LocalType.PAIS, getPaisUrlPath());
    }

    public String getPaisUrlPath() {
        return this.paisUrlPath;
    }

    public String getSiglaPais() {
        return this.siglaPais;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((this.estado == null) ? 0 : this.estado.hashCode());
        result = prime * result + ((this.pais == null) ? 0 : this.pais.hashCode());
        return result;
    }

    public void setCapitalEstado(final Boolean capitalEstado) {
        this.capitalEstado = capitalEstado;
    }

    public void setCapitalPais(final Boolean capitalPais) {
        this.capitalPais = capitalPais;
    }

    public void setContinente(final Continente continente) {
        this.continente = continente;
    }

    public void setContinenteNome(final String continenteNome) {
        this.continenteNome = continenteNome;
    }

    public void setContinenteUrlPath(final String continenteUrlPath) {
        this.continenteUrlPath = continenteUrlPath;
    }

    public void setEstado(final Estado estado) {
        this.estado = estado;
    }

    public void setEstadoNome(final String estadoNome) {
        this.estadoNome = estadoNome;
    }

    public void setEstadoSigla(final String estadoSigla) {
        this.estadoSigla = estadoSigla;
    }

    public void setEstadoUrlPath(final String estadoUrlPath) {
        this.estadoUrlPath = estadoUrlPath;
    }

    public void setPais(final Pais pais) {
        this.pais = pais;
    }

    public void setPaisNome(final String paisNome) {
        this.paisNome = paisNome;
    }

    public void setPaisUrlPath(final String paisUrlPath) {
        this.paisUrlPath = paisUrlPath;
    }

    public void setSiglaPais(final String siglaPais) {
        this.siglaPais = siglaPais;
    }
}
