package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * @author Carlos Nascimento
 */
@Entity
@Table(name = "preferencias_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_preferencias_viagem", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_preferencias_viagem", nullable = false, unique = true)) })
@TypeDefs(value = { @TypeDef(name = "TipoOrganizacaoViagem", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoOrganizacaoViagem") }) })
public class PreferenciasViagem extends EntidadeGeradoraAcao<Long> {

    private static final long serialVersionUID = -4352297911321368957L;

    @Embedded
    private PreferenciasViagemCompanhia companhia = new PreferenciasViagemCompanhia();

    @Column(name = "costuma_planejar_viagens")
    private Boolean costumaPlanejarViagens;

    @Embedded
    private PreferenciasViagemEstilo estilo = new PreferenciasViagemEstilo();

    @Embedded
    private PreferenciasViagemHospedagem hospedagem = new PreferenciasViagemHospedagem();

    @Embedded
    private PreferenciasViagemPeriodo periodo = new PreferenciasViagemPeriodo();

    @Embedded
    private PreferenciasViagemMeioTransporte preferenciasViagemMeioTransporte = new PreferenciasViagemMeioTransporte();

    @Column(name = "tipo_organizacao")
    @Type(type = "TipoOrganizacaoViagem")
    private TipoOrganizacaoViagem tipoOrganizacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) getUsuario();
    }

    @Override
    public Usuario getAutor() {
        return getUsuario();
    }

    public PreferenciasViagemCompanhia getCompanhia() {
        return this.companhia;
    }

    public Boolean getCostumaPlanejarViagens() {
        return this.costumaPlanejarViagens;
    }

    public PreferenciasViagemEstilo getEstilo() {
        return this.estilo;
    }

    public PreferenciasViagemHospedagem getHospedagem() {
        return this.hospedagem;
    }

    public PreferenciasViagemMeioTransporte getMeioTransporte() {
        return this.preferenciasViagemMeioTransporte;
    }

    public PreferenciasViagemPeriodo getPeriodo() {
        return this.periodo;
    }

    public TipoOrganizacaoViagem getTipoOrganizacao() {
        return this.tipoOrganizacao;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setCompanhia(final PreferenciasViagemCompanhia companhia) {
        this.companhia = companhia;
    }

    public void setCostumaPlanejarViagens(final Boolean costumaPlanejarViagens) {
        this.costumaPlanejarViagens = costumaPlanejarViagens;
    }

    public void setEstilo(final PreferenciasViagemEstilo estilo) {
        this.estilo = estilo;
    }

    public void setHospedagem(final PreferenciasViagemHospedagem hospedagem) {
        this.hospedagem = hospedagem;
    }

    public void setMeioTransporte(final PreferenciasViagemMeioTransporte preferenciasViagemMeioTransporte) {
        this.preferenciasViagemMeioTransporte = preferenciasViagemMeioTransporte;
    }

    public void setPeriodo(final PreferenciasViagemPeriodo periodo) {
        this.periodo = periodo;
    }

    public void setTipoOrganizacao(final TipoOrganizacaoViagem tipoOrganizacao) {
        this.tipoOrganizacao = tipoOrganizacao;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
