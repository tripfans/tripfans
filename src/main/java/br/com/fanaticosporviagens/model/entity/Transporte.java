package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
@Entity
// @DiscriminatorValue("5")
public abstract class Transporte<T extends DetalheTrecho> extends AtividadeLongaDuracao {

    private static final long serialVersionUID = -4502497745843903465L;

    @Column
    private String descricao;

    @Column(name = "quantidade_paradas")
    private Integer quantidadeParadas;

    // private Period tempoViagem;

    @OneToMany(mappedBy = "transporte", targetEntity = TrechoTransporte.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @OrderBy(value = "partida.data, partida.hora")
    private List<TrechoTransporte<T>> trechos = new ArrayList<TrechoTransporte<T>>();

    public boolean addTrecho(final TrechoTransporte<T> trecho) {
        if (this.trechos != null) {
            trecho.setTransporte(this);
            return this.trechos.add(trecho);
        }
        return false;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Integer getQuantidadeParadas() {
        return this.quantidadeParadas;
    }

    public TrechoTransporte<T> getTrechoFinal() {
        if (getTrechos() != null) {
            return this.trechos.get(this.trechos.size() - 1);
        }
        return null;
    }

    public TrechoTransporte<T> getTrechoInicial() {
        if (getTrechos() != null) {
            return this.trechos.get(0);
        }
        return null;
    }

    public List<TrechoTransporte<T>> getTrechos() {
        if (this.trechos != null && !this.trechos.isEmpty()) {
            Collections.sort(this.trechos);
        }
        return this.trechos;
    }

    public boolean removeTrecho(final TrechoTransporte<T> trecho) {
        if (this.trechos != null) {
            trecho.setTransporte(null);
            return this.trechos.remove(trecho);
        }
        return false;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setQuantidadeParadas(final Integer quantidadeParadas) {
        this.quantidadeParadas = quantidadeParadas;
    }

    public void setTrechos(final List<TrechoTransporte<T>> trechos) {
        this.trechos = trechos;
    }
}
