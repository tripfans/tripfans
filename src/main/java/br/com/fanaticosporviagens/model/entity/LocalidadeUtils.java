package br.com.fanaticosporviagens.model.entity;

import java.util.Comparator;

/**
 * Classe utilitaria para assuntos de Locais
 *
 * @author carlosn
 *
 */
public class LocalidadeUtils {

    public class DistanciaAtividadeLocalComparator implements Comparator<Atividade> {

        private final CoordenadaGeografica pontoCentral;

        public DistanciaAtividadeLocalComparator(final CoordenadaGeografica pontoCentral) {
            this.pontoCentral = pontoCentral;
        }

        @Override
        public int compare(final Atividade atividade1, final Atividade atividade2) {
            /*System.out.println("DISTANCIA 1 : "
                    + atividade1.getLocal().getNome()
                    + " = "
                    + ViagemService.this.localService.distanciaEntreCoordenadasGeograficas(atividade1.getLocal().getCoordenadaGeografica(),
                            this.pontoCentral));

            System.out.println("DISTANCIA 2 : "
                    + atividade2.getLocal().getNome()
                    + " = "
                    + ViagemService.this.localService.distanciaEntreCoordenadasGeograficas(atividade2.getLocal().getCoordenadaGeografica(),
                            this.pontoCentral));

            System.out.println("RESULTADO = "
                    + Double.compare(ViagemService.this.localService.distanciaEntreCoordenadasGeograficas(atividade1.getLocal()
                            .getCoordenadaGeografica(), this.pontoCentral), ViagemService.this.localService.distanciaEntreCoordenadasGeograficas(
                                    (atividade2.getLocal().getCoordenadaGeografica()), this.pontoCentral)));*/

            return Double.compare(
                    LocalidadeUtils.distanciaEntreCoordenadasGeograficas(atividade1.getLocal().getCoordenadaGeografica(), this.pontoCentral),
                    LocalidadeUtils.distanciaEntreCoordenadasGeograficas((atividade2.getLocal().getCoordenadaGeografica()), this.pontoCentral));
        }
    }

    public class DistanciaLocalParaOutroLocalComparator implements Comparator<Local> {

        private final CoordenadaGeografica pontoCentral;

        public DistanciaLocalParaOutroLocalComparator(final CoordenadaGeografica pontoCentral) {
            this.pontoCentral = pontoCentral;
        }

        @Override
        public int compare(final Local local1, final Local local2) {

            return Double.compare(LocalidadeUtils.distanciaEntreCoordenadasGeograficas(local1.getCoordenadaGeografica(), this.pontoCentral),
                    LocalidadeUtils.distanciaEntreCoordenadasGeograficas((local2.getCoordenadaGeografica()), this.pontoCentral));
        }
    }

    private static double deg2rad(final double deg) {
        return (deg * Math.PI / 180.0);
    }

    /* Calculate distance between two points in latitude
    and longitude taking into account height difference.
    If you are not interested in height difference pass 0.0.
     Uses Haversine method as its base. lat1, lon1 Start point
    lat2, lon2 End point el1 Start altitude in meters el2 End altitude
    in meters */
    public static double distance(final double lat1, final double lat2, final double lon1, final double lon2, final double el1, final double el2) {

        final int R = 6371; // Radius of the earth

        final Double latDistance = deg2rad(lat2 - lat1);
        final Double lonDistance = deg2rad(lon2 - lon1);
        final Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        final Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        final double height = el1 - el2;
        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        return Math.sqrt(distance);
    }

    public static double distanciaEntreCoordenadasGeograficas(final CoordenadaGeografica coordenadaGeografica1,
            final CoordenadaGeografica coordenadaGeografica2) {
        return distanciaEntreDoisPontos(coordenadaGeografica1.getLatitude(), coordenadaGeografica2.getLatitude(),
                coordenadaGeografica1.getLongitude(), coordenadaGeografica2.getLongitude());
    }

    public static double distanciaEntreDoisPontos(final Double lat1, final Double lat2, final Double lon1, final Double lon2) {
        if (lat1 == null || lat2 == null || lon1 == null || lon2 == null) {
            return -1;
        }
        return distance(lat1, lat2, lon1, lon2, 0, 0);
    }

    public static double distanciaEntreLocais(final Local local1, final Local local2) {
        return distanciaEntreCoordenadasGeograficas(local1.getCoordenadaGeografica(), local2.getCoordenadaGeografica());
    }

}
