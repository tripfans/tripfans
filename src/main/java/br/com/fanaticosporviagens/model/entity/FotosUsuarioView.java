package br.com.fanaticosporviagens.model.entity;

public class FotosUsuarioView {

    private Long idAlbum;

    private Long idUsuario;

    private Long idViagem;

    private Integer quantidadeFotosAlbum;

    private Integer quantidadeFotosViagem;

    public Long getIdAlbum() {
        return this.idAlbum;
    }

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public Long getIdViagem() {
        return this.idViagem;
    }

    public Integer getQuantidadeFotosAlbum() {
        return this.quantidadeFotosAlbum;
    }

    public Integer getQuantidadeFotosViagem() {
        return this.quantidadeFotosViagem;
    }

    public void setIdAlbum(final Long idAlbum) {
        this.idAlbum = idAlbum;
    }

    public void setIdUsuario(final Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setIdViagem(final Long idViagem) {
        this.idViagem = idViagem;
    }

    public void setQuantidadeFotosAlbum(final Integer quantidadeFotosAlbum) {
        this.quantidadeFotosAlbum = quantidadeFotosAlbum;
    }

    public void setQuantidadeFotosViagem(final Integer quantidadeFotosViagem) {
        this.quantidadeFotosViagem = quantidadeFotosViagem;
    }

}
