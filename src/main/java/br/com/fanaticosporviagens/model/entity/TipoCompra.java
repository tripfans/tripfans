package br.com.fanaticosporviagens.model.entity;

import java.util.Locale;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

public enum TipoCompra {
    ANTIGUIDADES(1),
    ARTE(2),
    ARTESANATO(3),
    BELEZA_E_SAUDE(4),
    BRINQUEDOS_E_JOGOS(5),
    CALCADOS(6),
    COMPRAS(7),
    ELETRONICOS(8),
    GASTRONOMIA(9),
    HOBBIES(10),
    JOIAS(11),
    LIVROS(12),
    LOJAS_DEPARTAMENTOS(13),
    MATERIAIS_ESPORTIVOS(14),
    MOBILIA_E_DECORACAO(15),
    MUSICA(16),
    PAPELARIA(17),
    PONTAS_DE_ESTOQUE(18),
    PRESENTES(19),
    ROUPAS_FEMININAS(20),
    ROUPAS_INFANTIS(21),
    ROUPAS_MASCULINAS(22);

    private int codigo;

    private final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    TipoCompra(final int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.messageSource.getMessage(this.getClass().getSimpleName() + "." + name(), null, Locale.getDefault());
    }
}
