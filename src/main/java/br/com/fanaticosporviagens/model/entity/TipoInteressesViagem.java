package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoInteressesViagem implements EnumTypeInteger {

    ARTES_CULTURA(1, "artesCultura", "Artes / Cultura"),
    AVENTURA(2, "aventura", "Aventura"),
    CACA_PESCA(3, "cacaPesca", "Caça & Pesca"),
    CARNAVAL(4, "carnaval", "Carnaval"),
    COMPRAS(5, "compras", "Compras"),
    CRUZEIROS(6, "cruzeiros", "Cruzeiros"),
    DESCANSO_RELAXAMENTO(7, "descansoRelaxamento", "Descanso / Relaxamento"),
    ECOTURISMO(8, "ecoturismo", "Ecoturismo"),
    ESCOLAR(9, "escolar", "Excursões escolares"),
    ESPORTES(10, "esportes", "Práticas Desportivas"),
    ESTRADA(11, "estrada", "Estrada"),
    EVENTOS(12, "eventos", "Congressos, Seminários"),
    EVENTOS_ESPORTIVOS(13, "eventosEsportivos", "Eventos esportivos"),
    FESTAS_TEMATICAS(14, "festasTematicas", "Festas Temáticas"),
    GASTRONOMIA(15, "gastronomia", "Gastronomia"),
    HISTORIA(16, "historia", "História"),
    JOGOS_APOSTAS(17, "jogosApostas", "Jogos / Apostas"),
    MERGULHO(18, "mergulho", "Mergulho"),
    MICARETAS(19, "micaretas", "Micaretas"),
    NEGOCIOS(20, "negocios", "Negócios"),
    NEVE(21, "neve", "Neve"),
    PARQUES_TEMATICOS(22, "parquesTematicos", "Parques temáticos"),
    PRAIA(23, "praia", "Praia"),
    RELIGIAO(24, "religiao", "Religião"),
    REVEILLON(25, "reveillon", "Reveillon"),
    ROMANTISMO(26, "romantismo", "Romantismo"),
    SAFARI(27, "safari", "Safari"),
    SHOWS(28, "shows", "Shows");

    public static TipoInteressesViagem getTipoPorNome(final String nome) {
        for (final TipoInteressesViagem tipo : values()) {
            if (tipo.getNome().equals(nome)) {
                return tipo;
            }
        }
        throw new IllegalArgumentException("Nome do tipo de interesse inválido!");
    }

    private final String nome;

    private final EnumI18nUtil util;

    TipoInteressesViagem(final int codigo, final String nome, final String... descricoes) {
        this.nome = nome;
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public String getNome() {
        return this.nome;
    }

}