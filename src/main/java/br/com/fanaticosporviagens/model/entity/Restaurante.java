package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * 
 * @author André Thiago
 * 
 */
@Entity
@DiscriminatorValue("10")
@TypeDefs(value = { @TypeDef(name = "TipoCozinhaRestaurante", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoCozinha") }) })
public class Restaurante extends LocalEnderecavel {

    private static final long serialVersionUID = 1562695861288442657L;

}
