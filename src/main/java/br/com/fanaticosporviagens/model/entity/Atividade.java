package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * Campos futuros na atividade
 * 1. Fotos
 *
 * @author gustavo
 * @author Carlos
 *
 */
@Entity
@Table(name = "viagem_atividade")
@TypeDefs(value = {
        @TypeDef(name = "TipoTransporte", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoTransporte") }),
        @TypeDef(name = "TipoAtividade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoAtividade") }) })
public class Atividade {

    @Column(name = "anotacoes", nullable = true)
    private String anotacoes;

    @Column(name = "data_hora_fim", nullable = true)
    private Date dataHoraFim;

    @Column(name = "data_hora_inicio", nullable = true)
    private Date dataHoraInicio;

    @Column(name = "descricao_atividade", nullable = true)
    private String descricao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem_dia", nullable = true)
    private DiaViagem dia;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem_dia_fim", nullable = true)
    private DiaViagem diaFim;

    @Id
    @Column(name = "id_viagem_atividade")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem_atividade_anterior", nullable = true, insertable = false)
    private Atividade itemAnterior;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem_atividade_proximo", nullable = true, insertable = false)
    private Atividade itemProximo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = true)
    private Local local;

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "nome", column = @Column(name = "destino_nome_local")),
        @AttributeOverride(name = "descricao", column = @Column(name = "destino_descricao_local")),
        @AttributeOverride(name = "endereco.descricao", column = @Column(name = "destino_endereco_local")),
        @AttributeOverride(name = "endereco.bairro", column = @Column(name = "destino_bairro_local")),
        @AttributeOverride(name = "endereco.cep", column = @Column(name = "destino_cep_local")) })
    private LocalEnderecavelNaoCadastradoAtividade localDestinoNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "nome", column = @Column(name = "local_nome")),
        @AttributeOverride(name = "descricao", column = @Column(name = "local_descricao")),
        @AttributeOverride(name = "endereco.descricao", column = @Column(name = "local_endereco")),
        @AttributeOverride(name = "endereco.bairro", column = @Column(name = "local_bairro")),
        @AttributeOverride(name = "endereco.cep", column = @Column(name = "local_cep")) })
    private LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "nome", column = @Column(name = "origem_nome_local")),
        @AttributeOverride(name = "descricao", column = @Column(name = "origem_descricao_local")),
        @AttributeOverride(name = "endereco.descricao", column = @Column(name = "origem_endereco_local")),
        @AttributeOverride(name = "endereco.bairro", column = @Column(name = "origem_bairro_local")),
        @AttributeOverride(name = "endereco.cep", column = @Column(name = "origem_cep_local")) })
    private LocalEnderecavelNaoCadastradoAtividade localOrigemNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    @Column
    private Integer ordem;

    @Column
    private boolean paga;

    @Column
    private boolean reservada;

    @Type(type = "TipoAtividade")
    @Column(name = "tipo_atividade", nullable = true)
    private TipoAtividade tipo;

    @Column(name = "titulo_atividade", nullable = true)
    private String titulo;

    @Type(type = "TipoTransporte")
    @Column(name = "transporte_codigo", nullable = true)
    private TipoTransporte transporte;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transporte_destino_id_local", nullable = true)
    private Local transporteLocalDestino;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transporte_origem_id_local", nullable = true)
    private Local transporteLocalOrigem;

    @Column(name = "valor_atividade", nullable = true)
    private BigDecimal valor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = true)
    private Viagem viagem;

    private void definirTipoPeloLocal() {
        if (this.tipo == null && this.local != null) {
            if (this.local instanceof Atracao || this.local instanceof LocalInteresseTuristico) {
                this.tipo = TipoAtividade.VISITA_ATRACAO;
            } else if (this.local instanceof Restaurante) {
                this.tipo = TipoAtividade.ALIMENTACAO;
            } else if (this.local instanceof Hotel) {
                this.tipo = TipoAtividade.HOSPEDAGEM;
            }
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Atividade other = (Atividade) obj;
        if (this.id == null) {
            if (other.getId() != null)
                return false;
        } else if (!this.id.equals(other.getId()))
            return false;
        return true;
    }

    public String getAnotacoes() {
        return this.anotacoes;
    }

    public LocalGeografico getCidadeDestinoTransporte() {
        if (this.transporteLocalDestino != null) {
            if (this.transporteLocalDestino instanceof Cidade || this.transporteLocalDestino instanceof LocalInteresseTuristico) {
                return (LocalGeografico) this.transporteLocalDestino;
            } else if (this.transporteLocalDestino instanceof LocalEnderecavel) {
                return this.transporteLocalDestino.getLocalizacao().getCidade().getLocal();
            }
        }
        return null;
    }

    public LocalGeografico getCidadeOrigemTransporte() {
        if (this.transporteLocalOrigem != null) {
            if (this.transporteLocalOrigem instanceof Cidade || this.transporteLocalOrigem instanceof LocalInteresseTuristico) {
                return (LocalGeografico) this.transporteLocalOrigem;
            } else if (this.transporteLocalOrigem instanceof LocalEnderecavel) {
                return this.transporteLocalOrigem.getLocalizacao().getCidade().getLocal();
            }
        }
        return null;
    }

    public Atividade getCopia(final DiaViagem diaCopia) {
        return getCopia(null, diaCopia);
    }

    public Atividade getCopia(final Viagem viagemCopia) {
        return getCopia(viagemCopia, null);
    }

    private Atividade getCopia(final Viagem viagemCopia, final DiaViagem diaCopia) {
        final Atividade copia = new Atividade();

        copia.setViagem(viagemCopia);
        copia.setDia(diaCopia);

        copia.setTipo(getTipo());
        copia.setLocal(getLocal());
        copia.setTitulo(getTitulo());
        copia.setAnotacoes(getAnotacoes());
        copia.setPaga(isPaga());
        copia.setReservada(isReservada());
        copia.setLocalNaoCadastrado(getLocalNaoCadastrado());
        copia.setLocalOrigemNaoCadastrado(getLocalOrigemNaoCadastrado());
        copia.setLocalDestinoNaoCadastrado(getLocalDestinoNaoCadastrado());

        copia.setTransporte(getTransporte());
        copia.setTransporteLocalOrigem(getTransporteLocalOrigem());
        copia.setTransporteLocalDestino(getTransporteLocalDestino());

        copia.setDataHoraFim(getDataHoraFim());
        copia.setDataHoraInicio(getDataHoraInicio());
        copia.setDescricao(getDescricao());
        copia.setValor(getValor());

        return copia;
    }

    public Date getDataHoraFim() {
        return this.dataHoraFim;
    }

    public Date getDataHoraInicio() {
        return this.dataHoraInicio;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getDescricaoDono() {
        if (this.viagem != null) {
            return "Item da viagem";
        }
        return "Item do " + this.dia.getDescricao();
    }

    public DiaViagem getDia() {
        return this.dia;
    }

    public DiaViagem getDiaFim() {
        return this.diaFim;
    }

    public String getEmail() {
        if (this.local == null) {
            return null;
        }
        return this.local.getEmail();
    }

    public String getEndereco() {
        if (this.local != null) {
            final Endereco endereco = this.local.getEndereco();

            if (endereco != null) {
                final StringBuilder enderecoFormatado = new StringBuilder();

                if (endereco.getDescricao() != null && !endereco.getDescricao().trim().isEmpty()) {
                    enderecoFormatado.append(endereco.getDescricao());
                    if (endereco.getBairro() != null && !endereco.getBairro().trim().isEmpty()) {
                        enderecoFormatado.append(", ");
                        enderecoFormatado.append(endereco.getBairro());
                    }
                    if (endereco.getCep() != null && !endereco.getCep().trim().isEmpty()) {
                        enderecoFormatado.append(" - CEP ");
                        enderecoFormatado.append(endereco.getCep());
                    }
                }
                return enderecoFormatado.toString();
            }
        }
        return null;
    }

    public String getEnderecoLocal() {
        if (this.local != null && this.local.getEndereco() != null) {
            return this.local.getEndereco().getDescricao();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getEndereco(true).getDescricao();
        }
        return null;
    }

    public String getEnderecoLocalDestino() {
        if (getTransporteLocalDestino() != null) {
            return this.transporteLocalDestino.getEndereco(true).getDescricao();
        } else if (getLocalDestinoNaoCadastrado() != null) {
            return this.localDestinoNaoCadastrado.getEndereco(true).getDescricao();
        }
        return null;
    }

    public String getEnderecoLocalOrigem() {
        if (getTransporteLocalOrigem() != null) {
            return this.transporteLocalOrigem.getEndereco(true).getDescricao();
        } else if (getLocalOrigemNaoCadastrado() != null) {
            return this.localOrigemNaoCadastrado.getEndereco(true).getDescricao();
        }
        return null;
    }

    public String getIcone() {
        if (getTipo() == null) {
            return "";
        }
        return this.getTipo().getIcone();
    }

    public String getIconeAlt() {
        if (this.local != null) {
            return "Local do tipo " + this.local.getTipoLocal();
        } else if (this.transporte != null) {
            return "Transporte do tipo " + this.transporte;
        }
        return "Item avulso";

    }

    public Long getId() {
        return this.id;
    }

    public Atividade getItemAnterior() {
        return this.itemAnterior;
    }

    public Atividade getItemPrimeiro() {
        if (this.itemAnterior == null || this.getId().equals(this.itemAnterior.getId())) {
            return this;
        }
        return this.itemAnterior.getItemPrimeiro();
    }

    public Atividade getItemProximo() {
        return this.itemProximo;
    }

    public Atividade getItemUltimo() {
        if (this.itemProximo == null) {
            return this;
        }
        return this.itemProximo.getItemUltimo();
    }

    public Set<Local> getLocaisVisitados() {
        final Set<Local> locaisVisitados = new HashSet<Local>();

        if (this.local != null) {
            locaisVisitados.add(this.local);
        }
        if (this.transporteLocalDestino != null) {
            locaisVisitados.add(this.transporteLocalDestino);
        }
        if (this.transporteLocalOrigem != null) {
            locaisVisitados.add(this.transporteLocalOrigem);
        }

        return locaisVisitados;
    }

    public Local getLocal() {
        return this.local;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalDestinoNaoCadastrado() {
        return this.localDestinoNaoCadastrado;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalNaoCadastrado() {
        return this.localNaoCadastrado;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalOrigemNaoCadastrado() {
        return this.localOrigemNaoCadastrado;
    }

    public String getNomeLocal() {
        if (getLocal() != null) {
            return this.local.getNome();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getNome();
        }
        return null;
    }

    public String getNomeLocalDestino() {
        if (getTransporteLocalDestino() != null) {
            return this.transporteLocalDestino.getNome();
        } else if (getLocalDestinoNaoCadastrado() != null) {
            return this.localDestinoNaoCadastrado.getNome();
        }
        return null;
    }

    public String getNomeLocalOrigem() {
        if (getTransporteLocalOrigem() != null) {
            return this.transporteLocalOrigem.getNome();
        } else if (getLocalOrigemNaoCadastrado() != null) {
            return this.localOrigemNaoCadastrado.getNome();
        }
        return null;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    public String getSite() {
        if (this.local != null) {
            return this.local.getSite();
        }
        return null;
    }

    public String getTelefone() {
        if (this.local != null) {
            return this.local.getTelefone();
        }
        return null;
    }

    public TipoAtividade getTipo() {
        return this.tipo;
    }

    public String getTitulo() {
        if (this.titulo == null) {
            return this.getTituloItem();
        }
        return this.titulo;
    }

    public String getTituloItem() {
        if (this.isPersonalizada()) {
            return this.titulo;
        } else if (this.local != null) {
            return this.local.getNomeCompletoAte(LocalType.PAIS);
        } else if (this.localNaoCadastrado != null) {
            return this.localNaoCadastrado.getNome();
        } else if (this.transporte != null) {
            return this.transporte.getDescricao();
        }
        return this.titulo;
    }

    public String getTituloItemReduzido() {
        if (this.isPersonalizada()) {
            return this.titulo;
        } else if (this.local != null) {
            return this.local.getNome();
        } else if (this.localNaoCadastrado != null) {
            return this.localNaoCadastrado.getNome();
        } else if (this.transporte != null) {
            return this.transporte.getDescricao();
        }
        return this.titulo;
    }

    public TipoTransporte getTransporte() {
        return this.transporte;
    }

    public Local getTransporteLocalDestino() {
        return this.transporteLocalDestino;
    }

    public String getTransporteLocalDestinoUrl() {
        if (this.transporteLocalDestino == null) {
            return null;
        }
        return this.transporteLocalDestino.getUrl();
    }

    public Local getTransporteLocalOrigem() {
        return this.transporteLocalOrigem;
    }

    public String getTransporteLocalOrigemUrl() {
        if (this.transporteLocalOrigem == null) {
            return null;
        }
        return this.transporteLocalOrigem.getUrl();
    }

    public String getUrl() {
        if (this.local == null) {
            return null;
        }
        return this.local.getUrl();
    }

    public BigDecimal getValor() {
        return this.valor;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    public boolean isAtividadeAlimentacao() {
        return TipoAtividade.ALIMENTACAO.equals(this.getTipo());

    }

    public boolean isAtividadeHospedagem() {
        return TipoAtividade.HOSPEDAGEM.equals(this.getTipo());

    }

    public boolean isAtividadeTransporte() {
        return TipoAtividade.TRANSPORTE.equals(this.getTipo());

    }

    public boolean isAtividadeVisitaAtracao() {
        return TipoAtividade.VISITA_ATRACAO.equals(this.getTipo());

    }

    public boolean isComecaNoDia(final Long idDia) {
        if (this.getDia() == null || idDia == 0) {
            return true;
        }
        return this.getDia() != null && this.getDia().getId().equals(idDia);
    }

    public boolean isPaga() {
        return this.paga;
    }

    public boolean isPenultimo() {
        return false; // (this.itemProximo != null && this.itemProximo.isUltimo());
    }

    public boolean isPersonalizada() {
        return TipoAtividade.PERSONALIZADO.equals(this.getTipo());

    }

    public boolean isPrimeiro() {
        return (this.ordem == 0);
    }

    public boolean isReservada() {
        return this.reservada;
    }

    public boolean isSegundo() {
        return (this.ordem == 1);
    }

    public boolean isTerminaEmOutroDia() {
        return this.getDiaFim() != null && this.getDia() != this.getDiaFim();
    }

    public boolean isTerminaNoDia(final Long idDia) {
        if (this.getDia() == null || idDia == 0) {
            return true;
        }
        return this.getDiaFim() != null && this.getDiaFim().getId().equals(idDia);
    }

    public boolean isUltimo() {
        return false;// (this.itemProximo == null);
    }

    public void setAnotacoes(final String anotacoes) {
        this.anotacoes = anotacoes;
    }

    public void setDataHoraFim(final Date dataHoraFim) {
        this.dataHoraFim = dataHoraFim;
    }

    public void setDataHoraInicio(final Date dataHoraInicio) {
        this.dataHoraInicio = dataHoraInicio;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDia(final DiaViagem diaViagem) {
        this.dia = diaViagem;
    }

    public void setDiaFim(final DiaViagem diaFim) {
        this.diaFim = diaFim;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setItemAnterior(final Atividade itemAnterior) {
        this.itemAnterior = itemAnterior;
    }

    public void setItemProximo(final Atividade itemProximo) {
        this.itemProximo = itemProximo;
    }

    public void setLocal(final Local local) {
        this.local = local;
        definirTipoPeloLocal();
    }

    public void setLocalDestinoNaoCadastrado(final LocalEnderecavelNaoCadastradoAtividade localDestinoNaoCadastrado) {
        this.localDestinoNaoCadastrado = localDestinoNaoCadastrado;
    }

    public void setLocalNaoCadastrado(final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado) {
        this.localNaoCadastrado = localNaoCadastrado;
    }

    public void setLocalOrigemNaoCadastrado(final LocalEnderecavelNaoCadastradoAtividade localOrigemNaoCadastrado) {
        this.localOrigemNaoCadastrado = localOrigemNaoCadastrado;
    }

    public void setOrdem(final Integer ordem) {
        this.ordem = ordem;
    }

    public void setPaga(final boolean paga) {
        this.paga = paga;
    }

    public void setReservada(final boolean reservada) {
        this.reservada = reservada;
    }

    public void setTipo(final TipoAtividade tipo) {
        this.tipo = tipo;
    }

    public void setTitulo(final String titulo) {
        this.titulo = titulo;
    }

    public void setTransporte(final TipoTransporte transporte) {
        this.transporte = transporte;
    }

    public void setTransporteLocalDestino(final Local transporteLocalDestino) {
        this.transporteLocalDestino = transporteLocalDestino;
    }

    public void setTransporteLocalOrigem(final Local transporteLocalOrigem) {
        this.transporteLocalOrigem = transporteLocalOrigem;
    }

    public void setValor(final BigDecimal valor) {
        this.valor = valor;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }
}
