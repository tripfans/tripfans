package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.BooleanUtils;
import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.util.HibernateUtil;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_acao_usuario")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_acao_usuario", allocationSize = 1)
@Table(name = "acao_usuario")
@TypeDefs(value = { @TypeDef(name = "TipoAcao", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoAcao") }) })
public class AcaoUsuario extends BaseEntity<Long> {

    private static final long serialVersionUID = 7130631917172285822L;

    @Any(metaColumn = @Column(name = "tipo_acao_principal"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "Album", targetEntity = Album.class),
            @MetaValue(value = "Avaliacao", targetEntity = Avaliacao.class), @MetaValue(value = "Comentario", targetEntity = Comentario.class),
            @MetaValue(value = "Convite", targetEntity = Convite.class), @MetaValue(value = "Dica", targetEntity = Dica.class),
            @MetaValue(value = "Foto", targetEntity = Foto.class), @MetaValue(value = "InteressesViagem", targetEntity = InteressesViagem.class),
            @MetaValue(value = "PedidoDica", targetEntity = PedidoDica.class), @MetaValue(value = "Pergunta", targetEntity = Pergunta.class),
            @MetaValue(value = "PreferenciasViagem", targetEntity = PreferenciasViagem.class),
            @MetaValue(value = "Resposta", targetEntity = Resposta.class), @MetaValue(value = "PlanoViagem", targetEntity = PlanoViagem.class),
            @MetaValue(value = "Viagem", targetEntity = Viagem.class), @MetaValue(value = "VotoUtil", targetEntity = VotoUtil.class), })
    @JoinColumn(name = "id_acao_principal")
    public Acao acaoPrincipal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_autor", nullable = false)
    private Usuario autor;

    /**
     * Data em que a ação original foi criada
     */
    @Column(name = "data_acao", nullable = false)
    private Date data;

    /**
     * Data em que este objeto foi gravado na base
     */
    @Column(name = "data_criacao", nullable = false)
    private Date dataCriacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_destinatario", nullable = true)
    private Usuario destinatario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_cidade", nullable = true)
    private Cidade localCidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente", nullable = true)
    private Continente localContinente;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_enderecavel", nullable = true)
    private Local localEnderecavel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_estado", nullable = true)
    private Estado localEstado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_interesse_turistico", nullable = true)
    private LocalInteresseTuristico localInteresseTuristico;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_pais", nullable = true)
    private Pais localPais;

    @Column(name = "ponto", nullable = false)
    private BigDecimal pontos;

    @Column(name = "publica", nullable = false, columnDefinition = "boolean not null default false")
    private Boolean publica = false;

    @Column(name = "publica_na_home", nullable = false, columnDefinition = "boolean not null default false")
    private boolean publicaNaHome;

    @Column(name = "quantidade", nullable = true)
    private Integer quantidade;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao rastreioAvaliacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_comentario", nullable = true)
    private Comentario rastreioComentario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_convite", nullable = true)
    private Convite rastreioConvite;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = true)
    private Dica rastreioDica;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_foto_perfil", nullable = true)
    private Foto rastreioFotoPerfil;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_interesses_viagem", nullable = true)
    private InteressesViagem rastreioInteressesViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pergunta", nullable = true)
    private Pergunta rastreioPergunta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_preferencias_viagem", nullable = true)
    private PreferenciasViagem rastreioPreferenciasViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_resposta", nullable = true)
    private Resposta rastreioResposta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_voto_util", nullable = true)
    private VotoUtil rastreioVotoUtil;

    @Type(type = "TipoAcao")
    @Column(name = "id_tipo_acao", nullable = false)
    private TipoAcao tipoAcao;

    @Column(name = "tipo_acao_principal", insertable = false, updatable = false)
    private String tipoAcaoPrincipal;

    @Column(name = "visivel", nullable = false, columnDefinition = "boolean not null default false")
    private Boolean visivel = false;

    public AcaoUsuario() {
        super();
    }

    public AcaoUsuario(final Acao acao, final TipoAcao tipoAcao) {
        this(acao.getAutor(), tipoAcao, acao, acao.getAlvo(), acao.getDestinatario());
    }

    public AcaoUsuario(final Usuario autor, final TipoAcao tipoAcao, final Acao acao, final AlvoAcao objetoAlvo, final Usuario destinatario) {
        setDataCriacao(new Date());
        if (acao instanceof PedidoDica && BooleanUtils.isTrue(((PedidoDica) acao).getRespondido())) {
            setData(((PedidoDica) acao).getDataResposta());
        } else if (acao instanceof Convite && ((Convite) acao).isAceito()) {
            setData(((Convite) acao).getDataAceite());
        } else if (acao.getDataUltimaAlteracao() != null) {
            setData(acao.getDataUltimaAlteracao().toDate());
        } else {
            setData(acao.getDataCriacao() != null ? acao.getDataCriacao().toDate() : new Date());
        }
        atribuirValores(tipoAcao, autor);
        atribuirValores(objetoAlvo);
        this.destinatario = destinatario;
        setAcaoPrincipal(acao);
        if (acao instanceof Avaliacao) {
            this.rastreioAvaliacao = (Avaliacao) acao;
        } else if (acao instanceof Comentario) {
            this.rastreioComentario = (Comentario) acao;
        } else if (acao instanceof Dica) {
            this.rastreioDica = (Dica) acao;
        } else if (acao instanceof VotoUtil) {
            this.rastreioVotoUtil = (VotoUtil) acao;
        } else if (acao instanceof Convite) {
            this.rastreioConvite = (Convite) acao;
        } else if (acao instanceof InteressesViagem) {
            this.rastreioInteressesViagem = (InteressesViagem) acao;
        } else if (acao instanceof PreferenciasViagem) {
            this.rastreioPreferenciasViagem = (PreferenciasViagem) acao;
        } else if (acao instanceof Foto) {
            this.rastreioFotoPerfil = (Foto) acao;
        } else if (acao instanceof Pergunta) {
            this.rastreioPergunta = (Pergunta) acao;
        } else if (acao instanceof Resposta) {
            this.rastreioResposta = (Resposta) acao;
        }
    }

    @Transient
    public Acao getAcao() {
        if (getAcaoPrincipal() != null) {
            return getAcaoPrincipal();
        }
        if (isAcaoSobreAvaliacao()) {
            return this.rastreioAvaliacao;
        } else if (this.rastreioComentario != null) {
            return this.rastreioComentario;
        } else if (isAcaoSobreDica()) {
            return this.rastreioDica;
        } else if (isAcaoSobreVotoUtil()) {
            return this.rastreioVotoUtil;
        } else if (this.rastreioConvite != null) {
            return this.rastreioConvite;
        } else if (this.rastreioInteressesViagem != null) {
            return this.rastreioInteressesViagem;
        } else if (this.rastreioPreferenciasViagem != null) {
            return this.rastreioPreferenciasViagem;
        } else if (this.rastreioFotoPerfil != null) {
            return this.rastreioFotoPerfil;
        } else if (isAcaoSobrePergunta()) {
            return this.rastreioPergunta;
        } else if (isAcaoSobreResposta()) {
            return this.rastreioResposta;
        }
        return null;
    }

    public Acao getAcaoPrincipal() {
        return this.acaoPrincipal;
    }

    @Transient
    public AlvoAcao getAlvo() {
        if (getAcao() != null) {
            if (getAcao().getAlvo() == null && getAcao() instanceof AlvoAcao) {
                return (AlvoAcao) getAcao();
            }
            return getAcao().getAlvo();
        }
        return null;
    }

    public Usuario getAutor() {
        return this.autor;
    }

    public Date getData() {
        return this.data;
    }

    public Date getDataCriacao() {
        return this.dataCriacao;
    }

    public Usuario getDestinatario() {
        return this.destinatario;
    }

    public Cidade getLocalCidade() {
        return this.localCidade;
    }

    public Continente getLocalContinente() {
        return this.localContinente;
    }

    public Local getLocalEnderecavel() {
        return this.localEnderecavel;
    }

    public Estado getLocalEstado() {
        return this.localEstado;
    }

    public LocalInteresseTuristico getLocalInteresseTuristico() {
        return this.localInteresseTuristico;
    }

    public Pais getLocalPais() {
        return this.localPais;
    }

    public BigDecimal getPontos() {
        return this.pontos;
    }

    public Boolean getPublica() {
        return this.publica;
    }

    public Integer getQuantidade() {
        return this.quantidade;
    }

    public Avaliacao getRastreioAvaliacao() {
        return this.rastreioAvaliacao;
    }

    public Comentario getRastreioComentario() {
        return this.rastreioComentario;
    }

    public Convite getRastreioConvite() {
        return this.rastreioConvite;
    }

    public Dica getRastreioDica() {
        return this.rastreioDica;
    }

    public Foto getRastreioFotoPerfil() {
        return this.rastreioFotoPerfil;
    }

    public InteressesViagem getRastreioInteressesViagem() {
        return this.rastreioInteressesViagem;
    }

    public Pergunta getRastreioPergunta() {
        return this.rastreioPergunta;
    }

    public PreferenciasViagem getRastreioPreferenciasViagem() {
        return this.rastreioPreferenciasViagem;
    }

    public Resposta getRastreioResposta() {
        return this.rastreioResposta;
    }

    public VotoUtil getRastreioVotoUtil() {
        return this.rastreioVotoUtil;
    }

    public TipoAcao getTipoAcao() {
        return this.tipoAcao;
    }

    public String getTipoAcaoPrincipal() {
        return this.tipoAcaoPrincipal;
    }

    public Boolean getVisivel() {
        return this.visivel;
    }

    public boolean isAcaoSobre(final Class<? extends Acao> classe) {
        return classe.getSimpleName().equals(this.tipoAcaoPrincipal);
    }

    public boolean isAcaoSobreAlbum() {
        return isAcaoSobre(Album.class);
    }

    public boolean isAcaoSobreAvaliacao() {
        return this.rastreioAvaliacao != null;
    }

    public boolean isAcaoSobreComentario() {
        return isAcaoSobre(Comentario.class);
    }

    public boolean isAcaoSobreConvite() {
        return isAcaoSobre(Convite.class);
    }

    public boolean isAcaoSobreDica() {
        return this.rastreioDica != null;
    }

    public boolean isAcaoSobreFoto() {
        return isAcaoSobre(Foto.class);
    }

    public boolean isAcaoSobreInteressesViagem() {
        return isAcaoSobre(InteressesViagem.class);
    }

    public boolean isAcaoSobrePergunta() {
        return this.rastreioPergunta != null;
    }

    public boolean isAcaoSobrePreferenciasViagem() {
        return isAcaoSobre(PreferenciasViagem.class);
    }

    public boolean isAcaoSobreResposta() {
        return this.rastreioResposta != null;
    }

    public boolean isAcaoSobreViagem() {
        return isAcaoSobre(Viagem.class);
    }

    public boolean isAcaoSobreVotoUtil() {
        return this.rastreioVotoUtil != null;
    }

    public boolean isPublicaNaHome() {
        return this.publicaNaHome;
    }

    public void setAcao(final TipoAcao tipoAcao) {
        this.tipoAcao = tipoAcao;
    }

    public void setAcaoPrincipal(final Acao acaoPrincipal) {
        this.acaoPrincipal = acaoPrincipal;
    }

    public void setAutor(final Usuario usuario) {
        this.autor = usuario;
    }

    public void setData(final Date dataAcao) {
        this.data = dataAcao;
    }

    public void setDataCriacao(final Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setDestinatario(final Usuario destinatario) {
        this.destinatario = destinatario;
    }

    public void setLocalCidade(final Cidade localCidade) {
        this.localCidade = localCidade;
    }

    public void setLocalContinente(final Continente localContinente) {
        this.localContinente = localContinente;
    }

    public void setLocalEnderecavel(final Local localEnderecavel) {
        this.localEnderecavel = localEnderecavel;
    }

    public void setLocalEstado(final Estado localEstado) {
        this.localEstado = localEstado;
    }

    public void setLocalInteresseTuristico(final LocalInteresseTuristico localInteresseTuristico) {
        this.localInteresseTuristico = localInteresseTuristico;
    }

    public void setLocalPais(final Pais localPais) {
        this.localPais = localPais;
    }

    public void setPontos(final BigDecimal pontos) {
        this.pontos = pontos;
    }

    public void setPublica(final Boolean publica) {
        this.publica = publica;
    }

    public void setPublicaNaHome(final boolean publicaNaHome) {
        this.publicaNaHome = publicaNaHome;
    }

    public void setQuantidade(final Integer quantidade) {
        this.quantidade = quantidade;
    }

    public void setRastreioAvaliacao(final Avaliacao avaliacao) {
        this.rastreioAvaliacao = avaliacao;
    }

    public void setRastreioComentario(final Comentario rastreioComentario) {
        this.rastreioComentario = rastreioComentario;
    }

    public void setRastreioConvite(final Convite rastreioConvite) {
        this.rastreioConvite = rastreioConvite;
    }

    public void setRastreioDica(final Dica dica) {
        this.rastreioDica = dica;
    }

    public void setRastreioFotoPerfil(final Foto rastreioFotoPerfil) {
        this.rastreioFotoPerfil = rastreioFotoPerfil;
    }

    public void setRastreioInteressesViagem(final InteressesViagem rastreioInteressesViagem) {
        this.rastreioInteressesViagem = rastreioInteressesViagem;
    }

    public void setRastreioPergunta(final Pergunta rastreioPergunta) {
        this.rastreioPergunta = rastreioPergunta;
    }

    public void setRastreioPreferenciasViagem(final PreferenciasViagem rastreioPreferenciasViagem) {
        this.rastreioPreferenciasViagem = rastreioPreferenciasViagem;
    }

    public void setRastreioResposta(final Resposta rastreioResposta) {
        this.rastreioResposta = rastreioResposta;
    }

    public void setRastreioVotoUtil(final VotoUtil votoUtil) {
        this.rastreioVotoUtil = votoUtil;
    }

    public void setTipoAcao(final TipoAcao tipoAcao) {
        this.tipoAcao = tipoAcao;
    }

    public void setTipoAcaoPrincipal(final String tipoAcaoPrincipal) {
        this.tipoAcaoPrincipal = tipoAcaoPrincipal;
    }

    public void setVisivel(final Boolean visivel) {
        this.visivel = visivel;
    }

    private void atribuirValores(final AlvoAcao alvo) {
        if (alvo instanceof Local) {
            final Local local = (Local) alvo;
            if (local.isTipoLocalEnderecavel()) {
                final LocalEnderecavel localEnderecavel = (LocalEnderecavel) HibernateUtil.getInstance().derreferenciarProxy(local);
                this.localEnderecavel = localEnderecavel;
                this.localInteresseTuristico = localEnderecavel.getLocalInteresseTuristico();
                this.localCidade = localEnderecavel.getCidade();
                this.localEstado = localEnderecavel.getEstado();
                this.localPais = localEnderecavel.getPais();
                this.localContinente = localEnderecavel.getContinente();
            } else if (local.isTipoLocalInteresseTuristico()) {
                final LocalInteresseTuristico localInteresseTuristico = (LocalInteresseTuristico) HibernateUtil.getInstance().derreferenciarProxy(
                        local);
                this.localInteresseTuristico = localInteresseTuristico;
                this.localCidade = localInteresseTuristico.getCidade();
                this.localEstado = localInteresseTuristico.getEstado();
                this.localPais = localInteresseTuristico.getPais();
                this.localContinente = localInteresseTuristico.getContinente();
            } else if (local.isTipoCidade()) {
                final Cidade cidade = (Cidade) HibernateUtil.getInstance().derreferenciarProxy(local);
                this.localCidade = cidade;
                this.localEstado = cidade.getEstado();
                this.localPais = cidade.getPais();
                this.localContinente = cidade.getContinente();
            } else if (local.isTipoEstado()) {
                final Estado estado = (Estado) HibernateUtil.getInstance().derreferenciarProxy(local);
                this.localEstado = estado;
                this.localPais = estado.getPais();
                this.localContinente = estado.getContinente();
            } else if (local.isTipoPais()) {
                final Pais pais = (Pais) HibernateUtil.getInstance().derreferenciarProxy(local);
                this.localPais = pais;
                this.localContinente = pais.getContinente();
            } else if (local.isTipoContinente()) {
                this.localContinente = (Continente) HibernateUtil.getInstance().derreferenciarProxy(local);
            }
        }
    }

    private void atribuirValores(final TipoAcao tipoAcao) {
        this.tipoAcao = tipoAcao;
        this.visivel = tipoAcao.isVisivel();
        this.publica = tipoAcao.isPublicavel();
        this.publicaNaHome = tipoAcao.isPublicavelNaHome();
        this.pontos = tipoAcao.getPontos();
    }

    private void atribuirValores(final TipoAcao tipoAcao, final Usuario usuario) {
        this.autor = usuario;

        atribuirValores(tipoAcao);
    }

}
