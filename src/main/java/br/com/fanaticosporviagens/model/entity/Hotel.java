package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.apache.commons.lang3.StringUtils;

/**
 * @author André Thiago
 *
 */
@Entity
@DiscriminatorValue("8")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class Hotel extends LocalEnderecavel {

    private static final long serialVersionUID = 6698634482480838949L;

    @Column(name = "estrelas")
    private Integer estrelas;

    @Column(name = "numero_quartos")
    private Integer numeroQuartos;

    public Integer getEstrelas() {
        return this.estrelas;
    }

    public Integer getNumeroQuartos() {
        return this.numeroQuartos;
    }

    @Override
    public String getUrlBooking() {
        String urlBooking = "";
        if (StringUtils.isNotEmpty(super.getUrlBooking())) {
            urlBooking = super.getUrlBooking();
        } else {
            final LocalidadeEmbeddable localidade = getLocalizacao();
            if (localidade != null) {
                final LocalEmbeddable localidadePai = localidade.getCidade() != null ? localidade.getCidade() : localidade
                        .getLocalInteresseTuristico();
                if (localidadePai != null) {
                    urlBooking = localidadePai.getLocal().getUrlBooking();
                }
            }
        }

        if (StringUtils.isNotEmpty(urlBooking)) {
            return urlBooking + "?aid=382185";
        }
        return "http://www.booking.com/index.html?aid=382185";
    }

    public void setEstrelas(final Integer estrelas) {
        this.estrelas = estrelas;
    }

    public void setNumeroQuartos(final Integer numeroQuartos) {
        this.numeroQuartos = numeroQuartos;
    }

}
