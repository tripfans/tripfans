package br.com.fanaticosporviagens.model.entity;

/**
 * Interface a ser implementada por entidades que devem ser indexadas no Solr.
 * 
 * @author André Thiago
 * 
 */
public interface Indexavel {

    public String getEdgyTextField();

    public Long getId();

    public String getNome();

    public String getNomeExibicao();

    public Integer getTipoEntidade();

    public String getTokenField();

    public String getUrlFotoAlbum();

    public String getUrlFotoBig();

    public String getUrlFotoSmall();

    public String getUrlPath();

}
