package br.com.fanaticosporviagens.model.entity;

/**
 * 
 * @author @author Carlos Nascimento
 * 
 */
public enum FormatoTemperatura {

    CELSIUS, FAHRENHEIT

}
