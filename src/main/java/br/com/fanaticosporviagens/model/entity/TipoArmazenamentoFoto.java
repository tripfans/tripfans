package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum TipoArmazenamentoFoto implements EnumTypeInteger {

    EXTERNO(2, "Externo"),
    LOCAL(1, "Local");

    private final Integer codigo;

    private final String descricao;

    TipoArmazenamentoFoto(final Integer codigo, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

}
