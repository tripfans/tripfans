package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author André Thiago
 *
 */
@Entity
@Table(name = "amizade")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_amizade", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_amizade", nullable = false, unique = true)) })
public class Amizade extends BaseEntity<Long> implements AlvoAcao {

    private static final long serialVersionUID = 4270422382856435426L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario_amigo", updatable = false)
    private Usuario amigo;

    @Column(name = "data_criacao", nullable = false)
    private Date dataCriacao = new Date();

    @Column(name = "email_facebook_usuario_amigo")
    private String emailAmigoFacebook;

    @Column(name = "id_usuario_amigo", insertable = false, updatable = false)
    private Long idAmigo;

    @Column(name = "id_facebook_usuario_amigo")
    private String idAmigoFacebook;

    @Column(name = "importada_facebook")
    private Boolean importadaFacebook = false;

    @Column(name = "nome_usuario_amigo")
    private String nomeAmigo;

    @Column(name = "removida_facebook", nullable = false, columnDefinition = "BOOLEAN NOT NULL DEFAULT FALSE")
    private final Boolean removidaFacebook = false;

    @Column(name = "removida_tripfans", nullable = false, columnDefinition = "BOOLEAN NOT NULL DEFAULT FALSE")
    private final Boolean removidaTripfans = false;

    @Column(name = "username_facebook_usuario_amigo")
    private String usernameAmigoFacebook;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_usuario", updatable = false)
    private Usuario usuario;

    public Amizade() {
    }

    public Amizade(final Usuario usuario, final Usuario amigo) {
        super();
        this.usuario = usuario;
        this.amigo = amigo;
        this.nomeAmigo = amigo.getDisplayName();
    }

    public Usuario getAmigo() {
        return this.amigo;
    }

    public Date getDataCriacao() {
        return this.dataCriacao;
    }

    @Override
    public String getDescricaoAlvo() {
        return this.getAmigo().getDisplayName();
    }

    @Override
    public Usuario getDestinatario() {
        return getAmigo();
    }

    public String getEmailAmigoFacebook() {
        return this.emailAmigoFacebook;
    }

    public Long getIdAmigo() {
        return this.idAmigo;
    }

    public String getIdAmigoFacebook() {
        return this.idAmigoFacebook;
    }

    public Boolean getImportadaFacebook() {
        return this.importadaFacebook;
    }

    public String getNomeAmigo() {
        return this.nomeAmigo;
    }

    public Boolean getRemovidaFacebook() {
        return this.removidaFacebook;
    }

    public Boolean getRemovidaTripfans() {
        return this.removidaTripfans;
    }

    @Transient
    public String getUrlFotoSmall() {
        if (this.getAmigo() != null) {
            return this.getAmigo().getUrlFotoSmall();
        }
        return "";
    }

    public String getUsernameAmigoFacebook() {
        return this.usernameAmigoFacebook;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public boolean isFacebook() {
        return this.importadaFacebook;
    }

    public boolean isSomenteFacebook() {
        return isFacebook() && (this.amigo == null || !this.amigo.isEnabled());
    }

    public boolean isSomenteTripfans() {
        return !isFacebook();
    }

    public boolean isTripfans() {
        if (this.amigo != null) {
            return this.amigo.isEnabled();
        }
        return false;
    }

    public void setAmigo(final Usuario amigo) {
        this.amigo = amigo;
    }

    public void setDataCriacao(final Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setEmailAmigoFacebook(final String emailAmigoFacebook) {
        this.emailAmigoFacebook = emailAmigoFacebook;
    }

    public void setIdAmigo(final Long idAmigo) {
        this.idAmigo = idAmigo;
    }

    public void setIdAmigoFacebook(final String idAmigoFacebook) {
        this.idAmigoFacebook = idAmigoFacebook;
    }

    public void setImportadaFacebook(final Boolean importadaFacebook) {
        this.importadaFacebook = importadaFacebook;
    }

    public void setNomeAmigo(final String nomeAmigo) {
        this.nomeAmigo = nomeAmigo;
    }

    public void setUsernameAmigoFacebook(final String usernameAmigoFacebook) {
        this.usernameAmigoFacebook = usernameAmigoFacebook;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
