package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Classe que representa atividades do dia a dia
 * Ex: Ir a praias, praças, museus, fortes, castelos, pontes, etc
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@DiscriminatorValue("2")
public class VisitaAtracao extends AtividadeCurtaDuracao {

    private static final long serialVersionUID = -5734542096330268947L;

    @Override
    public TipoAtividade getTipo() {
        return TipoAtividade.VISITA_ATRACAO;
    }

}