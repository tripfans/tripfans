package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * 
 * @author Carlos
 * 
 */
// @Entity
public class ImportacaoAtracao extends BaseEntity<Long> {

    private static final long serialVersionUID = 1164079985238337140L;

    private String address;

    private String canonicalUrl;

    private String categoryIconPrefix;

    private String categoryIconSuffix;

    private String categoryId;

    private String categoryName;

    private String categoryPluralName;

    private String categoryShortName;

    private String cc;

    private Integer checkinsCount;

    private String city;

    private String country;

    private String crossStreet;

    private String formattedPhone;

    private String foursquareId;

    private String lat;

    private String lng;

    private String name;

    private String phone;

    private String postalCode;

    private Double rating;

    private String state;

    private Integer tipCount;

    private String url;

    private Integer usersCount;

    private boolean verified;

}
