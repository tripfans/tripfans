package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.context.i18n.LocaleContextHolder;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * 
 * @author André Thiago
 * 
 */
public enum TipoCozinha implements EnumTypeInteger {
   
    AFRICANA(1, "Africana"),
    ALEMA(2, "Alemã"),
    AMERICANA(3, "Americana"),
    ARABE(4, "Árabe"),
    ARGENTINA(5, "Argentina"),
    ASIATICA(6, "Asiática"),
    AUSTRIACA(7, "Austríaca"),
    BAIANA(8, "Baiana"),
    BOTEQUIM(9, "Botequim"),
    BRASILEIRA(10, "Brasileira"),
    CAFE(11, "Café"),
    CANTINA_ITALIANA(12, "Cantina Italiana"),
    CAPIXABA(13, "Capixaba"),
    CARIBENHA(14, "Caribenha"),
    CARNES(15, "Carnes"),
    CASEIRA(16, "Caseira"),
    CHINESA(17, "Chinesa"),
    CHURRASCARIA(18, "Churrascaria"),
    COMIDINHAS(19, "Comidinhas"),
    CONTEMPORANEA(20, "Contemporânea"),
    COREANA(21, "Coreana"),
    DELICATESSEN(22, "Delicatessen"),
    ESPANHOLA(23, "Espanhola"),
    FEIJOADA(24, "Feijoada"),
    FRANCESA(25, "Francesa"),
    GALETERIA(26, "Galeteria"),
    GOIANA(27, "Goiana"),
    GREGA(28, "Grega"),
    HOLANDESA(29, "Holandesa"),
    INDIANA(30, "Indiana"),
    INTERNACIONAL(31, "Internacional"),
    IRANIANA(32, "Iraniana"),
    IRLANDESA(33, "Irlandesa"),
    ITALIANA(34, "Italiana"),
    ITALIANA_RODIZIO(35, "Rodízio Italiano"),
    JAPONESA(36, "Japonesa"),
    JUDAICA(37, "Judaica"),
    LANCHONETE(60, "Lanchonete"),
    MARROQUINA(38, "Marroquina"),
    MEDITERRANEA(39, "Mediterrânea"),
    MEXICANA(40, "Mexicana"),
    MINEIRA(41, "Mineira"),
    NORDESTINA(42, "Nordestina"),
    PADARIA(43, "Padaria"),
    PARANESE(44, "Paraense"),
    PEIXES_FRUTOS_DO_MAR(45, "Peixes e Frutos do Mar"),
    PERUANA(46, "Peruana"),
    PIZZARIA(47, "Pizzaria"),
    POLONESA(48, "Polonesa"),
    PORTUGUESA(49, "Portuguesa"),
    PUB(50, "Pub"),
    REGIONAL(51, "Regional"),
    RUSSA(52, "Russa"),
    SOPAS(53, "Sopas"),
    SORVETERIA(62, "Sorveteria"),
    SUICA(59, "Suiça"),
    TAILANDESA(54, "Tailandesa"),
    TURCA(55, "Turca"),
    URUGUAIA(56, "Uruguaia"),
    VARIADA(57, "Variada"),
    VEGETARIANA(58, "Vegetariana");

    private final int codigo;

    private final String defaultLocale = "pt_BR";

    private final HashMap<String, String> descricoes = new HashMap<String, String>();

    TipoCozinha(final int codigo, final String... descricoes) {
        this.codigo = codigo;

        final ArrayList<String> locales = new ArrayList<String>();
        locales.add("pt_BR");

        for (int i = 0; i < descricoes.length; i++) {
            this.descricoes.put(locales.get(i), descricoes[i]);
        }
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        final String descricao = this.descricoes.get(LocaleContextHolder.getLocale().toString());

        if (descricao == null) {
            this.descricoes.get(this.defaultLocale);
        }

        return descricao;

    }

}