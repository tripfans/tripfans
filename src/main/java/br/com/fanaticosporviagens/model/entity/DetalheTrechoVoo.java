package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author Pedro Sebba
 * @author Carlos Nascimento
 */
@Entity
@DiscriminatorValue("1")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class DetalheTrechoVoo extends DetalheTrecho {

    private static final long serialVersionUID = 9094425746663513295L;

    @Column
    private String alimentacao;

    @Column
    private String classe;

    @Column
    private String distancia;

    @Column
    private String entreterimento;

    @Column(name = "modelo_aeronave")
    private String modeloAeronave;

    @Column(name = "percentual_atraso_voos")
    private Integer percentualVoosAtrasados;

    @Column
    private String poltrona;

    public String getAlimentacao() {
        return this.alimentacao;
    }

    public String getClasse() {
        return this.classe;
    }

    public String getDistancia() {
        return this.distancia;
    }

    public String getEntreterimento() {
        return this.entreterimento;
    }

    public String getModeloAeronave() {
        return this.modeloAeronave;
    }

    public Integer getPercentualVoosAtrasados() {
        return this.percentualVoosAtrasados;
    }

    public String getPoltrona() {
        return this.poltrona;
    }

    public void setAlimentacao(final String alimentacao) {
        this.alimentacao = alimentacao;
    }

    public void setClasse(final String classe) {
        this.classe = classe;
    }

    public void setDistancia(final String distancia) {
        this.distancia = distancia;
    }

    public void setEntreterimento(final String entreterimento) {
        this.entreterimento = entreterimento;
    }

    public void setModeloAeronave(final String modeloAeronave) {
        this.modeloAeronave = modeloAeronave;
    }

    public void setPercentualVoosAtrasados(final Integer percentualVoosAtrasados) {
        this.percentualVoosAtrasados = percentualVoosAtrasados;
    }

    public void setPoltrona(final String poltrona) {
        this.poltrona = poltrona;
    }

}
