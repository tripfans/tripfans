package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public enum TipoServicoEmpresa implements EnumTypeInteger {

    ALIMENTACAO(3, "companhia_aerea.png", "Alimentação"),
    HOTELARIA(2, "companhia_aerea.png", "Hotelaria"),
    LOCACAO_VEICULOS(4, "companhia_aerea.png", "Locação de veículos"),
    TRANSPORTE_AEREO(1, "companhia_aerea.png", "Transporte Aéreo");

    /*
     * Retirado de www.viajeros.com
     * // TODO
     * Para servir de base para montarmos nossa propria lista
     * 
    Operador Turismo
    Bus
    Tour Operador
    Consulado/Embajada
    Operador Turismo Aventura
    Guía Turístico
    Transporte Aéreo
    Hospital/Clínica
    Centro de Buceo
    Taxi
    Aeropuerto
    Terminal de Buses
    Rent-a-car
    Transporte Marítimo
    Internet
    Transfer
    Farmacia
    Crucero
    Teleférico/Funicular
    Taller/Gomería
    Casa de Cambio
    Renta de Motos/Bicicletas
    Oficina de Turismo
    Accesorios para Viajeros
    Tienda de Fotografía
    Gasolinera
    Metro
    Lavandería
    
    */

    public static TipoServicoEmpresa getEnumPorCodigo(final int codigo) {
        for (final TipoServicoEmpresa type : values()) {
            if (type.getCodigo().equals(codigo)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Código de viagem inválido!");
    }

    private final Integer codigo;

    private final String descricao;

    private final String iconeMapa;

    private TipoServicoEmpresa(final Integer codigo, final String iconeMapa, final String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.iconeMapa = iconeMapa;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }
}
