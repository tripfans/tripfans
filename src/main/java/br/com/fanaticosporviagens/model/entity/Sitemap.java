package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "seo_sitemap")
public class Sitemap {

    @Id
    @Column(name = "id_seo_sitemap")
    private Long id;

    @ManyToMany
    @JoinTable(name = "seo_sitemap_local", joinColumns = { @JoinColumn(name = "id_seo_sitemap") }, inverseJoinColumns = { @JoinColumn(name = "id_local") })
    private List<Local> locais;

    @Column(name = "nm_seo_sitemap")
    private String nome;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seo_sitemap_type", nullable = false)
    private SitemapType tipo;

    @Column(name = "url_path")
    private String urlPath;

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public SitemapType getTipo() {
        return this.tipo;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setTipo(final SitemapType tipo) {
        this.tipo = tipo;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public List<Local> getLocais() {
        return locais;
    }

    public void setLocais(List<Local> locais) {
        this.locais = locais;
    }

}
