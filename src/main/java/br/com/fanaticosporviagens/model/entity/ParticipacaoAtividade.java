package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "participacao_atividade")
@TypeDef(name = "TipoParticipacao", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoParticipacaoAtividade") })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_participacao_atividade", allocationSize = 1)
public class ParticipacaoAtividade extends BaseEntity<Long> {

    private static final long serialVersionUID = 954350839872473505L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_atividade", nullable = false)
    private AtividadePlano atividadePlano;

    @Column
    private Boolean confirmado = Boolean.TRUE;

    @Column
    private String observacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_participante_viagem", nullable = false)
    private ParticipanteViagem participante;

    @Column(name = "programa_fidelidade")
    private String programaFidelidade;

    @Column
    private String ticket;

    @Type(type = "TipoParticipacao")
    @Column(name = "tipo_participacao")
    private TipoParticipacaoAtividade tipoParticipacao;

    @Column
    private Double valor;

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    public Boolean getConfirmado() {
        return this.confirmado;
    }

    public String getObservacao() {
        return this.observacao;
    }

    public ParticipanteViagem getParticipante() {
        return this.participante;
    }

    public String getProgramaFidelidade() {
        return this.programaFidelidade;
    }

    public String getTicket() {
        return this.ticket;
    }

    public TipoParticipacaoAtividade getTipoParticipacao() {
        return this.tipoParticipacao;
    }

    public Double getValor() {
        return this.valor;
    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
    }

    public void setConfirmado(final Boolean confirmado) {
        this.confirmado = confirmado;
    }

    public void setObservacao(final String observacao) {
        this.observacao = observacao;
    }

    public void setParticipante(final ParticipanteViagem participante) {
        this.participante = participante;
    }

    public void setProgramaFidelidade(final String programaFidelidade) {
        this.programaFidelidade = programaFidelidade;
    }

    public void setTicket(final String ticket) {
        this.ticket = ticket;
    }

    public void setTipoParticipacao(final TipoParticipacaoAtividade tipoParticipacao) {
        this.tipoParticipacao = tipoParticipacao;
    }

    public void setValor(final Double valor) {
        this.valor = valor;
    }
}
