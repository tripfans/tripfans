package br.com.fanaticosporviagens.model.entity;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.SocialShareableItem;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

/**
 * @author André Thiago
 *
 */
@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_dica")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_dica", allocationSize = 1)
@Table(name = "dica")
@DynamicUpdate
public class Dica extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel, Votavel, SocialShareableItem {

    private static final long serialVersionUID = -7501593361415392503L;

    private static final String TEXTO_TWITTER = "Escrevi uma dica sobre %s com TripFans: ";

    private static final String TITULO_FACEBOOK = "Escreveu uma dica sobre %s.";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_atividade", nullable = true)
    private AtividadePlano atividadePlano;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "autor", nullable = true)
    private Usuario autor;

    @Column(name = "descricao", nullable = false, length = 2000)
    private String descricao;

    /**
     * Id do usuario autor no facebook
     */
    @Column(name = "id_autor_facebook", nullable = true)
    private String idAutorFacebook;

    /**
     * Id da dica no foursquare
     */
    @Column(name = "id_foursquare", nullable = true)
    private String idFoursquare;

    @OneToMany(mappedBy = "dica", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ClassificacaoDica> itensClassificacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "local_dica", nullable = false)
    private Local localDica;

    @Column(name = "nome_autor")
    private String nomeAutor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_dica", nullable = true)
    private PedidoDica pedidoDica;

    @Column(name = "qtd_voto_util", nullable = false)
    private Long quantidadeVotoUtil = 0L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sugestao_local", nullable = true)
    private SugestaoLocal sugestaoLocal;

    @Column(name = "url_path", nullable = false, unique = true, updatable = false)
    private String urlPath;

    public void ajustarItensClassificacao() {
        if (this.itensClassificacao != null) {
            final Iterator<ClassificacaoDica> iterator = this.itensClassificacao.iterator();
            while (iterator.hasNext()) {
                final ClassificacaoDica classificacao = iterator.next();
                if (classificacao.getItem() == null) {
                    iterator.remove();
                } else {
                    classificacao.setDica(this);
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.localDica;
    }

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public Date getDataCadastro() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    public String getDescricao() {
        if (this.descricao != null) {
            return this.descricao.replaceAll("\r\n", "<br/>");
        }
        return null;
    }

    @Override
    public String getDescricaoAlvo() {
        if (getAlvo() != null) {
            return getAlvo().getDescricaoAlvo();
        }
        return null;
    }

    @Override
    public String getFacebookPostText() {
        return getDescricao();
    }

    @Override
    public String getFacebookPostTitle() {
        return String.format(TITULO_FACEBOOK, getLocalDica().getNome());
    }

    public String getIdAutorFacebook() {
        return this.idAutorFacebook;
    }

    public String getIdFoursquare() {
        return this.idFoursquare;
    }

    public Long getIdPedidoDica() {
        return this.pedidoDica != null ? this.pedidoDica.getId() : null;
    }

    public List<ClassificacaoDica> getItensClassificacao() {
        return this.itensClassificacao;
    }

    public Local getLocalDica() {
        return this.localDica;
    }

    public String getNomeAutor() {
        return this.nomeAutor;
    }

    public PedidoDica getPedidoDica() {
        return this.pedidoDica;
    }

    @Override
    public String getPrefix() {
        return "d";
    }

    @Override
    public Long getQuantidadeVotoUtil() {
        return this.quantidadeVotoUtil;
    }

    @Override
    public String getShareableLink() {
        final StringBuffer linkBuffer = new StringBuffer(UrlLocalResolver.getUrl(this.getLocalDica()));
        linkBuffer.append("/dicas?item=");
        if (getUrlPath() != null) {
            linkBuffer.append(this.getUrlPath());
        } else {
            linkBuffer.append(this.getId());
        }
        return linkBuffer.toString();
    }

    public SugestaoLocal getSugestaoLocal() {
        return this.sugestaoLocal;
    }

    @Override
    public String getTwitterPostText() {
        return String.format(TEXTO_TWITTER, getLocalDica().getNome());
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    @Override
    public void incrementaQuantidadeVotoUtil() {
        this.quantidadeVotoUtil += 1;
    }

    public boolean isImportadaFoursquare() {
        return this.idFoursquare != null;
    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
        this.nomeAutor = autor.getDisplayName();
    }

    public void setDataCadastro(final Date dataCadastro) {
        if (dataCadastro != null) {
            this.setDataCriacao(new LocalDateTime(dataCadastro));
        }
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setIdAutorFacebook(final String idAutorFacebook) {
        this.idAutorFacebook = idAutorFacebook;
    }

    public void setIdFoursquare(final String idFoursquare) {
        this.idFoursquare = idFoursquare;
    }

    public void setItensClassificacao(final List<ClassificacaoDica> itensClassificacao) {
        this.itensClassificacao = itensClassificacao;
    }

    public void setLocalDica(final Local localDica) {
        this.localDica = localDica;
    }

    public void setNomeAutor(final String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }

    public void setPedidoDica(final PedidoDica pedidoDica) {
        this.pedidoDica = pedidoDica;
    }

    public void setQuantidadeVotoUtil(final Long quantidadeVotoUtil) {
        this.quantidadeVotoUtil = quantidadeVotoUtil;
    }

    public void setSugestaoLocal(final SugestaoLocal sugestaoLocal) {
        this.sugestaoLocal = sugestaoLocal;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}
