package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;
import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;

/**
 * Representa os serviços que um {@link Hotel} disponibiliza aos seus hóspedes.
 * 
 * @author André Thiago
 * 
 */
public enum TipoServicoHotel implements EnumTypeInteger {

    ACADEMIA(20, "Academia"),
    AR_CONDICIONADO(1, "Ar-condicionado"),
    BANHEIRA_HIDROMASSAGEM(9, "Banheira de hidromassagem"),
    BAR(3, "Bar"),
    BUSINESS_CENTER(12, "Business Center"),
    CABELEIREIRO(16, "Cabeleireiro"),
    COFRE(2, "Cofre"),
    DEPOSITO_BAGAGENS(4, "Depósito de Bagagens"),
    ESTACIONAMENTO(19, "Estacionamento"),
    INTERNET(17, "Internet"),
    INTERNET_WIRELESS(18, "Internet Wireless"),
    LAVANDERIA(11, "Lavanderia"),
    MASSAGEM(8, "Massagem"),
    PISCINA(6, "Piscina"),
    RESTAURANTE(5, "Restaurante"),
    SAUNA(7, "Sauna"),
    SERVICO_PASSAR_ROUPAS(13, "Serviço de passar roupas"),
    SERVICOS_CRIANCAS(15, "Serviços para crianças"),
    SPA(10, "Spa"),
    TRASLADO_AEROPORTO(14, "Traslado para Aeroporto");

    private final EnumI18nUtil util;

    TipoServicoHotel(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

}
