package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_classificacao_dica")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_classificacao_dica", allocationSize = 1)
@Table(name = "classificacao_dica")
public class ClassificacaoDica extends BaseEntity<Long> {

    private static final long serialVersionUID = 2655136919297489232L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = false)
    private Dica dica;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_item_classificacao_dica", nullable = false)
    private ItemClassificacaoDica item;

    public String getDescricao() {
        return this.item.getDescricao();
    }

    public Dica getDica() {
        return this.dica;
    }

    public ItemClassificacaoDica getItem() {
        return this.item;
    }

    public void setDica(final Dica dica) {
        this.dica = dica;
    }

    public void setItem(final ItemClassificacaoDica item) {
        this.item = item;
    }

}
