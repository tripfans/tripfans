package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum ViagemTransporte implements EnumTypeInteger {
    AEREO(2, "plane.png", "Avião"),
    AQUATICO(1, "warn.png", "Navio/Barco"),
    TERRESTRE(3, "car.png", "Carro"),
    TERRESTRE_ALUGUEL_DE_CARRO(4, "car.png", "Aluguel de Carro"),
    TERRESTRE_BICICLETA(6, "car.png", "Bicicleta"),
    TERRESTRE_ONIBUS(7, "car.png", "Ônibus"),
    TERRESTRE_TAXI(5, "car.png", "Taxi");

    // TERRESTRE(3, "icon-transporte-terrestre.png", "Terrestre"),
    // TERRESTRE_ALUGUEL_DE_CARRO(4, "icon-transporte-terrestre-aluguel_carro.png", "Aluguél de Carro"),
    // TERRESTRE_TAXI(5, "icon-transporte-terrestre-taxi.png", "Taxi");

    public static ViagemTransporte getEnumPorCodigo(final Integer codigo) {
        for (final ViagemTransporte transporte : values()) {
            if (transporte.getCodigo().equals(codigo)) {
                return transporte;
            }
        }
        throw new IllegalArgumentException("Código do Transporte Inválido!");
    }

    public static ViagemTransporte[] valuesOrdenadosPorCodigo() {
        return new ViagemTransporte[] { TERRESTRE, AEREO, AQUATICO };
    }

    private String icone;

    private final EnumI18nUtil util;

    ViagemTransporte(final int codigo, final String icone, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        this.icone = icone;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public String getIcone() {
        return this.icone;
    }

}
