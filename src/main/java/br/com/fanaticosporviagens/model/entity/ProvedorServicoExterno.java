package br.com.fanaticosporviagens.model.entity;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@Table(name = "provedor_servico_externo")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_provedor_servico_externo", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_provedor_servico_externo", nullable = false, unique = true)) })
public class ProvedorServicoExterno extends BaseEntity<Long> {

    public enum Catalogo {
        FACEBOOK(2L), GOOGLE(1L);

        private final Long codigo;

        Catalogo(final Long codigo) {
            this.codigo = codigo;
        }

        public Long getCodigo() {
            return this.codigo;
        }
    }

    private static final long serialVersionUID = -4408796373980444121L;

    @Column(name = "nome")
    private String nome;

    @Transient
    private Set<TipoServicoExterno> tiposServico;

    public String getNome() {
        return this.nome;
    }

    public Set<TipoServicoExterno> getTiposServico() {
        return this.tiposServico;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setTiposServico(final Set<TipoServicoExterno> tiposServico) {
        this.tiposServico = tiposServico;
    }

}
