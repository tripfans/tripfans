package br.com.fanaticosporviagens.model.entity;

import java.lang.reflect.InvocationTargetException;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.beanutils.PropertyUtils;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "configuracao_emails")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_configuracao_emails", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_configuracao_emails", nullable = false, unique = true)) })
public class ConfiguracaoNotificacoesPorEmail extends BaseEntity<Long> {

    private static final long serialVersionUID = 4940537278377908842L;

    @Column(name = "aceitar_convite_tripfans")
    private Boolean aceitarConviteTripfans = true;

    @Column(name = "aceitar_convite_viagem")
    private Boolean aceitarConviteViagem = true;

    @Column(name = "aceitar_recomendacoes_lugares")
    private Boolean aceitarRecomendacaoLugares = true;

    @Column(name = "receber_comentarios_diario")
    private Boolean receberComentariosDiarioViagem = true;

    @Column(name = "receber_comentarios_diversos")
    private Boolean receberComentariosDicasRelatosAvaliacoes = true;

    @Column(name = "receber_comentarios_fotos")
    private Boolean receberComentariosFotos = true;

    @Column(name = "receber_comentarios_perfil")
    private Boolean receberComentariosPerfil = true;

    @Column(name = "receber_comentarios_plano")
    private Boolean receberComentariosPlanoViagem = true;

    @Column(name = "receber_convites_amigos")
    private Boolean receberConfirmacoesOuSolicitacoesAmizade = true;

    @Column(name = "receber_convites_grupos")
    private Boolean receberConfirmacoesOuSolicitacoesGrupo = true;

    @Column(name = "receber_convite_viagem")
    private Boolean receberConviteViagem = true;

    @Column(name = "receber_indicacoes_util")
    private Boolean receberIndicacoesDeUtil = false;

    @Column(name = "receber_noticias_locais_plano")
    private Boolean receberNoticiasSobreLugaresQuePlaneja = false;

    @Column(name = "receber_noticias_locais_interesse")
    private Boolean receberNoticiasSobreLugaresQueSegue = true;

    @Column(name = "receber_novidades")
    private Boolean receberNovidades = false;

    @Column(name = "receber_ofertas")
    private Boolean receberOfertas = false;

    @Column(name = "receber_pedido_dicas_viagem")
    private Boolean receberPedidoDica = true;

    @Column(name = "receber_recomendacoes_lugares")
    private Boolean receberRecomendacaoLugares = true;

    @Column(name = "receber_resposta_pedido_dicas_viagem")
    private Boolean receberRespostaPedidoDica = true;

    @Column(name = "receber_respostas")
    private Boolean receberRespostasPerguntasComentarios = false;

    @Column(name = "receber_convite_dicas_lugares")
    private Boolean receberSolicitacaoParaRecomendarLugares = true;

    @Column(name = "receber_convite_dicas_viagem")
    private Boolean receberSolicitacaoParaRecomendarViagem = true;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = true)
    private Usuario usuario;

    public Boolean getAceitarConviteTripfans() {
        return this.aceitarConviteTripfans;
    }

    public Boolean getAceitarConviteViagem() {
        return this.aceitarConviteViagem;
    }

    public Boolean getAceitarRecomendacaoLugares() {
        return this.aceitarRecomendacaoLugares;
    }

    public Boolean getReceberComentariosDiarioViagem() {
        return this.receberComentariosDiarioViagem;
    }

    public Boolean getReceberComentariosDicasRelatosAvaliacoes() {
        return this.receberComentariosDicasRelatosAvaliacoes;
    }

    public Boolean getReceberComentariosFotos() {
        return this.receberComentariosFotos;
    }

    public Boolean getReceberComentariosPerfil() {
        return this.receberComentariosPerfil;
    }

    public Boolean getReceberComentariosPlanoViagem() {
        return this.receberComentariosPlanoViagem;
    }

    public Boolean getReceberConfirmacoesOuSolicitacoesAmizade() {
        return this.receberConfirmacoesOuSolicitacoesAmizade;
    }

    public Boolean getReceberConfirmacoesOuSolicitacoesGrupo() {
        return this.receberConfirmacoesOuSolicitacoesGrupo;
    }

    public Boolean getReceberConviteViagem() {
        return this.receberConviteViagem;
    }

    public Boolean getReceberIndicacoesDeUtil() {
        return this.receberIndicacoesDeUtil;
    }

    public Boolean getReceberNoticiasSobreLugaresQuePlaneja() {
        return this.receberNoticiasSobreLugaresQuePlaneja;
    }

    public Boolean getReceberNoticiasSobreLugaresQueSegue() {
        return this.receberNoticiasSobreLugaresQueSegue;
    }

    public Boolean getReceberNovidades() {
        return this.receberNovidades;
    }

    public Boolean getReceberOfertas() {
        return this.receberOfertas;
    }

    public Boolean getReceberPedidoDica() {
        return this.receberPedidoDica;
    }

    public Boolean getReceberRecomendacaoLugares() {
        return this.receberRecomendacaoLugares;
    }

    public Boolean getReceberRespostaPedidoDica() {
        return this.receberRespostaPedidoDica;
    }

    public Boolean getReceberRespostasPerguntasComentarios() {
        return this.receberRespostasPerguntasComentarios;
    }

    public Boolean getReceberSolicitacaoParaRecomendarLugares() {
        return this.receberSolicitacaoParaRecomendarLugares;
    }

    public Boolean getReceberSolicitacaoParaRecomendarViagem() {
        return this.receberSolicitacaoParaRecomendarViagem;
    }

    public boolean getStatusConfiguracao(final String nomeDaConfiguracao) {
        try {
            return (Boolean) PropertyUtils.getProperty(this, nomeDaConfiguracao);
        } catch (final IllegalAccessException e) {
        } catch (final InvocationTargetException e) {
        } catch (final NoSuchMethodException e) {
        } catch (final Exception e) {
        }
        return false;
    }

    public boolean getStatusConfiguracao(final TipoNotificacaoPorEmail tipoNotificacao) {
        return getStatusConfiguracao(tipoNotificacao.getAtributoDaClasse());
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setAceitarConviteTripfans(final Boolean aceitarConviteTripfans) {
        this.aceitarConviteTripfans = aceitarConviteTripfans;
    }

    public void setAceitarConviteViagem(final Boolean aceitarConviteViagem) {
        this.aceitarConviteViagem = aceitarConviteViagem;
    }

    public void setAceitarRecomendacaoLugares(final Boolean aceitarRecomendacaoLugares) {
        this.aceitarRecomendacaoLugares = aceitarRecomendacaoLugares;
    }

    public void setReceberComentariosDiarioViagem(final Boolean receberComentariosDiarioViagem) {
        this.receberComentariosDiarioViagem = receberComentariosDiarioViagem;
    }

    public void setReceberComentariosDicasRelatosAvaliacoes(final Boolean receberComentariosDicasRelatosAvaliacoes) {
        this.receberComentariosDicasRelatosAvaliacoes = receberComentariosDicasRelatosAvaliacoes;
    }

    public void setReceberComentariosFotos(final Boolean receberComentariosFotos) {
        this.receberComentariosFotos = receberComentariosFotos;
    }

    public void setReceberComentariosPerfil(final Boolean receberComentariosPerfil) {
        this.receberComentariosPerfil = receberComentariosPerfil;
    }

    public void setReceberComentariosPlanoViagem(final Boolean receberComentariosPlanoViagem) {
        this.receberComentariosPlanoViagem = receberComentariosPlanoViagem;
    }

    public void setReceberConfirmacoesOuSolicitacoesAmizade(final Boolean receberConfirmacoesOuSolicitacoesAmizade) {
        this.receberConfirmacoesOuSolicitacoesAmizade = receberConfirmacoesOuSolicitacoesAmizade;
    }

    public void setReceberConfirmacoesOuSolicitacoesGrupo(final Boolean receberConfirmacoesOuSolicitacoesGrupo) {
        this.receberConfirmacoesOuSolicitacoesGrupo = receberConfirmacoesOuSolicitacoesGrupo;
    }

    public void setReceberConviteViagem(final Boolean receberConviteViagem) {
        this.receberConviteViagem = receberConviteViagem;
    }

    public void setReceberIndicacoesDeUtil(final Boolean receberIndicacoesDeUtil) {
        this.receberIndicacoesDeUtil = receberIndicacoesDeUtil;
    }

    public void setReceberNoticiasSobreLugaresQuePlaneja(final Boolean receberNoticiasSobreLugaresQuePlaneja) {
        this.receberNoticiasSobreLugaresQuePlaneja = receberNoticiasSobreLugaresQuePlaneja;
    }

    public void setReceberNoticiasSobreLugaresQueSegue(final Boolean receberNoticiasSobreLugaresQueSegue) {
        this.receberNoticiasSobreLugaresQueSegue = receberNoticiasSobreLugaresQueSegue;
    }

    public void setReceberNovidades(final Boolean receberNovidades) {
        this.receberNovidades = receberNovidades;
    }

    public void setReceberOfertas(final Boolean receberOfertas) {
        this.receberOfertas = receberOfertas;
    }

    public void setReceberPedidoDica(final Boolean receberPedidoDica) {
        this.receberPedidoDica = receberPedidoDica;
    }

    public void setReceberRecomendacaoLugares(final Boolean receberRecomendacaoLugares) {
        this.receberRecomendacaoLugares = receberRecomendacaoLugares;
    }

    public void setReceberRespostaPedidoDica(final Boolean receberRespostaPedidoDica) {
        this.receberRespostaPedidoDica = receberRespostaPedidoDica;
    }

    public void setReceberRespostasPerguntasComentarios(final Boolean receberRespostasPerguntasComentarios) {
        this.receberRespostasPerguntasComentarios = receberRespostasPerguntasComentarios;
    }

    public void setReceberSolicitacaoParaRecomendarLugares(final Boolean receberSolicitacaoParaRecomendarLugares) {
        this.receberSolicitacaoParaRecomendarLugares = receberSolicitacaoParaRecomendarLugares;
    }

    public void setReceberSolicitacaoParaRecomendarViagem(final Boolean receberSolicitacaoParaRecomendarViagem) {
        this.receberSolicitacaoParaRecomendarViagem = receberSolicitacaoParaRecomendarViagem;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
