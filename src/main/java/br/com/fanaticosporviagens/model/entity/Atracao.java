package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Andr� Thiago
 * 
 */
@Entity
@DiscriminatorValue("7")
public class Atracao extends LocalEnderecavel {

    private static final long serialVersionUID = 672267076499285856L;

}
