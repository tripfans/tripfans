package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * @author Gustavo Paiva
 * 
 */
@Entity
@Table(name = "local_url_path_antiga")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_local_url_path_antiga", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_local_url_path_antiga", nullable = false, unique = true)) })
@DynamicUpdate
public class LocalUrlPathAntiga extends BaseEntity<Long> {

    private static final long serialVersionUID = 2488379811036887846L;

    @Column(name = "dt_alteracao", nullable = true, updatable = false)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataAlteracao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = false, updatable = false)
    private Local local;

    @Column(name = "url_path", nullable = false, updatable = false)
    private String urlPath;

    public LocalDateTime getDataAlteracao() {
        return this.dataAlteracao;
    }

    public Local getLocal() {
        return this.local;
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setDataAlteracao(final LocalDateTime dataAlteracao) {
        this.dataAlteracao = dataAlteracao;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}