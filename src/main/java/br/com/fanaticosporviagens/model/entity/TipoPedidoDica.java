package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_tipo_pedido_dica")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_tipo_pedido_dica", allocationSize = 1)
@Table(name = "tipo_pedido_dica")
public class TipoPedidoDica extends BaseEntity<Long> {

    private static final long serialVersionUID = -6558519776332441601L;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_item_classificacao_dica", nullable = false)
    private ItemClassificacaoDica item;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = false)
    private PedidoDica pedidoDica;

    public TipoPedidoDica() {

    }

    public TipoPedidoDica(final PedidoDica pedidoDica, final ItemClassificacaoDica item) {
        this.pedidoDica = pedidoDica;
        this.item = item;
    }

    public String getDescricao() {
        return this.item.getDescricao();
    }

    public ItemClassificacaoDica getItem() {
        return this.item;
    }

    public PedidoDica getPedidoDica() {
        return this.pedidoDica;
    }

    public void setItem(final ItemClassificacaoDica item) {
        this.item = item;
    }

    public void setPedidoDica(final PedidoDica pedidoDica) {
        this.pedidoDica = pedidoDica;
    }

}
