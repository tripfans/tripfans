package br.com.fanaticosporviagens.model.entity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public class UsuarioConexao {

    private String accessToken;

    private String displayName;

    private Long expireTime;

    private String imageUrl;

    private String profileUrl;

    private String providerId;

    private String providerUserId;

    private Integer rank;

    private String refreshToken;

    private String secret;

    private String userId;
}
