package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@Table(name = "viagem_destino")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_viagem_destino")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_viagem_destino", allocationSize = 1)
public class DestinoViagem extends BaseEntity<Long> implements Comparable<DestinoViagem> {

    private static final long serialVersionUID = -6349615439439403442L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = false)
    private LocalGeografico destino;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dia_viagem_fim", nullable = true)
    private DiaViagem diaFim;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dia_viagem_inicio", nullable = true)
    private DiaViagem diaInicio;

    @Column
    @JoinColumn(name = "ordem")
    private Integer ordem;

    @Column(name = "quantidade_dias")
    private Integer quantidadeDias;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "id_viagem", nullable = true)
    private Viagem viagem;

    public void adicionaDia() {
        this.quantidadeDias++;
    }

    @Override
    public int compareTo(final DestinoViagem outro) {
        if (this.getId() > outro.getId()) {
            return 1;
        } else if (this.getId() < outro.getId()) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(final Object obj) {
        final DestinoViagem destinoViagem = (DestinoViagem) obj;
        if (this.getDestino().getId() == destinoViagem.getDestino().getId() && this.getViagem().getId() == destinoViagem.getViagem().getId()) {
            return true;
        }
        return false;
    }

    public LocalGeografico getDestino() {
        return this.destino;
    }

    public DiaViagem getDiaFim() {
        return this.diaFim;
    }

    public DiaViagem getDiaInicio() {
        if (this.diaInicio == null && this.ordem == 1) {
            return this.viagem.getDia(1);
        }
        return this.diaInicio;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    public Integer getQuantidadeDias() {
        return this.quantidadeDias;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    public void setDestino(final LocalGeografico destino) {
        this.destino = destino;
    }

    public void setDiaFim(final DiaViagem diaFim) {
        this.diaFim = diaFim;
    }

    public void setDiaInicio(final DiaViagem diaInicio) {
        this.diaInicio = diaInicio;
    }

    public void setOrdem(final Integer ordem) {
        this.ordem = ordem;
    }

    public void setQuantidadeDias(final Integer quantidadeDias) {
        this.quantidadeDias = quantidadeDias;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }
}
