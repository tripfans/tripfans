package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name = "viagem_dia")
// @SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_viagem_dia", allocationSize = 1)
public class DiaViagem {

    @OneToMany(mappedBy = "dia", fetch = FetchType.LAZY)
    @OrderBy("ordem")
    @IndexColumn(base = 0, name = "ORDEM")
    // @OrderColumn(name = "ORDEM")
    @Fetch(FetchMode.SUBSELECT)
    private List<Atividade> atividadesQueComecam;

    @OneToMany(mappedBy = "diaFim", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private List<Atividade> atividadesQueTerminam;

    @Id
    @Column(name = "id_viagem_dia")
    private Long id;

    @Column(name = "numero_dia")
    private Integer numero;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = false)
    private Viagem viagem;

    public DiaViagem() {
        super();
    }

    public DiaViagem(final Viagem viagem, final Integer numero) {
        super();
        this.viagem = viagem;
        this.numero = numero;
    }

    public boolean addAtividade(final Atividade atividade) {
        atividade.setViagem(null);
        atividade.setDia(this);
        atividade.setOrdem(this.atividadesQueComecam.size());
        return this.atividadesQueComecam.add(atividade);
    }

    public List<Atividade> getAtividades() {
        final List<Atividade> listaAtividades = new ArrayList<Atividade>();
        listaAtividades.addAll(this.getAtividadesQueComecam());
        listaAtividades.addAll(this.getAtividadesQueTerminam());
        return listaAtividades;
    }

    public List<Atividade> getAtividadesOrdenadas() {
        final ArrayList<Atividade> atividadesOrdenados = new ArrayList<Atividade>();
        if ((this.getAtividadesQueTerminam() != null)) {

            for (final Atividade atividade : this.getAtividadesQueTerminam()) {
                if (atividade.isTerminaEmOutroDia()) {
                    atividadesOrdenados.add(atividade);
                }
            }
        }
        if ((this.getAtividadesQueComecam() != null)) {
            atividadesOrdenados.addAll(this.getAtividadesQueComecam());
        }
        return atividadesOrdenados;
    }

    @Deprecated
    public List<Atividade> getAtividadesOrdenadas_old() {
        final ArrayList<Atividade> atividadesOrdenados = new ArrayList<Atividade>();

        if ((this.getAtividadesQueTerminam() != null)) {
            for (final Atividade atividade : this.getAtividadesQueTerminam()) {
                if (atividade.isTerminaEmOutroDia()) {
                    atividadesOrdenados.add(atividade);
                }
            }
        }
        if ((this.getAtividadesQueComecam() != null) && !this.getAtividadesQueComecam().isEmpty()) {
            Atividade atividade = this.getAtividadesQueComecam().get(0).getItemPrimeiro();
            atividadesOrdenados.add(atividade);
            while (atividade.getItemProximo() != null) {
                atividade = atividade.getItemProximo();
                atividadesOrdenados.add(atividade);
            }
        }

        return atividadesOrdenados;
    }

    public List<Atividade> getAtividadesQueComecam() {
        return this.atividadesQueComecam;
    }

    public List<Atividade> getAtividadesQueTerminam() {
        return this.atividadesQueTerminam;
    }

    public DiaViagem getCopia(final Viagem viagemCopia) {
        final DiaViagem copia = new DiaViagem();
        copia.setViagem(viagemCopia);

        copia.setNumero(getNumero());

        if (this.getAtividades() != null && !this.getAtividades().isEmpty()) {
            copia.setAtividadesQueComecam(new ArrayList<Atividade>());
            copia.setAtividadesQueTerminam(new ArrayList<Atividade>());
            Atividade atividadeCopiaAnterior = null;
            for (final Atividade atividade : this.getAtividadesQueComecam()) {
                final Atividade atividadeCopia = atividade.getCopia(copia);

                copia.getAtividadesQueComecam().add(atividadeCopia);

                if (atividadeCopiaAnterior != null) {
                    atividadeCopia.setItemAnterior(atividadeCopiaAnterior);
                    atividadeCopiaAnterior.setItemProximo(atividadeCopia);
                }

                atividadeCopiaAnterior = atividadeCopia;
            }
        }

        return copia;
    }

    public Date getDataViagem() {
        if (this.viagem.getDataInicio() == null) {
            return null;
        }
        final Calendar calendarViagem = Calendar.getInstance();
        calendarViagem.setTime(this.viagem.getDataInicio().toDate());
        calendarViagem.add(Calendar.DATE, (this.numero - 1));
        return calendarViagem.getTime();
    }

    public String getDescricao() {
        final SimpleDateFormat sdf = new SimpleDateFormat(" - EEEE, dd/MM/yyyy");

        final StringBuilder descricao = new StringBuilder();
        descricao.append("Dia ");
        descricao.append(getNumero());

        if (getDataViagem() != null) {
            descricao.append(sdf.format(getDataViagem()));
        }
        return descricao.toString();
    }

    public List<Local> getDestinos() {
        final Map<Long, Local> destinos = new LinkedHashMap<Long, Local>();
        Local destino = null;
        for (final Atividade atividade : this.getAtividadesOrdenadas()) {
            if (atividade.getLocal() != null) {
                destino = atividade.getLocal().getLocalizacao().getCidadeLocal();
                if (!destinos.containsKey(destino.getId())) {
                    destinos.put(destino.getId(), destino);
                }
            } else if (atividade.isAtividadeTransporte()) {
                if (atividade.isComecaNoDia(this.id) && atividade.getTransporteLocalOrigem() != null) {
                    if (atividade.getTransporteLocalOrigem() instanceof Cidade
                            || atividade.getTransporteLocalOrigem() instanceof LocalInteresseTuristico) {
                        destinos.put(atividade.getTransporteLocalOrigem().getId(), atividade.getTransporteLocalOrigem());
                    }
                } else if (atividade.isTerminaNoDia(this.id) && atividade.getTransporteLocalDestino() != null) {
                    if (atividade.getTransporteLocalDestino() instanceof Cidade
                            || atividade.getTransporteLocalDestino() instanceof LocalInteresseTuristico) {
                        destinos.put(atividade.getTransporteLocalDestino().getId(), atividade.getTransporteLocalDestino());
                    }
                }
            }
        }
        return new ArrayList<Local>(destinos.values());
    }

    public Long getId() {
        return this.id;
    }

    public Atividade getItemPrimeiro() {
        if ((this.getAtividadesQueComecam() != null) && !this.getAtividadesQueComecam().isEmpty()) {
            return this.getAtividadesQueComecam().get(0).getItemPrimeiro();
        }
        return null;
    }

    public Atividade getItemUltimo() {
        if ((this.getAtividadesQueComecam() != null) && !this.getAtividadesQueComecam().isEmpty()) {
            return this.getAtividadesQueComecam().get(0).getItemUltimo();
        }
        return null;
    }

    public List<Atividade> getItens() {
        return this.getAtividades();
    }

    public List<Atividade> getItensOrdenados() {
        return getAtividadesOrdenadas();
    }

    public Set<Local> getLocaisVisitados() {
        final Set<Local> locais = new HashSet<Local>();
        if (this.getAtividades() != null) {
            for (final Atividade atividade : this.getAtividades()) {
                locais.addAll(atividade.getLocaisVisitados());
            }
        }
        return locais;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public BigDecimal getValor() {
        BigDecimal valor = new BigDecimal(0);
        if (this.getAtividades() != null) {
            for (final Atividade atividade : this.getAtividadesQueComecam()) {
                if (atividade.getValor() != null) {
                    valor = valor.add(atividade.getValor());
                }
            }
        }
        return valor;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    public void setAtividadesQueComecam(final List<Atividade> atividadesQueComecam) {
        this.atividadesQueComecam = atividadesQueComecam;
    }

    public void setAtividadesQueTerminam(final List<Atividade> atividadesQueTerminam) {
        this.atividadesQueTerminam = atividadesQueTerminam;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNumero(final Integer numero) {
        this.numero = numero;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }
}
