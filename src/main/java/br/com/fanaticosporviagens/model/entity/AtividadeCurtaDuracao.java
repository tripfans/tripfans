package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Entity;

/**
 * Classe que representa Atividades que durem minutos ou horas
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
public class AtividadeCurtaDuracao extends AtividadePlano {

    private static final long serialVersionUID = 3262199763585816115L;

    /**
     * Duracao em minutos
     */
    private Integer duracao;

    @Override
    public String getDescricaoAtividade() {
        return getNomeLocal();
    }

    public Integer getDuracao() {
        return this.duracao;
    }

    public void setDuracao(final Integer duracao) {
        this.duracao = duracao;
    }

}
