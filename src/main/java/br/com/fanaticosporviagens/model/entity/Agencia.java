package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Pedro Sebba
 * 
 */
@Entity
@DiscriminatorValue("6")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class Agencia extends LocalEnderecavel {

    private static final long serialVersionUID = 6565371068198313198L;

}
