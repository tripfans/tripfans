package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.component.BreadCrumbItem;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;
import br.com.fanaticosporviagens.util.local.GeradorPlSqlAtualizacaoDesnormalizacao;

/**
 * Importante - Caso seja acrescentado ou tirado parâmetos nessa classe também devem ser atualizadas as classes:
 *
 * {@link LocalidadeEmbeddable} {@link GeradorPlSqlAtualizacaoDesnormalizacao}
 *
 *
 *
 *
 * @param <L>
 */
@Embeddable
@TypeDefs(value = { @TypeDef(name = "LocalTypeDef", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.LocalType") }) })
public class LocalEmbeddable<L extends Local> implements Serializable, BreadCrumbItem {

    private static final long serialVersionUID = -4163589217230692363L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "d_local_referenciado_id_foto_padrao", nullable = true)
    private Foto fotoPadrao;

    @Column(name = "d_local_referenciado_id", nullable = true)
    private Long id;

    @Column(name = "d_local_referenciado_id_facebook", nullable = true)
    private String idFacebook;

    @Column(name = "d_local_referenciado_id_foursquare", nullable = true)
    private String idFoursquare;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "d_local_referenciado_id", nullable = true, updatable = false, insertable = false)
    private L local;

    @Column(name = "d_local_referenciado_nome", nullable = true)
    private String nome;

    @Column(name = "d_local_referenciado_sigla", nullable = true)
    private String sigla;

    @Type(type = "LocalTypeDef")
    @Column(name = "d_local_referenciado_type", nullable = true)
    private LocalType tipo;

    @Column(name = "d_local_referenciado_url_path", nullable = true)
    private String urlPath;

    public LocalEmbeddable() {
        super();
    }

    public LocalEmbeddable(final L local) {
        super();
        setLocal(local);
    }

    public Foto getFotoPadrao() {
        return this.fotoPadrao;
    }

    public Long getId() {
        return this.id;
    }

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public String getIdFoursquare() {
        return this.idFoursquare;
    }

    public L getLocal() {
        return this.local;
    }

    @Override
    public String getNome() {
        return this.nome;
    }

    public String getSigla() {
        return this.sigla;
    }

    public LocalType getTipo() {
        return this.tipo;
    }

    @Override
    public String getUrl() {
        if (this.getTipo() != null && (this.getTipo().isTipoPais() || this.getTipo().isTipoEstado())) {
            return null;
        }
        return UrlLocalResolver.getUrl(this.getTipo(), getUrlPath());
    }

    public String getUrlPath() {
        return this.urlPath;
    }

    public void setFotoPadrao(final Foto fotoPadrao) {
        this.fotoPadrao = fotoPadrao;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setIdFoursquare(final String idFoursquare) {
        this.idFoursquare = idFoursquare;
    }

    public void setLocal(final L local) {
        this.local = local;
        this.id = local.getId();
        this.idFacebook = local.getIdFacebook();
        this.idFoursquare = local.getIdFoursquare();
        this.nome = local.getNome();
        this.urlPath = local.getUrlPath();
        this.tipo = local.getTipoLocal();

        if (local.getTipoLocal().isTipoLocalGeografico()) {
            final LocalGeografico localGeografico = (LocalGeografico) local;
            this.sigla = localGeografico.getSigla();
        }
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setSigla(final String sigla) {
        this.sigla = sigla;
    }

    public void setTipo(final LocalType tipo) {
        this.tipo = tipo;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}