package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.BatchSize;

import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

/**
 * Um local endereçável é um local que possui um endereço associado. Por possuir um endereço ele está associado a uma cidade. <br/>
 * São locais endereçáveis: atrações, hotéis, restaurantes.
 * 
 * @author André Thiago
 * 
 */
@Entity
public abstract class LocalEnderecavel extends Local {

    private static final long serialVersionUID = 2825349949805113357L;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_cidade")
    private Cidade cidade;

    @Deprecated
    @Column(name = "nm_local_cidade", nullable = true)
    private String cidadeNome;

    @Deprecated
    @Column(name = "url_path_local_cidade", nullable = true)
    private String cidadeUrlPath;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "local", orphanRemoval = false)
    @BatchSize(size = 16)
    private Set<ClassificacaoLocal> classificacoes;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_continente")
    private Continente continente;

    @Deprecated
    @Column(name = "nm_local_continente", nullable = true)
    private String continenteNome;

    @Deprecated
    @Column(name = "url_path_local_continente", nullable = true)
    private String continenteUrlPath;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_estado")
    private Estado estado;

    @Deprecated
    @Column(name = "nm_local_estado", nullable = true)
    private String estadoNome;

    @Deprecated
    @Column(name = "sg_local_estado", nullable = true)
    private String estadoSigla;

    @Deprecated
    @Column(name = "url_path_local_estado", nullable = true)
    private String estadoUrlPath;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "d_local_interesse_turistico_id", insertable = false, updatable = false)
    private LocalInteresseTuristico localInteresseTuristico;

    @Deprecated
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_pais")
    private Pais pais;

    @Deprecated
    @Column(name = "nm_local_pais", nullable = true)
    private String paisNome;

    @Deprecated
    @Column(name = "url_path_local_pais", nullable = true)
    private String paisUrlPath;

    @Column(name = "parceiro", nullable = false)
    private Boolean parceiro; // indica se é parceiro

    /**
     * Rede do hotel ou restaurante
     */
    @Column(name = "rede")
    private String rede;

    public LocalEnderecavel() {
        setTipoLocal(LocalType.getLocalTypePorClasse(getClass()));
    }

    /**
     * Utilize getLocalizacao().getCidade().getLocal();
     * 
     * @return
     */
    @Deprecated
    public Cidade getCidade() {
        return this.cidade;
    }

    /**
     * Utilize getLocalizacao().getCidadeNome();
     * 
     * @return
     */
    @Deprecated
    public String getCidadeNome() {
        return this.cidadeNome;
    }

    /**
     * Utilize getLocalizacao().getCidadeUrl();
     * 
     * @return
     */
    @Deprecated
    public String getCidadeUrl() {
        return UrlLocalResolver.getUrl(LocalType.CIDADE, getCidadeUrlPath());
    }

    /**
     * Utilize getLocalizacao().getCidadeUrlPath();
     * 
     * @return
     */
    @Deprecated
    public String getCidadeUrlPath() {
        return this.cidadeUrlPath;
    }

    public Set<ClassificacaoLocal> getClassificacoes() {
        return this.classificacoes;
    }

    /**
     * Utilize getLocalizacao().getContinente().getLocal();
     * 
     * @return
     */
    @Deprecated
    public Continente getContinente() {
        return this.continente;
    }

    /**
     * Utilize getLocalizacao().getContinenteNome();
     * 
     * @return
     */
    @Deprecated
    public String getContinenteNome() {
        return this.continenteNome;
    }

    /**
     * Utilize getLocalizacao().getContinenteUrl();
     * 
     * @return
     */
    @Deprecated
    public String getContinenteUrl() {
        return UrlLocalResolver.getUrl(LocalType.CONTINENTE, getContinenteUrlPath());
    }

    /**
     * Utilize getLocalizacao().getContinenteUrlPath();
     * 
     * @return
     */
    @Deprecated
    public String getContinenteUrlPath() {
        return this.continenteUrlPath;
    }

    /**
     * Utilize getLocalizacao().getEstado().getLocal();
     * 
     * @return
     */
    @Deprecated
    public Estado getEstado() {
        return this.estado;
    }

    /**
     * Utilize getLocalizacao().getEstadoNome();
     * 
     * @return
     */
    @Deprecated
    public String getEstadoNome() {
        return this.estadoNome;
    }

    /**
     * Utilize getLocalizacao().getEstadoSigla();
     * 
     * @return
     */
    @Deprecated
    public String getEstadoSigla() {
        return this.estadoSigla;
    }

    /**
     * Utilize getLocalizacao().getEstadoUrl();
     * 
     * @return
     */
    @Deprecated
    public String getEstadoUrl() {
        return UrlLocalResolver.getUrl(LocalType.ESTADO, getEstadoUrlPath());
    }

    /**
     * Utilize getLocalizacao().getEstadoUrlPath();
     * 
     * @return
     */
    @Deprecated
    public String getEstadoUrlPath() {
        return this.estadoUrlPath;
    }

    @Override
    public List<Local> getLocaisInteressados() {
        final List<Local> interessados = new ArrayList<Local>();

        interessados.add(this);
        if (this.cidade != null) {
            interessados.add(this.cidade);
        }
        if (this.estado != null) {
            interessados.add(this.estado);
        }
        if (this.pais != null) {
            interessados.add(this.pais);
        }
        if (this.continente != null) {
            interessados.add(this.continente);
        }

        return interessados;
    }

    public LocalInteresseTuristico getLocalInteresseTuristico() {
        return this.localInteresseTuristico;
    }

    public String getNomeCidadeComEstadoPais() {
        String nomeCidadeComEstadoPais = getCidadeNome();
        if (this.getEstadoNome() != null) {
            nomeCidadeComEstadoPais += ", " + this.getEstadoNome();
            if (this.getPaisNome() != null) {
                nomeCidadeComEstadoPais += ", " + this.getPaisNome();
            }
        }
        return nomeCidadeComEstadoPais;
    }

    public String getNomeCidadeComPais() {
        String nomeCidadeComPais = getCidadeNome();
        if (this.getPaisNome() != null) {
            nomeCidadeComPais += ", " + this.getPaisNome();
        }
        return nomeCidadeComPais;
    }

    public String getNomeCidadeComSiglaEstado() {
        String nomeCidadeComSiglaEstado = getCidadeNome();
        if (this.getEstadoSigla() != null) {
            nomeCidadeComSiglaEstado += ", " + this.getEstadoSigla();
        }
        return nomeCidadeComSiglaEstado;
    }

    public String getNomeCidadeComSiglaEstadoNomePais() {
        String nomeCidadeComSiglaEstadoNomePais = getCidadeNome();
        if (this.getEstadoSigla() != null) {
            nomeCidadeComSiglaEstadoNomePais += ", " + this.getEstadoSigla();
        }
        if (this.getPaisNome() != null) {
            nomeCidadeComSiglaEstadoNomePais += ", " + this.getPaisNome();
        }
        return nomeCidadeComSiglaEstadoNomePais;
    }

    @Override
    public Local getPai() {
        return this.cidade;
    }

    /**
     * Utilize getLocalizacao().getPais().getLocal();
     * 
     * @return
     */
    @Deprecated
    public Pais getPais() {
        return this.pais;
    }

    /**
     * Utilize getLocalizacao().getPaisNome();
     * 
     * @return
     */
    @Deprecated
    public String getPaisNome() {
        return this.paisNome;
    }

    /**
     * Utilize getLocalizacao().getPaisUrl();
     * 
     * @return
     */
    @Deprecated
    public String getPaisUrl() {
        return UrlLocalResolver.getUrl(LocalType.PAIS, getPaisUrlPath());
    }

    /**
     * Utilize getLocalizacao().getPaisUrlPath();
     * 
     * @return
     */
    @Deprecated
    public String getPaisUrlPath() {
        return this.paisUrlPath;
    }

    public Boolean getParceiro() {
        return this.parceiro;
    }

    public String getRede() {
        return this.rede;
    }

    public void setCidade(final Cidade cidade) {
        this.cidade = cidade;
    }

    public void setCidadeNome(final String cidadeNome) {
        this.cidadeNome = cidadeNome;
    }

    public void setCidadeUrlPath(final String cidadeUrlPath) {
        this.cidadeUrlPath = cidadeUrlPath;
    }

    public void setClassificacoes(final Set<ClassificacaoLocal> classificacoes) {
        this.classificacoes = classificacoes;
    }

    public void setContinente(final Continente continente) {
        this.continente = continente;
    }

    public void setContinenteNome(final String continenteNome) {
        this.continenteNome = continenteNome;
    }

    public void setContinenteUrlPath(final String continenteUrlPath) {
        this.continenteUrlPath = continenteUrlPath;
    }

    public void setEstado(final Estado estado) {
        this.estado = estado;
    }

    public void setEstadoNome(final String estadoNome) {
        this.estadoNome = estadoNome;
    }

    public void setEstadoSigla(final String estadoSigla) {
        this.estadoSigla = estadoSigla;
    }

    public void setEstadoUrlPath(final String estadoUrlPath) {
        this.estadoUrlPath = estadoUrlPath;
    }

    public void setLocalInteresseTuristico(final LocalInteresseTuristico localInteresseTuristico) {
        this.localInteresseTuristico = localInteresseTuristico;
    }

    public void setPais(final Pais pais) {
        this.pais = pais;
    }

    public void setPaisNome(final String paisNome) {
        this.paisNome = paisNome;
    }

    public void setPaisUrlPath(final String paisUrlPath) {
        this.paisUrlPath = paisUrlPath;
    }

    public void setParceiro(final Boolean parceiro) {
        this.parceiro = parceiro;
    }

    public void setRede(final String rede) {
        this.rede = rede;
    }

}
