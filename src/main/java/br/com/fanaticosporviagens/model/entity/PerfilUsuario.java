package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "perfil_usuario")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_perfil_usuario", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_perfil_usuario", nullable = false, unique = true)) })
public class PerfilUsuario extends BaseEntity<Long> {

    private static final long serialVersionUID = -8489237052665206963L;

    @Column(name = "mensagem_personalizada")
    private String mensagemPersonalizada;

    @Column(name = "mostrar_amigos_grupos")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarAmigosGrupos;

    @Column(name = "mostrar_foto")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarFoto;

    @Column(name = "mostrar_informacoes_adicionais")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarInformacoesAdicionais;

    // private VisibilidadePerfil mostrarLocationStreamSubscription;

    @Column(name = "mostrar_informacoes_basicas")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarInformacoesBasicas;

    @Column(name = "mostrar_proximas_viagens")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarProximasViagens;

    // private VisibilidadePerfil mostrarTravelStats;

    @Column(name = "mostrar_sites_perfis_adicionais")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarSitesPerfisAdicionais;

    @Column(name = "mostrar_ultimas_atividades")
    @Enumerated(EnumType.ORDINAL)
    private TipoVisibilidade mostrarUltimasAtividades;

    @Column(name = "publico")
    private Boolean publico;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = true)
    private Usuario usuario;

    public String getMensagemPersonalizada() {
        return this.mensagemPersonalizada;
    }

    public TipoVisibilidade getMostrarAmigosGrupos() {
        return this.mostrarAmigosGrupos;
    }

    public TipoVisibilidade getMostrarFoto() {
        return this.mostrarFoto;
    }

    public TipoVisibilidade getMostrarInformacoesAdicionais() {
        return this.mostrarInformacoesAdicionais;
    }

    public TipoVisibilidade getMostrarInformacoesBasicas() {
        return this.mostrarInformacoesBasicas;
    }

    public TipoVisibilidade getMostrarProximasViagens() {
        return this.mostrarProximasViagens;
    }

    public TipoVisibilidade getMostrarSitesPerfisAdicionais() {
        return this.mostrarSitesPerfisAdicionais;
    }

    public TipoVisibilidade getMostrarUltimasAtividades() {
        return this.mostrarUltimasAtividades;
    }

    public Boolean getPublico() {
        return this.publico;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setMensagemPersonalizada(final String mensagemPersonalizada) {
        this.mensagemPersonalizada = mensagemPersonalizada;
    }

    public void setMostrarAmigosGrupos(final TipoVisibilidade mostrarAmigosGrupos) {
        this.mostrarAmigosGrupos = mostrarAmigosGrupos;
    }

    public void setMostrarFoto(final TipoVisibilidade mostrarFoto) {
        this.mostrarFoto = mostrarFoto;
    }

    public void setMostrarInformacoesAdicionais(final TipoVisibilidade mostrarInformacoesAdicionais) {
        this.mostrarInformacoesAdicionais = mostrarInformacoesAdicionais;
    }

    public void setMostrarInformacoesBasicas(final TipoVisibilidade mostrarInformacoesBasicas) {
        this.mostrarInformacoesBasicas = mostrarInformacoesBasicas;
    }

    public void setMostrarProximasViagens(final TipoVisibilidade mostrarProximasViagens) {
        this.mostrarProximasViagens = mostrarProximasViagens;
    }

    public void setMostrarSitesPerfisAdicionais(final TipoVisibilidade mostrarSitesPerfisAdicionais) {
        this.mostrarSitesPerfisAdicionais = mostrarSitesPerfisAdicionais;
    }

    public void setMostrarUltimasAtividades(final TipoVisibilidade mostrarUltimasAtividades) {
        this.mostrarUltimasAtividades = mostrarUltimasAtividades;
    }

    public void setPublico(final Boolean publico) {
        this.publico = publico;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
