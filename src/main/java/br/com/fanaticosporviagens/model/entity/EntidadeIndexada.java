package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.beans.Field;

import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * POJO para representar uma entidade indexada, utilizado para resultados
 * de pesquisa textual e para indexação.
 *
 * @author André Thiago
 *
 */
public class EntidadeIndexada implements Indexavel, Serializable, Comparable<EntidadeIndexada> {

    public static final String FIELD_BAIRRO = "bairro";

    public static final String FIELD_ESTRELAS = "estrelas";

    public static final String FIELD_ID_CIDADE = "idCidade";

    public static final String FIELD_IDS_TAGS = "idsTags";

    public static final String FIELD_NOME = "nome";

    public static final String FIELD_TAGS = "tags";

    public static final String FIELD_TIPO_ENTIDADE = "tipoEntidade";

    public static final String INDEX_LOCAL_PREFIX = "L";

    public static final String INDEX_USUARIO_PREFIX = "U";

    private static final long serialVersionUID = 2081494475750097159L;

    @Field
    private String bairro;

    @Field
    private String cep;

    @Field
    private boolean cidadeDestaque;

    @Field
    private String edgyTextField;

    @Field
    private String endereco;

    @Field
    private Integer estrelas;

    @Field
    private boolean fotoPadraoAnonima;

    @Field
    private String id;

    @Field
    private String idCidade;

    @Field
    private String idEstado;

    @Field
    private String idFotoPanoramio;

    @Field
    private String idLocalTuristico;

    @Field
    private String idPais;

    @Field
    private List<Long> idsTags = new ArrayList<Long>();

    @Field
    private String idUsuarioFotoPanoramio;

    private boolean indexar;

    @Field
    private Double latitude;

    @Field
    private String localizacao;

    @Field
    private Double longitude;

    @Field
    private String nome;

    @Field
    private String nomeCidade;

    @Field
    private String nomeEstado;

    @Field
    private String nomeExibicao;

    @Field
    private String nomeLocalTuristico;

    @Field
    private String nomePais;

    @Field
    private Double notaTripAdvisor;

    @Field
    private Long quantidadeInteressesJaFoi;

    @Field
    private Integer quantidadeReviewsTripAdvisor;

    @Field
    private String siglaEstado;

    @Field
    private String siglaPais;

    @Field
    private List<String> tags = new ArrayList<String>();

    @Field
    private Integer tipoAgrupador;

    @Field
    private Integer tipoEntidade;

    @Field
    private String tokenField;

    @Field
    private Double tripfansRanking;

    @Field
    private String urlBooking;

    @Field
    private String urlFotoAlbum;

    @Field
    private String urlFotoBig;

    @Field
    private String urlFotoSmall;

    @Field
    private String urlPath;

    @Field
    private boolean usarFotoPanoramio;

    @Field
    private List<Integer> usuariosJaForam;

    public EntidadeIndexada() {
    }

    public EntidadeIndexada(final Indexavel indexavel) {
        super();
        this.id = indexavel.getId().toString();
        this.nome = indexavel.getNome();
        this.nomeExibicao = indexavel.getNomeExibicao();
        this.edgyTextField = indexavel.getEdgyTextField();
        this.tipoEntidade = indexavel.getTipoEntidade();
        this.tokenField = indexavel.getTokenField();
        this.urlPath = indexavel.getUrlPath();
        this.urlFotoAlbum = indexavel.getUrlFotoAlbum();
        this.urlFotoBig = indexavel.getUrlFotoBig();
        this.urlFotoSmall = indexavel.getUrlFotoSmall();
    }

    public EntidadeIndexada(final String nomeParaPesquisa) {
        this.edgyTextField = nomeParaPesquisa;
        this.tokenField = nomeParaPesquisa;
    }

    public void adicionaTag(final Long idTag, final String nomeTag) {
        this.idsTags.add(idTag);
        this.tags.add(nomeTag);
    }

    public void adicionaUsuariosJaForamLocal(final List<Integer> idsUsuarios) {
        if (this.usuariosJaForam == null) {
            this.usuariosJaForam = new ArrayList<Integer>();
        }
        this.usuariosJaForam.addAll(idsUsuarios);
    }

    public void ajustaAgrupador() {
        if (LocalType.getEnumPorCodigo(this.tipoEntidade).isTipoLocalInteresseTuristico()) {
            // os locais de interesse turístico são tratados como cidades na indexação
            this.tipoAgrupador = LocalType.CIDADE.getCodigo();
        } else {
            this.tipoAgrupador = this.tipoEntidade;
        }
    }

    @Override
    public int compareTo(final EntidadeIndexada toCompare) {
        if (this.getTripfansRanking() > toCompare.getTripfansRanking()) {
            return 1;
        } else if (getTripfansRanking() < toCompare.getTripfansRanking()) {
            return -1;
        }
        return 0;
    }

    public void configuraLocalizacao() {
        this.localizacao = "";
        this.localizacao += this.latitude.toString();
        this.localizacao += "," + this.longitude;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final EntidadeIndexada other = (EntidadeIndexada) obj;
        if (this.id == null) {
            if (other.id != null)
                return false;
        } else if (!this.id.equals(other.id))
            return false;
        return true;
    }

    public String getBairro() {
        return this.bairro;
    }

    public String getCategoria() {
        if (getTipoAgrupador() != null) {
            return CategoriasPesquisa.getDescricaoPorCodigo(getTipoAgrupador());
        }
        return null;
    }

    public String getCep() {
        return this.cep;
    }

    @Override
    public String getEdgyTextField() {
        return this.edgyTextField;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public Integer getEstrelas() {
        return this.estrelas;
    }

    @Override
    public Long getId() {
        return Long.valueOf(this.id.replace(INDEX_LOCAL_PREFIX, "").replace(INDEX_USUARIO_PREFIX, ""));
    }

    public String getIdCidade() {
        return this.idCidade;
    }

    public String getIdEstado() {
        return this.idEstado;
    }

    public String getIdFotoPanoramio() {
        return this.idFotoPanoramio;
    }

    public String getIdLocalTuristico() {
        return this.idLocalTuristico;
    }

    public String getIdPais() {
        return this.idPais;
    }

    public List<Long> getIdsTags() {
        return this.idsTags;
    }

    public String getIdUsuarioFotoPanoramio() {
        return this.idUsuarioFotoPanoramio;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    @JsonIgnore
    public Local getLocalEnderecavel() {
        Local local = null;
        if (this.tipoEntidade == LocalType.ATRACAO.getCodigo()) {
            local = new Atracao();
        } else if (this.tipoEntidade == LocalType.HOTEL.getCodigo()) {
            local = new Hotel();
            ((Hotel) local).setEstrelas(this.getEstrelas());
        } else if (this.tipoEntidade == LocalType.RESTAURANTE.getCodigo()) {
            local = new Restaurante();
        } else if (this.tipoEntidade == LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo()) {
            local = new LocalInteresseTuristico();
        }
        if (local != null) {
            local.setId(this.getId());
            if (local.getEndereco() == null) {
                local.setEndereco(new Endereco());
            }
            local.getEndereco().setBairro(this.getBairro());
            local.getEndereco().setCep(this.getCep());
            local.getEndereco().setDescricao(this.getEndereco());

            // local.setIdCidade(Long.valueOf(this.getIdCidade()));

            CoordenadaGeografica coordenadaGeografica = local.getCoordenadaGeografica();
            if (local.getCoordenadaGeografica() == null) {
                coordenadaGeografica = new CoordenadaGeografica();
            }

            if (this.getLatitude() != null) {
                coordenadaGeografica.setLatitude(this.getLatitude());
                coordenadaGeografica.setLongitude(this.getLongitude());
                local.setCoordenadaGeografica(coordenadaGeografica);
            }

            local.setNome(this.getNome());
            local.setQuantidadeInteressesJaFoi(this.getQuantidadeInteressesJaFoi());
            // local.setTipoEntidade();
            local.setUrlPath(this.getUrlPath());
            local.setUrlFotoAlbum(this.getUrlFotoAlbum());
            local.setUrlFotoBig(this.getUrlFotoBig());
            local.setUrlFotoSmall(this.getUrlFotoSmall());
            local.setFotoPadraoAnonima(this.isFotoPadraoAnonima());
            local.setUsarFotoPanoramio(this.isUsarFotoPanoramio());
            local.setIdFotoPanoramio(this.getIdFotoPanoramio());
            local.setIdUsuarioFotoPanoramio(this.getIdUsuarioFotoPanoramio());
            if (getNotaTripAdvisor() != null) {
                local.setNotaTripAdvisor(new BigDecimal(this.getNotaTripAdvisor()));
                local.setQuantidadeReviewsTripAdvisor(this.getQuantidadeReviewsTripAdvisor());
            }
            if (CollectionUtils.isNotEmpty(this.idsTags)) {
                local.setIdsTags(this.idsTags);
            }
        }
        return local;
    }

    public String getLocalizacao() {
        return this.localizacao;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    @Override
    public String getNome() {
        return this.nome;
    }

    public String getNomeCidade() {
        return this.nomeCidade;
    }

    public String getNomeComSiglaEstado() {
        if (StringUtils.isNotEmpty(this.siglaEstado)) {
            return this.nome + ", " + this.siglaEstado;
        }
        return this.nome;
    }

    public String getNomeComSiglaEstadoPais() {
        String nomeSiglaEstadoPais = this.nome;
        if (StringUtils.isNotEmpty(this.siglaEstado)) {
            nomeSiglaEstadoPais += ", " + this.siglaEstado;
        }
        if (StringUtils.isNotEmpty(this.nomePais)) {
            nomeSiglaEstadoPais += ", " + this.nomePais;
        }
        return nomeSiglaEstadoPais;
    }

    public String getNomeComSiglaEstadoSiglaPais() {
        String nomeSiglaEstadoSiglaPais = this.nome;
        if (StringUtils.isNotEmpty(this.siglaEstado)) {
            nomeSiglaEstadoSiglaPais += ", " + this.siglaEstado;
        }
        if (StringUtils.isNotEmpty(this.siglaPais)) {
            nomeSiglaEstadoSiglaPais += ", " + this.siglaPais;
        }
        return nomeSiglaEstadoSiglaPais;
    }

    public String getNomeEstado() {
        return this.nomeEstado;
    }

    /**
     * A descrição completa de um local retorna todo o caminho para chegar ao mesmo. Por exemplo, para a cidade Rio de Janeiro,
     * ele deve retornar Rio de Janeiro, Rio de Janeiro, Brasil
     *
     * @return a descrição completa de um local
     */
    @Override
    public String getNomeExibicao() {
        return this.nomeExibicao;
    }

    public String getNomeLocalTuristico() {
        return this.nomeLocalTuristico;
    }

    public String getNomePais() {
        return this.nomePais;
    }

    public Double getNotaTripAdvisor() {
        return this.notaTripAdvisor;
    }

    public Long getQuantidadeInteressesJaFoi() {
        return this.quantidadeInteressesJaFoi;
    }

    public Integer getQuantidadeReviewsTripAdvisor() {
        return this.quantidadeReviewsTripAdvisor;
    }

    public String getSiglaEstado() {
        return this.siglaEstado;
    }

    public String getSiglaPais() {
        return this.siglaPais;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public Integer getTipoAgrupador() {
        return this.tipoAgrupador;
    }

    @Override
    public Integer getTipoEntidade() {
        return this.tipoEntidade;
    }

    @Override
    public String getTokenField() {
        return this.tokenField;
    }

    public Double getTripfansRanking() {
        return this.tripfansRanking;
    }

    public String getUrl() {
        if (isLocal()) {
            return UrlLocalResolver.LOCAL_URL_MAPPING + "/" + getUrlPath();
        } else {
            return "/perfil/" + getUrlPath();
        }
    }

    public String getUrlBooking() {
        return this.urlBooking;
    }

    @Override
    public String getUrlFotoAlbum() {
        return this.urlFotoAlbum;
    }

    @Override
    public String getUrlFotoBig() {
        return this.urlFotoBig;
    }

    @Override
    public String getUrlFotoSmall() {
        return this.urlFotoSmall;
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public List<Integer> getUsuariosJaForam() {
        return this.usuariosJaForam;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
        return result;
    }

    public boolean isCidadeDestaque() {
        return this.cidadeDestaque;
    }

    public boolean isFotoPadraoAnonima() {
        return this.fotoPadraoAnonima;
    }

    public boolean isIndexar() {
        return this.indexar;
    }

    public boolean isLocal() {
        return this.tipoEntidade != CategoriasPesquisa.PESSOAS.getCodigo();
    }

    public boolean isUsarFotoPanoramio() {
        return this.usarFotoPanoramio;
    }

    public void montaNomeExibicao() {
        String descricaoCompleta = this.nome;
        if (this.nomeLocalTuristico != null && !this.tipoEntidade.equals(LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo())) {
            descricaoCompleta += ", " + this.nomeLocalTuristico;
        }
        if (this.nomeCidade != null && !this.tipoEntidade.equals(LocalType.CIDADE.getCodigo())) {
            descricaoCompleta += ", " + this.nomeCidade;
        }
        if (this.nomeEstado != null && !this.tipoEntidade.equals(LocalType.ESTADO.getCodigo())) {
            descricaoCompleta += ", " + this.nomeEstado;
        }
        if (this.nomePais != null && !this.tipoEntidade.equals(LocalType.PAIS.getCodigo())) {
            descricaoCompleta += ", " + this.nomePais;
        }
        this.nomeExibicao = descricaoCompleta;
        this.tokenField = this.nomeExibicao;
        this.edgyTextField = this.nomeExibicao;
    }

    public void setBairro(final String bairro) {
        this.bairro = bairro;
    }

    public void setCep(final String cep) {
        this.cep = cep;
    }

    public void setCidadeDestaque(final boolean cidadeDestaque) {
        this.cidadeDestaque = cidadeDestaque;
    }

    public void setEdgyTextField(final String edgyTextField) {
        this.edgyTextField = edgyTextField;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public void setEstrelas(final Integer estrelas) {
        this.estrelas = estrelas;
    }

    public void setFotoPadraoAnonima(final boolean fotoPadraoAnonima) {
        this.fotoPadraoAnonima = fotoPadraoAnonima;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setIdCidade(final String idCidade) {
        this.idCidade = idCidade;
    }

    public void setIdEstado(final String idEstado) {
        this.idEstado = idEstado;
    }

    public void setIdFotoPanoramio(final String idFotoPanoramio) {
        this.idFotoPanoramio = idFotoPanoramio;
    }

    public void setIdLocalTuristico(final String idLocalTuristico) {
        this.idLocalTuristico = idLocalTuristico;
    }

    public void setIdPais(final String idPais) {
        this.idPais = idPais;
    }

    public void setIdsTags(final List<Long> idsTags) {
        this.idsTags = idsTags;
    }

    public void setIdUsuarioFotoPanoramio(final String idUsuarioFotoPanoramio) {
        this.idUsuarioFotoPanoramio = idUsuarioFotoPanoramio;
    }

    public void setIndexar(final boolean indexar) {
        this.indexar = indexar;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLocalizacao(final String localizacao) {
        this.localizacao = localizacao;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeCidade(final String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public void setNomeEstado(final String nomeEstado) {
        this.nomeEstado = nomeEstado;
    }

    public void setNomeExibicao(final String nomeExibicao) {
        this.nomeExibicao = nomeExibicao;
    }

    public void setNomeLocalTuristico(final String nomeLocalTuristico) {
        this.nomeLocalTuristico = nomeLocalTuristico;
    }

    public void setNomePais(final String nomePais) {
        this.nomePais = nomePais;
    }

    public void setNotaTripAdvisor(final Double notaTripAdvisor) {
        this.notaTripAdvisor = notaTripAdvisor;
    }

    public void setQuantidadeInteressesJaFoi(final Long quantidadeInteressesJaFoi) {
        this.quantidadeInteressesJaFoi = quantidadeInteressesJaFoi;
    }

    public void setQuantidadeReviewsTripAdvisor(final Integer quantidadeReviewsTripAdvisor) {
        this.quantidadeReviewsTripAdvisor = quantidadeReviewsTripAdvisor;
    }

    public void setSiglaEstado(final String siglaEstado) {
        this.siglaEstado = siglaEstado;
    }

    public void setSiglaPais(final String siglaPais) {
        this.siglaPais = siglaPais;
    }

    public void setTags(final List<String> tags) {
        this.tags = tags;
    }

    public void setTipoAgrupador(final Integer tipoAgrupador) {
        this.tipoAgrupador = tipoAgrupador;
    }

    public void setTipoEntidade(final Integer tipoEntidade) {
        this.tipoEntidade = tipoEntidade;
    }

    public void setTokenField(final String tokenField) {
        this.tokenField = tokenField;
    }

    public void setTripfansRanking(final Double tripfansRanking) {
        this.tripfansRanking = tripfansRanking;
    }

    public void setUrlBooking(final String urlBooking) {
        this.urlBooking = urlBooking;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setUsarFotoPanoramio(final boolean usarFotoPanoramio) {
        this.usarFotoPanoramio = usarFotoPanoramio;
    }

    public void setUsuariosJaForam(final List<Integer> usuariosJaForam) {
        this.usuariosJaForam = usuariosJaForam;
    }

}
