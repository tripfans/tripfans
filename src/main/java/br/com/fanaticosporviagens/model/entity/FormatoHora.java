package br.com.fanaticosporviagens.model.entity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public enum FormatoHora {

    FORMATO_24_HORAS, FORMATO_AM_PM;

}
