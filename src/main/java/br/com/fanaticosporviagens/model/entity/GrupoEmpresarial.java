package br.com.fanaticosporviagens.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "grupo_empresarial")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_grupo_empresarial", allocationSize = 1)
@TypeDefs(value = { @TypeDef(name = "TipoServicoEmpresa", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoServicoEmpresa") }) })
public class GrupoEmpresarial extends BaseEntity<Long> {

    private static final long serialVersionUID = -1773213854374303665L;

    @Column(nullable = false)
    private Boolean ativo;

    @Column(nullable = false)
    private Boolean internacional;

    @Column(nullable = false)
    private Boolean nacional;

    @Column
    private String nome;

    @Column(name = "nome_fantasia", nullable = false)
    private String nomeFantasia;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_pais_origem")
    private Pais paisOrigem;

    @Column(name = "tipo_empresa", nullable = false)
    @Type(type = "TipoServicoEmpresa")
    private TipoServicoEmpresa tipoEmpresa;

    public Boolean getAtivo() {
        return this.ativo;
    }

    public Boolean getInternacional() {
        return this.internacional;
    }

    public Boolean getNacional() {
        return this.nacional;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeFantasia() {
        return this.nomeFantasia;
    }

    public Pais getPaisOrigem() {
        return this.paisOrigem;
    }

    public TipoServicoEmpresa getTipoEmpresa() {
        return this.tipoEmpresa;
    }

    public void setAtivo(final Boolean ativo) {
        this.ativo = ativo;
    }

    public void setInternacional(final Boolean internacional) {
        this.internacional = internacional;
    }

    public void setNacional(final Boolean nacional) {
        this.nacional = nacional;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeFantasia(final String nomeFantasia) {
        this.nomeFantasia = nomeFantasia;
    }

    public void setPaisOrigem(final Pais paisOrigem) {
        this.paisOrigem = paisOrigem;
    }

    public void setTipoEmpresa(final TipoServicoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }

}
