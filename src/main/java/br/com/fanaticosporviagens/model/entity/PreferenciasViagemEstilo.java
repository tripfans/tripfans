package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class PreferenciasViagemEstilo implements Serializable {

    private static final long serialVersionUID = 5191842111865811618L;

    @Column(name = "conforto")
    private Boolean conforto = false;

    @Column(name = "custo_beneficio")
    private Boolean custoBeneficio = false;

    @Column(name = "luxo")
    private Boolean luxo = false;

    @Column(name = "mochileiro")
    private Boolean mochileiro = false;

    public Boolean getConforto() {
        return this.conforto;
    }

    public Boolean getCustoBeneficio() {
        return this.custoBeneficio;
    }

    public Boolean getLuxo() {
        return this.luxo;
    }

    public Boolean getMochileiro() {
        return this.mochileiro;
    }

    public void setConforto(final Boolean conforto) {
        this.conforto = conforto;
    }

    public void setCustoBeneficio(final Boolean custoBeneficio) {
        this.custoBeneficio = custoBeneficio;
    }

    public void setLuxo(final Boolean luxo) {
        this.luxo = luxo;
    }

    public void setMochileiro(final Boolean mochileiro) {
        this.mochileiro = mochileiro;
    }
}