package br.com.fanaticosporviagens.model.entity;

public interface Votavel {

    public Usuario getAutor();

    public Long getQuantidadeVotoUtil();

    public void incrementaQuantidadeVotoUtil();

}
