package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "item_diario_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_item_diario_viagem", allocationSize = 1)
public class RelatoViagem extends BaseEntity<Long> implements Comentavel, Votavel {

    private static final long serialVersionUID = 6214530799441612605L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_atividade", nullable = true)
    private AtividadePlano atividadePlano;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @Column(name = "avaliar_atividade")
    private Boolean avaliarAtividade = Boolean.FALSE;

    @Column(name = "data_criacao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataCriacao;

    @Column(name = "data_publicacao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataPublicacao;

    @Column(name = "data_ultima_alteracao")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate dataUltimaAlteracao;

    @Column(name = "dia_fim")
    private Integer diaFim;

    @Column(name = "dia_inicio")
    private Integer diaInicio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_diario_viagem", nullable = true)
    private DiarioViagem diarioViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_foto_principal", nullable = true)
    private Foto fotoPrincipal;

    @Column(name = "importado")
    private Boolean importadoDoPlano = Boolean.FALSE;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = true)
    private Local local;

    @Column(name = "capitulo")
    private Integer numeroCapitulo;

    @Column
    private Boolean ocultar = Boolean.FALSE;

    @Column
    private String texto;

    @Column(name = "titulo_capitulo", nullable = true)
    private String tituloCapitulo;

    public AtividadePlano getAtividade() {
        return this.atividadePlano;
    }

    @Override
    public Usuario getAutor() {
        return getAtividade().getViagem().getCriador();
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public Boolean getAvaliarAtividade() {
        return this.avaliarAtividade;
    }

    public LocalDate getDataCriacao() {
        return this.dataCriacao;
    }

    public LocalDate getDataPublicacao() {
        return this.dataPublicacao;
    }

    public LocalDate getDataUltimaAlteracao() {
        return this.dataUltimaAlteracao;
    }

    @Override
    public String getDescricaoAlvo() {
        // TODO ver o nome que será retornado
        return null;
    }

    @Override
    public Usuario getDestinatario() {
        return getAtividade().getViagem().getCriador();
    }

    public Integer getDiaFim() {
        return this.diaFim;
    }

    public Integer getDiaInicio() {
        return this.diaInicio;
    }

    public DiarioViagem getDiarioViagem() {
        return this.diarioViagem;
    }

    public Foto getFotoPrincipal() {
        return this.fotoPrincipal;
    }

    public List<Foto> getFotos() {
        if (this.atividadePlano != null) {
            return this.atividadePlano.getFotos();
        }
        return null;
    }

    public Boolean getImportadoDoPlano() {
        return this.importadoDoPlano;
    }

    public Local getLocal() {
        return this.local;
    }

    public String getNomeLocal() {
        if (this.atividadePlano != null) {
            return this.atividadePlano.getNomeLocal();
        } else
            return this.local.getNome();
    }

    public Integer getNumeroCapitulo() {
        return this.numeroCapitulo;
    }

    public Boolean getOcultar() {
        return this.ocultar;
    }

    @Override
    public Long getQuantidadeVotoUtil() {
        // TODO Auto-generated method stub
        return null;
    }

    public String getTexto() {
        return this.texto;
    }

    public String getTituloCapitulo() {
        return this.tituloCapitulo;
    }

    @Override
    public void incrementaQuantidadeVotoUtil() {

    }

    public void setAtividade(final AtividadePlano atividadePlano) {
        this.atividadePlano = atividadePlano;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setAvaliarAtividade(final Boolean avaliarAtividade) {
        this.avaliarAtividade = avaliarAtividade;
    }

    public void setDataCriacao(final LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setDataPublicacao(final LocalDate dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public void setDataUltimaAlteracao(final LocalDate dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
    }

    public void setDiaFim(final Integer diaFim) {
        this.diaFim = diaFim;
    }

    public void setDiaInicio(final Integer diaInicio) {
        this.diaInicio = diaInicio;
    }

    public void setDiarioViagem(final DiarioViagem diarioViagem) {
        this.diarioViagem = diarioViagem;
    }

    public void setFotoPrincipal(final Foto fotoPrincipal) {
        this.fotoPrincipal = fotoPrincipal;
    }

    public void setImportadoDoPlano(final Boolean importadoDoPlano) {
        this.importadoDoPlano = importadoDoPlano;
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setNumeroCapitulo(final Integer numeroCapitulo) {
        this.numeroCapitulo = numeroCapitulo;
    }

    public void setOcultar(final Boolean ocultar) {
        this.ocultar = ocultar;
    }

    public void setTexto(final String texto) {
        this.texto = texto;
    }

    public void setTituloCapitulo(final String tituloCapitulo) {
        this.tituloCapitulo = tituloCapitulo;
    }

}
