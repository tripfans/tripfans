package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.model.entity.PlanoViagem.DiaViagem;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Table(name = "diario_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_diario_viagem", allocationSize = 1)
@TypeDefs(value = { @TypeDef(name = "FormatoExibicao", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.DiarioViagem$FormatoExibicao") }) })
public class DiarioViagem extends EntidadeGeradoraAcao<Long> implements Comentavel {

    private static final long serialVersionUID = -5369063793814865181L;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_acao_publicacao", nullable = true)
    private AcaoUsuario acaoPublicacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_album", nullable = true)
    private Album album;

    @Column(name = "compartilhado_facebook", nullable = true)
    private Boolean compartilhadoFacebook = false;

    @Column(name = "compartilhado_twitter", nullable = true)
    private Boolean compartilhadoTwitter = false;

    @Column(name = "exibir_introducao")
    private Boolean exibirIntroducao = Boolean.TRUE;

    @Column(name = "formato_exibicao")
    @Type(type = "FormatoExibicao")
    private FormatoExibicao formatoExibicao;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_foto_capa", nullable = true)
    private Foto fotoCapa;

    @OneToMany(mappedBy = "diarioViagem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @IndexColumn(name = "ordem")
    private List<RelatoViagem> relatos = new ArrayList<RelatoViagem>();

    @Column(name = "texto_introducao")
    private String textoIntroducao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = false)
    private PlanoViagem planoViagem;

    @Column(name = "tipo_visibilidade")
    @Type(type = "Visibilidade")
    private TipoVisibilidade visibilidade;

    public void addItem(final RelatoViagem relatos) {
        relatos.setDiarioViagem(this);
        this.relatos.add(relatos);
    }

    public AcaoUsuario getAcaoPublicacao() {
        return this.acaoPublicacao;
    }

    public Album getAlbum() {
        return this.album;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.planoViagem;
    }

    @Override
    public Usuario getAutor() {
        return this.getViagem().getCriador();
    }

    public Collection<Capitulo> getCapitulos() {
        final Map<Integer, Capitulo> mapaCapitulos = new HashMap<Integer, Capitulo>();

        for (final DiaViagem diaViagem : getViagem().getDiasViagem()) {
            mapaCapitulos.put(diaViagem.getNumero(), new Capitulo(diaViagem));
        }

        for (final RelatoViagem relato : getRelatos()) {
            mapaCapitulos.get(relato.getNumeroCapitulo()).getRelatos().add(relato);
        }
        return mapaCapitulos.values();
    }

    public Boolean getCompartilhadoFacebook() {
        return this.compartilhadoFacebook;
    }

    public Boolean getCompartilhadoTwitter() {
        return this.compartilhadoTwitter;
    }

    @Override
    public String getDescricaoAlvo() {
        // TODO ver o nome que será retornado
        return null;
    }

    public Boolean getExibirIntroducao() {
        return this.exibirIntroducao;
    }

    public FormatoExibicao getFormatoExibicao() {
        return this.formatoExibicao;
    }

    public Foto getFotoCapa() {
        return this.fotoCapa;
    }

    public List<RelatoViagem> getRelatos() {
        return this.relatos;
    }

    public String getTextoIntroducao() {
        return this.textoIntroducao;
    }

    public PlanoViagem getViagem() {
        return this.planoViagem;
    }

    public TipoVisibilidade getVisibilidade() {
        return this.visibilidade;
    }

    public boolean isAcaoPublicacaoNaoGerada() {
        return this.acaoPublicacao == null;
    }

    public boolean isRascunho() {
        return !isPublicado();
    }

    public void publicar() {
        final LocalDateTime data = new LocalDateTime();
        setDataPublicacao(data);
        getViagem().setDataPublicacao(data);
    }

    public void setAcaoPublicacao(final AcaoUsuario acaoPublicacao) {
        this.acaoPublicacao = acaoPublicacao;
    }

    public void setAlbum(final Album album) {
        this.album = album;
    }

    public void setCompartilhadoFacebook(final Boolean compartilhadoFacebook) {
        this.compartilhadoFacebook = compartilhadoFacebook;
    }

    public void setCompartilhadoTwitter(final Boolean compartilhadoTwitter) {
        this.compartilhadoTwitter = compartilhadoTwitter;
    }

    public void setExibirIntroducao(final Boolean exibirIntroducao) {
        this.exibirIntroducao = exibirIntroducao;
    }

    public void setFormatoExibicao(final FormatoExibicao formatoExibicao) {
        this.formatoExibicao = formatoExibicao;
    }

    public void setFotoCapa(final Foto fotoCapa) {
        this.fotoCapa = fotoCapa;
    }

    public void setRelatos(final List<RelatoViagem> relatos) {
        this.relatos = relatos;
    }

    public void setTextoIntroducao(final String textoIntroducao) {
        this.textoIntroducao = textoIntroducao;
    }

    public void setViagem(final PlanoViagem planoViagem) {
        this.planoViagem = planoViagem;
    }

    public void setVisibilidade(final TipoVisibilidade visibilidade) {
        this.visibilidade = visibilidade;
    }

    /**
     * Representa um conjunto de relatos. Se o tipo de exibição do diário for "Dia-a-Dia", então representa um dia
     * 
     * @author CarlosN
     * 
     */
    public class Capitulo {

        public DiaViagem diaViagem;

        public Integer numero;

        public List<RelatoViagem> relatos = new LinkedList<RelatoViagem>();

        public Capitulo(final DiaViagem diaViagem) {
            this.diaViagem = diaViagem;
            this.numero = diaViagem.getNumero();
        }

        public Capitulo(final Integer numero) {
            this.numero = numero;
        }

        public DiaViagem getDiaViagem() {
            return this.diaViagem;
        }

        public Integer getNumero() {
            return this.numero;
        }

        public Integer getQuantidadeRelatos() {
            if (this.relatos != null) {
                this.relatos.size();
            }
            return 0;
        }

        public List<RelatoViagem> getRelatos() {
            return this.relatos;
        }

        public void setItens(final List<RelatoViagem> relatos) {
            this.relatos = relatos;
        }

        public void setNumero(final Integer numero) {
            this.numero = numero;
        }

    }

    public enum FormatoExibicao implements EnumTypeInteger {

        CAPITULOS(1, "Capitulos"),
        DIA_A_DIA(3, "Dia-a-Dia"),
        LIVRE(2, "Livre");

        private final EnumI18nUtil util;

        FormatoExibicao(final int codigo, final String... descricoes) {
            this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        }

        @Override
        public Integer getCodigo() {
            return this.util.getCodigo();
        }

        @Override
        public String getDescricao() {
            return this.util.getDescricao();
        }
    }

}
