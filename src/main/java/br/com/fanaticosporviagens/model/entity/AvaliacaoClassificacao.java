package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_avaliacao_classificacao")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_avaliacao_classificacao", allocationSize = 1)
@Table(name = "avaliacao_classificacao", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_avaliacao", "id_avaliacao_criterio",
        "id_avaliacao_qualidade" }) })
@TypeDefs(value = {
        @TypeDef(name = "TipoAvaliacaoCriterio", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.AvaliacaoCriterio") }),
        @TypeDef(name = "TipoAvaliacaoQualidade", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade") }) })
public class AvaliacaoClassificacao extends BaseEntity<Long> {

    private static final long serialVersionUID = -5205600273002199317L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = false)
    private Avaliacao avaliacao;

    @Type(type = "TipoAvaliacaoCriterio")
    @Column(name = "id_avaliacao_criterio", nullable = false)
    private AvaliacaoCriterio criterio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_avaliado", nullable = false)
    private Local localAvaliado;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "id_avaliacao_qualidade", nullable = false)
    private AvaliacaoQualidade qualidade;

    public AvaliacaoClassificacao() {
    }

    public AvaliacaoClassificacao(final Avaliacao avaliacao, final AvaliacaoCriterio criterio, final AvaliacaoQualidade qualidade) {
        this.avaliacao = avaliacao;
        this.localAvaliado = avaliacao.getLocalAvaliado();
        this.criterio = criterio;
        this.qualidade = qualidade;
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public AvaliacaoCriterio getCriterio() {
        return this.criterio;
    }

    public Local getLocalAvaliado() {
        return this.localAvaliado;
    }

    public AvaliacaoQualidade getQualidade() {
        return this.qualidade;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setCriterio(final AvaliacaoCriterio criterio) {
        this.criterio = criterio;
    }

    public void setLocalAvaliado(final Local localAvaliado) {
        this.localAvaliado = localAvaliado;
    }

    public void setQualidade(final AvaliacaoQualidade qualidade) {
        this.qualidade = qualidade;
    }

}
