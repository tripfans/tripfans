package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Classe que reune diversas informacoes para montagem do ranking das cidades
 * 
 * @author Carlos Nascimento
 */
@Embeddable
public class CriteriosRankingCidade {

    /**
     * 6º Criterio - Cidades beneficiadas por serem proximas das sedes da Copa do Mundo de 2014
     */
    @Column(name = "beneficiada_copa_2014")
    private Boolean beneficiadaCopa2014;

    /**
     * 3º Criterio - Principais cidades, segundo o guia 4 rodas
     */
    @Column(name = "destaque_4_rodas")
    private Boolean destaque4Rodas;

    /**
     * 9º Criterio - Outras cidades com certa importancia para os viajantes, segundo o guia 4 rodas
     */
    @Column(name = "destaque_outras_4_rodas")
    private Boolean destaqueOutras4Rodas;

    /**
     * 2º Criterio - Cidades indutoras do desenvolvimento do turismo
     */
    @Column(name = "destino_indutor")
    private Boolean destinoIndutor;

    /**
     * 8º Criterio - Cidades quem contem atracoes/hoteis/restaurantes
     * dentre os melhores do ano, segundo o guia 4 rodas
     */
    @Column(name = "melhor_do_ano_4_rodas")
    private Boolean melhorDoAno4Rodas;

    /**
     * 4º Criterio - Cidades mais desejadas, segundo o Ministerio do Turismo
     */
    @Column(name = "ranking_mais_desejadas", unique = true)
    private Integer rankingMaisDesejadas;

    /**
     * 1º Criterio - Cidades mais visitadas, segundo o Ministerio do Turismo
     */
    @Column(name = "ranking_mais_visitadas", unique = true)
    private Integer rankingMaisVisitadas;

    /**
     * 7º Criterio - Cidades enumeradas em roteiros regionais, segundo o guia 4 rodas
     */
    @Column(name = "roteiro_4_rodas")
    private Boolean roteiro4Rodas;

    /**
     * 5º Criterio - Sedes da Copa do Mundo de 2014
     */
    @Column(name = "sede_copa_2014")
    private Boolean sedeCopa2014;

    public Boolean getBeneficiadaCopa2014() {
        return this.beneficiadaCopa2014;
    }

    public Boolean getDestaque4Rodas() {
        return this.destaque4Rodas;
    }

    public Boolean getDestaqueOutras4Rodas() {
        return this.destaqueOutras4Rodas;
    }

    public Boolean getDestinoIndutor() {
        return this.destinoIndutor;
    }

    public Boolean getMelhorDoAno4Rodas() {
        return this.melhorDoAno4Rodas;
    }

    public Integer getRankingMaisDesejadas() {
        return this.rankingMaisDesejadas;
    }

    public Integer getRankingMaisVisitadas() {
        return this.rankingMaisVisitadas;
    }

    public Boolean getRoteiro4Rodas() {
        return this.roteiro4Rodas;
    }

    public Boolean getSedeCopa2014() {
        return this.sedeCopa2014;
    }

    public void setBeneficiadaCopa2014(final Boolean beneficiadaCopa2014) {
        this.beneficiadaCopa2014 = beneficiadaCopa2014;
    }

    public void setDestaque4Rodas(final Boolean destaque4Rodas) {
        this.destaque4Rodas = destaque4Rodas;
    }

    public void setDestaqueOutras4Rodas(final Boolean destaqueOutras4Rodas) {
        this.destaqueOutras4Rodas = destaqueOutras4Rodas;
    }

    public void setDestinoIndutor(final Boolean destinoIndutor) {
        this.destinoIndutor = destinoIndutor;
    }

    public void setMelhorDoAno4Rodas(final Boolean melhorDoAno4Rodas) {
        this.melhorDoAno4Rodas = melhorDoAno4Rodas;
    }

    public void setRankingMaisDesejadas(final Integer rankingMaisDesejadas) {
        this.rankingMaisDesejadas = rankingMaisDesejadas;
    }

    public void setRankingMaisVisitadas(final Integer rankingMaisVisitadas) {
        this.rankingMaisVisitadas = rankingMaisVisitadas;
    }

    public void setRoteiro4Rodas(final Boolean roteiro4Rodas) {
        this.roteiro4Rodas = roteiro4Rodas;
    }

    public void setSedeCopa2014(final Boolean sedeCopa2014) {
        this.sedeCopa2014 = sedeCopa2014;
    }

}
