package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * 
 * @author André Thiago
 * 
 */
@Entity
@Table(name = "classificacao_local_classe")
public class ClasseLocal {

    @ManyToMany
    @JoinTable(name = "classificacao_local_categoria_classe", joinColumns = { @JoinColumn(name = "id_classificacao_local_classe") }, inverseJoinColumns = { @JoinColumn(name = "id_classificacao_local_categoria") })
    @OrderBy("descricao")
    private List<CategoriaLocal> categorias;

    @Id
    @Column(name = "id_classificacao_local_classe")
    private Integer codigo;

    @Column(name = "nm_classificacao_local_classe")
    private String descricao;

    public List<CategoriaLocal> getCategorias() {
        return this.categorias;
    }

    public Integer getCodigo() {
        return this.codigo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setCategorias(final List<CategoriaLocal> categorias) {
        this.categorias = categorias;
    }

    public void setCodigo(final Integer codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

}
