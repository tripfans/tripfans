package br.com.fanaticosporviagens.model.entity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public enum FormatoData {

    DD_MM_YYYY, MM_DD_YYYY;

}
