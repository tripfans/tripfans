package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.hibernate.annotations.Type;

@Embeddable
public class AvaliacaoConsolidada implements Serializable {

    private static final long serialVersionUID = -6241626766539348968L;

    @Type(type = "TipoAvaliacaoQualidade")
    @Column(name = "classificacao_geral", nullable = true)
    private AvaliacaoQualidade classificacaoGeral;

    @Column(name = "data_consolidacao_avaliacao", nullable = true)
    private Date dataConsolidacao;

    @Column(name = "nota_geral", nullable = true)
    private BigDecimal notaGeral;

    @Column(name = "qtd_avaliacao_bom", insertable = false, updatable = false)
    private Long quantidadeAvaliacaoBom = 0L;

    @Column(name = "qtd_avaliacao_excelente", insertable = false, updatable = false)
    private Long quantidadeAvaliacaoExcelente = 0L;

    @Column(name = "qtd_avaliacao_pessimo", insertable = false, updatable = false)
    private Long quantidadeAvaliacaoPessimo = 0L;

    @Column(name = "qtd_avaliacao_regular", insertable = false, updatable = false)
    private Long quantidadeAvaliacaoRegular = 0L;

    @Column(name = "qtd_avaliacao_ruim", insertable = false, updatable = false)
    private Long quantidadeAvaliacaoRuim = 0L;

    public AvaliacaoQualidade getClassificacaoGeral() {
        return this.classificacaoGeral;
    }

    public Date getDataConsolidacao() {
        return this.dataConsolidacao;
    }

    public BigDecimal getNotaGeral() {
        return this.notaGeral;
    }

    public Long getPorcentagemAvaliacaoBom() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoBom);
    }

    public Long getPorcentagemAvaliacaoExcelente() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoExcelente);
    }

    public Long getPorcentagemAvaliacaoPessimo() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoPessimo);
    }

    public Long getPorcentagemAvaliacaoPositivas() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoBom + this.quantidadeAvaliacaoExcelente);
    }

    public Long getPorcentagemAvaliacaoRegular() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoRegular);
    }

    public Long getPorcentagemAvaliacaoRuim() {
        return getPorcentagemAvaliacao(this.quantidadeAvaliacaoRuim);
    }

    public Long getQuantidadeAvaliacaoBom() {
        return this.quantidadeAvaliacaoBom;
    }

    public Long getQuantidadeAvaliacaoExcelente() {
        return this.quantidadeAvaliacaoExcelente;
    }

    public Long getQuantidadeAvaliacaoPessimo() {
        return this.quantidadeAvaliacaoPessimo;
    }

    public Long getQuantidadeAvaliacaoRegular() {
        return this.quantidadeAvaliacaoRegular;
    }

    public Long getQuantidadeAvaliacaoRuim() {
        return this.quantidadeAvaliacaoRuim;
    }

    public Long getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacaoBom + this.quantidadeAvaliacaoExcelente + this.quantidadeAvaliacaoRegular + this.quantidadeAvaliacaoRuim
                + this.quantidadeAvaliacaoPessimo;
    }

    public void setClassificacaoGeral(final AvaliacaoQualidade classificacaoGeral) {
        this.classificacaoGeral = classificacaoGeral;
    }

    public void setDataConsolidacao(final Date dataConsolidacao) {
        this.dataConsolidacao = dataConsolidacao;
    }

    public void setNotaGeral(final BigDecimal notaGeral) {
        this.notaGeral = notaGeral;
    }

    public void setQuantidadeAvaliacaoBom(final Long quantidadeAvaliacaoBom) {
        this.quantidadeAvaliacaoBom = quantidadeAvaliacaoBom;
    }

    public void setQuantidadeAvaliacaoExcelente(final Long quantidadeAvaliacaoExcelente) {
        this.quantidadeAvaliacaoExcelente = quantidadeAvaliacaoExcelente;
    }

    public void setQuantidadeAvaliacaoPessimo(final Long quantidadeAvaliacaoPessimo) {
        this.quantidadeAvaliacaoPessimo = quantidadeAvaliacaoPessimo;
    }

    public void setQuantidadeAvaliacaoRegular(final Long quantidadeAvaliacaoRegular) {
        this.quantidadeAvaliacaoRegular = quantidadeAvaliacaoRegular;
    }

    public void setQuantidadeAvaliacaoRuim(final Long quantidadeAvaliacaoRuim) {
        this.quantidadeAvaliacaoRuim = quantidadeAvaliacaoRuim;
    }

    private Long getPorcentagemAvaliacao(final Long quantidadeAvaliacoes) {
        final float f = ((float) quantidadeAvaliacoes / getQuantidadeAvaliacoes());
        return (long) (f * 100);
    }

}
