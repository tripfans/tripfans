package br.com.fanaticosporviagens.model.entity;

import java.util.Locale;

import org.springframework.context.MessageSource;

import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

public enum TipoVidaNoturna {
    ADULTO(1),
    AMERICAN_BAR(2),
    AO_VIVO(3),
    BAR_PISCINA(4),
    BAR_PRAIA(5),
    CAFE(6),
    CASA_NOTURNA(7),
    CASINO(8),
    CLUB_PRIVE(9),
    CONCERTO(10),
    DANCA(11),
    FAMILIAR(12),
    HAPPY_HOUR(13),
    PAQUERA(14),
    PIANO_BAR(15),
    PUB(16),
    SNACK_BAR(17),
    TAVERNA(18),
    TEATRO(19);

    private int codigo;

    private final MessageSource messageSource = SpringBeansProvider.getBean(MessageSource.class);

    TipoVidaNoturna(final int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public String getNome() {
        return this.messageSource.getMessage(this.getClass().getSimpleName() + "." + name(), null, Locale.getDefault());
    }
}
