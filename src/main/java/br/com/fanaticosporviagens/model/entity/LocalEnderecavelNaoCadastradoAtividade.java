package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

/**
 * @author CarlosN
 */
@Embeddable
public class LocalEnderecavelNaoCadastradoAtividade extends LocalNaoCadastradoAtividade {

    @Embedded
    private Endereco endereco = new Endereco();

    public LocalEnderecavelNaoCadastradoAtividade() {
    }

    public Endereco getEndereco() {
        return this.endereco;
    }

    public Endereco getEndereco(final boolean seForNullCria) {
        if (seForNullCria && this.endereco == null) {
            this.endereco = new Endereco();
        }
        return this.endereco;
    }

    public void setEndereco(final Endereco endereco) {
        this.endereco = endereco;
    }

}
