package br.com.fanaticosporviagens.model.entity;

import java.util.Arrays;

import org.springframework.context.i18n.LocaleContextHolder;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum Mes implements EnumTypeInteger {

    ABRIL(4, new String[] { "Abril" }),
    AGOSTO(8, new String[] { "Agosto" }),
    DEZEMBRO(12, new String[] { "Dezembro" }),
    FEVEREIRO(2, new String[] { "Fevereiro" }),
    JANEIRO(1, new String[] { "Janeiro" }),
    JULHO(7, new String[] { "Julho" }),
    JUNHO(6, new String[] { "Junho" }),
    MAIO(5, new String[] { "Maio" }),
    MARCO(3, new String[] { "Março" }),
    NOVEMBRO(11, new String[] { "Novembro" }),
    OUTUBRO(10, new String[] { "Outubro" }),
    SETEMBRO(9, new String[] { "Setembro" });

    public static Mes getMes(final Integer numeroMes) {
        for (int i = 0; i < values().length; i++) {
            if (values()[i].getCodigo() == numeroMes) {
                return values()[i];
            }
        }
        return null;
    }

    public static Mes[] getMesesOrdenados() {
        final Mes[] meses = values();
        Arrays.sort(meses, new MesesComparator());
        return meses;
    }

    private int codigo;

    private String[] descricoes;

    Mes(final int codigo, final String[] descricoes) {
        this.descricoes = descricoes;
        this.codigo = codigo;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        if (LocaleContextHolder.getLocale().toString().equals("pt_BR")) {
            return this.descricoes[0];
        }
        return null;
    }

}