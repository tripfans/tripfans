package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/*
 * @author Pedro Sebba
 */

public enum TipoParticipacaoAtividade implements EnumTypeInteger {

    ACOMPANHAR(1, "Acompanhar"),
    COMER(2, "Comer"),
    DIRIGIR(3, "Dirigir"),
    ENCONTRAR(4, "Encontrar"),
    HOSPEDAR(5, "Hospedar"),
    VIAJAR(6, "Viajar");

    private final EnumI18nUtil util;

    TipoParticipacaoAtividade(final int codigo, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public TipoParticipacaoAtividade getEnumPorCodigo(final Integer codigo) {
        for (final TipoParticipacaoAtividade tipoParticipacao : values()) {
            if (tipoParticipacao.getCodigo().equals(codigo)) {
                return tipoParticipacao;
            }
        }
        throw new IllegalArgumentException("Código do Tipo de Participação Inválido!");
    }

}