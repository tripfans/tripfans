package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author Carlos Nascimento
 */
@Entity
@DiscriminatorValue("6")
public class AluguelVeiculo extends AtividadeLongaDuracao {

    private static final long serialVersionUID = 1215288967953190848L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_grupo_empresarial", nullable = true)
    private GrupoEmpresarial empresa;

    @Override
    public String getDescricaoAtividade() {
        // TODO I18N
        return "Aluguel de veículo";
    }

    @Override
    public String getDescricaoFim() {
        // TODO I18N
        return "Devolução";
    }

    @Override
    public String getDescricaoInicio() {
        // TODO I18N
        return "Retirada";
    }

    public GrupoEmpresarial getEmpresa() {
        return this.empresa;
    }

    public void setEmpresa(final GrupoEmpresarial empresa) {
        this.empresa = empresa;
    }

}
