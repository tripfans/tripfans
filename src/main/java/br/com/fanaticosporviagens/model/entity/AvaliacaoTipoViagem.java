package br.com.fanaticosporviagens.model.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_avaliacao_tipo_viagem")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_avaliacao_tipo_viagem", allocationSize = 1)
@Table(name = "avaliacao_tipo_viagem")
@TypeDefs(value = { @TypeDef(name = "AvaliacaoTipoViagemTypeDef", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.TipoViagem") }) })
public class AvaliacaoTipoViagem extends BaseEntity<Long> {

    private static final long serialVersionUID = -8102575520949663541L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = false)
    private Avaliacao avaliacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_viagem", nullable = false)
    private TipoViagem tipo;

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public TipoViagem getTipo() {
        return this.tipo;
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setTipo(final TipoViagem tipo) {
        this.tipo = tipo;
    }

}
