package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

@Entity
@Table(name = "classificacao_local_categoria")
@DiscriminatorColumn(name = "local_type", discriminatorType = DiscriminatorType.INTEGER)
public class CategoriaLocal implements EnumTypeInteger {

    @ManyToMany
    @JoinTable(name = "classificacao_local_categoria_classe", joinColumns = { @JoinColumn(name = "id_classificacao_local_categoria") }, inverseJoinColumns = { @JoinColumn(name = "id_classificacao_local_classe") })
    @OrderBy("descricao")
    private List<ClasseLocal> classes;

    @Id
    @Column(name = "id_classificacao_local_categoria")
    private Integer codigo;

    @Column(name = "nm_classificacao_local_categoria")
    private String descricao;

    @Type(type = "LocalTypeDef")
    @Column(name = "id_local_type", nullable = false, insertable = false, updatable = false)
    private LocalType localType;

    public List<ClasseLocal> getClasses() {
        return this.classes;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;

    }

    public LocalType getLocalType() {
        return this.localType;
    }

    public void setClasses(final List<ClasseLocal> classes) {
        this.classes = classes;
    }

    public void setCodigo(final Integer codigo) {
        this.codigo = codigo;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setLocalType(final LocalType localType) {
        this.localType = localType;
    }

}
