package br.com.fanaticosporviagens.model.entity;

import java.util.List;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum LocalType implements EnumTypeInteger {

    AEROPORTO(5, Aeroporto.class, "marcador.png", "/resources/images/marcador.png", "Aeroporto", "Aeroportos", "Aeroportos", true, false,
              valueSeoItemScopeType("http://schema.org/Airport")),
    AGENCIA(6, Agencia.class, "marcador.png", "/resources/images/marcador.png", "Agência", "Agências", "Agências", true, false,
            valueSeoItemScopeType("http://schema.org/TravelAgency")),
    ATRACAO(7, Atracao.class, "attraction.png", "/resources/images/atracao.jpg", "Atração", "Atrações", "O que fazer", true, false,
            valueSeoItemScopeType("http://schema.org/TouristAttraction")),
    CIDADE(4, Cidade.class, "marcador.png", "/resources/images/localGeografico.jpg", "Cidade", "Cidades", "Cidades", false, true,
           valueSeoItemScopeType("http://schema.org/City")),
    CONTINENTE(1, Continente.class, "marcador.png", "/resources/images/localGeografico.jpg", "Continente", "Continentes", "Continentes", false, true,
               valueSeoItemScopeType("http://schema.org/Place")),
    ESTADO(3, Estado.class, "marcador.png", "/resources/images/localGeografico.jpg", "Estado", "Estados", "Estados", false, true,
           valueSeoItemScopeType("http://schema.org/State")),
    HOTEL(8, Hotel.class, "hotel.png", "/resources/images/hotel.jpg", "Hotel", "Hotéis", "Onde se hospedar", true, false,
          valueSeoItemScopeType("http://schema.org/LodgingBusiness")),
    IMOVEL_ALUGUEL_TEMPORADA(9, ImovelAluguelTemporada.class, "marcador.png", "/resources/images/marcador.png", "Imóvel para Aluguel de Temporada",
                             "Imoveis para Aluguel de Temporada", "Imoveis para Aluguel de Temporada", true, false,
                             valueSeoItemScopeType("http://schema.org/LodgingBusiness")),
    LOCADORA_VEICULOS(11, LocadoraVeiculos.class, "locadora.png", "/resources/images/locadora.png", "Locadora de veículos", "Locadoras de veículos",
                      "Locadoras de veículos", true, false, valueSeoItemScopeType("http://schema.org/AutoRental")),
    LOCAL_INTERESSE_TURISTICO(12, LocalInteresseTuristico.class, "marcador.png", "/resources/images/localGeografico.jpg",
                              "Local com interesse turístico", "Locais com interesse turístico", "Local com interesse turístico", false, true,
                              valueSeoItemScopeType("http://schema.org/City")),
    PAIS(2, Pais.class, "marcador.png", "/resources/images/localGeografico.jpg", "País", "Paises", "Paises", false, true,
         valueSeoItemScopeType("http://schema.org/Country")),
    RESTAURANTE(10, Restaurante.class, "restaurant.png", "/resources/images/restaurante.jpg", "Restaurante", "Restaurantes", "Onde comer", true,
                false, valueSeoItemScopeType("http://schema.org/Restaurant"));

    public static LocalType getEnumPorCodigo(final int codigo) {
        for (final LocalType type : values()) {
            if (type.getCodigo().equals(codigo)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Código de viagem inválido!");
    }

    public static LocalType getLocalTypePorClasse(final Class<? extends Local> clazz) {
        for (final LocalType type : values()) {
            if (type.getLocalClass().equals(clazz)) {
                return type;
            }
        }
        return null;
        // throw new IllegalArgumentException("Classe inválida!");
    }

    public static LocalType getLocalTypePorUrlPath(final String urlPath) {
        for (final LocalType tipo : LocalType.values()) {
            if (tipo.getUrlPath().equals(urlPath)) {
                return tipo;
            }
        }
        throw new IllegalArgumentException("UrlPath inválida. (" + urlPath + ")");
    }

    private static String valueSeoItemScopeType(final String seoItemScopeType) {
        return seoItemScopeType;
    }

    private Class<?> clazz;

    private final Integer codigo;

    private final String descricao;

    private final String descricaoPlural;

    private final String iconeAvatar;

    private final String iconeMapa;

    private final String seoItemScopeType;

    private final boolean tipoLocalEnderecavel;

    private final boolean tipoLocalGeografico;

    private final String tituloListaLocais;

    private LocalType(final Integer codigo, final Class<?> clazz, final String iconeMapa, final String iconeAvatar, final String descricao,
            final String descricaoPlural, final String tituloListaLocais, final boolean tipoLocalEnderecavel, final boolean tipoLocalGeografico,
            final String seoItemScopeType) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.descricaoPlural = descricaoPlural;
        this.clazz = clazz;
        this.iconeMapa = iconeMapa;
        this.iconeAvatar = iconeAvatar;
        this.tipoLocalEnderecavel = tipoLocalEnderecavel;
        this.tituloListaLocais = tituloListaLocais;
        this.tipoLocalGeografico = tipoLocalGeografico;
        this.seoItemScopeType = seoItemScopeType;
    }

    public List<AvaliacaoCriterio> getAvaliacoesCriterios() {
        return AvaliacaoCriterio.getCriterios(this);
    }

    public List<AvaliacaoItemBomPara> getAvaliacoesItensBomPara() {
        return AvaliacaoItemBomPara.getItens(this);
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    public String getDescricaoPlural() {
        return this.descricaoPlural;
    }

    public String getIconeAvatar() {
        return this.iconeAvatar;
    }

    public String getIconeMapa() {
        return this.iconeMapa;
    }

    public Class<?> getLocalClass() {
        return this.clazz;
    }

    public String getSeoItemScopeType() {
        return this.seoItemScopeType;
    }

    public String getTituloListaLocais() {
        return this.tituloListaLocais;
    }

    public String getUrlPath() {
        return this.toString().toLowerCase();
    }

    public boolean isTipoAeroporto() {
        return this == LocalType.AEROPORTO;
    }

    public boolean isTipoAgencia() {
        return this == LocalType.AGENCIA;
    }

    public boolean isTipoAtracao() {
        return this == LocalType.ATRACAO;
    }

    public boolean isTipoCidade() {
        return this == LocalType.CIDADE;
    }

    public boolean isTipoContinente() {
        return this == LocalType.CONTINENTE;
    }

    public boolean isTipoEstado() {
        return this == LocalType.ESTADO;
    }

    public boolean isTipoHotel() {
        return this == LocalType.HOTEL;
    }

    public boolean isTipoImovelAluguelTemporada() {
        return this == LocalType.IMOVEL_ALUGUEL_TEMPORADA;
    }

    public boolean isTipoLocalEnderecavel() {
        return this.tipoLocalEnderecavel;
    }

    public boolean isTipoLocalGeografico() {
        return this.tipoLocalGeografico;
    }

    public boolean isTipoLocalInteresseTuristico() {
        return this == LocalType.LOCAL_INTERESSE_TURISTICO;
    }

    public boolean isTipoPais() {
        return this == LocalType.PAIS;
    }

    public boolean isTipoRestaurante() {
        return this == LocalType.RESTAURANTE;
    }
}
