package br.com.fanaticosporviagens.model.entity;


public enum LocalTypeGroup {

    LOCAL_ENDERECAVEL(LocalType.AEROPORTO, LocalType.AGENCIA, LocalType.ATRACAO, LocalType.HOTEL, LocalType.IMOVEL_ALUGUEL_TEMPORADA,
                      LocalType.LOCADORA_VEICULOS, LocalType.RESTAURANTE),
    LOCAL_GEOGRAFICO(LocalType.CONTINENTE, LocalType.PAIS, LocalType.ESTADO, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO);

    private final LocalType[] types;

    LocalTypeGroup(final LocalType... types) {
        this.types = types;
    }

    public LocalType[] getTypes() {
        return this.types;
    }
}
