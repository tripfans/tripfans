package br.com.fanaticosporviagens.model.entity;

public enum AvaliacaoType {

    ATRACAO(7),
    CIDADE(4),
    CONTINENTE(1),
    ESTADO(3),
    HOTEL(5),
    PAIS(2),
    RESTAURANTE(6);

    private final Integer idType;

    private AvaliacaoType(final Integer idType) {
        this.idType = idType;
    }

    public Integer getIdType() {
        return this.idType;
    }
}
