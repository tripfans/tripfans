package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "noticia_sobre_local_para_local", uniqueConstraints = { @UniqueConstraint(columnNames = { "id_avaliacao_publicada",
        "id_dica_publicada", "id_local_interessado" }) })
public class NoticiaSobreLocalParaLocal extends NoticiaSobreLocal {

    private static final long serialVersionUID = -6649369047770152174L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_interessado", nullable = true)
    private Local localInteressadoNaNoticia;

    public NoticiaSobreLocalParaLocal() {
        super();
    }

    public NoticiaSobreLocalParaLocal(final Avaliacao avaliacaoPublicada, final Local localInteressadoNaNoticia) {
        super(avaliacaoPublicada);
        this.localInteressadoNaNoticia = localInteressadoNaNoticia;
    }

    public NoticiaSobreLocalParaLocal(final Dica dicaPublicada, final Local localInteressadoNaNoticia) {
        super(dicaPublicada);
        this.localInteressadoNaNoticia = localInteressadoNaNoticia;
    }

    public Local getLocalInteressadoNaNoticia() {
        return this.localInteressadoNaNoticia;
    }

    public void setLocalInteressadoNaNoticia(final Local localInteressadoNaNoticia) {
        this.localInteressadoNaNoticia = localInteressadoNaNoticia;
    }

}
