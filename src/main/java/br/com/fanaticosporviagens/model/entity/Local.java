package br.com.fanaticosporviagens.model.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.EmailValidator;
import org.apache.commons.validator.UrlValidator;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDateTime;
import org.springframework.util.AutoPopulatingList;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.component.BreadCrumb;
import br.com.fanaticosporviagens.infra.component.BreadCrumbGenerator;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;
import br.com.fanaticosporviagens.infra.util.UrlLocalResolver;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica.Perimetro;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author André Thiago
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "local_type", discriminatorType = DiscriminatorType.INTEGER)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_local")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_local", allocationSize = 1)
@Table(name = "local", uniqueConstraints = { @UniqueConstraint(columnNames = { "url_path" }) })
@TypeDefs(value = { @TypeDef(name = "LocalTypeDef", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.LocalType") }) })
@BatchSize(size = 10)
public abstract class Local extends BaseEntity<Long> implements UrlNameable, AlvoAcao, BreadCrumbGenerator {

    private static final long serialVersionUID = 1172696462226687348L;

    @Embedded
    private AvaliacaoConsolidada avaliacaoConsolidadaGeral;

    @Column(name = "cadastro_concluido")
    private Boolean cadastroConcluido = false;

    @Column(name = "candidato_exclusao")
    private Boolean candidatoExclusao;

    @Embedded
    private CoordenadaGeografica coordenadaGeografica = new CoordenadaGeografica();

    @Embedded
    @AttributeOverrides({ @AttributeOverride(name = "latitude", column = @Column(name = "latitude_fotos")),
        @AttributeOverride(name = "longitude", column = @Column(name = "longitude_fotos")) })
    private CoordenadaGeografica coordenadaGeograficaFotos = new CoordenadaGeografica();

    /**
     * Data que o usuario solicitou o cadastro deste local
     */
    @Column(name = "data_cadastro", nullable = true)
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime dataCadastro;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "email")
    private String email;

    @Embedded
    private Endereco endereco;

    @Column(name = "excluido")
    private Boolean excluido;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_foto", nullable = true)
    private Foto fotoPadrao;

    @Column(name = "foto_padrao_anonima")
    private Boolean fotoPadraoAnonima = true;

    @Transient
    @Valid
    private final List<Foto> fotos = new AutoPopulatingList<Foto>(Foto.class);

    @Column(name = "id_facebook")
    private String idFacebook;

    @Column(name = "id_foto_panoramio")
    private String idFotoPanoramio;

    @Column(name = "id_google_places")
    private String idGooglePlaces;

    @Transient
    private List<Long> idsTags;

    @Column(name = "id_usuario_foto_panoramio")
    private String idUsuarioFotoPanoramio;

    /**
     * ID do usuário que solicitou o cadastro deste local
     */
    @Column(name = "id_usuario_solicitante")
    private Long idUsuarioSolcitante;

    @Column(name = "informacoesAdicionais", length = 500)
    private String informacoesAdicionais;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_local_foursquare", nullable = true)
    private LocalFoursquare localFoursquare;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_local_google", nullable = true)
    private LocalGoogle localGoogle;

    @Embedded
    private LocalidadeEmbeddable localizacao;

    @Column(name = "mostrar_outras_fotos_panoramio")
    private Boolean mostrarOutrasFotosPanoramio = false;

    @Column(name = "nome")
    private String nome;

    @Column(name = "nota_booking", nullable = true, columnDefinition = "NUMERIC(4,2) DEFAULT 0", insertable = false, updatable = false)
    private BigDecimal notaBooking;

    @Column(name = "nota_ranking", nullable = true, columnDefinition = "NUMERIC(19,2) DEFAULT 0", insertable = false, updatable = false)
    private BigDecimal notaRanking;

    @Column(name = "nota_tripadvisor", nullable = true, columnDefinition = "NUMERIC(19,2) DEFAULT 0", insertable = false, updatable = false)
    private BigDecimal notaTripAdvisor;

    /**
     * Categorias advindas de redes sociais, como Foursquare e Google places
     */
    @Column(name = "outras_categorias")
    private String outrasCategorias;

    /** photo_reference do Google Places */
    @Column(name = "photo_reference_google_places")
    private String photoRefereceGooglePlaces;

    @Column(name = "qtd_avaliacao", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAvaliacoes = 0L;

    @Column(name = "qtd_dica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeDicas = 0L;

    @Column(name = "qtd_foto", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeFotos = 0L;

    @Column(name = "qtd_interesse", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteresses = 0L;

    @Column(name = "qtd_interesse_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesDesejaIr = 0L;

    @Column(name = "qtd_interesse_imperdivel", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesImperdivel = 0L;

    @Column(name = "qtd_interesse_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesIndica = 0L;

    @Column(name = "qtd_interesse_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesJaFoi = 0L;

    @Column(name = "qtd_interesse_local_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesLocalFavorito = 0L;

    @Column(name = "qtd_interesse_seguir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeInteressesSeguir = 0L;

    @Column(name = "qtd_noticia", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeNoticias = 0L;

    @Column(name = "qtd_pergunta", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePerguntas = 0L;

    @Column(name = "qtd_reviews_tripadvisor", nullable = true, columnDefinition = "NUMERIC(19,2) DEFAULT 0", insertable = false, updatable = false)
    private Integer quantidadeReviewsTripAdvisor;

    @Column(name = "ranking_kk", nullable = true, insertable = false, updatable = false)
    private Integer rankingKeKanto;

    /** reference do Google Places */
    @Column(name = "reference_google_places")
    private String referenceGooglePlaces;

    @Column(name = "site")
    private String site;

    @Column(name = "status_processo_validacao", updatable = false)
    private Boolean statusProcessoValidacao = true;

    @Column(name = "telefone")
    private String telefone;

    @Type(type = "LocalTypeDef")
    @Column(name = "local_type", nullable = false, insertable = false, updatable = false)
    private LocalType tipoLocal;

    @Column(name = "tripfans_ranking", nullable = true, insertable = false, updatable = false)
    private Integer tripFansRanking;

    @Column(name = "url_booking")
    private String urlBooking;

    @Column(name = "url_foto_album")
    private String urlFotoAlbum;

    @Column(name = "url_foto_big")
    private String urlFotoBig;

    @Column(name = "url_foto_small")
    private String urlFotoSmall;

    @Column(name = "url_path", unique = true)
    private String urlPath;

    @Column(name = "usar_foto_google_places")
    private Boolean usarFotoGooglePlaces = false;

    @Column(name = "usar_foto_panoramio")
    private Boolean usarFotoPanoramio = false;

    @Column(name = "validado")
    private Boolean validado = true;

    public void calcularAvaliacaoGeral(final AvaliacaoQualidade ultimaClassificacao) {
        long notaAnterior = 0;
        if (this.quantidadeAvaliacoes == null || this.quantidadeAvaliacoes == 0) {
            this.quantidadeAvaliacoes = 0L;
        } else {
            notaAnterior = this.avaliacaoConsolidadaGeral.getClassificacaoGeral().getNota() * this.quantidadeAvaliacoes;
        }
        this.quantidadeAvaliacoes += 1;
        final int notaAtual = BigDecimal.valueOf(notaAnterior + ultimaClassificacao.getNota())
                .divide(BigDecimal.valueOf(this.quantidadeAvaliacoes), BigDecimal.ROUND_HALF_UP).intValue();
        if (this.avaliacaoConsolidadaGeral == null) {
            this.avaliacaoConsolidadaGeral = new AvaliacaoConsolidada();
        }
        this.avaliacaoConsolidadaGeral.setClassificacaoGeral(AvaliacaoQualidade.getAvaliacaoQualidadePorNota(notaAtual));
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        final Local other = (Local) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        return true;
    }

    public AvaliacaoConsolidada getAvaliacaoConsolidadaGeral() {
        return this.avaliacaoConsolidadaGeral;
    }

    @Override
    @JsonIgnore
    public BreadCrumb getBreadCrumb() {
        return new BreadCrumb(this);
    }

    public Boolean getCadastroConcluido() {
        return this.cadastroConcluido;
    }

    public Boolean getCandidatoExclusao() {
        return this.candidatoExclusao;
    }

    public CoordenadaGeografica getCoordenadaGeografica() {
        if (this.coordenadaGeografica == null) {
            this.coordenadaGeografica = new CoordenadaGeografica();
        }
        return this.coordenadaGeografica;
    }

    public CoordenadaGeografica getCoordenadaGeograficaFotos() {
        if (this.coordenadaGeograficaFotos == null) {
            this.coordenadaGeograficaFotos = new CoordenadaGeografica();
        }
        return this.coordenadaGeograficaFotos;
    }

    public LocalDateTime getDataCadastro() {
        return this.dataCadastro;
    }

    public String getDescricao() {
        return this.descricao;
    }

    @Override
    public String getDescricaoAlvo() {
        return getNome();
    }

    @Override
    public Usuario getDestinatario() {
        return null;
    }

    public String getEmail() {
        return this.email;
    }

    public Endereco getEndereco() {
        return this.endereco;
    }

    public Endereco getEndereco(final boolean seForNullCria) {
        if (seForNullCria && this.endereco == null) {
            this.endereco = new Endereco();
        }
        return this.endereco;
    }

    public Boolean getExcluido() {
        return this.excluido;
    }

    public Foto getFotoPadrao() {
        return this.fotoPadrao;
    }

    public Boolean getFotoPadraoAnonima() {
        return this.fotoPadraoAnonima;
    }

    public List<Foto> getFotos() {
        return this.fotos;
    }

    public String getIconeAvatar() {
        return this.tipoLocal.getIconeAvatar();
    }

    public String getIconeMapa() {
        return this.tipoLocal.getIconeMapa();
    }

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public String getIdFotoPanoramio() {
        return this.idFotoPanoramio;
    }

    public String getIdFoursquare() {
        if (this.localFoursquare != null) {
            return this.localFoursquare.getId();
        }
        return null;
    }

    public String getIdGooglePlaces() {
        return this.idGooglePlaces;
    }

    public List<Long> getIdsTags() {
        return this.idsTags;
    }

    public String getIdUsuarioFotoPanoramio() {
        return this.idUsuarioFotoPanoramio;
    }

    public Long getIdUsuarioSolcitante() {
        return this.idUsuarioSolcitante;
    }

    public String getInformacoesAdicionais() {
        return this.informacoesAdicionais;
    }

    public Double getLatitude() {
        if (getCoordenadaGeografica() != null) {
            return this.coordenadaGeografica.getLatitude();
        }
        return null;
    }

    public List<Local> getLocaisInteressados() {
        final List<Local> interessados = new ArrayList<Local>();

        interessados.add(this);

        return interessados;
    }

    public LocalFoursquare getLocalFoursquare() {
        return this.localFoursquare;
    }

    public LocalGoogle getLocalGoogle() {
        return this.localGoogle;
    }

    public LocalidadeEmbeddable getLocalizacao() {
        return this.localizacao;
    }

    public String getLocalizacaoCompleta() {

        final List<LocalEmbeddable<? extends Local>> locaisEmOrdemHierarquica = getLocalizacao().getLocaisEmOrdemHierarquica();

        final StringBuilder nomeCompleto = new StringBuilder();

        for (int i = 1; i < locaisEmOrdemHierarquica.size(); i++) {
            final LocalEmbeddable<? extends Local> local = locaisEmOrdemHierarquica.get(i);
            if (local.getTipo() != null && !local.getTipo().isTipoContinente()) {
                if (nomeCompleto.length() != 0) {
                    nomeCompleto.append(", ");
                }
                nomeCompleto.append(local.getNome());
            }
        }

        return nomeCompleto.toString();
    }

    public Double getLongitude() {
        if (getCoordenadaGeografica() != null) {
            return this.coordenadaGeografica.getLongitude();
        }
        return null;
    }

    public Boolean getMostrarOutrasFotosPanoramio() {
        return this.mostrarOutrasFotosPanoramio;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeCompleto() {
        String nomeCompleto = this.getNome();
        if (this.getPai() != null) {
            nomeCompleto += ", " + getLocalizacaoCompleta();
        }
        return nomeCompleto;
    }

    public String getNomeCompletoAte(final LocalType localType) {
        final StringBuilder nomeCompleto = new StringBuilder();
        nomeCompleto.append(this.getNome());
        Local pai = this.getPai();
        while (pai != null) {
            nomeCompleto.append(", ");
            nomeCompleto.append(pai.getNome());
            if (pai.getTipoLocal().equals(localType)) {
                return nomeCompleto.toString();
            }
            pai = pai.getPai();
        }
        return nomeCompleto.toString();
    }

    public BigDecimal getNotaBooking() {
        return this.notaBooking;
    }

    public BigDecimal getNotaRanking() {
        return this.notaRanking;
    }

    public BigDecimal getNotaTripAdvisor() {
        return this.notaTripAdvisor;
    }

    public String getOutrasCategorias() {
        return this.outrasCategorias;
    }

    public abstract Local getPai();

    /**
     * Utilizado para marcar uma area no mapa que pode ser utilizada para buscar locais ou fotos nas proximidades
     *
     * @return
     */
    public Perimetro getPerimetroParaConsultaNoMapa() {
        Double espaco = 0.0D;
        if (this.isTipoCidade()) {
            espaco = 0.03;
        } else {
            espaco = 0.0005;
        }
        CoordenadaGeografica coordenada = null;
        if (getCoordenadaGeograficaFotos() != null && getCoordenadaGeograficaFotos().getLatitude() != null) {
            coordenada = getCoordenadaGeograficaFotos();
        } else {
            coordenada = getCoordenadaGeografica();
        }
        if (coordenada != null && coordenada.getLatitude() != null) {
            return coordenada.getPerimetro(espaco);
        }
        return null;
    }

    public String getPhotoRefereceGooglePlaces() {
        return this.photoRefereceGooglePlaces;
    }

    @Override
    public String getPrefix() {
        return "l";
    }

    public Long getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public Long getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public Long getQuantidadeFotos() {
        return this.quantidadeFotos;
    }

    public Long getQuantidadeInteresses() {
        return this.quantidadeInteresses;
    }

    public Long getQuantidadeInteressesDesejaIr() {
        return this.quantidadeInteressesDesejaIr;
    }

    public Long getQuantidadeInteressesImperdivel() {
        return this.quantidadeInteressesImperdivel;
    }

    public Long getQuantidadeInteressesIndica() {
        return this.quantidadeInteressesIndica;
    }

    public Long getQuantidadeInteressesJaFoi() {
        return this.quantidadeInteressesJaFoi;
    }

    public Long getQuantidadeInteressesLocalFavorito() {
        return this.quantidadeInteressesLocalFavorito;
    }

    public Long getQuantidadeInteressesSeguir() {
        return this.quantidadeInteressesSeguir;
    }

    public Long getQuantidadeNoticias() {
        return this.quantidadeNoticias;
    }

    public Long getQuantidadePerguntas() {
        return this.quantidadePerguntas;
    }

    public Integer getQuantidadeReviewsTripAdvisor() {
        return this.quantidadeReviewsTripAdvisor;
    }

    public Integer getRankingKeKanto() {
        return this.rankingKeKanto;
    }

    public String getReferenceGooglePlaces() {
        return this.referenceGooglePlaces;
    }

    public String getSeoItemScopeType() {
        return getTipoLocal().getSeoItemScopeType();
    }

    public String getSite() {
        return this.site;
    }

    public Boolean getStatusProcessoValidacao() {
        return this.statusProcessoValidacao;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public LocalType getTipoLocal() {
        return this.tipoLocal;
    }

    public List<Local> getTodosPais() {
        final List<Local> pais = new ArrayList<Local>();
        Local local = this;
        while (local.getPai() != null) {
            pais.add(local.getPai());
            local = local.getPai();
        }
        return pais;
    }

    @JsonIgnore
    public String getUrl() {
        return UrlLocalResolver.getUrl(this);
    }

    public String getUrlBooking() {
        return this.urlBooking;
    }

    public String getUrlFotoAlbum() {
        if (this.urlFotoAlbum == null && getFotoPadrao() != null) {
            return getFotoPadrao().getUrlAlbum();
        }
        return this.urlFotoAlbum;
    }

    public String getUrlFotoAvatar() {
        if (this.urlFotoSmall == null && getFotoPadrao() != null) {
            return getFotoPadrao().getUrlAvatar();
        }
        return this.urlFotoSmall;
    }

    public String getUrlFotoBig() {
        return this.urlFotoBig;
    }

    public String getUrlFotoCapa() {
        if (this.urlFotoBig == null && getFotoPadrao() != null) {
            return getFotoPadrao().getUrlBig();
        }
        return this.urlFotoBig;
    }

    public String getUrlFotoSmall() {
        return this.urlFotoSmall;
    }

    /*public boolean isFotoPadraoAnonima() {
        return Boolean.TRUE.equals(this.fotoPadraoAnonima) || this.fotoPadrao == null;
    }*/

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public Boolean getUsarFotoGooglePlaces() {
        return this.usarFotoGooglePlaces;
    }

    public Boolean getUsarFotoPanoramio() {
        if (this.usarFotoPanoramio == null) {
            return false;
        }
        return this.usarFotoPanoramio;
    }

    public Boolean getValidado() {
        return this.validado;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }

    public boolean isPossuiAlgumInteresse() {
        return this.quantidadeInteressesDesejaIr != 0 || this.quantidadeInteressesJaFoi != 0 || this.quantidadeInteressesLocalFavorito != 0
                || this.quantidadeInteressesSeguir != 0;
    }

    public boolean isTipoAeroporto() {
        return this.tipoLocal.isTipoAeroporto();
    }

    public boolean isTipoAgencia() {
        return this.tipoLocal.isTipoAgencia();
    }

    public boolean isTipoAtracao() {
        return this.tipoLocal.isTipoAtracao();
    }

    public boolean isTipoCidade() {
        return this.tipoLocal.isTipoCidade();
    }

    public boolean isTipoContinente() {
        return this.tipoLocal.isTipoContinente();
    }

    public boolean isTipoEstado() {
        return this.tipoLocal.isTipoEstado();
    }

    public boolean isTipoHotel() {
        return this.tipoLocal.isTipoHotel();
    }

    public boolean isTipoImovelAluguelTemporada() {
        return this.tipoLocal.isTipoImovelAluguelTemporada();
    }

    public boolean isTipoLocalEnderecavel() {
        return this.tipoLocal.isTipoLocalEnderecavel();
    }

    public boolean isTipoLocalGeografico() {
        return this.tipoLocal.isTipoLocalGeografico();
    }

    public boolean isTipoLocalInteresseTuristico() {
        return this.tipoLocal.isTipoLocalInteresseTuristico();
    }

    public boolean isTipoPais() {
        return this.tipoLocal.isTipoPais();
    }

    public boolean isTipoRestaurante() {
        return this.tipoLocal.isTipoRestaurante();
    }

    public boolean possuiFotos() {
        return this.fotos != null && !this.fotos.isEmpty() && this.fotos.get(0).temArquivo();
    }

    public void setAvaliacaoConsolidadaGeral(final AvaliacaoConsolidada avaliacaoConsolidadaGeral) {
        this.avaliacaoConsolidadaGeral = avaliacaoConsolidadaGeral;
    }

    public void setCadastroConcluido(final Boolean cadastroConcluido) {
        this.cadastroConcluido = cadastroConcluido;
    }

    public void setCandidatoExclusao(final Boolean candidatoExclusao) {
        this.candidatoExclusao = candidatoExclusao;
    }

    public void setCoordenadaGeografica(final CoordenadaGeografica coordenadaGeografica) {
        this.coordenadaGeografica = coordenadaGeografica;
    }

    public void setCoordenadaGeograficaFotos(final CoordenadaGeografica coordenadaGeograficaFotos) {
        this.coordenadaGeograficaFotos = coordenadaGeograficaFotos;
    }

    public void setDataCadastro(final LocalDateTime dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setEmail(final String email) {
        if ((email == null) || (email.trim().equals(""))) {
            this.email = null;
        } else {
            final String emailTratado = email.trim().toLowerCase();

            final EmailValidator emailValidator = EmailValidator.getInstance();

            if (!emailValidator.isValid(emailTratado)) {
                throw new IllegalArgumentException("Email informado inválido.  [" + email + "]");
            }

            this.email = emailTratado;
        }
    }

    public void setEndereco(final Endereco endereco) {
        this.endereco = endereco;
    }

    public void setExcluido(final Boolean excluido) {
        this.excluido = excluido;
    }

    public void setFotoPadrao(final Foto fotoPadrao) {
        this.fotoPadrao = fotoPadrao;
        if (this.fotoPadrao != null) {
            if (fotoPadrao.getUrlAlbum() != null) {
                setUrlFotoAlbum(fotoPadrao.getUrlAlbum());
            }
            if (fotoPadrao.getUrlBig() != null) {
                setUrlFotoBig(fotoPadrao.getUrlBig());
            }
            if (fotoPadrao.getUrlSmall() != null) {
                setUrlFotoSmall(fotoPadrao.getUrlSmall());
            }
            setFotoPadraoAnonima(Boolean.TRUE.equals(fotoPadrao.getAnonima()));
        } else {
            setFotoPadraoAnonima(true);
        }
    }

    public void setFotoPadraoAnonima(final Boolean fotoPadraoAnonima) {
        this.fotoPadraoAnonima = fotoPadraoAnonima;
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setIdFotoPanoramio(final String idFotoPanoramio) {
        this.idFotoPanoramio = idFotoPanoramio;
    }

    public void setIdFoursquare(final String idFoursquare) {
        if (this.localFoursquare == null && StringUtils.isNotEmpty(idFoursquare)) {
            this.localFoursquare = new LocalFoursquare();
        }
        if (this.localFoursquare != null) {
            this.localFoursquare.setId(idFoursquare);
        }
    }

    public void setIdGooglePlaces(final String idGooglePlaces) {
        this.idGooglePlaces = idGooglePlaces;
    }

    public void setIdsTags(final List<Long> idsTags) {
        this.idsTags = idsTags;
    }

    public void setIdUsuarioFotoPanoramio(final String idUsuarioFotoPanoramio) {
        this.idUsuarioFotoPanoramio = idUsuarioFotoPanoramio;
    }

    public void setIdUsuarioSolcitante(final Long idUsuarioSolcitante) {
        this.idUsuarioSolcitante = idUsuarioSolcitante;
    }

    public void setInformacoesAdicionais(final String informacoesAdicionais) {
        this.informacoesAdicionais = informacoesAdicionais;
    }

    public void setLocalFoursquare(final LocalFoursquare localFoursquare) {
        this.localFoursquare = localFoursquare;
    }

    public void setLocalGoogle(final LocalGoogle localGoogle) {
        this.localGoogle = localGoogle;
    }

    public void setLocalizacao(final LocalidadeEmbeddable localizacao) {
        this.localizacao = localizacao;
    }

    public void setMostrarOutrasFotosPanoramio(final Boolean mostrarOutrasFotosPanoramio) {
        this.mostrarOutrasFotosPanoramio = mostrarOutrasFotosPanoramio;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNotaBooking(final BigDecimal notaBooking) {
        this.notaBooking = notaBooking;
    }

    public void setNotaRanking(final BigDecimal notaRanking) {
        this.notaRanking = notaRanking;
    }

    public void setNotaTripAdvisor(final BigDecimal notaTripAdvisor) {
        this.notaTripAdvisor = notaTripAdvisor;
    }

    public void setOutrasCategorias(final String outrasCategorias) {
        this.outrasCategorias = outrasCategorias;
    }

    public void setPhotoRefereceGooglePlaces(final String photoRefereceGooglePlaces) {
        this.photoRefereceGooglePlaces = photoRefereceGooglePlaces;
    }

    public void setQuantidadeAvaliacoes(final Long quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

    public void setQuantidadeDicas(final Long quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

    public void setQuantidadeFotos(final Long quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setQuantidadeInteresses(final Long quantidadeInteresses) {
        this.quantidadeInteresses = quantidadeInteresses;
    }

    public void setQuantidadeInteressesDesejaIr(final Long quantidadeInteressesDesejaIr) {
        this.quantidadeInteressesDesejaIr = quantidadeInteressesDesejaIr;
    }

    public void setQuantidadeInteressesImperdivel(final Long quantidadeInteressesImperdivel) {
        this.quantidadeInteressesImperdivel = quantidadeInteressesImperdivel;
    }

    public void setQuantidadeInteressesIndica(final Long quantidadeInteressesIndica) {
        this.quantidadeInteressesIndica = quantidadeInteressesIndica;
    }

    public void setQuantidadeInteressesJaFoi(final Long quantidadeInteressesJaFoi) {
        this.quantidadeInteressesJaFoi = quantidadeInteressesJaFoi;
    }

    public void setQuantidadeInteressesLocalFavorito(final Long quantidadeInteressesLocalFavorito) {
        this.quantidadeInteressesLocalFavorito = quantidadeInteressesLocalFavorito;
    }

    public void setQuantidadeInteressesSeguir(final Long quantidadeInteressesSeguir) {
        this.quantidadeInteressesSeguir = quantidadeInteressesSeguir;
    }

    public void setQuantidadeNoticias(final Long quantidadeNoticias) {
        this.quantidadeNoticias = quantidadeNoticias;
    }

    public void setQuantidadePerguntas(final Long quantidadePerguntas) {
        this.quantidadePerguntas = quantidadePerguntas;
    }

    public void setQuantidadeReviewsTripAdvisor(final Integer quantidadeReviewsTripAdvisor) {
        this.quantidadeReviewsTripAdvisor = quantidadeReviewsTripAdvisor;
    }

    public void setRankingKeKanto(final Integer rankingKeKanto) {
        this.rankingKeKanto = rankingKeKanto;
    }

    public void setReferenceGooglePlaces(final String referenceGooglePlaces) {
        this.referenceGooglePlaces = referenceGooglePlaces;
    }

    public void setSite(final String site) {
        if ((site == null) || (site.trim().equals(""))) {
            this.site = null;
        } else {
            String siteTratado = site.toLowerCase().trim();
            if (!(siteTratado.startsWith("http://") || siteTratado.startsWith("https://"))) {
                siteTratado = "http://" + siteTratado;
            }
            final String[] schemes = { "http", "https" };
            final UrlValidator urlValidator = new UrlValidator(schemes);

            if (!urlValidator.isValid(siteTratado)) {
                throw new IllegalArgumentException("Site informado inválido.  [" + site + "]");
            }

            this.site = siteTratado;
        }
    }

    public void setStatusProcessoValidacao(final Boolean statusProcessoValidacao) {
        this.statusProcessoValidacao = statusProcessoValidacao;
    }

    public void setTelefone(final String telefone) {
        this.telefone = telefone;
    }

    public void setTipoLocal(final LocalType tipoLocal) {
        this.tipoLocal = tipoLocal;
    }

    public void setUrlFotoAlbum(final String urlFotoAlbum) {
        this.urlFotoAlbum = urlFotoAlbum;
    }

    public void setUrlFotoBig(final String urlFotoBig) {
        this.urlFotoBig = urlFotoBig;
    }

    public void setUrlFotoSmall(final String urlFotoSmall) {
        this.urlFotoSmall = urlFotoSmall;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

    public void setUsarFotoGooglePlaces(final Boolean usarFotoGooglePlaces) {
        this.usarFotoGooglePlaces = usarFotoGooglePlaces;
    }

    public void setUsarFotoPanoramio(final Boolean usarFotoPanoramio) {
        this.usarFotoPanoramio = usarFotoPanoramio;
    }

    public void setValidado(final Boolean validado) {
        this.validado = validado;
        if (validado == false) {
            this.statusProcessoValidacao = false;
        }
    }

}