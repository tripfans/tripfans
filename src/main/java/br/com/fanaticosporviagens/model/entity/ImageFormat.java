package br.com.fanaticosporviagens.model.entity;

public enum ImageFormat {

    ALBUM(280, "_album"),
    AVATAR(50, "_avatar"),
    BIG(1024, "_big"),
    CAROUSEL(94, "_carousel"),
    CROP(480, "_crop"),
    ORIGINAL(600, ""),
    SMALL(75, "_small"),
    USER_PROFILE(280, "_profile");

    private final String suffix;

    private final Integer width;

    private ImageFormat(final Integer width, final String suffix) {
        this.width = width;
        this.suffix = suffix;
    }

    public String getSuffix() {
        return this.suffix;
    }

    public int getWidth() {
        return this.width;
    }

}
