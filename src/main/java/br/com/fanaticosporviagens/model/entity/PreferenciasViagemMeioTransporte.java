package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class PreferenciasViagemMeioTransporte implements Serializable {

    private static final long serialVersionUID = 1813639201675832001L;

    @Column(name = "aviao")
    private Boolean aviao = false;

    @Column(name = "carro")
    private Boolean carro = false;

    @Column(name = "navio")
    private Boolean navio = false;

    @Column(name = "onibus")
    private Boolean onibus = false;

    @Column(name = "trem")
    private Boolean trem = false;

    public Boolean getAviao() {
        return this.aviao;
    }

    public Boolean getCarro() {
        return this.carro;
    }

    public Boolean getNavio() {
        return this.navio;
    }

    public Boolean getOnibus() {
        return this.onibus;
    }

    public Boolean getTrem() {
        return this.trem;
    }

    public void setAviao(final Boolean aviao) {
        this.aviao = aviao;
    }

    public void setCarro(final Boolean carro) {
        this.carro = carro;
    }

    public void setNavio(final Boolean navio) {
        this.navio = navio;
    }

    public void setOnibus(final Boolean onibus) {
        this.onibus = onibus;
    }

    public void setTrem(final Boolean trem) {
        this.trem = trem;
    }

}
