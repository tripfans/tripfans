package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

public class ViagemLocalVisitado implements Comparable<ViagemLocalVisitado> {

    private final Local local;

    private final List<ViagemLocalVisitado> sublocais = new ArrayList<ViagemLocalVisitado>();

    public ViagemLocalVisitado(final Local local) {
        super();

        this.local = local;
    }

    public void addSubLocal(final Local local) {
        this.sublocais.add(new ViagemLocalVisitado(local));
    }

    public void addSubLocal(final ViagemLocalVisitado subLocal) {
        this.sublocais.add(subLocal);
    }

    @Override
    public int compareTo(final ViagemLocalVisitado other) {
        final String nome = this.local.getNome() + " - " + this.local.getId();
        final String nomeOther = other.local.getNome() + " - " + other.local.getId();
        return nome.compareTo(nomeOther);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ViagemLocalVisitado other = (ViagemLocalVisitado) obj;
        if (this.local == null) {
            if (other.local != null)
                return false;
        } else if (!this.local.getId().equals(other.local.getId()))
            return false;
        return true;
    }

    public Long getId() {
        return this.local.getId();
    }

    public Long getIdPai() {
        if (this.local.getPai() == null) {
            return null;
        }
        return this.local.getPai().getId();
    }

    public Local getLocal() {
        return this.local;
    }

    public List<ViagemLocalVisitado> getSublocais() {
        return this.sublocais;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.local == null) ? 0 : this.local.getId().hashCode());
        return result;
    }

}
