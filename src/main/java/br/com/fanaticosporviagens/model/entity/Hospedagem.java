package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("4")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class Hospedagem extends AtividadeLongaDuracao {

    private static final long serialVersionUID = -2548396105871531077L;

    @Column(name = "descricao_quartos", nullable = true)
    private String descricaoQuartos;

    @Column(name = "quantidade_quartos", nullable = true)
    private Integer quantidadeQuartos;

    @Column(name = "tipo_quartos", nullable = true)
    private String tipoQuartos;

    @Override
    public String getDescricaoAtividade() {
        return getNomeLocal();
    }

    // TODO I18N
    @Override
    public String getDescricaoFim() {
        return "Partida";
    }

    // TODO I18N
    @Override
    public String getDescricaoInicio() {
        return "Chegada";
    }

    public String getDescricaoQuartos() {
        return this.descricaoQuartos;
    }

    public Integer getQuantidadeQuartos() {
        return this.quantidadeQuartos;
    }

    @Override
    public TipoAtividade getTipo() {
        return TipoAtividade.HOSPEDAGEM;
    }

    public String getTipoQuartos() {
        return this.tipoQuartos;
    }

    public void setDescricaoQuartos(final String descricaoQuartos) {
        this.descricaoQuartos = descricaoQuartos;
    }

    public void setQuantidadeQuartos(final Integer quantidadeQuartos) {
        this.quantidadeQuartos = quantidadeQuartos;
    }

    public void setTipoQuartos(final String tipoQuartos) {
        this.tipoQuartos = tipoQuartos;
    }

}
