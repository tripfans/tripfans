package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.infra.model.entity.GenericEnumUserType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_noticia_sobre_local")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_noticia_sobre_local", allocationSize = 1)
@TypeDefs(value = { @TypeDef(name = "LocalTypeDef", typeClass = GenericEnumUserType.class, parameters = { @Parameter(name = "enumClass", value = "br.com.fanaticosporviagens.model.entity.LocalType") }) })
public abstract class NoticiaSobreLocal extends BaseEntity<Long> {

    private static final long serialVersionUID = -1070369221891795977L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao_publicada", nullable = true)
    private Avaliacao avaliacaoPublicada;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_noticia_cidade", nullable = true)
    private Cidade cidadeLocalPublicado;

    @Column(name = "data_publicacao", nullable = false)
    private Date dataPublicacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica_publicada", nullable = true)
    private Dica dicaPublicada;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_noticia_estado", nullable = true)
    private Estado estadoLocalPublicado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_noticia", nullable = false)
    private Local localPublicado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local_noticia_pais", nullable = true)
    private Pais paisLocalPublicado;

    @Type(type = "LocalTypeDef")
    @Column(name = "local_type", nullable = false)
    private LocalType tipoLocal;

    public NoticiaSobreLocal() {
        super();
    }

    public NoticiaSobreLocal(final Avaliacao avaliacaoPublicada) {
        super();
        this.dataPublicacao = new Date();
        this.avaliacaoPublicada = avaliacaoPublicada;
        popularDadosLocalPublicado(avaliacaoPublicada.getLocalAvaliado());
    }

    public NoticiaSobreLocal(final Dica dicaPublicada) {
        super();
        this.dataPublicacao = new Date();
        this.dicaPublicada = dicaPublicada;
        popularDadosLocalPublicado(dicaPublicada.getLocalDica());
    }

    public Avaliacao getAvaliacaoPublicada() {
        return this.avaliacaoPublicada;
    }

    public Cidade getCidadeLocalPublicado() {
        return this.cidadeLocalPublicado;
    }

    public Date getDataPublicacao() {
        return this.dataPublicacao;
    }

    public Dica getDicaPublicada() {
        return this.dicaPublicada;
    }

    public Estado getEstadoLocalPublicado() {
        return this.estadoLocalPublicado;
    }

    public Local getLocalPublicado() {
        return this.localPublicado;
    }

    public Pais getPaisLocalPublicado() {
        return this.paisLocalPublicado;
    }

    public LocalType getTipoLocal() {
        return this.tipoLocal;
    }

    public boolean isSobreAvaliacao() {
        return (this.avaliacaoPublicada != null);
    }

    public boolean isSobreDica() {
        return (this.dicaPublicada != null);
    }

    private void popularDadosLocalPublicado(final Local local) {
        this.localPublicado = local;
        this.tipoLocal = local.getTipoLocal();
        if (local.isTipoLocalEnderecavel()) {
            final LocalEnderecavel localEnderecavel = (LocalEnderecavel) local;
            this.cidadeLocalPublicado = localEnderecavel.getCidade();
            this.estadoLocalPublicado = localEnderecavel.getEstado();
            this.paisLocalPublicado = localEnderecavel.getPais();
        } else if (local.isTipoCidade()) {
            final Cidade cidade = (Cidade) local;
            this.cidadeLocalPublicado = cidade;
            this.estadoLocalPublicado = cidade.getEstado();
            this.paisLocalPublicado = cidade.getPais();
        } else if (local.isTipoEstado()) {
            final Estado estado = (Estado) local;
            this.estadoLocalPublicado = estado;
            this.paisLocalPublicado = estado.getPais();
        } else if (local.isTipoPais()) {
            this.paisLocalPublicado = (Pais) local;
        }

    }

    public void setAvaliacaoPublicada(final Avaliacao avaliacaoPublicada) {
        this.avaliacaoPublicada = avaliacaoPublicada;
    }

    public void setCidadeLocalPublicado(final Cidade cidadeLocalPublicado) {
        this.cidadeLocalPublicado = cidadeLocalPublicado;
    }

    public void setDataPublicacao(final Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public void setDicaPublicada(final Dica dicaPublicada) {
        this.dicaPublicada = dicaPublicada;
    }

    public void setEstadoLocalPublicado(final Estado estadoLocalPublicado) {
        this.estadoLocalPublicado = estadoLocalPublicado;
    }

    public void setLocalPublicado(final Local localPublicado) {
        this.localPublicado = localPublicado;
    }

    public void setPaisLocalPublicado(final Pais paisLocalPublicado) {
        this.paisLocalPublicado = paisLocalPublicado;
    }

    public void setTipoLocal(final LocalType tipoLocal) {
        this.tipoLocal = tipoLocal;
    }

}
