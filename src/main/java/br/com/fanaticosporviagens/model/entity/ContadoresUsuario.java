package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class ContadoresUsuario implements Serializable {

    private static final long serialVersionUID = 7476722944027073530L;

    @Column(name = "qtd_amigo", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAmigos;

    @Column(name = "qtd_atividade", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAtividades;

    @Column(name = "qtd_atracoes_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAtracoesDesejaIr;

    @Column(name = "qtd_atracoes_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAtracoesFavorito;

    @Column(name = "qtd_atracoes_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAtracoesIndica;

    @Column(name = "qtd_atracoes_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAtracoesJaFoi;

    @Column(name = "qtd_avaliacao", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeAvaliacoes;

    @Column(name = "qtd_cidades_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeCidadesDesejaIr;

    @Column(name = "qtd_cidades_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeCidadesFavorito;

    @Column(name = "qtd_cidades_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeCidadesIndica;

    @Column(name = "qtd_cidades_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeCidadesJaFoi;

    @Column(name = "qtd_comentario", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeComentarios;

    @Column(name = "qtd_dica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeDicas;

    @Column(name = "qtd_foto", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeFotos;

    @Column(name = "qtd_hoteis_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeHoteisDesejaIr;

    @Column(name = "qtd_hoteis_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeHoteisFavorito;

    @Column(name = "qtd_hoteis_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeHoteisIndica;

    @Column(name = "qtd_hoteis_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeHoteisJaFoi;

    @Column(name = "qtd_paises_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePaisesDesejaIr;

    @Column(name = "qtd_paises_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePaisesFavorito;

    @Column(name = "qtd_paises_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePaisesIndica;

    @Column(name = "qtd_paises_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePaisesJaFoi;

    @Column(name = "qtd_pergunta", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadePerguntas;

    @Column(name = "qtd_resposta", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeRespostas;

    @Column(name = "qtd_restaurantes_deseja_ir", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeRestaurantesDesejaIr;

    @Column(name = "qtd_restaurantes_favorito", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeRestaurantesFavorito;

    @Column(name = "qtd_restaurantes_indica", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeRestaurantesIndica;

    @Column(name = "qtd_restaurantes_ja_foi", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeRestaurantesJaFoi;

    @Column(name = "qtd_viagem", nullable = false, columnDefinition = "BIGINT DEFAULT 0", insertable = false, updatable = false)
    private Long quantidadeViagens;

    public boolean getPossuiInteressesEmCidades() {
        return this.quantidadeCidadesDesejaIr > 0 || this.quantidadeCidadesFavorito > 0 || this.quantidadeCidadesIndica > 0
                || this.quantidadeCidadesJaFoi > 0;
    }

    public Long getQuantidadeAmigos() {
        return this.quantidadeAmigos;
    }

    public Long getQuantidadeAtividades() {
        return this.quantidadeAtividades;
    }

    public Long getQuantidadeAtracoesDesejaIr() {
        return this.quantidadeAtracoesDesejaIr;
    }

    public Long getQuantidadeAtracoesFavorito() {
        return this.quantidadeAtracoesFavorito;
    }

    public Long getQuantidadeAtracoesIndica() {
        return this.quantidadeAtracoesIndica;
    }

    public Long getQuantidadeAtracoesJaFoi() {
        return this.quantidadeAtracoesJaFoi;
    }

    public Long getQuantidadeAvaliacoes() {
        return this.quantidadeAvaliacoes;
    }

    public Long getQuantidadeCidadesDesejaIr() {
        return this.quantidadeCidadesDesejaIr;
    }

    public Long getQuantidadeCidadesFavorito() {
        return this.quantidadeCidadesFavorito;
    }

    public Long getQuantidadeCidadesIndica() {
        return this.quantidadeCidadesIndica;
    }

    public Long getQuantidadeCidadesJaFoi() {
        return this.quantidadeCidadesJaFoi;
    }

    public Long getQuantidadeComentarios() {
        return this.quantidadeComentarios;
    }

    public Long getQuantidadeDicas() {
        return this.quantidadeDicas;
    }

    public Long getQuantidadeFotos() {
        return this.quantidadeFotos;
    }

    public Long getQuantidadeHoteisDesejaIr() {
        return this.quantidadeHoteisDesejaIr;
    }

    public Long getQuantidadeHoteisFavorito() {
        return this.quantidadeHoteisFavorito;
    }

    public Long getQuantidadeHoteisIndica() {
        return this.quantidadeHoteisIndica;
    }

    public Long getQuantidadeHoteisJaFoi() {
        return this.quantidadeHoteisJaFoi;
    }

    public Long getQuantidadePaisesDesejaIr() {
        return this.quantidadePaisesDesejaIr;
    }

    public Long getQuantidadePaisesFavorito() {
        return this.quantidadePaisesFavorito;
    }

    public Long getQuantidadePaisesIndica() {
        return this.quantidadePaisesIndica;
    }

    public Long getQuantidadePaisesJaFoi() {
        return this.quantidadePaisesJaFoi;
    }

    public Long getQuantidadePerguntas() {
        return this.quantidadePerguntas;
    }

    public Long getQuantidadeRespostas() {
        return this.quantidadeRespostas;
    }

    public Long getQuantidadeRestaurantesDesejaIr() {
        return this.quantidadeRestaurantesDesejaIr;
    }

    public Long getQuantidadeRestaurantesFavorito() {
        return this.quantidadeRestaurantesFavorito;
    }

    public Long getQuantidadeRestaurantesIndica() {
        return this.quantidadeRestaurantesIndica;
    }

    public Long getQuantidadeRestaurantesJaFoi() {
        return this.quantidadeRestaurantesJaFoi;
    }

    public Long getQuantidadeViagens() {
        return this.quantidadeViagens;
    }

    public void setQuantidadeAmigos(final Long quantidadeAmigos) {
        this.quantidadeAmigos = quantidadeAmigos;
    }

    public void setQuantidadeAtividades(final Long quantidadeAtividades) {
        this.quantidadeAtividades = quantidadeAtividades;
    }

    public void setQuantidadeAtracoesDesejaIr(final Long quantidadeAtracoesDesejaIr) {
        this.quantidadeAtracoesDesejaIr = quantidadeAtracoesDesejaIr;
    }

    public void setQuantidadeAtracoesFavorito(final Long quantidadeAtracoesFavorito) {
        this.quantidadeAtracoesFavorito = quantidadeAtracoesFavorito;
    }

    public void setQuantidadeAtracoesIndica(final Long quantidadeAtracoesIndica) {
        this.quantidadeAtracoesIndica = quantidadeAtracoesIndica;
    }

    public void setQuantidadeAtracoesJaFoi(final Long quantidadeAtracoesJaFoi) {
        this.quantidadeAtracoesJaFoi = quantidadeAtracoesJaFoi;
    }

    public void setQuantidadeAvaliacoes(final Long quantidadeAvaliacoes) {
        this.quantidadeAvaliacoes = quantidadeAvaliacoes;
    }

    public void setQuantidadeCidadesDesejaIr(final Long quantidadeCidadesDesejaIr) {
        this.quantidadeCidadesDesejaIr = quantidadeCidadesDesejaIr;
    }

    public void setQuantidadeCidadesFavorito(final Long quantidadeCidadesFavorito) {
        this.quantidadeCidadesFavorito = quantidadeCidadesFavorito;
    }

    public void setQuantidadeCidadesIndica(final Long quantidadeCidadesIndica) {
        this.quantidadeCidadesIndica = quantidadeCidadesIndica;
    }

    public void setQuantidadeCidadesJaFoi(final Long quantidadeCidadesJaFoi) {
        this.quantidadeCidadesJaFoi = quantidadeCidadesJaFoi;
    }

    public void setQuantidadeComentarios(final Long quantidadeComentarios) {
        this.quantidadeComentarios = quantidadeComentarios;
    }

    public void setQuantidadeDicas(final Long quantidadeDicas) {
        this.quantidadeDicas = quantidadeDicas;
    }

    public void setQuantidadeFotos(final Long quantidadeFotos) {
        this.quantidadeFotos = quantidadeFotos;
    }

    public void setQuantidadeHoteisDesejaIr(final Long quantidadeHoteisDesejaIr) {
        this.quantidadeHoteisDesejaIr = quantidadeHoteisDesejaIr;
    }

    public void setQuantidadeHoteisFavorito(final Long quantidadeHoteisFavorito) {
        this.quantidadeHoteisFavorito = quantidadeHoteisFavorito;
    }

    public void setQuantidadeHoteisIndica(final Long quantidadeHoteisIndica) {
        this.quantidadeHoteisIndica = quantidadeHoteisIndica;
    }

    public void setQuantidadeHoteisJaFoi(final Long quantidadeHoteisJaFoi) {
        this.quantidadeHoteisJaFoi = quantidadeHoteisJaFoi;
    }

    public void setQuantidadePaisesDesejaIr(final Long quantidadePaisesDesejaIr) {
        this.quantidadePaisesDesejaIr = quantidadePaisesDesejaIr;
    }

    public void setQuantidadePaisesFavorito(final Long quantidadePaisesFavorito) {
        this.quantidadePaisesFavorito = quantidadePaisesFavorito;
    }

    public void setQuantidadePaisesIndica(final Long quantidadePaisesIndica) {
        this.quantidadePaisesIndica = quantidadePaisesIndica;
    }

    public void setQuantidadePaisesJaFoi(final Long quantidadePaisesJaFoi) {
        this.quantidadePaisesJaFoi = quantidadePaisesJaFoi;
    }

    public void setQuantidadePerguntas(final Long quantidadePerguntas) {
        this.quantidadePerguntas = quantidadePerguntas;
    }

    public void setQuantidadeRespostas(final Long quantidadeRespostas) {
        this.quantidadeRespostas = quantidadeRespostas;
    }

    public void setQuantidadeRestaurantesDesejaIr(final Long quantidadeRestaurantesDesejaIr) {
        this.quantidadeRestaurantesDesejaIr = quantidadeRestaurantesDesejaIr;
    }

    public void setQuantidadeRestaurantesFavorito(final Long quantidadeRestaurantesFavorito) {
        this.quantidadeRestaurantesFavorito = quantidadeRestaurantesFavorito;
    }

    public void setQuantidadeRestaurantesIndica(final Long quantidadeRestaurantesIndica) {
        this.quantidadeRestaurantesIndica = quantidadeRestaurantesIndica;
    }

    public void setQuantidadeRestaurantesJaFoi(final Long quantidadeRestaurantesJaFoi) {
        this.quantidadeRestaurantesJaFoi = quantidadeRestaurantesJaFoi;
    }

    public void setQuantidadeViagens(final Long quantidadeViagens) {
        this.quantidadeViagens = quantidadeViagens;
    }

}