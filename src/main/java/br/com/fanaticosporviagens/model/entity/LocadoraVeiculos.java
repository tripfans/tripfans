package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@DiscriminatorValue("11")
public class LocadoraVeiculos extends Empresa {

    private static final long serialVersionUID = -5063796966904810819L;

}
