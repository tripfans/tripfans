package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PontuacaoUsuarioLocal implements Serializable {

    private static final long serialVersionUID = 2087939361717181828L;

    private BigDecimal pontos;

    private Usuario usuario;

    public PontuacaoUsuarioLocal() {
        super();
    }

    public PontuacaoUsuarioLocal(final BigDecimal pontos, final Usuario usuario) {
        super();
        this.pontos = pontos;
        this.usuario = usuario;
    }

    public BigDecimal getPontos() {
        return this.pontos;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setPontos(final BigDecimal pontos) {
        this.pontos = pontos;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
