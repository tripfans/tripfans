package br.com.fanaticosporviagens.model.entity;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "tipo_empresa", discriminatorType = DiscriminatorType.INTEGER)
public abstract class Empresa extends LocalEnderecavel {

    private static final long serialVersionUID = -2490805194466031227L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_grupo_empresarial", nullable = true)
    private GrupoEmpresarial grupoEmpresarial;

    public GrupoEmpresarial getGrupoEmpresarial() {
        return this.grupoEmpresarial;
    }

    public void setGrupoEmpresarial(final GrupoEmpresarial grupoEmpresarial) {
        this.grupoEmpresarial = grupoEmpresarial;
    }

}
