package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 * @author André Thiago
 * 
 */
public enum CategoriasPesquisa implements EnumTypeInteger {

    AEROPORTOS(LocalType.AEROPORTO.getCodigo(), "marker.png", "Aeroportos"),
    ATRACOES(LocalType.ATRACAO.getCodigo(), "house.png", "Atrações"),
    CIDADES(LocalType.CIDADE.getCodigo(), "marker.png", "Destinos"),
    ESTADOS(LocalType.ESTADO.getCodigo(), "marker.png", "Estados"),
    HOTEIS(LocalType.HOTEL.getCodigo(), "luggage.png", "Hotéis"),
    PAISES(LocalType.PAIS.getCodigo(), "marker.png", "Países"),
    PESSOAS(11, "group.png", "Pessoas"),
    RESTAURANTES(LocalType.RESTAURANTE.getCodigo(), "drink.png", "Restaurantes");

    public static String getDescricaoPorCodigo(final int codigo) {
        for (final CategoriasPesquisa categoria : values()) {
            if (categoria.getCodigo().equals(codigo)) {
                return categoria.getDescricao();
            }
        }
        throw new IllegalArgumentException("Código de viagem inválido!");
    }

    private String icone;

    private final EnumI18nUtil util;

    CategoriasPesquisa(final int codigo, final String icone, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        this.icone = icone;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public String getIcone() {
        return this.icone;
    }

}
