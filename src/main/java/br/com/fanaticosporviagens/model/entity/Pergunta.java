package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.DynamicUpdate;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;
import br.com.fanaticosporviagens.infra.model.entity.UrlNameable;

/**
 * @author André Thiago
 * 
 */
@Entity
@Table(name = "pergunta")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_pergunta", allocationSize = 1)
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_pergunta", nullable = false, unique = true)) })
@DynamicUpdate
public class Pergunta extends EntidadeGeradoraAcao<Long> implements UrlNameable, AlvoAcao, Comentavel {

    private static final long serialVersionUID = -4026529390199850486L;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_autor", nullable = false)
    private Usuario autor;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_local", nullable = true)
    @NotNull(message = "Você deve informar sobre qual destino é a sua pergunta")
    private Local local;

    @Column(name = "qtd_respostas", nullable = true)
    private Long quantidadeRespostas;

    @Column(name = "texto_pergunta", length = 200, nullable = false)
    @NotNull(message = "Escreva a sua pergunta")
    @Size(min = 10, message = "Sua pergunta deve ter pelo menos 10 caracteres")
    private String textoPergunta;

    @Column(name = "url_path", nullable = false, unique = true, updatable = false)
    private String urlPath;

    public void adicionarResposta() {
        if (this.quantidadeRespostas == null) {
            this.quantidadeRespostas = Long.valueOf(0);
        }
        this.quantidadeRespostas += 1;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) this.local;
    }

    @Override
    public Usuario getAutor() {
        return this.autor;
    }

    public Date getData() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    @Override
    public String getDescricaoAlvo() {
        return getAlvo().getDescricaoAlvo();
    }

    public Local getLocal() {
        return this.local;
    }

    @Override
    public String getPrefix() {
        return "p";
    }

    public Long getQuantidadeRespostas() {
        return this.quantidadeRespostas;
    }

    public String getTextoPergunta() {
        return this.textoPergunta;
    }

    @Override
    public String getUrlPath() {
        return this.urlPath;
    }

    public void setAutor(final Usuario autor) {
        this.autor = autor;
    }

    public void setData(final Date data) {
        if (data != null) {
            this.setDataCriacao(new LocalDateTime(data));
        }
    }

    public void setLocal(final Local local) {
        this.local = local;
    }

    public void setQuantidadeRespostas(final Long quantidadeRespostas) {
        this.quantidadeRespostas = quantidadeRespostas;
    }

    public void setTextoPergunta(final String textoPergunta) {
        this.textoPergunta = textoPergunta;
    }

    @Override
    public void setUrlPath(final String urlPath) {
        this.urlPath = urlPath;
    }

}