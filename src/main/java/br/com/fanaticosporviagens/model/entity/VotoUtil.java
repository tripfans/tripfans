package br.com.fanaticosporviagens.model.entity;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Any;
import org.hibernate.annotations.AnyMetaDef;
import org.hibernate.annotations.MetaValue;
import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.acaousuario.AlvoAcao;

@Entity
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_voto_util")) })
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_voto_util", allocationSize = 1)
@Table(name = "voto_util")
public class VotoUtil extends EntidadeGeradoraAcao<Long> {

    private static final long serialVersionUID = 5171258823441341685L;

    @Any(metaColumn = @Column(name = "tipo_objeto_votado"), fetch = FetchType.EAGER)
    @AnyMetaDef(idType = "long", metaType = "string", metaValues = { @MetaValue(value = "Avaliacao", targetEntity = Avaliacao.class),
            @MetaValue(value = "Dica", targetEntity = Dica.class), @MetaValue(value = "RelatoViagem", targetEntity = RelatoViagem.class),
            @MetaValue(value = "Resposta", targetEntity = Resposta.class), })
    @JoinColumn(name = "id_objeto_votado")
    public Votavel objetoVotado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_avaliacao", nullable = true)
    private Avaliacao avaliacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_avaliador", nullable = false)
    private Usuario avaliador;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dica", nullable = true)
    private Dica dica;

    @Column(name = "id_avaliacao", insertable = false, updatable = false)
    private Long idAvaliacao;

    @Column(name = "id_dica", insertable = false, updatable = false)
    private Long idDica;

    @Column(name = "id_objeto_votado", insertable = false, updatable = false)
    private Long idObjetoVotado;

    @Column(name = "id_relato_viagem", insertable = false, updatable = false)
    private Long idRelatoViagem;

    @Column(name = "id_resposta", insertable = false, updatable = false)
    private Long idResposta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_relato_viagem", nullable = true)
    private RelatoViagem relatoViagem;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_resposta", nullable = true)
    private Resposta resposta;

    @Column(name = "tipo_objeto_votado", insertable = false, updatable = false)
    private String tipoObjetoVotado;

    public VotoUtil() {
        super();
    }

    public VotoUtil(final Usuario avaliador, final Date dataVoto, final Votavel votavel) {
        super();
        this.avaliador = avaliador;
        setDataVoto(dataVoto);
        this.objetoVotado = votavel;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends AlvoAcao> T getAlvo() {
        return (T) getObjetoAlvo();
    }

    @Override
    public Usuario getAutor() {
        return getAvaliador();
    }

    public Avaliacao getAvaliacao() {
        return this.avaliacao;
    }

    public Usuario getAvaliador() {
        return this.avaliador;
    }

    public Date getDataVoto() {
        return this.getDataCriacao() != null ? this.getDataCriacao().toDate() : null;
    }

    public Dica getDica() {
        return this.dica;
    }

    public Long getIdObjetoVotado() {
        return this.idObjetoVotado;
    }

    public Votavel getObjetoVotado() {
        return this.objetoVotado;
    }

    public RelatoViagem getRelatoViagem() {
        return this.relatoViagem;
    }

    public Resposta getResposta() {
        if (getObjetoVotado() instanceof Resposta) {
            return (Resposta) this.objetoVotado;
        }
        return this.resposta;
    }

    public boolean isVoto(final Class<? extends Votavel> classe) {
        return classe.getSimpleName().equals(this.tipoObjetoVotado);
    }

    public boolean isVotoAvaliacao() {
        return this.idAvaliacao != null || isVoto(Avaliacao.class);
    }

    public boolean isVotoDica() {
        return this.idDica != null || isVoto(Dica.class);
    }

    public boolean isVotoRelatoViagem() {
        return this.idRelatoViagem != null || isVoto(RelatoViagem.class);
    }

    public boolean isVotoResposta() {
        return this.idResposta != null || isVoto(Resposta.class);
    }

    public void setAvaliacao(final Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public void setAvaliador(final Usuario avaliador) {
        this.avaliador = avaliador;
    }

    public void setDataVoto(final Date dataVoto) {
        if (dataVoto != null) {
            this.setDataCriacao(new LocalDateTime(dataVoto));
        }
    }

    public void setDica(final Dica dica) {
        this.dica = dica;
    }

    public void setIdObjetoVotado(final Long idObjetoVotado) {
        this.idObjetoVotado = idObjetoVotado;
    }

    public void setRelatoViagem(final RelatoViagem relatoViagem) {
        this.relatoViagem = relatoViagem;
    }

    public void setResposta(final Resposta resposta) {
        this.resposta = resposta;
    }

    @SuppressWarnings("unchecked")
    @Transient
    protected <T extends AlvoAcao> T getObjetoAlvo() {
        if (this.objetoVotado != null) {
            return (T) this.objetoVotado;
        } else if (this.avaliacao != null) {
            return (T) this.avaliacao;
        } else if (this.dica != null) {
            return (T) this.dica;
        } else if (this.resposta != null) {
            return (T) this.resposta;
        }
        return null;
    }

}
