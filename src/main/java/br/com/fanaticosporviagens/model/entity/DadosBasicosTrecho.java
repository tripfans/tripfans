package br.com.fanaticosporviagens.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 * @author Carlos Nascimento
 */
@Embeddable
public class DadosBasicosTrecho implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate data;

    @Column
    private String descricao;

    @Column
    private Integer dia;

    @Column
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentLocalTimeAsTimestamp")
    private LocalTime hora;

    /*
     * Para o caso de aluguel de carro o local de partida é o onde pega o carro e o local de chegada é onde deixa o carro.
     */
    @Column
    private LocalEnderecavel local;

    @Embedded
    private LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();

    @Column
    private String observacao;

    @Column
    private String portao;

    @Column
    private String terminal;

    public LocalDate getData() {
        return this.data;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Integer getDia() {
        return this.dia;
    }

    public LocalTime getHora() {
        return this.hora;
    }

    public LocalEnderecavel getLocal() {
        return this.local;
    }

    public LocalEnderecavelNaoCadastradoAtividade getLocalNaoCadastrado() {
        if (this.localNaoCadastrado == null) {
            this.localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();
        }
        return this.localNaoCadastrado;
    }

    public String getNomeLocal() {
        if (getLocal() != null) {
            /*if (getLocal().isTipoAeroporto()) {
                return getLocal().getCidade().getNome();
            }*/
            return this.local.getNome();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getNome();
        }
        return null;
    }

    public String getNomeLocalEspecifico() {
        if (getLocal() != null) {
            return this.local.getNome();
        } else if (getLocalNaoCadastrado() != null) {
            return this.localNaoCadastrado.getNome();
        }
        return null;
    }

    public String getObservacao() {
        return this.observacao;
    }

    public String getPortao() {
        return this.portao;
    }

    public String getTerminal() {
        return this.terminal;
    }

    public void setData(final LocalDate data) {
        this.data = data;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDia(final Integer dia) {
        this.dia = dia;
    }

    public void setHora(final LocalTime hora) {
        this.hora = hora;
    }

    public void setLocal(final LocalEnderecavel local) {
        this.local = local;
    }

    public void setLocalNaoCadastrado(final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado) {
        this.localNaoCadastrado = localNaoCadastrado;
    }

    public void setNomeLocal(final String nome) {
        getLocalNaoCadastrado().setNome(nome);
    }

    public void setObservacao(final String observacao) {
        this.observacao = observacao;
    }

    public void setPortao(final String portao) {
        this.portao = portao;
    }

    public void setTerminal(final String terminal) {
        this.terminal = terminal;
    }

}
