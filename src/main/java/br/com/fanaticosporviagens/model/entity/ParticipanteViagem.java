package br.com.fanaticosporviagens.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;

/**
 *
 * @author Carlos Nascimento
 *
 */
@Entity
@Table(name = "participante_viagem")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "seq_participante_viagem", allocationSize = 1)
public class ParticipanteViagem extends BaseEntity<Long> {

    private static final long serialVersionUID = 2105989981906800012L;

    @Column(name = "confirmado", nullable = true)
    private Boolean confirmado;

    @Column(name = "email", nullable = true)
    private String email;

    @Column(name = "faixa_etaria", nullable = false)
    private Integer faixaEtaria;

    @Column(name = "nome", nullable = true)
    private String nome;

    @Column(name = "pode_adicionar_atividade", nullable = false)
    private Boolean podeAdicionarAtividade = false;

    @Column(name = "pode_editar_viagem", nullable = false)
    private Boolean podeEditarViagem = false;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario", nullable = true)
    private Usuario usuario;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_viagem", nullable = false)
    private Viagem viagem;

    @Column(name = "viaja", nullable = false)
    private Boolean viaja;

    public Boolean getConfirmado() {
        return this.confirmado;
    }

    public String getEmail() {
        if (getUsuario() != null) {
            return this.usuario.getEmail();
        }
        return this.email;
    }

    public Integer getFaixaEtaria() {
        return this.faixaEtaria;
    }

    public String getNome() {
        if (getUsuario() != null) {
            return this.usuario.getPrimeiroNome() + " " + this.usuario.getUltimoNome();
        }
        return this.nome;
    }

    public Boolean getPodeAdicionarAtividade() {
        return this.podeAdicionarAtividade;
    }

    public Boolean getPodeEditarViagem() {
        return this.podeEditarViagem;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public Viagem getViagem() {
        return this.viagem;
    }

    public Boolean getViaja() {
        return this.viaja;
    }

    public void setConfirmado(final Boolean confirmado) {
        this.confirmado = confirmado;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setFaixaEtaria(final Integer faixaEtaria) {
        if (faixaEtaria == null) {
            this.faixaEtaria = 1;
        } else {
            this.faixaEtaria = faixaEtaria;
        }
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    @Transient
    public void setPermissao(final Integer permissao) {
        if (permissao != null) {
            if (permissao == 1) {
                this.podeAdicionarAtividade = true;
                this.podeEditarViagem = true;
                return;
            } else if (permissao == 2) {
                this.podeAdicionarAtividade = true;
                this.podeEditarViagem = false;
                return;
            }
        }
        this.podeAdicionarAtividade = false;
        this.podeEditarViagem = false;
    }

    public void setPodeAdicionarAtividade(final Boolean podeAdicionarAtividade) {
        this.podeAdicionarAtividade = podeAdicionarAtividade;
    }

    public void setPodeEditarViagem(final Boolean podeEditarViagem) {
        this.podeEditarViagem = podeEditarViagem;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

    public void setViagem(final Viagem viagem) {
        this.viagem = viagem;
    }

    public void setViaja(final Boolean viaja) {
        this.viaja = viaja;
    }

}
