package br.com.fanaticosporviagens.model.entity;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

public enum AvaliacaoItemBomPara implements EnumTypeInteger {

    AO_AR_LIVRE(1, "Ao ar livre", new LocalType[] { LocalType.RESTAURANTE, LocalType.ATRACAO, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO }),
    BAIXO_ORCAMENTO(2, "Baixo orçamento", new LocalType[] { LocalType.RESTAURANTE, LocalType.ATRACAO, LocalType.HOTEL, LocalType.CIDADE,
            LocalType.LOCAL_INTERESSE_TURISTICO }),
    BOA_VISTA(3, "Boa vista", new LocalType[] { LocalType.RESTAURANTE, LocalType.ATRACAO, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO }),
    CASAIS(4, "Casais", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE, LocalType.ATRACAO }),
    COM_AMIGOS(5, "Com amigos", new LocalType[] { LocalType.RESTAURANTE, LocalType.ATRACAO }),
    COMPRAS(15, "Compras", new LocalType[] { LocalType.ATRACAO, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO }),
    COZINHA_LOCAL(6, "Cozinha local", new LocalType[] { LocalType.RESTAURANTE, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO }),
    DESCANSO_RELAXAMENTO(14, "Descanso/Relaxamento", new LocalType[] { LocalType.ATRACAO, LocalType.HOTEL, LocalType.CIDADE,
            LocalType.LOCAL_INTERESSE_TURISTICO }),
    ENTRETENIMENTO(7, "Entretenimento", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE, LocalType.ATRACAO, LocalType.CIDADE,
            LocalType.LOCAL_INTERESSE_TURISTICO }),
    FAMILIA_COM_FILHOS(8, "Família com filhos", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE }),
    GRUPOS(9, "Grupos", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE, LocalType.ATRACAO }),
    NEGOCIOS(10, "Negócios", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE, LocalType.CIDADE, LocalType.LOCAL_INTERESSE_TURISTICO }),
    OCASIOES_ESPECIAIS(11, "Ocasiões especiais", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE }),
    ROMANCE(12, "Romance", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE, LocalType.ATRACAO, LocalType.CIDADE,
            LocalType.LOCAL_INTERESSE_TURISTICO }),
    SOZINHOS_E_SOZINHAS(13, "Sozinhos e Sozinhas", new LocalType[] { LocalType.HOTEL, LocalType.RESTAURANTE });

    public static List<AvaliacaoItemBomPara> getItens(final LocalType tipoLocal) {
        final List<AvaliacaoItemBomPara> lista = new ArrayList<AvaliacaoItemBomPara>();
        for (final AvaliacaoItemBomPara item : AvaliacaoItemBomPara.values()) {
            if (item.possuiTipoLocal(tipoLocal)) {
                lista.add(item);
            }
        }
        return lista;
    }

    private final Integer codigo;

    private final String descricao;

    private final LocalType[] tiposLocal;

    private AvaliacaoItemBomPara(final Integer codigo, final String descricao, final LocalType[] tiposLocal) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tiposLocal = tiposLocal;
    }

    @Override
    public Integer getCodigo() {
        return this.codigo;
    }

    @Override
    public String getDescricao() {
        return this.descricao;
    }

    public LocalType[] getTiposLocal() {
        return this.tiposLocal;
    }

    public Boolean possuiTipoLocal(final LocalType tipoLocalVerificado) {
        for (final LocalType tipo : this.tiposLocal) {
            if (tipo.equals(tipoLocalVerificado)) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

}
