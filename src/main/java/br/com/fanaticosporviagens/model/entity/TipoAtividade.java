package br.com.fanaticosporviagens.model.entity;

import br.com.fanaticosporviagens.infra.model.entity.EnumI18nUtil;
import br.com.fanaticosporviagens.infra.model.entity.EnumTypeInteger;

/**
 *
 * @author Pedro Sebba
 * @author Carlos Nascimento
 *
 */
public enum TipoAtividade implements EnumTypeInteger {

    ALIMENTACAO(3, "restaurante", "Onde comer"),
    ATRACAO_ALIMENTACAO(6, "atracao-alimentacao", "O que fazer / Onde comer"),
    HOSPEDAGEM(2, "hotel", "Hospedagem"),
    PERSONALIZADO(5, "personalizado", "Personalizado"),
    TRANSPORTE(1, "transporte", "Transporte"),
    VISITA_ATRACAO(4, "atracao", "O que fazer");

    public static TipoAtividade[] listaOrdenada() {
        return new TipoAtividade[] { VISITA_ATRACAO, ALIMENTACAO, HOSPEDAGEM, TRANSPORTE, PERSONALIZADO };
    }

    public static TipoAtividade[] listaReduzidaOrdenada() {
        return new TipoAtividade[] { VISITA_ATRACAO, ALIMENTACAO, PERSONALIZADO };
    }

    private String icone;

    private final EnumI18nUtil util;

    TipoAtividade(final int codigo, final String icone, final String... descricoes) {
        this.util = new EnumI18nUtil(codigo, new String[] { "pt_BR" }, descricoes);
        this.icone = icone;
    }

    @Override
    public Integer getCodigo() {
        return this.util.getCodigo();
    }

    @Override
    public String getDescricao() {
        return this.util.getDescricao();
    }

    public TipoAtividade getEnumPorCodigo(final Integer codigo) {
        for (final TipoAtividade tipoAtividade : values()) {
            if (tipoAtividade.getCodigo().equals(codigo)) {
                return tipoAtividade;
            }
        }
        throw new IllegalArgumentException("Código do Tipo de AtividadePlano Inválido!");
    }

    public String getIcone() {
        return this.icone;
    }

    public boolean isAtividadeAlimentacao() {
        return TipoAtividade.ALIMENTACAO.equals(this);
    }

    public boolean isAtividadeAtracaoAlimentaca() {
        return TipoAtividade.ATRACAO_ALIMENTACAO.equals(this);
    }

    public boolean isAtividadeHospedagem() {
        return TipoAtividade.HOSPEDAGEM.equals(this);
    }

    public boolean isAtividadePersonalizada() {
        return TipoAtividade.PERSONALIZADO.equals(this);
    }

    public boolean isAtividadeTransporte() {
        return TipoAtividade.TRANSPORTE.equals(this);
    }

    public boolean isAtividadeVisitaAtracao() {
        return TipoAtividade.VISITA_ATRACAO.equals(this);
    }
}