package br.com.fanaticosporviagens.notificacao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.acaousuario.Acao;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.EmailTemplate;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.ConfiguracaoNotificacoesPorEmail;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.Notificacao;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.Resposta;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.TipoNotificacaoPorEmail;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.VotoUtil;
import br.com.fanaticosporviagens.usuario.ConfiguracaoNotificacoesPorEmailService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Service
public class NotificacaoService extends GenericCRUDService<Notificacao, Long> {

    @Autowired
    private ConfiguracaoNotificacoesPorEmailService configuracaoNotificacoesPorEmailService;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private UsuarioService usuarioService;

    public List<Notificacao> consultarNotificacoesRecentes(final Usuario usuario) {
        if (usuario == null) {
            throw new SystemException("Usuário não pode ser nulo.");
        }
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        final List<Notificacao> listaNotificacoes = this.searchRepository.consultarPorNamedQuery("Notificacao.consultarTodas", parametros, 5,
                Notificacao.class);

        this.usuarioService.atualizarUltimaVerificacaoNotificacoes(usuario);
        return listaNotificacoes;
    }

    public Long consultarQuantidadeNotificacoesNaoVisualizadas(final Usuario usuario) throws ParseException {
        final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
        final Date dataInicioNotificacoes = formatter.parse("01/01/2015");
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", usuario.getId());
        parametros.put("dataInicioNotificacoes", dataInicioNotificacoes);
        return this.searchRepository.queryByNamedQueryUniqueResult("Notificacao.consultarQuantidadeNotificacoesNaoVisualizadas", parametros);
    }

    public Long consultarQuantidadeNotificacoesPendentes(final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", idUsuario);
        return this.searchRepository.queryByNamedQueryUniqueResult("Notificacao.consultarQuantidadeNotificacoesPendentes", parametros);
    }

    public List<Notificacao> consultarTodas(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        final List<Notificacao> notificacoes = this.searchRepository.consultarPorNamedQuery("Notificacao.consultarTodas", parametros,
                Notificacao.class);
        this.usuarioService.atualizarUltimaVerificacaoNotificacoes(usuario);
        return notificacoes;
    }

    public void enviarEmail(final Notificacao notificacao, final TipoAcao tipoAcao, final Usuario destinatario) {
        if (destinatario != null) {
            // Recupera o tipo de notificação configurado para esta Ação
            final TipoNotificacaoPorEmail tipoNotificacao = tipoAcao.getTipoNotificacaoEmail();
            // Recupera as configuracoes de notificação do usuario destinatario
            final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoes = this.configuracaoNotificacoesPorEmailService
                    .consultarConfiguracoesEmailsDoUsuario(destinatario);
            // Verifica se o usuario deseja receber notificações para este tipo de Ação
            System.out.println("teste");
            if (configuracaoNotificacoes != null && configuracaoNotificacoes.getStatusConfiguracao(tipoNotificacao)) {
                this.enviarNotificacaoPorEmail(notificacao);
            }
        }
    }

    @Transactional
    public void notificar(final AcaoUsuario acaoUsuario) {

        // Caso o usuario logado seja o mesmo do do destinatario, não deverá ocorrer a notificação
        if (acaoUsuario.getAutor() == null || acaoUsuario.getDestinatario() == null
                || acaoUsuario.getAutor().getId().equals(acaoUsuario.getDestinatario().getId())) {
            return;
        }

        if (acaoUsuario.getTipoAcao().isNotificavel()) {
            final Notificacao notificacao = new Notificacao(acaoUsuario);
            super.incluir(notificacao);
            if (acaoUsuario.getTipoAcao().isNotificavelEmail()) {
                this.enviarEmail(notificacao, acaoUsuario.getTipoAcao(), acaoUsuario.getDestinatario());
            }
        }
    }

    @Transactional
    public Notificacao notificar(final Convite convite) {
        TipoAcao tipoAcao = null;
        if (convite.isAmizade()) {
            tipoAcao = TipoAcao.CONVIDAR_PARA_AMIZADE;
        }
        if (convite.isRecomendacaoViagem()) {
            tipoAcao = TipoAcao.CONVIDAR_PARA_RECOMENDAR_SOBRE_VIAGEM;
        }
        final Notificacao notificacao = new Notificacao(convite, tipoAcao);
        super.incluir(notificacao);
        return notificacao;
    }

    @Transactional
    public void retirarPendencia(final List<Notificacao> notificacoesParaAtualizar) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("listaNotificacoes", notificacoesParaAtualizar);
        this.getRepository().alterarPorNamedQuery("Notificacao.retirarPendencia", parametros);
    }

    /*@Transactional
    public void retirarPendencia(final Amizade amizade) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("amizade", amizade);
        this.getRepository().alterarPorNamedQuery("Notificacao.retirarPendencia", parametros);
    }

    @Transactional
    public void retirarPendencia(final Convite convite) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("convite", convite);
        this.getRepository().alterarPorNamedQuery("Notificacao.retirarPendencia", parametros);
    }*/

    @Transactional
    public void retirarPendencia(final Notificacao notificacao) {

        // Se a notificacao for pendente
        if (notificacao.isPendente()) {
            // Se for notificacao de convite e for do tipo TipoAcao.CONVIDAR_PARA_AMIZADE, não retirar a pendencia.
            // Nesse caso, retirar somente quando aceitar ou ignorar o Convite
            // TODO verificar se deverão ser incluidas aqui os outros casos de convite
            if (notificacao.isNotificacaoConvite() && TipoAcao.CONVIDAR_PARA_AMIZADE.equals(notificacao.getAcaoUsuario().getTipoAcao())) {
                return;
            }

            final Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("notificacao", notificacao);
            this.getRepository().alterarPorNamedQuery("Notificacao.retirarPendencia", parametros);
        }
        // TODO verificar a necessidade desse trecho. Estava no código antigo mas nao consegui entender porque
        /*if (((Convite) notificacao.getAcaoPrincipal()).getAutor().getId().equals(getUsuarioAutenticado().getId())) {

        }*/
    }

    @Transactional
    public <T extends Acao> void retirarPendencia(final T acao) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("acaoPrincipal", acao);
        this.getRepository().alterarPorNamedQuery("Notificacao.retirarPendencia", parametros);
    }

    private void enviarNotificacaoPorEmail(final Notificacao notificacao) {

        // TODO pegar o email principal
        final String destinatario = notificacao.getDestinatario().getUsername();
        final StringBuffer assunto = new StringBuffer();
        final StringBuffer textoAcao = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        if (notificacao.getAcaoUsuario() != null) {
            final AcaoUsuario acaoUsuario = notificacao.getAcaoUsuario();

            // Quem - nome do usuario autor
            assunto.append(notificacao.getOriginador().getDisplayName());
            assunto.append(" ");

            // O que - Ação executada
            textoAcao.append(acaoUsuario.getTipoAcao().getDescricaoAcaoNoPassado());
            textoAcao.append(" ");
            // Preposição
            /*textoAcao.append(acaoUsuario.getTipoAcao().getPreposicaoPosAcao());
            textoAcao.append(" ");*/
            // Pronome
            textoAcao.append(acaoUsuario.getTipoAcao().getPronome());
            textoAcao.append(" ");
            // Onde - Sobre o que objeto a ação foi executada
            textoAcao.append(acaoUsuario.getTipoAcao().getDescricaoAlvo());

            if (!acaoUsuario.getTipoAcao().getPreposicaoPosAlvo().isEmpty()) {
                if (acaoUsuario.getAlvo().getDescricaoAlvo() != null) {
                    textoAcao.append(" ");
                    textoAcao.append(acaoUsuario.getTipoAcao().getPreposicaoPosAlvo());
                    textoAcao.append(" ");
                    textoAcao.append(acaoUsuario.getAlvo().getDescricaoAlvo());
                }
            }
            textoAcao.append(". ");

            params.put("autor", acaoUsuario.getAutor());
            params.put("destinatario", acaoUsuario.getDestinatario());
            params.put("textoAcao", textoAcao.toString());

            params.put("url", montarUrlNotificacao(notificacao));

            try {

                this.emailService.enviarEmail(destinatario, "Olá " + notificacao.getDestinatario().getDisplayName() + ", " + assunto.toString() + " "
                        + textoAcao.toString(), EmailTemplate.EMAIL_NOTIFICACAO, params);
            } catch (final Exception e) {
                e.printStackTrace();
                // TODO: handle exception
            }

        }
    }

    private String getUrlComentario(final Comentario comentario, final Usuario destinatarioNotificacao) {

        String url = " ";
        if (comentario.isComentarioAlbum()) {
            url = "/perfil/" + comentario.getAlbum().getAutor().getUrlPath() + "/albuns/" + comentario.getAlbum().getUrlPath() + "#comentarios";
        } else if (comentario.isComentarioAvaliacao()) {
            url = "/perfil/" + comentario.getAvaliacao().getAutor().getUrlPath() + "?tab=avaliacoes&destaque="
                    + comentario.getObjetoComentado().getId() + "#comentarios";
        } else if (comentario.isComentarioDica()) {
            url = "/perfil/" + comentario.getDica().getAutor().getUrlPath() + "?tab=dicas&destaque=" + comentario.getObjetoComentado().getId()
                    + "#comentarios";
        } else if (comentario.isComentarioDiarioViagem()) {
            url = "/planoViagem/diario/" + comentario.getObjetoComentado().getId() + "#comentarios";
        } else if (comentario.isComentarioFoto()) {
            url = "/fotos/" + comentario.getFoto().getUrlPath();
        } else if (comentario.isComentarioPerfil()) {
            url = "/perfil/" + comentario.getUsuario().getUrlPath() + "?tab=principal#" + comentario.getUrlPath();
        } else if (comentario.isComentarioRelatoViagem()) {
            // TODO
            url = "";
        } else if (comentario.isComentarioViagem()) {
            // TODO
            url = "";
        } /*else if (comentario.isRespostaComentario()) {
            return getUrlComentario(comentario.getComentarioPai(), destinatario);
          }*/
        return url;
    }

    private String montarUrlNotificacao(final Notificacao notificacao) {
        String url = "a";
        if (notificacao.isNotificacaoAmizade()) {
            url = "/perfil/" + notificacao.getOriginador().getUrlPath();
        } else if (notificacao.isNotificacaoComentario()) {
            Comentario comentario = (Comentario) notificacao.getAcaoPrincipal();
            if (comentario.isRespostaComentario()) {
                comentario = comentario.getComentarioPai();
            }
            url = getUrlComentario(comentario, notificacao.getDestinatario());
        } else if (notificacao.isNotificacaoConvite()) {
            final Convite convite = (Convite) notificacao.getAcaoPrincipal();
            if (convite.isAmizade()) {
                if (convite.isAceito()) {
                    url = "/perfil/" + convite.getConvidado().getUrlPath();
                } else {
                    url = "/perfil/amigos/" + convite.getConvidado().getUrlPath() + "/solicitacoesAmizade";
                }
            } else {
                // TODO apenas para resolver o null pointer; temos que definir essa url
                url = "/perfil/" + convite.getConvidado().getUrlPath();
            }
        } else if (notificacao.isNotificacaoVotoUtil()) {
            final VotoUtil votoUtil = (VotoUtil) notificacao.getAcaoPrincipal();

            if (votoUtil.isVotoAvaliacao()) {
                url = "/perfil/" + votoUtil.getObjetoVotado().getAutor().getUrlPath() + "?tab=avaliacoes&destaque=" + votoUtil.getIdObjetoVotado();
            } else if (votoUtil.isVotoDica()) {
                url = "/perfil/" + votoUtil.getObjetoVotado().getAutor().getUrlPath() + "?tab=dicas&destaque=" + votoUtil.getIdObjetoVotado();
            } else if (votoUtil.isVotoRelatoViagem()) {
                // url = "/perfil/" + votoUtil.getObjetoVotado().getAutor().getUrlPath() + "?tab=dicas&destaque=" + votoUtil.getIdObjetoVotado();
            } else if (votoUtil.isVotoResposta()) {
                url = "/perguntas/verPergunta/" + votoUtil.getResposta().getPergunta().getUrlPath() + "#respostas";
            }

        } else if (notificacao.isNotificacaoResposta()) {
            url = "/perguntas/verPergunta/" + ((Resposta) notificacao.getAcaoPrincipal()).getPergunta().getUrlPath() + "#respostas";
        } else if (notificacao.isNotificacaoPedidoDica()) {
            final PedidoDica pedidoDica = ((PedidoDica) notificacao.getAcaoPrincipal());
            // url = "/dicas/pedidoDica/listagemPedidosDicaAmigos";
            url = "/viagem/" + pedidoDica.getViagem().getId() + "/ajudar/" + pedidoDica.getId() + "/sugerirLocais";
            if (notificacao.getTipoAcao().equals(TipoAcao.RESPONDER_PEDIDO_DICA)) {
                // url = "/dicas/pedidoDica/verResposta/" + notificacao.getAcaoPrincipal().getId();
                url = "/viagem/" + pedidoDica.getId() + "/planejamento/sugestoesAmigos";
            }
        }
        return url;
    }
}
