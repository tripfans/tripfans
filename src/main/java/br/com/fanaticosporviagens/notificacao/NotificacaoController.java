package br.com.fanaticosporviagens.notificacao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.model.entity.Notificacao;

/**
 * @author André Thiago
 *
 */
@Controller
public class NotificacaoController extends BaseController {

    private final Log log = LogFactory.getLog(NotificacaoController.class);

    @Autowired
    private NotificacaoService notificacaoService;

    // TODO fazer a Notificacao implementar UrlNameable
    @RequestMapping(value = "/atualizarNotificacao/{notificacao}", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody JSONResponse atualizarNotificacao(@PathVariable("notificacao") final Notificacao notificacao) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Vai tirar a pendência da notificação " + notificacao.getId());
        }
        // this.notificacaoService.retirarPendencia(notificacao);
        return this.jsonResponse.success();
    }
}
