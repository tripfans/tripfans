package br.com.fanaticosporviagens.pergunta;

import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Pergunta;
import br.com.fanaticosporviagens.model.entity.Resposta;

/**
 * @author André Thiago
 * 
 */
@Controller
@RequestMapping("/perguntas")
public class PerguntasController extends BaseController {

    @Autowired
    private RespostaService respostaService;

    @Autowired
    private PerguntaService service;

    @RequestMapping(value = "/perguntar/incluir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String incluir(@Valid @ModelAttribute("pergunta") final Pergunta pergunta, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addAttribute("errors", result.getAllErrors());
            return "redirect:/perguntas/listar";
        }
        this.service.incluir(pergunta);
        final Local local = pergunta.getLocal();
        if (local.getValidado()) {
            success("Pergunta incluída com sucesso");
            return "redirect:" + local.getUrl() + "?tab=perguntas";
        } else {
            success("Pergunta incluída com sucesso. Ela será exibida assim que este local for validado pela Equipe do TripFans.");
            return "redirect:/perfil/" + getUsuarioAutenticado().getUrlPath();
        }
    }

    @RequestMapping(value = "listar/{local}", method = { RequestMethod.GET })
    public String listar(@PathVariable("local") final Local local, final Model model) {
        String retorno = "";
        if (this.getSearchData().getStart() == 0 && this.getSearchData().getSort() == null) {
            retorno = "/perguntas/perguntasLocal";
        } else {
            retorno = "/perguntas/listaPerguntas";
        }
        model.addAttribute("local", local);
        this.populaModelComPerguntas(model, local);
        return retorno;
    }

    @RequestMapping(value = "/listar", method = RequestMethod.GET)
    public String listar(final Model model) {
        String retorno = "";
        if (this.getSearchData().getSort() == null && (this.getSearchData().getStart() == null || this.getSearchData().getStart() == 0)) {
            this.getSearchData().setStart(0);
            retorno = "/perguntas/todasPerguntas";
        } else {
            retorno = "/perguntas/listaPerguntas";
        }
        this.populaModelComPerguntas(model, null);
        return retorno;
    }

    @RequestMapping(value = "/perguntar", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String perguntar(@RequestParam(required = false) final Local local, final Model model) {
        if (local != null) {
            model.addAttribute("local", local);
        }
        return "/perguntas/formPergunta";
    }

    private void populaModelComPerguntas(final Model model, final Local local) {
        List<Pergunta> perguntas = Collections.emptyList();
        if (local == null) {
            perguntas = this.service.consultarTodas();
        } else if (local.getQuantidadePerguntas() >= 1) {
            perguntas = this.service.consultarTodas(local);
        }
        model.addAttribute("perguntas", perguntas);
    }

    private void populaModelComRespostas(final Model model, final Pergunta pergunta) {
        final List<Resposta> respostas = this.respostaService.consultarRespostas(pergunta);
        model.addAttribute("respostas", respostas);
        model.addAttribute(pergunta);
    }

    @RequestMapping(value = "/responder", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody
    StatusMessage responder(final Resposta resposta, final Model model, final RedirectAttributes redirectAttributes) {
        this.respostaService.incluir(resposta);
        final Pergunta pergunta = resposta.getPergunta();
        redirectAttributes.addAttribute("pergunta", pergunta.getUrlPath());
        return this.success("Muito obrigado por responder essa pergunta! Continue contribuindo.");
    }

    @RequestMapping(value = "/verPergunta/{pergunta}", method = { RequestMethod.GET })
    public String verPergunta(@PathVariable("pergunta") final Pergunta pergunta, final Model model) {
        this.populaModelComRespostas(model, pergunta);
        if (pergunta.getLocal() != null) {
            this.getSearchData().setStart(0);
            this.getSearchData().setLimit(5);
            model.addAttribute("perguntas", this.service.consultarPerguntasRelacionadas(pergunta));
        }
        return "/perguntas/verPergunta";
    }

    @RequestMapping(value = "/verRespostas/{pergunta}", method = { RequestMethod.GET })
    public String verRespostas(@PathVariable("pergunta") final Pergunta pergunta, final Model model) {
        this.populaModelComRespostas(model, pergunta);
        return "/perguntas/listaRespostas";
    }
}
