package br.com.fanaticosporviagens.pergunta;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Pergunta;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author André Thiago
 * 
 */
@Service
public class PerguntaService extends GenericCRUDService<Pergunta, Long> {

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    public List<Pergunta> consultarPerguntasRelacionadas(final Pergunta pergunta) {
        if (pergunta.getLocal() != null) {
            final List<Pergunta> perguntas = consultarTodas(pergunta.getLocal());
            perguntas.remove(pergunta);
            return perguntas;
        }
        return null;
    }

    public Long consultarQuantidadePerguntas(final Local local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local);
        return this.searchRepository.queryByNamedQueryUniqueResult("Pergunta.consultarQuantidadePerguntasLocal", parametros);
    }

    public List<Pergunta> consultarTodas() {
        return this.searchRepository.consultarPorNamedQuery("Pergunta.consultarTodasPerguntas", Pergunta.class);
    }

    /**
     * Consulta todas as perguntas sobre determinado local
     * 
     * @param local
     *            o local da pergunta
     * @return
     */
    public List<Pergunta> consultarTodas(final Local local) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("local", local);
        return this.searchRepository.consultarPorNamedQuery("Pergunta.consultarTodasPerguntasLocal", parametros, Pergunta.class);
    }

    public List<Pergunta> consultarTodasPorUsuario(final Usuario usuarioAutor) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("autor", usuarioAutor);
        return this.searchRepository.consultarPorNamedQuery("Pergunta.consultarTodasPerguntas", parametros, Pergunta.class);
    }

    @Override
    @Transactional
    public void incluir(final Pergunta pergunta) {
        this.urlGeneratorService.generate(pergunta);
        pergunta.setAutor(getUsuarioAutenticado());
        pergunta.setData(new Date());
        pergunta.setQuantidadeRespostas(0L);
        if (pergunta.getLocal().getValidado()) {
            pergunta.setDataPublicacao(new LocalDateTime());
        }
        super.incluir(pergunta);
    }

}
