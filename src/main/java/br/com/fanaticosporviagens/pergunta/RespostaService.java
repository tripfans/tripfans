package br.com.fanaticosporviagens.pergunta;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.Pergunta;
import br.com.fanaticosporviagens.model.entity.Resposta;

@Service
public class RespostaService extends GenericCRUDService<Resposta, Long> {

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    public List<Resposta> consultarRespostas(final Pergunta pergunta) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("pergunta", pergunta);
        return this.searchRepository.consultarPorNamedQuery("Resposta.consultarRespostas", parametros, Resposta.class);
    }

    @Override
    @Transactional
    public void incluir(final Resposta resposta) {
        this.urlGeneratorService.generate(resposta);
        resposta.setAutor(getUsuarioAutenticado());
        resposta.setData(new Date());
        resposta.setDataPublicacao(new LocalDateTime());
        resposta.getPergunta().adicionarResposta();
        super.incluir(resposta);
    }

}
