package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.PreferenciasViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

@Component
public class GerenciadorAcoesUsuarioPreferenciasViagem extends GerenciadorAcoesUsuarioPadrao<PreferenciasViagem> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT preferencia ");
        hql.append("  FROM PreferenciasViagem preferencia");
        hql.append(" WHERE preferencia.dataGeracaoAcaoAlteracao IS NULL ");
        hql.append("    OR preferencia.dataUltimaAlteracao > preferencia.dataGeracaoAcaoAlteracao");

        return consultarAcoesPendentes(hql.toString());

    }

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT preferencia ");
        hql.append("  FROM PreferenciasViagem preferencia");
        hql.append(" WHERE preferencia.dataGeracaoAcaoInclusao IS NULL");

        return consultarAcoesPendentes(hql.toString());
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

    private List<AcaoUsuario> consultarAcoesPendentes(final String hql) {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<PreferenciasViagem> preferencias = this.getSession().createQuery(hql).list();

        acoesUsuario.addAll(criarAcoesUsuario(preferencias, TipoAcao.ALTERAR_PREFERENCIAS_VIAGEM_PERFIL));
        return acoesUsuario;
    }

}
