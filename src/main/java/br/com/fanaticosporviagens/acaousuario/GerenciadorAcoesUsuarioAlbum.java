package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Album;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioAlbum extends GerenciadorAcoesUsuarioPadrao<Album> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT album ");
        hql.append("  FROM Album album");
        hql.append(" WHERE album.dataGeracaoAcaoInclusao IS NULL");
        // hql.append("   AND album.dataPublicacao IS NOT NULL");
        hql.append("   AND album.dataExclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<Album> albuns = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(albuns, TipoAcao.ADICIONAR_ALBUM));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        final StringBuffer update = new StringBuffer();
        update.append("UPDATE Album album ");
        update.append("SET fotoPrincipal = NULL ");
        update.append("WHERE album = :acao ");

        this.getSession().createQuery(update.toString()).setParameter("acao", acao).executeUpdate();

        final StringBuffer deleteFotos = new StringBuffer();
        deleteFotos.append("DELETE FROM Foto ");
        deleteFotos.append("WHERE album = :acao ");

        this.getSession().createQuery(deleteFotos.toString()).setParameter("acao", acao).executeUpdate();
    }

}
