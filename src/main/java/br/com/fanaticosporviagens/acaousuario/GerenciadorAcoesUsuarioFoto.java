package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Album;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.FotosUsuarioView;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioFoto extends GerenciadorAcoesUsuarioPadrao<Foto> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT foto ");
        hql.append("  FROM Foto foto");
        hql.append(" WHERE (foto.dataGeracaoAcaoAlteracao IS NULL");
        hql.append("    OR foto.dataUltimaAlteracao > foto.dataGeracaoAcaoAlteracao)");
        hql.append("   AND foto.anonima = false");
        hql.append("   AND foto.usuario.id IS NOT NULL)");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<Foto> fotos = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(fotos, TipoAcao.ALTERAR_FOTO_PERFIL));

        return acoesUsuario;
    }

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final StringBuffer sql = new StringBuffer();

        sql.append("SELECT id_autor id_usuario, ");
        sql.append("       id_viagem, ");
        sql.append("       COUNT(id_viagem) quantidade_fotos_viagem, ");
        sql.append("       id_album, ");
        sql.append("       COUNT(id_album) quantidade_fotos_album ");
        sql.append("  FROM foto f ");
        sql.append(" WHERE f.data_geracao_acao_inclusao IS NULL ");
        sql.append("   AND f.data_exclusao IS NULL ");
        sql.append("   AND f.data_publicacao IS NOT NULL ");
        sql.append("   AND (f.id_viagem IS NOT NULL OR f.id_album IS NOT NULL) ");
        sql.append("   AND f.anonima = FALSE ");
        sql.append(" GROUP BY id_autor, id_viagem, id_album");

        final List<FotosUsuarioView> fotos = this.searchRepository.consultaSQL(sql.toString(), null, FotosUsuarioView.class);

        for (final FotosUsuarioView foto : fotos) {

            if (foto.getQuantidadeFotosAlbum() > 0) {
                inserirAcaoUsuario(foto.getIdUsuario(), null, TipoAcao.ADICIONAR_FOTO_ALBUM, foto.getIdAlbum(), Album.class,
                        foto.getQuantidadeFotosAlbum(), new Date());

                atualizarDataGeracaoAcaoFotosAlbum(foto.getIdAlbum());
            }
            if (foto.getQuantidadeFotosViagem() > 0) {
                inserirAcaoUsuario(foto.getIdUsuario(), null, TipoAcao.ADICIONAR_FOTO_VIAGEM, foto.getIdViagem(), PlanoViagem.class,
                        foto.getQuantidadeFotosViagem(), new Date());
                atualizarDataGeracaoAcaoFotosViagem(foto.getIdViagem());
            }
        }

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        final StringBuffer updateAlbum = new StringBuffer();
        updateAlbum.append("UPDATE Album ");
        updateAlbum.append("SET fotoPrincipal = NULL ");
        updateAlbum.append("WHERE fotoPrincipal = :acao ");

        final Query query = this.getSession().createQuery(updateAlbum.toString()).setParameter("acao", acao);
        query.executeUpdate();
    }

    @Transactional
    private void atualizarDataGeracaoAcaoFotosAlbum(final Long id) {

        final StringBuffer update = new StringBuffer();

        update.append("UPDATE Foto foto");
        update.append("   SET foto.dataGeracaoAcaoInclusao = :dataGeracaoAcao ");
        update.append(" WHERE idAlbum = :id ");
        update.append("   AND foto.dataGeracaoAcaoInclusao IS NULL ");

        final Query updateQuery = this.getSession().createQuery(update.toString());

        updateQuery.setParameter("id", id);// , StandardBasicTypes.LONG);
        updateQuery.setParameter("dataGeracaoAcao", new Date());// , StandardBasicTypes.TIMESTAMP);

        updateQuery.executeUpdate();
    }

    @Transactional
    private void atualizarDataGeracaoAcaoFotosViagem(final Long id) {

        final StringBuffer update = new StringBuffer();

        update.append("UPDATE Foto foto");
        update.append("   SET foto.dataGeracaoAcaoInclusao = :dataGeracaoAcao ");
        update.append(" WHERE idViagem = :id ");
        update.append("   AND foto.dataGeracaoAcaoInclusao IS NULL ");

        final Query updateQuery = this.getSession().createQuery(update.toString());

        updateQuery.setParameter("id", id);// , StandardBasicTypes.LONG);
        updateQuery.setParameter("dataGeracaoAcao", new Date());// , StandardBasicTypes.TIMESTAMP);

        updateQuery.executeUpdate();
    }

}
