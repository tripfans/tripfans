package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.dica.model.repository.DicaRepository;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Dica;

public class GeradorAcaoUsuarioDica {// extends AbstractGeradorAcaoUsuario<Dica> {

    // @Override
    public List<AcaoUsuario> gerarAcoesUsuarioInclusao(final Dica entidade) {
        final Dica dica = entidade;
        final DicaRepository dicaRepository = SpringBeansProvider.getBean(DicaRepository.class);
        final Dica dicaAux = dicaRepository.consultarPorId(Dica.class, dica.getId());

        final List<AcaoUsuario> acoes = new ArrayList<AcaoUsuario>();
        // acoes.add(new AcaoUsuario(TipoAcao.DICA_ESCREVER, dicaAux.getAutor(), dicaAux));
        return acoes;
    }

}
