package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

@Component
public class GerenciadorAcoesUsuarioPedidoDica extends GerenciadorAcoesUsuarioPadrao<PedidoDica> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT pedido ");
        hql.append("  FROM PedidoDica pedido");
        hql.append(" WHERE pedido.dataGeracaoAcaoAlteracao IS NULL");
        hql.append("   AND pedido.dataPublicacao IS NOT NULL");
        hql.append("   AND pedido.dataExclusao IS NULL");
        hql.append("   AND pedido.respondido = true");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final List<PedidoDica> pedidos = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(pedidos, TipoAcao.RESPONDER_PEDIDO_DICA));

        return acoesUsuario;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT pedido ");
        hql.append("  FROM PedidoDica pedido");
        hql.append(" WHERE pedido.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND pedido.dataPublicacao IS NOT NULL");
        hql.append("   AND pedido.dataExclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final List<PedidoDica> pedidos = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(super.criarAcoesUsuario(pedidos, TipoAcao.CONVIDAR_PARA_RECOMENDAR_SOBRE_VIAGEM));

        return acoesUsuario;
    }

    @Override
    protected List<AcaoUsuario> criarAcoesUsuario(final List<PedidoDica> pedidos, final TipoAcao tipoAcao) {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        for (final PedidoDica pedido : pedidos) {
            if (BooleanUtils.isTrue(pedido.getRespondido())) {
                acoesUsuario.add(new AcaoUsuario(pedido.getPraQuemPedir(), tipoAcao, pedido, pedido.getAlvo(), pedido.getAutor()));
            }
        }
        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
