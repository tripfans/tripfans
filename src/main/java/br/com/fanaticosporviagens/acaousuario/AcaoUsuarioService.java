package br.com.fanaticosporviagens.acaousuario;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.acaousuario.GeradorAcaoUsuarioPadrao.MomentoGeracaoAcao;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.security.SecurityService;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PontuacaoUsuarioLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;

@Service
public class AcaoUsuarioService extends GenericCRUDService<AcaoUsuario, Long> {

    @Autowired
    private AcaoUsuarioRepository acaoUsuarioRepository;

    @Autowired
    private NotificacaoService notificacaoService;

    @Autowired
    private SecurityService securityService;

    public AcaoUsuario consultarAcaoUsuario(final Usuario usuario, final AlvoAcao alvoAcao) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        if (alvoAcao instanceof Avaliacao) {
            parametros.put("rastreioAvaliacao", alvoAcao);
        }
        if (alvoAcao instanceof Dica) {
            parametros.put("rastreioDica", usuario);
        }
        final List<AcaoUsuario> resultado = this.searchRepository.consultarPorNamedQuery("AcaoUsuario.consultarAcaoUsuario", parametros,
                AcaoUsuario.class);
        if (!resultado.isEmpty()) {
            return resultado.get(0);
        }
        return null;
    }

    public List<AcaoUsuario> consultarAcoesAmigosEmLocal(final Usuario usuario, final Local local, final Integer tamanhoAmostra) {
        if (usuario == null || local == null) {
            return null;
        }
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        if (local.isTipoLocalEnderecavel()) {
            parametros.put("localEnderecavel", local);
        } else if (local.isTipoLocalInteresseTuristico()) {
            parametros.put("localInteresseTuristico", local);
        } else if (local.isTipoCidade()) {
            parametros.put("localCidade", local);
        }
        return this.searchRepository.consultarPorNamedQuery("AcaoUsuario.consultarAcoesAmigosEmLocal", parametros, tamanhoAmostra, AcaoUsuario.class);
    }

    public List<AcaoUsuario> consultarAcoesUsuario(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        return this.searchRepository.consultarPorNamedQuery("AcaoUsuario.consultarAcoesUsuario", parametros, AcaoUsuario.class);
    }

    public List<AcaoUsuario> consultarAcoesUsuario(final Usuario usuario, final Integer tamanhoAmostra) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        return this.searchRepository.consultarPorNamedQuery("AcaoUsuario.consultarAcoesUsuario", parametros, tamanhoAmostra, AcaoUsuario.class);
    }

    public List<PontuacaoUsuarioLocal> consultarPontuacaoUsuarioLocal(final Local local, final Integer tamanhoAmostra) {
        return this.acaoUsuarioRepository.consultarPontuacaoUsuarioLocal(local, tamanhoAmostra);
    }

    public List<AcaoUsuario> consultarUltimasAcoes(final Integer tamanhoAmostra) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        return this.searchRepository.consultarPorNamedQuery("AcaoUsuario.consultarUltimasAcoes", parametros, tamanhoAmostra, AcaoUsuario.class);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Transactional
    public <T extends Acao> void registrarAcaoAlteracao(final T entidade) {
        if (entidade instanceof Acao) {
            final GeradorAcaoUsuarioPadrao gerador = new GeradorAcaoUsuarioPadrao(this, entidade, MomentoGeracaoAcao.POS_ALTERACAO);
            final Thread thread = new Thread(gerador);
            thread.run();
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Transactional
    public <T extends Acao> void registrarAcaoInclusao(final T entidade) {
        if (entidade instanceof Acao) {
            final GeradorAcaoUsuarioPadrao gerador = new GeradorAcaoUsuarioPadrao(this, entidade, MomentoGeracaoAcao.POS_INCLUSAO);
            final Thread thread = new Thread(gerador);
            thread.run();
        }
    }

    void registrarAcoes(final List<AcaoUsuario> acoes) {
        for (final AcaoUsuario acao : acoes) {
            registrarAcao(acao);
        }
    }

    private void registrarAcao(final AcaoUsuario acaoUsuario) {
        if (acaoUsuario != null) { // esse if é para evitar o caso de dica advinda do foursquare, por exemplo
            this.acaoUsuarioRepository.incluir(acaoUsuario);
            if (acaoUsuario.getTipoAcao().isNotificavel()) {
                this.notificacaoService.notificar(acaoUsuario);
            }
        }
    }

}
