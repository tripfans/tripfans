package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

@Component
public class GerenciadorAcoesUsuarioComentario extends GerenciadorAcoesUsuarioPadrao<Comentario> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        for (final Comentario comentario : consultarComentarios()) {
            acoesUsuario.add(criarAcaoUsuario(comentario, getTipoAcao(comentario)));
        }

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

    private List<Comentario> consultarComentarios() {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT comentario ");
        hql.append("  FROM Comentario comentario");
        hql.append(" WHERE comentario.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND comentario.dataExclusao IS NULL");

        final List<Comentario> comentarios = this.getSession().createQuery(hql.toString()).list();
        return comentarios;
    }

    private TipoAcao getTipoAcao(final Comentario comentario) {
        if (comentario.isRespostaComentario()) {
            return TipoAcao.RESPONDER_COMENTARIO;
        } else if (comentario.isComentarioAlbum()) {
            return TipoAcao.COMENTAR_ALBUM;
        } else if (comentario.isComentarioAvaliacao()) {
            return TipoAcao.COMENTAR_AVALIACAO;
        } else if (comentario.isComentarioDica()) {
            return TipoAcao.COMENTAR_DICA;
        } else if (comentario.isComentarioDiarioViagem()) {
            return TipoAcao.COMENTAR_DIARIO_VIAGEM;
        } else if (comentario.isComentarioFoto()) {
            return TipoAcao.COMENTAR_FOTO;
        } else if (comentario.isComentarioPerfil()) {
            return TipoAcao.COMENTAR_PERFIL;
        } else if (comentario.isComentarioRelatoViagem()) {
            return TipoAcao.COMENTAR_RELATO_LOCAL;
        }
        return null;
    }
}
