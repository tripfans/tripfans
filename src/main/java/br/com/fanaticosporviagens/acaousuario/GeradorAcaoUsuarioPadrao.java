package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.annotations.GeradorAcoes;

public class GeradorAcaoUsuarioPadrao<T extends Acao> implements GeradorAcaoUsuario<T>, Runnable {

    private final AcaoUsuarioService acaoUsuarioService;

    private final T entidade;

    private final MomentoGeracaoAcao momentoGeracao;

    public GeradorAcaoUsuarioPadrao(final AcaoUsuarioService acaoUsuarioService, final T entidade, final MomentoGeracaoAcao momentoGeracao) {
        this.acaoUsuarioService = acaoUsuarioService;
        this.entidade = entidade;
        this.momentoGeracao = momentoGeracao;
    }

    @Override
    public void run() {
        this.acaoUsuarioService.registrarAcoes(gerarAcoesUsuario());
    }

    private List<AcaoUsuario> gerarAcoesUsuario() {
        final List<AcaoUsuario> acoes = new ArrayList<AcaoUsuario>();
        if (this.entidade.getClass().isAnnotationPresent(GeradorAcoes.class)) {
            AcaoUsuario acaoUsuario = null;
            final GeradorAcoes geradorAcoes = this.entidade.getClass().getAnnotation(GeradorAcoes.class);

            Usuario autor = null;
            Usuario destinatario = null;
            AlvoAcao alvoGeral = null;
            AlvoAcao alvo = null;

            if (!geradorAcoes.alvo().isEmpty()) {
                alvoGeral = (AlvoAcao) getNestedPropertyValue(this.entidade, geradorAcoes.alvo());
            }
            // Varre as ações mapeadas na Entidade
            for (final br.com.fanaticosporviagens.model.entity.annotations.Acao acao : geradorAcoes.acoes()) {
                if (alvoGeral == null) {
                    alvo = (AlvoAcao) getNestedPropertyValue(this.entidade, acao.alvo());
                    // Verificar se o alvo continua null: se sim, parar geração desta açao
                    if (alvo == null) {
                        continue;
                    }
                } else {
                    alvo = alvoGeral;
                }
                // Verificar se é uma inclusao ou alteracao e se a Acao deve ser gerada nessas situações
                if (MomentoGeracaoAcao.POS_INCLUSAO.equals(this.momentoGeracao) && acao.tipo().isExecutarPosInclusao()
                        || MomentoGeracaoAcao.POS_ALTERACAO.equals(this.momentoGeracao) && acao.tipo().isExecutarPosAlteracao()) {

                    if (MomentoGeracaoAcao.POS_ALTERACAO.equals(this.momentoGeracao) && acao.tipo().isExecutarPosAlteracao()) {
                        // Verificar se a ação já foi gerada para os casos necessários
                        if (!acao.referenciaAcaoGerada().isEmpty()) {
                            final Object acaoReferenciada = getNestedPropertyValue(this.entidade, acao.referenciaAcaoGerada());
                            if (acaoReferenciada != null) {
                                break;
                            }
                        }
                    }

                    // Verifica se a classe permitida pelo Tipo é a mesma do alvo e se a condicao é verdadeira, caso tenha sido declarada
                    if ((acao.tipo().getClasseDoAlvo() != null && (acao.tipo().getClasseDoAlvo().equals(alvo.getClass()) || acao.tipo()
                            .getClasseDoAlvo().isAssignableFrom(alvo.getClass())))
                            && (acao.condicao().isEmpty() || (Boolean) getNestedPropertyValue(this.entidade, acao.condicao()))) {
                        if (!acao.autor().isEmpty()) {
                            autor = (Usuario) getNestedPropertyValue(this.entidade, acao.autor());
                        } else {
                            autor = (Usuario) getNestedPropertyValue(this.entidade, geradorAcoes.autor());
                        }
                        if (!acao.destinatario().isEmpty()) {
                            destinatario = (Usuario) getNestedPropertyValue(this.entidade, acao.destinatario());
                        } else {
                            destinatario = (Usuario) getNestedPropertyValue(this.entidade, geradorAcoes.destinatario());
                        }
                        if (autor != null) {
                            acaoUsuario = new AcaoUsuario(autor, acao.tipo(), this.entidade, alvo, destinatario);
                        }
                        if (acaoUsuario != null) {
                            if (!acao.referenciaAcaoGerada().isEmpty()) {
                                setNestedPropertyValue(this.entidade, acao.referenciaAcaoGerada(), acaoUsuario);
                            }
                            acoes.add(acaoUsuario);
                            // break;
                        }
                    }
                }
            }
        }

        return acoes;
    }

    private Object getNestedPropertyValue(final Object source, final String property) {
        try {
            return PropertyUtils.getNestedProperty(source, property);
        } catch (final NestedNullException e) {
            return null;
        } catch (final NoSuchMethodException e) {
            return null;
        } catch (final Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private void setNestedPropertyValue(final Object source, final String property, final Object value) {
        try {
            PropertyUtils.setNestedProperty(source, property, value);
        } catch (final NestedNullException e) {
        } catch (final NoSuchMethodException e) {
        } catch (final Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public enum MomentoGeracaoAcao {
        POS_ALTERACAO,
        POS_INCLUSAO;
    }

}
