package br.com.fanaticosporviagens.acaousuario;

public interface GerenciadorAcoesUsuario<T extends Acao> {

    public void executar();

}
