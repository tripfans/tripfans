package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioDica extends GerenciadorAcoesUsuarioPadrao<Dica> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT dica ");
        hql.append("  FROM Dica dica");
        hql.append(" WHERE dica.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND dica.dataPublicacao IS NOT NULL");
        hql.append("   AND dica.dataExclusao IS NULL");
        hql.append("   AND dica.atividadePlano IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final List<Dica> dicas = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(dicas, TipoAcao.ESCREVER_DICA));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
