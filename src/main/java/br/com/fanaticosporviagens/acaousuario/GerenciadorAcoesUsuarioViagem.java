package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.DiarioViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

@Component
public class GerenciadorAcoesUsuarioViagem extends GerenciadorAcoesUsuarioPadrao<DiarioViagem> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT viagem ");
        hql.append("  FROM Viagem viagem");
        hql.append(" WHERE viagem.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND viagem.dataPublicacao IS NOT NULL");
        hql.append("   AND viagem.dataExclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<DiarioViagem> viagens = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(viagens, TipoAcao.PUBLICAR_PLANO_VIAGEM));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
