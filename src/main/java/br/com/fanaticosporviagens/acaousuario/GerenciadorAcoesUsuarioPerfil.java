package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Component
public class GerenciadorAcoesUsuarioPerfil extends GerenciadorAcoesUsuarioPadrao<Usuario> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT usuario ");
        hql.append("  FROM Usuario usuario");
        hql.append(" WHERE (usuario.dataGeracaoAcaoAlteracao IS NULL");
        hql.append("    OR usuario.dataUltimaAlteracaoDadosPerfil > usuario.dataGeracaoAcaoAlteracao)");
        hql.append("   AND usuario.dataConfirmacao IS NOT NULL");
        hql.append("   AND usuario.dataComplementoCadastro IS NOT NULL");
        hql.append("   AND usuario.ativo = true");
        hql.append("   AND usuario.bloqueado = false");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<Usuario> usuarios = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(usuarios, TipoAcao.ALTERAR_INFORMACOES_BASICAS_PERFIL));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
