package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.InteressesViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

@Component
public class GerenciadorAcoesUsuarioInteressesViagem extends GerenciadorAcoesUsuarioPadrao<InteressesViagem> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT interesse ");
        hql.append("  FROM InteressesViagem interesse");
        hql.append(" WHERE interesse.dataGeracaoAcaoAlteracao IS NULL ");
        hql.append("    OR interesse.dataUltimaAlteracao > interesse.dataGeracaoAcaoAlteracao");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<InteressesViagem> interesses = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(interesses, TipoAcao.ALTERAR_INTERESSES_VIAGEM_PERFIL));

        return acoesUsuario;
    }

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT interesse ");
        hql.append("  FROM InteressesViagem interesse");
        hql.append(" WHERE interesse.dataGeracaoAcaoInclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<InteressesViagem> interesses = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(interesses, TipoAcao.ALTERAR_INTERESSES_VIAGEM_PERFIL));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
