package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Component
public class GerenciadorAcoesUsuarioConvite extends GerenciadorAcoesUsuarioPadrao<Convite> {

    @Override
    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        for (final Convite convite : consultarConvitesAceitos()) {
            acoesUsuario.addAll(criarAcoesUsuario(convite));
        }
        return acoesUsuario;
    }

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        for (final Convite convite : consultarConvitesEnviados()) {
            acoesUsuario.add(criarAcaoUsuario(convite, getTipoAcao(convite, false)));
        }
        return acoesUsuario;
    }

    private List<Convite> consultarConvitesAceitos() {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT convite ");
        hql.append("  FROM Convite convite");
        // hql.append(" WHERE convite.dataGeracaoAcaoInclusao IS NOT NULL");
        // hql.append("   AND convite.dataGeracaoAcaoAlteracao IS NULL");
        hql.append(" WHERE convite.dataGeracaoAcaoAlteracao IS NULL");
        hql.append("   AND convite.convidado.id IS NOT NULL");
        hql.append("   AND convite.dataExclusao IS NULL");
        hql.append("   AND convite.aceito = true");
        return this.getSession().createQuery(hql.toString()).list();
    }

    private List<Convite> consultarConvitesEnviados() {
        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT convite ");
        hql.append("  FROM Convite convite");
        hql.append(" WHERE convite.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND convite.dataExclusao IS NULL");
        hql.append("   AND convite.aceito = false");
        return this.getSession().createQuery(hql.toString()).list();
    }

    protected AcaoUsuario criarAcaoUsuario(final Usuario autor, final TipoAcao tipoAcao, final Acao acao, final AlvoAcao objetoAlvo,
            final Usuario destinatario) {
        return new AcaoUsuario(autor, tipoAcao, acao, objetoAlvo, destinatario);
    }

    protected List<AcaoUsuario> criarAcoesUsuario(final Convite convite) {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        // Quando aceitar o convite de amizade, gerar duas ações de aceite (uma para cada)
        // Adicionando ação para o usuario convidado
        // Comentado por enquanto. Acho que nesse caso nao é necessario notificar quem foi convidado
        /*acoesUsuario
                .add(this.criarAcaoUsuario(convite.getAutor(), getTipoAcao(convite, true), convite, convite.getAlvo(), convite.getDestinatario()));*/
        // Adicionando ação para o usuario que convidou
        if (convite.getDestinatario() != null) {
            acoesUsuario.add(this.criarAcaoUsuario(convite.getDestinatario(), getTipoAcao(convite, true), convite, convite.getAutor(),
                    convite.getAutor()));
        }
        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

    private TipoAcao getTipoAcao(final Convite convite, final boolean aceito) {
        if (convite.isAmizade()) {
            // TODO ver se devemos retirar essa variavel boolean do metodo getTipoAcao
            // TODO Não está funcionando o aceito - Acho que o problema de cache do hibernate continua... Tentar resolver
            // Enquanto isso mantendo a variavel boolean
            if (convite.isAceito() || aceito) {
                return TipoAcao.ACEITAR_CONVITE_AMIZADE;
            } else {
                return TipoAcao.CONVIDAR_PARA_AMIZADE;
            }
        } else if (convite.isRecomendacaoLocal()) {
            return TipoAcao.CONVIDAR_PARA_RECOMENDAR_SOBRE_LOCAL;
        } else if (convite.isRecomendacaoViagem()) {
            return TipoAcao.CONVIDAR_PARA_RECOMENDAR_SOBRE_VIAGEM;
        } else if (convite.isParticipacaoViagem()) {
            if (convite.isAceito()) {
                return TipoAcao.ACEITAR_CONVITE_VIAGEM;
            } else {
                return TipoAcao.CONVIDAR_PARA_PARTICIPAR_DE_VIAGEM;
            }
        } else if (convite.isTripfans()) {
            if (aceito) {
                return TipoAcao.ACEITAR_CONVITE_TRIPFANS;
            } else {
                return TipoAcao.CONVIDAR_PARA_TRIPFANS;
            }
        } else if (convite.isRecomendacaoLocal()) {
            return TipoAcao.CONVIDAR_PARA_RECOMENDAR_SOBRE_LOCAL;
        }
        return null;
    }
}
