package br.com.fanaticosporviagens.acaousuario;

import java.util.HashMap;

public class AcaoUsuarioConfig {

    private static AcaoUsuarioConfig config = new AcaoUsuarioConfig();

    public static AcaoUsuarioConfig getInstance() {
        return config;
    }

    private final HashMap<Class<? extends Acao>, GeradorAcaoUsuario<? extends Acao>> geradores = new HashMap<Class<? extends Acao>, GeradorAcaoUsuario<? extends Acao>>();

    private AcaoUsuarioConfig() {
        // this.geradores.put(Avaliacao.class, new GeradorAcaoUsuarioAvaliacao());
        /*this.geradores.put(Dica.class, new GeradorAcaoUsuarioDica());
        this.geradores.put(VotoUtil.class, new GeradorAcaoUsuarioVotoUtil());*/
    }

    public GeradorAcaoUsuario<? extends Acao> getGeradorAcaoUsuario(final Class<? extends Acao> classAcao) {
        return this.geradores.get(classAcao);
    }
}
