package br.com.fanaticosporviagens.acaousuario;

import java.io.Serializable;
import java.util.Date;

import org.joda.time.LocalDateTime;

import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public interface Acao {

    public <T extends AlvoAcao> T getAlvo();

    public Usuario getAutor();

    public LocalDateTime getDataCriacao();

    public Date getDataExclusao();

    public Date getDataGeracaoAcaoAlteracao();

    public Date getDataGeracaoAcaoInclusao();

    public LocalDateTime getDataPublicacao();

    public LocalDateTime getDataUltimaAlteracao();

    public Usuario getDestinatario();

    public <T extends Serializable> T getId();

    public void setDataCriacao(LocalDateTime dataCriacao);

    public void setDataExclusao(Date dataExclusao);

    public void setDataGeracaoAcaoAlteracao(Date dataGeracaoAcao);

    public void setDataGeracaoAcaoInclusao(Date dataGeracaoAcao);

    public void setDataPublicacao(LocalDateTime dataPublicacao);

    public void setDataUltimaAlteracao(LocalDateTime dataUltimaAlteracao);

}
