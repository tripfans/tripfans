package br.com.fanaticosporviagens.acaousuario;

import br.com.fanaticosporviagens.infra.model.entity.IEntity;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * 
 * @author Carlos Nascimento
 * 
 */
public interface AlvoAcao extends IEntity<Long> {

    public String getDescricaoAlvo();

    public Usuario getDestinatario();

}
