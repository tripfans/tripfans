package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioLocalView;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioInteresseLocal extends GerenciadorAcoesUsuarioPadrao<InteresseUsuarioEmLocal> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final StringBuffer sql = new StringBuffer();

        sql.append("SELECT id_usuario_interessado id_usuario, ");
        sql.append("       SUM((CASE WHEN (i.interesse_desejar_ir AND i.interesse_desejar_ir != i.anterior_desejar_ir) ");
        sql.append("                   THEN 1 ELSE 0 ");
        sql.append("               END)) quantidade_deseja_ir, ");

        sql.append("       SUM((CASE WHEN (i.interesse_indica AND i.interesse_indica != i.anterior_indica) ");
        sql.append("                   THEN 1 ELSE 0 ");
        sql.append("               END)) quantidade_indica, ");

        sql.append("       SUM((CASE WHEN (i.interesse_ja_foi AND i.interesse_ja_foi != i.anterior_ja_foi) ");
        sql.append("                   THEN 1 ELSE 0 ");
        sql.append("               END)) quantidade_ja_foi, ");

        sql.append("       SUM((CASE WHEN (i.interesse_local_favorito AND i.interesse_local_favorito != i.anterior_local_favorito) ");
        sql.append("                   THEN 1 ELSE 0 ");
        sql.append("               END)) quantidade_favorito, ");

        sql.append("       SUM((CASE WHEN (i.interesse_imperdivel AND i.interesse_imperdivel != i.anterior_imperdivel) ");
        sql.append("                   THEN 1 ELSE 0 ");
        sql.append("               END)) quantidade_imperdivel ");

        sql.append("  FROM interesse_usuario_em_local i ");
        sql.append(" WHERE id_usuario_interessado IS NOT NULL ");
        sql.append("   AND (data_geracao_acao_inclusao IS NULL OR data_ultima_alteracao >  data_geracao_acao_alteracao ) ");
        sql.append(" GROUP BY id_usuario_interessado");

        final Date dataLimite = new Date();

        final List<InteresseUsuarioLocalView> interesses = this.searchRepository.consultaSQL(sql.toString(), null, InteresseUsuarioLocalView.class);

        for (final InteresseUsuarioLocalView interesse : interesses) {
            // final Object[] obj = (Object[]) interesse;
            // acoesUsuario.add(criarAcaoUsuario(interesse, getTipoAcao(interesse)));

            // marcou X nova(s) cidade que deseja visitar (em seu mapa?)

            if (interesse.getQuantidadeDesejaIr() > 0) {
                inserirAcaoUsuario(interesse.getIdUsuario(), null, TipoAcao.MARCAR_LOCAL_DESEJA_IR, null, InteresseUsuarioEmLocal.class,
                        interesse.getQuantidadeDesejaIr(), new Date());
            }
            if (interesse.getQuantidadeFavorito() > 0) {
                inserirAcaoUsuario(interesse.getIdUsuario(), null, TipoAcao.MARCAR_LOCAL_FAVORITO, null, InteresseUsuarioEmLocal.class,
                        interesse.getQuantidadeFavorito(), new Date());
            }
            if (interesse.getQuantidadeImperdivel() > 0) {
                inserirAcaoUsuario(interesse.getIdUsuario(), null, TipoAcao.MARCAR_LOCAL_IMPERDIVEL, null, InteresseUsuarioEmLocal.class,
                        interesse.getQuantidadeImperdivel(), new Date());
            }
            if (interesse.getQuantidadeIndica() > 0) {
                inserirAcaoUsuario(interesse.getIdUsuario(), null, TipoAcao.MARCAR_LOCAL_INDICA, null, InteresseUsuarioEmLocal.class,
                        interesse.getQuantidadeIndica(), new Date());
            }
            if (interesse.getQuantidadeJaFoi() > 0) {
                inserirAcaoUsuario(interesse.getIdUsuario(), null, TipoAcao.MARCAR_LOCAL_JA_FOI, null, InteresseUsuarioEmLocal.class,
                        interesse.getQuantidadeJaFoi(), new Date());
            }

            atualizarGeracaoAcaoInteresses(interesse.getIdUsuario(), dataLimite);

        }

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

    @Transactional
    private void atualizarGeracaoAcaoInteresses(final Long idUsuario, final Date dataLimite) {

        final StringBuffer update = new StringBuffer();

        update.append("UPDATE interesse_usuario_em_local ");
        update.append("   SET data_geracao_acao_inclusao = :dataGeracaoAcao, ");
        update.append("       data_geracao_acao_alteracao = :dataGeracaoAcao, ");
        update.append("       data_ultima_alteracao = :dataGeracaoAcao, ");

        update.append("       anterior_desejar_ir = interesse_desejar_ir, ");
        update.append("       anterior_local_favorito = interesse_local_favorito, ");
        update.append("       anterior_imperdivel = interesse_imperdivel, ");
        update.append("       anterior_indica = interesse_indica, ");
        update.append("       anterior_ja_foi = interesse_ja_foi ");

        update.append(" WHERE id_usuario_interessado = :idUsuario ");
        update.append("   AND data_criacao < :dataLimite ");

        final Session session = getSession();

        final SQLQuery updateQuery = session.createSQLQuery(update.toString());

        updateQuery.setParameter("idUsuario", idUsuario);
        updateQuery.setParameter("dataGeracaoAcao", new Date());
        updateQuery.setParameter("dataLimite", dataLimite);

        updateQuery.executeUpdate();
    }

}
