package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.VotoUtil;

public class GerenciadorAcoesUsuarioVotoUtil extends GerenciadorAcoesUsuarioPadrao<VotoUtil> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT votoUtil ");
        hql.append("  FROM VotoUtil votoUtil");
        hql.append(" WHERE votoUtil.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND votoUtil.dataExclusao IS NULL");
        // hql.append("   AND votoUtil.dataPublicacao IS NOT NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final List<VotoUtil> votosUteis = this.getSession().createQuery(hql.toString()).list();

        AcaoUsuario acaoUsuario = null;

        for (final VotoUtil votoUtil : votosUteis) {

            // Gerar ação para quem fez indicação
            acaoUsuario = criarAcaoUsuario(votoUtil, getTipoAcaoVotosEnviados(votoUtil));
            acaoUsuario.setAutor(votoUtil.getAutor());
            acaoUsuario.setDestinatario(votoUtil.getDestinatario());
            acoesUsuario.add(acaoUsuario);

            // Gerar ação para quem recebeu a indicação
            acaoUsuario = criarAcaoUsuario(votoUtil, getTipoAcaoVotosRecebidos(votoUtil));
            // inverte os papéis
            acaoUsuario.setAutor(votoUtil.getDestinatario());
            acaoUsuario.setDestinatario(votoUtil.getAutor());
            acoesUsuario.add(acaoUsuario);
        }

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

    private TipoAcao getTipoAcaoVotosEnviados(final VotoUtil votoUtil) {
        if (votoUtil.isVotoAvaliacao()) {
            return TipoAcao.INDICAR_AVALIACAO_COMO_UTIL;
        } else if (votoUtil.isVotoDica()) {
            return TipoAcao.INDICAR_DICA_COMO_UTIL;
        } else if (votoUtil.isVotoRelatoViagem()) {
            return TipoAcao.INDICAR_RELATO_COMO_UTIL;
        }
        if (votoUtil.isVotoResposta()) {
            return TipoAcao.INDICAR_RESPOSTA_COMO_UTIL;
        }
        return null;
    }

    private TipoAcao getTipoAcaoVotosRecebidos(final VotoUtil votoUtil) {
        if (votoUtil.isVotoAvaliacao()) {
            return TipoAcao.RECEBER_INDICACAO_AVALIACAO_UTIL;
        } else if (votoUtil.isVotoDica()) {
            return TipoAcao.RECEBER_INDICACAO_DICA_UTIL;
        } else if (votoUtil.isVotoRelatoViagem()) {
            return TipoAcao.RECEBER_INDICACAO_RELATO_UTIL;
        }
        if (votoUtil.isVotoResposta()) {
            return TipoAcao.RECEBER_INDICACAO_RESPOSTA_UTIL;
        }
        return null;
    }

}
