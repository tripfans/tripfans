package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioAvaliacao extends GerenciadorAcoesUsuarioPadrao<Avaliacao> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT avaliacao ");
        hql.append("  FROM Avaliacao avaliacao");
        hql.append(" WHERE avaliacao.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND avaliacao.dataPublicacao IS NOT NULL");
        hql.append("   AND avaliacao.atividadePlano IS NULL");
        hql.append("   AND avaliacao.dataExclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();

        final List<Avaliacao> avaliacoes = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(avaliacoes, TipoAcao.ESCREVER_AVALIACAO));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        final StringBuffer deleteFotos = new StringBuffer();
        deleteFotos.append("DELETE FROM Foto ");
        deleteFotos.append("WHERE avaliacao = :acao ");

        final Query query = this.getSession().createQuery(deleteFotos.toString()).setParameter("acao", acao);
        query.executeUpdate();
    }

}
