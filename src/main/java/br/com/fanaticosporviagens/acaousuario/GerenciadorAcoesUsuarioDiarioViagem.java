package br.com.fanaticosporviagens.acaousuario;

import java.util.ArrayList;
import java.util.List;

import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.DiarioViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;

public class GerenciadorAcoesUsuarioDiarioViagem extends GerenciadorAcoesUsuarioPadrao<DiarioViagem> {

    @Override
    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT diario ");
        hql.append("  FROM DiarioViagem diario");
        hql.append(" WHERE diario.dataGeracaoAcaoInclusao IS NULL");
        hql.append("   AND diario.dataPublicacao IS NOT NULL");
        hql.append("   AND diario.dataExclusao IS NULL");

        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        final List<DiarioViagem> diariosViagem = this.getSession().createQuery(hql.toString()).list();

        acoesUsuario.addAll(criarAcoesUsuario(diariosViagem, TipoAcao.PUBLICAR_DIARIO_VIAGEM));

        return acoesUsuario;
    }

    @Override
    public void excluirAcoesEspecificas(final Acao acao) {
        // TODO Auto-generated method stub

    }

}
