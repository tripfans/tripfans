package br.com.fanaticosporviagens.acaousuario;

/**
 * Objetivo:
 * O módulo de ações do usuário tem o objetivo de efetuar o loga de ações do usuário e pontuar as ações para
 * fazer o ranking dos melhores usuários.
 * 
 * Funciamento do mecanismo de log de ações do usuário:
 * As entidades que geram log de ações do usuário devem implementar a interface AcaoPontuavel e possuir
 * um gerador de ações (@see br.com.fanaticosporviagens.acaousuario.GeradorAcaoUsuario).
 * O gerador de ações deve ser configurado no método construtor de @see br.com.fanaticosporviagens.acaousuario.AcaoUsuarioConfig
 * Exemplo: this.geradores.put(VotoUtil.class, new GeradorAcaoUsuarioVotoUtil());
 * 
 * Sobre o gerador de ações (@see br.com.fanaticosporviagens.acaousuario.GeradorAcaoUsuario):
 * O gerador de ações deve implementar a interface @see br.com.fanaticosporviagens.acaousuario.GeradorAcaoUsuario
 * ou herdar de @see br.com.fanaticosporviagens.acaousuario.GeradorAcaoUsuarioEmpty
 * 
 */
public interface AcaoPontuavel extends Acao {

}
