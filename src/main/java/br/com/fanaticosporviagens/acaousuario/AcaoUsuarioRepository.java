package br.com.fanaticosporviagens.acaousuario;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PontuacaoUsuarioLocal;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.repository.UsuarioRepository;

@Repository
public class AcaoUsuarioRepository extends GenericCRUDRepository<AcaoUsuario, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<PontuacaoUsuarioLocal> consultarPontuacaoUsuarioLocal(final Local local, final Integer tamanhoAmostra) {
        final ArrayList<PontuacaoUsuarioLocal> pontuacoes = new ArrayList<PontuacaoUsuarioLocal>();

        final StringBuilder hql = new StringBuilder();
        hql.append("select acaoUsuario.autor.id ");
        hql.append("     , acaoUsuario.autor.displayName ");
        hql.append("     , sum(acaoUsuario.pontos) ");
        hql.append("  from br.com.fanaticosporviagens.model.entity.AcaoUsuario acaoUsuario ");
        if (local.isTipoLocalEnderecavel()) {
            hql.append(" where acaoUsuario.localEnderecavel = :local ");
        } else if (local.isTipoLocalInteresseTuristico()) {
            hql.append(" where acaoUsuario.localInteresseTuristico = :local ");
        } else if (local.isTipoCidade()) {
            hql.append(" where acaoUsuario.localCidade = :local ");
        } else if (local.isTipoEstado()) {
            hql.append(" where acaoUsuario.localEstado = :local ");
        } else if (local.isTipoPais()) {
            hql.append(" where acaoUsuario.localPais = :local ");
        } else if (local.isTipoContinente()) {
            hql.append(" where acaoUsuario.localContinente = :local ");
        }
        hql.append(" group by acaoUsuario.autor.id, acaoUsuario.autor.displayName ");
        hql.append(" order by sum(acaoUsuario.pontos) desc, acaoUsuario.autor.displayName ");

        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("local", local);

        final List<Object> itens = this.searchRepository.consultaHQL(hql.toString(), params, 0, tamanhoAmostra);

        final HashMap<Long, PontuacaoUsuarioLocal> mapPontuacoes = new HashMap<Long, PontuacaoUsuarioLocal>();
        final Long[] idsUsuarios = new Long[itens.size()];
        int indiceId = 0;
        for (final Object item : itens) {
            final Object[] resultado = (Object[]) item;
            final Long idUsuario = (Long) resultado[0];
            final BigDecimal pontosUsuario = (BigDecimal) resultado[2];

            final PontuacaoUsuarioLocal pontuacao = new PontuacaoUsuarioLocal();
            pontuacao.setPontos(pontosUsuario);

            idsUsuarios[indiceId] = idUsuario;
            indiceId++;
            pontuacoes.add(pontuacao);
            mapPontuacoes.put(idUsuario, pontuacao);
        }

        if (!itens.isEmpty()) {
            final List<Usuario> usuarios = this.usuarioRepository.consultarPorIds(Usuario.class, idsUsuarios);
            for (final Usuario usuario : usuarios) {
                mapPontuacoes.get(usuario.getId()).setUsuario(usuario);
            }
        }

        return pontuacoes;

        // final StringBuilder hql = new StringBuilder();
        // hql.append("select new br.com.fanaticosporviagens.model.entity.PontuacaoUsuarioLocal( ");
        // hql.append("       acaoUsuario.local ");
        // hql.append("     , acaoUsuario.usuario ");
        // hql.append("     , new java.math.BigDecimal(sum(acaoUsuario.pontos)) ");
        // hql.append("     ) ");
        // hql.append("  from br.com.fanaticosporviagens.model.entity.AcaoUsuario acaoUsuario ");
        // hql.append(" where acaoUsuario.local = :local ");
        // hql.append(" group by acaoUsuario.local, acaoUsuario.usuario ");
        // hql.append(" order by sum(acaoUsuario.pontos) desc ");
        //
        // final HashMap<String, Object> params = new HashMap<String, Object>();
        // params.put("local", local);
        //
        // return this.searchRepository.consultaHQL(hql.toString(), params, 0, tamanhoAmostra);
    }

}
