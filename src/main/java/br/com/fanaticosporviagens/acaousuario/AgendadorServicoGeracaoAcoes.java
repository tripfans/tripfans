package br.com.fanaticosporviagens.acaousuario;

import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.util.TripFansEnviroment;

@Component
public class AgendadorServicoGeracaoAcoes {

    /*@Autowired
    private List<GerenciadorAcoesUsuario<?>> gerenciadoresAcoesUsuario;*/

    @Autowired
    private TripFansEnviroment tripFansEnviroment;

    @Scheduled(fixedDelay = 60000)
    // @Async
    public void agendar() {

        HttpURLConnection conexaoHttp = null;

        // By Carlos
        // Gato para funcionar o envio de email.
        // Não consegui fazer funcionar o EmailService fora do RequestContext, pois este é necessário para geração dos emails gerados pelos templates
        // Freemarker (.ftl)
        try {
            final URL urlGeradorAcoes = new URL(this.tripFansEnviroment.getServerUrl() + "/geradorAcoes/executar");
            conexaoHttp = (HttpURLConnection) urlGeradorAcoes.openConnection();
            conexaoHttp.setRequestMethod("GET");
            conexaoHttp.connect();
            conexaoHttp.getContent();
        } catch (final Exception e) {
        } finally {
            conexaoHttp.disconnect();
            conexaoHttp = null;
        }

        /*for (final GerenciadorAcoesUsuario<?> gerenciador : this.gerenciadoresAcoesUsuario) {
            gerenciador.executar();
        }*/
    }
}
