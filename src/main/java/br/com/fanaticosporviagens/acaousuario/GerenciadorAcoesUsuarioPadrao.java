package br.com.fanaticosporviagens.acaousuario;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.hibernate.CacheMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.infra.util.HibernateUtil;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;

public abstract class GerenciadorAcoesUsuarioPadrao<T extends Acao> implements GerenciadorAcoesUsuario<T> {

    protected GenericSearchRepository searchRepository;

    private AcaoUsuarioService acaoUsuarioService;

    private Class<T> classeGeradora;

    private EntityManager entityManager;

    private EntityManagerFactory entityManagerFactory;

    private NotificacaoService notificacaoService;

    private Session session;

    private JpaTransactionManager transactionManager;

    @SuppressWarnings("unchecked")
    public GerenciadorAcoesUsuarioPadrao() {
        try {
            this.classeGeradora = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        } catch (final Exception e) {
        }
    }

    public List<AcaoUsuario> consultarAcoesAlteracaoPendentes() {
        return null;
    }

    public List<T> consultarAcoesExclusaoPendentes() {
        return null;
    }

    public List<AcaoUsuario> consultarAcoesInclusaoPendentes() {
        return null;
    }

    @Transactional
    public final void excluirAcoes() {
        openSession();

        final String nomeDaClasse = getClasseGeradora().getSimpleName();

        final Session session = getSession();

        final StringBuffer hql = new StringBuffer();
        hql.append("SELECT objeto ");
        hql.append("  FROM " + nomeDaClasse + " objeto");
        hql.append(" WHERE objeto.dataExclusao IS NOT NULL");

        List<Acao> acoes = null;

        final Transaction transaction = session.getTransaction();
        try {
            // acoes = this.getEntityManager().createQuery(hql.toString()).getResultList();

            if (!transaction.isActive()) {
                transaction.begin();
            }
            acoes = session.createQuery(hql.toString()).list();

            for (final Acao acao : acoes) {
                // Excluir Notificações
                StringBuffer delete = new StringBuffer();
                delete.append("DELETE FROM Notificacao notificacao ");
                delete.append(" WHERE notificacao.acaoPrincipal = :acaoPrincipal");

                Query query = session.createQuery(delete.toString());
                query.setParameter("acaoPrincipal", acao);
                query.executeUpdate();

                excluirAcoesEspecificas(acao);

                // Excluir Ações

                delete = new StringBuffer();
                delete.append("DELETE FROM AcaoUsuario acaoUsuario ");
                delete.append(" WHERE acaoUsuario.acaoPrincipal = :acaoPrincipal");

                query = session.createQuery(delete.toString());
                query.setParameter("acaoPrincipal", acao);
                query.executeUpdate();

                // Excluir objetos
                session.delete(session.merge(acao));

                // Excluir filhos???
            }

            transaction.commit();

        } catch (final Exception e) {
            e.printStackTrace();
            transaction.rollback();
        } finally {
            closeSession();
        }

        // transaction.commit();

    }

    public abstract void excluirAcoesEspecificas(Acao acao);

    @Override
    @Transactional
    public final void executar() {
        final Transaction transaction = null;
        try {
            // openSession();
            // transaction = this.session.getTransaction();
            // if (!transaction.isActive()) {
            // transaction.begin();
            // }
            gerarAcoesInclusao();
            gerarAcoesAlteracao();
            excluirAcoes();
            // transaction.commit();
        } catch (final Exception e) {

            e.printStackTrace();
            // transaction.rollback();
        } finally {
            // closeSession();
        }
    }

    public AcaoUsuarioService getAcaoUsuarioService() {
        return this.acaoUsuarioService;
    }

    public Class<T> getClasseGeradora() {
        return this.classeGeradora;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return this.entityManagerFactory;
    }

    public NotificacaoService getNotificacaoService() {
        return this.notificacaoService;
    }

    public GenericSearchRepository getSearchRepository() {
        return this.searchRepository;
    }

    public JpaTransactionManager getTransactionManager() {
        return this.transactionManager;
    }

    @Autowired
    public void setAcaoUsuarioService(final AcaoUsuarioService acaoUsuarioService) {
        this.acaoUsuarioService = acaoUsuarioService;
    }

    @PersistenceContext
    public void setEntityManager(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Autowired
    public void setEntityManagerFactory(final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Autowired
    public void setNotificacaoService(final NotificacaoService notificacaoService) {
        this.notificacaoService = notificacaoService;
    }

    @Autowired
    public void setSearchRepository(final GenericSearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    @Autowired
    public void setTransactionManager(final JpaTransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    protected AcaoUsuario criarAcaoUsuario(final T acao, final TipoAcao tipoAcao) {
        return new AcaoUsuario(acao, tipoAcao);
    }

    protected List<AcaoUsuario> criarAcoesUsuario(final List<T> acoes, final TipoAcao tipoAcao) {
        final List<AcaoUsuario> acoesUsuario = new ArrayList<AcaoUsuario>();
        for (final T acao : acoes) {
            acoesUsuario.add(criarAcaoUsuario(acao, tipoAcao));
        }
        return acoesUsuario;
    }

    protected Session getSession() {
        // if (this.session == null || !this.session.isOpen()) {
        openSession();
        // }
        // Verificar se isso ira gerar algum problema
        // Foi colocado para limpar a session, pois o Hibernate estava fazendo cache de alguns objetos e estava impedindo o correto funcionamento da
        // geracao das acoes
        this.session.clear();
        return this.session;
    }

    @Transactional
    protected <T extends Acao> void inserirAcaoUsuario(final Long idAutor, final Long idDestinatario, final TipoAcao tipoAcao, final Long idObjeto,
            final Class<T> classeObjeto, final Integer quantidade, final Date dataObjeto) {

        /*getEntityManager().getTransaction().begin();*/
        final Session session = getSession();
        final Transaction transaction = session.getTransaction();

        try {

            if (!transaction.isActive()) {
                transaction.begin();
            }

            final StringBuffer insert = new StringBuffer();
            insert.append("INSERT INTO acao_usuario ");
            insert.append("            (");
            insert.append("            id_acao_usuario,");
            insert.append("            id_usuario_autor,");
            insert.append("            id_usuario_destinatario,");
            insert.append("            id_tipo_acao,");
            insert.append("            tipo_acao_principal,");
            insert.append("            id_acao_principal,");
            insert.append("            visivel,");
            insert.append("            publica,");
            insert.append("            publica_na_home,");
            insert.append("            ponto,");
            insert.append("            quantidade,");
            insert.append("            data_acao,");
            insert.append("            data_criacao");
            insert.append("            )");
            insert.append("     VALUES (nextval('seq_acao_usuario'), :idAutor, :idDestinatario, :idTipoAcao, :tipoAcaoPrincipal, :idAcaoPrincipal, :visivel, :publica, :publicaNaHome, :ponto, :quantidade, :dataAcao, :dataCriacao) ");

            final SQLQuery insertQuery = this.session.createSQLQuery(insert.toString());

            insertQuery.setParameter("idAutor", idAutor, StandardBasicTypes.LONG);
            insertQuery.setParameter("idDestinatario", idDestinatario, StandardBasicTypes.LONG);
            insertQuery.setParameter("idTipoAcao", tipoAcao.getCodigo(), StandardBasicTypes.INTEGER);
            insertQuery.setParameter("tipoAcaoPrincipal", classeObjeto.getSimpleName(), StandardBasicTypes.STRING);
            insertQuery.setParameter("idAcaoPrincipal", idObjeto, StandardBasicTypes.LONG);
            insertQuery.setParameter("visivel", tipoAcao.isVisivel(), StandardBasicTypes.BOOLEAN);
            insertQuery.setParameter("publica", tipoAcao.isPublicavel(), StandardBasicTypes.BOOLEAN);
            insertQuery.setParameter("publicaNaHome", tipoAcao.isPublicavelNaHome(), StandardBasicTypes.BOOLEAN);
            insertQuery.setParameter("ponto", tipoAcao.getPontos(), StandardBasicTypes.BIG_DECIMAL);
            insertQuery.setParameter("quantidade", quantidade, StandardBasicTypes.INTEGER);
            insertQuery.setParameter("dataAcao", dataObjeto, StandardBasicTypes.TIMESTAMP);
            insertQuery.setParameter("dataCriacao", new Date(), StandardBasicTypes.TIMESTAMP);

            insertQuery.executeUpdate();

            final StringBuffer update = new StringBuffer();

            update.append("UPDATE " + classeObjeto.getSimpleName() + " entidade");
            update.append("   SET entidade.dataGeracaoAcaoInclusao = :dataGeracaoAcao ");
            update.append(" WHERE entidade.id = :id ");
            update.append("   AND entidade.dataGeracaoAcaoInclusao IS NULL ");

            final Query updateQuery = this.session.createQuery(update.toString());

            updateQuery.setParameter("id", idObjeto, StandardBasicTypes.LONG);
            updateQuery.setParameter("dataGeracaoAcao", new Date(), StandardBasicTypes.TIMESTAMP);

            updateQuery.executeUpdate();

            transaction.commit();

        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            // session.close();
        }

    }

    private void closeSession() {
        if (this.session != null && this.session.isOpen()) {
            this.session.clear();
            this.session.close();
        }
    }

    @Transactional
    private final void gerarAcoesAlteracao() {
        /*final DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName(this.getClass().getSimpleName() + "_gerarAcoesAlteracao");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        final TransactionStatus ts = this.getTransactionManager().getTransaction(def);*/
        openSession();
        incluirAcoesUsuario(consultarAcoesAlteracaoPendentes(), true);
        closeSession();

        // this.getTransactionManager().commit(ts);
    }

    @Transactional
    private final void gerarAcoesInclusao() {
        /*final DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        def.setName(this.getClass().getSimpleName() + "_gerarAcoesInclusao");
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        final TransactionStatus ts = this.getTransactionManager().getTransaction(def);*/

        openSession();
        incluirAcoesUsuario(consultarAcoesInclusaoPendentes(), false);
        closeSession();

        // this.getTransactionManager().commit(ts);
    }

    @Transactional
    private final void incluirAcaoUsuario(AcaoUsuario acaoUsuario, final boolean alteracao) {
        this.getAcaoUsuarioService().incluir(acaoUsuario);
        HibernateUtil.getInstance().derreferenciarProxy(acaoUsuario.getAcao());

        final Class<? extends Acao> classe = acaoUsuario.getAcao().getClass();

        final Session session = this.getSession();
        final Acao acao = (Acao) session.load(classe, acaoUsuario.getAcao().getId());
        if (alteracao) {
            acao.setDataGeracaoAcaoAlteracao(new Date());
        } else {
            acao.setDataGeracaoAcaoInclusao(new Date());
        }
        final Transaction transaction = session.getTransaction();
        try {
            if (!transaction.isActive()) {
                transaction.begin();
            }
            session.merge(acao);
            // System.out.println(acao.getDataGeracaoAcaoAlteracao());
            transaction.commit();

            // Recuperando novamente para evitar LAZY Exception
            session.refresh(acaoUsuario);
            acaoUsuario = (AcaoUsuario) session.get(AcaoUsuario.class, acaoUsuario.getId());
            // Notificar
            if (acaoUsuario.getTipoAcao().isNotificavel()) {
                this.getNotificacaoService().notificar(acaoUsuario);
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            // session.close();
        }

    }

    @Transactional
    private final void incluirAcoesUsuario(final List<AcaoUsuario> acoesUsuario, final boolean alteracao) {
        if (acoesUsuario != null) {
            for (final AcaoUsuario acaoUsuario : acoesUsuario) {
                incluirAcaoUsuario(acaoUsuario, alteracao);
            }
        }
    }

    private void openSession() {
        if (this.session != null) {
            if (this.session.isOpen()) {
                return;
            }
            closeSession();
        }
        this.entityManager = this.getEntityManagerFactory().createEntityManager();
        this.session = (Session) this.entityManager.getDelegate();

        // TODO ver o impacto disso. Ver se afetara outras partes da aplicacao
        this.session.setCacheMode(CacheMode.IGNORE);
        this.session.clear();
    }

}
