package br.com.fanaticosporviagens.social;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;

@Service
public class UsuarioConectadoRedeSocialService {

    @Autowired
    private GenericSearchRepository searchRepository;

    public boolean usuarioConectadoProvedor(final String userId, final String provider) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("userid", userId);
        params.put("provider", provider);
        final String id = this.searchRepository.queryByNamedNativeQueryUniqueResult("UsuarioConectadoService.usuarioConectadoProvedor", params);
        return id != null;
    }
}
