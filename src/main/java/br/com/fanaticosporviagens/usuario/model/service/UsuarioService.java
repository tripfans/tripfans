package br.com.fanaticosporviagens.usuario.model.service;

import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.FacebookProfile;
import org.springframework.social.facebook.api.Page;
import org.springframework.social.foursquare.api.Foursquare;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.plus.Person;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import br.com.fanaticosporviagens.acaousuario.AcaoUsuarioService;
import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.exception.ServiceException;
import br.com.fanaticosporviagens.infra.exception.UsernameAlreadyInUseException;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.EmailTemplate;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.ConfiguracaoNotificacoesPorEmail;
import br.com.fanaticosporviagens.model.entity.ContaServicoExterno;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.FormaCadastroUsuario;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.InteressesViagem;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.PreferenciasUsuario;
import br.com.fanaticosporviagens.model.entity.PreferenciasViagem;
import br.com.fanaticosporviagens.model.entity.TipoAcao;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.model.entity.TipoServicoExterno;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.ConfiguracaoNotificacoesPorEmailService;
import br.com.fanaticosporviagens.usuario.ConfiguracoesGeraisEmailUsuarioView;
import br.com.fanaticosporviagens.usuario.InteressesPreferenciasViagemView;
import br.com.fanaticosporviagens.usuario.InteressesViagemService;
import br.com.fanaticosporviagens.usuario.PreferenciasUsuarioService;
import br.com.fanaticosporviagens.usuario.PreferenciasViagemService;
import br.com.fanaticosporviagens.usuario.form.CadastroForm;
import br.com.fanaticosporviagens.usuario.model.repository.UsuarioRepository;
import br.com.fanaticosporviagens.usuario.view.PerfilUsuarioView;
import br.com.fanaticosporviagens.usuario.view.UsuarioConviteFacebookView;
import br.com.fanaticosporviagens.util.SorteadorItens;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UsuarioService extends GenericCRUDService<Usuario, Long> implements UserDetailsService, Serializable {

    private static final long serialVersionUID = 7474153545288017049L;

    @Autowired
    private AcaoUsuarioService acaoUsuarioService;

    @Autowired
    private AmizadeService amizadeService;

    @Resource
    private ProviderManager authenticationManager;

    @Autowired
    private ConfiguracaoNotificacoesPorEmailService configuracaoNotificacoesPorEmailService;

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private InteressesViagemService interessesViagemService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private LocalService localService;

    private final Log log = LogFactory.getLog(UsuarioService.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PreferenciasUsuarioService preferenciasUsuarioService;

    @Autowired
    private PreferenciasViagemService preferenciasViagemService;

    @Autowired
    private SorteadorItens sorteadorItens;

    @Autowired
    private UrlPathGeneratorService urlPathGeneratorService;

    @Autowired
    private UsuarioRepository usuarioRepository;

    private void aceitarConvitePorIdConvite(final Long idConvite, final Usuario novoUsuario) {
        final Convite convite = this.conviteService.consultarPorId(idConvite);
        if (convite != null) {
            convite.setConvidado(novoUsuario);
            convite.aceitar();
            // se o convite for para o Tripfans, criar um de amizade se não forem amigos pelo facebook
            if (convite.isTripfans()) {
                // verificar se o convidado não é amigo do convidante no facebook
                if (!(convite.getIdFacebookConvidado() != null && this.amizadeService.saoAmigosNoFacebook(convite.getAutor().getId(),
                        convite.getIdFacebookConvidado()))) {
                    // caso nao forem amigos, criar um convite de amizade
                    final Convite novoConvite = new Convite(convite.getAutor(), novoUsuario).paraAmizade();
                    // Setando a data de geracao da acao para evitar que o gerador envie um email desnecessario
                    novoConvite.setDataGeracaoAcaoInclusao(new Date());
                    this.conviteService.incluirConvite(novoConvite, true);
                }
            }
            this.conviteService.salvar(convite);
        }
    }

    private Convite aceitarConvitePorIdFacebookConvidado(final Long idUsuarioConvidou, final String idFacebookConvidado) {
        final Convite convite = this.conviteService.consultarConviteTripFansPendenteViaFacebook(idUsuarioConvidou, idFacebookConvidado);
        if (convite != null) {
            convite.aceitar();
            this.conviteService.salvar(convite);
            return convite;
        }
        return null;
    }

    private void aceitarConvitePorIdUsuarioConvidou(final Long idUsuarioConvidou, final Usuario novoUsuario) {
        if (novoUsuario.getIdFacebook() != null) {
            final Convite convite = aceitarConvitePorIdFacebookConvidado(idUsuarioConvidou, novoUsuario.getIdFacebook());
            if (convite != null) {
                convite.setConvidado(novoUsuario);
                this.conviteService.salvar(convite);
            }
        }
    }

    @Transactional
    public void alterarSenha(final Usuario usuario, final String senhaAtual, final String senhaNova) {
        String senhaAtualCriptografada = null;
        if (senhaAtual != null) {
            senhaAtualCriptografada = this.criptografaSenha(senhaAtual, usuario.getPasswordSalt());
        }
        final String senhaNovaCriptografada = this.criptografaSenha(senhaNova, usuario.getPasswordSalt());

        if (senhaAtualCriptografada == null || usuario.getPassword().equals(senhaAtualCriptografada)) {
            usuario.setPassword(senhaNovaCriptografada);
            this.alterar(usuario);
        } else {
            throw new ServiceException("A senha atual está incorreta!");
        }
    }

    @Transactional
    public Usuario alterarSenhaAposRecuperacao(final String username, final String codigoConfirmacao, final String senhaNova) {
        if (confirmarCodigoRecuperacaoSenha(username, codigoConfirmacao)) {
            final Usuario usuario = consultarUsuarioPorUsername(username);
            final String senhaNovaCriptografada = this.criptografaSenha(senhaNova, usuario.getPasswordSalt());
            usuario.setPassword(senhaNovaCriptografada);
            usuario.setCodigoRecuperacaoSenha(null);
            this.alterar(usuario);
            return usuario;
        }
        return null;
    }

    private Usuario associarContaDeFacebookProfile(final Usuario usuario, final Connection<Facebook> connection, final Long idUsuarioConvidou) {
        final FacebookProfile profile = connection.getApi().userOperations().getUserProfile();
        usuario.setConectadoFacebook(true);
        usuario.setIdFacebook(profile.getId());
        if (!usuario.isEnabled()) {
            // consultar por email, pois, pode existir um outro usuário com o mesmo e-mail, ativo, mas que não estava conectado ao Facebook ainda
            final Map<String, Object> params = new HashMap<String, Object>();
            params.put("email", connection.fetchUserProfile().getEmail());
            final List<Usuario> usuariosAtivos = this.searchRepository.consultarPorNamedQuery("Usuario.consultarUsuarioAtivoEmail", params,
                    Usuario.class);
            if (!usuariosAtivos.isEmpty()) {
                final Usuario usuarioAtivo = usuariosAtivos.get(0);
                trocarReferenciasUsuarioImportadoPorUsuarioAtual(usuario, usuarioAtivo);
                excluirUsuarioImportado(usuario);
                usuarioAtivo.setConectadoFacebook(true);
                usuarioAtivo.setIdFacebook(profile.getId());
                this.alterar(usuarioAtivo);
                return usuarioAtivo;
            } else {
                usuario.setDataCadastro(new Date());
                usuario.setDataConfirmacao(new Date());
                criarContaDeFacebookProfile(usuario, connection);
                this.urlPathGeneratorService.generate(usuario);
                usuario.setConectadoFacebook(true);
                usuario.setIdFacebook(profile.getId());
                if (idUsuarioConvidou != null) {
                    usuario.setIdUsuarioConvidante(idUsuarioConvidou);
                    aceitarConvitePorIdUsuarioConvidou(idUsuarioConvidou, usuario);
                }
            }
        }
        this.alterar(usuario);
        return usuario;
    }

    private void associarContaDeGoogleProfile(final Usuario usuario, final Connection<Google> connection) {
        // final Person profile = connection.getApi().personOperations().getGoogleProfile();
        final Person profile = connection.getApi().plusOperations().getGoogleProfile();
        usuario.setConectadoGoogle(true);
        usuario.setIdGoogle(profile.getId());
        this.alterar(usuario);
    }

    private void associarContaDeTwitterProfile(final Usuario usuario, final Connection<Twitter> connection) {
        final TwitterProfile profile = connection.getApi().userOperations().getUserProfile();
        usuario.setConectadoTwitter(true);
        usuario.setIdTwitter(profile.getId());
        this.alterar(usuario);
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public Usuario associarContaTripFansComContaDeExternaServiceProfile(final Usuario usuario, final Connection<?> connection,
            final Long idUsuarioConvidou) {
        if (connection.getApi() instanceof Facebook) {
            final Usuario usuarioAssociado = this.associarContaDeFacebookProfile(usuario, (Connection<Facebook>) connection, idUsuarioConvidou);
            return usuarioAssociado;
        } else if (connection.getApi() instanceof Google) {
            this.associarContaDeGoogleProfile(usuario, (Connection<Google>) connection);
        } else if (connection.getApi() instanceof Twitter) {
            this.associarContaDeTwitterProfile(usuario, (Connection<Twitter>) connection);
        }
        return usuario;
    }

    @Transactional
    public void atualizarUltimaVerificacaoNotificacoes(final Usuario usuario) {
        usuario.setDataUltimaVerificacaoNotificacoes(new LocalDateTime());
        this.salvar(usuario);
    }

    @Transactional
    public boolean autenticar(final String username, final String password) {
        final UsernamePasswordAuthenticationToken usernameAndPassword = new UsernamePasswordAuthenticationToken(username, password);
        return this.autenticar(usernameAndPassword);
    }

    private boolean autenticar(final UsernamePasswordAuthenticationToken usernameAndPassword) {
        final Authentication authentication = this.authenticationManager.authenticate(usernameAndPassword);
        // Place the new Authentication object in the security context.
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return true;
    }

    @Transactional
    public Usuario autenticarExternalProfile(final String username) {
        final Usuario usuario = (Usuario) this.loadUserByUsername(username);

        if (!usuario.isConfirmado()) {
            usuario.setDataConfirmacao(new Date());
            this.alterar(usuario);
        }

        final UsernamePasswordAuthenticationToken usernameAndPassword = new UsernamePasswordAuthenticationToken(usuario.getUsername(), null, null);
        usernameAndPassword.setDetails(usuario);
        SecurityContextHolder.getContext().setAuthentication(usernameAndPassword);

        return usuario;
    }

    @Transactional
    public void completarCadastro(final Usuario usuario) {
        usuario.setDataComplementoCadastro(new Date());
        alterar(usuario);
    }

    @Transactional
    public void completarDadosBasicos(final Usuario usuario, final String nome, final String sobreNome, final String genero,
            final Boolean exibirGenero, final Cidade cidade) {
        usuario.setPrimeiroNome(nome);
        usuario.setUltimoNome(sobreNome);
        usuario.setDisplayName(nome + " " + sobreNome);
        usuario.setGenero(genero);
        if (genero.equalsIgnoreCase("F")) {
            // TODO modificar isso; no login, vai ter que informar o sexo; esse código não será mais necessário
            // usuario.getFoto().setNome("blank-female.jpg");
            // // TODO encontrar uma forma melhor de fazer isso
            // usuario.getFoto().setUrlExterna(
            // "http://" + this.environment.getProperty(CommonAppConstants.APPLICATION_DOMAIN) + "/fanaticosporviagens/resources/images/"
            // + usuario.getFoto().getNome());
        }
        usuario.setExibirGenero(exibirGenero);
        usuario.setDataComplementoCadastro(new Date());

        this.alterar(usuario);
    }

    private void completarDadosBasicosDeExternalProfile(final Usuario usuario, String cidadeNatal, String cidadeAtual, final String locale) {

        if (cidadeNatal != null && cidadeNatal.contains(",")) {
            final String[] cidadePais = cidadeNatal.split(",");
            cidadeNatal = cidadePais[0];
        }
        if (cidadeAtual != null && cidadeAtual.contains(",")) {
            final String[] cidadePais = cidadeAtual.split(",");
            cidadeAtual = cidadePais[0];
        }

        // usuario.setCidadeNatal(this.localService.consultarCidadePorNome(cidadeNatal, null));
        // usuario.setCidadeResidencia(this.localService.consultarCidadePorNome(cidadeAtual, null));
        usuario.setLocale(locale);
    }

    @Transactional
    public void conectarProvedorExterno(final Usuario usuario, final Connection<?> connection) {
        if (connection.getApi() instanceof Facebook) {
            usuario.setConectadoFacebook(true);
            final String idFacebook = connection.createData().getProviderUserId();
            final Usuario usuarioImportado = consultarUsuarioPorIdFacebook(idFacebook);
            if (usuarioImportado != null) {
                trocarReferenciasUsuarioImportadoPorUsuarioAtual(usuarioImportado, usuario);
                excluirUsuarioImportado(usuarioImportado);
            }
            usuario.setIdFacebook(idFacebook);
        }
        if (connection.getApi() instanceof Foursquare) {
            usuario.setConectadoFoursquare(true);
            usuario.setIdFoursquare(connection.createData().getProviderUserId());
        }
        if (connection.getApi() instanceof Google) {
            usuario.setConectadoGoogle(true);
            usuario.setIdGoogle(connection.createData().getProviderUserId());
        }
        if (connection.getApi() instanceof Twitter) {
            usuario.setConectadoTwitter(true);
            usuario.setIdTwitter(new Long(connection.createData().getProviderUserId()));
        }
        this.alterar(usuario);
    }

    private void configurarFotoFacebook(final Usuario usuario, final String idFacebook) {
        final Foto foto = new Foto();
        foto.setUsuario(usuario);
        foto.setAutor(usuario);
        foto.setAnonima(false);
        foto.setTipoArmazenamento(TipoArmazenamentoFoto.EXTERNO);
        foto.setUrlExternaSmall(String.format(TripFansAppConstants.FACEBOOK_PROFILE_SMALL_PHOTO, idFacebook));
        foto.setUrlExternaAlbum(String.format(TripFansAppConstants.FACEBOOK_PROFILE_LARGE_PHOTO, idFacebook));
        foto.setUrlExternaBig(String.format(TripFansAppConstants.FACEBOOK_PROFILE_LARGE_PHOTO, idFacebook));
    }

    private void configurarFotoPerfil(final Usuario usuario) {
        final Foto foto = new Foto();
        foto.setAutor(usuario);
        foto.setUsuario(usuario);
        foto.setTipoArmazenamento(TipoArmazenamentoFoto.LOCAL);
        foto.setAnonima(true);
        // Impede a geracao automatica da Acao TipoAcao.ALTERAR_FOTO_PERFIL
        foto.setDataGeracaoAcaoAlteracao(new Date());
    }

    private Usuario configurarNovoUsuario(final CadastroForm cadastroForm) {
        final Usuario usuario = new Usuario();
        usuario.setUsername(cadastroForm.getUsername());
        usuario.setCidadeResidencia(cadastroForm.getCidade());
        usuario.setDisplayName(cadastroForm.getDisplayName());
        usuario.setPrimeiroNome(cadastroForm.getDisplayName());
        usuario.setEmail(cadastroForm.getUsername());
        usuario.setGenero(cadastroForm.getGenero());
        usuario.setAtivo(true);
        usuario.setBloqueado(false);
        usuario.setDataCadastro(new Date());
        usuario.setPassword(this.criptografaSenha(cadastroForm.getPassword(), usuario.getPasswordSalt()));
        usuario.setFormaCadastro(FormaCadastroUsuario.TRIPFANS);
        // Impede a geracao automatica da Acao TipoAcao.ALTERAR_INFORMACOES_BASICAS_PERFIL
        usuario.setDataGeracaoAcaoAlteracao(new Date());
        this.urlPathGeneratorService.generate(usuario);
        return usuario;
    }

    private Usuario configurarNovoUsuario(final String username) {
        final Usuario usuario = new Usuario();
        usuario.setUsername(username);
        usuario.setEmail(username);
        usuario.setAtivo(true);
        usuario.setBloqueado(false);
        usuario.setDataCadastro(new Date());
        // Impede a geracao automatica da Acao TipoAcao.ALTERAR_INFORMACOES_BASICAS_PERFIL
        usuario.setDataGeracaoAcaoAlteracao(new Date());
        this.urlPathGeneratorService.generate(usuario);
        return usuario;
    }

    @Transactional
    public boolean confirmarCodigoRecuperacaoSenha(final String username, final String codigoConfirmacao) {
        try {
            final Usuario usuario = consultarUsuarioPorUsername(username);
            if (usuario.getCodigoRecuperacaoSenha() != null && usuario.getCodigoRecuperacaoSenha().equals(codigoConfirmacao)) {
                return true;
            }
        } catch (final Exception e) {
            throw new BusinessException("Erro ao confirmar o código do de recuperação da senha.");
        }
        return false;
    }

    @Transactional
    public void confirmarEmailUsuario(final Long userId, final String codigoConfirmacao) {
        final Usuario usuario = this.consultarPorId(userId);

        if (usuario.isConfirmado()) {
            // TODO Mensagem informando que ja foi confirmado
            return;
        }
        if (codigoConfirmacao != null && codigoConfirmacao.equals(this.criptografarTexto("" + usuario.getUsername().hashCode()))) {
            usuario.setDataConfirmacao(new Date());
            this.alterar(usuario);
        } else {
            // TODO
            throw new RuntimeException("Erro ao validar. O código de confirmação está incorreto!");
        }
    }

    public ConfiguracaoNotificacoesPorEmail consultarConfiguracoesEmails(final Usuario usuario) {
        return this.configuracaoNotificacoesPorEmailService.consultarConfiguracoesEmailsDoUsuario(usuario);
    }

    /**
     * Realiza uma consulta para verificar quais dos emails informados como parametros, já possuem cadastro no TripFans.
     */
    public List<String> consultarEmailsJaCadastrados(final List<String> listaEmails) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("listaEmails", listaEmails);
        return this.searchRepository.consultarPorNamedQuery("Usuario.consultarEmailsJaCadastrados", parametros, String.class);
    }

    /**
     * Recupera informações relativas ao Perfil do usuário
     *
     * @param idUsuario
     */
    public PerfilUsuarioView consultarPerfilUsuario(final Usuario usuarioDoPerfil) {
        final PerfilUsuarioView perfilView = new PerfilUsuarioView();
        if (this.getUsuarioAutenticado() != null) {
            final boolean convidouQuemEstaVendo = this.conviteService.temConviteAmizadePendente(usuarioDoPerfil.getId(), this.getUsuarioAutenticado()
                    .getId());
            final boolean foiConvidadoPorQuemEstaVendo = this.conviteService.temConviteAmizadePendente(this.getUsuarioAutenticado().getId(),
                    usuarioDoPerfil.getId());
            perfilView.setConvidouQuemEstaVendo(convidouQuemEstaVendo);
            perfilView.setFoiConvidadoPorQuemEstaVendo(foiConvidadoPorQuemEstaVendo);
            if (!convidouQuemEstaVendo && !foiConvidadoPorQuemEstaVendo) {
                final Amizade amizade = this.amizadeService.consultarAmizade(usuarioDoPerfil.getId(), this.getUsuarioAutenticado().getId());
                perfilView.setAmigo(amizade != null);
                perfilView.setAmigoSomenteFacebook(amizade != null && amizade.isSomenteFacebook());
                perfilView.setAmigoImportadoFacebook(amizade != null && amizade.getImportadaFacebook());
            }
        }
        perfilView.setVisivel((this.getUsuarioAutenticado() != null && this.getUsuarioAutenticado().equals(usuarioDoPerfil))
                || usuarioDoPerfil.isPerfilPublico() || perfilView.isAmigo());
        return perfilView;
    }

    public PreferenciasUsuario consultarPreferenciasDoUsuario(final Usuario usuario) {
        return this.preferenciasUsuarioService.consultarPreferenciasDoUsuario(usuario);
    }

    @SuppressWarnings("unchecked")
    public Usuario consultarUsuario(final Connection<?> connection) {
        if (connection != null) {
            Object api = null;
            String userID = null;
            // Foi necessario fazer o try/catch pois as vezes ocorre um erro de proxy ao chamar o connection.getApi()
            try {
                api = connection.getApi();
                // Esta linha apenas tenta fazer com q o erro ocorra
                if (api instanceof Facebook) {
                    final FacebookProfile profile = ((Connection<Facebook>) connection).getApi().userOperations().getUserProfile();
                    userID = profile.getId();
                }
            } catch (final Exception e) {
                // Se o erro ocorrer, pegar
                api = connection.getKey().getProviderId();
                userID = connection.getKey().getProviderUserId();
                System.out.println(userID);
            }
            if (api instanceof Facebook || "facebook".equals(api)) {
                // Connection<Facebook> connectionFacebook = (Connection<Facebook>) connection;

                return this.consultarUsuarioPorIdFacebook(userID);
            }
            return (Usuario) loadUserByUsername(connection.fetchUserProfile().getEmail());
        }
        return null;
    }

    public Usuario consultarUsuarioPorIdFacebook(final String idUsuarioFacebook) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idFacebook", idUsuarioFacebook);
        return this.searchRepository.queryByNamedQueryUniqueResult("Usuario.consultarUsuarioPorPerfilFacebook", parametros);
    }

    /**
     * Consulta o usuario pelo ID do facebook ou endereco de email (username)
     *
     * @param profile
     * @return
     */
    // public Usuario consultarUsuarioPorPerfilFacebook(final Reference profile) {
    public Usuario consultarUsuarioPorPerfilFacebook(final FacebookProfile profile) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        // TODO ver a necessidade de realizar a consulta tambem pelo username do perfil
        parametros.put("idFacebook", profile.getId());
        // parametros.put("usernameFacebook", profile.getUsername());
        final List<Usuario> resultado = this.searchRepository.consultarPorNamedQuery("Usuario.consultarUsuarioPorPerfilFacebook", parametros,
                Usuario.class);
        if (resultado != null && !resultado.isEmpty()) {
            return resultado.get(0);
        }
        return null;
    }

    public Usuario consultarUsuarioPorUsername(final String username) {
        try {
            return (Usuario) loadUserByUsername(username);
        } catch (final Exception e) {
            return null;
        }
    }

    @Transactional
    public void cortarFotoPerfil(Usuario usuario, final Rectangle coordenadas) throws IOException {
        usuario = this.consultarPorId(usuario.getId());
        final Foto foto = usuario.getFoto();
        this.fotoService.cortarFotoUsuario(foto, coordenadas);
    }

    @Transactional
    public Usuario criarConta(final CadastroForm cadastroForm, final Long idUsuarioConvidante, final Long idConvite, final String idFacebookConvidado)
            throws UsernameAlreadyInUseException {
        // Verificar se o usuário já existe
        try {
            if (this.loadUserByUsername(cadastroForm.getUsername()) != null) {
                throw new UsernameAlreadyInUseException(cadastroForm.getUsername());
            }
        } catch (final UsernameNotFoundException unfe) {
            // Usuario nao existe... continuar execucao
        }
        final Usuario usuario = this.configurarNovoUsuario(cadastroForm);
        if (idUsuarioConvidante != null) {
            usuario.setIdUsuarioConvidante(idUsuarioConvidante);
        }
        this.incluir(usuario);
        this.configurarFotoPerfil(usuario);
        this.enviarEmailConfirmacaoCadastro(usuario);

        if (idConvite != null) {
            aceitarConvitePorIdConvite(idConvite, usuario);
        } else if (idFacebookConvidado != null) {
            final Convite convite = aceitarConvitePorIdFacebookConvidado(idUsuarioConvidante, idFacebookConvidado);
            convite.setConvidado(usuario);
        } else if (idUsuarioConvidante != null) {
            aceitarConvitePorIdUsuarioConvidou(idUsuarioConvidante, usuario);
        }
        return usuario;
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public Usuario criarContaDeExternalServiceProfile(final Connection<?> connection, final Long idUsuarioConvidante, final Long idConvite) {
        final UserProfile userProfile = connection.fetchUserProfile();

        final Usuario usuario = this.criarContaDeUserProfile(userProfile);

        if (idUsuarioConvidante != null) {
            usuario.setIdUsuarioConvidante(idUsuarioConvidante);
        }

        if (!usuario.isCadastroCompleto()) {
            if (connection.getApi() instanceof Facebook) {
                this.criarContaDeFacebookProfile(usuario, (Connection<Facebook>) connection);
            } else if (connection.getApi() instanceof Google) {
                this.criarContaDeGoogleProfile(usuario, (Connection<Google>) connection);
            } else if (connection.getApi() instanceof Twitter) {
                this.criarContaDeTwitterProfile(usuario, (Connection<Twitter>) connection);
            }
            if (usuario.getFoto() == null) {
                // o Google não tem foto padrão; portanto, a foto pode ser nula
                this.configurarFotoPerfil(usuario);
            }
        }

        if (usuario.getFoto() != null) {
            // Impede a geracao automatica da Acao TipoAcao.ALTERAR_FOTO_PERFIL
            usuario.getFoto().setDataGeracaoAcaoAlteracao(new Date());
        }
        this.alterar(usuario);

        if (idConvite != null) {
            aceitarConvitePorIdConvite(idConvite, usuario);
        } else if (idUsuarioConvidante != null) {
            aceitarConvitePorIdUsuarioConvidou(idUsuarioConvidante, usuario);
        }
        return usuario;

    }

    private void criarContaDeFacebookProfile(final Usuario usuario, final Connection<Facebook> connection) {
        final FacebookProfile profile = connection.getApi().userOperations().getUserProfile();

        final String email = connection.fetchUserProfile().getEmail();
        usuario.setUsername(email);
        usuario.setAtivo(true);
        usuario.setEmail(email);

        if (profile.getLocation() != null) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Id do local de residência retornado pelo Facebook: " + profile.getLocation().getId());
            }
            Local localResidencia = this.localService.consultarLocalPorIdFacebook(profile.getLocation().getId());
            if (localResidencia == null) {
                // faz outra pesquisa baseada no nome
                final Page page = connection.getApi().fetchObject(profile.getLocation().getId(), Page.class);
                localResidencia = this.localService.consultarCidadePorReferenciaFacebook(page);
            }
            if (localResidencia != null && localResidencia.isTipoCidade()) {
                usuario.setCidadeResidencia((Cidade) localResidencia);
            }
        }

        if (profile.getHometown() != null) {
            if (this.log.isDebugEnabled()) {
                this.log.debug("Id do local de natal retornado pelo Facebook: " + profile.getHometown().getId());
            }
            Local localNatal = this.localService.consultarLocalPorIdFacebook(profile.getHometown().getId());
            if (localNatal == null) {
                final Page page = connection.getApi().fetchObject(profile.getHometown().getId(), Page.class);
                localNatal = this.localService.consultarCidadePorReferenciaFacebook(page);
            }
            if (localNatal != null && localNatal.isTipoCidade()) {
                usuario.setCidadeNatal((Cidade) localNatal);
            }
        }

        usuario.setConectadoFacebook(true);
        if (profile.getBirthday() != null) {
            usuario.setDataNascimento(new Date(profile.getBirthday()));
        }
        usuario.setDisplayName(profile.getName());
        usuario.setEmail(profile.getEmail());
        usuario.setFormaCadastro(FormaCadastroUsuario.FACEBOOK);
        usuario.setFotoExterna(true);
        if (usuario.getFoto() == null) {
            configurarFotoFacebook(usuario, profile.getId());
        }
        this.definirGenero(usuario, profile.getGender());
        usuario.setIdFacebook(profile.getId());
        usuario.setImportadoFacebook(true);
        usuario.setLocale(profile.getLocale() != null ? profile.getLocale().toString() : null);
        usuario.setPrimeiroNome(profile.getFirstName());
        usuario.setNomeMeio(profile.getMiddleName());
    }

    private void criarContaDeGoogleProfile(final Usuario usuario, final Connection<Google> connection) {
        // TODO mudar: se personOperations() for diferente null usá-lo; caso contrário, usar LegacyProfile
        // final Person profile = connection.getApi().personOperations().getGoogleProfile();
        // final LegacyGoogleProfile legacyProfile = connection.getApi().userOperations().getUserProfile();

        final Person profile = connection.getApi().plusOperations().getGoogleProfile();

        usuario.setConectadoGoogle(true);
        usuario.setDisplayName(profile.getDisplayName());
        usuario.setEmail(profile.getAccountEmail());
        usuario.setFormaCadastro(FormaCadastroUsuario.GOOGLE);
        this.definirGenero(usuario, profile.getGender());
        if (usuario.getFoto() == null) {
            if (profile.getImageUrl() != null) {
                final Foto foto = new Foto();
                foto.setUsuario(usuario);
                foto.setAutor(usuario);
                foto.setTipoArmazenamento(TipoArmazenamentoFoto.EXTERNO);
                usuario.setFotoExterna(true);
                foto.setAnonima(false);
                foto.setUrlExternaSmall(profile.getImageUrl());
                foto.setUrlExternaAlbum(profile.getImageUrl());
                foto.setUrlExternaBig(profile.getImageUrl());
            }
        }
        usuario.setIdGoogle(profile.getId());
        usuario.setImportadoGoogle(true);
        // usuario.setLocale(profile.getLocale());
        usuario.setPrimeiroNome(profile.getGivenName());
        usuario.setUltimoNome(profile.getFamilyName());
    }

    private void criarContaDeTwitterProfile(final Usuario usuario, final Connection<Twitter> connection) {
        final TwitterProfile profile = connection.getApi().userOperations().getUserProfile();

        final String cidadeAtual = profile.getLocation();

        usuario.setConectadoTwitter(true);
        usuario.setFormaCadastro(FormaCadastroUsuario.TWITTER);

        this.completarDadosBasicosDeExternalProfile(usuario, null, cidadeAtual, profile.getLanguage());
    }

    @Transactional
    public Usuario criarContaDeUserProfile(final UserProfile userProfile) {
        Usuario usuario = null;
        try {
            usuario = (Usuario) this.loadUserByUsername(userProfile.getEmail());
        } catch (final UsernameNotFoundException e) {
            // Continua a execução
        }
        if (usuario == null) {
            usuario = this.configurarNovoUsuario(userProfile.getEmail());
            usuario.setPrimeiroNome(userProfile.getFirstName());
            usuario.setUltimoNome(userProfile.getLastName());

            usuario.setDisplayName(usuario.getPrimeiroNome() + " " + usuario.getUltimoNome());
            usuario.setDataConfirmacao(new Date());
            this.incluir(usuario);
        }
        return usuario;
    }

    @Transactional
    public void criarSenha(final Usuario usuario, final String senhaNova) {
        final String senhaNovaCriptografada = this.criptografaSenha(senhaNova, usuario.getPasswordSalt());
        usuario.setPassword(senhaNovaCriptografada);
        this.alterar(usuario);
    }

    /**
     * Criptografar Texto com MD5
     *
     * @param texto
     * @return
     */
    private String criptografarTexto(final String texto) {
        return DigestUtils.md5DigestAsHex(texto.getBytes());
    }

    /**
     * Criptografar a senha com MD5.
     *
     * @param password
     * @param salt
     * @return a senha criptograda
     */
    private String criptografaSenha(final String password, final String salt) {
        return this.passwordEncoder.encodePassword(password, salt);
    }

    private void definirGenero(final Usuario usuario, final String genero) {
        if (genero != null) {
            if (genero.equalsIgnoreCase("male") || genero.equalsIgnoreCase("masculino") || genero.equalsIgnoreCase("m")) {
                usuario.setGenero("M");
            } else if (genero.equalsIgnoreCase("female") || genero.equalsIgnoreCase("feminino") || genero.equalsIgnoreCase("f")) {
                usuario.setGenero("F");
            }
        }
    }

    @Transactional
    public void desconectarProvedorExterno(final Usuario usuario, final String providerId) {
        if ("facebook".equals(providerId)) {
            usuario.setConectadoFacebook(false);
        }
        if ("foursquare".equals(providerId)) {
            usuario.setConectadoFoursquare(false);
        }
        if ("google".equals(providerId)) {
            usuario.setConectadoGoogle(false);
        }
        if ("twitter".equals(providerId)) {
            usuario.setConectadoTwitter(false);
        }
        this.alterar(usuario);
    }

    @Transactional
    public void enviarEmailCodigoRecuperacaoSenha(final Usuario usuario) {
        final String assunto = "Recuperação de senha no TripFans";
        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("usuario", usuario);
        model.put("codigoRecuperacao", gerarCodigoRecuperacaoSenha(usuario));
        this.emailService.enviarEmail(usuario.getUsername(), assunto, EmailTemplate.EMAIL_RECUPERACAO_SENHA, model);
    }

    public void enviarEmailConfirmacaoCadastro(final Usuario usuario) {
        // final String destino = usuario.getUsername();
        /* final String destino = "carlinhoswake@gmail.com"; */
        final String assunto = "Confirmação de cadastro no TripFans";
        // final String mensagem = getTextoConfirmacaoCadastro(usuario);

        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("usuario", usuario);
        model.put("codigoConfirmacao", this.criptografarTexto("" + usuario.getUsername().hashCode()));
        model.put("codigoUsuario", usuario.getUrlPath());

        this.emailService.enviarEmail(usuario.getUsername(), assunto, EmailTemplate.EMAIL_CONFIRMACAO, model);
    }

    public void enviarEmailConfirmacaoNovoEmail(final ContaServicoExterno contaServicoExterno) {
        final Usuario usuario = contaServicoExterno.getUsuario();
        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("usuario", usuario);
        model.put("codigoConfirmacao", this.criptografarTexto("" + usuario.getUsername().hashCode()));
        model.put("codigoUsuario", usuario.getUrlPath());

        // this.emailService.enviarEmail(destino, assunto, EmailService.Template.EMAIL_CONFIRMACAO, model);
    }

    @Transactional
    public void excluirEmail(final ContaServicoExterno contaExterna) {
        final Usuario usuario = contaExterna.getUsuario();
        usuario.removeContaServicoExterno(contaExterna);
        this.alterar(usuario);
    }

    private void excluirUsuarioImportado(final Usuario usuarioImportado) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuarioInativo", usuarioImportado);
        getRepository().alterarPorNamedQuery("Usuario.removerUsuarioFacebookAtivo", parametros);
    }

    private String gerarCodigoRecuperacaoSenha(final Usuario usuario) {
        final String codigo = RandomStringUtils.random(8, false, true);
        usuario.setCodigoRecuperacaoSenha(codigo);
        this.salvar(usuario);
        return codigo;
    }

    @Transactional
    public void incluirEmail(Usuario usuario, final String email) {

        // Recuperar Usuario para evitar Lazy
        usuario = this.consultarPorId(usuario.getId());

        this.verificarDuplicidadeEmail(usuario, email);

        final ContaServicoExterno contaServicoExterno = new ContaServicoExterno();
        contaServicoExterno.setTipoServicoExterno(TipoServicoExterno.EMAIL);
        contaServicoExterno.setDataCadastro(new Date());
        contaServicoExterno.setUsername(email);

        usuario.addContaServicoExterno(contaServicoExterno);

        this.alterar(usuario);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException, DataAccessException {

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("email", username);

        final Usuario usuario = this.searchRepository.queryByNamedQueryUniqueResult("Usuario.consultarUsuarioAtivoEmail", parametros);

        if (usuario == null) {
            throw new UsernameNotFoundException("Usuario nao encontrado.");
        }

        // Verifica se o usuario tem permissao e esta ativo
        if (usuario.isAccountNonLocked() && usuario.isEnabled()) {
            return usuario;
        } else {
            // TODO throw Usuario bloqueado exception
            return usuario;
        }
    }

    @Override
    protected void posInclusao(final Usuario usuario) {
        // Criar as configurações padrão
        final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoes = new ConfiguracaoNotificacoesPorEmail();
        configuracaoNotificacoes.setUsuario(usuario);
        this.configuracaoNotificacoesPorEmailService.salvar(configuracaoNotificacoes);

        // Gerar Acao TipoAcao.ENTRAR_NO_TRIPFANS
        final AcaoUsuario acaoUsuario = new AcaoUsuario(usuario, TipoAcao.ENTRAR_NO_TRIPFANS, usuario, null, null);
        this.acaoUsuarioService.incluir(acaoUsuario);
    }

    @Override
    protected void preAlteracao(final Usuario usuario) {
        usuario.setDataUltimaAlteracao(new LocalDateTime());
    }

    @Transactional
    public void salvarConfiguracoesEmails(final ConfiguracoesGeraisEmailUsuarioView view) {
        final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoesPorEmail = view.getConfiguracaoNotificacoesPorEmail();
        if (configuracaoNotificacoesPorEmail.getId() == null) {
            configuracaoNotificacoesPorEmail.setUsuario(this.getUsuarioAutenticado());
        }
        this.configuracaoNotificacoesPorEmailService.salvar(configuracaoNotificacoesPorEmail);
    }

    /**
     * Inclui/Altera a foto de um perfil.
     *
     * @param usuario
     */
    @Transactional
    public void salvarFotoPerfil(final Usuario usuario, final InputStream arquivo, final String nomeArquivo) throws IOException {
        final Foto foto = usuario.getFoto();
        if (foto.isTipoArmazenamentoExterno()) {
            foto.setTipoArmazenamento(TipoArmazenamentoFoto.LOCAL);
        }
        usuario.setFotoExterna(false);
        foto.setArquivoFoto(IOUtils.toByteArray(arquivo), nomeArquivo);
        foto.setAnonima(false);
        foto.setUsuario(usuario);
        this.fotoService.salvarFotoPerfilUsuario(foto);
        this.alterar(usuario);
    }

    /**
     * Inclui/Altera a foto de um perfil.
     *
     * @param usuario
     */
    @Transactional
    public void salvarFotoPerfil(final Usuario usuario, final MultipartFile arquivo) throws IOException {
        this.salvarFotoPerfil(usuario, arquivo.getInputStream(), arquivo.getOriginalFilename());
    }

    @Transactional
    public void salvarInteressesPreferenciasViagem(final InteressesPreferenciasViagemView view) {
        final InteressesViagem interessesViagem = view.getInteressesViagem();
        if (interessesViagem != null) {
            if (interessesViagem.getId() == null) {
                interessesViagem.setUsuario(this.getUsuarioAutenticado());
            }
            this.interessesViagemService.salvar(interessesViagem);
        }
        final PreferenciasViagem preferenciasViagem = view.getPreferenciasViagem();
        if (preferenciasViagem != null) {
            if (preferenciasViagem.getId() == null) {
                preferenciasViagem.setUsuario(this.getUsuarioAutenticado());
            }
            this.preferenciasViagemService.salvar(preferenciasViagem);
        }
    }

    @Transactional
    public void salvarPreferenciasUsuario(final ConfiguracoesGeraisEmailUsuarioView view) {
        if (view.getPreferencias().getId() == null) {
            view.getPreferencias().setUsuario(this.getUsuarioAutenticado());
        }
        this.preferenciasUsuarioService.salvar(view.getPreferencias());
    }

    private void trocarReferenciasUsuarioImportadoPorUsuarioAtual(final Usuario usuarioImportado, final Usuario usuarioAtivo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuarioAtivo", usuarioAtivo);
        parametros.put("usuarioInativo", usuarioImportado);
        getRepository().alterarPorNamedQuery("Usuario.atualizarAmizadeTrocarAmigoFacebookPorUsuarioAtivo", parametros);
        getRepository().alterarPorNamedQuery("Usuario.atualizarAmizadeTrocarUsuarioFacebookPorUsuarioAtivo", parametros);
        getRepository().alterarPorNamedQuery("Usuario.removerInteresseEmLocalImportadoDuplicado", parametros);
        getRepository().alterarPorNamedQuery("Usuario.atualizarInteresseEmLocalTrocarUsuarioFacebookPorUsuarioAtivo", parametros);
    }

    public boolean urlPathUsuarioEstaDisponivel(final String urlPath) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("urlPath", urlPath);
        final String urlPathRetorno = this.searchRepository.queryByNamedNativeQueryUniqueResult("Usuario.urlPathUsuarioEstaDisponivel", parametros);

        return urlPathRetorno == null && !verificarUrlPathsReservadas(urlPath);
    }

    public List<UsuarioConviteFacebookView> usuariosParaConviteFacebook(final Usuario usuario) {
        if (usuario.estaConectadoFacebook()) {
            final Map<String, Object> parametros = new HashMap<String, Object>();
            parametros.put("idUsuario", usuario);
            final List<UsuarioConviteFacebookView> usuarios = this.searchRepository.consultarPorNamedNativeQuery(
                    "Usuario.usuariosParaConviteFacebook", parametros, UsuarioConviteFacebookView.class);
            return this.sorteadorItens.sortear(usuarios, 10);
        }
        return Collections.emptyList();
    }

    private void verificarDuplicidadeEmail(final Usuario usuario, final String email) {
        boolean existe = usuario.getUsername().equals(email);
        if (usuario.getContasEmServicosExternos() != null) {
            for (final ContaServicoExterno conta : usuario.getContasEmServicosExternos()) {
                if (conta.isEmail() && conta.getUsername().equals(email)) {
                    existe = true;
                    break;
                }
            }
        }
        if (existe) {
            throw new ServiceException("O email informado já está cadastrado.");
        }
    }

    /**
     * Verifica se a urlPath informada consta na lista de urlPaths reservadas
     *
     * @param urlPath
     * @return true se a urlPath estiver reservada
     */
    private boolean verificarUrlPathsReservadas(final String urlPath) {
        if (urlPath != null) {
            final List<String> urlPaths = new ArrayList<String>();
            urlPaths.add("carlos".toLowerCase());
            urlPaths.add("carlos-nascimento".toLowerCase());
            urlPaths.add("carlinhos".toLowerCase());
            return urlPaths.contains(urlPath.trim().toLowerCase());
        }
        return false;
    }

}
