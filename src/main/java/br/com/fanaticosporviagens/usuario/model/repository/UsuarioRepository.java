package br.com.fanaticosporviagens.usuario.model.repository;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.Usuario;

@Repository
public class UsuarioRepository extends GenericCRUDRepository<Usuario, Long> {

}
