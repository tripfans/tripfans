package br.com.fanaticosporviagens.usuario.model.service;

import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.ContaServicoExterno;

@Service
public class ContaServicoExternoService extends GenericCRUDService<ContaServicoExterno, Long> {

}
