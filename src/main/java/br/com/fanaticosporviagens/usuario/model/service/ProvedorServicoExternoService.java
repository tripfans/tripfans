package br.com.fanaticosporviagens.usuario.model.service;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.ProvedorServicoExterno;

@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ProvedorServicoExternoService extends GenericCRUDService<ProvedorServicoExterno, Long> {

}
