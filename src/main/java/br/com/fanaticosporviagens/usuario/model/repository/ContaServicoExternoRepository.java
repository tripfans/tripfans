package br.com.fanaticosporviagens.usuario.model.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.NoSuchConnectionException;
import org.springframework.social.connect.NotConnectedException;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.ContaServicoExterno;

public class ContaServicoExternoRepository extends GenericCRUDRepository<ContaServicoExterno, Long> implements ConnectionRepository {

    private ConnectionFactoryLocator connectionFactoryLocator;

    private final ServiceProviderConnectionMapper connectionMapper = new ServiceProviderConnectionMapper();

    private JdbcTemplate jdbcTemplate;

    // private final TextEncryptor textEncryptor;

    private String tablePrefix;

    private String userId;

    @Override
    public void addConnection(final Connection<?> connection) {

    }

    @Override
    public MultiValueMap<String, Connection<?>> findAllConnections() {
        final List<Connection<?>> resultList = this.jdbcTemplate.query(selectFromUserConnection() + " where userId = ? order by providerId, rank",
                this.connectionMapper, this.userId);
        final MultiValueMap<String, Connection<?>> connections = new LinkedMultiValueMap<String, Connection<?>>();
        final Set<String> registeredProviderIds = this.connectionFactoryLocator.registeredProviderIds();
        for (final String registeredProviderId : registeredProviderIds) {
            connections.put(registeredProviderId, Collections.<Connection<?>> emptyList());
        }
        for (final Connection<?> connection : resultList) {
            final String providerId = connection.getKey().getProviderId();
            if (connections.get(providerId).size() == 0) {
                connections.put(providerId, new LinkedList<Connection<?>>());
            }
            connections.add(providerId, connection);
        }
        return connections;
    }

    @Override
    public <A> List<Connection<A>> findConnections(final Class<A> apiType) {
        final List<?> connections = findConnections(getProviderId(apiType));
        return (List<Connection<A>>) connections;
    }

    @Override
    public List<Connection<?>> findConnections(final String providerId) {
        return this.jdbcTemplate.query(selectFromUserConnection() + " where userId = ? and providerId = ? order by rank", this.connectionMapper,
                this.userId, providerId);
    }

    @Override
    public MultiValueMap<String, Connection<?>> findConnectionsToUsers(final MultiValueMap<String, String> providerUserIds) {
        if (providerUserIds.isEmpty()) {
            throw new IllegalArgumentException("Unable to execute find: no providerUserIds provided");
        }
        final StringBuilder providerUsersCriteriaSql = new StringBuilder();
        final MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", this.userId);
        for (final Iterator<Entry<String, List<String>>> it = providerUserIds.entrySet().iterator(); it.hasNext();) {
            final Entry<String, List<String>> entry = it.next();
            final String providerId = entry.getKey();
            providerUsersCriteriaSql.append("providerId = :providerId_").append(providerId).append(" and providerUserId in (:providerUserIds_")
                    .append(providerId).append(")");
            parameters.addValue("providerId_" + providerId, providerId);
            parameters.addValue("providerUserIds_" + providerId, entry.getValue());
            if (it.hasNext()) {
                providerUsersCriteriaSql.append(" or ");
            }
        }
        final List<Connection<?>> resultList = new NamedParameterJdbcTemplate(this.jdbcTemplate).query(selectFromUserConnection()
                + " where userId = :userId and " + providerUsersCriteriaSql + " order by providerId, rank", parameters, this.connectionMapper);
        final MultiValueMap<String, Connection<?>> connectionsForUsers = new LinkedMultiValueMap<String, Connection<?>>();
        for (final Connection<?> connection : resultList) {
            final String providerId = connection.getKey().getProviderId();
            final List<String> userIds = providerUserIds.get(providerId);
            List<Connection<?>> connections = connectionsForUsers.get(providerId);
            if (connections == null) {
                connections = new ArrayList<Connection<?>>(userIds.size());
                for (int i = 0; i < userIds.size(); i++) {
                    connections.add(null);
                }
                connectionsForUsers.put(providerId, connections);
            }
            final String providerUserId = connection.getKey().getProviderUserId();
            final int connectionIndex = userIds.indexOf(providerUserId);
            connections.set(connectionIndex, connection);
        }
        return connectionsForUsers;
    }

    @Override
    public <A> Connection<A> findPrimaryConnection(final Class<A> apiType) {
        final String providerId = getProviderId(apiType);
        return (Connection<A>) findPrimaryConnection(providerId);

    }

    @Override
    public <A> Connection<A> getConnection(final Class<A> apiType, final String providerUserId) {
        final String providerId = getProviderId(apiType);
        return (Connection<A>) getConnection(new ConnectionKey(providerId, providerUserId));
    }

    @Override
    public Connection<?> getConnection(final ConnectionKey connectionKey) {
        try {
            // return jdbcTemplate.queryForObject(selectFromUserConnection() + " where userId = ? and providerId = ? and providerUserId = ?",
            // connectionMapper, userId, connectionKey.getProviderId(), connectionKey.getProviderUserId());
            return null;
        } catch (final EmptyResultDataAccessException e) {
            throw new NoSuchConnectionException(connectionKey);
        }
    }

    @Override
    public <A> Connection<A> getPrimaryConnection(final Class<A> apiType) {
        final String providerId = getProviderId(apiType);
        final Connection<A> connection = (Connection<A>) findPrimaryConnection(providerId);
        if (connection == null) {
            throw new NotConnectedException(providerId);
        }
        return connection;
    }

    @Override
    public void removeConnection(final ConnectionKey connectionKey) {
        // jdbcTemplate.update("delete from " + tablePrefix + "UserConnection where userId = ? and providerId = ? and providerUserId = ?", userId,
        // connectionKey.getProviderId(), connectionKey.getProviderUserId());
    }

    @Override
    public void removeConnections(final String providerId) {
        // jdbcTemplate.update("delete from " + tablePrefix + "UserConnection where userId = ? and providerId = ?", userId, providerId);
    }

    @Override
    public void updateConnection(final Connection<?> connection) {
        final ConnectionData data = connection.createData();
        // jdbcTemplate
        // .update("update "
        // + tablePrefix
        // +
        // "UserConnection set displayName = ?, profileUrl = ?, imageUrl = ?, accessToken = ?, secret = ?, refreshToken = ?, expireTime = ? where userId = ? and providerId = ? and providerUserId = ?",
        // data.getDisplayName(), data.getProfileUrl(), data.getImageUrl(), encrypt(data.getAccessToken()), encrypt(data.getSecret()),
        // encrypt(data.getRefreshToken()), data.getExpireTime(), userId, data.getProviderId(), data.getProviderUserId());
    }

    private String encrypt(final String text) {
        // return text != null ? textEncryptor.encrypt(text) : text;
        return null;
    }

    private Connection<?> findPrimaryConnection(final String providerId) {
        final List<Connection<?>> connections = this.jdbcTemplate.query(selectFromUserConnection()
                + " where userId = ? and providerId = ? and rank = 1", this.connectionMapper, this.userId, providerId);
        if (connections.size() > 0) {
            return connections.get(0);
        } else {
            return null;
        }
    }

    private <A> String getProviderId(final Class<A> apiType) {
        return this.connectionFactoryLocator.getConnectionFactory(apiType).getProviderId();
    }

    private String selectFromUserConnection() {
        return "select userId, providerId, providerUserId, displayName, profileUrl, imageUrl, accessToken, secret, refreshToken, expireTime from "
                + this.tablePrefix + "UserConnection";
    }

    private final class ServiceProviderConnectionMapper implements RowMapper<Connection<?>> {

        @Override
        public Connection<?> mapRow(final ResultSet rs, final int rowNum) throws SQLException {
            final ConnectionData connectionData = mapConnectionData(rs);
            final ConnectionFactory<?> connectionFactory = ContaServicoExternoRepository.this.connectionFactoryLocator
                    .getConnectionFactory(connectionData.getProviderId());
            return connectionFactory.createConnection(connectionData);
        }

        private String decrypt(final String encryptedText) {
            // return encryptedText != null ? textEncryptor.decrypt(encryptedText) : encryptedText;
            return null;
        }

        private Long expireTime(final long expireTime) {
            return expireTime == 0 ? null : expireTime;
        }

        private ConnectionData mapConnectionData(final ResultSet rs) throws SQLException {
            return new ConnectionData(rs.getString("providerId"), rs.getString("providerUserId"), rs.getString("displayName"),
                    rs.getString("profileUrl"), rs.getString("imageUrl"), decrypt(rs.getString("accessToken")), decrypt(rs.getString("secret")),
                    decrypt(rs.getString("refreshToken")), expireTime(rs.getLong("expireTime")));
        }

    }
}
