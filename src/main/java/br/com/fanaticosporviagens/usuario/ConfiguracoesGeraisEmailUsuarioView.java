package br.com.fanaticosporviagens.usuario;

import br.com.fanaticosporviagens.model.entity.ConfiguracaoNotificacoesPorEmail;
import br.com.fanaticosporviagens.model.entity.PreferenciasUsuario;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class ConfiguracoesGeraisEmailUsuarioView {

    private ConfiguracaoNotificacoesPorEmail configuracaoNotificacoesPorEmail;

    private PreferenciasUsuario preferencias;

    private Usuario usuario;

    public ConfiguracoesGeraisEmailUsuarioView() {
    }

    public ConfiguracoesGeraisEmailUsuarioView(final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoesPorEmail,
            final PreferenciasUsuario preferencias, final Usuario usuario) {
        super();
        this.configuracaoNotificacoesPorEmail = configuracaoNotificacoesPorEmail;
        this.preferencias = preferencias;
        this.usuario = usuario;
    }

    public ConfiguracoesGeraisEmailUsuarioView(final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoesPorEmail, final Usuario usuario) {
        this(configuracaoNotificacoesPorEmail, null, usuario);
    }

    public ConfiguracoesGeraisEmailUsuarioView(final PreferenciasUsuario preferencias, final Usuario usuario) {
        this(null, preferencias, usuario);
    }

    public ConfiguracaoNotificacoesPorEmail getConfiguracaoNotificacoesPorEmail() {
        if (this.configuracaoNotificacoesPorEmail == null) {
            this.configuracaoNotificacoesPorEmail = new ConfiguracaoNotificacoesPorEmail();
        }
        return this.configuracaoNotificacoesPorEmail;
    }

    public PreferenciasUsuario getPreferencias() {
        return this.preferencias;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setConfiguracaoNotificacoesPorEmail(final ConfiguracaoNotificacoesPorEmail configuracaoNotificacoesPorEmail) {
        this.configuracaoNotificacoesPorEmail = configuracaoNotificacoesPorEmail;
    }

    public void setPreferencias(final PreferenciasUsuario preferencias) {
        this.preferencias = preferencias;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

}
