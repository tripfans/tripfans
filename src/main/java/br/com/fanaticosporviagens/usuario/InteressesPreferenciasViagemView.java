package br.com.fanaticosporviagens.usuario;

import br.com.fanaticosporviagens.model.entity.InteressesViagem;
import br.com.fanaticosporviagens.model.entity.PreferenciasViagem;

/**
 * POJO para ser utilizado na tela de configurações de interesses ({@link InteressesViagem}) e preferências ({@link PreferenciasViagem})
 * de viagem.
 * 
 * @author André Thiago
 * 
 */
public class InteressesPreferenciasViagemView {

    private InteressesViagem interessesViagem;

    private PreferenciasViagem preferenciasViagem;

    public InteressesPreferenciasViagemView() {
    }

    public InteressesPreferenciasViagemView(final InteressesViagem interessesViagem) {
        super();
        this.interessesViagem = interessesViagem;
    }

    public InteressesPreferenciasViagemView(final InteressesViagem interessesViagem, final PreferenciasViagem preferenciasViagem) {
        super();
        this.interessesViagem = interessesViagem;
        this.preferenciasViagem = preferenciasViagem;
    }

    public InteressesViagem getInteressesViagem() {
        return this.interessesViagem;
    }

    public PreferenciasViagem getPreferenciasViagem() {
        return this.preferenciasViagem;
    }

    public void setInteressesViagem(final InteressesViagem interessesViagem) {
        this.interessesViagem = interessesViagem;
    }

    public void setPreferenciasViagem(final PreferenciasViagem preferenciasViagem) {
        this.preferenciasViagem = preferenciasViagem;
    }

}
