package br.com.fanaticosporviagens.usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

/**
 * Controller do passo-a-passo de confirmação de conta do usuário.
 *
 * @author André Thiago
 *
 */
@Controller
public class ConfirmacaoContaController extends BaseController {

    @Autowired
    private InteressesViagemService interessesViagemService;

    @Autowired
    private PreferenciasViagemService preferenciasViagemService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/confirmacao/{urlPath}")
    public String confirmacao(@PathVariable("urlPath") final String urlPath, @RequestParam(required = false) final String codigoConfirmacao,
            final NativeWebRequest request, final Model model) {
        try {
            final Usuario usuario = this.usuarioService.consultarPorUrlPath(urlPath);
            if (codigoConfirmacao != null && usuario != null) {
                this.usuarioService.confirmarEmailUsuario(usuario.getId(), codigoConfirmacao);
                this.httpSession.setAttribute("usuario", usuario);
            } else {
                model.addAttribute("requererConfirmacao", "true");
            }
        } catch (final Exception e) {
            e.printStackTrace();
            // result.reject("exception.badCredentials", e.getMessage());
            // model.addAttribute("command", result.getModel());
            return "/conta/erroConfirmacao";
        }
        return "/conta/contaConfirmada";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoBemVindo/{usuario}")
    public String confirmacaoBemVindo(@PathVariable("usuario") final Usuario usuario, final NativeWebRequest request, final Model model) {
        if (usuario != null) {
            this.usuarioService.completarCadastro(usuario);
            this.httpSession.setAttribute("usuario", usuario);
        } else {
            model.addAttribute("requererConfirmacao", "true");
        }
        return "/conta/confirmacao/confirmacaoBemVindo";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoConexoesExternas/{usuario}")
    public String confirmacaoPassoConexoesExternas(@PathVariable("usuario") final Usuario usuario, final Model model) {
        return "/conta/confirmacao/confirmacaoPassoConexoesExternas";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoFinal/{usuario}")
    public String confirmacaoPassoFinal(@PathVariable("usuario") final Usuario usuario, final Model model) {
        return "/conta/confirmacao/confirmacaoPassoFinal";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoFotoPerfil/{usuario}")
    public String confirmacaoPassoFotoPerfil(@PathVariable("usuario") final Usuario usuario, final Model model) {
        return "/conta/confirmacao/confirmacaoPassoFotoPerfil";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoInformacoesBasicas/{usuario}")
    public String confirmacaoPassoInformacoesBasicas(@PathVariable("usuario") final Usuario usuario, final Model model) {
        return "/conta/confirmacao/confirmacaoPassoInformacoesBasicas";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoInteressesViagem/{usuario}")
    public String confirmacaoPassoInteressesViagem(@PathVariable("usuario") final Usuario usuario, final Model model) {
        model.addAttribute("view",
                new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(getUsuarioAutenticado()),
                        this.preferenciasViagemService.consultarPreferenciasViagemDoUsuario(getUsuarioAutenticado())));
        return "/conta/confirmacao/confirmacaoPassoInteressesViagem";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/confirmacaoPassoPreferenciasViagem/{usuario}")
    public String confirmacaoPassoPreferenciasViagem(@PathVariable("usuario") final Usuario usuario, final Model model) {
        model.addAttribute("view",
                new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(getUsuarioAutenticado()),
                        this.preferenciasViagemService.consultarPreferenciasViagemDoUsuario(getUsuarioAutenticado())));
        return "/conta/confirmacao/confirmacaoPassoPreferenciasViagem";
    }

    @RequestMapping(value = "/reenviarEmailConfirmacao/{usuario}")
    public @ResponseBody StatusMessage reenviarEmailConfirmacao(@PathVariable("usuario") final Usuario usuario, final Model model,
            final RedirectAttributes redirectAttributes) {
        this.usuarioService.enviarEmailConfirmacaoCadastro(usuario);
        return success("E-mail reenviado com sucesso.");
    }

    @ModelAttribute(value = "contaUsuario")
    public Usuario setupUsuario() {
        if (getUsuarioAutenticado() != null) {
            return this.usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        }
        return null;
    }

}
