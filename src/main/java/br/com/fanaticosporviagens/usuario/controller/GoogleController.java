package br.com.fanaticosporviagens.usuario.controller;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fanaticosporviagens.gmail.service.GoogleService;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.usuario.view.ContatoView;

import com.google.gdata.client.GoogleAuthTokenFactory;
import com.google.gdata.client.Service.GDataRequestFactory;
import com.google.gdata.client.http.GoogleGDataRequest;

@Controller
public class GoogleController extends BaseController {

    @RequestMapping(value = "/consultarContatosGoogle")
    public String consultarContatosGoogle(final Principal currentUser, final Model model) throws Exception, IOException,
            com.google.gdata.util.ServiceException {
        final GoogleService googleService = new GoogleService();
        final GDataRequestFactory gDataRequestFactory = new GoogleGDataRequest.Factory();
        final GoogleAuthTokenFactory googleAuthTokenFactory = new GoogleAuthTokenFactory("tripfans", "tripfans", null);
        googleAuthTokenFactory.setUserToken(googleService.getGoogleAccessToken());
        final List<ContatoView> listaContatos = googleService.recuperarContatos(gDataRequestFactory, googleAuthTokenFactory,
                googleService.getGoogleAccessToken());

        // final List<String> listaEmails = new ArrayList<String>(Arrays.asList("sebbaweb@terra.com.br", "tripfans1@gmail.com",
        // "tripfans@tripfans.com.br", "tripfans6@gmail.com", "tripfans2@gmail.com", "sebbaweb@gmail.com", "andrethiagos@gmail.com",
        // "gleidson.fernandes@gmail.com"));
        // final List<String> listaNomes = new ArrayList<String>(Arrays.asList("Pedro Terra", "tripfans 1", "tripfans", "tripfans 6", "tripfans 2",
        // "Pedro gmail", "Andre Thiago", "Gleidson"));
        //
        // final List<ContatoView> listaContatosTF = new ArrayList<ContatoView>();
        // final List<ContatoView> listaContatos = new ArrayList<ContatoView>();
        // ContatoView contato;
        // for (int i = 0; i < 8; i++) {
        // contato = new ContatoView();
        // contato.setNome(listaNomes.get(i));
        // contato.setEmail(listaEmails.get(i));
        // contato.setCadastradoTripfans(true);
        // listaContatosTF.add(contato);
        // listaContatos.add(contato);
        // }
        //
        // model.addAttribute("contatos", listaContatosTF);
        model.addAttribute("contatos", listaContatos);

        return "/perfil/listaContatos";
    }

    @RequestMapping(value = "/convidarContatosGoogle")
    public String convidarContatosGoogle(@RequestParam final ArrayList<ContatoView> contatos, final Model model) {
        System.out.println(contatos.get(0).getEmail());
        return "/perfil/listaContatos";
    }
}
