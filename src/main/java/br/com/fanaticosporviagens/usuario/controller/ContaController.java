package br.com.fanaticosporviagens.usuario.controller;

import java.awt.Rectangle;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.exception.ServiceException;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.model.entity.ContaServicoExterno;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.tipo.TipoService;
import br.com.fanaticosporviagens.usuario.ConfiguracoesGeraisEmailUsuarioView;
import br.com.fanaticosporviagens.usuario.InteressesPreferenciasViagemView;
import br.com.fanaticosporviagens.usuario.InteressesViagemService;
import br.com.fanaticosporviagens.usuario.PreferenciasViagemService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Controller
@SessionAttributes({ "usuario" })
@PreAuthorize("isAuthenticated()")
public class ContaController extends BaseController {

    @Autowired
    private FotoService fotoService;

    @Autowired
    private InteressesViagemService interessesViagemService;

    @Autowired
    private PreferenciasViagemService preferenciasViagemService;

    @Autowired
    private TipoService tipoService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/conta/alterarSenha", method = RequestMethod.POST)
    public @ResponseBody
    StatusMessage alterarSenha(@RequestParam final String senhaAtual, @RequestParam final String senhaNova, final Model model) {
        try {
            this.usuarioService.alterarSenha(getUsuarioAutenticado(), senhaAtual, senhaNova);
            return success("A senha foi alterada com sucesso.");
        } catch (final ServiceException e) {
            return error(e.getMessage());
        }
    }

    @RequestMapping(value = "/conta/amigos")
    public String amigos(final Model model) {
        return minhaConta("amigos", model);
    }

    @RequestMapping(value = "/conta/carregarFotoCrop")
    public String carregarFotoCrop(final Model model) throws Exception, IOException {
        return "/conta/cortarFotoPerfil";
    }

    @RequestMapping(value = "/conta/conexoes")
    public String conexoes(final Model model) {
        return minhaConta("conexoes", model);
    }

    @RequestMapping(value = "/conta/config")
    public String config(final Model model) {
        return minhaConta("conexoes", model);
    }

    @RequestMapping(value = "/conta/consultarDisponibilidadeNome")
    public @ResponseBody
    JSONResponse consultarDisponibilidadeNome(@RequestParam final String nomeUsuario) {
        final boolean disponivel = this.usuarioService.urlPathUsuarioEstaDisponivel(nomeUsuario);
        String resposta = "Nome de usuário não disponível.";
        if (disponivel) {
            resposta = "Este nome de usuário está disponível";
        }
        this.jsonResponse.addParam("disponivel", disponivel);
        this.jsonResponse.addParam("resposta", resposta);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/conta/cortarFoto")
    public @ResponseBody
    Map<String, ? extends Object> cortarFoto(@ModelAttribute final Usuario usuario, @RequestParam final int x, @RequestParam final int y,
            @RequestParam final int width, @RequestParam final int height) throws IOException, InterruptedException {

        this.usuarioService.cortarFotoPerfil(usuario, new Rectangle(x, y, width, height));
        Thread.sleep(2000);
        return Collections.singletonMap("success", "true");
    }

    @RequestMapping(value = "/conta/criarSenha", method = RequestMethod.POST)
    public @ResponseBody
    StatusMessage criarSenha(@RequestParam final String senhaNova, final Model model) {
        try {
            this.usuarioService.criarSenha(getUsuarioAutenticado(), senhaNova);
            return success("A senha foi alterada com sucesso.");
        } catch (final ServiceException e) {
            return error(e.getMessage());
        }
    }

    @RequestMapping(value = "/conta/fotosPerfil")
    public String fotosPerfil(final Model model) {
        return minhaConta("fotosPerfil", model);
    }

    @RequestMapping(value = "/conta/geral")
    public String geral(final Model model) {
        final ConfiguracoesGeraisEmailUsuarioView view = new ConfiguracoesGeraisEmailUsuarioView(
                this.usuarioService.consultarPreferenciasDoUsuario(getUsuarioAutenticado()), getUsuarioAutenticado());
        model.addAttribute("view", view);
        return minhaConta("geral", model);
    }

    @RequestMapping(value = "/conta/home")
    public String home(final Principal currentUser, final Model model) {

        // model.addAttribute("connectionsToProviders", getConnectionRepository().findAllConnections());
        // model.addAttribute(accountRepository.findAccountByUsername(currentUser.getName()));

        if (currentUser != null) {
            return "/conta/home";
        }

        return "home";
    }

    @RequestMapping(value = "/conta/informacoesBasicas")
    public String informacoesBasicas(final Model model) {
        return minhaConta("informacoesBasicas", model);
    }

    @RequestMapping(value = "/conta/interesses")
    public String interesses(final Model model) {
        model.addAttribute("view",
                new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(getUsuarioAutenticado()),
                        this.preferenciasViagemService.consultarPreferenciasViagemDoUsuario(getUsuarioAutenticado())));
        return minhaConta("interesses", model);
    }

    @RequestMapping(value = "/conta/minhaConta")
    public String minhaConta(final Model model) {
        return "/conta/minhaConta";
    }

    @RequestMapping(value = "/conta/minhaConta/{pagina}")
    public String minhaConta(@PathVariable final String pagina, final Model model) {
        if (pagina != null) {
            if (AjaxUtils.isAjaxRequest(this.request)) {
                return "/conta/" + pagina;
            }
            model.addAttribute("pagina", pagina);
        }
        return "/conta/minhaConta";
    }

    @RequestMapping(value = "/conta/notificacoes")
    public String notificacoes(final Model model) {
        final ConfiguracoesGeraisEmailUsuarioView view = new ConfiguracoesGeraisEmailUsuarioView(
                this.usuarioService.consultarConfiguracoesEmails(getUsuarioAutenticado()), this.usuarioService.consultarPorId(getUsuarioAutenticado()
                        .getId()));
        model.addAttribute("view", view);
        return minhaConta("notificacoes", model);
    }

    @RequestMapping(value = "/conta/perfil")
    public String perfil(final Model model) {
        return minhaConta("informacoesBasicas", model);
    }

    @RequestMapping(value = "/conta/preferencias")
    public String preferencias(final Model model) {
        model.addAttribute("view",
                new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(getUsuarioAutenticado()),
                        this.preferenciasViagemService.consultarPreferenciasViagemDoUsuario(getUsuarioAutenticado())));
        return minhaConta("preferencias", model);
    }

    @RequestMapping(value = "/conta/privacidade")
    public String privacidade(final Model model) {
        return minhaConta("visibilidade", model);
    }

    @RequestMapping(value = "/conta/publicacaoDados")
    public String publicacaoDados(final Model model) {
        return minhaConta("publicacaoDados", model);
    }

    @RequestMapping(value = "/conta/removerFotoPerfil")
    public @ResponseBody
    StatusMessage removerFotoPerfil(@ModelAttribute final Usuario usuario) throws IOException, InterruptedException {
        try {
            this.fotoService.removerFotoPerfil(usuario);
            this.usuarioService.alterar(usuario);
            Thread.sleep(2000);
            this.httpSession.setAttribute("usuario", usuario);
        } catch (final Exception e) {
            return error("Ocorreu um problema durante a alteração da sua foto. Por favor tente novamente mais tarde.");
        }
        return success("Sua foto de perfil foi alterada com sucesso.");
    }

    @RequestMapping(value = "/conta/salvarAlteracoes")
    public @ResponseBody
    StatusMessage salvarAlteracoes(@ModelAttribute final Usuario usuario) {
        usuario.setDataUltimaAlteracaoDadosPerfil(new LocalDateTime());
        this.usuarioService.alterar(usuario);
        this.httpSession.setAttribute("usuario", usuario);
        return success("As alterações foram salvas com sucesso.");
    }

    @RequestMapping(value = "/conta/salvarConfiguracoesEmails")
    public @ResponseBody
    StatusMessage salvarConfiguracoesEmails(@ModelAttribute final ConfiguracoesGeraisEmailUsuarioView view) {
        this.usuarioService.salvarConfiguracoesEmails(view);
        return success("As alterações foram salvas com sucesso.");
    }

    @RequestMapping(value = "/conta/salvarFoto")
    public @ResponseBody
    StatusMessage salvarFoto(@ModelAttribute Usuario usuario, final HttpServletRequest request, final HttpServletResponse response)
            throws IOException, InterruptedException {
        try {
            final InputStream inputStream = request.getInputStream();
            final String filename = request.getHeader("X-File-Name");

            // Recarregando usuario da base para evitar insconsistencias
            final Usuario perfil = this.usuarioService.consultarPorId(usuario.getId());

            this.usuarioService.salvarFotoPerfil(perfil, inputStream, filename);
            this.usuarioService.alterar(perfil);
            usuario = perfil;
            this.httpSession.setAttribute("usuario", usuario);
            do {
                Thread.sleep(300);
            } while (!this.fotoService.existeFotoUsuario(usuario.getFoto()));
            Thread.sleep(2000);
        } catch (final Exception e) {
            return error("Ocorreu um problema durante a alteração da sua foto. Por favor tente novamente mais tarde.");
        }
        return success("Sua foto de perfil foi alterada com sucesso.");
    }

    @RequestMapping(value = "/conta/salvarPreferenciasInteressesViagem")
    public @ResponseBody
    StatusMessage salvarPreferenciasInteressesViagem(@ModelAttribute final InteressesPreferenciasViagemView view) {
        this.usuarioService.salvarInteressesPreferenciasViagem(view);
        return success("As alterações foram salvas com sucesso.");
    }

    @RequestMapping(value = "/conta/salvarPreferenciasUsuario")
    public @ResponseBody
    StatusMessage salvarPreferenciasUsuario(@ModelAttribute final ConfiguracoesGeraisEmailUsuarioView view) {
        this.usuarioService.salvarPreferenciasUsuario(view);
        return success("As alterações foram salvas com sucesso.");
    }

    @RequestMapping(value = "/conta/senha")
    public String senha(final Model model) {
        return minhaConta("senha", model);
    }

    @ModelAttribute(value = "contaUsuario")
    public Usuario setupUsuario() {
        return this.usuarioService.consultarPorId(getUsuarioAutenticado().getId());
    }

    @RequestMapping(value = "/conta/verificarEmail")
    public @ResponseBody
    Map<String, ? extends Object> verificarEmail(final ContaServicoExterno contaServicoExterno, final Model model) {
        this.usuarioService.enviarEmailConfirmacaoNovoEmail(contaServicoExterno);
        return Collections.singletonMap("success", "Um link de confirmação foi enviado para o seu email.");
    }

    @RequestMapping(value = "/conta/viagem")
    public String viagem(final Model model) {
        model.addAttribute("tiposViagem", this.tipoService.tipoViagemConsultarAtivos());
        return minhaConta("viagem", model);
    }

    // @RequestMapping(value = "/conta/visibilidade")
    // @DenyAll
    // public String visibilidade(final Model model) {
    // return minhaConta("visibilidade", model);
    // }

}
