package br.com.fanaticosporviagens.usuario.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.exception.UsernameAlreadyInUseException;
import br.com.fanaticosporviagens.infra.interceptor.TripFansAuthenticationSuccessHandler;
import br.com.fanaticosporviagens.infra.spring.config.TripFansSignInAdapter;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.form.CadastroForm;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Controller
public class SignupController extends BaseController {

    @Autowired
    private TripFansAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    private TripFansSignInAdapter signInAdapter;

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/avisoEmailEnviado", method = RequestMethod.GET)
    public String avisoEmailEnviado() {
        return "/avisoEmailEnviado";
    }

    private void limparHttpSession() {
        this.httpSession.removeAttribute(TripFansAppConstants.ID_CONVITE);
        this.httpSession.removeAttribute(TripFansAppConstants.ID_USUARIO_CONVIDANTE);
        this.httpSession.removeAttribute(TripFansAppConstants.ID_FACEBOOK_CONVIDADO);
    }

    private Long recuperarIdConvite() {
        try {
            return Long.parseLong(this.httpSession.getAttribute(TripFansAppConstants.ID_CONVITE) + "");
        } catch (final Exception e) {
            return null;
        }
    }

    private String recuperarIdFacebookConvidado() {
        try {
            return (String) this.httpSession.getAttribute(TripFansAppConstants.ID_FACEBOOK_CONVIDADO);
        } catch (final Exception e) {
            return null;
        }
    }

    private Long recuperarIdUsuarioConvidante() {
        try {
            return (Long) this.httpSession.getAttribute(TripFansAppConstants.ID_USUARIO_CONVIDANTE);
        } catch (final Exception e) {
            return null;
        }
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public @ResponseBody Map<String, ? extends Object> signup(@Valid final CadastroForm cadastroForm, final BindingResult result,
            final NativeWebRequest request, final HttpSession session, final Model model) {
        if (result.hasErrors()) {
            return null;
        }
        try {
            final Usuario usuario = this.usuarioService.criarConta(cadastroForm, recuperarIdUsuarioConvidante(), recuperarIdConvite(),
                    recuperarIdFacebookConvidado());
            session.setAttribute("displayName", usuario.getDisplayName());
            session.setAttribute("urlPath", usuario.getUrlPath());
            session.setAttribute("username", usuario.getUsername());
            final Map<String, Object> resultado = new HashMap<String, Object>();
            this.usuarioService.autenticar(cadastroForm.getUsername(), cadastroForm.getPassword());

            final HttpServletResponse response = request.getNativeResponse(HttpServletResponse.class);
            this.authenticationSuccessHandler.onAuthenticationSuccess(request.getNativeRequest(HttpServletRequest.class), response,
                    SecurityContextHolder.getContext().getAuthentication());

            limparHttpSession();
            return resultado;
        } catch (final UsernameAlreadyInUseException e) {
            return Collections.singletonMap("error", e.getMessage());
        }
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(final HttpSession session, final NativeWebRequest request, final Model model) {

        final ProviderSignInUtils providerSignInUtils = new ProviderSignInUtils(this.connectionFactoryLocator, this.usersConnectionRepository);

        final Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        String targetUrl = "/home";
        if (connection != null) {
            Usuario usuario = null;
            try {
                usuario = this.usuarioService.consultarUsuario(connection);
                if (usuario != null) {
                    usuario = this.usuarioService.associarContaTripFansComContaDeExternaServiceProfile(usuario, connection,
                            recuperarIdUsuarioConvidante());
                } else {
                    // usuário não encontrado na base; cria um novo
                    usuario = this.usuarioService
                            .criarContaDeExternalServiceProfile(connection, recuperarIdUsuarioConvidante(), recuperarIdConvite());
                }
            } catch (final UsernameNotFoundException e) {
                // usuário não encontrado na base; cria um novo
                usuario = this.usuarioService.criarContaDeExternalServiceProfile(connection, recuperarIdUsuarioConvidante(), recuperarIdConvite());
            }
            providerSignInUtils.doPostSignUp(usuario.getUsername(), request);
            targetUrl = this.signInAdapter.signIn(usuario.getUsername(), connection, request);
            model.addAttribute("cadastroForm", CadastroForm.fromUsuario(usuario));
            limparHttpSession();
        } else {
            model.addAttribute("cadastroForm", new CadastroForm());
        }
        return "redirect:" + targetUrl;
    }

}