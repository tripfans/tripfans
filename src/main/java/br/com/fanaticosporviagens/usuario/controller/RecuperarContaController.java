package br.com.fanaticosporviagens.usuario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.spring.config.SignInUtils;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Controller
@SessionAttributes({ "usuario" })
public class RecuperarContaController extends BaseController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/recuperarConta/alterarSenha", method = RequestMethod.POST)
    public String alterarSenha(@RequestParam final String emailRecuperacao, @RequestParam final String codigoRecuperacao,
            @RequestParam final String senhaNova, final Model model) {
        final Usuario usuario = this.usuarioService.alterarSenhaAposRecuperacao(emailRecuperacao, codigoRecuperacao, senhaNova);
        if (usuario != null) {
            this.usuarioService.autenticar(usuario.getUsername(), senhaNova);
            SignInUtils.signin(usuario, this.httpRequest);
            this.statusMessage.success("Senha alterada com sucesso!", null);
            return "redirect:/perfil/" + usuario.getUrlPath();
        }
        throw new br.com.fanaticosporviagens.infra.exception.SystemException("Erro ao alterar senha do usuário.");
    }

    @RequestMapping(value = "/recuperarConta/confirmarCodigo", method = RequestMethod.POST)
    public String confirmarCodigoRecuperacaoSenha(@RequestParam final String emailRecuperacao, @RequestParam final String codigoRecuperacao,
            final Model model) {
        if (this.usuarioService.confirmarCodigoRecuperacaoSenha(emailRecuperacao, codigoRecuperacao)) {
            model.addAttribute("codigoConfirmado", true);
            model.addAttribute("emailRecuperacao", emailRecuperacao);
            model.addAttribute("codigoRecuperacao", codigoRecuperacao);
        }
        return "/signin/alterarSenha";
    }

    @RequestMapping(value = "/recuperarConta/confirmarEmail", method = RequestMethod.POST)
    public @ResponseBody
    StatusMessage confirmarEmailRecuperacaoSenha(@RequestParam(value = "emailRecuperacao", required = true) final String emailUsuario,
            final Model model) {
        final Usuario usuario = this.usuarioService.consultarUsuarioPorUsername(emailUsuario);
        if (usuario != null) {
            this.usuarioService.enviarEmailCodigoRecuperacaoSenha(usuario);
            return this.success("Um e-mail foi enviado para você! Por favor, verifique sua caixa de entrada.");
        }
        return this.error("O e-mail informado não está cadastrado no TripFans. <br/> Por favor, verifique se você digitou o email corretamente.");

    }

    @RequestMapping(value = "/recuperarConta/verificarUsuario", method = RequestMethod.POST)
    public String verificarUsuarioRecuperacaoSenha(@RequestParam(value = "emailRecuperacao", required = true) final String emailUsuario,
            final Model model) {
        if (AjaxUtils.isAjaxRequest(this.request)) {
            final Usuario usuario = this.usuarioService.consultarUsuarioPorUsername(emailUsuario);
            model.addAttribute("usuarioRecuperacao", usuario);
            return "/signin/verificarUsuarioRecuperacaoSenha";
        }
        return null;
    }

}
