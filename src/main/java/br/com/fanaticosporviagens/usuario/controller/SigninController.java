package br.com.fanaticosporviagens.usuario.controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.validator.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionData;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInAttempt;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.support.URIBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.social.UsuarioConectadoRedeSocialService;
import br.com.fanaticosporviagens.usuario.form.CadastroForm;
import br.com.fanaticosporviagens.usuario.form.LoginForm;

@Controller
public class SigninController extends BaseController {

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private ConnectionFactoryLocator connectionFactoryLocator;

    @Autowired
    private SignInAdapter signInAdapter;

    @Autowired
    private UsersConnectionRepository usersConnectionRepository;

    @Autowired
    private UsuarioConectadoRedeSocialService usuarioConectadoService;

    @RequestMapping(value = "/cadastrar", method = RequestMethod.GET)
    public String cadastrar(final Model model, @RequestParam(value = "ucv", required = false) final String idUsuarioConvidante,
            @RequestParam(value = "cv", required = false) final String idConvite,
            @RequestParam(value = "u", required = false) final String emailConvidadoCriptografado) {
        if (isUsuarioAutenticado()) {
            return "redirect:/home";
        }
        registrarUsuarioConvidante(idUsuarioConvidante);
        registrarConvite(idConvite);
        registrarEmailConvidado(model, emailConvidadoCriptografado);
        model.addAttribute("cadastro", true);
        if (AjaxUtils.isAjaxRequest(this.httpRequest)) {
            return "/signin/signInUpPanel";
        }
        return "/signin/signin";
    }

    @RequestMapping(value = "/signin/fb/complete", method = RequestMethod.GET)
    public String completarConexaoFacebook(final String providerUserId, final String displayName, final String profileUrl, final String imageUrl,
            final String accessToken, final String secret, final String refreshToken, final Long expireTime, final NativeWebRequest request,
            final Model model) {
        final ConnectionData connectionData = new ConnectionData("facebook", providerUserId, displayName, profileUrl, imageUrl, accessToken, secret,
                refreshToken, expireTime);
        final Connection<?> connection = this.connectionFactoryLocator.getConnectionFactory("facebook").createConnection(connectionData);
        final List<String> userIds = this.usersConnectionRepository.findUserIdsWithConnection(connection);
        if (userIds.size() == 0) {
            final ProviderSignInAttempt signInAttempt = new ProviderSignInAttempt(connection);
            this.request.setAttribute(ProviderSignInAttempt.class.getName(), signInAttempt, RequestAttributes.SCOPE_SESSION);
            return ("redirect:/signup");
        } else if (userIds.size() == 1) {
            this.usersConnectionRepository.createConnectionRepository(userIds.get(0)).updateConnection(connection);
            final String originalUrl = this.signInAdapter.signIn(userIds.get(0), connection, request);
            // return originalUrl != null ? redirect(originalUrl) : redirect(postSignInUrl);
            return "redirect:/home";
        } else {
            return URIBuilder.fromUri("/signin").queryParam("error", "multiple_users").build().toString();
        }
    }

    @RequestMapping(value = "/entrar", method = RequestMethod.GET)
    public String entrar(final Model model, @RequestParam(value = "ucv", required = false) final String idUsuarioConvidante,
            @RequestParam(value = "cv", required = false) final String idConvite,
            @RequestParam(value = "u", required = false) final String emailConvidadoCriptografado,
            @RequestParam(value = "idf", required = false) final String idFacebookConvidado) {
        if (isUsuarioAutenticado()) {
            return "redirect:/home";
        }
        registrarUsuarioConvidante(idUsuarioConvidante);
        registrarConvite(idConvite);
        registrarIdFacebookConvidado(idFacebookConvidado);
        if (emailConvidadoCriptografado != null) {
            return this.cadastrar(model, idUsuarioConvidante, idConvite, emailConvidadoCriptografado);
        }

        final SavedRequest savedRequest = (SavedRequest) this.httpSession.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
        if (savedRequest != null && savedRequest.getRedirectUrl() != null && savedRequest.getRedirectUrl().contains("showMsgRoteiro")) {
            model.addAttribute("showMsgRoteiro", true);
        }

        return "/signin/signin";
    }

    @RequestMapping(value = "/entrarPop", method = RequestMethod.GET)
    public String entrarPopup(final Model model, @RequestParam(value = "to", required = false) final String urlToRedirect,
            final HttpServletResponse httpResponse) {
        if (AjaxUtils.isAjaxRequest(this.httpRequest)) {
            model.addAttribute("isAjaxRequest", true);
            model.addAttribute("targetUrlParameter", urlToRedirect);
            return "/signin/signInUpPanel";
        } else {
            model.addAttribute("isAjaxRequest", false);
            model.addAttribute("requererLogin", true);
            /*if (this.request.getHeader("Referer") != null) {
                return "redirect:" + this.request.getHeader("Referer");
            }*/
            if (urlToRedirect != null) {
                // Salvar request para redirecionar após login
                new HttpSessionRequestCache().saveRequest(this.httpRequest, httpResponse);
                return "redirect:" + urlToRedirect;
            }
            if (this.request.getHeader("Referer") != null) {
                return "redirect:" + this.request.getHeader("Referer");
            }
            return "redirect:/home";
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(final Model model, @RequestParam(required = false) final Boolean standardSubmit,
            @RequestParam(required = false) final String urlToRedirect, final HttpServletResponse httpResponse) {
        if (AjaxUtils.isAjaxRequest(this.httpRequest)) {
            return "/signin/signInAjax";
        }
        if (BooleanUtils.isTrue(standardSubmit)) {
            // carrega a página no modo padrão, i.e, modo não Ajax
            return "/signin";
        } else {
            model.addAttribute("requererLogin", true);
            if (urlToRedirect != null) {
                // Salvar request para redirecionar após login
                new HttpSessionRequestCache().saveRequest(this.httpRequest, httpResponse);
                return "redirect:" + urlToRedirect;
            }
            if (this.request.getHeader("Referer") != null) {
                return "redirect:" + this.request.getHeader("Referer");
            }
            return "redirect:/home";
        }
    }

    private void registrarConvite(final String idConvite) {
        if (idConvite != null) {
            // Colocar o ID do convite na session
            try {
                this.httpSession.setAttribute(TripFansAppConstants.ID_CONVITE, Long.parseLong(idConvite));
            } catch (final Exception e) {
            }
        }
    }

    private void registrarEmailConvidado(final Model model, final String emailConvidadoCriptografado) {
        if (emailConvidadoCriptografado != null) {
            try {
                final String email = CriptografiaUtil.decrypt(emailConvidadoCriptografado);
                if (EmailValidator.getInstance().isValid(email)) {
                    model.addAttribute("username", email);
                }
            } catch (final Exception e) {
            }
        }
    }

    private void registrarIdFacebookConvidado(final String idFacebookConvidado) {
        if (idFacebookConvidado != null) {
            // Colocar o ID do Facebook do convidado na session
            try {
                this.httpSession.setAttribute(TripFansAppConstants.ID_FACEBOOK_CONVIDADO, idFacebookConvidado);
            } catch (final Exception e) {
            }
        }

    }

    private void registrarUsuarioConvidante(final String idUsuarioConvidante) {
        if (idUsuarioConvidante != null) {
            // Colocar o ID do usuario que convidou na session
            try {
                this.httpSession.setAttribute(TripFansAppConstants.ID_USUARIO_CONVIDANTE,
                        Long.parseLong(CriptografiaUtil.decrypt(idUsuarioConvidante)));
            } catch (final Exception e) {
            }
        }
    }

    @RequestMapping(value = "/entrar", method = RequestMethod.POST)
    public @ResponseBody Map<String, ? extends Object> signin(@Valid final LoginForm loginForm,
            @RequestParam(required = false) final String targetUrlParameter, final BindingResult result, final NativeWebRequest request,
            final Model model) {

        try {

            if (isUsuarioAutenticado()) {
                result.reject("exception.userAlreadyLogged");
                return Collections.singletonMap("targetUrl", request.getContextPath() + "/home");
            }

            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginForm.getUsername(),
                    loginForm.getPassword());
            final Usuario usuario = new Usuario();
            usuario.setUsername(loginForm.getUsername());
            token.setDetails(usuario);

            final Authentication auth = this.authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(auth);

            final Map<String, String> response = new HashMap<String, String>();
            response.put("targetUrl", targetUrlParameter != null ? targetUrlParameter : request.getContextPath() + "/home");

            // Verificar se o usuário já está autenticado
            model.addAttribute("cadastroForm", new CadastroForm());

            return response;

        } catch (final BadCredentialsException bce) {
            result.reject("exception.badCredentials");
            model.addAttribute("command", result.getModel());

            return Collections.singletonMap("error", getMessage("exception.badCredentials"));
        } catch (final Exception e) {
            e.printStackTrace();
            result.reject("exception.badCredentials", e.getMessage());

            return Collections.singletonMap("error", getMessage("exception.badCredentials"));
        }

    }

    @RequestMapping(value = "/signout", method = RequestMethod.GET)
    public String signout(final Model model) {
        return "redirect:/home";
    }

    @RequestMapping(value = "/verificaUsuarioConectadoFacebook", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse verificaUsuarioConectadoFacebook(@RequestParam("uid") final String uid, @RequestParam("accessToken") final String accessToken) {
        this.jsonResponse.setSuccess(this.usuarioConectadoService.usuarioConectadoProvedor(uid, "facebook"));
        return this.jsonResponse;
    }

}