package br.com.fanaticosporviagens.usuario.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fanaticosporviagens.acaousuario.GerenciadorAcoesUsuario;
import br.com.fanaticosporviagens.infra.controller.BaseController;

@Controller()
@RequestMapping("/geradorAcoes")
public class AcaoUsuarioController extends BaseController {

    @Autowired
    private List<GerenciadorAcoesUsuario> gerenciadoresAcoesUsuario;

    @RequestMapping(value = "/executar")
    public String executar(final Model model) {
        for (final GerenciadorAcoesUsuario gerenciador : this.gerenciadoresAcoesUsuario) {
            try {
                gerenciador.executar();
            } catch (final Exception e) {
                System.out.println("erro na geraçao das açoes : " + gerenciador.getClass().getName() + " msg : " + e.getMessage());
            }
        }
        return "/home";
    }
}
