package br.com.fanaticosporviagens.usuario.controller;

import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.acaousuario.AcaoUsuarioService;
import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.amizade.AmizadeView;
import br.com.fanaticosporviagens.avaliacao.AvaliacaoService;
import br.com.fanaticosporviagens.comentario.model.service.ComentarioService;
import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.foto.AlbumService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.gmail.service.GoogleService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.security.SecurityService;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.Album;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario;
import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.Pergunta;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;
import br.com.fanaticosporviagens.noticia.NoticiaService;
import br.com.fanaticosporviagens.notificacao.NotificacaoService;
import br.com.fanaticosporviagens.pergunta.PerguntaService;
import br.com.fanaticosporviagens.usuario.InteressesPreferenciasViagemView;
import br.com.fanaticosporviagens.usuario.InteressesViagemService;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;
import br.com.fanaticosporviagens.usuario.view.ContatoView;
import br.com.fanaticosporviagens.usuario.view.PerfilUsuarioExternoView;
import br.com.fanaticosporviagens.usuario.view.UsuarioConviteFacebookView;
import br.com.fanaticosporviagens.viagem.ViagemService;
import br.com.fanaticosporviagens.viagem.model.service.PlanoViagemService;

import com.google.gdata.client.GoogleAuthTokenFactory;
import com.google.gdata.client.Service.GDataRequestFactory;
import com.google.gdata.client.http.GoogleGDataRequest;

/**
 * @author Carlos Nascimento
 */
@Controller
@RequestMapping("/perfil")
public class PerfilController extends BaseController {

    @Autowired
    private AcaoUsuarioService acaoUsuarioService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private ComentarioService comentarioService;

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private DicaService dicaService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private InteressesViagemService interessesViagemService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private LocalService localService;

    @Autowired
    private NoticiaService noticiaService;

    @Autowired
    private NotificacaoService notificacaoService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private PerguntaService perguntaService;

    @Autowired
    private PlanoViagemService planoViagemService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ViagemService viagemService;

    @RequestMapping(value = "/{perfil}/albuns/{album}/adicionarFoto")
    public @ResponseBody JSONResponse adicionarFotoAlbum(@PathVariable final Album album, final HttpServletRequest request) throws IOException {

        request.getInputStream();
        request.getHeader("X-File-Name");

        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/albuns/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String albuns(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("albuns", this.albumService.consultarAlbunsPorAutor(perfil.getId()));
        return "/perfil/fotos/albuns";
    }

    @RequestMapping(value = "/amigos/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String amigos(@PathVariable final Usuario perfil, final Model model, @RequestParam(required = false) final String acao,
            final RedirectAttributes redirectAttributes) {

        final List<AmizadeView> listaAmizades = this.amizadeService.consultarAmizadesVerificandoConvitesTripfans(perfil);

        model.addAttribute("amizadesTripFans", AmizadeView.filtrarAmizadesSomenteTripFans(listaAmizades));
        model.addAttribute("amizadesFacebook", AmizadeView.filtrarAmizadesSomenteFacebook(listaAmizades));

        model.addAttribute("convitesRecebidosPendentes", this.conviteService.convitesAmizadeRecebidosPendentes(perfil.getId()));
        model.addAttribute("convitesEnviadosPendentes", this.conviteService.convitesAmizadeEnviadosPendentes(perfil.getId()));

        model.addAttribute("idUsuarioCriptografado", CriptografiaUtil.encrypt(perfil.getId().toString()));

        redirectAttributes.addFlashAttribute("acao", acao);
        return "/perfil/amigos";
    }

    @RequestMapping(value = "/atividades/{perfil}")
    public String atividades(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesUsuario(perfil));
        return "/perfil/todasAtividades";
    }

    @RequestMapping(value = "/avaliacoes/{perfil}")
    public String avaliacoes(@PathVariable final Usuario perfil, @RequestParam(required = false) final Avaliacao destaque, final Model model) {
        model.addAttribute("avaliacoes", this.avaliacaoService.consultarAvaliacoesConsolidadasPorUsuario(perfil));
        model.addAttribute("avaliacaoDestaque", destaque);
        return "/perfil/avaliacoes";
    }

    @RequestMapping(value = "/carregarFotos/{perfil}")
    public @ResponseBody JSONResponse carregarFotos(@PathVariable final Usuario perfil, final Model model) {
        return new JSONResponse(this.fotoService.consultarFotosPorAutor(perfil.getId()), this.searchData.getTotal(), this.jsonFields);
    }

    @RequestMapping(value = "/comentarios/{perfil}")
    public String comentarios(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("comentario", new Comentario());
        model.addAttribute("comentarios", this.comentarioService.consultarComentariosPerfil(perfil.getId()));
        return "/perfil/comentarios";
    }

    @RequestMapping(value = "/consultarContatosGoogle")
    @PreAuthorize("isAuthenticated()")
    public String consultarContatosGoogle(final Principal currentUser, final Model model) throws Exception, IOException,
            com.google.gdata.util.ServiceException {
        final GoogleService googleService = new GoogleService();
        final GDataRequestFactory gDataRequestFactory = new GoogleGDataRequest.Factory();
        final GoogleAuthTokenFactory googleAuthTokenFactory = new GoogleAuthTokenFactory("tripfans", "tripfans", null);
        googleAuthTokenFactory.setUserToken(googleService.getGoogleAccessToken());
        final List<ContatoView> listaContatos = googleService.recuperarContatos(gDataRequestFactory, googleAuthTokenFactory,
                googleService.getGoogleAccessToken());

        model.addAttribute("contatos", listaContatos);
        model.addAttribute("quantidadeContatos", listaContatos.size());

        return "/perfil/listaContatos";
    }

    @RequestMapping(value = "/consultarLocaisPerimetro")
    public @ResponseBody JSONResponse consultarLocaisPerimetro(@RequestParam final Double latitudeNE, @RequestParam final Double longitudeNE,
            @RequestParam final Double latitudeSW, @RequestParam final Double longitudeSW, @RequestParam("perfil") final Long idUsuario,
            final Model model) {
        final CoordenadaGeografica coordenadaNE = new CoordenadaGeografica(latitudeNE, longitudeNE);
        final CoordenadaGeografica coordenadaSW = new CoordenadaGeografica(latitudeSW, longitudeSW);
        if (!(this.jsonFields == null)) {
            return new JSONResponse(this.interesseUsuarioEmLocalService.consultarInteressesUsuarioEmDestinosPorPerimetro(idUsuario, coordenadaNE,
                    coordenadaSW), this.jsonFields);
        }
        return this.jsonResponse.success();
    }

    /*
     * @RequestMapping(value = "/convidarContatosPorEnderecosDeEmail") public String convidarContatosPorEnderecosDeEmail(final ArrayList<ContatoView>
     * contatos, final Model model) { System.out.println(contatos.get(0).getEmail()); return "/perfil/listaContatos"; }
     */

    @RequestMapping(value = "/convidarContatosPorEnderecosDeEmail", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage convidarContatosPorEnderecosDeEmail(final String enderecosEmail) {
        this.conviteService.convidarParaTripFansPorEmails(enderecosEmail);
        return this.success(String.format("Seus convites foram enviados! Obrigado!"));
    }

    @RequestMapping(value = "/{perfil}/albuns/{album}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String detalharAlbum(@PathVariable final Usuario perfil, @PathVariable final Album album, final Model model) {
        model.addAttribute("album", album);
        return "/perfil/fotos/detalharAlbum";
    }

    /*
     * @RequestMapping(value = "/convidarAmigosFacebook", method = RequestMethod.POST)
     *
     * @PreAuthorize("isAuthenticated()") public @ResponseBody StatusMessage convidarAmigosFacebookPorMensagemPrivada(final String userIds) {
     * this.conviteService.convidarParaTripFansPorPorMensagemPrivada(userIds); return
     * this.success(String.format("Seus convites foram enviados! Obrigado!")); }
     */

    @RequestMapping(value = "/{perfil}/fotos/detalharFotosSemAlbum")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String detalharFotosSemAlbum(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("fotos", this.fotoService.consultarFotosSemAlbumPorAutor(perfil.getId()));
        return "/perfil/fotos/detalharFotosSemAlbum";
    }

    @RequestMapping(value = "/dicas/{perfil}")
    public String dicas(@PathVariable final Usuario perfil, @RequestParam(required = false) final Dica destaque, final Model model) {
        model.addAttribute("dicas", this.dicaService.consultarDicasPorUsuario(perfil));
        model.addAttribute("dicaDestaque", destaque);
        return "/perfil/dicas";
    }

    @RequestMapping(value = "/excluirComentario/{id}")
    @PreAuthorize("#comentario.usuario.username == authentication.name")
    public String excluirComentario(@PathVariable(value = "id") final Comentario comentario, final Model model) {
        this.comentarioService.excluir(comentario);
        model.addAttribute("comentarios", this.comentarioService.consultarComentariosPerfil(this.getUsuarioAutenticado().getId()));
        return "/perfil/listaComentarios";
    }

    @RequestMapping(value = "/{perfil}/{pagina}")
    public String exibirPagina(@PathVariable final Usuario perfil, @PathVariable final String pagina, final Model model,
            final RedirectAttributes redirectAttributes) {
        String parameterName = null;
        final Enumeration<String> parameterNames = this.httpRequest.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            parameterName = parameterNames.nextElement();
            redirectAttributes.addFlashAttribute(parameterName, this.httpRequest.getParameter(parameterName));
        }
        if (pagina != null) {
            if (AjaxUtils.isAjaxRequest(this.request)) {
                return "/perfil/" + pagina;
            }
            model.addAttribute("pagina", pagina);
        }
        return perfil(perfil, model, null, null);
    }

    @RequestMapping("/{userId}/foto")
    public ResponseEntity<byte[]> foto(@PathVariable final String userId) throws IOException {
        final InputStream in = this.httpSession.getServletContext().getResourceAsStream("/resources/images/blank-male.jpg");

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);

        return new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/fotos/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String fotos(@PathVariable final Usuario perfil, final Model model) {
        // model.addAttribute("fotos", this.fotoService.consultarFotosPorAutor(perfil.getId()));
        // return "/perfil/fotos/fotos";
        model.addAttribute("albuns", this.albumService.consultarAlbunsPorAutor(perfil.getId()));
        model.addAttribute("quantidadeFotosSemAlbum", this.fotoService.consultarQuantidadeFotosSemAlbumPorAutor(perfil.getId()));
        return "/perfil/fotos/albuns";
    }

    @RequestMapping(value = "/interesses/{perfil}")
    public String interesses(@PathVariable final Usuario perfil, final Model model, final RedirectAttributes redirectAttributes) {
        model.addAttribute("interessesView", new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(perfil)));
        return exibirPagina(perfil, "interesses", model, redirectAttributes);
    }

    @RequestMapping(value = "/maisAtividades/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String maisAtividades(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesUsuario(perfil));
        return "/perfil/listaAtividades";
    }

    @RequestMapping(value = "/maisAvaliacoes/{perfil}")
    public String maisAvaliacoes(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("avaliacoes", this.avaliacaoService.consultarAvaliacoesConsolidadasPorUsuario(perfil));
        return "/perfil/listaAvaliacoes";
    }

    @RequestMapping(value = "/maisDicas/{perfil}")
    public String maisDicas(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("dicas", this.dicaService.consultarDicasPorUsuario(perfil));
        return "/perfil/listaDicas";
    }

    @RequestMapping(value = "/maisNotificacoes/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String maisNotificacoes(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("notificacoes", this.notificacaoService.consultarTodas(perfil));
        return "/perfil/listaNotificacoes";
    }

    @RequestMapping(value = "/mapa/{perfil}")
    public String mapa(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("interessesUsuarioEmCidades", this.interesseUsuarioEmLocalService.consultarInteressesUsuarioEmCidades(perfil.getId()));
        model.addAttribute("perfil", perfil);
        return "/perfil/mapaViagens";
    }

    @RequestMapping(value = "/mapa/compartilhar/{perfil}")
    public String mapaCompartilhar(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("interessesUsuarioEmCidades", this.interesseUsuarioEmLocalService.consultarInteressesUsuarioEmCidades(perfil.getId()));
        model.addAttribute("perfil", perfil);
        return "/perfil/compartilharMapa";
    }

    @RequestMapping(value = "/meuPerfil")
    @PreAuthorize("isAuthenticated()")
    public String meuPerfil(final Model model, final RedirectAttributes redirectAttributes) {
        final Usuario perfil = this.usuarioService.consultarPorId(getUsuarioAutenticado().getId());
        if (perfil == null) {
            throw new NotFoundException("Usuário não encontrado.");
        }
        // Se for o perfil do usuario TripFans, retornar para a tela inicial (home)
        if (perfil.getId() == 1) {
            return "redirect:/home";
        }
        model.addAttribute("perfilView", this.usuarioService.consultarPerfilUsuario(perfil));
        model.addAttribute("perfil", perfil);
        model.addAttribute("interessesView", new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(perfil)));

        return "/perfil/perfil";
    }

    @RequestMapping(value = "/meusLocais/{perfil}")
    @PreAuthorize("isAuthenticated()")
    public String meusLocais(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("locaisPendentes", this.localService.consultarLocaisPendentesAprovacao(getUsuarioAutenticado()));
        return "/perfil/locaisPendentes";
    }

    @RequestMapping(value = "/mostrarCaixaConvidarAmigosFacebook", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public String mostrarCaixaConvidarAmigosFacebook(final Model model) {
        final List<UsuarioConviteFacebookView> usuarios = this.usuarioService.usuariosParaConviteFacebook(getUsuarioAutenticado());
        if (CollectionUtils.isNotEmpty(usuarios)) {
            return "/convite/caixaConvidarAmigosFacebook";
        }
        return "";
    }

    @RequestMapping(value = "/mostrarNotificacoesRecentes/{perfil}")
    public String mostrarNotificacoesRecentes(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("notificacoes", this.notificacaoService.consultarNotificacoesRecentes(perfil));
        return "/perfil/notificacoesRecentes";
    }

    @RequestMapping(value = "/noticias/{perfil}")
    public String noticias(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("noticias", this.noticiaService.consultarNoticias(perfil));

        return "/perfil/noticias";
    }

    @RequestMapping(value = "/notificacoes/{perfil}")
    public String notificacoes(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("notificacoes", this.notificacaoService.consultarTodas(perfil));
        return "/perfil/todasNotificacoes";
    }

    @RequestMapping(value = "/pedidosDica/{perfil}")
    @PreAuthorize("isAuthenticated()")
    public String pedidosDica(@PathVariable final Usuario perfil, final Model model) {
        final List<PedidoDica> pedidos = this.pedidoDicaService.consultarTodosPedidosDica(perfil);
        model.addAttribute("quantidadePedidos", pedidos.size());
        model.addAttribute("pedidos", pedidos);
        return "/perfil/pedidosDica";
    }

    @RequestMapping(value = "/{perfil}")
    public String perfil(@PathVariable("perfil") final Usuario perfil, final Model model, @RequestParam(required = false) final String acao,
            final RedirectAttributes redirectAttributes) {
        if (perfil == null) {
            throw new NotFoundException("Usuário não encontrado.");
        }
        // Se for o perfil do usuario TripFans, retornar para a tela inicial (home)
        if (perfil.getId() == 1) {
            return "redirect:/home";
        } else if (Boolean.FALSE.equals(perfil.getAtivo()) && perfil.getIdFacebook() != null) {
            return "redirect:/perfil/fb/" + perfil.getIdFacebook();
        }
        model.addAttribute("perfil", perfil);
        model.addAttribute("perfilView", this.usuarioService.consultarPerfilUsuario(perfil));
        if (acao == null) {
            model.addAttribute("interessesView",
                    new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(perfil)));
        }
        return "/perfil/perfil";
    }

    @RequestMapping(value = "/fb/{idUsuarioFB}")
    public String perfilFacebook(@PathVariable final String idUsuarioFB, final Model model) {
        Amizade amizade = null;
        if (this.getUsuarioAutenticado() != null) {
            amizade = this.amizadeService.consultarAmizadePorIdFacebookAmigo(this.getUsuarioAutenticado().getId(), idUsuarioFB);
        }
        if (amizade != null) {
            if (amizade.isSomenteFacebook()) {
                model.addAttribute("perfilFB", new PerfilUsuarioExternoView(amizade));
                model.addAttribute("idUsuarioCriptografado", CriptografiaUtil.encrypt(getUsuarioAutenticado().getId().toString()));
                return "/perfil/perfilFacebook";
            } else {
                return perfil(amizade.getAmigo(), model, null, null);
            }
        } else {
            final Usuario usuarioAmizade = this.usuarioService.consultarUsuarioPorIdFacebook(idUsuarioFB);
            if (Boolean.TRUE.equals(usuarioAmizade.getAtivo())) {
                perfil(usuarioAmizade, model, null, null);
            }
            /* Não exibe o perfil facebook se não for amigo do usuario logado */
            else {
                /*model.addAttribute("perfilFB", new PerfilUsuarioExternoView(usuarioAmizade));
                return "/perfil/perfilFacebook";*/
                return "/erros/403";
            }
        }
        return null;
    }

    @RequestMapping(value = "/perguntas/{perfil}")
    public String perguntas(@PathVariable final Usuario perfil, @RequestParam(required = false) final Pergunta destaque, final Model model) {
        model.addAttribute("perguntas", this.perguntaService.consultarTodasPorUsuario(perfil));
        model.addAttribute("perguntaDestaque", destaque);
        return "/perfil/perguntas";
    }

    @RequestMapping(value = "/planos/{perfil}")
    public String planos(@PathVariable final Usuario perfil, final Model model) {
        return "/perfil/planos";
    }

    @RequestMapping(value = "/popover/{perfil}")
    public String popover(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("userPopover", perfil);
        // urlFotoPerfil
        return "/perfil/popover";
    }

    @RequestMapping(value = "/postarComentario")
    @PreAuthorize("isAuthenticated()")
    public String postarComentario(@RequestParam("perfil") final Usuario perfil, @RequestParam final String texto,
            @RequestParam(required = false) final boolean postarNoFacebook, final Model model) {
        this.comentarioService.postarComentario(this.getUsuarioAutenticado(), TipoComentario.PERFIL, perfil.getId(), null, texto, postarNoFacebook);
        model.addAttribute("comentarios", this.comentarioService.consultarComentariosPerfil(perfil.getId()));
        return "/perfil/listaComentarios";
    }

    @RequestMapping(value = "/preConsultaContatosGoogle")
    @PreAuthorize("isAuthenticated()")
    public String preConsultaContatosGoogle(final Model model) throws Exception, IOException {
        return "/perfil/aguardarListaContatos";
    }

    @RequestMapping(value = "/principal/{perfil}")
    public String principal(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("perfil", perfil);

        final boolean isFriend = this.securityService.isFriend(perfil.getId());
        // se for amigo, mostrar as amizades, atividades e viagens
        List<Viagem> viagens = this.viagemService.consultarViagensPublicas(perfil);
        if (isFriend) {
            model.addAttribute("amizades", this.amizadeService.consultarAmizadesAtivasRecentemente(perfil, 9));
            model.addAttribute("atividades", this.acaoUsuarioService.consultarAcoesUsuario(perfil, 5));
            if (perfil.equals(getUsuarioAutenticado())) {
                viagens = this.viagemService.consultarTodasViagensUsuario(perfil);
            } else {
                viagens = this.viagemService.consultarTodasViagensAmigosPodemVer(perfil);
            }
        } else {
            viagens = this.viagemService.consultarViagensPublicas(perfil);
        }

        model.addAttribute("viagens", viagens);

        model.addAttribute("interessesView", new InteressesPreferenciasViagemView(this.interessesViagemService.consultarInteressesDoUsuario(perfil)));

        if (getUsuarioAutenticado() != null && perfil.getConectadoFacebook()) {
            model.addAttribute("idUsuarioCriptografado", CriptografiaUtil.encrypt(perfil.getId().toString()));
            model.addAttribute("usuariosParaConvidar", this.usuarioService.usuariosParaConviteFacebook(getUsuarioAutenticado()));
        }

        return "/perfil/principal";
    }

    @RequestMapping(value = "/salvarInteressesUsuarioEmCidade")
    public @ResponseBody JSONResponse salvarInteressesUsuarioEmCidade(InteresseUsuarioEmLocal interesseUsuarioEmLocal, final Model model) {
        final Long idLocal = interesseUsuarioEmLocal.getLocal().getId();
        interesseUsuarioEmLocal = this.interesseUsuarioEmLocalService.salvarInteresseUsuarioEmLocal(interesseUsuarioEmLocal);
        if (interesseUsuarioEmLocal == null) {
            // Retorna o ID do local para poder remover do Mapa
            return new JSONResponse(Collections.singletonList(idLocal));
        }
        return new JSONResponse(Collections.singletonList(interesseUsuarioEmLocal), this.jsonFields);
    }

    @RequestMapping(value = "/amigos/{perfil}/solicitacoesAmizade")
    @PreAuthorize("isAuthenticated()")
    public String solicitacoesAmizade(@PathVariable final Usuario perfil, @RequestParam(required = false) final String acao,
            final RedirectAttributes redirectAttributes, final Model model) {
        model.addAttribute("amizades", this.amizadeService.consultarAmizadesVerificandoConvitesTripfans(perfil));
        model.addAttribute("convitesRecebidosPendentes", this.conviteService.convitesAmizadeRecebidosPendentes(perfil.getId()));
        model.addAttribute("convitesEnviadosPendentes", this.conviteService.convitesAmizadeEnviadosPendentes(perfil.getId()));
        model.addAttribute("pagina", "amigos");
        model.addAttribute("acao", "solicitacoesAmizade");
        return this.perfil(perfil, model, "solicitacoesAmizade", redirectAttributes);
    }

    @RequestMapping(value = "/solicitarAmizadeContato", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage solicitarAmizadeContato(final String enderecoEmail) {
        final Usuario usuarioConvidado = this.usuarioService.consultarUsuarioPorUsername(enderecoEmail);
        if (usuarioConvidado != null) {
            this.conviteService.convidarParaAmizade(getUsuarioAutenticado(), usuarioConvidado);
            return this.success(String.format("Convite de amizade enviado!"));
        }
        return this.statusMessage;
    }

    @RequestMapping(value = "/viagens/{perfil}")
    @PreAuthorize("@securityService.isFriend(#perfil.id)")
    public String viagens(@PathVariable final Usuario perfil, final Model model) {
        model.addAttribute("viagens", this.viagemService.consultarTodasViagensAmigosPodemVer(perfil));
        return "/perfil/viagens";
    }
}
