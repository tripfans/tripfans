package br.com.fanaticosporviagens.usuario.controller;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.GenericTypeResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.DuplicateConnectionException;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.support.OAuth1ConnectionFactory;
import org.springframework.social.connect.support.OAuth2ConnectionFactory;
import org.springframework.social.connect.web.ConnectInterceptor;
import org.springframework.social.connect.web.ConnectSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.web.servlet.view.RedirectView;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.interceptor.DisconnectInterceptor;
import br.com.fanaticosporviagens.infra.interceptor.FacebookAfterConnectInterceptor;
import br.com.fanaticosporviagens.infra.interceptor.FoursquareAfterConnectInterceptor;
import br.com.fanaticosporviagens.infra.interceptor.GoogleAfterConnectInterceptor;
import br.com.fanaticosporviagens.infra.interceptor.TwitterAfterConnectInterceptor;
import br.com.fanaticosporviagens.infra.spring.config.SpringBeansProvider;

@Controller
@RequestMapping("/connect")
public class ConnectController extends BaseController {

    private static final String DUPLICATE_CONNECTION_ATTRIBUTE = "social.addConnection.duplicate";

    private static final String PROVIDER_ERROR_ATTRIBUTE = "social.provider.error";

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private static ConnectionFactoryLocator getConnectionFactoryLocator() {
        return SpringBeansProvider.getBean(ConnectionFactoryLocator.class);
    }

    private static ConnectionRepository getConnectionRepository() {
        return getConnectionRepository(getAuthentication().getName());
    }

    private static ConnectionRepository getConnectionRepository(final String username) {
        final UsersConnectionRepository usersConnectionRepository = SpringBeansProvider.getBean(UsersConnectionRepository.class);
        if (usersConnectionRepository != null && username != null) {
            return usersConnectionRepository.createConnectionRepository(username);
        }
        return null;
    }

    private final MultiValueMap<Class<?>, ConnectInterceptor<?>> connectInterceptors = new LinkedMultiValueMap<Class<?>, ConnectInterceptor<?>>();

    private final MultiValueMap<Class<?>, DisconnectInterceptor<?>> disconnectInterceptors = new LinkedMultiValueMap<Class<?>, DisconnectInterceptor<?>>();

    private final ConnectSupport webSupport = new ConnectSupport();

    public ConnectController() {
        this.addInterceptor(new FacebookAfterConnectInterceptor());
        this.addDisconnectInterceptor(new FacebookAfterConnectInterceptor());
        this.addInterceptor(new FoursquareAfterConnectInterceptor());
        this.addDisconnectInterceptor(new FoursquareAfterConnectInterceptor());
        this.addInterceptor(new GoogleAfterConnectInterceptor());
        this.addDisconnectInterceptor(new GoogleAfterConnectInterceptor());
        this.addInterceptor(new TwitterAfterConnectInterceptor());
        this.addDisconnectInterceptor(new TwitterAfterConnectInterceptor());
    }

    /**
     * Adds a DisconnectInterceptor to receive callbacks during the disconnection process. Useful for programmatic configuration.
     * 
     * @param interceptor
     *            the connect interceptor to add
     */
    public void addDisconnectInterceptor(final DisconnectInterceptor<?> interceptor) {
        final Class<?> serviceApiType = GenericTypeResolver.resolveTypeArgument(interceptor.getClass(), DisconnectInterceptor.class);
        this.disconnectInterceptors.add(serviceApiType, interceptor);
    }

    /**
     * Adds a ConnectInterceptor to receive callbacks during the connection process. Useful for programmatic configuration.
     * 
     * @param interceptor
     *            the connect interceptor to add
     */
    public void addInterceptor(final ConnectInterceptor<?> interceptor) {
        final Class<?> serviceApiType = GenericTypeResolver.resolveTypeArgument(interceptor.getClass(), ConnectInterceptor.class);
        this.connectInterceptors.add(serviceApiType, interceptor);
    }

    /**
     * Process a connect form submission by commencing the process of establishing a connection to the provider on behalf of the member. For OAuth1,
     * fetches a new request token from the provider, temporarily stores it in the session, then redirects the member to the provider's site for
     * authorization. For OAuth2, redirects the user to the provider's site for authorization.
     */
    @RequestMapping(value = "/{providerId}", method = { RequestMethod.POST, RequestMethod.GET })
    public RedirectView connect(@PathVariable final String providerId, final NativeWebRequest request) {
        final ConnectionFactory<?> connectionFactory = getConnectionFactoryLocator().getConnectionFactory(providerId);
        setRedirectAttributes(request);
        final MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
        this.preConnect(connectionFactory, parameters, request);
        try {
            return new RedirectView(this.webSupport.buildOAuthUrl(connectionFactory, request, parameters));
        } catch (final Exception e) {
            request.setAttribute(PROVIDER_ERROR_ATTRIBUTE, e, RequestAttributes.SCOPE_SESSION);
            return this.connectionStatusRedirect(providerId, request);
        }
    }

    /**
     * Render the status of connections across all providers to the user as HTML in their web browser.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String connectionStatus(final NativeWebRequest request, final Model model) {
        this.setNoCache(request);
        this.processFlash(request, model);
        final Map<String, List<Connection<?>>> connections = getConnectionRepository().findAllConnections();
        model.addAttribute("providerIds", getConnectionFactoryLocator().registeredProviderIds());
        model.addAttribute("connectionMap", connections);
        return this.connectView();
    }

    /**
     * Process the authorization callback from an OAuth 1 service provider. Called after the user authorizes the connection, generally done by having
     * he or she click "Allow" in their web browser at the provider's site. On authorization verification, connects the user's local account to the
     * account they hold at the service provider Removes the request token from the session since it is no longer valid after the connection is
     * established.
     */
    @RequestMapping(value = "/{providerId}", method = RequestMethod.GET, params = "oauth_token")
    public RedirectView oauth1Callback(@PathVariable final String providerId, final NativeWebRequest request) {
        try {
            final OAuth1ConnectionFactory<?> connectionFactory = (OAuth1ConnectionFactory<?>) getConnectionFactoryLocator().getConnectionFactory(
                    providerId);
            final Connection<?> connection = this.webSupport.completeConnection(connectionFactory, request);
            this.addConnection(connection, connectionFactory, request);
        } catch (final Exception e) {
            request.setAttribute(PROVIDER_ERROR_ATTRIBUTE, e, RequestAttributes.SCOPE_SESSION);
            // logger.warn("Exception while handling OAuth1 callback (" + e.getMessage() + "). Redirecting to " + providerId
            // +" connection status page.");
        }
        return this.connectionStatusRedirect(providerId, request);
    }

    /**
     * Render the status of the connections to the service provider to the user as HTML in their web browser.
     */
    /*
     * @RequestMapping(value = "/{providerId}", method = RequestMethod.GET) public String connectionStatus(@PathVariable final String providerId,
     * final NativeWebRequest request, final Model model) { this.setNoCache(request); this.processFlash(request, model); final List<Connection<?>>
     * connections = getConnectionRepository().findConnections(providerId); if (connections.isEmpty()) { return this.connectView(providerId); } else {
     * model.addAttribute("connections", connections); return this.connectedView(providerId); } }
     */

    /**
     * Process the authorization callback from an OAuth 2 service provider. Called after the user authorizes the connection, generally done by having
     * he or she click "Allow" in their web browser at the provider's site. On authorization verification, connects the user's local account to the
     * account they hold at the service provider.
     */
    @RequestMapping(value = "/{providerId}", method = RequestMethod.GET, params = "code")
    public RedirectView oauth2Callback(@PathVariable final String providerId, final NativeWebRequest request, final HttpServletResponse response) {
        try {
            final OAuth2ConnectionFactory<?> connectionFactory = (OAuth2ConnectionFactory<?>) getConnectionFactoryLocator().getConnectionFactory(
                    providerId);
            final Connection<?> connection = this.webSupport.completeConnection(connectionFactory, request);
            this.addConnection(connection, connectionFactory, request);
        } catch (final Exception e) {
            e.printStackTrace();
            request.setAttribute(PROVIDER_ERROR_ATTRIBUTE, e, RequestAttributes.SCOPE_SESSION);
            // logger.warn("Exception while handling OAuth2 callback (" + e.getMessage() + "). Redirecting to " + providerId
            // +" connection status page.");
        }
        return this.connectionStatusRedirect(providerId, request);
    }

    /**
     * Remove a single provider connection associated with a user account. The user has decided they no longer wish to use the service provider
     * account from this application. Note: requires {@link HiddenHttpMethodFilter} to be registered with the '_method' request parameter set to
     * 'DELETE' to convert web browser POSTs to DELETE requests.
     */
    @RequestMapping(value = "/{providerId}/{providerUserId}", method = RequestMethod.DELETE)
    public RedirectView removeConnection(@PathVariable final String providerId, @PathVariable final String providerUserId,
            final NativeWebRequest request) {
        final ConnectionFactory<?> connectionFactory = getConnectionFactoryLocator().getConnectionFactory(providerId);
        setRedirectAttributes(request);
        this.preDisconnect(connectionFactory, request);
        getConnectionRepository().removeConnection(new ConnectionKey(providerId, providerUserId));
        this.postDisconnect(connectionFactory, request);
        return this.connectionStatusRedirect(providerId, request);
    }

    /**
     * Remove all provider connections for a user account. The user has decided they no longer wish to use the service provider from this application.
     * Note: requires {@link HiddenHttpMethodFilter} to be registered with the '_method' request parameter set to 'DELETE' to convert web browser
     * POSTs to DELETE requests.
     */
    @RequestMapping(value = "/{providerId}", method = RequestMethod.DELETE)
    public RedirectView removeConnections(@PathVariable final String providerId, final NativeWebRequest request) {
        final ConnectionFactory<?> connectionFactory = getConnectionFactoryLocator().getConnectionFactory(providerId);
        setRedirectAttributes(request);
        this.preDisconnect(connectionFactory, request);
        getConnectionRepository().removeConnections(providerId);
        this.postDisconnect(connectionFactory, request);
        return this.connectionStatusRedirect(providerId, request);
    }

    /**
     * Configures the base secure URL for the application this controller is being used in e.g. <code>https://myapp.com</code>. Defaults to null. If
     * specified, will be used to generate OAuth callback URLs. If not specified, OAuth callback URLs are generated from web request info. You may
     * wish to set this property if requests into your application flow through a proxy to your application server. In this case, the request URI may
     * contain a scheme, host, and/or port value that points to an internal server not appropriate for an external callback URL. If you have this
     * problem, you can set this property to the base external URL for your application and it will be used to construct the callback URL instead.
     * 
     * @param applicationUrl
     *            the application URL value
     */
    public void setApplicationUrl(final String applicationUrl) {
        this.webSupport.setApplicationUrl(applicationUrl);
    }

    /**
     * Configure the list of connect interceptors that should receive callbacks during the connection process. Convenient when an instance of this
     * class is configured using a tool that supports JavaBeans-based configuration.
     * 
     * @param interceptors
     *            the connect interceptors to add
     */
    public void setConnectInterceptors(final List<ConnectInterceptor<?>> interceptors) {
        for (final ConnectInterceptor<?> interceptor : interceptors) {
            this.addInterceptor(interceptor);
        }
    }

    /**
     * Configure the list of discconnect interceptors that should receive callbacks when connections are removed. Convenient when an instance of this
     * class is configured using a tool that supports JavaBeans-based configuration.
     * 
     * @param interceptors
     *            the connect interceptors to add
     */
    public void setDisconnectInterceptors(final List<DisconnectInterceptor<?>> interceptors) {
        for (final DisconnectInterceptor<?> interceptor : interceptors) {
            this.addDisconnectInterceptor(interceptor);
        }
    }

    /**
     * Returns the view name of a page to display for a provider when the user is connected to the provider. Typically this page would allow the user
     * to disconnect from the provider. Defaults to "connect/{providerId}Connected". May be overridden to return a custom view name.
     * 
     * @param providerId
     *            the ID of the provider to display the connection status for.
     */
    protected String connectedView(final String providerId) {
        return this.getViewPath() + providerId + "Connected";
    }

    /**
     * Returns a RedirectView with the URL to redirect to after a connection is created or deleted. Defaults to "/connect/{providerId}" relative to
     * DispatcherServlet's path. May be overridden to handle custom redirection needs.
     * 
     * @param providerId
     *            the ID of the provider for which a connection was created or deleted.
     * @param request
     *            the NativeWebRequest used to access the servlet path when constructing the redirect path.
     */
    protected RedirectView connectionStatusRedirect(final String providerId, final NativeWebRequest request) {
        String path = "/conta/minhaConta?tab=config";
        try {
            // Recuperando a URL em que o usuario chamou o connect/{providerId}
            if (request.getAttribute(PROVIDER_ERROR_ATTRIBUTE, RequestAttributes.SCOPE_SESSION) == null) {
                if (request.getAttribute("redirectUrl", RequestAttributes.SCOPE_SESSION) != null) {
                    path = (String) request.getAttribute("redirectUrl", RequestAttributes.SCOPE_SESSION);
                    request.removeAttribute("redirectUrl", RequestAttributes.SCOPE_SESSION);
                }
            } else {
                if (request.getAttribute("redirectUrlOnError", RequestAttributes.SCOPE_SESSION) != null) {
                    path = (String) request.getAttribute("redirectUrlOnError", RequestAttributes.SCOPE_SESSION);
                    request.removeAttribute("redirectUrlOnError", RequestAttributes.SCOPE_SESSION);
                } else {
                    final HttpServletRequest servletRequest = request.getNativeRequest(HttpServletRequest.class);
                    final String urlAnterior = servletRequest.getHeader("referer");
                    if (urlAnterior != null && !urlAnterior.contains("/conta/minhaConta")) {
                        path = urlAnterior;
                    }
                }
            }
            if (request.getAttribute(DUPLICATE_CONNECTION_ATTRIBUTE, RequestAttributes.SCOPE_SESSION) != null) {
                // Multiple entries indicate that multiple application users are associated with the connection
                // and handled as an error by ProviderSignInController.
                this.error("Já existe um outro usuário conectado com esta conta do %s. Se você está usando uma máquina/dispositivo compartilhado, "
                        + "por favor verifique se sua conta está corretamente conectada.", providerId.toUpperCase());
                request.removeAttribute(DUPLICATE_CONNECTION_ATTRIBUTE, RequestAttributes.SCOPE_SESSION);
            } else if (request.getAttribute(PROVIDER_ERROR_ATTRIBUTE, RequestAttributes.SCOPE_SESSION) != null) {
                this.error("Ocorreu um problema ao tentar se conectar ao %s. Tente novamente mais tarde.", providerId.toUpperCase());
                request.removeAttribute(PROVIDER_ERROR_ATTRIBUTE, RequestAttributes.SCOPE_SESSION);
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return new RedirectView(path, true);
    }

    /**
     * Returns the view name of a general connection status page, typically displaying the user's connection status for all providers. Defaults to
     * "/connect/status". May be overridden to return a custom view name.
     */
    protected String connectView() {
        return this.getViewPath() + "status";
    }

    /**
     * Returns the view name of a page to display for a provider when the user is not connected to the provider. Typically this page would offer the
     * user an opportunity to create a connection with the provider. Defaults to "connect/{providerId}Connect". May be overridden to return a custom
     * view name.
     * 
     * @param providerId
     *            the ID of the provider to display the connection status for.
     */
    protected String connectView(final String providerId) {
        return this.getViewPath() + providerId + "Connect";
    }

    private void addConnection(final Connection<?> connection, final ConnectionFactory<?> connectionFactory, final WebRequest request) {
        try {
            getConnectionRepository().addConnection(connection);
            this.postConnect(connectionFactory, connection, request);
        } catch (final DuplicateConnectionException e) {
            request.setAttribute(DUPLICATE_CONNECTION_ATTRIBUTE, e, RequestAttributes.SCOPE_SESSION);
        }
    }

    private void convertSessionAttributeToModelAttribute(final String attributeName, final WebRequest request, final Model model) {
        if (request.getAttribute(attributeName, RequestAttributes.SCOPE_SESSION) != null) {
            model.addAttribute(attributeName, Boolean.TRUE);
            request.removeAttribute(attributeName, RequestAttributes.SCOPE_SESSION);
        }
    }

    private String getViewPath() {
        return "connect/";
    }

    private List<ConnectInterceptor<?>> interceptingConnectionsTo(final ConnectionFactory<?> connectionFactory) {
        final Class<?> serviceType = GenericTypeResolver.resolveTypeArgument(connectionFactory.getClass(), ConnectionFactory.class);
        List<ConnectInterceptor<?>> typedInterceptors = this.connectInterceptors.get(serviceType);
        if (typedInterceptors == null) {
            typedInterceptors = Collections.emptyList();
        }
        return typedInterceptors;
    }

    private List<DisconnectInterceptor<?>> interceptingDisconnectionsTo(final ConnectionFactory<?> connectionFactory) {
        final Class<?> serviceType = GenericTypeResolver.resolveTypeArgument(connectionFactory.getClass(), ConnectionFactory.class);
        List<DisconnectInterceptor<?>> typedInterceptors = this.disconnectInterceptors.get(serviceType);
        if (typedInterceptors == null) {
            typedInterceptors = Collections.emptyList();
        }
        return typedInterceptors;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void postConnect(final ConnectionFactory<?> connectionFactory, final Connection<?> connection, final WebRequest request) {
        for (final ConnectInterceptor interceptor : this.interceptingConnectionsTo(connectionFactory)) {
            interceptor.postConnect(connection, request);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void postDisconnect(final ConnectionFactory<?> connectionFactory, final WebRequest request) {
        for (final DisconnectInterceptor interceptor : this.interceptingDisconnectionsTo(connectionFactory)) {
            interceptor.postDisconnect(connectionFactory, request);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void preConnect(final ConnectionFactory<?> connectionFactory, final MultiValueMap<String, String> parameters, final WebRequest request) {
        // Necessario para receber o refreshtoken do Google
        parameters.set("access_type", "offline");
        for (final ConnectInterceptor interceptor : this.interceptingConnectionsTo(connectionFactory)) {
            interceptor.preConnect(connectionFactory, parameters, request);
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void preDisconnect(final ConnectionFactory<?> connectionFactory, final WebRequest request) {
        for (final DisconnectInterceptor interceptor : this.interceptingDisconnectionsTo(connectionFactory)) {
            interceptor.preDisconnect(connectionFactory, request);
        }
    }

    private void processFlash(final WebRequest request, final Model model) {
        this.convertSessionAttributeToModelAttribute(DUPLICATE_CONNECTION_ATTRIBUTE, request, model);
        this.convertSessionAttributeToModelAttribute(PROVIDER_ERROR_ATTRIBUTE, request, model);
    }

    private void setNoCache(final NativeWebRequest request) {
        final HttpServletResponse response = request.getNativeResponse(HttpServletResponse.class);
        if (response != null) {
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 1L);
            response.setHeader("Cache-Control", "no-cache");
            response.addHeader("Cache-Control", "no-store");
        }
    }

    private void setRedirectAttributes(final NativeWebRequest request) {
        if (request.getParameter("redirectUrl") != null && !request.getParameter("redirectUrl").isEmpty()) {
            request.setAttribute("redirectUrl", request.getParameter("redirectUrl"), RequestAttributes.SCOPE_SESSION);
        }
        if (request.getParameter("redirectUrlOnError") != null && !request.getParameter("redirectUrlOnError").isEmpty()) {
            request.setAttribute("redirectUrlOnError", request.getParameter("redirectUrlOnError"), RequestAttributes.SCOPE_SESSION);
        }
    }

}
