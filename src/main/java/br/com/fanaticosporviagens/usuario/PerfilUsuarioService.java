package br.com.fanaticosporviagens.usuario;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.PerfilUsuario;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author André Thiago
 * 
 */
@Service
public class PerfilUsuarioService extends GenericCRUDService<PerfilUsuario, Long> {

    public PerfilUsuario consultarPerfilDoUsuario(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        return this.searchRepository.queryByNamedQueryUniqueResult("PerfilUsuario.consultarPerfilDoUsuario", parametros);
    }

}
