package br.com.fanaticosporviagens.usuario;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;

@Component
public class PosLoginService {

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private UsuarioService usuarioService;

    @Inject
    protected HttpSession httpSession;

    public void popularAtributosSessao(final Usuario usuario) {
        final List<Long> idsAmigos = this.amizadeService.consultarIdsAmigos(usuario);
        this.httpSession.setAttribute("IDS_AMIGOS", idsAmigos);
        this.httpSession.setAttribute("idUsuarioCriptografado", CriptografiaUtil.encrypt(usuario.getId().toString()));
        usuario.setDataUltimoLogin(new LocalDate());
        this.usuarioService.alterar(usuario);
    }

}
