package br.com.fanaticosporviagens.usuario;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.PreferenciasViagem;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author André Thiago
 * 
 */
@Service
public class PreferenciasViagemService extends GenericCRUDService<PreferenciasViagem, Long> {

    public PreferenciasViagem consultarPreferenciasViagemDoUsuario(final Usuario usuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("usuario", usuario);
        return this.searchRepository.queryByNamedQueryUniqueResult("PreferenciasViagem.consultarPreferenciasViagemDoUsuario", parametros);
    }

}
