package br.com.fanaticosporviagens.usuario.form;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class LoginForm {

    @Size(min = 6)
    private String password;

    private boolean rememberMe;

    @NotEmpty
    @Email
    private String username;

    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return this.username;
    }

    public boolean isRememberMe() {
        return this.rememberMe;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setRememberMe(final boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public void setUsername(final String username) {
        this.username = username;
    }
}
