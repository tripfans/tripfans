package br.com.fanaticosporviagens.usuario.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class CadastroForm {

    public static CadastroForm fromUsuario(final Usuario usuario) {
        return new CadastroForm(usuario.getPrimeiroNome(), usuario.getUltimoNome(), usuario.getCidadeResidencia());
    }

    private Cidade cidade;

    @NotEmpty
    private String displayName;

    private Boolean exibirGenero;

    private String genero;

    // Id do usuario
    private Long id;

    private String nome;

    @Size(min = 6)
    private String password;

    private String sobreNome;

    @NotNull
    private String username;

    public CadastroForm() {
    }

    public CadastroForm(final Long id) {
        this.id = id;
    }

    public CadastroForm(final String nome, final String sobreNome, final Cidade cidade) {
        this.nome = nome;
        this.sobreNome = sobreNome;
        this.cidade = cidade;
    }

    public Cidade getCidade() {
        return this.cidade;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public Boolean getExibirGenero() {
        return this.exibirGenero;
    }

    public String getGenero() {
        return this.genero;
    }

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public String getPassword() {
        return this.password;
    }

    public String getSobreNome() {
        return this.sobreNome;
    }

    public String getUsername() {
        return this.username;
    }

    public void setCidade(final Cidade cidade) {
        this.cidade = cidade;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public void setExibirGenero(final Boolean exibirGenero) {
        this.exibirGenero = exibirGenero;
    }

    public void setGenero(final String genero) {
        this.genero = genero;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setSobreNome(final String sobreNome) {
        this.sobreNome = sobreNome;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

}
