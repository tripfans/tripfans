package br.com.fanaticosporviagens.usuario.view;

import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class PerfilUsuarioExternoView {

    private String id;

    private String nome;

    private Integer quantidadeCidadesVisitadas;

    public PerfilUsuarioExternoView() {
    }

    public PerfilUsuarioExternoView(final Amizade amizade) {
        this.id = amizade.getIdAmigoFacebook();
        this.nome = amizade.getNomeAmigo();
    }

    public PerfilUsuarioExternoView(final Usuario usuario) {
        this.id = usuario.getIdFacebook();
        this.nome = usuario.getDisplayName();
    }

    public String getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }

    public Integer getQuantidadeCidadesVisitadas() {
        return this.quantidadeCidadesVisitadas;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setQuantidadeCidadesVisitadas(final Integer quantidadeCidadesVisitadas) {
        this.quantidadeCidadesVisitadas = quantidadeCidadesVisitadas;
    }
}
