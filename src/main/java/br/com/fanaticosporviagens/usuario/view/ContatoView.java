package br.com.fanaticosporviagens.usuario.view;

/**
 * Representa um contato de um usuario em contas externas (ex: Gmail)
 * 
 * @author Carlos Nascimento
 * 
 */
public class ContatoView {

    /**
     * Indica se o contato já é amigo do usuario
     */
    private boolean amigo = false;

    /**
     * Indica se o contato já está no TripFans
     */
    private boolean cadastradoTripfans = false;

    private String email;

    private String nome;

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final ContatoView other = (ContatoView) obj;
        if (this.email == null) {
            if (other.email != null)
                return false;
        } else if (!this.email.equals(other.email))
            return false;
        return true;
    }

    public String getEmail() {
        return this.email;
    }

    public String getNome() {
        return this.nome;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.email == null) ? 0 : this.email.hashCode());
        return result;
    }

    public boolean isAmigo() {
        return this.amigo;
    }

    public boolean isCadastradoTripfans() {
        return this.cadastradoTripfans;
    }

    public void setAmigo(final boolean amigo) {
        this.amigo = amigo;
    }

    public void setCadastradoTripfans(final boolean cadastradoTripfans) {
        this.cadastradoTripfans = cadastradoTripfans;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

}
