package br.com.fanaticosporviagens.usuario.view;

public class PerfilUsuarioView {

    private boolean amigo = false;

    private boolean amigoImportadoFacebook = false;

    private boolean amigoSomenteFacebook = false;

    private boolean convidouQuemEstaVendo = false;

    private boolean foiConvidadoPorQuemEstaVendo = false;

    private boolean visivel = false;

    public boolean isAmigo() {
        return this.amigo;
    }

    public boolean isAmigoImportadoFacebook() {
        return this.amigoImportadoFacebook;
    }

    public boolean isAmigoSomenteFacebook() {
        return this.amigoSomenteFacebook;
    }

    public boolean isConvidouQuemEstaVendo() {
        return this.convidouQuemEstaVendo;
    }

    public boolean isConviteAmizadePendente() {
        return this.foiConvidadoPorQuemEstaVendo || this.convidouQuemEstaVendo;
    }

    public boolean isFoiConvidadoPorQuemEstaVendo() {
        return this.foiConvidadoPorQuemEstaVendo;
    }

    public boolean isVisivel() {
        return this.visivel;
    }

    public void setAmigo(final boolean amigo) {
        this.amigo = amigo;
    }

    public void setAmigoImportadoFacebook(boolean amigoImportadoFacebook) {
        this.amigoImportadoFacebook = amigoImportadoFacebook;
    }

    public void setAmigoSomenteFacebook(boolean amigoSomenteFacebook) {
        this.amigoSomenteFacebook = amigoSomenteFacebook;
    }

    public void setConvidouQuemEstaVendo(final boolean convidouQuemEstaVendo) {
        this.convidouQuemEstaVendo = convidouQuemEstaVendo;
    }

    public void setFoiConvidadoPorQuemEstaVendo(final boolean foiConvidadoPorQuemEstaVendo) {
        this.foiConvidadoPorQuemEstaVendo = foiConvidadoPorQuemEstaVendo;
    }

    public void setVisivel(final boolean visivel) {
        this.visivel = visivel;
    }

}
