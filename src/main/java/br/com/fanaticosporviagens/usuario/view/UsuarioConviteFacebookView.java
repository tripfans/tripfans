package br.com.fanaticosporviagens.usuario.view;

public class UsuarioConviteFacebookView {

    private String idFacebook;

    private String nome;

    public String getIdFacebook() {
        return this.idFacebook;
    }

    public String getNome() {
        return this.nome;
    }

    public String getUrlFotoAlbum() {
        return "";
    }

    public void setIdFacebook(final String idFacebook) {
        this.idFacebook = idFacebook;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

}
