package br.com.fanaticosporviagens.viagem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public enum ViagemListarModoVisao {
    MINHAS_VIAGENS(titulo("Minhas viagens"), ordem(1), necessitaUsuarioAutenticado(true), modoVisaoHabilitado(true)),
    VIAGENS_AGENCIAS(titulo("Viagens oferecidas por agências"), ordem(6), necessitaUsuarioAutenticado(false), modoVisaoHabilitado(true)),
    VIAGENS_AMIGOS(titulo("Viagens dos meus amigos"), ordem(3), necessitaUsuarioAutenticado(true), modoVisaoHabilitado(true)),
    VIAGENS_CONVIDADO(titulo("Viagens que fui convidado"), ordem(2), necessitaUsuarioAutenticado(true), modoVisaoHabilitado(true)),
    VIAGENS_PUBLICAS(titulo("Viagens públicas"), ordem(5), necessitaUsuarioAutenticado(false), modoVisaoHabilitado(true)),
    VIAGENS_TRIPFANS(titulo("Exemplos de viagens do TripFans"), ordem(4), necessitaUsuarioAutenticado(false), modoVisaoHabilitado(true));

    public static List<ViagemListarModoVisao> getVisoesOrdenadas() {
        final List<ViagemListarModoVisao> visoes = new ArrayList<ViagemListarModoVisao>();
        for (final ViagemListarModoVisao visao : ViagemListarModoVisao.values()) {
            if (visao.isModoVisaoHabilitado()) {
                visoes.add(visao);
            }
        }
        ordenarVisoes(visoes);

        return visoes;
    }

    public static List<ViagemListarModoVisao> getVisoesOrdenadasQueNaoPrecisaoDeUsuarioAutenticado() {
        final List<ViagemListarModoVisao> visoes = new ArrayList<ViagemListarModoVisao>();
        for (final ViagemListarModoVisao visao : ViagemListarModoVisao.values()) {
            if (visao.isModoVisaoHabilitado() && !visao.isNecessitaUsuarioAutenticado()) {
                visoes.add(visao);
            }
        }
        ordenarVisoes(visoes);

        return visoes;
    }

    private static boolean modoVisaoHabilitado(final boolean modoVisaoHabilitado) {
        return modoVisaoHabilitado;
    }

    private static boolean necessitaUsuarioAutenticado(final boolean necessitaUsuarioAutenticado) {
        return necessitaUsuarioAutenticado;
    }

    private static Integer ordem(final Integer ordem) {
        return ordem;
    }

    private static void ordenarVisoes(final List<ViagemListarModoVisao> visoes) {
        Collections.sort(visoes, new Comparator<ViagemListarModoVisao>() {

            @Override
            public int compare(final ViagemListarModoVisao visao1, final ViagemListarModoVisao visao2) {
                return visao1.getOrdem().compareTo(visao2.getOrdem());
            }
        });
    }

    private static String titulo(final String titulo) {
        return titulo;
    }

    private final String descricao;

    private final boolean modoVisaoHabilitado;

    private final boolean necessitaUsuarioAutenticado;

    private final Integer ordem;

    private ViagemListarModoVisao(final String descricao, final Integer ordem, final boolean necessitaUsuarioAutenticado,
            final boolean modoVisaoHabilitado) {
        this.descricao = descricao;
        this.necessitaUsuarioAutenticado = necessitaUsuarioAutenticado;
        this.modoVisaoHabilitado = modoVisaoHabilitado;
        this.ordem = ordem;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public Integer getOrdem() {
        return this.ordem;
    }

    public String getUrl() {
        return "/viagem/listar/" + this;
    }

    public boolean isModoVisaoHabilitado() {
        return this.modoVisaoHabilitado;
    }

    public boolean isNecessitaUsuarioAutenticado() {
        return this.necessitaUsuarioAutenticado;
    }
}
