package br.com.fanaticosporviagens.viagem;

public class ViagemPermissao {

    private boolean usuarioEOCriadorDaViagem;

    private boolean usuarioPodeAdicionarDia;

    private boolean usuarioPodeAlterarDadosDaViagem;

    private boolean usuarioPodeAlterarItem;

    private boolean usuarioPodeCopiarViagem;

    private boolean usuarioPodeExcluirDia;

    private boolean usuarioPodeExcluirItem;

    private boolean usuarioPodeExcluirViagem;

    private boolean usuarioPodeIncluirItem;

    private boolean usuarioPodeIncluirViagem;

    private boolean usuarioPodeMoverItem;

    private boolean usuarioPodeMovimentarItem;

    private boolean usuarioPodeVerViagem;

    public boolean isUsuarioEOCriadorDaViagem() {
        return this.usuarioEOCriadorDaViagem;
    }

    public boolean isUsuarioPodeAdicionarDia() {
        return this.usuarioPodeAdicionarDia;
    }

    public boolean isUsuarioPodeAlterarDadosDaViagem() {
        return this.usuarioPodeAlterarDadosDaViagem;
    }

    public boolean isUsuarioPodeAlterarItem() {
        return this.usuarioPodeAlterarItem;
    }

    public boolean isUsuarioPodeCopiarViagem() {
        return this.usuarioPodeCopiarViagem;
    }

    public boolean isUsuarioPodeExcluirDia() {
        return this.usuarioPodeExcluirDia;
    }

    public boolean isUsuarioPodeExcluirItem() {
        return this.usuarioPodeExcluirItem;
    }

    public boolean isUsuarioPodeExcluirViagem() {
        return this.usuarioPodeExcluirViagem;
    }

    public boolean isUsuarioPodeIncluirItem() {
        return this.usuarioPodeIncluirItem;
    }

    public boolean isUsuarioPodeIncluirViagem() {
        return this.usuarioPodeIncluirViagem;
    }

    public boolean isUsuarioPodeMoverItem() {
        return this.usuarioPodeMoverItem;
    }

    public boolean isUsuarioPodeMovimentarItem() {
        return this.usuarioPodeMovimentarItem;
    }

    public boolean isUsuarioPodeVerViagem() {
        return this.usuarioPodeVerViagem;
    }

    public void setUsuarioEOCriadorDaViagem(final boolean usuarioEOCriadorDaViagem) {
        this.usuarioEOCriadorDaViagem = usuarioEOCriadorDaViagem;
    }

    public void setUsuarioPodeIncluirViagem(final boolean usuarioPodeIncluirViagem) {
        this.usuarioPodeIncluirViagem = usuarioPodeIncluirViagem;
    }

    void setUsuarioPodeAdicionarDia(final boolean usuarioPodeAdicionarDia) {
        this.usuarioPodeAdicionarDia = usuarioPodeAdicionarDia;
    }

    void setUsuarioPodeAlterarDadosDaViagem(final boolean usuarioPodeAlterarDadosDaViagem) {
        this.usuarioPodeAlterarDadosDaViagem = usuarioPodeAlterarDadosDaViagem;
    }

    void setUsuarioPodeAlterarItem(final boolean usuarioPodeAlterarItem) {
        this.usuarioPodeAlterarItem = usuarioPodeAlterarItem;
    }

    void setUsuarioPodeCopiarViagem(final boolean usuarioPodeCopiarViagem) {
        this.usuarioPodeCopiarViagem = usuarioPodeCopiarViagem;
    }

    void setUsuarioPodeExcluirDia(final boolean usuarioPodeExcluirDia) {
        this.usuarioPodeExcluirDia = usuarioPodeExcluirDia;
    }

    void setUsuarioPodeExcluirItem(final boolean usuarioPodeExcluirItem) {
        this.usuarioPodeExcluirItem = usuarioPodeExcluirItem;
    }

    void setUsuarioPodeExcluirViagem(final boolean usuarioPodeExcluirViagem) {
        this.usuarioPodeExcluirViagem = usuarioPodeExcluirViagem;
    }

    void setUsuarioPodeIncluirItem(final boolean usuarioPodeIncluirItem) {
        this.usuarioPodeIncluirItem = usuarioPodeIncluirItem;
    }

    void setUsuarioPodeMoverItem(final boolean usuarioPodeMoverItem) {
        this.usuarioPodeMoverItem = usuarioPodeMoverItem;
    }

    void setUsuarioPodeMovimentarItem(final boolean usuarioPodeMovimentarItem) {
        this.usuarioPodeMovimentarItem = usuarioPodeMovimentarItem;
    }

    void setUsuarioPodeVerViagem(final boolean usuarioPodeVerViagem) {
        this.usuarioPodeVerViagem = usuarioPodeVerViagem;
    }

}
