package br.com.fanaticosporviagens.viagem.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.GrupoEmpresarialService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.Agencia;
import br.com.fanaticosporviagens.model.entity.Alimentacao;
import br.com.fanaticosporviagens.model.entity.AluguelVeiculo;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.DetalheTrechoVoo;
import br.com.fanaticosporviagens.model.entity.GrupoEmpresarial;
import br.com.fanaticosporviagens.model.entity.Hospedagem;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.TrechoTransporte;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.VisitaAtracao;
import br.com.fanaticosporviagens.model.entity.Voo;
import br.com.fanaticosporviagens.viagem.model.service.AtividadeService;
import br.com.fanaticosporviagens.viagem.model.service.PlanoViagemService;

/**
 * @author Pedro Sebba (sebbaweb@gmail.com)
 * @author Carlos Nascimento
 */

@Controller
// TODO: lembrar de mudar o nome da classe para planejamentoController
@RequestMapping("/planejamento")
public class AtividadeController extends BaseController {

    @Autowired
    private AtividadeService atividadeService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private GrupoEmpresarialService grupoEmpresarialService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PlanoViagemService planoViagemService;

    @RequestMapping(value = "/voo/adicionarTrecho", method = { RequestMethod.POST })
    public String adicionarTrecho(final Model model, final HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        final Voo voo = new Voo();
        voo.addTrecho(new TrechoTransporte<DetalheTrechoVoo>());

        final String indice = request.getParameter("indice");
        model.addAttribute("voo", voo);
        model.addAttribute("trechos[" + indice + "]", new TrechoTransporte<DetalheTrechoVoo>());

        redirectAttributes.addFlashAttribute("indice", indice);
        request.setAttribute("indice", indice);
        return "/planoViagem/trechoVoo";
    }

    @RequestMapping(value = "/consultarAgenciasPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<Agencia> consultarAgenciasPorNome(@RequestParam("term") final String nome) {
        final List<Agencia> agencias = this.localService.consultarAgenciasPorNome(nome);
        return agencias;
    }

    @RequestMapping(value = "/consultarCompanhias", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<GrupoEmpresarial> consultarCompanhias(@RequestParam("term") final String nome) {
        return this.grupoEmpresarialService.consultarTodos();
    }

    @RequestMapping(value = "/consultarHoteisPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<Hotel> consultarHoteisPorNome(@RequestParam("term") final String nome,
            @RequestParam(value = "idViagem", required = false) final PlanoViagem planoViagem) {
        final List<Hotel> hoteis = this.localService.consultarHoteisPorNome(nome, planoViagem != null ? planoViagem.getCidades() : null);
        return hoteis;
    }

    @RequestMapping(value = "/consultarLocadoras", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<GrupoEmpresarial> consultarLocadoras(@RequestParam("term") final String nome) {
        return this.grupoEmpresarialService.consultarTodos();
    }

    @RequestMapping(value = "/aluguelVeiculo/editar/{id}", method = { RequestMethod.GET })
    public String editarAluguelVeiculo(@PathVariable(value = "id") final AluguelVeiculo aluguelVeiculo, final Model model) {
        model.addAttribute("atividade", aluguelVeiculo);
        final PlanoViagem planoViagem = aluguelVeiculo.getViagem();
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/aluguelVeiculo";
    }

    @RequestMapping(value = "/hospedagem/editar/{id}", method = { RequestMethod.GET })
    public String editarHospedagem(@PathVariable(value = "id") final Hospedagem hospedagem, final Model model) {
        model.addAttribute("hospedagem", hospedagem);
        final PlanoViagem planoViagem = hospedagem.getViagem();
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/hospedagem";
    }

    @RequestMapping(value = "/restaurante/editar/{id}", method = { RequestMethod.GET })
    public String editarRestaurante(@PathVariable(value = "id") final Alimentacao alimentacao, final Model model) {
        model.addAttribute("atividade", alimentacao);
        final PlanoViagem planoViagem = alimentacao.getViagem();
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/restaurante";
    }

    @RequestMapping(value = "/atracao/editar/{id}", method = { RequestMethod.GET })
    public String editarRestaurante(@PathVariable(value = "id") final VisitaAtracao atracao, final Model model) {
        model.addAttribute("atividade", atracao);
        final PlanoViagem planoViagem = atracao.getViagem();
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/atracao";
    }

    @RequestMapping(value = "/voo/editar/{id}", method = { RequestMethod.GET })
    public String editarVoo(@PathVariable(value = "id") final Voo voo, final Model model) {
        model.addAttribute("voo", voo);
        model.addAttribute(voo.getViagem());
        return "/planoViagem/planejamento/voo";
    }

    @RequestMapping(value = "/atividade/excluir/{idAtividade}", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public JSONResponse excluirAtividade(@PathVariable("idAtividade") final AtividadePlano atividadePlano, final Model model) {
        this.atividadeService.excluir(atividadePlano);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/atividade/planoViagem/{idViagem}", method = { RequestMethod.GET })
    public String formAtividade(@PathVariable final Long idViagem, final Model model) {
        if (!model.containsAttribute("atividade")) {
            // model.addAttribute("atividade", new AtividadePlano());
        }
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planejamentoviagem/hospedagem";
    }

    @RequestMapping(value = "/hospedagem/planoViagem/{idViagem}", method = { RequestMethod.GET })
    public String formHospedagem(@PathVariable final Long idViagem, final Model model) {
        if (!model.containsAttribute("hospedagem")) {
            model.addAttribute("hospedagem", new Hospedagem());
        }
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/hospedagem";
    }

    @RequestMapping(value = "/aluguelVeiculo/incluir/{idViagem}", method = { RequestMethod.GET })
    public String incluirAluguelVeiculo(@PathVariable final Long idViagem, final Model model) {
        final AluguelVeiculo aluguelVeiculo = new AluguelVeiculo();
        model.addAttribute("atividade", aluguelVeiculo);
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/aluguelVeiculo";
    }

    @RequestMapping(value = "/incluirAtividade", method = { RequestMethod.POST })
    public String incluirAtividade(@Valid @ModelAttribute("atividade") final AtividadePlano atividadePlano, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("viagem", atividadePlano.getViagem());
            return "/planejamentoviagem/atividade";
        }
        this.planoViagemService.incluirAtividade(atividadePlano);
        /*if (atividade.possuiFotos()) {
            this.fotoService.incluirFotos(atividade);
        }*/
        return "redirect:/planoViagem/listarAtividades/planoViagem/" + atividadePlano.getViagem().getId();
    }

    @RequestMapping(value = "/atracao/incluir/{idViagem}", method = { RequestMethod.GET })
    public String incluirAtracao(@PathVariable final Long idViagem, final Model model) {
        final VisitaAtracao atracao = new VisitaAtracao();
        model.addAttribute("atividade", atracao);
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/atracao";
    }

    @RequestMapping(value = "/restaurante/incluir/{idViagem}", method = { RequestMethod.GET })
    public String incluirRestaurante(@PathVariable final Long idViagem, final Model model) {
        final Alimentacao alimentacao = new Alimentacao();
        model.addAttribute("atividade", alimentacao);
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/restaurante";
    }

    @RequestMapping(value = "/voo/incluir/{idViagem}", method = { RequestMethod.GET })
    public String incluirVoo(@PathVariable final Long idViagem, final Model model) {
        final Voo voo = new Voo();
        // Adiciona um trecho vazio
        voo.addTrecho(new TrechoTransporte<DetalheTrechoVoo>());
        model.addAttribute("voo", voo);
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/voo";
    }

    @RequestMapping(value = "/salvarAluguelVeiculo", method = { RequestMethod.POST })
    public String salvarAluguelVeiculo(@Valid @ModelAttribute("aluguelVeiculo") final AluguelVeiculo atividade, final BindingResult result,
            final Model model) {

        this.atividadeService.salvar(atividade);
        final PlanoViagem planoViagem = atividade.getViagem();

        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

    @RequestMapping(value = "/salvarAtividade", method = { RequestMethod.POST })
    public String salvarAtividade(@Valid @ModelAttribute("atividade") final AtividadePlano atividadePlano, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {

        this.atividadeService.salvar(atividadePlano);
        final PlanoViagem planoViagem = atividadePlano.getViagem();

        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

    @RequestMapping(value = "/salvarAtracao", method = { RequestMethod.POST })
    public String salvarAtracao(@Valid @ModelAttribute("atracao") final VisitaAtracao atividade, final BindingResult result, final Model model) {

        this.atividadeService.salvar(atividade);
        final PlanoViagem planoViagem = atividade.getViagem();

        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

    @RequestMapping(value = "/salvarHospedagem", method = { RequestMethod.POST })
    public String salvarHospedagem(@Valid @ModelAttribute("hospedagem") final Hospedagem hospedagem, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            model.addAttribute("hospedagem", hospedagem);
            return "/planejamentoviagem/hospedagem";
        }
        this.planoViagemService.salvarHospedagem(hospedagem);
        final PlanoViagem planoViagem = hospedagem.getViagem();
        /*if (hospedagem.possuiFotos()) {
            this.fotoService.incluirFotos(hospedagem);
        }*/
        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

    @RequestMapping(value = "/salvarRestaurante", method = { RequestMethod.POST })
    public String salvarRestaurante(@Valid @ModelAttribute("restaurante") final Alimentacao atividade, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {

        this.atividadeService.salvar(atividade);
        final PlanoViagem planoViagem = atividade.getViagem();

        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

    @RequestMapping(value = "/salvarVoo", method = { RequestMethod.POST })
    public String salvarVoo(@Valid @ModelAttribute("voo") final Voo voo, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {

        this.atividadeService.salvar(voo);
        final PlanoViagem planoViagem = voo.getViagem();

        return "redirect:/planoViagem/detalharViagem?id=" + planoViagem.getId();
    }

}
