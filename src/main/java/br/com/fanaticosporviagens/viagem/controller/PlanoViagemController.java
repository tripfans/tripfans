package br.com.fanaticosporviagens.viagem.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.DetalheTrechoVoo;
import br.com.fanaticosporviagens.model.entity.DiarioViagem;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.TrechoTransporte;
import br.com.fanaticosporviagens.model.entity.Voo;
import br.com.fanaticosporviagens.tipo.TipoService;
import br.com.fanaticosporviagens.viagem.model.service.AtividadeService;
import br.com.fanaticosporviagens.viagem.model.service.DiarioViagemService;
import br.com.fanaticosporviagens.viagem.model.service.PlanoViagemService;
import br.com.fanaticosporviagens.viagem.view.FiltroLocaisViagem;

/**
 * @author Pedro Sebba (sebbaweb@gmail.com)
 */
@Controller
@RequestMapping("/planoViagem")
public class PlanoViagemController extends BaseController {

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private AtividadeService atividadeService;

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private DiarioViagemService diarioViagemService;

    @Autowired
    private DicaService dicaService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PlanoViagemService planoViagemService;

    @Autowired
    private TipoService tipoService;

    @RequestMapping(value = "/diario/{diario}/dia/{dia}/adicionarAtividade", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody JSONResponse adicionarAtividade(@PathVariable final DiarioViagem diario, @PathVariable final Integer dia,
            @RequestParam(required = false) final LocalEnderecavel local, @RequestParam(required = false) final String titulo,
            @RequestParam(required = false) final LocalDate dataInicio, @RequestParam(required = false) final LocalTime horaInicio, final Model model) {
        if ((titulo == null || titulo.trim().isEmpty()) && local == null) {
            this.jsonResponse.addParam("message", "Você deve informar um título ou um local para sua atividade");
            this.jsonResponse.setSuccess(false);
            return this.jsonResponse;
        }
        final AtividadePlano atividadePlano = diario.getViagem().addAtividade(local, dia);
        if (dataInicio != null) {
            atividadePlano.setDataInicio(dataInicio);
        }
        if (horaInicio != null) {
            atividadePlano.setHoraInicio(horaInicio);
        }
        if (titulo != null && titulo.trim().isEmpty()) {
            atividadePlano.setTitulo(null);
        } else {
            atividadePlano.setTitulo(titulo);
        }
        this.diarioViagemService.salvar(diario);
        model.addAttribute("atividade", atividadePlano);
        this.jsonResponse.addParam("idAtividade", atividadePlano.getId());
        String localAtividade = null;
        if (atividadePlano.getTitulo() != null) {
            localAtividade = atividadePlano.getTitulo();
        }
        if (atividadePlano.getLocal() != null) {
            if (localAtividade != null) {
                localAtividade += " - ";
            }
            localAtividade += atividadePlano.getLocal().getNome();
        }
        this.jsonResponse.addParam("localAtividade", localAtividade);
        this.jsonResponse.addParam("iconeAtividade", atividadePlano.getTipo().getIcone());
        return this.jsonResponse.success();
        // return "/planoViagem/diario/editarAtividade";
    }

    @RequestMapping(value = "/diario/{diario}/alterarAtividade/{atividade}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage alterarAtividade(@PathVariable final AtividadePlano atividadePlano,
            @RequestParam(required = false) final LocalEnderecavel local, @RequestParam(required = false) final String titulo,
            @RequestParam(required = false) final LocalTime horaInicio, final Model model) {
        if ((titulo == null || titulo.trim().isEmpty()) && local == null) {
            this.success("Você deve informar um título ou um local para sua atividade");
        }
        if (local != null) {
            atividadePlano.setLocal(local);
        }
        if (horaInicio != null) {
            atividadePlano.setHoraInicio(horaInicio);
        }
        if (titulo != null && titulo.trim().isEmpty()) {
            atividadePlano.setTitulo(null);
        } else {
            atividadePlano.setTitulo(titulo);
        }

        this.atividadeService.salvar(atividadePlano);
        model.addAttribute("atividade", atividadePlano);
        return this.success("Informações atualizadas com sucesso!");
    }

    private void appendParamIfNotNull(final StringBuffer url, final String paramName, final Object paramValue) {
        if (paramValue != null) {
            url.append("&" + paramName + "=" + paramValue);
        }
    }

    @RequestMapping(value = "/atividades")
    public String atividades(@RequestParam("id") final PlanoViagem planoViagem, final Model model) throws InstantiationException,
            IllegalAccessException {
        model.addAttribute("viagem", planoViagem);
        model.addAttribute("atividadesView", this.atividadeService.consultarAtividadesViagemAgrupadas(planoViagem.getId()));
        return "/planoViagem/atividades";
    }

    @RequestMapping(value = "/atualizarDestinosViagem")
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody Map<String, ? extends Object> atualizarDestinosViagem(@ModelAttribute final PlanoViagem planoViagem, final Model model,
            final DestinoViagemListWrapper destinosWrapper) {
        this.planoViagemService.salvar(planoViagem, destinosWrapper);
        return Collections.singletonMap("success", "As alterações foram salvas com sucesso.");
    }

    @RequestMapping(value = "/companheiros")
    public String companheiros(@RequestParam("id") final PlanoViagem planoViagem, final Model model) {
        model.addAttribute("viagem", planoViagem);
        model.addAttribute("amizades", this.amizadeService.consultarAmizades(this.getUsuarioAutenticado()));
        model.addAttribute("convitesViagem", this.conviteService.consultarConvitesAmigosParaViagem(planoViagem.getId()));
        model.addAttribute("convitesRecomendacao", this.conviteService.consultarConvitesAmigosParaRecomendacao(planoViagem.getId()));
        return "/planoViagem/companheiros";
    }

    @RequestMapping(value = "/consultarCidadeEPaisPorNome", method = { RequestMethod.GET }, produces = "application/json")
    @ResponseBody
    public List<Cidade> consultarCidadePorNome(@RequestParam("term") final String nome) {
        final List<Cidade> cidades = this.localService.consultarCidadesPorNome(nome);
        return cidades;
    }

    @RequestMapping(value = "{viagem}/convidarAmigos")
    @ResponseBody
    public JSONResponse convidarAmigos(@PathVariable final PlanoViagem planoViagem, @RequestParam(required = false) final List<Amizade> amizades,
            @RequestParam(required = false) final String mensagem, final Model model) throws InstantiationException, IllegalAccessException {
        // this.conviteService.convidarParaViagem(planoViagem, this.getUsuarioAutenticado(), amizades, null, mensagem);
        this.wizardSalvar(planoViagem, null, model);
        // jsonResponse.addParam("idsAmigos", value)
        return this.jsonResponse;
    }

    /*
     * @RequestMapping(value = "/listarAtividades/planoViagem/{idViagem}", method = { RequestMethod.GET }) public String
     * listarAtividadesViagem(@PathVariable final Long idViagem, final Model model) { final PlanoViagem viagem =
     * this.viagemService.consultarPorId(idViagem); model.addAttribute(viagem); return "/planoViagem/planejamento/listaAtividadesViagem"; }
     */

    @RequestMapping(value = "/{viagem}/criarDiario")
    @PreAuthorize("isAuthenticated()")
    public String criarDiario(@PathVariable final PlanoViagem planoViagem, final Model model) {
        if (Boolean.TRUE.equals(planoViagem.getPossuiDiario())) {
            return "/diario/editarDiario";
        }
        final DiarioViagem diario = this.diarioViagemService.criarDiarioViagem(planoViagem);
        model.addAttribute("diario", diario);
        return "/planoViagem/editarDiario";
    }

    @RequestMapping(value = "/editar")
    @PreAuthorize("isAuthenticated()")
    public String criarViagem(final Model model) {
        model.addAttribute("viagem", new PlanoViagem());
        model.addAttribute("editar", true);
        return "/planoViagem/exibirViagem";
    }

    @RequestMapping(value = "/destinoViagemCard/{local}")
    public String destinoViagemCard(@PathVariable final Local local, final Model model) {
        model.addAttribute("local", local);
        return "/planoViagem/destinoViagemCard";
    }

    @RequestMapping(value = "/detalharViagem/{idViagem}")
    @PreAuthorize("isAuthenticated()")
    public String detalharViagem(@PathVariable final Long idViagem, final Model model) {
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        if (planoViagem != null) {
            model.addAttribute("viagem", planoViagem);
            return "/planoViagem/detalheViagem";
        }
        return "/erros/404";
    }

    @RequestMapping(value = "/diario")
    public String diario(@RequestParam("id") final Long idViagem, final Model model) {
        return "/planoViagem/diario";
    }

    @RequestMapping(value = "/diario/editarAtividade/{atividade}")
    @PreAuthorize("isAuthenticated()")
    public String editarAtividade(@PathVariable final AtividadePlano atividadePlano, final Model model) {
        model.addAttribute("diario", atividadePlano.getViagem().getDiario());
        model.addAttribute("atividade", atividadePlano);
        model.addAttribute("anosVisita", getAnosVisita());
        model.addAttribute("itensClassificacao", this.dicaService.consultarItensClassificacaoDica());
        model.addAttribute("itensBomPara", atividadePlano.getLocal().getTipoLocal().getAvaliacoesItensBomPara());
        model.addAttribute("tiposViagem", this.tipoService.tipoViagemConsultarAtivos());

        return "/planoViagem/diario/editarAtividade";
    }

    @RequestMapping(value = "/editarDiario/{diario}")
    // @PreAuthorize("isAuthenticated(), #diario.viagem.criador.username == authentication.name")
    @PreAuthorize("#diario.viagem.criador.username == authentication.name")
    public String editarDiario(@PathVariable final DiarioViagem diario, final Model model) {
        model.addAttribute("diario", diario);
        return "/planoViagem/diario/editarDiario";
    }

    @RequestMapping(value = "/editar/{idViagem}")
    @PreAuthorize("isAuthenticated()")
    public String editarViagem(@PathVariable final Long idViagem, @RequestParam(value = "d", required = false) final Long idDiario, final Model model) {
        model.addAttribute("viagem", this.planoViagemService.consultarViagemUsuarioCriador(idViagem));
        model.addAttribute("idDiario", idDiario);
        model.addAttribute("editar", true);
        return "/planoViagem/exibirViagem";
    }

    @RequestMapping(value = "/editarViagem")
    @PreAuthorize("isAuthenticated()")
    @Deprecated
    // TODO remover das telas q chamam e usar o metodo acima
    public String editarViagem2(@RequestParam(value = "id", required = false) final Long idViagem, final Model model) {
        if (idViagem != null) {
            model.addAttribute("viagem", this.planoViagemService.consultarViagemUsuarioCriador(idViagem));
        } else {
            model.addAttribute("viagem", new PlanoViagem());
        }
        model.addAttribute("editar", true);
        return "/planoViagem/exibirViagem";
    }

    @RequestMapping(value = "/escolherTipoAtividade", method = { RequestMethod.GET })
    public String escolherTipoAtividadeViagem(@RequestParam("id") final Long idViagem, final Model model) {
        model.addAttribute("idViagem", idViagem);
        return "/planoViagem/planejamento/escolhaTipoAtividade";
    }

    @RequestMapping(value = "/diario/excluirAtividade/{atividade}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody JSONResponse excluirAtividade(@PathVariable final AtividadePlano atividadePlano, final Model model) {
        this.atividadeService.excluir(atividadePlano);
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/diario/atividade/relato/excluir")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage excluirRelatoAtividadeDiario(final AtividadePlano atividadePlano, final Model model) {
        this.atividadeService.excluirRelatoAtividade(atividadePlano);
        return this.success("Seu relato foi excluído com sucesso");
    }

    @RequestMapping(value = "/excluir/{viagem}")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public JSONResponse excluirViagem(@PathVariable final PlanoViagem planoViagem, final Model model) {
        if (planoViagem != null) {
            this.planoViagemService.excluir(planoViagem);
        }
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/diario/{diario}/dia/{dia}")
    public String exibirAtividadesDia(@PathVariable final DiarioViagem diario, @PathVariable final Integer dia, final Model model) {
        model.addAttribute("diaViagem", diario.getViagem().getDiaViagem(dia));
        return "/planoViagem/diario/exibirAtividadesDia";
    }

    @RequestMapping(value = "/diario/atividade/{atividade}/carrosselFotos")
    @PreAuthorize("isAuthenticated()")
    public String exibirCarrosselFotos(@PathVariable final AtividadePlano atividadePlano, @RequestParam(required = false) final String exibirOpcoes,
            final RedirectAttributes redirectAttributes, final Model model) {
        model.addAttribute("atividade", atividadePlano);
        redirectAttributes.addFlashAttribute("exibirOpcoes", exibirOpcoes);
        return "/planoViagem/diario/carrosselFotos";
    }

    @RequestMapping(value = "/diario/{diario}/slideshow/informacaoAtividade/{dia}/{atividade}")
    public String exibirInformacaoAtividadeSlideShow(@PathVariable final DiarioViagem diario, @PathVariable final Integer dia,
            @PathVariable final AtividadePlano atividadePlano, final Model model) {
        model.addAttribute("diario", diario);
        model.addAttribute("diaViagem", diario.getViagem().getDiaViagem(dia));
        model.addAttribute("atividade", atividadePlano);
        return "/planoViagem/diario/informacaoAtividade";
    }

    @RequestMapping(value = "/diario/{diario}/slideshow")
    public String exibirSlideShow(@PathVariable final DiarioViagem diario, final Model model) {
        model.addAttribute("diario", diario);
        return "/planoViagem/diario/exibirSlideShow";
    }

    @RequestMapping(value = "/exibirViagem")
    public String exibirViagem(@RequestParam(value = "id") final Long idViagem, final Model model) {
        model.addAttribute("viagem", this.planoViagemService.consultarPorId(idViagem));
        return "/planoViagem/exibirViagem";
    }

    @RequestMapping("/session/foto")
    public ResponseEntity<byte[]> foto() throws IOException {
        final byte[] foto = (byte[]) this.httpSession.getAttribute("arquivoFotoViagem");

        if (foto != null) {
            final HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.IMAGE_PNG);
            headers.setContentLength(foto.length);

            return new ResponseEntity<byte[]>(foto, headers, HttpStatus.OK);
        }
        return null;
    }

    @RequestMapping("/{viagemId}/foto")
    public ResponseEntity<byte[]> foto(@PathVariable final Long viagemId) throws IOException {
        final InputStream in = this.httpSession.getServletContext().getResourceAsStream("/resources/images/no_photo.png");

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);

        return new ResponseEntity<byte[]>(IOUtils.toByteArray(in), headers, HttpStatus.CREATED);
    }

    @RequestMapping("/session/foto2")
    public void foto2(final HttpServletResponse response) throws IOException {
        // final InputStream foto = (InputStream) this.httpSession.getAttribute("arquivoFotoViagem");

        // this.writeFileContentToHttpResponse(foto, response);
    }

    @RequestMapping(value = "/fotos")
    public String fotos(@RequestParam("id") final Long idViagem, final Model model) {
        model.addAttribute("fotos", this.fotoService.consultarFotosViagem(idViagem));
        return "/planoViagem/fotos";
    }

    @ModelAttribute("avaliacoesQualidade")
    public AvaliacaoQualidade[] getAvaliacoesQualidade() {
        return new AvaliacaoQualidade[] { AvaliacaoQualidade.PESSIMO, AvaliacaoQualidade.RUIM, AvaliacaoQualidade.REGULAR, AvaliacaoQualidade.BOM,
                AvaliacaoQualidade.EXCELENTE };
    }

    @RequestMapping(value = "/incluirViagem", method = { RequestMethod.POST })
    public String incluirViagem(@Valid @ModelAttribute("viagem") final PlanoViagem planoViagem, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            model.addAttribute("viagem", planoViagem);
            return "/planoViagem/planejamento/planejarViagem";
        }
        this.planoViagemService.incluir(planoViagem);
        // if (viagem.possuiFotos()) {
        // this.fotoService.incluirFotos(hospedagem);
        // }
        return "redirect:/planoViagem/listarAtividades/planoViagem/" + planoViagem.getId();
    }

    @RequestMapping(value = "/listaDias")
    public String listaDias() throws InstantiationException, IllegalAccessException {
        return "/planoViagem/listaDias";
    }

    @RequestMapping(value = "/diario/{diario}/listarAtividadesDia/{dia}")
    @PreAuthorize("isAuthenticated()")
    public String listarAtividadesDiarioDia(@PathVariable final DiarioViagem diario, @PathVariable final Integer dia, final Model model) {
        model.addAttribute("diaViagem", diario.getViagem().getDiaViagem(dia));
        return "/planoViagem/diario/listaAtividadesDia";
    }

    @RequestMapping(value = "/listarAtividades/planoViagem/{idViagem}", method = { RequestMethod.GET })
    public String listarAtividadesViagem(@PathVariable final Long idViagem, final Model model) {
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        model.addAttribute(planoViagem);
        return "/planoViagem/planejamento/listaAtividadesViagem";
    }

    @RequestMapping(value = "/meusDestinos")
    @PreAuthorize("isAuthenticated()")
    public String meusDestinos(@RequestParam("id") final Long idViagem, final Model model) {
        final PlanoViagem planoViagem = this.planoViagemService.consultarViagemUsuarioCriador(idViagem);
        model.addAttribute("viagem", planoViagem);
        return "/planoViagem/meusDestinos";
    }

    @RequestMapping(value = "/minhasDicas")
    public String minhasDicas(@RequestParam("id") final Long idViagem, final Model model) {
        return "/planoViagem/minhasDicas";
    }

    @RequestMapping(value = "/diario/ocultarAtividade/{atividade}/{ocultar}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage ocultarAtividade(@PathVariable final AtividadePlano atividadePlano, @PathVariable final boolean ocultar,
            final Model model) {
        atividadePlano.setOculta(ocultar);
        this.atividadeService.alterar(atividadePlano);
        return this.success("Esta atividade " + (ocultar ? "não" : "") + " será exibida em seu diário");
    }

    @RequestMapping(value = "/painel")
    public String painel(@RequestParam("id") final Long idUsuario, final Model model) {
        model.addAttribute("viagens", this.planoViagemService.consultarViagensUsuario(idUsuario));
        return "/planoViagem/painelViagens";
    }

    @RequestMapping(value = "/viagem", method = { RequestMethod.GET })
    public String planejarViagem(final Model model) {
        if (!model.containsAttribute("viagem")) {
            model.addAttribute("viagem", new PlanoViagem());
        }
        return "/planoViagem/planejamento/planejarViagem";
    }

    @RequestMapping(value = "/diario/{diario}/publicar", method = RequestMethod.POST)
    public @ResponseBody JSONResponse publicarDiario(@PathVariable final DiarioViagem diario, final Model model) {
        this.diarioViagemService.publicar(diario);
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/diario/atividade/salvar")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public StatusMessage salvarAtividadeDiario(final AtividadePlano atividadePlano, final Model model) {
        this.atividadeService.salvar(atividadePlano);
        return this.success("Seu relato foi salvo com sucesso");
    }

    @RequestMapping(value = "/diario/salvarDiario", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public JSONResponse salvarDiario(@Valid @ModelAttribute("diario") final DiarioViagem diario, final Model model) {
        this.diarioViagemService.salvar(diario);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/salvarFoto")
    public @ResponseBody JSONResponse salvarFoto(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

        final InputStream inputStream = request.getInputStream();
        final String filename = request.getHeader("X-File-Name");

        // TODO pensar em outra forma de guardar o aquivo e remover este arquivo da session
        this.httpSession.setAttribute("arquivoFotoViagem", IOUtils.toByteArray(inputStream));
        this.httpSession.setAttribute("nomeFotoViagem", filename);

        this.jsonResponse.addParam("urlFoto", request.getContextPath() + "/planoViagem/session/foto");

        return this.jsonResponse;
    }

    @RequestMapping(value = "/diario/salvarFotoCapa")
    public @ResponseBody JSONResponse salvarFotoCapa(@RequestParam("idDiario") final DiarioViagem diario, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        final InputStream inputStream = request.getInputStream();
        final String filename = request.getHeader("X-File-Name");

        final Foto foto = this.diarioViagemService.salvarFotoCapaDiarioViagem(diario, inputStream, filename);

        this.jsonResponse.addParam("urlFotoCapa", foto.getUrlBig());

        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/diario/salvarFoto")
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody JSONResponse salvarFotoDiario(@RequestParam("idAtividade") final AtividadePlano atividadePlano,
            @RequestParam(value = "descricaoFoto", required = false) final String descricaoFoto, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {

        final InputStream inputStream = request.getInputStream();
        final String filename = request.getHeader("X-File-Name");

        final Foto foto = this.atividadeService.salvarFoto(atividadePlano, inputStream, filename, descricaoFoto);

        this.jsonResponse.addParam("urlFotoCarrosel", foto.getUrlCarrosel());
        this.jsonResponse.addParam("urlFotoOriginal", foto.getUrlOriginal());

        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/salvarViagem")
    @PreAuthorize("isAuthenticated()")
    public String salvarViagem(final PlanoViagem planoViagem, final Model model, final DestinoViagemListWrapper destinosWrapper,
            @RequestParam(required = false) final Long idDiario) {

        final Object arquivoFotoViagem = this.httpSession.getAttribute("arquivoFotoViagem");
        if (arquivoFotoViagem != null) {
            try {
                // TODO remover da session
                // final InputStream inputStream = (InputStream) this.httpSession.getAttribute("arquivoFotoViagem");
                final byte[] aquivoFoto = (byte[]) this.httpSession.getAttribute("arquivoFotoViagem");
                final String filename = (String) this.httpSession.getAttribute("nomeFotoViagem");

                this.planoViagemService.salvarFotoIlustrativaViagem(planoViagem, aquivoFoto, filename);
            } catch (final Exception e) {
                // TODO tratar, pensar no que fazer se der erro aqui
                System.out.println("ERRO " + e.getMessage());
                e.printStackTrace();
                // throw new RuntimeException(e.getMessage(), e);
            } finally {
                this.httpSession.removeAttribute("arquivoFotoViagem");
                this.httpSession.removeAttribute("nomeFotoViagem");
            }
        }

        if (destinosWrapper != null && !destinosWrapper.isEmpty()) {
            this.planoViagemService.salvar(planoViagem, destinosWrapper);
        } else {
            this.planoViagemService.salvar(planoViagem);
        }

        model.addAttribute("viagem", planoViagem);
        // model.addAttribute("atividades", viagem);
        if (idDiario != null) {
            return "redirect:/planoViagem/editarDiario/" + idDiario;
        }
        return "redirect:/planoViagem/detalharViagem/" + planoViagem.getId();
    }

    @RequestMapping(value = "{viagem}/solicitarDicas")
    @ResponseBody
    public JSONResponse solicitarDicas(@PathVariable final PlanoViagem planoViagem, @RequestParam(required = false) final List<Amizade> amizades,
            @RequestParam(required = false) final String mensagem, final Model model) throws InstantiationException, IllegalAccessException {
        // this.conviteService.solicitarRecomendacoesParaViagem(planoViagem, this.getUsuarioAutenticado(), amizades, null, mensagem);
        this.wizardSalvar(planoViagem, null, model);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/videos")
    public String videos(@RequestParam("id") final Long idViagem, final Model model) {
        return "/planoViagem/videos";
    }

    @RequestMapping(value = "/diario/{diario}")
    public String visualizarDiario(@PathVariable final DiarioViagem diario, final Model model) {
        model.addAttribute("diario", diario);
        return "/planoViagem/diario/exibirDiario";
    }

    @RequestMapping(value = "/wizard")
    @PreAuthorize("isAuthenticated()")
    public String wizard(final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("viagem", new PlanoViagem());
        return "/planoViagem/wizard/wizard";
    }

    // TODO esse método é temporário; deve ser removido
    @RequestMapping(value = "/wizard/{viagem}")
    @PreAuthorize("isAuthenticated()")
    public String wizard(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("viagem", planoViagem);
        final Voo voo = new Voo();
        // Adiciona um trecho vazio
        voo.addTrecho(new TrechoTransporte<DetalheTrechoVoo>());
        model.addAttribute("voo", voo);
        return "/planoViagem/wizard/wizard";
    }

    @RequestMapping(value = "/wizard/convidarSolicitarDicas")
    @ResponseBody
    public JSONResponse wizardConvidarSolicitarDicas(@ModelAttribute("viagem") final PlanoViagem planoViagem,
            @RequestParam(required = false) final List<Amizade> as_values_amigosConvidados,
            @RequestParam(required = false) final List<Amizade> as_values_amigosSolicitados,
            @RequestParam(required = false) final String mensagemConvite, @RequestParam(required = false) final String mensagemSolicitacaoDicas,
            final Model model) throws InstantiationException, IllegalAccessException {
        /*this.conviteService.convidarParaViagem(planoViagem, this.getUsuarioAutenticado(), as_values_amigosConvidados, null, mensagemConvite);
        this.conviteService.solicitarRecomendacoesParaViagem(planoViagem, this.getUsuarioAutenticado(), as_values_amigosSolicitados, null,
                mensagemSolicitacaoDicas);*/
        return this.wizardSalvar(planoViagem, null, model);
    }

    @RequestMapping(value = "/diario/wizard")
    @PreAuthorize("isAuthenticated()")
    public String wizardDiario(final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("criacaoDiario", true);
        return wizard(model);
    }

    @RequestMapping(value = "/{viagem}/diario/wizard/passo/2")
    public String wizardDiarioPasso2(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException,
            IllegalAccessException {
        model.addAttribute("criacaoDiario", true);
        return wizardPasso4(planoViagem, model);
    }

    @RequestMapping(value = "/{viagem}/diario/wizard/passo/3")
    public String wizardDiarioPasso3(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException,
            IllegalAccessException {
        model.addAttribute("criacaoDiario", true);
        return wizardPasso5(planoViagem, model);
    }

    @RequestMapping(value = "/diario/wizard/salvar")
    public String wizardDiarioSalvar(@Valid @ModelAttribute("viagem") final PlanoViagem planoViagem, final DestinoViagemListWrapper destinosWrapper,
            final Model model) throws InstantiationException, IllegalAccessException {
        planoViagem.setCriador(this.getUsuarioAutenticado());
        this.criarDiario(planoViagem, model);
        this.salvarViagem(planoViagem, model, destinosWrapper, null);

        return editarDiario(planoViagem.getDiario(), model);
    }

    @RequestMapping(value = "/{viagem}/wizard/excluirAtividade")
    @ResponseBody
    public JSONResponse wizardExcluirAtividade(@PathVariable final PlanoViagem planoViagem, @RequestParam final AtividadePlano atividadePlano,
            final Model model) throws InstantiationException, IllegalAccessException {
        planoViagem.removeAtividade(atividadePlano);
        this.planoViagemService.alterar(planoViagem);
        model.addAttribute("viagem", planoViagem);
        this.jsonResponse.addParam("success", true);
        // Id do local para remover
        this.jsonResponse.addParam("idLocal", atividadePlano.getLocal().getId());
        return this.jsonResponse;
    }

    @RequestMapping(value = "/{viagem}/wizard/incluirAtividade")
    @ResponseBody
    public JSONResponse wizardIncluirAtividade(@PathVariable final PlanoViagem planoViagem, @RequestParam final LocalEnderecavel local,
            @RequestParam(required = false) final Integer dia, final Model model) throws InstantiationException, IllegalAccessException {
        final AtividadePlano atividadePlano = planoViagem.addAtividade(local, dia);

        // this.viagemService.alterar(viagem);
        this.atividadeService.incluir(atividadePlano);

        model.addAttribute("viagem", planoViagem);
        this.jsonResponse.addParam("success", true);
        this.jsonResponse.addParam("idAtividade", atividadePlano.getId());
        return this.jsonResponse;
    }

    @RequestMapping(value = "/{viagem}/wizard/listarLocais")
    public String wizardListarLocais(@PathVariable final PlanoViagem planoViagem, @RequestParam final Long idCidade,
            @RequestParam final LocalType tipoLocal, @RequestParam final FiltroLocaisViagem filtroLocais,
            @RequestParam(required = false) final Integer[] categorias, @RequestParam(required = false) final String nomeLocal,
            @RequestParam(required = false) final Integer[] tiposCozinha, @RequestParam(required = false) final Integer[] tiposHospedagem,
            @RequestParam(required = false) final Integer estrelas, @RequestParam(required = false) final Integer precoMinino,
            @RequestParam(required = false) final Integer precoMaximo, final RedirectAttributes redirectAttributes, final Model model)
            throws InstantiationException, IllegalAccessException {

        model.addAttribute("viagem", planoViagem);
        if (planoViagem.getCidades() != null && !planoViagem.getCidades().isEmpty()) {
            model.addAttribute("cidade", planoViagem.getCidades().iterator().next());
        }

        model.addAttribute("locais", this.localService.consultarLocais(filtroLocais, tipoLocal, idCidade, nomeLocal, categorias, tiposCozinha,
                tiposHospedagem, estrelas, precoMinino, precoMaximo));

        // Query string para refazer a mesma consulta em caso de paginacao
        final StringBuffer urlConsulta = new StringBuffer();

        urlConsulta.append("/planoViagem/" + planoViagem.getId() + "/wizard/listarLocais?");

        urlConsulta.append("filtroLocais=" + filtroLocais);
        urlConsulta.append("&idCidade=" + idCidade);
        urlConsulta.append("&tipoLocal=" + tipoLocal);

        this.appendParamIfNotNull(urlConsulta, "nomeLocal", nomeLocal);
        this.appendParamIfNotNull(urlConsulta, "categorias", categorias);
        this.appendParamIfNotNull(urlConsulta, "tiposCozinha", tiposCozinha);
        this.appendParamIfNotNull(urlConsulta, "tiposHospedagem", tiposHospedagem);
        this.appendParamIfNotNull(urlConsulta, "estrelas", estrelas);
        this.appendParamIfNotNull(urlConsulta, "precoMinino", precoMinino);
        this.appendParamIfNotNull(urlConsulta, "precoMaximo", precoMaximo);

        model.addAttribute("urlConsulta", urlConsulta.toString());

        return "/planoViagem/listaAtracoesViagem";
    }

    @RequestMapping(value = "/{viagem}/wizard/passo/2")
    public String wizardPasso2(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("viagem", planoViagem);
        final Voo voo = new Voo();
        // Adiciona um trecho vazio
        voo.addTrecho(new TrechoTransporte<DetalheTrechoVoo>());
        model.addAttribute("voo", voo);
        return "/planoViagem/wizard/passo2";
    }

    @RequestMapping(value = "/{viagem}/wizard/passo/3")
    public String wizardPasso3(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("viagem", planoViagem);
        return "/planoViagem/wizard/passo3";
    }

    @RequestMapping(value = "/{viagem}/wizard/passo/4")
    public String wizardPasso4(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("amizades", this.amizadeService.consultarAmizades(planoViagem.getCriador()));
        model.addAttribute("viagem", planoViagem);
        return "/planoViagem/wizard/passo4";
    }

    @RequestMapping(value = "/{viagem}/wizard/passo/5")
    public String wizardPasso5(@PathVariable final PlanoViagem planoViagem, final Model model) throws InstantiationException, IllegalAccessException {
        model.addAttribute("viagem", planoViagem);
        return "/planoViagem/wizard/passo5";
    }

    @RequestMapping(value = "/wizard/salvar")
    @ResponseBody
    public JSONResponse wizardSalvar(@Valid @ModelAttribute("viagem") final PlanoViagem planoViagem, final DestinoViagemListWrapper destinosWrapper,
            final Model model) throws InstantiationException, IllegalAccessException {
        planoViagem.setCriador(this.getUsuarioAutenticado());
        // this.salvarViagem(viagem, model, destinosWrapper, null);

        this.jsonResponse.addParam("success", true);
        // this.jsonResponse.addParam("idViagem", viagem.getId());
        this.jsonResponse.addParam("idViagem", 2);
        return this.jsonResponse;
    }

    @RequestMapping(value = "/diario/wizard/selecionarAmigos")
    @ResponseBody
    public JSONResponse wizardSelecionarAmigos(@ModelAttribute("viagem") final PlanoViagem planoViagem,
            @RequestParam(required = false) final List<Amizade> as_values_amigosConvidados, final Model model) throws InstantiationException,
            IllegalAccessException {
        return this.wizardSalvar(planoViagem, null, model);
    }

    @RequestMapping(value = "/wizard/teste")
    public String wizardTeste(final Model model) throws InstantiationException, IllegalAccessException {
        // model.addAttribute("viagem", new PlanoViagem());
        return "/planoViagem/wizard/teste";
    }

    @RequestMapping(value = "/wizard/teste2")
    public String wizardTeste2(final Model model) throws InstantiationException, IllegalAccessException {
        return "/planoViagem/wizard/teste2";
    }

    private void writeFileContentToHttpResponse(final InputStream inputStream, final HttpServletResponse response) throws IOException {

        final InputStream foto = (InputStream) this.httpSession.getAttribute("arquivoFotoViagem");

        // final byte[] byteArray = IOUtils.toByteArray(foto);

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        int next = foto.read();
        while (next > -1) {
            bos.write(next);
            next = foto.read();
        }
        bos.flush();
        final byte[] byteArray = bos.toByteArray();

        response.setContentType(MediaType.IMAGE_PNG.toString());
        response.setContentLength(byteArray.length);

        response.getOutputStream().write(byteArray);
        response.getOutputStream().flush();
    }
}
