package br.com.fanaticosporviagens.viagem;

import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Atividade;
import br.com.fanaticosporviagens.model.entity.DiaViagem;
import br.com.fanaticosporviagens.model.entity.Viagem;

@Repository
public class AtividadeRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected GenericSearchRepository searchRepository;

    public void alterar(final Atividade item) {
        this.entityManager.persist(item);
    }

    public void alterarAnterior(final Atividade itemQueSeraAlterado, final Atividade itemAnterior) {
        final StringBuilder update = new StringBuilder();
        update.append("UPDATE Atividade ");
        update.append("   SET itemAnterior = " + ((itemAnterior != null) ? itemAnterior.getId().toString() : "null"));
        update.append(" WHERE id = " + itemQueSeraAlterado.getId());

        this.entityManager.createQuery(update.toString()).executeUpdate();

    }

    public void alterarAnteriorProximo(final Atividade itemQueSeraAlterado, final Atividade itemAnterior, final Atividade itemProximo) {
        final StringBuilder update = new StringBuilder();
        update.append("UPDATE Atividade ");
        update.append("   SET itemAnterior = " + ((itemAnterior != null) ? itemAnterior.getId().toString() : "null"));
        update.append("     , itemProximo = " + ((itemProximo != null) ? itemProximo.getId().toString() : "null"));
        update.append(" WHERE id = " + itemQueSeraAlterado.getId());

        this.entityManager.createQuery(update.toString()).executeUpdate();
    }

    public void alterarDiaAtividade(final Atividade atividadeQueSeraAlterada, final Viagem viagem, final DiaViagem diaInicio, DiaViagem diaFim) {
        if (diaInicio != null) {
            // Se o dia fim não foi informado e se a atividade termina em outro dia
            if (diaFim == null) {
                if (atividadeQueSeraAlterada.isTerminaEmOutroDia()) {
                    diaFim = atividadeQueSeraAlterada.getDiaFim();
                    // se o dia para qual está sendo movido for menor do que o dia que a atividade a termina
                    if (diaInicio.getNumero() >= diaFim.getNumero()) {
                        // Diferença entre o dia inicio e o dia fim
                        final Integer quantidadeDiasDiferenca = diaInicio.getNumero() - diaFim.getNumero();
                        // Calculando o novo dia de termino da atividade
                        Integer novoDiaTermino = diaInicio.getNumero() + quantidadeDiasDiferenca;
                        if (novoDiaTermino > diaInicio.getViagem().getQuantidadeDias()) {
                            novoDiaTermino = diaInicio.getViagem().getQuantidadeDias();
                        }
                        diaFim = diaInicio.getViagem().getDia(novoDiaTermino);
                    }
                } else {
                    diaFim = diaInicio;
                }
                atividadeQueSeraAlterada.setViagem(null);
                atividadeQueSeraAlterada.setDia(diaInicio);
                atividadeQueSeraAlterada.setDiaFim(diaFim);
                final Integer indice = recuperarIndiceUltimaAtividadeDoDia(diaInicio);
                atividadeQueSeraAlterada.setOrdem(indice == null ? 0 : indice + 1);
                diaInicio.getAtividadesQueComecam().add(atividadeQueSeraAlterada);

            }
        }
        this.alterar(atividadeQueSeraAlterada);
    }

    public void alterarProximo(final Atividade itemQueSeraAlterado, final Atividade itemProximo) {
        final StringBuilder update = new StringBuilder();
        update.append("UPDATE Atividade ");
        update.append("   SET itemProximo = " + ((itemProximo != null) ? itemProximo.getId().toString() : "null"));
        update.append(" WHERE id = " + itemQueSeraAlterado.getId());

        this.entityManager.createQuery(update.toString()).executeUpdate();
    }

    public void alterarViagemDiaAnteriorProximo(final Atividade atividadeQueSeraAlterada, final Viagem viagem, final DiaViagem diaInicio,
            DiaViagem diaFim, final Atividade atividadeAnterior, final Atividade proximaAtividade) {

        /*final DiaViagem diaInicioAntigo = atividadeQueSeraAlterada.getDia();
        final DiaViagem diaFimAntigo = atividadeQueSeraAlterada.getDiaFim();*/

        // DiaViagem diaFimNovo = null;

        if (diaInicio != null) {
            // Se o dia fim não foi informado e se a atividade termina em outro dia
            if (diaFim == null) {
                if (atividadeQueSeraAlterada.isTerminaEmOutroDia()) {
                    diaFim = atividadeQueSeraAlterada.getDiaFim();
                    // se o dia para qual está sendo movido for menor do que o dia que a atividade a termina
                    if (diaInicio.getNumero() >= diaFim.getNumero()) {
                        // Diferença entre o dia inicio e o dia fim
                        final Integer quantidadeDiasDiferenca = diaInicio.getNumero() - diaFim.getNumero();
                        // Calculando o novo dia de termino da atividade
                        Integer novoDiaTermino = diaInicio.getNumero() + quantidadeDiasDiferenca;
                        if (novoDiaTermino > diaInicio.getViagem().getQuantidadeDias()) {
                            novoDiaTermino = diaInicio.getViagem().getQuantidadeDias();
                        }
                        diaFim = diaInicio.getViagem().getDia(novoDiaTermino);
                    }
                } else {
                    diaFim = diaInicio;
                }
                atividadeQueSeraAlterada.setDiaFim(diaFim);
            }
        }
        final StringBuilder update = new StringBuilder();
        update.append("UPDATE Atividade ");
        update.append("   SET itemAnterior = " + ((atividadeAnterior != null) ? atividadeAnterior.getId().toString() : "null"));
        update.append("     , itemProximo = " + ((proximaAtividade != null) ? proximaAtividade.getId().toString() : "null"));
        update.append("     , viagem = " + ((viagem != null) ? viagem.getId().toString() : "null"));
        update.append("     , dia = " + ((diaInicio != null) ? diaInicio.getId().toString() : "null"));
        // if (diaFimNovo != null) {
        update.append("     , diaFim = " + ((diaFim != null) ? diaFim.getId().toString() : "null"));
        // }
        update.append(" WHERE id = " + atividadeQueSeraAlterada.getId());

        this.entityManager.createQuery(update.toString()).executeUpdate();
    }

    public Atividade consultarAOrganizarPorIdLocal(final Long idViagem, final Long idLocal) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idViagem", idViagem);
        params.put("idLocal", idLocal);
        try {
            return (Atividade) this.searchRepository
                    .consultaHQL(
                            "SELECT atividade FROM Atividade atividade WHERE atividade.viagem.id = :idViagem AND atividade.local.id = :idLocal ORDER BY atividade.id DESC",
                            params).get(0);
        } catch (final java.lang.IndexOutOfBoundsException e) {
            throw new NotFoundException("Atividade não encontrada");
        }
    }

    public Atividade consultarPorId(final Long idAtividade) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idAtividade", idAtividade);
        try {
            return (Atividade) this.searchRepository.consultaHQL("select item from Atividade item where item.id = :idAtividade", params).get(0);
        } catch (final java.lang.IndexOutOfBoundsException e) {
            throw new NotFoundException("Atividade não encontrada");
        }
    }

    public void excluir(final Atividade atividade) {
        /*final String delete = "delete from Atividade item where item.id = " + item.getId();
        this.entityManager.createQuery(delete).executeUpdate();*/
        this.entityManager.remove(atividade);
    }

    public void excluirAtividadesViagem(final Long idViagem) {
        this.entityManager.createQuery(
                "delete from Atividade item where exists (select dia from DiaViagem dia where dia = item.dia and dia.viagem.id = " + idViagem + ")")
                .executeUpdate();
        this.entityManager.createQuery("delete from Atividade item where item.viagem.id = " + idViagem).executeUpdate();
    }

    public void excluirItensDia(final Long idDia) {
        this.entityManager.createQuery("delete from Atividade item where item.dia = " + idDia).executeUpdate();
    }

    public Long getId() {
        return new Long(this.entityManager.createNativeQuery("select nextval('seq_viagem_atividade')").getSingleResult().toString());
    }

    public void inserir(final Atividade atividade) {
        final Integer ordem = recuperarIndiceUltimaAtividade(atividade);
        atividade.setOrdem(ordem == null ? 0 : ordem + 1);

        this.entityManager.persist(atividade);
    }

    public Long recuperarDiaAntesAlteracao(final Long idAtividade, final Long idDiaAtual) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idAtividade", idAtividade);

        final Query query = this.entityManager.createNativeQuery("SELECT id_viagem_dia FROM viagem_item WHERE id_viagem_item = :idAtividade");
        query.setParameter("idAtividade", idAtividade);

        return new Long(query.getSingleResult().toString());
    }

    public Integer recuperarIndiceUltimaAtividade(final Atividade atividade) {
        if (atividade.getDia() != null) {
            return recuperarIndiceUltimaAtividadeDoDia(atividade.getDia());
        } else {
            return recuperarIndiceUltimaAtividadeDaViagem(atividade.getViagem());
        }
    }

    private Integer recuperarIndiceUltimaAtividade(final String nomeColuna, final Long id) {
        final StringBuffer hqlQuery = new StringBuffer("SELECT max(ordem) FROM viagem_atividade WHERE " + nomeColuna + " = :id");

        final Query query = this.entityManager.createNativeQuery(hqlQuery.toString());
        query.setParameter("id", id);

        final Object result = query.getSingleResult();
        Integer ordem = null;
        if (result != null) {
            ordem = new Integer(result.toString());
        }
        return ordem;
    }

    public Integer recuperarIndiceUltimaAtividadeDaViagem(final Viagem viagem) {
        return recuperarIndiceUltimaAtividade("id_viagem", viagem.getId());
    }

    public Integer recuperarIndiceUltimaAtividadeDoDia(final DiaViagem diaViagem) {
        return recuperarIndiceUltimaAtividade("id_viagem_dia", diaViagem.getId());
    }

    public void refresh(final Atividade item) {
        this.entityManager.refresh(item);
    }
}
