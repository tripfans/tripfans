package br.com.fanaticosporviagens.viagem.view;

public enum FiltroLocaisViagem {

    CONSULTAR_LOCAIS,
    LOCAIS_MAIS_VISITADOS,
    LOCAIS_MELHORES_AVALIADOS,
    LOCAIS_PATROCINADOS,
    LOCAIS_RECOMENDADOS_POR_AMIGOS;

}
