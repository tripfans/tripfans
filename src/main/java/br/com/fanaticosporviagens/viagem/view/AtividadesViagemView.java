package br.com.fanaticosporviagens.viagem.view;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.AtividadeLongaDuracao;
import br.com.fanaticosporviagens.model.entity.Transporte;
import br.com.fanaticosporviagens.model.entity.TrechoTransporte;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.PlanoViagem.TipoPeriodoViagem;

public class AtividadesViagemView {

    private final Map<Object, GrupoAtividades> mapaGruposAtividades = new LinkedHashMap<Object, GrupoAtividades>();

    public AtividadesViagemView(final PlanoViagem planoViagem) {
        criarGrupos(planoViagem);
    }

    /**
     * Adiciona uma atividadePlano neste grupo
     */
    public void adicionarAtividadeNoGrupo(final AtividadePlano atividadePlano) throws InstantiationException, IllegalAccessException {
        final PlanoViagem planoViagem = atividadePlano.getViagem();
        // Objeto que distingue o a data ou dia inicial do grupo
        Object chaveDoGrupoInicio = null;
        // Objeto que distingue o a chave data ou dia final (caso a atividadePlano termine em outro dia) do grupo
        Object chaveDoGrupoTermino = null;
        // Objeto que guarda os trechos do transporte (caso a atividadePlano seja de transporte)
        final List<TrechoTransporte<?>> trechosOutrosDias = new ArrayList<TrechoTransporte<?>>();

        if (planoViagem.isPeriodoPorDatas()) {
            if (atividadePlano.getDataInicio() != null) {
                // A chave é a data inicial da atividadePlano
                chaveDoGrupoInicio = new LocalDate(atividadePlano.getDataInicio());
                // se for transporte
                /*if (atividadePlano.isTranporte()) {
                    // varre os trechos do transporte
                    for (final TrechoTransporte<?> trecho : ((Transporte<?>) atividadePlano).getTrechos()) {
                        // Se a data final do trecho for após a data incial da atividadePlano
                        // if (trecho.getChegada().getData() != null && ((LocalDate) chaveDoGrupoInicio).isBefore(trecho.getChegada().getData())) {
                        // adicionar o trecho como uma atividadePlano para aparecer na tela do planejamento
                        trechosOutrosDias.add(trecho);
                        // }
                    }
                } else*/
                // Se a atividadePlano for de "longa" duração,
                if (!atividadePlano.isTranporte() && atividadePlano.isLongaDuracao() && atividadePlano.getDataFim() != null) {
                    // adcionar a data fim para criar uma atividadePlano "fake" de término na lista
                    chaveDoGrupoTermino = new LocalDate(atividadePlano.getDataFim());
                }
            }
        } else if (planoViagem.isPeriodoPorQuantidadeDias()) {
            // A chave é o dia inicial da atividadePlano
            chaveDoGrupoInicio = atividadePlano.getDiaInicio();
            // se for transporte
            /*if (atividadePlano.isTranporte()) {
                // varre os trechos do transporte
                for (final TrechoTransporte<?> trecho : ((Transporte<?>) atividadePlano).getTrechos()) {
                    // adicionar o trecho como uma atividadePlano para aparecer na tela do planejamento
                    trechosOutrosDias.add(trecho);
                }
            } else*/
            if (!atividadePlano.isTranporte() && atividadePlano.isLongaDuracao() && ((AtividadeLongaDuracao) atividadePlano).getDiaFim() != null) {
                chaveDoGrupoTermino = ((AtividadeLongaDuracao) atividadePlano).getDiaFim();
            }
        } else {
            chaveDoGrupoInicio = GrupoAtividades.INDEFINIDO;
        }

        // Recuperar o a lista de atividades com a mesma data desta atividadePlano (se houver)
        List<AtividadeView> listaAtividades = getListaAtividadesDoGrupo(chaveDoGrupoInicio);
        if (listaAtividades != null) {
            if (atividadePlano.isTranporte()) {
                for (final TrechoTransporte<?> trecho : ((Transporte<?>) atividadePlano).getTrechos()) {
                    final List<AtividadeView> listaAtividadesTermino = getListaAtividadesDoGrupo(trecho.getPartida().getData());
                    if (listaAtividadesTermino != null) {
                        listaAtividadesTermino.add(new AtividadeView(criarAtividadeAPartirDeTrechoTransporte(trecho), true));
                    }
                }
            } else {
                listaAtividades.add(new AtividadeView(atividadePlano));

                if (chaveDoGrupoTermino != null) {
                    final List<AtividadeView> listaAtividadesTermino = getListaAtividadesDoGrupo(chaveDoGrupoTermino);
                    if (listaAtividadesTermino != null) {
                        listaAtividadesTermino.add(new AtividadeView(atividadePlano, true));
                    }
                }
            }

            /*if (trechosOutrosDias != null) {
                for (final TrechoTransporte<?> trecho : trechosOutrosDias) {
                    final List<AtividadeView> listaAtividadesTermino = getListaAtividadesDoGrupo(trecho.getPartida().getData());
                    if (listaAtividadesTermino != null) {
                        listaAtividadesTermino.add(new AtividadeView(criarAtividadeAPartirDeTrechoTransporte(trecho), true));
                    }
                }
            }*/
        } else {
            // Se não tiver um grupo com a mesma data, adicionar no grupo
            // sem data definida
            listaAtividades = getListaAtividadesDoGrupo(GrupoAtividades.INDEFINIDO);
            listaAtividades.add(new AtividadeView(atividadePlano));
        }
    }

    public List<GrupoAtividades> getGruposAtividades() {
        return new ArrayList<GrupoAtividades>(this.mapaGruposAtividades.values());
    }

    /**
     * Retorna a lista de Atividades do grupo mapeado com a chave informada
     * 
     * @param chaveDoGrupo
     */
    public List<AtividadeView> getListaAtividadesDoGrupo(Object chaveDoGrupo) {
        if (chaveDoGrupo == null) {
            chaveDoGrupo = GrupoAtividades.INDEFINIDO;
        }
        // Recuperar o grupo com a chave informada (se houver)
        final GrupoAtividades grupoAtividades = this.mapaGruposAtividades.get(chaveDoGrupo);

        if (grupoAtividades != null) {
            // Recuperar a lista de atividades do grupo
            return grupoAtividades.getAtividadesView();
        }
        return null;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private Transporte<?> criarAtividadeAPartirDeTrechoTransporte(final TrechoTransporte<?> trecho) throws InstantiationException,
            IllegalAccessException {
        final Transporte<?> transporte = trecho.getTransporte();
        // Criar uma atividadePlano "fake" como os dados do trecho
        final Transporte transporteFake = trecho.getTransporte().getClass().newInstance();
        transporteFake.setId(transporte.getId());
        transporteFake.addTrecho(trecho);
        transporteFake.setHoraInicio(trecho.getPartida().getHora());
        transporteFake.setHoraFim(trecho.getChegada().getHora());

        return transporteFake;
    }

    private void criarGrupos(final PlanoViagem planoViagem) {

        GrupoAtividades grupo = null;
        if (planoViagem.isPeriodoPorDatas()) {
            LocalDate dataInicial = new LocalDate(planoViagem.getDataInicio());
            final LocalDate dataFinal = new LocalDate(planoViagem.getDataFim());

            while (dataInicial.isBefore(dataFinal) || dataInicial.equals(dataFinal)) {
                grupo = new GrupoAtividades(dataInicial, TipoPeriodoViagem.POR_DATAS);
                this.mapaGruposAtividades.put(grupo.getChave(), grupo);
                // Somar um dia
                dataInicial = dataInicial.plusDays(1);
            }
        } else if (planoViagem.isPeriodoPorQuantidadeDias()) {
            for (int i = 1; i <= planoViagem.getQuantidadeDias(); i++) {
                grupo = new GrupoAtividades(i, TipoPeriodoViagem.POR_QUANTIDADE_DIAS);
                this.mapaGruposAtividades.put(grupo.getChave(), grupo);
            }
        }
        this.mapaGruposAtividades.put(GrupoAtividades.INDEFINIDO, new GrupoAtividades(GrupoAtividades.INDEFINIDO, TipoPeriodoViagem.NAO_DEFINIDO));
    }

    public class AtividadeView {

        private AtividadePlano atividadePlano;

        private Long idReferencia;

        /**
         * Indica se esta atividadePlano representa o término de outra atividadePlano.
         * Está vinculada a uma atividadePlano que se iniciou em outro dia.
         */
        private boolean termino;

        public AtividadeView(final AtividadePlano atividadePlano) {
            this.atividadePlano = atividadePlano;
            this.idReferencia = atividadePlano.getId();
        }

        public AtividadeView(final AtividadePlano atividadePlano, final boolean termino) {
            this(atividadePlano);
            this.termino = termino;
        }

        public AtividadePlano getAtividade() {
            return this.atividadePlano;
        }

        public Long getIdReferencia() {
            return this.idReferencia;
        }

        public boolean isTermino() {
            return this.termino;
        }

        public void setAtividade(final AtividadePlano atividadePlano) {
            this.atividadePlano = atividadePlano;
        }

        public void setIdReferencia(final Long idReferencia) {
            this.idReferencia = idReferencia;
        }

        public void setTermino(final boolean termino) {
            this.termino = termino;
        }

    }

    public class GrupoAtividades {

        private static final String INDEFINIDO = "INDEFINIDO";

        private static final long serialVersionUID = -5967842898166693384L;

        private final List<AtividadeView> atividades = new LinkedList<AtividadeView>();

        /**
         * Objeto que distingue cada dia da viagem.
         * No momento da criacao dos grupos essa chave auxilia na separacao das atividades por data ou dia
         * Pode ser nos formatos:
         * 
         * - Data (quanto o tipo do periodo da PlanoViagem for POR_DATAS)
         * - Dia (quanto o tipo do periodo da PlanoViagem for POR_QUANTIDADE_DIAS)
         * - Indefinido (quanto o tipo do periodo da PlanoViagem for NAO_DEFINIDO)
         */
        private final Object chave;

        private final PlanoViagem.TipoPeriodoViagem tipoPeriodo;

        public GrupoAtividades(final Object chave, final PlanoViagem.TipoPeriodoViagem tipoPeriodo) {
            this.chave = chave;
            this.tipoPeriodo = tipoPeriodo;
        }

        public List<AtividadeView> getAtividadesView() {
            return this.atividades;
        }

        public Object getChave() {
            return this.chave;
        }

        /**
         * Nome que sera exibido no agrupamento de atividades na tela de planejamento
         * 
         * @return
         */
        public String getNome() {
            if (this.tipoPeriodo.equals(TipoPeriodoViagem.POR_DATAS)) {
                final DateTimeFormatter fmt = DateTimeFormat.forPattern("EE',' dd 'de ' MMMM 'de' yyyy");
                return ((LocalDate) getChave()).toString(fmt);
            } else if (this.tipoPeriodo.equals(TipoPeriodoViagem.POR_QUANTIDADE_DIAS)) {
                return getChave() + "º dia";
            } else if (this.tipoPeriodo.equals(TipoPeriodoViagem.NAO_DEFINIDO)) {
                return "Sem data definida / Fora do período da viagem";
            }
            return "";
        }
    }
}
