package br.com.fanaticosporviagens.viagem;

import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class PermissaoViagemException extends BusinessException {

    private static final long serialVersionUID = 6900477316525472521L;

    private Usuario autorViagem;

    public PermissaoViagemException(final String message) {
        super(message);
    }

    public PermissaoViagemException(final String message, final Usuario autorViagem) {
        super(message);
        this.autorViagem = autorViagem;
    }

    public Usuario getAutorViagem() {
        return this.autorViagem;
    }

}
