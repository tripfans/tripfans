package br.com.fanaticosporviagens.viagem;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.infra.component.SolrResponseTripFans;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.model.repository.SortDirection;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.util.AjaxUtils;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.locais.SugestaoLocalService;
import br.com.fanaticosporviagens.model.entity.Atividade;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.DestinoViagem;
import br.com.fanaticosporviagens.model.entity.DiaViagem;
import br.com.fanaticosporviagens.model.entity.Endereco;
import br.com.fanaticosporviagens.model.entity.EntidadeIndexada;
import br.com.fanaticosporviagens.model.entity.InteresseAmigosEmLocal;
import br.com.fanaticosporviagens.model.entity.InteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavel;
import br.com.fanaticosporviagens.model.entity.LocalEnderecavelNaoCadastradoAtividade;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.LocalInteresseTuristico;
import br.com.fanaticosporviagens.model.entity.LocalType;
import br.com.fanaticosporviagens.model.entity.ParticipanteViagem;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.SugestaoLocal;
import br.com.fanaticosporviagens.model.entity.TipoAtividade;
import br.com.fanaticosporviagens.model.entity.TipoInteresseUsuarioEmLocal;
import br.com.fanaticosporviagens.model.entity.TipoTransporte;
import br.com.fanaticosporviagens.model.entity.TipoVisibilidade;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;
import br.com.fanaticosporviagens.pesquisaTextual.model.LocalParamsTextualSearchQuery;
import br.com.fanaticosporviagens.pesquisaTextual.model.service.PesquisaTextualService;
import br.com.fanaticosporviagens.util.TripFansEnviroment;

@Controller
@RequestMapping("/viagem")
public class ViagemController extends BaseController {

    @Autowired
    private AtividadeRepository atividadeRepository;

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private PesquisaTextualService pesquisaTextualService;

    @Autowired
    private SugestaoLocalService sugestaoLocalService;

    @Autowired
    private TripFansEnviroment tripFansEnviroment;

    @Autowired
    private ViagemService viagemService;

    @RequestMapping(value = "/{idViagem}/viajantes/{participante}/aceitarConvite/{convite}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView aceitarConvite(@PathVariable final Long idViagem, @PathVariable final ParticipanteViagem participante,
            @PathVariable final Convite convite, final ModelAndView model) {
        this.viagemService.aceitarConviteParticipacaoViagem(convite, participante);
        return exibir(model, idViagem);
    }

    @RequestMapping(value = "adicionarLocalEmNovaViagem", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarAtividadeEmNovaViagem(final ModelAndView model,
            @RequestParam(value = "tituloViagem", required = false) final String tituloViagem,
            @RequestParam(value = "local", required = true) final String local) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();

        final Viagem viagem = new Viagem();
        if (tituloViagem != null && !tituloViagem.trim().isEmpty()) {
            viagem.setTitulo(tituloViagem);
        } else {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            viagem.setTitulo("Viagem criada em " + sdf.format(new Date()));
        }

        this.viagemService.inserirViagem(usuarioAutenticado, viagem);
        final Atividade item = new Atividade();
        item.setViagem(viagem);
        final Local localVisita = this.localService.consultarPorUrlPath(local);
        item.setLocal(localVisita);

        this.viagemService.incluirAtividade(usuarioAutenticado, item);

        return this.success(localVisita.getNome() + " foi adicionado ao seu novo plano de viagem.");
    }

    @RequestMapping(value = "adicionarLocalEmViagem/{idViagem}/{numeroDia}/{local}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarAtividadeEmViagem(@PathVariable final Long idViagem, @PathVariable final Integer numeroDia,
            @PathVariable final String local, @RequestParam(value = "idSugestaoLocal", required = false) final Long idSugestaoLocal) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        final Local localVisita = this.localService.consultarPorUrlPath(local);
        if (localVisita == null) {
            return this.error(("O local informado não existe."));
        }
        final StatusMessage message = new StatusMessage();
        /*if (localVisita instanceof Cidade || localVisita instanceof LocalInteresseTuristico) {
            viagem.addDestino((LocalGeografico) localVisita);
            this.viagemService.salvar(viagem);
        } else*/
        if (localVisita instanceof LocalEnderecavel || localVisita instanceof LocalInteresseTuristico) {

            final Atividade atividade = new Atividade();
            atividade.setLocal(localVisita);

            if (numeroDia > 0) {
                final DiaViagem dia = viagem.getDia(numeroDia);
                atividade.setDia(dia);
            } else {
                atividade.setViagem(viagem);
            }

            if (idSugestaoLocal != null) {
                this.viagemService.incluirAtividadeAPartirSugestaoAmigo(usuarioAutenticado, atividade, idSugestaoLocal);
            } else {
                this.viagemService.incluirAtividade(usuarioAutenticado, atividade);
            }
            message.addParam("idAtividade", atividade.getId());
        }
        return message.success(localVisita.getNome() + " foi adicionado ao seu plano de viagem.");
    }

    @RequestMapping(value = "/{idViagem}/adicionarAtividades", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarAtividades(@PathVariable final Long idViagem, @RequestParam(required = false) final Integer dia,
            @RequestParam final Long[] idsLocais) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        this.viagemService.inserirAtividades(viagem, usuarioAutenticado, dia, idsLocais);

        return this.success("Atividades adicionadas com sucesso.");
    }

    @RequestMapping(value = "{idViagem}/adicionarDia", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarDia(final ModelAndView model, @PathVariable final Long idViagem) {
        this.viagemService.adicionarDiaViagem(this.getUsuarioAutenticado(), idViagem);
        return this.success("Dia adicionado com sucesso.");
    }

    @RequestMapping(value = "adicionarLocalEmNovaViagem/{urlLocal}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarLocalEmNovaViagem(@PathVariable("urlLocal") final String urlLocal) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Local local = this.localService.consultarPorUrlPath(urlLocal);

        if (local == null) {
            // TODO lancar excecao
        }

        final Viagem viagem = new Viagem();
        String nomeViagem = "Viagem para ";

        if (local instanceof LocalEnderecavel) {
            LocalGeografico cidade = null;
            if (local.getLocalizacao().getCidade() != null) {
                cidade = ((LocalEnderecavel) local).getLocalizacao().getCidade().getLocal();
            } else if (local.getLocalizacao().getLocalInteresseTuristico() != null) {
                cidade = ((LocalEnderecavel) local).getLocalizacao().getLocalInteresseTuristico().getLocal();
            }
            nomeViagem += cidade.getNome();
            viagem.addDestino(cidade);
        } else if ((local instanceof Cidade || local instanceof LocalInteresseTuristico)) {
            nomeViagem += local.getNome();
            viagem.addDestino((LocalGeografico) local);
        }

        viagem.setTitulo(nomeViagem);
        viagem.setVisibilidade(TipoVisibilidade.AMIGOS);
        viagem.setUsuarioCriador(usuarioAutenticado);

        this.viagemService.inserirViagem(usuarioAutenticado, viagem);

        if (local instanceof LocalEnderecavel) {
            final Atividade atividade = new Atividade();
            atividade.setViagem(viagem);
            atividade.setLocal(local);
            this.viagemService.incluirAtividade(usuarioAutenticado, atividade);
        }

        return this.success(local.getNome() + " foi adicionado ao seu plano de viagem.");
    }

    @RequestMapping(value = "{idViagem}/adicionarParticipante", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage adicionarParticipante(@ModelAttribute("participante") final ParticipanteViagem participante,
            @PathVariable final Long idViagem, final ModelAndView model) {
        this.viagemService.adicionarParticipante(idViagem, participante);
        return this.success("");
    }

    @RequestMapping(value = "{idViagem}/alterar", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage alterarViagem(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "titulo", required = true) final String titulo,
            @RequestParam(value = "descricao", required = false) final String descricao, @RequestParam(value = "dataInicio") final String dataInicio,
            @RequestParam(value = "visibilidade") final TipoVisibilidade visibilidade) throws ParseException {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        viagem.setTitulo(titulo);
        viagem.setDescricao(descricao);
        viagem.setVisibilidade(visibilidade);

        Date dataInicioViagem = null;
        if (dataInicio != null && !dataInicio.trim().isEmpty()) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dataInicioViagem = sdf.parse(dataInicio);
            viagem.setDataInicio(new LocalDate(dataInicioViagem));
        } else {
            viagem.setDataInicio(null);
        }

        this.viagemService.alterarViagem(usuarioAutenticado, viagem);
        return this.success("Viagem alterada com sucesso");
    }

    @RequestMapping(value = "{idViagem}/planejamento/atividades", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView atividades(final ModelAndView model, @PathVariable final Long idViagem) {
        model.addObject("mostrarAcoes", true);
        model.addObject("mostrarAcoesMover", false);
        model.addObject("mostarDiaAtividade", true);
        return this.exibirPagina(idViagem, "atividades", model);
    }

    private void configurarCustosAtividade(final Atividade atividade, String valor, final boolean paga, final boolean reservada) {
        if (valor != null && !valor.trim().isEmpty()) {
            valor = valor.replace(".", "");
            atividade.setValor(new BigDecimal(valor.replace(',', '.')));
        }
        atividade.setPaga(paga);
        atividade.setReservada(reservada);
    }

    private void configurarDiaAtividade(final Viagem viagem, final Atividade atividade, final String diaInicio, final String diaFim) {
        if (diaInicio.equalsIgnoreCase("0")) {
            atividade.setViagem(viagem);
        } else {
            final Long idDia = new Long(diaInicio);
            for (final DiaViagem dia : viagem.getDias()) {
                if (dia.getId().equals(idDia)) {
                    atividade.setDia(dia);
                    break;
                }
            }
            if (diaFim != null) {
                final Long idDiaFim = new Long(diaFim);
                for (final DiaViagem dia : viagem.getDias()) {
                    if (dia.getId().equals(idDiaFim)) {
                        atividade.setDiaFim(dia);
                        break;
                    }
                }
            }
        }
    }

    private void configurarHorariosAtividade(final Atividade atividade, final String strDataInicio, final String strDataFim) throws ParseException {
        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        if (strDataInicio != null && !strDataInicio.trim().isEmpty()) {
            final Date dataHoraInicio = sdf.parse(strDataInicio);
            atividade.setDataHoraInicio(dataHoraInicio);
        } else {
            atividade.setDataHoraInicio(null);
        }

        if (strDataFim != null && !strDataFim.trim().isEmpty()) {
            final Date dataHoraFim = sdf.parse(strDataFim);
            atividade.setDataHoraFim(dataHoraFim);
        } else {
            atividade.setDataHoraFim(null);
        }
    }

    private void configurarOutrasInformacoesAtividade(final Atividade atividade, final String descricao, final String anotacoes) {
        atividade.setDescricao(descricao);
        atividade.setAnotacoes(anotacoes);
    }

    private void configurarTransporteAtividade(final Atividade atividade, final TipoTransporte tipoTransporte, final TipoAtividade tipoAtividade,
            final Long local, final String nomeLocal, final String enderecoLocal, final Long localTransporteOrigem,
            final String nomeLocalTransporteOrigem, final String enderecoLocalTransporteOrigem, final Long localTransporteDestino,
            final String nomeLocalTransporteDestino, final String enderecoLocalTransporteDestino, final String tituloAtividade) {
        if (TipoAtividade.TRANSPORTE.equals(tipoAtividade)) {
            atividade.setTransporte(tipoTransporte);
            if (localTransporteOrigem != null) {
                final Local localOrigem = this.localService.consultarPorId(localTransporteOrigem);
                if (localOrigem != null) {
                    atividade.setTransporteLocalOrigem(localOrigem);
                }
            } else {
                final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();
                localNaoCadastrado.setNome(nomeLocalTransporteOrigem);
                if (enderecoLocal != null) {
                    localNaoCadastrado.setEndereco(new Endereco(enderecoLocalTransporteOrigem));
                }
                atividade.setLocalOrigemNaoCadastrado(localNaoCadastrado);
            }
            if (localTransporteDestino != null) {
                final Local localDestino = this.localService.consultarPorId(localTransporteDestino);
                if (localDestino != null) {
                    atividade.setTransporteLocalDestino(localDestino);
                }
            } else {
                final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();
                localNaoCadastrado.setNome(nomeLocalTransporteDestino);
                if (enderecoLocal != null) {
                    localNaoCadastrado.setEndereco(new Endereco(enderecoLocalTransporteDestino));
                }
                atividade.setLocalDestinoNaoCadastrado(localNaoCadastrado);
            }
        } else if (TipoAtividade.PERSONALIZADO.equals(tipoAtividade)) {
            atividade.setTitulo(tituloAtividade);
        } else {
            if (local != null) {
                final Local localVisita = this.localService.consultarPorId(local);
                if (localVisita != null) {
                    atividade.setLocal(localVisita);
                    if (localVisita.isTipoRestaurante()) {
                        atividade.setTipo(TipoAtividade.ALIMENTACAO);
                    } else if (localVisita.isTipoHotel()) {
                        atividade.setTipo(TipoAtividade.HOSPEDAGEM);
                    } else {
                        atividade.setTipo(TipoAtividade.VISITA_ATRACAO);
                    }
                }
            } else {
                final LocalEnderecavelNaoCadastradoAtividade localNaoCadastrado = new LocalEnderecavelNaoCadastradoAtividade();
                localNaoCadastrado.setNome(nomeLocal);
                if (enderecoLocal != null) {
                    localNaoCadastrado.setEndereco(new Endereco(enderecoLocal));
                }
                atividade.setLocalNaoCadastrado(localNaoCadastrado);
                atividade.setTipo(TipoAtividade.PERSONALIZADO);
                atividade.setTitulo(nomeLocal);
            }
        }
    }

    @RequestMapping(value = "{idViagem}/consultarItem", method = { RequestMethod.POST })
    public @ResponseBody JSONResponse consultarAtividade(@RequestParam(value = "idItem", required = true) final Long idItem) {
        final Atividade item = this.viagemService.consultarAtividadePorId(idItem);

        this.jsonResponse.addParam("tipo", item.getTipo().toString());
        this.jsonResponse.addParam("idItem", item.getId());
        this.jsonResponse.addParam("itemDono", (item.getViagem() != null) ? "viagem" : item.getDia().getId().toString());

        this.jsonResponse.addParam("localId", null);
        this.jsonResponse.addParam("localNome", null);
        this.jsonResponse.addParam("localNomeCompleto", null);
        this.jsonResponse.addParam("localUrlPath", null);
        this.jsonResponse.addParam("transporte", null);
        this.jsonResponse.addParam("transporteLocalOrigemId", null);
        this.jsonResponse.addParam("transporteLocalOrigemNome", null);
        this.jsonResponse.addParam("transporteLocalOrigemNomeCompleto", null);
        this.jsonResponse.addParam("transporteLocalOrigemUrlPath", null);
        this.jsonResponse.addParam("transporteLocalDestinoId", null);
        this.jsonResponse.addParam("transporteLocalDestinoNome", null);
        this.jsonResponse.addParam("transporteLocalDestinoNomeCompleto", null);
        this.jsonResponse.addParam("transporteLocalDestinoUrlPath", null);
        this.jsonResponse.addParam("titulo", null);
        this.jsonResponse.addParam("dataInicio", null);
        this.jsonResponse.addParam("dataFim", null);
        this.jsonResponse.addParam("valor", null);
        this.jsonResponse.addParam("descricao", null);

        if (item.getLocal() != null) {
            this.jsonResponse.addParam("localId", item.getLocal().getId());
            this.jsonResponse.addParam("localNome", item.getLocal().getNome());
            this.jsonResponse.addParam("localNomeCompleto", item.getLocal().getNomeCompleto());
            this.jsonResponse.addParam("localUrlPath", item.getLocal().getUrlPath());
        }
        if (item.getTransporte() != null) {
            this.jsonResponse.addParam("transporte", item.getTransporte().toString());
        }
        if (item.getTransporteLocalOrigem() != null) {
            this.jsonResponse.addParam("transporteLocalOrigemId", item.getTransporteLocalOrigem().getId());
            this.jsonResponse.addParam("transporteLocalOrigemNome", item.getTransporteLocalOrigem().getNome());
            this.jsonResponse.addParam("transporteLocalOrigemNomeCompleto", item.getTransporteLocalOrigem().getNomeCompleto());
            this.jsonResponse.addParam("transporteLocalOrigemUrlPath", item.getTransporteLocalOrigem().getUrlPath());
        }
        if (item.getTransporteLocalDestino() != null) {
            this.jsonResponse.addParam("transporteLocalDestinoId", item.getTransporteLocalDestino().getId());
            this.jsonResponse.addParam("transporteLocalDestinoNome", item.getTransporteLocalDestino().getNome());
            this.jsonResponse.addParam("transporteLocalDestinoNomeCompleto", item.getTransporteLocalDestino().getNomeCompleto());
            this.jsonResponse.addParam("transporteLocalDestinoUrlPath", item.getTransporteLocalDestino().getUrlPath());
        }
        this.jsonResponse.addParam("titulo", item.getTitulo());

        final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        if (item.getDataHoraInicio() != null) {
            this.jsonResponse.addParam("dataInicio", sdf.format(item.getDataHoraInicio()));
        }
        if (item.getDataHoraFim() != null) {
            this.jsonResponse.addParam("dataFim", sdf.format(item.getDataHoraFim()));
        }
        if (item.getValor() != null) {
            this.jsonResponse.addParam("valor", item.getValor().toString());
        }
        this.jsonResponse.addParam("descricao", item.getDescricao());

        return this.jsonResponse.success();
    }

    private void convertToPDF() {
        /*
        final Document document = new Document();
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        final URL url = new URL(this.tripFansEnviroment.getServerUrl() + "/viagem/" + idViagem + "/imprimirInterno/"
                + getUsuarioAutenticado().getId());
        final URL urlCss = new URL(this.tripFansEnviroment.getServerUrl() + "/wro/mainResponsive.css");
        final HttpURLConnection conexaoHttp = (HttpURLConnection) url.openConnection();
        conexaoHttp.setRequestMethod("GET");
        conexaoHttp.connect();

        final HttpURLConnection conexaoHttpCss = (HttpURLConnection) urlCss.openConnection();
        conexaoHttpCss.setRequestMethod("GET");
        conexaoHttpCss.connect();

        System.out.println(conexaoHttp.getContent());

        // conexaoHttp.

        final DOMParser parser = new DOMParser();

        final BufferedReader buff = new BufferedReader(new InputStreamReader((InputStream) conexaoHttp.getContent()));
        // box.setText("Getting data ...");
        final StringBuffer html = new StringBuffer();
        String line;
        do {
            line = buff.readLine();
            html.append(line + "\n");
        } while (line != null);
        // box.setText(text.toString());

        final Tidy tidy = new Tidy();
        final org.w3c.dom.Document xmlDoc = tidy.parseDOM(conexaoHttp.getInputStream(), System.out);

        tidy.parse(new BufferedReader(new InputStreamReader((InputStream) conexaoHttp.getContent())), System.out);

        tidy.pprint(xmlDoc, System.out);

        // xmlDoc.getTextContent();

        // xmlDoc.

        parser.parse(new InputSource(new StringReader(html.toString())));

        conexaoHttp.getContentType();
        conexaoHttp.getContentLength();
        // conexaoHttp.getContentType()

        final org.w3c.dom.Document doc = parser.getDocument();

        System.out.println((doc));
        // parser.parse(url.toString());
        parser.parse(new InputSource(conexaoHttp.getInputStream()));

        System.out.println(parser.getDocument());

        // parser.

        // parser.getDocument().geT*/

        /*final BufferedReader buff = new BufferedReader(new InputStreamReader((InputStream) conexaoHttpCss.getContent()));
        final StringBuffer cssBuff = new StringBuffer();
        String line;
        do {
            line = buff.readLine();
            if (line != null) {
                cssBuff.append(line + "\n");
            }
        } while (line != null);

        String cssFinal = cssBuff.toString();

        System.out.println(cssFinal);

        cssFinal = cssFinal.replaceAll("../resources", "www.tripfans.com.br/resources");
        cssFinal = cssFinal.replaceAll("font: inherit;", "");

        System.out.println(cssFinal);

        final ByteArrayInputStream bais = new ByteArrayInputStream(cssFinal.getBytes());

        // XMLWorker.

        // CSS
        final CSSResolver cssResolver = new StyleAttrCSSResolver();
        final CssFile cssFile = XMLWorkerHelper.getCSS(bais);
        cssResolver.addCss(cssFile);

        // HTML
        final XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontProvider.register("http://fonts.googleapis.com/css?family=Open+Sans+Condensed:light&v1");
        fontProvider.register("http://fonts.googleapis.com/css?family=PT+Sans+Narrow");

        /*fontProvider.register("resources/fonts/Cardo-Regular.ttf");
        fontProvider.register("resources/fonts/Cardo-Bold.ttf");
        fontProvider.register("resources/fonts/Cardo-Italic.ttf");
        fontProvider.addFontSubstitute("lowagie", "cardo");*/

        /*final CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
        final HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

        // Pipelines
        final PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
        final HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        final CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

        // XML Worker
        final XMLWorker worker = new XMLWorker(css, true);
        final XMLParser p = new XMLParser(worker);
        p.parse(conexaoHttp.getInputStream());

        // step 5
        document.close();

        // XMLWorkerHelper.getInstance().parseXHtml(writer, document, conexaoHttp.getInputStream(), bais, );
        // document.close();*/
    }

    @RequestMapping(value = "{idViagem}/copiar", method = { RequestMethod.POST, RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String copiarViagem(final ModelAndView model, @PathVariable final Long idViagem) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagemCopia = this.viagemService.copiarViagem(usuarioAutenticado, idViagem);

        this.success("Viagem copiada com o título '" + viagemCopia.getTitulo() + "'.");
        return "redirect:/viagem/" + viagemCopia.getId();
    }

    @RequestMapping(value = "/criar", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView criar(@RequestParam(value = "destino", required = false) final LocalGeografico destino, final ModelAndView model) {
        final Viagem viagem = new Viagem();

        if (destino != null) {
            final DestinoViagem destinoViagem = new DestinoViagem();
            destinoViagem.setDestino(destino);
            viagem.addDestino(destinoViagem);
        }

        model.addObject("viagem", viagem);
        model.setViewName("/viagem/planejamento/editarViagemForm");
        return model;
    }

    @RequestMapping(value = "/criar/{viagem}/{passo}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView criar(@PathVariable final Viagem viagem, @PathVariable final Integer passo, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarItem() && (passo > 1 && passo <= 4)) {
            model.addObject("viagem", viagem);

            if (passo == 2) {
                model.addObject("urlPathCidadeSelecionada", viagem.getPrimeiroDestino().getDestino().getUrlPath());
                model.addObject("tipoLocal", LocalType.ATRACAO);
                model.addObject("diaInicio", 0);
                model.addObject("cidadeSelecionada", viagem.getPrimeiroDestino().getDestino());
            }

            if (passo == 3) {
                pedirAjudaAmigos(viagem, model);
            }
            model.addObject("showFooter", false);
            model.setViewName("/viagem/planejamento/passo" + passo);
            return model;
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    @RequestMapping(value = "/{viagem}/criarAtividade", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView criarAtividade(@PathVariable final Viagem viagem, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarItem()) {
            model.addObject("latLongDestinos", getLatLongDestinos(viagem));
            model.addObject("viagem", viagem);
            model.addObject("atividade", new Atividade());
            model.addObject("tiposAtividade", TipoAtividade.listaReduzidaOrdenada());
            model.addObject("transportes", TipoTransporte.listaOrdenada());
            model.setViewName("/viagem/planejamento/editarAtividadeForm");
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    @RequestMapping(value = "/{viagem}/criarAtividade/{tipoAtividade}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView criarAtividade(@PathVariable final Viagem viagem, final ModelAndView model, @PathVariable final TipoAtividade tipoAtividade,
            @RequestParam(required = false) final Integer diaInicio, @RequestParam(required = false) final Integer diaFim,
            @RequestParam(required = false) final String urlPathOrigem, @RequestParam(required = false) final String urlPathDestino) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarItem()) {
            model.addObject("latLongDestinos", getLatLongDestinos(viagem));
            model.addObject("viagem", viagem);
            final Atividade atividade = new Atividade();
            if (tipoAtividade.equals(TipoAtividade.HOSPEDAGEM)) {
                model.addObject("tiposAtividade", new TipoAtividade[] { TipoAtividade.HOSPEDAGEM });
            } else if (tipoAtividade.equals(TipoAtividade.TRANSPORTE)) {
                if (urlPathOrigem != null) {
                    final Local origem = this.localService.consultarPorUrlPath(urlPathOrigem);
                    if (origem != null) {
                        atividade.setTransporteLocalOrigem(origem);
                    }
                }
                if (urlPathDestino != null) {
                    final Local destino = this.localService.consultarPorUrlPath(urlPathDestino);
                    if (destino != null) {
                        atividade.setTransporteLocalDestino(destino);
                    }
                }

                model.addObject("tiposAtividade", new TipoAtividade[] { TipoAtividade.TRANSPORTE });
            } else {
                model.addObject("tiposAtividade", TipoAtividade.listaReduzidaOrdenada());
            }
            model.addObject("atividade", atividade);
            model.addObject("transportes", TipoTransporte.listaOrdenada());
            model.addObject("tipo", tipoAtividade);
            if (diaInicio != null) {
                final DiaViagem dia = viagem.getDia(diaInicio);
                if (dia != null) {
                    atividade.setDia(dia);
                }
            }
            if (diaFim != null) {
                final DiaViagem dia = viagem.getDia(diaFim);
                if (dia != null) {
                    atividade.setDiaFim(dia);
                }
            }
            model.setViewName("/viagem/planejamento/editarAtividadeForm");
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    @RequestMapping(value = "/planejamento/criar", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView criarRoteiro(@RequestParam(value = "destino", required = false) final LocalGeografico destino, final ModelAndView model) {
        model.addObject("abrirPopup", true);
        final ModelAndView inicio = this.inicio(model);
        if (destino != null) {
            model.addObject("destino", destino);
        }
        return inicio;
    }

    @RequestMapping(value = "/criar", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String criarViagem(@RequestParam(value = "titulo", required = true) final String titulo,
            @RequestParam(value = "descricao", required = false) final String descricao,
            @RequestParam(value = "quantidadeDias", required = true) final Integer quantidadeDias,
            @RequestParam(value = "dataInicio") final String dataInicio, @RequestParam(value = "visibilidade") final TipoVisibilidade visibilidade,
            final Model model) throws ParseException {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        Date dataInicioViagem = null;
        if (dataInicio != null && !dataInicio.trim().isEmpty()) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dataInicioViagem = sdf.parse(dataInicio);
        }
        final Viagem viagem = new Viagem(titulo, descricao, quantidadeDias, dataInicioViagem, usuarioAutenticado, visibilidade);
        this.viagemService.inserirViagem(usuarioAutenticado, viagem);
        return "/viagem/planejamento/editarPlano";
    }

    @RequestMapping(value = "{idViagem}/planejamento/destinosPrincipais", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView destinosPrincipais(ModelAndView model, @PathVariable final Long idViagem) {
        model = this.exibirViagem(model, idViagem, ViagemModoVisao.LISTA_ITENS.getNome());
        return this.exibirPagina(idViagem, "destinosPrincipais", model);
    }

    @RequestMapping(value = "/editar/{idViagem}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView editar(@PathVariable final Long idViagem, final ModelAndView model) {
        final Viagem viagem = this.viagemService.consultarViagem(getUsuarioAutenticado(), idViagem);
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarDadosDaViagem()) {
            model.addObject("viagem", viagem);
            model.setViewName("/viagem/planejamento/editarViagemForm");
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    @RequestMapping(value = "/editarAtividade/{idAtividade}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView editarAtividade(@PathVariable final Long idAtividade, final ModelAndView model) {
        final Atividade atividade = this.viagemService.consultarAtividadePorId(idAtividade);
        Viagem viagem = null;
        if (atividade.getViagem() != null) {
            viagem = atividade.getViagem();
        } else {
            viagem = atividade.getDia().getViagem();
            atividade.getDia().getViagem().getDias();
        }
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarDadosDaViagem()) {
            model.addObject("viagem", viagem);
            model.addObject("atividade", atividade);
            model.addObject("tiposAtividade", TipoAtividade.listaReduzidaOrdenada());
            if (atividade.getTipo().equals(TipoAtividade.HOSPEDAGEM)) {
                model.addObject("tiposAtividade", new TipoAtividade[] { TipoAtividade.HOSPEDAGEM });
            } else if (atividade.getTipo().equals(TipoAtividade.TRANSPORTE)) {
                model.addObject("tiposAtividade", new TipoAtividade[] { TipoAtividade.TRANSPORTE });
            } else {
                model.addObject("tiposAtividade", TipoAtividade.listaReduzidaOrdenada());
            }
            model.addObject("tipo", atividade.getTipo());
            model.addObject("transportes", TipoTransporte.listaOrdenada());
            model.setViewName("/viagem/planejamento/editarAtividadeForm");
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    @RequestMapping(value = "{idViagem}/planejamento/enviarRoteiroPorEmail", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage enviarRoteiroPorEmail(@PathVariable final Long idViagem, final ModelAndView model) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        try {
            final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(usuarioAutenticado, viagem);
            if (viagemPermissao.isUsuarioPodeAlterarDadosDaViagem()) {
                // takeSnapshot(url.toString());

                this.viagemService.enviarRoteiroPorEmail(viagem, getUsuarioAutenticado());
            } else {
                return this.error("Sem permissão para acessar este roteiro");
            }
        } catch (final Exception e) {
            // e.printStackTrace();
            return this.error(e.getMessage());
        }
        return this.success("O roteiro foi enviado com sucesso para o seu email.");
    }

    @RequestMapping(value = "/{viagem}/ajudar/enviarSugestoes", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView enviarSugestoes(@PathVariable final Viagem viagem,
            @RequestParam(value = "idsLocais", required = false) final List<Long> idsLocais,
            @RequestParam(value = "idsLocaisImperdiveis", required = false) final List<Long> idsLocaisImperdiveis, final ModelAndView model) {
        return enviarSugestoes(viagem, null, idsLocais, idsLocaisImperdiveis, model);
    }

    @RequestMapping(value = "/{viagem}/ajudar/{pedido}/enviarSugestoes", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView enviarSugestoes(@PathVariable final Viagem viagem, @PathVariable final PedidoDica pedido,
            @RequestParam(value = "idsLocais", required = false) final List<Long> idsLocais,
            @RequestParam(value = "idsLocaisImperdiveis", required = false) final List<Long> idsLocaisImperdiveis, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeVerViagem()) {
            if (pedido != null && pedido.getAutor().equals(getUsuarioAutenticado())) {
                this.sugestaoLocalService.enviarSugestoes(pedido, idsLocais, idsLocaisImperdiveis);
            } else {
                this.sugestaoLocalService.enviarSugestoes(viagem, idsLocais, idsLocaisImperdiveis);
            }
        }
        return model;
    }

    @RequestMapping(value = "{idViagem}/excluirItem", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage excluirAtividade(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idItem) {
        try {
            this.viagemService.excluirAtividade(this.getUsuarioAutenticado(), idItem);
            return this.success("Atividade excluída com sucesso");
        } catch (final BusinessException e) {
            return this.error(e.getMessage());
        }
    }

    @RequestMapping(value = "{idViagem}/excluirDia", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage excluirDia(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idDia", required = true) final Long idDia) {
        try {
            this.viagemService.excluirDia(this.getUsuarioAutenticado(), idDia);
            return this.success("Dia excluído com sucesso");
        } catch (final BusinessException e) {
            return this.error(e.getMessage());
        }
    }

    @RequestMapping(value = "{idViagem}/excluir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage excluirViagem(final ModelAndView model, @PathVariable final Long idViagem) {
        try {
            this.viagemService.excluirViagem(this.getUsuarioAutenticado(), idViagem);
            return this.success("Viagem excluída com sucesso");
        } catch (final BusinessException e) {
            return this.error(e.getMessage());
        }
    }

    @RequestMapping(value = "{idViagem}", method = { RequestMethod.GET, RequestMethod.POST })
    // @PreAuthorize("isAuthenticated()")
    public ModelAndView exibir(final ModelAndView model, @PathVariable final Long idViagem) {
        this.exibirViagem(model, idViagem, ViagemModoVisao.LISTA_ITENS.getNome());
        model.setViewName("/viagem/planejamento/principal");
        if (model.getModel().containsKey("necessarioAutenticar") || model.getModel().containsKey("necessarioAmizade")) {
            model.setViewName("/viagem/planejamento/naoAutorizado");
        }
        return model;
    }

    @RequestMapping(value = "{idViagem}/{aba}", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView exibir(ModelAndView model, @PathVariable final Long idViagem, @PathVariable final String aba) {
        model = this.exibir(model, idViagem);
        model.addObject("aba", aba);
        return model;
    }

    @RequestMapping(value = "/{idViagem}/planejamento/{pagina}")
    public ModelAndView exibirPagina(@PathVariable final Long idViagem, @PathVariable final String pagina, final ModelAndView model) {
        exibir(model, idViagem);
        if (pagina != null) {
            final Viagem viagem = (Viagem) model.getModel().get("viagem");
            if (getUsuarioAutenticado() != null && !getUsuarioAutenticado().getId().equals(viagem.getUsuarioCriador().getId())) {
                model.addObject("diaAberto", true);
            }
            if (AjaxUtils.isAjaxRequest(this.request)) {
                model.setViewName("/viagem/planejamento/" + pagina);
                return model;
            }
            model.addObject("pagina", pagina);
        }
        return model;
    }

    @RequestMapping(value = "/{idViagem}/planejamento/{pagina}/{aba}")
    public ModelAndView exibirPagina(@PathVariable final Long idViagem, @PathVariable final String pagina, @PathVariable final String aba,
            ModelAndView model) {
        model = exibirPagina(idViagem, pagina, model);
        model.addObject("aba", aba);
        return model;
    }

    @RequestMapping(value = "{idViagem}/exibir/{modoVisao}", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView exibirViagem(final ModelAndView model, @PathVariable final Long idViagem, @PathVariable final String modoVisao) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        Viagem viagem = null;
        try {
            viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);
        } catch (final PermissaoViagemException e) {
            if (usuarioAutenticado == null) {
                model.addObject("necessarioAutenticar", true);
            } else {
                model.addObject("autorViagem", e.getAutorViagem());
                model.addObject("necessarioAmizade", true);
            }
            return model;
        }

        final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(usuarioAutenticado, viagem);
        model.addObject("viagem", viagem);
        model.addObject("primeiroDestino", viagem.getPrimeiroDestino().getDestino());
        model.addObject("permissao", viagemPermissao);
        model.addObject("tiposAtividade", TipoAtividade.listaReduzidaOrdenada());
        model.addObject("transportes", TipoTransporte.listaOrdenada());
        model.addObject("visibilidades", TipoVisibilidade.values());

        if (usuarioAutenticado == null || (!viagemPermissao.isUsuarioPodeAlterarDadosDaViagem())) {
            model.addObject("diaAberto", true);
        }

        if (ViagemModoVisao.CALENDARIO.toString().equalsIgnoreCase(modoVisao)) {
            model.addObject("modoVisao", ViagemModoVisao.CALENDARIO);
        } else {
            model.addObject("modoVisao", ViagemModoVisao.LISTA_ITENS);

        }
        model.addObject("modoVisaoCalendario", ViagemModoVisao.CALENDARIO);
        model.addObject("modoVisaoListaItens", ViagemModoVisao.LISTA_ITENS);

        final List<ViagemModoVisao> visoes = Arrays.asList(ViagemModoVisao.values());
        Collections.reverse(Arrays.asList(ViagemModoVisao.values()));
        model.addObject("visoesViagem", visoes);

        model.setViewName("/viagem/viagemExibir");

        return model;
    }

    private String getLatLongDestinos(final Viagem viagem) {
        final StringBuffer latLongDestinos = new StringBuffer();
        for (final LocalGeografico cidade : viagem.getCidades()) {
            latLongDestinos.append("(" + cidade.getLatitude() + "," + cidade.getLongitude() + ");");
        }
        return latLongDestinos.toString();
    }

    @RequestMapping(value = "/iframe/{viagem}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView iframe(@PathVariable final Viagem viagem, @RequestParam(required = false) final LocalType tipoLocal,
            @RequestParam(required = false) final String cidade, @RequestParam(required = false) final TipoAtividade tipoAtividade,
            @RequestParam(required = false) final Integer diaInicio, @RequestParam(required = false) final Integer diaFim,
            @RequestParam(required = false) final String acao, final ModelAndView model) {
        if (cidade == null || cidade.trim().equals("")) {
            model.addObject("urlPathCidadeSelecionada", viagem.getPrimeiroDestino().getDestino().getUrlPath());
        } else {
            model.addObject("urlPathCidadeSelecionada", cidade);
        }
        model.addObject("tipoLocal", tipoLocal);
        model.addObject("diaInicio", diaInicio);
        model.addObject("diaFim", diaFim);
        model.addObject("cidadeSelecionada", cidade);
        model.addObject("tipoAtividade", tipoAtividade);
        model.addObject("acao", acao);
        model.addObject("viagem", viagem);
        model.setViewName("/viagem/planejamento/iframe");
        return model;
    }

    @RequestMapping(value = "{idViagem}/planejamento/imprimir", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView imprimir(@PathVariable final Long idViagem, final ModelAndView model) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(usuarioAutenticado, viagem);
        model.addObject("viagem", viagem);
        model.addObject("permissao", viagemPermissao);
        model.addObject("mostrarAcoes", false);
        model.addObject("impressao", true);
        model.setViewName("/viagem/planejamento/imprimirNovo");
        return model;
    }

    /** Sem autenticacao **/
    @RequestMapping(value = "{idViagem}/imprimirInterno/{usuario}", method = { RequestMethod.GET })
    public ModelAndView imprimirInterno(@PathVariable final Long idViagem, final Usuario usuario, final ModelAndView model) {
        final Viagem viagem = this.viagemService.consultarViagem(usuario, idViagem);
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(usuario, viagem);
        if (permissao.isUsuarioPodeAlterarDadosDaViagem()) {
            model.addObject("viagem", viagem);
            model.addObject("permissao", permissao);
            model.addObject("mostrarAcoes", false);
            model.addObject("impressao", true);
            model.addObject("impressaoPDF", true);
            model.addObject("showHeader", false);
            model.addObject("showFooter", false);
            model.setViewName("/viagem/planejamento/imprimirNovo");
        }
        return model;
    }

    @RequestMapping(value = "/adicionar", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody String incluirViagem(final ModelAndView model, @RequestParam(value = "titulo", required = true) final String titulo,
            @RequestParam(value = "descricao", required = false) final String descricao,
            @RequestParam(value = "quantidadeDias", required = true) final Integer quantidadeDias,
            @RequestParam(value = "dataInicio") final String dataInicio, @RequestParam(value = "visibilidade") final TipoVisibilidade visibilidade)
                    throws ParseException {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();

        Date dataInicioViagem = null;
        if (dataInicio != null && !dataInicio.trim().isEmpty()) {
            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            dataInicioViagem = sdf.parse(dataInicio);
        }

        final Viagem viagem = new Viagem(titulo, descricao, quantidadeDias, dataInicioViagem, usuarioAutenticado, visibilidade);

        this.viagemService.inserirViagem(usuarioAutenticado, viagem);

        return viagem.getId().toString();
    }

    @RequestMapping(value = "/planejamento/inicio", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView inicio(ModelAndView model) {
        model = listar(model, ViagemListarModoVisao.MINHAS_VIAGENS);
        model.addObject("viagem", new Viagem());
        model.setViewName("/viagem/planejamento/inicio");
        return model;
    }

    @RequestMapping(value = "/listar", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listar(final ModelAndView model) {
        return listar(model, ViagemListarModoVisao.MINHAS_VIAGENS);
    }

    @RequestMapping(value = "/listar/{visaoListar}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listar(final ModelAndView model, @PathVariable ViagemListarModoVisao visaoListar) {

        final Usuario usuario = this.getUsuarioAutenticado();

        List<ViagemListarModoVisao> visoes = null;
        if (usuario == null) {
            visoes = ViagemListarModoVisao.getVisoesOrdenadasQueNaoPrecisaoDeUsuarioAutenticado();
            if (visaoListar.isNecessitaUsuarioAutenticado()) {
                visaoListar = ViagemListarModoVisao.VIAGENS_TRIPFANS;
            }
        } else {
            visoes = ViagemListarModoVisao.getVisoesOrdenadas();

            if (visaoListar.equals(ViagemListarModoVisao.MINHAS_VIAGENS)) {
                model.addObject("viagensProximas", this.viagemService.viagemConsultarProximasViagens(usuario));
                model.addObject("viagensRealizadas", this.viagemService.viagemConsultarViagensRealizadas(usuario));
            } else if (visaoListar.equals(ViagemListarModoVisao.VIAGENS_CONVIDADO)) {
                // model.addObject("viagensAmigos", this.viagemService.viagemConsultarViagensAmigos(usuario));
            } else if (visaoListar.equals(ViagemListarModoVisao.VIAGENS_AMIGOS)) {
                model.addObject("viagensAmigos", this.viagemService.viagemConsultarViagensAmigos(usuario));
            }
        }
        if (visaoListar.equals(ViagemListarModoVisao.VIAGENS_AGENCIAS)) {
            model.addObject("viagensAgencias", this.viagemService.viagemConsultarViagensAgencias());
        } else if (visaoListar.equals(ViagemListarModoVisao.VIAGENS_PUBLICAS)) {
            model.addObject("viagensPublicas", this.viagemService.viagemConsultarViagensPublicas());
        } else if (visaoListar.equals(ViagemListarModoVisao.VIAGENS_TRIPFANS)) {
            model.addObject("viagensTripFans", this.viagemService.viagemConsultarViagensTripFans());
        }

        model.addObject("permissao", this.viagemService.viagemObterPermissoes(this.getUsuarioAutenticado(), null));
        model.addObject("visibilidades", Arrays.asList(TipoVisibilidade.values()));
        model.addObject("visoesViagem", Arrays.asList(ViagemModoVisao.values()));
        model.addObject("visaoListar", visaoListar);
        model.addObject("visoesListar", visoes);
        model.addObject("modoVisaoCalendario", ViagemModoVisao.CALENDARIO);
        model.addObject("modoVisaoListaItens", ViagemModoVisao.LISTA_ITENS);

        model.setViewName("/viagem/viagemListar");

        return model;
    }

    @RequestMapping(value = "{idViagem}/listarCompartilhamentos", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listarCompartilhamentos(@PathVariable final Long idViagem, final ModelAndView model) {
        model.addObject("participantes", this.viagemService.consultarCompartilhamentos(idViagem));
        model.setViewName("/viagem/planejamento/listaParticipantes");
        return model;
    }

    @RequestMapping(value = "{idViagem}/listarViajantes", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listarViajantes(@PathVariable final Long idViagem, final ModelAndView model) {
        model.addObject("participantes", this.viagemService.consultarViajantes(idViagem));
        model.setViewName("/viagem/planejamento/listaParticipantes");
        return model;
    }

    @RequestMapping(value = "{idViagem}/moverAtividade", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividade(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(required = true) final Long idAtividade, @RequestParam(required = true) final Integer posicao) {
        this.viagemService.moverAtividadeParaPosicao(getUsuarioAutenticado(), idAtividade, posicao);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/moverItemDecrementarPosicao", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaBaixo(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idAtividade, @RequestParam(required = false) Integer quantidadePosicoes) {
        if (quantidadePosicoes == null) {
            quantidadePosicoes = -1;
        } else if (quantidadePosicoes > 0) {
            quantidadePosicoes = -quantidadePosicoes;
        }
        this.viagemService.moverAtividade(getUsuarioAutenticado(), idAtividade, quantidadePosicoes);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/moverItemIncrementarPosicao", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaCima(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idAtividade, @RequestParam(required = false) Integer quantidadePosicoes) {
        if (quantidadePosicoes == null) {
            quantidadePosicoes = 1;
        }
        this.viagemService.moverAtividade(this.getUsuarioAutenticado(), idAtividade, quantidadePosicoes);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/movimentarAtividadesParaDia", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaDia(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idsAtividades", required = true) final Long idsAtividades[],
            @RequestParam(value = "idDiaDestino", required = true) final Long idDiaDestino) {

        if (idDiaDestino == 0) {
            return this.movimentarAtividadesParaDiaADefinir(model, idViagem, idsAtividades);
        } else {
            final List<Atividade> atividades = this.viagemService.movimentarAtividadesParaDia(this.getUsuarioAutenticado(), idsAtividades,
                    idDiaDestino, null);
        }
        /*if (atividade.getDiaFim() != null) {
            statusMessage.addParam("diaFim", atividade.getDiaFim().getId());
        }*/
        return this.success("As atividades foram movidas com sucesso");
    }

    @RequestMapping(value = "{idViagem}/movimentarItemParaDia", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaDia(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idItem,
            @RequestParam(value = "idDiaDestino", required = true) final Long idDiaDestino) {
        if (idDiaDestino == 0) {
            return moverAtividadeParaDiaADefinir(model, idViagem, idItem);
        }

        final Atividade atividade = this.viagemService.movimentarAtividadeParaDia(this.getUsuarioAutenticado(), idItem, idDiaDestino, null);
        final StatusMessage statusMessage = new StatusMessage();
        if (atividade.getDiaFim() != null) {
            statusMessage.addParam("diaFim", atividade.getDiaFim().getId());
        }
        return statusMessage.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/movimentarItemParaViagem", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaDiaADefinir(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idItem) {
        this.viagemService.movimentarAtividadeParaOrganizar(this.getUsuarioAutenticado(), idItem);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/moverItemParaPrimeiro", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaPrimeiro(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idAtividade) {
        this.viagemService.moverAtividadeParaPrimeiro(this.getUsuarioAutenticado(), idAtividade);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/moverItemParaUltimo", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage moverAtividadeParaUltimo(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "idItem", required = true) final Long idAtividade) {
        this.viagemService.moverAtividadeParaUltimo(this.getUsuarioAutenticado(), idAtividade);
        return this.success("A atividade foi movida com sucesso");
    }

    @RequestMapping(value = "{idViagem}/movimentarAtividadesParaDiaADefinir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage movimentarAtividadesParaDiaADefinir(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(required = true) final Long[] idsAtividades) {
        this.viagemService.movimentarAtividadesParaDiaADefinir(this.getUsuarioAutenticado(), idsAtividades);
        return this.success("As atividades foram movidas com sucesso");
    }

    @RequestMapping(value = "{idViagem}/organizarRoteiroAutomaticamente", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage organizarRoteiroAutomaticamente(@PathVariable final Long idViagem, final ModelAndView model) {
        this.viagemService.organizarRoteiroAutomaticamente(idViagem);
        return this.success("Roteiro organizado com sucesso!");
    }

    @RequestMapping(value = "/{viagem}/pedirAjudaAmigos", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView pedirAjudaAmigos(@PathVariable final Viagem viagem, final ModelAndView model) {
        final List<Local> locais = new ArrayList<Local>();
        for (final DestinoViagem destino : viagem.getDestinosViagem()) {
            locais.add(destino.getDestino());
        }
        final Map<Usuario, List<Local>> mapaAmigosLocais = new HashMap<Usuario, List<Local>>();

        // Recuperar amigos que já foram
        final List<InteresseAmigosEmLocal> listaInteresses = this.interesseUsuarioEmLocalService.consultarInteressesAmigosEmLocais(viagem.getAutor(),
                locais, TipoInteresseUsuarioEmLocal.JA_FOI, -1);

        if (listaInteresses == null || listaInteresses.isEmpty()) {
            model.addObject("possuiAmigos", false);
        } else {
            List<Local> locaisAmigos = null;
            for (final InteresseAmigosEmLocal interesseAmigo : listaInteresses) {

                for (final InteresseUsuarioEmLocal interesse : interesseAmigo.getAmostraInteresses()) {
                    if (!mapaAmigosLocais.containsKey(interesse.getUsuario())) {
                        locaisAmigos = new ArrayList<Local>();
                        locaisAmigos.add(interesse.getLocal());
                        mapaAmigosLocais.put(interesse.getUsuario(), locaisAmigos);
                    } else {
                        mapaAmigosLocais.get(interesse.getUsuario()).add(interesse.getLocal());
                    }
                }

            }
            model.addObject("possuiAmigos", true);
            model.addObject("amigosJaForam", mapaAmigosLocais);

            // Recuperar pedidos já enviados
            final List<PedidoDica> pedidosJaEnviados = this.pedidoDicaService.consultarTodosPedidosPorViagem(viagem);
            final List<Long> idsUsuariosJaEnviados = new ArrayList<Long>();
            if (!pedidosJaEnviados.isEmpty()) {
                for (final PedidoDica pedido : pedidosJaEnviados) {
                    idsUsuariosJaEnviados.add(pedido.getPraQuemPedir().getId());
                }
                model.addObject("idsUsuariosJaEnviados", idsUsuariosJaEnviados);
            }
        }
        model.setViewName("/viagem/planejamento/pedirAjudaAmigos");
        return model;
    }

    @RequestMapping(value = "/{idViagem}/pedirDicas", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage pedirDicas(@PathVariable final Long idViagem, @RequestParam(required = false) final Long[] idsAmigosTripFans,
            @RequestParam(required = false) final String[] idsAmigosFacebook,
            @RequestParam(value = "requestId", required = false) final String facebookAppRequestId,
            @RequestParam(required = false) final String mensagem, @RequestParam(required = false) final String facebookPostId, final Model model) {
        this.viagemService.pedirDicas(idViagem, idsAmigosTripFans, idsAmigosFacebook, facebookAppRequestId, mensagem);
        return this.success(String.format("Seus pedidos foram enviados com sucesso!"));
    }

    @RequestMapping(value = "/{idViagem}/pedirDicasAmigosPorEnderecosDeEmail", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage pedirDicasAmigosPorEnderecosDeEmail(@PathVariable final Long idViagem, final String enderecosEmail) {
        this.viagemService.pedirDicasDeViagemParaAmigosPorEmails(idViagem, enderecosEmail);
        return this.success(String.format("Seus pedidos foram enviados com sucesso!"));
    }

    @RequestMapping(value = "/{viagem}/pontosMapa", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView pontosMapa(@PathVariable final Viagem viagem, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeVerViagem()) {
            model.addObject("viagem", viagem);
            model.setViewName("/viagem/planejamento/pontosMapa");
        }
        return model;
    }

    @RequestMapping(value = "{idViagem}/publicar", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage publicarViagem(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam final TipoVisibilidade visibilidade) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.publicarViagem(usuarioAutenticado, idViagem, visibilidade);

        return this.success("Viagem '" + viagem.getTitulo() + "' foi publicada com sucesso.");
    }

    @RequestMapping(value = "/{idViagem}/recarregarDia/{idDia}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView recarregarDia(final ModelAndView model, @PathVariable final Long idViagem, @PathVariable final Long idDia) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);
        final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(usuarioAutenticado, viagem);

        model.addObject("viagem", viagem);
        model.addObject("permissao", viagemPermissao);

        if (idDia == 0) {
            model.addObject("diaID", 0);
            model.addObject("valorDia", viagem.getValorItens());
            model.addObject("atividadesOrdenadas", viagem.getItensOrdenados());
        } else {
            model.addObject("diaID", idDia);
            DiaViagem diaViagem = null;
            for (final DiaViagem dia : viagem.getDias()) {
                if (dia.getId().equals(idDia)) {
                    diaViagem = dia;
                    break;
                }
            }
            model.addObject("dia", diaViagem);
            model.addObject("numeroDia", diaViagem.getNumero());
            model.addObject("dataDia", diaViagem.getDataViagem());
            model.addObject("valorDia", diaViagem.getValor());
            model.addObject("atividadesOrdenadas", diaViagem.getItensOrdenados());
        }
        model.addObject("mostrarAcoes", true);
        model.addObject("mostrarAcoesMover", true);
        model.addObject("diaAberto", true);
        model.setViewName("/viagem/planejamento/painelDia3");

        return model;
    }

    @RequestMapping(value = "/{idViagem}/recarregarPainelAtividades/{tipoAtividade}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView recarregarPainelAtividades(final ModelAndView model, @PathVariable final Long idViagem,
            @PathVariable final TipoAtividade tipoAtividade) {

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);
        final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(usuarioAutenticado, viagem);

        model.addObject("viagem", viagem);
        model.addObject("permissao", viagemPermissao);
        model.addObject("tipoAtividade", tipoAtividade);
        model.addObject("atividadesPorTipo", viagem.getAtividadesPorTipoOrdenadas(tipoAtividade));
        model.addObject("mostrarAcoes", true);
        model.addObject("mostrarAcoesMover", false);
        model.addObject("painelAberto", true);
        model.setViewName("/viagem/planejamento/painelAtividadesPorTipo");

        return model;
    }

    @RequestMapping(value = "/{idViagem}/recarregarResumoAtividades", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView recarregarResumoAtividades(final ModelAndView model, @PathVariable final Long idViagem) {
        final Viagem viagem = this.viagemService.consultarViagem(this.getUsuarioAutenticado(), idViagem);
        final ViagemPermissao viagemPermissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        model.addObject("viagem", viagem);
        model.addObject("permissao", viagemPermissao);
        model.setViewName("/viagem/planejamento/painelResumoAtividades");
        return model;
    }

    /**
     * Remover atividade (Local) do plano pelo idAtividade
     *
     * @param idViagem
     * @param idAtividade
     * @return
     */
    @RequestMapping(value = "removerAtividadeViagem/{idViagem}/{idAtividade}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage removerAtividadeViagem(@PathVariable final Long idViagem, @PathVariable final Long idAtividade) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        this.viagemService.excluirAtividade(usuarioAutenticado, idAtividade);
        return this.success("Local removido do seu plano de viagem.");
    }

    /**
     * Remover Local do plano pelo idLocal quando não souber o idAtividade
     *
     * @param idViagem
     * @param idLocal
     * @return
     */
    @RequestMapping(value = "removerLocalViagem/{idViagem}/{idLocal}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage removerLocalViagem(@PathVariable final Long idViagem, @PathVariable final Long idLocal) {
        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        this.viagemService.excluirAtividadePeloIdLocal(usuarioAutenticado, idViagem, idLocal);
        return this.success("Local removido do seu plano de viagem.");
    }

    @RequestMapping(value = "{idViagem}/removerParticipante/{participante}", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage removerParticipante(@PathVariable final ParticipanteViagem participante, @PathVariable final Long idViagem,
            final ModelAndView model) {
        this.viagemService.removerParticipante(idViagem, participante);
        return this.success("");
    }

    /**
     * TODO Remover após conclusão
     * ################
     * Metodo temporario para ajuste da ordem das atividades de uma viagem
     * ################
     */
    @RequestMapping(value = "/reordenarAtividades/{viagem}", method = { RequestMethod.GET })
    public ModelAndView reordenarAtividadesViagem(final ModelAndView model, @PathVariable final Viagem viagem) {

        int ordem = 0;
        for (final Atividade atividade : viagem.getItensOrdenados()) {
            atividade.setOrdem(ordem);
            ordem++;
            this.atividadeRepository.alterar(atividade);
        }

        for (final DiaViagem dia : viagem.getDias()) {
            ordem = 0;
            for (final Atividade atividade : dia.getAtividadesOrdenadas_old()) {
                if (atividade.isComecaNoDia(dia.getId())) {
                    atividade.setOrdem(ordem);
                    ordem++;
                    this.atividadeRepository.alterar(atividade);
                }
            }
        }
        this.viagemService.alterar(viagem);
        return null;
    }

    /**
     * TODO Remover após conclusão
     * ################
     * Metodo temporario para ajuste da ordem das atividades de uma viagem
     * ################
     */
    @RequestMapping(value = "/reordenarTodasAsViagens", method = { RequestMethod.GET })
    public ModelAndView reordenarTodasViagensDaBase(final ModelAndView model) {
        final List<Viagem> viagens = this.viagemService.consultarTodos();
        for (final Viagem viagem : viagens) {
            reordenarAtividadesViagem(model, viagem);
        }
        return null;
    }

    @RequestMapping(value = "/salvar", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody JSONResponse salvar(@ModelAttribute("viagem") final Viagem viagem,
            @RequestParam(value = "quantidadeDias", required = false) final Integer quantidadeDias,
            @RequestParam("as_values_destinos") final LocalGeografico[] destinos, final Model model) {
        String mensagem = "";
        if (viagem.getId() == null) {
            mensagem = "criada";
        } else {
            mensagem = "alterada";
        }
        this.viagemService.salvar(getUsuarioAutenticado(), viagem, quantidadeDias, destinos);
        return new JSONResponse().addParam("idViagem", viagem.getId()).addParam("message", "Viagem " + mensagem + " com sucesso").success();
    }

    @RequestMapping(value = "{idViagem}/salvarAtividade", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public @ResponseBody StatusMessage salvarAtividade(final ModelAndView model, @PathVariable final Long idViagem,
            @RequestParam(value = "itemDono", required = true) final String diaInicio,
            @RequestParam(value = "itemDonoOriginal", required = true) final String diaInicioOriginal,
            @RequestParam(value = "diaFim", required = false) final String diaFim,
            @RequestParam(value = "diaFimOriginal", required = false) final String diaFimOriginal,
            @RequestParam(value = "tipo", required = true) final TipoAtividade tipoAtividade,
            @RequestParam(value = "idItem", required = true) final Long idAtividade,
            @RequestParam(value = "local", required = false) final Long local,
            @RequestParam(value = "nomeLocal", required = false) final String nomeLocal,
            @RequestParam(value = "enderecoLocal", required = false) final String enderecoLocal,
            @RequestParam(value = "localTransporteOrigem", required = false) final Long localTransporteOrigem,
            @RequestParam(value = "nomeLocalTransporteOrigem", required = false) final String nomeLocalTransporteOrigem,
            @RequestParam(value = "enderecoLocalTransporteOrigem", required = false) final String enderecoLocalTransporteOrigem,
            @RequestParam(value = "localTransporteDestino", required = false) final Long localTransporteDestino,
            @RequestParam(value = "nomeLocalTransporteDestino", required = false) final String nomeLocalTransporteDestino,
            @RequestParam(value = "enderecoLocalTransporteDestino", required = false) final String enderecoLocalTransporteDestino,
            @RequestParam(value = "transporte", required = false) final TipoTransporte transporte,
            @RequestParam(value = "titulo", required = false) final String titulo,
            @RequestParam(value = "dataInicio", required = false) final String strDataInicio,
            @RequestParam(value = "dataFim", required = false) final String strDataFim,
            @RequestParam(value = "reservada", required = false) final boolean reservada,
            @RequestParam(value = "valor", required = false) final String valor, @RequestParam(value = "paga", required = false) final boolean paga,
            @RequestParam(value = "descricao", required = false) final String descricao,
            @RequestParam(value = "anotacoes", required = false) final String anotacoes) throws ParseException {

        final boolean eUmaInsercao = (idAtividade == null);

        final String diaInicioParaConfigurar = eUmaInsercao ? diaInicio : diaInicioOriginal;

        final Usuario usuarioAutenticado = this.getUsuarioAutenticado();
        final Viagem viagem = this.viagemService.consultarViagem(usuarioAutenticado, idViagem);

        final Atividade atividade = eUmaInsercao ? new Atividade() : this.viagemService.consultarAtividadePorId(idAtividade);

        atividade.setTipo(tipoAtividade);

        atividade.setLocal(null);
        atividade.setTransporte(null);
        atividade.setTransporteLocalOrigem(null);
        atividade.setTransporteLocalDestino(null);
        atividade.setTitulo(null);

        configurarDiaAtividade(viagem, atividade, diaInicioParaConfigurar, diaFim);

        configurarTransporteAtividade(atividade, transporte, tipoAtividade, local, nomeLocal, enderecoLocal, localTransporteOrigem,
                nomeLocalTransporteOrigem, enderecoLocalTransporteOrigem, localTransporteDestino, nomeLocalTransporteDestino,
                enderecoLocalTransporteDestino, titulo);

        configurarHorariosAtividade(atividade, strDataInicio, strDataFim);

        configurarCustosAtividade(atividade, valor, paga, reservada);

        configurarOutrasInformacoesAtividade(atividade, descricao, anotacoes);

        final StatusMessage statusMessage = new StatusMessage().addParam("idAtividade", atividade.getId());
        if (eUmaInsercao) {
            this.viagemService.incluirAtividade(usuarioAutenticado, atividade);
            if (atividade.isTerminaEmOutroDia()) {
                statusMessage.addParam("houveMovimentacao", true);
                statusMessage.addParam("diaFim", diaFim);
            }
        } else {
            this.viagemService.alterarAtividade(usuarioAutenticado, atividade);
        }

        if (!eUmaInsercao) {
            // verificar se houve movimentacao do dia inicial
            if (!diaInicioOriginal.equals(diaInicio)) {
                statusMessage.addParam("houveMovimentacao", true);
                if (diaInicio.equals("0")) {
                    this.viagemService.movimentarAtividadeParaOrganizar(this.getUsuarioAutenticado(), idAtividade);
                } else {
                    this.viagemService.movimentarAtividadeParaDia(this.getUsuarioAutenticado(), idAtividade, new Long(diaInicio),
                            diaFim != null ? new Long(diaFim) : null);
                }
            }
            // verificar se houve movimentacao do dia final
            if (diaFimOriginal != null && !diaFimOriginal.equals(diaFim)) {
                statusMessage.addParam("houveMovimentacao", true);
                statusMessage.addParam("diaFim", diaFim);
            }
        }

        return statusMessage.success("Atividade salva com sucesso");
    }

    @RequestMapping(value = "/{viagem}/ajudar/sugerirLocais", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sugerirLocais(@PathVariable final Viagem viagem, final ModelAndView model) {
        return this.sugerirLocais(viagem, null, model);
    }

    @RequestMapping(value = "/{viagem}/ajudar/{pedido}/sugerirLocais", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sugerirLocais(@PathVariable final Viagem viagem, @PathVariable final PedidoDica pedido, ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeVerViagem()) {
            model = sugestoesLugares(viagem, viagem.getPrimeiroDestino().getDestino().getUrlPath(), null, null, false, null, null, null, null, null,
                    null, null, false, true, false, null, null, null, null, model);

            if (pedido != null && pedido.getAutor().equals(getUsuarioAutenticado()) && Boolean.FALSE.equals(pedido.getRespondido())) {
                model.addObject("pedido", pedido);
            }
            model.setViewName("/viagem/ajudar/sugerirLocais");
        }
        return model;
    }

    @RequestMapping(value = "/{viagem}/sugestoesAmigos", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sugestoesAmigos(@PathVariable final Viagem viagem, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeVerViagem()) {

            final Map<Usuario, List<SugestaoLocal>> mapaSugestoesAmigos = new HashMap<Usuario, List<SugestaoLocal>>();
            final List<Long> idsUsuariosEnviaramPedidos = new ArrayList<Long>();
            final List<SugestaoLocal> sugestoesLocais = this.sugestaoLocalService.consultarSugestoesAbertas(viagem);

            // Recuperar pedidos já enviados
            final List<PedidoDica> pedidosEnviados = this.pedidoDicaService.consultarTodosPedidosPorViagem(viagem);
            model.addObject("pedidosEnviados", pedidosEnviados);
            for (final PedidoDica pedido : pedidosEnviados) {
                mapaSugestoesAmigos.put(pedido.getPraQuemPedir(), new ArrayList<SugestaoLocal>());
            }

            for (final SugestaoLocal sugestaoLocal : sugestoesLocais) {
                if (!mapaSugestoesAmigos.containsKey(sugestaoLocal.getAutor())) {
                    mapaSugestoesAmigos.put(sugestaoLocal.getAutor(), new ArrayList<SugestaoLocal>());
                }
                mapaSugestoesAmigos.get(sugestaoLocal.getAutor()).add(sugestaoLocal);
                idsUsuariosEnviaramPedidos.add(sugestaoLocal.getAutor().getId());
            }
            model.addObject("mapaSugestoesAmigos", mapaSugestoesAmigos);
            model.addObject("idsUsuariosEnviaramPedidos", idsUsuariosEnviaramPedidos);
            model.setViewName("/viagem/planejamento/sugestoesAmigos");
        }
        return model;
    }

    @RequestMapping(value = "/{viagem}/{urlPathDestino}/sugestoesLugares", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView sugestoesLugares(@PathVariable final Viagem viagem, @PathVariable final String urlPathDestino,
            @RequestParam(value = "tipoLocal", required = false) LocalType tipoLocal, @RequestParam(required = false) final Integer diaInicio,
            @RequestParam(required = false) final boolean filtro, @RequestParam(required = false) final String nome,
            @RequestParam(required = false) final Long[] tags, @RequestParam(required = false) final Integer[] idsClasses,
            @RequestParam(required = false) final Integer[] estrelas, @RequestParam(required = false) final String[] bairros,
            @RequestParam(value = "sort", required = false) final String ordem,
            @RequestParam(value = "dir", required = false) final String direcaoOrdem,
            @RequestParam(value = "wizard", required = false) final boolean wizard,
            @RequestParam(value = "recomendacao", required = false) final boolean recomendacao,
            @RequestParam(value = "reiniciar", required = false) final boolean reiniciar,
            @RequestParam(value = "locaisJaSelecionados", required = false) final List<Long> idsLocaisJaSelecionados,
            @RequestParam(value = "locaisJaSelecionadosImperdiveis", required = false) final List<Long> idsLocaisJaSelecionadosImperdiveis,
            @RequestParam(value = "columns", required = false) final Integer columns,
            @RequestParam(value = "columnSize", required = false) final Integer columnSize,

            final ModelAndView model) {

        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeAlterarItem() || recomendacao) {
            model.addObject("viagem", viagem);
            final LocalGeografico local = (LocalGeografico) this.localService.consultarPorUrlPath(urlPathDestino);
            String viewName = null;

            if (recomendacao && reiniciar) {
                viewName = "/ajudar/listaSugestoes";
            } else if (reiniciar) {
                viewName = "/planejamento/sugestoesLugares";
            } else {
                viewName = "/planejamento/listaSugestoesLugares";
            }

            if ((this.searchData.getStart() == null || this.searchData.getStart() == 0) && !filtro) {
                this.searchData.setStart(0);
                if (wizard) {
                    model.addObject("wizard", wizard);
                }
                if (recomendacao) {
                    if (reiniciar) {
                        viewName = "/ajudar/listaSugestoes";
                    } else {
                        viewName = "/ajudar/sugerirLocais";
                    }
                } else {
                    viewName = "/planejamento/sugestoesLugares";
                }
            }
            this.searchData.setLimit(10);

            if (tipoLocal == null) {
                tipoLocal = LocalType.ATRACAO;
            }

            final List<Integer> tipos = new ArrayList<Integer>();
            tipos.add(tipoLocal.getCodigo());
            if (tipoLocal.equals(LocalType.ATRACAO)) {
                tipos.add(LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo());
            }

            final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery();
            params.setTerm(nome);

            params.setTypes(tipos);
            params.addExtraParam(EntidadeIndexada.FIELD_ID_CIDADE, local.getId());

            if (filtro) {
                if (tags != null && tags.length > 0) {
                    params.addExtraParam(EntidadeIndexada.FIELD_IDS_TAGS, tags);
                    model.addObject("tags", StringUtils.join(tags, ","));
                    model.addObject("tagsList", Arrays.asList(tags));
                }
                if (estrelas != null && estrelas.length > 0) {
                    params.addExtraParam(EntidadeIndexada.FIELD_ESTRELAS, estrelas);
                    model.addObject("estrelas", estrelas != null ? StringUtils.join(estrelas, ",") : null);
                    model.addObject("estrelasList", Arrays.asList(estrelas));
                }
                if (bairros != null && bairros.length > 0) {
                    params.addExtraParam(EntidadeIndexada.FIELD_BAIRRO, bairros);
                    model.addObject("bairros", bairros != null ? StringUtils.join(bairros, ",") : null);
                    model.addObject("bairrosList", Arrays.asList(bairros));
                }

            } else {
                params.setGroup(true);
            }
            if (ordem == null) {
                params.setSortByRanking(true);
            }
            SortDirection direction = SortDirection.DESC;
            if (direcaoOrdem != null) {
                try {
                    direction = SortDirection.valueOf(direcaoOrdem.toUpperCase());
                } catch (final Exception e) {
                    // continua
                }
            }

            if (!this.getSearchData().isPaginationDataValid()) {
                this.getSearchData().setStart(0);
                this.getSearchData().setLimit(20);
            }
            final SolrResponseTripFans respostaSolr = this.pesquisaTextualService.consultarEntidades(params);
            final List<EntidadeIndexada> resultados = respostaSolr.getResultados();
            final List<org.apache.solr.client.solrj.response.FacetField> camposAgrupados = respostaSolr.getCamposAgrupados();

            model.addObject("sugestoesLocais", resultados);
            model.addObject("camposAgrupados", camposAgrupados);

            model.addObject("filtro", filtro);
            model.addObject("tipoLocal", tipoLocal);
            model.addObject("diaInicio", diaInicio);
            model.addObject("recomendacao", recomendacao);
            model.addObject("reiniciar", reiniciar);

            model.addObject("columns", columns);
            model.addObject("columnSize", columnSize);

            if (diaInicio != null && diaInicio > 0) {
                model.addObject("idDiaInicio", viagem.getDia(diaInicio).getId());
            }
            model.addObject("destino", local);
            model.addObject("proximasSugestoes", this.searchData.getStart() + 10);

            model.addObject("locaisJaSelecionados", idsLocaisJaSelecionados);
            model.addObject("locaisJaSelecionadosImperdiveis", idsLocaisJaSelecionadosImperdiveis);

            model.setViewName("/viagem/" + viewName);
        } else {
            model.setViewName("/erros/403");
        }
        return model;
    }

    private void takeSnapshot(final String url) throws MalformedURLException {

        /*final File pathToBinary = new File("/usr/firefox/firefox");
        final FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
        final FirefoxProfile firefoxProfile = new FirefoxProfile();*/

        // Inicializar WebDriver
        // final WebDriver driver = new ChromeDriver();

        // System.setProperty("webdriver.chrome.driver", "/usr/bin/google-chrome");

        // System.setProperty("webdriver.chrome.driver", "/home/carlosn/chromedriver/chromedriver");
        // final WebDriver driver = new ChromeDriver();(

        // final WebDriver driver = new RemoteWebDriver(new URL("http://localhost:9515"), DesiredCapabilities.chrome());

        /*final WebDriver driver = new FirefoxDriver(ffBinary, firefoxProfile);

        // Maximizar janela
        driver.manage().window().maximize();
        // Aguardar a pagina carregar
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // Ir para a pagina
        driver.get(url);

        final JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        jsExecutor.executeScript("window.scrollTo(0, 0)");
        final Boolean check = (Boolean) jsExecutor
                .executeScript("return document.documentElement.scrollHeight > document.documentElement.clientHeight");
        Long scrollH = (Long) jsExecutor.executeScript("return document.documentElement.scrollHeight");
        final Long clientH = (Long) jsExecutor.executeScript("return document.documentElement.clientHeight");
        int index = 1;
        if (check) {
            while (scrollH.intValue() > 0) {
                final File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
                try {
                    FileUtils.copyFile(file, new File("/home/carlosn/Downloads/snap" + index + ".png"), true);
                } catch (final IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                jsExecutor.executeScript("window.scrollTo(0, " + clientH * index + ")");
                scrollH = scrollH - clientH;
                try {
                    Thread.sleep(2000);
                } catch (final Exception e) {
                    e.printStackTrace();
                }
                index++;
            }
        }

        // byte[] data = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        // final File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        // Fechar WebDriver
        driver.close();

        // return
         */
    }

    @RequestMapping(value = "/minhasProximasViagens", method = { RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public ModelAndView viagemListarMinhasProximasViagensJSON(final ModelAndView model) {
        model.addObject("viagens", this.viagemService.viagemConsultarProximasViagens(this.getUsuarioAutenticado()));
        model.setViewName("/viagem/viagemListarJSON");
        return model;
    }

    @RequestMapping(value = "{viagem}/planejamento/visaoGeral", method = { RequestMethod.GET, RequestMethod.POST })
    public ModelAndView visaoGeral(final ModelAndView model, @PathVariable() final Viagem viagem) {
        model.addObject("atividade", new Atividade());
        model.addObject("mostrarAcoes", true);
        model.addObject("mostrarAcoesMover", true);

        if (viagem.getQuantidadeDias() != null && viagem.getQuantidadeDias() > 0) {
            recarregarDia(model, viagem.getId(), viagem.getDia(1).getId());
        }

        return this.exibirPagina(viagem.getId(), "visaoGeral", model);
    }

    @RequestMapping(value = "/{viagem}/mapa", method = { RequestMethod.GET })
    // @PreAuthorize("isAuthenticated()")
    public ModelAndView visualizarMapaCompleto(@PathVariable final Viagem viagem, final ModelAndView model) {
        final ViagemPermissao permissao = this.viagemService.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeVerViagem()) {
            model.addObject("viagem", viagem);
            model.setViewName("/viagem/planejamento/mapa");
        }
        return model;
    }

}