package br.com.fanaticosporviagens.viagem.model.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.foto.AlbumService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.DestinoViagem;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.Hospedagem;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.viagem.controller.DestinoViagemListWrapper;
import br.com.fanaticosporviagens.viagem.model.repository.AtividadePlanoRepository;

/**
 * 
 * @author Pedro Sebba(sebbaweb@gmail.com)
 * 
 */

@Service
public class PlanoViagemService extends GenericCRUDService<PlanoViagem, Long> {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private AtividadePlanoRepository atividadeRepository;

    @Autowired
    private AtividadeService atividadeService;

    @Autowired
    private FotoService fotoService;

    @Override
    @Transactional
    @PreAuthorize("#viagem.criador.username == authentication.name")
    public void alterar(final PlanoViagem planoViagem) {
        super.alterar(planoViagem);
    }

    @PostAuthorize("returnObject.criador.username == authentication.name")
    public PlanoViagem consultarViagemUsuarioCriador(final Long idViagem) {
        return this.consultarPorId(idViagem);
    }

    public List<PlanoViagem> consultarViagensUsuario(final Long idUsuario) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idUsuario", idUsuario);
        if (getUsuarioAutenticado() != null && idUsuario == getUsuarioAutenticado().getId()) {
            return this.searchRepository.consultarPorNamedQuery("PlanoViagem.consultarViagensUsuarioAtual", parametros, PlanoViagem.class);
        }
        return this.searchRepository.consultarPorNamedQuery("PlanoViagem.consultarViagensUsuario", parametros, PlanoViagem.class);
    }

    @Override
    @Transactional
    @PreAuthorize("#viagem.criador.username == authentication.name")
    public void excluir(final PlanoViagem planoViagem) {
        super.excluir(planoViagem);
    }

    @Transactional
    public void incluirAtividade(final AtividadePlano atividadePlano) {
        this.atividadeRepository.incluir(atividadePlano);
    }

    @Transactional
    public void incluirDestino(final Cidade cidade, final PlanoViagem planoViagem) {
        final DestinoViagem destinoViagem = new DestinoViagem();
        destinoViagem.setDestino(cidade);
        // destinoViagem.setViagem(viagem);
        int ordem = 0;
        if (planoViagem.getDestinosViagem() != null) {
            for (final DestinoViagem destino : planoViagem.getDestinosViagem()) {
                if (ordem < destino.getOrdem()) {
                    ordem = destino.getOrdem();
                }
            }
        }
        destinoViagem.setOrdem(++ordem);
        planoViagem.getDestinosViagem().add(destinoViagem);
        alterar(planoViagem);
    }

    @Transactional
    public void incluirHospedagem(final Hospedagem hospedagem) {
        this.atividadeRepository.incluir(hospedagem);
    }

    @Override
    public void preAlteracao(final PlanoViagem planoViagem) {
        configurarViagem(planoViagem);
        planoViagem.setDataUltimaAlteracao(new LocalDateTime());
    }

    @Override
    public void preInclusao(final PlanoViagem planoViagem) {
        planoViagem.setCriador(getUsuarioAutenticado());
        configurarViagem(planoViagem);
        planoViagem.setDataUltimaAlteracao(new LocalDateTime());
    }

    @Transactional
    // TODO Tem de criar um preAuthorize que leve em consideração as pessoas que tem direito sobre a viagem. Tipo os participantes.
    @PreAuthorize("#viagem.criador.username == authentication.name")
    public void salvar(final PlanoViagem planoViagem, final DestinoViagemListWrapper destinosWrapper) {
        salvar(configurarDestinosViagem(planoViagem, destinosWrapper));
    }

    @Transactional
    public void salvarFotoIlustrativaViagem(final PlanoViagem planoViagem, final byte[] arquivo, final String nomeArquivo) throws IOException {
        Foto foto = planoViagem.getFotoIlustrativa();
        if (foto == null) {
            foto = new Foto();
        }
        foto.setArquivoFoto(arquivo, nomeArquivo);
        // Se a foto for anonima, forçar a geração de um nome
        foto.setViagem(planoViagem);
        foto.setTipoArmazenamento(TipoArmazenamentoFoto.LOCAL);
        this.fotoService.salvarFotoIlustrativaDeViagem(foto);
        planoViagem.setFotoIlustrativa(foto);
        alterar(planoViagem);
    }

    @Transactional
    public void salvarFotoIlustrativaViagem(final PlanoViagem planoViagem, final InputStream arquivo, final String nomeArquivo) throws IOException {
        salvarFotoIlustrativaViagem(planoViagem, IOUtils.toByteArray(arquivo), nomeArquivo);
    }

    @Transactional
    public void salvarHospedagem(final Hospedagem hospedagem) {
        this.atividadeService.salvar(hospedagem);
    }

    private PlanoViagem configurarDestinosViagem(final PlanoViagem planoViagem, final DestinoViagemListWrapper destinosWrapper) {
        /*final List<DestinoViagem> destinosViagemRecebidos = destinosWrapper.getList();

        for (final DestinoViagem destinoViagem : destinosViagemRecebidos) {
            viagem.addDestino(destinoViagem);
        }*/

        // TODO: Galera, o código acima, substitui esse abaixo com vantagens e desvantagens.
        // A vantagem é que não deleta tudo e reinsere toda vez. A desvantagem é que ele é mais longo
        // Se acharem que está uma porcaria é só descomentar essa porção abaixo e excluir essa de cima!
        // Abraço para as crianças.
        planoViagem.getDestinosViagem().clear();
        for (final DestinoViagem destinoViagemRecebido : destinosWrapper.getList()) {
            planoViagem.getDestinosViagem().add(destinoViagemRecebido);
        }

        return planoViagem;
    }

    private void configurarViagem(final PlanoViagem planoViagem) {
        if (planoViagem.getDestinosViagem() != null) {
            for (final DestinoViagem destino : planoViagem.getDestinosViagem()) {
                if (destino != null) {
                    // destino.setViagem(viagem);
                }
            }
        }
    }
}
