package br.com.fanaticosporviagens.viagem.model.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;

/**
 * @author Pedro Sebba (sebbaweb@gmail.com)
 * 
 */
@Repository
public class PlanoViagemRepository extends GenericCRUDRepository<PlanoViagem, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<PlanoViagem> consultarViagensUsuario(final Long idUsuario) {
        final StringBuilder select = new StringBuilder();

        select.append("SELECT v.id, ");
        select.append("       v.nome, ");
        select.append("       v.\"dataInicio\", ");
        select.append("       v.\"dataFim\" ");

        select.append("  FROM viagem v ");
        select.append(" WHERE v.criador = :idUsuario");
        final Map<String, Object> parametros = new HashMap<String, Object>();

        parametros.put("idUsuario", idUsuario);

        return this.searchRepository.consultaSQL(select.toString(), parametros, PlanoViagem.class);

    }
}
