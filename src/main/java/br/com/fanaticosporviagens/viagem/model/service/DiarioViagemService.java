package br.com.fanaticosporviagens.viagem.model.service;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.avaliacao.AvaliacaoService;
import br.com.fanaticosporviagens.dica.model.service.DicaService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.DiarioViagem;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.RelatoViagem;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;

@Service
public class DiarioViagemService extends GenericCRUDService<DiarioViagem, Long> {

    @Autowired
    private AvaliacaoService avaliacaoService;

    @Autowired
    private DicaService dicaService;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private PlanoViagemService planoViagemService;

    @PostAuthorize("returnObject.viagem.criador.username == authentication.name")
    @Transactional
    public DiarioViagem criarDiarioViagem(final PlanoViagem planoViagem) {
        final DiarioViagem diario = new DiarioViagem();
        diario.setDataCriacao(new LocalDateTime());
        diario.setViagem(planoViagem);
        diario.setFormatoExibicao(DiarioViagem.FormatoExibicao.DIA_A_DIA);

        RelatoViagem itemDiario = null;
        for (final AtividadePlano atividadePlano : planoViagem.getAtividades()) {
            if (atividadePlano != null) {
                itemDiario = new RelatoViagem();
                itemDiario.setAtividade(atividadePlano);
                itemDiario.setNumeroCapitulo(atividadePlano.getDiaInicio());
                itemDiario.setImportadoDoPlano(true);
                diario.addItem(itemDiario);
            }
        }

        planoViagem.setDiario(diario);
        planoViagem.setFinalizada(true);
        planoViagem.setPossuiDiario(true);
        this.planoViagemService.salvar(planoViagem);

        return diario;
    }

    @PostAuthorize("returnObject.viagem.criador.username == authentication.name")
    // @PreAuthorize("diario.viagem.criador.username == authentication.name")
    @Transactional
    public DiarioViagem publicar(final DiarioViagem diario) {
        if (!diario.isPublicado()) {
            this.publicarConteudoDiario(diario);
            diario.publicar();
            this.alterar(diario);
        }
        return diario;
    }

    @Transactional
    public Foto salvarFotoCapaDiarioViagem(final DiarioViagem diario, final InputStream arquivo, final String nomeArquivo) throws IOException {
        Foto foto = diario.getFotoCapa();
        if (foto == null) {
            foto = new Foto();
            foto.setAutor(getUsuarioAutenticado());
            foto.setViagem(diario.getViagem());
            foto.setTipoArmazenamento(TipoArmazenamentoFoto.LOCAL);
        }
        foto.setArquivoFoto(IOUtils.toByteArray(arquivo), nomeArquivo);
        this.fotoService.salvarFotoCapaDiarioViagem(foto);
        this.fotoService.salvar(foto);
        diario.setFotoCapa(foto);
        alterar(diario);
        return foto;
    }

    private void publicarConteudoAtividadeDiario(final AtividadePlano atividadePlano) {
        if (atividadePlano.getPossuiAvaliacao()) {
            this.avaliacaoService.publicar(atividadePlano.getAvaliacao());
        }

        if (atividadePlano.getPossuiDicas()) {
            this.dicaService.publicar(atividadePlano.getDicas());
        }
        if (atividadePlano.getPossuiFotos()) {
            this.fotoService.publicar(atividadePlano.getFotos());
        }
    }

    /**
     * Relaliza a publicação dos elementos (relatos, fotos, avaliações e dicas) contidos no diário
     */
    private void publicarConteudoDiario(final DiarioViagem diario) {
        /*if (diario.getViagem().getDiasViagem() != null) {
            for (final DiaViagem diaViagem : diario.getViagem().getDiasViagem()) {
                // if (diaViagem.get)
            }
        }*/
        if (diario.getViagem().getAtividades() != null) {
            for (final AtividadePlano atividadePlano : diario.getViagem().getAtividades()) {
                if (atividadePlano != null) {
                    publicarConteudoAtividadeDiario(atividadePlano);
                }
            }
        }
    }
}
