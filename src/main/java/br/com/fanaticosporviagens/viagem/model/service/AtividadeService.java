package br.com.fanaticosporviagens.viagem.model.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Foto;
import br.com.fanaticosporviagens.model.entity.ParticipacaoAtividade;
import br.com.fanaticosporviagens.model.entity.RelatoViagem;
import br.com.fanaticosporviagens.model.entity.TipoArmazenamentoFoto;
import br.com.fanaticosporviagens.model.entity.Transporte;
import br.com.fanaticosporviagens.model.entity.TrechoTransporte;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.viagem.view.AtividadesViagemView;

@Service
public class AtividadeService extends GenericCRUDService<AtividadePlano, Long> {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    @Autowired
    private PlanoViagemService planoViagemService;

    public List<AtividadePlano> consultarAtividadesViagem(final Long idViagem) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", idViagem);
        return this.searchRepository.consultarPorNamedQuery("AtividadePlano.consultarAtividadesViagem", parametros, AtividadePlano.class);
    }

    public AtividadesViagemView consultarAtividadesViagemAgrupadas(final Long idViagem) throws InstantiationException, IllegalAccessException {
        final PlanoViagem planoViagem = this.planoViagemService.consultarPorId(idViagem);
        final List<AtividadePlano> atividadePlanos = this.consultarAtividadesViagem(idViagem);
        final AtividadesViagemView view = new AtividadesViagemView(planoViagem);
        for (final AtividadePlano atividadePlano : atividadePlanos) {
            view.adicionarAtividadeNoGrupo(atividadePlano);
        }
        return view;
    }

    @Transactional
    public void excluirRelatoAtividade(final AtividadePlano atividadePlano) {
        final RelatoViagem relato = atividadePlano.getRelato();
        atividadePlano.setRelato(null);
        this.alterar(atividadePlano);
        this.entityManager.remove(relato);
    }

    /**
     * Importa uma foto para uma atividade.
     * 
     * @param usuario
     */
    @Transactional
    public void importarFoto(final AtividadePlano atividadePlano, final Foto foto) {
        atividadePlano.addFoto(foto);
        this.salvar(atividadePlano);
    }

    @Transactional
    public void importarFotos(final AtividadePlano atividadePlano, final List<Foto> fotos) {
        for (final Foto foto : fotos) {
            importarFoto(atividadePlano, foto);
        }
    }

    @Transactional
    public void importarFotosFacebook(final AtividadePlano atividadePlano, final List<String> idsFotosFacebook) {
        for (final String idFotoFacebook : idsFotosFacebook) {

        }
    }

    @Override
    public void preAlteracao(final AtividadePlano atividadePlano) {
        // configurarAtividade(atividade);
    }

    @Override
    public void preInclusao(final AtividadePlano atividadePlano) {
        this.urlGeneratorService.generate(atividadePlano);
        configurarAtividade(atividadePlano);
    }

    /**
     * Inclui/Altera a foto de uma atividade.
     * 
     * @param usuario
     */
    @Transactional
    public Foto salvarFoto(final AtividadePlano atividadePlano, final InputStream arquivo, final String nomeArquivo, final String descricaoFoto)
            throws IOException {
        final Foto foto = new Foto();
        foto.setAutor(getUsuarioAutenticado());
        foto.setArquivoFoto(IOUtils.toByteArray(arquivo), nomeArquivo);
        foto.setAtividade(atividadePlano);
        foto.setAnonima(false);
        // foto.setLocal(atividade.getLocal());
        foto.setTipoArmazenamento(TipoArmazenamentoFoto.LOCAL);
        this.urlGeneratorService.generate(foto);
        this.fotoService.incluirFoto(foto, false, false);
        incluirFotoAtividade(foto, atividadePlano);
        return foto;
    }

    private void configurarAtividade(final AtividadePlano atividadePlano) {
        if (atividadePlano instanceof Transporte<?>) {
            configurarTransporte((Transporte<?>) atividadePlano);
        }
        configurarParticipantes(atividadePlano);
    }

    private void configurarParticipantes(final AtividadePlano atividadePlano) {
        if (atividadePlano.getParticipantes() != null) {
            for (final ParticipacaoAtividade participacao : atividadePlano.getParticipantes()) {
                participacao.setAtividade(atividadePlano);
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private void configurarTransporte(final Transporte transporte) {
        final List<TrechoTransporte> trechos = transporte.getTrechos();

        if (transporte != null && transporte.getTrechos() != null) {
            // Varre todos os trechos
            for (final TrechoTransporte<?> trecho : trechos) {
                trecho.setTransporte(transporte);
            }
        }

        final TrechoTransporte trechoInicial = transporte.getTrechoInicial();
        final TrechoTransporte trechoFinal = transporte.getTrechoFinal();

        // Alterar as datas/horas de inicio do transporte
        if (trechoInicial != null) {
            transporte.setDataInicio(transporte.getTrechoInicial().getPartida().getData());
            transporte.setHoraInicio(trechoInicial.getPartida().getHora());

        }
        // Alterar as datas/horas de fim do transporte
        if (trechoFinal != null) {
            transporte.setDataFim(trechoInicial.getChegada().getData());
            transporte.setHoraFim(trechoInicial.getChegada().getHora());

        }
    }

    private void incluirFotoAtividade(final Foto foto, final AtividadePlano atividadePlano) {
        final Query query = this.entityManager.createNativeQuery("INSERT INTO foto_atividade(id_foto, id_atividade) VALUES(?, ?)");
        query.setParameter(1, foto.getId());
        query.setParameter(2, atividadePlano.getId());
        query.executeUpdate();
    }

}