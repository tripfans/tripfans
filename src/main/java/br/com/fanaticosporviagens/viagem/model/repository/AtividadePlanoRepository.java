package br.com.fanaticosporviagens.viagem.model.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;

/**
 * @author Pedro Sebba (sebbaweb@gmail.com)
 * 
 */
@Repository
public class AtividadePlanoRepository extends GenericCRUDRepository<AtividadePlano, Long> {

    public List<AtividadePlano> consultarAtividadesViagem(final Long idViagem) {

        final StringBuilder select = new StringBuilder();

        select.append("SELECT a ");

        select.append("  FROM AtividadePlano a ");
        select.append(" WHERE a.viagem.id = :idViagem ");
        select.append(" ORDER BY a.dataInicio, a.ordem, a.dia ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idViagem", idViagem);

        return this.searchRepository.consultaHQL(select.toString(), parametros);

    }
}
