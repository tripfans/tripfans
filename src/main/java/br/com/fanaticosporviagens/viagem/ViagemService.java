package br.com.fanaticosporviagens.viagem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.amizade.AmizadeService;
import br.com.fanaticosporviagens.convite.ConviteService;
import br.com.fanaticosporviagens.dica.model.service.PedidoDicaService;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.exception.SystemException;
import br.com.fanaticosporviagens.infra.model.service.BaseEmailService;
import br.com.fanaticosporviagens.infra.model.service.EmailTemplate;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.security.SecurityService;
import br.com.fanaticosporviagens.infra.util.CriptografiaUtil;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.fanaticosporviagens.locais.SugestaoLocalService;
import br.com.fanaticosporviagens.model.entity.Amizade;
import br.com.fanaticosporviagens.model.entity.Atividade;
import br.com.fanaticosporviagens.model.entity.Convite;
import br.com.fanaticosporviagens.model.entity.DestinoViagem;
import br.com.fanaticosporviagens.model.entity.DiaViagem;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.LocalGeografico;
import br.com.fanaticosporviagens.model.entity.LocalidadeUtils;
import br.com.fanaticosporviagens.model.entity.LocalidadeUtils.DistanciaAtividadeLocalComparator;
import br.com.fanaticosporviagens.model.entity.ParticipanteViagem;
import br.com.fanaticosporviagens.model.entity.PedidoDica;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.SugestaoLocal;
import br.com.fanaticosporviagens.model.entity.TipoAtividade;
import br.com.fanaticosporviagens.model.entity.TipoVisibilidade;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;
import br.com.fanaticosporviagens.usuario.model.service.UsuarioService;
import br.com.fanaticosporviagens.util.TripFansEnviroment;
import br.com.fanaticosporviagens.viagem.controller.DestinoViagemListWrapper;

@Service
public class ViagemService extends GenericCRUDService<Viagem, Long> {

    @Autowired
    private AmizadeService amizadeService;

    @Autowired
    private AtividadeRepository atividadeRepository;

    @Autowired
    private ConviteService conviteService;

    @Autowired
    private ViagemDiaRepository diaRepository;

    @Autowired
    private BaseEmailService emailService;

    @Autowired
    private LocalService localService;

    @Autowired
    private PedidoDicaService pedidoDicaService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private SugestaoLocalService sugestaoLocalService;

    @Autowired
    private TripFansEnviroment tripFansEnviroment;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private ViagemRepository viagemRepository;

    @Transactional
    public void aceitarConviteParticipacaoViagem(final Convite convite, final ParticipanteViagem participanteViagem) {
        if (convite != null && convite.isParticipacaoViagem() && !convite.isAceito() && !convite.isIgnorado()) {
            if (participanteViagem.getUsuario() != null && convite.getDestinatario() != null) {
                if (!participanteViagem.getUsuario().equals(convite.getDestinatario())) {
                    throw new BusinessException("Erro ao aceitar o convite para Viagem: Usuário convidado não corresponde ao participante da Viagem");
                }
            } else if (participanteViagem.getEmail() != null && convite.getEmailConvidado() != null) {
                if (!participanteViagem.getEmail().equalsIgnoreCase(convite.getEmailConvidado())) {
                    throw new BusinessException(
                            "Erro ao aceitar o convite para Viagem: Email do convidado não corresponde ao email do participante da Viagem");
                }
            }
            this.conviteService.aceitar(convite);
            participanteViagem.setConfirmado(true);
            this.viagemRepository.alterarParticipante(participanteViagem);
        } else {
            throw new BusinessException("Erro ao aceitar o convite para Viagem: O Convite não é válido!");
        }
    }

    @Transactional
    public void adicionarDiaViagem(final Usuario usuarioAutenticado, final Long idViagem) {
        final Viagem viagem = consultarViagem(usuarioAutenticado, idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeAdicionarDia()) {
            Integer qtdDiasViagem = viagem.getQuantidadeDias();
            if (qtdDiasViagem == null) {
                qtdDiasViagem = 0;
            }
            final DiaViagem dia = new DiaViagem(viagem, qtdDiasViagem + 1);
            dia.setId(this.diaRepository.getId());

            if (viagem.getDias() == null) {
                viagem.setDias(new ArrayList<DiaViagem>());
            }

            viagem.getDias().add(dia);

            this.diaRepository.inserir(dia);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para incluir dia(s) nesta viagem");
        }
    }

    @Transactional
    public void adicionarParticipante(final Long idViagem, final ParticipanteViagem participante) {
        final Viagem viagem = consultarViagem(getUsuarioAutenticado(), idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
            participante.setViagem(viagem);
            if (participante.getFaixaEtaria() == null) {
                participante.setFaixaEtaria(1);
            }
            if (participante.getNome().indexOf("@") != -1) {
                participante.setEmail(participante.getNome());
                participante.setNome(participante.getNome().substring(0, participante.getNome().indexOf("@")));
            }

            this.viagemRepository.incluirParticipante(participante);
            // Se viaja e é adulto
            if (Boolean.TRUE.equals(participante.getViaja()) && participante.getFaixaEtaria() == 1) {
                // enviar convite
                this.conviteService.convidarParaViagem(viagem, participante, "Mensagem para convidar os amigos");
            }
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    @Transactional
    public void alterarAtividade(final Usuario usuarioAutenticado, final Atividade atividade) {
        Viagem viagem = atividade.getViagem();
        if (viagem == null) {
            viagem = atividade.getDia().getViagem();
        }
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeAlterarItem()) {
            final DiaViagem diaInicio = atividade.getDia();
            final DiaViagem diaFim = atividade.getDiaFim();
            if (diaInicio != null && diaFim != null) {
                // se o dia final for antes do dia inicio
                if (diaFim.getNumero() < diaInicio.getNumero()) {
                    // o dia fim passa a ser o mesmo dia inicial
                    atividade.setDiaFim(diaInicio);
                }
            }
            this.atividadeRepository.alterar(atividade);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar atividades");
        }
    }

    @Transactional
    public void alterarViagem(final Usuario usuarioAutenticado, final Viagem viagem) {
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
            this.viagemRepository.alterar(viagem);

        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    /**
     * Metodo para excluir um local incluido pela tela de sugestões de uma viagem
     *
     * @param idViagem
     * @param idLocal
     * @return
     */
    public Atividade consultarAtividadeAOrganizarPorIdLocal(final Long idViagem, final Long idLocal) {
        return this.atividadeRepository.consultarAOrganizarPorIdLocal(idViagem, idLocal);
    }

    public Atividade consultarAtividadePorId(final Long idAtividade) {
        return this.atividadeRepository.consultarPorId(idAtividade);
    }

    @Transactional
    public List<ParticipanteViagem> consultarCompartilhamentos(final Long idViagem) {
        final Viagem viagem = consultarViagem(getUsuarioAutenticado(), idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeVerViagem()) {
            return this.viagemRepository.consultarCompartilhamentos(idViagem);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para ver esta viagem");
        }
    }

    public List<RoteiroViagemView> consultarRoteirosDestaques(final Usuario usuarioAtual, final Long idDestino) {
        return this.viagemRepository.consultarRoteirosDestaques(usuarioAtual, idDestino);
    }

    public List<Viagem> consultarTodasViagensAmigosPodemVer(final Usuario criador) {
        return this.viagemRepository.consultarTodasViagensAmigosPodemVer(criador);
    }

    public List<Viagem> consultarTodasViagensUsuario(final Usuario criador) {
        return this.viagemRepository.consultarTodasViagensUsuario(criador);
    }

    public Viagem consultarViagem(final Usuario usuarioAutenticado, final Long idViagem) {
        Viagem viagem = null;
        try {
            viagem = this.viagemRepository.consultarPorId(idViagem);
        } catch (final NotFoundException e) {
            throw new BusinessException("A viagem informada não existe.");
        }

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeVerViagem()) {
            return viagem;
        } else {
            throw new PermissaoViagemException("Você não possui permissão para ver esta viagem", viagem.getAutor());
        }
    }

    public List<Viagem> consultarViagensPublicas(final Usuario criador) {
        return this.viagemRepository.consultarViagensPublicas(criador);
    }

    @Transactional
    public List<ParticipanteViagem> consultarViajantes(final Long idViagem) {
        final Viagem viagem = consultarViagem(getUsuarioAutenticado(), idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeVerViagem()) {
            return this.viagemRepository.consultarViajantes(idViagem);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para ver esta viagem");
        }
    }

    @Transactional
    public Viagem copiarViagem(final Usuario usuarioAutenticado, final Long idViagem) {
        final Viagem viagem = consultarViagem(usuarioAutenticado, idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeCopiarViagem()) {

            final Viagem viagemCopia = viagem.getCopia(usuarioAutenticado);

            // viagemCopia.setId(this.viagemRepository.getId());

            for (final Atividade item : viagemCopia.getItensOrdenados()) {
                item.setId(this.atividadeRepository.getId());
            }

            for (final DiaViagem dia : viagemCopia.getDias()) {
                dia.setId(this.diaRepository.getId());

                for (final Atividade item : dia.getItensOrdenados()) {
                    item.setId(this.atividadeRepository.getId());
                }
            }

            viagemCopia.setDataInclusao(new Date());
            this.viagemRepository.inserir(viagemCopia);

            for (final Atividade item : viagemCopia.getItensOrdenados()) {
                this.atividadeRepository.inserir(item);
            }

            for (final DiaViagem dia : viagemCopia.getDias()) {
                this.diaRepository.inserir(dia);

                for (final Atividade item : dia.getItensOrdenados()) {
                    this.atividadeRepository.inserir(item);
                }
            }

            for (final Atividade item : viagemCopia.getItensOrdenados()) {
                this.atividadeRepository.alterarAnteriorProximo(item, item.getItemAnterior(), item.getItemProximo());
            }
            for (final DiaViagem dia : viagemCopia.getDias()) {
                for (final Atividade item : dia.getItensOrdenados()) {
                    this.atividadeRepository.alterarAnteriorProximo(item, item.getItemAnterior(), item.getItemProximo());
                }
            }

            return viagemCopia;
        }
        return null;
    }

    @Transactional
    public void enviarRoteiroPorEmail(final Usuario destinatario, final Long idViagem, final BaseEmailService.AnexoEmail anexo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();

        final Viagem viagem = this.consultarPorId(idViagem);
        // Recuperar novamente para evitar Lazy
        final Usuario usuarioDestinatario = this.usuarioService.consultarPorId(destinatario.getId());
        // final ViagemPermissao permissoes = viagemObterPermissoes(usuarioDestinatario, viagem);

        // if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
        final String assunto = "Seu Roteiro de Viagem: \"" + viagem.getTitulo() + "\"";
        String mensagem = "Muito obrigado por utilizar a nossa plataforma para criar seu Roteiro de Viagem <strong> \"" + viagem.getTitulo()
                + "\"</strong>.";
        mensagem += "<br/>Segue o seu Roteiro em PDF. Tenha uma ótima Viagem!";

        mensagem += "<br/>";
        mensagem += "<br/>";
        mensagem += "Atencionsamente,";
        mensagem += "<p>";
        mensagem += "<strong>Equipe TripFans</strong>";
        mensagem += "</p>";
        try {
            parametros.put("nomeUsuarioConvidante", StringEscapeUtils.escapeHtml(usuarioDestinatario.getDisplayName()));
            parametros.put("idUsuarioConvidante", URLEncoder.encode(CriptografiaUtil.encrypt(usuarioDestinatario.getId() + ""), "UTF-8"));

            parametros.put("textoAcao", mensagem);
            parametros.put("hideFooter", true);
            parametros.put("textoLink", "");

            // parametros.put("autor", destinatario);
            parametros.put("destinatario", usuarioDestinatario);

            parametros.put("url", "");

            this.emailService.enviarEmail(usuarioDestinatario.getEmail(), assunto, EmailTemplate.EMAIL_NOTIFICACAO, parametros, anexo);

        } catch (final Exception e) {
            throw new SystemException("Erro ao enviar email: " + e.getMessage());
        }
        /*} else {
            throw new PermissaoViagemException("Você não possui permissão para acessar esta viagem");
        }*/
    }

    // @Async
    public void enviarRoteiroPorEmail(final Viagem viagem, final Usuario usuario) throws IOException {

        final URL url = new URL(this.tripFansEnviroment.getServerUrl() + "/viagem/" + viagem.getId() + "/imprimirInterno/" + usuario.getId());

        final String nomeArquivo = viagem.getTitulo() + "_" + viagem.getId() + ".pdf";
        String nomeArquivoFormatado = nomeArquivo;

        try {

            final Path currentRelativePath = Paths.get("");
            String dir = currentRelativePath.toAbsolutePath().toString();
            dir = dir + "/";

            final String OS = System.getProperty("os.name").toLowerCase();

            if ((OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0)) {
                nomeArquivoFormatado = nomeArquivo.replace(" ", "\\ ");
            }
            final String caminhoCompletoArquivo = dir + nomeArquivoFormatado;

            final String[] command = { "wkhtmltopdf", url.toString(), nomeArquivoFormatado };
            final ProcessBuilder processBuilder = new ProcessBuilder(command);

            // definindo o working directory
            processBuilder.directory(new File(dir));

            final Process process = processBuilder.start();

            // Read out dir output
            final InputStream is = process.getInputStream();
            final InputStreamReader isr = new InputStreamReader(is);
            final BufferedReader br = new BufferedReader(isr);
            String line;
            // System.out.printf("Output of running %s is:\n", Arrays.toString(command));
            while ((line = br.readLine()) != null) {
                // System.out.println(line);
            }

            // Wait to get exit value
            try {
                final int exitValue = process.waitFor();

                FileInputStream arquivo = null;

                // Aguardar pela geração do arquivo
                for (int i = 0; i < 30; i++) {
                    try {
                        arquivo = new FileInputStream(new File(caminhoCompletoArquivo));
                        break;
                    } catch (final Exception e) {
                        // Aguarda 20 segundos
                        Thread.sleep(20000);
                    }
                }

                if (arquivo != null) {
                    final BaseEmailService.AnexoEmail anexo = new BaseEmailService.AnexoEmail(IOUtils.toByteArray(arquivo), viagem.getNome(), "pdf",
                            "application/pdf");

                    this.enviarRoteiroPorEmail(usuario, viagem.getId(), anexo);
                }

                // System.out.println("\n\nExit Value is " + exitValue);
            } catch (final InterruptedException e) {
                // e.printStackTrace();
                throw new SystemException("Erro ao enviar Roteiro de Viagem por email");
            }

            // caminhoCompletoArquivo = caminhoCompletoArquivo.replace("\\", "");

        } catch (final Exception e) {
            // e.printStackTrace();
            throw new SystemException("Erro ao enviar Roteiro de Viagem por email");
        }

    }

    @Transactional
    public void excluirAtividade(final Usuario usuarioAutenticado, final Long idAtividade) {
        Atividade atividade = null;
        DiaViagem dia = null;

        try {
            atividade = consultarAtividadePorId(idAtividade);
        } catch (final NotFoundException e) {
            throw new BusinessException("O dia informado não existe.");
        }

        Viagem viagem = atividade.getViagem();
        if (viagem == null) {
            viagem = atividade.getDia().getViagem();
            dia = atividade.getDia();
        }

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeExcluirItem()) {

            /*final Atividade itemAnterior = atividade.getItemAnterior();
            final Atividade itemProximo = atividade.getItemProximo();

            if (itemAnterior != null && itemProximo != null) {
            this.atividadeRepository.alterarProximo(itemAnterior, itemProximo);
            this.atividadeRepository.alterarAnterior(itemProximo, itemAnterior);
            } else if (itemAnterior == null && itemProximo != null) {
            this.atividadeRepository.alterarAnterior(itemProximo, null);
            } else if (itemAnterior != null && itemProximo == null) {
            this.atividadeRepository.alterarProximo(itemAnterior, null);
            }*/
            // this.atividadeRepository.excluir(atividade);
            if (dia != null) {
                dia.getAtividadesQueComecam().remove(atividade);
            } else {
                viagem.getAtividades().remove(atividade);
            }
            this.atividadeRepository.excluir(atividade);
            this.salvar(viagem);

        } else {
            throw new PermissaoViagemException("Você não possui permissão para excluir atividades");
        }
    }

    @Transactional
    public void excluirAtividadePeloIdLocal(final Usuario usuarioAutenticado, final Long idViagem, final Long idLocal) {
        final Atividade atividade = this.consultarAtividadeAOrganizarPorIdLocal(idViagem, idLocal);

        excluirAtividade(usuarioAutenticado, atividade.getId());
    }

    @Transactional
    public void excluirDia(final Usuario usuarioAutenticado, final Long idDia) {
        DiaViagem dia = null;
        try {
            dia = this.diaRepository.consultarPorId(idDia);
        } catch (final NotFoundException e) {
            throw new BusinessException("O dia informado não existe.");
        }
        final Viagem viagem = dia.getViagem();

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeExcluirDia()) {
            this.atividadeRepository.excluirItensDia(idDia);
            this.diaRepository.excluir(idDia);
            if (!viagem.getDias().isEmpty()) {
                this.diaRepository.decrementarNumero(viagem, dia.getNumero());
            } else {
                viagem.setDataInicio(null);
            }
        } else {
            throw new PermissaoViagemException("Você não possui permissão para excluir dia(s)");
        }
    }

    @Transactional
    public void excluirViagem(final Usuario usuarioAutenticado, final Long idViagem) {
        final Viagem viagem = consultarViagem(usuarioAutenticado, idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeExcluirViagem()) {
            this.atividadeRepository.excluirAtividadesViagem(idViagem);
            this.diaRepository.excluirDiasViagem(idViagem);
            this.viagemRepository.excluirDestinosViagem(idViagem);
            this.pedidoDicaService.excluirPedidos(viagem);
            this.conviteService.excluirConvites(viagem);
            this.viagemRepository.excluir(idViagem);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    @Transactional
    public void incluirAtividade(final Usuario usuarioAutenticado, final Atividade atividade) {
        Viagem viagemPermissao = atividade.getViagem();
        if (viagemPermissao == null) {
            viagemPermissao = atividade.getDia().getViagem();
        }
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagemPermissao);

        if (permissoes.isUsuarioPodeIncluirItem()) {

            atividade.setId(this.atividadeRepository.getId());
            this.atividadeRepository.inserir(atividade);

            /*final Viagem viagem = item.getViagem();
            final DiaViagem dia = item.getDia();

            Atividade itemUltimo = null;
            if (viagem != null) {
            itemUltimo = viagem.getItemUltimo();
            } else if (dia != null) {
            itemUltimo = dia.getItemUltimo();
            }
            if (itemUltimo != null) {
            this.atividadeRepository.refresh(itemUltimo);

            itemUltimo.setItemProximo(item);
            item.setItemAnterior(itemUltimo);

            this.atividadeRepository.alterarProximo(itemUltimo, item);
            this.atividadeRepository.alterarAnterior(item, itemUltimo);
            }

            if (viagem != null) {
            this.viagemRepository.refresh(viagem);
            }
            if (dia != null) {
            this.diaRepository.refresh(dia);
            this.viagemRepository.refresh(dia.getViagem());
            }*/
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar atividades");
        }
    }

    @Transactional
    public void incluirAtividadeAPartirSugestaoAmigo(final Usuario usuarioAutenticado, final Atividade atividade, final Long idSugestaoLocal) {
        incluirAtividade(usuarioAutenticado, atividade);
        final SugestaoLocal sugestao = this.sugestaoLocalService.consultarPorId(idSugestaoLocal);
        sugestao.setAceito(true);
        this.sugestaoLocalService.alterar(sugestao);
    }

    @Transactional
    public void inserirAtividades(final Viagem viagem, final Usuario usuarioAutenticado, final Integer numeroDia, final Long[] idsLocais) {

        final ViagemPermissao permissao = this.viagemObterPermissoes(getUsuarioAutenticado(), viagem);
        if (permissao.isUsuarioPodeIncluirItem()) {

            if (idsLocais != null && idsLocais.length > 0) {
                Local localVisita = null;
                for (int i = 0; i < idsLocais.length; i++) {
                    localVisita = this.localService.consultarPorId(idsLocais[i]);
                    final Atividade atividade = new Atividade();

                    if (numeroDia == null || numeroDia == 0 || viagem.getQuantidadeDias() == null) {
                        atividade.setViagem(viagem);
                    } else {
                        atividade.setDia(viagem.getDia(numeroDia));
                    }

                    atividade.setLocal(localVisita);
                    this.incluirAtividade(usuarioAutenticado, atividade);
                }
            }
        }
    }

    @Transactional
    public void inserirViagem(final Usuario usuarioAutenticado, final Viagem viagem) {

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeIncluirViagem()) {
            // viagem.setId(this.viagemRepository.getId());

            if (viagem.getTitulo() == null || viagem.getTitulo().trim().equals("")) {
                viagem.setTitulo("Viagem para " + viagem.getPrimeiroDestino().getDestino().getNome());
            }
            if (viagem.getVisibilidade() == null) {
                viagem.setVisibilidade(TipoVisibilidade.SOMENTE_EU);
            }

            viagem.setDataInclusao(new Date());
            if (viagem.getDias() != null) {
                for (final DiaViagem dia : viagem.getDias()) {
                    dia.setId(this.diaRepository.getId());
                }
            }
            this.viagemRepository.inserir(viagem);
            if (viagem.getDias() != null) {
                for (final DiaViagem dia : viagem.getDias()) {
                    this.diaRepository.inserir(dia);
                }
            }
        }
    }

    /**
     * Muda a ordem de uma atividade na lista
     *
     * @param usuarioAutenticado
     * @param idAtividade
     * @param quantidadePosicoes
     *            numero de posições a alterar (positivo se for mover para baixo e negativo para cima)
     */
    @Transactional
    public void moverAtividade(final Usuario usuarioAutenticado, final Long idAtividade, final int quantidadePosicoes) {
        final Atividade atividade = consultarAtividadePorId(idAtividade);
        final int posicao = (atividade.getOrdem()) + quantidadePosicoes;
        moverAtividadeParaPosicao(usuarioAutenticado, idAtividade, posicao);
    }

    @Transactional
    public void moverAtividadeParaPosicao(final Usuario usuarioAutenticado, final Long idAtividade, final int posicao) {
        final Atividade atividade = consultarAtividadePorId(idAtividade);
        DiaViagem dia = null;

        Viagem viagem = atividade.getViagem();
        if (viagem == null) {
            viagem = atividade.getDia().getViagem();
            dia = atividade.getDia();
        }
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeMoverItem()) {
            List<Atividade> atividades = null;

            if (dia != null) {
                atividades = dia.getAtividadesQueComecam();
            } else {
                atividades = viagem.getAtividades();
            }
            if (posicao >= 0 && posicao < atividades.size() && posicao != atividade.getOrdem()) {
                atividades.remove(atividade);
                atividades.add(posicao, atividade);
                this.alterar(viagem);
            }
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar atividades");
        }
    }

    @Transactional
    public void moverAtividadeParaPrimeiro(final Usuario usuarioAutenticado, final Long idAtividade) {
        moverAtividadeParaPosicao(usuarioAutenticado, idAtividade, 0);
    }

    @Transactional
    public void moverAtividadeParaUltimo(final Usuario usuarioAutenticado, final Long idAtividade) {
        final Integer ultimaPosicao = this.atividadeRepository.recuperarIndiceUltimaAtividade(consultarAtividadePorId(idAtividade));
        if (ultimaPosicao != null) {
            moverAtividadeParaPosicao(usuarioAutenticado, idAtividade, ultimaPosicao);
        }
    }

    @Transactional
    public Atividade movimentarAtividadeParaDia(final Usuario usuarioAutenticado, final Long idAtividade, final Long idDiaInicio, final Long idDiaFim) {
        final Atividade atividade = consultarAtividadePorId(idAtividade);
        final DiaViagem diaAntesDaAlteracao = atividade.getDia();

        Viagem viagem = atividade.getViagem();
        if (viagem == null) {
            viagem = atividade.getDia().getViagem();
        }
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeMovimentarItem()) {

            if (atividade.getDia() != null && atividade.getDia().getId().equals(idDiaInicio)) {
                return atividade;
            }
            final DiaViagem diaInicio = this.diaRepository.consultarPorId(idDiaInicio);
            DiaViagem diaFim = null;
            if (idDiaFim != null) {
                diaFim = this.diaRepository.consultarPorId(idDiaFim);
                // se o dia final for antes do dia inicio
                if (diaFim.getNumero() < diaInicio.getNumero()) {
                    // o dia fim passa a ser o mesmo dia inicial
                    diaFim = diaInicio;
                }
            }

            // Remover do dia antigo ou do "a organizar"
            if (diaAntesDaAlteracao != null) {
                diaAntesDaAlteracao.getAtividadesQueComecam().remove(atividade);
            } else {
                viagem.getAtividades().remove(atividade);
            }

            this.atividadeRepository.alterarDiaAtividade(atividade, viagem, diaInicio, diaFim);
            this.alterar(viagem);

            // Pega a ultima atividade do dia destino
            /*final Atividade ultimaAtividade = diaInicio.getItemUltimo();
            // Pega a atividade anterior da atividade movida
            final Atividade atividadeAnterior = atividade.getItemAnterior();
            // Pega a proxima atividade da atividade movida
            final Atividade proximaAtividade = atividade.getItemProximo();

            // Se o dia destino tiver atividade
            if (ultimaAtividade != null) {
            // Altera a atividade movida para ser depois da ultima atividade do dia destino
            this.atividadeRepository.alterarProximo(ultimaAtividade, atividade);
            }
            // Se a atividade movida possuia atividade anterior
            if (atividadeAnterior != null) {
            // Liga a atividade que era a anterior com a que era a proxima
            this.atividadeRepository.alterarProximo(atividadeAnterior, proximaAtividade);
            }
            // Se a atividade movida possuia proxima atividade
            if (proximaAtividade != null) {
            // Liga a atividade que era a proxima com a que era a anterior
            this.atividadeRepository.alterarAnterior(proximaAtividade, atividadeAnterior);
            }
            // Altera o dia inicio, o dia fim e a atividade anterior
            this.atividadeRepository.alterarViagemDiaAnteriorProximo(atividade, null, diaInicio, diaFim, ultimaAtividade, null);

            atividade = this.consultarAtividadePorId(idAtividade);*/
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar atividades");
        }
        return atividade;
    }

    /**
     * Move a atividade para "sem dia definido"
     *
     * @param usuarioAutenticado
     * @param idAtividade
     */
    @Transactional
    public void movimentarAtividadeParaOrganizar(final Usuario usuarioAutenticado, final Long idAtividade) {
        final Atividade atividade = consultarAtividadePorId(idAtividade);

        Viagem viagem = atividade.getViagem();
        final DiaViagem dia = atividade.getDia();
        if (viagem == null) {
            viagem = atividade.getDia().getViagem();
        }
        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioPodeMovimentarItem()) {

            if (atividade.getViagem() != null) {
                return;
            }

            dia.getAtividadesQueComecam().remove(atividade);

            atividade.setDia(null);
            atividade.setDiaFim(null);
            atividade.setViagem(viagem);
            final Integer indice = this.atividadeRepository.recuperarIndiceUltimaAtividadeDaViagem(viagem);

            atividade.setOrdem(indice == null ? 0 : indice + 1);

            this.alterarAtividade(usuarioAutenticado, atividade);
            this.alterar(viagem);

            /*final Atividade atividadeAnterior = atividade.getItemAnterior();
            final Atividade atividadeProximo = atividade.getItemProximo();

            final Viagem viagem = atividade.getDia().getViagem();
            final Atividade atividadeUltimo = viagem.getItemUltimo();

            if (atividadeUltimo != null) {
            this.atividadeRepository.alterarProximo(atividadeUltimo, atividade);
            }
            if (atividadeAnterior != null) {
            this.atividadeRepository.alterarProximo(atividadeAnterior, atividadeProximo);
            }
            if (atividadeProximo != null) {
            this.atividadeRepository.alterarAnterior(atividadeProximo, atividadeAnterior);
            }

            this.atividadeRepository.alterarViagemDiaAnteriorProximo(atividade, viagem, null, null, atividadeUltimo, null);*/

        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar atividades");
        }
    }

    @Transactional
    public List<Atividade> movimentarAtividadesParaDia(final Usuario usuarioAutenticado, final Long[] idsAtividade, final Long idDiaInicio,
            final Long idDiaFim) {
        final List<Atividade> lista = new ArrayList<Atividade>();
        for (final Long idAtividade : idsAtividade) {
            lista.add(movimentarAtividadeParaDia(usuarioAutenticado, idAtividade, idDiaInicio, idDiaFim));
        }
        return lista;
    }

    @Transactional
    public void movimentarAtividadesParaDiaADefinir(final Usuario usuarioAutenticado, final Long[] idsAtividade) {
        for (final Long idAtividade : idsAtividade) {
            movimentarAtividadeParaOrganizar(usuarioAutenticado, idAtividade);
        }
    }

    @Transactional
    public void organizarRoteiroAutomaticamente(final Long idViagem) {
        final Viagem viagem = consultarViagem(getUsuarioAutenticado(), idViagem);
        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {

            // Separar as atividades por cidade (se for mais de uma)
            // - se tiver atividades que não perteçam a algum destino da viagem, considerar a cidade mais próxima
            if (viagem.getDestinosViagem().size() > 1) {

                // Map separando os detinos e suas atividades
                final Map<DestinoViagem, List<Atividade>> destinoAtividades = separarAtividadesPorDestino(viagem, viagem.getAtividades());

                // Definir quantos dias para cada destino com base no numero de atividades
                calcularDiasPorDestinoViagem(viagem, destinoAtividades);

                // Dia inicial da distribuição de atividades
                int diaInicio = 1;

                final List<DestinoViagem> listaDestinos = new ArrayList<DestinoViagem>(viagem.getDestinosViagem());

                for (final DestinoViagem destino : listaDestinos) {
                    organizarAtividades(viagem, destino, destinoAtividades.get(destino), diaInicio);
                    // Ao proximo dia inicial é somado a quantidades de dia do destino atual
                    diaInicio += destino.getQuantidadeDias();
                    // não permitir o dia inicial ser maior que a quantidade total de dias da viagem
                    if (diaInicio > viagem.getQuantidadeDias()) {
                        diaInicio = viagem.getQuantidadeDias();
                    }
                }
            } else {
                // Se for somente uma cidade
                organizarAtividades(viagem, viagem.getPrimeiroDestino(), viagem.getAtividades(), 1);
            }
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    @Transactional
    public void pedirDicas(final Long idViagem, final Long[] idsUsuariosTripFansSolicitados, final String[] idsUsuariosFacebookSolicitados,
            final String facebookAppRequestId, final String mensagem) {
        final Viagem viagem = this.consultarPorId(idViagem);
        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        PedidoDica pedido = null;
        Usuario usuario = null;
        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {

            if (idsUsuariosTripFansSolicitados != null) {
                for (final Long id : idsUsuariosTripFansSolicitados) {
                    usuario = this.usuarioService.consultarPorId(id);
                    pedido = new PedidoDica(getUsuarioAutenticado(), usuario, viagem, null, mensagem);
                    this.pedidoDicaService.incluir(pedido);
                }
            }
            if (idsUsuariosFacebookSolicitados != null) {

                for (final String idUsuarioFacebook : idsUsuariosFacebookSolicitados) {
                    usuario = this.usuarioService.consultarUsuarioPorIdFacebook(idUsuarioFacebook);
                    pedido = new PedidoDica(getUsuarioAutenticado(), usuario, viagem, facebookAppRequestId, mensagem);
                    this.pedidoDicaService.incluir(pedido);
                }
            }

        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    @Transactional
    public void pedirDicasDeViagemParaAmigosPorEmails(final Long idViagem, final String enderecosEmailPorVirgula) {

        final Viagem viagem = this.consultarPorId(idViagem);
        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
            this.pedidoDicaService.pedirDicasDeViagemParaAmigosPorEmails(viagem, enderecosEmailPorVirgula);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }
    }

    @Transactional
    public Viagem publicarViagem(final Usuario usuarioAutenticado, final Long idViagem, final TipoVisibilidade visibilidade) {
        final Viagem viagem = consultarViagem(usuarioAutenticado, idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);

        if (permissoes.isUsuarioEOCriadorDaViagem()) {
            viagem.setDataPublicacao(new LocalDateTime());
            viagem.setVisibilidade(visibilidade);
            this.alterarViagem(usuarioAutenticado, viagem);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para acessar esta viagem");
        }
        return viagem;
    }

    @Transactional
    public void removerParticipante(final Long idViagem, final ParticipanteViagem participante) {
        final Viagem viagem = consultarViagem(getUsuarioAutenticado(), idViagem);

        final ViagemPermissao permissoes = viagemObterPermissoes(getUsuarioAutenticado(), viagem);

        if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
            this.viagemRepository.removerParticipante(participante);
        } else {
            throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
        }

    }

    @Transactional
    public void salvar(final Usuario usuarioAutenticado, Viagem viagem, final Integer quantidadeDias, final LocalGeografico[] destinos) {
        if (destinos != null) {
            viagem = configurarDestinosViagem(viagem, destinos);
        }
        configurarDiasViagem(viagem, quantidadeDias);
        if (viagem.getId() == null) {
            viagem.setUsuarioCriador(usuarioAutenticado);
            this.inserirViagem(usuarioAutenticado, viagem);
        } else {
            final ViagemPermissao permissoes = viagemObterPermissoes(usuarioAutenticado, viagem);
            if (permissoes.isUsuarioPodeAlterarDadosDaViagem()) {
                this.alterarViagem(usuarioAutenticado, viagem);
            } else {
                throw new PermissaoViagemException("Você não possui permissão para alterar esta viagem");
            }
        }
    }

    public List<PlanoViagem> viagemConsultarProximasViagens(final Usuario usuario) {
        return this.viagemRepository.consultarProximasViagens(usuario);
    }

    public List<PlanoViagem> viagemConsultarViagensAgencias() {
        // TODO Fazer query para viagem de agências
        return new ArrayList<PlanoViagem>();
    }

    public List<PlanoViagem> viagemConsultarViagensAmigos(final Usuario usuario) {
        return this.viagemRepository.consultarViagensAmigos(usuario);
    }

    public List<PlanoViagem> viagemConsultarViagensPublicas() {
        return this.viagemRepository.consultarViagensPublicas();
    }

    public List<PlanoViagem> viagemConsultarViagensRealizadas(final Usuario usuario) {
        return this.viagemRepository.consultarViagensRealizadas(usuario);
    }

    public List<Viagem> viagemConsultarViagensTripFans() {
        final Usuario usuarioTripFans = this.usuarioService.consultarPorId(1L);
        return this.viagemRepository.consultarViagensPublicas(usuarioTripFans);
    }

    public ViagemPermissao viagemObterPermissoes(final Usuario usuarioAutenticado, final Viagem viagem) {
        final ViagemPermissao permissao = new ViagemPermissao();

        if (viagem != null) {
            permissao.setUsuarioEOCriadorDaViagem(viagem.getUsuarioCriador().equals(usuarioAutenticado));
        }

        boolean usuarioAmigoCriador = false;
        if (usuarioAutenticado != null && viagem != null && !viagem.getUsuarioCriador().equals(usuarioAutenticado)
                && viagem.getVisibilidade().equals(TipoVisibilidade.AMIGOS)) {

            final Amizade amizade = this.amizadeService.consultarAmizade(viagem.getUsuarioCriador().getId(), usuarioAutenticado.getId());

            if (amizade != null && !amizade.getRemovidaTripfans()) {
                usuarioAmigoCriador = true;
            }
        }

        if (usuarioAutenticado != null) {
            permissao.setUsuarioPodeIncluirViagem(true);
        }

        if (viagem != null
                && (viagem.usuarioPodeVer(usuarioAutenticado, usuarioAmigoCriador) || (!viagem.getUsuarioCriador().equals(usuarioAutenticado) && (this.pedidoDicaService
                        .existePedidoDica(viagem, usuarioAutenticado) || this.conviteService.existeConviteRecomendacaoSobreViagem(viagem,
                        usuarioAutenticado))))) {
            permissao.setUsuarioPodeVerViagem(true);
            if (usuarioAutenticado != null) {
                permissao.setUsuarioPodeCopiarViagem(true);
            }
        }

        if (viagem != null && viagem.usuarioPodeEditar(usuarioAutenticado)) {
            permissao.setUsuarioPodeAlterarDadosDaViagem(true);
            permissao.setUsuarioPodeExcluirViagem(true);
            permissao.setUsuarioPodeVerViagem(true);

            permissao.setUsuarioPodeAdicionarDia(true);
            permissao.setUsuarioPodeExcluirDia(true);

            permissao.setUsuarioPodeIncluirItem(true);
            permissao.setUsuarioPodeAlterarItem(true);
            permissao.setUsuarioPodeExcluirItem(true);
            permissao.setUsuarioPodeMoverItem(true);
            permissao.setUsuarioPodeMovimentarItem(true);
        }

        if (this.securityService.isAdmin()) {
            permissao.setUsuarioPodeVerViagem(true);
        }

        return permissao;
    }

    /**
     * Faz uma dias para cada Destino da Viagem levando em conta a quantidade de atividades por destino
     *
     * @param viagem
     * @param destinoAtividades
     */
    private void calcularDiasPorDestinoViagem(final Viagem viagem, final Map<DestinoViagem, List<Atividade>> destinoAtividades) {
        final Integer totalAtividades = viagem.getAtividades().size();
        final Integer quantidadeDias = viagem.getQuantidadeDias();
        Integer quantidadeDiasAtribuido = 0;
        Double percentualAtividadesDoDestino;
        for (final DestinoViagem destino : destinoAtividades.keySet()) {
            percentualAtividadesDoDestino = 0d;
            final List<Atividade> atividades = destinoAtividades.get(destino);
            if (!atividades.isEmpty()) {
                percentualAtividadesDoDestino = (new Double(atividades.size()) / totalAtividades);
                destino.setQuantidadeDias((int) (percentualAtividadesDoDestino * quantidadeDias));
            } else {
                destino.setQuantidadeDias(1);
            }
            quantidadeDiasAtribuido += destino.getQuantidadeDias();
        }

        if (quantidadeDias > quantidadeDiasAtribuido) {
            int diasFaltam = quantidadeDias - quantidadeDiasAtribuido;
            final SortedSet<DestinoViagem> destinos = new TreeSet<DestinoViagem>(destinoAtividades.keySet());

            for (final DestinoViagem destino : destinos) {
                destino.adicionaDia();
                diasFaltam--;
                if (diasFaltam == 0) {
                    break;
                }
            }
        }

        // TODO Verificar se o total de dias somados passam do total de dias da viagem e ver o que fazer para equalizar
    }

    private Viagem configurarDestinosViagem(final Viagem viagem, final DestinoViagemListWrapper destinosWrapper) {
        viagem.getDestinosViagem().clear();
        for (final DestinoViagem destinoViagemRecebido : destinosWrapper.getList()) {
            viagem.getDestinosViagem().add(destinoViagemRecebido);
        }
        return viagem;
    }

    private Viagem configurarDestinosViagem(final Viagem viagem, final LocalGeografico[] destinos) {
        viagem.getDestinosViagem().clear();
        for (int i = 0; i < destinos.length; i++) {
            if (destinos[i] != null) {
                final DestinoViagem destinoViagem = new DestinoViagem();
                destinoViagem.setDestino(destinos[i]);
                destinoViagem.setOrdem(i + 1);
                destinoViagem.setViagem(viagem);
                viagem.getDestinosViagem().add(destinoViagem);
            }
        }
        return viagem;
    }

    private Viagem configurarDiasViagem(final Viagem viagem, final Integer novaQuantidadeDias) {
        if (novaQuantidadeDias != null) {
            // Caso a viagem não possua dias, cria uma nova lista
            if (viagem.getDias() == null) {
                viagem.setDias(new ArrayList<DiaViagem>());
                for (int i = 0; i < novaQuantidadeDias; i++) {
                    viagem.getDias().add(new DiaViagem(viagem, i + 1));
                }
            }
            // Caso a viagem JÁ possua dias:
            else {
                Integer qtdDiasViagem = viagem.getQuantidadeDias();
                if (qtdDiasViagem == null) {
                    qtdDiasViagem = 0;
                }
                // se for mais dias do que a nova quantidade, adicionar os dias na lista
                if (qtdDiasViagem < novaQuantidadeDias) {
                    for (int i = qtdDiasViagem; i < novaQuantidadeDias; i++) {
                        final Long idDia = this.diaRepository.getId();
                        final DiaViagem diaViagem = new DiaViagem(viagem, i + 1);
                        diaViagem.setId(idDia);
                        viagem.getDias().add(diaViagem);
                    }
                }
                // se for menos dias do que a nova quantidade, remover os dias na lista
                else if (qtdDiasViagem > novaQuantidadeDias) {
                    final List<DiaViagem> diasARemover = new ArrayList<DiaViagem>();

                    final List<Atividade> ativdadesARemover = new ArrayList<Atividade>();
                    // TODO implementar
                    // o que fazer com as atividades dos dias removidos?
                    // jogar para o "data a definir"?
                    for (final DiaViagem dia : viagem.getDias()) {
                        if (dia.getNumero() > novaQuantidadeDias) {
                            if (!dia.getAtividades().isEmpty()) {
                                for (final Atividade atividade : dia.getAtividades()) {
                                    if (atividade.isComecaNoDia(dia.getId())) {
                                        ativdadesARemover.add(atividade);
                                    } else if (atividade.isTerminaNoDia(dia.getId())) {
                                        atividade.setDiaFim(null);
                                    }
                                }
                            }
                            // Remover dia
                            diasARemover.add(dia);
                        }
                    }
                    if (!ativdadesARemover.isEmpty()) {
                        for (final Atividade atividade : ativdadesARemover) {
                            this.excluirAtividade(this.getUsuarioAutenticado(), atividade.getId());
                        }
                    }
                    if (!diasARemover.isEmpty()) {
                        for (final DiaViagem dia : diasARemover) {
                            viagem.getDias().remove(dia);
                        }
                    }
                }
            }
        }
        return viagem;
    }

    private void distribuirAtividades(final Viagem viagem, final DestinoViagem destino, final List<Atividade> atividades,
            final Integer quantidadeDias, final Integer diaInicio) {
        /*
         * Calcular a quantidade de atividades (separadas por tipo) a serem distribuidas por cada dia
         * Se estiver "sobrando" Atividades, colocar mais nos primeiros dias de viagem
         */
        final int quantidadeAtividades = atividades.size();
        int quantidadeAtividadesPorDia = 0;
        int sobraAtividades = 0;
        if (quantidadeAtividades > 0) {
            quantidadeAtividadesPorDia = quantidadeAtividades / quantidadeDias;
            if (quantidadeAtividadesPorDia > 0) {
                sobraAtividades = quantidadeAtividades % quantidadeDias;
            } else {
                quantidadeAtividadesPorDia = 1;
            }
        }

        /*
         * Para cada dia neste destino, tentar distribuir de forma proporcional as atividades, colocando em primeiro as mais próximas do centro da cidade
         */
        for (int dia = diaInicio; dia <= ((diaInicio + quantidadeDias) - 1); dia++) {

            if (!atividades.isEmpty()) {
                for (int i = 1; i <= quantidadeAtividadesPorDia; i++) {
                    Atividade atividade = extrairAtividadeMaisProximaCentroCidade(viagem, atividades, destino.getDestino());
                    // Adicionar atividade no dia
                    viagem.getDia(dia).addAtividade(atividade);
                    // Se tiver sobrando adicionar outra
                    if (sobraAtividades > 0) {
                        atividade = extrairAtividadeMaisProximaCentroCidade(viagem, atividades, destino.getDestino());
                        viagem.getDia(dia).addAtividade(atividade);
                        sobraAtividades--;
                    }
                    // this.salvar(viagem);
                }
            }
        }
    }

    private Atividade extrairAtividadeMaisProximaCentroCidade(final Viagem viagem, final List<Atividade> atividades,
            final LocalGeografico localGeografico) {
        // Ordenar pela distancia
        Collections.sort(atividades, new LocalidadeUtils().new DistanciaAtividadeLocalComparator(localGeografico.getCoordenadaGeografica()));
        // Remover da lista
        final Atividade atividade = atividades.remove(0);
        // Remover da lista de "a organizar" da viagem
        viagem.getAtividades().remove(atividade);
        // Reordenar
        for (int i = 0; i < viagem.getAtividades().size(); i++) {
            viagem.getAtividades().get(i).setOrdem(i);
        }
        return atividade;
    }

    /**
     * Retorna a cidade na qual a atividade se encontra mais proxima
     *
     * @param atividades
     * @param localGeografico
     * @return
     */
    private LocalGeografico extrairCidadeOndeAtividadeMaisProxima(final Atividade atividade, final List<LocalGeografico> locaisGeografico) {
        if (atividade.getLocal() != null) {
            Collections.sort(locaisGeografico, new LocalidadeUtils().new DistanciaLocalParaOutroLocalComparator(atividade.getLocal()
                    .getCoordenadaGeografica()));
            return locaisGeografico.get(0);
        } else {
            return null;
        }
    }

    private LocalGeografico extrairCidadeOndeAtividadeMaisProximaPorDestinosViagem(final Atividade atividade, final List<DestinoViagem> destinos) {
        final List<LocalGeografico> locais = new ArrayList<LocalGeografico>();
        for (final DestinoViagem destinoViagem : destinos) {
            locais.add(destinoViagem.getDestino());
        }
        return extrairCidadeOndeAtividadeMaisProxima(atividade, locais);
    }

    /**
     * Tenta organizar as atividades que estão na situação "a organizar" distribuindo-as de uma forma proporcional entre dos dias da Viagem
     *
     * @param viagem
     *            Viagem a ser organizada
     * @param destino
     *            Destino que será considerado para distribuição da atividades
     * @param atividades
     *            Lista de atividades que serão distribuidas
     * @param diaInicio
     *            Dia inicial da distribuição
     */
    private void organizarAtividades(final Viagem viagem, final DestinoViagem destino, final List<Atividade> atividades, final Integer diaInicio) {

        // Colocar primeiro os mais proximos do centro
        // Tentar colocar um numero igual de atividades antes e depois de cada restaurante (ex: )

        // Comparador de distâncias
        final DistanciaAtividadeLocalComparator comparator = new LocalidadeUtils().new DistanciaAtividadeLocalComparator(destino.getDestino()
                .getCoordenadaGeografica());

        // - Separar as atividades por cada tipo (Atracao, Restaurante, Hotel)
        // - Ordenar por distancia do "centro" da cidade colocando as mais próximas em primeiro
        final List<Atividade> atracoes = separarAtividadesPorTipo(atividades, TipoAtividade.VISITA_ATRACAO);
        Collections.sort(atracoes, comparator);
        final List<Atividade> restaurantes = separarAtividadesPorTipo(atividades, TipoAtividade.ALIMENTACAO);
        Collections.sort(restaurantes, comparator);
        final List<Atividade> hoteis = separarAtividadesPorTipo(atividades, TipoAtividade.HOSPEDAGEM);
        Collections.sort(hoteis, comparator);

        int quantidadeDiasPorCidade = 0;
        // Numero de dias planejados para este destino
        if (viagem.getDestinosViagem().size() > 1) {
            quantidadeDiasPorCidade = destino.getQuantidadeDias();
        } else {
            quantidadeDiasPorCidade = viagem.getQuantidadeDias();
        }

        // Quantidade de hoteis neste destino
        final int quantidadeHoteis = hoteis.size();
        // Calcular quantos dias serão gastos em cada hotel (caso tenha mais de um)
        final int quantidadeDiasPorHotel = quantidadeHoteis > 0 ? quantidadeDiasPorCidade / quantidadeHoteis : 0;

        // Tentar colocar hotel no inicio da viagem e terminando no ultimo dia (se for 1) e se for mais de 1 dividir os dias pelas quantidades
        // de hoteis ou não fazer hoteis
        for (int dia = diaInicio, j = 1; j <= quantidadeHoteis; dia = (dia + quantidadeDiasPorHotel), j++) {
            // Pegar o hotel mais próximo do 'centro' da cidade viagem.getAtividades()
            final Atividade atividade = extrairAtividadeMaisProximaCentroCidade(viagem, hoteis, destino.getDestino());
            viagem.getDia(dia).addAtividade(atividade);
            atividade.setDiaFim(viagem.getDia(dia + quantidadeDiasPorHotel));
        }

        // Distribuir os restaurantes
        distribuirAtividades(viagem, destino, restaurantes, quantidadeDiasPorCidade, diaInicio);

        // Distribuir as atrações
        distribuirAtividades(viagem, destino, atracoes, quantidadeDiasPorCidade, diaInicio);

        this.salvar(viagem);

        // Se sobrar alguma atividade, manter no dia a organizar...
        /*final List<Atividade> sobraAtividades = viagem.getAtividades();
        // (Teoricamente não deveria acontecer para os casos abaixo -> Testar se há possibilidade de acontecer isso)
        sobraAtividades.addAll(hoteis);
        sobraAtividades.addAll(atracoes);
        sobraAtividades.addAll(restaurantes);

        // Reordenar as que sobraram
        for (int i = 0; i < sobraAtividades.size(); i++) {
        sobraAtividades.get(i).setOrdem(i);
        }
        this.salvar(viagem);*/

    }

    private List<Atividade> recuperarAtividadesPorCidade(final Viagem viagem, final LocalGeografico local, final List<Atividade> atividades) {
        final List<Atividade> atividadesFiltradas = new ArrayList<Atividade>();
        for (final Atividade atividade : viagem.getAtividades()) {
            // verificar se a atividade ocorre no local informado
            if (atividade.getLocal() != null) {
                viagem.getDestinosViagem().contains(atividade.getLocal());
                // final aaaa
                final LocalGeografico localMaisProximo = extrairCidadeOndeAtividadeMaisProximaPorDestinosViagem(atividade, viagem.getDestinosViagem());
                if (localMaisProximo.getId().equals(local.getId())) {
                    atividadesFiltradas.add(atividade);
                }

            }
        }
        return atividadesFiltradas;

    }

    private Map<DestinoViagem, List<Atividade>> separarAtividadesPorDestino(final Viagem viagem, final List<Atividade> atividades) {
        final Map<DestinoViagem, List<Atividade>> atividadesDestino = new HashMap<DestinoViagem, List<Atividade>>();
        for (final DestinoViagem destino : viagem.getDestinosViagem()) {
            atividadesDestino.put(destino, recuperarAtividadesPorCidade(viagem, destino.getDestino(), atividades));
        }
        return atividadesDestino;
    }

    private List<Atividade> separarAtividadesPorTipo(final List<Atividade> atividades, final TipoAtividade tipoAtividade) {
        final ArrayList<Atividade> atividadesPorTipo = new ArrayList<Atividade>();
        if ((atividades != null) && !atividades.isEmpty()) {
            for (final Atividade atividade : atividades) {
                if (atividade.getTipo().equals(tipoAtividade)) {
                    atividadesPorTipo.add(atividade);
                }
            }
        }
        return atividadesPorTipo;
    }

}
