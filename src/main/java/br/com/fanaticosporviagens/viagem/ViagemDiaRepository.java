package br.com.fanaticosporviagens.viagem;

import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.DiaViagem;
import br.com.fanaticosporviagens.model.entity.Viagem;

@Repository
public class ViagemDiaRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected GenericSearchRepository searchRepository;

    public DiaViagem consultarPorId(final Long idDia) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idDia", idDia);
        try {
            return (DiaViagem) this.searchRepository.consultaHQL("select dia from DiaViagem dia where dia.id = :idDia", params).get(0);
        } catch (final java.lang.IndexOutOfBoundsException e) {
            throw new NotFoundException("Dia não encontrado");
        }
    }

    @Transactional
    public void decrementarNumero(final Viagem viagem, final Integer numeroInicioDecremento) {
        this.entityManager.createQuery(
                "update DiaViagem set numero = (numero - 1)  where viagem = " + viagem.getId() + " and numero > " + numeroInicioDecremento)
                .executeUpdate();
    }

    @Transactional
    public void excluir(final Long idDia) {
        this.entityManager.createQuery("delete from DiaViagem dia where dia.id = " + idDia).executeUpdate();
    }

    @Transactional
    public void excluirDiasViagem(final Long idViagem) {
        this.entityManager.createQuery("delete from DiaViagem dia where dia.viagem.id = " + idViagem).executeUpdate();
    }

    public Long getId() {
        return new Long(this.entityManager.createNativeQuery("select nextval('seq_viagem_dia')").getSingleResult().toString());
    }

    @Transactional
    public void inserir(final DiaViagem dia) {
        this.entityManager.persist(dia);
    }

    public void refresh(final DiaViagem dia) {
        this.entityManager.refresh(dia);
    }
}
