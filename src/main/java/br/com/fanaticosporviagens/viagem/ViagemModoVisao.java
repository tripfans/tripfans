package br.com.fanaticosporviagens.viagem;

public enum ViagemModoVisao {
    CALENDARIO("Calendário", "viagemExibirCalendario", "icons/calendar_view_week.png"),
    LISTA_ITENS("Lista de ítens", "viagemExibirListaItens", "icons/application_view_list.png");

    private final String icone;

    private final String nome;

    private final String view;

    ViagemModoVisao(final String nome, final String view, final String icone) {
        this.nome = nome;
        this.view = view;
        this.icone = icone;
    }

    public String getIcone() {
        return this.icone;
    }

    public String getNome() {
        return this.nome;
    }

    public String getView() {
        return this.view;
    }

    public boolean isCalendario() {
        return this.equals(ViagemModoVisao.CALENDARIO);
    }

    public boolean isListaItens() {
        return this.equals(ViagemModoVisao.LISTA_ITENS);
    }
}
