package br.com.fanaticosporviagens.viagem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.exception.NotFoundException;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.ParticipanteViagem;
import br.com.fanaticosporviagens.model.entity.PlanoViagem;
import br.com.fanaticosporviagens.model.entity.TipoVisibilidade;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Viagem;

@Repository
public class ViagemRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected GenericSearchRepository searchRepository;

    @Transactional
    public void alterar(final Viagem viagem) {
        this.entityManager.persist(viagem);
    }

    @Transactional
    public void alterarParticipante(final ParticipanteViagem participante) {
        this.entityManager.merge(participante);
    }

    public List<ParticipanteViagem> consultarCompartilhamentos(final Long idViagem) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idViagem", idViagem);
        return this.searchRepository.consultaHQL(
                "SELECT participante FROM ParticipanteViagem participante WHERE participante.viagem.id = :idViagem and participante.viaja = false ",
                params);
    }

    public Viagem consultarPorId(final Long idViagem) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idViagem", idViagem);

        final Viagem viagem = this.searchRepository.queryByHQLUniqueResult("select viagem from Viagem viagem where viagem.id = :idViagem ", params);
        if (viagem == null) {
            throw new NotFoundException("Viagem não encontrada");
        }
        return viagem;

    }

    public List<PlanoViagem> consultarProximasViagens(final Usuario usuario) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("dataAtual", new LocalDate());
        params.put("usuarioCriador", usuario);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where (viagem.dataInicio is null or viagem.dataInicio >= :dataAtual) ");
        hql.append("   and viagem.usuarioCriador = :usuarioCriador ");
        hql.append(" order by viagem.dataInicio desc, viagem.dataCriacao desc ");

        return consultar(hql.toString(), params);
    }

    public List<RoteiroViagemView> consultarRoteirosDestaques(final Usuario usuarioAtual, final Long idDestino) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idDestino", idDestino);

        List<RoteiroViagemView> roteirosDestaques = new ArrayList<RoteiroViagemView>();

        if (usuarioAtual == null) {
            roteirosDestaques = this.searchRepository.consultarPorNamedNativeQuery("Viagem.consultarRoteirosPublicosDestaques", params,
                    RoteiroViagemView.class);

        } else {
            params.put("idUsuario", usuarioAtual.getId());
            roteirosDestaques = this.searchRepository.consultarPorNamedNativeQuery("Viagem.consultarRoteirosDestaquesQueUsuarioPodeVer", params,
                    RoteiroViagemView.class);
            params.remove("idUsuario");
        }

        for (final RoteiroViagemView roteiro : roteirosDestaques) {
            params.put("idRoteiro", roteiro.getIdRoteiro());
            final String urlFotoAtividade = this.searchRepository.queryByNamedNativeQueryUniqueResult("Viagem.consultarFotoAtividade", params);
            if (StringUtils.isNotEmpty(urlFotoAtividade)) {
                roteiro.setUrlFotoRoteiro(urlFotoAtividade);
            }
        }

        return roteirosDestaques;
    }

    public List<Viagem> consultarTodasViagensAmigosPodemVer(final Usuario criador) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("visibilidades", new TipoVisibilidade[] { TipoVisibilidade.PUBLICO, TipoVisibilidade.AMIGOS });
        params.put("criador", criador);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where viagem.visibilidade IN :visibilidades ");
        hql.append("   and viagem.usuarioCriador = :criador ");
        hql.append(" order by viagem.dataCriacao desc ");

        return this.searchRepository.consultaHQL(hql.toString(), params);
    }

    public List<Viagem> consultarTodasViagensUsuario(final Usuario criador) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("criador", criador);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where viagem.usuarioCriador = :criador ");
        hql.append(" order by viagem.dataCriacao desc ");

        return this.searchRepository.consultaHQL(hql.toString(), params);
    }

    public List<PlanoViagem> consultarViagensAmigos(final Usuario usuario) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("visibilidadeAmigo", TipoVisibilidade.AMIGOS);
        params.put("visibilidadePublico", TipoVisibilidade.PUBLICO);
        params.put("usuario", usuario);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where (viagem.visibilidade = :visibilidadeAmigo or viagem.visibilidade = :visibilidadePublico) ");
        hql.append("   and exists( select 1 ");
        hql.append("                 from Amizade amizade ");
        hql.append("                where amizade.amigo = viagem.usuarioCriador ");
        hql.append("                  and amizade.usuario = :usuario ");
        hql.append("             ) ");
        hql.append(" order by viagem.dataInicio desc, viagem.dataCriacao desc ");

        return consultar(hql.toString(), params);
    }

    public List<PlanoViagem> consultarViagensPublicas() {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("visibilidadePublica", TipoVisibilidade.PUBLICO);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where viagem.visibilidade = :visibilidadePublica ");
        hql.append(" order by viagem.dataCriacao desc ");

        return consultar(hql.toString(), params);
    }

    public List<Viagem> consultarViagensPublicas(final Usuario criador) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("visibilidadePublica", TipoVisibilidade.PUBLICO);
        params.put("criador", criador);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where viagem.visibilidade = :visibilidadePublica ");
        hql.append("   and viagem.usuarioCriador = :criador ");
        hql.append(" order by viagem.dataCriacao desc ");

        return this.searchRepository.consultaHQL(hql.toString(), params);
    }

    public List<PlanoViagem> consultarViagensRealizadas(final Usuario usuario) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("usuarioCriador", usuario);

        final StringBuilder hql = new StringBuilder();
        hql.append("select viagem");
        hql.append("  from Viagem viagem ");
        hql.append(" where viagem.dataInicio < current_date ");
        hql.append("   and viagem.usuarioCriador = :usuarioCriador ");
        hql.append(" order by viagem.dataInicio desc, viagem.dataCriacao desc ");

        return consultar(hql.toString(), params);
    }

    public List<ParticipanteViagem> consultarViajantes(final Long idViagem) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("idViagem", idViagem);
        return this.searchRepository.consultaHQL(
                "SELECT participante FROM ParticipanteViagem participante WHERE participante.viagem.id = :idViagem and participante.viaja = true ",
                params);
    }

    @Transactional
    public void excluir(final Long idViagem) {
        this.entityManager.createQuery("delete from Viagem where id = " + idViagem).executeUpdate();
    }

    @Transactional
    public void excluirDestinosViagem(final Long idViagem) {
        this.entityManager.createQuery("DELETE FROM DestinoViagem WHERE viagem.id = " + idViagem).executeUpdate();
    }

    public Long getId() {
        return new Long(this.entityManager.createNativeQuery("select nextval('seq_viagem')").getSingleResult().toString());
    }

    @Transactional
    public void incluirParticipante(final ParticipanteViagem participante) {
        this.entityManager.persist(participante);
    }

    @Transactional
    public void inserir(final Viagem viagem) {
        this.entityManager.persist(viagem);
    }

    public void refresh(final Viagem viagem) {
        this.entityManager.refresh(viagem);
    }

    @Transactional
    public void removerParticipante(final ParticipanteViagem participante) {
        this.entityManager.remove(participante);
    }

    private List<PlanoViagem> consultar(final String hql, final Map<String, Object> params) {
        return this.searchRepository.consultaHQL(hql, params);
    }

}
