package br.com.fanaticosporviagens.viagem;

import br.com.fanaticosporviagens.model.entity.Usuario;

public class RoteiroViagemView {

    private Long idAutor;

    private Long idRoteiro;

    private String nomeAutor;

    private String tituloRoteiro;

    private String urlFotoAutor;

    private String urlFotoRoteiro;

    public Usuario getAutor() {
        final Usuario autor = new Usuario();
        autor.setId(this.idAutor);
        autor.setDisplayName(this.nomeAutor);
        autor.setUrlFotoSmall(this.urlFotoAutor);
        return autor;
    }

    public Long getIdAutor() {
        return this.idAutor;
    }

    public Long getIdRoteiro() {
        return this.idRoteiro;
    }

    public String getNomeAutor() {
        return this.nomeAutor;
    }

    public String getTituloRoteiro() {
        return this.tituloRoteiro;
    }

    public String getUrlFotoAutor() {
        return this.urlFotoAutor;
    }

    public String getUrlFotoRoteiro() {
        return this.urlFotoRoteiro;
    }

    public void setIdAutor(final Long idAutor) {
        this.idAutor = idAutor;
    }

    public void setIdRoteiro(final Long idRoteiro) {
        this.idRoteiro = idRoteiro;
    }

    public void setNomeAutor(final String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }

    public void setTituloRoteiro(final String tituloRoteiro) {
        this.tituloRoteiro = tituloRoteiro;
    }

    public void setUrlFotoAutor(final String urlFotoAutor) {
        this.urlFotoAutor = urlFotoAutor;
    }

    public void setUrlFotoRoteiro(final String urlFotoRoteiro) {
        this.urlFotoRoteiro = urlFotoRoteiro;
    }

}
