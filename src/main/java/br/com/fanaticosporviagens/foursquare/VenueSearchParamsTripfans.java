package br.com.fanaticosporviagens.foursquare;

import java.util.Map;

import org.springframework.social.foursquare.api.VenueSearchParams;

public class VenueSearchParamsTripfans extends VenueSearchParams {

    private String near;

    public String getNear() {
        return this.near;
    }

    public VenueSearchParams near(final String near) {
        this.near = near;
        return this;
    }

    @Override
    public Map<String, String> toParameters() {
        final Map<String, String> params = super.toParameters();
        if (this.near != null) {
            params.put("near", this.near.toString());
        }
        return params;
    }

}
