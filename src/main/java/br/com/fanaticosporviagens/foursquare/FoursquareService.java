package br.com.fanaticosporviagens.foursquare;

import java.util.List;

import javax.inject.Inject;

import org.springframework.core.env.Environment;
import org.springframework.social.connect.Connection;
import org.springframework.social.foursquare.api.Checkin;
import org.springframework.social.foursquare.api.CheckinInfo;
import org.springframework.social.foursquare.api.Foursquare;
import org.springframework.social.foursquare.api.Friends;
import org.springframework.social.foursquare.api.Photos;
import org.springframework.social.foursquare.api.Tip;
import org.springframework.social.foursquare.api.Tips;
import org.springframework.social.foursquare.api.Venue;
import org.springframework.social.foursquare.api.impl.FoursquareTemplate;
import org.springframework.stereotype.Service;

import br.com.fanaticosporviagens.facebook.service.BaseProviderService;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author Carlos Nascimento
 */
@Service
public class FoursquareService extends BaseProviderService {

    public static final String FOURSQUARE_CATEGORY_ARTS_ENTERTAINMENT_ID = "4d4b7104d754a06370d81259";

    public static final String FOURSQUARE_CATEGORY_COLLEGE_UNIVERSITY_ID = "4d4b7105d754a06372d81259";

    public static final String FOURSQUARE_CATEGORY_FOOD_ID = "4d4b7105d754a06374d81259";

    public static final String FOURSQUARE_CATEGORY_NIGHTLIFE_SPOT_ID = "4d4b7105d754a06376d81259";

    public static final String FOURSQUARE_CATEGORY_OUTDOORS_RECREATION_ID = "4d4b7105d754a06377d81259";

    public static final String FOURSQUARE_CATEGORY_PROFESSIONAL_ID = "4d4b7105d754a06375d81259";

    public static final String FOURSQUARE_CATEGORY_RESIDENCE_ID = "4e67e38e036454776db1fb3a";

    public static final String FOURSQUARE_CATEGORY_SHOPS_SERVICE_ID = "4d4b7105d754a06378d81259";

    public static final String FOURSQUARE_CATEGORY_TRAVEL_TRANSPORT = "4d4b7105d754a06379d81259";

    public static final String FOURSQUARE_GROUP_CHECKIN = "checkin";

    public static final String FOURSQUARE_GROUP_VENUE = "venue ";

    public static final String FOURSQUARE_SORT_FRIENDS = "friends";

    public static final String FOURSQUARE_SORT_POPULAR = "popular";

    public static final String FOURSQUARE_SORT_RECENT = "recent";

    public static final String FOURSQUARE_SUBCATEGORY_HOTEIS_ID = "4bf58dd8d48988d1fa931735";

    @Inject
    private Environment environment;

    public Friends consultarAmigosUsuario(final Usuario usuario) {
        return getFoursquare(usuario).userOperations().getFriends(usuario.getIdFoursquare());
    }

    public List<Checkin> consultarCheckinsAmigo(final String idAmigoFoursquare) {
        return consultarCheckinsAmigo(null, idAmigoFoursquare);
    }

    public List<Checkin> consultarCheckinsAmigo(final Usuario usuario, final String idAmigoFoursquare) {
        final CheckinInfo checkinInfo = getFoursquare(usuario).userOperations().getCheckins();
        return checkinInfo.getCheckins();
    }

    public List<Checkin> consultarCheckinsUsuario() {
        return consultarCheckinsUsuario(null);
    }

    public List<Checkin> consultarCheckinsUsuario(final Usuario usuario) {
        final CheckinInfo checkinInfo = getFoursquare(usuario).userOperations().getCheckins(usuario.getIdFoursquare());
        return checkinInfo.getCheckins();
    }

    public Photos consultarPhotosLocal(final Local local) {
        return this.consultarPhotosLocal(local, 0, 9, false);
    }

    public Photos consultarPhotosLocal(final Local local, final Integer offset, final Integer limit, final boolean apenasDeAmigos) {
        if (local.getIdFoursquare() != null) {
            return consultarPhotosLocal(local.getIdFoursquare(), offset, limit, apenasDeAmigos);
        }
        return null;
    }

    public Photos consultarPhotosLocal(final String idLocalFoursquare, final Integer offset, final Integer limit, final boolean apenasDeAmigos) {
        if (idLocalFoursquare != null && !idLocalFoursquare.trim().equals("")) {
            final String group = apenasDeAmigos ? FOURSQUARE_GROUP_CHECKIN : FOURSQUARE_GROUP_VENUE;
            return getFoursquareWithoutAuthentication().venueOperations().getPhotos(idLocalFoursquare, group, offset, limit);
        }
        return null;
    }

    public Tips consultarTipsAmigo(final Usuario usuario, final String idAmigoFoursquare) {
        return getFoursquare(usuario).userOperations().getRecentTips(idAmigoFoursquare, 0, 0);
    }

    public List<Tip> consultarTipsDeLocaisProximos(final Double latitude, final Double longitude, final Integer offset, final boolean apenasDeAmigos) {
        if (latitude != null && longitude != null) {
            return getFoursquareWithoutAuthentication().tipOperations().search(latitude, longitude, "", offset, apenasDeAmigos);
        }
        return null;
    }

    public List<Tip> consultarTipsDeLocaisProximos(final Local local, final Integer offset, final boolean apenasDeAmigos) {
        return this.consultarTipsDeLocaisProximos(local.getLatitude(), local.getLongitude(), offset, apenasDeAmigos);
    }

    public Tips consultarTipsLocal(final Local local) {
        if (local.getIdFoursquare() != null) {
            // final String sort = getUsuarioAutenticado() != null ? FOURSQUARE_SORT_FRIENDS : FOURSQUARE_SORT_POPULAR;
            final String sort = FOURSQUARE_SORT_POPULAR;
            return consultarTipsLocal(local.getIdFoursquare(), sort, 0, 10);
        }
        return null;
    }

    public Tips consultarTipsLocal(final String idLocalFoursquare, final String sort, final Integer offset, final Integer limit) {
        if (idLocalFoursquare != null && !idLocalFoursquare.trim().equals("")) {
            return getFoursquareWithoutAuthentication().venueOperations().getTips(idLocalFoursquare, sort, offset, limit);
        }
        return null;
    }

    public Tips consultarTipsUsuario(final Usuario usuario) {
        return getFoursquare(usuario).userOperations().getRecentTips(0, 0);
    }

    protected Foursquare getFoursquare() {
        return getFoursquare(null);
    }

    protected Foursquare getFoursquare(final Usuario usuario) {
        final Connection<Foursquare> foursquare = getConnection(usuario, Foursquare.class);
        // TODO ver como pegar os parametros para criar o template
        return foursquare != null ? foursquare.getApi() : new FoursquareTemplate(this.environment.getProperty("foursquare.clientId"),
                this.environment.getProperty("foursquare.clientSecret"));
    }

    protected Foursquare getFoursquareWithoutAuthentication() {
        /*return new FoursquareTemplateTripfans(this.environment.getProperty("foursquare.clientId"),
                this.environment.getProperty("foursquare.clientSecret"));*/
        return null;
    }

    public List<Venue> recuperarLocaisSemelhantesNoFoursquare(final Local local) {
        /*if (local.getIdFoursquare() != null) {
            return recuperarLocalNoFoursquare(local.getIdFoursquare());
        }*/
        final Foursquare foursquare = getFoursquareWithoutAuthentication();

        final VenueSearchParamsTripfans params = new VenueSearchParamsTripfans();

        if (local.isTipoRestaurante()) {
            params.categoryId(FOURSQUARE_CATEGORY_FOOD_ID);
        } else if (local.isTipoHotel()) {
            params.categoryId(FOURSQUARE_SUBCATEGORY_HOTEIS_ID);
        } else if (local.isTipoAtracao()) {
            /*                params.categoryId(FOURSQUARE_CATEGORY_ARTS_ENTERTAINMENT_ID + "," + FOURSQUARE_CATEGORY_COLLEGE_UNIVERSITY_ID + ","
                                    + FOURSQUARE_CATEGORY_NIGHTLIFE_SPOT_ID + "," + FOURSQUARE_CATEGORY_OUTDOORS_RECREATION_ID + ","
                                    + FOURSQUARE_CATEGORY_PROFESSIONAL_ID + "," + FOURSQUARE_CATEGORY_RESIDENCE_ID + "," + FOURSQUARE_CATEGORY_SHOPS_SERVICE_ID
                                    + "," + FOURSQUARE_CATEGORY_TRAVEL_TRANSPORT + ",");
             */
        }

        if (local.getLatitude() != null && local.getLongitude() != null) {
            params.location(local.getLatitude(), local.getLongitude());
        } else {
            final String siglaEstado = "";
            params.near(local.getLocalizacao().getCidade().getNome() + ", " + (local.getLocalizacao().getEstadoSigla()));
        }

        // TODO Ver se usa esse parametro ou nao > ele limita a busca por uma proximidade mais precisa com relacao a lat/long.
        // O problema é se a nossa lat/long estiver errada ele nao encontra o local > aconteceu com o exemplo q eu estava usando : Porcão de
        // Brasilia
        // params.basis(Intent.MATCH);

        // Remover as "stopwords" do nome para realizar a pesquisa
        // System.out.println("Nome para a pesquisa antes de alterar : " + local.getNome());
        // final String nome = Stopwords.removeStopWords(local.getNome());
        final String nome = local.getNome();
        // System.out.println("Nome alterada para a pesquisa: " + nome);

        params.query(nome);

        // Limitar aos 5 primeiros resultados
        params.limit(10);

        // foursquare.venueOperations().explore(ExploreQuery)

        try {
            return foursquare.venueOperations().search(params);
        } catch (final Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return null;
        // return new ArrayList<Venue>();
    }

    public Venue recuperarLocalNoFoursquare(final String venueId) {
        final Foursquare foursquare = getFoursquareWithoutAuthentication();
        try {
            return foursquare.venueOperations().getVenue(venueId);

        } catch (final Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
