package br.com.fanaticosporviagens.foursquare;

import java.util.LinkedList;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

/**
 * Foi necessário criar esta classe para sobrescrever um comportamento do Template padrão do spring-social-foursquare.
 * O problema se dava por conta de um interceptor que é adicionado no costrutor da classe pai do template.
 * Esse interceptor é o <tt>OAuth2RequestInterceptor</tt> que não permite acessar a API do Foursquare sem um accessToken.
 *
 * @author CarlosN
 *
 */
public class FoursquareTemplateTripfans {// extends FoursquareTemplate {

    public FoursquareTemplateTripfans() {
        // this(null, null);
    }

    /*public FoursquareTemplateTripfans(final String clientId, final String clientSecret) {
        super(clientId, clientSecret);
    }*/

    /**
     * Remove o interceptador que impede o acesso a API sem o accessToken
     */
    // @Override
    protected void configureRestTemplate(final RestTemplate restTemplate) {
        restTemplate.setInterceptors(new LinkedList<ClientHttpRequestInterceptor>());
        /*restTemplate = new RestTemplate(ClientHttpRequestFactorySelector.getRequestFactory());
        restTemplate.setMessageConverters(getMessageConverters());*/
    }
}
