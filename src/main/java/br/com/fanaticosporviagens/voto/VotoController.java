package br.com.fanaticosporviagens.voto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Dica;
import br.com.fanaticosporviagens.model.entity.Resposta;
import br.com.fanaticosporviagens.model.entity.Votavel;

/**
 * @author André Thiago
 * 
 */
@Controller
@RequestMapping("/voto")
@PreAuthorize("isAuthenticated()")
public class VotoController extends BaseController {

    @Autowired
    private VotoService service;

    @RequestMapping(value = "/avaliacao/votarComoUtil/{avaliacao}", method = { RequestMethod.POST }, produces = "application/json")
    @ResponseBody
    public JSONResponse votarComoUtil(@PathVariable final Avaliacao avaliacao, final Model model) {
        return votarComoUtil(avaliacao);
    }

    @RequestMapping(value = "/dica/votarComoUtil/{dica}", method = { RequestMethod.POST }, produces = "application/json")
    @ResponseBody
    public JSONResponse votarComoUtil(@PathVariable final Dica dica, final Model model) {
        return votarComoUtil(dica);
    }

    @RequestMapping(value = "/resposta/votarComoUtil/{resposta}", method = { RequestMethod.POST }, produces = "application/json")
    @ResponseBody
    public JSONResponse votarComoUtil(@PathVariable final Resposta resposta, final Model model) {
        return votarComoUtil(resposta);
    }

    private JSONResponse votarComoUtil(final Votavel votavel) {
        try {
            this.service.avaliarComoUtil(votavel);
            this.jsonResponse.addParam("success", true);
            this.jsonResponse.addParam("quantidadeVotoUtil", votavel.getQuantidadeVotoUtil());
            this.jsonResponse.addParam("message", "Obrigado pelo seu voto!");
        } catch (final BusinessException e) {
            this.jsonResponse.addParam("success", false);
            this.jsonResponse.addParam("quantidadeVotoUtil", votavel.getQuantidadeVotoUtil());
            this.jsonResponse.addParam("message", e.getMessage());
        }
        return this.jsonResponse;
    }

}