package br.com.fanaticosporviagens.voto;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.infra.exception.BusinessException;
import br.com.fanaticosporviagens.infra.model.service.BaseService;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Votavel;
import br.com.fanaticosporviagens.model.entity.VotoUtil;

@Service
public class VotoService extends BaseService {

    @Autowired
    private VotoUtilRepository votoUtilRepository;

    @Transactional
    public Long avaliarComoUtil(final Votavel votavel) {
        final Usuario avaliador = getUsuarioAutenticado();

        validarSeAvaliadorDiferenteDeAutor(avaliador, votavel);

        validarSeAvaliadorJaVotou(avaliador, votavel);

        final VotoUtil voto = new VotoUtil(avaliador, new Date(), votavel);
        votavel.incrementaQuantidadeVotoUtil();
        this.votoUtilRepository.incluir(voto);

        return votavel.getQuantidadeVotoUtil();
    }

    private void validarSeAvaliadorDiferenteDeAutor(final Usuario avaliador, final Votavel votavel) {
        if (votavel.getAutor().equals(avaliador)) {
            throw new BusinessException("Seu voto não foi computado por você ser o autor deste item.");
        }
    }

    private void validarSeAvaliadorJaVotou(final Usuario avaliador, final Votavel votavel) {
        final VotoUtil voto = this.votoUtilRepository.consultarVotoUtil(avaliador, votavel);

        if (voto != null) {
            throw new BusinessException("Seu voto não foi computado pois você já marcou como útil esse item.");
        }
    }

}
