package br.com.fanaticosporviagens.voto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.model.entity.Votavel;
import br.com.fanaticosporviagens.model.entity.VotoUtil;

@Repository
public class VotoUtilRepository extends GenericCRUDRepository<VotoUtil, Long> {
    @Autowired
    private GenericSearchRepository searchRepository;

    public VotoUtil consultarVotoUtil(final Usuario avaliador, final Votavel votavel) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("avaliador", avaliador);
        parametros.put("objetoVotado", votavel);

        final StringBuilder hql = new StringBuilder();
        hql.append("select voto ");
        hql.append("  from VotoUtil voto ");
        hql.append(" where voto.avaliador = :avaliador ");
        hql.append("   and voto.objetoVotado = :objetoVotado ");

        return this.searchRepository.queryByHQLUniqueResult(hql.toString(), parametros);
    }

}
