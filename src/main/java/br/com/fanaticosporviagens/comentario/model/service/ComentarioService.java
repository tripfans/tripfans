package br.com.fanaticosporviagens.comentario.model.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario;
import br.com.fanaticosporviagens.model.entity.Comentavel;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author Carlos Nascimento
 */
@Service
public class ComentarioService extends GenericCRUDService<Comentario, Long> {

    @Autowired
    protected GenericCRUDRepository genericCRUDRepository;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private UrlPathGeneratorService urlPathGeneratorService;

    public List<Comentario> consultarComentarios(final TipoComentario tipo, final Long idObjetoAlvo) {
        final Map<String, Object> parametros = new HashMap<String, Object>();

        if (tipo.equals(TipoComentario.AVALIACAO)) {
            parametros.put("idAvaliacao", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.DIARIO)) {
            parametros.put("idDiario", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.RELATO_VIAGEM)) {
            parametros.put("idItemDiario", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.PERFIL)) {
            parametros.put("idPerfil", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.VIAGEM)) {
            parametros.put("idViagem", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.ALBUM)) {
            parametros.put("idAlbum", idObjetoAlvo);
        } else if (tipo.equals(TipoComentario.FOTO)) {
            parametros.put("idFoto", idObjetoAlvo);
        }
        return this.searchRepository.consultarPorNamedQuery("Comentario.consultarComentarios", parametros, Comentario.class);
    }

    public List<Comentario> consultarComentariosPerfil(final Long idPerfil) {
        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idPerfil", idPerfil);

        return this.searchRepository.consultarPorNamedQuery("Comentario.consultarComentarios", parametros, Comentario.class);
    }

    @Transactional
    public void postarComentario(final Usuario autor, final TipoComentario tipo, final Long idObjetoAlvo, final Comentario comentarioPai,
            final String texto, final boolean postarNoFacebook) {
        final Comentario comentario = criarComentario(tipo, idObjetoAlvo, autor, texto);
        comentario.setComentarioPai(comentarioPai);

        this.urlPathGeneratorService.generate(comentario);

        this.incluir(comentario);
        if (postarNoFacebook) {
            this.facebookService.postarNoMural(comentario);
        }
    }

    @SuppressWarnings("unchecked")
    private Comentario criarComentario(final TipoComentario tipo, final Long idOjetoAlvo, final Usuario autor, final String texto) {
        final Comentario comentario = new Comentario();
        comentario.setAutor(autor);
        comentario.setTipo(tipo);
        comentario.setTexto(texto);
        comentario.setData(new Date());

        comentario.setObjetoAlvo(tipo, (Comentavel) this.genericCRUDRepository.consultarPorId(tipo.getClasseAlvo(), idOjetoAlvo));

        return comentario;
    }
}
