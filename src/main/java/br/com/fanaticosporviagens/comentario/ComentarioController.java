package br.com.fanaticosporviagens.comentario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.fanaticosporviagens.comentario.model.service.ComentarioService;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.infra.model.entity.BaseEntity;
import br.com.fanaticosporviagens.model.entity.Comentario;
import br.com.fanaticosporviagens.model.entity.Comentario.TipoComentario;

@Controller
@RequestMapping("/comentarios")
public class ComentarioController extends BaseController {

    @Autowired
    private ComentarioService comentarioService;

    @RequestMapping(value = "/excluir/{id}")
    @PreAuthorize("#comentario.autor.username == authentication.name")
    public @ResponseBody
    JSONResponse excluir(@PathVariable(value = "id") final Comentario comentario, final Model model) {
        this.comentarioService.excluir(comentario);
        model.addAttribute("comentarios",
                this.comentarioService.consultarComentarios(comentario.getTipo(), (Long) ((BaseEntity<?>) comentario.getObjetoAlvo()).getId()));
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/exibir/{comentario}")
    public String listaComentarios(@PathVariable final Comentario comentario, final Model model) {
        return "";
    }

    @RequestMapping(value = "/lista/{tipo}/{idObjetoAlvo}")
    public String listaComentarios(@PathVariable("tipo") final TipoComentario tipo, @PathVariable final Long idObjetoAlvo,
            @RequestParam(value = "id", required = true) final String htmlElementId, @RequestParam(required = false) final String imgSize,
            final Model model) {
        model.addAttribute("comentarios", this.comentarioService.consultarComentarios(tipo, idObjetoAlvo));
        model.addAttribute("id", htmlElementId);
        model.addAttribute("tipo", tipo);
        model.addAttribute("idAlvo", idObjetoAlvo);
        model.addAttribute("imgSize", imgSize);
        return "/comentarios/listaComentarios";
    }

    @RequestMapping(value = "/postarComentario")
    @PreAuthorize("isAuthenticated()")
    public String postar(@RequestParam("tipo") final TipoComentario tipoComentario, @RequestParam final Long idObjetoAlvo,
            @RequestParam(required = false) final Comentario comentarioPai, @RequestParam final String texto,
            @RequestParam(required = false) final boolean postarNoFacebook, final Model model) {
        this.comentarioService.postarComentario(this.getUsuarioAutenticado(), tipoComentario, idObjetoAlvo, comentarioPai, texto, postarNoFacebook);
        if (comentarioPai != null) {
            model.addAttribute("comentario", comentarioPai);
            return "/comentarios/comentario";
        } else {
            this.getSearchData().setStart(0);
            model.addAttribute("comentarios", this.comentarioService.consultarComentarios(tipoComentario, idObjetoAlvo));
            model.addAttribute("tipo", tipoComentario);
            model.addAttribute("idAlvo", idObjetoAlvo);
            return "/comentarios/listaComentarios";
        }
    }
}
