package br.com.fanaticosporviagens.popup;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fanaticosporviagens.infra.controller.BaseController;

@Controller
@RequestMapping("/popup")
public class PopupController extends BaseController {

    @RequestMapping(value = "/popupBrowserIncompativel", method = { RequestMethod.GET })
    public String popupBrowserIncompativel(final Model model) {
        return "/popup/popupBrowserIncompativel";
    }

}
