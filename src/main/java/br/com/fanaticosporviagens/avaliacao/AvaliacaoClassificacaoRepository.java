package br.com.fanaticosporviagens.avaliacao;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.model.entity.AvaliacaoClassificacao;

/**
 * @author André Thiago
 * 
 */
@Repository
public class AvaliacaoClassificacaoRepository extends GenericCRUDRepository<AvaliacaoClassificacao, Long> {

    public void incluir(final List<AvaliacaoClassificacao> classificacoes) {
        for (final AvaliacaoClassificacao classificacao : classificacoes) {
            incluir(classificacao);
        }
    }

}
