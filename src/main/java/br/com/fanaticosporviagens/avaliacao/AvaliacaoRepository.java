package br.com.fanaticosporviagens.avaliacao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.fanaticosporviagens.infra.model.repository.GenericCRUDRepository;
import br.com.fanaticosporviagens.infra.model.repository.GenericSearchRepository;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.AvaliacaoConsolidadaPorCriterio;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;

/**
 * @author André Thiago
 * 
 */
@Repository
public class AvaliacaoRepository extends GenericCRUDRepository<Avaliacao, Long> {

    @Autowired
    private GenericSearchRepository searchRepository;

    public List<AvaliacaoConsolidadaPorCriterio> consultarAvaliacoesConsolidadasPorCriterio(final Local localAvaliado) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select avaliacaoConsolidada ");
        hql.append("  from AvaliacaoConsolidadaPorCriterio avaliacaoConsolidada ");
        hql.append(" where avaliacaoConsolidada.localAvaliado = :localAvaliado ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("localAvaliado", localAvaliado);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Avaliacao> consultarAvaliacoesConsolidadasPorUsuario(final Usuario usuario) {
        final StringBuilder hql = new StringBuilder();
        hql.append("SELECT avaliacao ");
        hql.append("  FROM Avaliacao avaliacao ");
        hql.append(" WHERE avaliacao.autor = :autor");
        hql.append("   AND avaliacao.dataExclusao IS NULL");
        hql.append(" AND avaliacao.dataPublicacao is not null ");
        hql.append(" ORDER BY avaliacao.dataCriacao DESC");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("autor", usuario);

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Avaliacao> consultarAvaliacoesLocalPorUsuario(final Usuario usuario, final Local local) {
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("usuario", usuario);
        params.put("local", local);
        return this.searchRepository.consultarPorNamedQuery("Avaliacao.consultarAvaliacoesLocalPorUsuario", params, Avaliacao.class);
    }

    public List<Avaliacao> consultarAvaliacoesMaisRecentes(final Local localAvaliado, final int tamanhoAmostra) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select avaliacao ");
        hql.append("  from Avaliacao avaliacao ");
        if (localAvaliado.isTipoLocalGeografico()) {
            hql.append(" where (   avaliacao.localAvaliado.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.continente.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.pais.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.estado.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.cidade.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.localInteresseTuristico.id = :localAvaliado ");
            hql.append("       ) ");
        } else {
            hql.append(" where avaliacao.localAvaliado.id = :localAvaliado ");
        }
        hql.append("   AND avaliacao.dataExclusao IS NULL");
        hql.append("   AND avaliacao.dataPublicacao is not null ");
        hql.append(" order by avaliacao.dataCriacao desc ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("localAvaliado", localAvaliado.getId());

        if (tamanhoAmostra >= 1) {
            return this.searchRepository.consultaHQL(hql.toString(), parametros, 0, tamanhoAmostra);
        }
        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public List<Avaliacao> consultarAvaliacoesPorLocalAvaliado(final Local localAvaliado) {
        final StringBuilder hql = new StringBuilder();
        hql.append("select avaliacao ");
        hql.append("  from Avaliacao avaliacao ");
        if (localAvaliado.isTipoLocalGeografico()) {
            hql.append(" where (   avaliacao.localAvaliado.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.continente.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.pais.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.estado.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.cidade.id = :localAvaliado ");
            hql.append("        or avaliacao.localAvaliado.localizacao.localInteresseTuristico.id = :localAvaliado ");
            hql.append("       ) ");
        } else {
            hql.append(" where avaliacao.localAvaliado.id = :localAvaliado ");
        }
        hql.append("   AND avaliacao.dataExclusao IS NULL");
        hql.append(" AND avaliacao.dataPublicacao is not null ");
        hql.append(" order by avaliacao.quantidadeVotoUtil desc, avaliacao.dataCriacao desc ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("localAvaliado", localAvaliado.getId());

        return this.searchRepository.consultaHQL(hql.toString(), parametros);
    }

    public void incrementarVotoUtil(final Avaliacao avaliacao) {

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("idAvaliacao", avaliacao.getId());

        this.searchRepository.executarSQL("update avaliacao set qtd_voto_util = qtd_voto_util + 1 where id_avaliacao = :idAvaliacao ", parametros);
    }

    public void tornarAvaliacoesNaoParticipantesConsolidacao(final Local localAvaliado, final Usuario autorAvaliacao) {
        final StringBuilder hql = new StringBuilder();
        hql.append("update Avaliacao ");
        hql.append("   set participaConsolidacao = false ");
        hql.append(" where participaConsolidacao = true ");
        hql.append("   and localAvaliado = :localAvaliado ");
        hql.append("   and autor = :autorAvaliacao ");

        final Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("localAvaliado", localAvaliado);
        parametros.put("autorAvaliacao", autorAvaliacao);

        this.searchRepository.executarHQL(hql.toString(), parametros);
    }
}
