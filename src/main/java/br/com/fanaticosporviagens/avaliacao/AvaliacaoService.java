package br.com.fanaticosporviagens.avaliacao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.sprockets.google.Place.Review;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.fanaticosporviagens.acaousuario.AcaoUsuarioService;
import br.com.fanaticosporviagens.foto.FotoService;
import br.com.fanaticosporviagens.gmail.service.GoogleService;
import br.com.fanaticosporviagens.infra.model.service.GenericCRUDService;
import br.com.fanaticosporviagens.infra.util.UrlPathGeneratorService;
import br.com.fanaticosporviagens.model.entity.AcaoUsuario;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.AvaliacaoConsolidadaPorCriterio;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.model.entity.Usuario;
import br.com.fanaticosporviagens.publicacao.PublicacaoService;

/**
 * @author André Thiago
 * 
 */
@Service
public class AvaliacaoService extends GenericCRUDService<Avaliacao, Long> {

    @Autowired
    private AcaoUsuarioService acaoUsuarioService;

    @Autowired
    private AvaliacaoClassificacaoRepository avaliacaoClassificacaoRepository;

    @Autowired
    private AvaliacaoRepository avaliacaoRepository;

    @Autowired
    private FotoService fotoService;

    @Autowired
    private GoogleService googleService;

    @Autowired
    private PublicacaoService publicacaoService;

    @Autowired
    private UrlPathGeneratorService urlGeneratorService;

    public List<AvaliacaoConsolidadaPorCriterio> consultarAvaliacoesConsolidadasPorCriterio(final Local localAvaliado) {
        return this.avaliacaoRepository.consultarAvaliacoesConsolidadasPorCriterio(localAvaliado);
    }

    public List<Avaliacao> consultarAvaliacoesConsolidadasPorUsuario(final Usuario usuario) {
        return this.avaliacaoRepository.consultarAvaliacoesConsolidadasPorUsuario(usuario);
    }

    public List<Review> consultarAvaliacoesGoogle(final Local local) {
        List<Review> reviews = this.googleService.consultarReviewsLocal(local);
        if (reviews == null) {
            reviews = new ArrayList<Review>();
        }
        return reviews;
    }

    public List<Avaliacao> consultarAvaliacoesMaisRecentes(final Local localAvaliado, final int tamanhoAmostra) {
        return this.avaliacaoRepository.consultarAvaliacoesMaisRecentes(localAvaliado, tamanhoAmostra);
    }

    public List<Avaliacao> consultarAvaliacoesPorLocalAvaliado(final Local localAvaliado) {
        return this.avaliacaoRepository.consultarAvaliacoesPorLocalAvaliado(localAvaliado);
    }

    @Override
    @Transactional
    @PreAuthorize("#avaliacao.autor.username == authentication.name")
    public void excluir(final Avaliacao avaliacao) {
        super.excluir(avaliacao);
    }

    @Override
    @Transactional
    // TODO ver se demos publicar logo de cara ou nao. Talvez tenha q aprovar primeiro
    public void incluir(final Avaliacao avaliacao) {
        this.incluir(avaliacao, true);
    }

    @Transactional
    // TODO ver se demos publicar logo de cara ou nao. Talvez tenha q aprovar primeiro
    public void incluir(final Avaliacao avaliacao, final boolean publicar) {
        // this.avaliacaoRepository.tornarAvaliacoesNaoParticipantesConsolidacao(avaliacao.getLocalAvaliado(), getUsuarioAutenticado());

        this.urlGeneratorService.generate(avaliacao);
        avaliacao.setAutor(getUsuarioAutenticado());
        avaliacao.setDataAvaliacao(new Date());
        avaliacao.ajustarQuantidadeFotos();
        avaliacao.ajustarItensBomPara();
        avaliacao.ajustarTiposViagem();
        avaliacao.setParticipaConsolidacao(Boolean.TRUE);
        if (publicar) {
            avaliacao.setDataPublicacao(new LocalDateTime());
        } else {
            avaliacao.setDataPublicacao(null);
        }
        avaliacao.getLocalAvaliado().calcularAvaliacaoGeral(avaliacao.getClassificacaoGeral());
        this.avaliacaoRepository.incluir(avaliacao);
        this.avaliacaoClassificacaoRepository.incluir(avaliacao.getAvaliacoesClassificacaoAplicaveis());

        if (avaliacao.possuiFotos()) {
            this.fotoService.incluirFotos(avaliacao);
        }
    }

    @Override
    public void preExclusao(final Avaliacao avaliacao) {
        if (avaliacao.getAtividade() != null) {
            avaliacao.getAtividade().setAvaliacao(null);
        }
        final AcaoUsuario acaoUsuario = this.acaoUsuarioService.consultarAcaoUsuario(getUsuarioAutenticado(), avaliacao);
        if (acaoUsuario != null) {
            this.acaoUsuarioService.excluir(acaoUsuario);
        }
    }

    @Transactional
    public void publicar(final Avaliacao avaliacao) {
        avaliacao.setDataPublicacao(new LocalDateTime());
        this.avaliacaoRepository.alterar(avaliacao);
    }

    public boolean usuarioNaoPodeAvaliar(final Usuario usuario, final Local local) {
        final List<Avaliacao> avaliacoes = this.avaliacaoRepository.consultarAvaliacoesLocalPorUsuario(usuario, local);
        final LocalDateTime trintaDiasAtras = LocalDateTime.now().minusDays(30);
        for (final Avaliacao avaliacao : avaliacoes) {
            if (avaliacao.getDataCriacao().isAfter(trintaDiasAtras)) {
                return true;
            }
        }
        return false;
    }
}
