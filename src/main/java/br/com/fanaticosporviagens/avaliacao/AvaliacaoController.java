package br.com.fanaticosporviagens.avaliacao;

import java.util.Collections;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.validation.Valid;

import net.sf.sprockets.google.Place.Review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.OperationNotPermittedException;
import org.springframework.social.RevokedAuthorizationException;
import org.springframework.social.UncategorizedApiException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fanaticosporviagens.facebook.service.FacebookService;
import br.com.fanaticosporviagens.infra.component.StatusMessage;
import br.com.fanaticosporviagens.infra.controller.BaseController;
import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.InteresseUsuarioEmLocalService;
import br.com.fanaticosporviagens.model.entity.AtividadePlano;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.AvaliacaoQualidade;
import br.com.fanaticosporviagens.model.entity.Local;
import br.com.fanaticosporviagens.tipo.TipoService;
import br.com.fanaticosporviagens.twitter.service.TwitterService;
import br.com.fanaticosporviagens.viagem.model.service.AtividadeService;

/**
 * @author André Thiago
 * 
 */
// @Controller
// @RequestMapping("/avaliacao")
public class AvaliacaoController extends BaseController {

    @Autowired
    private AtividadeService atividadeService;

    @Autowired
    private FacebookService facebookService;

    @Autowired
    private InteresseUsuarioEmLocalService interesseUsuarioEmLocalService;

    @Autowired
    private AvaliacaoService service;

    @Autowired
    private TipoService tipoService;

    @Autowired
    private TwitterService twitterService;

    @RequestMapping(value = "/avaliar/{local}", method = { RequestMethod.GET })
    @PreAuthorize("isAuthenticated()")
    public String avaliacao(@PathVariable final Local local, final Model model) {
        final Avaliacao avaliacao = new Avaliacao();
        if (!model.containsAttribute("avaliacao")) {
            model.addAttribute("avaliacao", avaliacao);
        }
        model.addAttribute("naoPodeAvaliar", this.service.usuarioNaoPodeAvaliar(getUsuarioAutenticado(), local));
        final String referer = this.request.getHeader("Referer");
        if (referer != null) {
            model.addAttribute("urlRedirect", referer);
        }
        model.addAttribute("anosVisita", getAnosVisita());
        model.addAttribute("local", local);
        model.addAttribute("criteriosAvaliacao", local.getTipoLocal().getAvaliacoesCriterios());
        model.addAttribute("itensBomPara", local.getTipoLocal().getAvaliacoesItensBomPara());
        model.addAttribute("tiposViagem", this.tipoService.tipoViagemConsultarAtivos());

        model.addAttribute("avaliacoesRecentes", this.service.consultarAvaliacoesMaisRecentes(local, 4));

        return "/avaliacao/avaliacao";
    }

    @RequestMapping(value = "/avaliacaoComSucesso")
    @PreAuthorize("isAuthenticated()")
    public String avaliacaoComSucesso(final Model model) {
        return "/avaliacao/avaliacaoComSucesso";
    }

    @RequestMapping(value = "/avaliar/incluir", method = { RequestMethod.POST })
    @PreAuthorize("isAuthenticated()")
    public String avaliarLocal(@Valid @ModelAttribute("avaliacao") final Avaliacao avaliacao, final BindingResult result, final Model model,
            final RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "/avaliacao/avaliar";
        }
        final Local localAvaliado = avaliacao.getLocalAvaliado();
        if (localAvaliado.getValidado()) {
            this.service.incluir(avaliacao);
            try {
                this.interesseUsuarioEmLocalService.publicarInteresseJaFoi(avaliacao.getAutor(), avaliacao.getLocalAvaliado());
                final List<Local> locais = avaliacao.getLocalAvaliado().getTodosPais();
                for (final Local local : locais) {
                    try {
                        this.interesseUsuarioEmLocalService.publicarInteresseJaFoi(avaliacao.getAutor(), local);
                    } catch (final PersistenceException e) {
                        // geralmente é erro de violação de UK; o local já foi marcado anteriomente
                        continue;
                    }
                }
            } catch (final PersistenceException e) {
                // geralmente é erro de violação de UK; o local já foi marcado anteriomente
            }
            redirectAttributes.addFlashAttribute("avaliacao", avaliacao);
            redirectAttributes.addFlashAttribute("local", localAvaliado);
            redirectAttributes.addFlashAttribute("promocaoTabletValida", promocaoTabletValida());
            return "redirect:/avaliacao/avaliacaoComSucesso";
        } else {
            this.service.incluir(avaliacao, false);
            success("Avaliação incluída com sucesso. Ela será exibida assim que este local for validado pela Equipe do TripFans.");
            return "redirect:/perfil/" + getUsuarioAutenticado().getUrlPath();
        }
    }

    @RequestMapping(value = "/compartilharFacebook/{avaliacao}", method = { RequestMethod.GET })
    public @ResponseBody StatusMessage compartilharFacebook(@PathVariable final Avaliacao avaliacao, final Model model) {
        try {
            this.facebookService.postarNoMural(avaliacao);
        } catch (final OperationNotPermittedException e) {
            return error("Não foi possível compartilhar a dica.");
        } catch (final RevokedAuthorizationException e) {
            return error("Não é possível compartilhar a dica no Facebook, pois o TripFans teve a autorização de compartilhamento revogado"
                    + " (isso aconteceu OU porque você OU o Facebook removeu esta autorização).");
        } catch (final UncategorizedApiException e) {
            return error("Houve um erro ao compartilhar a dica no Facebook. Tente novamente mais tarde.");
        } catch (final Exception e) {
            return error("Houve um erro ao compartilhar a dica no Facebook. Tente novamente mais tarde.");
        }
        return success("Avaliação compartilhada no Facebook com sucesso.");
    }

    @RequestMapping(value = "/compartilharTwitter/{avaliacao}", method = { RequestMethod.GET })
    public @ResponseBody StatusMessage compartilharTwitter(@PathVariable final Avaliacao avaliacao, final Model model) {
        try {
            this.twitterService.postarTweet(avaliacao);
        } catch (final Exception e) {
            return error("Não foi possível compartilhar a avaliação.");
        }
        return success("Avaliação compartilhada no Twitter com sucesso.");
    }

    @RequestMapping(value = "/excluir/{avaliacao}")
    @PreAuthorize("isAuthenticated()")
    @ResponseBody
    public JSONResponse excluirAvaliacao(@PathVariable final Avaliacao avaliacao, final Model model) {
        if (avaliacao != null) {
            this.service.excluir(avaliacao);
        }
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/exibirCard/{avaliacao}", method = { RequestMethod.GET })
    public String exibirCard(@PathVariable final Avaliacao avaliacao, final Model model) {
        model.addAttribute("avaliacao", avaliacao);
        return "/avaliacao/exibirCardAvaliacao";
    }

    @ModelAttribute("avaliacoesQualidade")
    public AvaliacaoQualidade[] getAvaliacoesQualidade() {
        return new AvaliacaoQualidade[] { AvaliacaoQualidade.PESSIMO, AvaliacaoQualidade.RUIM, AvaliacaoQualidade.REGULAR, AvaliacaoQualidade.BOM,
                AvaliacaoQualidade.EXCELENTE };
    }

    @RequestMapping(value = "/atividade/{atividade}/incluir", method = { RequestMethod.POST })
    public @ResponseBody JSONResponse incluirAvaliacaoAtividade(@Valid @ModelAttribute("avaliacao") final Avaliacao avaliacao,
            @PathVariable final AtividadePlano atividadePlano, final Model model) {
        avaliacao.setAutor(getUsuarioAutenticado());
        atividadePlano.setAvaliacao(avaliacao);
        this.service.incluir(avaliacao, false);
        // this.atividadeService.salvar(atividade);
        this.jsonResponse.addParam("id", avaliacao.getId());
        return this.jsonResponse.success();
    }

    @RequestMapping(value = "/listar/{localAvaliado}", method = { RequestMethod.GET })
    public String listarAvaliacoes(@PathVariable final Local localAvaliado,
            @RequestParam(value = "item", required = false) final Avaliacao avaliacaoDestaque, final Model model) {
        List<Avaliacao> avaliacoes = Collections.emptyList();
        if (localAvaliado.getQuantidadeAvaliacoes() >= 1) {
            avaliacoes = this.service.consultarAvaliacoesPorLocalAvaliado(localAvaliado);
        }
        model.addAttribute("avaliacaoDestaque", avaliacaoDestaque);
        model.addAttribute("avaliacoes", avaliacoes);
        model.addAttribute("local", localAvaliado);
        if ((this.getSearchData().getStart() != null && this.getSearchData().getStart() == 0) && !this.getSearchData().isOrderDataValid()) {
            return "/avaliacao/avaliacoesLocal";
        } else {
            return "/avaliacao/listaAvaliacoes";
        }
    }

    @RequestMapping(value = "/google/listar/{local}", method = { RequestMethod.GET })
    public String listarAvaliacoesGoogle(@PathVariable final Local local,
            @RequestParam(value = "item", required = false) final Avaliacao avaliacaoDestaque, final Model model) {
        final List<Review> avaliacoesGoogle = this.service.consultarAvaliacoesGoogle(local);
        model.addAttribute("avaliacoesGoogle", avaliacoesGoogle);
        return "/avaliacao/listaAvaliacoesGoogle";
    }

    @RequestMapping(value = "/oQueAvaliar")
    @PreAuthorize("isAuthenticated()")
    public String oQueAvaliar() {
        return "/avaliacao/oQueAvaliar";
    }

}
