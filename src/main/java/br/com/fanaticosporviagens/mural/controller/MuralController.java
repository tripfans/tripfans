package br.com.fanaticosporviagens.mural.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.fanaticosporviagens.infra.controller.BaseController;

/**
 * @author André Thiago
 * 
 */
@Controller
@RequestMapping("/mural")
public class MuralController extends BaseController {

    @RequestMapping(value = "/mural/{idLocal}", method = { RequestMethod.GET })
    public String mural(@PathVariable final Long idLocal, final Model model) {
        return "/mural/mural";
    }

}
