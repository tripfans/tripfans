package br.com.tripfans.importador;

import java.time.LocalTime;

public class HotelTripFans {

    public Integer categoriaExpedia;

    public String codigoAeroporto;

    public String codigoEstadoProvincia;

    public String codigoPais;

    public String codigoPostal;

    public String descricao;

    public String descricaoEN;

    public String descricaoES;

    public String descricaoFR;

    public String descricaoPT;

    public String endereco;

    public Integer estrelas;

    public LocalTime horarioCheckin;

    public LocalTime horarioCheckout;

    public Long id;

    public Long idBooking;

    public String idCidadeBooking;

    public String idContinenteBooking;

    public Long idExpedia;

    public Long idRegiaoExpedia;

    public Long idRegiaoTripfans;

    public Double latitude;

    public Double longitude;

    public String moeda;

    public String nome;

    public String nomeCidadeBooking;

    public String nomeCidadePreferenciaBooking;

    public String nomeUnicoCidadeBooking;

    public Integer numeroAvaliacoesBooking;

    public Integer numeroQuartosBooking;

    public String observacao;

    public Double pontuacaoAvaliacoesBooking;

    public Double precoMaximoBooking;

    public Double precoMinimoBooking;

    public boolean preferencialBooking;

    public Double rankingBooking;

    public String tituloFotoExpedia;

    public String urlBooking;

    public String urlExpedia;

    public String urlFotoPadraoBooking;

    public String urlFotoPadraoExpedia;

    @Override
    public boolean equals(final Object hotel) {
        if (hotel == null || !(hotel instanceof HotelTripFans)) {
            return false;
        }
        return ((HotelTripFans) hotel).id.equals(this.id);
    }

    public Integer getCategoriaExpedia() {
        return this.categoriaExpedia;
    }

    public String getCodigoAeroporto() {
        return this.codigoAeroporto;
    }

    public String getCodigoEstadoProvincia() {
        return this.codigoEstadoProvincia;
    }

    public String getCodigoPais() {
        return this.codigoPais;
    }

    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getDescricaoEN() {
        return this.descricaoEN;
    }

    public String getDescricaoES() {
        return this.descricaoES;
    }

    public String getDescricaoFR() {
        return this.descricaoFR;
    }

    public String getDescricaoPT() {
        return this.descricaoPT;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public Integer getEstrelas() {
        return this.estrelas;
    }

    public LocalTime getHorarioCheckin() {
        return this.horarioCheckin;
    }

    public LocalTime getHorarioCheckout() {
        return this.horarioCheckout;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdBooking() {
        return this.idBooking;
    }

    public String getIdCidadeBooking() {
        return this.idCidadeBooking;
    }

    public String getIdContinenteBooking() {
        return this.idContinenteBooking;
    }

    public Long getIdExpedia() {
        return this.idExpedia;
    }

    public Long getIdRegiaoExpedia() {
        return this.idRegiaoExpedia;
    }

    public Long getIdRegiaoTripfans() {
        return this.idRegiaoTripfans;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getMoeda() {
        return this.moeda;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeCidadeBooking() {
        return this.nomeCidadeBooking;
    }

    public String getNomeCidadePreferenciaBooking() {
        return this.nomeCidadePreferenciaBooking;
    }

    public String getNomeUnicoCidadeBooking() {
        return this.nomeUnicoCidadeBooking;
    }

    public Integer getNumeroAvaliacoesBooking() {
        return this.numeroAvaliacoesBooking;
    }

    public Integer getNumeroQuartosBooking() {
        return this.numeroQuartosBooking;
    }

    public String getObservacao() {
        return this.observacao;
    }

    public Double getPontuacaoAvaliacoesBooking() {
        return this.pontuacaoAvaliacoesBooking;
    }

    public Double getPrecoMaximoBooking() {
        return this.precoMaximoBooking;
    }

    public Double getPrecoMinimoBooking() {
        return this.precoMinimoBooking;
    }

    public Double getRankingBooking() {
        return this.rankingBooking;
    }

    public String getTituloFotoExpedia() {
        return this.tituloFotoExpedia;
    }

    public String getUrlBooking() {
        return this.urlBooking;
    }

    public String getUrlExpedia() {
        return this.urlExpedia;
    }

    public String getUrlFotoPadraoBooking() {
        return this.urlFotoPadraoBooking;
    }

    public String getUrlFotoPadraoExpedia() {
        return this.urlFotoPadraoExpedia;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    public boolean isPreferencialBooking() {
        return this.preferencialBooking;
    }

    public void setCategoriaExpedia(final Integer categoriaExpedia) {
        this.categoriaExpedia = categoriaExpedia;
    }

    public void setCodigoAeroporto(final String codigoAeroporto) {
        this.codigoAeroporto = codigoAeroporto;
    }

    public void setCodigoEstadoProvincia(final String codigoEstadoProvincia) {
        this.codigoEstadoProvincia = codigoEstadoProvincia;
    }

    public void setCodigoPais(final String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public void setCodigoPostal(final String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDescricaoEN(final String descricaoEN) {
        this.descricaoEN = descricaoEN;
    }

    public void setDescricaoES(final String descricaoES) {
        this.descricaoES = descricaoES;
    }

    public void setDescricaoFR(final String descricaoFR) {
        this.descricaoFR = descricaoFR;
    }

    public void setDescricaoPT(final String descricaoPT) {
        this.descricaoPT = descricaoPT;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public void setEstrelas(final Integer estrelas) {
        this.estrelas = estrelas;
    }

    public void setHorarioCheckin(final LocalTime horarioCheckin) {
        this.horarioCheckin = horarioCheckin;
    }

    public void setHorarioCheckout(final LocalTime horarioCheckout) {
        this.horarioCheckout = horarioCheckout;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setIdBooking(final Long idBooking) {
        this.idBooking = idBooking;
    }

    public void setIdCidadeBooking(final String idCidadeBooking) {
        this.idCidadeBooking = idCidadeBooking;
    }

    public void setIdContinenteBooking(final String idContinenteBooking) {
        this.idContinenteBooking = idContinenteBooking;
    }

    public void setIdExpedia(final Long idExpedia) {
        this.idExpedia = idExpedia;
    }

    public void setIdRegiaoExpedia(final Long idRegiaoExpedia) {
        this.idRegiaoExpedia = idRegiaoExpedia;
    }

    public void setIdRegiaoTripfans(final Long idRegiaoTripfans) {
        this.idRegiaoTripfans = idRegiaoTripfans;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setMoeda(final String moeda) {
        this.moeda = moeda;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeCidadeBooking(final String nomeCidadeBooking) {
        this.nomeCidadeBooking = nomeCidadeBooking;
    }

    public void setNomeCidadePreferenciaBooking(final String nomeCidadePreferenciaBooking) {
        this.nomeCidadePreferenciaBooking = nomeCidadePreferenciaBooking;
    }

    public void setNomeUnicoCidadeBooking(final String nomeUnicoCidadeBooking) {
        this.nomeUnicoCidadeBooking = nomeUnicoCidadeBooking;
    }

    public void setNumeroAvaliacoesBooking(final Integer numeroAvaliacoesBooking) {
        this.numeroAvaliacoesBooking = numeroAvaliacoesBooking;
    }

    public void setNumeroQuartosBooking(final Integer numeroQuartosBooking) {
        this.numeroQuartosBooking = numeroQuartosBooking;
    }

    public void setObservacao(final String observacao) {
        this.observacao = observacao;
    }

    public void setPontuacaoAvaliacoesBooking(final Double pontuacaoAvaliacoesBooking) {
        this.pontuacaoAvaliacoesBooking = pontuacaoAvaliacoesBooking;
    }

    public void setPrecoMaximoBooking(final Double precoMaximoBooking) {
        this.precoMaximoBooking = precoMaximoBooking;
    }

    public void setPrecoMinimoBooking(final Double precoMinimoBooking) {
        this.precoMinimoBooking = precoMinimoBooking;
    }

    public void setPreferencialBooking(final boolean preferencialBooking) {
        this.preferencialBooking = preferencialBooking;
    }

    public void setRankingBooking(final Double rankingBooking) {
        this.rankingBooking = rankingBooking;
    }

    public void setTituloFotoExpedia(final String tituloFotoExpedia) {
        this.tituloFotoExpedia = tituloFotoExpedia;
    }

    public void setUrlBooking(final String urlBooking) {
        this.urlBooking = urlBooking;
    }

    public void setUrlExpedia(final String urlExpedia) {
        this.urlExpedia = urlExpedia;
    }

    public void setUrlFotoPadraoBooking(final String urlFotoPadraoBooking) {
        this.urlFotoPadraoBooking = urlFotoPadraoBooking;
    }

    public void setUrlFotoPadraoExpedia(final String urlFotoPadraoExpedia) {
        this.urlFotoPadraoExpedia = urlFotoPadraoExpedia;
    }

    @Override
    public String toString() {
        return "HotelTripFans [" + (this.id != null ? "id=" + this.id + ", " : "") + (this.nome != null ? "nome=" + this.nome + ", " : "")
                + (this.endereco != null ? "endereco=" + this.endereco + ", " : "")
                + (this.latitude != null ? "latitude=" + this.latitude + ", " : "")
                + (this.longitude != null ? "longitude=" + this.longitude + ", " : "")
                + (this.idExpedia != null ? "idExpedia=" + this.idExpedia + ", " : "")
                + (this.idBooking != null ? "idBooking=" + this.idBooking + ", " : "")
                + (this.descricao != null ? "descricao=" + this.descricao + ", " : "")
                + (this.estrelas != null ? "estrelas=" + this.estrelas + ", " : "")
                + (this.urlBooking != null ? "urlBooking=" + this.urlBooking : "") + "]";
    }

}
