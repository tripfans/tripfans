package br.com.tripfans.importador.ean;

public class HotelEAN {

    public Integer alturaFotoPadrao;

    public Integer categoriaExpedia;

    public String codigoAeroporto;

    public String codigoEstadoProvincia;

    public String codigoPais;

    public String codigoPostal;

    public String descricao;

    public String descricaoEN;

    public String descricaoES;

    public String descricaoFR;

    private String descricaoLocalizacao;

    public String descricaoPT;

    public String endereco;

    public Integer estrelas;

    public String horarioCheckin;

    public String horarioCheckout;

    public Long hotelId;

    public Long idExpedia;

    public Long idRegiaoExpedia;

    public Long idRegiaoTripfans;

    public Integer larguraFotoPadrao;

    public Double latitude;

    public Double longitude;

    public String moeda;

    public String nome;

    public String observacao;

    public String tituloFotoPadrao;

    public String urlExpedia;

    /**
     * Tamanhos das fotos:
     *
     * - Standard
     * s (S): 200 x 162
     * b (M): 350 x 284
     * z (XL): 1000 x 812
     *
     * - Wide
     * e (wide XS): 160 x 90
     * l (wide S): 255 x 144
     *
     * - Quadrada
     * t: 70 x 70
     * n: 90 x 90
     * g: 140 x 140
     * d: 180 x 180
     */
    public String urlFotoPadrao;

    public Integer getAlturaFotoPadrao() {
        return this.alturaFotoPadrao;
    }

    public Integer getCategoriaExpedia() {
        return this.categoriaExpedia;
    }

    public String getCodigoAeroporto() {
        return this.codigoAeroporto;
    }

    public String getCodigoEstadoProvincia() {
        return this.codigoEstadoProvincia;
    }

    public String getCodigoPais() {
        return this.codigoPais;
    }

    public String getCodigoPostal() {
        return this.codigoPostal;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public String getDescricaoEN() {
        return this.descricaoEN;
    }

    public String getDescricaoES() {
        return this.descricaoES;
    }

    public String getDescricaoFR() {
        return this.descricaoFR;
    }

    public String getDescricaoLocalizacao() {
        return this.descricaoLocalizacao;
    }

    public String getDescricaoPT() {
        return this.descricaoPT;
    }

    public String getEndereco() {
        return this.endereco;
    }

    public Integer getEstrelas() {
        return this.estrelas;
    }

    public String getHorarioCheckin() {
        return this.horarioCheckin;
    }

    public String getHorarioCheckout() {
        return this.horarioCheckout;
    }

    public Long getHotelId() {
        return this.hotelId;
    }

    public Long getIdExpedia() {
        return this.idExpedia;
    }

    public Long getIdRegiaoExpedia() {
        return this.idRegiaoExpedia;
    }

    public Long getIdRegiaoTripfans() {
        return this.idRegiaoTripfans;
    }

    public Integer getLarguraFotoPadrao() {
        return this.larguraFotoPadrao;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getMoeda() {
        return this.moeda;
    }

    public String getNome() {
        return this.nome;
    }

    public String getObservacao() {
        return this.observacao;
    }

    public String getTituloFotoPadrao() {
        return this.tituloFotoPadrao;
    }

    public String getUrlExpedia() {
        return this.urlExpedia;
    }

    public String getUrlFotoPadrao() {
        return this.urlFotoPadrao;
    }

    public void setAlturaFotoPadrao(final Integer alturaFotoPadrao) {
        this.alturaFotoPadrao = alturaFotoPadrao;
    }

    public void setCategoriaExpedia(final Integer categoriaExpedia) {
        this.categoriaExpedia = categoriaExpedia;
    }

    public void setCodigoAeroporto(final String codigoAeroporto) {
        this.codigoAeroporto = codigoAeroporto;
    }

    public void setCodigoEstadoProvincia(final String codigoEstadoProvincia) {
        this.codigoEstadoProvincia = codigoEstadoProvincia;
    }

    public void setCodigoPais(final String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public void setCodigoPostal(final String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public void setDescricao(final String descricao) {
        this.descricao = descricao;
    }

    public void setDescricaoEN(final String descricaoEN) {
        this.descricaoEN = descricaoEN;
    }

    public void setDescricaoES(final String descricaoES) {
        this.descricaoES = descricaoES;
    }

    public void setDescricaoFR(final String descricaoFR) {
        this.descricaoFR = descricaoFR;
    }

    public void setDescricaoLocalizacao(final String descricaoLocalizacao) {
        this.descricaoLocalizacao = descricaoLocalizacao;
    }

    public void setDescricaoPT(final String descricaoPT) {
        this.descricaoPT = descricaoPT;
    }

    public void setEndereco(final String endereco) {
        this.endereco = endereco;
    }

    public void setEstrelas(final Integer estrelas) {
        this.estrelas = estrelas;
    }

    public void setHorarioCheckin(final String horarioCheckin) {
        this.horarioCheckin = horarioCheckin;
    }

    public void setHorarioCheckout(final String horarioCheckout) {
        this.horarioCheckout = horarioCheckout;
    }

    public void setHotelId(final Long hotelId) {
        this.hotelId = hotelId;
    }

    public void setIdExpedia(final Long idExpedia) {
        this.idExpedia = idExpedia;
    }

    public void setIdRegiaoExpedia(final Long idRegiaoExpedia) {
        this.idRegiaoExpedia = idRegiaoExpedia;
    }

    public void setIdRegiaoTripfans(final Long idRegiaoTripfans) {
        this.idRegiaoTripfans = idRegiaoTripfans;
    }

    public void setLarguraFotoPadrao(final Integer larguraFotoPadrao) {
        this.larguraFotoPadrao = larguraFotoPadrao;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setMoeda(final String moeda) {
        this.moeda = moeda;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setObservacao(final String observacao) {
        this.observacao = observacao;
    }

    public void setTituloFotoPadrao(final String tituloFotoPadrao) {
        this.tituloFotoPadrao = tituloFotoPadrao;
    }

    public void setUrlExpedia(final String urlExpedia) {
        this.urlExpedia = urlExpedia;
    }

    public void setUrlFotoPadrao(final String urlFotoPadrao) {
        this.urlFotoPadrao = urlFotoPadrao;
    }

    @Override
    public String toString() {
        return "HotelEAN [" + (this.hotelId != null ? "hotelID=" + this.hotelId + ", " : "") + (this.nome != null ? "nome=" + this.nome + ", " : "")
                + (this.endereco != null ? "endereco=" + this.endereco + ", " : "")
                + (this.latitude != null ? "latitude=" + this.latitude + ", " : "")
                + (this.longitude != null ? "longitude=" + this.longitude + ", " : "")
                + (this.descricao != null ? "descricao=" + this.descricao + ", " : "") + (this.estrelas != null ? "estrelas=" + this.estrelas : "")
                + "]";
    }

}
