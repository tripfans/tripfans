package br.com.tripfans.importador.ean;

import java.lang.reflect.InvocationTargetException;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.tripfans.importador.HotelTripFans;

@Component
public class ImportadorHoteisEAN {

    public static void main(final String[] args) {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(ImportadorEANSpringConfig.class);
        final ImportadorHoteisEAN importador = ctx.getBean(ImportadorHoteisEAN.class);

        importador.importa();

        System.err.println("FIM.");
    }

    @Autowired
    private ImportadorEANRepository repository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    public void importa() {
        int start = 0;
        final int limit = 1000;

        final long total = this.repository.consultaTotalHoteisEAN();

        // Passo 1: consulta os hoteis da base EAN
        List<HotelEAN> hoteis = this.repository.consultaHoteisEAN(start, limit);

        System.out.println("Iniciando importação ...");

        final Instant inicio = Instant.now();

        Instant fim = null;
        Instant fimParcial = null;
        Duration timeElapsed = null;
        Duration timeElapsedMedio = null;
        Integer tempoEstimado = null;

        double percentual = 0.0;
        final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(1);
        nf.setMinimumFractionDigits(1);

        while (CollectionUtils.isNotEmpty(hoteis)) {

            // tempo restante = media de execucacao a cada mil / total

            // Passo 2: pega os resultados e grava na base do TripFans
            insereBaseTripFans(hoteis);

            // percentual executado = limit / total
            if (start > 0) {

                if (!nf.format(percentual).equals(nf.format(new Double(start) / total))) {
                    if (fimParcial == null) {
                        fimParcial = Instant.now();
                        timeElapsedMedio = Duration.between(inicio, fimParcial);
                        System.err.println("Tempo medio: " + timeElapsedMedio.toMillis() / 1000.0 + " segundos");
                    }
                    percentual = (new Double(start) / total) * 100;

                    System.out.println("Total de hoteis importados: " + start + " de um total de " + total + " - " + (nf.format(percentual) + "%"));
                    fim = Instant.now();

                    timeElapsed = Duration.between(inicio, fim);
                    System.out.println("Tempo decorrido: " + timeElapsed.toMinutes() + " minuto(s)");

                    if (tempoEstimado == null) {
                        tempoEstimado = (int) (((total) / limit) * ((timeElapsedMedio.toMillis() / 1000.0))) / 60;
                    }

                    System.err.println("Tempo estimado: " + tempoEstimado + " minutos");

                }
            }

            start += limit;
            hoteis = this.repository.consultaHoteisEAN(start, limit);
        }

        // Passo Final: ao fim de tudo, faz um update da data da última atualização

    }

    private void insereBaseTripFans(final List<HotelEAN> hoteis) {

        final List<HotelTripFans> hoteisTripFans = new ArrayList<>();
        hoteis.parallelStream().forEach((hotelEan) -> {

            final HotelTripFans newHotel = nova(hotelEan);
            if (newHotel != null) {
                hoteisTripFans.add(newHotel);
            }
            // System.out.println(hotelEan);
            });

        this.transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(final TransactionStatus status) {
                ImportadorHoteisEAN.this.repository.incluirHoteis(hoteisTripFans);
                return null;
            }
        });

    }

    private HotelTripFans nova(final HotelEAN hotelEan) {
        HotelTripFans hotel = null;

        try {
            final Long idRegiaoTripfans = ImportadorHoteisEAN.this.repository.recuperarIdRegiaoTripfans(hotelEan.getIdRegiaoExpedia());

            hotel = new HotelTripFans();

            hotel.setIdRegiaoTripfans(idRegiaoTripfans);

            BeanUtils.copyProperties(hotel, hotelEan);

            try {
                hotel.setHorarioCheckin(parseTime(hotelEan.horarioCheckin));
            } catch (final Exception e) {
                hotel.setObservacao(" * Horario Checkin: " + hotelEan.horarioCheckin);
            }
            try {
                hotel.setHorarioCheckout(parseTime(hotelEan.horarioCheckout));
            } catch (final Exception e) {
                hotel.setObservacao(" * Horario Checkout: " + hotelEan.horarioCheckout);
            }

            hotel.idExpedia = hotelEan.getHotelId();
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        return hotel;
    }

    private LocalTime parseTime(String timeString) {
        LocalTime time = null;
        if (timeString != null && !timeString.isEmpty()) {
            if ("noon".equalsIgnoreCase(timeString)) {
                return LocalTime.NOON;
            } else if ("midnight".equalsIgnoreCase(timeString)) {
                time = LocalTime.MIDNIGHT;
            } else {
                timeString = timeString.toUpperCase().replaceAll("\\s+", "").replaceAll("\\.", "").replace("HRS", "");
                // Os formatos comuns foram: H PM OU H:MM AM
                if (timeString.contains(":")) {
                    if (timeString.lastIndexOf(":") > 2 || !timeString.contains("M")) {
                        time = LocalTime.parse(timeString);
                    } else {
                        time = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("h:mma"));
                    }
                } else {
                    if (timeString.contains("M")) {
                        time = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("ha"));
                    } else {
                        time = LocalTime.parse(timeString, DateTimeFormatter.ofPattern("Hmm"));
                    }
                }
            }
        }
        // Evitando erro ao inserir no Banco - Estava tentando inserir '-infinity'
        if (LocalTime.MIDNIGHT.equals(time)) {
            return LocalTime.of(23, 59);
        }
        return time;
    }

}
