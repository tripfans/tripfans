package br.com.tripfans.importador.ean;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.support.TransactionTemplate;

@Configuration
@PropertySource(value = { "classpath:application.properties", "classpath:eandb.properties" })
@ComponentScan(basePackages = "br.com.tripfans.importador")
@EnableAsync
public class ImportadorEANSpringConfig {

    @Autowired
    private Environment environment;

    @Bean
    public DataSource eanDataSource() {
        final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(this.environment.getProperty("eandb.url"));
        dataSource.setUsername(this.environment.getProperty("eandb.username"));
        dataSource.setPassword(this.environment.getProperty("eandb.password"));
        dataSource.setDriverClassName(this.environment.getProperty("eandb.driver"));
        return dataSource;
    }

    @Bean
    @Qualifier("namedParameterJdbcTemplateEAN")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplateEAN() {
        return new NamedParameterJdbcTemplate(eanDataSource());
    }

    @Bean
    @Qualifier("namedParameterJdbcTemplateTripFans")
    public NamedParameterJdbcTemplate namedParameterJdbcTemplateTripFans() {
        return new NamedParameterJdbcTemplate(tripfansDataSource());
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(tripfansDataSource());
    }

    @Bean
    public TransactionTemplate transactionTemplate() {
        return new TransactionTemplate(transactionManager());
    }

    @Bean
    public DataSource tripfansDataSource() {
        final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(this.environment.getProperty("db.driver"));
        dataSource.setUrl(this.environment.getProperty("db.url"));
        dataSource.setUsername(this.environment.getProperty("db.username"));
        dataSource.setPassword(this.environment.getProperty("db.password"));
        return dataSource;
    }

}
