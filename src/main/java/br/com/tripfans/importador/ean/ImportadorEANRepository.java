package br.com.tripfans.importador.ean;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Repository;

import br.com.tripfans.importador.HotelTripFans;
import br.com.tripfans.importador.RegiaoTripFans;

@Repository
public class ImportadorEANRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateEAN;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateTripFans;

    public void atualizarPaiRegioes() {

        StringBuffer update1 = null;
        final Map<String, Object> params1 = new HashMap<String, Object>();
        StringBuffer update2 = null;
        final Map<String, Object> params2 = new HashMap<String, Object>();

        for (int tipo = 2; tipo < 8; tipo++) {

            // tipo: 3 até 8
            update1 = new StringBuffer();
            update1.append("UPDATE tripfans_regiao tr ");
            update1.append("   SET sigla_iso_pais = pai.sigla_iso_pais ");
            update1.append("  FROM tripfans_regiao pai ");
            update1.append(" INNER JOIN tripfans_regiao filho ");
            update1.append("         ON pai.id_expedia = filho.id_pai_expedia ");
            update1.append("        AND filho.tipo_expedia = :tipoFilho ");
            update1.append(" WHERE tr.id_regiao_tripfans = filho.id_regiao_tripfans ");

            params1.put("tipoFilho", tipo + 1);
            this.namedParameterJdbcTemplateTripFans.update(update1.toString(), params1);

            // tipo: 2 até 6
            if (tipo <= 6) {
                update2 = new StringBuffer();
                update2.append("UPDATE tripfans_regiao tr ");
                update2.append("   SET sigla_iso_pais = pai.sigla_iso_pais ");
                update2.append("  FROM tripfans_regiao pai ");
                update2.append(" INNER JOIN tripfans_regiao filho ");
                update2.append("         ON on filho.id_pai_expedia = pai.id_expedia ");
                update2.append("        and pai.tipo_expedia = :tipoPai ");
                update2.append(" WHERE tr.id_regiao_tripfans = filho.id_regiao_tripfans ");

                params2.put("tipoPai", tipo++);

                this.namedParameterJdbcTemplateTripFans.update(update2.toString(), params2);
            }

        }

    }

    public List<HotelEAN> consultaHoteisEAN(final int start, final int limit) {

        final StringBuffer sql = new StringBuffer();

        sql.append(" SELECT ");
        sql.append("        h.EANHotelID id, ");
        sql.append("        hbr.Name nome,");
        sql.append("        h.name, ");
        sql.append("        h.address1 endereco, ");
        sql.append("        h.StateProvince codigoEstadoProvincia, ");
        sql.append("        h.PostalCode codigoPostal, ");
        sql.append("        h.Country codigoPais, ");
        sql.append("        h.latitude, ");
        sql.append("        h.longitude, ");
        sql.append("        h.AirportCode codigoAeroporto, ");

        sql.append("        h.PropertyCategory categoriaExpedia, ");
        sql.append("        h.PropertyCurrency moeda, ");
        sql.append("        h.starrating estrelas, ");

        sql.append("        h.Location descricaoLocalizacao, ");
        sql.append("        h.RegionID idRegiaoExpedia, ");
        sql.append("        h.CheckInTime checkInTime, ");
        sql.append("        h.CheckOutTime checkOutTime, ");

        sql.append("        pdl.PropertyDescription descricao, ");
        sql.append("        pdl.PropertyDescription descricaoEN, ");
        sql.append("        pdlpt.PropertyDescription descricaoPT, ");

        sql.append("        hil.Caption tituloFotoPadrao, ");
        sql.append("        hil.URL urlFotoPadrao, ");
        sql.append("        hil.Width larguraFotoPadrao, ");
        sql.append("        hil.Height alturaFotoPadrao ");

        sql.append("   FROM ");
        sql.append("        eanprod.activepropertylist h ");
        sql.append("            LEFT JOIN ");
        sql.append("        eanprod.propertydescriptionlist pdl ON h.EANHotelID = pdl.EANHotelID ");
        sql.append("            LEFT JOIN ");
        sql.append("        eanprod.propertydescriptionlist_pt_br pdlpt ON h.EANHotelID = pdlpt.EANHotelID ");
        sql.append("            LEFT JOIN ");
        sql.append("        eanprod.activepropertylist_pt_br hbr ON h.EANHotelID = hbr.EANHotelID");
        sql.append("            LEFT JOIN ");
        sql.append("        eanprod.hotelimagelist hil ON h.EANHotelID = hil.EANHotelID");

        sql.append("   ORDER BY h.EANHotelID");
        sql.append("   LIMIT %d, %d");

        return this.namedParameterJdbcTemplateEAN.query(String.format(sql.toString(), start, limit), new RowMapper<HotelEAN>() {

            @Override
            public HotelEAN mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final HotelEAN hotel = new HotelEAN();
                hotel.setNome(rs.getString("nome"));
                if (hotel.getNome() == null) {
                    hotel.setNome(rs.getString("name"));
                }
                hotel.setDescricao(rs.getString("descricao"));
                hotel.setDescricaoEN(rs.getString("descricaoEN"));
                hotel.setDescricaoPT(rs.getString("descricaoPT"));
                hotel.setEndereco(rs.getString("endereco"));
                hotel.setEstrelas(rs.getInt("estrelas"));
                hotel.setHotelId(rs.getLong("id"));
                hotel.setLatitude(rs.getDouble("latitude"));
                hotel.setLongitude(rs.getDouble("longitude"));

                hotel.setCodigoEstadoProvincia(rs.getString("codigoEstadoProvincia"));
                hotel.setCodigoPostal(rs.getString("codigoPostal"));
                hotel.setCodigoPais(rs.getString("codigoPais"));
                hotel.setCodigoAeroporto(rs.getString("codigoAeroporto"));
                hotel.setCategoriaExpedia(rs.getInt("categoriaExpedia"));
                hotel.setMoeda(rs.getString("moeda"));

                hotel.setDescricaoLocalizacao(rs.getString("descricaoLocalizacao"));
                hotel.setIdRegiaoExpedia(rs.getLong("idRegiaoExpedia"));
                hotel.setHorarioCheckin(rs.getString("checkInTime"));
                hotel.setHorarioCheckout(rs.getString("checkOutTime"));

                hotel.setUrlFotoPadrao(rs.getString("urlFotoPadrao"));
                hotel.setLarguraFotoPadrao(rs.getInt("larguraFotoPadrao"));
                hotel.setAlturaFotoPadrao(rs.getInt("alturaFotoPadrao"));
                hotel.setTituloFotoPadrao(rs.getString("tituloFotoPadrao"));

                return hotel;

            }
        });
    }

    public String consultarCityCoordinatesList(final Long regionId) {
        final String sql = "select coordinates from eanprod.citycoordinateslist p where p.regionid = :regionId";
        final Map<String, Object> params = new HashMap<>();
        params.put("regionId", regionId);

        try {
            final Map<String, Object> coordinates = this.namedParameterJdbcTemplateEAN.queryForMap(sql, params);
            return (String) coordinates.get("coordinates");
        } catch (final EmptyResultDataAccessException e) {
            return null;
        }

    }

    public List<RegiaoEAN> consultaRegioesEAN(final int start) {

        final StringBuffer sql = new StringBuffer();

        sql.append("SELECT ");
        sql.append("       r.regionid regionId, ");
        sql.append("       r.RegionName regionName, ");
        sql.append("       rbr.RegionName regionNameBR, ");
        sql.append("       c.centerlatitude latitude, ");
        sql.append("       c.centerlongitude longitude, ");
        sql.append("       r.regiontype type, ");
        sql.append("       rbr.RegionNameLong regionNameLongBR, ");
        sql.append("       r.RegionNameLong regionNameLong, ");
        sql.append("       r.parentregionid parentRegionId, ");
        sql.append("       r.parentregiontype parentRegionType, ");
        sql.append("       r.parentregionname parentRegionName, ");
        sql.append("       r.parentregionname parentRegionNameLong, ");

        sql.append("       xr.RegionCode regionCode, ");
        sql.append("       xr.RegionLocalCode regionLocalCode, ");
        sql.append("       xr.WikipediaLink wikipediaLink, ");
        sql.append("       cl.CountryCode countryCode ");

        sql.append("  FROM ");
        sql.append("       eanprod.parentregionlist r  ");
        sql.append("  LEFT JOIN ");
        sql.append("       eanprod.regionlist_pt_br rbr ON r.RegionID = rbr.RegionID ");
        sql.append("  LEFT JOIN ");
        sql.append("       eanprod.regioncentercoordinateslist c ON r.regionid = c.regionid ");
        sql.append("  LEFT JOIN ");
        sql.append("       eanprod.countrylist cl ON r.regionid = cl.CountryID ");
        sql.append("  LEFT JOIN ");
        sql.append("       eanextras.regions xr ON r.regionid = xr.ID ");
        sql.append(" WHERE ");
        sql.append("       r.regiontype in (:tipos) ");
        sql.append(" ORDER BY ");
        sql.append("       r.RegionID, r.RegionType, rbr.RegionName ");
        sql.append(" LIMIT %d, 100 ");

        final Map<String, Object> params = new HashMap<>();
        params.put("tipos", Arrays.asList(RegionType.values()).stream().map(tipo -> tipo.getName()).collect(Collectors.toList()));

        return this.namedParameterJdbcTemplateEAN.query(String.format(sql.toString(), start), params, new RowMapper<RegiaoEAN>() {

            @Override
            public RegiaoEAN mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final RegiaoEAN regiao = new RegiaoEAN();
                regiao.setRegionId(rs.getLong("regionId"));
                regiao.setRegionNameBR(rs.getString("regionNameBR"));
                regiao.setRegionName(rs.getString("regionName"));
                regiao.setLatitude(rs.getDouble("latitude") != 0.0 ? rs.getDouble("latitude") : null);
                regiao.setLongitude(rs.getDouble("longitude") != 0.0 ? rs.getDouble("longitude") : null);
                regiao.setType(RegionType.findByName(rs.getString("type")));
                regiao.setRegionNameLong(rs.getString("regionNameLong"));
                regiao.setRegionNameLongBR(rs.getString("regionNameLongBR"));

                regiao.setParentRegionId(rs.getLong("parentRegionId"));
                regiao.setParentRegionName(rs.getString("parentRegionName"));
                regiao.setParentRegionNameLong(rs.getString("parentRegionNameLong"));
                regiao.setParentRegionType(RegionType.findByName(rs.getString("parentRegionType")));

                regiao.setRegionCode(rs.getString("regionCode"));
                regiao.setRegionLocalCode(rs.getString("regionLocalCode"));
                regiao.setCountryCode(rs.getString("countryCode"));
                regiao.setWikipediaLink(rs.getString("wikipediaLink"));

                return regiao;
            }
        });

    }

    public Map<String, Object> consultarLatLongPointInterest(final Long regionId) {
        final String select = "SELECT latitude, longitude FROM eanprod.pointsofinterestcoordinateslist where regionId = :regionId";
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("regionId", regionId);

        final Map<String, Object> coordenadas = this.namedParameterJdbcTemplateEAN.queryForMap(select, params);

        final BigDecimal latitude = (BigDecimal) coordenadas.get("latitude");
        final BigDecimal longitude = (BigDecimal) coordenadas.get("longitude");

        coordenadas.put("latitude", latitude.doubleValue());
        coordenadas.put("longitude", longitude.doubleValue());

        return coordenadas;
    }

    public Long consultaTotalHoteisEAN() {
        final StringBuffer sql = new StringBuffer();

        sql.append(" SELECT count(*) ");
        sql.append("   FROM eanprod.activepropertylist ");

        final Map<String, Object> params = new HashMap<String, Object>();

        try {
            return this.namedParameterJdbcTemplateEAN.queryForObject(sql.toString(), params, Long.class);
        } catch (final EmptyResultDataAccessException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    public void incluirHoteis(final List<HotelTripFans> hoteis) {

        /*hoteis.forEach((hotel) -> {
            System.out.println(hotel);
        });*/

        final StringBuffer insert = new StringBuffer();

        insert.append("INSERT INTO hotel_tripfans( ");
        insert.append("            nome, endereco, latitude, longitude, estrelas, descricao, id_regiao_tripfans, ");
        insert.append("            descricao_en, descricao_pt, descricao_es, descricao_fr, ");
        insert.append("            id_expedia, region_expedia_id, url_expedia, categoria_expedia, ");
        insert.append("            horario_checkin, horario_checkout, ");
        insert.append("            codigo_pais, codigo_moeda, codigo_postal, observacao ");
        insert.append("       )");
        insert.append("VALUES (");
        insert.append("       :nome, :endereco, :latitude, :longitude, :estrelas, :descricao, :idRegiaoTripfans, ");
        insert.append("       :descricaoEN, :descricaoPT, :descricaoES, :descricaoFR,  ");
        insert.append("       :idExpedia, :expediaRegionId, :urlExpedia, :categoriaExpedia, ");
        insert.append("       :horarioCheckin, :horarioCheckout, ");
        insert.append("       :countryCode, :propertyCurrency, :postalCode, :observacao ");
        insert.append("       )");

        try {
            this.namedParameterJdbcTemplateTripFans.batchUpdate(insert.toString(),
                    SqlParameterSourceUtils.createBatch(hoteis.stream().filter(Objects::nonNull).collect(Collectors.toList()).toArray()));

        } catch (final Exception e) {
            e.printStackTrace();
        }

    }

    public void incluirRegioes(final List<RegiaoTripFans> regioesTripFans) {

        for (final RegiaoTripFans regiao : regioesTripFans) {
            System.out.println(regiao);
        }

        final String insert = "INSERT INTO tripfans_regiao(nome_regiao, nome_regiao_en, nome_regiao_completo, nome_regiao_completo_en, id_expedia, tipo_expedia, latitude, longitude, id_pai_expedia, nome_pai_regiao, nome_pai_regiao_completo, tipo_pai_expedia, sigla_iso_pais, origem_importacao, alterado_apos_importacao) "
                + " VALUES (:nome, :nomeIngles, :nomeCompleto, :nomeCompletoIngles, :idExpedia, :tipoExpedia, :latitude, :longitude, :idPaiExpedia, :nomePai, :nomePaiCompleto, :tipoPaiExpedia, :siglaISOPais, :origemImportacao, :alteradoAposImportacao)";

        this.namedParameterJdbcTemplateTripFans.batchUpdate(insert, SqlParameterSourceUtils.createBatch(regioesTripFans.toArray()));

    }

    public Long recuperarIdRegiaoTripfans(final Long eanRegionId) {

        final StringBuffer sql = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        sql.append(" SELECT id_regiao_tripfans ");
        sql.append("   FROM tripfans_regiao ");
        sql.append("  WHERE id_expedia = :eanRegionId ");

        params.put("eanRegionId", eanRegionId);

        Long idRegiaoTripfans = null;

        try {
            idRegiaoTripfans = this.namedParameterJdbcTemplateTripFans.queryForObject(sql.toString(), params, Long.class);
        } catch (final EmptyResultDataAccessException e) {
            System.err.println(String.format("Região Tripfans não encontrada para a EAN region ID: '%s' ", eanRegionId));
        }

        return idRegiaoTripfans;
    }

}
