package br.com.tripfans.importador.ean;

public enum RegionType {

    // @formatter:off
    CONTINENT(1, "Continent"),
    COUNTRY(2, "Country"),
    PROVINCE(3, "Province (State)"),
    CITY(4, "City"),
    MULTI_REGION(5, "Multi-Region (within a country)"),
    MULTI_CITY(6, "Multi-City (Vicinity)"),
    NEIGHBORHOOD(7, "Neighborhood"),
    POINT_OF_INTEREST(8, "Point of Interest Shadow");
    // @formatter:on

    public static RegionType findByName(final String name) {
        for (final RegionType type : values()) {
            if (type.name.equals(name)) {
                return type; 
            }
        }
        return null;
    }

    private Integer code;

    private String name;

    RegionType(final Integer code, final String name) {
        this.code = code;
        this.name = name;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.name();
    }

}
