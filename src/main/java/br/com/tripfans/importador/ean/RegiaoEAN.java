package br.com.tripfans.importador.ean;

class RegiaoEAN {

    private String countryCode;

    private String erroAoImportar;

    private Double latitude;

    private Double longitude;

    private Long parentRegionId;

    private String parentRegionName;

    private String parentRegionNameLong;

    private RegionType parentRegionType;

    private String regionCode;

    private Long regionId;

    private String regionLocalCode;

    private String regionName;

    private String regionNameBR;

    private String regionNameLong;

    private String regionNameLongBR;

    private RegionType type;

    private String wikipediaLink;

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getErroAoImportar() {
        return this.erroAoImportar;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public Long getParentRegionId() {
        return this.parentRegionId;
    }

    public String getParentRegionName() {
        return this.parentRegionName;
    }

    public String getParentRegionNameLong() {
        return this.parentRegionNameLong;
    }

    public RegionType getParentRegionType() {
        return this.parentRegionType;
    }

    public String getRegionCode() {
        return this.regionCode;
    }

    public Long getRegionId() {
        return this.regionId;
    }

    public String getRegionLocalCode() {
        return this.regionLocalCode;
    }

    public String getRegionName() {
        return this.regionName;
    }

    public String getRegionNameBR() {
        return this.regionNameBR;
    }

    public String getRegionNameLong() {
        return this.regionNameLong;
    }

    public String getRegionNameLongBR() {
        return this.regionNameLongBR;
    }

    public RegionType getType() {
        return this.type;
    }

    public String getWikipediaLink() {
        return this.wikipediaLink;
    }

    public void setCountryCode(final String countryCode) {
        this.countryCode = countryCode;
    }

    public void setErroAoImportar(final String erroAoImportar) {
        this.erroAoImportar = erroAoImportar;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setParentRegionId(final Long parentRegionId) {
        this.parentRegionId = parentRegionId;
    }

    public void setParentRegionName(final String parentRegionName) {
        this.parentRegionName = parentRegionName;
    }

    public void setParentRegionNameLong(final String parentRegionNameLong) {
        this.parentRegionNameLong = parentRegionNameLong;
    }

    public void setParentRegionType(final RegionType parentRegionType) {
        this.parentRegionType = parentRegionType;
    }

    public void setRegionCode(final String regionCode) {
        this.regionCode = regionCode;
    }

    public void setRegionId(final Long regionId) {
        this.regionId = regionId;
    }

    public void setRegionLocalCode(final String regionLocalCode) {
        this.regionLocalCode = regionLocalCode;
    }

    public void setRegionName(final String regionName) {
        this.regionName = regionName;
    }

    public void setRegionNameBR(final String regionNameBR) {
        this.regionNameBR = regionNameBR;
    }

    public void setRegionNameLong(final String regionNameLong) {
        this.regionNameLong = regionNameLong;
    }

    public void setRegionNameLongBR(final String regionNameLongBR) {
        this.regionNameLongBR = regionNameLongBR;
    }

    public void setType(final RegionType type) {
        this.type = type;
    }

    public void setWikipediaLink(final String wikipediaLink) {
        this.wikipediaLink = wikipediaLink;
    }

    @Override
    public String toString() {
        return "RegiaoEAN [" + (this.regionId != null ? "regionId=" + this.regionId + ", " : "")
                + (this.regionNameBR != null ? "regionNameBR=" + this.regionNameBR + ", " : "")
                + (this.regionNameLongBR != null ? "regionNameLongBR=" + this.regionNameLongBR + ", " : "")
                + (this.regionName != null ? "regionName=" + this.regionName + ", " : "")
                + (this.regionNameLong != null ? "regionNameLong=" + this.regionNameLong + ", " : "")
                + (this.latitude != null ? "latitude=" + this.latitude + ", " : "")
                + (this.longitude != null ? "longitude=" + this.longitude + ", " : "") + (this.type != null ? "type=" + this.type : "") + "]";
    }

}
