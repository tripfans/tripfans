package br.com.tripfans.importador.ean;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import br.com.tripfans.importador.RegiaoTripFans;

@Component
public class ImportadorRegioesEAN {

    public static void main(final String[] args) {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(ImportadorEANSpringConfig.class);
        final ImportadorRegioesEAN importador = ctx.getBean(ImportadorRegioesEAN.class);

        System.out.println("INICIO...");

        final Instant inicio = Instant.now();

        importador.importa();

        final Instant fim = Instant.now();

        final Duration timeElapsed = Duration.between(inicio, fim);
        System.out.println("Tempo de execução: " + timeElapsed.toMinutes() + " minutos");

        System.out.println("FIM.");
    }

    @Autowired
    private ImportadorEANRepository repository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    private void ajustaCoordenadas(final List<RegiaoEAN> regioes) {

        regioes.parallelStream().filter(regiao -> regiao.getLatitude() == null || regiao.getLatitude() == 0.0).forEach((regiao) -> {

            if (regiao.getType().equals(RegionType.CITY)) {
                final String coordinatesList = this.repository.consultarCityCoordinatesList(regiao.getRegionId());
                if (StringUtils.isNotEmpty(coordinatesList)) {
                    final String[] coordinates = coordinatesList.split(":");
                    Double somaLatitudes = 0.0;
                    Double somaLongitudes = 0.0;
                    for (final String coordinate : coordinates) {
                        final Double latitude = Double.valueOf(coordinate.split(";")[0]);
                        final Double longitude = Double.valueOf(coordinate.split(";")[1]);
                        somaLatitudes += latitude;
                        somaLongitudes += longitude;
                    }
                    regiao.setLatitude(somaLatitudes / coordinates.length);
                    regiao.setLongitude(somaLongitudes / coordinates.length);
                }
            } else if (regiao.getType().equals(RegionType.POINT_OF_INTEREST)) {
                try {
                    final Map<String, Object> coordenadas = this.repository.consultarLatLongPointInterest(regiao.getRegionId());
                    regiao.setLatitude((Double) coordenadas.get("latitude"));
                    regiao.setLongitude((Double) coordenadas.get("longitude"));
                } catch (final Exception e) {
                    regiao.setErroAoImportar(e.getMessage());
                }
            }
        });

    }

    public void importa() {
        int start = 0;

        // Passo 1: consulta as regiões da base EAN
        List<RegiaoEAN> regioes = this.repository.consultaRegioesEAN(start);

        while (CollectionUtils.isNotEmpty(regioes)) {
            ajustaCoordenadas(regioes);
            // Passo 2: pega os resultados e grava na base do TripFans
            insereBaseTripFans(regioes);
            start += 100;
            regioes = this.repository.consultaRegioesEAN(start);
        }

        // Passo 3: atualiza os pais de cada região
        this.repository.atualizarPaiRegioes();

        // Passo Final: ao fim de tudo, faz um update da data da última atualização

    }

    private void insereBaseTripFans(final List<RegiaoEAN> regioes) {

        final List<RegiaoTripFans> regioesTripFans = new ArrayList<>();
        regioes.parallelStream().forEach((regiaoEan) -> {
            regioesTripFans.add(nova(regiaoEan));
        });

        this.transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(final TransactionStatus status) {
                ImportadorRegioesEAN.this.repository.incluirRegioes(regioesTripFans);
                return null;
            }
        });

    }

    private RegiaoTripFans nova(final RegiaoEAN regiaoEan) {
        final RegiaoTripFans regiao = new RegiaoTripFans();

        regiao.setNome(regiaoEan.getRegionNameBR());
        regiao.setNomeIngles(regiaoEan.getRegionName());
        regiao.setNomeCompleto(regiaoEan.getRegionNameLongBR());
        regiao.setNomeCompletoIngles(regiaoEan.getRegionNameLong());
        regiao.setIdExpedia(regiaoEan.getRegionId());
        regiao.setTipoExpedia(regiaoEan.getType().getCode());
        regiao.setLatitude(regiaoEan.getLatitude());
        regiao.setLongitude(regiaoEan.getLongitude());
        regiao.setCodigoRegiaoExpedia(regiaoEan.getRegionCode());
        regiao.setCodigoEstadoProvincia(regiaoEan.getRegionLocalCode());
        regiao.setLinkWikipedia(regiaoEan.getWikipediaLink());

        regiao.setOrigemImportacao("ean");

        regiao.setIdPaiExpedia(regiaoEan.getParentRegionId());
        regiao.setNomePai(regiaoEan.getParentRegionName());
        regiao.setNomePaiCompleto(regiaoEan.getParentRegionNameLong());
        if (regiaoEan.getParentRegionType() != null) {
            regiao.setTipoPaiExpedia(regiaoEan.getParentRegionType().getCode());
        }
        regiao.setSiglaISOPais(regiaoEan.getCountryCode());

        regiao.setErroAoImportar(regiaoEan.getErroAoImportar());

        return regiao;
    }
}
