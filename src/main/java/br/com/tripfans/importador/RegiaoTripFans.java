package br.com.tripfans.importador;

public class RegiaoTripFans {

    boolean alteradoAposImportacao;

    String codigoEstadoProvincia;

    String codigoRegiaoExpedia;

    String erroAoImportar;

    Long id;

    String idBooking;

    Long idExpedia;

    Long idPaiExpedia;

    Double latitude;

    String linkWikipedia;

    Double longitude;

    String nome;

    String nomeCompleto;

    String nomeCompletoIngles;

    String nomeIngles;

    String nomePai;

    String nomePaiCompleto;

    String origemImportacao;

    String siglaISOPais;

    Integer tipoExpedia;

    Integer tipoPaiExpedia;

    public String getCodigoEstadoProvincia() {
        return this.codigoEstadoProvincia;
    }

    public String getCodigoRegiaoExpedia() {
        return this.codigoRegiaoExpedia;
    }

    public String getErroAoImportar() {
        return this.erroAoImportar;
    }

    public Long getId() {
        return this.id;
    }

    public String getIdBooking() {
        return this.idBooking;
    }

    public Long getIdExpedia() {
        return this.idExpedia;
    }

    public Long getIdPaiExpedia() {
        return this.idPaiExpedia;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public String getLinkWikipedia() {
        return this.linkWikipedia;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getNome() {
        return this.nome;
    }

    public String getNomeCompleto() {
        return this.nomeCompleto;
    }

    public String getNomeCompletoIngles() {
        return this.nomeCompletoIngles;
    }

    public String getNomeIngles() {
        return this.nomeIngles;
    }

    public String getNomePai() {
        return this.nomePai;
    }

    public String getNomePaiCompleto() {
        return this.nomePaiCompleto;
    }

    public String getOrigemImportacao() {
        return this.origemImportacao;
    }

    public String getSiglaISOPais() {
        return this.siglaISOPais;
    }

    public Integer getTipoExpedia() {
        return this.tipoExpedia;
    }

    public Integer getTipoPaiExpedia() {
        return this.tipoPaiExpedia;
    }

    public boolean isAlteradoAposImportacao() {
        return this.alteradoAposImportacao;
    }

    public void setAlteradoAposImportacao(final boolean alteradoAposImportacao) {
        this.alteradoAposImportacao = alteradoAposImportacao;
    }

    public void setCodigoEstadoProvincia(final String codigoEstadoProvincia) {
        this.codigoEstadoProvincia = codigoEstadoProvincia;
    }

    public void setCodigoRegiaoExpedia(final String codigoRegiaoExpedia) {
        this.codigoRegiaoExpedia = codigoRegiaoExpedia;
    }

    public void setErroAoImportar(final String erroAoImportar) {
        this.erroAoImportar = erroAoImportar;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setIdBooking(final String idBooking) {
        this.idBooking = idBooking;
    }

    public void setIdExpedia(final Long idExpedia) {
        this.idExpedia = idExpedia;
    }

    public void setIdPaiExpedia(final Long idPaiExpedia) {
        this.idPaiExpedia = idPaiExpedia;
    }

    public void setLatitude(final Double latitude) {
        this.latitude = latitude;
    }

    public void setLinkWikipedia(final String linkWikipedia) {
        this.linkWikipedia = linkWikipedia;
    }

    public void setLongitude(final Double longitude) {
        this.longitude = longitude;
    }

    public void setNome(final String nome) {
        this.nome = nome;
    }

    public void setNomeCompleto(final String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public void setNomeCompletoIngles(final String nomeCompletoIngles) {
        this.nomeCompletoIngles = nomeCompletoIngles;
    }

    public void setNomeIngles(final String nomeIngles) {
        this.nomeIngles = nomeIngles;
    }

    public void setNomePai(final String nomePai) {
        this.nomePai = nomePai;
    }

    public void setNomePaiCompleto(final String nomePaiCompleto) {
        this.nomePaiCompleto = nomePaiCompleto;
    }

    public void setOrigemImportacao(final String origemImportacao) {
        this.origemImportacao = origemImportacao;
    }

    public void setSiglaISOPais(final String siglaISOPais) {
        this.siglaISOPais = siglaISOPais;
    }

    public void setTipoExpedia(final Integer tipoExpedia) {
        this.tipoExpedia = tipoExpedia;
    }

    public void setTipoPaiExpedia(final Integer tipoPaiExpedia) {
        this.tipoPaiExpedia = tipoPaiExpedia;
    }

    @Override
    public String toString() {
        return "RegiaoTripFans [" + (this.idExpedia != null ? "idExpedia=" + this.idExpedia + ", " : "")
                + (this.nome != null ? "nome=" + this.nome + ", " : "")
                + (this.nomeCompleto != null ? "nomeCompleto=" + this.nomeCompleto + ", " : "")
                + (this.nomeIngles != null ? "nomeIngles=" + this.nomeIngles + ", " : "")
                + (this.nomeCompletoIngles != null ? "nomeCompletoIngles=" + this.nomeCompletoIngles + ", " : "")
                + (this.latitude != null ? "latitude=" + this.latitude + ", " : "")
                + (this.longitude != null ? "longitude=" + this.longitude + ", " : "")
                + (this.tipoExpedia != null ? "tipoExpedia=" + this.tipoExpedia : "") + "]";
    }

}
