package br.com.tripfans.importador.booking;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Repository;

import br.com.tripfans.importador.HotelTripFans;
import br.com.tripfans.importador.RegiaoTripFans;
import br.com.tripfans.importador.ean.RegionType;

@Repository
public class ImportadorBookingRepository {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateEAN;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplateTripFans;

    @Async
    public void alterarHoteis(final List<HotelBooking> hoteisBooking) {

        final StringBuffer update = new StringBuffer();

        update.append(" UPDATE hotel_tripfans ");
        update.append("    SET ");
        update.append("        id_booking = :id, ");
        update.append("        city_id_booking = :cityId, ");
        update.append("        city_name_booking = :cityName, ");
        update.append("        city_name_preferred_booking = :cityNamePreferred, ");
        update.append("        city_name_unique_booking = :cityNameUnique, ");
        update.append("        continent_id_booking = :continentId, ");
        update.append("        min_rate_booking = :minRate, ");
        update.append("        max_rate_booking = :maxRate, ");
        update.append("        numero_avaliacoes_booking = :numberOfReviews, ");
        update.append("        numero_quartos_booking = :numberOfRooms, ");
        update.append("        url_booking = :url,  ");
        update.append("        photo_url_booking = :photoUrl, ");
        update.append("        preferencial_booking = :preferred, ");
        update.append("        ranking_booking = :publicRanking,  ");
        update.append("        pontuacao_avaliacoes_booking = :reviewScore, ");
        update.append("        alterado_apos_importacao = true ");
        update.append("  WHERE id_hotel_tripfans = :idHotelTripfans ");

        this.namedParameterJdbcTemplateTripFans.batchUpdate(update.toString(), SqlParameterSourceUtils.createBatch(hoteisBooking.toArray()));
    }

    @Async
    public void alterarHotel(final Long idHotelTripfans, final HotelBooking hotelBooking) {

        final StringBuffer update = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        update.append(" UPDATE hotel_tripfans ");
        update.append("    SET ");
        update.append("        id_booking = :idBooking, ");
        update.append("        city_id_booking = :cityId, ");
        update.append("        city_name_booking = :cityName, ");
        update.append("        city_name_preferred_booking = :cityNamePreferred, ");
        update.append("        city_name_unique_booking = :cityNameUnique, ");
        update.append("        continent_id_booking = :continentId, ");
        update.append("        min_rate_booking = :minRate, ");
        update.append("        max_rate_booking = :maxRate, ");
        update.append("        numero_avaliacoes_booking = :numberOfReviews, ");
        update.append("        numero_quartos_booking = :numberOfRooms, ");
        update.append("        url_booking = :url,  ");
        update.append("        photo_url_booking = :photoUrl, ");
        update.append("        preferencial_booking = :preferred, ");
        update.append("        ranking_booking = :publicRanking,  ");
        update.append("        pontuacao_avaliacoes_booking = :reviewScore, ");
        update.append("        alterado_apos_importacao = true ");
        update.append("  WHERE id_hotel_tripfans = :idHotelTripfans ");

        params.put("idBooking", hotelBooking.getId());
        params.put("cityId", hotelBooking.getCityId());
        params.put("cityName", hotelBooking.getCityName());
        params.put("cityNamePreferred", hotelBooking.getCityNamePreferred());
        params.put("cityNameUnique", hotelBooking.getCityNameUnique());
        params.put("continentId", hotelBooking.getContinentId());
        params.put("minRate", hotelBooking.getMinRate());
        params.put("maxRate", hotelBooking.getMaxRate());
        params.put("numberOfReviews", hotelBooking.getNumberOfReviews());
        params.put("numberOfRooms", hotelBooking.getNumberOfRooms());
        params.put("url", hotelBooking.getUrl());
        params.put("photoUrl", hotelBooking.getPhotoUrl());
        params.put("preferred", hotelBooking.isPreferred());
        params.put("publicRanking", hotelBooking.getPublicRanking());
        params.put("reviewScore", hotelBooking.getReviewScore());

        params.put("idHotelTripfans", idHotelTripfans);

        this.namedParameterJdbcTemplateTripFans.update(update.toString(), params);
    }

    @Async
    public void alterarRegiao(final Long idRegiaoTripans, final CidadeBooking cidadeBooking) {

        final StringBuffer update = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        update.append(" UPDATE tripfans_regiao ");
        update.append("    SET id_booking = :idBooking, ");
        update.append("        url_booking = :urlBooking, ");
        update.append("        numero_hoteis_booking = :numeroHoteisBooking, ");
        update.append("        alterado_apos_importacao = true ");
        update.append("  WHERE id_regiao_tripfans = :idRegiaoTripfans ");

        params.put("idBooking", cidadeBooking.getId());
        params.put("urlBooking", cidadeBooking.getUrlDeepLink());
        params.put("numeroHoteisBooking", cidadeBooking.getNumberOfHotels());
        params.put("idRegiaoTripfans", idRegiaoTripans);

        this.namedParameterJdbcTemplateTripFans.update(update.toString(), params);

    }

    public List<HotelTripFans> consultaHoteisTripFans(final int start, final int limit, final String idCidadeBooking, final Double latitude,
            final Double longitude) {

        final StringBuffer sql = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        sql.append("SELECT ");
        sql.append("       ht.id_hotel_tripfans id, ");
        sql.append("       ht.id_booking idBooking, ");
        sql.append("       ht.nome, ");
        sql.append("       ht.endereco, ");
        sql.append("       ht.id_cidade, ");
        sql.append("       ht.latitude, ");
        sql.append("       ht.longitude ");
        sql.append("  FROM hotel_tripfans ht ");
        sql.append(" INNER JOIN tripfans_regiao tr ON ht.id_regiao_tripfans = tr.id_regiao_tripfans ");
        sql.append(" WHERE tr.id_booking = :idCidadeBooking ");

        if (latitude != null) {
            sql.append("   AND (to_char(ht.latitude, 'FM999.9999') ~* '" + latitude.toString().substring(0, latitude.toString().indexOf('.') + 1)
                    + "' ");
            sql.append("   AND  to_char(ht.longitude, 'FM999.9999') ~* '" + longitude.toString().substring(0, longitude.toString().indexOf('.') + 1)
                    + "') ");
        }

        sql.append(" ORDER BY ht.id_hotel_tripfans ");
        sql.append(" OFFSET %d LIMIT %d ");

        params.put("idCidadeBooking", idCidadeBooking);

        return this.namedParameterJdbcTemplateTripFans.query(String.format(sql.toString(), start, limit), params, new RowMapper<HotelTripFans>() {

            @Override
            public HotelTripFans mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final HotelTripFans hotel = new HotelTripFans();
                hotel.setId(rs.getLong("id"));
                hotel.setIdBooking(rs.getLong("idBooking"));
                hotel.setNome(rs.getString("nome"));
                hotel.setEndereco(rs.getString("endereco"));
                hotel.setLatitude(rs.getDouble("latitude"));
                hotel.setLongitude(rs.getDouble("longitude"));

                return hotel;
            }
        });
    }

    public List<RegiaoTripFans> consultaRegioesTripFans(final int start, final int limit, final Integer tipoExpedia, final String nomeRegiao,
            final String siglaISOPais, final String idBooking) {

        // TODO deve se executar o seguinte comando antes de executar essa query:
        // CREATE EXTENSION unaccent;

        String inicialNomeRegiao = null;
        if (nomeRegiao.length() > 2) {
            inicialNomeRegiao = nomeRegiao.substring(0, 3);
        } else {
            inicialNomeRegiao = nomeRegiao;
        }

        String sql =

        "  SELECT * "

        + "  FROM tripfans_regiao "

        + " WHERE tipo_expedia = :tipoExpedia "

        + "   AND sigla_iso_pais = :siglaISOPais "

        + "   AND origem_importacao = :tipoImportacao ";

        if (!inicialNomeRegiao.contains("'") && !inicialNomeRegiao.contains("(")) {
            sql += "   AND unaccent(nome_regiao_en) ~* unaccent('^" + inicialNomeRegiao + "') ";
        }

        sql += "   AND (alterado_apos_importacao IS NULL OR alterado_apos_importacao = false) "

        + "    OR id_booking = :idBooking "

        + " ORDER BY id_booking, id_expedia offset %d limit %d ";

        sql = String.format(sql, start, limit);

        final Map<String, Object> params = new HashMap<>();
        params.put("tipoExpedia", tipoExpedia);
        params.put("siglaISOPais", siglaISOPais.toUpperCase());
        params.put("tipoImportacao", "Expedia");

        params.put("idBooking", idBooking);

        return this.namedParameterJdbcTemplateTripFans.query(sql, params, new RowMapper<RegiaoTripFans>() {

            @Override
            public RegiaoTripFans mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                final RegiaoTripFans regiao = new RegiaoTripFans();
                regiao.setId(rs.getLong("id_regiao_tripfans"));
                regiao.setNome(rs.getString("nome_regiao"));
                regiao.setNomeIngles(rs.getString("nome_regiao_en"));
                regiao.setLatitude(rs.getDouble("latitude"));
                regiao.setLongitude(rs.getDouble("longitude"));
                regiao.setSiglaISOPais(rs.getString("sigla_iso_pais").toUpperCase());

                regiao.setIdBooking(rs.getString("id_booking"));

                return regiao;
            }
        });
    }

    @Async
    public void incluirHoteis(final List<HotelBooking> hoteis) {

        final StringBuffer insert = new StringBuffer();

        insert.append(" INSERT INTO hotel_tripfans( ");
        insert.append("             nome, endereco, latitude, longitude, estrelas, descricao, id_regiao_tripfans, ");
        insert.append("             descricao_en, descricao_pt, descricao_es, descricao_fr, ");
        insert.append("             id_booking, city_id_booking, city_name_booking, city_name_preferred_booking, city_name_unique_booking, ");
        insert.append("             continent_id_booking, min_rate_booking, max_rate_booking, numero_avaliacoes_booking, ");
        insert.append("             numero_quartos_booking, url_booking, photo_url_booking, preferencial_booking,  ");
        insert.append("             ranking_booking, pontuacao_avaliacoes_booking, ");
        insert.append("             codigo_pais, codigo_moeda, codigo_postal ");
        insert.append("             ) ");

        insert.append("     VALUES ( ");
        insert.append("             :name, :address, :latitude, :longitude, :stars, :descriptionPT, :idRegiaoTripfans, ");
        insert.append("             :descriptionEN, :descriptionPT, :descriptionES, :descriptionFR,  ");
        insert.append("             :id, :cityId, :cityName, :cityNamePreferred, :cityNameUnique, ");
        insert.append("             :continentId, :minRate, :maxRate, :numberOfReviews, ");
        insert.append("             :numberOfRooms, :url, :photoUrl, :preferred, ");
        insert.append("             :publicRanking, :reviewScore, ");
        insert.append("             :countryCode, :currencycode, :zipCode ");
        insert.append("            )");

        this.namedParameterJdbcTemplateTripFans.batchUpdate(insert.toString(), SqlParameterSourceUtils.createBatch(hoteis.toArray()));

    }

    @Async
    public void incluirHotel(final HotelBooking hotel) {

        final Long idRegiaoTripfans = recuperarIdRegiaoTripfans(hotel.getCityId());

        // TODO O que fazer quando a cidade do hotel não estiver em nossa base?
        // Supostamente deveria estar, pois antes de rodar a carga de Hoteis do booking, rodamos a de cidades.

        final StringBuffer insert = new StringBuffer();

        insert.append(" INSERT INTO hotel_tripfans( ");
        insert.append("             nome, endereco, latitude, longitude, estrelas, descricao, id_regiao_tripfans, ");
        insert.append("             descricao_en, descricao_pt, descricao_es, descricao_fr, ");
        insert.append("             id_booking, city_id_booking, city_name_booking, city_name_preferred_booking, city_name_unique_booking, ");
        insert.append("             continent_id_booking, min_rate_booking, max_rate_booking, numero_avaliacoes_booking, ");
        insert.append("             numero_quartos_booking, url_booking, photo_url_booking, preferencial_booking,  ");
        insert.append("             ranking_booking, pontuacao_avaliacoes_booking, ");
        insert.append("             codigo_pais, codigo_moeda, codigo_postal ");
        insert.append("             ) ");

        insert.append("     VALUES ( ");
        insert.append("             :name, :address, :latitude, :longitude, :stars, :descriptionPT, " + idRegiaoTripfans + ", ");
        insert.append("             :descriptionEN, :descriptionPT, :descriptionES, :descriptionFR,  ");
        insert.append("             :id, :cityId, :cityName, :cityNamePreferred, :cityNameUnique, ");
        insert.append("             :continentId, :minRate, :maxRate, :numberOfReviews, ");
        insert.append("             :numberOfRooms, :url, :photoUrl, :preferred, ");
        insert.append("             :publicRanking, :reviewScore, ");
        insert.append("             :countryCode, :currencycode, :zipCode ");
        insert.append("            )");

        this.namedParameterJdbcTemplateTripFans.batchUpdate(insert.toString(), SqlParameterSourceUtils.createBatch(new Object[] { hotel }));

    }

    @Async
    public void incluirRegiao(final CidadeBooking cidadeBooking) {

        final StringBuffer insert = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        insert.append(" INSERT INTO tripfans_regiao( ");
        insert.append("             id_booking, nome_regiao_en, tipo_expedia, ");
        insert.append("             sigla_iso_pais, origem_importacao, url_booking, ");
        insert.append("             numero_hoteis_booking, alterado_apos_importacao) ");
        insert.append("      VALUES (:idBooking, :nome, :tipoExpedia, :siglaISOPais, :origemImportacao, :urlBooking, :numeroHoteisBooking, :alterado) ");

        params.put("idBooking", cidadeBooking.getId());
        params.put("nome", cidadeBooking.getFullName());
        params.put("tipoExpedia", RegionType.CITY.getCode());
        params.put("siglaISOPais", cidadeBooking.getCountryCode().toUpperCase());
        params.put("origemImportacao", "Booking");
        params.put("urlBooking", cidadeBooking.getUrlDeepLink());
        params.put("numeroHoteisBooking", cidadeBooking.getNumberOfHotels());
        params.put("alterado", false);

        try {
            this.namedParameterJdbcTemplateTripFans.update(insert.toString(), params);
            // System.out.println("Incluiu na base: " + cidadeBooking.getFullName());
        } catch (final DuplicateKeyException e) {
            // System.err.println("Já existe na base: " + cidadeBooking.getFullName());
        }

    }

    public Long recuperarIdRegiaoTripfans(final String bookingCityId) {

        final StringBuffer sql = new StringBuffer();
        final Map<String, Object> params = new HashMap<String, Object>();

        sql.append(" SELECT id_regiao_tripfans ");
        sql.append("   FROM tripfans_regiao ");
        sql.append("  WHERE id_booking = :bookingCityId ");

        params.put("bookingCityId", bookingCityId);

        Long idRegiaoTripfans = null;

        params.put("bookingCityId", bookingCityId);

        try {
            idRegiaoTripfans = this.namedParameterJdbcTemplateTripFans.queryForObject(sql.toString(), params, Long.class);
        } catch (final EmptyResultDataAccessException e) {
            System.err.println(String.format("Região Tripfans não encontrada para a ID da cidade do Booking: '%s' ", bookingCityId));
        } catch (final IncorrectResultSizeDataAccessException e) {
            System.err.println(String.format("Erro ao recuperar Região Tripfans para a ID da cidade do Booking: '%s' ", bookingCityId));
        }

        return idRegiaoTripfans;
    }

}
