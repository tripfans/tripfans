package br.com.tripfans.importador.booking;

public class HotelBooking {

    /**
     * Endereço da propriedade
     */
    String address;

    String checkinTime;

    String checkoutTime;

    /**
     * Identificador único da cidade
     */
    String cityId;

    /**
     * Cidade da propriedade
     */
    String cityName;

    /**
     * Nome preferencial da cidade, para uso da Booking.com
     */
    String cityNamePreferred;

    /**
     * Nome exclusivo da cidade, para uso no endereço do site
     */
    String cityNameUnique;

    /**
     * ID exclusiva do continente onde a propriedade está localizada
     */
    Long continentId;

    /**
     * Código do país da propriedade
     */
    String countryCode;

    /**
     * A moeda em que os preços desta propriedade são divulgados (três dígitos, letra maiúscula - ex.: EUR)
     */
    String currencycode;

    /**
     * Breve descrição da propriedade em Ingles
     */
    String descriptionEN;

    /**
     * Breve descrição da propriedade em Espanhol
     */
    String descriptionES;

    /**
     * Breve descrição da propriedade em Frances
     */
    String descriptionFR;

    /**
     * Breve descrição da propriedade em Portugues
     */
    String descriptionPT;

    /**
     * ID do hotel
     */
    Long id;

    /**
     * Id do hotel do TripFans para fins de alteracao
     */
    Long idHotelTripfans;

    /**
     * Id da regiao do TripFans para fins de inclusão
     */
    Long idRegiaoTripfans;

    /**
     * Latitude decimal
     */
    Double latitude;

    /**
     * Longitude decimal
     */
    Double longitude;

    //
    /**
     * A tarifa mais alta cobrada pela propriedade, calculada em média
     */
    Double maxRate;

    /**
     * A tarifa mais baixa cobrada pela propriedade, calculada em média
     */
    Double minRate;

    /**
     * Nome da propriedade
     */
    String name;

    /**
     * Número de avaliações do hotel
     */
    Integer numberOfReviews;

    /**
     * O número total de quartos da propriedade
     */
    Integer numberOfRooms;

    /**
     * Endereço do site da principal foto da propriedade
     */
    String photoUrl;

    /**
     * Se o valor for 1,esta é uma propriedade preferencial da Booking.com
     */
    boolean preferred;

    /**
     * A classificação da propriedade (quanto maior, melhor)
     */
    Double publicRanking;

    /**
     * Pontuação do hotel de acordo com avaliações de hospedes
     */
    Double reviewScore;

    /**
     * Número de estrelas da propriedade
     */
    Double stars;

    /**
     * Endereço da página da propriedade na Booking.com
     *
     * Aviso: Sua ID de afiliado deve ser adicionada a este endereço de site
     * Idiomas podem ser adicionados
     */
    String url;

    /**
     * Código postal
     */
    String zipCode;

    public String getAddress() {
        return this.address;
    }

    public String getCheckinTime() {
        return this.checkinTime;
    }

    public String getCheckoutTime() {
        return this.checkoutTime;
    }

    public String getCityId() {
        return this.cityId;
    }

    public String getCityName() {
        return this.cityName;
    }

    public String getCityNamePreferred() {
        return this.cityNamePreferred;
    }

    public String getCityNameUnique() {
        return this.cityNameUnique;
    }

    public Long getContinentId() {
        return this.continentId;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getCurrencycode() {
        return this.currencycode;
    }

    public String getDescriptionEN() {
        return this.descriptionEN;
    }

    public String getDescriptionES() {
        return this.descriptionES;
    }

    public String getDescriptionFR() {
        return this.descriptionFR;
    }

    public String getDescriptionPT() {
        return this.descriptionPT;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdHotelTripfans() {
        return this.idHotelTripfans;
    }

    public Long getIdRegiaoTripfans() {
        return this.idRegiaoTripfans;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public Double getMaxRate() {
        return this.maxRate;
    }

    public Double getMinRate() {
        return this.minRate;
    }

    public String getName() {
        return this.name;
    }

    public Integer getNumberOfReviews() {
        return this.numberOfReviews;
    }

    public Integer getNumberOfRooms() {
        return this.numberOfRooms;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public Double getPublicRanking() {
        return this.publicRanking;
    }

    public Double getReviewScore() {
        return this.reviewScore;
    }

    public Double getStars() {
        return this.stars;
    }

    public String getUrl() {
        return this.url;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public boolean isPreferred() {
        return this.preferred;
    }

    @Override
    public String toString() {
        return "Hotel Booking [" + (this.id != null ? "id=" + this.id + ", " : "") + (this.name != null ? "nome=" + this.name + ", " : "") + "]";
    }

}
