package br.com.tripfans.importador.booking;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.collections.CollectionUtils;
import org.simmetrics.StringMetric;
import org.simmetrics.builders.StringMetricBuilder;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.simplifiers.Simplifiers;
import org.simmetrics.tokenizers.Tokenizers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import br.com.tripfans.importador.RegiaoTripFans;
import br.com.tripfans.importador.ean.ImportadorEANSpringConfig;
import br.com.tripfans.importador.ean.RegionType;

@Component
public class ImportadorRegioesBooking {

    private final static String ARQUIVO_CIDADES = "/home/carlosn/Downloads/tf/booking/cidades.tsv";

    private final static String DELIMITADOR = "\\t";

    final static StringMetric SIMILARITY_METRIC = StringMetricBuilder.with(new CosineSimilarity<String>())
            .simplify(Simplifiers.toLowerCase(Locale.ENGLISH)).simplify(Simplifiers.removeDiacritics()).simplify(Simplifiers.removeAll("'"))
            .simplify(Simplifiers.replaceNonWord()).tokenize(Tokenizers.whitespace()).build();

    public static void main(final String[] args) throws IOException {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(ImportadorEANSpringConfig.class);
        final ImportadorRegioesBooking importador = ctx.getBean(ImportadorRegioesBooking.class);

        System.out.println("Inicio da importação ...");

        final Instant inicio = Instant.now();

        importador.importa();

        final Instant fim = Instant.now();

        final Duration timeElapsed = Duration.between(inicio, fim);
        System.out.println("Tempo de execução: " + timeElapsed.toMinutes() + " minutos");
    }

    @Autowired
    private ImportadorBookingRepository repository;

    final AtomicReference<Integer> totalLinhas = new AtomicReference<>();

    private RegiaoTripFans encontraCorrespondenteTripFans(final CidadeBooking cidadeBooking, final List<RegiaoTripFans> regioes) {
        if (cidadeBooking.id.equals(regioes.get(0).getIdBooking())) {
            return regioes.get(0);
        }

        final Optional<RegiaoTripFans> regiaoTripFans = regioes.parallelStream().filter(cidade -> (getGrauSimilaridade(cidadeBooking, cidade) > 0.9))
                .findFirst();

        if (regiaoTripFans.isPresent()) {
            return regiaoTripFans.get();
        } else {
            return null;
        }
    }

    private CidadeBooking from(final String linha) {

        final String[] valores = linha.split(DELIMITADOR, -1);
        final CidadeBooking cidade = new CidadeBooking();

        cidade.fullName = valores[0];
        if (valores[1] != null && !valores[1].isEmpty()) {
            cidade.numberOfHotels = Integer.parseInt(valores[1]);
        }
        cidade.countryCode = valores[2];
        cidade.id = valores[3];
        cidade.urlDeepLink = valores[4];

        return cidade;
    }

    public float getGrauSimilaridade(final CidadeBooking cidadeBooking, final RegiaoTripFans regiao) {

        return SIMILARITY_METRIC.compare(regiao.getNomeIngles() + " " + regiao.getSiglaISOPais(),
                cidadeBooking.getFullName() + " " + cidadeBooking.getCountryCode());
    }

    private void importa() throws IOException {

        this.totalLinhas.set(0);

        final BufferedReader reader = new BufferedReader(new FileReader(ARQUIVO_CIDADES));

        final LineNumberReader lnr = new LineNumberReader(new FileReader(ARQUIVO_CIDADES));
        // Numero maximo de linhas dos arquivos de hoteis
        lnr.skip(Integer.MAX_VALUE);
        this.totalLinhas.accumulateAndGet(lnr.getLineNumber(), (anterior, atual) -> anterior + atual);
        System.out.println("Total de linhas do arquivo: " + ARQUIVO_CIDADES + ": " + lnr.getLineNumber());
        lnr.close();

        reader.readLine(); // pula a primeira linha

        final AtomicReference<List<CidadeBooking>> cidadesInclusao = new AtomicReference<>();
        final AtomicReference<Map<Long, CidadeBooking>> cidadesAlteracao = new AtomicReference<>();
        cidadesInclusao.set(new ArrayList<CidadeBooking>());
        cidadesAlteracao.set(new HashMap<>());

        final AtomicReference<Integer> totalLinhasLidas = new AtomicReference<>();
        totalLinhasLidas.set(0);

        final AtomicReference<Integer> totalIncluidos = new AtomicReference<>();
        totalIncluidos.set(0);

        final AtomicReference<Integer> totalAlterados = new AtomicReference<>();
        totalAlterados.set(0);

        // @formatter:off
        reader.lines()
        .parallel()
        .forEach(
                linha -> {

                    final CidadeBooking cidade = from(linha);
                    if (cidade != null) {

                        int start = 0;
                        final int limit = 1000;
                        RegiaoTripFans regiaoTripfans = null;

                        List<RegiaoTripFans> regioes = this.repository.consultaRegioesTripFans(start, limit, RegionType.CITY.getCode(),
                                cidade.fullName, cidade.countryCode, cidade.id);

                        while (CollectionUtils.isNotEmpty(regioes)) {
                            regiaoTripfans = encontraCorrespondenteTripFans(cidade, regioes);
                            if (regiaoTripfans != null) {
                                break;
                            }
                            start += limit;
                            regioes = this.repository.consultaRegioesTripFans(start, limit, RegionType.CITY.getCode(), cidade.fullName, cidade.countryCode, cidade.id);
                        }

                        if (regiaoTripfans != null) {

                            if (regiaoTripfans.getIdBooking() != null) {
                                //System.out.println(String.format("Já existe na base: %s ", regiaoTripfans.getNomeIngles()));
                            } else {

                                /*System.out.println(String.format("Encontrou correspondência: %s ----- %s",
                                        cidade.getFullName() + ", " + cidade.getCountryCode(), regiaoTripfans.getNomeIngles() + ", "
                                                + regiaoTripfans.getSiglaISOPais()));*/

                                this.repository.alterarRegiao(regiaoTripfans.getId(), cidade);
                                totalAlterados.accumulateAndGet(1, (anterior, atual) -> anterior + atual);


                            }
                            //cidadesAlteracao.get().put(regiaoTripfans.getId(), cidade);

                        } else {

                            /*System.err.println(String.format("NÃO encontrou correspondência: %s",
                                    cidade.getFullName() + ", " + cidade.getCountryCode()));*/

                            this.repository.incluirRegiao(cidade);
                            totalIncluidos.accumulateAndGet(1, (anterior, atual) -> anterior + atual);

                            //cidadesInclusao.get().add(cidade);

                        }
                    }

                    totalLinhasLidas.accumulateAndGet(1, (anterior, atual) -> anterior + atual);

                    if (totalLinhasLidas.get() % 1000 == 0) {
                        System.err.println("Total de linhas lidas: " + totalLinhasLidas.get() + " de um total de " + this.totalLinhas.get() + " (" + ((totalLinhasLidas.get() * 100) / this.totalLinhas.get() + "%)"));
                        System.err.println("Total incluidas: " + totalIncluidos.get());
                        System.err.println("Total alteradas: " + totalAlterados.get());
                    }

                });

        // @formatter:on

        reader.close();

    }
}
