package br.com.tripfans.importador.booking;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.simmetrics.StringMetric;
import org.simmetrics.builders.StringMetricBuilder;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.simplifiers.Simplifiers;
import org.simmetrics.tokenizers.Tokenizers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import br.com.tripfans.importador.HotelTripFans;
import br.com.tripfans.importador.ean.ImportadorEANSpringConfig;

@Component
@EnableAsync
public class ImportadorHoteisBooking {

    private final static String DELIMITADOR = "\\t";

    private final static String DIRETORIO_ARQUIVOS_HOTEIS = "/home/carlosn/Downloads/tf/booking/hoteis/tmp";

    private final static Integer ID_AFILIACAO_BOOKING = 382185;

    final static StringMetric SIMILARITY_METRIC = StringMetricBuilder.with(new CosineSimilarity<String>())
            .simplify(Simplifiers.toLowerCase(Locale.ENGLISH)).simplify(Simplifiers.removeDiacritics()).simplify(Simplifiers.removeAll("'"))
            .simplify(Simplifiers.replaceNonWord()).tokenize(Tokenizers.whitespace()).build();

    public static void main(final String[] args) throws IOException {
        final ApplicationContext ctx = new AnnotationConfigApplicationContext(ImportadorEANSpringConfig.class);
        final ImportadorHoteisBooking importador = ctx.getBean(ImportadorHoteisBooking.class);

        importador.importar();
    }

    @Autowired
    private ImportadorBookingRepository repository;

    final AtomicReference<Integer> totalLinhas = new AtomicReference<>();

    public double distFrom(final HotelBooking hotelBooking, final HotelTripFans hotelTripFans) {
        final double lat1 = hotelBooking.latitude;
        final double lat2 = hotelTripFans.latitude;

        final double lng1 = hotelBooking.longitude;
        final double lng2 = hotelTripFans.longitude;
        final double earthRadius = 6371.0;
        final double dLat = Math.toRadians(lat2 - lat1);
        final double dLng = Math.toRadians(lng2 - lng1);
        final double sindLat = Math.sin(dLat / 2);
        final double sindLng = Math.sin(dLng / 2);
        final double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        final double dist = earthRadius * c;

        return dist;
    }

    private HotelTripFans encontraCorrespondenteTripFans(final HotelBooking hotelBooking, final List<HotelTripFans> hoteis) {

        final Optional<HotelTripFans> hotelTripfans = hoteis.parallelStream().filter(hotel -> (getGrauSimilaridade(hotelBooking, hotel) > 0.67))
                .findFirst();

        if (hotelTripfans.isPresent()) {
            return hotelTripfans.get();
        } else {
            return null;
        }

    }

    private HotelBooking from(final String linha) {

        final String[] valores = linha.split(DELIMITADOR, -1);
        final HotelBooking hotel = new HotelBooking();

        hotel.id = Long.parseLong(valores[0]);
        hotel.name = valores[1];
        hotel.address = valores[2];

        hotel.zipCode = valores[3];

        hotel.cityName = valores[4];
        hotel.countryCode = StringUtils.isNotEmpty(valores[5]) ? valores[5].toUpperCase() : null;
        hotel.cityId = valores[6];
        hotel.stars = StringUtils.isNotEmpty(valores[7]) ? Double.parseDouble(valores[7]) : null;
        hotel.currencycode = valores[8];

        hotel.minRate = StringUtils.isNotEmpty(valores[9]) ? Double.parseDouble(valores[9]) : null;
        hotel.maxRate = StringUtils.isNotEmpty(valores[10]) ? Double.parseDouble(valores[10]) : null;
        // Se o valor for 1,esta é uma propriedade preferencial da Booking.com
        hotel.preferred = ("1".equals(valores[11]));

        hotel.numberOfRooms = StringUtils.isNotEmpty(valores[12]) ? Integer.parseInt(valores[12]) : null;

        hotel.longitude = Double.parseDouble(valores[13]);
        hotel.latitude = Double.parseDouble(valores[14]);

        hotel.publicRanking = StringUtils.isNotEmpty(valores[15]) ? Double.parseDouble(valores[15]) : null;

        // Sua ID de afiliado deve ser adicionada a este endereço de site
        hotel.url = valores[16].replace(".html", ".pt-br.html") + "?aid=" + ID_AFILIACAO_BOOKING;

        hotel.photoUrl = valores[17].replace(".html", ".pt-br.html");

        hotel.descriptionEN = valores[18];
        hotel.descriptionFR = valores[19];
        hotel.descriptionES = valores[20];
        hotel.descriptionPT = valores[24];

        hotel.cityNameUnique = valores[33];
        hotel.cityNamePreferred = valores[34];
        hotel.continentId = StringUtils.isNotEmpty(valores[35]) ? Long.parseLong(valores[35]) : null;
        hotel.reviewScore = StringUtils.isNotEmpty(valores[36]) ? Double.parseDouble(valores[36]) : null;
        hotel.numberOfReviews = StringUtils.isNotEmpty(valores[37]) ? Integer.parseInt(valores[37]) : null;

        return hotel;

    }

    public float getGrauSimilaridade(final HotelBooking hotelBooking, final HotelTripFans hotelTripfans) {
        final double distancia = distFrom(hotelBooking, hotelTripfans);
        if (distancia <= 0.020) {

            float result = SIMILARITY_METRIC.compare(hotelTripfans.getNome() + " " + hotelTripfans.getEndereco(), hotelBooking.name + " "
                    + hotelBooking.address);
            if (result < 0.67) {
                result = SIMILARITY_METRIC.compare(hotelTripfans.getNome(), hotelBooking.name);
            }

            return result;
        }
        return 0.0F;
    }

    private final List<Path> getListaArquivosHoteis() throws IOException, FileNotFoundException {
        final List<Path> listaArquivos = new ArrayList<>();

        final Path dir = FileSystems.getDefault().getPath(DIRETORIO_ARQUIVOS_HOTEIS);

        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.{tsv}")) {
            for (final Path entry : stream) {

                final LineNumberReader lnr = new LineNumberReader(new FileReader(entry.toFile()));
                // Numero maximo de linhas dos arquivos de hoteis
                lnr.skip(Integer.MAX_VALUE);
                this.totalLinhas.accumulateAndGet(lnr.getLineNumber(), (anterior, atual) -> anterior + atual);
                System.out.println(" Total de linhas do arquivo: " + entry.getFileName() + ": " + lnr.getLineNumber());
                lnr.close();

                listaArquivos.add(entry);
            }

            System.out.println("TOTAL DE LINHAS: " + this.totalLinhas);
        } catch (final DirectoryIteratorException ex) {
            throw ex.getCause();
        }

        return listaArquivos;
    }

    private void importar() throws IOException, FileNotFoundException {

        this.totalLinhas.set(0);

        final List<Path> listaArquivos = getListaArquivosHoteis();
        // final Path arquivo = getArquivoHotel();

        final AtomicReference<List<HotelBooking>> hoteisBookingInclusao = new AtomicReference<>();
        hoteisBookingInclusao.set(new ArrayList<>());

        final AtomicReference<List<HotelBooking>> hoteisBookingAlteracao = new AtomicReference<>();
        hoteisBookingAlteracao.set(new ArrayList<>());

        final AtomicReference<Map<String, String>> idsCidadesNaoEncontradas = new AtomicReference<>();
        idsCidadesNaoEncontradas.set(new HashMap<>());

        final AtomicReference<Integer> quantidadeLinhasLidas = new AtomicReference<>();
        quantidadeLinhasLidas.set(0);

        final AtomicReference<Instant> inicio = new AtomicReference<>();
        inicio.set(Instant.now());
        final AtomicReference<Duration> timeElapsed = new AtomicReference<>();

        // @formatter:off
        //listaArquivos.parallelStream().forEach(file -> {
        listaArquivos.forEach(file -> {

            BufferedReader reader;
            try {
                reader = new BufferedReader(new FileReader(file.toFile()));

                //reader = new BufferedReader(new FileReader(arquivo.toFile()));

                // Pula a primeira linha
                reader.readLine();

                reader.lines()
                .parallel()
                .forEach(
                        linha -> {

                            try {

                                final HotelBooking hotel = from(linha);

                                Instant fim = null;

                                if (hotel != null) {
                                    int start = 0;
                                    final int limit = 1000;
                                    HotelTripFans hotelTripfans = null;

                                    List<HotelTripFans> hoteis = this.repository.consultaHoteisTripFans(start, limit, hotel.cityId, hotel.latitude, hotel.longitude);

                                    while (CollectionUtils.isNotEmpty(hoteis)) {
                                        //System.out.println("numero hoteis encontrados na cidade " + hotel.cityId + ": " + hoteis.size());
                                        hotelTripfans = encontraCorrespondenteTripFans(hotel, hoteis);

                                        if (hotelTripfans != null) {
                                            break;
                                        }
                                        start += limit;
                                        hoteis = this.repository.consultaHoteisTripFans(start, limit, hotel.cityId, hotel.latitude, hotel.longitude);
                                    }

                                    if (hotelTripfans != null) {
                                        /*System.out.println(String.format("Encontrou correspondência: %s ----- %s", hotel.name + ", " + hotel.address,
                                                hotelTripfans.getNome() + ", " + hotelTripfans.getEndereco()));*/


                                        /*System.out.println("#booking#");
                                        System.out.println(hotel.latitude);
                                        System.out.println(hotel.longitude);

                                        System.out.println("#tripfans#");
                                        System.out.println(hotelTripfans.latitude);
                                        System.out.println(hotelTripfans.longitude);*/



                                        //if (hotelTripfans.getIdBooking() == null) {

                                        hotel.idRegiaoTripfans = hotelTripfans.getIdRegiaoTripfans();
                                        hotel.idHotelTripfans = hotelTripfans.getId();

                                        this.repository.alterarHotel(hotelTripfans.getId(), hotel);

                                        /*synchronized (hoteisBookingAlteracao) {
                                            hoteisBookingAlteracao.get().add(hotel);
                                        }*/
                                        //}

                                    } else {

                                        // Verifica se o id está na lista de cidades nao encontradas
                                        final String idCidadeBooking = idsCidadesNaoEncontradas.get().get(hotel.getCityId());
                                        if (idCidadeBooking == null) {
                                            // verifica se a cidade já está na base
                                            final Long idRegiaoTripfans = this.repository.recuperarIdRegiaoTripfans(hotel.getCityId());
                                            // se não estiver na base, adiciona na lista de cidades não encontradas
                                            if (idRegiaoTripfans == null) {
                                                idsCidadesNaoEncontradas.get().put(hotel.getCityId(), hotel.getCityId());
                                            } else {
                                                hotel.idRegiaoTripfans = idRegiaoTripfans;
                                            }
                                        }

                                        /*System.err.println(String.format("NÃO encontrou correspondência: %s - %s - %s",
                                                hotel.name + "{lat: " + hotel.latitude + " / long: " + hotel.longitude +"}", hotel.cityName + "[" + hotel.cityId +"]", hotel.countryCode.toUpperCase()));*/

                                        this.repository.incluirHotel(hotel);
                                        /*synchronized (hoteisBookingInclusao) {
                                            hoteisBookingInclusao.get().add(hotel);
                                        }*/
                                    }

                                    if (quantidadeLinhasLidas.getAndAccumulate(1, (anterior, atual) -> anterior + atual) % 1000 == 0) {

                                        /*List<HotelBooking> hoteisInclusao = null;
                                        List<HotelBooking> hoteisAlteracao = null;
                                        System.out.println("Flushing... ");
                                        synchronized (hoteisBookingInclusao) {
                                            hoteisInclusao = hoteisBookingInclusao.get();
                                            System.err.println("Incluindo " + hoteisInclusao.size() + " hoteis...");
                                            //this.repository.incluirHoteis(hoteisInclusao);
                                            hoteisBookingInclusao.set(new ArrayList<>());
                                        }
                                        synchronized (hoteisBookingAlteracao) {
                                            hoteisAlteracao = hoteisBookingAlteracao.get();
                                            System.err.println("Alterando " + hoteisAlteracao.size() + " hoteis...");
                                            //this.repository.alterarHoteis(hoteisAlteracao);
                                            hoteisBookingAlteracao.set(new ArrayList<>());
                                        }*/

                                        final double percentual = ((quantidadeLinhasLidas.get()) * 100.0 / this.totalLinhas.get());
                                        final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
                                        nf.setMaximumFractionDigits(1);
                                        nf.setMinimumFractionDigits(1);

                                        System.out.println("Total de hoteis importados: " + quantidadeLinhasLidas + " de um total de " + this.totalLinhas + " - " + (nf.format(percentual) + "%"));

                                        fim = Instant.now();

                                        timeElapsed.set(Duration.between(inicio.get(), fim));
                                        System.out.println("Tempo decorrido: " + timeElapsed.get().toMillis() / 1000 + " segundo(s)");

                                    }

                                }
                            } catch (final Exception e) {
                                // TODO Tratar o hoteis lidos que deram erro durante a leitura da linha do arquivo
                            }
                        }
                        );

                System.err.println("Arquivo " + file.getFileName() + " concluido. ");

                file.toFile().delete();

            } catch (final Exception e) {
                e.printStackTrace();
            }
        });
        // @formatter:on

    }

}
