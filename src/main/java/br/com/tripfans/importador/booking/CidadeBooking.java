package br.com.tripfans.importador.booking;

public class CidadeBooking {

    String countryCode;

    String fullName;

    String id;

    Integer numberOfHotels;

    String urlDeepLink;

    public String getCountryCode() {
        return this.countryCode;
    }

    public String getFullName() {
        return this.fullName;
    }

    public String getId() {
        return this.id;
    }

    public Integer getNumberOfHotels() {
        return this.numberOfHotels;
    }

    public String getUrlDeepLink() {
        return this.urlDeepLink;
    }

}
