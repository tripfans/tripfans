package br.com.tripfans.externalservices.hotel.booking;

import br.com.tripfans.externalservices.hotel.RoomOfferDetails;
import br.com.tripfans.importador.HotelTripFans;

public class BookingRoomOfferDetails extends RoomOfferDetails {

    public BookingRoomOfferDetails(final HotelTripFans hotel) {
        super(hotel);
    }

    @Override
    public String getPartnerIconName() {
        return "booking";
    }

    @Override
    public String getPartnerName() {
        return "Booking";
    }

    @Override
    public Float getPartnerPercentageComission() {
        return 0.03F;
    }

}
