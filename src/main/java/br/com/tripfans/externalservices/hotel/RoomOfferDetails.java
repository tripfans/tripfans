package br.com.tripfans.externalservices.hotel;

import br.com.tripfans.importador.HotelTripFans;

public abstract class RoomOfferDetails {

    private Integer currentAllotment;

    private String expediaPropertyId;

    private HotelTripFans hotel;

    private Integer maxRoomOccupancy;

    private Integer minGuestAge;

    // private BedTypes bedTypes;

    private Boolean nonRefundable;

    private String promoDescription;

    private String promoDetailText;

    private String promoId;

    private Boolean propertyAvailable;

    private Boolean propertyRestricted;

    private Integer quotedRoomOccupancy;

    private String rateCode;

    private RateInfo rateInfo;

    private String rateKey;

    private String rateType;

    private String roomDescription;

    private String roomTypeCode;

    private String selectRoomUrl;

    private String smokingPreferences;

    public RoomOfferDetails(final HotelTripFans hotel) {
        this.hotel = hotel;
    }

    public Integer getCurrentAllotment() {
        return this.currentAllotment;
    }

    public String getExpediaPropertyId() {
        return this.expediaPropertyId;
    }

    public HotelTripFans getHotel() {
        return this.hotel;
    }

    public Float getHotelStars() {
        if (this.hotel.getEstrelas() != null) {
            return new Float(this.hotel.getEstrelas());
        }
        return null;
    }

    public Integer getMaxRoomOccupancy() {
        return this.maxRoomOccupancy;
    }

    public Integer getMinGuestAge() {
        return this.minGuestAge;
    }

    public Float getNightlyRateTotal() {
        if (this.rateInfo != null) {
            return this.rateInfo.getNightlyRateTotal();
        }
        return null;
    }

    public Boolean getNonRefundable() {
        return this.nonRefundable;
    }

    public abstract String getPartnerIconName();

    public abstract String getPartnerName();

    public abstract Float getPartnerPercentageComission();

    public String getPromoDescription() {
        return this.promoDescription;
    }

    public String getPromoDetailText() {
        return this.promoDetailText;
    }

    public String getPromoId() {
        return this.promoId;
    }

    public Boolean getPropertyAvailable() {
        return this.propertyAvailable;
    }

    public Boolean getPropertyRestricted() {
        return this.propertyRestricted;
    }

    public Integer getQuotedRoomOccupancy() {
        return this.quotedRoomOccupancy;
    }

    public String getRateCode() {
        return this.rateCode;
    }

    public RateInfo getRateInfo() {
        return this.rateInfo;
    }

    public String getRateKey() {
        return this.rateKey;
    }

    public String getRateType() {
        return this.rateType;
    }

    public String getRoomDescription() {
        return this.roomDescription;
    }

    public String getRoomTypeCode() {
        return this.roomTypeCode;
    }

    public String getSelectRoomUrl() {
        return this.selectRoomUrl;
    }

    public String getSmokingPreferences() {
        return this.smokingPreferences;
    }

    /**
     * Percentual que dividimos com os usuários: 50%
     *
     * @return
     */
    public Float getTripFansPercentageComission() {
        return 0.5F;
    }

    public void setCurrentAllotment(final Integer currentAllotment) {
        this.currentAllotment = currentAllotment;
    }

    public void setExpediaPropertyId(final String expediaPropertyId) {
        this.expediaPropertyId = expediaPropertyId;
    }

    public void setHotel(final HotelTripFans hotel) {
        this.hotel = hotel;
    }

    public void setMaxRoomOccupancy(final Integer maxRoomOccupancy) {
        this.maxRoomOccupancy = maxRoomOccupancy;
    }

    public void setMinGuestAge(final Integer minGuestAge) {
        this.minGuestAge = minGuestAge;
    }

    public void setNonRefundable(final Boolean nonRefundable) {
        this.nonRefundable = nonRefundable;
    }

    public void setPromoDescription(final String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public void setPromoDetailText(final String promoDetailText) {
        this.promoDetailText = promoDetailText;
    }

    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }

    public void setPropertyAvailable(final Boolean propertyAvailable) {
        this.propertyAvailable = propertyAvailable;
    }

    public void setPropertyRestricted(final Boolean propertyRestricted) {
        this.propertyRestricted = propertyRestricted;
    }

    public void setQuotedRoomOccupancy(final Integer quotedRoomOccupancy) {
        this.quotedRoomOccupancy = quotedRoomOccupancy;
    }

    public void setRateCode(final String rateCode) {
        this.rateCode = rateCode;
    }

    public void setRateInfo(final RateInfo rateInfo) {
        this.rateInfo = rateInfo;
    }

    public void setRateKey(final String rateKey) {
        this.rateKey = rateKey;
    }

    public void setRateType(final String rateType) {
        this.rateType = rateType;
    }

    public void setRoomDescription(final String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public void setRoomTypeCode(final String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }

    public void setSelectRoomUrl(final String selectRoomUrl) {
        this.selectRoomUrl = selectRoomUrl;
    }

    public void setSmokingPreferences(final String smokingPreferences) {
        this.smokingPreferences = smokingPreferences;
    }

    // private ValueAdds valueAdds;

}
