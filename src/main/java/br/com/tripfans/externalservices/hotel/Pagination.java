package br.com.tripfans.externalservices.hotel;

/**
 * Dados de paginação para a API EAN
 *
 * @author carlosn
 */
public class Pagination {

    /**
     * The key for the specific cached response requested. Use the value returned in the previous hotel list response.
     */
    private String cacheKey;

    /**
     * Defines the EAN server location of the requested cache. Use the value returned in the previous hotel list response.
     */
    private String cacheLocation;

    private Integer page;

    /**
     * Must send with value E to allow paging system to accurately indicate additional results
     */
    private String supplierType;

    public String getCacheKey() {
        return this.cacheKey;
    }

    public String getCacheLocation() {
        return this.cacheLocation;
    }

    public Integer getPage() {
        return this.page;
    }

    public String getSupplierType() {
        return this.supplierType;
    }

    public void setCacheKey(final String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public void setCacheLocation(final String cacheLocation) {
        this.cacheLocation = cacheLocation;
    }

    public void setPage(final Integer page) {
        this.page = page;
    }

    public void setSupplierType(final String supplierType) {
        this.supplierType = supplierType;
    }

}
