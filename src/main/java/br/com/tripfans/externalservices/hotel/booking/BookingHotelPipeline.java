package br.com.tripfans.externalservices.hotel.booking;

import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

public class BookingHotelPipeline implements Pipeline {

    private final List<BookingHotel> results = new ArrayList<BookingHotel>();

    public List<BookingHotel> getResults() {
        return this.results;
    }

    @Override
    public void process(final ResultItems resultItems, final Task task) {
        for (final Object object : resultItems.getAll().values()) {
            this.results.add((BookingHotel) object);
            System.out.println(((BookingHotel) object).getName());
        }
    }

}
