package br.com.tripfans.externalservices.hotel;

import java.util.List;

public class Filter {

    private Pagination pagination;

    private List<String> propertyCategory;

    private String propertyName;

    private Integer[] rate;

    private SortType sortType;

    private Float[] starRating;

    private Float[] tripAdvisorRating;

    public Integer getMaxRate() {
        if (this.rate != null) {
            return this.rate[1];
        }
        return null;
    }

    public Float getMaxStarRating() {
        if (this.starRating != null) {
            return this.starRating[1];
        }
        return null;
    }

    public Float getMaxTripAdvisorRating() {
        if (this.tripAdvisorRating != null) {
            return this.tripAdvisorRating[1];
        }
        return null;
    }

    public Integer getMinRate() {
        if (this.rate != null) {
            return this.rate[0];
        }
        return null;
    }

    public Float getMinStarRating() {
        if (this.starRating != null) {
            return this.starRating[0];
        }
        return null;
    }

    public Float getMinTripAdvisorRating() {
        if (this.tripAdvisorRating != null) {
            return this.tripAdvisorRating[0];
        }
        return null;
    }

    public Pagination getPagination() {
        return this.pagination;
    }

    public List<String> getPropertyCategory() {
        return this.propertyCategory;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public SortType getSortType() {
        return this.sortType;
    }

    public void setPagination(final Pagination pagination) {
        this.pagination = pagination;
    }

    public void setPropertyCategory(final List<String> propertyCategory) {
        this.propertyCategory = propertyCategory;
    }

    public void setPropertyName(final String propertyName) {
        this.propertyName = propertyName;
    }

    public void setRate(final Integer[] rate) {
        this.rate = rate;
    }

    public void setSortType(final SortType sortType) {
        this.sortType = sortType;
    }

    public void setStarRating(final Float[] starRating) {
        this.starRating = starRating;
    }

    public void setTripAdvisorRating(final Float[] tripAdvisorRating) {
        this.tripAdvisorRating = tripAdvisorRating;
    }

}
