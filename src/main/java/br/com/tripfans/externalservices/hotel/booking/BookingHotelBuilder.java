package br.com.tripfans.externalservices.hotel.booking;

import org.apache.commons.lang3.StringUtils;

import us.codecraft.webmagic.selector.Selectable;

public class BookingHotelBuilder {

    public BookingHotel build(final Selectable selectable, final String hotelID) {
        final BookingHotel hotel = new BookingHotel(new Long(hotelID));

        fillName(selectable, hotel);
        fillFinalPrice(selectable, hotel);
        fillDiscountPercentage(selectable, hotel);
        fillOriginalPrice(selectable, hotel);

        // fillAddress(selectable, hotel);
        // fillBookingUrl(selectable, hotel);
        /*fillRanking(selectable, hotel);
        fillStars(selectable, hotel);
        fillPhotos(selectable, hotel);
        fillGeoLocation(selectable, hotel);*/
        return hotel;
    }

    private void fillAddress(final Selectable selectable, final BookingHotel hotel) {
        hotel.address = selectable.xpath("span[@id='hp_address_subtitle']/text()").toString();
    }

    /*private void fillBookingUrl(final Selectable selectable, final BookingHotel hotel) {
        hotel.url = selectable.getUrl().toString();
    }*/

    private void fillDiscountPercentage(final Selectable selectable, final BookingHotel hotel) {

        String valor = selectable.xpath("//div[@class=sr-prc--discount]/text()").toString();
        if (valor != null) {
            valor = valor.replaceAll("[^0-9]", "");
            hotel.discountPercentage = new Float(valor);
        }

        // hotel.discountPercentage = selectable.xpath("//div[@class=sr_smart_price]/text()");
    }

    private void fillFinalPrice(final Selectable selectable, final BookingHotel hotel) {
        String valor = selectable.xpath("//div[@class=sr-prc--final]/text()").toString();
        if (valor != null) {
            valor = valor.replaceAll("[^0-9]", "");
            hotel.finalPrice = new Float(valor);
        }
    }

    private void fillGeoLocation(final Selectable selectable, final BookingHotel hotel) {
        final Selectable latLong = selectable.regex("data-coords=\"[-+]?\\d+.\\d+,[-+]?\\d+.\\d+\"");
        if (latLong.match()) {
            final String ll = latLong.get().replace("data-coords=", "").replaceAll("\"", "");
            final String[] values = ll.split(",");
            hotel.latitude = Double.valueOf(values[1]);
            hotel.longitude = Double.valueOf(values[0]);
        }
    }

    private void fillName(final Selectable selectable, final BookingHotel hotel) {
        hotel.name = selectable.xpath("//span[@class=sr-hotel__name]/text()").toString().trim();
    }

    private void fillOriginalPrice(final Selectable selectable, final BookingHotel hotel) {
        String valor = selectable.xpath("//div[@class=sr-prc--crossed]/text()").toString();
        if (valor != null) {
            valor = valor.replaceAll("[^0-9]", "");
            hotel.originalPrice = new Float(valor);
        }
    }

    private void fillPhotos(final Selectable selectable, final BookingHotel hotel) {
        final Selectable photoSelectable = selectable.xpath("div[@id='photos_distinct']/a[1]");
        if (photoSelectable.match()) {
            final Selectable regex = photoSelectable.regex("href=\".+\"");
            /*if (regex.match()) {
                hotel.urlPhotoMedium = UtilUrlExtractor.extract(regex.get());
                hotel.urlPhotoBig = hotel.urlPhotoMedium.replace("max300", "840x460");
                hotel.urlPhotoSmall = hotel.urlPhotoMedium.replace("max300", "max200");
            }*/
        }
    }

    private void fillRanking(final Selectable selectable, final BookingHotel hotel) {
        final String ratingString = selectable.xpath("div[@id='reviewFloater']/div[1]/a/span[2]/span[1]/text()").toString();
        if (StringUtils.isNotEmpty(ratingString)) {
            hotel.ranking = Double.valueOf(ratingString.replace(",", "."));
        }
    }

    private void fillRoomDescription(final Selectable selectable, final BookingHotel hotel) {
        hotel.name = selectable.xpath("//a[@class=hotel_name_link]/text()").toString();
    }

    private void fillSelectRoomUrl(final Selectable selectable, final BookingHotel hotel) {
        hotel.selectRoomUrl = selectable.xpath("//a[@class=hotel_name_link]/span/text()").toString().trim();
    }

    private void fillStars(final Selectable selectable, final BookingHotel hotel) {
        final Selectable starsSelectable = selectable.xpath("div[@id='wrap-hotelselectable-top']/h1/span[2]/span/i");
        if (starsSelectable.match()) {
            final Selectable regex = starsSelectable.regex("title=\".+\"");
            if (regex.match()) {
                final String stars = regex.get().replaceAll("[^0-9]+", "");
                hotel.stars = Integer.valueOf(stars);
            }
        }
    }
}
