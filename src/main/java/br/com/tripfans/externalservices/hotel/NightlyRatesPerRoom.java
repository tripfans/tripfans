package br.com.tripfans.externalservices.hotel;

import java.io.Serializable;
import java.util.List;

import com.ean.client.v3.domain.NightlyRate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Contains details on the nightly rates for a room.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NightlyRatesPerRoom implements Serializable {

    @JsonProperty("NightlyRate")
    private List<NightlyRate> nightlyRates;

    @JsonProperty("@size")
    private int size;

    public List<NightlyRate> getNightlyRates() {
        return this.nightlyRates;
    }

    public int getSize() {
        return this.size;
    }

    public void setNightlyRates(final List<NightlyRate> nightlyRates) {
        this.nightlyRates = nightlyRates;
    }

    public void setSize(final int size) {
        this.size = size;
    }

}
