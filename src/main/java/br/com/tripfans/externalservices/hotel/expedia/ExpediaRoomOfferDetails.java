package br.com.tripfans.externalservices.hotel.expedia;

import br.com.tripfans.externalservices.hotel.RoomOfferDetails;
import br.com.tripfans.importador.HotelTripFans;

public class ExpediaRoomOfferDetails extends RoomOfferDetails {

    public ExpediaRoomOfferDetails(final HotelTripFans hotel) {
        super(hotel);
    }

    @Override
    public String getPartnerIconName() {
        return "expedia";
    }

    @Override
    public String getPartnerName() {
        return "Expedia";
    }

    @Override
    public Float getPartnerPercentageComission() {
        return 0.06F;
    }

}
