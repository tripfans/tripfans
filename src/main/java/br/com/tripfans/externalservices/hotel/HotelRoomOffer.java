package br.com.tripfans.externalservices.hotel;

import br.com.tripfans.importador.HotelTripFans;

public abstract class HotelRoomOffer<T> {

    protected HotelTripFans hotel;

    protected RoomOfferDetails roomOfferDetails;

    public HotelRoomOffer(final HotelTripFans hotel) {
        this.hotel = hotel;
    }

    public HotelTripFans getHotel() {
        return this.hotel;
    }

    public RoomOfferDetails getRoomOfferDetails() {
        return this.roomOfferDetails;
    }

    public abstract HotelRoomOffer<T> populateRoomOfferDetails(final T hotelObject);

    public void setHotel(final HotelTripFans hotel) {
        this.hotel = hotel;
    }

    public void setRoomOfferDetails(final RoomOfferDetails roomOfferDetails) {
        this.roomOfferDetails = roomOfferDetails;
    }

}
