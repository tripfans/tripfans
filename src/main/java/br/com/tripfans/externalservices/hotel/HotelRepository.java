package br.com.tripfans.externalservices.hotel;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.NotWritablePropertyException;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Repository;

import br.com.tripfans.importador.HotelTripFans;

@Repository("hotelTripFansRepository")
public class HotelRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Autowired
    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private HotelTripFans consultarHotel(final String sql, final String parameterName, final Long parameterValue) {

        final Map<String, Object> params = new HashMap<String, Object>();

        params.put(parameterName, parameterValue);

        try {

            return this.namedParameterJdbcTemplate.query(sql.toString(), params, new RowMapper<HotelTripFans>() {

                @Override
                public HotelTripFans mapRow(final ResultSet rs, final int rowNum) throws SQLException {

                    final HotelTripFans hotel = BeanUtils.instantiate(HotelTripFans.class);
                    final BeanWrapper bw = PropertyAccessorFactory.forBeanPropertyAccess(hotel);

                    bw.setAutoGrowNestedPaths(true);

                    final ResultSetMetaData meta_data = rs.getMetaData();
                    final int columnCount = meta_data.getColumnCount();

                    for (int index = 1; index <= columnCount; index++) {
                        try {
                            final String column = JdbcUtils.lookupColumnName(meta_data, index);
                            final Object value = JdbcUtils.getResultSetValue(rs, index, Class.forName(meta_data.getColumnClassName(index)));

                            if (value != null) {
                                if (column.startsWith("horarioCheck")) {
                                    bw.setPropertyValue(column, LocalTime.parse(value.toString(), DateTimeFormatter.ofPattern("HH:mm:ss")));
                                    // bw.setPropertyValue(column, org.joda.time.LocalTime.parse(value.toString(),
                                    // DateTimeFormat.forPattern("HH:mm:ss")));
                                } else {
                                    bw.setPropertyValue(column, value);
                                }
                            }

                        } catch (TypeMismatchException | NotWritablePropertyException | ClassNotFoundException e) {
                            // Ignore
                            e.printStackTrace();
                        }
                    }
                    return hotel;
                }
            }).get(0);

        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public HotelTripFans consultarHotelPorIdBooking(final Long idHotelBooking) {
        final StringBuffer sql = getSQLConsultaHotelTripFans();

        sql.append(" WHERE id_booking = :idHotelBooking ");
        sql.append(" ORDER BY id ");

        return consultarHotel(sql.toString(), "idHotelBooking", idHotelBooking);
    }

    public HotelTripFans consultarHotelPorIdEAN(final Long idHotelExpedia) {
        final StringBuffer sql = getSQLConsultaHotelTripFans();

        sql.append(" WHERE id_expedia = :idHotelExpedia");
        sql.append(" ORDER BY id ");

        return consultarHotel(sql.toString(), "idHotelExpedia", idHotelExpedia);

    }

    private StringBuffer getSQLConsultaHotelTripFans() {
        final StringBuffer sql = new StringBuffer();

        sql.append("SELECT ");
        sql.append("       id_hotel_tripfans AS \"id\", ");
        sql.append("       nome, ");
        sql.append("       endereco, ");
        sql.append("       latitude, ");
        sql.append("       longitude, ");
        sql.append("       estrelas, ");
        sql.append("       descricao, ");
        sql.append("       id_regiao_tripfans AS \"idRegiaoTripfans\", ");
        sql.append("       horario_checkin AS \"horarioCheckin\", ");
        sql.append("       horario_checkout AS \"horarioCheckout\", ");
        sql.append("       descricao_en AS \"descricaoEN\", ");
        sql.append("       descricao_pt AS \"descricaoPT\", ");
        sql.append("       descricao_es AS \"descricaoES\", ");
        sql.append("       descricao_fr AS \"descricaoFR\", ");
        sql.append("       id_expedia AS \"idExpedia\", ");
        sql.append("       region_expedia_id AS \"idRegiaoExpedia\", ");
        sql.append("       url_expedia AS \"urlExpedia\", ");
        sql.append("       categoria_expedia AS \"categoriaExpedia\", ");
        sql.append("       id_booking AS \"idBooking\", ");
        sql.append("       city_id_booking AS \"idCidadeBooking\", ");
        sql.append("       city_name_preferred_booking AS \"nomeCidadePreferenciaBooking\", ");
        sql.append("       city_name_unique_booking AS \"nomeUnicoCidadeBooking\", ");
        sql.append("       continent_id_booking AS \"idContinenteBooking\", ");
        sql.append("       min_rate_booking AS \"precoMinimoBooking\", ");
        sql.append("       max_rate_booking AS \"precoMaximoBooking\", ");
        sql.append("       numero_avaliacoes_booking AS \"numeroAvaliacoesBooking\", ");
        sql.append("       numero_quartos_booking AS \"numeroQuartosBooking\", ");
        sql.append("       url_booking AS \"urlBooking\", ");
        sql.append("       photo_url_booking AS \"urlFotoPadraoBooking\", ");
        sql.append("       preferencial_booking AS \"preferencialBooking\", ");
        sql.append("       ranking_booking AS \"rankingBooking\", ");
        sql.append("       pontuacao_avaliacoes_booking AS \"pontuacaoAvaliacoesBooking\", ");
        sql.append("       city_name_booking AS \"nomeCidadeBooking\", ");
        sql.append("       codigo_pais AS \"codigoPais\", ");
        sql.append("       codigo_moeda AS \"moeda\", ");
        sql.append("       codigo_postal AS \"codigoPostal\", ");
        sql.append("       observacao ");
        sql.append("  FROM hotel_tripfans ");

        return sql;
    }

}
