package br.com.tripfans.externalservices.hotel;

import java.io.Serializable;

import com.ean.wsapi.hotel.v3.BaseRateInfoDiscounts;
import com.ean.wsapi.hotel.v3.BaseRateInfoNightlyRatesPerRoom;
import com.ean.wsapi.hotel.v3.BaseRateInfoSurcharges;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Contains the details on the chargeable rate.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseRateInfo implements Serializable {

    private Float averageBaseRate;

    private Float averageRate;

    private Float commissionableUsdTotal;

    private String currencyCode;

    protected BaseRateInfoDiscounts discounts;

    protected Float discountTotal;

    protected Float eanCompensationOffline;

    protected Float eanCompensationOnline;

    protected Float grossProfitOffline;

    protected Float grossProfitOnline;

    private Float maxNightlyRate;

    protected BaseRateInfoNightlyRatesPerRoom nightlyRatesPerRoom;

    private Float nightlyRateTotal;

    protected String promoDescription;

    protected BaseRateInfoSurcharges surcharges;

    private Float surchargeTotal;

    private Float total;

    /**
     * Returns the averageBaseRate for the ChargeableRateInfo
     *
     * @return Float for the averageBaseRate
     */
    public Float getAverageBaseRate() {
        return this.averageBaseRate;
    }

    /**
     * Returns the averageRate for the ChargeableRateInfo
     *
     * @return Float for the averageRate
     */
    public Float getAverageRate() {
        return this.averageRate;
    }

    /**
     * Returns the commissionableUsdTotal for the ChargeableRateInfo
     *
     * @return Float for the commissionableUsdTotal
     */
    public Float getCommissionableUsdTotal() {
        return this.commissionableUsdTotal;
    }

    /**
     * Returns the currencyCode for the ChargeableRateInfo
     *
     * @return String for the currencyCode
     */
    public String getCurrencyCode() {
        return this.currencyCode;
    }

    public BaseRateInfoDiscounts getDiscounts() {
        return this.discounts;
    }

    public Float getDiscountTotal() {
        return this.discountTotal;
    }

    public Float getEanCompensationOffline() {
        return this.eanCompensationOffline;
    }

    public Float getEanCompensationOnline() {
        return this.eanCompensationOnline;
    }

    public Float getGrossProfitOffline() {
        return this.grossProfitOffline;
    }

    public Float getGrossProfitOnline() {
        return this.grossProfitOnline;
    }

    /**
     * Returns the maxNightlyRate for the ChargeableRateInfo
     *
     * @return Float for the maxNightlyRate
     */
    public Float getMaxNightlyRate() {
        return this.maxNightlyRate;
    }

    /**
     * Returns the nightlyRatesPerRoom for the ChargeableRateInfo
     *
     * @return NightlyRatesPerRoom for the nightlyRatesPerRoom
     */
    public BaseRateInfoNightlyRatesPerRoom getNightlyRatesPerRoom() {
        return this.nightlyRatesPerRoom;
    }

    /**
     * Returns the nightlyRateTotal for the ChargeableRateInfo
     *
     * @return Float for the nightlyRateTotal
     */
    public Float getNightlyRateTotal() {
        return this.nightlyRateTotal;
    }

    public String getPromoDescription() {
        return this.promoDescription;
    }

    /**
     * Returns the surcharges for the ChargeableRateInfo
     *
     * @return Surcharges for the surcharges
     */
    public BaseRateInfoSurcharges getSurcharges() {
        return this.surcharges;
    }

    /**
     * Returns the surchargeTotal for the ChargeableRateInfo
     *
     * @return Float for the surchargeTotal
     */
    public Float getSurchargeTotal() {
        return this.surchargeTotal;
    }

    /**
     * Returns the total for the ChargeableRateInfo
     *
     * @return Float for the total
     */
    public Float getTotal() {
        return this.total;
    }

    public void setAverageBaseRate(final Float averageBaseRate) {
        this.averageBaseRate = averageBaseRate;
    }

    public void setAverageRate(final Float averageRate) {
        this.averageRate = averageRate;
    }

    public void setCommissionableUsdTotal(final Float commissionableUsdTotal) {
        this.commissionableUsdTotal = commissionableUsdTotal;
    }

    public void setCurrencyCode(final String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public void setDiscounts(final BaseRateInfoDiscounts discounts) {
        this.discounts = discounts;
    }

    public void setDiscountTotal(final Float discountTotal) {
        this.discountTotal = discountTotal;
    }

    public void setEanCompensationOffline(final Float eanCompensationOffline) {
        this.eanCompensationOffline = eanCompensationOffline;
    }

    public void setEanCompensationOnline(final Float eanCompensationOnline) {
        this.eanCompensationOnline = eanCompensationOnline;
    }

    public void setGrossProfitOffline(final Float grossProfitOffline) {
        this.grossProfitOffline = grossProfitOffline;
    }

    public void setGrossProfitOnline(final Float grossProfitOnline) {
        this.grossProfitOnline = grossProfitOnline;
    }

    public void setMaxNightlyRate(final Float maxNightlyRate) {
        this.maxNightlyRate = maxNightlyRate;
    }

    public void setNightlyRatesPerRoom(final BaseRateInfoNightlyRatesPerRoom nightlyRatesPerRoom) {
        this.nightlyRatesPerRoom = nightlyRatesPerRoom;
    }

    public void setNightlyRateTotal(final Float nightlyRateTotal) {
        this.nightlyRateTotal = nightlyRateTotal;
    }

    public void setPromoDescription(final String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public void setSurcharges(final BaseRateInfoSurcharges surcharges) {
        this.surcharges = surcharges;
    }

    public void setSurchargeTotal(final Float surchargeTotal) {
        this.surchargeTotal = surchargeTotal;
    }

    public void setTotal(final Float total) {
        this.total = total;
    }

}
