package br.com.tripfans.externalservices.hotel.booking;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Selectable;
import br.com.tripfans.externalservices.hotel.Filter;
import br.com.tripfans.externalservices.hotel.HotelService;
import br.com.tripfans.externalservices.hotel.Room;
import br.com.tripfans.externalservices.hotel.Rooms;

public class BookingHotelSearchProcessor implements PageProcessor {

    private static final String BOOKING_SEARCH_URL = "http://www.booking.com/searchresults.pt-br.html";

    public static void main(final String[] args) {
        final BookingHotelSearchProcessor bookingHotelSearchProcessor = new BookingHotelSearchProcessor();
        bookingHotelSearchProcessor.search("-671824", null, null, null, null);
    }

    private List<BookingHotel> results = new ArrayList<BookingHotel>();

    private final Site site = Site.me().setRetryTimes(3).setSleepTime(1000);

    public List<BookingHotel> getResults() {
        return this.results;
    }

    @Override
    public Site getSite() {
        return this.site;
    }

    @Override
    public void process(final Page page) {
        if (page.getUrl().toString().contains("searchresults") && page.getHtml().xpath("div[@id='search_results_table']").match()) {

            final Selectable hotelList = page.getHtml().xpath("div[@id='hotellist_inner']/div");

            for (final Selectable hotelDiv : hotelList.nodes()) {
                final String hotelID = hotelDiv.xpath("div[@class='sr_item']/@data-hotelid").get();
                if (hotelID != null) {
                    page.putField(hotelID, new BookingHotelBuilder().build(hotelDiv, hotelID));
                }
            }
        }
    }

    public void search(final String bookingCityID, final String arrivalDate, final String departureDate, final Rooms rooms, final Filter filter) {

        final DateTimeFormatter formatterRead = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        final LocalDate dataInicio = LocalDate.parse(arrivalDate, formatterRead);
        final LocalDate dataFim = LocalDate.parse(departureDate, formatterRead);

        /* Forçando alguns Headers para simular um cliente browser Desktop.
         * Isso porque os resultados vinham diferentes sem esses cabeçalhos. */
        this.site.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
        this.site.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        this.site.addHeader("Accept-Encoding", "gzip, deflate, sdch");
        this.site.addHeader("Accept-Language", "pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4");
        this.site.addHeader("Cache-Control", "max-age=0");
        this.site.addHeader("Host", "www.booking.com");
        this.site.addHeader("Proxy-Connection", "keep-alive");
        this.site.addHeader("Upgrade-Insecure-Requests", "1");

        final BookingHotelPipeline pipeline = new BookingHotelPipeline();

        final StringBuffer url = new StringBuffer(BOOKING_SEARCH_URL);

        // City
        url.append("?src=city&nflt=&ss_raw=&city=" + bookingCityID);
        // Checkin
        url.append("&checkin_monthday=" + dataInicio.format(DateTimeFormatter.ofPattern("d")));
        url.append("&checkin_year_month=" + dataInicio.format(DateTimeFormatter.ofPattern("yyyy-M")));
        // Checkout
        url.append("&checkout_monthday=" + dataFim.format(DateTimeFormatter.ofPattern("d")));
        url.append("&checkout_year_month=" + dataFim.format(DateTimeFormatter.ofPattern("yyyy-M")));

        // Rooms
        if (rooms.getRooms() != null) {
            // Rooms Number
            url.append("&no_rooms=" + rooms.getRooms().size());

            // Cada adulto equivale a 1 "A" - no minimo 1 adulto
            String roomUrl = "A";

            int allAdults = 0;
            final List<Integer> allChildAges = new ArrayList();

            for (final Room room : rooms.getRooms()) {
                allAdults += room.getNumberOfAdults();
                if (room.getChildAges() != null) {
                    allChildAges.addAll(room.getChildAges());
                }
            }

            // Adicionar os demais adultos
            for (int i = 1; i < allAdults; i++) {
                roomUrl += ",A";
            }
            url.append("&group_adults=" + allAdults);

            // Adicionar as idades das crianças
            if (allChildAges != null && !allChildAges.isEmpty()) {
                for (int i = 0; i < allChildAges.size(); i++) {
                    roomUrl += "," + allChildAges.get(i);
                }
                url.append("&group_children=" + allChildAges.size());
            }

            try {
                url.append("&room1=" + URLEncoder.encode(roomUrl, "UTF-8"));
            } catch (final UnsupportedEncodingException e) {
            }
        }

        // Sort
        if (filter.getSortType() != null) {
            url.append("&order=" + filter.getSortType().getBookingSortValue());
        }

        // Filters
        String filters = "";

        // Estrelas
        if (filter.getMinStarRating() != null && filter.getMaxStarRating() != null) {
            for (int i = filter.getMinStarRating().intValue(); i <= filter.getMaxStarRating(); i++) {
                filters += "class=" + i + ";";
            }
        }

        // Preço
        // TODO - Ver como fazer. Parece que são 4 faixas de preços que ele calcula de acordo com os resultados
        // Ex: pri=2;pri=3;

        // Avaliações de usuários
        // TODO - Ver como fazer. Parece que tb são quatro opções. de 60 ou mais até 90 ou mais, de 10 em 10
        // review_score=90;review_score=80;review_score=70;

        if (filters != "") {
            try {
                url.append("&nflt=" + URLEncoder.encode(filters, "UTF-8"));
            } catch (final UnsupportedEncodingException e) {
            }
        }

        // Paginação: Testar
        url.append("&rows=" + HotelService.DEFAULT_NUMBER_OF_RESULTS);
        if (filter.getPagination() != null && filter.getPagination().getPage() != null) {
            url.append("&offset="
                    + ((filter.getPagination().getPage() * HotelService.DEFAULT_NUMBER_OF_RESULTS) - HotelService.DEFAULT_NUMBER_OF_RESULTS));
        }

        url.append("&lp_bdp=1");
        url.append("&tfl_cwh=1");

        /*
        &highlighted_hotels=
         */

        final Spider spider = Spider.create(this).addUrl(url.toString()).thread(1).addPipeline(pipeline);
        spider.run();

        this.results = pipeline.getResults();

    }

}
