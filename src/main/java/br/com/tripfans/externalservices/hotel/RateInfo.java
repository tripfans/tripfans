package br.com.tripfans.externalservices.hotel;

import java.io.Serializable;
import java.util.List;

/**
 * Contains rate information.
 */
public class RateInfo implements Serializable {

    // protected RoomGroup roomGroup;

    protected String cancellationPolicy;

    protected BaseRateInfo chargeableRateInfo;

    protected List<String> confirmationNumbers;

    protected BaseRateInfo convertedRateInfo;

    // protected PromoType promoType;
    protected Integer currentAllotment;

    protected Boolean depositRequired;

    protected Boolean guaranteeRequired;

    // protected CancelPolicyInfoList cancelPolicyInfoList;
    protected Boolean nonRefundable;

    protected Boolean online;

    protected String pkgSavingsAmount;

    protected String pkgSavingsPercent;

    protected Boolean priceBreakdown;

    protected Boolean promo;

    protected String promoDescription;

    protected String promoDetailText;

    protected String promoId;

    protected Boolean rateChange;

    // protected Deposit deposit;
    protected String taxRate;

    public String getCancellationPolicy() {
        return this.cancellationPolicy;
    }

    public BaseRateInfo getChargeableRateInfo() {
        return this.chargeableRateInfo;
    }

    public List<String> getConfirmationNumbers() {
        return this.confirmationNumbers;
    }

    public BaseRateInfo getConvertedRateInfo() {
        return this.convertedRateInfo;
    }

    public Integer getCurrentAllotment() {
        return this.currentAllotment;
    }

    public Boolean getDepositRequired() {
        return this.depositRequired;
    }

    public Boolean getGuaranteeRequired() {
        return this.guaranteeRequired;
    }

    public Float getNightlyRateTotal() {
        if (this.chargeableRateInfo != null) {
            return this.chargeableRateInfo.getNightlyRateTotal();
        }
        return null;
    }

    public Boolean getNonRefundable() {
        return this.nonRefundable;
    }

    public Boolean getOnline() {
        return this.online;
    }

    public String getPkgSavingsAmount() {
        return this.pkgSavingsAmount;
    }

    public String getPkgSavingsPercent() {
        return this.pkgSavingsPercent;
    }

    public Boolean getPriceBreakdown() {
        return this.priceBreakdown;
    }

    public Boolean getPromo() {
        return this.promo;
    }

    public String getPromoDescription() {
        return this.promoDescription;
    }

    public String getPromoDetailText() {
        return this.promoDetailText;
    }

    public String getPromoId() {
        return this.promoId;
    }

    public Boolean getRateChange() {
        return this.rateChange;
    }

    public String getTaxRate() {
        return this.taxRate;
    }

    public void setCancellationPolicy(final String cancellationPolicy) {
        this.cancellationPolicy = cancellationPolicy;
    }

    public void setChargeableRateInfo(final BaseRateInfo chargeableRateInfo) {
        this.chargeableRateInfo = chargeableRateInfo;
    }

    public void setConfirmationNumbers(final List<String> confirmationNumbers) {
        this.confirmationNumbers = confirmationNumbers;
    }

    public void setConvertedRateInfo(final BaseRateInfo convertedRateInfo) {
        this.convertedRateInfo = convertedRateInfo;
    }

    public void setCurrentAllotment(final Integer currentAllotment) {
        this.currentAllotment = currentAllotment;
    }

    public void setDepositRequired(final Boolean depositRequired) {
        this.depositRequired = depositRequired;
    }

    // protected RatePlanType ratePlanType;

    // protected HotelFees hotelFees;
    // protected RatePlanType rateType;

    public void setGuaranteeRequired(final Boolean guaranteeRequired) {
        this.guaranteeRequired = guaranteeRequired;
    }

    public void setNonRefundable(final Boolean nonRefundable) {
        this.nonRefundable = nonRefundable;
    }

    public void setOnline(final Boolean online) {
        this.online = online;
    }

    public void setPkgSavingsAmount(final String pkgSavingsAmount) {
        this.pkgSavingsAmount = pkgSavingsAmount;
    }

    public void setPkgSavingsPercent(final String pkgSavingsPercent) {
        this.pkgSavingsPercent = pkgSavingsPercent;
    }

    public void setPriceBreakdown(final Boolean priceBreakdown) {
        this.priceBreakdown = priceBreakdown;
    }

    public void setPromo(final Boolean promo) {
        this.promo = promo;
    }

    public void setPromoDescription(final String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public void setPromoDetailText(final String promoDetailText) {
        this.promoDetailText = promoDetailText;
    }

    public void setPromoId(final String promoId) {
        this.promoId = promoId;
    }

    public void setRateChange(final Boolean rateChange) {
        this.rateChange = rateChange;
    }

    public void setTaxRate(final String taxRate) {
        this.taxRate = taxRate;
    }

}
