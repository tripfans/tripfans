package br.com.tripfans.externalservices.hotel.hoteis;

import br.com.tripfans.externalservices.hotel.RoomOfferDetails;
import br.com.tripfans.importador.HotelTripFans;

public class HoteisRoomOfferDetails extends RoomOfferDetails {

    public HoteisRoomOfferDetails(final HotelTripFans hotel) {
        super(hotel);
    }

    @Override
    public String getPartnerIconName() {
        return "hoteis";
    }

    @Override
    public String getPartnerName() {
        return "Hoteis.com";
    }

    @Override
    public Float getPartnerPercentageComission() {
        return 0.06F;
    }

}
