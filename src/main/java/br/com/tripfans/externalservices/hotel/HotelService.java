package br.com.tripfans.externalservices.hotel;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fanaticosporviagens.infra.controller.JSONResponse;
import br.com.fanaticosporviagens.locais.LocalService;
import br.com.tripfans.externalservices.hotel.booking.BookingHotel;
import br.com.tripfans.externalservices.hotel.booking.BookingHotelRoomOffer;
import br.com.tripfans.externalservices.hotel.booking.BookingHotelSearchProcessor;
import br.com.tripfans.externalservices.hotel.expedia.ExpediaHotelRoomOffer;
import br.com.tripfans.externalservices.hotel.expedia.ExpediaResponseData;
import br.com.tripfans.externalservices.hotel.expedia.ExpediaRoomOfferDetails;
import br.com.tripfans.externalservices.hotel.hoteis.HoteisHotelRoomOffer;
import br.com.tripfans.importador.HotelTripFans;
import br.com.tripfans.importador.RegiaoTripFans;

import com.ean.wsapi.hotel.v3.HotelListRequest;
import com.ean.wsapi.hotel.v3.HotelListResponse;
import com.ean.wsapi.hotel.v3.HotelSummary;
import com.ean.wsapi.hotel.v3.LocaleType;
import com.ean.wsapi.hotel.v3.SortType;
import com.geeoz.ean.dsl.GetHotelListBuilder;
import com.geeoz.ean.ws.EanWsException;

@RestController
@RequestMapping(value = "/hoteis")
public class HotelService {

    // Se esse valor for alterado, deverá ser alterado na view também (constante no arquivo app.js)
    public static int DEFAULT_NUMBER_OF_RESULTS = 20;

    @Autowired
    private HotelRepository hotelTripFansRepository;

    @Autowired
    private LocalService localService;

    /*@RequestMapping(value = "/search/{city}/{stateProvinceCode}/{countryCode}", method = RequestMethod.GET)
    public JSONResponse consultarHoteis(@PathVariable final String city, @PathVariable final String stateProvinceCode,
            @PathVariable final String countryCode, @RequestParam final String arrivalDate, @RequestParam final String departureDate,
            @RequestParam final int adults, @RequestParam final int children) throws EanWsException {
        final GetHotelListBuilder hotelListBuilder = createHotelListBuilder(arrivalDate, departureDate, adults, children, 20);

        return new JSONResponse(hotelListBuilder.search(city, stateProvinceCode, countryCode).call().getHotelList());
    }*/

    @RequestMapping(value = "/moreResults/{cacheKey}/{cacheLocation}/{supplierType}", method = RequestMethod.GET)
    public JSONResponse carregarMaisHoteis(@PathVariable final String cacheKey, @PathVariable final String cacheLocation,
            @PathVariable final String supplierType) throws EanWsException {

        final GetHotelListBuilder hotelListBuilder = new GetHotelListBuilder();
        hotelListBuilder.request().setCacheKey(cacheKey);
        hotelListBuilder.request().setCacheLocation(cacheLocation);
        hotelListBuilder.request().setSupplierType(supplierType);

        return new JSONResponse(hotelListBuilder.call());
    }

    private HotelSearchResponseData combinarResultadosPesquisaHoteis(final Filter filter, final ExpediaResponseData expediaResponseData,
            final List<HotelRoomOffer> bookingHotelsRoomOffersList) {

        // Lista para agrupar as ofertas
        final List<HotelRoomOffer> hotelRoomOffersList = new ArrayList<HotelRoomOffer>();

        // Adicionando as ofertas da Expedia
        hotelRoomOffersList.addAll(expediaResponseData.getHotelRoomOffers());
        // Adicionando as ofertas do Booking.com
        hotelRoomOffersList.addAll(bookingHotelsRoomOffersList);

        final Map<HotelTripFans, List<RoomOfferDetails>> map =
        // Juntando todas as listas de ofertas em uma só
                Stream.of(hotelRoomOffersList)
                .flatMap(hotelRoomOffers -> hotelRoomOffers.stream())
                // Agrupando ofertas pelo hotel e criando um Map (a chave será o hotel)
                .collect(
                        Collectors.groupingBy(hotelRoomOffer -> hotelRoomOffer.getHotel(),
                                Collectors.mapping(hotelRoomOffer -> hotelRoomOffer.getRoomOfferDetails(), Collectors.toList())));

        // Lista para consolidar tudo e ordenar os resultados
        final List<HotelSearchResult> results = new ArrayList<HotelSearchResult>();

        // Convertendo o resultado agrupamento (Map) para uma lista (List)
        map.entrySet().stream().forEach(obj -> {
            // Aplicando a forma de ordenação escolhida para as ofertas de cada hotel (coloca a "melhor oferta" em primeiro lugar)
            obj.getValue().sort(filter.getSortType().getComparator());
            results.add(new HotelSearchResult(obj.getKey(), obj.getValue()));
        });

        // TODO ordenar todas as ofertas de acordo com a ordenação escolhida e considerando a "Melhor oferta" de cada hotel
        // O que foi feito acima, ordena somente as ofertas de cada hotel coloca em primeiro lugar a "melhor oferta"
        results.sort(new SearchResultComparator());

        // Retorna a resposta com a lista final e os dados de resposta da Expedia (Para fins de controle de paginação, etc)
        return new HotelSearchResponseData(results, expediaResponseData);
    }

    private void configureFilters(final Filter filter, final HotelListRequest request) {

        request.setMaxRate(filter.getMaxRate());
        if (filter.getMinRate() != null && filter.getMinRate() < filter.getMaxRate()) {
            request.setMinRate(filter.getMinRate());
        }
        request.setMaxStarRating(filter.getMaxStarRating());
        if (filter.getMinStarRating() != null && filter.getMinStarRating() < filter.getMaxStarRating()) {
            request.setMinStarRating(filter.getMinStarRating());
        }
        request.setMaxTripAdvisorRating(filter.getMaxTripAdvisorRating());
        if (filter.getMinTripAdvisorRating() != null && filter.getMinTripAdvisorRating() < filter.getMaxTripAdvisorRating()) {
            request.setMinTripAdvisorRating(filter.getMinTripAdvisorRating());
        }

        request.setSort(filter.getSortType() != null ? SortType.valueOf(filter.getSortType().name()) : null);
        request.setPropertyName(filter.getPropertyName());

        if (filter.getPropertyCategory() != null) {
            for (final String propertyCategory : filter.getPropertyCategory()) {
                request.getPropertyCategory().add(propertyCategory);

            }
        }
    }

    private void configurePagination(final Filter filter, final HotelListRequest request) {
        // Must send with value 'E' to allow paging system to accurately indicate additional results.
        request.setSupplierType("E");

        request.setCacheKey(filter.getPagination().getCacheKey());
        request.setCacheLocation(filter.getPagination().getCacheLocation());
    }

    @RequestMapping(value = "/search/{idRegiaoTripFans}", method = RequestMethod.GET)
    public JSONResponse consultarHoteis(@PathVariable final Long idRegiaoTripFans, @RequestParam final String arrivalDate,
            @RequestParam final String departureDate, final Rooms rooms, final Filter filter) throws EanWsException, InterruptedException,
            ExecutionException {

        // Recuperar Região Tripfans
        final RegiaoTripFans regiaoTripFans = this.localService.consultarRegiaoPorId(idRegiaoTripFans);

        final CompletableFuture<ExpediaResponseData> expediaHotelSearchFuture = new CompletableFuture<ExpediaResponseData>();
        CompletableFuture<ExpediaResponseData> hoteisHotelSearchFuture = new CompletableFuture<ExpediaResponseData>();

        /* Consultas de hoteis de diversos parceiros executadas paralelamente */

        if (regiaoTripFans.getIdExpedia() != null) {
            /* -- Expedia.com -- */
            expediaHotelSearchFuture.exceptionally(ex -> {
                ex.printStackTrace();
                return null;
            });

            CompletableFuture.runAsync(() -> {
                try {
                    final ExpediaResponseData expediaResponseData = consultarHoteisExpedia(regiaoTripFans.getNomeIngles(),
                            regiaoTripFans.getCodigoRegiaoExpedia(), regiaoTripFans.getSiglaISOPais(), arrivalDate, departureDate, rooms, filter);
                    expediaHotelSearchFuture.complete(expediaResponseData);

                } catch (final EanWsException ex) {
                    expediaHotelSearchFuture.completeExceptionally(ex);
                }
            });
            /* ---- */

            /* -- Hoteis.com -- */
            hoteisHotelSearchFuture = expediaHotelSearchFuture.thenApply(t -> consultarHoteisHoteis(t));
            /* ---- */
        }

        CompletableFuture<List<HotelRoomOffer>> bookingHotelSearchFuture = new CompletableFuture<List<HotelRoomOffer>>();

        if (regiaoTripFans.getIdBooking() != null) {
            /* -- Bookig.com -- */
            bookingHotelSearchFuture = CompletableFuture.supplyAsync(() -> consultarHoteisBooking(regiaoTripFans.getIdBooking(), arrivalDate,
                    departureDate, rooms, filter));
            /* ---- */
        }

        /* Juntar todos os resultados e ordernar pelos que melhor atendem os parametros da consulta solicitada */
        final CompletableFuture hotelSearchFuture = hoteisHotelSearchFuture.thenCombine(bookingHotelSearchFuture,
                (result1, result2) -> combinarResultadosPesquisaHoteis(filter, result1, result2));

        return new JSONResponse(hotelSearchFuture.get());
    }

    private List<HotelRoomOffer> consultarHoteisBooking(final String idCityBooking, final String arrivalDate, final String departureDate,
            final Rooms rooms, final Filter filter) {

        final BookingHotelSearchProcessor bookingHotelSearchProcessor = new BookingHotelSearchProcessor();
        bookingHotelSearchProcessor.search(idCityBooking, arrivalDate, departureDate, rooms, filter);

        return createHotelSearchResult(bookingHotelSearchProcessor.getResults(), arrivalDate, departureDate);
    }

    private ExpediaResponseData consultarHoteisExpedia(final String city, final String stateProvinceCode, final String countryCode,
            final String arrivalDate, final String departureDate, final Rooms rooms, final Filter filter) throws EanWsException {

        final GetHotelListBuilder hotelListBuilder = createHotelListBuilder(arrivalDate, departureDate, rooms, filter);

        return createHotelSearchResult(hotelListBuilder.search(city, stateProvinceCode, countryCode).call());
    }

    private ExpediaResponseData consultarHoteisHoteis(final ExpediaResponseData expediaResponseData) {

        final List<HotelRoomOffer> listaHoteis = new ArrayList<HotelRoomOffer>();

        expediaResponseData.getHotelRoomOffers().stream().forEach(obj -> {
            listaHoteis.add(new HoteisHotelRoomOffer(obj.getHotel(), (ExpediaRoomOfferDetails) obj.getRoomOfferDetails()));
        });

        expediaResponseData.getHotelRoomOffers().addAll(listaHoteis);

        return expediaResponseData;
    }

    private GetHotelListBuilder createHotelListBuilder(final String arrivalDate, final String departureDate, final Rooms rooms, final Filter filter) {

        final GetHotelListBuilder hotelListBuilder = new GetHotelListBuilder();

        if (filter.getPagination() != null) {
            configurePagination(filter, hotelListBuilder.request());
        } else {

            hotelListBuilder.dates(arrivalDate, departureDate).numberOfResults(DEFAULT_NUMBER_OF_RESULTS).locale(LocaleType.PT_BR).currency("BRL");

            if (rooms != null && rooms.getRooms() != null) {
                for (final Room room : rooms.getRooms()) {
                    hotelListBuilder.room(room.getNumberOfAdults(),
                            room.getChildAges() != null ? ArrayUtils.toPrimitive(room.getChildAges().toArray(new Integer[0])) : new int[0]);
                }
            }

            if (filter != null) {
                configureFilters(filter, hotelListBuilder.request());
            }
        }

        return hotelListBuilder;
    }

    private ExpediaResponseData createHotelSearchResult(final HotelListResponse hotelListResponse) {

        final ExpediaResponseData expediaResponseData = new ExpediaResponseData();

        final List<HotelRoomOffer> result = new ArrayList<HotelRoomOffer>();

        expediaResponseData.setCachedSupplierResponse(hotelListResponse.getCachedSupplierResponse());
        expediaResponseData.setCacheKey(hotelListResponse.getCacheKey());
        expediaResponseData.setCacheLocation(hotelListResponse.getCacheLocation());
        expediaResponseData.setMoreResultsAvailable(hotelListResponse.isMoreResultsAvailable());
        expediaResponseData.setNumberOfRoomsRequested(hotelListResponse.getNumberOfRoomsRequested());

        if (!hotelListResponse.getHotelList().getHotelSummary().isEmpty()) {

            for (final HotelSummary hotelSummary : hotelListResponse.getHotelList().getHotelSummary()) {

                final HotelTripFans hotel = this.hotelTripFansRepository.consultarHotelPorIdEAN(hotelSummary.getHotelId());

                if (hotel != null) {
                    final HotelRoomOffer hotelRoomOffer = new ExpediaHotelRoomOffer(hotel);

                    if (!hotelSummary.getRoomRateDetailsList().getRoomRateDetails().isEmpty()) {
                        hotelRoomOffer.populateRoomOfferDetails(hotelSummary.getRoomRateDetailsList().getRoomRateDetails().get(0));
                    }
                    result.add(hotelRoomOffer);
                } else {
                    System.out.println("O hotel " + hotelSummary.getName() + " [" + hotelSummary.getHotelId()
                            + "] da Expedia, não foi encontrado na base TF.");
                }
            }

        }

        expediaResponseData.setHotelRoomOffers(result);

        return expediaResponseData;
    }

    private List<HotelRoomOffer> createHotelSearchResult(final List<BookingHotel> hotelList, final String arrivalDate, final String departureDate) {

        final Integer dias = Period.between(parseDate(arrivalDate), parseDate(departureDate)).getDays();

        final List<HotelRoomOffer> result = new ArrayList<HotelRoomOffer>();

        if (!hotelList.isEmpty()) {

            for (final BookingHotel bookingHotel : hotelList) {

                final HotelTripFans hotel = this.hotelTripFansRepository.consultarHotelPorIdBooking(bookingHotel.getId());

                if (hotel != null) {

                    final HotelRoomOffer hotelRoomOffer = new BookingHotelRoomOffer(hotel, dias);
                    hotelRoomOffer.populateRoomOfferDetails(bookingHotel);

                    // Verificar se o preço veio na resposta e ignorá-los. Isso porque as vezes tem alguns hoteis que não possuem preço
                    if (hotelRoomOffer.getRoomOfferDetails().getNightlyRateTotal() != null) {
                        result.add(hotelRoomOffer);
                    }

                } else {
                    System.out.println("O hotel " + bookingHotel.getName() + " [" + bookingHotel.getId()
                            + "] da Expedia, não foi encontrado na base TF.");
                }
            }
        }

        return result;
    }

    private String getUrl(final String parceiro, final String hotelId, final String dataInicio, final String dataFim, final Rooms rooms)
            throws UnsupportedEncodingException {
        if (parceiro != null) {
            if (parceiro.equalsIgnoreCase("expedia")) {
                return getUrlExpedia(hotelId, dataInicio, dataFim, rooms);
            } else if (parceiro.equalsIgnoreCase("hoteis")) {
                return getUrlHoteis(hotelId, dataInicio, dataFim, rooms);
            }
        }
        return null;
    }

    /**
     * Constroi a URL para redirecionar para Expedia.com
     */
    private String getUrlExpedia(final String hotelId, final String dataInicio, final String dataFim, final Rooms rooms)
            throws UnsupportedEncodingException {

        // URL base
        final String urlBase = "http://www.dpbolvw.net/click-7944491-10479934-1420669072000";

        // URL a ser redirecionada
        String urlExpedia = "https://www.expedia.com.br/h" + hotelId + ".Hotel-Reservas";

        // Datas - MM/DD/YYYY
        urlExpedia += "?chkin=" + dataInicio + "&chkout=" + dataFim;

        // Lingua
        urlExpedia += "&langid=1046";

        // Quartos
        if (rooms != null && rooms.getRooms() != null) {
            String urlQuartos = "";
            int numeroQuarto = 1;
            for (final Room room : rooms.getRooms()) {
                urlQuartos += "&rm" + numeroQuarto + "=a" + room.getNumberOfAdults();
                if (room.getChildAges() != null && !room.getChildAges().isEmpty()) {
                    for (final Integer idadeCrianca : room.getChildAges()) {
                        urlQuartos += ":c" + idadeCrianca;
                    }
                }
                numeroQuarto++;
            }

            urlExpedia += urlQuartos;
        }

        return urlBase + "?url=" + URLEncoder.encode(urlExpedia, "UTF-8") + "#rooms-and-rates";
    }

    /**
     * Constroi a URL para redirecionar para Hoteis.com
     */
    private String getUrlHoteis(final String hotelId, final String dataInicio, final String dataFim, final Rooms rooms)
            throws UnsupportedEncodingException {

        // URL base
        final String urlBase = "https://ad.zanox.com/ppc/?35940003C1042123267";

        // URL a ser redirecionada
        String urlHoteis = "http://www.hoteis.com/ho" + hotelId;

        // Datas - MM/DD/YYYY
        urlHoteis += "/?q-localised-check-in=" + URLEncoder.encode(dataInicio, "UTF-8") + "&q-localised-check-out="
                + URLEncoder.encode(dataFim, "UTF-8");

        // Quartos
        String urlQuartos = "";
        if (rooms != null && rooms.getRooms() != null) {
            int numeroQuarto = 0;
            for (final Room room : rooms.getRooms()) {

                urlQuartos += "&q-room-" + numeroQuarto + "-adults=" + room.getNumberOfAdults();

                if (room.getChildAges() != null && !room.getChildAges().isEmpty()) {
                    urlQuartos += "&q-room-" + numeroQuarto + "-children=" + room.getChildAges().size();
                    int numeroCrianca = 0;
                    for (final Integer idadeCrianca : room.getChildAges()) {
                        urlQuartos += "&q-room-" + numeroQuarto + "-child-" + numeroCrianca + "-age=" + idadeCrianca;
                        numeroCrianca++;
                    }
                }
                numeroQuarto++;
            }

            urlHoteis += urlQuartos;
        }

        return urlBase + "&ULP=[[" + urlHoteis + "&tab=description" + "]]";
    }

    private LocalDate parseDate(final String date) {
        final DateTimeFormatter formatterRead = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        return LocalDate.parse(date, formatterRead);
    }

    @RequestMapping(value = "/verOferta/{parceiro}/{hotelId}", method = RequestMethod.GET)
    public JSONResponse verOferta(@PathVariable final String parceiro, @PathVariable final String hotelId, @RequestParam final String arrivalDate,
            @RequestParam final String departureDate, @RequestParam(required = false) final String directLink, final Rooms rooms)
            throws EanWsException, UnsupportedEncodingException {

        if (directLink != null) {
            return new JSONResponse(directLink);
        }

        final LocalDate dataInicio = parseDate(arrivalDate);
        final LocalDate dataFim = parseDate(departureDate);

        final DateTimeFormatter formatterWrite = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        final String urlReserva = getUrl(parceiro, hotelId, dataInicio.format(formatterWrite), dataFim.format(formatterWrite), rooms);

        return new JSONResponse(urlReserva);
    }
}