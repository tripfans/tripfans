package br.com.tripfans.externalservices.hotel;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Contains details on the nightly rate.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NightlyRate implements Serializable {

    private BigDecimal baseRate = BigDecimal.ZERO;

    private boolean promo;

    private BigDecimal rate = BigDecimal.ZERO;

    public BigDecimal getBaseRate() {
        return this.baseRate;
    }

    public BigDecimal getRate() {
        return this.rate;
    }

    public boolean isPromo() {
        return this.promo;
    }

    public void setBaseRate(final BigDecimal baseRate) {
        this.baseRate = baseRate;
    }

    public void setPromo(final boolean promo) {
        this.promo = promo;
    }

    public void setRate(final BigDecimal rate) {
        this.rate = rate;
    }

}
