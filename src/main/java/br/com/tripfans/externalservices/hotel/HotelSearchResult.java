package br.com.tripfans.externalservices.hotel;

import java.util.Comparator;
import java.util.List;

import br.com.tripfans.importador.HotelTripFans;

public class HotelSearchResult {

    private HotelTripFans hotel;

    private List<RoomOfferDetails> roomOfferDetails;

    public HotelSearchResult(final HotelTripFans hotel, final List<RoomOfferDetails> roomOfferDetails) {
        this.hotel = hotel;
        this.roomOfferDetails = roomOfferDetails;
    }

    public RoomOfferDetails getBestRoomOfferDetails() {
        if (this.roomOfferDetails != null) {
            return this.roomOfferDetails.get(0);
        }
        return null;
    }

    public HotelTripFans getHotel() {
        return this.hotel;
    }

    public List<RoomOfferDetails> getRoomOfferDetails() {
        return this.roomOfferDetails;
    }

    public void setHotel(final HotelTripFans hotel) {
        this.hotel = hotel;
    }

    public void setRoomOfferDetails(final List<RoomOfferDetails> roomOfferDetails) {
        this.roomOfferDetails = roomOfferDetails;
    }

}

class SearchResultComparator implements Comparator<HotelSearchResult> {
    @Override
    public int compare(final HotelSearchResult o1, final HotelSearchResult o2) {
        final Float f1 = o1.getBestRoomOfferDetails().getNightlyRateTotal();
        final Float f2 = o2.getBestRoomOfferDetails().getNightlyRateTotal();
        return Float.compare(f1 != null ? f1 : 0F, f2 != null ? f2 : 0F);
    }
}
