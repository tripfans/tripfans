package br.com.tripfans.externalservices.hotel;

import java.util.List;

import br.com.tripfans.externalservices.hotel.expedia.ExpediaResponseData;

public class HotelSearchResponseData {

    private final ExpediaResponseData expediaResponseData;

    private final List<HotelSearchResult> results;

    public HotelSearchResponseData(final List<HotelSearchResult> results, final ExpediaResponseData expediaResponseData) {
        this.results = results;
        this.expediaResponseData = expediaResponseData;
    }

    public ExpediaResponseData getExpediaResponseData() {
        return this.expediaResponseData;
    }

    public List<HotelSearchResult> getResults() {
        return this.results;
    }

}
