package br.com.tripfans.externalservices.hotel.booking;

import br.com.tripfans.externalservices.hotel.BaseRateInfo;
import br.com.tripfans.externalservices.hotel.HotelRoomOffer;
import br.com.tripfans.externalservices.hotel.RateInfo;
import br.com.tripfans.importador.HotelTripFans;

public class BookingHotelRoomOffer extends HotelRoomOffer<BookingHotel> {

    public static final String TRIPFANS_BOOKING_PARTNER_ID = "382185";

    private final Integer days;

    public BookingHotelRoomOffer(final HotelTripFans hotel, final Integer days) {
        super(hotel);
        this.days = days;
    }

    @Override
    public BookingHotelRoomOffer populateRoomOfferDetails(final BookingHotel bookingHotel) {
        if (this.roomOfferDetails == null) {
            this.roomOfferDetails = new BookingRoomOfferDetails(this.hotel);

            // Adicionar o TripFans partnerID
            this.roomOfferDetails.setSelectRoomUrl(bookingHotel.getSelectRoomUrl() + ";aid=" + TRIPFANS_BOOKING_PARTNER_ID + ";");

            // Copiar informações de preço
            final RateInfo rateInfo = new RateInfo();
            // rateInfo.setCancellationPolicy(cancellationPolicy);

            final BaseRateInfo chargeableRateInfo = new BaseRateInfo();

            chargeableRateInfo.setNightlyRateTotal(bookingHotel.getFinalPrice());
            chargeableRateInfo.setCommissionableUsdTotal(bookingHotel.getFinalPrice());

            if (bookingHotel.getFinalPrice() != null) {

                chargeableRateInfo.setAverageRate(bookingHotel.getFinalPrice() / this.days);
                if (bookingHotel.getOriginalPrice() != null) {
                    chargeableRateInfo.setAverageBaseRate(bookingHotel.getOriginalPrice() / this.days);
                    rateInfo.setPromo(true);
                }

            }

            // Media do preço base
            // chargeableRateInfo.setAverageBaseRate(averageBaseRate);
            //
            // Media do preço final (com desconto)
            // chargeableRateInfo.setAverageRate(averageBaseRate);

            rateInfo.setChargeableRateInfo(chargeableRateInfo);
            this.roomOfferDetails.setRateInfo(rateInfo);
        }
        return this;
    }
}
