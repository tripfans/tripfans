package br.com.tripfans.externalservices.hotel;

import java.util.List;

public class Rooms {

    private List<Room> rooms;

    public List<Room> getRooms() {
        return this.rooms;
    }

    public void setRooms(final List<Room> rooms) {
        this.rooms = rooms;
    }

}
