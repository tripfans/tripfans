package br.com.tripfans.externalservices.hotel.booking;

import us.codecraft.webmagic.model.HasKey;

public class BookingHotel implements HasKey {

    String address;

    protected Float discountPercentage;

    protected Float finalPrice;

    protected Long id;

    Double latitude;

    Double longitude;

    String name;

    // Room reinforcements
    // protected ValueAdds valueAdds;

    protected Float originalPrice;

    Double ranking;

    protected String roomDescription;

    /**
     * URL para selecionar e reservar o(s) quarto(s)
     **/
    String selectRoomUrl;

    Integer stars;

    Long tripfansCity;

    String url;

    String urlPhotoBig;

    String urlPhotoMedium;

    String urlPhotoSmall;

    public BookingHotel(final Long id) {
        this.id = id;
    }

    public String getAddress() {
        return this.address;
    }

    public Float getDiscountPercentage() {
        return this.discountPercentage;
    }

    public Float getFinalPrice() {
        return this.finalPrice;
    }

    public Long getId() {
        return this.id;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public String getName() {
        return this.name;
    }

    public Float getOriginalPrice() {
        return this.originalPrice;
    }

    public Double getRanking() {
        return this.ranking;
    }

    public String getRoomDescription() {
        return this.roomDescription;
    }

    public String getSelectRoomUrl() {
        return this.selectRoomUrl;
    }

    public Integer getStars() {
        return this.stars;
    }

    public Long getTripfansCity() {
        return this.tripfansCity;
    }

    public String getUrl() {
        return this.url;
    }

    public String getUrlPhotoBig() {
        return this.urlPhotoBig;
    }

    public String getUrlPhotoMedium() {
        return this.urlPhotoMedium;
    }

    public String getUrlPhotoSmall() {
        return this.urlPhotoSmall;
    }

    @Override
    public String key() {
        return this.id + "";
    }

    public void setDiscountPercentage(final Float discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public void setFinalPrice(final Float finalPrice) {
        this.finalPrice = finalPrice;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setOriginalPrice(final Float originalPrice) {
        this.originalPrice = originalPrice;
    }

    public void setRoomDescription(final String roomDescription) {
        this.roomDescription = roomDescription;
    }
}