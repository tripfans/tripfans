package br.com.tripfans.externalservices.hotel;

import java.util.List;

public class Room {

    public Room() {

    }
    
    private List<Integer> childAges;

    private int numberOfAdults;

    public List<Integer> getChildAges() {
        return this.childAges;
    }

    public int getNumberOfAdults() {
        return this.numberOfAdults;
    }

    public void setChildAges(final List<Integer> childAges) {
        this.childAges = childAges;
    }

    public void setNumberOfAdults(final int numberOfAdults) {
        this.numberOfAdults = numberOfAdults;
    }

}
