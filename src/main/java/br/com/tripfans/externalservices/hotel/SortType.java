package br.com.tripfans.externalservices.hotel;

import java.util.Comparator;

class PriceComparator implements Comparator<RoomOfferDetails> {
    @Override
    public int compare(final RoomOfferDetails o1, final RoomOfferDetails o2) {
        final Float f1 = o1.getNightlyRateTotal();
        final Float f2 = o2.getNightlyRateTotal();
        return Float.compare(f1 != null ? f1 : 0F, f2 != null ? f2 : 0F);
    }
}

public enum SortType {

    OVERALL_VALUE("Recomendados", new PriceComparator(), "popularity"),
    PRICE("Menor Preço", new PriceComparator(), "price_for_two"),
    PRICE_REVERSE("Maior Preço", new PriceComparator().reversed(), null),
    QUALITY("Estrelas (de 5 a 1)", new StarsComparator().reversed(), "class"),
    QUALITY_REVERSE("Estrelas (de 1 a 5)", new StarsComparator(), "class_asc"),
    TRIP_ADVISOR("Melhores Avaliados", new PriceComparator(), "score");

    private String bookingSortValue;

    private Comparator<RoomOfferDetails> comparator;

    private String descricao;

    SortType(final String descricao, final Comparator<RoomOfferDetails> comparator, final String bookingSortValue) {
        this.descricao = descricao;
        this.comparator = comparator;
        this.bookingSortValue = bookingSortValue;
    }

    public String getBookingSortValue() {
        return this.bookingSortValue;
    }

    public Comparator<RoomOfferDetails> getComparator() {
        return this.comparator;
    }

    public String getDescricao() {
        return this.descricao;
    }

}

class StarsComparator implements Comparator<RoomOfferDetails> {
    @Override
    public int compare(final RoomOfferDetails o1, final RoomOfferDetails o2) {
        final Float f1 = o1.getHotelStars();
        final Float f2 = o2.getHotelStars();
        return Float.compare(f1 != null ? f1 : 0F, f2 != null ? f2 : 0F);
    }
}