package br.com.tripfans.externalservices.hotel.expedia;

import java.util.List;

import br.com.tripfans.externalservices.hotel.HotelRoomOffer;

import com.ean.wsapi.hotel.v3.CachedSupplierResponse;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class ExpediaResponseData {

    private CachedSupplierResponse cachedSupplierResponse;

    private String cacheKey;

    private String cacheLocation;

    @JsonIgnore
    @org.codehaus.jackson.annotate.JsonIgnore
    private List<HotelRoomOffer> hotelRoomOffers;

    private Boolean moreResultsAvailable;

    private Integer numberOfRoomsRequested;

    public CachedSupplierResponse getCachedSupplierResponse() {
        return this.cachedSupplierResponse;
    }

    public String getCacheKey() {
        return this.cacheKey;
    }

    public String getCacheLocation() {
        return this.cacheLocation;
    }

    public List<HotelRoomOffer> getHotelRoomOffers() {
        return this.hotelRoomOffers;
    }

    public Boolean getMoreResultsAvailable() {
        return this.moreResultsAvailable;
    }

    public Integer getNumberOfRoomsRequested() {
        return this.numberOfRoomsRequested;
    }

    public void setCachedSupplierResponse(final CachedSupplierResponse cachedSupplierResponse) {
        this.cachedSupplierResponse = cachedSupplierResponse;
    }

    public void setCacheKey(final String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public void setCacheLocation(final String cacheLocation) {
        this.cacheLocation = cacheLocation;
    }

    public void setHotelRoomOffers(final List<HotelRoomOffer> hotelRoomOffers) {
        this.hotelRoomOffers = hotelRoomOffers;
    }

    public void setMoreResultsAvailable(final Boolean moreResultsAvailable) {
        this.moreResultsAvailable = moreResultsAvailable;
    }

    public void setNumberOfRoomsRequested(final Integer numberOfRoomsRequested) {
        this.numberOfRoomsRequested = numberOfRoomsRequested;
    }

}
