package br.com.tripfans.externalservices.hotel.hoteis;

import org.springframework.beans.BeanUtils;

import br.com.tripfans.externalservices.hotel.BaseRateInfo;
import br.com.tripfans.externalservices.hotel.HotelRoomOffer;
import br.com.tripfans.externalservices.hotel.RateInfo;
import br.com.tripfans.externalservices.hotel.expedia.ExpediaRoomOfferDetails;
import br.com.tripfans.importador.HotelTripFans;

public class HoteisHotelRoomOffer extends HotelRoomOffer<ExpediaRoomOfferDetails> {

    public HoteisHotelRoomOffer(final HotelTripFans hotel, final ExpediaRoomOfferDetails roomRateDetails) {
        super(hotel);
        populateRoomOfferDetails(roomRateDetails);
    }

    @Override
    public HoteisHotelRoomOffer populateRoomOfferDetails(final ExpediaRoomOfferDetails roomRateDetails) {
        if (this.roomOfferDetails == null) {
            this.roomOfferDetails = new HoteisRoomOfferDetails(this.hotel);
        }
        // Copiar os valores basicos
        BeanUtils.copyProperties(roomRateDetails, this.roomOfferDetails);

        // Copiar informações de preço
        final RateInfo rateInfo = new RateInfo();
        BeanUtils.copyProperties(roomRateDetails.getRateInfo(), rateInfo);

        final BaseRateInfo chargeableRateInfo = new BaseRateInfo();
        BeanUtils.copyProperties(roomRateDetails.getRateInfo().getChargeableRateInfo(), chargeableRateInfo);

        rateInfo.setChargeableRateInfo(chargeableRateInfo);
        this.roomOfferDetails.setRateInfo(rateInfo);
        return this;
    }

}
