package br.com.tripfans.externalservices.hotel;

public class Hotel {

    private String address1;

    private Float hotelRating;

    private Long id;

    private Float latitude;

    private String locationDescription;

    private Float longitude;

    private String name;

    private String propertyCategory;

    private String shortDescription;

    private String thumbNailUrl;

    private String tripAdvisorRatingUrl;

    @Override
    public boolean equals(final Object hotel) {
        if (hotel == null || !(hotel instanceof Hotel)) {
            return false;
        }
        return ((Hotel) hotel).id.equals(this.id);
    }

    public String getAddress1() {
        return this.address1;
    }

    public Float getHotelRating() {
        return this.hotelRating;
    }

    public Long getId() {
        return this.id;
    }

    public Float getLatitude() {
        return this.latitude;
    }

    public String getLocationDescription() {
        return this.locationDescription;
    }

    public Float getLongitude() {
        return this.longitude;
    }

    public String getName() {
        return this.name;
    }

    public String getPropertyCategory() {
        return this.propertyCategory;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public String getThumbNailUrl() {
        return this.thumbNailUrl;
    }

    public String getTripAdvisorRatingUrl() {
        return this.tripAdvisorRatingUrl;
    }

    @Override
    public int hashCode() {
        if (this.id == null) {
            return super.hashCode();
        }
        return this.id.hashCode();
    }

    public void setAddress1(final String address1) {
        this.address1 = address1;
    }

    public void setHotelRating(final Float hotelRating) {
        this.hotelRating = hotelRating;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setLatitude(final Float latitude) {
        this.latitude = latitude;
    }

    public void setLocationDescription(final String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public void setLongitude(final Float longitude) {
        this.longitude = longitude;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setPropertyCategory(final String propertyCategory) {
        this.propertyCategory = propertyCategory;
    }

    public void setShortDescription(final String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public void setThumbNailUrl(final String thumbNailUrl) {
        this.thumbNailUrl = thumbNailUrl;
    }

    public void setTripAdvisorRatingUrl(final String tripAdvisorRatingUrl) {
        this.tripAdvisorRatingUrl = tripAdvisorRatingUrl;
    }

}
