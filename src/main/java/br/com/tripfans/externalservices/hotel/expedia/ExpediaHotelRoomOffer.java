package br.com.tripfans.externalservices.hotel.expedia;

import org.springframework.beans.BeanUtils;

import br.com.tripfans.externalservices.hotel.BaseRateInfo;
import br.com.tripfans.externalservices.hotel.HotelRoomOffer;
import br.com.tripfans.externalservices.hotel.RateInfo;
import br.com.tripfans.importador.HotelTripFans;

public class ExpediaHotelRoomOffer extends HotelRoomOffer<com.ean.wsapi.hotel.v3.RoomRateDetails> {

    public ExpediaHotelRoomOffer(final HotelTripFans hotel) {
        super(hotel);
    }

    @Override
    public ExpediaHotelRoomOffer populateRoomOfferDetails(final com.ean.wsapi.hotel.v3.RoomRateDetails roomRateDetailsExpedia) {
        if (this.roomOfferDetails == null) {
            this.roomOfferDetails = new ExpediaRoomOfferDetails(this.hotel);
        }
        // Copiar os valores basicos
        BeanUtils.copyProperties(roomRateDetailsExpedia, this.roomOfferDetails);

        // Copiar informações de preço
        final RateInfo rateInfo = new RateInfo();
        BeanUtils.copyProperties(roomRateDetailsExpedia.getRateInfos().getRateInfo().get(0), rateInfo);

        final BaseRateInfo chargeableRateInfo = new BaseRateInfo();
        BeanUtils.copyProperties(roomRateDetailsExpedia.getRateInfos().getRateInfo().get(0).getChargeableRateInfo(), chargeableRateInfo);

        rateInfo.setChargeableRateInfo(chargeableRateInfo);
        this.roomOfferDetails.setRateInfo(rateInfo);
        return this;
    }

}
