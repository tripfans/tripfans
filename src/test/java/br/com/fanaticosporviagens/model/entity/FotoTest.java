package br.com.fanaticosporviagens.model.entity;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import br.com.fanaticosporviagens.infra.util.AppEnviromentConstants;
import br.com.fanaticosporviagens.infra.util.TripFansAppConstants;

public class FotoTest {

    private static final String TRIPFANS = "localhost:8080/tripfans";

    private Foto foto;

    @Before
    public void prepara() {
        System.setProperty(AppEnviromentConstants.SERVER_URL, TRIPFANS);
        this.foto = new Foto();
    }

    @Test
    public void verificaUrlsFotoAnonimaUsuarioFeminino() {
        final Usuario usuario = new Usuario();
        usuario.setGenero("F");
        this.foto.setUsuario(usuario);
        this.foto.definirFotoPadraoAnonima();
        Assert.assertEquals("Foto anônima album feminina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_FEMININA_ALBUM,
                this.foto.getUrlAlbum());
        Assert.assertEquals("Foto anônima big feminina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_FEMININA_BIG,
                this.foto.getUrlBig());
        Assert.assertEquals("Foto anônima small feminina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_FEMININA_SMALL,
                this.foto.getUrlSmall());
        Assert.assertEquals("Foto anônima album feminina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_FEMININA_ALBUM, usuario.getUrlFotoAlbum());
        Assert.assertEquals("Foto anônima big feminina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_FEMININA_BIG, usuario.getUrlFotoBig());
        Assert.assertEquals("Foto anônima small feminina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_FEMININA_SMALL, usuario.getUrlFotoSmall());
    }

    @Test
    public void verificaUrlsFotoAnonimaUsuarioMasculino() {
        final Usuario usuario = new Usuario();
        usuario.setGenero("M");
        this.foto.setUsuario(usuario);
        this.foto.definirFotoPadraoAnonima();
        Assert.assertEquals("Foto anônima album masculina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_ALBUM,
                this.foto.getUrlAlbum());
        Assert.assertEquals("Foto anônima big masculina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_BIG,
                this.foto.getUrlBig());
        Assert.assertEquals("Foto anônima small masculina não está correta", TRIPFANS + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_SMALL,
                this.foto.getUrlSmall());
        Assert.assertEquals("Foto anônima album masculina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_ALBUM, usuario.getUrlFotoAlbum());
        Assert.assertEquals("Foto anônima big masculina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_BIG, usuario.getUrlFotoBig());
        Assert.assertEquals("Foto anônima small masculina desnormalizada no usuário não está correta", TRIPFANS
                + TripFansAppConstants.FOTO_ANONIMA_MASCULINA_SMALL, usuario.getUrlFotoSmall());
    }

}
