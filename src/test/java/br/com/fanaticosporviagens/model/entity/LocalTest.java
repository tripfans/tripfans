package br.com.fanaticosporviagens.model.entity;

import junit.framework.Assert;

import org.junit.Test;

public class LocalTest {

    @Test
    public void avaliacaoConsolidadadaDeveSerIgualClassificacaoPrimeiraAvaliacao() {
        final Atracao atracao = new Atracao();
        atracao.calcularAvaliacaoGeral(AvaliacaoQualidade.BOM);
        Assert.assertEquals("Como é a primeira avaliação, ela deve ser a classificação geral do local.", AvaliacaoQualidade.BOM, atracao
                .getAvaliacaoConsolidadaGeral().getClassificacaoGeral());
    }

    @Test
    public void avaliacaoConsolidadaDeDiversasRuimMesmoComExcelenteContinuaRuim() {
        final Atracao atracao = new Atracao();
        atracao.setQuantidadeAvaliacoes(99L);
        final AvaliacaoConsolidada avaliacaoConsolidada = new AvaliacaoConsolidada();
        avaliacaoConsolidada.setClassificacaoGeral(AvaliacaoQualidade.RUIM);
        atracao.setAvaliacaoConsolidadaGeral(avaliacaoConsolidada);
        atracao.calcularAvaliacaoGeral(AvaliacaoQualidade.EXCELENTE);
        Assert.assertEquals("Avaliação consolidada de diversas RUIM continua RUIM.", AvaliacaoQualidade.RUIM, atracao.getAvaliacaoConsolidadaGeral()
                .getClassificacaoGeral());
    }

    @Test
    public void avaliacaoConsolidadaDeDuasAvaliacoesBoas() {
        final Atracao atracao = new Atracao();
        atracao.setQuantidadeAvaliacoes(1L);
        final AvaliacaoConsolidada avaliacaoConsolidada = new AvaliacaoConsolidada();
        avaliacaoConsolidada.setClassificacaoGeral(AvaliacaoQualidade.BOM);
        atracao.setAvaliacaoConsolidadaGeral(avaliacaoConsolidada);
        atracao.calcularAvaliacaoGeral(AvaliacaoQualidade.BOM);
        Assert.assertEquals("Avaliação consolidada de duas BOM deve ser BOM.", AvaliacaoQualidade.BOM, atracao.getAvaliacaoConsolidadaGeral()
                .getClassificacaoGeral());
    }

    @Test
    public void avaliacaoConsolidadaDeDuasAvaliacoesBoasDuasRuim() {
        final Atracao atracao = new Atracao();
        atracao.setQuantidadeAvaliacoes(3L);
        final AvaliacaoConsolidada avaliacaoConsolidada = new AvaliacaoConsolidada();
        avaliacaoConsolidada.setClassificacaoGeral(AvaliacaoQualidade.REGULAR);
        atracao.setAvaliacaoConsolidadaGeral(avaliacaoConsolidada);
        atracao.calcularAvaliacaoGeral(AvaliacaoQualidade.RUIM);
        Assert.assertEquals("Avaliação consolidada de duas BOM e duas RUIM deve ser REGULAR.", AvaliacaoQualidade.REGULAR, atracao
                .getAvaliacaoConsolidadaGeral().getClassificacaoGeral());
    }

}
