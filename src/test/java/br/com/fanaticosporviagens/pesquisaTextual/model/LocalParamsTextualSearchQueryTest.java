package br.com.fanaticosporviagens.pesquisaTextual.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;

public class LocalParamsTextualSearchQueryTest {

    @Test
    public void latitudeLongitudeComParenteses() {
        final String ll = "(-15.45,89.9999)";

        final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery();
        params.setLl(ll);

        final CoordenadaGeografica coordenada = params.getCoordinates().get(0);
        assertEquals(coordenada.getLatitude(), Double.valueOf(-15.45));
        assertEquals(coordenada.getLongitude(), Double.valueOf(89.9999));
    }

    @Test
    public void latitudeLongitudeDupla() {
        final String ll = "(-15.45,89.9999);(89.999,90.001)";

        final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery();
        params.setLl(ll);

        final CoordenadaGeografica coordenada0 = params.getCoordinates().get(0);
        final CoordenadaGeografica coordenada1 = params.getCoordinates().get(1);

        assertEquals(coordenada0.getLatitude(), Double.valueOf(-15.45));
        assertEquals(coordenada0.getLongitude(), Double.valueOf(89.9999));

        assertEquals(coordenada1.getLatitude(), Double.valueOf(89.999));
        assertEquals(coordenada1.getLongitude(), Double.valueOf(90.001));
    }

    @Test(expected = IllegalArgumentException.class)
    public void latitudeLongitudeFormatoInvalido() {
        final String ll = "-15.45,";

        final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery();
        params.setLl(ll);
    }

    @Test
    public void latitudeLongitudeSimples() {
        final String ll = "-15.45,89.9999";

        final LocalParamsTextualSearchQuery params = new LocalParamsTextualSearchQuery();
        params.setLl(ll);

        final CoordenadaGeografica coordenada = params.getCoordinates().get(0);

        assertEquals(coordenada.getLatitude(), Double.valueOf(-15.45));
        assertEquals(coordenada.getLongitude(), Double.valueOf(89.9999));
    }

}
