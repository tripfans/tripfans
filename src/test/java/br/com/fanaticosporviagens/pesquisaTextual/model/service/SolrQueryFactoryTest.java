package br.com.fanaticosporviagens.pesquisaTextual.model.service;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.junit.Test;

import br.com.fanaticosporviagens.model.entity.CoordenadaGeografica;
import br.com.fanaticosporviagens.model.entity.LocalType;

public class SolrQueryFactoryTest {

    private final SolrQueryFactory factory = new SolrQueryFactory();

    @Test
    public void queryComOrdenacaoGeografica() {
        final Map<String, Object> filtrosAdicionais = new HashMap<String, Object>();
        filtrosAdicionais.put("tipoLocal", new Object[] { LocalType.ATRACAO.getCodigo(), LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo() });

        final TripFansSolrQueryParams params = new TripFansSolrQueryParams().withTerm("universal")
                .addExtraParam("tipoLocal", new Object[] { LocalType.ATRACAO.getCodigo(), LocalType.LOCAL_INTERESSE_TURISTICO.getCodigo() })
                .addGeoCoord(new CoordenadaGeografica(28.53834, -81.37924)).sortBy("geodist()");

        final SolrQuery query = this.factory.buildQuery(params);

        assertEquals("(edgyTextField:\"universal\" OR (tokenField:universal*))", query.getQuery());
        assertEquals("(tipoLocal:7 OR tipoLocal:12)", query.getFilterQueries()[0]);
        assertEquals("28.53834,-81.37924", query.get("pt"));
        assertEquals("localizacao", query.get("sfield"));
        assertEquals("geodist() asc", query.getSortField());
    }

    @Test
    public void testaQueryComApenasUmToken() {
        final SolrQuery query = this.factory.buildQuery("Rio");
        assertEquals("A query não veio igual", "edgyTextField:\"Rio\" OR (tokenField:Rio*)", query.getQuery());
    }

    @Test
    public void testaQueryComMaisDeUmToken() {
        final SolrQuery query1 = this.factory.buildQuery("Rio de Janei");
        assertEquals("A query com 2 tokens não veio igual", "edgyTextField:\"rio janei\" OR (tokenField:rio AND tokenField:janei*)",
                query1.getQuery());

        final SolrQuery query2 = this.factory.buildQuery("São Miguel das Barras");
        assertEquals("A query com 3 tokens não veio igual",
                "edgyTextField:\"são miguel barras\" OR (tokenField:são AND tokenField:miguel AND tokenField:barras*)", query2.getQuery());
    }

    @Test
    public void testaQueryComMaisDeUmTokenEFiltros() {
        final Map<String, Object> filtrosAdicionais = new HashMap<String, Object>();
        filtrosAdicionais.put("tipoLocal", new Object[] { LocalType.ESTADO.getCodigo(), LocalType.CIDADE.getCodigo() });

        final SolrQuery query1 = this.factory.buildQuery("Rio de Janei", filtrosAdicionais);
        assertEquals("A query com filtros adicionais não veio igual",
                "(edgyTextField:\"rio janei\" OR (tokenField:rio AND tokenField:janei*)) AND ((tipoLocal:3 OR tipoLocal:4))", query1.getQuery());

        filtrosAdicionais.put("xpto", "algo");
        final SolrQuery query2 = this.factory.buildQuery("Rio de Janei", filtrosAdicionais);
        assertEquals("A query com filtros adicionais não veio igual",
                "(edgyTextField:\"rio janei\" OR (tokenField:rio AND tokenField:janei*)) AND ((tipoLocal:3 OR tipoLocal:4) AND xpto:algo)",
                query2.getQuery());
    }

}
