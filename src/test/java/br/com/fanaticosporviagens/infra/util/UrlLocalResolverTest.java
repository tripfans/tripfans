package br.com.fanaticosporviagens.infra.util;

import junit.framework.Assert;

import org.junit.Test;

import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Cidade;
import br.com.fanaticosporviagens.model.entity.Estado;
import br.com.fanaticosporviagens.model.entity.Hotel;
import br.com.fanaticosporviagens.model.entity.Pais;
import br.com.fanaticosporviagens.model.entity.Restaurante;

public class UrlLocalResolverTest {

    @Test
    public void resolveUrlLocais() {
        final Cidade cidade = new Cidade();
        cidade.setUrlPath("maceio");
        final Hotel hotel = new Hotel();
        hotel.setUrlPath("maceio-mar-hotel");
        final Estado estado = new Estado();
        estado.setUrlPath("rio-de-janeiro");
        final Pais pais = new Pais();
        pais.setUrlPath("holanda");
        final Restaurante restaurante = new Restaurante();
        restaurante.setUrlPath("coco-bambu-brasilia");
        final Atracao atracao = new Atracao();
        atracao.setUrlPath("cristo-redentor");
        Assert.assertEquals("A url retornada para a cidade está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/maceio",
                UrlLocalResolver.getUrl(cidade));
        Assert.assertEquals("A url retornada para o hotel está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/maceio-mar-hotel",
                UrlLocalResolver.getUrl(hotel));
        Assert.assertEquals("A url retornada para o estado está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/rio-de-janeiro",
                UrlLocalResolver.getUrl(estado));
        Assert.assertEquals("A url retornada para pais está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/holanda",
                UrlLocalResolver.getUrl(pais));
        Assert.assertEquals("A url retornada para o restaurante está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/coco-bambu-brasilia",
                UrlLocalResolver.getUrl(restaurante));
        Assert.assertEquals("A url retornada para a atração está incorreta.", UrlLocalResolver.LOCAL_URL_MAPPING + "/cristo-redentor",
                UrlLocalResolver.getUrl(atracao));
    }

}
