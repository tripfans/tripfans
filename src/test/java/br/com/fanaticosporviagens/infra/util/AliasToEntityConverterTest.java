package br.com.fanaticosporviagens.infra.util;

import java.math.BigInteger;

import junit.framework.Assert;

import org.junit.Test;

import br.com.fanaticosporviagens.infra.converter.AliasToEntityConverter;
import br.com.fanaticosporviagens.model.entity.Cidade;

public class AliasToEntityConverterTest {

    @Test(expected = NoSuchMethodException.class)
    public void conversaoComPropriedadeNaoExistenteDeveLancarExcecao() throws Exception {
        final String[] aliases = { "id", "nome", "numavaliacoes", "urlPath", "estado.id" };
        final Object[] values = { 2345L, "Recife", 52L, "recife", 33L };

        final AliasToEntityConverter entityConverter = new AliasToEntityConverter();
        entityConverter.convert(Cidade.class, aliases, values);
    }

    @Test
    public void conversaoComPropriedadesAninhadas() throws Exception {
        final String[] aliases = { "id", "nome", "urlPath", "estado.id", "estado.nome", "estado.pais.id", "estado.pais.nome",
                "estado.pais.continente.nome" };
        final Object[] values = { 2345L, "Recife", "recife", 33L, "Pernambuco", 5L, "Brasil", "América do Sul" };

        final AliasToEntityConverter entityConverter = new AliasToEntityConverter();
        final Object object = entityConverter.convert(Cidade.class, aliases, values);

        Assert.assertNotNull("Entidade retornada não pode ser nula.", object);
        Assert.assertTrue("Objeto deve ser do tipo Cidade", object instanceof Cidade);
        final Cidade cidade = (Cidade) object;
        Assert.assertEquals("Id deve ser 2345", Long.valueOf(2345), cidade.getId());
        Assert.assertEquals("Nome deve ser Recife", "Recife", cidade.getNome());
        Assert.assertEquals("Url deve ser recife", "recife", cidade.getUrlPath());
        Assert.assertNotNull("O estado da cidade não pode ser nulo", cidade.getEstado());
        Assert.assertEquals("Id do estado deve ser 33", Long.valueOf(33L), cidade.getEstado().getId());
        Assert.assertEquals("Nome do estado deve ser Pernambuco", "Pernambuco", cidade.getEstado().getNome());
        Assert.assertNotNull("O país do estado não pode ser nulo", cidade.getEstado().getPais());
        Assert.assertEquals("Id do país deve ser 5", Long.valueOf(5), cidade.getEstado().getPais().getId());
        Assert.assertEquals("Nome do país deve ser Brasil", "Brasil", cidade.getEstado().getPais().getNome());
        Assert.assertNotNull("O continente do país não pode ser nulo", cidade.getEstado().getPais().getContinente());
        Assert.assertEquals("Nome do continente deve ser América do Sul", "América do Sul", cidade.getEstado().getPais().getContinente().getNome());
    }

    @Test(expected = IllegalArgumentException.class)
    public void conversaoComQuantidadeAliasesDiferenteQuantidadeValoresDeveLancarExcecao() throws Exception {
        final String[] aliases = { "id", "nome", "numAvaliacoes" };
        final Object[] values = { 2345L, "Recife", 52L, "recife" };

        final AliasToEntityConverter entityConverter = new AliasToEntityConverter();
        entityConverter.convert(Cidade.class, aliases, values);
    }

    @Test
    public void conversaoSimples() throws Exception {
        final String[] aliases = { "id", "nome", "urlPath" };
        final Object[] values = { 2345L, "Recife", "recife" };

        final AliasToEntityConverter entityConverter = new AliasToEntityConverter();
        final Object object = entityConverter.convert(Cidade.class, aliases, values);

        Assert.assertNotNull("Entidade retornada não pode ser nula.", object);
        Assert.assertTrue("Objeto deve ser do tipo Cidade", object instanceof Cidade);
        final Cidade cidade = (Cidade) object;
        Assert.assertEquals("Id deve ser 2345", Long.valueOf(2345), cidade.getId());
        Assert.assertEquals("Nome deve ser Recife", "Recife", cidade.getNome());
        Assert.assertEquals("Url deve ser recife", "recife", cidade.getUrlPath());
    }

    @Test
    public void conversaoSimplesComConversaoDeTipos() throws Exception {
        final BigInteger bigInt = new BigInteger("2345");
        final String[] aliases = { "id", "nome", "urlPath" };
        final Object[] values = { bigInt, "Recife", "recife" };

        final AliasToEntityConverter entityConverter = new AliasToEntityConverter();
        final Object object = entityConverter.convert(Cidade.class, aliases, values);

        Assert.assertNotNull("Entidade retornada não pode ser nula.", object);
        Assert.assertTrue("Objeto deve ser do tipo Cidade", object instanceof Cidade);
        final Cidade cidade = (Cidade) object;
        Assert.assertEquals("Id deve ser 2345", Long.valueOf(2345), cidade.getId());
        Assert.assertEquals("Nome deve ser Recife", "Recife", cidade.getNome());
        Assert.assertEquals("Url deve ser recife", "recife", cidade.getUrlPath());
    }

}
