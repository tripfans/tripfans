package br.com.fanaticosporviagens.avaliacao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.Assert;

import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import br.com.fanaticosporviagens.model.entity.Atracao;
import br.com.fanaticosporviagens.model.entity.Avaliacao;
import br.com.fanaticosporviagens.model.entity.Usuario;

public class AvaliacaoServiceTest {

    @Mock
    private AvaliacaoRepository avaliacaoRepository;

    private AvaliacaoService avaliacaoService;

    @Test
    public void avaliacaoImpossivelUsuarioAvaliouLocalHaMenosTrintaDias() {
        final Usuario usuario = new Usuario();
        final Atracao local = new Atracao();
        Mockito.when(this.avaliacaoRepository.consultarAvaliacoesLocalPorUsuario(usuario, local)).thenReturn(listaComAvaliacaoHaMenosTrintaDias());
        final boolean naoPodeAvaliar = this.avaliacaoService.usuarioNaoPodeAvaliar(usuario, local);
        Assert.assertTrue("Não pode avaliar o local pois avaliação foi feita há menos de 30 dias", naoPodeAvaliar);
    }

    @Test
    public void avaliacaoImpossivelUsuarioAvaliouLocalHaTrintaDias() {
        final Usuario usuario = new Usuario();
        final Atracao local = new Atracao();
        Mockito.when(this.avaliacaoRepository.consultarAvaliacoesLocalPorUsuario(usuario, local)).thenReturn(listaComAvaliacaoHaMenosTrintaDias());
        final boolean naoPodeAvaliar = this.avaliacaoService.usuarioNaoPodeAvaliar(usuario, local);
        Assert.assertTrue("Não pode avaliar o local pois avaliação foi feita há 30 dias", naoPodeAvaliar);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void avaliacaoPossivelUsuarioAindaNaoAvaliouLocal() {
        final Usuario usuario = new Usuario();
        final Atracao local = new Atracao();
        Mockito.when(this.avaliacaoRepository.consultarAvaliacoesLocalPorUsuario(usuario, local)).thenReturn(Collections.EMPTY_LIST);
        final boolean naoPodeAvaliar = this.avaliacaoService.usuarioNaoPodeAvaliar(usuario, local);
        Assert.assertFalse("Deve ser possivel avaliar o local porque é a primeira.", naoPodeAvaliar);
    }

    @Test
    public void avaliacaoPossivelUsuarioAvaliouLocalHaMaisTrintaDias() {
        final Usuario usuario = new Usuario();
        final Atracao local = new Atracao();
        Mockito.when(this.avaliacaoRepository.consultarAvaliacoesLocalPorUsuario(usuario, local)).thenReturn(listaComAvaliacaoHaMaisTrintaDias());
        final boolean naoPodeAvaliar = this.avaliacaoService.usuarioNaoPodeAvaliar(usuario, local);
        Assert.assertFalse("Deve ser possivel avaliar o local pois avaliação foi feita ha mais de 30 dias", naoPodeAvaliar);
    }

    @Before
    public void prepara() {
        MockitoAnnotations.initMocks(this);
        this.avaliacaoService = new AvaliacaoService();
        ReflectionTestUtils.setField(this.avaliacaoService, "avaliacaoRepository", this.avaliacaoRepository);
    }

    private List<Avaliacao> listaComAvaliacaoHaExatosTrintaDias() {
        final List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
        final Avaliacao avaliacao1 = new Avaliacao();
        avaliacao1.setDataCriacao(LocalDateTime.now().minusDays(30));
        avaliacoes.add(avaliacao1);
        return avaliacoes;
    }

    private List<Avaliacao> listaComAvaliacaoHaMaisTrintaDias() {
        final List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
        final Avaliacao avaliacao1 = new Avaliacao();
        avaliacao1.setDataCriacao(LocalDateTime.now().minusDays(31));
        avaliacoes.add(avaliacao1);
        return avaliacoes;
    }

    private List<Avaliacao> listaComAvaliacaoHaMenosTrintaDias() {
        final List<Avaliacao> avaliacoes = new ArrayList<Avaliacao>();
        final Avaliacao avaliacao1 = new Avaliacao();
        avaliacao1.setDataCriacao(LocalDateTime.now().minusDays(1));
        avaliacoes.add(avaliacao1);
        return avaliacoes;
    }
}
