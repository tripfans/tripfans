1. Instale o Maven na sua máquina e adicione-o ao seu classpath, de forma que os binários do Maven, como o mvn, sejam encontrados.
2. Via linha de comando, entre no diretório /src/main/webapp/WEB-INF/lib do projeto.
cd <caminho do projeto>/src/main/webapp/WEB-INF/lib

3. Execute os comandos a seguir:

mvn install:install-file -Dfile=gdata-client-1.0.jar -DgroupId=com.google.gdata -DartifactId=gdata-client -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=gdata-contacts-3.0.jar -DgroupId=com.google.gdata -DartifactId=gdata-contacts -Dversion=3.0 -Dpackaging=jar
mvn install:install-file -Dfile=gdata-contacts-meta-3.0.jar -DgroupId=com.google.gdata -DartifactId=gdata-contacts-meta -Dversion=3.0 -Dpackaging=jar
mvn install:install-file -Dfile=gdata-core-1.0.jar -DgroupId=com.google.gdata -DartifactId=gdata-core -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=spring-social-google-1.0.0.M1.jar -DgroupId=org.springframework.social -DartifactId=spring-social-google -Dversion=1.0.0.M1 -Dpackaging=jar
mvn install:install-file -Dfile=spring-social-foursquare-1.0.8.2-TRIPFANS.jar -DgroupId=org.springframework.social -DartifactId=spring-social-foursquare -Dversion=1.0.8.2-TRIPFANS -Dpackaging=jar
mvn install:install-file -Dfile=mail-amqp-1.0.0.jar -DgroupId=br.com.tripfans -DartifactId=mail-amqp -Dversion=1.0.0 -Dpackaging=jar
