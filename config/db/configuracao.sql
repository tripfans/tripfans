﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.0
-- Dumped by pg_dump version 9.3.0
-- Started on 2013-10-29 09:42:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 276 (class 1259 OID 65538)
-- Name: configuracao; Type: TABLE; Schema: public; Owner: fpv; Tablespace: 
--

CREATE TABLE configuracao (
    id integer NOT NULL,
    chave character varying(200),
    valor character varying(200)
);


ALTER TABLE public.configuracao OWNER TO fpv;

--
-- TOC entry 275 (class 1259 OID 65536)
-- Name: Configuracao_id_seq; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE "Configuracao_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Configuracao_id_seq" OWNER TO fpv;

--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 275
-- Name: Configuracao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: fpv
--

ALTER SEQUENCE "Configuracao_id_seq" OWNED BY configuracao.id;


--
-- TOC entry 2170 (class 2604 OID 65541)
-- Name: id; Type: DEFAULT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY configuracao ALTER COLUMN id SET DEFAULT nextval('"Configuracao_id_seq"'::regclass);


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 275
-- Name: Configuracao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: fpv
--

SELECT pg_catalog.setval('"Configuracao_id_seq"', 25, true);


--
-- TOC entry 2281 (class 0 OID 65538)
-- Dependencies: 276
-- Data for Name: configuracao; Type: TABLE DATA; Schema: public; Owner: fpv
--

INSERT INTO configuracao (id, chave, valor) values
(1,	'application.protocol',	'http'),
(2,	'application.domain',	'localhost:8080'),
(3,	'application.context',	'/tripfans'),
(4,	'application.imagesPath',	'/fotos'),
(5,	'application.imagesRootDirectory',	'/home/andre/fanaticos/'),
(6,	'application.isDevel',	'false'),
(7,	'application.isDevelAtWork',	'false'),
(8,	'application.enableSocialNetworkActions',	'true'),
(9,	'mail.host',	'smtp.gmail.com'),
(10,	'google.clientId',	'202244445170.apps.googleusercontent.com'),
(11,	'google.clientSecret',	'7Anvl1QYfQv8pJ_3F44r3P6J'),
(12,	'facebook.clientId',	'253813521323850'),
(13,	'facebook.clientSecret',	'cc9136958d737fa0e883a477b46f517e'),
(14,	'twitter.consumerKey',	'lOOAG5qTkUqUFCybDMwP8Q'),
(15,	'twitter.consumerSecret',	'U7lbAVRsd1CDA8kEzAFDgS6RnzSy0CsuArIPEkaXg'),
(16,	'foursquare.clientId',	'M2U4JYJ05N1OD44LMP1TF2FMSIQ4GSEJCCAYYHGZCURBQXQ4'),
(17,	'foursquare.clientSecret',	'Z3N1MXRJRWVKYKGKOFFXVEHPV4G3CW0GSBG4TVXZRVA02TAR'),
(18,	'bitly.accessToken',	'4f1750aa924580782cc7146b3b55db1c7f7e8baf'),
(19,	'bitly.shortenUrl',	'https://api-ssl.bitly.com/v3/shorten'),
(20,	'solr.serverUrl',	'http://localhost:8983/solr'),
(21,	'solr.allowCompression',	'true'),
(22,	'db.url',	'jdbc:postgresql://127.0.0.1:5432/fanaticos'),
(23,	'db.username',	'fpv'),
(24,	'db.password',	'fpv'),
(25,	'hibernate.showSql',	'false'),
(26,	'application.enabledPedidoDica','true'),
(27,	'rabbit.host','localhost'),
(28,	'rabbit.queue','teste'),
(29,	'rabbit.port','5672');



--
-- TOC entry 2172 (class 2606 OID 65543)
-- Name: Configuracao_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv; Tablespace: 
--

ALTER TABLE ONLY configuracao
    ADD CONSTRAINT "Configuracao_pkey" PRIMARY KEY (id);


-- Completed on 2013-10-29 09:42:09

--
-- PostgreSQL database dump complete
--

