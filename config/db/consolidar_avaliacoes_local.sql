﻿/*
select consolidar_avaliacoes_local(10223);

select l.id_local, l.nota_ranking, l.nota_geral, l.qtd_avaliacao,l.qtd_avaliacao_excelente, l.qtd_avaliacao_bom, l.qtd_avaliacao_regular,l.qtd_avaliacao_ruim, l.qtd_avaliacao_pessimo
  , (l.qtd_avaliacao_excelente * 0.01 + l.qtd_avaliacao_bom * 0.005 - l.qtd_avaliacao_ruim * 0.005 - l.qtd_avaliacao_pessimo * 0.01) ranking
  , (l.qtd_avaliacao_excelente * 0.01 + l.qtd_avaliacao_bom * 0.005 - l.qtd_avaliacao_ruim * 0.005 - l.qtd_avaliacao_pessimo * 0.01) / (l.qtd_avaliacao_excelente + l.qtd_avaliacao_bom + l.qtd_avaliacao_regular + l.qtd_avaliacao_ruim + l.qtd_avaliacao_pessimo) ranking_2
from local l where l.id_local = 10223;
*/

CREATE OR REPLACE FUNCTION consolidar_avaliacoes_local(p_id_local_avaliado bigint)
  RETURNS integer AS
$BODY$
DECLARE

    c_criterio_pessimo   CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 1;
    c_criterio_ruim      CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 2;
    c_criterio_regular   CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 3;
    c_criterio_bom       CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 4;
    c_criterio_excelente CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 5;

    v_id_avaliacao_criterio_anterior avaliacao_consolidada.id_avaliacao_criterio%type := NULL;
    v_id_avaliacao_criterio_atual    avaliacao_consolidada.id_avaliacao_criterio%type := NULL;

    v_qtd_avaliacao_bom               avaliacao_consolidada.qtd_avaliacao_bom%type := 0;
    v_qtd_avaliacao_excelente         avaliacao_consolidada.qtd_avaliacao_excelente%type := 0;
    v_qtd_avaliacao_regular           avaliacao_consolidada.qtd_avaliacao_regular%type := 0;
    v_qtd_avaliacao_ruim              avaliacao_consolidada.qtd_avaliacao_ruim%type := 0;
    v_qtd_avaliacao_pessimo           avaliacao_consolidada.qtd_avaliacao_pessimo%type := 0;

    v_classificacao_geral             avaliacao_consolidada.classificacao_geral%type := 0;
    v_nota_total                      avaliacao_consolidada.nota_geral%type := 0;
    v_nota_quantidade                 avaliacao_consolidada.nota_geral%type := 0;
    v_nota_geral                      avaliacao_consolidada.nota_geral%type := 0;
    v_nota_ranking                    local.nota_ranking%type := 0;
    v_nota_ranking_total              local.nota_ranking%type := 0;

    cur_avaliacao RECORD;
BEGIN

    -- Consolidação das avaliações por critério

    DELETE FROM avaliacao_consolidada WHERE id_local_avaliado = p_id_local_avaliado;

    FOR cur_avaliacao IN (SELECT a.id_local_avaliado, ac.id_avaliacao_criterio, ac.id_avaliacao_qualidade, count(ac.id_avaliacao_classificacao) qtd_avaliacoes
                            FROM avaliacao a, avaliacao_classificacao ac
                           WHERE a.id_avaliacao = ac.id_avaliacao
                             AND a.id_local_avaliado = p_id_local_avaliado
                             AND a.participa_consolidacao = true
                        GROUP BY a.id_local_avaliado, ac.id_avaliacao_criterio, ac.id_avaliacao_qualidade
                        ORDER BY ac.id_avaliacao_criterio) LOOP

        IF(v_id_avaliacao_criterio_atual IS NULL)THEN
            v_id_avaliacao_criterio_anterior := cur_avaliacao.id_avaliacao_criterio;
        END IF;
        v_id_avaliacao_criterio_atual := cur_avaliacao.id_avaliacao_criterio;

        IF(v_id_avaliacao_criterio_anterior <> v_id_avaliacao_criterio_atual)THEN

            v_nota_total := (
                            (v_qtd_avaliacao_excelente * 5) 
                          + (v_qtd_avaliacao_bom * 4) 
                          + (v_qtd_avaliacao_regular * 3)
                          + (v_qtd_avaliacao_ruim * 2)
                          + (v_qtd_avaliacao_pessimo * 1)
                          );
            v_nota_quantidade := (v_qtd_avaliacao_excelente
                                + v_qtd_avaliacao_bom
                                + v_qtd_avaliacao_regular
                                + v_qtd_avaliacao_ruim
                                + v_qtd_avaliacao_pessimo);
            v_nota_geral := v_nota_total / v_nota_quantidade;

	    v_classificacao_geral := round(v_nota_geral);

            INSERT INTO avaliacao_consolidada (
                  id_avaliacao_consolidada
                , id_local_avaliado
                , id_avaliacao_criterio
                , data_consolidacao_avaliacao
                , qtd_avaliacao_excelente
                , qtd_avaliacao_bom
                , qtd_avaliacao_regular
                , qtd_avaliacao_ruim
                , qtd_avaliacao_pessimo
                , classificacao_geral
                , nota_geral
            ) VALUES (
                  nextval('seq_avaliacao_consolidada')
                , p_id_local_avaliado
                , v_id_avaliacao_criterio_anterior
                , current_timestamp
                , v_qtd_avaliacao_excelente
                , v_qtd_avaliacao_bom
                , v_qtd_avaliacao_regular
                , v_qtd_avaliacao_ruim
                , v_qtd_avaliacao_pessimo
                , v_classificacao_geral
                , v_nota_geral
            );

            v_qtd_avaliacao_excelente := 0;
            v_qtd_avaliacao_bom := 0;
            v_qtd_avaliacao_regular := 0;
            v_qtd_avaliacao_ruim := 0;
            v_qtd_avaliacao_pessimo := 0;
            
        END IF;

        IF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_pessimo)THEN
            v_qtd_avaliacao_pessimo := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_ruim)THEN
            v_qtd_avaliacao_ruim := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_regular)THEN
            v_qtd_avaliacao_regular := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_bom)THEN
            v_qtd_avaliacao_bom := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_excelente)THEN
            v_qtd_avaliacao_excelente := cur_avaliacao.qtd_avaliacoes;
        END IF;

        v_id_avaliacao_criterio_anterior := v_id_avaliacao_criterio_atual;
    END LOOP;

    -- Consolidação da avaliação geral
    v_qtd_avaliacao_excelente := 0;
    v_qtd_avaliacao_bom := 0;
    v_qtd_avaliacao_regular := 0;
    v_qtd_avaliacao_ruim := 0;
    v_qtd_avaliacao_pessimo := 0;

    FOR cur_avaliacao IN (SELECT a.id_local_avaliado, a.classificacao_geral, count(id_avaliacao) qtd_avaliacoes
                            FROM avaliacao a
                           WHERE a.id_local_avaliado = p_id_local_avaliado
                             AND a.participa_consolidacao = true
                        GROUP BY a.id_local_avaliado, a.classificacao_geral) LOOP

        IF(cur_avaliacao.classificacao_geral = c_criterio_pessimo)THEN
            v_qtd_avaliacao_pessimo := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_ruim)THEN
            v_qtd_avaliacao_ruim := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_regular)THEN
            v_qtd_avaliacao_regular := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_bom)THEN
            v_qtd_avaliacao_bom := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_excelente)THEN
            v_qtd_avaliacao_excelente := cur_avaliacao.qtd_avaliacoes;
        END IF;

    END LOOP;

    v_nota_ranking_total := (
		    ((v_qtd_avaliacao_excelente * 5) + (v_qtd_avaliacao_excelente * 0.01))
		  + ((v_qtd_avaliacao_bom * 4) + (v_qtd_avaliacao_bom * 0.005)) 
		  + (v_qtd_avaliacao_regular * 3)
		  + ((v_qtd_avaliacao_ruim * 2) - (v_qtd_avaliacao_ruim * 0.005))
		  + ((v_qtd_avaliacao_pessimo * 1) - (v_qtd_avaliacao_pessimo * 0.01))
		  );
    v_nota_total := (
		    (v_qtd_avaliacao_excelente * 5) 
		  + (v_qtd_avaliacao_bom * 4) 
		  + (v_qtd_avaliacao_regular * 3)
		  + (v_qtd_avaliacao_ruim * 2)
		  + (v_qtd_avaliacao_pessimo * 1)
		  );
    v_nota_quantidade := (v_qtd_avaliacao_excelente
			+ v_qtd_avaliacao_bom
			+ v_qtd_avaliacao_regular
			+ v_qtd_avaliacao_ruim
			+ v_qtd_avaliacao_pessimo);
    v_nota_geral := v_nota_total / v_nota_quantidade;
	v_nota_ranking := v_nota_ranking_total / v_nota_quantidade;
    -- raise notice '[% = % / %]', v_nota_geral, v_nota_total, v_nota_quantidade;

    v_classificacao_geral := round(v_nota_geral);

    UPDATE local
       SET data_consolidacao_avaliacao = current_timestamp
         , qtd_avaliacao_excelente = v_qtd_avaliacao_excelente
         , qtd_avaliacao_bom = v_qtd_avaliacao_bom
         , qtd_avaliacao_regular = v_qtd_avaliacao_regular
         , qtd_avaliacao_ruim = v_qtd_avaliacao_ruim
         , qtd_avaliacao_pessimo = v_qtd_avaliacao_pessimo
         , classificacao_geral = v_classificacao_geral
         , nota_geral = v_nota_geral
         , nota_ranking = v_nota_ranking
     WHERE id_local = p_id_local_avaliado;

    return 0;
END;
$BODY$
  LANGUAGE plpgsql;