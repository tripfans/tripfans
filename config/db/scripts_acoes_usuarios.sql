-- SCRIPT PARA REMOVER ACOES REPETIDAS
DELETE 
  FROM acao_usuario
 WHERE id_acao_usuario IN (
        SELECT acao.id_acao_usuario
		  FROM acao_usuario acao, 
		       (SELECT DISTINCT tipo_acao_principal, id_acao_principal, id_tipo_acao, id_usuario_autor, data_acao, count(data_acao) quantidade_repeticoes, min(data_criacao) menor_data_criacao 
		                   FROM acao_usuario 
		               GROUP BY data_acao, tipo_acao_principal, id_acao_principal, id_tipo_acao, id_usuario_autor
		                 HAVING COUNT(data_acao) > 1) acoes_repetidas
		 WHERE acao.data_acao = acoes_repetidas.data_acao
		   AND data_criacao <> acoes_repetidas.menor_data_criacao
       );
       
-- SCRIPT PARA GERAR ACOES DE 'ENTOU NO TRIPFANS' () PARA OS USUARIOS QUE NAO TIVERAM A ACAO GERADA AUTOMATICAMENTE
       
INSERT 
  INTO acao_usuario (id_acao_usuario, id_acao_principal, data_acao, data_criacao, ponto, publica, publica_na_home, id_tipo_acao, visivel, id_usuario_autor)
       SELECT nextval('seq_acao_usuario'), u.id_usuario, u.data_criacao, u.data_criacao, 2.0, false, false, 60, true, u.id_usuario
         FROM usuario u 
        WHERE NOT EXISTS (SELECT a.id_usuario_autor
                            FROM acao_usuario a 
                           WHERE a.id_usuario_autor = u.id_usuario
                             AND a.id_tipo_acao = 60)
          AND u.ativo = true       