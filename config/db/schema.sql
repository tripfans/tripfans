--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.3
-- Dumped by pg_dump version 9.1.4
-- Started on 2013-05-17 12:59:38 BRT

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 246 (class 3079 OID 11645)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2489 (class 0 OID 0)
-- Dependencies: 246
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 263 (class 1255 OID 16386)
-- Dependencies: 784 6
-- Name: consolidar_avaliacoes_local(bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION consolidar_avaliacoes_local(p_id_local_avaliado bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE

    c_criterio_pessimo   CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 1;
    c_criterio_ruim      CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 2;
    c_criterio_regular   CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 3;
    c_criterio_bom       CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 4;
    c_criterio_excelente CONSTANT avaliacao_consolidada.id_avaliacao_criterio%type := 5;

    v_id_avaliacao_criterio_anterior avaliacao_consolidada.id_avaliacao_criterio%type := NULL;
    v_id_avaliacao_criterio_atual    avaliacao_consolidada.id_avaliacao_criterio%type := NULL;

    v_qtd_avaliacao_bom               avaliacao_consolidada.qtd_avaliacao_bom%type := 0;
    v_qtd_avaliacao_excelente         avaliacao_consolidada.qtd_avaliacao_excelente%type := 0;
    v_qtd_avaliacao_regular           avaliacao_consolidada.qtd_avaliacao_regular%type := 0;
    v_qtd_avaliacao_ruim              avaliacao_consolidada.qtd_avaliacao_ruim%type := 0;
    v_qtd_avaliacao_pessimo           avaliacao_consolidada.qtd_avaliacao_pessimo%type := 0;

    v_classificacao_geral             avaliacao_consolidada.classificacao_geral%type := 0;
    v_nota_total                      avaliacao_consolidada.nota_geral%type := 0;
    v_nota_quantidade                 avaliacao_consolidada.nota_geral%type := 0;
    v_nota_geral                      avaliacao_consolidada.nota_geral%type := 0;
    v_nota_ranking                    local.nota_ranking%type := 0;
    v_nota_ranking_total              local.nota_ranking%type := 0;

    cur_avaliacao RECORD;
BEGIN

    -- Consolidação das avaliações por critério

    DELETE FROM avaliacao_consolidada WHERE id_local_avaliado = p_id_local_avaliado;

    FOR cur_avaliacao IN (SELECT a.id_local_avaliado, ac.id_avaliacao_criterio, ac.id_avaliacao_qualidade, count(ac.id_avaliacao_classificacao) qtd_avaliacoes
                            FROM avaliacao a, avaliacao_classificacao ac
                           WHERE a.id_avaliacao = ac.id_avaliacao
                             AND a.id_local_avaliado = p_id_local_avaliado
                             AND a.participa_consolidacao = true
                        GROUP BY a.id_local_avaliado, ac.id_avaliacao_criterio, ac.id_avaliacao_qualidade
                        ORDER BY ac.id_avaliacao_criterio) LOOP

        IF(v_id_avaliacao_criterio_atual IS NULL)THEN
            v_id_avaliacao_criterio_anterior := cur_avaliacao.id_avaliacao_criterio;
        END IF;
        v_id_avaliacao_criterio_atual := cur_avaliacao.id_avaliacao_criterio;

        IF(v_id_avaliacao_criterio_anterior <> v_id_avaliacao_criterio_atual)THEN

            v_nota_total := (
                            (v_qtd_avaliacao_excelente * 5) 
                          + (v_qtd_avaliacao_bom * 4) 
                          + (v_qtd_avaliacao_regular * 3)
                          + (v_qtd_avaliacao_ruim * 2)
                          + (v_qtd_avaliacao_pessimo * 1)
                          );
            v_nota_quantidade := (v_qtd_avaliacao_excelente
                                + v_qtd_avaliacao_bom
                                + v_qtd_avaliacao_regular
                                + v_qtd_avaliacao_ruim
                                + v_qtd_avaliacao_pessimo);
            v_nota_geral := v_nota_total / v_nota_quantidade;

	    v_classificacao_geral := round(v_nota_geral);

            INSERT INTO avaliacao_consolidada (
                  id_avaliacao_consolidada
                , id_local_avaliado
                , id_avaliacao_criterio
                , data_consolidacao_avaliacao
                , qtd_avaliacao_excelente
                , qtd_avaliacao_bom
                , qtd_avaliacao_regular
                , qtd_avaliacao_ruim
                , qtd_avaliacao_pessimo
                , classificacao_geral
                , nota_geral
            ) VALUES (
                  nextval('seq_avaliacao_consolidada')
                , p_id_local_avaliado
                , v_id_avaliacao_criterio_anterior
                , current_timestamp
                , v_qtd_avaliacao_excelente
                , v_qtd_avaliacao_bom
                , v_qtd_avaliacao_regular
                , v_qtd_avaliacao_ruim
                , v_qtd_avaliacao_pessimo
                , v_classificacao_geral
                , v_nota_geral
            );

            v_qtd_avaliacao_excelente := 0;
            v_qtd_avaliacao_bom := 0;
            v_qtd_avaliacao_regular := 0;
            v_qtd_avaliacao_ruim := 0;
            v_qtd_avaliacao_pessimo := 0;
            
        END IF;

        IF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_pessimo)THEN
            v_qtd_avaliacao_pessimo := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_ruim)THEN
            v_qtd_avaliacao_ruim := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_regular)THEN
            v_qtd_avaliacao_regular := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_bom)THEN
            v_qtd_avaliacao_bom := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.id_avaliacao_qualidade = c_criterio_excelente)THEN
            v_qtd_avaliacao_excelente := cur_avaliacao.qtd_avaliacoes;
        END IF;

        v_id_avaliacao_criterio_anterior := v_id_avaliacao_criterio_atual;
    END LOOP;

    -- Consolidação da avaliação geral
    v_qtd_avaliacao_excelente := 0;
    v_qtd_avaliacao_bom := 0;
    v_qtd_avaliacao_regular := 0;
    v_qtd_avaliacao_ruim := 0;
    v_qtd_avaliacao_pessimo := 0;

    FOR cur_avaliacao IN (SELECT a.id_local_avaliado, a.classificacao_geral, count(id_avaliacao) qtd_avaliacoes
                            FROM avaliacao a
                           WHERE a.id_local_avaliado = p_id_local_avaliado
                             AND a.participa_consolidacao = true
                        GROUP BY a.id_local_avaliado, a.classificacao_geral) LOOP

        IF(cur_avaliacao.classificacao_geral = c_criterio_pessimo)THEN
            v_qtd_avaliacao_pessimo := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_ruim)THEN
            v_qtd_avaliacao_ruim := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_regular)THEN
            v_qtd_avaliacao_regular := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_bom)THEN
            v_qtd_avaliacao_bom := cur_avaliacao.qtd_avaliacoes;
        ELSIF(cur_avaliacao.classificacao_geral = c_criterio_excelente)THEN
            v_qtd_avaliacao_excelente := cur_avaliacao.qtd_avaliacoes;
        END IF;

    END LOOP;

    v_nota_ranking_total := (
		    ((v_qtd_avaliacao_excelente * 5) + (v_qtd_avaliacao_excelente * 0.01))
		  + ((v_qtd_avaliacao_bom * 4) + (v_qtd_avaliacao_bom * 0.005)) 
		  + (v_qtd_avaliacao_regular * 3)
		  + ((v_qtd_avaliacao_ruim * 2) - (v_qtd_avaliacao_ruim * 0.005))
		  + ((v_qtd_avaliacao_pessimo * 1) - (v_qtd_avaliacao_pessimo * 0.01))
		  );
    v_nota_total := (
		    (v_qtd_avaliacao_excelente * 5) 
		  + (v_qtd_avaliacao_bom * 4) 
		  + (v_qtd_avaliacao_regular * 3)
		  + (v_qtd_avaliacao_ruim * 2)
		  + (v_qtd_avaliacao_pessimo * 1)
		  );
    v_nota_quantidade := (v_qtd_avaliacao_excelente
			+ v_qtd_avaliacao_bom
			+ v_qtd_avaliacao_regular
			+ v_qtd_avaliacao_ruim
			+ v_qtd_avaliacao_pessimo);
    v_nota_geral := v_nota_total / v_nota_quantidade;
	v_nota_ranking := v_nota_ranking_total / v_nota_quantidade;
    -- raise notice '[% = % / %]', v_nota_geral, v_nota_total, v_nota_quantidade;

    v_classificacao_geral := round(v_nota_geral);

    UPDATE local
       SET data_consolidacao_avaliacao = current_timestamp
         , qtd_avaliacao_excelente = v_qtd_avaliacao_excelente
         , qtd_avaliacao_bom = v_qtd_avaliacao_bom
         , qtd_avaliacao_regular = v_qtd_avaliacao_regular
         , qtd_avaliacao_ruim = v_qtd_avaliacao_ruim
         , qtd_avaliacao_pessimo = v_qtd_avaliacao_pessimo
         , classificacao_geral = v_classificacao_geral
         , nota_geral = v_nota_geral
         , nota_ranking = v_nota_ranking
     WHERE id_local = p_id_local_avaliado;

    return 0;
END;
$$;


ALTER FUNCTION public.consolidar_avaliacoes_local(p_id_local_avaliado bigint) OWNER TO postgres;

--
-- TOC entry 274 (class 1255 OID 16387)
-- Dependencies: 784 6
-- Name: consolidar_quantidades_local(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION consolidar_quantidades_local() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    qtd_registros_alterados integer;
    cur_quantidade RECORD;
BEGIN
	update local set qtd_avaliacao = 0, qtd_avaliacao_bom = 0, qtd_avaliacao_excelente = 0, qtd_avaliacao_pessimo = 0,
	qtd_avaliacao_regular = 0, qtd_avaliacao_ruim = 0, classificacao_geral = 0, nota_geral = 0.00,  qtd_dica = 0, qtd_foto = 0, qtd_noticia = 0, qtd_pergunta = 0,
	qtd_interesse = 0, qtd_interesse_deseja_ir = 0, qtd_interesse_imperdivel = 0, qtd_interesse_indica = 0,
	qtd_interesse_ja_foi = 0, qtd_interesse_local_favorito = 0, qtd_interesse_seguir = 0;
	
    qtd_registros_alterados := 0;

    FOR cur_quantidade IN (select l.id_local, count(a.id_avaliacao) qtd_avaliacao
                             from local l, avaliacao a
                            where l.id_local = a.id_local_avaliado	
                            group by l.id_local
                           having count(a.id_avaliacao) <> l.qtd_avaliacao) LOOP
        update local 
           set qtd_avaliacao = cur_quantidade.qtd_avaliacao
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select c.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local c, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_cidade = c.id_local)
							group by c.id_local
                       having count(a.id_avaliacao) <> c.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select e.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local e, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_estado = e.id_local)
							group by e.id_local
                       having count(a.id_avaliacao) <> e.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select p.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local p, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_pais = p.id_local)
							group by p.id_local
                       having count(a.id_avaliacao) <> p.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(d.id_dica) qtd_dica
                             from local l, dica d
                            where l.id_local = d.local_dica	
                            group by l.id_local
                           having count(d.id_dica) <> l.qtd_dica) LOOP
        update local 
           set qtd_dica = cur_quantidade.qtd_dica
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select c.id_local, count(d.id_dica) qtd_dica
							from local c, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_cidade = c.id_local)
							group by c.id_local
                       having count(d.id_dica) <> c.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select e.id_local, count(d.id_dica) qtd_dica
							from local e, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_estado = e.id_local)
							group by e.id_local
                       having count(d.id_dica) <> e.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select p.id_local, count(d.id_dica) qtd_dica
							from local p, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_pais = p.id_local)
							group by p.id_local
                       having count(d.id_dica) <> p.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND l.id_local = f.id_local
							group by l.id_local
                            having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_cidade = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_estado = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_pais = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(n.id_noticia_sobre_local) qtd_noticia
                             from local l, noticia_sobre_local_para_local n
                            where l.id_local = n.id_local_interessado
                            group by l.id_local
                           having count(n.id_noticia_sobre_local) <> l.qtd_noticia) LOOP
        update local 
           set qtd_noticia = cur_quantidade.qtd_noticia
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(p.id_pergunta) qtd_pergunta
                             from local l, pergunta p
                            where l.id_local = p.id_local
                            group by l.id_local
                           having count(p.id_pergunta) <> l.qtd_pergunta) LOOP
        update local 
           set qtd_pergunta = cur_quantidade.qtd_pergunta
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_desejar_ir = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_deseja_ir) LOOP
        update local
           set qtd_interesse_deseja_ir = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_imperdivel = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_imperdivel) LOOP
        update local
           set qtd_interesse_imperdivel = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_indica = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_indica) LOOP
        update local
           set qtd_interesse_indica = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_ja_foi = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_ja_foi) LOOP
        update local
           set qtd_interesse_ja_foi = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_local_favorito = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_local_favorito) LOOP
        update local
           set qtd_interesse_local_favorito = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_seguir = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_seguir) LOOP
        update local
           set qtd_interesse_seguir = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and (i.interesse_desejar_ir = true
									OR i.interesse_local_favorito = true
									OR i.interesse_imperdivel = true
									OR i.interesse_indica = true
									OR i.interesse_ja_foi = true
									OR i.interesse_seguir = true)
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse) LOOP
        update local
           set qtd_interesse = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$$;


ALTER FUNCTION public.consolidar_quantidades_local() OWNER TO postgres;

--
-- TOC entry 258 (class 1255 OID 16388)
-- Dependencies: 6 784
-- Name: consolidar_quantidades_local_geografico(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION consolidar_quantidades_local_geografico() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	c_tp_aeroporto constant local.local_type%type := 5;
	c_tp_agencia constant local.local_type%type := 6;
	c_tp_atracao constant local.local_type%type := 7;
	c_tp_hotel constant local.local_type%type := 8;
	c_tp_imovel_aluguel_temporada constant local.local_type%type := 9;
	c_tp_locadora_veiculos constant local.local_type%type := 11;
	c_tp_restaurante constant local.local_type%type := 10;

	c_tp_continente constant local.local_type%type := 1;
	c_tp_pais constant local.local_type%type := 2;
	c_tp_estado constant local.local_type%type := 3;
	c_tp_cidade constant local.local_type%type := 4;
	
    qtd_registros_alterados integer;
	
    cur_local_geografico RECORD;
    cur_quantidade RECORD;

	v_qtd_aeroporto local.qtd_aeroporto%type;
	v_qtd_agencia local.qtd_agencia%type;
	v_qtd_atracao local.qtd_atracao%type;
	v_qtd_hotel local.qtd_hotel%type;
	v_qtd_restaurante local.qtd_restaurante%type;
	v_qtd_cidade local.qtd_cidade%type;
	v_qtd_estado local.qtd_estado%type;
	v_qtd_pais local.qtd_pais%type;
	
BEGIN
    qtd_registros_alterados := 0;

    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_continente) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_continente = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_pais) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_pais = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_estado) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_estado = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_cidade) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_cidade = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$$;


ALTER FUNCTION public.consolidar_quantidades_local_geografico() OWNER TO postgres;

--
-- TOC entry 265 (class 1255 OID 16389)
-- Dependencies: 784 6
-- Name: consolidar_quantidades_usuario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION consolidar_quantidades_usuario() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
    qtd_registros_alterados integer;
    cur_quantidade RECORD;
BEGIN
    qtd_registros_alterados := 0;

    FOR cur_quantidade IN (select u.id_usuario, count(a.id_amizade) qtd
                             from usuario u, amizade a
                            where u.id_usuario = a.id_usuario	
                            group by u.id_usuario
                           having count(a.id_amizade) <> u.qtd_amigo) LOOP
        update usuario
           set qtd_amigo = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(a.id_avaliacao) qtd
                             from usuario u, avaliacao a
                            where u.id_usuario = a.autor
                            group by u.id_usuario
                           having count(a.id_avaliacao) <> u.qtd_avaliacao) LOOP
        update usuario
           set qtd_avaliacao = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(c.id_comentario) qtd
                             from usuario u, comentario c
                            where u.id_usuario = c.id_autor
                            group by u.id_usuario
                           having count(c.id_comentario) <> u.qtd_comentario) LOOP
        update usuario
           set qtd_comentario = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(d.id_dica) qtd
                             from usuario u, dica d
                            where u.id_usuario = d.autor
                            group by u.id_usuario
                           having count(d.id_dica) <> u.qtd_dica) LOOP
        update usuario
           set qtd_dica = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(f.id_foto) qtd
                             from usuario u, foto f
                            where u.id_usuario = f.id_autor and f.anonima = false
                            group by u.id_usuario
                           having count(f.id_foto) <> u.qtd_foto) LOOP
        update usuario
           set qtd_foto = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(n.id_noticia_sobre_local) qtd
                             from usuario u, noticia_sobre_local_para_usuario n
                            where u.id_usuario = n.id_usuario_interessado
                            group by u.id_usuario
                           having count(n.id_noticia_sobre_local) <> u.qtd_noticia) LOOP
        update usuario
           set qtd_noticia = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(p.id_pergunta) qtd
                             from usuario u, pergunta p
                            where u.id_usuario = p.id_autor
                            group by u.id_usuario
                           having count(p.id_pergunta) <> u.qtd_pergunta) LOOP
        update usuario
           set qtd_pergunta = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(r.id_resposta) qtd
                             from usuario u, resposta r
                            where u.id_usuario = r.id_autor
                            group by u.id_usuario
                           having count(r.id_resposta) <> u.qtd_resposta) LOOP
        update usuario
           set qtd_resposta = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(v.id) qtd
                             from usuario u, viagem v
                            where u.id_usuario = v.criador
                            group by u.id_usuario
                           having count(v.id) <> u.qtd_viagem) LOOP
        update usuario
           set qtd_viagem = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_desejar_ir = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_deseja_ir) LOOP
        update usuario
           set qtd_interesse_deseja_ir = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_imperdivel = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_imperdivel) LOOP
        update usuario
           set qtd_interesse_imperdivel = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_indica = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_indica) LOOP
        update usuario
           set qtd_interesse_indica = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_ja_foi = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_ja_foi) LOOP
        update usuario
           set qtd_interesse_ja_foi = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_local_favorito = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_local_favorito) LOOP
        update usuario
           set qtd_interesse_local_favorito = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_seguir = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_seguir) LOOP
        update usuario
           set qtd_interesse_seguir = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and (i.interesse_desejar_ir = true
									OR i.interesse_escreveu_avaliacao = true
									OR i.interesse_escreveu_dica = true
									OR i.interesse_escreveu_pergunta = true
									OR i.interesse_local_favorito = true
									OR i.interesse_imperdivel = true
									OR i.interesse_indica = true
									OR i.interesse_ja_foi = true
									OR i.interesse_respondeu_pergunta = true
									OR i.interesse_seguir = true)
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_local_algum_interesse) LOOP
        update usuario
           set qtd_local_algum_interesse = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$$;


ALTER FUNCTION public.consolidar_quantidades_usuario() OWNER TO postgres;

--
-- TOC entry 261 (class 1255 OID 19897)
-- Dependencies: 6 784
-- Name: localidade_atualizar(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION localidade_atualizar() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
 DECLARE 
    v_qtd_locais_desatualizados  INTEGER; 
    cursor_tabela                RECORD; 
 BEGIN 
    v_qtd_locais_desatualizados := 0; 

    v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_local();

    v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_local_mais_especifico();

    -- v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + localidade_atualizar_outras_tabelas('noticia_sobre_local_para_local');

    RETURN v_qtd_locais_desatualizados; 
 END; 
 $$;


ALTER FUNCTION public.localidade_atualizar() OWNER TO postgres;

--
-- TOC entry 268 (class 1255 OID 19892)
-- Dependencies: 6 784
-- Name: localidade_atualizar_local(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION localidade_atualizar_local() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
 DECLARE 
    v_qtd_locais_desatualizados   INTEGER; 
    cursor_local                 RECORD; 
 BEGIN 
    v_qtd_locais_desatualizados := 0; 

        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      4
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_cidade_id = l.id_local
                           AND (
                               ((l2.d_cidade_id_facebook <> l.id_facebook) OR (l2.d_cidade_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_cidade_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_cidade_id_foto_padrao <> l.id_foto) OR (l2.d_cidade_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_cidade_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_cidade_id_foursquare <> l.id_foursquare) OR (l2.d_cidade_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_cidade_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_cidade_nome <> l.nome) OR (l2.d_cidade_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_cidade_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_cidade_sigla <> l.sigla) OR (l2.d_cidade_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_cidade_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_cidade_type <> l.local_type) OR (l2.d_cidade_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_cidade_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_cidade_url_path <> l.url_path) OR (l2.d_cidade_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_cidade_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_cidade_id_facebook = cursor_local.id_facebook
                  , d_cidade_id_foto_padrao = cursor_local.id_foto
                  , d_cidade_id_foursquare = cursor_local.id_foursquare
                  , d_cidade_nome = cursor_local.nome
                  , d_cidade_sigla = cursor_local.sigla
                  , d_cidade_type = cursor_local.local_type
                  , d_cidade_url_path = cursor_local.url_path
            WHERE d_cidade_id = cursor_local.id_local
               AND (
                   ((d_cidade_id_facebook <> cursor_local.id_facebook) OR (d_cidade_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_cidade_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_cidade_id_foto_padrao <> cursor_local.id_foto) OR (d_cidade_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_cidade_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_cidade_id_foursquare <> cursor_local.id_foursquare) OR (d_cidade_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_cidade_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_cidade_nome <> cursor_local.nome) OR (d_cidade_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_cidade_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_cidade_sigla <> cursor_local.sigla) OR (d_cidade_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_cidade_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_cidade_type <> cursor_local.local_type) OR (d_cidade_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_cidade_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_cidade_url_path <> cursor_local.url_path) OR (d_cidade_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_cidade_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;

        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      1
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_continente_id = l.id_local
                           AND (
                               ((l2.d_continente_id_facebook <> l.id_facebook) OR (l2.d_continente_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_continente_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_continente_id_foto_padrao <> l.id_foto) OR (l2.d_continente_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_continente_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_continente_id_foursquare <> l.id_foursquare) OR (l2.d_continente_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_continente_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_continente_nome <> l.nome) OR (l2.d_continente_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_continente_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_continente_sigla <> l.sigla) OR (l2.d_continente_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_continente_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_continente_type <> l.local_type) OR (l2.d_continente_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_continente_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_continente_url_path <> l.url_path) OR (l2.d_continente_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_continente_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_continente_id_facebook = cursor_local.id_facebook
                  , d_continente_id_foto_padrao = cursor_local.id_foto
                  , d_continente_id_foursquare = cursor_local.id_foursquare
                  , d_continente_nome = cursor_local.nome
                  , d_continente_sigla = cursor_local.sigla
                  , d_continente_type = cursor_local.local_type
                  , d_continente_url_path = cursor_local.url_path
            WHERE d_continente_id = cursor_local.id_local
               AND (
                   ((d_continente_id_facebook <> cursor_local.id_facebook) OR (d_continente_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_continente_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_continente_id_foto_padrao <> cursor_local.id_foto) OR (d_continente_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_continente_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_continente_id_foursquare <> cursor_local.id_foursquare) OR (d_continente_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_continente_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_continente_nome <> cursor_local.nome) OR (d_continente_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_continente_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_continente_sigla <> cursor_local.sigla) OR (d_continente_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_continente_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_continente_type <> cursor_local.local_type) OR (d_continente_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_continente_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_continente_url_path <> cursor_local.url_path) OR (d_continente_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_continente_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;

        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      3
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_estado_id = l.id_local
                           AND (
                               ((l2.d_estado_id_facebook <> l.id_facebook) OR (l2.d_estado_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_estado_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_estado_id_foto_padrao <> l.id_foto) OR (l2.d_estado_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_estado_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_estado_id_foursquare <> l.id_foursquare) OR (l2.d_estado_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_estado_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_estado_nome <> l.nome) OR (l2.d_estado_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_estado_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_estado_sigla <> l.sigla) OR (l2.d_estado_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_estado_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_estado_type <> l.local_type) OR (l2.d_estado_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_estado_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_estado_url_path <> l.url_path) OR (l2.d_estado_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_estado_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_estado_id_facebook = cursor_local.id_facebook
                  , d_estado_id_foto_padrao = cursor_local.id_foto
                  , d_estado_id_foursquare = cursor_local.id_foursquare
                  , d_estado_nome = cursor_local.nome
                  , d_estado_sigla = cursor_local.sigla
                  , d_estado_type = cursor_local.local_type
                  , d_estado_url_path = cursor_local.url_path
            WHERE d_estado_id = cursor_local.id_local
               AND (
                   ((d_estado_id_facebook <> cursor_local.id_facebook) OR (d_estado_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_estado_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_estado_id_foto_padrao <> cursor_local.id_foto) OR (d_estado_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_estado_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_estado_id_foursquare <> cursor_local.id_foursquare) OR (d_estado_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_estado_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_estado_nome <> cursor_local.nome) OR (d_estado_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_estado_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_estado_sigla <> cursor_local.sigla) OR (d_estado_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_estado_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_estado_type <> cursor_local.local_type) OR (d_estado_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_estado_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_estado_url_path <> cursor_local.url_path) OR (d_estado_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_estado_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;

        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      5
                    , 6
                    , 7
                    , 8
                    , 9
                    , 11
                    , 10
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_local_enderecavel_id = l.id_local
                           AND (
                               ((l2.d_local_enderecavel_id_facebook <> l.id_facebook) OR (l2.d_local_enderecavel_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_local_enderecavel_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_local_enderecavel_id_foto_padrao <> l.id_foto) OR (l2.d_local_enderecavel_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_local_enderecavel_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_local_enderecavel_id_foursquare <> l.id_foursquare) OR (l2.d_local_enderecavel_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_local_enderecavel_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_local_enderecavel_nome <> l.nome) OR (l2.d_local_enderecavel_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_local_enderecavel_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_local_enderecavel_sigla <> l.sigla) OR (l2.d_local_enderecavel_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_local_enderecavel_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_local_enderecavel_type <> l.local_type) OR (l2.d_local_enderecavel_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_local_enderecavel_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_local_enderecavel_url_path <> l.url_path) OR (l2.d_local_enderecavel_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_local_enderecavel_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_local_enderecavel_id_facebook = cursor_local.id_facebook
                  , d_local_enderecavel_id_foto_padrao = cursor_local.id_foto
                  , d_local_enderecavel_id_foursquare = cursor_local.id_foursquare
                  , d_local_enderecavel_nome = cursor_local.nome
                  , d_local_enderecavel_sigla = cursor_local.sigla
                  , d_local_enderecavel_type = cursor_local.local_type
                  , d_local_enderecavel_url_path = cursor_local.url_path
            WHERE d_local_enderecavel_id = cursor_local.id_local
               AND (
                   ((d_local_enderecavel_id_facebook <> cursor_local.id_facebook) OR (d_local_enderecavel_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_local_enderecavel_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_local_enderecavel_id_foto_padrao <> cursor_local.id_foto) OR (d_local_enderecavel_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_local_enderecavel_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_local_enderecavel_id_foursquare <> cursor_local.id_foursquare) OR (d_local_enderecavel_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_local_enderecavel_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_local_enderecavel_nome <> cursor_local.nome) OR (d_local_enderecavel_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_local_enderecavel_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_local_enderecavel_sigla <> cursor_local.sigla) OR (d_local_enderecavel_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_local_enderecavel_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_local_enderecavel_type <> cursor_local.local_type) OR (d_local_enderecavel_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_local_enderecavel_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_local_enderecavel_url_path <> cursor_local.url_path) OR (d_local_enderecavel_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_local_enderecavel_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;

        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      12
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_local_interesse_turistico_id = l.id_local
                           AND (
                               ((l2.d_local_interesse_turistico_id_facebook <> l.id_facebook) OR (l2.d_local_interesse_turistico_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_local_interesse_turistico_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_local_interesse_turistico_id_foto_padrao <> l.id_foto) OR (l2.d_local_interesse_turistico_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_local_interesse_turistico_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_local_interesse_turistico_id_foursquare <> l.id_foursquare) OR (l2.d_local_interesse_turistico_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_local_interesse_turistico_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_local_interesse_turistico_nome <> l.nome) OR (l2.d_local_interesse_turistico_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_local_interesse_turistico_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_local_interesse_turistico_sigla <> l.sigla) OR (l2.d_local_interesse_turistico_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_local_interesse_turistico_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_local_interesse_turistico_type <> l.local_type) OR (l2.d_local_interesse_turistico_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_local_interesse_turistico_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_local_interesse_turistico_url_path <> l.url_path) OR (l2.d_local_interesse_turistico_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_local_interesse_turistico_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_local_interesse_turistico_id_facebook = cursor_local.id_facebook
                  , d_local_interesse_turistico_id_foto_padrao = cursor_local.id_foto
                  , d_local_interesse_turistico_id_foursquare = cursor_local.id_foursquare
                  , d_local_interesse_turistico_nome = cursor_local.nome
                  , d_local_interesse_turistico_sigla = cursor_local.sigla
                  , d_local_interesse_turistico_type = cursor_local.local_type
                  , d_local_interesse_turistico_url_path = cursor_local.url_path
            WHERE d_local_interesse_turistico_id = cursor_local.id_local
               AND (
                   ((d_local_interesse_turistico_id_facebook <> cursor_local.id_facebook) OR (d_local_interesse_turistico_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_local_interesse_turistico_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_local_interesse_turistico_id_foto_padrao <> cursor_local.id_foto) OR (d_local_interesse_turistico_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_local_interesse_turistico_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_local_interesse_turistico_id_foursquare <> cursor_local.id_foursquare) OR (d_local_interesse_turistico_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_local_interesse_turistico_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_local_interesse_turistico_nome <> cursor_local.nome) OR (d_local_interesse_turistico_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_local_interesse_turistico_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_local_interesse_turistico_sigla <> cursor_local.sigla) OR (d_local_interesse_turistico_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_local_interesse_turistico_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_local_interesse_turistico_type <> cursor_local.local_type) OR (d_local_interesse_turistico_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_local_interesse_turistico_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_local_interesse_turistico_url_path <> cursor_local.url_path) OR (d_local_interesse_turistico_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_local_interesse_turistico_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;


        FOR cursor_local IN (SELECT
                  id_local
                , id_facebook
                , id_foto
                , id_foursquare
                , nome
                , sigla
                , local_type
                , url_path
             FROM Local l
            WHERE l.local_type IN (
                      2
                   )
                AND EXISTS (SELECT 1
                         FROM Local l2
                         WHERE l2.d_pais_id = l.id_local
                           AND (
                               ((l2.d_pais_id_facebook <> l.id_facebook) OR (l2.d_pais_id_facebook IS NULL AND l.id_facebook IS NOT NULL) OR (l2.d_pais_id_facebook IS NOT NULL AND l.id_facebook IS NULL))
                            OR ((l2.d_pais_id_foto_padrao <> l.id_foto) OR (l2.d_pais_id_foto_padrao IS NULL AND l.id_foto IS NOT NULL) OR (l2.d_pais_id_foto_padrao IS NOT NULL AND l.id_foto IS NULL))
                            OR ((l2.d_pais_id_foursquare <> l.id_foursquare) OR (l2.d_pais_id_foursquare IS NULL AND l.id_foursquare IS NOT NULL) OR (l2.d_pais_id_foursquare IS NOT NULL AND l.id_foursquare IS NULL))
                            OR ((l2.d_pais_nome <> l.nome) OR (l2.d_pais_nome IS NULL AND l.nome IS NOT NULL) OR (l2.d_pais_nome IS NOT NULL AND l.nome IS NULL))
                            OR ((l2.d_pais_sigla <> l.sigla) OR (l2.d_pais_sigla IS NULL AND l.sigla IS NOT NULL) OR (l2.d_pais_sigla IS NOT NULL AND l.sigla IS NULL))
                            OR ((l2.d_pais_type <> l.local_type) OR (l2.d_pais_type IS NULL AND l.local_type IS NOT NULL) OR (l2.d_pais_type IS NOT NULL AND l.local_type IS NULL))
                            OR ((l2.d_pais_url_path <> l.url_path) OR (l2.d_pais_url_path IS NULL AND l.url_path IS NOT NULL) OR (l2.d_pais_url_path IS NOT NULL AND l.url_path IS NULL))
                            )
                        )
            )
        LOOP

            UPDATE Local
                SET d_pais_id_facebook = cursor_local.id_facebook
                  , d_pais_id_foto_padrao = cursor_local.id_foto
                  , d_pais_id_foursquare = cursor_local.id_foursquare
                  , d_pais_nome = cursor_local.nome
                  , d_pais_sigla = cursor_local.sigla
                  , d_pais_type = cursor_local.local_type
                  , d_pais_url_path = cursor_local.url_path
            WHERE d_pais_id = cursor_local.id_local
               AND (
                   ((d_pais_id_facebook <> cursor_local.id_facebook) OR (d_pais_id_facebook IS NULL AND cursor_local.id_facebook IS NOT NULL) OR (d_pais_id_facebook IS NOT NULL AND cursor_local.id_facebook IS NULL))
                OR ((d_pais_id_foto_padrao <> cursor_local.id_foto) OR (d_pais_id_foto_padrao IS NULL AND cursor_local.id_foto IS NOT NULL) OR (d_pais_id_foto_padrao IS NOT NULL AND cursor_local.id_foto IS NULL))
                OR ((d_pais_id_foursquare <> cursor_local.id_foursquare) OR (d_pais_id_foursquare IS NULL AND cursor_local.id_foursquare IS NOT NULL) OR (d_pais_id_foursquare IS NOT NULL AND cursor_local.id_foursquare IS NULL))
                OR ((d_pais_nome <> cursor_local.nome) OR (d_pais_nome IS NULL AND cursor_local.nome IS NOT NULL) OR (d_pais_nome IS NOT NULL AND cursor_local.nome IS NULL))
                OR ((d_pais_sigla <> cursor_local.sigla) OR (d_pais_sigla IS NULL AND cursor_local.sigla IS NOT NULL) OR (d_pais_sigla IS NOT NULL AND cursor_local.sigla IS NULL))
                OR ((d_pais_type <> cursor_local.local_type) OR (d_pais_type IS NULL AND cursor_local.local_type IS NOT NULL) OR (d_pais_type IS NOT NULL AND cursor_local.local_type IS NULL))
                OR ((d_pais_url_path <> cursor_local.url_path) OR (d_pais_url_path IS NULL AND cursor_local.url_path IS NOT NULL) OR (d_pais_url_path IS NOT NULL AND cursor_local.url_path IS NULL))
            )
            ;

            v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
        END LOOP;


    RETURN v_qtd_locais_desatualizados; 
 END; 
 $$;


ALTER FUNCTION public.localidade_atualizar_local() OWNER TO postgres;

--
-- TOC entry 259 (class 1255 OID 19894)
-- Dependencies: 784 6
-- Name: localidade_atualizar_local_mais_especifico(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION localidade_atualizar_local_mais_especifico() RETURNS integer
    LANGUAGE plpgsql
    AS $$ 
 DECLARE 
    v_qtd_locais_desatualizados   INTEGER; 
    cursor_local                 RECORD; 
    v_sql                     TEXT; 
    v_update                  TEXT; 
 BEGIN 
    v_qtd_locais_desatualizados := 0; 

    UPDATE Local 
        SET d_local_mais_especifico_id = id_local
          , d_local_mais_especifico_id_facebook = id_facebook
          , d_local_mais_especifico_id_foto_padrao = id_foto
          , d_local_mais_especifico_id_foursquare = id_foursquare
          , d_local_mais_especifico_nome = nome
          , d_local_mais_especifico_sigla = sigla
          , d_local_mais_especifico_type = local_type
          , d_local_mais_especifico_url_path = url_path
        WHERE ((d_local_mais_especifico_id <> id_local) OR (d_local_mais_especifico_id IS NULL AND id_local IS NOT NULL) OR (d_local_mais_especifico_id IS NOT NULL AND id_local IS NULL))
           OR ((d_local_mais_especifico_id_facebook <> id_facebook) OR (d_local_mais_especifico_id_facebook IS NULL AND id_facebook IS NOT NULL) OR (d_local_mais_especifico_id_facebook IS NOT NULL AND id_facebook IS NULL))
           OR ((d_local_mais_especifico_id_foto_padrao <> id_foto) OR (d_local_mais_especifico_id_foto_padrao IS NULL AND id_foto IS NOT NULL) OR (d_local_mais_especifico_id_foto_padrao IS NOT NULL AND id_foto IS NULL))
           OR ((d_local_mais_especifico_id_foursquare <> id_foursquare) OR (d_local_mais_especifico_id_foursquare IS NULL AND id_foursquare IS NOT NULL) OR (d_local_mais_especifico_id_foursquare IS NOT NULL AND id_foursquare IS NULL))
           OR ((d_local_mais_especifico_nome <> nome) OR (d_local_mais_especifico_nome IS NULL AND nome IS NOT NULL) OR (d_local_mais_especifico_nome IS NOT NULL AND nome IS NULL))
           OR ((d_local_mais_especifico_sigla <> sigla) OR (d_local_mais_especifico_sigla IS NULL AND sigla IS NOT NULL) OR (d_local_mais_especifico_sigla IS NOT NULL AND sigla IS NULL))
           OR ((d_local_mais_especifico_type <> local_type) OR (d_local_mais_especifico_type IS NULL AND local_type IS NOT NULL) OR (d_local_mais_especifico_type IS NOT NULL AND local_type IS NULL))
           OR ((d_local_mais_especifico_url_path <> url_path) OR (d_local_mais_especifico_url_path IS NULL AND url_path IS NOT NULL) OR (d_local_mais_especifico_url_path IS NOT NULL AND url_path IS NULL))
    ;

    RETURN v_qtd_locais_desatualizados; 
 END; 
 $$;


ALTER FUNCTION public.localidade_atualizar_local_mais_especifico() OWNER TO postgres;

--
-- TOC entry 260 (class 1255 OID 19895)
-- Dependencies: 784 6
-- Name: localidade_atualizar_outras_tabelas(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION localidade_atualizar_outras_tabelas(p_table_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $_$ 
 DECLARE 
    v_qtd_locais_desatualizados   INTEGER; 
    cursor_local                 RECORD; 
    v_sql                     TEXT; 
    v_update                  TEXT; 
 BEGIN 
    v_qtd_locais_desatualizados := 0; 

    v_sql := 'SELECT 
              d_cidade_id
            , d_cidade_id_facebook
            , d_cidade_id_foto_padrao
            , d_cidade_id_foursquare
            , d_cidade_nome
            , d_cidade_sigla
            , d_cidade_type
            , d_cidade_url_path
            , d_continente_id
            , d_continente_id_facebook
            , d_continente_id_foto_padrao
            , d_continente_id_foursquare
            , d_continente_nome
            , d_continente_sigla
            , d_continente_type
            , d_continente_url_path
            , d_estado_id
            , d_estado_id_facebook
            , d_estado_id_foto_padrao
            , d_estado_id_foursquare
            , d_estado_nome
            , d_estado_sigla
            , d_estado_type
            , d_estado_url_path
            , d_local_enderecavel_id
            , d_local_enderecavel_id_facebook
            , d_local_enderecavel_id_foto_padrao
            , d_local_enderecavel_id_foursquare
            , d_local_enderecavel_nome
            , d_local_enderecavel_sigla
            , d_local_enderecavel_type
            , d_local_enderecavel_url_path
            , d_local_interesse_turistico_id
            , d_local_interesse_turistico_id_facebook
            , d_local_interesse_turistico_id_foto_padrao
            , d_local_interesse_turistico_id_foursquare
            , d_local_interesse_turistico_nome
            , d_local_interesse_turistico_sigla
            , d_local_interesse_turistico_type
            , d_local_interesse_turistico_url_path
            , d_local_mais_especifico_id
            , d_local_mais_especifico_id_facebook
            , d_local_mais_especifico_id_foto_padrao
            , d_local_mais_especifico_id_foursquare
            , d_local_mais_especifico_nome
            , d_local_mais_especifico_sigla
            , d_local_mais_especifico_type
            , d_local_mais_especifico_url_path
            , d_pais_id
            , d_pais_id_facebook
            , d_pais_id_foto_padrao
            , d_pais_id_foursquare
            , d_pais_nome
            , d_pais_sigla
            , d_pais_type
            , d_pais_url_path
      FROM Local l 
     WHERE EXISTS (SELECT 1 FROM '|| p_table_name ||' d 
                           WHERE l.d_local_mais_especifico_id = d.d_local_mais_especifico_id
                             AND (
                       ((l.d_cidade_id <> d.d_cidade_id) or (l.d_cidade_id IS NULL AND d.d_cidade_id IS NOT NULL) or (l.d_cidade_id IS NOT NULL AND d.d_cidade_id IS NULL))
                    OR ((l.d_cidade_id_facebook <> d.d_cidade_id_facebook) or (l.d_cidade_id_facebook IS NULL AND d.d_cidade_id_facebook IS NOT NULL) or (l.d_cidade_id_facebook IS NOT NULL AND d.d_cidade_id_facebook IS NULL))
                    OR ((l.d_cidade_id_foto_padrao <> d.d_cidade_id_foto_padrao) or (l.d_cidade_id_foto_padrao IS NULL AND d.d_cidade_id_foto_padrao IS NOT NULL) or (l.d_cidade_id_foto_padrao IS NOT NULL AND d.d_cidade_id_foto_padrao IS NULL))
                    OR ((l.d_cidade_id_foursquare <> d.d_cidade_id_foursquare) or (l.d_cidade_id_foursquare IS NULL AND d.d_cidade_id_foursquare IS NOT NULL) or (l.d_cidade_id_foursquare IS NOT NULL AND d.d_cidade_id_foursquare IS NULL))
                    OR ((l.d_cidade_nome <> d.d_cidade_nome) or (l.d_cidade_nome IS NULL AND d.d_cidade_nome IS NOT NULL) or (l.d_cidade_nome IS NOT NULL AND d.d_cidade_nome IS NULL))
                    OR ((l.d_cidade_sigla <> d.d_cidade_sigla) or (l.d_cidade_sigla IS NULL AND d.d_cidade_sigla IS NOT NULL) or (l.d_cidade_sigla IS NOT NULL AND d.d_cidade_sigla IS NULL))
                    OR ((l.d_cidade_type <> d.d_cidade_type) or (l.d_cidade_type IS NULL AND d.d_cidade_type IS NOT NULL) or (l.d_cidade_type IS NOT NULL AND d.d_cidade_type IS NULL))
                    OR ((l.d_cidade_url_path <> d.d_cidade_url_path) or (l.d_cidade_url_path IS NULL AND d.d_cidade_url_path IS NOT NULL) or (l.d_cidade_url_path IS NOT NULL AND d.d_cidade_url_path IS NULL))
                    OR ((l.d_continente_id <> d.d_continente_id) or (l.d_continente_id IS NULL AND d.d_continente_id IS NOT NULL) or (l.d_continente_id IS NOT NULL AND d.d_continente_id IS NULL))
                    OR ((l.d_continente_id_facebook <> d.d_continente_id_facebook) or (l.d_continente_id_facebook IS NULL AND d.d_continente_id_facebook IS NOT NULL) or (l.d_continente_id_facebook IS NOT NULL AND d.d_continente_id_facebook IS NULL))
                    OR ((l.d_continente_id_foto_padrao <> d.d_continente_id_foto_padrao) or (l.d_continente_id_foto_padrao IS NULL AND d.d_continente_id_foto_padrao IS NOT NULL) or (l.d_continente_id_foto_padrao IS NOT NULL AND d.d_continente_id_foto_padrao IS NULL))
                    OR ((l.d_continente_id_foursquare <> d.d_continente_id_foursquare) or (l.d_continente_id_foursquare IS NULL AND d.d_continente_id_foursquare IS NOT NULL) or (l.d_continente_id_foursquare IS NOT NULL AND d.d_continente_id_foursquare IS NULL))
                    OR ((l.d_continente_nome <> d.d_continente_nome) or (l.d_continente_nome IS NULL AND d.d_continente_nome IS NOT NULL) or (l.d_continente_nome IS NOT NULL AND d.d_continente_nome IS NULL))
                    OR ((l.d_continente_sigla <> d.d_continente_sigla) or (l.d_continente_sigla IS NULL AND d.d_continente_sigla IS NOT NULL) or (l.d_continente_sigla IS NOT NULL AND d.d_continente_sigla IS NULL))
                    OR ((l.d_continente_type <> d.d_continente_type) or (l.d_continente_type IS NULL AND d.d_continente_type IS NOT NULL) or (l.d_continente_type IS NOT NULL AND d.d_continente_type IS NULL))
                    OR ((l.d_continente_url_path <> d.d_continente_url_path) or (l.d_continente_url_path IS NULL AND d.d_continente_url_path IS NOT NULL) or (l.d_continente_url_path IS NOT NULL AND d.d_continente_url_path IS NULL))
                    OR ((l.d_estado_id <> d.d_estado_id) or (l.d_estado_id IS NULL AND d.d_estado_id IS NOT NULL) or (l.d_estado_id IS NOT NULL AND d.d_estado_id IS NULL))
                    OR ((l.d_estado_id_facebook <> d.d_estado_id_facebook) or (l.d_estado_id_facebook IS NULL AND d.d_estado_id_facebook IS NOT NULL) or (l.d_estado_id_facebook IS NOT NULL AND d.d_estado_id_facebook IS NULL))
                    OR ((l.d_estado_id_foto_padrao <> d.d_estado_id_foto_padrao) or (l.d_estado_id_foto_padrao IS NULL AND d.d_estado_id_foto_padrao IS NOT NULL) or (l.d_estado_id_foto_padrao IS NOT NULL AND d.d_estado_id_foto_padrao IS NULL))
                    OR ((l.d_estado_id_foursquare <> d.d_estado_id_foursquare) or (l.d_estado_id_foursquare IS NULL AND d.d_estado_id_foursquare IS NOT NULL) or (l.d_estado_id_foursquare IS NOT NULL AND d.d_estado_id_foursquare IS NULL))
                    OR ((l.d_estado_nome <> d.d_estado_nome) or (l.d_estado_nome IS NULL AND d.d_estado_nome IS NOT NULL) or (l.d_estado_nome IS NOT NULL AND d.d_estado_nome IS NULL))
                    OR ((l.d_estado_sigla <> d.d_estado_sigla) or (l.d_estado_sigla IS NULL AND d.d_estado_sigla IS NOT NULL) or (l.d_estado_sigla IS NOT NULL AND d.d_estado_sigla IS NULL))
                    OR ((l.d_estado_type <> d.d_estado_type) or (l.d_estado_type IS NULL AND d.d_estado_type IS NOT NULL) or (l.d_estado_type IS NOT NULL AND d.d_estado_type IS NULL))
                    OR ((l.d_estado_url_path <> d.d_estado_url_path) or (l.d_estado_url_path IS NULL AND d.d_estado_url_path IS NOT NULL) or (l.d_estado_url_path IS NOT NULL AND d.d_estado_url_path IS NULL))
                    OR ((l.d_local_enderecavel_id <> d.d_local_enderecavel_id) or (l.d_local_enderecavel_id IS NULL AND d.d_local_enderecavel_id IS NOT NULL) or (l.d_local_enderecavel_id IS NOT NULL AND d.d_local_enderecavel_id IS NULL))
                    OR ((l.d_local_enderecavel_id_facebook <> d.d_local_enderecavel_id_facebook) or (l.d_local_enderecavel_id_facebook IS NULL AND d.d_local_enderecavel_id_facebook IS NOT NULL) or (l.d_local_enderecavel_id_facebook IS NOT NULL AND d.d_local_enderecavel_id_facebook IS NULL))
                    OR ((l.d_local_enderecavel_id_foto_padrao <> d.d_local_enderecavel_id_foto_padrao) or (l.d_local_enderecavel_id_foto_padrao IS NULL AND d.d_local_enderecavel_id_foto_padrao IS NOT NULL) or (l.d_local_enderecavel_id_foto_padrao IS NOT NULL AND d.d_local_enderecavel_id_foto_padrao IS NULL))
                    OR ((l.d_local_enderecavel_id_foursquare <> d.d_local_enderecavel_id_foursquare) or (l.d_local_enderecavel_id_foursquare IS NULL AND d.d_local_enderecavel_id_foursquare IS NOT NULL) or (l.d_local_enderecavel_id_foursquare IS NOT NULL AND d.d_local_enderecavel_id_foursquare IS NULL))
                    OR ((l.d_local_enderecavel_nome <> d.d_local_enderecavel_nome) or (l.d_local_enderecavel_nome IS NULL AND d.d_local_enderecavel_nome IS NOT NULL) or (l.d_local_enderecavel_nome IS NOT NULL AND d.d_local_enderecavel_nome IS NULL))
                    OR ((l.d_local_enderecavel_sigla <> d.d_local_enderecavel_sigla) or (l.d_local_enderecavel_sigla IS NULL AND d.d_local_enderecavel_sigla IS NOT NULL) or (l.d_local_enderecavel_sigla IS NOT NULL AND d.d_local_enderecavel_sigla IS NULL))
                    OR ((l.d_local_enderecavel_type <> d.d_local_enderecavel_type) or (l.d_local_enderecavel_type IS NULL AND d.d_local_enderecavel_type IS NOT NULL) or (l.d_local_enderecavel_type IS NOT NULL AND d.d_local_enderecavel_type IS NULL))
                    OR ((l.d_local_enderecavel_url_path <> d.d_local_enderecavel_url_path) or (l.d_local_enderecavel_url_path IS NULL AND d.d_local_enderecavel_url_path IS NOT NULL) or (l.d_local_enderecavel_url_path IS NOT NULL AND d.d_local_enderecavel_url_path IS NULL))
                    OR ((l.d_local_interesse_turistico_id <> d.d_local_interesse_turistico_id) or (l.d_local_interesse_turistico_id IS NULL AND d.d_local_interesse_turistico_id IS NOT NULL) or (l.d_local_interesse_turistico_id IS NOT NULL AND d.d_local_interesse_turistico_id IS NULL))
                    OR ((l.d_local_interesse_turistico_id_facebook <> d.d_local_interesse_turistico_id_facebook) or (l.d_local_interesse_turistico_id_facebook IS NULL AND d.d_local_interesse_turistico_id_facebook IS NOT NULL) or (l.d_local_interesse_turistico_id_facebook IS NOT NULL AND d.d_local_interesse_turistico_id_facebook IS NULL))
                    OR ((l.d_local_interesse_turistico_id_foto_padrao <> d.d_local_interesse_turistico_id_foto_padrao) or (l.d_local_interesse_turistico_id_foto_padrao IS NULL AND d.d_local_interesse_turistico_id_foto_padrao IS NOT NULL) or (l.d_local_interesse_turistico_id_foto_padrao IS NOT NULL AND d.d_local_interesse_turistico_id_foto_padrao IS NULL))
                    OR ((l.d_local_interesse_turistico_id_foursquare <> d.d_local_interesse_turistico_id_foursquare) or (l.d_local_interesse_turistico_id_foursquare IS NULL AND d.d_local_interesse_turistico_id_foursquare IS NOT NULL) or (l.d_local_interesse_turistico_id_foursquare IS NOT NULL AND d.d_local_interesse_turistico_id_foursquare IS NULL))
                    OR ((l.d_local_interesse_turistico_nome <> d.d_local_interesse_turistico_nome) or (l.d_local_interesse_turistico_nome IS NULL AND d.d_local_interesse_turistico_nome IS NOT NULL) or (l.d_local_interesse_turistico_nome IS NOT NULL AND d.d_local_interesse_turistico_nome IS NULL))
                    OR ((l.d_local_interesse_turistico_sigla <> d.d_local_interesse_turistico_sigla) or (l.d_local_interesse_turistico_sigla IS NULL AND d.d_local_interesse_turistico_sigla IS NOT NULL) or (l.d_local_interesse_turistico_sigla IS NOT NULL AND d.d_local_interesse_turistico_sigla IS NULL))
                    OR ((l.d_local_interesse_turistico_type <> d.d_local_interesse_turistico_type) or (l.d_local_interesse_turistico_type IS NULL AND d.d_local_interesse_turistico_type IS NOT NULL) or (l.d_local_interesse_turistico_type IS NOT NULL AND d.d_local_interesse_turistico_type IS NULL))
                    OR ((l.d_local_interesse_turistico_url_path <> d.d_local_interesse_turistico_url_path) or (l.d_local_interesse_turistico_url_path IS NULL AND d.d_local_interesse_turistico_url_path IS NOT NULL) or (l.d_local_interesse_turistico_url_path IS NOT NULL AND d.d_local_interesse_turistico_url_path IS NULL))
                    OR ((l.d_local_mais_especifico_id <> d.d_local_mais_especifico_id) or (l.d_local_mais_especifico_id IS NULL AND d.d_local_mais_especifico_id IS NOT NULL) or (l.d_local_mais_especifico_id IS NOT NULL AND d.d_local_mais_especifico_id IS NULL))
                    OR ((l.d_local_mais_especifico_id_facebook <> d.d_local_mais_especifico_id_facebook) or (l.d_local_mais_especifico_id_facebook IS NULL AND d.d_local_mais_especifico_id_facebook IS NOT NULL) or (l.d_local_mais_especifico_id_facebook IS NOT NULL AND d.d_local_mais_especifico_id_facebook IS NULL))
                    OR ((l.d_local_mais_especifico_id_foto_padrao <> d.d_local_mais_especifico_id_foto_padrao) or (l.d_local_mais_especifico_id_foto_padrao IS NULL AND d.d_local_mais_especifico_id_foto_padrao IS NOT NULL) or (l.d_local_mais_especifico_id_foto_padrao IS NOT NULL AND d.d_local_mais_especifico_id_foto_padrao IS NULL))
                    OR ((l.d_local_mais_especifico_id_foursquare <> d.d_local_mais_especifico_id_foursquare) or (l.d_local_mais_especifico_id_foursquare IS NULL AND d.d_local_mais_especifico_id_foursquare IS NOT NULL) or (l.d_local_mais_especifico_id_foursquare IS NOT NULL AND d.d_local_mais_especifico_id_foursquare IS NULL))
                    OR ((l.d_local_mais_especifico_nome <> d.d_local_mais_especifico_nome) or (l.d_local_mais_especifico_nome IS NULL AND d.d_local_mais_especifico_nome IS NOT NULL) or (l.d_local_mais_especifico_nome IS NOT NULL AND d.d_local_mais_especifico_nome IS NULL))
                    OR ((l.d_local_mais_especifico_sigla <> d.d_local_mais_especifico_sigla) or (l.d_local_mais_especifico_sigla IS NULL AND d.d_local_mais_especifico_sigla IS NOT NULL) or (l.d_local_mais_especifico_sigla IS NOT NULL AND d.d_local_mais_especifico_sigla IS NULL))
                    OR ((l.d_local_mais_especifico_type <> d.d_local_mais_especifico_type) or (l.d_local_mais_especifico_type IS NULL AND d.d_local_mais_especifico_type IS NOT NULL) or (l.d_local_mais_especifico_type IS NOT NULL AND d.d_local_mais_especifico_type IS NULL))
                    OR ((l.d_local_mais_especifico_url_path <> d.d_local_mais_especifico_url_path) or (l.d_local_mais_especifico_url_path IS NULL AND d.d_local_mais_especifico_url_path IS NOT NULL) or (l.d_local_mais_especifico_url_path IS NOT NULL AND d.d_local_mais_especifico_url_path IS NULL))
                    OR ((l.d_pais_id <> d.d_pais_id) or (l.d_pais_id IS NULL AND d.d_pais_id IS NOT NULL) or (l.d_pais_id IS NOT NULL AND d.d_pais_id IS NULL))
                    OR ((l.d_pais_id_facebook <> d.d_pais_id_facebook) or (l.d_pais_id_facebook IS NULL AND d.d_pais_id_facebook IS NOT NULL) or (l.d_pais_id_facebook IS NOT NULL AND d.d_pais_id_facebook IS NULL))
                    OR ((l.d_pais_id_foto_padrao <> d.d_pais_id_foto_padrao) or (l.d_pais_id_foto_padrao IS NULL AND d.d_pais_id_foto_padrao IS NOT NULL) or (l.d_pais_id_foto_padrao IS NOT NULL AND d.d_pais_id_foto_padrao IS NULL))
                    OR ((l.d_pais_id_foursquare <> d.d_pais_id_foursquare) or (l.d_pais_id_foursquare IS NULL AND d.d_pais_id_foursquare IS NOT NULL) or (l.d_pais_id_foursquare IS NOT NULL AND d.d_pais_id_foursquare IS NULL))
                    OR ((l.d_pais_nome <> d.d_pais_nome) or (l.d_pais_nome IS NULL AND d.d_pais_nome IS NOT NULL) or (l.d_pais_nome IS NOT NULL AND d.d_pais_nome IS NULL))
                    OR ((l.d_pais_sigla <> d.d_pais_sigla) or (l.d_pais_sigla IS NULL AND d.d_pais_sigla IS NOT NULL) or (l.d_pais_sigla IS NOT NULL AND d.d_pais_sigla IS NULL))
                    OR ((l.d_pais_type <> d.d_pais_type) or (l.d_pais_type IS NULL AND d.d_pais_type IS NOT NULL) or (l.d_pais_type IS NOT NULL AND d.d_pais_type IS NULL))
                    OR ((l.d_pais_url_path <> d.d_pais_url_path) or (l.d_pais_url_path IS NULL AND d.d_pais_url_path IS NOT NULL) or (l.d_pais_url_path IS NOT NULL AND d.d_pais_url_path IS NULL))
                             )
                         )
     ORDER BY l.id_local';

         v_update := 'UPDATE ' || p_table_name || ' 
                SET d_cidade_id =  $1
                  , d_cidade_id_facebook =  $2
                  , d_cidade_id_foto_padrao =  $3
                  , d_cidade_id_foursquare =  $4
                  , d_cidade_nome =  $5
                  , d_cidade_sigla =  $6
                  , d_cidade_type =  $7
                  , d_cidade_url_path =  $8
                  , d_continente_id =  $9
                  , d_continente_id_facebook =  $10
                  , d_continente_id_foto_padrao =  $11
                  , d_continente_id_foursquare =  $12
                  , d_continente_nome =  $13
                  , d_continente_sigla =  $14
                  , d_continente_type =  $15
                  , d_continente_url_path =  $16
                  , d_estado_id =  $17
                  , d_estado_id_facebook =  $18
                  , d_estado_id_foto_padrao =  $19
                  , d_estado_id_foursquare =  $20
                  , d_estado_nome =  $21
                  , d_estado_sigla =  $22
                  , d_estado_type =  $23
                  , d_estado_url_path =  $24
                  , d_local_enderecavel_id =  $25
                  , d_local_enderecavel_id_facebook =  $26
                  , d_local_enderecavel_id_foto_padrao =  $27
                  , d_local_enderecavel_id_foursquare =  $28
                  , d_local_enderecavel_nome =  $29
                  , d_local_enderecavel_sigla =  $30
                  , d_local_enderecavel_type =  $31
                  , d_local_enderecavel_url_path =  $32
                  , d_local_interesse_turistico_id =  $33
                  , d_local_interesse_turistico_id_facebook =  $34
                  , d_local_interesse_turistico_id_foto_padrao =  $35
                  , d_local_interesse_turistico_id_foursquare =  $36
                  , d_local_interesse_turistico_nome =  $37
                  , d_local_interesse_turistico_sigla =  $38
                  , d_local_interesse_turistico_type =  $39
                  , d_local_interesse_turistico_url_path =  $40
                  , d_local_mais_especifico_id =  $41
                  , d_local_mais_especifico_id_facebook =  $42
                  , d_local_mais_especifico_id_foto_padrao =  $43
                  , d_local_mais_especifico_id_foursquare =  $44
                  , d_local_mais_especifico_nome =  $45
                  , d_local_mais_especifico_sigla =  $46
                  , d_local_mais_especifico_type =  $47
                  , d_local_mais_especifico_url_path =  $48
                  , d_pais_id =  $49
                  , d_pais_id_facebook =  $50
                  , d_pais_id_foto_padrao =  $51
                  , d_pais_id_foursquare =  $52
                  , d_pais_nome =  $53
                  , d_pais_sigla =  $54
                  , d_pais_type =  $55
                  , d_pais_url_path =  $56
             WHERE d_local_mais_especifico_id = $57';

     FOR cursor_local IN EXECUTE v_sql 
     LOOP 
         EXECUTE v_update  
                USING cursor_local.d_cidade_id
                    , cursor_local.d_cidade_id_facebook
                    , cursor_local.d_cidade_id_foto_padrao
                    , cursor_local.d_cidade_id_foursquare
                    , cursor_local.d_cidade_nome
                    , cursor_local.d_cidade_sigla
                    , cursor_local.d_cidade_type
                    , cursor_local.d_cidade_url_path
                    , cursor_local.d_continente_id
                    , cursor_local.d_continente_id_facebook
                    , cursor_local.d_continente_id_foto_padrao
                    , cursor_local.d_continente_id_foursquare
                    , cursor_local.d_continente_nome
                    , cursor_local.d_continente_sigla
                    , cursor_local.d_continente_type
                    , cursor_local.d_continente_url_path
                    , cursor_local.d_estado_id
                    , cursor_local.d_estado_id_facebook
                    , cursor_local.d_estado_id_foto_padrao
                    , cursor_local.d_estado_id_foursquare
                    , cursor_local.d_estado_nome
                    , cursor_local.d_estado_sigla
                    , cursor_local.d_estado_type
                    , cursor_local.d_estado_url_path
                    , cursor_local.d_local_enderecavel_id
                    , cursor_local.d_local_enderecavel_id_facebook
                    , cursor_local.d_local_enderecavel_id_foto_padrao
                    , cursor_local.d_local_enderecavel_id_foursquare
                    , cursor_local.d_local_enderecavel_nome
                    , cursor_local.d_local_enderecavel_sigla
                    , cursor_local.d_local_enderecavel_type
                    , cursor_local.d_local_enderecavel_url_path
                    , cursor_local.d_local_interesse_turistico_id
                    , cursor_local.d_local_interesse_turistico_id_facebook
                    , cursor_local.d_local_interesse_turistico_id_foto_padrao
                    , cursor_local.d_local_interesse_turistico_id_foursquare
                    , cursor_local.d_local_interesse_turistico_nome
                    , cursor_local.d_local_interesse_turistico_sigla
                    , cursor_local.d_local_interesse_turistico_type
                    , cursor_local.d_local_interesse_turistico_url_path
                    , cursor_local.d_local_mais_especifico_id
                    , cursor_local.d_local_mais_especifico_id_facebook
                    , cursor_local.d_local_mais_especifico_id_foto_padrao
                    , cursor_local.d_local_mais_especifico_id_foursquare
                    , cursor_local.d_local_mais_especifico_nome
                    , cursor_local.d_local_mais_especifico_sigla
                    , cursor_local.d_local_mais_especifico_type
                    , cursor_local.d_local_mais_especifico_url_path
                    , cursor_local.d_pais_id
                    , cursor_local.d_pais_id_facebook
                    , cursor_local.d_pais_id_foto_padrao
                    , cursor_local.d_pais_id_foursquare
                    , cursor_local.d_pais_nome
                    , cursor_local.d_pais_sigla
                    , cursor_local.d_pais_type
                    , cursor_local.d_pais_url_path
                    , cursor_local.d_local_mais_especifico_id;
         v_qtd_locais_desatualizados := v_qtd_locais_desatualizados + 1; 
     END LOOP; 

    RETURN v_qtd_locais_desatualizados; 
 END; 
 $_$;


ALTER FUNCTION public.localidade_atualizar_outras_tabelas(p_table_name character varying) OWNER TO postgres;

--
-- TOC entry 262 (class 1255 OID 16390)
-- Dependencies: 784 6
-- Name: popular_desnormalizacao_nome_cidade_estado_pais_continente(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION popular_desnormalizacao_nome_cidade_estado_pais_continente() RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE
	c_tp_continente constant local.local_type%type := 1;
	c_tp_pais constant local.local_type%type := 2;
	c_tp_estado constant local.local_type%type := 3;
	c_tp_cidade constant local.local_type%type := 4;
	
    qtd_updates_executados integer;
	
    cur_local_geografico RECORD;
BEGIN
    qtd_updates_executados := 0;

    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path
                                   from local l
                                  where l.local_type = c_tp_continente) LOOP
		
		UPDATE local
		   SET nm_local_continente = cur_local_geografico.nome
		     , url_path_local_continente = cur_local_geografico.url_path
		 WHERE id_local_continente = cur_local_geografico.id_local
		   AND ((nm_local_continente is null or nm_local_continente <> cur_local_geografico.nome)
		       OR (url_path_local_continente is null or url_path_local_continente <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path
                                   from local l
                                  where l.local_type = c_tp_pais) LOOP
		
		UPDATE local
		   SET nm_local_pais = cur_local_geografico.nome
		     , url_path_local_pais = cur_local_geografico.url_path
		 WHERE id_local_pais = cur_local_geografico.id_local
		   AND ((nm_local_pais is null or nm_local_pais <> cur_local_geografico.nome)
		       OR (url_path_local_pais is null or url_path_local_pais <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path, l.sigla
                                   from local l
                                  where l.local_type = c_tp_estado) LOOP
		
		UPDATE local
		   SET nm_local_estado = cur_local_geografico.nome
		     , url_path_local_estado = cur_local_geografico.url_path
		     , sg_local_estado = cur_local_geografico.sigla
		 WHERE id_local_estado = cur_local_geografico.id_local
		   AND (   (nm_local_estado is null or nm_local_estado <> cur_local_geografico.nome)
		        OR (url_path_local_estado is null or url_path_local_estado <> cur_local_geografico.url_path)
		        OR (cur_local_geografico.sigla is not null and (sg_local_estado is null or sg_local_estado <> cur_local_geografico.sigla))
		       );
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;

    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path
                                   from local l
                                  where l.local_type = c_tp_cidade) LOOP
		
		UPDATE local
		   SET nm_local_cidade = cur_local_geografico.nome
		     , url_path_local_cidade = cur_local_geografico.url_path
		 WHERE id_local_cidade = cur_local_geografico.id_local
		   AND ((nm_local_cidade is null or nm_local_cidade <> cur_local_geografico.nome)
		       OR (url_path_local_cidade is null or url_path_local_cidade <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;

    return qtd_updates_executados;
END;
$$;


ALTER FUNCTION public.popular_desnormalizacao_nome_cidade_estado_pais_continente() OWNER TO postgres;

--
-- TOC entry 266 (class 1255 OID 18987)
-- Dependencies: 784 6
-- Name: prc_tgr_amizade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_amizade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_amigo = qtd_amigo - 1 where id_usuario = old.id_usuario;
        RETURN old;
    ELSIF (TG_OP = 'UPDATE') THEN
        IF ((new.removida_tripfans AND (NOT old.removida_tripfans)) OR (new.removida_facebook AND (NOT old.removida_facebook))) THEN
        	update usuario set qtd_amigo = qtd_amigo - 1 where id_usuario = new.id_usuario;
        	RETURN new;
        ELSIF IF ((old.removida_tripfans AND (NOT new.removida_tripfans)) OR (old.removida_facebook AND (NOT new.removida_facebook))) THEN
        	update usuario set qtd_amigo = qtd_amigo + 1 where id_usuario = new.id_usuario;
        	RETURN new;
        END IF;
    END IF;
	
    update usuario set qtd_amigo = qtd_amigo + 1 where id_usuario = new.id_usuario;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_amizade() OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 19011)
-- Dependencies: 784 6
-- Name: prc_tgr_atividade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_atividade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE' AND old.visivel) THEN 
        update usuario set qtd_atividade = qtd_atividade - 1 where id_usuario = old.id_usuario_autor;
        RETURN old;
    END IF;
	
    IF (new.visivel) THEN
    	update usuario set qtd_atividade = qtd_atividade + 1 where id_usuario = new.id_usuario_autor;
    	update usuario set data_ultima_acao = current_timestamp where id_usuario = new.id_usuario_autor;
    	RETURN new;
    END IF;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_atividade() OWNER TO postgres;

--
-- TOC entry 264 (class 1255 OID 18989)
-- Dependencies: 6 784
-- Name: prc_tgr_avaliacao(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_avaliacao() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
 v_count integer;
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_avaliacao = qtd_avaliacao - 1 where id_local = old.id_local_avaliado;
        update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_cidade = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_estado = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_pais = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_continente = l.id_local);
        update usuario set qtd_avaliacao = qtd_avaliacao - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
	
    update local set qtd_avaliacao = qtd_avaliacao + 1 where id_local = new.id_local_avaliado;
    update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_cidade = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_estado = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_pais = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_continente = l.id_local);
    update usuario set qtd_avaliacao = qtd_avaliacao + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_avaliacao() OWNER TO postgres;

--
-- TOC entry 267 (class 1255 OID 18991)
-- Dependencies: 6 784
-- Name: prc_tgr_comentario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_comentario() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_comentario = qtd_comentario - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
	
    update usuario set qtd_comentario = qtd_comentario + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_comentario() OWNER TO postgres;

--
-- TOC entry 276 (class 1255 OID 18993)
-- Dependencies: 784 6
-- Name: prc_tgr_dica_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_dica_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_dica = qtd_dica - 1 where id_local = old.local_dica;
        update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_cidade = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_estado = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_pais = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_continente = l.id_local);
		update usuario set qtd_dica = qtd_dica - 1 where id_usuario = old.autor;
        update atividade set qtd_dicas = qtd_dicas - 1 where id = old.id_atividade;
        RETURN old;
    END IF;
    update local set qtd_dica = qtd_dica + 1 where id_local = new.local_dica;
    update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_cidade = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_estado = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_pais = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_continente = l.id_local);
    update usuario set qtd_dica = qtd_dica + 1 where id_usuario = new.autor;
    update atividade set qtd_dicas = qtd_dicas + 1 where id = new.id_atividade;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_dica_quantidade() OWNER TO postgres;

--
-- TOC entry 270 (class 1255 OID 18997)
-- Dependencies: 6 784
-- Name: prc_tgr_foto_atividade_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_foto_atividade_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update atividade set qtd_fotos = qtd_fotos - 1 where id = old.id_atividade;      
        RETURN old;
    END IF;
    IF (TG_OP = 'INSERT') THEN
        update atividade set qtd_fotos = qtd_fotos + 1 where id = new.id_atividade;      
        RETURN new;
    END IF;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_foto_atividade_quantidade() OWNER TO postgres;

--
-- TOC entry 269 (class 1255 OID 18995)
-- Dependencies: 784 6
-- Name: prc_tgr_foto_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_foto_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_foto = qtd_foto - 1 where id_local = old.id_local;
        update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_cidade = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_estado = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_pais = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_continente = l.id_local and l1.local_type = 7);
        update album set quantidade_fotos = quantidade_fotos - 1 where id = old.id_album;
		update usuario set qtd_foto = qtd_foto - 1 where id_usuario = old.id_autor;
        RETURN old;
   	END IF;
    IF (TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND old.anonima = true)) THEN
	    update local set qtd_foto = qtd_foto + 1 where id_local = new.id_local;
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_cidade = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_estado = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_pais = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_continente = l.id_local and l1.local_type = 7);
	    update album set quantidade_fotos = quantidade_fotos + 1 where id = new.id_album;
	    update usuario set qtd_foto = qtd_foto + 1 where id_usuario = new.id_autor and new.anonima = false;
	    RETURN new;
    END IF;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_foto_quantidade() OWNER TO postgres;

--
-- TOC entry 275 (class 1255 OID 19009)
-- Dependencies: 6 784
-- Name: prc_tgr_interesse_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_interesse_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF (TG_OP = 'INSERT') THEN
	
		IF (new.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_desejar_ir
				OR new.interesse_escreveu_avaliacao
				OR new.interesse_escreveu_dica
				OR new.interesse_escreveu_pergunta
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_respondeu_pergunta
				OR new.interesse_seguir
				) THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse + 1 WHERE id_usuario = new.id_usuario_interessado;
		END IF;
		
		IF (new.interesse_desejar_ir
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_seguir
				) THEN
			UPDATE local SET qtd_interesse = qtd_interesse + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		RETURN new;
    ELSIF (TG_OP = 'UPDATE') THEN 
	
		IF ((NOT old.interesse_desejar_ir) AND new.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_desejar_ir AND (NOT new.interesse_desejar_ir)) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_imperdivel) AND new.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_imperdivel AND (NOT new.interesse_imperdivel)) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_indica) AND new.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_indica AND (NOT new.interesse_indica)) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_ja_foi) AND new.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_ja_foi AND (NOT new.interesse_ja_foi)) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_local_favorito) AND new.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_local_favorito AND (NOT new.interesse_local_favorito)) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_seguir) AND new.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_seguir AND (NOT new.interesse_seguir)) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;

		IF ((old.interesse_desejar_ir
				OR old.interesse_escreveu_avaliacao
				OR old.interesse_escreveu_dica
				OR old.interesse_escreveu_pergunta
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_respondeu_pergunta
				OR old.interesse_seguir
				) 
			 AND (NOT new.interesse_desejar_ir
				AND NOT new.interesse_escreveu_avaliacao
				AND NOT new.interesse_escreveu_dica
				AND NOT new.interesse_escreveu_pergunta
				AND NOT new.interesse_local_favorito
				AND NOT new.interesse_imperdivel
				AND NOT new.interesse_indica
				AND NOT new.interesse_ja_foi
				AND NOT new.interesse_respondeu_pergunta
				AND NOT new.interesse_seguir
			 ))THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse - 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF ((new.interesse_desejar_ir
				OR new.interesse_escreveu_avaliacao
				OR new.interesse_escreveu_dica
				OR new.interesse_escreveu_pergunta
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_respondeu_pergunta
				OR new.interesse_seguir
				) 
			 AND (NOT old.interesse_desejar_ir
				AND NOT old.interesse_escreveu_avaliacao
				AND NOT old.interesse_escreveu_dica
				AND NOT old.interesse_escreveu_pergunta
				AND NOT old.interesse_local_favorito
				AND NOT old.interesse_imperdivel
				AND NOT old.interesse_indica
				AND NOT old.interesse_ja_foi
				AND NOT old.interesse_respondeu_pergunta
				AND NOT old.interesse_seguir
			 ))THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse + 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF ((old.interesse_desejar_ir
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_seguir
				) 
				AND (NOT new.interesse_desejar_ir
				AND NOT new.interesse_local_favorito
				AND NOT new.interesse_imperdivel
				AND NOT new.interesse_indica
				AND NOT new.interesse_ja_foi
				AND NOT new.interesse_seguir
				)) THEN
			UPDATE local SET qtd_interesse = qtd_interesse - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;

		IF ((new.interesse_desejar_ir
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_seguir
				) 
				AND (NOT old.interesse_desejar_ir
				AND NOT old.interesse_local_favorito
				AND NOT old.interesse_imperdivel
				AND NOT old.interesse_indica
				AND NOT old.interesse_ja_foi
				AND NOT old.interesse_seguir
				)) THEN
			UPDATE local SET qtd_interesse = qtd_interesse + 1 WHERE id_local = old.id_local_de_interesse;
		END IF;

		RETURN new;
    ELSIF (TG_OP = 'DELETE') THEN 
	
		IF (old.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_desejar_ir
				OR old.interesse_escreveu_avaliacao
				OR old.interesse_escreveu_dica
				OR old.interesse_escreveu_pergunta
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_respondeu_pergunta
				OR old.interesse_seguir
				) THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse - 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF (old.interesse_desejar_ir
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_seguir
				) THEN
			UPDATE local SET qtd_interesse = qtd_interesse - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		RETURN old;
    END IF;
END;
$$;


ALTER FUNCTION public.prc_tgr_interesse_quantidade() OWNER TO postgres;

--
-- TOC entry 271 (class 1255 OID 18999)
-- Dependencies: 6 784
-- Name: prc_tgr_noticia_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_noticia_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_noticia = qtd_noticia - 1 where id_local = old.id_local_interessado;
        RETURN old;
    END IF;
    update local set qtd_noticia = qtd_noticia + 1 where id_local = new.id_local_interessado;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_noticia_quantidade() OWNER TO postgres;

--
-- TOC entry 272 (class 1255 OID 19001)
-- Dependencies: 6 784
-- Name: prc_tgr_noticia_usuario(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_noticia_usuario() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_noticia = qtd_noticia - 1 where id_usuario = old.id_usuario_interessado;
        RETURN old;
    END IF;
    update usuario set qtd_noticia = qtd_noticia + 1 where id_usuario = new.id_usuario_interessado;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_noticia_usuario() OWNER TO postgres;

--
-- TOC entry 277 (class 1255 OID 19003)
-- Dependencies: 784 6
-- Name: prc_tgr_pergunta_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_pergunta_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_pergunta = qtd_pergunta - 1 where id_local = old.id_local;
        update usuario set qtd_pergunta = qtd_pergunta - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
    update local set qtd_pergunta = qtd_pergunta + 1 where id_local = new.id_local;
    update usuario set qtd_pergunta = qtd_pergunta + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_pergunta_quantidade() OWNER TO postgres;

--
-- TOC entry 273 (class 1255 OID 19005)
-- Dependencies: 6 784
-- Name: prc_tgr_resposta_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_resposta_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_resposta = qtd_resposta - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
    update usuario set qtd_resposta = qtd_resposta + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_resposta_quantidade() OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 19007)
-- Dependencies: 6 784
-- Name: prc_tgr_viagem_quantidade(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION prc_tgr_viagem_quantidade() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_viagem = qtd_viagem - 1 where id_usuario = old.criador;
        RETURN old;
    END IF;
    update usuario set qtd_viagem = qtd_viagem + 1 where id_usuario = new.criador;
    RETURN new;
END;
$$;


ALTER FUNCTION public.prc_tgr_viagem_quantidade() OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 166 (class 1259 OID 20279)
-- Dependencies: 2173 2174 2175 6
-- Name: acao_usuario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE acao_usuario (
    id_acao_usuario bigint NOT NULL,
    tipo_acao_principal character varying(255),
    id_acao_principal bigint,
    data_acao timestamp without time zone NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    ponto numeric(19,2) NOT NULL,
    publica boolean DEFAULT false NOT NULL,
    publica_na_home boolean DEFAULT false NOT NULL,
    quantidade integer,
    id_tipo_acao integer NOT NULL,
    visivel boolean DEFAULT false NOT NULL,
    id_usuario_autor bigint NOT NULL,
    id_usuario_destinatario bigint,
    id_local_cidade bigint,
    id_local_continente bigint,
    id_local_enderecavel bigint,
    id_local_estado bigint,
    id_local_pais bigint,
    id_avaliacao bigint,
    id_comentario bigint,
    id_convite bigint,
    id_dica bigint,
    id_foto_perfil bigint,
    id_interesses_viagem bigint,
    id_pergunta bigint,
    id_preferencias_viagem bigint,
    id_resposta bigint,
    id_voto_util bigint
);


ALTER TABLE public.acao_usuario OWNER TO fpv;

--
-- TOC entry 167 (class 1259 OID 20287)
-- Dependencies: 2176 6
-- Name: album; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE album (
    id bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    descricao character varying(500),
    id_facebook character varying(255),
    id_foto_principal bigint,
    importado_facebook boolean,
    quantidade_fotos bigint DEFAULT 0,
    titulo character varying(500),
    url_externa character varying(1000),
    url_path character varying(255) NOT NULL,
    visibilidade integer,
    id_autor bigint NOT NULL
);


ALTER TABLE public.album OWNER TO fpv;

--
-- TOC entry 168 (class 1259 OID 20296)
-- Dependencies: 2177 2178 6
-- Name: amizade; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE amizade (
    id_amizade bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    email_facebook_usuario_amigo character varying(255),
    id_usuario_amigo bigint,
    id_facebook_usuario_amigo character varying(255),
    importada_facebook boolean,
    nome_usuario_amigo character varying(255),
    removida_facebook boolean DEFAULT false NOT NULL,
    removida_tripfans boolean DEFAULT false NOT NULL,
    username_facebook_usuario_amigo character varying(255),
    id_usuario bigint
);


ALTER TABLE public.amizade OWNER TO fpv;

--
-- TOC entry 169 (class 1259 OID 20306)
-- Dependencies: 2179 2180 2181 6
-- Name: atividade; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE atividade (
    atividade_type integer NOT NULL,
    id bigint NOT NULL,
    anotacao character varying(255),
    comentario character varying(255),
    confirmacao character varying(255),
    data_fim date,
    "dataFimTZ" character varying(255),
    data_inicio date,
    "dataInicioTZ" character varying(255),
    codigo_referencia_reserva character varying(255),
    data_reserva timestamp without time zone,
    email_contato character varying(255),
    nome_agencia character varying(255),
    nome_contato character varying(255),
    restricoes character varying(255),
    site_agencia character varying(255),
    telefone_agencia character varying(255),
    telefone_contato character varying(255),
    valor_pago numeric(19,2),
    dia_inicio integer,
    hora_fim timestamp without time zone,
    hora_inicio timestamp without time zone,
    id_avaliacao bigint,
    id_relato_viagem bigint,
    descricao_local character varying(255),
    endereco character varying(255),
    nome_local character varying(255),
    oculta boolean,
    ordem integer,
    qtd_dicas integer DEFAULT 0 NOT NULL,
    qtd_fotos integer DEFAULT 0 NOT NULL,
    qtd_participantes integer DEFAULT 0 NOT NULL,
    titulo character varying(255),
    url_path character varying(255) NOT NULL,
    duracao integer,
    dia_fim integer,
    fim_mesmo_local boolean,
    descricao_local_fim character varying(255),
    endereco_local_fim character varying(255),
    nome_local_fim character varying(255),
    quantidade_dias integer,
    descricao_quartos character varying(255),
    quantidade_quartos integer,
    tipo_quartos character varying(255),
    descricao character varying(255),
    quantidade_paradas integer,
    codigo_reserva_voo character varying(255),
    id_local_agencia bigint,
    id_destino_viagem bigint,
    id_local bigint,
    id_viagem bigint,
    id_local_fim bigint,
    id_grupo_empresarial bigint
);


ALTER TABLE public.atividade OWNER TO fpv;

--
-- TOC entry 170 (class 1259 OID 20317)
-- Dependencies: 2182 2183 2184 2185 2186 2187 2188 2189 2190 2191 2192 2193 2194 2195 2196 6
-- Name: avaliacao; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE avaliacao (
    id_avaliacao bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    ano_visita integer NOT NULL,
    classificacao_atendimento integer DEFAULT 0 NOT NULL,
    classificacao_beleza integer DEFAULT 0 NOT NULL,
    classificacao_comida integer DEFAULT 0 NOT NULL,
    classificacao_custo integer DEFAULT 0 NOT NULL,
    classificacao_geral integer DEFAULT 0 NOT NULL,
    classificacao_limpeza integer DEFAULT 0 NOT NULL,
    classificacao_localizacao integer DEFAULT 0 NOT NULL,
    classificacao_locomocao_transito integer DEFAULT 0 NOT NULL,
    classificacao_lojas integer DEFAULT 0 NOT NULL,
    classificacao_organicacao integer DEFAULT 0 NOT NULL,
    classificacao_praca_alimentacao integer DEFAULT 0 NOT NULL,
    classificacao_preco integer DEFAULT 0 NOT NULL,
    classificacao_seguranca integer DEFAULT 0 NOT NULL,
    classificacao_servicos_oferecidos integer DEFAULT 0 NOT NULL,
    descricao character varying(2000) NOT NULL,
    mes_visita integer NOT NULL,
    nota_geral integer,
    participa_consolidacao boolean DEFAULT false NOT NULL,
    qtd_foto bigint,
    qtd_voto_util bigint NOT NULL,
    titulo character varying(255) NOT NULL,
    url_path character varying(255) NOT NULL,
    id_atividade bigint,
    id_autor bigint NOT NULL,
    id_local_avaliado bigint
);


ALTER TABLE public.avaliacao OWNER TO fpv;

--
-- TOC entry 171 (class 1259 OID 20340)
-- Dependencies: 6
-- Name: avaliacao_bom_para; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE avaliacao_bom_para (
    id_avaliacao_bom_para bigint NOT NULL,
    id_avaliacao_item_bom_para integer NOT NULL,
    id_avaliacao bigint
);


ALTER TABLE public.avaliacao_bom_para OWNER TO fpv;

--
-- TOC entry 172 (class 1259 OID 20345)
-- Dependencies: 6
-- Name: avaliacao_classificacao; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE avaliacao_classificacao (
    id_avaliacao_classificacao bigint NOT NULL,
    id_avaliacao_criterio integer NOT NULL,
    id_avaliacao_qualidade integer NOT NULL,
    id_avaliacao bigint NOT NULL,
    id_local_avaliado bigint NOT NULL
);


ALTER TABLE public.avaliacao_classificacao OWNER TO fpv;

--
-- TOC entry 173 (class 1259 OID 20350)
-- Dependencies: 6
-- Name: avaliacao_consolidada; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE avaliacao_consolidada (
    id_avaliacao_consolidada bigint NOT NULL,
    classificacao_geral integer,
    data_consolidacao_avaliacao timestamp without time zone,
    nota_geral numeric(19,2),
    qtd_avaliacao_bom bigint,
    qtd_avaliacao_excelente bigint,
    qtd_avaliacao_pessimo bigint,
    qtd_avaliacao_regular bigint,
    qtd_avaliacao_ruim bigint,
    id_avaliacao_criterio integer NOT NULL,
    id_local_avaliado bigint NOT NULL
);


ALTER TABLE public.avaliacao_consolidada OWNER TO fpv;

--
-- TOC entry 174 (class 1259 OID 20355)
-- Dependencies: 6
-- Name: avaliacao_tipo_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE avaliacao_tipo_viagem (
    id_avaliacao_tipo_viagem bigint NOT NULL,
    id_tipo_viagem integer NOT NULL,
    id_avaliacao bigint NOT NULL
);


ALTER TABLE public.avaliacao_tipo_viagem OWNER TO fpv;

--
-- TOC entry 175 (class 1259 OID 20360)
-- Dependencies: 6
-- Name: classificacao_dica; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE classificacao_dica (
    id_classificacao_dica bigint NOT NULL,
    id_item_classificacao_dica integer NOT NULL,
    id_dica bigint NOT NULL
);


ALTER TABLE public.classificacao_dica OWNER TO fpv;

--
-- TOC entry 176 (class 1259 OID 20365)
-- Dependencies: 6
-- Name: classificacao_local; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE classificacao_local (
    id_classificacao_local bigint NOT NULL,
    id_categoria_local integer NOT NULL,
    id_classe_local integer,
    id_local bigint NOT NULL
);


ALTER TABLE public.classificacao_local OWNER TO fpv;

--
-- TOC entry 177 (class 1259 OID 20370)
-- Dependencies: 2197 6
-- Name: comentario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE comentario (
    id_comentario bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    aprovado boolean,
    bloqueado boolean DEFAULT false,
    id_pai bigint,
    motivobloqueio character varying(255),
    texto character varying(2000),
    id_tipo_comentario integer NOT NULL,
    url_path character varying(255) NOT NULL,
    visibilidade integer,
    id_album bigint,
    id_autor bigint NOT NULL,
    id_avaliacao bigint,
    id_diario_viagem bigint,
    id_dica bigint,
    id_foto bigint,
    local bigint,
    id_item_diario_viagem bigint,
    id_usuario bigint,
    id_viagem bigint
);


ALTER TABLE public.comentario OWNER TO fpv;

--
-- TOC entry 178 (class 1259 OID 20379)
-- Dependencies: 6
-- Name: configuracao_emails; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE configuracao_emails (
    id_configuracao_emails bigint NOT NULL,
    aceitar_convite_tripfans boolean,
    aceitar_convite_viagem boolean,
    aceitar_recomendacoes_lugares boolean,
    receber_comentarios_diario boolean,
    receber_comentarios_diversos boolean,
    receber_comentarios_fotos boolean,
    receber_comentarios_perfil boolean,
    receber_comentarios_plano boolean,
    receber_convites_amigos boolean,
    receber_convites_grupos boolean,
    receber_convite_viagem boolean,
    receber_indicacoes_util boolean,
    receber_noticias_locais_plano boolean,
    receber_noticias_locais_interesse boolean,
    receber_novidades boolean,
    receber_ofertas boolean,
    receber_recomendacoes_lugares boolean,
    receber_respostas boolean,
    receber_convite_dicas_lugares boolean,
    receber_convite_dicas_viagem boolean,
    id_usuario bigint
);


ALTER TABLE public.configuracao_emails OWNER TO fpv;

--
-- TOC entry 179 (class 1259 OID 20384)
-- Dependencies: 6
-- Name: conta_servico_externo; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE conta_servico_externo (
    id_conta_servico_externo bigint NOT NULL,
    access_token character varying(255),
    data_cadastro timestamp without time zone,
    data_confirmacao timestamp without time zone,
    descricao character varying(255),
    display_name character varying(255),
    expire_time bigint,
    image_url character varying(255),
    login_automatico boolean,
    profile_url character varying(255),
    rank integer,
    refresh_token character varying(255),
    secret character varying(255),
    id_tipo_servico_externo integer NOT NULL,
    url character varying(255),
    username character varying(255),
    id_provedor_servico_externo bigint,
    id_usuario bigint
);


ALTER TABLE public.conta_servico_externo OWNER TO fpv;

--
-- TOC entry 180 (class 1259 OID 20392)
-- Dependencies: 2198 2199 6
-- Name: convite; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE convite (
    id_convite bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    aceito boolean DEFAULT false NOT NULL,
    data_aceite timestamp without time zone,
    email_convidado character varying(255),
    id_request_facebook character varying(255),
    id_facebook_convidado character varying(255),
    ignorado boolean DEFAULT false NOT NULL,
    tipo_convite integer NOT NULL,
    id_convidado bigint,
    id_remetente bigint NOT NULL,
    id_viagem bigint
);


ALTER TABLE public.convite OWNER TO fpv;

--
-- TOC entry 181 (class 1259 OID 20402)
-- Dependencies: 6
-- Name: destino_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE destino_viagem (
    id bigint NOT NULL,
    ordem integer,
    id_local bigint NOT NULL,
    id_viagem bigint NOT NULL
);


ALTER TABLE public.destino_viagem OWNER TO fpv;

--
-- TOC entry 182 (class 1259 OID 20407)
-- Dependencies: 6
-- Name: detalhe_trecho; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE detalhe_trecho (
    detalhe_trecho_type integer NOT NULL,
    id bigint NOT NULL,
    alimentacao character varying(255),
    classe character varying(255),
    distancia character varying(255),
    entreterimento character varying(255),
    modelo_aeronave character varying(255),
    percentual_atraso_voos integer,
    poltrona character varying(255)
);


ALTER TABLE public.detalhe_trecho OWNER TO fpv;

--
-- TOC entry 183 (class 1259 OID 20415)
-- Dependencies: 6
-- Name: diario_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE diario_viagem (
    id bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    compartilhado_facebook boolean,
    compartilhado_twitter boolean,
    exibir_introducao boolean,
    formato_exibicao integer,
    texto_introducao character varying(255),
    tipo_visibilidade integer,
    id_acao_publicacao bigint,
    id_album bigint,
    id_foto_capa bigint,
    id_viagem bigint NOT NULL
);


ALTER TABLE public.diario_viagem OWNER TO fpv;

--
-- TOC entry 184 (class 1259 OID 20420)
-- Dependencies: 6
-- Name: dica; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE dica (
    id_dica bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    descricao character varying(2000) NOT NULL,
    id_autor_facebook character varying(255),
    id_foursquare character varying(255),
    nome_autor character varying(255),
    qtd_voto_util integer NOT NULL,
    url_path character varying(255) NOT NULL,
    id_atividade bigint,
    autor bigint,
    local_dica bigint NOT NULL
);


ALTER TABLE public.dica OWNER TO fpv;

--
-- TOC entry 161 (class 1259 OID 16472)
-- Dependencies: 6
-- Name: seq_foto; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_foto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_foto OWNER TO fpv;

--
-- TOC entry 162 (class 1259 OID 16474)
-- Dependencies: 2148 2149 6
-- Name: foto; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE foto (
    id_foto bigint DEFAULT nextval('seq_foto'::regclass) NOT NULL,
    descricao character varying(500),
    fonte_imagem character varying(255),
    tipo_armazenamento integer DEFAULT 1 NOT NULL,
    url_externa_album character varying(300),
    url_externa_small character varying(300),
    url_externa_big character varying(300),
    url_local_album character varying(300),
    url_local_small character varying(300),
    url_local_big character varying(300),
    largura_album integer,
    altura_album integer,
    largura_small integer,
    altura_small integer,
    largura_big integer,
    altura_big integer,
    url_path character varying(300),
    id_autor bigint NOT NULL,
    id_local bigint,
    anonima boolean,
    url_crop character varying(255),
    data date,
    id_facebook character varying(255),
    importada_facebook boolean,
    ordem integer,
    data_publicacao date,
    data_ultima_alteracao date,
    id_atividade bigint,
    id_avaliacao bigint,
    id_usuario bigint,
    id_viagem bigint,
    id_album bigint,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_criacao timestamp without time zone NOT NULL
);


ALTER TABLE public.foto OWNER TO fpv;

--
-- TOC entry 185 (class 1259 OID 20428)
-- Dependencies: 6
-- Name: foto_atividade; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE foto_atividade (
    id_atividade bigint NOT NULL,
    id_foto bigint NOT NULL
);


ALTER TABLE public.foto_atividade OWNER TO fpv;

--
-- TOC entry 186 (class 1259 OID 20431)
-- Dependencies: 6
-- Name: grupo_empresarial; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE grupo_empresarial (
    id bigint NOT NULL,
    ativo boolean NOT NULL,
    internacional boolean NOT NULL,
    nacional boolean NOT NULL,
    nome character varying(255),
    nome_fantasia character varying(255) NOT NULL,
    tipo_empresa integer NOT NULL,
    id_pais_origem bigint
);


ALTER TABLE public.grupo_empresarial OWNER TO fpv;

--
-- TOC entry 187 (class 1259 OID 20439)
-- Dependencies: 6
-- Name: grupo_empresarial_aeroporto; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE grupo_empresarial_aeroporto (
    id bigint NOT NULL,
    ativo boolean NOT NULL,
    codeshare boolean NOT NULL,
    id_local bigint,
    id_grupo_empresarial bigint
);


ALTER TABLE public.grupo_empresarial_aeroporto OWNER TO fpv;

--
-- TOC entry 188 (class 1259 OID 20444)
-- Dependencies: 6
-- Name: interesse_usuario_em_local; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE interesse_usuario_em_local (
    id_interesse_usuario_em_local bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    interesse_desejar_ir boolean NOT NULL,
    anterior_desejar_ir boolean,
    interesse_escreveu_avaliacao boolean NOT NULL,
    interesse_escreveu_dica boolean NOT NULL,
    interesse_escreveu_pergunta boolean NOT NULL,
    interesse_local_favorito boolean NOT NULL,
    anterior_local_favorito boolean,
    id_usuario_facebook character varying(255),
    interesse_imperdivel boolean NOT NULL,
    anterior_imperdivel boolean,
    interesse_indica boolean NOT NULL,
    anterior_indica boolean,
    interesse_ja_foi boolean NOT NULL,
    anterior_ja_foi boolean,
    nome_usuario character varying(255),
    interesse_respondeu_pergunta boolean NOT NULL,
    interesse_seguir boolean NOT NULL,
    id_local_de_interesse bigint,
    id_usuario_interessado bigint
);


ALTER TABLE public.interesse_usuario_em_local OWNER TO fpv;

--
-- TOC entry 189 (class 1259 OID 20452)
-- Dependencies: 6
-- Name: interesses_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE interesses_viagem (
    id_interesses_viagem bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    artes_cultura boolean,
    aventura boolean,
    caca_pesca boolean,
    carnaval boolean,
    compras boolean,
    cruzeiros boolean,
    descanso_relaxamento boolean,
    ecoturismo boolean,
    escolar boolean,
    esportes boolean,
    estrada boolean,
    eventos boolean,
    eventos_esportivos boolean,
    festas_tematicas boolean,
    gastronomia boolean,
    historia boolean,
    jogos_apostas boolean,
    mergulho boolean,
    micaretas boolean,
    negocios boolean,
    neve boolean,
    parques_tematicos boolean,
    praia boolean,
    religiao boolean,
    reveillon boolean,
    romantismo boolean,
    safari boolean,
    shows boolean,
    id_usuario bigint NOT NULL
);


ALTER TABLE public.interesses_viagem OWNER TO fpv;

--
-- TOC entry 190 (class 1259 OID 20457)
-- Dependencies: 6
-- Name: item_diario_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE item_diario_viagem (
    id bigint NOT NULL,
    avaliar_atividade boolean,
    data_criacao date,
    data_publicacao date,
    data_ultima_alteracao date,
    dia_fim integer,
    dia_inicio integer,
    importado boolean,
    capitulo integer,
    ocultar boolean,
    texto character varying(255),
    titulo_capitulo character varying(255),
    id_atividade bigint,
    id_avaliacao bigint,
    id_diario_viagem bigint,
    id_foto_principal bigint,
    id_local bigint,
    ordem integer
);


ALTER TABLE public.item_diario_viagem OWNER TO fpv;

--
-- TOC entry 163 (class 1259 OID 16503)
-- Dependencies: 2150 2151 2152 2153 2154 2155 2156 2157 2158 2159 2160 2161 2162 2163 2164 2165 2166 2167 2168 2169 2170 2171 2172 6
-- Name: local; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE local (
    local_type integer NOT NULL,
    id_local bigint NOT NULL,
    descricao character varying,
    nome character varying(255),
    numero_avaliacoes bigint NOT NULL,
    url_path character varying(255),
    email character varying(255),
    endereco character varying(255),
    parceiro boolean NOT NULL,
    site character varying(255),
    telefone character varying(255),
    estrelas integer,
    numero_quartos integer,
    sigla character varying(255),
    id_tipo_cozinha integer,
    numcode character varying(255),
    sigla_iso character varying(255),
    sigla_iso3 character varying(255),
    id_local_cidade bigint,
    id_local_continente bigint,
    id_local_estado bigint,
    id_local_pais bigint,
    id_local_localizacao bigint,
    cep character(10),
    latitude double precision,
    longitude double precision,
    id_grupo_empresarial bigint,
    data_consolidacao_avaliacao timestamp without time zone,
    qtd_avaliacao_bom bigint,
    qtd_avaliacao_excelente bigint,
    qtd_avaliacao_pessimo bigint,
    qtd_avaliacao_regular bigint,
    qtd_avaliacao_ruim bigint,
    classificacao_geral integer,
    nota_geral numeric(19,2),
    qtd_avaliacao bigint DEFAULT 0 NOT NULL,
    qtd_dica bigint DEFAULT 0 NOT NULL,
    qtd_foto bigint DEFAULT 0 NOT NULL,
    qtd_noticia bigint DEFAULT 0 NOT NULL,
    qtd_pergunta bigint DEFAULT 0 NOT NULL,
    qtd_aeroporto bigint DEFAULT 0 NOT NULL,
    qtd_agencia bigint DEFAULT 0 NOT NULL,
    qtd_atracao bigint DEFAULT 0 NOT NULL,
    qtd_hotel bigint DEFAULT 0 NOT NULL,
    qtd_restaurante bigint DEFAULT 0 NOT NULL,
    nm_local_continente character varying(255),
    nm_local_pais character varying(255),
    qtd_cidade bigint DEFAULT 0 NOT NULL,
    nm_local_cidade character varying(255),
    nm_local_estado character varying(255),
    qtd_estado bigint DEFAULT 0 NOT NULL,
    qtd_pais bigint DEFAULT 0 NOT NULL,
    url_path_local_cidade character varying(255),
    url_path_local_continente character varying(255),
    url_path_local_estado character varying(255),
    url_path_local_pais character varying(255),
    id_foto bigint,
    nota_ranking numeric(19,2) DEFAULT 0,
    sg_local_estado character varying(255),
    qtd_interesse bigint DEFAULT 0 NOT NULL,
    qtd_interesse_deseja_ir bigint DEFAULT 0 NOT NULL,
    qtd_interesse_imperdivel bigint DEFAULT 0 NOT NULL,
    qtd_interesse_indica bigint DEFAULT 0 NOT NULL,
    qtd_interesse_ja_foi bigint DEFAULT 0 NOT NULL,
    qtd_interesse_local_favorito bigint DEFAULT 0 NOT NULL,
    qtd_interesse_seguir bigint DEFAULT 0 NOT NULL,
    capital_estado boolean DEFAULT false NOT NULL,
    capital_pais boolean DEFAULT false NOT NULL,
    id_facebook character varying(255),
    id_foursquare character varying(255),
    beneficiada_copa_2014 boolean,
    destaque_4_rodas boolean,
    destaque_outras_4_rodas boolean,
    destino_indutor boolean,
    melhor_do_ano_4_rodas boolean,
    ranking_mais_desejadas integer,
    ranking_mais_visitadas integer,
    roteiro_4_rodas boolean,
    sede_copa_2014 boolean,
    ranking_nacional integer,
    sigla_iso_pais character varying(255),
    d_cidade_id bigint,
    d_cidade_id_facebook character varying(255),
    d_cidade_id_foursquare character varying(255),
    d_cidade_nome character varying(255),
    d_cidade_sigla character varying(255),
    d_cidade_type integer,
    d_cidade_url_path character varying(255),
    d_continente_id bigint,
    d_continente_id_facebook character varying(255),
    d_continente_id_foursquare character varying(255),
    d_continente_nome character varying(255),
    d_continente_sigla character varying(255),
    d_continente_type integer,
    d_continente_url_path character varying(255),
    d_estado_id bigint,
    d_estado_id_facebook character varying(255),
    d_estado_id_foursquare character varying(255),
    d_estado_nome character varying(255),
    d_estado_sigla character varying(255),
    d_estado_type integer,
    d_estado_url_path character varying(255),
    d_local_enderecavel_id bigint,
    d_local_enderecavel_id_facebook character varying(255),
    d_local_enderecavel_id_foursquare character varying(255),
    d_local_enderecavel_nome character varying(255),
    d_local_enderecavel_sigla character varying(255),
    d_local_enderecavel_type integer,
    d_local_enderecavel_url_path character varying(255),
    d_local_interesse_turistico_id bigint,
    d_local_interesse_turistico_id_facebook character varying(255),
    d_local_interesse_turistico_id_foursquare character varying(255),
    d_local_interesse_turistico_nome character varying(255),
    d_local_interesse_turistico_sigla character varying(255),
    d_local_interesse_turistico_type integer,
    d_local_interesse_turistico_url_path character varying(255),
    d_local_mais_especifico_id bigint,
    d_local_mais_especifico_id_facebook character varying(255),
    d_local_mais_especifico_id_foursquare character varying(255),
    d_local_mais_especifico_nome character varying(255),
    d_local_mais_especifico_sigla character varying(255),
    d_local_mais_especifico_type integer,
    d_local_mais_especifico_url_path character varying(255),
    d_pais_id bigint,
    d_pais_id_facebook character varying(255),
    d_pais_id_foursquare character varying(255),
    d_pais_nome character varying(255),
    d_pais_sigla character varying(255),
    d_pais_type integer,
    d_pais_url_path character varying(255),
    d_cidade_id_foto_padrao bigint,
    d_continente_id_foto_padrao bigint,
    d_estado_id_foto_padrao bigint,
    d_local_enderecavel_id_foto_padrao bigint,
    d_local_interesse_turistico_id_foto_padrao bigint,
    d_local_mais_especifico_id_foto_padrao bigint,
    d_pais_id_foto_padrao bigint,
    cep_4sq character varying(255),
    checkins_count_4sq integer,
    complemento_endereco_4sq character varying(255),
    dados_confirmado_4sq boolean,
    descricao_4sq character varying(255),
    distancia_local_original_4sq integer,
    email_4sq character varying(255),
    endereco_4sq character varying(255),
    facebook_4sq character varying(255),
    id_categoria_4sq character varying(255),
    latitude_4sq double precision,
    local_nao_encontrado_4sq boolean,
    longitude_4sq double precision,
    nome_4sq character varying(255),
    nome_categoria_4sq character varying(255),
    short_url_4sq character varying(255),
    sigla_categoria_4sq character varying(255),
    telefone_4sq character varying(255),
    tip_count_4sq integer,
    twitter_4sq character varying(255),
    url_4sq character varying(255),
    users_count_4sq integer,
    quantidade_locais_semelhantes_4sq integer,
    local_combinou_4sq boolean
);


ALTER TABLE public.local OWNER TO fpv;

--
-- TOC entry 191 (class 1259 OID 20465)
-- Dependencies: 6
-- Name: log_exception; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE log_exception (
    id_log_exception bigint NOT NULL,
    dt_ocorrencia timestamp without time zone NOT NULL,
    dd_ocorrencia timestamp without time zone NOT NULL,
    tx_exception_cause_message character varying(2000),
    nm_exception_cause character varying(2000),
    tx_exception_message character varying(4000),
    nm_exception character varying(2000) NOT NULL,
    tx_exception_stack_trace text NOT NULL,
    ed_request_action_url character varying(2000),
    tx_request_parameters text,
    nm_request_remote_user character varying(2000),
    ed_request_url character varying(2000),
    nm_request_user_principal character varying(2000),
    id_usuario bigint
);


ALTER TABLE public.log_exception OWNER TO fpv;

--
-- TOC entry 192 (class 1259 OID 20473)
-- Dependencies: 6
-- Name: noticia_sobre_local_para_local; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE noticia_sobre_local_para_local (
    id_noticia_sobre_local bigint NOT NULL,
    data_publicacao timestamp without time zone NOT NULL,
    local_type integer NOT NULL,
    id_avaliacao_publicada bigint,
    id_local_noticia_cidade bigint,
    id_dica_publicada bigint,
    id_local_noticia_estado bigint,
    id_local_noticia bigint NOT NULL,
    id_local_noticia_pais bigint,
    id_local_interessado bigint
);


ALTER TABLE public.noticia_sobre_local_para_local OWNER TO fpv;

--
-- TOC entry 193 (class 1259 OID 20478)
-- Dependencies: 6
-- Name: noticia_sobre_local_para_usuario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE noticia_sobre_local_para_usuario (
    id_noticia_sobre_local bigint NOT NULL,
    data_publicacao timestamp without time zone NOT NULL,
    local_type integer NOT NULL,
    id_avaliacao_publicada bigint,
    id_local_noticia_cidade bigint,
    id_dica_publicada bigint,
    id_local_noticia_estado bigint,
    id_local_noticia bigint NOT NULL,
    id_local_noticia_pais bigint,
    id_usuario_interessado bigint NOT NULL
);


ALTER TABLE public.noticia_sobre_local_para_usuario OWNER TO fpv;

--
-- TOC entry 194 (class 1259 OID 20483)
-- Dependencies: 2200 6
-- Name: notificacao; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE notificacao (
    id_notificacao bigint NOT NULL,
    tipo_acao_principal character varying(255),
    id_acao_principal bigint,
    data_notificacao timestamp without time zone NOT NULL,
    pendente boolean DEFAULT true NOT NULL,
    id_acao_usuario bigint,
    id_destinatario bigint NOT NULL,
    id_originador bigint NOT NULL
);


ALTER TABLE public.notificacao OWNER TO fpv;

--
-- TOC entry 195 (class 1259 OID 20489)
-- Dependencies: 6
-- Name: participacao_atividade; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE participacao_atividade (
    id bigint NOT NULL,
    confirmado boolean,
    observacao character varying(255),
    programa_fidelidade character varying(255),
    ticket character varying(255),
    tipo_participacao integer,
    valor double precision,
    id_atividade bigint NOT NULL,
    id_participante_viagem bigint NOT NULL
);


ALTER TABLE public.participacao_atividade OWNER TO fpv;

--
-- TOC entry 196 (class 1259 OID 20497)
-- Dependencies: 6
-- Name: participante_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE participante_viagem (
    id bigint NOT NULL,
    email character varying(255),
    nome character varying(255),
    pode_editar_viagem boolean NOT NULL,
    id_usuario bigint,
    id_viagem bigint NOT NULL
);


ALTER TABLE public.participante_viagem OWNER TO fpv;

--
-- TOC entry 197 (class 1259 OID 20505)
-- Dependencies: 6
-- Name: perfil_usuario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE perfil_usuario (
    id_perfil_usuario bigint NOT NULL,
    mensagem_personalizada character varying(255),
    mostrar_amigos_grupos integer,
    mostrar_foto integer,
    mostrar_informacoes_adicionais integer,
    mostrar_informacoes_basicas integer,
    mostrar_proximas_viagens integer,
    mostrar_sites_perfis_adicionais integer,
    mostrar_ultimas_atividades integer,
    publico boolean,
    id_usuario bigint
);


ALTER TABLE public.perfil_usuario OWNER TO fpv;

--
-- TOC entry 198 (class 1259 OID 20510)
-- Dependencies: 6
-- Name: pergunta; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE pergunta (
    id_pergunta bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    qtd_respostas bigint,
    texto_pergunta character varying(200) NOT NULL,
    url_path character varying(255) NOT NULL,
    id_autor bigint NOT NULL,
    id_local bigint NOT NULL
);


ALTER TABLE public.pergunta OWNER TO fpv;

--
-- TOC entry 199 (class 1259 OID 20515)
-- Dependencies: 6
-- Name: preferencias_usuario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE preferencias_usuario (
    id_preferencias_usuario bigint NOT NULL,
    adicionar_mapas_direcoes boolean,
    formato_data integer,
    formato_distancia integer,
    formato_hora integer,
    formato_temperatura integer,
    id_usuario bigint
);


ALTER TABLE public.preferencias_usuario OWNER TO fpv;

--
-- TOC entry 200 (class 1259 OID 20520)
-- Dependencies: 6
-- Name: preferencias_viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE preferencias_viagem (
    id_preferencias_viagem bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    amigos boolean,
    casal boolean,
    familia boolean,
    sozinho boolean,
    costuma_planejar_viagens boolean,
    conforto boolean,
    custo_beneficio boolean,
    luxo boolean,
    mochileiro boolean,
    albergue boolean,
    bed_breakfast boolean,
    camping boolean,
    casa_apartamento boolean,
    hotel boolean,
    pousada boolean,
    resort boolean,
    alta_temporada boolean,
    baixa_temporada boolean,
    feriados boolean,
    ferias boolean,
    finais_semana boolean,
    aviao boolean,
    carro boolean,
    navio boolean,
    onibus boolean,
    trem boolean,
    tipo_organizacao integer,
    id_usuario bigint NOT NULL
);


ALTER TABLE public.preferencias_viagem OWNER TO fpv;

--
-- TOC entry 201 (class 1259 OID 20525)
-- Dependencies: 6
-- Name: provedor_servico_externo; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE provedor_servico_externo (
    id_provedor_servico_externo bigint NOT NULL,
    nome character varying(255)
);


ALTER TABLE public.provedor_servico_externo OWNER TO fpv;

--
-- TOC entry 202 (class 1259 OID 20530)
-- Dependencies: 6
-- Name: resposta; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE resposta (
    id_resposta bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    qtd_voto_util bigint NOT NULL,
    texto_resposta character varying(1000) NOT NULL,
    url_path character varying(255) NOT NULL,
    id_autor bigint NOT NULL,
    id_pergunta bigint NOT NULL
);


ALTER TABLE public.resposta OWNER TO fpv;

--
-- TOC entry 207 (class 1259 OID 21224)
-- Dependencies: 6
-- Name: seq_acao_usuario; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_acao_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_acao_usuario OWNER TO fpv;

--
-- TOC entry 208 (class 1259 OID 21226)
-- Dependencies: 6
-- Name: seq_album; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_album
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_album OWNER TO fpv;

--
-- TOC entry 209 (class 1259 OID 21228)
-- Dependencies: 6
-- Name: seq_amizade; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_amizade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_amizade OWNER TO fpv;

--
-- TOC entry 210 (class 1259 OID 21230)
-- Dependencies: 6
-- Name: seq_atividade; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_atividade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_atividade OWNER TO fpv;

--
-- TOC entry 211 (class 1259 OID 21232)
-- Dependencies: 6
-- Name: seq_avaliacao; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_avaliacao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avaliacao OWNER TO fpv;

--
-- TOC entry 212 (class 1259 OID 21234)
-- Dependencies: 6
-- Name: seq_avaliacao_bom_para; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_avaliacao_bom_para
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avaliacao_bom_para OWNER TO fpv;

--
-- TOC entry 213 (class 1259 OID 21236)
-- Dependencies: 6
-- Name: seq_avaliacao_classificacao; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_avaliacao_classificacao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avaliacao_classificacao OWNER TO fpv;

--
-- TOC entry 214 (class 1259 OID 21238)
-- Dependencies: 6
-- Name: seq_avaliacao_consolidada; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_avaliacao_consolidada
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avaliacao_consolidada OWNER TO fpv;

--
-- TOC entry 215 (class 1259 OID 21240)
-- Dependencies: 6
-- Name: seq_avaliacao_tipo_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_avaliacao_tipo_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_avaliacao_tipo_viagem OWNER TO fpv;

--
-- TOC entry 216 (class 1259 OID 21242)
-- Dependencies: 6
-- Name: seq_classificacao_dica; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_classificacao_dica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_classificacao_dica OWNER TO fpv;

--
-- TOC entry 217 (class 1259 OID 21244)
-- Dependencies: 6
-- Name: seq_classificacao_local; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_classificacao_local
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_classificacao_local OWNER TO fpv;

--
-- TOC entry 218 (class 1259 OID 21246)
-- Dependencies: 6
-- Name: seq_comentario; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_comentario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_comentario OWNER TO fpv;

--
-- TOC entry 219 (class 1259 OID 21248)
-- Dependencies: 6
-- Name: seq_configuracao_emails; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_configuracao_emails
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_configuracao_emails OWNER TO fpv;

--
-- TOC entry 220 (class 1259 OID 21250)
-- Dependencies: 6
-- Name: seq_conta_servico_externo; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_conta_servico_externo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_conta_servico_externo OWNER TO fpv;

--
-- TOC entry 221 (class 1259 OID 21252)
-- Dependencies: 6
-- Name: seq_convite; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_convite
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_convite OWNER TO fpv;

--
-- TOC entry 222 (class 1259 OID 21254)
-- Dependencies: 6
-- Name: seq_destino_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_destino_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_destino_viagem OWNER TO fpv;

--
-- TOC entry 223 (class 1259 OID 21256)
-- Dependencies: 6
-- Name: seq_detalhe_trecho; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_detalhe_trecho
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_detalhe_trecho OWNER TO fpv;

--
-- TOC entry 224 (class 1259 OID 21258)
-- Dependencies: 6
-- Name: seq_diario_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_diario_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_diario_viagem OWNER TO fpv;

--
-- TOC entry 225 (class 1259 OID 21260)
-- Dependencies: 6
-- Name: seq_dica; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_dica
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_dica OWNER TO fpv;

--
-- TOC entry 226 (class 1259 OID 21262)
-- Dependencies: 6
-- Name: seq_grupo_empresarial; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_grupo_empresarial
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_grupo_empresarial OWNER TO fpv;

--
-- TOC entry 227 (class 1259 OID 21264)
-- Dependencies: 6
-- Name: seq_grupo_empresarial_aeroporto; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_grupo_empresarial_aeroporto
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_grupo_empresarial_aeroporto OWNER TO fpv;

--
-- TOC entry 228 (class 1259 OID 21266)
-- Dependencies: 6
-- Name: seq_interesse_usuario_em_local; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_interesse_usuario_em_local
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_interesse_usuario_em_local OWNER TO fpv;

--
-- TOC entry 229 (class 1259 OID 21268)
-- Dependencies: 6
-- Name: seq_interesses_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_interesses_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_interesses_viagem OWNER TO fpv;

--
-- TOC entry 230 (class 1259 OID 21270)
-- Dependencies: 6
-- Name: seq_item_diario_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_item_diario_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_item_diario_viagem OWNER TO fpv;

--
-- TOC entry 164 (class 1259 OID 16623)
-- Dependencies: 6
-- Name: seq_local; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_local
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_local OWNER TO fpv;

--
-- TOC entry 231 (class 1259 OID 21272)
-- Dependencies: 6
-- Name: seq_log_exception; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_log_exception
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_log_exception OWNER TO fpv;

--
-- TOC entry 232 (class 1259 OID 21274)
-- Dependencies: 6
-- Name: seq_noticia_sobre_local; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_noticia_sobre_local
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_noticia_sobre_local OWNER TO fpv;

--
-- TOC entry 233 (class 1259 OID 21276)
-- Dependencies: 6
-- Name: seq_notificacao; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_notificacao
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_notificacao OWNER TO fpv;

--
-- TOC entry 234 (class 1259 OID 21278)
-- Dependencies: 6
-- Name: seq_participacao_atividade; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_participacao_atividade
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_participacao_atividade OWNER TO fpv;

--
-- TOC entry 235 (class 1259 OID 21280)
-- Dependencies: 6
-- Name: seq_participante_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_participante_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_participante_viagem OWNER TO fpv;

--
-- TOC entry 236 (class 1259 OID 21282)
-- Dependencies: 6
-- Name: seq_perfil_usuario; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_perfil_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_perfil_usuario OWNER TO fpv;

--
-- TOC entry 237 (class 1259 OID 21284)
-- Dependencies: 6
-- Name: seq_pergunta; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_pergunta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_pergunta OWNER TO fpv;

--
-- TOC entry 238 (class 1259 OID 21286)
-- Dependencies: 6
-- Name: seq_preferencias_usuario; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_preferencias_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_preferencias_usuario OWNER TO fpv;

--
-- TOC entry 239 (class 1259 OID 21288)
-- Dependencies: 6
-- Name: seq_preferencias_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_preferencias_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_preferencias_viagem OWNER TO fpv;

--
-- TOC entry 240 (class 1259 OID 21290)
-- Dependencies: 6
-- Name: seq_provedor_servico_externo; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_provedor_servico_externo
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_provedor_servico_externo OWNER TO fpv;

--
-- TOC entry 241 (class 1259 OID 21292)
-- Dependencies: 6
-- Name: seq_resposta; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_resposta
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_resposta OWNER TO fpv;

--
-- TOC entry 242 (class 1259 OID 21294)
-- Dependencies: 6
-- Name: seq_trecho_tranporte; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_trecho_tranporte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_trecho_tranporte OWNER TO fpv;

--
-- TOC entry 243 (class 1259 OID 21296)
-- Dependencies: 6
-- Name: seq_usuario; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_usuario
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_usuario OWNER TO fpv;

--
-- TOC entry 244 (class 1259 OID 21298)
-- Dependencies: 6
-- Name: seq_viagem; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_viagem
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_viagem OWNER TO fpv;

--
-- TOC entry 245 (class 1259 OID 21300)
-- Dependencies: 6
-- Name: seq_voto_util; Type: SEQUENCE; Schema: public; Owner: fpv
--

CREATE SEQUENCE seq_voto_util
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_voto_util OWNER TO fpv;

--
-- TOC entry 203 (class 1259 OID 20538)
-- Dependencies: 6
-- Name: trecho_transporte; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE trecho_transporte (
    id bigint NOT NULL,
    assentos character varying(255),
    data_chegada date,
    descricao_chegada character varying(255),
    dia_chegada integer,
    hora_chegada timestamp without time zone,
    local_chegada bytea,
    descricao_local_chegada character varying(255),
    endereco_local_chegada character varying(255),
    nome_local_chegada character varying(255),
    observacao_chegada character varying(255),
    portao_chegada character varying(255),
    terminal_chegada character varying(255),
    codigo character varying(255),
    nome_companhia character varying(255),
    data_partida date,
    descricao_partida character varying(255),
    dia_partida integer,
    hora_partida timestamp without time zone,
    local_partida bytea,
    descricao_local_partida character varying(255),
    endereco_local_partida character varying(255),
    nome_local_partida character varying(255),
    observacao_partida character varying(255),
    portao_partida character varying(255),
    terminal_partida character varying(255),
    id_grupo_empresarial bigint,
    id_detalhe_trecho bigint,
    id_transporte bigint NOT NULL
);


ALTER TABLE public.trecho_transporte OWNER TO fpv;

--
-- TOC entry 165 (class 1259 OID 16667)
-- Dependencies: 6
-- Name: userconnection; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE userconnection (
    userid character varying(255) NOT NULL,
    providerid character varying(255) NOT NULL,
    provideruserid character varying(255) NOT NULL,
    rank integer NOT NULL,
    displayname character varying(255),
    profileurl character varying(512),
    imageurl character varying(512),
    accesstoken character varying(255) NOT NULL,
    secret character varying(255),
    refreshtoken character varying(255),
    expiretime bigint
);


ALTER TABLE public.userconnection OWNER TO fpv;

--
-- TOC entry 204 (class 1259 OID 20546)
-- Dependencies: 2201 2202 2203 2204 2205 2206 2207 2208 2209 2210 2211 2212 2213 2214 2215 2216 2217 2218 2219 2220 2221 2222 2223 2224 2225 2226 2227 6
-- Name: usuario; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE usuario (
    id_usuario bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    ativo boolean,
    bloqueado boolean,
    conectado_facebook boolean DEFAULT false,
    conectado_foursquare boolean DEFAULT false,
    conectado_google boolean DEFAULT false,
    conectado_twitter boolean DEFAULT false,
    mostrar_albuns_foto integer DEFAULT 2 NOT NULL,
    mostrar_diarios_viagens integer DEFAULT 2 NOT NULL,
    mostrar_dicas integer DEFAULT 1 NOT NULL,
    mostrar_fotos integer DEFAULT 2 NOT NULL,
    mostrar_informacoes_perfil integer DEFAULT 1 NOT NULL,
    mostrar_planos_viagens integer DEFAULT 2 NOT NULL,
    qtd_amigo bigint DEFAULT 0 NOT NULL,
    qtd_atividade bigint DEFAULT 0 NOT NULL,
    qtd_avaliacao bigint DEFAULT 0 NOT NULL,
    qtd_comentario bigint DEFAULT 0 NOT NULL,
    qtd_dica bigint DEFAULT 0 NOT NULL,
    qtd_foto bigint DEFAULT 0 NOT NULL,
    qtd_interesse_deseja_ir bigint DEFAULT 0 NOT NULL,
    qtd_interesse_imperdivel bigint DEFAULT 0 NOT NULL,
    qtd_interesse_indica bigint DEFAULT 0 NOT NULL,
    qtd_interesse_ja_foi bigint DEFAULT 0 NOT NULL,
    qtd_interesse_local_favorito bigint DEFAULT 0 NOT NULL,
    qtd_interesse_seguir bigint DEFAULT 0 NOT NULL,
    qtd_local_algum_interesse bigint DEFAULT 0 NOT NULL,
    qtd_noticia bigint DEFAULT 0 NOT NULL,
    qtd_pergunta bigint DEFAULT 0 NOT NULL,
    qtd_resposta bigint DEFAULT 0 NOT NULL,
    qtd_viagem bigint DEFAULT 0 NOT NULL,
    data_cadastro timestamp without time zone,
    data_complemento_cadastro timestamp without time zone,
    data_confirmacao timestamp without time zone,
    data_nascimento date,
    data_residencia date,
    data_ultima_acao timestamp without time zone,
    data_ultima_verificacao_checkins date,
    data_ultima_verificacao_notificacoes date,
    data_ultimo_login date,
    display_name character varying(255),
    email character varying(255),
    exibir_genero boolean,
    forma_cadastro integer,
    foto_externa boolean,
    frase_perfil character varying(255),
    genero character varying(255),
    id_facebook character varying(255),
    id_foursquare character varying(255),
    id_google character varying(255),
    id_twitter bigint,
    id_usuario_convidou bigint,
    importado_facebook boolean,
    importado_google boolean,
    informacoes_adicionais character varying(255),
    locale character varying(255),
    nome_meio character varying(255),
    password character varying(255),
    primeiro_nome character varying(255),
    tipo_exibicao_data_nascimento integer,
    ultimo_nome character varying(255),
    url_path character varying(255) NOT NULL,
    username character varying(255),
    id_cidade_natal bigint,
    id_cidade_residencia bigint,
    id_foto bigint
);


ALTER TABLE public.usuario OWNER TO fpv;

--
-- TOC entry 205 (class 1259 OID 20581)
-- Dependencies: 6
-- Name: viagem; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE viagem (
    id bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    compartilhada_facebook boolean,
    compartilhada_twitter boolean,
    criador_viaja boolean,
    data_fim date,
    "dataFimTZ" character varying(255),
    data_inicio date,
    "dataInicioTZ" character varying(255),
    descricao character varying(500) NOT NULL,
    finalizada boolean,
    mostrar_detalhes integer,
    nome character varying(255) NOT NULL,
    possui_diario boolean,
    quantidade_dias integer,
    situacao integer,
    tipo_periodo integer,
    visibilidade integer,
    criador bigint NOT NULL,
    id_foto_ilustrativa bigint
);


ALTER TABLE public.viagem OWNER TO fpv;

--
-- TOC entry 206 (class 1259 OID 20589)
-- Dependencies: 6
-- Name: voto_util; Type: TABLE; Schema: public; Owner: fpv
--

CREATE TABLE voto_util (
    id_voto_util bigint NOT NULL,
    data_criacao timestamp without time zone NOT NULL,
    data_exclusao timestamp without time zone,
    data_geracao_acao_alteracao timestamp without time zone,
    data_geracao_acao_inclusao timestamp without time zone,
    data_publicacao timestamp without time zone,
    data_ultima_alteracao timestamp without time zone,
    id_avaliacao bigint,
    id_dica bigint,
    id_relato_viagem bigint,
    id_resposta bigint,
    id_usuario_avaliador bigint NOT NULL
);


ALTER TABLE public.voto_util OWNER TO fpv;

--
-- TOC entry 2245 (class 2606 OID 20286)
-- Dependencies: 166 166
-- Name: acao_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT acao_usuario_pkey PRIMARY KEY (id_acao_usuario);


--
-- TOC entry 2247 (class 2606 OID 20295)
-- Dependencies: 167 167
-- Name: album_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY album
    ADD CONSTRAINT album_pkey PRIMARY KEY (id);


--
-- TOC entry 2249 (class 2606 OID 20305)
-- Dependencies: 168 168
-- Name: amizade_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY amizade
    ADD CONSTRAINT amizade_pkey PRIMARY KEY (id_amizade);


--
-- TOC entry 2251 (class 2606 OID 20316)
-- Dependencies: 169 169
-- Name: atividade_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT atividade_pkey PRIMARY KEY (id);


--
-- TOC entry 2255 (class 2606 OID 20344)
-- Dependencies: 171 171
-- Name: avaliacao_bom_para_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_bom_para
    ADD CONSTRAINT avaliacao_bom_para_pkey PRIMARY KEY (id_avaliacao_bom_para);


--
-- TOC entry 2257 (class 2606 OID 20349)
-- Dependencies: 172 172
-- Name: avaliacao_classificacao_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_classificacao
    ADD CONSTRAINT avaliacao_classificacao_pkey PRIMARY KEY (id_avaliacao_classificacao);


--
-- TOC entry 2259 (class 2606 OID 20354)
-- Dependencies: 173 173
-- Name: avaliacao_consolidada_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_consolidada
    ADD CONSTRAINT avaliacao_consolidada_pkey PRIMARY KEY (id_avaliacao_consolidada);


--
-- TOC entry 2253 (class 2606 OID 20339)
-- Dependencies: 170 170
-- Name: avaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT avaliacao_pkey PRIMARY KEY (id_avaliacao);


--
-- TOC entry 2261 (class 2606 OID 20359)
-- Dependencies: 174 174
-- Name: avaliacao_tipo_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_tipo_viagem
    ADD CONSTRAINT avaliacao_tipo_viagem_pkey PRIMARY KEY (id_avaliacao_tipo_viagem);


--
-- TOC entry 2263 (class 2606 OID 20364)
-- Dependencies: 175 175
-- Name: classificacao_dica_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY classificacao_dica
    ADD CONSTRAINT classificacao_dica_pkey PRIMARY KEY (id_classificacao_dica);


--
-- TOC entry 2265 (class 2606 OID 20369)
-- Dependencies: 176 176
-- Name: classificacao_local_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY classificacao_local
    ADD CONSTRAINT classificacao_local_pkey PRIMARY KEY (id_classificacao_local);


--
-- TOC entry 2267 (class 2606 OID 20378)
-- Dependencies: 177 177
-- Name: comentario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT comentario_pkey PRIMARY KEY (id_comentario);


--
-- TOC entry 2269 (class 2606 OID 20383)
-- Dependencies: 178 178
-- Name: configuracao_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY configuracao_emails
    ADD CONSTRAINT configuracao_emails_pkey PRIMARY KEY (id_configuracao_emails);


--
-- TOC entry 2271 (class 2606 OID 20391)
-- Dependencies: 179 179
-- Name: conta_servico_externo_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY conta_servico_externo
    ADD CONSTRAINT conta_servico_externo_pkey PRIMARY KEY (id_conta_servico_externo);


--
-- TOC entry 2273 (class 2606 OID 20401)
-- Dependencies: 180 180
-- Name: convite_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY convite
    ADD CONSTRAINT convite_pkey PRIMARY KEY (id_convite);


--
-- TOC entry 2275 (class 2606 OID 20406)
-- Dependencies: 181 181
-- Name: destino_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY destino_viagem
    ADD CONSTRAINT destino_viagem_pkey PRIMARY KEY (id);


--
-- TOC entry 2277 (class 2606 OID 20414)
-- Dependencies: 182 182
-- Name: detalhe_trecho_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY detalhe_trecho
    ADD CONSTRAINT detalhe_trecho_pkey PRIMARY KEY (id);


--
-- TOC entry 2279 (class 2606 OID 20419)
-- Dependencies: 183 183
-- Name: diario_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY diario_viagem
    ADD CONSTRAINT diario_viagem_pkey PRIMARY KEY (id);


--
-- TOC entry 2281 (class 2606 OID 20427)
-- Dependencies: 184 184
-- Name: dica_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY dica
    ADD CONSTRAINT dica_pkey PRIMARY KEY (id_dica);


--
-- TOC entry 2285 (class 2606 OID 20443)
-- Dependencies: 187 187
-- Name: grupo_empresarial_aeroporto_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY grupo_empresarial_aeroporto
    ADD CONSTRAINT grupo_empresarial_aeroporto_pkey PRIMARY KEY (id);


--
-- TOC entry 2283 (class 2606 OID 20438)
-- Dependencies: 186 186
-- Name: grupo_empresarial_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY grupo_empresarial
    ADD CONSTRAINT grupo_empresarial_pkey PRIMARY KEY (id);


--
-- TOC entry 2287 (class 2606 OID 20451)
-- Dependencies: 188 188
-- Name: interesse_usuario_em_local_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY interesse_usuario_em_local
    ADD CONSTRAINT interesse_usuario_em_local_pkey PRIMARY KEY (id_interesse_usuario_em_local);


--
-- TOC entry 2289 (class 2606 OID 20456)
-- Dependencies: 189 189
-- Name: interesses_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY interesses_viagem
    ADD CONSTRAINT interesses_viagem_pkey PRIMARY KEY (id_interesses_viagem);


--
-- TOC entry 2291 (class 2606 OID 20464)
-- Dependencies: 190 190
-- Name: item_diario_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT item_diario_viagem_pkey PRIMARY KEY (id);


--
-- TOC entry 2234 (class 2606 OID 16760)
-- Dependencies: 163 163
-- Name: local_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT local_pkey PRIMARY KEY (id_local);


--
-- TOC entry 2236 (class 2606 OID 17947)
-- Dependencies: 163 163
-- Name: local_ranking_mais_desejadas_key; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT local_ranking_mais_desejadas_key UNIQUE (ranking_mais_desejadas);


--
-- TOC entry 2238 (class 2606 OID 17949)
-- Dependencies: 163 163
-- Name: local_ranking_mais_visitadas_key; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT local_ranking_mais_visitadas_key UNIQUE (ranking_mais_visitadas);


--
-- TOC entry 2293 (class 2606 OID 20472)
-- Dependencies: 191 191
-- Name: log_exception_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY log_exception
    ADD CONSTRAINT log_exception_pkey PRIMARY KEY (id_log_exception);


--
-- TOC entry 2295 (class 2606 OID 20477)
-- Dependencies: 192 192
-- Name: noticia_sobre_local_para_local_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT noticia_sobre_local_para_local_pkey PRIMARY KEY (id_noticia_sobre_local);


--
-- TOC entry 2297 (class 2606 OID 20482)
-- Dependencies: 193 193
-- Name: noticia_sobre_local_para_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT noticia_sobre_local_para_usuario_pkey PRIMARY KEY (id_noticia_sobre_local);


--
-- TOC entry 2299 (class 2606 OID 20488)
-- Dependencies: 194 194
-- Name: notificacao_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY notificacao
    ADD CONSTRAINT notificacao_pkey PRIMARY KEY (id_notificacao);


--
-- TOC entry 2301 (class 2606 OID 20496)
-- Dependencies: 195 195
-- Name: participacao_atividade_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participacao_atividade
    ADD CONSTRAINT participacao_atividade_pkey PRIMARY KEY (id);


--
-- TOC entry 2303 (class 2606 OID 20504)
-- Dependencies: 196 196
-- Name: participante_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participante_viagem
    ADD CONSTRAINT participante_viagem_pkey PRIMARY KEY (id);


--
-- TOC entry 2305 (class 2606 OID 20509)
-- Dependencies: 197 197
-- Name: perfil_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT perfil_usuario_pkey PRIMARY KEY (id_perfil_usuario);


--
-- TOC entry 2307 (class 2606 OID 20514)
-- Dependencies: 198 198
-- Name: pergunta_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY pergunta
    ADD CONSTRAINT pergunta_pkey PRIMARY KEY (id_pergunta);


--
-- TOC entry 2229 (class 2606 OID 16788)
-- Dependencies: 162 162
-- Name: pk_foto; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT pk_foto PRIMARY KEY (id_foto);


--
-- TOC entry 2309 (class 2606 OID 20519)
-- Dependencies: 199 199
-- Name: preferencias_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY preferencias_usuario
    ADD CONSTRAINT preferencias_usuario_pkey PRIMARY KEY (id_preferencias_usuario);


--
-- TOC entry 2311 (class 2606 OID 20524)
-- Dependencies: 200 200
-- Name: preferencias_viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY preferencias_viagem
    ADD CONSTRAINT preferencias_viagem_pkey PRIMARY KEY (id_preferencias_viagem);


--
-- TOC entry 2313 (class 2606 OID 20529)
-- Dependencies: 201 201
-- Name: provedor_servico_externo_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY provedor_servico_externo
    ADD CONSTRAINT provedor_servico_externo_pkey PRIMARY KEY (id_provedor_servico_externo);


--
-- TOC entry 2315 (class 2606 OID 20537)
-- Dependencies: 202 202
-- Name: resposta_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY resposta
    ADD CONSTRAINT resposta_pkey PRIMARY KEY (id_resposta);


--
-- TOC entry 2317 (class 2606 OID 20545)
-- Dependencies: 203 203
-- Name: trecho_transporte_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY trecho_transporte
    ADD CONSTRAINT trecho_transporte_pkey PRIMARY KEY (id);


--
-- TOC entry 2240 (class 2606 OID 17874)
-- Dependencies: 165 165 165
-- Name: uk_providerid_provideruserid; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY userconnection
    ADD CONSTRAINT uk_providerid_provideruserid UNIQUE (providerid, provideruserid);


--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 2240
-- Name: CONSTRAINT uk_providerid_provideruserid ON userconnection; Type: COMMENT; Schema: public; Owner: fpv
--

COMMENT ON CONSTRAINT uk_providerid_provideruserid ON userconnection IS 'Unique para não permitir que mais de um usuário esteja associado com uma mesma conexão.';


--
-- TOC entry 2231 (class 2606 OID 16800)
-- Dependencies: 162 162
-- Name: uk_url_path; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT uk_url_path UNIQUE (url_path);


--
-- TOC entry 2242 (class 2606 OID 16802)
-- Dependencies: 165 165 165 165
-- Name: userconnection_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY userconnection
    ADD CONSTRAINT userconnection_pkey PRIMARY KEY (userid, providerid, provideruserid);


--
-- TOC entry 2319 (class 2606 OID 20580)
-- Dependencies: 204 204
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);


--
-- TOC entry 2321 (class 2606 OID 20588)
-- Dependencies: 205 205
-- Name: viagem_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY viagem
    ADD CONSTRAINT viagem_pkey PRIMARY KEY (id);


--
-- TOC entry 2323 (class 2606 OID 20593)
-- Dependencies: 206 206
-- Name: voto_util_pkey; Type: CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT voto_util_pkey PRIMARY KEY (id_voto_util);


--
-- TOC entry 2232 (class 1259 OID 16816)
-- Dependencies: 163
-- Name: fki_foto; Type: INDEX; Schema: public; Owner: fpv
--

CREATE INDEX fki_foto ON local USING btree (id_foto);


--
-- TOC entry 2243 (class 1259 OID 16818)
-- Dependencies: 165 165 165
-- Name: userconnectionrank; Type: INDEX; Schema: public; Owner: fpv
--

CREATE UNIQUE INDEX userconnectionrank ON userconnection USING btree (userid, providerid, rank);


--
-- TOC entry 2473 (class 2620 OID 21344)
-- Dependencies: 168 266
-- Name: tgr_amizade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_amizade AFTER INSERT OR DELETE OR UPDATE ON amizade FOR EACH ROW EXECUTE PROCEDURE prc_tgr_amizade();


--
-- TOC entry 2472 (class 2620 OID 21355)
-- Dependencies: 279 166
-- Name: tgr_atividade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_atividade AFTER INSERT OR DELETE ON acao_usuario FOR EACH ROW EXECUTE PROCEDURE prc_tgr_atividade();


--
-- TOC entry 2474 (class 2620 OID 21345)
-- Dependencies: 264 170
-- Name: tgr_avaliacao; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_avaliacao AFTER INSERT OR DELETE ON avaliacao FOR EACH ROW EXECUTE PROCEDURE prc_tgr_avaliacao();


--
-- TOC entry 2475 (class 2620 OID 21346)
-- Dependencies: 267 177
-- Name: tgr_comentario; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_comentario AFTER INSERT OR DELETE ON comentario FOR EACH ROW EXECUTE PROCEDURE prc_tgr_comentario();


--
-- TOC entry 2476 (class 2620 OID 21347)
-- Dependencies: 184 276
-- Name: tgr_dica_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_dica_quantidade AFTER INSERT OR DELETE ON dica FOR EACH ROW EXECUTE PROCEDURE prc_tgr_dica_quantidade();


--
-- TOC entry 2477 (class 2620 OID 21348)
-- Dependencies: 185 270
-- Name: tgr_foto_atividade_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_foto_atividade_quantidade AFTER INSERT OR DELETE OR UPDATE ON foto_atividade FOR EACH ROW EXECUTE PROCEDURE prc_tgr_foto_atividade_quantidade();


--
-- TOC entry 2471 (class 2620 OID 18996)
-- Dependencies: 269 162
-- Name: tgr_foto_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_foto_quantidade AFTER INSERT OR DELETE OR UPDATE ON foto FOR EACH ROW EXECUTE PROCEDURE prc_tgr_foto_quantidade();


--
-- TOC entry 2478 (class 2620 OID 21354)
-- Dependencies: 275 188
-- Name: tgr_interesse_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_interesse_quantidade AFTER INSERT OR DELETE OR UPDATE ON interesse_usuario_em_local FOR EACH ROW EXECUTE PROCEDURE prc_tgr_interesse_quantidade();


--
-- TOC entry 2479 (class 2620 OID 21349)
-- Dependencies: 271 192
-- Name: tgr_noticia_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_noticia_quantidade AFTER INSERT OR DELETE ON noticia_sobre_local_para_local FOR EACH ROW EXECUTE PROCEDURE prc_tgr_noticia_quantidade();


--
-- TOC entry 2480 (class 2620 OID 21350)
-- Dependencies: 272 193
-- Name: tgr_noticia_usuario; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_noticia_usuario AFTER INSERT OR DELETE ON noticia_sobre_local_para_usuario FOR EACH ROW EXECUTE PROCEDURE prc_tgr_noticia_usuario();


--
-- TOC entry 2481 (class 2620 OID 21351)
-- Dependencies: 277 198
-- Name: tgr_pergunta_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_pergunta_quantidade AFTER INSERT OR DELETE ON pergunta FOR EACH ROW EXECUTE PROCEDURE prc_tgr_pergunta_quantidade();


--
-- TOC entry 2482 (class 2620 OID 21352)
-- Dependencies: 202 273
-- Name: tgr_resposta_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_resposta_quantidade AFTER INSERT OR DELETE ON resposta FOR EACH ROW EXECUTE PROCEDURE prc_tgr_resposta_quantidade();


--
-- TOC entry 2483 (class 2620 OID 21353)
-- Dependencies: 205 278
-- Name: tgr_viagem_quantidade; Type: TRIGGER; Schema: public; Owner: fpv
--

CREATE TRIGGER tgr_viagem_quantidade AFTER INSERT OR DELETE ON viagem FOR EACH ROW EXECUTE PROCEDURE prc_tgr_viagem_quantidade();


--
-- TOC entry 2426 (class 2606 OID 20994)
-- Dependencies: 190 170 2252
-- Name: fk10f186d81e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT fk10f186d81e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2427 (class 2606 OID 20999)
-- Dependencies: 190 183 2278
-- Name: fk10f186d87f52366c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT fk10f186d87f52366c FOREIGN KEY (id_diario_viagem) REFERENCES diario_viagem(id);


--
-- TOC entry 2429 (class 2606 OID 21009)
-- Dependencies: 190 163 2233
-- Name: fk10f186d8de58fcc3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT fk10f186d8de58fcc3 FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2425 (class 2606 OID 20989)
-- Dependencies: 190 169 2250
-- Name: fk10f186d8efa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT fk10f186d8efa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2428 (class 2606 OID 21004)
-- Dependencies: 190 162 2228
-- Name: fk10f186d8f898632a; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY item_diario_viagem
    ADD CONSTRAINT fk10f186d8f898632a FOREIGN KEY (id_foto_principal) REFERENCES foto(id_foto);


--
-- TOC entry 2452 (class 2606 OID 21129)
-- Dependencies: 204 2318 198
-- Name: fk19c3c8b0bf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY pergunta
    ADD CONSTRAINT fk19c3c8b0bf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2453 (class 2606 OID 21134)
-- Dependencies: 198 163 2233
-- Name: fk19c3c8b0de58fcc3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY pergunta
    ADD CONSTRAINT fk19c3c8b0de58fcc3 FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2387 (class 2606 OID 20769)
-- Dependencies: 173 163 2233
-- Name: fk1c26beef13165237; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_consolidada
    ADD CONSTRAINT fk1c26beef13165237 FOREIGN KEY (id_local_avaliado) REFERENCES local(id_local);


--
-- TOC entry 2466 (class 2606 OID 21199)
-- Dependencies: 2252 170 206
-- Name: fk1d5c0e6d1e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT fk1d5c0e6d1e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2468 (class 2606 OID 21209)
-- Dependencies: 184 2280 206
-- Name: fk1d5c0e6d1feae7b9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT fk1d5c0e6d1feae7b9 FOREIGN KEY (id_dica) REFERENCES dica(id_dica);


--
-- TOC entry 2469 (class 2606 OID 21214)
-- Dependencies: 206 2290 190
-- Name: fk1d5c0e6d2d07974c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT fk1d5c0e6d2d07974c FOREIGN KEY (id_relato_viagem) REFERENCES item_diario_viagem(id);


--
-- TOC entry 2467 (class 2606 OID 21204)
-- Dependencies: 204 2318 206
-- Name: fk1d5c0e6d62c22919; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT fk1d5c0e6d62c22919 FOREIGN KEY (id_usuario_avaliador) REFERENCES usuario(id_usuario);


--
-- TOC entry 2470 (class 2606 OID 21219)
-- Dependencies: 206 202 2314
-- Name: fk1d5c0e6dca67ad35; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY voto_util
    ADD CONSTRAINT fk1d5c0e6dca67ad35 FOREIGN KEY (id_resposta) REFERENCES resposta(id_resposta);


--
-- TOC entry 2416 (class 2606 OID 20914)
-- Dependencies: 163 184 2233
-- Name: fk2f0ba33e79fd73; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY dica
    ADD CONSTRAINT fk2f0ba33e79fd73 FOREIGN KEY (local_dica) REFERENCES local(id_local);


--
-- TOC entry 2415 (class 2606 OID 20909)
-- Dependencies: 2318 204 184
-- Name: fk2f0ba3d7edd9a2; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY dica
    ADD CONSTRAINT fk2f0ba3d7edd9a2 FOREIGN KEY (autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2414 (class 2606 OID 20904)
-- Dependencies: 169 2250 184
-- Name: fk2f0ba3efa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY dica
    ADD CONSTRAINT fk2f0ba3efa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2327 (class 2606 OID 20934)
-- Dependencies: 2252 170 162
-- Name: fk300d041e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d041e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2328 (class 2606 OID 20939)
-- Dependencies: 2318 162 204
-- Name: fk300d0473df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d0473df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2330 (class 2606 OID 21318)
-- Dependencies: 162 204 2318
-- Name: fk300d04bf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d04bf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2325 (class 2606 OID 20919)
-- Dependencies: 167 162 2246
-- Name: fk300d04dd203acb; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d04dd203acb FOREIGN KEY (id_album) REFERENCES album(id);


--
-- TOC entry 2329 (class 2606 OID 20944)
-- Dependencies: 2320 162 205
-- Name: fk300d04e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d04e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2326 (class 2606 OID 20924)
-- Dependencies: 2250 162 169
-- Name: fk300d04efa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk300d04efa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2406 (class 2606 OID 20864)
-- Dependencies: 2318 180 204
-- Name: fk38b82ce6616b1fe; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY convite
    ADD CONSTRAINT fk38b82ce6616b1fe FOREIGN KEY (id_remetente) REFERENCES usuario(id_usuario);


--
-- TOC entry 2405 (class 2606 OID 20859)
-- Dependencies: 204 180 2318
-- Name: fk38b82ce6b62ce5d8; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY convite
    ADD CONSTRAINT fk38b82ce6b62ce5d8 FOREIGN KEY (id_convidado) REFERENCES usuario(id_usuario);


--
-- TOC entry 2407 (class 2606 OID 20869)
-- Dependencies: 205 180 2320
-- Name: fk38b82ce6e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY convite
    ADD CONSTRAINT fk38b82ce6e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2421 (class 2606 OID 20969)
-- Dependencies: 2282 186 187
-- Name: fk3a5a516335647182; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY grupo_empresarial_aeroporto
    ADD CONSTRAINT fk3a5a516335647182 FOREIGN KEY (id_grupo_empresarial) REFERENCES grupo_empresarial(id);


--
-- TOC entry 2420 (class 2606 OID 20964)
-- Dependencies: 187 2233 163
-- Name: fk3a5a5163dd5f3825; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY grupo_empresarial_aeroporto
    ADD CONSTRAINT fk3a5a5163dd5f3825 FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2424 (class 2606 OID 20984)
-- Dependencies: 189 204 2318
-- Name: fk4f576c2973df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY interesses_viagem
    ADD CONSTRAINT fk4f576c2973df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2448 (class 2606 OID 21109)
-- Dependencies: 196 195 2302
-- Name: fk523bae8c647248ec; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participacao_atividade
    ADD CONSTRAINT fk523bae8c647248ec FOREIGN KEY (id_participante_viagem) REFERENCES participante_viagem(id);


--
-- TOC entry 2447 (class 2606 OID 21104)
-- Dependencies: 195 169 2250
-- Name: fk523bae8cefa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participacao_atividade
    ADD CONSTRAINT fk523bae8cefa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2404 (class 2606 OID 20854)
-- Dependencies: 179 204 2318
-- Name: fk540b427f73df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY conta_servico_externo
    ADD CONSTRAINT fk540b427f73df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2403 (class 2606 OID 20849)
-- Dependencies: 179 201 2312
-- Name: fk540b427fb14aa509; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY conta_servico_externo
    ADD CONSTRAINT fk540b427fb14aa509 FOREIGN KEY (id_provedor_servico_externo) REFERENCES provedor_servico_externo(id_provedor_servico_externo);


--
-- TOC entry 2369 (class 2606 OID 20679)
-- Dependencies: 167 2318 204
-- Name: fk5897e6fbf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY album
    ADD CONSTRAINT fk5897e6fbf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2370 (class 2606 OID 20684)
-- Dependencies: 167 162 2228
-- Name: fk5897e6ff898632a; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY album
    ADD CONSTRAINT fk5897e6ff898632a FOREIGN KEY (id_foto_principal) REFERENCES foto(id_foto);


--
-- TOC entry 2331 (class 2606 OID 16975)
-- Dependencies: 2233 163 163
-- Name: fk625df6b21274f7d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b21274f7d FOREIGN KEY (id_local_pais) REFERENCES local(id_local);


--
-- TOC entry 2349 (class 2606 OID 19813)
-- Dependencies: 2233 163 163
-- Name: fk625df6b2299a62c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b2299a62c FOREIGN KEY (d_local_interesse_turistico_id) REFERENCES local(id_local);


--
-- TOC entry 2342 (class 2606 OID 19778)
-- Dependencies: 163 2233 163
-- Name: fk625df6b3098b12e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b3098b12e FOREIGN KEY (d_cidade_id) REFERENCES local(id_local);


--
-- TOC entry 2337 (class 2606 OID 21014)
-- Dependencies: 163 186 2282
-- Name: fk625df6b35647182; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b35647182 FOREIGN KEY (id_grupo_empresarial) REFERENCES grupo_empresarial(id);


--
-- TOC entry 2332 (class 2606 OID 16985)
-- Dependencies: 2233 163 163
-- Name: fk625df6b490fe24f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b490fe24f FOREIGN KEY (id_local_cidade) REFERENCES local(id_local);


--
-- TOC entry 2341 (class 2606 OID 19773)
-- Dependencies: 163 163 2233
-- Name: fk625df6b4fbeece; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b4fbeece FOREIGN KEY (d_pais_id) REFERENCES local(id_local);


--
-- TOC entry 2333 (class 2606 OID 16990)
-- Dependencies: 2233 163 163
-- Name: fk625df6b510ba6d3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b510ba6d3 FOREIGN KEY (id_local_estado) REFERENCES local(id_local);


--
-- TOC entry 2350 (class 2606 OID 19818)
-- Dependencies: 163 2233 163
-- Name: fk625df6b57df400e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b57df400e FOREIGN KEY (d_continente_id) REFERENCES local(id_local);


--
-- TOC entry 2344 (class 2606 OID 19788)
-- Dependencies: 162 2228 163
-- Name: fk625df6b58733ccc; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b58733ccc FOREIGN KEY (d_local_interesse_turistico_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2334 (class 2606 OID 16995)
-- Dependencies: 2233 163 163
-- Name: fk625df6b5a2cb030; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b5a2cb030 FOREIGN KEY (id_local_localizacao) REFERENCES local(id_local);


--
-- TOC entry 2345 (class 2606 OID 19793)
-- Dependencies: 2228 163 162
-- Name: fk625df6b5f71bf80; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b5f71bf80 FOREIGN KEY (d_pais_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2347 (class 2606 OID 19803)
-- Dependencies: 2228 163 162
-- Name: fk625df6b88c04c55; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b88c04c55 FOREIGN KEY (d_estado_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2338 (class 2606 OID 19758)
-- Dependencies: 163 2228 162
-- Name: fk625df6b9595f56c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b9595f56c FOREIGN KEY (d_local_mais_especifico_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2335 (class 2606 OID 17000)
-- Dependencies: 163 163 2233
-- Name: fk625df6b9c0eeae9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6b9c0eeae9 FOREIGN KEY (id_local_continente) REFERENCES local(id_local);


--
-- TOC entry 2340 (class 2606 OID 19768)
-- Dependencies: 162 163 2228
-- Name: fk625df6bb53407a3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6bb53407a3 FOREIGN KEY (d_local_enderecavel_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2343 (class 2606 OID 19783)
-- Dependencies: 162 163 2228
-- Name: fk625df6bb8e33ad7; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6bb8e33ad7 FOREIGN KEY (d_cidade_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2351 (class 2606 OID 19823)
-- Dependencies: 163 2233 163
-- Name: fk625df6bba5371ee; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6bba5371ee FOREIGN KEY (d_estado_id) REFERENCES local(id_local);


--
-- TOC entry 2346 (class 2606 OID 19798)
-- Dependencies: 163 162 2228
-- Name: fk625df6bc3e652aa; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6bc3e652aa FOREIGN KEY (d_continente_id_foto_padrao) REFERENCES foto(id_foto);


--
-- TOC entry 2339 (class 2606 OID 19763)
-- Dependencies: 2233 163 163
-- Name: fk625df6bd209458c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6bd209458c FOREIGN KEY (d_local_mais_especifico_id) REFERENCES local(id_local);


--
-- TOC entry 2348 (class 2606 OID 19808)
-- Dependencies: 163 163 2233
-- Name: fk625df6be78afbad; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk625df6be78afbad FOREIGN KEY (d_local_enderecavel_id) REFERENCES local(id_local);


--
-- TOC entry 2422 (class 2606 OID 20974)
-- Dependencies: 163 188 2233
-- Name: fk7c694a8868bf6b92; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY interesse_usuario_em_local
    ADD CONSTRAINT fk7c694a8868bf6b92 FOREIGN KEY (id_local_de_interesse) REFERENCES local(id_local);


--
-- TOC entry 2423 (class 2606 OID 20979)
-- Dependencies: 204 2318 188
-- Name: fk7c694a88e55f112d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY interesse_usuario_em_local
    ADD CONSTRAINT fk7c694a88e55f112d FOREIGN KEY (id_usuario_interessado) REFERENCES usuario(id_usuario);


--
-- TOC entry 2445 (class 2606 OID 21094)
-- Dependencies: 194 204 2318
-- Name: fk7d03b31a189f6774; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY notificacao
    ADD CONSTRAINT fk7d03b31a189f6774 FOREIGN KEY (id_destinatario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2444 (class 2606 OID 21089)
-- Dependencies: 2244 194 166
-- Name: fk7d03b31a6b9068f2; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY notificacao
    ADD CONSTRAINT fk7d03b31a6b9068f2 FOREIGN KEY (id_acao_usuario) REFERENCES acao_usuario(id_acao_usuario);


--
-- TOC entry 2446 (class 2606 OID 21099)
-- Dependencies: 194 204 2318
-- Name: fk7d03b31a7817e40f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY notificacao
    ADD CONSTRAINT fk7d03b31a7817e40f FOREIGN KEY (id_originador) REFERENCES usuario(id_usuario);


--
-- TOC entry 2386 (class 2606 OID 20764)
-- Dependencies: 172 163 2233
-- Name: fk8a1f5f9913165237; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_classificacao
    ADD CONSTRAINT fk8a1f5f9913165237 FOREIGN KEY (id_local_avaliado) REFERENCES local(id_local);


--
-- TOC entry 2385 (class 2606 OID 20759)
-- Dependencies: 170 172 2252
-- Name: fk8a1f5f991e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_classificacao
    ADD CONSTRAINT fk8a1f5f991e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2388 (class 2606 OID 20774)
-- Dependencies: 174 170 2252
-- Name: fk914d9d461e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_tipo_viagem
    ADD CONSTRAINT fk914d9d461e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2419 (class 2606 OID 20959)
-- Dependencies: 163 186 2233
-- Name: fk99ae51d5faf29613; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY grupo_empresarial
    ADD CONSTRAINT fk99ae51d5faf29613 FOREIGN KEY (id_pais_origem) REFERENCES local(id_local);


--
-- TOC entry 2393 (class 2606 OID 20799)
-- Dependencies: 170 177 2252
-- Name: fk9e0de7e11e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e11e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2396 (class 2606 OID 20814)
-- Dependencies: 177 184 2280
-- Name: fk9e0de7e11feae7b9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e11feae7b9 FOREIGN KEY (id_dica) REFERENCES dica(id_dica);


--
-- TOC entry 2397 (class 2606 OID 20819)
-- Dependencies: 177 162 2228
-- Name: fk9e0de7e11fecea7b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e11fecea7b FOREIGN KEY (id_foto) REFERENCES foto(id_foto);


--
-- TOC entry 2394 (class 2606 OID 20804)
-- Dependencies: 177 177 2266
-- Name: fk9e0de7e15b05a3e4; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e15b05a3e4 FOREIGN KEY (id_pai) REFERENCES comentario(id_comentario);


--
-- TOC entry 2400 (class 2606 OID 20834)
-- Dependencies: 177 204 2318
-- Name: fk9e0de7e173df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e173df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2399 (class 2606 OID 20829)
-- Dependencies: 177 190 2290
-- Name: fk9e0de7e17c2c330f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e17c2c330f FOREIGN KEY (id_item_diario_viagem) REFERENCES item_diario_viagem(id);


--
-- TOC entry 2395 (class 2606 OID 20809)
-- Dependencies: 177 183 2278
-- Name: fk9e0de7e17f52366c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e17f52366c FOREIGN KEY (id_diario_viagem) REFERENCES diario_viagem(id);


--
-- TOC entry 2392 (class 2606 OID 20794)
-- Dependencies: 2318 204 177
-- Name: fk9e0de7e1bf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e1bf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2391 (class 2606 OID 20789)
-- Dependencies: 177 167 2246
-- Name: fk9e0de7e1dd203acb; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e1dd203acb FOREIGN KEY (id_album) REFERENCES album(id);


--
-- TOC entry 2401 (class 2606 OID 20839)
-- Dependencies: 177 205 2320
-- Name: fk9e0de7e1e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e1e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2398 (class 2606 OID 20824)
-- Dependencies: 177 163 2233
-- Name: fk9e0de7e1f6f20ce7; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY comentario
    ADD CONSTRAINT fk9e0de7e1f6f20ce7 FOREIGN KEY (local) REFERENCES local(id_local);


--
-- TOC entry 2336 (class 2606 OID 17115)
-- Dependencies: 162 2228 163
-- Name: fk_foto; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY local
    ADD CONSTRAINT fk_foto FOREIGN KEY (id_foto) REFERENCES foto(id_foto);


--
-- TOC entry 2324 (class 2606 OID 17120)
-- Dependencies: 162 163 2233
-- Name: fk_local; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto
    ADD CONSTRAINT fk_local FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2408 (class 2606 OID 20874)
-- Dependencies: 2233 163 181
-- Name: fka3ad4a986969a7a; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY destino_viagem
    ADD CONSTRAINT fka3ad4a986969a7a FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2409 (class 2606 OID 20879)
-- Dependencies: 205 181 2320
-- Name: fka3ad4a98e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY destino_viagem
    ADD CONSTRAINT fka3ad4a98e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2437 (class 2606 OID 21054)
-- Dependencies: 193 2318 204
-- Name: fka9351be3e55f112d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fka9351be3e55f112d FOREIGN KEY (id_usuario_interessado) REFERENCES usuario(id_usuario);


--
-- TOC entry 2455 (class 2606 OID 21144)
-- Dependencies: 200 2318 204
-- Name: fkb666a72f73df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY preferencias_viagem
    ADD CONSTRAINT fkb666a72f73df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2374 (class 2606 OID 20704)
-- Dependencies: 2252 170 169
-- Name: fkc1b71d071e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d071e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2377 (class 2606 OID 20719)
-- Dependencies: 169 190 2290
-- Name: fkc1b71d072d07974c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d072d07974c FOREIGN KEY (id_relato_viagem) REFERENCES item_diario_viagem(id);


--
-- TOC entry 2380 (class 2606 OID 20734)
-- Dependencies: 169 186 2282
-- Name: fkc1b71d0735647182; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d0735647182 FOREIGN KEY (id_grupo_empresarial) REFERENCES grupo_empresarial(id);


--
-- TOC entry 2375 (class 2606 OID 20709)
-- Dependencies: 181 169 2274
-- Name: fkc1b71d07896907f6; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d07896907f6 FOREIGN KEY (id_destino_viagem) REFERENCES destino_viagem(id);


--
-- TOC entry 2379 (class 2606 OID 20729)
-- Dependencies: 163 169 2233
-- Name: fkc1b71d07a16ebc8e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d07a16ebc8e FOREIGN KEY (id_local_fim) REFERENCES local(id_local);


--
-- TOC entry 2376 (class 2606 OID 20714)
-- Dependencies: 169 163 2233
-- Name: fkc1b71d07de58fcc3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d07de58fcc3 FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2378 (class 2606 OID 20724)
-- Dependencies: 205 169 2320
-- Name: fkc1b71d07e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d07e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2373 (class 2606 OID 20699)
-- Dependencies: 163 169 2233
-- Name: fkc1b71d07fea812f1; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY atividade
    ADD CONSTRAINT fkc1b71d07fea812f1 FOREIGN KEY (id_local_agencia) REFERENCES local(id_local);


--
-- TOC entry 2430 (class 2606 OID 21019)
-- Dependencies: 192 163 2233
-- Name: fkc732b160557cbd67; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkc732b160557cbd67 FOREIGN KEY (id_local_interessado) REFERENCES local(id_local);


--
-- TOC entry 2372 (class 2606 OID 20694)
-- Dependencies: 168 204 2318
-- Name: fkcb3d412573df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY amizade
    ADD CONSTRAINT fkcb3d412573df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2371 (class 2606 OID 20689)
-- Dependencies: 2318 168 204
-- Name: fkcb3d41258e8f1bef; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY amizade
    ADD CONSTRAINT fkcb3d41258e8f1bef FOREIGN KEY (id_usuario_amigo) REFERENCES usuario(id_usuario);


--
-- TOC entry 2465 (class 2606 OID 21194)
-- Dependencies: 205 2228 162
-- Name: fkcf5124819373869e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY viagem
    ADD CONSTRAINT fkcf5124819373869e FOREIGN KEY (id_foto_ilustrativa) REFERENCES foto(id_foto);


--
-- TOC entry 2464 (class 2606 OID 21189)
-- Dependencies: 205 2318 204
-- Name: fkcf512481fe6a31f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY viagem
    ADD CONSTRAINT fkcf512481fe6a31f FOREIGN KEY (criador) REFERENCES usuario(id_usuario);


--
-- TOC entry 2412 (class 2606 OID 20894)
-- Dependencies: 2228 162 183
-- Name: fkd1dc97048eef83d9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY diario_viagem
    ADD CONSTRAINT fkd1dc97048eef83d9 FOREIGN KEY (id_foto_capa) REFERENCES foto(id_foto);


--
-- TOC entry 2410 (class 2606 OID 20884)
-- Dependencies: 183 2244 166
-- Name: fkd1dc97049ab8bcf3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY diario_viagem
    ADD CONSTRAINT fkd1dc97049ab8bcf3 FOREIGN KEY (id_acao_publicacao) REFERENCES acao_usuario(id_acao_usuario);


--
-- TOC entry 2411 (class 2606 OID 20889)
-- Dependencies: 183 2246 167
-- Name: fkd1dc9704dd203acb; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY diario_viagem
    ADD CONSTRAINT fkd1dc9704dd203acb FOREIGN KEY (id_album) REFERENCES album(id);


--
-- TOC entry 2413 (class 2606 OID 20899)
-- Dependencies: 205 183 2320
-- Name: fkd1dc9704e3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY diario_viagem
    ADD CONSTRAINT fkd1dc9704e3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2384 (class 2606 OID 20754)
-- Dependencies: 171 170 2252
-- Name: fkd71433e51e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao_bom_para
    ADD CONSTRAINT fkd71433e51e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2442 (class 2606 OID 21079)
-- Dependencies: 193 2233 163
-- Name: fkd868777128fb189a9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd868777128fb189a9351be3 FOREIGN KEY (id_local_noticia) REFERENCES local(id_local);


--
-- TOC entry 2435 (class 2606 OID 21044)
-- Dependencies: 192 2233 163
-- Name: fkd868777128fb189c732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd868777128fb189c732b160 FOREIGN KEY (id_local_noticia) REFERENCES local(id_local);


--
-- TOC entry 2443 (class 2606 OID 21084)
-- Dependencies: 2233 193 163
-- Name: fkd86877798e0a677a9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd86877798e0a677a9351be3 FOREIGN KEY (id_local_noticia_pais) REFERENCES local(id_local);


--
-- TOC entry 2436 (class 2606 OID 21049)
-- Dependencies: 2233 163 192
-- Name: fkd86877798e0a677c732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd86877798e0a677c732b160 FOREIGN KEY (id_local_noticia_pais) REFERENCES local(id_local);


--
-- TOC entry 2439 (class 2606 OID 21064)
-- Dependencies: 193 2233 163
-- Name: fkd868777b7cf62c9a9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd868777b7cf62c9a9351be3 FOREIGN KEY (id_local_noticia_cidade) REFERENCES local(id_local);


--
-- TOC entry 2432 (class 2606 OID 21029)
-- Dependencies: 192 163 2233
-- Name: fkd868777b7cf62c9c732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd868777b7cf62c9c732b160 FOREIGN KEY (id_local_noticia_cidade) REFERENCES local(id_local);


--
-- TOC entry 2440 (class 2606 OID 21069)
-- Dependencies: 2280 184 193
-- Name: fkd868777b8bbb70fa9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd868777b8bbb70fa9351be3 FOREIGN KEY (id_dica_publicada) REFERENCES dica(id_dica);


--
-- TOC entry 2433 (class 2606 OID 21034)
-- Dependencies: 192 184 2280
-- Name: fkd868777b8bbb70fc732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd868777b8bbb70fc732b160 FOREIGN KEY (id_dica_publicada) REFERENCES dica(id_dica);


--
-- TOC entry 2441 (class 2606 OID 21074)
-- Dependencies: 193 2233 163
-- Name: fkd868777bfcb274da9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd868777bfcb274da9351be3 FOREIGN KEY (id_local_noticia_estado) REFERENCES local(id_local);


--
-- TOC entry 2434 (class 2606 OID 21039)
-- Dependencies: 2233 192 163
-- Name: fkd868777bfcb274dc732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd868777bfcb274dc732b160 FOREIGN KEY (id_local_noticia_estado) REFERENCES local(id_local);


--
-- TOC entry 2438 (class 2606 OID 21059)
-- Dependencies: 2252 193 170
-- Name: fkd868777c0fc8e75a9351be3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_usuario
    ADD CONSTRAINT fkd868777c0fc8e75a9351be3 FOREIGN KEY (id_avaliacao_publicada) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2431 (class 2606 OID 21024)
-- Dependencies: 192 170 2252
-- Name: fkd868777c0fc8e75c732b160; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY noticia_sobre_local_para_local
    ADD CONSTRAINT fkd868777c0fc8e75c732b160 FOREIGN KEY (id_avaliacao_publicada) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2383 (class 2606 OID 20749)
-- Dependencies: 170 163 2233
-- Name: fkd935d09913165237; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT fkd935d09913165237 FOREIGN KEY (id_local_avaliado) REFERENCES local(id_local);


--
-- TOC entry 2382 (class 2606 OID 20744)
-- Dependencies: 170 204 2318
-- Name: fkd935d099bf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT fkd935d099bf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2381 (class 2606 OID 20739)
-- Dependencies: 170 169 2250
-- Name: fkd935d099efa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY avaliacao
    ADD CONSTRAINT fkd935d099efa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2359 (class 2606 OID 20629)
-- Dependencies: 2252 166 170
-- Name: fkdce561bf1e9e209f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf1e9e209f FOREIGN KEY (id_avaliacao) REFERENCES avaliacao(id_avaliacao);


--
-- TOC entry 2362 (class 2606 OID 20644)
-- Dependencies: 184 2280 166
-- Name: fkdce561bf1feae7b9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf1feae7b9 FOREIGN KEY (id_dica) REFERENCES dica(id_dica);


--
-- TOC entry 2358 (class 2606 OID 20624)
-- Dependencies: 166 2233 163
-- Name: fkdce561bf21274f7d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf21274f7d FOREIGN KEY (id_local_pais) REFERENCES local(id_local);


--
-- TOC entry 2368 (class 2606 OID 20674)
-- Dependencies: 206 166 2322
-- Name: fkdce561bf22bb79ae; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf22bb79ae FOREIGN KEY (id_voto_util) REFERENCES voto_util(id_voto_util);


--
-- TOC entry 2365 (class 2606 OID 20659)
-- Dependencies: 2306 166 198
-- Name: fkdce561bf2680f053; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf2680f053 FOREIGN KEY (id_pergunta) REFERENCES pergunta(id_pergunta);


--
-- TOC entry 2363 (class 2606 OID 20649)
-- Dependencies: 162 2228 166
-- Name: fkdce561bf46714116; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf46714116 FOREIGN KEY (id_foto_perfil) REFERENCES foto(id_foto);


--
-- TOC entry 2354 (class 2606 OID 20604)
-- Dependencies: 2233 163 166
-- Name: fkdce561bf490fe24f; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf490fe24f FOREIGN KEY (id_local_cidade) REFERENCES local(id_local);


--
-- TOC entry 2357 (class 2606 OID 20619)
-- Dependencies: 166 2233 163
-- Name: fkdce561bf510ba6d3; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf510ba6d3 FOREIGN KEY (id_local_estado) REFERENCES local(id_local);


--
-- TOC entry 2360 (class 2606 OID 20634)
-- Dependencies: 166 2266 177
-- Name: fkdce561bf56393df5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf56393df5 FOREIGN KEY (id_comentario) REFERENCES comentario(id_comentario);


--
-- TOC entry 2366 (class 2606 OID 20664)
-- Dependencies: 200 2310 166
-- Name: fkdce561bf78e398cc; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf78e398cc FOREIGN KEY (id_preferencias_viagem) REFERENCES preferencias_viagem(id_preferencias_viagem);


--
-- TOC entry 2352 (class 2606 OID 20594)
-- Dependencies: 2318 204 166
-- Name: fkdce561bf8e92e92d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf8e92e92d FOREIGN KEY (id_usuario_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2353 (class 2606 OID 20599)
-- Dependencies: 204 2318 166
-- Name: fkdce561bf8efe14e5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf8efe14e5 FOREIGN KEY (id_usuario_destinatario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2355 (class 2606 OID 20609)
-- Dependencies: 166 2233 163
-- Name: fkdce561bf9c0eeae9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bf9c0eeae9 FOREIGN KEY (id_local_continente) REFERENCES local(id_local);


--
-- TOC entry 2364 (class 2606 OID 20654)
-- Dependencies: 166 2288 189
-- Name: fkdce561bfbc8a304c; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bfbc8a304c FOREIGN KEY (id_interesses_viagem) REFERENCES interesses_viagem(id_interesses_viagem);


--
-- TOC entry 2356 (class 2606 OID 20614)
-- Dependencies: 163 2233 166
-- Name: fkdce561bfbcb9aa86; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bfbcb9aa86 FOREIGN KEY (id_local_enderecavel) REFERENCES local(id_local);


--
-- TOC entry 2367 (class 2606 OID 20669)
-- Dependencies: 166 2314 202
-- Name: fkdce561bfca67ad35; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bfca67ad35 FOREIGN KEY (id_resposta) REFERENCES resposta(id_resposta);


--
-- TOC entry 2361 (class 2606 OID 20639)
-- Dependencies: 180 2272 166
-- Name: fkdce561bff525d079; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY acao_usuario
    ADD CONSTRAINT fkdce561bff525d079 FOREIGN KEY (id_convite) REFERENCES convite(id_convite);


--
-- TOC entry 2417 (class 2606 OID 20949)
-- Dependencies: 185 162 2228
-- Name: fkddceaf8c1fecea7b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto_atividade
    ADD CONSTRAINT fkddceaf8c1fecea7b FOREIGN KEY (id_foto) REFERENCES foto(id_foto);


--
-- TOC entry 2418 (class 2606 OID 20954)
-- Dependencies: 169 2250 185
-- Name: fkddceaf8cefa0b97b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY foto_atividade
    ADD CONSTRAINT fkddceaf8cefa0b97b FOREIGN KEY (id_atividade) REFERENCES atividade(id);


--
-- TOC entry 2458 (class 2606 OID 21159)
-- Dependencies: 186 203 2282
-- Name: fke1fee67835647182; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY trecho_transporte
    ADD CONSTRAINT fke1fee67835647182 FOREIGN KEY (id_grupo_empresarial) REFERENCES grupo_empresarial(id);


--
-- TOC entry 2460 (class 2606 OID 21169)
-- Dependencies: 203 2250 169
-- Name: fke1fee6784d554aeb; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY trecho_transporte
    ADD CONSTRAINT fke1fee6784d554aeb FOREIGN KEY (id_transporte) REFERENCES atividade(id);


--
-- TOC entry 2459 (class 2606 OID 21164)
-- Dependencies: 203 2276 182
-- Name: fke1fee67882be689a; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY trecho_transporte
    ADD CONSTRAINT fke1fee67882be689a FOREIGN KEY (id_detalhe_trecho) REFERENCES detalhe_trecho(id);


--
-- TOC entry 2402 (class 2606 OID 20844)
-- Dependencies: 178 204 2318
-- Name: fke70f18e773df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY configuracao_emails
    ADD CONSTRAINT fke70f18e773df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2457 (class 2606 OID 21154)
-- Dependencies: 2306 198 202
-- Name: fkebb727212680f053; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY resposta
    ADD CONSTRAINT fkebb727212680f053 FOREIGN KEY (id_pergunta) REFERENCES pergunta(id_pergunta);


--
-- TOC entry 2456 (class 2606 OID 21149)
-- Dependencies: 204 202 2318
-- Name: fkebb72721bf54c97e; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY resposta
    ADD CONSTRAINT fkebb72721bf54c97e FOREIGN KEY (id_autor) REFERENCES usuario(id_usuario);


--
-- TOC entry 2451 (class 2606 OID 21124)
-- Dependencies: 197 204 2318
-- Name: fkee9777bb73df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY perfil_usuario
    ADD CONSTRAINT fkee9777bb73df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2389 (class 2606 OID 20779)
-- Dependencies: 175 184 2280
-- Name: fkef57a7231feae7b9; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY classificacao_dica
    ADD CONSTRAINT fkef57a7231feae7b9 FOREIGN KEY (id_dica) REFERENCES dica(id_dica);


--
-- TOC entry 2454 (class 2606 OID 21139)
-- Dependencies: 199 204 2318
-- Name: fkf3afc64073df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY preferencias_usuario
    ADD CONSTRAINT fkf3afc64073df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2463 (class 2606 OID 21184)
-- Dependencies: 162 204 2228
-- Name: fkf814f32e1fecea7b; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32e1fecea7b FOREIGN KEY (id_foto) REFERENCES foto(id_foto);


--
-- TOC entry 2461 (class 2606 OID 21174)
-- Dependencies: 2233 204 163
-- Name: fkf814f32e728c4c08; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32e728c4c08 FOREIGN KEY (id_cidade_natal) REFERENCES local(id_local);


--
-- TOC entry 2462 (class 2606 OID 21179)
-- Dependencies: 204 2233 163
-- Name: fkf814f32e9233b7a1; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT fkf814f32e9233b7a1 FOREIGN KEY (id_cidade_residencia) REFERENCES local(id_local);


--
-- TOC entry 2449 (class 2606 OID 21114)
-- Dependencies: 196 2318 204
-- Name: fkfa98e1ce73df5d09; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participante_viagem
    ADD CONSTRAINT fkfa98e1ce73df5d09 FOREIGN KEY (id_usuario) REFERENCES usuario(id_usuario);


--
-- TOC entry 2450 (class 2606 OID 21119)
-- Dependencies: 2320 196 205
-- Name: fkfa98e1cee3cc8b5; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY participante_viagem
    ADD CONSTRAINT fkfa98e1cee3cc8b5 FOREIGN KEY (id_viagem) REFERENCES viagem(id);


--
-- TOC entry 2390 (class 2606 OID 20784)
-- Dependencies: 176 163 2233
-- Name: fkfc10b3ebf9a4462d; Type: FK CONSTRAINT; Schema: public; Owner: fpv
--

ALTER TABLE ONLY classificacao_local
    ADD CONSTRAINT fkfc10b3ebf9a4462d FOREIGN KEY (id_local) REFERENCES local(id_local);


--
-- TOC entry 2488 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2013-05-17 13:00:47 BRT

--
-- PostgreSQL database dump complete
--

