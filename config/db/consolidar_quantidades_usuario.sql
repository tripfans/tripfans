CREATE OR REPLACE FUNCTION consolidar_quantidades_usuario()
  RETURNS integer AS
$BODY$
DECLARE
    qtd_registros_alterados integer;
    cur_quantidade RECORD;
BEGIN
    qtd_registros_alterados := 0;

    FOR cur_quantidade IN (select u.id_usuario, count(a.id_amizade) qtd
                             from usuario u, amizade a
                            where u.id_usuario = a.id_usuario	
                            group by u.id_usuario
                           having count(a.id_amizade) <> u.qtd_amigo) LOOP
        update usuario
           set qtd_amigo = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(a.id_avaliacao) qtd
                             from usuario u, avaliacao a
                            where u.id_usuario = a.id_autor
                            group by u.id_usuario
                           having count(a.id_avaliacao) <> u.qtd_avaliacao) LOOP
        update usuario
           set qtd_avaliacao = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(c.id_comentario) qtd
                             from usuario u, comentario c
                            where u.id_usuario = c.id_autor
                            group by u.id_usuario
                           having count(c.id_comentario) <> u.qtd_comentario) LOOP
        update usuario
           set qtd_comentario = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(d.id_dica) qtd
                             from usuario u, dica d
                            where u.id_usuario = d.autor
                            group by u.id_usuario
                           having count(d.id_dica) <> u.qtd_dica) LOOP
        update usuario
           set qtd_dica = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(f.id_foto) qtd
                             from usuario u, foto f
                            where u.id_usuario = f.id_autor and f.anonima = false
                            group by u.id_usuario
                           having count(f.id_foto) <> u.qtd_foto) LOOP
        update usuario
           set qtd_foto = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(n.id_noticia_sobre_local) qtd
                             from usuario u, noticia_sobre_local_para_usuario n
                            where u.id_usuario = n.id_usuario_interessado
                            group by u.id_usuario
                           having count(n.id_noticia_sobre_local) <> u.qtd_noticia) LOOP
        update usuario
           set qtd_noticia = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(p.id_pergunta) qtd
                             from usuario u, pergunta p
                            where u.id_usuario = p.id_autor
                            group by u.id_usuario
                           having count(p.id_pergunta) <> u.qtd_pergunta) LOOP
        update usuario
           set qtd_pergunta = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(r.id_resposta) qtd
                             from usuario u, resposta r
                            where u.id_usuario = r.id_autor
                            group by u.id_usuario
                           having count(r.id_resposta) <> u.qtd_resposta) LOOP
        update usuario
           set qtd_resposta = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(v.id) qtd
                             from usuario u, viagem v
                            where u.id_usuario = v.criador
                            group by u.id_usuario
                           having count(v.id) <> u.qtd_viagem) LOOP
        update usuario
           set qtd_viagem = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_desejar_ir = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_deseja_ir) LOOP
        update usuario
           set qtd_interesse_deseja_ir = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_imperdivel = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_imperdivel) LOOP
        update usuario
           set qtd_interesse_imperdivel = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_indica = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_indica) LOOP
        update usuario
           set qtd_interesse_indica = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_ja_foi = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_ja_foi) LOOP
        update usuario
           set qtd_interesse_ja_foi = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_local_favorito = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_local_favorito) LOOP
        update usuario
           set qtd_interesse_local_favorito = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and i.interesse_seguir = true
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_interesse_seguir) LOOP
        update usuario
           set qtd_interesse_seguir = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select u.id_usuario, count(i.id_interesse_usuario_em_local) qtd
                             from usuario u, interesse_usuario_em_local i
                            where u.id_usuario = i.id_usuario_interessado
							  and (i.interesse_desejar_ir = true
									OR i.interesse_escreveu_avaliacao = true
									OR i.interesse_escreveu_dica = true
									OR i.interesse_escreveu_pergunta = true
									OR i.interesse_local_favorito = true
									OR i.interesse_imperdivel = true
									OR i.interesse_indica = true
									OR i.interesse_ja_foi = true
									OR i.interesse_respondeu_pergunta = true
									OR i.interesse_seguir = true)
                            group by u.id_usuario
                           having count(i.id_interesse_usuario_em_local) <> u.qtd_local_algum_interesse) LOOP
        update usuario
           set qtd_local_algum_interesse = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select u.id_usuario, count(au.id_acao_usuario) qtd
                             from usuario u, acao_usuario au
                            where u.id_usuario = au.id_usuario_autor
                            group by u.id_usuario
                           having count(au.id_acao_usuario) <> u.qtd_atividade) LOOP
        update usuario
           set qtd_atividade = cur_quantidade.qtd
         where id_usuario = cur_quantidade.id_usuario;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$BODY$
LANGUAGE plpgsql;
