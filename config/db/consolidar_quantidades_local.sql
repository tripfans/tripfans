CREATE OR REPLACE FUNCTION consolidar_quantidades_local()
  RETURNS integer AS
$BODY$
DECLARE
    qtd_registros_alterados integer;
    cur_quantidade RECORD;
BEGIN
	update local set qtd_avaliacao = 0, qtd_avaliacao_bom = 0, qtd_avaliacao_excelente = 0, qtd_avaliacao_pessimo = 0,
	qtd_avaliacao_regular = 0, qtd_avaliacao_ruim = 0, classificacao_geral = 0, nota_geral = 0.00,  qtd_dica = 0, qtd_foto = 0, qtd_noticia = 0, qtd_pergunta = 0,
	qtd_interesse = 0, qtd_interesse_deseja_ir = 0, qtd_interesse_imperdivel = 0, qtd_interesse_indica = 0,
	qtd_interesse_ja_foi = 0, qtd_interesse_local_favorito = 0, qtd_interesse_seguir = 0;
	
    qtd_registros_alterados := 0;

    FOR cur_quantidade IN (select l.id_local, count(a.id_avaliacao) qtd_avaliacao
                             from local l, avaliacao a
                            where l.id_local = a.id_local_avaliado	
                            group by l.id_local
                           having count(a.id_avaliacao) <> l.qtd_avaliacao) LOOP
        update local 
           set qtd_avaliacao = cur_quantidade.qtd_avaliacao
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select c.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local c, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_cidade = c.id_local)
							group by c.id_local
                       having count(a.id_avaliacao) <> c.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select e.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local e, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_estado = e.id_local)
							group by e.id_local
                       having count(a.id_avaliacao) <> e.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select p.id_local, count(a.id_avaliacao) qtd_avaliacao
							from local p, avaliacao a
							where exists (select 1 from local l where l.id_local = a.id_local_avaliado and l.id_local_pais = p.id_local)
							group by p.id_local
                       having count(a.id_avaliacao) <> p.qtd_avaliacao) LOOP
    update local 
       set qtd_avaliacao = qtd_avaliacao + cur_quantidade.qtd_avaliacao
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(d.id_dica) qtd_dica
                             from local l, dica d
                            where l.id_local = d.local_dica	
                            group by l.id_local
                           having count(d.id_dica) <> l.qtd_dica) LOOP
        update local 
           set qtd_dica = cur_quantidade.qtd_dica
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select c.id_local, count(d.id_dica) qtd_dica
							from local c, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_cidade = c.id_local)
							group by c.id_local
                       having count(d.id_dica) <> c.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select e.id_local, count(d.id_dica) qtd_dica
							from local e, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_estado = e.id_local)
							group by e.id_local
                       having count(d.id_dica) <> e.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select p.id_local, count(d.id_dica) qtd_dica
							from local p, dica d
							where exists (select 1 from local l where l.id_local = d.local_dica and l.id_local_pais = p.id_local)
							group by p.id_local
                       having count(d.id_dica) <> p.qtd_dica) LOOP
    update local 
       set qtd_dica = qtd_dica + cur_quantidade.qtd_dica
     where id_local = cur_quantidade.id_local;

     qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND l.id_local = f.id_local
							group by l.id_local
                            having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_cidade = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_estado = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
    
    FOR cur_quantidade IN (select l.id_local, count(f.id_foto) qtd_foto
							from local l, foto f
							where f.anonima = false
							AND exists (
							select 1 from local lx where f.id_local = lx.id_local and lx.id_local_pais = l.id_local)
							group by l.id_local
                           having count(f.id_foto) <> l.qtd_foto) LOOP
        update local 
           set qtd_foto = qtd_foto + cur_quantidade.qtd_foto
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(n.id_noticia_sobre_local) qtd_noticia
                             from local l, noticia_sobre_local_para_local n
                            where l.id_local = n.id_local_interessado
                            group by l.id_local
                           having count(n.id_noticia_sobre_local) <> l.qtd_noticia) LOOP
        update local 
           set qtd_noticia = cur_quantidade.qtd_noticia
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(p.id_pergunta) qtd_pergunta
                             from local l, pergunta p
                            where l.id_local = p.id_local
                            group by l.id_local
                           having count(p.id_pergunta) <> l.qtd_pergunta) LOOP
        update local 
           set qtd_pergunta = cur_quantidade.qtd_pergunta
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_desejar_ir = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_deseja_ir) LOOP
        update local
           set qtd_interesse_deseja_ir = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_imperdivel = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_imperdivel) LOOP
        update local
           set qtd_interesse_imperdivel = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_indica = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_indica) LOOP
        update local
           set qtd_interesse_indica = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_ja_foi = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_ja_foi) LOOP
        update local
           set qtd_interesse_ja_foi = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_local_favorito = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_local_favorito) LOOP
        update local
           set qtd_interesse_local_favorito = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and i.interesse_seguir = true
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse_seguir) LOOP
        update local
           set qtd_interesse_seguir = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_quantidade IN (select l.id_local, count(i.id_interesse_usuario_em_local) qtd
                             from local l, interesse_usuario_em_local i
                            where l.id_local = i.id_local_de_interesse
							  and (i.interesse_desejar_ir = true
									OR i.interesse_local_favorito = true
									OR i.interesse_imperdivel = true
									OR i.interesse_indica = true
									OR i.interesse_ja_foi = true
									OR i.interesse_seguir = true)
                            group by l.id_local
                           having count(i.id_interesse_usuario_em_local) <> l.qtd_interesse) LOOP
        update local
           set qtd_interesse = cur_quantidade.qtd
         where id_local = cur_quantidade.id_local;

         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$BODY$
LANGUAGE plpgsql;