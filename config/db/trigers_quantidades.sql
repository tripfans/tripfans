CREATE OR REPLACE FUNCTION prc_tgr_amizade() 
    RETURNS TRIGGER 
AS $tgr_amizade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_amigo = qtd_amigo - 1 where id_usuario = old.id_usuario;
        RETURN old;
    ELSIF (TG_OP = 'UPDATE') THEN
        IF ((new.removida_tripfans AND (NOT old.removida_tripfans)) OR (new.removida_facebook AND (NOT old.removida_facebook))) THEN
        	update usuario set qtd_amigo = qtd_amigo - 1 where id_usuario = new.id_usuario;
        	RETURN new;
        ELSIF IF ((old.removida_tripfans AND (NOT new.removida_tripfans)) OR (old.removida_facebook AND (NOT new.removida_facebook))) THEN
        	update usuario set qtd_amigo = qtd_amigo + 1 where id_usuario = new.id_usuario;
        	RETURN new;
        END IF;
    END IF;
	
    update usuario set qtd_amigo = qtd_amigo + 1 where id_usuario = new.id_usuario;
    RETURN new;
END;
$tgr_amizade$ language plpgsql;

CREATE TRIGGER tgr_amizade
AFTER INSERT OR UPDATE OR DELETE ON amizade
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_amizade();

CREATE OR REPLACE FUNCTION prc_tgr_avaliacao()
  RETURNS trigger AS
$BODY$
DECLARE
 v_count integer;
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_avaliacao = qtd_avaliacao - 1 where id_local = old.id_local_avaliado;
        update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_cidade = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_estado = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_pais = l.id_local);
		update local l set qtd_avaliacao = qtd_avaliacao - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local_avaliado and l1.id_local_continente = l.id_local);
        update usuario set qtd_avaliacao = qtd_avaliacao - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
	
    update local set qtd_avaliacao = qtd_avaliacao + 1 where id_local = new.id_local_avaliado;
    update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_cidade = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_estado = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_pais = l.id_local);
	update local l set qtd_avaliacao = qtd_avaliacao + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local_avaliado and l1.id_local_continente = l.id_local);
    update usuario set qtd_avaliacao = qtd_avaliacao + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tgr_avaliacao
AFTER INSERT OR DELETE ON avaliacao
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_avaliacao();

CREATE OR REPLACE FUNCTION prc_tgr_comentario() 
    RETURNS TRIGGER 
AS $tgr_comentario$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_comentario = qtd_comentario - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
	
    update usuario set qtd_comentario = qtd_comentario + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$tgr_comentario$ language plpgsql;

CREATE TRIGGER tgr_comentario
AFTER INSERT OR DELETE ON comentario
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_comentario();

CREATE OR REPLACE FUNCTION prc_tgr_dica_quantidade() 
    RETURNS TRIGGER 
AS $tgr_dica_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_dica = qtd_dica - 1 where id_local = old.local_dica;
        update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_cidade = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_estado = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_pais = l.id_local);
		update local l set qtd_dica = qtd_dica - 1 where exists (select 1 from local l1 where l1.id_local = old.local_dica and l1.id_local_continente = l.id_local);
		update usuario set qtd_dica = qtd_dica - 1 where id_usuario = old.autor;
        update atividade set qtd_dicas = qtd_dicas - 1 where id = old.id_atividade;
        RETURN old;
    END IF;
    update local set qtd_dica = qtd_dica + 1 where id_local = new.local_dica;
    update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_cidade = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_estado = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_pais = l.id_local);
	update local l set qtd_dica = qtd_dica + 1 where exists (select 1 from local l1 where l1.id_local = new.local_dica and l1.id_local_continente = l.id_local);
    update usuario set qtd_dica = qtd_dica + 1 where id_usuario = new.autor;
    update atividade set qtd_dicas = qtd_dicas + 1 where id = new.id_atividade;
    RETURN new;
END;
$tgr_dica_quantidade$ language plpgsql;

CREATE TRIGGER tgr_dica_quantidade
AFTER INSERT OR DELETE ON dica
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_dica_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_foto_quantidade() 
    RETURNS TRIGGER 
AS $tgr_foto_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_foto = qtd_foto - 1 where id_local = old.id_local;
        update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_cidade = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_estado = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_pais = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto - 1 where exists (select 1 from local l1 where l1.id_local = old.id_local and l1.id_local_continente = l.id_local and l1.local_type = 7);
        update album set quantidade_fotos = quantidade_fotos - 1 where id = old.id_album;
		update usuario set qtd_foto = qtd_foto - 1 where id_usuario = old.id_autor;
        RETURN old;
   	END IF;
    IF (TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND old.anonima = true)) THEN
	    update local set qtd_foto = qtd_foto + 1 where id_local = new.id_local;
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_cidade = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_estado = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_pais = l.id_local and l1.local_type = 7);
	    update local l set qtd_foto = qtd_foto + 1 where exists (select 1 from local l1 where l1.id_local = new.id_local and l1.id_local_continente = l.id_local and l1.local_type = 7);
	    update album set quantidade_fotos = quantidade_fotos + 1 where id = new.id_album;
	    update usuario set qtd_foto = qtd_foto + 1 where id_usuario = new.id_autor and new.anonima = false;
	    RETURN new;
    END IF;
    RETURN new;
END;
$tgr_foto_quantidade$ language plpgsql;

CREATE TRIGGER tgr_foto_quantidade
AFTER INSERT OR DELETE OR UPDATE ON foto
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_foto_quantidade();
    
CREATE OR REPLACE FUNCTION prc_tgr_foto_atividade_quantidade() 
    RETURNS TRIGGER 
AS $tgr_foto_atividade_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update atividade set qtd_fotos = qtd_fotos - 1 where id = old.id_atividade;      
        RETURN old;
    END IF;
    IF (TG_OP = 'INSERT') THEN
        update atividade set qtd_fotos = qtd_fotos + 1 where id = new.id_atividade;      
        RETURN new;
    END IF;
    RETURN new;
END;
$tgr_foto_atividade_quantidade$ language plpgsql;

CREATE TRIGGER tgr_foto_atividade_quantidade
AFTER INSERT OR DELETE OR UPDATE ON foto_atividade
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_foto_atividade_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_noticia_quantidade() 
    RETURNS TRIGGER 
AS $tgr_noticia_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_noticia = qtd_noticia - 1 where id_local = old.id_local_interessado;
        RETURN old;
    END IF;
    update local set qtd_noticia = qtd_noticia + 1 where id_local = new.id_local_interessado;
    RETURN new;
END;
$tgr_noticia_quantidade$ language plpgsql;

CREATE TRIGGER tgr_noticia_quantidade
AFTER INSERT OR DELETE ON noticia_sobre_local_para_local
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_noticia_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_noticia_usuario() 
    RETURNS TRIGGER 
AS $tgr_noticia_usuario$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_noticia = qtd_noticia - 1 where id_usuario = old.id_usuario_interessado;
        RETURN old;
    END IF;
    update usuario set qtd_noticia = qtd_noticia + 1 where id_usuario = new.id_usuario_interessado;
    RETURN new;
END;
$tgr_noticia_usuario$ language plpgsql;

CREATE TRIGGER tgr_noticia_usuario
AFTER INSERT OR DELETE ON noticia_sobre_local_para_usuario
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_noticia_usuario();

CREATE OR REPLACE FUNCTION prc_tgr_pergunta_quantidade() 
    RETURNS TRIGGER 
AS $tgr_pergunta_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update local set qtd_pergunta = qtd_pergunta - 1 where id_local = old.id_local;
        update usuario set qtd_pergunta = qtd_pergunta - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
    update local set qtd_pergunta = qtd_pergunta + 1 where id_local = new.id_local;
    update usuario set qtd_pergunta = qtd_pergunta + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$tgr_pergunta_quantidade$ language plpgsql;

CREATE TRIGGER tgr_pergunta_quantidade
AFTER INSERT OR DELETE ON pergunta
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_pergunta_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_resposta_quantidade() 
    RETURNS TRIGGER 
AS $tgr_resposta_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_resposta = qtd_resposta - 1 where id_usuario = old.id_autor;
        RETURN old;
    END IF;
    update usuario set qtd_resposta = qtd_resposta + 1 where id_usuario = new.id_autor;
    RETURN new;
END;
$tgr_resposta_quantidade$ language plpgsql;

CREATE TRIGGER tgr_resposta_quantidade
AFTER INSERT OR DELETE ON resposta
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_resposta_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_viagem_quantidade() 
    RETURNS TRIGGER 
AS $tgr_viagem_quantidade$
BEGIN
    IF (TG_OP = 'DELETE') THEN 
        update usuario set qtd_viagem = qtd_viagem - 1 where id_usuario = old.criador;
        RETURN old;
    END IF;
    update usuario set qtd_viagem = qtd_viagem + 1 where id_usuario = new.criador;
    RETURN new;
END;
$tgr_viagem_quantidade$ language plpgsql;

CREATE TRIGGER tgr_viagem_quantidade
AFTER INSERT OR DELETE ON viagem
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_viagem_quantidade();

CREATE OR REPLACE FUNCTION prc_tgr_interesse_quantidade() 
    RETURNS TRIGGER 
AS $tgr_interesse_quantidade$
BEGIN
	IF (TG_OP = 'INSERT') THEN
	
		IF (new.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF (new.interesse_desejar_ir
				OR new.interesse_escreveu_avaliacao
				OR new.interesse_escreveu_dica
				OR new.interesse_escreveu_pergunta
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_respondeu_pergunta
				OR new.interesse_seguir
				) THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse + 1 WHERE id_usuario = new.id_usuario_interessado;
		END IF;
		
		IF (new.interesse_desejar_ir
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_seguir
				) THEN
			UPDATE local SET qtd_interesse = qtd_interesse + 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		RETURN new;
    ELSIF (TG_OP = 'UPDATE') THEN 
	
		IF ((NOT old.interesse_desejar_ir) AND new.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_desejar_ir AND (NOT new.interesse_desejar_ir)) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_imperdivel) AND new.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_imperdivel AND (NOT new.interesse_imperdivel)) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_indica) AND new.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_indica AND (NOT new.interesse_indica)) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_ja_foi) AND new.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_ja_foi AND (NOT new.interesse_ja_foi)) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_local_favorito) AND new.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_local_favorito AND (NOT new.interesse_local_favorito)) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;
		
		IF ((NOT old.interesse_seguir) AND new.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir + 1 WHERE id_local = new.id_local_de_interesse;
		ELSIF (old.interesse_seguir AND (NOT new.interesse_seguir)) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_usuario = new.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_local = new.id_local_de_interesse;
		END IF;

		IF ((old.interesse_desejar_ir
				OR old.interesse_escreveu_avaliacao
				OR old.interesse_escreveu_dica
				OR old.interesse_escreveu_pergunta
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_respondeu_pergunta
				OR old.interesse_seguir
				) 
			 AND (NOT new.interesse_desejar_ir
				AND NOT new.interesse_escreveu_avaliacao
				AND NOT new.interesse_escreveu_dica
				AND NOT new.interesse_escreveu_pergunta
				AND NOT new.interesse_local_favorito
				AND NOT new.interesse_imperdivel
				AND NOT new.interesse_indica
				AND NOT new.interesse_ja_foi
				AND NOT new.interesse_respondeu_pergunta
				AND NOT new.interesse_seguir
			 ))THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse - 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF ((new.interesse_desejar_ir
				OR new.interesse_escreveu_avaliacao
				OR new.interesse_escreveu_dica
				OR new.interesse_escreveu_pergunta
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_respondeu_pergunta
				OR new.interesse_seguir
				) 
			 AND (NOT old.interesse_desejar_ir
				AND NOT old.interesse_escreveu_avaliacao
				AND NOT old.interesse_escreveu_dica
				AND NOT old.interesse_escreveu_pergunta
				AND NOT old.interesse_local_favorito
				AND NOT old.interesse_imperdivel
				AND NOT old.interesse_indica
				AND NOT old.interesse_ja_foi
				AND NOT old.interesse_respondeu_pergunta
				AND NOT old.interesse_seguir
			 ))THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse + 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF ((old.interesse_desejar_ir
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_seguir
				) 
				AND (NOT new.interesse_desejar_ir
				AND NOT new.interesse_local_favorito
				AND NOT new.interesse_imperdivel
				AND NOT new.interesse_indica
				AND NOT new.interesse_ja_foi
				AND NOT new.interesse_seguir
				)) THEN
			UPDATE local SET qtd_interesse = qtd_interesse - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;

		IF ((new.interesse_desejar_ir
				OR new.interesse_local_favorito
				OR new.interesse_imperdivel
				OR new.interesse_indica
				OR new.interesse_ja_foi
				OR new.interesse_seguir
				) 
				AND (NOT old.interesse_desejar_ir
				AND NOT old.interesse_local_favorito
				AND NOT old.interesse_imperdivel
				AND NOT old.interesse_indica
				AND NOT old.interesse_ja_foi
				AND NOT old.interesse_seguir
				)) THEN
			UPDATE local SET qtd_interesse = qtd_interesse + 1 WHERE id_local = old.id_local_de_interesse;
		END IF;

		RETURN new;
    ELSIF (TG_OP = 'DELETE') THEN 
	
		IF (old.interesse_desejar_ir) THEN
			UPDATE usuario SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_deseja_ir = qtd_interesse_deseja_ir - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_imperdivel) THEN
			UPDATE usuario SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_imperdivel = qtd_interesse_imperdivel - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_indica) THEN
			UPDATE usuario SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_indica = qtd_interesse_indica - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_ja_foi) THEN
			UPDATE usuario SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_ja_foi = qtd_interesse_ja_foi - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_local_favorito) THEN
			UPDATE usuario SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_local_favorito = qtd_interesse_local_favorito - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_seguir) THEN
			UPDATE usuario SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_usuario = old.id_usuario_interessado;
			UPDATE local SET qtd_interesse_seguir = qtd_interesse_seguir - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		IF (old.interesse_desejar_ir
				OR old.interesse_escreveu_avaliacao
				OR old.interesse_escreveu_dica
				OR old.interesse_escreveu_pergunta
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_respondeu_pergunta
				OR old.interesse_seguir
				) THEN
			UPDATE usuario SET qtd_local_algum_interesse = qtd_local_algum_interesse - 1 WHERE id_usuario = old.id_usuario_interessado;
		END IF;
		
		IF (old.interesse_desejar_ir
				OR old.interesse_local_favorito
				OR old.interesse_imperdivel
				OR old.interesse_indica
				OR old.interesse_ja_foi
				OR old.interesse_seguir
				) THEN
			UPDATE local SET qtd_interesse = qtd_interesse - 1 WHERE id_local = old.id_local_de_interesse;
		END IF;
		
		RETURN old;
    END IF;
END;
$tgr_interesse_quantidade$ language plpgsql;

CREATE TRIGGER tgr_interesse_quantidade
AFTER INSERT OR UPDATE OR DELETE ON interesse_usuario_em_local
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_interesse_quantidade();
    
CREATE OR REPLACE FUNCTION prc_tgr_atividade() 
    RETURNS TRIGGER 
AS $tgr_atividade$
BEGIN
    IF (TG_OP = 'DELETE' AND old.visivel) THEN 
        update usuario set qtd_atividade = qtd_atividade - 1 where id_usuario = old.id_usuario_autor;
        RETURN old;
    END IF;
	
    IF (new.visivel) THEN
    	update usuario set qtd_atividade = qtd_atividade + 1 where id_usuario = new.id_usuario_autor;
    	update usuario set data_ultima_acao = current_timestamp where id_usuario = new.id_usuario_autor;
    	RETURN new;
    END IF;
    RETURN new;
END;
$tgr_atividade$ language plpgsql;

CREATE TRIGGER tgr_atividade
AFTER INSERT OR DELETE ON acao_usuario
    FOR EACH ROW EXECUTE PROCEDURE prc_tgr_atividade();

