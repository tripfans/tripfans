CREATE OR REPLACE FUNCTION consolidar_quantidades_local_geografico()
  RETURNS integer AS
$BODY$
DECLARE
	c_tp_aeroporto constant local.local_type%type := 5;
	c_tp_agencia constant local.local_type%type := 6;
	c_tp_atracao constant local.local_type%type := 7;
	c_tp_hotel constant local.local_type%type := 8;
	c_tp_imovel_aluguel_temporada constant local.local_type%type := 9;
	c_tp_locadora_veiculos constant local.local_type%type := 11;
	c_tp_restaurante constant local.local_type%type := 10;

	c_tp_continente constant local.local_type%type := 1;
	c_tp_pais constant local.local_type%type := 2;
	c_tp_estado constant local.local_type%type := 3;
	c_tp_cidade constant local.local_type%type := 4;
	
    qtd_registros_alterados integer;
	
    cur_local_geografico RECORD;
    cur_quantidade RECORD;

	v_qtd_aeroporto local.qtd_aeroporto%type;
	v_qtd_agencia local.qtd_agencia%type;
	v_qtd_atracao local.qtd_atracao%type;
	v_qtd_hotel local.qtd_hotel%type;
	v_qtd_restaurante local.qtd_restaurante%type;
	v_qtd_cidade local.qtd_cidade%type;
	v_qtd_estado local.qtd_estado%type;
	v_qtd_pais local.qtd_pais%type;
	
BEGIN
    qtd_registros_alterados := 0;

    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_continente) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_continente = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_pais) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_pais = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_estado) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_estado = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    FOR cur_local_geografico IN (select l.id_local
                                   from local l
                                  where l.local_type = c_tp_cidade) LOOP
		
		v_qtd_aeroporto := 0;
		v_qtd_agencia := 0;
		v_qtd_atracao := 0;
		v_qtd_hotel := 0;
		v_qtd_restaurante := 0;
		v_qtd_cidade := 0;
		v_qtd_estado := 0;
		v_qtd_pais := 0;
		
		FOR cur_quantidade IN (select l.local_type, count(l.id_local) qtd_tipo
							     from local l
							    where l.id_local_cidade = cur_local_geografico.id_local
								group by l.local_type) LOOP

			IF(cur_quantidade.local_type = c_tp_aeroporto)THEN
				v_qtd_aeroporto := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_agencia)THEN
				v_qtd_agencia := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_atracao)THEN
				v_qtd_atracao := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_hotel)THEN
				v_qtd_hotel := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_restaurante)THEN
				v_qtd_restaurante := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_cidade)THEN
				v_qtd_cidade := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_estado)THEN
				v_qtd_estado := cur_quantidade.qtd_tipo;
			ELSIF(cur_quantidade.local_type = c_tp_pais)THEN
				v_qtd_pais := cur_quantidade.qtd_tipo;
			END IF;
			
		END LOOP;

		UPDATE local
		   SET qtd_aeroporto = v_qtd_aeroporto
		     , qtd_agencia = v_qtd_agencia
			 , qtd_atracao = v_qtd_atracao
			 , qtd_hotel = v_qtd_hotel
			 , qtd_restaurante = v_qtd_restaurante
			 , qtd_cidade = v_qtd_cidade
			 , qtd_estado = v_qtd_estado
			 , qtd_pais = v_qtd_pais
		 WHERE id_local = cur_local_geografico.id_local;
		
         qtd_registros_alterados := qtd_registros_alterados + 1;
    END LOOP;

    return qtd_registros_alterados;
END;
$BODY$
LANGUAGE plpgsql;
