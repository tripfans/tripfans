-- 1 - ranking_mais_visitadas 
update local set ranking_mais_visitadas = 1 where id_local = 5484;
update local set ranking_mais_visitadas = 2 where id_local = 1072;
update local set ranking_mais_visitadas = 3 where id_local = 3902;
update local set ranking_mais_visitadas = 4 where id_local = 1877;
update local set ranking_mais_visitadas = 5 where id_local = 806;
update local set ranking_mais_visitadas = 6 where id_local = 3124;
update local set ranking_mais_visitadas = 7 where id_local = 4714;
update local set ranking_mais_visitadas = 8 where id_local = 5381;
update local set ranking_mais_visitadas = 9 where id_local = 946;
update local set ranking_mais_visitadas = 10 where id_local = 4396;
update local set ranking_mais_visitadas = 11 where id_local = 5028;
update local set ranking_mais_visitadas = 12 where id_local = 1242;
update local set ranking_mais_visitadas = 13 where id_local = 3562;
update local set ranking_mais_visitadas = 14 where id_local = 4654;
update local set ranking_mais_visitadas = 15 where id_local = 1197;
update local set ranking_mais_visitadas = 16 where id_local = 5133;
update local set ranking_mais_visitadas = 17 where id_local = 5040;
update local set ranking_mais_visitadas = 18 where id_local = 3847;
update local set ranking_mais_visitadas = 19 where id_local = 4013;
update local set ranking_mais_visitadas = 20 where id_local = 2902;
update local set ranking_mais_visitadas = 21 where id_local = 781;
update local set ranking_mais_visitadas = 22 where id_local = 5543;
update local set ranking_mais_visitadas = 23 where id_local = 2636;
update local set ranking_mais_visitadas = 24 where id_local = 1577;
update local set ranking_mais_visitadas = 25 where id_local = 5174;
update local set ranking_mais_visitadas = 26 where id_local = 2683;
update local set ranking_mais_visitadas = 27 where id_local = 5345;
update local set ranking_mais_visitadas = 28 where id_local = 5567;
update local set ranking_mais_visitadas = 29 where id_local = 3836;
update local set ranking_mais_visitadas = 30 where id_local = 4947;

--------------------------------------------------------------------------------------------------

-- 2 - destino_indutor 

update local set destino_indutor = true where id_local = 1160;
update local set destino_indutor = true where id_local = 3836;
update local set destino_indutor = true where id_local = 5567;
update local set destino_indutor = true where id_local = 900;
update local set destino_indutor = true where id_local = 3840;
update local set destino_indutor = true where id_local = 4654;
update local set destino_indutor = true where id_local = 418;
update local set destino_indutor = true where id_local = 1418;
update local set destino_indutor = true where id_local = 2683;
update local set destino_indutor = true where id_local = 1877;
update local set destino_indutor = true where id_local = 4132;
update local set destino_indutor = true where id_local = 4614;
update local set destino_indutor = true where id_local = 1750;
update local set destino_indutor = true where id_local = 1072;
update local set destino_indutor = true where id_local = 1627;
update local set destino_indutor = true where id_local = 1197;
update local set destino_indutor = true where id_local = 1754;
update local set destino_indutor = true where id_local = 1760;
update local set destino_indutor = true where id_local = 1643;
update local set destino_indutor = true where id_local = 3124;
update local set destino_indutor = true where id_local = 2052;
update local set destino_indutor = true where id_local = 3490;
update local set destino_indutor = true where id_local = 4714;
update local set destino_indutor = true where id_local = 946;
update local set destino_indutor = true where id_local = 3150;
update local set destino_indutor = true where id_local = 1242;
update local set destino_indutor = true where id_local = 4261;
update local set destino_indutor = true where id_local = 5156;
update local set destino_indutor = true where id_local = 3508;
update local set destino_indutor = true where id_local = 985;
update local set destino_indutor = true where id_local = 2902;
update local set destino_indutor = true where id_local = 703;
update local set destino_indutor = true where id_local = 401;
update local set destino_indutor = true where id_local = 339;
update local set destino_indutor = true where id_local = 448;
update local set destino_indutor = true where id_local = 342;
update local set destino_indutor = true where id_local = 720;
update local set destino_indutor = true where id_local = 723;
update local set destino_indutor = true where id_local = 5713;
update local set destino_indutor = true where id_local = 4013;
update local set destino_indutor = true where id_local = 1009;
update local set destino_indutor = true where id_local = 2351;
update local set destino_indutor = true where id_local = 5728;
update local set destino_indutor = true where id_local = 3284;
update local set destino_indutor = true where id_local = 3887;
update local set destino_indutor = true where id_local = 456;
update local set destino_indutor = true where id_local = 3764;
update local set destino_indutor = true where id_local = 3889;
update local set destino_indutor = true where id_local = 1330;
update local set destino_indutor = true where id_local = 4396;
update local set destino_indutor = true where id_local = 781;
update local set destino_indutor = true where id_local = 4596;
update local set destino_indutor = true where id_local = 3562;
update local set destino_indutor = true where id_local = 287;
update local set destino_indutor = true where id_local = 3902;
update local set destino_indutor = true where id_local = 806;
update local set destino_indutor = true where id_local = 2777;
update local set destino_indutor = true where id_local = 4873;
update local set destino_indutor = true where id_local = 1577;
update local set destino_indutor = true where id_local = 5484;
update local set destino_indutor = true where id_local = 3817;
update local set destino_indutor = true where id_local = 3827;
update local set destino_indutor = true where id_local = 4082;
update local set destino_indutor = true where id_local = 2619;
update local set destino_indutor = true where id_local = 1149;


--------------------------------------------------------------------------------------------------

-- 3 - destaque_4_rodas 

update local set destaque_4_rodas = true where id_local = 4654;
update local set destaque_4_rodas = true where id_local = 2683;
update local set destaque_4_rodas = true where id_local = 1877;
update local set destaque_4_rodas = true where id_local = 4132;
update local set destaque_4_rodas = true where id_local = 4669;
update local set destaque_4_rodas = true where id_local = 1750;
update local set destaque_4_rodas = true where id_local = 1072;
update local set destaque_4_rodas = true where id_local = 1197;
update local set destaque_4_rodas = true where id_local = 5030;
update local set destaque_4_rodas = true where id_local = 3124;
update local set destaque_4_rodas = true where id_local = 3490;
update local set destaque_4_rodas = true where id_local = 4714;
update local set destaque_4_rodas = true where id_local = 946;
update local set destaque_4_rodas = true where id_local = 3150;
update local set destaque_4_rodas = true where id_local = 4261;
update local set destaque_4_rodas = true where id_local = 3707;
update local set destaque_4_rodas = true where id_local = 5156;
update local set destaque_4_rodas = true where id_local = 649;
update local set destaque_4_rodas = true where id_local = 339;
update local set destaque_4_rodas = true where id_local = 448;
update local set destaque_4_rodas = true where id_local = 4013;
update local set destaque_4_rodas = true where id_local = 2351;
update local set destaque_4_rodas = true where id_local = 3887;
update local set destaque_4_rodas = true where id_local = 3889;
update local set destaque_4_rodas = true where id_local = 4396;
update local set destaque_4_rodas = true where id_local = 781;
update local set destaque_4_rodas = true where id_local = 3562;
update local set destaque_4_rodas = true where id_local = 3902;
update local set destaque_4_rodas = true where id_local = 806;
update local set destaque_4_rodas = true where id_local = 5484;
update local set destaque_4_rodas = true where id_local = 5488;
update local set destaque_4_rodas = true where id_local = 2619;


--------------------------------------------------------------------------------------------------

-- 4 - ranking_mais_desejadas 

update local set ranking_mais_desejadas = 1 where id_local = 3490;
update local set ranking_mais_desejadas = 2 where id_local = 946;
update local set ranking_mais_desejadas = 3 where id_local = 806;
update local set ranking_mais_desejadas = 4 where id_local = 3902;
update local set ranking_mais_desejadas = 5 where id_local = 4013;
update local set ranking_mais_desejadas = 6 where id_local = 3562;
update local set ranking_mais_desejadas = 7 where id_local = 781;
update local set ranking_mais_desejadas = 8 where id_local = 4714;
update local set ranking_mais_desejadas = 9 where id_local = 4261;
update local set ranking_mais_desejadas = 10 where id_local = 448;
update local set ranking_mais_desejadas = 11 where id_local = 5484;
update local set ranking_mais_desejadas = 12 where id_local = 3508;
update local set ranking_mais_desejadas = 13 where id_local = 339;
update local set ranking_mais_desejadas = 14 where id_local = 4947;
update local set ranking_mais_desejadas = 15 where id_local = 3150;
update local set ranking_mais_desejadas = 16 where id_local = 4396;
update local set ranking_mais_desejadas = 17 where id_local = 1072;
update local set ranking_mais_desejadas = 18 where id_local = 3124;
update local set ranking_mais_desejadas = 19 where id_local = 1750;
update local set ranking_mais_desejadas = 20 where id_local = 1877;

--------------------------------------------------------------------------------------------------

-- 5 - sede_copa_2014 

update local set sede_copa_2014 = true where lower(nome) in (
'belo horizonte',
'brasília',
'cuiabá',
'curitiba',
'fortaleza',
'manaus',
'natal',
'porto alegre',
'recife',
'rio de janeiro',
'salvador',
'são paulo'
)
and local_type= 4


--------------------------------------------------------------------------------------------------


-- 6 - beneficiada_copa_2014 

update local set beneficiada_copa_2014 = true where lower(nome) in (
'alcântara',
'alta floresta',
'autazes',
'alto paraíso de goiás',
'anchieta',
'angra dos reis',
'aparecida',
'aquidauana',
'aquiraz',
'aracati',
'aracruz',
'araxá',
'armação dos búzios',
'arraial do cabo',
'axixá',
'baía formosa',
'balneário camboriú',
'bananeiras',
'barão de melgaço',
'barra de santo antônio',
'barra de são miguel',
'barra do cunhaú',
'barreirinhas',
'beberibe',
'belém',
'bento gonçalves',
'blumenau',
'boipeba',
'bombinhas',
'bonito',
'brotas',
'brumadinho',
'cabedelo',
'cabo de santo agostinho',
'cáceres',
'caldas novas',
'camaçari',
'cambará do sul',
'camocim',
'campina grande',
'campo verde',
'campos do jordão',
'canavieiras',
'canela',
'canguaretama',
'caraíva',
'caravelas',
'careiro',
'caruaru',
'cascavel',
'caucaia',
'caxambu',
'chapada dos guimarães',
'goiás',
'conceição da barra',
'conde',
'conde',
'congonhas',
'cordisburgo',
'coronel xavier chaves',
'corumbá',
'cruz',
'curuípe',
'diamantina',
'esperança',
'fernando de noronha',
'florianópolis',
'foz do iguaçu',
'galinhos',
'galos',
'garopaba',
'gramado',
'guapimirim',
'guarabira',
'guarapari',
'icapuí',
'ilha grande',
'ilhabela',
'imbituba',
'ingá',
'ipojuca',
'iranduba',
'itacaré',
'itacoatiara',
'itapema',
'jaciara',
'jandaíra',
'japaratinga',
'jijoca de jericoacoara',
'joão pessoa',
'juscimeira',
'lagoa dourada',
'lagoa santa',
'lambari',
'linhares',
'lucena',
'luís correia',
'macau',
'maceió',
'manacapuru',
'manaquiri',
'maragogi',
'maraú',
'marechal deodoro',
'mariana',
'mata de são joão',
'mateiros',
'miranda',
'morretes',
'morro de são paulo',
'morros',
'muro alto',
'niterói',
'nobres',
'nova friburgo',
'nova petrópolis',
'olinda',
'ouro preto',
'paço do lumiar',
'paracuru',
'paranaguá',
'paraipaba',
'paraty',
'parintins',
'paripueira',
'parnaíba',
'paulista',
'petrópolis',
'pirenópolis',
'pocinhos',
'poconé',
'ponte alta do tocantins',
'porto de pedras',
'porto do mangue',
'porto seguro',
'prado',
'prados',
'presidente figueiredo',
'raposa',
'resende costa',
'rio preto da eva',
'sabará',
'santa cruz cabrália',
'santo amaro do maranhão',
'são félix do tocantins',
'são francisco de paula',
'são francisco do sul',
'são gonçalo do amarante',
'são joão del rei',
'são jorge',
'cavalcante',
'são josé de ribamar',
'são lourenço',
'são luís',
'são mateus',
'são miguel do gostoso',
'são miguel dos milagres',
'serra',
'serro',
'sete lagoas',
'silves',
'socorro',
'teresópolis',
'tibau do sul',
'tiradentes',
'touros',
'trairi',
'três corações',
'vila flor',
'vila velha',
'vitória'
)
and local_type= 4
and id_local not in (301, 2833, 2837, 3454, 520, 2687, 309, 2935, 5722, 3278, 1707, 2982, 386, 2817, 4637, 2831, 3099, 3982, 2943, 2525, 4055, 4456)



-----------------------------------------------------------------------------------------

-- 7 - roteiro_4_rodas 

update local set roteiro_4_rodas = true where lower(nome) in (
'águas de lindóia',
'aiuruoca',
'alto caparaó',
'anchieta',
'angra dos reis',
'aparecida',
'aquidauana',
'aquiraz',
'aracaju',
'arembebe',
--'arraial d'ajuda',
'arraial do cabo',
'baía formosa',
'balneário camboriú',
'bananal',
'barra de santo antônio',
'barra de são miguel',
'barra do piraí',
'barra grande',
'beberibe',
'belo horizonte',
'bento gonçalves',
'bertioga',
'blumenau',
'boipeba',
'bom jardim da serra',
'bombinhas',
'bonito',
'brasília',
'brumadinho',
'brusque',
'armação dos búzios',
'cabo de santo agostinho',
'cabo frio',
'caeté',
'caldas novas',
'cambará do sul',
'campo grande',
'campos do jordão',
'canela',
--'canoa quebrada',
--'caraíva',
'caravelas',
'castro',
'catas altas',
'caxambu',
'caxias do sul',
'chapada dos guimarães',
--'chapada dos veadeiros',
--'conceição do ibitipoca',
'conceição do mato dentro',
'conde',
'congonhas',
--'conservatória',
'cordisburgo',
'corumbá',
'coruripe',
--'costa do sauipe',
'criciúma',
'cuiabá',
--'cumbuco',
--'cumuruxatiba',
'cunha',
'curitiba',
'diamantina',
'domingos martins',
'florianópolis',
'formosa',
'fortaleza',
'galinhos',
'garibaldi',
'garopaba',
--'genipabu',
'goiânia',
'goiás',
'gonçalves',
'gramado',
'guarapari',
--'guarda do embaú',
'guarujá',
--'icaraí de amontada',
--'ilha de itaparica',
--'ilha do mel',
--'ilha grande', -- RJ
'ilhabela',
'ilhéus',
--'imbassaí',
'imbituba',
'itacaré',
'itajaí',
'itatiaia',
'itatiba',
--'itaúnas',
'jaraguá do sul',
--'jericoacoara',
'jijoca de jericoacoara',
'joão pessoa',
'joinville',
'jundiaí',
'lagoa santa',
'lagoinha',
'lajes',
'lapa',
'laranjeiras',
'luís correia',
'maceió',
'mangaratiba',
--'mangue seco',
'maragogi',
'mariana',
--'milho verde',
'miranda',
--'monte verde',
'morretes',
--'morro de são paulo',
'morungaba',
'natal',
'niterói',
'nova friburgo',
'nova petrópolis',
'olinda',
'ouro preto',
'paranaguá',
'paraty',
'parnaíba',
'passa quatro',
'penedo',
--'penedo', -- RJ
'penha',
'petrópolis',
'pirenópolis',
'poconé',
'poços de caldas',
'pomerode',
--'ponta do corumbau',
'ponta grossa',
'porto alegre',
'porto belo',
--'porto de galinhas',
'porto seguro',
'posse',
'pouso alto',
'prado',
--'praia da pipa',
--'praia do espelho',
--'praia do forte',
--'praia do francês',
--'praia do rosa',
'recife',
'rio das flores',
'rio das ostras',
'rio de janeiro',
'rio quente',
'sabará',
'salvador',
'santa bárbara',
'santa cruz cabrália',
--'santo andré',
'santo antônio do pinhal',
'santos',
'são bento do sapucaí',
'são cristóvão',
'são francisco de paula',
'são francisco do sul',
--'são francisco xavier',
'são joão del rei',
'são joaquim',
'são josé do barreiro',
'são josé dos ausentes',
'são josé dos campos',
'são lourenço',
'são miguel do gostoso',
'são miguel dos milagres',
'são sebastião',
'saquarema',
--'serra do cipó',
'serra negra',
'serro',
'sete lagoas',
'socorro',
'tamandaré',
'teresópolis',
'tibagi',
'tibau do sul',
'timbó',
'tiradentes',
'touros',
'trairi',
--'trancoso',
'ubatuba',
'urubici',
'valença',
'valença',
'vassouras',
'venda nova do imigrante',
'vila velha',
--'visconde de mauá',
'vitória'
)
and local_type= 4
and id_local not in (301, 2833, 2837, 3454, 520, 2687, 309, 2935, 5722, 3278, 1707, 2982, 386, 2817, 4637, 2831, 3099, 3982, 2943, 2525, 4055, 4456, 807)
order by nome


-- MATERIALIZAÇÃO DO RANKING

update local set nota_ranking = ((case when ranking_mais_visitadas is not null then 31 - ranking_mais_visitadas else 0 end )) + 
       (case when (destino_indutor = true) then 0.9 else 0 end) + 
       (case when (destaque_4_rodas = true) then 0.8 else 0 end) +
       (case when ranking_mais_desejadas is not null then 0.7 + ((21 - ranking_mais_desejadas) / 100.0) else 0 end) + 
       (case when (sede_copa_2014 = true) then 0.6 else 0 end) + 
       (case when (beneficiada_copa_2014 = true) then 0.5 else 0 end) + 
       (case when (roteiro_4_rodas = true) then 0.4 else 0 end) + 
       (case when (melhor_do_ano_4_rodas = true) then 0.3 else 0 end) +
       (case when (destaque_outras_4_rodas = true) then 0.3 else 0 end)