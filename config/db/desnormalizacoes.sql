insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'AMC','América Central','continente_amc',false);
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'AMN','América do Norte','continente_amn',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'AMS','América do Sul','continente_ams',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'ANT','Antártida','continente_ant',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'EUR','Europa','continente_eur',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'OCE','Oceania','continente_oce',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'AFR','África','continente_afr',false);			
insert into local (local_type,id_local,sigla,nome,url_path,parceiro) values (1,nextval('seq_local'),'ASI','Ásia','continente_asi',false);

----------------------------------------------
-- Tratamento de Local Mais Específico
----------------------------------------------
update local 
   set d_local_mais_especifico_id = id_local
where d_local_mais_especifico_id is null;

----------------------------------------------
-- Tratamento de Continente
----------------------------------------------
update local 
   set d_continente_id = id_local
where local_type = 1
  and d_continente_id is null;

update local 
   set d_continente_id = xxx_america_do_sul
where id_local = xxx_brasil or id_local_pais = xxx_brasil;
----------------------------------------------
-- Tratamento de País
----------------------------------------------
update local 
   set d_pais_id = id_local
where local_type = 2
  and d_pais_id is null;

update local 
   set d_pais_id = id_local_pais
where d_pais_id is null
  and id_local_pais is not null;
----------------------------------------------
-- Tratamento de Estado
----------------------------------------------
update local 
   set d_estado_id = id_local
where local_type = 3
  and d_estado_id is null;

update local 
   set d_estado_id = id_local_estado
where d_estado_id is null
  and id_local_estado is not null;

----------------------------------------------
-- Tratamento de Cidade
----------------------------------------------
update local 
   set d_cidade_id = id_local
where local_type = 4
  and d_cidade_id is null;

update local 
   set d_cidade_id = id_local_cidade
where d_cidade_id is null
  and id_local_cidade is not null;

----------------------------------------------
-- Tratamento de Local Enderecavel
----------------------------------------------
update local 
   set d_local_enderecavel_id = id_local
where local_type in (5,6,7,8,9,10,11)
  and d_local_enderecavel_id is null;

  
----------------------------------------------
-- Tratamento de Dica
----------------------------------------------
update dica
   set d_local_mais_especifico_id = local_dica
 where d_local_mais_especifico_id <> local_dica
   or (d_local_mais_especifico_id is null and local_dica is not null)
   or (d_local_mais_especifico_id is not null and local_dica is null);
   
----------------------------------------------
-- Campos desnormalizados
----------------------------------------------


create table tabela_desnormalizada(
   nm_tabela varchar
);

ALTER TABLE tabela_desnormalizada
ADD CONSTRAINT tabela_desnormalizada_pkey
PRIMARY KEY (nm_tabela);
 
insert into  tabela_desnormalizada(nm_tabela) values ('acao_usuario');
insert into  tabela_desnormalizada(nm_tabela) values ('dica');
insert into  tabela_desnormalizada(nm_tabela) values ('noticia_sobre_local_para_local');