CREATE OR REPLACE FUNCTION popular_desnormalizacao_nome_cidade_estado_pais_continente()
  RETURNS integer AS
$BODY$
DECLARE
	c_tp_continente constant local.local_type%type := 1;
	c_tp_pais constant local.local_type%type := 2;
	c_tp_estado constant local.local_type%type := 3;
	c_tp_cidade constant local.local_type%type := 4;
	
    qtd_updates_executados integer;
	
    cur_local_geografico RECORD;
BEGIN
    qtd_updates_executados := 0;

    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path
                                   from local l
                                  where l.local_type = c_tp_continente) LOOP
		
		UPDATE local
		   SET nm_local_continente = cur_local_geografico.nome
		     , url_path_local_continente = cur_local_geografico.url_path
		 WHERE id_local_continente = cur_local_geografico.id_local
		   AND ((nm_local_continente is null or nm_local_continente <> cur_local_geografico.nome)
		       OR (url_path_local_continente is null or url_path_local_continente <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path, l.sigla_iso
                                   from local l
                                  where l.local_type = c_tp_pais) LOOP
		
		UPDATE local
		   SET nm_local_pais = cur_local_geografico.nome
		     , url_path_local_pais = cur_local_geografico.url_path
 		     , sigla_iso_pais = cur_local_geografico.sigla_iso
		 WHERE id_local_pais = cur_local_geografico.id_local
		   AND ((nm_local_pais is null or nm_local_pais <> cur_local_geografico.nome)
		       OR (url_path_local_pais is null or url_path_local_pais <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;
	
    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path, l.sigla
                                   from local l
                                  where l.local_type = c_tp_estado) LOOP
		
		UPDATE local
		   SET nm_local_estado = cur_local_geografico.nome
		     , url_path_local_estado = cur_local_geografico.url_path
		     , sg_local_estado = cur_local_geografico.sigla
		 WHERE id_local_estado = cur_local_geografico.id_local
		   AND (   (nm_local_estado is null or nm_local_estado <> cur_local_geografico.nome)
		        OR (url_path_local_estado is null or url_path_local_estado <> cur_local_geografico.url_path)
		        OR (cur_local_geografico.sigla is not null and (sg_local_estado is null or sg_local_estado <> cur_local_geografico.sigla))
		       );
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;

    FOR cur_local_geografico IN (select l.id_local, l.nome, l.url_path
                                   from local l
                                  where l.local_type = c_tp_cidade) LOOP
		
		UPDATE local
		   SET nm_local_cidade = cur_local_geografico.nome
		     , url_path_local_cidade = cur_local_geografico.url_path
		 WHERE id_local_cidade = cur_local_geografico.id_local
		   AND ((nm_local_cidade is null or nm_local_cidade <> cur_local_geografico.nome)
		       OR (url_path_local_cidade is null or url_path_local_cidade <> cur_local_geografico.url_path));
		
        qtd_updates_executados := qtd_updates_executados + 1;
    END LOOP;

    return qtd_updates_executados;
END;
$BODY$
  LANGUAGE plpgsql;
